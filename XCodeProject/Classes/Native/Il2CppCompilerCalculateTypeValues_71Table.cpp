﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action`1<Zenject.DiContainer>
struct Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Zenject.SingletonId,Zenject.SingletonTypes>
struct Dictionary_2_t855EDD5F3748FD51AB316D8DBB6939DDE51A9DC4;
// System.Collections.Generic.Dictionary`2<Zenject.SingletonId,Zenject.StandardSingletonProviderCreator/ProviderInfo>
struct Dictionary_2_t7591F89A805922AD4F3EDE546ADC77E45B43A7A9;
// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByInstaller/InstallerSingletonId,Zenject.ISubContainerCreator>
struct Dictionary_2_t1FA0B8DA3B73D042A0D9F7C1048EF1A3DC525109;
// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByMethod/MethodSingletonId,Zenject.ISubContainerCreator>
struct Dictionary_2_t50707A64E6E606785AF10F0AAF88016E6D471149;
// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByNewPrefab/CustomSingletonId,Zenject.SubContainerSingletonProviderCreatorByNewPrefab/CreatorInfo>
struct Dictionary_2_t8846C037DA2FFEC58347809EC9DF1CF50E18792C;
// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource/CustomSingletonId,Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource/CreatorInfo>
struct Dictionary_2_t51C3DD62CC36B685F4B94CC25BCAF2A2F0A8E3A3;
// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>
struct LinkedList_1_t5A41455EC6EC7657DD6CF98C6FDA1A581DAE472B;
// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>
struct LinkedList_1_tD95317941B347B3D6848946C2964299622936C16;
// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>
struct LinkedList_1_t37788D54EFBFA8ADC2543D7DEA0723ADDC65C484;
// System.Collections.Generic.List`1<ModestTree.Util.ValuePair`2<System.Type,System.Int32>>
struct List_1_t48E35A490E0E56462F4B2D7D1DEE685515F9EFA9;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>
struct List_1_t864E51A8327073D1D23F99573C5924B750FFBFC6;
// System.Collections.Generic.List`1<Zenject.DisposableManager/LateDisposableInfo>
struct List_1_tB13268EE35215DC383466A2878231B718FBC8308;
// System.Collections.Generic.List`1<Zenject.GuiRenderableManager/RenderableInfo>
struct List_1_tF2A9A0B0A1B439359488D57AA4E3CC7D5D43CEF2;
// System.Collections.Generic.List`1<Zenject.IAnimatorIkHandler>
struct List_1_tA80F85D05C3A7FAA27FB0C82AAD84F9057E80A77;
// System.Collections.Generic.List`1<Zenject.IAnimatorMoveHandler>
struct List_1_tB50850FDAC454C489717C889D75F7DF8C545254E;
// System.Collections.Generic.List`1<Zenject.IFixedTickable>
struct List_1_t9BE4861DF8744524532C1FECB05B1972B831140C;
// System.Collections.Generic.List`1<Zenject.ILateTickable>
struct List_1_tA91F547197BE53A34015BB0D06B83EDEFC988324;
// System.Collections.Generic.List`1<Zenject.ITickable>
struct List_1_tF214108319785C2E14A76893BD2A1BA818E54783;
// System.Collections.Generic.List`1<Zenject.InitializableManager/InitializableInfo>
struct List_1_t42AC613DAF8DB5D3D38B374B2FE88C08FC02B976;
// System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>
struct List_1_t5F62B547F88E623C272155861602CF84E918E93B;
// System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>
struct List_1_t4514D67474103773FAF8C8A0DB672AF1E9EE86EA;
// System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>
struct List_1_tCEB34192A8ED851461BFA0B92DFF27C4BD50564B;
// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7;
// System.Delegate
struct Delegate_t;
// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32>
struct Func_2_t9288B55256D27898799397C666D0F5F7C7368101;
// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Type>
struct Func_2_t82F87C5DB7A2AF00476C31B61CDE3B766ECF7EAC;
// System.Func`2<System.Reflection.MethodInfo,System.Boolean>
struct Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9;
// System.Func`2<Zenject.DisposableManager/DisposableInfo,System.Int32>
struct Func_2_t9F898882BCEEADF871BCCDEB458D8BA8BCBAD7AB;
// System.Func`2<Zenject.DisposableManager/LateDisposableInfo,System.Int32>
struct Func_2_tA4A50610B774C8F8FA0322E0064879FE564992C8;
// System.Func`2<Zenject.GuiRenderableManager/RenderableInfo,System.Int32>
struct Func_2_tE41640669B16E9AB4090A388E79A45C1DD304A6D;
// System.Func`2<Zenject.InitializableManager/InitializableInfo,System.Int32>
struct Func_2_t88AC31EADD405EC4C1D8542902E4C695670DAE17;
// System.Func`2<Zenject.TypeValuePair,System.Object>
struct Func_2_tE0EA28C07F376CB2C0E3DBC985C2651B6C247623;
// System.IDisposable
struct IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// Zenject.CachedProvider
struct CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8;
// Zenject.DiContainer
struct DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9;
// Zenject.DisposableManager
struct DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3;
// Zenject.FixedTickablesTaskUpdater
struct FixedTickablesTaskUpdater_t8455644BFB7A3B25FBA48F26B4A5253F8208CF20;
// Zenject.GameObjectCreationParameters
struct GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4;
// Zenject.GuiRenderableManager
struct GuiRenderableManager_tAF974427AFC375E673F5B208DFF3F18D65278519;
// Zenject.IFixedTickable
struct IFixedTickable_t2430B4F1D057046F1D51D3C95DB1D769CF70905C;
// Zenject.IGuiRenderable
struct IGuiRenderable_t12DC636573ABBFD312ED80587EB01A176F0551F9;
// Zenject.IInitializable
struct IInitializable_t5B4CC737C46E9CCF6532C494C186FAD8B036902F;
// Zenject.ILateDisposable
struct ILateDisposable_tC23AFF90E2684B1E4E74D9500EF5CFB132ACCBA7;
// Zenject.ILateTickable
struct ILateTickable_tC140F042194A90915549D6C25F80670675A4FADE;
// Zenject.IPrefabProvider
struct IPrefabProvider_t03CCCDA76ECC8346AB1BF4B6BEB0BAD1209DABB0;
// Zenject.ISubContainerCreator
struct ISubContainerCreator_tF9496E42638FC023B3C3A79AFF45CFF044BBD281;
// Zenject.ITickable
struct ITickable_tD47D195A496F54B9D7589A428952FA6519EDEDA0;
// Zenject.InitializableManager
struct InitializableManager_tBAC718A10ABAC14E0DBD742F921323DBCD34B9F8;
// Zenject.InjectArgs
struct InjectArgs_t8A0EB8658CA9A35034FFFFA08D1DCB03051E521E;
// Zenject.InjectContext
struct InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649;
// Zenject.LateTickablesTaskUpdater
struct LateTickablesTaskUpdater_t03C7EF16845586540065A3D08D17571840CD4F24;
// Zenject.Lazy`1<System.Collections.Generic.List`1<Zenject.TickableManager>>
struct Lazy_1_t06CCCB9361FC529CFE74E4D0AB4387409C38D34F;
// Zenject.PrefabResourceSingletonProviderCreator
struct PrefabResourceSingletonProviderCreator_t95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5;
// Zenject.PrefabSingletonProviderCreator
struct PrefabSingletonProviderCreator_t4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F;
// Zenject.SingletonId
struct SingletonId_t688C4C4A8CDD9C7CA54DBA5F829AE333F8947DF3;
// Zenject.SingletonMarkRegistry
struct SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751;
// Zenject.StandardSingletonProviderCreator
struct StandardSingletonProviderCreator_t4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4;
// Zenject.SubContainerCreatorByNewPrefabInstaller
struct SubContainerCreatorByNewPrefabInstaller_t892C2B19E8FE3B7CB1963B14CBBD29F58041F118;
// Zenject.SubContainerDependencyProvider
struct SubContainerDependencyProvider_t93AC6894C486CF68BB1B0DE97E462D76416DB2D6;
// Zenject.SubContainerSingletonProviderCreatorByInstaller
struct SubContainerSingletonProviderCreatorByInstaller_t90FB582E91D07E51B7C4584C4E3E071397AE2B09;
// Zenject.SubContainerSingletonProviderCreatorByMethod
struct SubContainerSingletonProviderCreatorByMethod_t5BFA8689BE01E6E287C528411DC83D46F08BA07F;
// Zenject.SubContainerSingletonProviderCreatorByNewPrefab
struct SubContainerSingletonProviderCreatorByNewPrefab_t0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8;
// Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource
struct SubContainerSingletonProviderCreatorByNewPrefabResource_t376F2451341DE65400990403DF0B5A966ECCBCCB;
// Zenject.TickableManager
struct TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA;
// Zenject.TickablesTaskUpdater
struct TickablesTaskUpdater_tCE7741F2A32B9750025C1F357267144B1F62FBE0;
// Zenject.TransientProvider
struct TransientProvider_t4C4A97405255740DA79D817A70712C8F9F54A0BA;
// Zenject.TypeValuePair
struct TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B;
// Zenject.UntypedFactoryProvider
struct UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef DISPOSABLEMANAGER_TA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3_H
#define DISPOSABLEMANAGER_TA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager
struct  DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo> Zenject.DisposableManager::_disposables
	List_1_t864E51A8327073D1D23F99573C5924B750FFBFC6 * ____disposables_0;
	// System.Collections.Generic.List`1<Zenject.DisposableManager/LateDisposableInfo> Zenject.DisposableManager::_lateDisposables
	List_1_tB13268EE35215DC383466A2878231B718FBC8308 * ____lateDisposables_1;
	// System.Boolean Zenject.DisposableManager::_disposed
	bool ____disposed_2;
	// System.Boolean Zenject.DisposableManager::_lateDisposed
	bool ____lateDisposed_3;

public:
	inline static int32_t get_offset_of__disposables_0() { return static_cast<int32_t>(offsetof(DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3, ____disposables_0)); }
	inline List_1_t864E51A8327073D1D23F99573C5924B750FFBFC6 * get__disposables_0() const { return ____disposables_0; }
	inline List_1_t864E51A8327073D1D23F99573C5924B750FFBFC6 ** get_address_of__disposables_0() { return &____disposables_0; }
	inline void set__disposables_0(List_1_t864E51A8327073D1D23F99573C5924B750FFBFC6 * value)
	{
		____disposables_0 = value;
		Il2CppCodeGenWriteBarrier((&____disposables_0), value);
	}

	inline static int32_t get_offset_of__lateDisposables_1() { return static_cast<int32_t>(offsetof(DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3, ____lateDisposables_1)); }
	inline List_1_tB13268EE35215DC383466A2878231B718FBC8308 * get__lateDisposables_1() const { return ____lateDisposables_1; }
	inline List_1_tB13268EE35215DC383466A2878231B718FBC8308 ** get_address_of__lateDisposables_1() { return &____lateDisposables_1; }
	inline void set__lateDisposables_1(List_1_tB13268EE35215DC383466A2878231B718FBC8308 * value)
	{
		____lateDisposables_1 = value;
		Il2CppCodeGenWriteBarrier((&____lateDisposables_1), value);
	}

	inline static int32_t get_offset_of__disposed_2() { return static_cast<int32_t>(offsetof(DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3, ____disposed_2)); }
	inline bool get__disposed_2() const { return ____disposed_2; }
	inline bool* get_address_of__disposed_2() { return &____disposed_2; }
	inline void set__disposed_2(bool value)
	{
		____disposed_2 = value;
	}

	inline static int32_t get_offset_of__lateDisposed_3() { return static_cast<int32_t>(offsetof(DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3, ____lateDisposed_3)); }
	inline bool get__lateDisposed_3() const { return ____lateDisposed_3; }
	inline bool* get_address_of__lateDisposed_3() { return &____lateDisposed_3; }
	inline void set__lateDisposed_3(bool value)
	{
		____lateDisposed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPOSABLEMANAGER_TA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3_H
#ifndef U3CU3EC_T8EF59BB0CFCB03946AC97C9FA3191DA2E8977505_H
#define U3CU3EC_T8EF59BB0CFCB03946AC97C9FA3191DA2E8977505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager/<>c
struct  U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505_StaticFields
{
public:
	// Zenject.DisposableManager/<>c Zenject.DisposableManager/<>c::<>9
	U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505 * ___U3CU3E9_0;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32> Zenject.DisposableManager/<>c::<>9__4_1
	Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * ___U3CU3E9__4_1_1;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32> Zenject.DisposableManager/<>c::<>9__4_3
	Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * ___U3CU3E9__4_3_2;
	// System.Func`2<Zenject.DisposableManager/LateDisposableInfo,System.Int32> Zenject.DisposableManager/<>c::<>9__8_0
	Func_2_tA4A50610B774C8F8FA0322E0064879FE564992C8 * ___U3CU3E9__8_0_3;
	// System.Func`2<Zenject.DisposableManager/DisposableInfo,System.Int32> Zenject.DisposableManager/<>c::<>9__9_0
	Func_2_t9F898882BCEEADF871BCCDEB458D8BA8BCBAD7AB * ___U3CU3E9__9_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505_StaticFields, ___U3CU3E9__4_1_1)); }
	inline Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * get_U3CU3E9__4_1_1() const { return ___U3CU3E9__4_1_1; }
	inline Func_2_t9288B55256D27898799397C666D0F5F7C7368101 ** get_address_of_U3CU3E9__4_1_1() { return &___U3CU3E9__4_1_1; }
	inline void set_U3CU3E9__4_1_1(Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * value)
	{
		___U3CU3E9__4_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_3_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505_StaticFields, ___U3CU3E9__4_3_2)); }
	inline Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * get_U3CU3E9__4_3_2() const { return ___U3CU3E9__4_3_2; }
	inline Func_2_t9288B55256D27898799397C666D0F5F7C7368101 ** get_address_of_U3CU3E9__4_3_2() { return &___U3CU3E9__4_3_2; }
	inline void set_U3CU3E9__4_3_2(Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * value)
	{
		___U3CU3E9__4_3_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_3_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505_StaticFields, ___U3CU3E9__8_0_3)); }
	inline Func_2_tA4A50610B774C8F8FA0322E0064879FE564992C8 * get_U3CU3E9__8_0_3() const { return ___U3CU3E9__8_0_3; }
	inline Func_2_tA4A50610B774C8F8FA0322E0064879FE564992C8 ** get_address_of_U3CU3E9__8_0_3() { return &___U3CU3E9__8_0_3; }
	inline void set_U3CU3E9__8_0_3(Func_2_tA4A50610B774C8F8FA0322E0064879FE564992C8 * value)
	{
		___U3CU3E9__8_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505_StaticFields, ___U3CU3E9__9_0_4)); }
	inline Func_2_t9F898882BCEEADF871BCCDEB458D8BA8BCBAD7AB * get_U3CU3E9__9_0_4() const { return ___U3CU3E9__9_0_4; }
	inline Func_2_t9F898882BCEEADF871BCCDEB458D8BA8BCBAD7AB ** get_address_of_U3CU3E9__9_0_4() { return &___U3CU3E9__9_0_4; }
	inline void set_U3CU3E9__9_0_4(Func_2_t9F898882BCEEADF871BCCDEB458D8BA8BCBAD7AB * value)
	{
		___U3CU3E9__9_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__9_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T8EF59BB0CFCB03946AC97C9FA3191DA2E8977505_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T0E7AD8540DFCAB2F95DEB9EFE1F05174BE6DB939_H
#define U3CU3EC__DISPLAYCLASS4_0_T0E7AD8540DFCAB2F95DEB9EFE1F05174BE6DB939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t0E7AD8540DFCAB2F95DEB9EFE1F05174BE6DB939  : public RuntimeObject
{
public:
	// System.IDisposable Zenject.DisposableManager/<>c__DisplayClass4_0::disposable
	RuntimeObject* ___disposable_0;

public:
	inline static int32_t get_offset_of_disposable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t0E7AD8540DFCAB2F95DEB9EFE1F05174BE6DB939, ___disposable_0)); }
	inline RuntimeObject* get_disposable_0() const { return ___disposable_0; }
	inline RuntimeObject** get_address_of_disposable_0() { return &___disposable_0; }
	inline void set_disposable_0(RuntimeObject* value)
	{
		___disposable_0 = value;
		Il2CppCodeGenWriteBarrier((&___disposable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T0E7AD8540DFCAB2F95DEB9EFE1F05174BE6DB939_H
#ifndef U3CU3EC__DISPLAYCLASS4_1_T1A65DF68F99A2FE270546FF0FDD191C034479AF8_H
#define U3CU3EC__DISPLAYCLASS4_1_T1A65DF68F99A2FE270546FF0FDD191C034479AF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager/<>c__DisplayClass4_1
struct  U3CU3Ec__DisplayClass4_1_t1A65DF68F99A2FE270546FF0FDD191C034479AF8  : public RuntimeObject
{
public:
	// Zenject.ILateDisposable Zenject.DisposableManager/<>c__DisplayClass4_1::lateDisposable
	RuntimeObject* ___lateDisposable_0;

public:
	inline static int32_t get_offset_of_lateDisposable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_1_t1A65DF68F99A2FE270546FF0FDD191C034479AF8, ___lateDisposable_0)); }
	inline RuntimeObject* get_lateDisposable_0() const { return ___lateDisposable_0; }
	inline RuntimeObject** get_address_of_lateDisposable_0() { return &___lateDisposable_0; }
	inline void set_lateDisposable_0(RuntimeObject* value)
	{
		___lateDisposable_0 = value;
		Il2CppCodeGenWriteBarrier((&___lateDisposable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_1_T1A65DF68F99A2FE270546FF0FDD191C034479AF8_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_TF3FF397A1ADBDEBEE681BFB0035407634960E785_H
#define U3CU3EC__DISPLAYCLASS7_0_TF3FF397A1ADBDEBEE681BFB0035407634960E785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_tF3FF397A1ADBDEBEE681BFB0035407634960E785  : public RuntimeObject
{
public:
	// System.IDisposable Zenject.DisposableManager/<>c__DisplayClass7_0::disposable
	RuntimeObject* ___disposable_0;

public:
	inline static int32_t get_offset_of_disposable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tF3FF397A1ADBDEBEE681BFB0035407634960E785, ___disposable_0)); }
	inline RuntimeObject* get_disposable_0() const { return ___disposable_0; }
	inline RuntimeObject** get_address_of_disposable_0() { return &___disposable_0; }
	inline void set_disposable_0(RuntimeObject* value)
	{
		___disposable_0 = value;
		Il2CppCodeGenWriteBarrier((&___disposable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_TF3FF397A1ADBDEBEE681BFB0035407634960E785_H
#ifndef DISPOSABLEINFO_T6C27D5793195688438363D4DD565914D7E9897BD_H
#define DISPOSABLEINFO_T6C27D5793195688438363D4DD565914D7E9897BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager/DisposableInfo
struct  DisposableInfo_t6C27D5793195688438363D4DD565914D7E9897BD  : public RuntimeObject
{
public:
	// System.IDisposable Zenject.DisposableManager/DisposableInfo::Disposable
	RuntimeObject* ___Disposable_0;
	// System.Int32 Zenject.DisposableManager/DisposableInfo::Priority
	int32_t ___Priority_1;

public:
	inline static int32_t get_offset_of_Disposable_0() { return static_cast<int32_t>(offsetof(DisposableInfo_t6C27D5793195688438363D4DD565914D7E9897BD, ___Disposable_0)); }
	inline RuntimeObject* get_Disposable_0() const { return ___Disposable_0; }
	inline RuntimeObject** get_address_of_Disposable_0() { return &___Disposable_0; }
	inline void set_Disposable_0(RuntimeObject* value)
	{
		___Disposable_0 = value;
		Il2CppCodeGenWriteBarrier((&___Disposable_0), value);
	}

	inline static int32_t get_offset_of_Priority_1() { return static_cast<int32_t>(offsetof(DisposableInfo_t6C27D5793195688438363D4DD565914D7E9897BD, ___Priority_1)); }
	inline int32_t get_Priority_1() const { return ___Priority_1; }
	inline int32_t* get_address_of_Priority_1() { return &___Priority_1; }
	inline void set_Priority_1(int32_t value)
	{
		___Priority_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPOSABLEINFO_T6C27D5793195688438363D4DD565914D7E9897BD_H
#ifndef LATEDISPOSABLEINFO_T6055A4F005C800036D81CAC5CBE866498ADB98D7_H
#define LATEDISPOSABLEINFO_T6055A4F005C800036D81CAC5CBE866498ADB98D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager/LateDisposableInfo
struct  LateDisposableInfo_t6055A4F005C800036D81CAC5CBE866498ADB98D7  : public RuntimeObject
{
public:
	// Zenject.ILateDisposable Zenject.DisposableManager/LateDisposableInfo::LateDisposable
	RuntimeObject* ___LateDisposable_0;
	// System.Int32 Zenject.DisposableManager/LateDisposableInfo::Priority
	int32_t ___Priority_1;

public:
	inline static int32_t get_offset_of_LateDisposable_0() { return static_cast<int32_t>(offsetof(LateDisposableInfo_t6055A4F005C800036D81CAC5CBE866498ADB98D7, ___LateDisposable_0)); }
	inline RuntimeObject* get_LateDisposable_0() const { return ___LateDisposable_0; }
	inline RuntimeObject** get_address_of_LateDisposable_0() { return &___LateDisposable_0; }
	inline void set_LateDisposable_0(RuntimeObject* value)
	{
		___LateDisposable_0 = value;
		Il2CppCodeGenWriteBarrier((&___LateDisposable_0), value);
	}

	inline static int32_t get_offset_of_Priority_1() { return static_cast<int32_t>(offsetof(LateDisposableInfo_t6055A4F005C800036D81CAC5CBE866498ADB98D7, ___Priority_1)); }
	inline int32_t get_Priority_1() const { return ___Priority_1; }
	inline int32_t* get_address_of_Priority_1() { return &___Priority_1; }
	inline void set_Priority_1(int32_t value)
	{
		___Priority_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATEDISPOSABLEINFO_T6055A4F005C800036D81CAC5CBE866498ADB98D7_H
#ifndef GUIRENDERABLEMANAGER_TAF974427AFC375E673F5B208DFF3F18D65278519_H
#define GUIRENDERABLEMANAGER_TAF974427AFC375E673F5B208DFF3F18D65278519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GuiRenderableManager
struct  GuiRenderableManager_tAF974427AFC375E673F5B208DFF3F18D65278519  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.GuiRenderableManager/RenderableInfo> Zenject.GuiRenderableManager::_renderables
	List_1_tF2A9A0B0A1B439359488D57AA4E3CC7D5D43CEF2 * ____renderables_0;

public:
	inline static int32_t get_offset_of__renderables_0() { return static_cast<int32_t>(offsetof(GuiRenderableManager_tAF974427AFC375E673F5B208DFF3F18D65278519, ____renderables_0)); }
	inline List_1_tF2A9A0B0A1B439359488D57AA4E3CC7D5D43CEF2 * get__renderables_0() const { return ____renderables_0; }
	inline List_1_tF2A9A0B0A1B439359488D57AA4E3CC7D5D43CEF2 ** get_address_of__renderables_0() { return &____renderables_0; }
	inline void set__renderables_0(List_1_tF2A9A0B0A1B439359488D57AA4E3CC7D5D43CEF2 * value)
	{
		____renderables_0 = value;
		Il2CppCodeGenWriteBarrier((&____renderables_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIRENDERABLEMANAGER_TAF974427AFC375E673F5B208DFF3F18D65278519_H
#ifndef U3CU3EC_TDB8260FF47EC3120A1A86EDA379C3305207BAB54_H
#define U3CU3EC_TDB8260FF47EC3120A1A86EDA379C3305207BAB54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GuiRenderableManager/<>c
struct  U3CU3Ec_tDB8260FF47EC3120A1A86EDA379C3305207BAB54  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tDB8260FF47EC3120A1A86EDA379C3305207BAB54_StaticFields
{
public:
	// Zenject.GuiRenderableManager/<>c Zenject.GuiRenderableManager/<>c::<>9
	U3CU3Ec_tDB8260FF47EC3120A1A86EDA379C3305207BAB54 * ___U3CU3E9_0;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32> Zenject.GuiRenderableManager/<>c::<>9__1_2
	Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * ___U3CU3E9__1_2_1;
	// System.Func`2<Zenject.GuiRenderableManager/RenderableInfo,System.Int32> Zenject.GuiRenderableManager/<>c::<>9__1_0
	Func_2_tE41640669B16E9AB4090A388E79A45C1DD304A6D * ___U3CU3E9__1_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDB8260FF47EC3120A1A86EDA379C3305207BAB54_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tDB8260FF47EC3120A1A86EDA379C3305207BAB54 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tDB8260FF47EC3120A1A86EDA379C3305207BAB54 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tDB8260FF47EC3120A1A86EDA379C3305207BAB54 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDB8260FF47EC3120A1A86EDA379C3305207BAB54_StaticFields, ___U3CU3E9__1_2_1)); }
	inline Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * get_U3CU3E9__1_2_1() const { return ___U3CU3E9__1_2_1; }
	inline Func_2_t9288B55256D27898799397C666D0F5F7C7368101 ** get_address_of_U3CU3E9__1_2_1() { return &___U3CU3E9__1_2_1; }
	inline void set_U3CU3E9__1_2_1(Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * value)
	{
		___U3CU3E9__1_2_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_2_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDB8260FF47EC3120A1A86EDA379C3305207BAB54_StaticFields, ___U3CU3E9__1_0_2)); }
	inline Func_2_tE41640669B16E9AB4090A388E79A45C1DD304A6D * get_U3CU3E9__1_0_2() const { return ___U3CU3E9__1_0_2; }
	inline Func_2_tE41640669B16E9AB4090A388E79A45C1DD304A6D ** get_address_of_U3CU3E9__1_0_2() { return &___U3CU3E9__1_0_2; }
	inline void set_U3CU3E9__1_0_2(Func_2_tE41640669B16E9AB4090A388E79A45C1DD304A6D * value)
	{
		___U3CU3E9__1_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TDB8260FF47EC3120A1A86EDA379C3305207BAB54_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T1728CC787597C136EE43EED2FB9683D6E27BDD53_H
#define U3CU3EC__DISPLAYCLASS1_0_T1728CC787597C136EE43EED2FB9683D6E27BDD53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GuiRenderableManager/<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t1728CC787597C136EE43EED2FB9683D6E27BDD53  : public RuntimeObject
{
public:
	// Zenject.IGuiRenderable Zenject.GuiRenderableManager/<>c__DisplayClass1_0::renderable
	RuntimeObject* ___renderable_0;

public:
	inline static int32_t get_offset_of_renderable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t1728CC787597C136EE43EED2FB9683D6E27BDD53, ___renderable_0)); }
	inline RuntimeObject* get_renderable_0() const { return ___renderable_0; }
	inline RuntimeObject** get_address_of_renderable_0() { return &___renderable_0; }
	inline void set_renderable_0(RuntimeObject* value)
	{
		___renderable_0 = value;
		Il2CppCodeGenWriteBarrier((&___renderable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T1728CC787597C136EE43EED2FB9683D6E27BDD53_H
#ifndef RENDERABLEINFO_TF2C07BF667A1895744B8252956BA9CB8EADB8B6E_H
#define RENDERABLEINFO_TF2C07BF667A1895744B8252956BA9CB8EADB8B6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GuiRenderableManager/RenderableInfo
struct  RenderableInfo_tF2C07BF667A1895744B8252956BA9CB8EADB8B6E  : public RuntimeObject
{
public:
	// Zenject.IGuiRenderable Zenject.GuiRenderableManager/RenderableInfo::Renderable
	RuntimeObject* ___Renderable_0;
	// System.Int32 Zenject.GuiRenderableManager/RenderableInfo::Priority
	int32_t ___Priority_1;

public:
	inline static int32_t get_offset_of_Renderable_0() { return static_cast<int32_t>(offsetof(RenderableInfo_tF2C07BF667A1895744B8252956BA9CB8EADB8B6E, ___Renderable_0)); }
	inline RuntimeObject* get_Renderable_0() const { return ___Renderable_0; }
	inline RuntimeObject** get_address_of_Renderable_0() { return &___Renderable_0; }
	inline void set_Renderable_0(RuntimeObject* value)
	{
		___Renderable_0 = value;
		Il2CppCodeGenWriteBarrier((&___Renderable_0), value);
	}

	inline static int32_t get_offset_of_Priority_1() { return static_cast<int32_t>(offsetof(RenderableInfo_tF2C07BF667A1895744B8252956BA9CB8EADB8B6E, ___Priority_1)); }
	inline int32_t get_Priority_1() const { return ___Priority_1; }
	inline int32_t* get_address_of_Priority_1() { return &___Priority_1; }
	inline void set_Priority_1(int32_t value)
	{
		___Priority_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERABLEINFO_TF2C07BF667A1895744B8252956BA9CB8EADB8B6E_H
#ifndef INITIALIZABLEMANAGER_TBAC718A10ABAC14E0DBD742F921323DBCD34B9F8_H
#define INITIALIZABLEMANAGER_TBAC718A10ABAC14E0DBD742F921323DBCD34B9F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InitializableManager
struct  InitializableManager_tBAC718A10ABAC14E0DBD742F921323DBCD34B9F8  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.InitializableManager/InitializableInfo> Zenject.InitializableManager::_initializables
	List_1_t42AC613DAF8DB5D3D38B374B2FE88C08FC02B976 * ____initializables_0;
	// System.Boolean Zenject.InitializableManager::_hasInitialized
	bool ____hasInitialized_1;

public:
	inline static int32_t get_offset_of__initializables_0() { return static_cast<int32_t>(offsetof(InitializableManager_tBAC718A10ABAC14E0DBD742F921323DBCD34B9F8, ____initializables_0)); }
	inline List_1_t42AC613DAF8DB5D3D38B374B2FE88C08FC02B976 * get__initializables_0() const { return ____initializables_0; }
	inline List_1_t42AC613DAF8DB5D3D38B374B2FE88C08FC02B976 ** get_address_of__initializables_0() { return &____initializables_0; }
	inline void set__initializables_0(List_1_t42AC613DAF8DB5D3D38B374B2FE88C08FC02B976 * value)
	{
		____initializables_0 = value;
		Il2CppCodeGenWriteBarrier((&____initializables_0), value);
	}

	inline static int32_t get_offset_of__hasInitialized_1() { return static_cast<int32_t>(offsetof(InitializableManager_tBAC718A10ABAC14E0DBD742F921323DBCD34B9F8, ____hasInitialized_1)); }
	inline bool get__hasInitialized_1() const { return ____hasInitialized_1; }
	inline bool* get_address_of__hasInitialized_1() { return &____hasInitialized_1; }
	inline void set__hasInitialized_1(bool value)
	{
		____hasInitialized_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZABLEMANAGER_TBAC718A10ABAC14E0DBD742F921323DBCD34B9F8_H
#ifndef U3CU3EC_T7B801CA330862708CF35D062010EF9A7DDC2DF03_H
#define U3CU3EC_T7B801CA330862708CF35D062010EF9A7DDC2DF03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InitializableManager/<>c
struct  U3CU3Ec_t7B801CA330862708CF35D062010EF9A7DDC2DF03  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t7B801CA330862708CF35D062010EF9A7DDC2DF03_StaticFields
{
public:
	// Zenject.InitializableManager/<>c Zenject.InitializableManager/<>c::<>9
	U3CU3Ec_t7B801CA330862708CF35D062010EF9A7DDC2DF03 * ___U3CU3E9_0;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32> Zenject.InitializableManager/<>c::<>9__2_1
	Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * ___U3CU3E9__2_1_1;
	// System.Func`2<Zenject.InitializableManager/InitializableInfo,System.Int32> Zenject.InitializableManager/<>c::<>9__3_0
	Func_2_t88AC31EADD405EC4C1D8542902E4C695670DAE17 * ___U3CU3E9__3_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7B801CA330862708CF35D062010EF9A7DDC2DF03_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t7B801CA330862708CF35D062010EF9A7DDC2DF03 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t7B801CA330862708CF35D062010EF9A7DDC2DF03 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t7B801CA330862708CF35D062010EF9A7DDC2DF03 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7B801CA330862708CF35D062010EF9A7DDC2DF03_StaticFields, ___U3CU3E9__2_1_1)); }
	inline Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * get_U3CU3E9__2_1_1() const { return ___U3CU3E9__2_1_1; }
	inline Func_2_t9288B55256D27898799397C666D0F5F7C7368101 ** get_address_of_U3CU3E9__2_1_1() { return &___U3CU3E9__2_1_1; }
	inline void set_U3CU3E9__2_1_1(Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * value)
	{
		___U3CU3E9__2_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7B801CA330862708CF35D062010EF9A7DDC2DF03_StaticFields, ___U3CU3E9__3_0_2)); }
	inline Func_2_t88AC31EADD405EC4C1D8542902E4C695670DAE17 * get_U3CU3E9__3_0_2() const { return ___U3CU3E9__3_0_2; }
	inline Func_2_t88AC31EADD405EC4C1D8542902E4C695670DAE17 ** get_address_of_U3CU3E9__3_0_2() { return &___U3CU3E9__3_0_2; }
	inline void set_U3CU3E9__3_0_2(Func_2_t88AC31EADD405EC4C1D8542902E4C695670DAE17 * value)
	{
		___U3CU3E9__3_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T7B801CA330862708CF35D062010EF9A7DDC2DF03_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T9BE5C72A097402E1920A49E1094FF86EC365AB50_H
#define U3CU3EC__DISPLAYCLASS2_0_T9BE5C72A097402E1920A49E1094FF86EC365AB50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InitializableManager/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t9BE5C72A097402E1920A49E1094FF86EC365AB50  : public RuntimeObject
{
public:
	// Zenject.IInitializable Zenject.InitializableManager/<>c__DisplayClass2_0::initializable
	RuntimeObject* ___initializable_0;

public:
	inline static int32_t get_offset_of_initializable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t9BE5C72A097402E1920A49E1094FF86EC365AB50, ___initializable_0)); }
	inline RuntimeObject* get_initializable_0() const { return ___initializable_0; }
	inline RuntimeObject** get_address_of_initializable_0() { return &___initializable_0; }
	inline void set_initializable_0(RuntimeObject* value)
	{
		___initializable_0 = value;
		Il2CppCodeGenWriteBarrier((&___initializable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T9BE5C72A097402E1920A49E1094FF86EC365AB50_H
#ifndef INITIALIZABLEINFO_TEC3A736CF9BF3FF84D423D84F6644750D30F059A_H
#define INITIALIZABLEINFO_TEC3A736CF9BF3FF84D423D84F6644750D30F059A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InitializableManager/InitializableInfo
struct  InitializableInfo_tEC3A736CF9BF3FF84D423D84F6644750D30F059A  : public RuntimeObject
{
public:
	// Zenject.IInitializable Zenject.InitializableManager/InitializableInfo::Initializable
	RuntimeObject* ___Initializable_0;
	// System.Int32 Zenject.InitializableManager/InitializableInfo::Priority
	int32_t ___Priority_1;

public:
	inline static int32_t get_offset_of_Initializable_0() { return static_cast<int32_t>(offsetof(InitializableInfo_tEC3A736CF9BF3FF84D423D84F6644750D30F059A, ___Initializable_0)); }
	inline RuntimeObject* get_Initializable_0() const { return ___Initializable_0; }
	inline RuntimeObject** get_address_of_Initializable_0() { return &___Initializable_0; }
	inline void set_Initializable_0(RuntimeObject* value)
	{
		___Initializable_0 = value;
		Il2CppCodeGenWriteBarrier((&___Initializable_0), value);
	}

	inline static int32_t get_offset_of_Priority_1() { return static_cast<int32_t>(offsetof(InitializableInfo_tEC3A736CF9BF3FF84D423D84F6644750D30F059A, ___Priority_1)); }
	inline int32_t get_Priority_1() const { return ___Priority_1; }
	inline int32_t* get_address_of_Priority_1() { return &___Priority_1; }
	inline void set_Priority_1(int32_t value)
	{
		___Priority_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZABLEINFO_TEC3A736CF9BF3FF84D423D84F6644750D30F059A_H
#ifndef INSTALLERBASE_TAD3E3395B906666822EB47EAC578547EEB7DC56F_H
#define INSTALLERBASE_TAD3E3395B906666822EB47EAC578547EEB7DC56F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstallerBase
struct  InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.InstallerBase::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_0;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F, ____container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_0() const { return ____container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLERBASE_TAD3E3395B906666822EB47EAC578547EEB7DC56F_H
#ifndef KERNEL_T327BE4DD070D320F9C304747E752F8E71AB7188D_H
#define KERNEL_T327BE4DD070D320F9C304747E752F8E71AB7188D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Kernel
struct  Kernel_t327BE4DD070D320F9C304747E752F8E71AB7188D  : public RuntimeObject
{
public:
	// Zenject.TickableManager Zenject.Kernel::_tickableManager
	TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA * ____tickableManager_0;
	// Zenject.InitializableManager Zenject.Kernel::_initializableManager
	InitializableManager_tBAC718A10ABAC14E0DBD742F921323DBCD34B9F8 * ____initializableManager_1;
	// Zenject.DisposableManager Zenject.Kernel::_disposablesManager
	DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3 * ____disposablesManager_2;

public:
	inline static int32_t get_offset_of__tickableManager_0() { return static_cast<int32_t>(offsetof(Kernel_t327BE4DD070D320F9C304747E752F8E71AB7188D, ____tickableManager_0)); }
	inline TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA * get__tickableManager_0() const { return ____tickableManager_0; }
	inline TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA ** get_address_of__tickableManager_0() { return &____tickableManager_0; }
	inline void set__tickableManager_0(TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA * value)
	{
		____tickableManager_0 = value;
		Il2CppCodeGenWriteBarrier((&____tickableManager_0), value);
	}

	inline static int32_t get_offset_of__initializableManager_1() { return static_cast<int32_t>(offsetof(Kernel_t327BE4DD070D320F9C304747E752F8E71AB7188D, ____initializableManager_1)); }
	inline InitializableManager_tBAC718A10ABAC14E0DBD742F921323DBCD34B9F8 * get__initializableManager_1() const { return ____initializableManager_1; }
	inline InitializableManager_tBAC718A10ABAC14E0DBD742F921323DBCD34B9F8 ** get_address_of__initializableManager_1() { return &____initializableManager_1; }
	inline void set__initializableManager_1(InitializableManager_tBAC718A10ABAC14E0DBD742F921323DBCD34B9F8 * value)
	{
		____initializableManager_1 = value;
		Il2CppCodeGenWriteBarrier((&____initializableManager_1), value);
	}

	inline static int32_t get_offset_of__disposablesManager_2() { return static_cast<int32_t>(offsetof(Kernel_t327BE4DD070D320F9C304747E752F8E71AB7188D, ____disposablesManager_2)); }
	inline DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3 * get__disposablesManager_2() const { return ____disposablesManager_2; }
	inline DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3 ** get_address_of__disposablesManager_2() { return &____disposablesManager_2; }
	inline void set__disposablesManager_2(DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3 * value)
	{
		____disposablesManager_2 = value;
		Il2CppCodeGenWriteBarrier((&____disposablesManager_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNEL_T327BE4DD070D320F9C304747E752F8E71AB7188D_H
#ifndef SINGLETONIMPLIDS_T4E3E5E5CAD910340FBD8815F6F3ABEAC951BA612_H
#define SINGLETONIMPLIDS_T4E3E5E5CAD910340FBD8815F6F3ABEAC951BA612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonImplIds
struct  SingletonImplIds_t4E3E5E5CAD910340FBD8815F6F3ABEAC951BA612  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONIMPLIDS_T4E3E5E5CAD910340FBD8815F6F3ABEAC951BA612_H
#ifndef TOGETTER_T33C5B61AE94C79EEE199F4F9F1B351AB029277D3_H
#define TOGETTER_T33C5B61AE94C79EEE199F4F9F1B351AB029277D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonImplIds/ToGetter
struct  ToGetter_t33C5B61AE94C79EEE199F4F9F1B351AB029277D3  : public RuntimeObject
{
public:
	// System.Delegate Zenject.SingletonImplIds/ToGetter::_method
	Delegate_t * ____method_0;
	// System.Object Zenject.SingletonImplIds/ToGetter::_identifier
	RuntimeObject * ____identifier_1;

public:
	inline static int32_t get_offset_of__method_0() { return static_cast<int32_t>(offsetof(ToGetter_t33C5B61AE94C79EEE199F4F9F1B351AB029277D3, ____method_0)); }
	inline Delegate_t * get__method_0() const { return ____method_0; }
	inline Delegate_t ** get_address_of__method_0() { return &____method_0; }
	inline void set__method_0(Delegate_t * value)
	{
		____method_0 = value;
		Il2CppCodeGenWriteBarrier((&____method_0), value);
	}

	inline static int32_t get_offset_of__identifier_1() { return static_cast<int32_t>(offsetof(ToGetter_t33C5B61AE94C79EEE199F4F9F1B351AB029277D3, ____identifier_1)); }
	inline RuntimeObject * get__identifier_1() const { return ____identifier_1; }
	inline RuntimeObject ** get_address_of__identifier_1() { return &____identifier_1; }
	inline void set__identifier_1(RuntimeObject * value)
	{
		____identifier_1 = value;
		Il2CppCodeGenWriteBarrier((&____identifier_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGETTER_T33C5B61AE94C79EEE199F4F9F1B351AB029277D3_H
#ifndef TOMETHOD_T70AFA34E176CF0E2CF352F11754ADAF1DA951C51_H
#define TOMETHOD_T70AFA34E176CF0E2CF352F11754ADAF1DA951C51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonImplIds/ToMethod
struct  ToMethod_t70AFA34E176CF0E2CF352F11754ADAF1DA951C51  : public RuntimeObject
{
public:
	// System.Delegate Zenject.SingletonImplIds/ToMethod::_method
	Delegate_t * ____method_0;

public:
	inline static int32_t get_offset_of__method_0() { return static_cast<int32_t>(offsetof(ToMethod_t70AFA34E176CF0E2CF352F11754ADAF1DA951C51, ____method_0)); }
	inline Delegate_t * get__method_0() const { return ____method_0; }
	inline Delegate_t ** get_address_of__method_0() { return &____method_0; }
	inline void set__method_0(Delegate_t * value)
	{
		____method_0 = value;
		Il2CppCodeGenWriteBarrier((&____method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOMETHOD_T70AFA34E176CF0E2CF352F11754ADAF1DA951C51_H
#ifndef SINGLETONMARKREGISTRY_T4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751_H
#define SINGLETONMARKREGISTRY_T4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonMarkRegistry
struct  SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<Zenject.SingletonId,Zenject.SingletonTypes> Zenject.SingletonMarkRegistry::_singletonTypes
	Dictionary_2_t855EDD5F3748FD51AB316D8DBB6939DDE51A9DC4 * ____singletonTypes_0;

public:
	inline static int32_t get_offset_of__singletonTypes_0() { return static_cast<int32_t>(offsetof(SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751, ____singletonTypes_0)); }
	inline Dictionary_2_t855EDD5F3748FD51AB316D8DBB6939DDE51A9DC4 * get__singletonTypes_0() const { return ____singletonTypes_0; }
	inline Dictionary_2_t855EDD5F3748FD51AB316D8DBB6939DDE51A9DC4 ** get_address_of__singletonTypes_0() { return &____singletonTypes_0; }
	inline void set__singletonTypes_0(Dictionary_2_t855EDD5F3748FD51AB316D8DBB6939DDE51A9DC4 * value)
	{
		____singletonTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&____singletonTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONMARKREGISTRY_T4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751_H
#ifndef SINGLETONPROVIDERCREATOR_T9E289C081C7F5C38D3431B63AEA3079E327F703D_H
#define SINGLETONPROVIDERCREATOR_T9E289C081C7F5C38D3431B63AEA3079E327F703D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonProviderCreator
struct  SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D  : public RuntimeObject
{
public:
	// Zenject.StandardSingletonProviderCreator Zenject.SingletonProviderCreator::_standardProviderCreator
	StandardSingletonProviderCreator_t4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4 * ____standardProviderCreator_0;
	// Zenject.SubContainerSingletonProviderCreatorByMethod Zenject.SingletonProviderCreator::_subContainerMethodProviderCreator
	SubContainerSingletonProviderCreatorByMethod_t5BFA8689BE01E6E287C528411DC83D46F08BA07F * ____subContainerMethodProviderCreator_1;
	// Zenject.SubContainerSingletonProviderCreatorByInstaller Zenject.SingletonProviderCreator::_subContainerInstallerProviderCreator
	SubContainerSingletonProviderCreatorByInstaller_t90FB582E91D07E51B7C4584C4E3E071397AE2B09 * ____subContainerInstallerProviderCreator_2;
	// Zenject.SubContainerSingletonProviderCreatorByNewPrefab Zenject.SingletonProviderCreator::_subContainerPrefabProviderCreator
	SubContainerSingletonProviderCreatorByNewPrefab_t0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8 * ____subContainerPrefabProviderCreator_3;
	// Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource Zenject.SingletonProviderCreator::_subContainerPrefabResourceProviderCreator
	SubContainerSingletonProviderCreatorByNewPrefabResource_t376F2451341DE65400990403DF0B5A966ECCBCCB * ____subContainerPrefabResourceProviderCreator_4;
	// Zenject.PrefabSingletonProviderCreator Zenject.SingletonProviderCreator::_prefabProviderCreator
	PrefabSingletonProviderCreator_t4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F * ____prefabProviderCreator_5;
	// Zenject.PrefabResourceSingletonProviderCreator Zenject.SingletonProviderCreator::_prefabResourceProviderCreator
	PrefabResourceSingletonProviderCreator_t95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5 * ____prefabResourceProviderCreator_6;

public:
	inline static int32_t get_offset_of__standardProviderCreator_0() { return static_cast<int32_t>(offsetof(SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D, ____standardProviderCreator_0)); }
	inline StandardSingletonProviderCreator_t4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4 * get__standardProviderCreator_0() const { return ____standardProviderCreator_0; }
	inline StandardSingletonProviderCreator_t4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4 ** get_address_of__standardProviderCreator_0() { return &____standardProviderCreator_0; }
	inline void set__standardProviderCreator_0(StandardSingletonProviderCreator_t4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4 * value)
	{
		____standardProviderCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&____standardProviderCreator_0), value);
	}

	inline static int32_t get_offset_of__subContainerMethodProviderCreator_1() { return static_cast<int32_t>(offsetof(SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D, ____subContainerMethodProviderCreator_1)); }
	inline SubContainerSingletonProviderCreatorByMethod_t5BFA8689BE01E6E287C528411DC83D46F08BA07F * get__subContainerMethodProviderCreator_1() const { return ____subContainerMethodProviderCreator_1; }
	inline SubContainerSingletonProviderCreatorByMethod_t5BFA8689BE01E6E287C528411DC83D46F08BA07F ** get_address_of__subContainerMethodProviderCreator_1() { return &____subContainerMethodProviderCreator_1; }
	inline void set__subContainerMethodProviderCreator_1(SubContainerSingletonProviderCreatorByMethod_t5BFA8689BE01E6E287C528411DC83D46F08BA07F * value)
	{
		____subContainerMethodProviderCreator_1 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerMethodProviderCreator_1), value);
	}

	inline static int32_t get_offset_of__subContainerInstallerProviderCreator_2() { return static_cast<int32_t>(offsetof(SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D, ____subContainerInstallerProviderCreator_2)); }
	inline SubContainerSingletonProviderCreatorByInstaller_t90FB582E91D07E51B7C4584C4E3E071397AE2B09 * get__subContainerInstallerProviderCreator_2() const { return ____subContainerInstallerProviderCreator_2; }
	inline SubContainerSingletonProviderCreatorByInstaller_t90FB582E91D07E51B7C4584C4E3E071397AE2B09 ** get_address_of__subContainerInstallerProviderCreator_2() { return &____subContainerInstallerProviderCreator_2; }
	inline void set__subContainerInstallerProviderCreator_2(SubContainerSingletonProviderCreatorByInstaller_t90FB582E91D07E51B7C4584C4E3E071397AE2B09 * value)
	{
		____subContainerInstallerProviderCreator_2 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerInstallerProviderCreator_2), value);
	}

	inline static int32_t get_offset_of__subContainerPrefabProviderCreator_3() { return static_cast<int32_t>(offsetof(SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D, ____subContainerPrefabProviderCreator_3)); }
	inline SubContainerSingletonProviderCreatorByNewPrefab_t0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8 * get__subContainerPrefabProviderCreator_3() const { return ____subContainerPrefabProviderCreator_3; }
	inline SubContainerSingletonProviderCreatorByNewPrefab_t0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8 ** get_address_of__subContainerPrefabProviderCreator_3() { return &____subContainerPrefabProviderCreator_3; }
	inline void set__subContainerPrefabProviderCreator_3(SubContainerSingletonProviderCreatorByNewPrefab_t0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8 * value)
	{
		____subContainerPrefabProviderCreator_3 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerPrefabProviderCreator_3), value);
	}

	inline static int32_t get_offset_of__subContainerPrefabResourceProviderCreator_4() { return static_cast<int32_t>(offsetof(SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D, ____subContainerPrefabResourceProviderCreator_4)); }
	inline SubContainerSingletonProviderCreatorByNewPrefabResource_t376F2451341DE65400990403DF0B5A966ECCBCCB * get__subContainerPrefabResourceProviderCreator_4() const { return ____subContainerPrefabResourceProviderCreator_4; }
	inline SubContainerSingletonProviderCreatorByNewPrefabResource_t376F2451341DE65400990403DF0B5A966ECCBCCB ** get_address_of__subContainerPrefabResourceProviderCreator_4() { return &____subContainerPrefabResourceProviderCreator_4; }
	inline void set__subContainerPrefabResourceProviderCreator_4(SubContainerSingletonProviderCreatorByNewPrefabResource_t376F2451341DE65400990403DF0B5A966ECCBCCB * value)
	{
		____subContainerPrefabResourceProviderCreator_4 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerPrefabResourceProviderCreator_4), value);
	}

	inline static int32_t get_offset_of__prefabProviderCreator_5() { return static_cast<int32_t>(offsetof(SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D, ____prefabProviderCreator_5)); }
	inline PrefabSingletonProviderCreator_t4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F * get__prefabProviderCreator_5() const { return ____prefabProviderCreator_5; }
	inline PrefabSingletonProviderCreator_t4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F ** get_address_of__prefabProviderCreator_5() { return &____prefabProviderCreator_5; }
	inline void set__prefabProviderCreator_5(PrefabSingletonProviderCreator_t4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F * value)
	{
		____prefabProviderCreator_5 = value;
		Il2CppCodeGenWriteBarrier((&____prefabProviderCreator_5), value);
	}

	inline static int32_t get_offset_of__prefabResourceProviderCreator_6() { return static_cast<int32_t>(offsetof(SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D, ____prefabResourceProviderCreator_6)); }
	inline PrefabResourceSingletonProviderCreator_t95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5 * get__prefabResourceProviderCreator_6() const { return ____prefabResourceProviderCreator_6; }
	inline PrefabResourceSingletonProviderCreator_t95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5 ** get_address_of__prefabResourceProviderCreator_6() { return &____prefabResourceProviderCreator_6; }
	inline void set__prefabResourceProviderCreator_6(PrefabResourceSingletonProviderCreator_t95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5 * value)
	{
		____prefabResourceProviderCreator_6 = value;
		Il2CppCodeGenWriteBarrier((&____prefabResourceProviderCreator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONPROVIDERCREATOR_T9E289C081C7F5C38D3431B63AEA3079E327F703D_H
#ifndef STANDARDSINGLETONPROVIDERCREATOR_T4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4_H
#define STANDARDSINGLETONPROVIDERCREATOR_T4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.StandardSingletonProviderCreator
struct  StandardSingletonProviderCreator_t4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4  : public RuntimeObject
{
public:
	// Zenject.SingletonMarkRegistry Zenject.StandardSingletonProviderCreator::_markRegistry
	SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * ____markRegistry_0;
	// System.Collections.Generic.Dictionary`2<Zenject.SingletonId,Zenject.StandardSingletonProviderCreator/ProviderInfo> Zenject.StandardSingletonProviderCreator::_providerMap
	Dictionary_2_t7591F89A805922AD4F3EDE546ADC77E45B43A7A9 * ____providerMap_1;
	// Zenject.DiContainer Zenject.StandardSingletonProviderCreator::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_2;

public:
	inline static int32_t get_offset_of__markRegistry_0() { return static_cast<int32_t>(offsetof(StandardSingletonProviderCreator_t4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4, ____markRegistry_0)); }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * get__markRegistry_0() const { return ____markRegistry_0; }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 ** get_address_of__markRegistry_0() { return &____markRegistry_0; }
	inline void set__markRegistry_0(SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * value)
	{
		____markRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____markRegistry_0), value);
	}

	inline static int32_t get_offset_of__providerMap_1() { return static_cast<int32_t>(offsetof(StandardSingletonProviderCreator_t4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4, ____providerMap_1)); }
	inline Dictionary_2_t7591F89A805922AD4F3EDE546ADC77E45B43A7A9 * get__providerMap_1() const { return ____providerMap_1; }
	inline Dictionary_2_t7591F89A805922AD4F3EDE546ADC77E45B43A7A9 ** get_address_of__providerMap_1() { return &____providerMap_1; }
	inline void set__providerMap_1(Dictionary_2_t7591F89A805922AD4F3EDE546ADC77E45B43A7A9 * value)
	{
		____providerMap_1 = value;
		Il2CppCodeGenWriteBarrier((&____providerMap_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(StandardSingletonProviderCreator_t4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4, ____container_2)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_2() const { return ____container_2; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDSINGLETONPROVIDERCREATOR_T4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4_H
#ifndef SUBCONTAINERCREATORBYINSTALLER_T8BC96FA6ADBE57A6D916B2C30F2119F2DEB852AA_H
#define SUBCONTAINERCREATORBYINSTALLER_T8BC96FA6ADBE57A6D916B2C30F2119F2DEB852AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByInstaller
struct  SubContainerCreatorByInstaller_t8BC96FA6ADBE57A6D916B2C30F2119F2DEB852AA  : public RuntimeObject
{
public:
	// System.Type Zenject.SubContainerCreatorByInstaller::_installerType
	Type_t * ____installerType_0;
	// Zenject.DiContainer Zenject.SubContainerCreatorByInstaller::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByInstaller::_extraArgs
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ____extraArgs_2;

public:
	inline static int32_t get_offset_of__installerType_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorByInstaller_t8BC96FA6ADBE57A6D916B2C30F2119F2DEB852AA, ____installerType_0)); }
	inline Type_t * get__installerType_0() const { return ____installerType_0; }
	inline Type_t ** get_address_of__installerType_0() { return &____installerType_0; }
	inline void set__installerType_0(Type_t * value)
	{
		____installerType_0 = value;
		Il2CppCodeGenWriteBarrier((&____installerType_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorByInstaller_t8BC96FA6ADBE57A6D916B2C30F2119F2DEB852AA, ____container_1)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_1() const { return ____container_1; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__extraArgs_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByInstaller_t8BC96FA6ADBE57A6D916B2C30F2119F2DEB852AA, ____extraArgs_2)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get__extraArgs_2() const { return ____extraArgs_2; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of__extraArgs_2() { return &____extraArgs_2; }
	inline void set__extraArgs_2(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		____extraArgs_2 = value;
		Il2CppCodeGenWriteBarrier((&____extraArgs_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYINSTALLER_T8BC96FA6ADBE57A6D916B2C30F2119F2DEB852AA_H
#ifndef SUBCONTAINERCREATORBYMETHOD_TD7A32725F700CDD41E83CACEF9E7BEC301B00C0D_H
#define SUBCONTAINERCREATORBYMETHOD_TD7A32725F700CDD41E83CACEF9E7BEC301B00C0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByMethod
struct  SubContainerCreatorByMethod_tD7A32725F700CDD41E83CACEF9E7BEC301B00C0D  : public RuntimeObject
{
public:
	// System.Action`1<Zenject.DiContainer> Zenject.SubContainerCreatorByMethod::_installMethod
	Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * ____installMethod_0;
	// Zenject.DiContainer Zenject.SubContainerCreatorByMethod::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_1;

public:
	inline static int32_t get_offset_of__installMethod_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorByMethod_tD7A32725F700CDD41E83CACEF9E7BEC301B00C0D, ____installMethod_0)); }
	inline Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * get__installMethod_0() const { return ____installMethod_0; }
	inline Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A ** get_address_of__installMethod_0() { return &____installMethod_0; }
	inline void set__installMethod_0(Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * value)
	{
		____installMethod_0 = value;
		Il2CppCodeGenWriteBarrier((&____installMethod_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorByMethod_tD7A32725F700CDD41E83CACEF9E7BEC301B00C0D, ____container_1)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_1() const { return ____container_1; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYMETHOD_TD7A32725F700CDD41E83CACEF9E7BEC301B00C0D_H
#ifndef SUBCONTAINERCREATORBYNEWPREFAB_TBDEF1A00B4447F4E476F1427453D94C1773E99B8_H
#define SUBCONTAINERCREATORBYNEWPREFAB_TBDEF1A00B4447F4E476F1427453D94C1773E99B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefab
struct  SubContainerCreatorByNewPrefab_tBDEF1A00B4447F4E476F1427453D94C1773E99B8  : public RuntimeObject
{
public:
	// Zenject.GameObjectCreationParameters Zenject.SubContainerCreatorByNewPrefab::_gameObjectBindInfo
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ____gameObjectBindInfo_0;
	// Zenject.IPrefabProvider Zenject.SubContainerCreatorByNewPrefab::_prefabProvider
	RuntimeObject* ____prefabProvider_1;
	// Zenject.DiContainer Zenject.SubContainerCreatorByNewPrefab::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_2;

public:
	inline static int32_t get_offset_of__gameObjectBindInfo_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefab_tBDEF1A00B4447F4E476F1427453D94C1773E99B8, ____gameObjectBindInfo_0)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get__gameObjectBindInfo_0() const { return ____gameObjectBindInfo_0; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of__gameObjectBindInfo_0() { return &____gameObjectBindInfo_0; }
	inline void set__gameObjectBindInfo_0(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		____gameObjectBindInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_0), value);
	}

	inline static int32_t get_offset_of__prefabProvider_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefab_tBDEF1A00B4447F4E476F1427453D94C1773E99B8, ____prefabProvider_1)); }
	inline RuntimeObject* get__prefabProvider_1() const { return ____prefabProvider_1; }
	inline RuntimeObject** get_address_of__prefabProvider_1() { return &____prefabProvider_1; }
	inline void set__prefabProvider_1(RuntimeObject* value)
	{
		____prefabProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&____prefabProvider_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefab_tBDEF1A00B4447F4E476F1427453D94C1773E99B8, ____container_2)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_2() const { return ____container_2; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYNEWPREFAB_TBDEF1A00B4447F4E476F1427453D94C1773E99B8_H
#ifndef SUBCONTAINERCREATORBYNEWPREFABDYNAMICCONTEXT_TC409E09A3C80E147D7A66BF53D5E858F2049E167_H
#define SUBCONTAINERCREATORBYNEWPREFABDYNAMICCONTEXT_TC409E09A3C80E147D7A66BF53D5E858F2049E167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabDynamicContext
struct  SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167  : public RuntimeObject
{
public:
	// Zenject.GameObjectCreationParameters Zenject.SubContainerCreatorByNewPrefabDynamicContext::_gameObjectBindInfo
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ____gameObjectBindInfo_0;
	// Zenject.IPrefabProvider Zenject.SubContainerCreatorByNewPrefabDynamicContext::_prefabProvider
	RuntimeObject* ____prefabProvider_1;
	// Zenject.DiContainer Zenject.SubContainerCreatorByNewPrefabDynamicContext::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_2;

public:
	inline static int32_t get_offset_of__gameObjectBindInfo_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167, ____gameObjectBindInfo_0)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get__gameObjectBindInfo_0() const { return ____gameObjectBindInfo_0; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of__gameObjectBindInfo_0() { return &____gameObjectBindInfo_0; }
	inline void set__gameObjectBindInfo_0(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		____gameObjectBindInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_0), value);
	}

	inline static int32_t get_offset_of__prefabProvider_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167, ____prefabProvider_1)); }
	inline RuntimeObject* get__prefabProvider_1() const { return ____prefabProvider_1; }
	inline RuntimeObject** get_address_of__prefabProvider_1() { return &____prefabProvider_1; }
	inline void set__prefabProvider_1(RuntimeObject* value)
	{
		____prefabProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&____prefabProvider_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167, ____container_2)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_2() const { return ____container_2; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYNEWPREFABDYNAMICCONTEXT_TC409E09A3C80E147D7A66BF53D5E858F2049E167_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T2EE5CE06A6864907EFFB3C045C04C95ACABD656A_H
#define U3CU3EC__DISPLAYCLASS3_0_T2EE5CE06A6864907EFFB3C045C04C95ACABD656A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabInstaller/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t2EE5CE06A6864907EFFB3C045C04C95ACABD656A  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewPrefabInstaller Zenject.SubContainerCreatorByNewPrefabInstaller/<>c__DisplayClass3_0::<>4__this
	SubContainerCreatorByNewPrefabInstaller_t892C2B19E8FE3B7CB1963B14CBBD29F58041F118 * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewPrefabInstaller/<>c__DisplayClass3_0::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t2EE5CE06A6864907EFFB3C045C04C95ACABD656A, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewPrefabInstaller_t892C2B19E8FE3B7CB1963B14CBBD29F58041F118 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewPrefabInstaller_t892C2B19E8FE3B7CB1963B14CBBD29F58041F118 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewPrefabInstaller_t892C2B19E8FE3B7CB1963B14CBBD29F58041F118 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t2EE5CE06A6864907EFFB3C045C04C95ACABD656A, ___args_1)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_1() const { return ___args_1; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((&___args_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T2EE5CE06A6864907EFFB3C045C04C95ACABD656A_H
#ifndef SUBCONTAINERCREATORBYNEWPREFABWITHPARAMS_TED1333930F622E18559D720142612284A5EF363D_H
#define SUBCONTAINERCREATORBYNEWPREFABWITHPARAMS_TED1333930F622E18559D720142612284A5EF363D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabWithParams
struct  SubContainerCreatorByNewPrefabWithParams_tED1333930F622E18559D720142612284A5EF363D  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerCreatorByNewPrefabWithParams::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_0;
	// Zenject.IPrefabProvider Zenject.SubContainerCreatorByNewPrefabWithParams::_prefabProvider
	RuntimeObject* ____prefabProvider_1;
	// System.Type Zenject.SubContainerCreatorByNewPrefabWithParams::_installerType
	Type_t * ____installerType_2;
	// Zenject.GameObjectCreationParameters Zenject.SubContainerCreatorByNewPrefabWithParams::_gameObjectBindInfo
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ____gameObjectBindInfo_3;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabWithParams_tED1333930F622E18559D720142612284A5EF363D, ____container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_0() const { return ____container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__prefabProvider_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabWithParams_tED1333930F622E18559D720142612284A5EF363D, ____prefabProvider_1)); }
	inline RuntimeObject* get__prefabProvider_1() const { return ____prefabProvider_1; }
	inline RuntimeObject** get_address_of__prefabProvider_1() { return &____prefabProvider_1; }
	inline void set__prefabProvider_1(RuntimeObject* value)
	{
		____prefabProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&____prefabProvider_1), value);
	}

	inline static int32_t get_offset_of__installerType_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabWithParams_tED1333930F622E18559D720142612284A5EF363D, ____installerType_2)); }
	inline Type_t * get__installerType_2() const { return ____installerType_2; }
	inline Type_t ** get_address_of__installerType_2() { return &____installerType_2; }
	inline void set__installerType_2(Type_t * value)
	{
		____installerType_2 = value;
		Il2CppCodeGenWriteBarrier((&____installerType_2), value);
	}

	inline static int32_t get_offset_of__gameObjectBindInfo_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabWithParams_tED1333930F622E18559D720142612284A5EF363D, ____gameObjectBindInfo_3)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get__gameObjectBindInfo_3() const { return ____gameObjectBindInfo_3; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of__gameObjectBindInfo_3() { return &____gameObjectBindInfo_3; }
	inline void set__gameObjectBindInfo_3(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		____gameObjectBindInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYNEWPREFABWITHPARAMS_TED1333930F622E18559D720142612284A5EF363D_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T2666E49305BBB041ED6762ED555EAFF2BDAC9B52_H
#define U3CU3EC__DISPLAYCLASS7_0_T2666E49305BBB041ED6762ED555EAFF2BDAC9B52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabWithParams/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t2666E49305BBB041ED6762ED555EAFF2BDAC9B52  : public RuntimeObject
{
public:
	// Zenject.TypeValuePair Zenject.SubContainerCreatorByNewPrefabWithParams/<>c__DisplayClass7_0::argPair
	TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * ___argPair_0;

public:
	inline static int32_t get_offset_of_argPair_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t2666E49305BBB041ED6762ED555EAFF2BDAC9B52, ___argPair_0)); }
	inline TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * get_argPair_0() const { return ___argPair_0; }
	inline TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B ** get_address_of_argPair_0() { return &___argPair_0; }
	inline void set_argPair_0(TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * value)
	{
		___argPair_0 = value;
		Il2CppCodeGenWriteBarrier((&___argPair_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T2666E49305BBB041ED6762ED555EAFF2BDAC9B52_H
#ifndef SUBCONTAINERCREATORCACHED_TFC8561A0090D43D70962FC95E8ACD20596D4DE79_H
#define SUBCONTAINERCREATORCACHED_TFC8561A0090D43D70962FC95E8ACD20596D4DE79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorCached
struct  SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79  : public RuntimeObject
{
public:
	// Zenject.ISubContainerCreator Zenject.SubContainerCreatorCached::_subCreator
	RuntimeObject* ____subCreator_0;
	// System.Boolean Zenject.SubContainerCreatorCached::_isLookingUp
	bool ____isLookingUp_1;
	// Zenject.DiContainer Zenject.SubContainerCreatorCached::_subContainer
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____subContainer_2;

public:
	inline static int32_t get_offset_of__subCreator_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79, ____subCreator_0)); }
	inline RuntimeObject* get__subCreator_0() const { return ____subCreator_0; }
	inline RuntimeObject** get_address_of__subCreator_0() { return &____subCreator_0; }
	inline void set__subCreator_0(RuntimeObject* value)
	{
		____subCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&____subCreator_0), value);
	}

	inline static int32_t get_offset_of__isLookingUp_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79, ____isLookingUp_1)); }
	inline bool get__isLookingUp_1() const { return ____isLookingUp_1; }
	inline bool* get_address_of__isLookingUp_1() { return &____isLookingUp_1; }
	inline void set__isLookingUp_1(bool value)
	{
		____isLookingUp_1 = value;
	}

	inline static int32_t get_offset_of__subContainer_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79, ____subContainer_2)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__subContainer_2() const { return ____subContainer_2; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__subContainer_2() { return &____subContainer_2; }
	inline void set__subContainer_2(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____subContainer_2 = value;
		Il2CppCodeGenWriteBarrier((&____subContainer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORCACHED_TFC8561A0090D43D70962FC95E8ACD20596D4DE79_H
#ifndef SUBCONTAINERDEPENDENCYPROVIDER_T93AC6894C486CF68BB1B0DE97E462D76416DB2D6_H
#define SUBCONTAINERDEPENDENCYPROVIDER_T93AC6894C486CF68BB1B0DE97E462D76416DB2D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerDependencyProvider
struct  SubContainerDependencyProvider_t93AC6894C486CF68BB1B0DE97E462D76416DB2D6  : public RuntimeObject
{
public:
	// Zenject.ISubContainerCreator Zenject.SubContainerDependencyProvider::_subContainerCreator
	RuntimeObject* ____subContainerCreator_0;
	// System.Type Zenject.SubContainerDependencyProvider::_dependencyType
	Type_t * ____dependencyType_1;
	// System.Object Zenject.SubContainerDependencyProvider::_identifier
	RuntimeObject * ____identifier_2;

public:
	inline static int32_t get_offset_of__subContainerCreator_0() { return static_cast<int32_t>(offsetof(SubContainerDependencyProvider_t93AC6894C486CF68BB1B0DE97E462D76416DB2D6, ____subContainerCreator_0)); }
	inline RuntimeObject* get__subContainerCreator_0() const { return ____subContainerCreator_0; }
	inline RuntimeObject** get_address_of__subContainerCreator_0() { return &____subContainerCreator_0; }
	inline void set__subContainerCreator_0(RuntimeObject* value)
	{
		____subContainerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerCreator_0), value);
	}

	inline static int32_t get_offset_of__dependencyType_1() { return static_cast<int32_t>(offsetof(SubContainerDependencyProvider_t93AC6894C486CF68BB1B0DE97E462D76416DB2D6, ____dependencyType_1)); }
	inline Type_t * get__dependencyType_1() const { return ____dependencyType_1; }
	inline Type_t ** get_address_of__dependencyType_1() { return &____dependencyType_1; }
	inline void set__dependencyType_1(Type_t * value)
	{
		____dependencyType_1 = value;
		Il2CppCodeGenWriteBarrier((&____dependencyType_1), value);
	}

	inline static int32_t get_offset_of__identifier_2() { return static_cast<int32_t>(offsetof(SubContainerDependencyProvider_t93AC6894C486CF68BB1B0DE97E462D76416DB2D6, ____identifier_2)); }
	inline RuntimeObject * get__identifier_2() const { return ____identifier_2; }
	inline RuntimeObject ** get_address_of__identifier_2() { return &____identifier_2; }
	inline void set__identifier_2(RuntimeObject * value)
	{
		____identifier_2 = value;
		Il2CppCodeGenWriteBarrier((&____identifier_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERDEPENDENCYPROVIDER_T93AC6894C486CF68BB1B0DE97E462D76416DB2D6_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__6_TDB48A5FE52A7040AC7712FCF2CD0B77D425281B0_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__6_TDB48A5FE52A7040AC7712FCF2CD0B77D425281B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerDependencyProvider/<GetAllInstancesWithInjectSplit>d__6
struct  U3CGetAllInstancesWithInjectSplitU3Ed__6_tDB48A5FE52A7040AC7712FCF2CD0B77D425281B0  : public RuntimeObject
{
public:
	// System.Int32 Zenject.SubContainerDependencyProvider/<GetAllInstancesWithInjectSplit>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.SubContainerDependencyProvider/<GetAllInstancesWithInjectSplit>d__6::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.SubContainerDependencyProvider/<GetAllInstancesWithInjectSplit>d__6::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_2;
	// Zenject.SubContainerDependencyProvider Zenject.SubContainerDependencyProvider/<GetAllInstancesWithInjectSplit>d__6::<>4__this
	SubContainerDependencyProvider_t93AC6894C486CF68BB1B0DE97E462D76416DB2D6 * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerDependencyProvider/<GetAllInstancesWithInjectSplit>d__6::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__6_tDB48A5FE52A7040AC7712FCF2CD0B77D425281B0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__6_tDB48A5FE52A7040AC7712FCF2CD0B77D425281B0, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__6_tDB48A5FE52A7040AC7712FCF2CD0B77D425281B0, ___context_2)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_2() const { return ___context_2; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__6_tDB48A5FE52A7040AC7712FCF2CD0B77D425281B0, ___U3CU3E4__this_3)); }
	inline SubContainerDependencyProvider_t93AC6894C486CF68BB1B0DE97E462D76416DB2D6 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline SubContainerDependencyProvider_t93AC6894C486CF68BB1B0DE97E462D76416DB2D6 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(SubContainerDependencyProvider_t93AC6894C486CF68BB1B0DE97E462D76416DB2D6 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__6_tDB48A5FE52A7040AC7712FCF2CD0B77D425281B0, ___args_4)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_4() const { return ___args_4; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__6_TDB48A5FE52A7040AC7712FCF2CD0B77D425281B0_H
#ifndef SUBCONTAINERSINGLETONPROVIDERCREATORBYINSTALLER_T90FB582E91D07E51B7C4584C4E3E071397AE2B09_H
#define SUBCONTAINERSINGLETONPROVIDERCREATORBYINSTALLER_T90FB582E91D07E51B7C4584C4E3E071397AE2B09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByInstaller
struct  SubContainerSingletonProviderCreatorByInstaller_t90FB582E91D07E51B7C4584C4E3E071397AE2B09  : public RuntimeObject
{
public:
	// Zenject.SingletonMarkRegistry Zenject.SubContainerSingletonProviderCreatorByInstaller::_markRegistry
	SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * ____markRegistry_0;
	// Zenject.DiContainer Zenject.SubContainerSingletonProviderCreatorByInstaller::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_1;
	// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByInstaller/InstallerSingletonId,Zenject.ISubContainerCreator> Zenject.SubContainerSingletonProviderCreatorByInstaller::_subContainerCreators
	Dictionary_2_t1FA0B8DA3B73D042A0D9F7C1048EF1A3DC525109 * ____subContainerCreators_2;

public:
	inline static int32_t get_offset_of__markRegistry_0() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByInstaller_t90FB582E91D07E51B7C4584C4E3E071397AE2B09, ____markRegistry_0)); }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * get__markRegistry_0() const { return ____markRegistry_0; }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 ** get_address_of__markRegistry_0() { return &____markRegistry_0; }
	inline void set__markRegistry_0(SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * value)
	{
		____markRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____markRegistry_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByInstaller_t90FB582E91D07E51B7C4584C4E3E071397AE2B09, ____container_1)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_1() const { return ____container_1; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__subContainerCreators_2() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByInstaller_t90FB582E91D07E51B7C4584C4E3E071397AE2B09, ____subContainerCreators_2)); }
	inline Dictionary_2_t1FA0B8DA3B73D042A0D9F7C1048EF1A3DC525109 * get__subContainerCreators_2() const { return ____subContainerCreators_2; }
	inline Dictionary_2_t1FA0B8DA3B73D042A0D9F7C1048EF1A3DC525109 ** get_address_of__subContainerCreators_2() { return &____subContainerCreators_2; }
	inline void set__subContainerCreators_2(Dictionary_2_t1FA0B8DA3B73D042A0D9F7C1048EF1A3DC525109 * value)
	{
		____subContainerCreators_2 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerCreators_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERSINGLETONPROVIDERCREATORBYINSTALLER_T90FB582E91D07E51B7C4584C4E3E071397AE2B09_H
#ifndef INSTALLERSINGLETONID_T6A3E9FF03A9FA3DE51F56900C7FE68014D5D7544_H
#define INSTALLERSINGLETONID_T6A3E9FF03A9FA3DE51F56900C7FE68014D5D7544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByInstaller/InstallerSingletonId
struct  InstallerSingletonId_t6A3E9FF03A9FA3DE51F56900C7FE68014D5D7544  : public RuntimeObject
{
public:
	// System.Object Zenject.SubContainerSingletonProviderCreatorByInstaller/InstallerSingletonId::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_0;
	// System.Type Zenject.SubContainerSingletonProviderCreatorByInstaller/InstallerSingletonId::InstallerType
	Type_t * ___InstallerType_1;

public:
	inline static int32_t get_offset_of_ConcreteIdentifier_0() { return static_cast<int32_t>(offsetof(InstallerSingletonId_t6A3E9FF03A9FA3DE51F56900C7FE68014D5D7544, ___ConcreteIdentifier_0)); }
	inline RuntimeObject * get_ConcreteIdentifier_0() const { return ___ConcreteIdentifier_0; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_0() { return &___ConcreteIdentifier_0; }
	inline void set_ConcreteIdentifier_0(RuntimeObject * value)
	{
		___ConcreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of_InstallerType_1() { return static_cast<int32_t>(offsetof(InstallerSingletonId_t6A3E9FF03A9FA3DE51F56900C7FE68014D5D7544, ___InstallerType_1)); }
	inline Type_t * get_InstallerType_1() const { return ___InstallerType_1; }
	inline Type_t ** get_address_of_InstallerType_1() { return &___InstallerType_1; }
	inline void set_InstallerType_1(Type_t * value)
	{
		___InstallerType_1 = value;
		Il2CppCodeGenWriteBarrier((&___InstallerType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLERSINGLETONID_T6A3E9FF03A9FA3DE51F56900C7FE68014D5D7544_H
#ifndef SUBCONTAINERSINGLETONPROVIDERCREATORBYMETHOD_T5BFA8689BE01E6E287C528411DC83D46F08BA07F_H
#define SUBCONTAINERSINGLETONPROVIDERCREATORBYMETHOD_T5BFA8689BE01E6E287C528411DC83D46F08BA07F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByMethod
struct  SubContainerSingletonProviderCreatorByMethod_t5BFA8689BE01E6E287C528411DC83D46F08BA07F  : public RuntimeObject
{
public:
	// Zenject.SingletonMarkRegistry Zenject.SubContainerSingletonProviderCreatorByMethod::_markRegistry
	SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * ____markRegistry_0;
	// Zenject.DiContainer Zenject.SubContainerSingletonProviderCreatorByMethod::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_1;
	// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByMethod/MethodSingletonId,Zenject.ISubContainerCreator> Zenject.SubContainerSingletonProviderCreatorByMethod::_subContainerCreators
	Dictionary_2_t50707A64E6E606785AF10F0AAF88016E6D471149 * ____subContainerCreators_2;

public:
	inline static int32_t get_offset_of__markRegistry_0() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByMethod_t5BFA8689BE01E6E287C528411DC83D46F08BA07F, ____markRegistry_0)); }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * get__markRegistry_0() const { return ____markRegistry_0; }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 ** get_address_of__markRegistry_0() { return &____markRegistry_0; }
	inline void set__markRegistry_0(SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * value)
	{
		____markRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____markRegistry_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByMethod_t5BFA8689BE01E6E287C528411DC83D46F08BA07F, ____container_1)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_1() const { return ____container_1; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__subContainerCreators_2() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByMethod_t5BFA8689BE01E6E287C528411DC83D46F08BA07F, ____subContainerCreators_2)); }
	inline Dictionary_2_t50707A64E6E606785AF10F0AAF88016E6D471149 * get__subContainerCreators_2() const { return ____subContainerCreators_2; }
	inline Dictionary_2_t50707A64E6E606785AF10F0AAF88016E6D471149 ** get_address_of__subContainerCreators_2() { return &____subContainerCreators_2; }
	inline void set__subContainerCreators_2(Dictionary_2_t50707A64E6E606785AF10F0AAF88016E6D471149 * value)
	{
		____subContainerCreators_2 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerCreators_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERSINGLETONPROVIDERCREATORBYMETHOD_T5BFA8689BE01E6E287C528411DC83D46F08BA07F_H
#ifndef METHODSINGLETONID_TBC5ADCBA6BDCF4A9353CF364E0A903CCDFA2C651_H
#define METHODSINGLETONID_TBC5ADCBA6BDCF4A9353CF364E0A903CCDFA2C651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByMethod/MethodSingletonId
struct  MethodSingletonId_tBC5ADCBA6BDCF4A9353CF364E0A903CCDFA2C651  : public RuntimeObject
{
public:
	// System.Object Zenject.SubContainerSingletonProviderCreatorByMethod/MethodSingletonId::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_0;
	// System.Delegate Zenject.SubContainerSingletonProviderCreatorByMethod/MethodSingletonId::InstallerDelegate
	Delegate_t * ___InstallerDelegate_1;

public:
	inline static int32_t get_offset_of_ConcreteIdentifier_0() { return static_cast<int32_t>(offsetof(MethodSingletonId_tBC5ADCBA6BDCF4A9353CF364E0A903CCDFA2C651, ___ConcreteIdentifier_0)); }
	inline RuntimeObject * get_ConcreteIdentifier_0() const { return ___ConcreteIdentifier_0; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_0() { return &___ConcreteIdentifier_0; }
	inline void set_ConcreteIdentifier_0(RuntimeObject * value)
	{
		___ConcreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of_InstallerDelegate_1() { return static_cast<int32_t>(offsetof(MethodSingletonId_tBC5ADCBA6BDCF4A9353CF364E0A903CCDFA2C651, ___InstallerDelegate_1)); }
	inline Delegate_t * get_InstallerDelegate_1() const { return ___InstallerDelegate_1; }
	inline Delegate_t ** get_address_of_InstallerDelegate_1() { return &___InstallerDelegate_1; }
	inline void set_InstallerDelegate_1(Delegate_t * value)
	{
		___InstallerDelegate_1 = value;
		Il2CppCodeGenWriteBarrier((&___InstallerDelegate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODSINGLETONID_TBC5ADCBA6BDCF4A9353CF364E0A903CCDFA2C651_H
#ifndef SUBCONTAINERSINGLETONPROVIDERCREATORBYNEWPREFAB_T0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8_H
#define SUBCONTAINERSINGLETONPROVIDERCREATORBYNEWPREFAB_T0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByNewPrefab
struct  SubContainerSingletonProviderCreatorByNewPrefab_t0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8  : public RuntimeObject
{
public:
	// Zenject.SingletonMarkRegistry Zenject.SubContainerSingletonProviderCreatorByNewPrefab::_markRegistry
	SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * ____markRegistry_0;
	// Zenject.DiContainer Zenject.SubContainerSingletonProviderCreatorByNewPrefab::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_1;
	// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByNewPrefab/CustomSingletonId,Zenject.SubContainerSingletonProviderCreatorByNewPrefab/CreatorInfo> Zenject.SubContainerSingletonProviderCreatorByNewPrefab::_subContainerCreators
	Dictionary_2_t8846C037DA2FFEC58347809EC9DF1CF50E18792C * ____subContainerCreators_2;

public:
	inline static int32_t get_offset_of__markRegistry_0() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByNewPrefab_t0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8, ____markRegistry_0)); }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * get__markRegistry_0() const { return ____markRegistry_0; }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 ** get_address_of__markRegistry_0() { return &____markRegistry_0; }
	inline void set__markRegistry_0(SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * value)
	{
		____markRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____markRegistry_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByNewPrefab_t0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8, ____container_1)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_1() const { return ____container_1; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__subContainerCreators_2() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByNewPrefab_t0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8, ____subContainerCreators_2)); }
	inline Dictionary_2_t8846C037DA2FFEC58347809EC9DF1CF50E18792C * get__subContainerCreators_2() const { return ____subContainerCreators_2; }
	inline Dictionary_2_t8846C037DA2FFEC58347809EC9DF1CF50E18792C ** get_address_of__subContainerCreators_2() { return &____subContainerCreators_2; }
	inline void set__subContainerCreators_2(Dictionary_2_t8846C037DA2FFEC58347809EC9DF1CF50E18792C * value)
	{
		____subContainerCreators_2 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerCreators_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERSINGLETONPROVIDERCREATORBYNEWPREFAB_T0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8_H
#ifndef CREATORINFO_TBA7CED1A7F0F4F268BB6D2CA731322DAB3CAC812_H
#define CREATORINFO_TBA7CED1A7F0F4F268BB6D2CA731322DAB3CAC812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByNewPrefab/CreatorInfo
struct  CreatorInfo_tBA7CED1A7F0F4F268BB6D2CA731322DAB3CAC812  : public RuntimeObject
{
public:
	// Zenject.GameObjectCreationParameters Zenject.SubContainerSingletonProviderCreatorByNewPrefab/CreatorInfo::<GameObjectCreationParameters>k__BackingField
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ___U3CGameObjectCreationParametersU3Ek__BackingField_0;
	// Zenject.ISubContainerCreator Zenject.SubContainerSingletonProviderCreatorByNewPrefab/CreatorInfo::<Creator>k__BackingField
	RuntimeObject* ___U3CCreatorU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CGameObjectCreationParametersU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreatorInfo_tBA7CED1A7F0F4F268BB6D2CA731322DAB3CAC812, ___U3CGameObjectCreationParametersU3Ek__BackingField_0)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get_U3CGameObjectCreationParametersU3Ek__BackingField_0() const { return ___U3CGameObjectCreationParametersU3Ek__BackingField_0; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of_U3CGameObjectCreationParametersU3Ek__BackingField_0() { return &___U3CGameObjectCreationParametersU3Ek__BackingField_0; }
	inline void set_U3CGameObjectCreationParametersU3Ek__BackingField_0(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		___U3CGameObjectCreationParametersU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectCreationParametersU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCreatorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreatorInfo_tBA7CED1A7F0F4F268BB6D2CA731322DAB3CAC812, ___U3CCreatorU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CCreatorU3Ek__BackingField_1() const { return ___U3CCreatorU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CCreatorU3Ek__BackingField_1() { return &___U3CCreatorU3Ek__BackingField_1; }
	inline void set_U3CCreatorU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CCreatorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCreatorU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATORINFO_TBA7CED1A7F0F4F268BB6D2CA731322DAB3CAC812_H
#ifndef CUSTOMSINGLETONID_TC9251DAA97612544E328905AD12613D087E8EAE5_H
#define CUSTOMSINGLETONID_TC9251DAA97612544E328905AD12613D087E8EAE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByNewPrefab/CustomSingletonId
struct  CustomSingletonId_tC9251DAA97612544E328905AD12613D087E8EAE5  : public RuntimeObject
{
public:
	// System.Object Zenject.SubContainerSingletonProviderCreatorByNewPrefab/CustomSingletonId::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_0;
	// UnityEngine.Object Zenject.SubContainerSingletonProviderCreatorByNewPrefab/CustomSingletonId::Prefab
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___Prefab_1;

public:
	inline static int32_t get_offset_of_ConcreteIdentifier_0() { return static_cast<int32_t>(offsetof(CustomSingletonId_tC9251DAA97612544E328905AD12613D087E8EAE5, ___ConcreteIdentifier_0)); }
	inline RuntimeObject * get_ConcreteIdentifier_0() const { return ___ConcreteIdentifier_0; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_0() { return &___ConcreteIdentifier_0; }
	inline void set_ConcreteIdentifier_0(RuntimeObject * value)
	{
		___ConcreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of_Prefab_1() { return static_cast<int32_t>(offsetof(CustomSingletonId_tC9251DAA97612544E328905AD12613D087E8EAE5, ___Prefab_1)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_Prefab_1() const { return ___Prefab_1; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_Prefab_1() { return &___Prefab_1; }
	inline void set_Prefab_1(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___Prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMSINGLETONID_TC9251DAA97612544E328905AD12613D087E8EAE5_H
#ifndef SUBCONTAINERSINGLETONPROVIDERCREATORBYNEWPREFABRESOURCE_T376F2451341DE65400990403DF0B5A966ECCBCCB_H
#define SUBCONTAINERSINGLETONPROVIDERCREATORBYNEWPREFABRESOURCE_T376F2451341DE65400990403DF0B5A966ECCBCCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource
struct  SubContainerSingletonProviderCreatorByNewPrefabResource_t376F2451341DE65400990403DF0B5A966ECCBCCB  : public RuntimeObject
{
public:
	// Zenject.SingletonMarkRegistry Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource::_markRegistry
	SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * ____markRegistry_0;
	// Zenject.DiContainer Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_1;
	// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource/CustomSingletonId,Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource/CreatorInfo> Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource::_subContainerCreators
	Dictionary_2_t51C3DD62CC36B685F4B94CC25BCAF2A2F0A8E3A3 * ____subContainerCreators_2;

public:
	inline static int32_t get_offset_of__markRegistry_0() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByNewPrefabResource_t376F2451341DE65400990403DF0B5A966ECCBCCB, ____markRegistry_0)); }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * get__markRegistry_0() const { return ____markRegistry_0; }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 ** get_address_of__markRegistry_0() { return &____markRegistry_0; }
	inline void set__markRegistry_0(SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * value)
	{
		____markRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____markRegistry_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByNewPrefabResource_t376F2451341DE65400990403DF0B5A966ECCBCCB, ____container_1)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_1() const { return ____container_1; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__subContainerCreators_2() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByNewPrefabResource_t376F2451341DE65400990403DF0B5A966ECCBCCB, ____subContainerCreators_2)); }
	inline Dictionary_2_t51C3DD62CC36B685F4B94CC25BCAF2A2F0A8E3A3 * get__subContainerCreators_2() const { return ____subContainerCreators_2; }
	inline Dictionary_2_t51C3DD62CC36B685F4B94CC25BCAF2A2F0A8E3A3 ** get_address_of__subContainerCreators_2() { return &____subContainerCreators_2; }
	inline void set__subContainerCreators_2(Dictionary_2_t51C3DD62CC36B685F4B94CC25BCAF2A2F0A8E3A3 * value)
	{
		____subContainerCreators_2 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerCreators_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERSINGLETONPROVIDERCREATORBYNEWPREFABRESOURCE_T376F2451341DE65400990403DF0B5A966ECCBCCB_H
#ifndef CREATORINFO_TF98CAECC326F3E1A8014BD3668EC8A7CACC9309A_H
#define CREATORINFO_TF98CAECC326F3E1A8014BD3668EC8A7CACC9309A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource/CreatorInfo
struct  CreatorInfo_tF98CAECC326F3E1A8014BD3668EC8A7CACC9309A  : public RuntimeObject
{
public:
	// Zenject.GameObjectCreationParameters Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource/CreatorInfo::<GameObjectCreationParameters>k__BackingField
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ___U3CGameObjectCreationParametersU3Ek__BackingField_0;
	// Zenject.ISubContainerCreator Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource/CreatorInfo::<Creator>k__BackingField
	RuntimeObject* ___U3CCreatorU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CGameObjectCreationParametersU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreatorInfo_tF98CAECC326F3E1A8014BD3668EC8A7CACC9309A, ___U3CGameObjectCreationParametersU3Ek__BackingField_0)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get_U3CGameObjectCreationParametersU3Ek__BackingField_0() const { return ___U3CGameObjectCreationParametersU3Ek__BackingField_0; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of_U3CGameObjectCreationParametersU3Ek__BackingField_0() { return &___U3CGameObjectCreationParametersU3Ek__BackingField_0; }
	inline void set_U3CGameObjectCreationParametersU3Ek__BackingField_0(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		___U3CGameObjectCreationParametersU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectCreationParametersU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCreatorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreatorInfo_tF98CAECC326F3E1A8014BD3668EC8A7CACC9309A, ___U3CCreatorU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CCreatorU3Ek__BackingField_1() const { return ___U3CCreatorU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CCreatorU3Ek__BackingField_1() { return &___U3CCreatorU3Ek__BackingField_1; }
	inline void set_U3CCreatorU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CCreatorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCreatorU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATORINFO_TF98CAECC326F3E1A8014BD3668EC8A7CACC9309A_H
#ifndef CUSTOMSINGLETONID_T6F774376C22181BC6BF985D411FAABFCFC68BC1A_H
#define CUSTOMSINGLETONID_T6F774376C22181BC6BF985D411FAABFCFC68BC1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource/CustomSingletonId
struct  CustomSingletonId_t6F774376C22181BC6BF985D411FAABFCFC68BC1A  : public RuntimeObject
{
public:
	// System.Object Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource/CustomSingletonId::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_0;
	// System.String Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource/CustomSingletonId::ResourcePath
	String_t* ___ResourcePath_1;

public:
	inline static int32_t get_offset_of_ConcreteIdentifier_0() { return static_cast<int32_t>(offsetof(CustomSingletonId_t6F774376C22181BC6BF985D411FAABFCFC68BC1A, ___ConcreteIdentifier_0)); }
	inline RuntimeObject * get_ConcreteIdentifier_0() const { return ___ConcreteIdentifier_0; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_0() { return &___ConcreteIdentifier_0; }
	inline void set_ConcreteIdentifier_0(RuntimeObject * value)
	{
		___ConcreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of_ResourcePath_1() { return static_cast<int32_t>(offsetof(CustomSingletonId_t6F774376C22181BC6BF985D411FAABFCFC68BC1A, ___ResourcePath_1)); }
	inline String_t* get_ResourcePath_1() const { return ___ResourcePath_1; }
	inline String_t** get_address_of_ResourcePath_1() { return &___ResourcePath_1; }
	inline void set_ResourcePath_1(String_t* value)
	{
		___ResourcePath_1 = value;
		Il2CppCodeGenWriteBarrier((&___ResourcePath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMSINGLETONID_T6F774376C22181BC6BF985D411FAABFCFC68BC1A_H
#ifndef TASKUPDATER_1_TAAE053B2BFF1746D9F4F6DA69CD81358F82F5278_H
#define TASKUPDATER_1_TAAE053B2BFF1746D9F4F6DA69CD81358F82F5278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TaskUpdater`1<Zenject.IFixedTickable>
struct  TaskUpdater_1_tAAE053B2BFF1746D9F4F6DA69CD81358F82F5278  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1::_tasks
	LinkedList_1_t5A41455EC6EC7657DD6CF98C6FDA1A581DAE472B * ____tasks_0;
	// System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1::_queuedTasks
	List_1_t5F62B547F88E623C272155861602CF84E918E93B * ____queuedTasks_1;

public:
	inline static int32_t get_offset_of__tasks_0() { return static_cast<int32_t>(offsetof(TaskUpdater_1_tAAE053B2BFF1746D9F4F6DA69CD81358F82F5278, ____tasks_0)); }
	inline LinkedList_1_t5A41455EC6EC7657DD6CF98C6FDA1A581DAE472B * get__tasks_0() const { return ____tasks_0; }
	inline LinkedList_1_t5A41455EC6EC7657DD6CF98C6FDA1A581DAE472B ** get_address_of__tasks_0() { return &____tasks_0; }
	inline void set__tasks_0(LinkedList_1_t5A41455EC6EC7657DD6CF98C6FDA1A581DAE472B * value)
	{
		____tasks_0 = value;
		Il2CppCodeGenWriteBarrier((&____tasks_0), value);
	}

	inline static int32_t get_offset_of__queuedTasks_1() { return static_cast<int32_t>(offsetof(TaskUpdater_1_tAAE053B2BFF1746D9F4F6DA69CD81358F82F5278, ____queuedTasks_1)); }
	inline List_1_t5F62B547F88E623C272155861602CF84E918E93B * get__queuedTasks_1() const { return ____queuedTasks_1; }
	inline List_1_t5F62B547F88E623C272155861602CF84E918E93B ** get_address_of__queuedTasks_1() { return &____queuedTasks_1; }
	inline void set__queuedTasks_1(List_1_t5F62B547F88E623C272155861602CF84E918E93B * value)
	{
		____queuedTasks_1 = value;
		Il2CppCodeGenWriteBarrier((&____queuedTasks_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKUPDATER_1_TAAE053B2BFF1746D9F4F6DA69CD81358F82F5278_H
#ifndef TASKUPDATER_1_T53BC21CD324599BB87353961590F4AF920E9CA3E_H
#define TASKUPDATER_1_T53BC21CD324599BB87353961590F4AF920E9CA3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TaskUpdater`1<Zenject.ILateTickable>
struct  TaskUpdater_1_t53BC21CD324599BB87353961590F4AF920E9CA3E  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1::_tasks
	LinkedList_1_tD95317941B347B3D6848946C2964299622936C16 * ____tasks_0;
	// System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1::_queuedTasks
	List_1_t4514D67474103773FAF8C8A0DB672AF1E9EE86EA * ____queuedTasks_1;

public:
	inline static int32_t get_offset_of__tasks_0() { return static_cast<int32_t>(offsetof(TaskUpdater_1_t53BC21CD324599BB87353961590F4AF920E9CA3E, ____tasks_0)); }
	inline LinkedList_1_tD95317941B347B3D6848946C2964299622936C16 * get__tasks_0() const { return ____tasks_0; }
	inline LinkedList_1_tD95317941B347B3D6848946C2964299622936C16 ** get_address_of__tasks_0() { return &____tasks_0; }
	inline void set__tasks_0(LinkedList_1_tD95317941B347B3D6848946C2964299622936C16 * value)
	{
		____tasks_0 = value;
		Il2CppCodeGenWriteBarrier((&____tasks_0), value);
	}

	inline static int32_t get_offset_of__queuedTasks_1() { return static_cast<int32_t>(offsetof(TaskUpdater_1_t53BC21CD324599BB87353961590F4AF920E9CA3E, ____queuedTasks_1)); }
	inline List_1_t4514D67474103773FAF8C8A0DB672AF1E9EE86EA * get__queuedTasks_1() const { return ____queuedTasks_1; }
	inline List_1_t4514D67474103773FAF8C8A0DB672AF1E9EE86EA ** get_address_of__queuedTasks_1() { return &____queuedTasks_1; }
	inline void set__queuedTasks_1(List_1_t4514D67474103773FAF8C8A0DB672AF1E9EE86EA * value)
	{
		____queuedTasks_1 = value;
		Il2CppCodeGenWriteBarrier((&____queuedTasks_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKUPDATER_1_T53BC21CD324599BB87353961590F4AF920E9CA3E_H
#ifndef TASKUPDATER_1_T1970C92ED887D656C35B2A95ED880A1B63793D5F_H
#define TASKUPDATER_1_T1970C92ED887D656C35B2A95ED880A1B63793D5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TaskUpdater`1<Zenject.ITickable>
struct  TaskUpdater_1_t1970C92ED887D656C35B2A95ED880A1B63793D5F  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1::_tasks
	LinkedList_1_t37788D54EFBFA8ADC2543D7DEA0723ADDC65C484 * ____tasks_0;
	// System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1::_queuedTasks
	List_1_tCEB34192A8ED851461BFA0B92DFF27C4BD50564B * ____queuedTasks_1;

public:
	inline static int32_t get_offset_of__tasks_0() { return static_cast<int32_t>(offsetof(TaskUpdater_1_t1970C92ED887D656C35B2A95ED880A1B63793D5F, ____tasks_0)); }
	inline LinkedList_1_t37788D54EFBFA8ADC2543D7DEA0723ADDC65C484 * get__tasks_0() const { return ____tasks_0; }
	inline LinkedList_1_t37788D54EFBFA8ADC2543D7DEA0723ADDC65C484 ** get_address_of__tasks_0() { return &____tasks_0; }
	inline void set__tasks_0(LinkedList_1_t37788D54EFBFA8ADC2543D7DEA0723ADDC65C484 * value)
	{
		____tasks_0 = value;
		Il2CppCodeGenWriteBarrier((&____tasks_0), value);
	}

	inline static int32_t get_offset_of__queuedTasks_1() { return static_cast<int32_t>(offsetof(TaskUpdater_1_t1970C92ED887D656C35B2A95ED880A1B63793D5F, ____queuedTasks_1)); }
	inline List_1_tCEB34192A8ED851461BFA0B92DFF27C4BD50564B * get__queuedTasks_1() const { return ____queuedTasks_1; }
	inline List_1_tCEB34192A8ED851461BFA0B92DFF27C4BD50564B ** get_address_of__queuedTasks_1() { return &____queuedTasks_1; }
	inline void set__queuedTasks_1(List_1_tCEB34192A8ED851461BFA0B92DFF27C4BD50564B * value)
	{
		____queuedTasks_1 = value;
		Il2CppCodeGenWriteBarrier((&____queuedTasks_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKUPDATER_1_T1970C92ED887D656C35B2A95ED880A1B63793D5F_H
#ifndef TICKABLEMANAGER_TBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA_H
#define TICKABLEMANAGER_TBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TickableManager
struct  TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA  : public RuntimeObject
{
public:
	// Zenject.Lazy`1<System.Collections.Generic.List`1<Zenject.TickableManager>> Zenject.TickableManager::_parents
	Lazy_1_t06CCCB9361FC529CFE74E4D0AB4387409C38D34F * ____parents_0;
	// System.Collections.Generic.List`1<Zenject.ITickable> Zenject.TickableManager::_tickables
	List_1_tF214108319785C2E14A76893BD2A1BA818E54783 * ____tickables_1;
	// System.Collections.Generic.List`1<Zenject.IFixedTickable> Zenject.TickableManager::_fixedTickables
	List_1_t9BE4861DF8744524532C1FECB05B1972B831140C * ____fixedTickables_2;
	// System.Collections.Generic.List`1<Zenject.ILateTickable> Zenject.TickableManager::_lateTickables
	List_1_tA91F547197BE53A34015BB0D06B83EDEFC988324 * ____lateTickables_3;
	// System.Collections.Generic.List`1<ModestTree.Util.ValuePair`2<System.Type,System.Int32>> Zenject.TickableManager::_priorities
	List_1_t48E35A490E0E56462F4B2D7D1DEE685515F9EFA9 * ____priorities_4;
	// System.Collections.Generic.List`1<ModestTree.Util.ValuePair`2<System.Type,System.Int32>> Zenject.TickableManager::_fixedPriorities
	List_1_t48E35A490E0E56462F4B2D7D1DEE685515F9EFA9 * ____fixedPriorities_5;
	// System.Collections.Generic.List`1<ModestTree.Util.ValuePair`2<System.Type,System.Int32>> Zenject.TickableManager::_latePriorities
	List_1_t48E35A490E0E56462F4B2D7D1DEE685515F9EFA9 * ____latePriorities_6;
	// Zenject.TickablesTaskUpdater Zenject.TickableManager::_updater
	TickablesTaskUpdater_tCE7741F2A32B9750025C1F357267144B1F62FBE0 * ____updater_7;
	// Zenject.FixedTickablesTaskUpdater Zenject.TickableManager::_fixedUpdater
	FixedTickablesTaskUpdater_t8455644BFB7A3B25FBA48F26B4A5253F8208CF20 * ____fixedUpdater_8;
	// Zenject.LateTickablesTaskUpdater Zenject.TickableManager::_lateUpdater
	LateTickablesTaskUpdater_t03C7EF16845586540065A3D08D17571840CD4F24 * ____lateUpdater_9;
	// System.Boolean Zenject.TickableManager::_isPaused
	bool ____isPaused_10;

public:
	inline static int32_t get_offset_of__parents_0() { return static_cast<int32_t>(offsetof(TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA, ____parents_0)); }
	inline Lazy_1_t06CCCB9361FC529CFE74E4D0AB4387409C38D34F * get__parents_0() const { return ____parents_0; }
	inline Lazy_1_t06CCCB9361FC529CFE74E4D0AB4387409C38D34F ** get_address_of__parents_0() { return &____parents_0; }
	inline void set__parents_0(Lazy_1_t06CCCB9361FC529CFE74E4D0AB4387409C38D34F * value)
	{
		____parents_0 = value;
		Il2CppCodeGenWriteBarrier((&____parents_0), value);
	}

	inline static int32_t get_offset_of__tickables_1() { return static_cast<int32_t>(offsetof(TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA, ____tickables_1)); }
	inline List_1_tF214108319785C2E14A76893BD2A1BA818E54783 * get__tickables_1() const { return ____tickables_1; }
	inline List_1_tF214108319785C2E14A76893BD2A1BA818E54783 ** get_address_of__tickables_1() { return &____tickables_1; }
	inline void set__tickables_1(List_1_tF214108319785C2E14A76893BD2A1BA818E54783 * value)
	{
		____tickables_1 = value;
		Il2CppCodeGenWriteBarrier((&____tickables_1), value);
	}

	inline static int32_t get_offset_of__fixedTickables_2() { return static_cast<int32_t>(offsetof(TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA, ____fixedTickables_2)); }
	inline List_1_t9BE4861DF8744524532C1FECB05B1972B831140C * get__fixedTickables_2() const { return ____fixedTickables_2; }
	inline List_1_t9BE4861DF8744524532C1FECB05B1972B831140C ** get_address_of__fixedTickables_2() { return &____fixedTickables_2; }
	inline void set__fixedTickables_2(List_1_t9BE4861DF8744524532C1FECB05B1972B831140C * value)
	{
		____fixedTickables_2 = value;
		Il2CppCodeGenWriteBarrier((&____fixedTickables_2), value);
	}

	inline static int32_t get_offset_of__lateTickables_3() { return static_cast<int32_t>(offsetof(TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA, ____lateTickables_3)); }
	inline List_1_tA91F547197BE53A34015BB0D06B83EDEFC988324 * get__lateTickables_3() const { return ____lateTickables_3; }
	inline List_1_tA91F547197BE53A34015BB0D06B83EDEFC988324 ** get_address_of__lateTickables_3() { return &____lateTickables_3; }
	inline void set__lateTickables_3(List_1_tA91F547197BE53A34015BB0D06B83EDEFC988324 * value)
	{
		____lateTickables_3 = value;
		Il2CppCodeGenWriteBarrier((&____lateTickables_3), value);
	}

	inline static int32_t get_offset_of__priorities_4() { return static_cast<int32_t>(offsetof(TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA, ____priorities_4)); }
	inline List_1_t48E35A490E0E56462F4B2D7D1DEE685515F9EFA9 * get__priorities_4() const { return ____priorities_4; }
	inline List_1_t48E35A490E0E56462F4B2D7D1DEE685515F9EFA9 ** get_address_of__priorities_4() { return &____priorities_4; }
	inline void set__priorities_4(List_1_t48E35A490E0E56462F4B2D7D1DEE685515F9EFA9 * value)
	{
		____priorities_4 = value;
		Il2CppCodeGenWriteBarrier((&____priorities_4), value);
	}

	inline static int32_t get_offset_of__fixedPriorities_5() { return static_cast<int32_t>(offsetof(TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA, ____fixedPriorities_5)); }
	inline List_1_t48E35A490E0E56462F4B2D7D1DEE685515F9EFA9 * get__fixedPriorities_5() const { return ____fixedPriorities_5; }
	inline List_1_t48E35A490E0E56462F4B2D7D1DEE685515F9EFA9 ** get_address_of__fixedPriorities_5() { return &____fixedPriorities_5; }
	inline void set__fixedPriorities_5(List_1_t48E35A490E0E56462F4B2D7D1DEE685515F9EFA9 * value)
	{
		____fixedPriorities_5 = value;
		Il2CppCodeGenWriteBarrier((&____fixedPriorities_5), value);
	}

	inline static int32_t get_offset_of__latePriorities_6() { return static_cast<int32_t>(offsetof(TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA, ____latePriorities_6)); }
	inline List_1_t48E35A490E0E56462F4B2D7D1DEE685515F9EFA9 * get__latePriorities_6() const { return ____latePriorities_6; }
	inline List_1_t48E35A490E0E56462F4B2D7D1DEE685515F9EFA9 ** get_address_of__latePriorities_6() { return &____latePriorities_6; }
	inline void set__latePriorities_6(List_1_t48E35A490E0E56462F4B2D7D1DEE685515F9EFA9 * value)
	{
		____latePriorities_6 = value;
		Il2CppCodeGenWriteBarrier((&____latePriorities_6), value);
	}

	inline static int32_t get_offset_of__updater_7() { return static_cast<int32_t>(offsetof(TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA, ____updater_7)); }
	inline TickablesTaskUpdater_tCE7741F2A32B9750025C1F357267144B1F62FBE0 * get__updater_7() const { return ____updater_7; }
	inline TickablesTaskUpdater_tCE7741F2A32B9750025C1F357267144B1F62FBE0 ** get_address_of__updater_7() { return &____updater_7; }
	inline void set__updater_7(TickablesTaskUpdater_tCE7741F2A32B9750025C1F357267144B1F62FBE0 * value)
	{
		____updater_7 = value;
		Il2CppCodeGenWriteBarrier((&____updater_7), value);
	}

	inline static int32_t get_offset_of__fixedUpdater_8() { return static_cast<int32_t>(offsetof(TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA, ____fixedUpdater_8)); }
	inline FixedTickablesTaskUpdater_t8455644BFB7A3B25FBA48F26B4A5253F8208CF20 * get__fixedUpdater_8() const { return ____fixedUpdater_8; }
	inline FixedTickablesTaskUpdater_t8455644BFB7A3B25FBA48F26B4A5253F8208CF20 ** get_address_of__fixedUpdater_8() { return &____fixedUpdater_8; }
	inline void set__fixedUpdater_8(FixedTickablesTaskUpdater_t8455644BFB7A3B25FBA48F26B4A5253F8208CF20 * value)
	{
		____fixedUpdater_8 = value;
		Il2CppCodeGenWriteBarrier((&____fixedUpdater_8), value);
	}

	inline static int32_t get_offset_of__lateUpdater_9() { return static_cast<int32_t>(offsetof(TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA, ____lateUpdater_9)); }
	inline LateTickablesTaskUpdater_t03C7EF16845586540065A3D08D17571840CD4F24 * get__lateUpdater_9() const { return ____lateUpdater_9; }
	inline LateTickablesTaskUpdater_t03C7EF16845586540065A3D08D17571840CD4F24 ** get_address_of__lateUpdater_9() { return &____lateUpdater_9; }
	inline void set__lateUpdater_9(LateTickablesTaskUpdater_t03C7EF16845586540065A3D08D17571840CD4F24 * value)
	{
		____lateUpdater_9 = value;
		Il2CppCodeGenWriteBarrier((&____lateUpdater_9), value);
	}

	inline static int32_t get_offset_of__isPaused_10() { return static_cast<int32_t>(offsetof(TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA, ____isPaused_10)); }
	inline bool get__isPaused_10() const { return ____isPaused_10; }
	inline bool* get_address_of__isPaused_10() { return &____isPaused_10; }
	inline void set__isPaused_10(bool value)
	{
		____isPaused_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TICKABLEMANAGER_TBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA_H
#ifndef U3CU3EC_T6C58537CAEA272CC8B31FFADADB8872A1C4909D9_H
#define U3CU3EC_T6C58537CAEA272CC8B31FFADADB8872A1C4909D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TickableManager/<>c
struct  U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields
{
public:
	// Zenject.TickableManager/<>c Zenject.TickableManager/<>c::<>9
	U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9 * ___U3CU3E9_0;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Type> Zenject.TickableManager/<>c::<>9__17_0
	Func_2_t82F87C5DB7A2AF00476C31B61CDE3B766ECF7EAC * ___U3CU3E9__17_0_1;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32> Zenject.TickableManager/<>c::<>9__17_2
	Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * ___U3CU3E9__17_2_2;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Type> Zenject.TickableManager/<>c::<>9__18_0
	Func_2_t82F87C5DB7A2AF00476C31B61CDE3B766ECF7EAC * ___U3CU3E9__18_0_3;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32> Zenject.TickableManager/<>c::<>9__18_2
	Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * ___U3CU3E9__18_2_4;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Type> Zenject.TickableManager/<>c::<>9__19_0
	Func_2_t82F87C5DB7A2AF00476C31B61CDE3B766ECF7EAC * ___U3CU3E9__19_0_5;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32> Zenject.TickableManager/<>c::<>9__19_2
	Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * ___U3CU3E9__19_2_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields, ___U3CU3E9__17_0_1)); }
	inline Func_2_t82F87C5DB7A2AF00476C31B61CDE3B766ECF7EAC * get_U3CU3E9__17_0_1() const { return ___U3CU3E9__17_0_1; }
	inline Func_2_t82F87C5DB7A2AF00476C31B61CDE3B766ECF7EAC ** get_address_of_U3CU3E9__17_0_1() { return &___U3CU3E9__17_0_1; }
	inline void set_U3CU3E9__17_0_1(Func_2_t82F87C5DB7A2AF00476C31B61CDE3B766ECF7EAC * value)
	{
		___U3CU3E9__17_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields, ___U3CU3E9__17_2_2)); }
	inline Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * get_U3CU3E9__17_2_2() const { return ___U3CU3E9__17_2_2; }
	inline Func_2_t9288B55256D27898799397C666D0F5F7C7368101 ** get_address_of_U3CU3E9__17_2_2() { return &___U3CU3E9__17_2_2; }
	inline void set_U3CU3E9__17_2_2(Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * value)
	{
		___U3CU3E9__17_2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields, ___U3CU3E9__18_0_3)); }
	inline Func_2_t82F87C5DB7A2AF00476C31B61CDE3B766ECF7EAC * get_U3CU3E9__18_0_3() const { return ___U3CU3E9__18_0_3; }
	inline Func_2_t82F87C5DB7A2AF00476C31B61CDE3B766ECF7EAC ** get_address_of_U3CU3E9__18_0_3() { return &___U3CU3E9__18_0_3; }
	inline void set_U3CU3E9__18_0_3(Func_2_t82F87C5DB7A2AF00476C31B61CDE3B766ECF7EAC * value)
	{
		___U3CU3E9__18_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_2_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields, ___U3CU3E9__18_2_4)); }
	inline Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * get_U3CU3E9__18_2_4() const { return ___U3CU3E9__18_2_4; }
	inline Func_2_t9288B55256D27898799397C666D0F5F7C7368101 ** get_address_of_U3CU3E9__18_2_4() { return &___U3CU3E9__18_2_4; }
	inline void set_U3CU3E9__18_2_4(Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * value)
	{
		___U3CU3E9__18_2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__19_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields, ___U3CU3E9__19_0_5)); }
	inline Func_2_t82F87C5DB7A2AF00476C31B61CDE3B766ECF7EAC * get_U3CU3E9__19_0_5() const { return ___U3CU3E9__19_0_5; }
	inline Func_2_t82F87C5DB7A2AF00476C31B61CDE3B766ECF7EAC ** get_address_of_U3CU3E9__19_0_5() { return &___U3CU3E9__19_0_5; }
	inline void set_U3CU3E9__19_0_5(Func_2_t82F87C5DB7A2AF00476C31B61CDE3B766ECF7EAC * value)
	{
		___U3CU3E9__19_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__19_0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__19_2_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields, ___U3CU3E9__19_2_6)); }
	inline Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * get_U3CU3E9__19_2_6() const { return ___U3CU3E9__19_2_6; }
	inline Func_2_t9288B55256D27898799397C666D0F5F7C7368101 ** get_address_of_U3CU3E9__19_2_6() { return &___U3CU3E9__19_2_6; }
	inline void set_U3CU3E9__19_2_6(Func_2_t9288B55256D27898799397C666D0F5F7C7368101 * value)
	{
		___U3CU3E9__19_2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__19_2_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T6C58537CAEA272CC8B31FFADADB8872A1C4909D9_H
#ifndef U3CU3EC__DISPLAYCLASS17_0_T4C5290347F94326AE9985ADD59362ED1E5F70B30_H
#define U3CU3EC__DISPLAYCLASS17_0_T4C5290347F94326AE9985ADD59362ED1E5F70B30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TickableManager/<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_t4C5290347F94326AE9985ADD59362ED1E5F70B30  : public RuntimeObject
{
public:
	// Zenject.IFixedTickable Zenject.TickableManager/<>c__DisplayClass17_0::tickable
	RuntimeObject* ___tickable_0;

public:
	inline static int32_t get_offset_of_tickable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t4C5290347F94326AE9985ADD59362ED1E5F70B30, ___tickable_0)); }
	inline RuntimeObject* get_tickable_0() const { return ___tickable_0; }
	inline RuntimeObject** get_address_of_tickable_0() { return &___tickable_0; }
	inline void set_tickable_0(RuntimeObject* value)
	{
		___tickable_0 = value;
		Il2CppCodeGenWriteBarrier((&___tickable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_0_T4C5290347F94326AE9985ADD59362ED1E5F70B30_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_TF28C23203FE97B70CA80F3B3C2FCD4682173CA80_H
#define U3CU3EC__DISPLAYCLASS18_0_TF28C23203FE97B70CA80F3B3C2FCD4682173CA80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TickableManager/<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_tF28C23203FE97B70CA80F3B3C2FCD4682173CA80  : public RuntimeObject
{
public:
	// Zenject.ITickable Zenject.TickableManager/<>c__DisplayClass18_0::tickable
	RuntimeObject* ___tickable_0;

public:
	inline static int32_t get_offset_of_tickable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_tF28C23203FE97B70CA80F3B3C2FCD4682173CA80, ___tickable_0)); }
	inline RuntimeObject* get_tickable_0() const { return ___tickable_0; }
	inline RuntimeObject** get_address_of_tickable_0() { return &___tickable_0; }
	inline void set_tickable_0(RuntimeObject* value)
	{
		___tickable_0 = value;
		Il2CppCodeGenWriteBarrier((&___tickable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_TF28C23203FE97B70CA80F3B3C2FCD4682173CA80_H
#ifndef U3CU3EC__DISPLAYCLASS19_0_TB536BFDC426C4CABF221A7686C8A44173DCC5633_H
#define U3CU3EC__DISPLAYCLASS19_0_TB536BFDC426C4CABF221A7686C8A44173DCC5633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TickableManager/<>c__DisplayClass19_0
struct  U3CU3Ec__DisplayClass19_0_tB536BFDC426C4CABF221A7686C8A44173DCC5633  : public RuntimeObject
{
public:
	// Zenject.ILateTickable Zenject.TickableManager/<>c__DisplayClass19_0::tickable
	RuntimeObject* ___tickable_0;

public:
	inline static int32_t get_offset_of_tickable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_tB536BFDC426C4CABF221A7686C8A44173DCC5633, ___tickable_0)); }
	inline RuntimeObject* get_tickable_0() const { return ___tickable_0; }
	inline RuntimeObject** get_address_of_tickable_0() { return &___tickable_0; }
	inline void set_tickable_0(RuntimeObject* value)
	{
		___tickable_0 = value;
		Il2CppCodeGenWriteBarrier((&___tickable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS19_0_TB536BFDC426C4CABF221A7686C8A44173DCC5633_H
#ifndef TRANSIENTPROVIDER_T4C4A97405255740DA79D817A70712C8F9F54A0BA_H
#define TRANSIENTPROVIDER_T4C4A97405255740DA79D817A70712C8F9F54A0BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TransientProvider
struct  TransientProvider_t4C4A97405255740DA79D817A70712C8F9F54A0BA  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.TransientProvider::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_0;
	// System.Type Zenject.TransientProvider::_concreteType
	Type_t * ____concreteType_1;
	// System.Object Zenject.TransientProvider::_concreteIdentifier
	RuntimeObject * ____concreteIdentifier_2;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.TransientProvider::_extraArguments
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ____extraArguments_3;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(TransientProvider_t4C4A97405255740DA79D817A70712C8F9F54A0BA, ____container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_0() const { return ____container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__concreteType_1() { return static_cast<int32_t>(offsetof(TransientProvider_t4C4A97405255740DA79D817A70712C8F9F54A0BA, ____concreteType_1)); }
	inline Type_t * get__concreteType_1() const { return ____concreteType_1; }
	inline Type_t ** get_address_of__concreteType_1() { return &____concreteType_1; }
	inline void set__concreteType_1(Type_t * value)
	{
		____concreteType_1 = value;
		Il2CppCodeGenWriteBarrier((&____concreteType_1), value);
	}

	inline static int32_t get_offset_of__concreteIdentifier_2() { return static_cast<int32_t>(offsetof(TransientProvider_t4C4A97405255740DA79D817A70712C8F9F54A0BA, ____concreteIdentifier_2)); }
	inline RuntimeObject * get__concreteIdentifier_2() const { return ____concreteIdentifier_2; }
	inline RuntimeObject ** get_address_of__concreteIdentifier_2() { return &____concreteIdentifier_2; }
	inline void set__concreteIdentifier_2(RuntimeObject * value)
	{
		____concreteIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&____concreteIdentifier_2), value);
	}

	inline static int32_t get_offset_of__extraArguments_3() { return static_cast<int32_t>(offsetof(TransientProvider_t4C4A97405255740DA79D817A70712C8F9F54A0BA, ____extraArguments_3)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get__extraArguments_3() const { return ____extraArguments_3; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of__extraArguments_3() { return &____extraArguments_3; }
	inline void set__extraArguments_3(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		____extraArguments_3 = value;
		Il2CppCodeGenWriteBarrier((&____extraArguments_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSIENTPROVIDER_T4C4A97405255740DA79D817A70712C8F9F54A0BA_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_T3550B9DB07C2703E5BF787543F992CE37A492B2E_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_T3550B9DB07C2703E5BF787543F992CE37A492B2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TransientProvider/<GetAllInstancesWithInjectSplit>d__8
struct  U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E  : public RuntimeObject
{
public:
	// System.Int32 Zenject.TransientProvider/<GetAllInstancesWithInjectSplit>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.TransientProvider/<GetAllInstancesWithInjectSplit>d__8::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.TransientProvider/<GetAllInstancesWithInjectSplit>d__8::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_2;
	// Zenject.TransientProvider Zenject.TransientProvider/<GetAllInstancesWithInjectSplit>d__8::<>4__this
	TransientProvider_t4C4A97405255740DA79D817A70712C8F9F54A0BA * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.TransientProvider/<GetAllInstancesWithInjectSplit>d__8::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_4;
	// System.Type Zenject.TransientProvider/<GetAllInstancesWithInjectSplit>d__8::<instanceType>5__2
	Type_t * ___U3CinstanceTypeU3E5__2_5;
	// Zenject.InjectArgs Zenject.TransientProvider/<GetAllInstancesWithInjectSplit>d__8::<injectArgs>5__3
	InjectArgs_t8A0EB8658CA9A35034FFFFA08D1DCB03051E521E * ___U3CinjectArgsU3E5__3_6;
	// System.Object Zenject.TransientProvider/<GetAllInstancesWithInjectSplit>d__8::<instance>5__4
	RuntimeObject * ___U3CinstanceU3E5__4_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E, ___context_2)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_2() const { return ___context_2; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E, ___U3CU3E4__this_3)); }
	inline TransientProvider_t4C4A97405255740DA79D817A70712C8F9F54A0BA * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline TransientProvider_t4C4A97405255740DA79D817A70712C8F9F54A0BA ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(TransientProvider_t4C4A97405255740DA79D817A70712C8F9F54A0BA * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E, ___args_4)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_4() const { return ___args_4; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}

	inline static int32_t get_offset_of_U3CinstanceTypeU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E, ___U3CinstanceTypeU3E5__2_5)); }
	inline Type_t * get_U3CinstanceTypeU3E5__2_5() const { return ___U3CinstanceTypeU3E5__2_5; }
	inline Type_t ** get_address_of_U3CinstanceTypeU3E5__2_5() { return &___U3CinstanceTypeU3E5__2_5; }
	inline void set_U3CinstanceTypeU3E5__2_5(Type_t * value)
	{
		___U3CinstanceTypeU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinstanceTypeU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3CinjectArgsU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E, ___U3CinjectArgsU3E5__3_6)); }
	inline InjectArgs_t8A0EB8658CA9A35034FFFFA08D1DCB03051E521E * get_U3CinjectArgsU3E5__3_6() const { return ___U3CinjectArgsU3E5__3_6; }
	inline InjectArgs_t8A0EB8658CA9A35034FFFFA08D1DCB03051E521E ** get_address_of_U3CinjectArgsU3E5__3_6() { return &___U3CinjectArgsU3E5__3_6; }
	inline void set_U3CinjectArgsU3E5__3_6(InjectArgs_t8A0EB8658CA9A35034FFFFA08D1DCB03051E521E * value)
	{
		___U3CinjectArgsU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinjectArgsU3E5__3_6), value);
	}

	inline static int32_t get_offset_of_U3CinstanceU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E, ___U3CinstanceU3E5__4_7)); }
	inline RuntimeObject * get_U3CinstanceU3E5__4_7() const { return ___U3CinstanceU3E5__4_7; }
	inline RuntimeObject ** get_address_of_U3CinstanceU3E5__4_7() { return &___U3CinstanceU3E5__4_7; }
	inline void set_U3CinstanceU3E5__4_7(RuntimeObject * value)
	{
		___U3CinstanceU3E5__4_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinstanceU3E5__4_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_T3550B9DB07C2703E5BF787543F992CE37A492B2E_H
#ifndef UNTYPEDFACTORYPROVIDER_TE3B354C34152789A038D90393A5FA6F8BA78377C_H
#define UNTYPEDFACTORYPROVIDER_TE3B354C34152789A038D90393A5FA6F8BA78377C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.UntypedFactoryProvider
struct  UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.UntypedFactoryProvider::_factoryArgs
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ____factoryArgs_0;
	// Zenject.DiContainer Zenject.UntypedFactoryProvider::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_1;
	// System.Type Zenject.UntypedFactoryProvider::_factoryType
	Type_t * ____factoryType_2;
	// System.Type Zenject.UntypedFactoryProvider::_concreteType
	Type_t * ____concreteType_3;
	// System.Reflection.MethodInfo Zenject.UntypedFactoryProvider::_createMethod
	MethodInfo_t * ____createMethod_4;

public:
	inline static int32_t get_offset_of__factoryArgs_0() { return static_cast<int32_t>(offsetof(UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C, ____factoryArgs_0)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get__factoryArgs_0() const { return ____factoryArgs_0; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of__factoryArgs_0() { return &____factoryArgs_0; }
	inline void set__factoryArgs_0(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		____factoryArgs_0 = value;
		Il2CppCodeGenWriteBarrier((&____factoryArgs_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C, ____container_1)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_1() const { return ____container_1; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__factoryType_2() { return static_cast<int32_t>(offsetof(UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C, ____factoryType_2)); }
	inline Type_t * get__factoryType_2() const { return ____factoryType_2; }
	inline Type_t ** get_address_of__factoryType_2() { return &____factoryType_2; }
	inline void set__factoryType_2(Type_t * value)
	{
		____factoryType_2 = value;
		Il2CppCodeGenWriteBarrier((&____factoryType_2), value);
	}

	inline static int32_t get_offset_of__concreteType_3() { return static_cast<int32_t>(offsetof(UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C, ____concreteType_3)); }
	inline Type_t * get__concreteType_3() const { return ____concreteType_3; }
	inline Type_t ** get_address_of__concreteType_3() { return &____concreteType_3; }
	inline void set__concreteType_3(Type_t * value)
	{
		____concreteType_3 = value;
		Il2CppCodeGenWriteBarrier((&____concreteType_3), value);
	}

	inline static int32_t get_offset_of__createMethod_4() { return static_cast<int32_t>(offsetof(UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C, ____createMethod_4)); }
	inline MethodInfo_t * get__createMethod_4() const { return ____createMethod_4; }
	inline MethodInfo_t ** get_address_of__createMethod_4() { return &____createMethod_4; }
	inline void set__createMethod_4(MethodInfo_t * value)
	{
		____createMethod_4 = value;
		Il2CppCodeGenWriteBarrier((&____createMethod_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNTYPEDFACTORYPROVIDER_TE3B354C34152789A038D90393A5FA6F8BA78377C_H
#ifndef U3CU3EC_T77932B38149E4821035D5406B2767C4FD9097388_H
#define U3CU3EC_T77932B38149E4821035D5406B2767C4FD9097388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.UntypedFactoryProvider/<>c
struct  U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388_StaticFields
{
public:
	// Zenject.UntypedFactoryProvider/<>c Zenject.UntypedFactoryProvider/<>c::<>9
	U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Zenject.UntypedFactoryProvider/<>c::<>9__5_0
	Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * ___U3CU3E9__5_0_1;
	// System.Func`2<System.Type,System.Boolean> Zenject.UntypedFactoryProvider/<>c::<>9__6_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__6_0_2;
	// System.Func`2<Zenject.TypeValuePair,System.Object> Zenject.UntypedFactoryProvider/<>c::<>9__8_0
	Func_2_tE0EA28C07F376CB2C0E3DBC985C2651B6C247623 * ___U3CU3E9__8_0_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388_StaticFields, ___U3CU3E9__5_0_1)); }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * get_U3CU3E9__5_0_1() const { return ___U3CU3E9__5_0_1; }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 ** get_address_of_U3CU3E9__5_0_1() { return &___U3CU3E9__5_0_1; }
	inline void set_U3CU3E9__5_0_1(Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * value)
	{
		___U3CU3E9__5_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388_StaticFields, ___U3CU3E9__6_0_2)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__6_0_2() const { return ___U3CU3E9__6_0_2; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__6_0_2() { return &___U3CU3E9__6_0_2; }
	inline void set_U3CU3E9__6_0_2(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__6_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388_StaticFields, ___U3CU3E9__8_0_3)); }
	inline Func_2_tE0EA28C07F376CB2C0E3DBC985C2651B6C247623 * get_U3CU3E9__8_0_3() const { return ___U3CU3E9__8_0_3; }
	inline Func_2_tE0EA28C07F376CB2C0E3DBC985C2651B6C247623 ** get_address_of_U3CU3E9__8_0_3() { return &___U3CU3E9__8_0_3; }
	inline void set_U3CU3E9__8_0_3(Func_2_tE0EA28C07F376CB2C0E3DBC985C2651B6C247623 * value)
	{
		___U3CU3E9__8_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T77932B38149E4821035D5406B2767C4FD9097388_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_T3A26A58CB9541FB8DAC0331BEED3C0686C4A93BA_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_T3A26A58CB9541FB8DAC0331BEED3C0686C4A93BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.UntypedFactoryProvider/<GetAllInstancesWithInjectSplit>d__8
struct  U3CGetAllInstancesWithInjectSplitU3Ed__8_t3A26A58CB9541FB8DAC0331BEED3C0686C4A93BA  : public RuntimeObject
{
public:
	// System.Int32 Zenject.UntypedFactoryProvider/<GetAllInstancesWithInjectSplit>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.UntypedFactoryProvider/<GetAllInstancesWithInjectSplit>d__8::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.UntypedFactoryProvider Zenject.UntypedFactoryProvider/<GetAllInstancesWithInjectSplit>d__8::<>4__this
	UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C * ___U3CU3E4__this_2;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.UntypedFactoryProvider/<GetAllInstancesWithInjectSplit>d__8::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t3A26A58CB9541FB8DAC0331BEED3C0686C4A93BA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t3A26A58CB9541FB8DAC0331BEED3C0686C4A93BA, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t3A26A58CB9541FB8DAC0331BEED3C0686C4A93BA, ___U3CU3E4__this_2)); }
	inline UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_args_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t3A26A58CB9541FB8DAC0331BEED3C0686C4A93BA, ___args_3)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_3() const { return ___args_3; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_3() { return &___args_3; }
	inline void set_args_3(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_3 = value;
		Il2CppCodeGenWriteBarrier((&___args_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_T3A26A58CB9541FB8DAC0331BEED3C0686C4A93BA_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef FIXEDTICKABLESTASKUPDATER_T8455644BFB7A3B25FBA48F26B4A5253F8208CF20_H
#define FIXEDTICKABLESTASKUPDATER_T8455644BFB7A3B25FBA48F26B4A5253F8208CF20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FixedTickablesTaskUpdater
struct  FixedTickablesTaskUpdater_t8455644BFB7A3B25FBA48F26B4A5253F8208CF20  : public TaskUpdater_1_tAAE053B2BFF1746D9F4F6DA69CD81358F82F5278
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDTICKABLESTASKUPDATER_T8455644BFB7A3B25FBA48F26B4A5253F8208CF20_H
#ifndef INSTALLER_2_T32C08633D566B5D2B03A57AE72FD4C93B23E6405_H
#define INSTALLER_2_T32C08633D566B5D2B03A57AE72FD4C93B23E6405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Installer`2<UnityEngine.Animator,Zenject.AnimatorInstaller>
struct  Installer_2_t32C08633D566B5D2B03A57AE72FD4C93B23E6405  : public InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLER_2_T32C08633D566B5D2B03A57AE72FD4C93B23E6405_H
#ifndef LATETICKABLESTASKUPDATER_T03C7EF16845586540065A3D08D17571840CD4F24_H
#define LATETICKABLESTASKUPDATER_T03C7EF16845586540065A3D08D17571840CD4F24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.LateTickablesTaskUpdater
struct  LateTickablesTaskUpdater_t03C7EF16845586540065A3D08D17571840CD4F24  : public TaskUpdater_1_t53BC21CD324599BB87353961590F4AF920E9CA3E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATETICKABLESTASKUPDATER_T03C7EF16845586540065A3D08D17571840CD4F24_H
#ifndef SUBCONTAINERCREATORBYNEWPREFABINSTALLER_T892C2B19E8FE3B7CB1963B14CBBD29F58041F118_H
#define SUBCONTAINERCREATORBYNEWPREFABINSTALLER_T892C2B19E8FE3B7CB1963B14CBBD29F58041F118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabInstaller
struct  SubContainerCreatorByNewPrefabInstaller_t892C2B19E8FE3B7CB1963B14CBBD29F58041F118  : public SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167
{
public:
	// System.Type Zenject.SubContainerCreatorByNewPrefabInstaller::_installerType
	Type_t * ____installerType_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewPrefabInstaller::_extraArgs
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ____extraArgs_4;

public:
	inline static int32_t get_offset_of__installerType_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabInstaller_t892C2B19E8FE3B7CB1963B14CBBD29F58041F118, ____installerType_3)); }
	inline Type_t * get__installerType_3() const { return ____installerType_3; }
	inline Type_t ** get_address_of__installerType_3() { return &____installerType_3; }
	inline void set__installerType_3(Type_t * value)
	{
		____installerType_3 = value;
		Il2CppCodeGenWriteBarrier((&____installerType_3), value);
	}

	inline static int32_t get_offset_of__extraArgs_4() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabInstaller_t892C2B19E8FE3B7CB1963B14CBBD29F58041F118, ____extraArgs_4)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get__extraArgs_4() const { return ____extraArgs_4; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of__extraArgs_4() { return &____extraArgs_4; }
	inline void set__extraArgs_4(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		____extraArgs_4 = value;
		Il2CppCodeGenWriteBarrier((&____extraArgs_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYNEWPREFABINSTALLER_T892C2B19E8FE3B7CB1963B14CBBD29F58041F118_H
#ifndef SUBCONTAINERCREATORBYNEWPREFABMETHOD_T7476250ADAC1D7D6DF1A41D23FF9FCFFAE891E5E_H
#define SUBCONTAINERCREATORBYNEWPREFABMETHOD_T7476250ADAC1D7D6DF1A41D23FF9FCFFAE891E5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabMethod
struct  SubContainerCreatorByNewPrefabMethod_t7476250ADAC1D7D6DF1A41D23FF9FCFFAE891E5E  : public SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167
{
public:
	// System.Action`1<Zenject.DiContainer> Zenject.SubContainerCreatorByNewPrefabMethod::_installerMethod
	Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * ____installerMethod_3;

public:
	inline static int32_t get_offset_of__installerMethod_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabMethod_t7476250ADAC1D7D6DF1A41D23FF9FCFFAE891E5E, ____installerMethod_3)); }
	inline Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * get__installerMethod_3() const { return ____installerMethod_3; }
	inline Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A ** get_address_of__installerMethod_3() { return &____installerMethod_3; }
	inline void set__installerMethod_3(Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * value)
	{
		____installerMethod_3 = value;
		Il2CppCodeGenWriteBarrier((&____installerMethod_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYNEWPREFABMETHOD_T7476250ADAC1D7D6DF1A41D23FF9FCFFAE891E5E_H
#ifndef TICKABLESTASKUPDATER_TCE7741F2A32B9750025C1F357267144B1F62FBE0_H
#define TICKABLESTASKUPDATER_TCE7741F2A32B9750025C1F357267144B1F62FBE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TickablesTaskUpdater
struct  TickablesTaskUpdater_tCE7741F2A32B9750025C1F357267144B1F62FBE0  : public TaskUpdater_1_t1970C92ED887D656C35B2A95ED880A1B63793D5F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TICKABLESTASKUPDATER_TCE7741F2A32B9750025C1F357267144B1F62FBE0_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef ANIMATORINSTALLER_TA9DFC3F644F0EC68E3557ED92FA679C1D608650C_H
#define ANIMATORINSTALLER_TA9DFC3F644F0EC68E3557ED92FA679C1D608650C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AnimatorInstaller
struct  AnimatorInstaller_tA9DFC3F644F0EC68E3557ED92FA679C1D608650C  : public Installer_2_t32C08633D566B5D2B03A57AE72FD4C93B23E6405
{
public:
	// UnityEngine.Animator Zenject.AnimatorInstaller::_animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ____animator_1;

public:
	inline static int32_t get_offset_of__animator_1() { return static_cast<int32_t>(offsetof(AnimatorInstaller_tA9DFC3F644F0EC68E3557ED92FA679C1D608650C, ____animator_1)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get__animator_1() const { return ____animator_1; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of__animator_1() { return &____animator_1; }
	inline void set__animator_1(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		____animator_1 = value;
		Il2CppCodeGenWriteBarrier((&____animator_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORINSTALLER_TA9DFC3F644F0EC68E3557ED92FA679C1D608650C_H
#ifndef SINGLETONTYPES_T98C89A97CD1D325BCFF10A9F7EAE7F21C16D811E_H
#define SINGLETONTYPES_T98C89A97CD1D325BCFF10A9F7EAE7F21C16D811E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonTypes
struct  SingletonTypes_t98C89A97CD1D325BCFF10A9F7EAE7F21C16D811E 
{
public:
	// System.Int32 Zenject.SingletonTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SingletonTypes_t98C89A97CD1D325BCFF10A9F7EAE7F21C16D811E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONTYPES_T98C89A97CD1D325BCFF10A9F7EAE7F21C16D811E_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef STANDARDSINGLETONDECLARATION_T3B2071BCA7300F63743B0079D4703E141AFCFADD_H
#define STANDARDSINGLETONDECLARATION_T3B2071BCA7300F63743B0079D4703E141AFCFADD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.StandardSingletonDeclaration
struct  StandardSingletonDeclaration_t3B2071BCA7300F63743B0079D4703E141AFCFADD  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.StandardSingletonDeclaration::<Arguments>k__BackingField
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___U3CArgumentsU3Ek__BackingField_0;
	// Zenject.SingletonId Zenject.StandardSingletonDeclaration::<Id>k__BackingField
	SingletonId_t688C4C4A8CDD9C7CA54DBA5F829AE333F8947DF3 * ___U3CIdU3Ek__BackingField_1;
	// Zenject.SingletonTypes Zenject.StandardSingletonDeclaration::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_2;
	// System.Object Zenject.StandardSingletonDeclaration::<SpecificId>k__BackingField
	RuntimeObject * ___U3CSpecificIdU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CArgumentsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StandardSingletonDeclaration_t3B2071BCA7300F63743B0079D4703E141AFCFADD, ___U3CArgumentsU3Ek__BackingField_0)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_U3CArgumentsU3Ek__BackingField_0() const { return ___U3CArgumentsU3Ek__BackingField_0; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_U3CArgumentsU3Ek__BackingField_0() { return &___U3CArgumentsU3Ek__BackingField_0; }
	inline void set_U3CArgumentsU3Ek__BackingField_0(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___U3CArgumentsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgumentsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StandardSingletonDeclaration_t3B2071BCA7300F63743B0079D4703E141AFCFADD, ___U3CIdU3Ek__BackingField_1)); }
	inline SingletonId_t688C4C4A8CDD9C7CA54DBA5F829AE333F8947DF3 * get_U3CIdU3Ek__BackingField_1() const { return ___U3CIdU3Ek__BackingField_1; }
	inline SingletonId_t688C4C4A8CDD9C7CA54DBA5F829AE333F8947DF3 ** get_address_of_U3CIdU3Ek__BackingField_1() { return &___U3CIdU3Ek__BackingField_1; }
	inline void set_U3CIdU3Ek__BackingField_1(SingletonId_t688C4C4A8CDD9C7CA54DBA5F829AE333F8947DF3 * value)
	{
		___U3CIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(StandardSingletonDeclaration_t3B2071BCA7300F63743B0079D4703E141AFCFADD, ___U3CTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CSpecificIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(StandardSingletonDeclaration_t3B2071BCA7300F63743B0079D4703E141AFCFADD, ___U3CSpecificIdU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CSpecificIdU3Ek__BackingField_3() const { return ___U3CSpecificIdU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CSpecificIdU3Ek__BackingField_3() { return &___U3CSpecificIdU3Ek__BackingField_3; }
	inline void set_U3CSpecificIdU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CSpecificIdU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSpecificIdU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDSINGLETONDECLARATION_T3B2071BCA7300F63743B0079D4703E141AFCFADD_H
#ifndef PROVIDERINFO_TB4FC87C92AFF28793C7FF53C670E7A761A539C3E_H
#define PROVIDERINFO_TB4FC87C92AFF28793C7FF53C670E7A761A539C3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.StandardSingletonProviderCreator/ProviderInfo
struct  ProviderInfo_tB4FC87C92AFF28793C7FF53C670E7A761A539C3E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.StandardSingletonProviderCreator/ProviderInfo::<Arguments>k__BackingField
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___U3CArgumentsU3Ek__BackingField_0;
	// System.Object Zenject.StandardSingletonProviderCreator/ProviderInfo::<SingletonSpecificId>k__BackingField
	RuntimeObject * ___U3CSingletonSpecificIdU3Ek__BackingField_1;
	// Zenject.SingletonTypes Zenject.StandardSingletonProviderCreator/ProviderInfo::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_2;
	// Zenject.CachedProvider Zenject.StandardSingletonProviderCreator/ProviderInfo::<Provider>k__BackingField
	CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8 * ___U3CProviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CArgumentsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProviderInfo_tB4FC87C92AFF28793C7FF53C670E7A761A539C3E, ___U3CArgumentsU3Ek__BackingField_0)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_U3CArgumentsU3Ek__BackingField_0() const { return ___U3CArgumentsU3Ek__BackingField_0; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_U3CArgumentsU3Ek__BackingField_0() { return &___U3CArgumentsU3Ek__BackingField_0; }
	inline void set_U3CArgumentsU3Ek__BackingField_0(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___U3CArgumentsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgumentsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CSingletonSpecificIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProviderInfo_tB4FC87C92AFF28793C7FF53C670E7A761A539C3E, ___U3CSingletonSpecificIdU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CSingletonSpecificIdU3Ek__BackingField_1() const { return ___U3CSingletonSpecificIdU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CSingletonSpecificIdU3Ek__BackingField_1() { return &___U3CSingletonSpecificIdU3Ek__BackingField_1; }
	inline void set_U3CSingletonSpecificIdU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CSingletonSpecificIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSingletonSpecificIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProviderInfo_tB4FC87C92AFF28793C7FF53C670E7A761A539C3E, ___U3CTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CProviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ProviderInfo_tB4FC87C92AFF28793C7FF53C670E7A761A539C3E, ___U3CProviderU3Ek__BackingField_3)); }
	inline CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8 * get_U3CProviderU3Ek__BackingField_3() const { return ___U3CProviderU3Ek__BackingField_3; }
	inline CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8 ** get_address_of_U3CProviderU3Ek__BackingField_3() { return &___U3CProviderU3Ek__BackingField_3; }
	inline void set_U3CProviderU3Ek__BackingField_3(CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8 * value)
	{
		___U3CProviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProviderU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERINFO_TB4FC87C92AFF28793C7FF53C670E7A761A539C3E_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ANIMATORIKHANDLERMANAGER_T98049FECCF8DE622675A291C459428A6E441E197_H
#define ANIMATORIKHANDLERMANAGER_T98049FECCF8DE622675A291C459428A6E441E197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AnimatorIkHandlerManager
struct  AnimatorIkHandlerManager_t98049FECCF8DE622675A291C459428A6E441E197  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<Zenject.IAnimatorIkHandler> Zenject.AnimatorIkHandlerManager::_handlers
	List_1_tA80F85D05C3A7FAA27FB0C82AAD84F9057E80A77 * ____handlers_4;

public:
	inline static int32_t get_offset_of__handlers_4() { return static_cast<int32_t>(offsetof(AnimatorIkHandlerManager_t98049FECCF8DE622675A291C459428A6E441E197, ____handlers_4)); }
	inline List_1_tA80F85D05C3A7FAA27FB0C82AAD84F9057E80A77 * get__handlers_4() const { return ____handlers_4; }
	inline List_1_tA80F85D05C3A7FAA27FB0C82AAD84F9057E80A77 ** get_address_of__handlers_4() { return &____handlers_4; }
	inline void set__handlers_4(List_1_tA80F85D05C3A7FAA27FB0C82AAD84F9057E80A77 * value)
	{
		____handlers_4 = value;
		Il2CppCodeGenWriteBarrier((&____handlers_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORIKHANDLERMANAGER_T98049FECCF8DE622675A291C459428A6E441E197_H
#ifndef ANIMATORMOVEHANDLERMANAGER_TD3BD70043A984B9F8683F5D4A2798461623F1E8D_H
#define ANIMATORMOVEHANDLERMANAGER_TD3BD70043A984B9F8683F5D4A2798461623F1E8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AnimatorMoveHandlerManager
struct  AnimatorMoveHandlerManager_tD3BD70043A984B9F8683F5D4A2798461623F1E8D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<Zenject.IAnimatorMoveHandler> Zenject.AnimatorMoveHandlerManager::_handlers
	List_1_tB50850FDAC454C489717C889D75F7DF8C545254E * ____handlers_4;

public:
	inline static int32_t get_offset_of__handlers_4() { return static_cast<int32_t>(offsetof(AnimatorMoveHandlerManager_tD3BD70043A984B9F8683F5D4A2798461623F1E8D, ____handlers_4)); }
	inline List_1_tB50850FDAC454C489717C889D75F7DF8C545254E * get__handlers_4() const { return ____handlers_4; }
	inline List_1_tB50850FDAC454C489717C889D75F7DF8C545254E ** get_address_of__handlers_4() { return &____handlers_4; }
	inline void set__handlers_4(List_1_tB50850FDAC454C489717C889D75F7DF8C545254E * value)
	{
		____handlers_4 = value;
		Il2CppCodeGenWriteBarrier((&____handlers_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORMOVEHANDLERMANAGER_TD3BD70043A984B9F8683F5D4A2798461623F1E8D_H
#ifndef GUIRENDERER_T8385161CCA236AB0344F4B71A09E62D21078BC3E_H
#define GUIRENDERER_T8385161CCA236AB0344F4B71A09E62D21078BC3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GuiRenderer
struct  GuiRenderer_t8385161CCA236AB0344F4B71A09E62D21078BC3E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Zenject.GuiRenderableManager Zenject.GuiRenderer::_renderableManager
	GuiRenderableManager_tAF974427AFC375E673F5B208DFF3F18D65278519 * ____renderableManager_4;

public:
	inline static int32_t get_offset_of__renderableManager_4() { return static_cast<int32_t>(offsetof(GuiRenderer_t8385161CCA236AB0344F4B71A09E62D21078BC3E, ____renderableManager_4)); }
	inline GuiRenderableManager_tAF974427AFC375E673F5B208DFF3F18D65278519 * get__renderableManager_4() const { return ____renderableManager_4; }
	inline GuiRenderableManager_tAF974427AFC375E673F5B208DFF3F18D65278519 ** get_address_of__renderableManager_4() { return &____renderableManager_4; }
	inline void set__renderableManager_4(GuiRenderableManager_tAF974427AFC375E673F5B208DFF3F18D65278519 * value)
	{
		____renderableManager_4 = value;
		Il2CppCodeGenWriteBarrier((&____renderableManager_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIRENDERER_T8385161CCA236AB0344F4B71A09E62D21078BC3E_H
#ifndef MONOKERNEL_T3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF_H
#define MONOKERNEL_T3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoKernel
struct  MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Zenject.TickableManager Zenject.MonoKernel::_tickableManager
	TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA * ____tickableManager_4;
	// Zenject.InitializableManager Zenject.MonoKernel::_initializableManager
	InitializableManager_tBAC718A10ABAC14E0DBD742F921323DBCD34B9F8 * ____initializableManager_5;
	// Zenject.DisposableManager Zenject.MonoKernel::_disposablesManager
	DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3 * ____disposablesManager_6;
	// System.Boolean Zenject.MonoKernel::_hasInitialized
	bool ____hasInitialized_7;
	// System.Boolean Zenject.MonoKernel::_isDestroyed
	bool ____isDestroyed_8;

public:
	inline static int32_t get_offset_of__tickableManager_4() { return static_cast<int32_t>(offsetof(MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF, ____tickableManager_4)); }
	inline TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA * get__tickableManager_4() const { return ____tickableManager_4; }
	inline TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA ** get_address_of__tickableManager_4() { return &____tickableManager_4; }
	inline void set__tickableManager_4(TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA * value)
	{
		____tickableManager_4 = value;
		Il2CppCodeGenWriteBarrier((&____tickableManager_4), value);
	}

	inline static int32_t get_offset_of__initializableManager_5() { return static_cast<int32_t>(offsetof(MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF, ____initializableManager_5)); }
	inline InitializableManager_tBAC718A10ABAC14E0DBD742F921323DBCD34B9F8 * get__initializableManager_5() const { return ____initializableManager_5; }
	inline InitializableManager_tBAC718A10ABAC14E0DBD742F921323DBCD34B9F8 ** get_address_of__initializableManager_5() { return &____initializableManager_5; }
	inline void set__initializableManager_5(InitializableManager_tBAC718A10ABAC14E0DBD742F921323DBCD34B9F8 * value)
	{
		____initializableManager_5 = value;
		Il2CppCodeGenWriteBarrier((&____initializableManager_5), value);
	}

	inline static int32_t get_offset_of__disposablesManager_6() { return static_cast<int32_t>(offsetof(MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF, ____disposablesManager_6)); }
	inline DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3 * get__disposablesManager_6() const { return ____disposablesManager_6; }
	inline DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3 ** get_address_of__disposablesManager_6() { return &____disposablesManager_6; }
	inline void set__disposablesManager_6(DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3 * value)
	{
		____disposablesManager_6 = value;
		Il2CppCodeGenWriteBarrier((&____disposablesManager_6), value);
	}

	inline static int32_t get_offset_of__hasInitialized_7() { return static_cast<int32_t>(offsetof(MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF, ____hasInitialized_7)); }
	inline bool get__hasInitialized_7() const { return ____hasInitialized_7; }
	inline bool* get_address_of__hasInitialized_7() { return &____hasInitialized_7; }
	inline void set__hasInitialized_7(bool value)
	{
		____hasInitialized_7 = value;
	}

	inline static int32_t get_offset_of__isDestroyed_8() { return static_cast<int32_t>(offsetof(MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF, ____isDestroyed_8)); }
	inline bool get__isDestroyed_8() const { return ____isDestroyed_8; }
	inline bool* get_address_of__isDestroyed_8() { return &____isDestroyed_8; }
	inline void set__isDestroyed_8(bool value)
	{
		____isDestroyed_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOKERNEL_T3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF_H
#ifndef DEFAULTGAMEOBJECTKERNEL_TCE1A977E39CC0EF490528CF40EA6B2868BAC1382_H
#define DEFAULTGAMEOBJECTKERNEL_TCE1A977E39CC0EF490528CF40EA6B2868BAC1382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DefaultGameObjectKernel
struct  DefaultGameObjectKernel_tCE1A977E39CC0EF490528CF40EA6B2868BAC1382  : public MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTGAMEOBJECTKERNEL_TCE1A977E39CC0EF490528CF40EA6B2868BAC1382_H
#ifndef PROJECTKERNEL_T2E658E71A90EDA4D5E816495D605B0D0BB3E80AC_H
#define PROJECTKERNEL_T2E658E71A90EDA4D5E816495D605B0D0BB3E80AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProjectKernel
struct  ProjectKernel_t2E658E71A90EDA4D5E816495D605B0D0BB3E80AC  : public MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTKERNEL_T2E658E71A90EDA4D5E816495D605B0D0BB3E80AC_H
#ifndef SCENEKERNEL_T9BDB8CC78676A752727833F6396B0BFC500DE9C9_H
#define SCENEKERNEL_T9BDB8CC78676A752727833F6396B0BFC500DE9C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneKernel
struct  SceneKernel_t9BDB8CC78676A752727833F6396B0BFC500DE9C9  : public MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEKERNEL_T9BDB8CC78676A752727833F6396B0BFC500DE9C9_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7100 = { sizeof (SingletonImplIds_t4E3E5E5CAD910340FBD8815F6F3ABEAC951BA612), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7101 = { sizeof (ToMethod_t70AFA34E176CF0E2CF352F11754ADAF1DA951C51), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7101[1] = 
{
	ToMethod_t70AFA34E176CF0E2CF352F11754ADAF1DA951C51::get_offset_of__method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7102 = { sizeof (ToGetter_t33C5B61AE94C79EEE199F4F9F1B351AB029277D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7102[2] = 
{
	ToGetter_t33C5B61AE94C79EEE199F4F9F1B351AB029277D3::get_offset_of__method_0(),
	ToGetter_t33C5B61AE94C79EEE199F4F9F1B351AB029277D3::get_offset_of__identifier_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7103 = { sizeof (SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7103[1] = 
{
	SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751::get_offset_of__singletonTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7104 = { sizeof (SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7104[7] = 
{
	SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D::get_offset_of__standardProviderCreator_0(),
	SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D::get_offset_of__subContainerMethodProviderCreator_1(),
	SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D::get_offset_of__subContainerInstallerProviderCreator_2(),
	SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D::get_offset_of__subContainerPrefabProviderCreator_3(),
	SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D::get_offset_of__subContainerPrefabResourceProviderCreator_4(),
	SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D::get_offset_of__prefabProviderCreator_5(),
	SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D::get_offset_of__prefabResourceProviderCreator_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7105 = { sizeof (SingletonTypes_t98C89A97CD1D325BCFF10A9F7EAE7F21C16D811E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7105[19] = 
{
	SingletonTypes_t98C89A97CD1D325BCFF10A9F7EAE7F21C16D811E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7106 = { sizeof (StandardSingletonDeclaration_t3B2071BCA7300F63743B0079D4703E141AFCFADD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7106[4] = 
{
	StandardSingletonDeclaration_t3B2071BCA7300F63743B0079D4703E141AFCFADD::get_offset_of_U3CArgumentsU3Ek__BackingField_0(),
	StandardSingletonDeclaration_t3B2071BCA7300F63743B0079D4703E141AFCFADD::get_offset_of_U3CIdU3Ek__BackingField_1(),
	StandardSingletonDeclaration_t3B2071BCA7300F63743B0079D4703E141AFCFADD::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	StandardSingletonDeclaration_t3B2071BCA7300F63743B0079D4703E141AFCFADD::get_offset_of_U3CSpecificIdU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7107 = { sizeof (StandardSingletonProviderCreator_t4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7107[3] = 
{
	StandardSingletonProviderCreator_t4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4::get_offset_of__markRegistry_0(),
	StandardSingletonProviderCreator_t4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4::get_offset_of__providerMap_1(),
	StandardSingletonProviderCreator_t4B14D77521FFFB2CD2DBA69ACF1A2E22E09E08D4::get_offset_of__container_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7108 = { sizeof (ProviderInfo_tB4FC87C92AFF28793C7FF53C670E7A761A539C3E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7108[4] = 
{
	ProviderInfo_tB4FC87C92AFF28793C7FF53C670E7A761A539C3E::get_offset_of_U3CArgumentsU3Ek__BackingField_0(),
	ProviderInfo_tB4FC87C92AFF28793C7FF53C670E7A761A539C3E::get_offset_of_U3CSingletonSpecificIdU3Ek__BackingField_1(),
	ProviderInfo_tB4FC87C92AFF28793C7FF53C670E7A761A539C3E::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	ProviderInfo_tB4FC87C92AFF28793C7FF53C670E7A761A539C3E::get_offset_of_U3CProviderU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7109 = { sizeof (SubContainerSingletonProviderCreatorByInstaller_t90FB582E91D07E51B7C4584C4E3E071397AE2B09), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7109[3] = 
{
	SubContainerSingletonProviderCreatorByInstaller_t90FB582E91D07E51B7C4584C4E3E071397AE2B09::get_offset_of__markRegistry_0(),
	SubContainerSingletonProviderCreatorByInstaller_t90FB582E91D07E51B7C4584C4E3E071397AE2B09::get_offset_of__container_1(),
	SubContainerSingletonProviderCreatorByInstaller_t90FB582E91D07E51B7C4584C4E3E071397AE2B09::get_offset_of__subContainerCreators_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7110 = { sizeof (InstallerSingletonId_t6A3E9FF03A9FA3DE51F56900C7FE68014D5D7544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7110[2] = 
{
	InstallerSingletonId_t6A3E9FF03A9FA3DE51F56900C7FE68014D5D7544::get_offset_of_ConcreteIdentifier_0(),
	InstallerSingletonId_t6A3E9FF03A9FA3DE51F56900C7FE68014D5D7544::get_offset_of_InstallerType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7111 = { sizeof (SubContainerSingletonProviderCreatorByMethod_t5BFA8689BE01E6E287C528411DC83D46F08BA07F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7111[3] = 
{
	SubContainerSingletonProviderCreatorByMethod_t5BFA8689BE01E6E287C528411DC83D46F08BA07F::get_offset_of__markRegistry_0(),
	SubContainerSingletonProviderCreatorByMethod_t5BFA8689BE01E6E287C528411DC83D46F08BA07F::get_offset_of__container_1(),
	SubContainerSingletonProviderCreatorByMethod_t5BFA8689BE01E6E287C528411DC83D46F08BA07F::get_offset_of__subContainerCreators_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7112 = { sizeof (MethodSingletonId_tBC5ADCBA6BDCF4A9353CF364E0A903CCDFA2C651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7112[2] = 
{
	MethodSingletonId_tBC5ADCBA6BDCF4A9353CF364E0A903CCDFA2C651::get_offset_of_ConcreteIdentifier_0(),
	MethodSingletonId_tBC5ADCBA6BDCF4A9353CF364E0A903CCDFA2C651::get_offset_of_InstallerDelegate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7113 = { sizeof (SubContainerSingletonProviderCreatorByNewPrefab_t0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7113[3] = 
{
	SubContainerSingletonProviderCreatorByNewPrefab_t0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8::get_offset_of__markRegistry_0(),
	SubContainerSingletonProviderCreatorByNewPrefab_t0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8::get_offset_of__container_1(),
	SubContainerSingletonProviderCreatorByNewPrefab_t0DF0A5AB0E78C7D487324ACE4BFA592CD861A3C8::get_offset_of__subContainerCreators_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7114 = { sizeof (CustomSingletonId_tC9251DAA97612544E328905AD12613D087E8EAE5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7114[2] = 
{
	CustomSingletonId_tC9251DAA97612544E328905AD12613D087E8EAE5::get_offset_of_ConcreteIdentifier_0(),
	CustomSingletonId_tC9251DAA97612544E328905AD12613D087E8EAE5::get_offset_of_Prefab_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7115 = { sizeof (CreatorInfo_tBA7CED1A7F0F4F268BB6D2CA731322DAB3CAC812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7115[2] = 
{
	CreatorInfo_tBA7CED1A7F0F4F268BB6D2CA731322DAB3CAC812::get_offset_of_U3CGameObjectCreationParametersU3Ek__BackingField_0(),
	CreatorInfo_tBA7CED1A7F0F4F268BB6D2CA731322DAB3CAC812::get_offset_of_U3CCreatorU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7116 = { sizeof (SubContainerSingletonProviderCreatorByNewPrefabResource_t376F2451341DE65400990403DF0B5A966ECCBCCB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7116[3] = 
{
	SubContainerSingletonProviderCreatorByNewPrefabResource_t376F2451341DE65400990403DF0B5A966ECCBCCB::get_offset_of__markRegistry_0(),
	SubContainerSingletonProviderCreatorByNewPrefabResource_t376F2451341DE65400990403DF0B5A966ECCBCCB::get_offset_of__container_1(),
	SubContainerSingletonProviderCreatorByNewPrefabResource_t376F2451341DE65400990403DF0B5A966ECCBCCB::get_offset_of__subContainerCreators_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7117 = { sizeof (CustomSingletonId_t6F774376C22181BC6BF985D411FAABFCFC68BC1A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7117[2] = 
{
	CustomSingletonId_t6F774376C22181BC6BF985D411FAABFCFC68BC1A::get_offset_of_ConcreteIdentifier_0(),
	CustomSingletonId_t6F774376C22181BC6BF985D411FAABFCFC68BC1A::get_offset_of_ResourcePath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7118 = { sizeof (CreatorInfo_tF98CAECC326F3E1A8014BD3668EC8A7CACC9309A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7118[2] = 
{
	CreatorInfo_tF98CAECC326F3E1A8014BD3668EC8A7CACC9309A::get_offset_of_U3CGameObjectCreationParametersU3Ek__BackingField_0(),
	CreatorInfo_tF98CAECC326F3E1A8014BD3668EC8A7CACC9309A::get_offset_of_U3CCreatorU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7119 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7120 = { sizeof (SubContainerCreatorByInstaller_t8BC96FA6ADBE57A6D916B2C30F2119F2DEB852AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7120[3] = 
{
	SubContainerCreatorByInstaller_t8BC96FA6ADBE57A6D916B2C30F2119F2DEB852AA::get_offset_of__installerType_0(),
	SubContainerCreatorByInstaller_t8BC96FA6ADBE57A6D916B2C30F2119F2DEB852AA::get_offset_of__container_1(),
	SubContainerCreatorByInstaller_t8BC96FA6ADBE57A6D916B2C30F2119F2DEB852AA::get_offset_of__extraArgs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7121 = { sizeof (SubContainerCreatorByMethod_tD7A32725F700CDD41E83CACEF9E7BEC301B00C0D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7121[2] = 
{
	SubContainerCreatorByMethod_tD7A32725F700CDD41E83CACEF9E7BEC301B00C0D::get_offset_of__installMethod_0(),
	SubContainerCreatorByMethod_tD7A32725F700CDD41E83CACEF9E7BEC301B00C0D::get_offset_of__container_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7122 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7122[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7123 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7123[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7124 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7124[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7125 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7125[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7126 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7126[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7127 = { sizeof (SubContainerCreatorByNewPrefab_tBDEF1A00B4447F4E476F1427453D94C1773E99B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7127[3] = 
{
	SubContainerCreatorByNewPrefab_tBDEF1A00B4447F4E476F1427453D94C1773E99B8::get_offset_of__gameObjectBindInfo_0(),
	SubContainerCreatorByNewPrefab_tBDEF1A00B4447F4E476F1427453D94C1773E99B8::get_offset_of__prefabProvider_1(),
	SubContainerCreatorByNewPrefab_tBDEF1A00B4447F4E476F1427453D94C1773E99B8::get_offset_of__container_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7128 = { sizeof (SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7128[3] = 
{
	SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167::get_offset_of__gameObjectBindInfo_0(),
	SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167::get_offset_of__prefabProvider_1(),
	SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167::get_offset_of__container_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7129 = { sizeof (SubContainerCreatorByNewPrefabInstaller_t892C2B19E8FE3B7CB1963B14CBBD29F58041F118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7129[2] = 
{
	SubContainerCreatorByNewPrefabInstaller_t892C2B19E8FE3B7CB1963B14CBBD29F58041F118::get_offset_of__installerType_3(),
	SubContainerCreatorByNewPrefabInstaller_t892C2B19E8FE3B7CB1963B14CBBD29F58041F118::get_offset_of__extraArgs_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7130 = { sizeof (U3CU3Ec__DisplayClass3_0_t2EE5CE06A6864907EFFB3C045C04C95ACABD656A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7130[2] = 
{
	U3CU3Ec__DisplayClass3_0_t2EE5CE06A6864907EFFB3C045C04C95ACABD656A::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass3_0_t2EE5CE06A6864907EFFB3C045C04C95ACABD656A::get_offset_of_args_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7131 = { sizeof (SubContainerCreatorByNewPrefabMethod_t7476250ADAC1D7D6DF1A41D23FF9FCFFAE891E5E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7131[1] = 
{
	SubContainerCreatorByNewPrefabMethod_t7476250ADAC1D7D6DF1A41D23FF9FCFFAE891E5E::get_offset_of__installerMethod_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7132 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7132[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7133 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7133[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7134 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7134[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7135 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7135[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7136 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7136[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7137 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7137[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7138 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7138[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7139 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7139[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7140 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7140[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7141 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7141[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7142 = { sizeof (SubContainerCreatorByNewPrefabWithParams_tED1333930F622E18559D720142612284A5EF363D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7142[4] = 
{
	SubContainerCreatorByNewPrefabWithParams_tED1333930F622E18559D720142612284A5EF363D::get_offset_of__container_0(),
	SubContainerCreatorByNewPrefabWithParams_tED1333930F622E18559D720142612284A5EF363D::get_offset_of__prefabProvider_1(),
	SubContainerCreatorByNewPrefabWithParams_tED1333930F622E18559D720142612284A5EF363D::get_offset_of__installerType_2(),
	SubContainerCreatorByNewPrefabWithParams_tED1333930F622E18559D720142612284A5EF363D::get_offset_of__gameObjectBindInfo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7143 = { sizeof (U3CU3Ec__DisplayClass7_0_t2666E49305BBB041ED6762ED555EAFF2BDAC9B52), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7143[1] = 
{
	U3CU3Ec__DisplayClass7_0_t2666E49305BBB041ED6762ED555EAFF2BDAC9B52::get_offset_of_argPair_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7144 = { sizeof (SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7144[3] = 
{
	SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79::get_offset_of__subCreator_0(),
	SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79::get_offset_of__isLookingUp_1(),
	SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79::get_offset_of__subContainer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7145 = { sizeof (SubContainerDependencyProvider_t93AC6894C486CF68BB1B0DE97E462D76416DB2D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7145[3] = 
{
	SubContainerDependencyProvider_t93AC6894C486CF68BB1B0DE97E462D76416DB2D6::get_offset_of__subContainerCreator_0(),
	SubContainerDependencyProvider_t93AC6894C486CF68BB1B0DE97E462D76416DB2D6::get_offset_of__dependencyType_1(),
	SubContainerDependencyProvider_t93AC6894C486CF68BB1B0DE97E462D76416DB2D6::get_offset_of__identifier_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7146 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__6_tDB48A5FE52A7040AC7712FCF2CD0B77D425281B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7146[5] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__6_tDB48A5FE52A7040AC7712FCF2CD0B77D425281B0::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__6_tDB48A5FE52A7040AC7712FCF2CD0B77D425281B0::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__6_tDB48A5FE52A7040AC7712FCF2CD0B77D425281B0::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__6_tDB48A5FE52A7040AC7712FCF2CD0B77D425281B0::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__6_tDB48A5FE52A7040AC7712FCF2CD0B77D425281B0::get_offset_of_args_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7147 = { sizeof (TransientProvider_t4C4A97405255740DA79D817A70712C8F9F54A0BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7147[4] = 
{
	TransientProvider_t4C4A97405255740DA79D817A70712C8F9F54A0BA::get_offset_of__container_0(),
	TransientProvider_t4C4A97405255740DA79D817A70712C8F9F54A0BA::get_offset_of__concreteType_1(),
	TransientProvider_t4C4A97405255740DA79D817A70712C8F9F54A0BA::get_offset_of__concreteIdentifier_2(),
	TransientProvider_t4C4A97405255740DA79D817A70712C8F9F54A0BA::get_offset_of__extraArguments_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7148 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7148[8] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E::get_offset_of_args_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E::get_offset_of_U3CinstanceTypeU3E5__2_5(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E::get_offset_of_U3CinjectArgsU3E5__3_6(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t3550B9DB07C2703E5BF787543F992CE37A492B2E::get_offset_of_U3CinstanceU3E5__4_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7149 = { sizeof (UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7149[5] = 
{
	UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C::get_offset_of__factoryArgs_0(),
	UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C::get_offset_of__container_1(),
	UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C::get_offset_of__factoryType_2(),
	UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C::get_offset_of__concreteType_3(),
	UntypedFactoryProvider_tE3B354C34152789A038D90393A5FA6F8BA78377C::get_offset_of__createMethod_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7150 = { sizeof (U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388), -1, sizeof(U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7150[4] = 
{
	U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388_StaticFields::get_offset_of_U3CU3E9__5_0_1(),
	U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388_StaticFields::get_offset_of_U3CU3E9__6_0_2(),
	U3CU3Ec_t77932B38149E4821035D5406B2767C4FD9097388_StaticFields::get_offset_of_U3CU3E9__8_0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7151 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__8_t3A26A58CB9541FB8DAC0331BEED3C0686C4A93BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7151[4] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t3A26A58CB9541FB8DAC0331BEED3C0686C4A93BA::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t3A26A58CB9541FB8DAC0331BEED3C0686C4A93BA::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t3A26A58CB9541FB8DAC0331BEED3C0686C4A93BA::get_offset_of_U3CU3E4__this_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t3A26A58CB9541FB8DAC0331BEED3C0686C4A93BA::get_offset_of_args_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7152 = { sizeof (AnimatorIkHandlerManager_t98049FECCF8DE622675A291C459428A6E441E197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7152[1] = 
{
	AnimatorIkHandlerManager_t98049FECCF8DE622675A291C459428A6E441E197::get_offset_of__handlers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7153 = { sizeof (AnimatorInstaller_tA9DFC3F644F0EC68E3557ED92FA679C1D608650C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7153[1] = 
{
	AnimatorInstaller_tA9DFC3F644F0EC68E3557ED92FA679C1D608650C::get_offset_of__animator_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7154 = { sizeof (AnimatorMoveHandlerManager_tD3BD70043A984B9F8683F5D4A2798461623F1E8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7154[1] = 
{
	AnimatorMoveHandlerManager_tD3BD70043A984B9F8683F5D4A2798461623F1E8D::get_offset_of__handlers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7155 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7156 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7157 = { sizeof (DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7157[4] = 
{
	DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3::get_offset_of__disposables_0(),
	DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3::get_offset_of__lateDisposables_1(),
	DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3::get_offset_of__disposed_2(),
	DisposableManager_tA916CF9E0BD504B7D1DC880C3E0579AB069E8DD3::get_offset_of__lateDisposed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7158 = { sizeof (DisposableInfo_t6C27D5793195688438363D4DD565914D7E9897BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7158[2] = 
{
	DisposableInfo_t6C27D5793195688438363D4DD565914D7E9897BD::get_offset_of_Disposable_0(),
	DisposableInfo_t6C27D5793195688438363D4DD565914D7E9897BD::get_offset_of_Priority_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7159 = { sizeof (LateDisposableInfo_t6055A4F005C800036D81CAC5CBE866498ADB98D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7159[2] = 
{
	LateDisposableInfo_t6055A4F005C800036D81CAC5CBE866498ADB98D7::get_offset_of_LateDisposable_0(),
	LateDisposableInfo_t6055A4F005C800036D81CAC5CBE866498ADB98D7::get_offset_of_Priority_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7160 = { sizeof (U3CU3Ec__DisplayClass4_0_t0E7AD8540DFCAB2F95DEB9EFE1F05174BE6DB939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7160[1] = 
{
	U3CU3Ec__DisplayClass4_0_t0E7AD8540DFCAB2F95DEB9EFE1F05174BE6DB939::get_offset_of_disposable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7161 = { sizeof (U3CU3Ec__DisplayClass4_1_t1A65DF68F99A2FE270546FF0FDD191C034479AF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7161[1] = 
{
	U3CU3Ec__DisplayClass4_1_t1A65DF68F99A2FE270546FF0FDD191C034479AF8::get_offset_of_lateDisposable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7162 = { sizeof (U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505), -1, sizeof(U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7162[5] = 
{
	U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505_StaticFields::get_offset_of_U3CU3E9__4_1_1(),
	U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505_StaticFields::get_offset_of_U3CU3E9__4_3_2(),
	U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505_StaticFields::get_offset_of_U3CU3E9__8_0_3(),
	U3CU3Ec_t8EF59BB0CFCB03946AC97C9FA3191DA2E8977505_StaticFields::get_offset_of_U3CU3E9__9_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7163 = { sizeof (U3CU3Ec__DisplayClass7_0_tF3FF397A1ADBDEBEE681BFB0035407634960E785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7163[1] = 
{
	U3CU3Ec__DisplayClass7_0_tF3FF397A1ADBDEBEE681BFB0035407634960E785::get_offset_of_disposable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7164 = { sizeof (GuiRenderableManager_tAF974427AFC375E673F5B208DFF3F18D65278519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7164[1] = 
{
	GuiRenderableManager_tAF974427AFC375E673F5B208DFF3F18D65278519::get_offset_of__renderables_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7165 = { sizeof (RenderableInfo_tF2C07BF667A1895744B8252956BA9CB8EADB8B6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7165[2] = 
{
	RenderableInfo_tF2C07BF667A1895744B8252956BA9CB8EADB8B6E::get_offset_of_Renderable_0(),
	RenderableInfo_tF2C07BF667A1895744B8252956BA9CB8EADB8B6E::get_offset_of_Priority_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7166 = { sizeof (U3CU3Ec__DisplayClass1_0_t1728CC787597C136EE43EED2FB9683D6E27BDD53), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7166[1] = 
{
	U3CU3Ec__DisplayClass1_0_t1728CC787597C136EE43EED2FB9683D6E27BDD53::get_offset_of_renderable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7167 = { sizeof (U3CU3Ec_tDB8260FF47EC3120A1A86EDA379C3305207BAB54), -1, sizeof(U3CU3Ec_tDB8260FF47EC3120A1A86EDA379C3305207BAB54_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7167[3] = 
{
	U3CU3Ec_tDB8260FF47EC3120A1A86EDA379C3305207BAB54_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tDB8260FF47EC3120A1A86EDA379C3305207BAB54_StaticFields::get_offset_of_U3CU3E9__1_2_1(),
	U3CU3Ec_tDB8260FF47EC3120A1A86EDA379C3305207BAB54_StaticFields::get_offset_of_U3CU3E9__1_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7168 = { sizeof (GuiRenderer_t8385161CCA236AB0344F4B71A09E62D21078BC3E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7168[1] = 
{
	GuiRenderer_t8385161CCA236AB0344F4B71A09E62D21078BC3E::get_offset_of__renderableManager_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7169 = { sizeof (InitializableManager_tBAC718A10ABAC14E0DBD742F921323DBCD34B9F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7169[2] = 
{
	InitializableManager_tBAC718A10ABAC14E0DBD742F921323DBCD34B9F8::get_offset_of__initializables_0(),
	InitializableManager_tBAC718A10ABAC14E0DBD742F921323DBCD34B9F8::get_offset_of__hasInitialized_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7170 = { sizeof (InitializableInfo_tEC3A736CF9BF3FF84D423D84F6644750D30F059A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7170[2] = 
{
	InitializableInfo_tEC3A736CF9BF3FF84D423D84F6644750D30F059A::get_offset_of_Initializable_0(),
	InitializableInfo_tEC3A736CF9BF3FF84D423D84F6644750D30F059A::get_offset_of_Priority_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7171 = { sizeof (U3CU3Ec__DisplayClass2_0_t9BE5C72A097402E1920A49E1094FF86EC365AB50), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7171[1] = 
{
	U3CU3Ec__DisplayClass2_0_t9BE5C72A097402E1920A49E1094FF86EC365AB50::get_offset_of_initializable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7172 = { sizeof (U3CU3Ec_t7B801CA330862708CF35D062010EF9A7DDC2DF03), -1, sizeof(U3CU3Ec_t7B801CA330862708CF35D062010EF9A7DDC2DF03_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7172[3] = 
{
	U3CU3Ec_t7B801CA330862708CF35D062010EF9A7DDC2DF03_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t7B801CA330862708CF35D062010EF9A7DDC2DF03_StaticFields::get_offset_of_U3CU3E9__2_1_1(),
	U3CU3Ec_t7B801CA330862708CF35D062010EF9A7DDC2DF03_StaticFields::get_offset_of_U3CU3E9__3_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7173 = { sizeof (DefaultGameObjectKernel_tCE1A977E39CC0EF490528CF40EA6B2868BAC1382), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7174 = { sizeof (Kernel_t327BE4DD070D320F9C304747E752F8E71AB7188D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7174[3] = 
{
	Kernel_t327BE4DD070D320F9C304747E752F8E71AB7188D::get_offset_of__tickableManager_0(),
	Kernel_t327BE4DD070D320F9C304747E752F8E71AB7188D::get_offset_of__initializableManager_1(),
	Kernel_t327BE4DD070D320F9C304747E752F8E71AB7188D::get_offset_of__disposablesManager_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7175 = { sizeof (MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7175[5] = 
{
	MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF::get_offset_of__tickableManager_4(),
	MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF::get_offset_of__initializableManager_5(),
	MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF::get_offset_of__disposablesManager_6(),
	MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF::get_offset_of__hasInitialized_7(),
	MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF::get_offset_of__isDestroyed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7176 = { sizeof (ProjectKernel_t2E658E71A90EDA4D5E816495D605B0D0BB3E80AC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7177 = { sizeof (SceneKernel_t9BDB8CC78676A752727833F6396B0BFC500DE9C9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7178 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7178[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7179 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7179[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7180 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7180[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7181 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7181[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7182 = { sizeof (TickablesTaskUpdater_tCE7741F2A32B9750025C1F357267144B1F62FBE0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7183 = { sizeof (LateTickablesTaskUpdater_t03C7EF16845586540065A3D08D17571840CD4F24), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7184 = { sizeof (FixedTickablesTaskUpdater_t8455644BFB7A3B25FBA48F26B4A5253F8208CF20), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7185 = { sizeof (TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7185[11] = 
{
	TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA::get_offset_of__parents_0(),
	TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA::get_offset_of__tickables_1(),
	TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA::get_offset_of__fixedTickables_2(),
	TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA::get_offset_of__lateTickables_3(),
	TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA::get_offset_of__priorities_4(),
	TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA::get_offset_of__fixedPriorities_5(),
	TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA::get_offset_of__latePriorities_6(),
	TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA::get_offset_of__updater_7(),
	TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA::get_offset_of__fixedUpdater_8(),
	TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA::get_offset_of__lateUpdater_9(),
	TickableManager_tBD2D9B083C7654CBC5F3FE94918C0D5D35F9A8EA::get_offset_of__isPaused_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7186 = { sizeof (U3CU3Ec__DisplayClass17_0_t4C5290347F94326AE9985ADD59362ED1E5F70B30), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7186[1] = 
{
	U3CU3Ec__DisplayClass17_0_t4C5290347F94326AE9985ADD59362ED1E5F70B30::get_offset_of_tickable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7187 = { sizeof (U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9), -1, sizeof(U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7187[7] = 
{
	U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields::get_offset_of_U3CU3E9__17_0_1(),
	U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields::get_offset_of_U3CU3E9__17_2_2(),
	U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields::get_offset_of_U3CU3E9__18_0_3(),
	U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields::get_offset_of_U3CU3E9__18_2_4(),
	U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields::get_offset_of_U3CU3E9__19_0_5(),
	U3CU3Ec_t6C58537CAEA272CC8B31FFADADB8872A1C4909D9_StaticFields::get_offset_of_U3CU3E9__19_2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7188 = { sizeof (U3CU3Ec__DisplayClass18_0_tF28C23203FE97B70CA80F3B3C2FCD4682173CA80), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7188[1] = 
{
	U3CU3Ec__DisplayClass18_0_tF28C23203FE97B70CA80F3B3C2FCD4682173CA80::get_offset_of_tickable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7189 = { sizeof (U3CU3Ec__DisplayClass19_0_tB536BFDC426C4CABF221A7686C8A44173DCC5633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7189[1] = 
{
	U3CU3Ec__DisplayClass19_0_tB536BFDC426C4CABF221A7686C8A44173DCC5633::get_offset_of_tickable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7190 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7190[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7191 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7191[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7192 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7192[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7193 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7193[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7194 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7194[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7195 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7195[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7196 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7197 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7198 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7198[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7199 = { 0, 0, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
