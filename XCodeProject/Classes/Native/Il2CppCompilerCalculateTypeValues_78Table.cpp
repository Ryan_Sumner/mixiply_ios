﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BarcodeScanner.IParser
struct IParser_tC89E021D4530118E04425823F612C0839F2C9B31;
// BarcodeScanner.IWebcam
struct IWebcam_tF5BDCAF5321808E7288B5891EFA3F9BF3156B844;
// BarcodeScanner.Parser.ParserResult
struct ParserResult_t92EB97AE2A35D224A2C3624D2A7BD88D61431239;
// BarcodeScanner.ScannerSettings
struct ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69;
// GLTF.AttributeAccessor
struct AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C;
// GLTF.Schema.GLTFMaterial
struct GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99;
// GLTF.Schema.GLTFTexture
struct GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65;
// NatShareU.INatShare
struct INatShare_tC0D5A95AF026D2B0468CFD7FCC4B235DC14F64ED;
// NatShareU.Platforms.NatShareWebGL/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_tB47FCDA2F094FE229473C417AC822A75ABBC508C;
// NatShareU.Platforms.NatShareWebGL/ThumbnailDelegate
struct ThumbnailDelegate_t407F2620090BC25C8932333B96C8498F1C76A716;
// RenderHeads.Media.AVProMovieCapture.CaptureBase
struct CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110;
// RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS/Settings
struct Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E;
// RenderHeads.Media.AVProMovieCapture.CaptureFromScreen
struct CaptureFromScreen_tEB468CAE6962D15FD6A3F718B595BDF64D70007D;
// RenderHeads.Media.AVProMovieCapture.CaptureFromTexture
struct CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A;
// RenderHeads.Media.AVProMovieCapture.CaptureGUI
struct CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21;
// RenderHeads.Media.AVProMovieCapture.Demos.WebcamCaptureDemo/Instance[]
struct InstanceU5BU5D_tB835FF8E7053C7FFABF84B76C0C085AF13CBBF47;
// RenderHeads.Media.AVProMovieCapture.MotionBlur
struct MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7;
// RenderHeads.Media.AVProMovieCapture.MouseCursor
struct MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD;
// RenderHeads.Media.AVProMovieCapture.UnityAudioCapture
struct UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77;
// System.Action`2<System.String,System.String>
struct Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,GLTF.AttributeAccessor>
struct Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t8D4B47914EFD2300DFBC7D9626F3D538CFA7CA53;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0;
// System.Collections.Generic.List`1<Wizcorp.Utils.Logger.Service.ILogService>
struct List_1_t1BA564D048DB0BA822A5B24502CC2A0A59FF7DCC;
// System.Comparison`1<UnityEngine.Camera>
struct Comparison_1_t22D55C0E9B690772B778163906A02DEE7E89FFA5;
// System.EventHandler
struct EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C;
// System.IO.FileInfo
struct FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream[]
struct StreamU5BU5D_tDADB5390E6C9A2BDA8A341EECC9F7F9DE08DB362;
// System.Net.Http.HttpClient
struct HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7;
// System.Net.Http.HttpResponseMessage
struct HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904;
// System.Net.HttpListener
struct HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_tEFDFBE18E061A6065AB2FF735F1425FB59F919BC;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7;
// System.Threading.Tasks.Task
struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2;
// System.Threading.Tasks.Task`1<System.Net.Http.HttpResponseMessage>
struct Task_1_t519CB3A303B53176BD616AC273201D66B84FF270;
// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>
struct Task_1_t1359D75350E9D976BFA28AD96E417450DE277673;
// System.Threading.Thread
struct Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7;
// System.Type
struct Type_t;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8;
// UnityEngine.AnimationClip
struct AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_tF8897443AF1FC6C3E04911FAA9EDAE89B0A0A962;
// UnityEngine.AsyncOperation
struct AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.Behaviour[]
struct BehaviourU5BU5D_tB5A90D63D29342B8A4C5C3777683F6B9217C4317;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera[]
struct CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.GUIContent
struct GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0;
// UnityEngine.GUISkin
struct GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.RenderTexture
struct RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;
// UnityEngine.WebCamTexture
struct WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73;
// UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44;
// UnityGLTF.AsyncCoroutineHelper
struct AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1;
// UnityGLTF.Cache.AnimationCacheData[]
struct AnimationCacheDataU5BU5D_t30176203FA1A2587274023D60DE63D234914E64C;
// UnityGLTF.Cache.AnimationSamplerCacheData[]
struct AnimationSamplerCacheDataU5BU5D_t71BEA9683E6A207208FD0A440E556E5E8EC52580;
// UnityGLTF.Cache.BufferCacheData[]
struct BufferCacheDataU5BU5D_t80A6091FFA77C56F2DC95E720C81E4B43ADE59E4;
// UnityGLTF.Cache.MaterialCacheData[]
struct MaterialCacheDataU5BU5D_t5B04037660F1E2FB1B1793AD1EDAED07BD71F773;
// UnityGLTF.Cache.MeshCacheData[][]
struct MeshCacheDataU5BU5DU5BU5D_t835D67AD3D5E3890A3F216BA1A2B8F59729B4AD2;
// UnityGLTF.Cache.RefCountedCacheData
struct RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313;
// UnityGLTF.Cache.TextureCacheData[]
struct TextureCacheDataU5BU5D_tAC566BD6C592D6CEB6B07945C6E9235F14C37E69;
// UnityGLTF.Examples.MultiSceneComponent
struct MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A;
// UnityGLTF.Examples.SimpleHTTPServer
struct SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A;
// UnityGLTF.GLTFSceneImporter
struct GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854;
// UnityGLTF.Loader.FileLoader
struct FileLoader_t67D17B03BB535CB6735C669B564A5355BA21ADE6;
// UnityGLTF.Loader.ILoader
struct ILoader_tADAD26ECAE6A56C30083D1ECF482BB9B59F55371;
// UnityGLTF.Loader.WebRequestLoader
struct WebRequestLoader_t09E04D05D3AEA5847FB8314469A2D4AC3E6234CB;
// ZXing.BarcodeReader
struct BarcodeReader_tEC59FAC295BC5A75121E2CF723C04E0748921224;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASSETBUNDLELOADOPERATION_T4E376230379EB84B5F4A6D7F957B791348E72141_H
#define ASSETBUNDLELOADOPERATION_T4E376230379EB84B5F4A6D7F957B791348E72141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleLoadOperation
struct  AssetBundleLoadOperation_t4E376230379EB84B5F4A6D7F957B791348E72141  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLELOADOPERATION_T4E376230379EB84B5F4A6D7F957B791348E72141_H
#ifndef PARSERRESULT_T92EB97AE2A35D224A2C3624D2A7BD88D61431239_H
#define PARSERRESULT_T92EB97AE2A35D224A2C3624D2A7BD88D61431239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarcodeScanner.Parser.ParserResult
struct  ParserResult_t92EB97AE2A35D224A2C3624D2A7BD88D61431239  : public RuntimeObject
{
public:
	// System.String BarcodeScanner.Parser.ParserResult::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_0;
	// System.String BarcodeScanner.Parser.ParserResult::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ParserResult_t92EB97AE2A35D224A2C3624D2A7BD88D61431239, ___U3CTypeU3Ek__BackingField_0)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ParserResult_t92EB97AE2A35D224A2C3624D2A7BD88D61431239, ___U3CValueU3Ek__BackingField_1)); }
	inline String_t* get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(String_t* value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSERRESULT_T92EB97AE2A35D224A2C3624D2A7BD88D61431239_H
#ifndef ZXINGPARSER_T7B9AC8160C3A1565FA96F8714D30B9C7E852DE26_H
#define ZXINGPARSER_T7B9AC8160C3A1565FA96F8714D30B9C7E852DE26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarcodeScanner.Parser.ZXingParser
struct  ZXingParser_t7B9AC8160C3A1565FA96F8714D30B9C7E852DE26  : public RuntimeObject
{
public:
	// ZXing.BarcodeReader BarcodeScanner.Parser.ZXingParser::<Scanner>k__BackingField
	BarcodeReader_tEC59FAC295BC5A75121E2CF723C04E0748921224 * ___U3CScannerU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CScannerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ZXingParser_t7B9AC8160C3A1565FA96F8714D30B9C7E852DE26, ___U3CScannerU3Ek__BackingField_0)); }
	inline BarcodeReader_tEC59FAC295BC5A75121E2CF723C04E0748921224 * get_U3CScannerU3Ek__BackingField_0() const { return ___U3CScannerU3Ek__BackingField_0; }
	inline BarcodeReader_tEC59FAC295BC5A75121E2CF723C04E0748921224 ** get_address_of_U3CScannerU3Ek__BackingField_0() { return &___U3CScannerU3Ek__BackingField_0; }
	inline void set_U3CScannerU3Ek__BackingField_0(BarcodeReader_tEC59FAC295BC5A75121E2CF723C04E0748921224 * value)
	{
		___U3CScannerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScannerU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZXINGPARSER_T7B9AC8160C3A1565FA96F8714D30B9C7E852DE26_H
#ifndef UNITYWEBCAM_T3E74C79DAEF401347835D65AD9D25B86C1E36313_H
#define UNITYWEBCAM_T3E74C79DAEF401347835D65AD9D25B86C1E36313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarcodeScanner.Webcam.UnityWebcam
struct  UnityWebcam_t3E74C79DAEF401347835D65AD9D25B86C1E36313  : public RuntimeObject
{
public:
	// UnityEngine.WebCamTexture BarcodeScanner.Webcam.UnityWebcam::<Webcam>k__BackingField
	WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * ___U3CWebcamU3Ek__BackingField_0;
	// System.Int32 BarcodeScanner.Webcam.UnityWebcam::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_1;
	// System.Int32 BarcodeScanner.Webcam.UnityWebcam::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CWebcamU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UnityWebcam_t3E74C79DAEF401347835D65AD9D25B86C1E36313, ___U3CWebcamU3Ek__BackingField_0)); }
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * get_U3CWebcamU3Ek__BackingField_0() const { return ___U3CWebcamU3Ek__BackingField_0; }
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 ** get_address_of_U3CWebcamU3Ek__BackingField_0() { return &___U3CWebcamU3Ek__BackingField_0; }
	inline void set_U3CWebcamU3Ek__BackingField_0(WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * value)
	{
		___U3CWebcamU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWebcamU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnityWebcam_t3E74C79DAEF401347835D65AD9D25B86C1E36313, ___U3CWidthU3Ek__BackingField_1)); }
	inline int32_t get_U3CWidthU3Ek__BackingField_1() const { return ___U3CWidthU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CWidthU3Ek__BackingField_1() { return &___U3CWidthU3Ek__BackingField_1; }
	inline void set_U3CWidthU3Ek__BackingField_1(int32_t value)
	{
		___U3CWidthU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnityWebcam_t3E74C79DAEF401347835D65AD9D25B86C1E36313, ___U3CHeightU3Ek__BackingField_2)); }
	inline int32_t get_U3CHeightU3Ek__BackingField_2() const { return ___U3CHeightU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CHeightU3Ek__BackingField_2() { return &___U3CHeightU3Ek__BackingField_2; }
	inline void set_U3CHeightU3Ek__BackingField_2(int32_t value)
	{
		___U3CHeightU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYWEBCAM_T3E74C79DAEF401347835D65AD9D25B86C1E36313_H
#ifndef NATSHARE_T57B1B3F8122D1A2445260CBDA02BB51194C80442_H
#define NATSHARE_T57B1B3F8122D1A2445260CBDA02BB51194C80442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NatShareU.NatShare
struct  NatShare_t57B1B3F8122D1A2445260CBDA02BB51194C80442  : public RuntimeObject
{
public:

public:
};

struct NatShare_t57B1B3F8122D1A2445260CBDA02BB51194C80442_StaticFields
{
public:
	// NatShareU.INatShare NatShareU.NatShare::Implementation
	RuntimeObject* ___Implementation_0;

public:
	inline static int32_t get_offset_of_Implementation_0() { return static_cast<int32_t>(offsetof(NatShare_t57B1B3F8122D1A2445260CBDA02BB51194C80442_StaticFields, ___Implementation_0)); }
	inline RuntimeObject* get_Implementation_0() const { return ___Implementation_0; }
	inline RuntimeObject** get_address_of_Implementation_0() { return &___Implementation_0; }
	inline void set_Implementation_0(RuntimeObject* value)
	{
		___Implementation_0 = value;
		Il2CppCodeGenWriteBarrier((&___Implementation_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATSHARE_T57B1B3F8122D1A2445260CBDA02BB51194C80442_H
#ifndef NATSHAREANDROID_T4038EB6E58E38F1959DCB6762FF4128B853F4AE7_H
#define NATSHAREANDROID_T4038EB6E58E38F1959DCB6762FF4128B853F4AE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NatShareU.Platforms.NatShareAndroid
struct  NatShareAndroid_t4038EB6E58E38F1959DCB6762FF4128B853F4AE7  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaClass NatShareU.Platforms.NatShareAndroid::natshare
	AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * ___natshare_0;

public:
	inline static int32_t get_offset_of_natshare_0() { return static_cast<int32_t>(offsetof(NatShareAndroid_t4038EB6E58E38F1959DCB6762FF4128B853F4AE7, ___natshare_0)); }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * get_natshare_0() const { return ___natshare_0; }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 ** get_address_of_natshare_0() { return &___natshare_0; }
	inline void set_natshare_0(AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * value)
	{
		___natshare_0 = value;
		Il2CppCodeGenWriteBarrier((&___natshare_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATSHAREANDROID_T4038EB6E58E38F1959DCB6762FF4128B853F4AE7_H
#ifndef NATSHAREBRIDGE_T2A766E7C6E2043B7A97C213AD0EE5B37E7123977_H
#define NATSHAREBRIDGE_T2A766E7C6E2043B7A97C213AD0EE5B37E7123977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NatShareU.Platforms.NatShareBridge
struct  NatShareBridge_t2A766E7C6E2043B7A97C213AD0EE5B37E7123977  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATSHAREBRIDGE_T2A766E7C6E2043B7A97C213AD0EE5B37E7123977_H
#ifndef NATSHARENULL_T9B8BBA5A7C8CF157E2E1D53FD5DADE79595DCB94_H
#define NATSHARENULL_T9B8BBA5A7C8CF157E2E1D53FD5DADE79595DCB94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NatShareU.Platforms.NatShareNull
struct  NatShareNull_t9B8BBA5A7C8CF157E2E1D53FD5DADE79595DCB94  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATSHARENULL_T9B8BBA5A7C8CF157E2E1D53FD5DADE79595DCB94_H
#ifndef NATSHAREWEBGL_T4E822D25A7510CBFB9CDC3D24D0C09C2D3B264BE_H
#define NATSHAREWEBGL_T4E822D25A7510CBFB9CDC3D24D0C09C2D3B264BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NatShareU.Platforms.NatShareWebGL
struct  NatShareWebGL_t4E822D25A7510CBFB9CDC3D24D0C09C2D3B264BE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATSHAREWEBGL_T4E822D25A7510CBFB9CDC3D24D0C09C2D3B264BE_H
#ifndef NATSHAREIOS_TEF7F52531CD9F1C108A99CAD9A9D0A5516CCB25D_H
#define NATSHAREIOS_TEF7F52531CD9F1C108A99CAD9A9D0A5516CCB25D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NatShareU.Platforms.NatShareiOS
struct  NatShareiOS_tEF7F52531CD9F1C108A99CAD9A9D0A5516CCB25D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATSHAREIOS_TEF7F52531CD9F1C108A99CAD9A9D0A5516CCB25D_H
#ifndef U3CFINALRENDERCAPTUREU3ED__3_TA3C60BC9D1528B7467BCC9B6DDF74F0797C40457_H
#define U3CFINALRENDERCAPTUREU3ED__3_TA3C60BC9D1528B7467BCC9B6DDF74F0797C40457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureFromScreen/<FinalRenderCapture>d__3
struct  U3CFinalRenderCaptureU3Ed__3_tA3C60BC9D1528B7467BCC9B6DDF74F0797C40457  : public RuntimeObject
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureFromScreen/<FinalRenderCapture>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RenderHeads.Media.AVProMovieCapture.CaptureFromScreen/<FinalRenderCapture>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RenderHeads.Media.AVProMovieCapture.CaptureFromScreen RenderHeads.Media.AVProMovieCapture.CaptureFromScreen/<FinalRenderCapture>d__3::<>4__this
	CaptureFromScreen_tEB468CAE6962D15FD6A3F718B595BDF64D70007D * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFinalRenderCaptureU3Ed__3_tA3C60BC9D1528B7467BCC9B6DDF74F0797C40457, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFinalRenderCaptureU3Ed__3_tA3C60BC9D1528B7467BCC9B6DDF74F0797C40457, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFinalRenderCaptureU3Ed__3_tA3C60BC9D1528B7467BCC9B6DDF74F0797C40457, ___U3CU3E4__this_2)); }
	inline CaptureFromScreen_tEB468CAE6962D15FD6A3F718B595BDF64D70007D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CaptureFromScreen_tEB468CAE6962D15FD6A3F718B595BDF64D70007D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CaptureFromScreen_tEB468CAE6962D15FD6A3F718B595BDF64D70007D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINALRENDERCAPTUREU3ED__3_TA3C60BC9D1528B7467BCC9B6DDF74F0797C40457_H
#ifndef U3CKILLCUBEU3ED__10_TF0E4FE27186E2A9C445D85FCD0001E0290014949_H
#define U3CKILLCUBEU3ED__10_TF0E4FE27186E2A9C445D85FCD0001E0290014949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.Demos.SurroundCaptureDemo/<KillCube>d__10
struct  U3CKillCubeU3Ed__10_tF0E4FE27186E2A9C445D85FCD0001E0290014949  : public RuntimeObject
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.Demos.SurroundCaptureDemo/<KillCube>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RenderHeads.Media.AVProMovieCapture.Demos.SurroundCaptureDemo/<KillCube>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.GameObject RenderHeads.Media.AVProMovieCapture.Demos.SurroundCaptureDemo/<KillCube>d__10::go
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CKillCubeU3Ed__10_tF0E4FE27186E2A9C445D85FCD0001E0290014949, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CKillCubeU3Ed__10_tF0E4FE27186E2A9C445D85FCD0001E0290014949, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_go_2() { return static_cast<int32_t>(offsetof(U3CKillCubeU3Ed__10_tF0E4FE27186E2A9C445D85FCD0001E0290014949, ___go_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_go_2() const { return ___go_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_go_2() { return &___go_2; }
	inline void set_go_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___go_2 = value;
		Il2CppCodeGenWriteBarrier((&___go_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CKILLCUBEU3ED__10_TF0E4FE27186E2A9C445D85FCD0001E0290014949_H
#ifndef INSTANCE_TE55BE73BDE833127C4D2104E58CCB2E5EBEF78AE_H
#define INSTANCE_TE55BE73BDE833127C4D2104E58CCB2E5EBEF78AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.Demos.WebcamCaptureDemo/Instance
struct  Instance_tE55BE73BDE833127C4D2104E58CCB2E5EBEF78AE  : public RuntimeObject
{
public:
	// System.String RenderHeads.Media.AVProMovieCapture.Demos.WebcamCaptureDemo/Instance::name
	String_t* ___name_0;
	// UnityEngine.WebCamTexture RenderHeads.Media.AVProMovieCapture.Demos.WebcamCaptureDemo/Instance::texture
	WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * ___texture_1;
	// RenderHeads.Media.AVProMovieCapture.CaptureFromTexture RenderHeads.Media.AVProMovieCapture.Demos.WebcamCaptureDemo/Instance::capture
	CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A * ___capture_2;
	// RenderHeads.Media.AVProMovieCapture.CaptureGUI RenderHeads.Media.AVProMovieCapture.Demos.WebcamCaptureDemo/Instance::gui
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21 * ___gui_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Instance_tE55BE73BDE833127C4D2104E58CCB2E5EBEF78AE, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_texture_1() { return static_cast<int32_t>(offsetof(Instance_tE55BE73BDE833127C4D2104E58CCB2E5EBEF78AE, ___texture_1)); }
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * get_texture_1() const { return ___texture_1; }
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 ** get_address_of_texture_1() { return &___texture_1; }
	inline void set_texture_1(WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * value)
	{
		___texture_1 = value;
		Il2CppCodeGenWriteBarrier((&___texture_1), value);
	}

	inline static int32_t get_offset_of_capture_2() { return static_cast<int32_t>(offsetof(Instance_tE55BE73BDE833127C4D2104E58CCB2E5EBEF78AE, ___capture_2)); }
	inline CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A * get_capture_2() const { return ___capture_2; }
	inline CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A ** get_address_of_capture_2() { return &___capture_2; }
	inline void set_capture_2(CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A * value)
	{
		___capture_2 = value;
		Il2CppCodeGenWriteBarrier((&___capture_2), value);
	}

	inline static int32_t get_offset_of_gui_3() { return static_cast<int32_t>(offsetof(Instance_tE55BE73BDE833127C4D2104E58CCB2E5EBEF78AE, ___gui_3)); }
	inline CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21 * get_gui_3() const { return ___gui_3; }
	inline CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21 ** get_address_of_gui_3() { return &___gui_3; }
	inline void set_gui_3(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21 * value)
	{
		___gui_3 = value;
		Il2CppCodeGenWriteBarrier((&___gui_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCE_TE55BE73BDE833127C4D2104E58CCB2E5EBEF78AE_H
#ifndef NATIVEPLUGIN_TD0BE1E9473BBD3CD8A3F7C990F19D582270BD293_H
#define NATIVEPLUGIN_TD0BE1E9473BBD3CD8A3F7C990F19D582270BD293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.NativePlugin
struct  NativePlugin_tD0BE1E9473BBD3CD8A3F7C990F19D582270BD293  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEPLUGIN_TD0BE1E9473BBD3CD8A3F7C990F19D582270BD293_H
#ifndef UTILS_T432AAACD1A54848334280545542E16BC10339FB3_H
#define UTILS_T432AAACD1A54848334280545542E16BC10339FB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.Utils
struct  Utils_t432AAACD1A54848334280545542E16BC10339FB3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T432AAACD1A54848334280545542E16BC10339FB3_H
#ifndef U3CU3EC_TED58CC381B12285A5523E8913E006FB866934B2E_H
#define U3CU3EC_TED58CC381B12285A5523E8913E006FB866934B2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.Utils/<>c
struct  U3CU3Ec_tED58CC381B12285A5523E8913E006FB866934B2E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tED58CC381B12285A5523E8913E006FB866934B2E_StaticFields
{
public:
	// RenderHeads.Media.AVProMovieCapture.Utils/<>c RenderHeads.Media.AVProMovieCapture.Utils/<>c::<>9
	U3CU3Ec_tED58CC381B12285A5523E8913E006FB866934B2E * ___U3CU3E9_0;
	// System.Comparison`1<UnityEngine.Camera> RenderHeads.Media.AVProMovieCapture.Utils/<>c::<>9__2_0
	Comparison_1_t22D55C0E9B690772B778163906A02DEE7E89FFA5 * ___U3CU3E9__2_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tED58CC381B12285A5523E8913E006FB866934B2E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tED58CC381B12285A5523E8913E006FB866934B2E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tED58CC381B12285A5523E8913E006FB866934B2E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tED58CC381B12285A5523E8913E006FB866934B2E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tED58CC381B12285A5523E8913E006FB866934B2E_StaticFields, ___U3CU3E9__2_0_1)); }
	inline Comparison_1_t22D55C0E9B690772B778163906A02DEE7E89FFA5 * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline Comparison_1_t22D55C0E9B690772B778163906A02DEE7E89FFA5 ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(Comparison_1_t22D55C0E9B690772B778163906A02DEE7E89FFA5 * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TED58CC381B12285A5523E8913E006FB866934B2E_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ANIMATIONCACHEDATA_TF59B339794F2D645D67DD3BBC4F2B75D81985486_H
#define ANIMATIONCACHEDATA_TF59B339794F2D645D67DD3BBC4F2B75D81985486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Cache.AnimationCacheData
struct  AnimationCacheData_tF59B339794F2D645D67DD3BBC4F2B75D81985486  : public RuntimeObject
{
public:
	// UnityEngine.AnimationClip UnityGLTF.Cache.AnimationCacheData::<LoadedAnimationClip>k__BackingField
	AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * ___U3CLoadedAnimationClipU3Ek__BackingField_0;
	// UnityGLTF.Cache.AnimationSamplerCacheData[] UnityGLTF.Cache.AnimationCacheData::<Samplers>k__BackingField
	AnimationSamplerCacheDataU5BU5D_t71BEA9683E6A207208FD0A440E556E5E8EC52580* ___U3CSamplersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CLoadedAnimationClipU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AnimationCacheData_tF59B339794F2D645D67DD3BBC4F2B75D81985486, ___U3CLoadedAnimationClipU3Ek__BackingField_0)); }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * get_U3CLoadedAnimationClipU3Ek__BackingField_0() const { return ___U3CLoadedAnimationClipU3Ek__BackingField_0; }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE ** get_address_of_U3CLoadedAnimationClipU3Ek__BackingField_0() { return &___U3CLoadedAnimationClipU3Ek__BackingField_0; }
	inline void set_U3CLoadedAnimationClipU3Ek__BackingField_0(AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * value)
	{
		___U3CLoadedAnimationClipU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadedAnimationClipU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CSamplersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AnimationCacheData_tF59B339794F2D645D67DD3BBC4F2B75D81985486, ___U3CSamplersU3Ek__BackingField_1)); }
	inline AnimationSamplerCacheDataU5BU5D_t71BEA9683E6A207208FD0A440E556E5E8EC52580* get_U3CSamplersU3Ek__BackingField_1() const { return ___U3CSamplersU3Ek__BackingField_1; }
	inline AnimationSamplerCacheDataU5BU5D_t71BEA9683E6A207208FD0A440E556E5E8EC52580** get_address_of_U3CSamplersU3Ek__BackingField_1() { return &___U3CSamplersU3Ek__BackingField_1; }
	inline void set_U3CSamplersU3Ek__BackingField_1(AnimationSamplerCacheDataU5BU5D_t71BEA9683E6A207208FD0A440E556E5E8EC52580* value)
	{
		___U3CSamplersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSamplersU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCACHEDATA_TF59B339794F2D645D67DD3BBC4F2B75D81985486_H
#ifndef ASSETCACHE_T88E90B7130D52BEA080DAD5B41B03AE936D55350_H
#define ASSETCACHE_T88E90B7130D52BEA080DAD5B41B03AE936D55350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Cache.AssetCache
struct  AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350  : public RuntimeObject
{
public:
	// System.IO.Stream[] UnityGLTF.Cache.AssetCache::<ImageStreamCache>k__BackingField
	StreamU5BU5D_tDADB5390E6C9A2BDA8A341EECC9F7F9DE08DB362* ___U3CImageStreamCacheU3Ek__BackingField_0;
	// UnityEngine.Texture2D[] UnityGLTF.Cache.AssetCache::<ImageCache>k__BackingField
	Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* ___U3CImageCacheU3Ek__BackingField_1;
	// UnityGLTF.Cache.TextureCacheData[] UnityGLTF.Cache.AssetCache::<TextureCache>k__BackingField
	TextureCacheDataU5BU5D_tAC566BD6C592D6CEB6B07945C6E9235F14C37E69* ___U3CTextureCacheU3Ek__BackingField_2;
	// UnityGLTF.Cache.MaterialCacheData[] UnityGLTF.Cache.AssetCache::<MaterialCache>k__BackingField
	MaterialCacheDataU5BU5D_t5B04037660F1E2FB1B1793AD1EDAED07BD71F773* ___U3CMaterialCacheU3Ek__BackingField_3;
	// UnityGLTF.Cache.BufferCacheData[] UnityGLTF.Cache.AssetCache::<BufferCache>k__BackingField
	BufferCacheDataU5BU5D_t80A6091FFA77C56F2DC95E720C81E4B43ADE59E4* ___U3CBufferCacheU3Ek__BackingField_4;
	// UnityGLTF.Cache.MeshCacheData[][] UnityGLTF.Cache.AssetCache::<MeshCache>k__BackingField
	MeshCacheDataU5BU5DU5BU5D_t835D67AD3D5E3890A3F216BA1A2B8F59729B4AD2* ___U3CMeshCacheU3Ek__BackingField_5;
	// UnityGLTF.Cache.AnimationCacheData[] UnityGLTF.Cache.AssetCache::<AnimationCache>k__BackingField
	AnimationCacheDataU5BU5D_t30176203FA1A2587274023D60DE63D234914E64C* ___U3CAnimationCacheU3Ek__BackingField_6;
	// UnityEngine.GameObject[] UnityGLTF.Cache.AssetCache::<NodeCache>k__BackingField
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___U3CNodeCacheU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CImageStreamCacheU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350, ___U3CImageStreamCacheU3Ek__BackingField_0)); }
	inline StreamU5BU5D_tDADB5390E6C9A2BDA8A341EECC9F7F9DE08DB362* get_U3CImageStreamCacheU3Ek__BackingField_0() const { return ___U3CImageStreamCacheU3Ek__BackingField_0; }
	inline StreamU5BU5D_tDADB5390E6C9A2BDA8A341EECC9F7F9DE08DB362** get_address_of_U3CImageStreamCacheU3Ek__BackingField_0() { return &___U3CImageStreamCacheU3Ek__BackingField_0; }
	inline void set_U3CImageStreamCacheU3Ek__BackingField_0(StreamU5BU5D_tDADB5390E6C9A2BDA8A341EECC9F7F9DE08DB362* value)
	{
		___U3CImageStreamCacheU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageStreamCacheU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CImageCacheU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350, ___U3CImageCacheU3Ek__BackingField_1)); }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* get_U3CImageCacheU3Ek__BackingField_1() const { return ___U3CImageCacheU3Ek__BackingField_1; }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9** get_address_of_U3CImageCacheU3Ek__BackingField_1() { return &___U3CImageCacheU3Ek__BackingField_1; }
	inline void set_U3CImageCacheU3Ek__BackingField_1(Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* value)
	{
		___U3CImageCacheU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageCacheU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTextureCacheU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350, ___U3CTextureCacheU3Ek__BackingField_2)); }
	inline TextureCacheDataU5BU5D_tAC566BD6C592D6CEB6B07945C6E9235F14C37E69* get_U3CTextureCacheU3Ek__BackingField_2() const { return ___U3CTextureCacheU3Ek__BackingField_2; }
	inline TextureCacheDataU5BU5D_tAC566BD6C592D6CEB6B07945C6E9235F14C37E69** get_address_of_U3CTextureCacheU3Ek__BackingField_2() { return &___U3CTextureCacheU3Ek__BackingField_2; }
	inline void set_U3CTextureCacheU3Ek__BackingField_2(TextureCacheDataU5BU5D_tAC566BD6C592D6CEB6B07945C6E9235F14C37E69* value)
	{
		___U3CTextureCacheU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextureCacheU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CMaterialCacheU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350, ___U3CMaterialCacheU3Ek__BackingField_3)); }
	inline MaterialCacheDataU5BU5D_t5B04037660F1E2FB1B1793AD1EDAED07BD71F773* get_U3CMaterialCacheU3Ek__BackingField_3() const { return ___U3CMaterialCacheU3Ek__BackingField_3; }
	inline MaterialCacheDataU5BU5D_t5B04037660F1E2FB1B1793AD1EDAED07BD71F773** get_address_of_U3CMaterialCacheU3Ek__BackingField_3() { return &___U3CMaterialCacheU3Ek__BackingField_3; }
	inline void set_U3CMaterialCacheU3Ek__BackingField_3(MaterialCacheDataU5BU5D_t5B04037660F1E2FB1B1793AD1EDAED07BD71F773* value)
	{
		___U3CMaterialCacheU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaterialCacheU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CBufferCacheU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350, ___U3CBufferCacheU3Ek__BackingField_4)); }
	inline BufferCacheDataU5BU5D_t80A6091FFA77C56F2DC95E720C81E4B43ADE59E4* get_U3CBufferCacheU3Ek__BackingField_4() const { return ___U3CBufferCacheU3Ek__BackingField_4; }
	inline BufferCacheDataU5BU5D_t80A6091FFA77C56F2DC95E720C81E4B43ADE59E4** get_address_of_U3CBufferCacheU3Ek__BackingField_4() { return &___U3CBufferCacheU3Ek__BackingField_4; }
	inline void set_U3CBufferCacheU3Ek__BackingField_4(BufferCacheDataU5BU5D_t80A6091FFA77C56F2DC95E720C81E4B43ADE59E4* value)
	{
		___U3CBufferCacheU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBufferCacheU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CMeshCacheU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350, ___U3CMeshCacheU3Ek__BackingField_5)); }
	inline MeshCacheDataU5BU5DU5BU5D_t835D67AD3D5E3890A3F216BA1A2B8F59729B4AD2* get_U3CMeshCacheU3Ek__BackingField_5() const { return ___U3CMeshCacheU3Ek__BackingField_5; }
	inline MeshCacheDataU5BU5DU5BU5D_t835D67AD3D5E3890A3F216BA1A2B8F59729B4AD2** get_address_of_U3CMeshCacheU3Ek__BackingField_5() { return &___U3CMeshCacheU3Ek__BackingField_5; }
	inline void set_U3CMeshCacheU3Ek__BackingField_5(MeshCacheDataU5BU5DU5BU5D_t835D67AD3D5E3890A3F216BA1A2B8F59729B4AD2* value)
	{
		___U3CMeshCacheU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMeshCacheU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CAnimationCacheU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350, ___U3CAnimationCacheU3Ek__BackingField_6)); }
	inline AnimationCacheDataU5BU5D_t30176203FA1A2587274023D60DE63D234914E64C* get_U3CAnimationCacheU3Ek__BackingField_6() const { return ___U3CAnimationCacheU3Ek__BackingField_6; }
	inline AnimationCacheDataU5BU5D_t30176203FA1A2587274023D60DE63D234914E64C** get_address_of_U3CAnimationCacheU3Ek__BackingField_6() { return &___U3CAnimationCacheU3Ek__BackingField_6; }
	inline void set_U3CAnimationCacheU3Ek__BackingField_6(AnimationCacheDataU5BU5D_t30176203FA1A2587274023D60DE63D234914E64C* value)
	{
		___U3CAnimationCacheU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnimationCacheU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CNodeCacheU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350, ___U3CNodeCacheU3Ek__BackingField_7)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_U3CNodeCacheU3Ek__BackingField_7() const { return ___U3CNodeCacheU3Ek__BackingField_7; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_U3CNodeCacheU3Ek__BackingField_7() { return &___U3CNodeCacheU3Ek__BackingField_7; }
	inline void set_U3CNodeCacheU3Ek__BackingField_7(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___U3CNodeCacheU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNodeCacheU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETCACHE_T88E90B7130D52BEA080DAD5B41B03AE936D55350_H
#ifndef BUFFERCACHEDATA_T4013B91D6EF6FD8E4915E9B533EF479351F01F7D_H
#define BUFFERCACHEDATA_T4013B91D6EF6FD8E4915E9B533EF479351F01F7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Cache.BufferCacheData
struct  BufferCacheData_t4013B91D6EF6FD8E4915E9B533EF479351F01F7D  : public RuntimeObject
{
public:
	// System.UInt32 UnityGLTF.Cache.BufferCacheData::<ChunkOffset>k__BackingField
	uint32_t ___U3CChunkOffsetU3Ek__BackingField_0;
	// System.IO.Stream UnityGLTF.Cache.BufferCacheData::<Stream>k__BackingField
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___U3CStreamU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CChunkOffsetU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BufferCacheData_t4013B91D6EF6FD8E4915E9B533EF479351F01F7D, ___U3CChunkOffsetU3Ek__BackingField_0)); }
	inline uint32_t get_U3CChunkOffsetU3Ek__BackingField_0() const { return ___U3CChunkOffsetU3Ek__BackingField_0; }
	inline uint32_t* get_address_of_U3CChunkOffsetU3Ek__BackingField_0() { return &___U3CChunkOffsetU3Ek__BackingField_0; }
	inline void set_U3CChunkOffsetU3Ek__BackingField_0(uint32_t value)
	{
		___U3CChunkOffsetU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStreamU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BufferCacheData_t4013B91D6EF6FD8E4915E9B533EF479351F01F7D, ___U3CStreamU3Ek__BackingField_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_U3CStreamU3Ek__BackingField_1() const { return ___U3CStreamU3Ek__BackingField_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_U3CStreamU3Ek__BackingField_1() { return &___U3CStreamU3Ek__BackingField_1; }
	inline void set_U3CStreamU3Ek__BackingField_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___U3CStreamU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStreamU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERCACHEDATA_T4013B91D6EF6FD8E4915E9B533EF479351F01F7D_H
#ifndef MATERIALCACHEDATA_T446B81A7953C1C9447C97D43B76A29E0956B4BB1_H
#define MATERIALCACHEDATA_T446B81A7953C1C9447C97D43B76A29E0956B4BB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Cache.MaterialCacheData
struct  MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1  : public RuntimeObject
{
public:
	// UnityEngine.Material UnityGLTF.Cache.MaterialCacheData::<UnityMaterial>k__BackingField
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___U3CUnityMaterialU3Ek__BackingField_0;
	// UnityEngine.Material UnityGLTF.Cache.MaterialCacheData::<UnityMaterialWithVertexColor>k__BackingField
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1;
	// GLTF.Schema.GLTFMaterial UnityGLTF.Cache.MaterialCacheData::<GLTFMaterial>k__BackingField
	GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 * ___U3CGLTFMaterialU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CUnityMaterialU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1, ___U3CUnityMaterialU3Ek__BackingField_0)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_U3CUnityMaterialU3Ek__BackingField_0() const { return ___U3CUnityMaterialU3Ek__BackingField_0; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_U3CUnityMaterialU3Ek__BackingField_0() { return &___U3CUnityMaterialU3Ek__BackingField_0; }
	inline void set_U3CUnityMaterialU3Ek__BackingField_0(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___U3CUnityMaterialU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnityMaterialU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CUnityMaterialWithVertexColorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1, ___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_U3CUnityMaterialWithVertexColorU3Ek__BackingField_1() const { return ___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_U3CUnityMaterialWithVertexColorU3Ek__BackingField_1() { return &___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1; }
	inline void set_U3CUnityMaterialWithVertexColorU3Ek__BackingField_1(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CGLTFMaterialU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1, ___U3CGLTFMaterialU3Ek__BackingField_2)); }
	inline GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 * get_U3CGLTFMaterialU3Ek__BackingField_2() const { return ___U3CGLTFMaterialU3Ek__BackingField_2; }
	inline GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 ** get_address_of_U3CGLTFMaterialU3Ek__BackingField_2() { return &___U3CGLTFMaterialU3Ek__BackingField_2; }
	inline void set_U3CGLTFMaterialU3Ek__BackingField_2(GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 * value)
	{
		___U3CGLTFMaterialU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGLTFMaterialU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALCACHEDATA_T446B81A7953C1C9447C97D43B76A29E0956B4BB1_H
#ifndef MESHCACHEDATA_T8E3920989E4AF559033301071A663BCD94671213_H
#define MESHCACHEDATA_T8E3920989E4AF559033301071A663BCD94671213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Cache.MeshCacheData
struct  MeshCacheData_t8E3920989E4AF559033301071A663BCD94671213  : public RuntimeObject
{
public:
	// UnityEngine.Mesh UnityGLTF.Cache.MeshCacheData::<LoadedMesh>k__BackingField
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___U3CLoadedMeshU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,GLTF.AttributeAccessor> UnityGLTF.Cache.MeshCacheData::<MeshAttributes>k__BackingField
	Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C * ___U3CMeshAttributesU3Ek__BackingField_1;
	// UnityEngine.GameObject UnityGLTF.Cache.MeshCacheData::<PrimitiveGO>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CPrimitiveGOU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CLoadedMeshU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MeshCacheData_t8E3920989E4AF559033301071A663BCD94671213, ___U3CLoadedMeshU3Ek__BackingField_0)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_U3CLoadedMeshU3Ek__BackingField_0() const { return ___U3CLoadedMeshU3Ek__BackingField_0; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_U3CLoadedMeshU3Ek__BackingField_0() { return &___U3CLoadedMeshU3Ek__BackingField_0; }
	inline void set_U3CLoadedMeshU3Ek__BackingField_0(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___U3CLoadedMeshU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadedMeshU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CMeshAttributesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MeshCacheData_t8E3920989E4AF559033301071A663BCD94671213, ___U3CMeshAttributesU3Ek__BackingField_1)); }
	inline Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C * get_U3CMeshAttributesU3Ek__BackingField_1() const { return ___U3CMeshAttributesU3Ek__BackingField_1; }
	inline Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C ** get_address_of_U3CMeshAttributesU3Ek__BackingField_1() { return &___U3CMeshAttributesU3Ek__BackingField_1; }
	inline void set_U3CMeshAttributesU3Ek__BackingField_1(Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C * value)
	{
		___U3CMeshAttributesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMeshAttributesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CPrimitiveGOU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MeshCacheData_t8E3920989E4AF559033301071A663BCD94671213, ___U3CPrimitiveGOU3Ek__BackingField_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CPrimitiveGOU3Ek__BackingField_2() const { return ___U3CPrimitiveGOU3Ek__BackingField_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CPrimitiveGOU3Ek__BackingField_2() { return &___U3CPrimitiveGOU3Ek__BackingField_2; }
	inline void set_U3CPrimitiveGOU3Ek__BackingField_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CPrimitiveGOU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrimitiveGOU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHCACHEDATA_T8E3920989E4AF559033301071A663BCD94671213_H
#ifndef REFCOUNTEDCACHEDATA_T68F587D716850E3080A86210BDD8C96EFF7ED313_H
#define REFCOUNTEDCACHEDATA_T68F587D716850E3080A86210BDD8C96EFF7ED313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Cache.RefCountedCacheData
struct  RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313  : public RuntimeObject
{
public:
	// System.Boolean UnityGLTF.Cache.RefCountedCacheData::_isDisposed
	bool ____isDisposed_0;
	// System.Int32 UnityGLTF.Cache.RefCountedCacheData::_refCount
	int32_t ____refCount_1;
	// System.Object UnityGLTF.Cache.RefCountedCacheData::_refCountLock
	RuntimeObject * ____refCountLock_2;
	// UnityGLTF.Cache.MeshCacheData[][] UnityGLTF.Cache.RefCountedCacheData::<MeshCache>k__BackingField
	MeshCacheDataU5BU5DU5BU5D_t835D67AD3D5E3890A3F216BA1A2B8F59729B4AD2* ___U3CMeshCacheU3Ek__BackingField_3;
	// UnityGLTF.Cache.MaterialCacheData[] UnityGLTF.Cache.RefCountedCacheData::<MaterialCache>k__BackingField
	MaterialCacheDataU5BU5D_t5B04037660F1E2FB1B1793AD1EDAED07BD71F773* ___U3CMaterialCacheU3Ek__BackingField_4;
	// UnityGLTF.Cache.TextureCacheData[] UnityGLTF.Cache.RefCountedCacheData::<TextureCache>k__BackingField
	TextureCacheDataU5BU5D_tAC566BD6C592D6CEB6B07945C6E9235F14C37E69* ___U3CTextureCacheU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of__isDisposed_0() { return static_cast<int32_t>(offsetof(RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313, ____isDisposed_0)); }
	inline bool get__isDisposed_0() const { return ____isDisposed_0; }
	inline bool* get_address_of__isDisposed_0() { return &____isDisposed_0; }
	inline void set__isDisposed_0(bool value)
	{
		____isDisposed_0 = value;
	}

	inline static int32_t get_offset_of__refCount_1() { return static_cast<int32_t>(offsetof(RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313, ____refCount_1)); }
	inline int32_t get__refCount_1() const { return ____refCount_1; }
	inline int32_t* get_address_of__refCount_1() { return &____refCount_1; }
	inline void set__refCount_1(int32_t value)
	{
		____refCount_1 = value;
	}

	inline static int32_t get_offset_of__refCountLock_2() { return static_cast<int32_t>(offsetof(RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313, ____refCountLock_2)); }
	inline RuntimeObject * get__refCountLock_2() const { return ____refCountLock_2; }
	inline RuntimeObject ** get_address_of__refCountLock_2() { return &____refCountLock_2; }
	inline void set__refCountLock_2(RuntimeObject * value)
	{
		____refCountLock_2 = value;
		Il2CppCodeGenWriteBarrier((&____refCountLock_2), value);
	}

	inline static int32_t get_offset_of_U3CMeshCacheU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313, ___U3CMeshCacheU3Ek__BackingField_3)); }
	inline MeshCacheDataU5BU5DU5BU5D_t835D67AD3D5E3890A3F216BA1A2B8F59729B4AD2* get_U3CMeshCacheU3Ek__BackingField_3() const { return ___U3CMeshCacheU3Ek__BackingField_3; }
	inline MeshCacheDataU5BU5DU5BU5D_t835D67AD3D5E3890A3F216BA1A2B8F59729B4AD2** get_address_of_U3CMeshCacheU3Ek__BackingField_3() { return &___U3CMeshCacheU3Ek__BackingField_3; }
	inline void set_U3CMeshCacheU3Ek__BackingField_3(MeshCacheDataU5BU5DU5BU5D_t835D67AD3D5E3890A3F216BA1A2B8F59729B4AD2* value)
	{
		___U3CMeshCacheU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMeshCacheU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CMaterialCacheU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313, ___U3CMaterialCacheU3Ek__BackingField_4)); }
	inline MaterialCacheDataU5BU5D_t5B04037660F1E2FB1B1793AD1EDAED07BD71F773* get_U3CMaterialCacheU3Ek__BackingField_4() const { return ___U3CMaterialCacheU3Ek__BackingField_4; }
	inline MaterialCacheDataU5BU5D_t5B04037660F1E2FB1B1793AD1EDAED07BD71F773** get_address_of_U3CMaterialCacheU3Ek__BackingField_4() { return &___U3CMaterialCacheU3Ek__BackingField_4; }
	inline void set_U3CMaterialCacheU3Ek__BackingField_4(MaterialCacheDataU5BU5D_t5B04037660F1E2FB1B1793AD1EDAED07BD71F773* value)
	{
		___U3CMaterialCacheU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaterialCacheU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CTextureCacheU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313, ___U3CTextureCacheU3Ek__BackingField_5)); }
	inline TextureCacheDataU5BU5D_tAC566BD6C592D6CEB6B07945C6E9235F14C37E69* get_U3CTextureCacheU3Ek__BackingField_5() const { return ___U3CTextureCacheU3Ek__BackingField_5; }
	inline TextureCacheDataU5BU5D_tAC566BD6C592D6CEB6B07945C6E9235F14C37E69** get_address_of_U3CTextureCacheU3Ek__BackingField_5() { return &___U3CTextureCacheU3Ek__BackingField_5; }
	inline void set_U3CTextureCacheU3Ek__BackingField_5(TextureCacheDataU5BU5D_tAC566BD6C592D6CEB6B07945C6E9235F14C37E69* value)
	{
		___U3CTextureCacheU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextureCacheU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFCOUNTEDCACHEDATA_T68F587D716850E3080A86210BDD8C96EFF7ED313_H
#ifndef TEXTURECACHEDATA_T7D959BB98FA30695BC896C7F667ACB97EA0A99CF_H
#define TEXTURECACHEDATA_T7D959BB98FA30695BC896C7F667ACB97EA0A99CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Cache.TextureCacheData
struct  TextureCacheData_t7D959BB98FA30695BC896C7F667ACB97EA0A99CF  : public RuntimeObject
{
public:
	// GLTF.Schema.GLTFTexture UnityGLTF.Cache.TextureCacheData::TextureDefinition
	GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 * ___TextureDefinition_0;
	// UnityEngine.Texture UnityGLTF.Cache.TextureCacheData::Texture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___Texture_1;

public:
	inline static int32_t get_offset_of_TextureDefinition_0() { return static_cast<int32_t>(offsetof(TextureCacheData_t7D959BB98FA30695BC896C7F667ACB97EA0A99CF, ___TextureDefinition_0)); }
	inline GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 * get_TextureDefinition_0() const { return ___TextureDefinition_0; }
	inline GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 ** get_address_of_TextureDefinition_0() { return &___TextureDefinition_0; }
	inline void set_TextureDefinition_0(GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 * value)
	{
		___TextureDefinition_0 = value;
		Il2CppCodeGenWriteBarrier((&___TextureDefinition_0), value);
	}

	inline static int32_t get_offset_of_Texture_1() { return static_cast<int32_t>(offsetof(TextureCacheData_t7D959BB98FA30695BC896C7F667ACB97EA0A99CF, ___Texture_1)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_Texture_1() const { return ___Texture_1; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_Texture_1() { return &___Texture_1; }
	inline void set_Texture_1(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___Texture_1 = value;
		Il2CppCodeGenWriteBarrier((&___Texture_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURECACHEDATA_T7D959BB98FA30695BC896C7F667ACB97EA0A99CF_H
#ifndef SIMPLEHTTPSERVER_T255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A_H
#define SIMPLEHTTPSERVER_T255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Examples.SimpleHTTPServer
struct  SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A  : public RuntimeObject
{
public:
	// System.String[] UnityGLTF.Examples.SimpleHTTPServer::_indexFiles
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____indexFiles_0;
	// System.Threading.Thread UnityGLTF.Examples.SimpleHTTPServer::_serverThread
	Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * ____serverThread_2;
	// System.String UnityGLTF.Examples.SimpleHTTPServer::_rootDirectory
	String_t* ____rootDirectory_3;
	// System.Net.HttpListener UnityGLTF.Examples.SimpleHTTPServer::_listener
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * ____listener_4;
	// System.Int32 UnityGLTF.Examples.SimpleHTTPServer::_port
	int32_t ____port_5;

public:
	inline static int32_t get_offset_of__indexFiles_0() { return static_cast<int32_t>(offsetof(SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A, ____indexFiles_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__indexFiles_0() const { return ____indexFiles_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__indexFiles_0() { return &____indexFiles_0; }
	inline void set__indexFiles_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____indexFiles_0 = value;
		Il2CppCodeGenWriteBarrier((&____indexFiles_0), value);
	}

	inline static int32_t get_offset_of__serverThread_2() { return static_cast<int32_t>(offsetof(SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A, ____serverThread_2)); }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * get__serverThread_2() const { return ____serverThread_2; }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 ** get_address_of__serverThread_2() { return &____serverThread_2; }
	inline void set__serverThread_2(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * value)
	{
		____serverThread_2 = value;
		Il2CppCodeGenWriteBarrier((&____serverThread_2), value);
	}

	inline static int32_t get_offset_of__rootDirectory_3() { return static_cast<int32_t>(offsetof(SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A, ____rootDirectory_3)); }
	inline String_t* get__rootDirectory_3() const { return ____rootDirectory_3; }
	inline String_t** get_address_of__rootDirectory_3() { return &____rootDirectory_3; }
	inline void set__rootDirectory_3(String_t* value)
	{
		____rootDirectory_3 = value;
		Il2CppCodeGenWriteBarrier((&____rootDirectory_3), value);
	}

	inline static int32_t get_offset_of__listener_4() { return static_cast<int32_t>(offsetof(SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A, ____listener_4)); }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * get__listener_4() const { return ____listener_4; }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E ** get_address_of__listener_4() { return &____listener_4; }
	inline void set__listener_4(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * value)
	{
		____listener_4 = value;
		Il2CppCodeGenWriteBarrier((&____listener_4), value);
	}

	inline static int32_t get_offset_of__port_5() { return static_cast<int32_t>(offsetof(SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A, ____port_5)); }
	inline int32_t get__port_5() const { return ____port_5; }
	inline int32_t* get_address_of__port_5() { return &____port_5; }
	inline void set__port_5(int32_t value)
	{
		____port_5 = value;
	}
};

struct SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A_StaticFields
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.String> UnityGLTF.Examples.SimpleHTTPServer::_mimeTypeMappings
	RuntimeObject* ____mimeTypeMappings_1;

public:
	inline static int32_t get_offset_of__mimeTypeMappings_1() { return static_cast<int32_t>(offsetof(SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A_StaticFields, ____mimeTypeMappings_1)); }
	inline RuntimeObject* get__mimeTypeMappings_1() const { return ____mimeTypeMappings_1; }
	inline RuntimeObject** get_address_of__mimeTypeMappings_1() { return &____mimeTypeMappings_1; }
	inline void set__mimeTypeMappings_1(RuntimeObject* value)
	{
		____mimeTypeMappings_1 = value;
		Il2CppCodeGenWriteBarrier((&____mimeTypeMappings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEHTTPSERVER_T255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A_H
#ifndef FILELOADER_T67D17B03BB535CB6735C669B564A5355BA21ADE6_H
#define FILELOADER_T67D17B03BB535CB6735C669B564A5355BA21ADE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Loader.FileLoader
struct  FileLoader_t67D17B03BB535CB6735C669B564A5355BA21ADE6  : public RuntimeObject
{
public:
	// System.String UnityGLTF.Loader.FileLoader::_rootDirectoryPath
	String_t* ____rootDirectoryPath_0;
	// System.IO.Stream UnityGLTF.Loader.FileLoader::<LoadedStream>k__BackingField
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___U3CLoadedStreamU3Ek__BackingField_1;
	// System.Boolean UnityGLTF.Loader.FileLoader::<HasSyncLoadMethod>k__BackingField
	bool ___U3CHasSyncLoadMethodU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__rootDirectoryPath_0() { return static_cast<int32_t>(offsetof(FileLoader_t67D17B03BB535CB6735C669B564A5355BA21ADE6, ____rootDirectoryPath_0)); }
	inline String_t* get__rootDirectoryPath_0() const { return ____rootDirectoryPath_0; }
	inline String_t** get_address_of__rootDirectoryPath_0() { return &____rootDirectoryPath_0; }
	inline void set__rootDirectoryPath_0(String_t* value)
	{
		____rootDirectoryPath_0 = value;
		Il2CppCodeGenWriteBarrier((&____rootDirectoryPath_0), value);
	}

	inline static int32_t get_offset_of_U3CLoadedStreamU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FileLoader_t67D17B03BB535CB6735C669B564A5355BA21ADE6, ___U3CLoadedStreamU3Ek__BackingField_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_U3CLoadedStreamU3Ek__BackingField_1() const { return ___U3CLoadedStreamU3Ek__BackingField_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_U3CLoadedStreamU3Ek__BackingField_1() { return &___U3CLoadedStreamU3Ek__BackingField_1; }
	inline void set_U3CLoadedStreamU3Ek__BackingField_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___U3CLoadedStreamU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadedStreamU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CHasSyncLoadMethodU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FileLoader_t67D17B03BB535CB6735C669B564A5355BA21ADE6, ___U3CHasSyncLoadMethodU3Ek__BackingField_2)); }
	inline bool get_U3CHasSyncLoadMethodU3Ek__BackingField_2() const { return ___U3CHasSyncLoadMethodU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CHasSyncLoadMethodU3Ek__BackingField_2() { return &___U3CHasSyncLoadMethodU3Ek__BackingField_2; }
	inline void set_U3CHasSyncLoadMethodU3Ek__BackingField_2(bool value)
	{
		___U3CHasSyncLoadMethodU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILELOADER_T67D17B03BB535CB6735C669B564A5355BA21ADE6_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T6E835C75DA5F3031633DE840D6459AB56787E0D9_H
#define U3CU3EC__DISPLAYCLASS11_0_T6E835C75DA5F3031633DE840D6459AB56787E0D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Loader.FileLoader/<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t6E835C75DA5F3031633DE840D6459AB56787E0D9  : public RuntimeObject
{
public:
	// UnityGLTF.Loader.FileLoader UnityGLTF.Loader.FileLoader/<>c__DisplayClass11_0::<>4__this
	FileLoader_t67D17B03BB535CB6735C669B564A5355BA21ADE6 * ___U3CU3E4__this_0;
	// System.String UnityGLTF.Loader.FileLoader/<>c__DisplayClass11_0::pathToLoad
	String_t* ___pathToLoad_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t6E835C75DA5F3031633DE840D6459AB56787E0D9, ___U3CU3E4__this_0)); }
	inline FileLoader_t67D17B03BB535CB6735C669B564A5355BA21ADE6 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline FileLoader_t67D17B03BB535CB6735C669B564A5355BA21ADE6 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(FileLoader_t67D17B03BB535CB6735C669B564A5355BA21ADE6 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_pathToLoad_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t6E835C75DA5F3031633DE840D6459AB56787E0D9, ___pathToLoad_1)); }
	inline String_t* get_pathToLoad_1() const { return ___pathToLoad_1; }
	inline String_t** get_address_of_pathToLoad_1() { return &___pathToLoad_1; }
	inline void set_pathToLoad_1(String_t* value)
	{
		___pathToLoad_1 = value;
		Il2CppCodeGenWriteBarrier((&___pathToLoad_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T6E835C75DA5F3031633DE840D6459AB56787E0D9_H
#ifndef WEBREQUESTLOADER_T09E04D05D3AEA5847FB8314469A2D4AC3E6234CB_H
#define WEBREQUESTLOADER_T09E04D05D3AEA5847FB8314469A2D4AC3E6234CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Loader.WebRequestLoader
struct  WebRequestLoader_t09E04D05D3AEA5847FB8314469A2D4AC3E6234CB  : public RuntimeObject
{
public:
	// System.IO.Stream UnityGLTF.Loader.WebRequestLoader::<LoadedStream>k__BackingField
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___U3CLoadedStreamU3Ek__BackingField_0;
	// System.Net.Http.HttpClient UnityGLTF.Loader.WebRequestLoader::httpClient
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7 * ___httpClient_1;
	// System.Uri UnityGLTF.Loader.WebRequestLoader::baseAddress
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___baseAddress_2;

public:
	inline static int32_t get_offset_of_U3CLoadedStreamU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WebRequestLoader_t09E04D05D3AEA5847FB8314469A2D4AC3E6234CB, ___U3CLoadedStreamU3Ek__BackingField_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_U3CLoadedStreamU3Ek__BackingField_0() const { return ___U3CLoadedStreamU3Ek__BackingField_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_U3CLoadedStreamU3Ek__BackingField_0() { return &___U3CLoadedStreamU3Ek__BackingField_0; }
	inline void set_U3CLoadedStreamU3Ek__BackingField_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___U3CLoadedStreamU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadedStreamU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_httpClient_1() { return static_cast<int32_t>(offsetof(WebRequestLoader_t09E04D05D3AEA5847FB8314469A2D4AC3E6234CB, ___httpClient_1)); }
	inline HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7 * get_httpClient_1() const { return ___httpClient_1; }
	inline HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7 ** get_address_of_httpClient_1() { return &___httpClient_1; }
	inline void set_httpClient_1(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7 * value)
	{
		___httpClient_1 = value;
		Il2CppCodeGenWriteBarrier((&___httpClient_1), value);
	}

	inline static int32_t get_offset_of_baseAddress_2() { return static_cast<int32_t>(offsetof(WebRequestLoader_t09E04D05D3AEA5847FB8314469A2D4AC3E6234CB, ___baseAddress_2)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_baseAddress_2() const { return ___baseAddress_2; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_baseAddress_2() { return &___baseAddress_2; }
	inline void set_baseAddress_2(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___baseAddress_2 = value;
		Il2CppCodeGenWriteBarrier((&___baseAddress_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTLOADER_T09E04D05D3AEA5847FB8314469A2D4AC3E6234CB_H
#ifndef LOG_TA0BCB64FBB15B914060CBCDCAC37D04ED3FBA372_H
#define LOG_TA0BCB64FBB15B914060CBCDCAC37D04ED3FBA372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wizcorp.Utils.Logger.Log
struct  Log_tA0BCB64FBB15B914060CBCDCAC37D04ED3FBA372  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Wizcorp.Utils.Logger.Service.ILogService> Wizcorp.Utils.Logger.Log::services
	List_1_t1BA564D048DB0BA822A5B24502CC2A0A59FF7DCC * ___services_1;

public:
	inline static int32_t get_offset_of_services_1() { return static_cast<int32_t>(offsetof(Log_tA0BCB64FBB15B914060CBCDCAC37D04ED3FBA372, ___services_1)); }
	inline List_1_t1BA564D048DB0BA822A5B24502CC2A0A59FF7DCC * get_services_1() const { return ___services_1; }
	inline List_1_t1BA564D048DB0BA822A5B24502CC2A0A59FF7DCC ** get_address_of_services_1() { return &___services_1; }
	inline void set_services_1(List_1_t1BA564D048DB0BA822A5B24502CC2A0A59FF7DCC * value)
	{
		___services_1 = value;
		Il2CppCodeGenWriteBarrier((&___services_1), value);
	}
};

struct Log_tA0BCB64FBB15B914060CBCDCAC37D04ED3FBA372_StaticFields
{
public:
	// Wizcorp.Utils.Logger.Log Wizcorp.Utils.Logger.Log::Instance
	Log_tA0BCB64FBB15B914060CBCDCAC37D04ED3FBA372 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(Log_tA0BCB64FBB15B914060CBCDCAC37D04ED3FBA372_StaticFields, ___Instance_0)); }
	inline Log_tA0BCB64FBB15B914060CBCDCAC37D04ED3FBA372 * get_Instance_0() const { return ___Instance_0; }
	inline Log_tA0BCB64FBB15B914060CBCDCAC37D04ED3FBA372 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(Log_tA0BCB64FBB15B914060CBCDCAC37D04ED3FBA372 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOG_TA0BCB64FBB15B914060CBCDCAC37D04ED3FBA372_H
#ifndef ASSETBUNDLELOADASSETOPERATION_T5A1401110A453E5BC738720B9A4E596235815463_H
#define ASSETBUNDLELOADASSETOPERATION_T5A1401110A453E5BC738720B9A4E596235815463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleLoadAssetOperation
struct  AssetBundleLoadAssetOperation_t5A1401110A453E5BC738720B9A4E596235815463  : public AssetBundleLoadOperation_t4E376230379EB84B5F4A6D7F957B791348E72141
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLELOADASSETOPERATION_T5A1401110A453E5BC738720B9A4E596235815463_H
#ifndef ASSETBUNDLELOADLEVELOPERATION_T65E30AD7A5E27BDE9FA80E81AA8A99E14C397DD4_H
#define ASSETBUNDLELOADLEVELOPERATION_T65E30AD7A5E27BDE9FA80E81AA8A99E14C397DD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleLoadLevelOperation
struct  AssetBundleLoadLevelOperation_t65E30AD7A5E27BDE9FA80E81AA8A99E14C397DD4  : public AssetBundleLoadOperation_t4E376230379EB84B5F4A6D7F957B791348E72141
{
public:
	// System.String AssetBundles.AssetBundleLoadLevelOperation::m_AssetBundleName
	String_t* ___m_AssetBundleName_0;
	// System.String AssetBundles.AssetBundleLoadLevelOperation::m_LevelName
	String_t* ___m_LevelName_1;
	// System.Boolean AssetBundles.AssetBundleLoadLevelOperation::m_IsAdditive
	bool ___m_IsAdditive_2;
	// System.String AssetBundles.AssetBundleLoadLevelOperation::m_DownloadingError
	String_t* ___m_DownloadingError_3;
	// UnityEngine.AsyncOperation AssetBundles.AssetBundleLoadLevelOperation::m_Request
	AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * ___m_Request_4;

public:
	inline static int32_t get_offset_of_m_AssetBundleName_0() { return static_cast<int32_t>(offsetof(AssetBundleLoadLevelOperation_t65E30AD7A5E27BDE9FA80E81AA8A99E14C397DD4, ___m_AssetBundleName_0)); }
	inline String_t* get_m_AssetBundleName_0() const { return ___m_AssetBundleName_0; }
	inline String_t** get_address_of_m_AssetBundleName_0() { return &___m_AssetBundleName_0; }
	inline void set_m_AssetBundleName_0(String_t* value)
	{
		___m_AssetBundleName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_AssetBundleName_0), value);
	}

	inline static int32_t get_offset_of_m_LevelName_1() { return static_cast<int32_t>(offsetof(AssetBundleLoadLevelOperation_t65E30AD7A5E27BDE9FA80E81AA8A99E14C397DD4, ___m_LevelName_1)); }
	inline String_t* get_m_LevelName_1() const { return ___m_LevelName_1; }
	inline String_t** get_address_of_m_LevelName_1() { return &___m_LevelName_1; }
	inline void set_m_LevelName_1(String_t* value)
	{
		___m_LevelName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LevelName_1), value);
	}

	inline static int32_t get_offset_of_m_IsAdditive_2() { return static_cast<int32_t>(offsetof(AssetBundleLoadLevelOperation_t65E30AD7A5E27BDE9FA80E81AA8A99E14C397DD4, ___m_IsAdditive_2)); }
	inline bool get_m_IsAdditive_2() const { return ___m_IsAdditive_2; }
	inline bool* get_address_of_m_IsAdditive_2() { return &___m_IsAdditive_2; }
	inline void set_m_IsAdditive_2(bool value)
	{
		___m_IsAdditive_2 = value;
	}

	inline static int32_t get_offset_of_m_DownloadingError_3() { return static_cast<int32_t>(offsetof(AssetBundleLoadLevelOperation_t65E30AD7A5E27BDE9FA80E81AA8A99E14C397DD4, ___m_DownloadingError_3)); }
	inline String_t* get_m_DownloadingError_3() const { return ___m_DownloadingError_3; }
	inline String_t** get_address_of_m_DownloadingError_3() { return &___m_DownloadingError_3; }
	inline void set_m_DownloadingError_3(String_t* value)
	{
		___m_DownloadingError_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_DownloadingError_3), value);
	}

	inline static int32_t get_offset_of_m_Request_4() { return static_cast<int32_t>(offsetof(AssetBundleLoadLevelOperation_t65E30AD7A5E27BDE9FA80E81AA8A99E14C397DD4, ___m_Request_4)); }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * get_m_Request_4() const { return ___m_Request_4; }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D ** get_address_of_m_Request_4() { return &___m_Request_4; }
	inline void set_m_Request_4(AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * value)
	{
		___m_Request_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Request_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLELOADLEVELOPERATION_T65E30AD7A5E27BDE9FA80E81AA8A99E14C397DD4_H
#ifndef VECTOR3_T23F8955612D53E66C78FE37F87B1C09E9D78B28C_H
#define VECTOR3_T23F8955612D53E66C78FE37F87B1C09E9D78B28C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Vector3
struct  Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C 
{
public:
	// System.Single GLTF.Math.Vector3::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_2;
	// System.Single GLTF.Math.Vector3::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_3;
	// System.Single GLTF.Math.Vector3::<Z>k__BackingField
	float ___U3CZU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C, ___U3CXU3Ek__BackingField_2)); }
	inline float get_U3CXU3Ek__BackingField_2() const { return ___U3CXU3Ek__BackingField_2; }
	inline float* get_address_of_U3CXU3Ek__BackingField_2() { return &___U3CXU3Ek__BackingField_2; }
	inline void set_U3CXU3Ek__BackingField_2(float value)
	{
		___U3CXU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C, ___U3CYU3Ek__BackingField_3)); }
	inline float get_U3CYU3Ek__BackingField_3() const { return ___U3CYU3Ek__BackingField_3; }
	inline float* get_address_of_U3CYU3Ek__BackingField_3() { return &___U3CYU3Ek__BackingField_3; }
	inline void set_U3CYU3Ek__BackingField_3(float value)
	{
		___U3CYU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CZU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C, ___U3CZU3Ek__BackingField_4)); }
	inline float get_U3CZU3Ek__BackingField_4() const { return ___U3CZU3Ek__BackingField_4; }
	inline float* get_address_of_U3CZU3Ek__BackingField_4() { return &___U3CZU3Ek__BackingField_4; }
	inline void set_U3CZU3Ek__BackingField_4(float value)
	{
		___U3CZU3Ek__BackingField_4 = value;
	}
};

struct Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C_StaticFields
{
public:
	// GLTF.Math.Vector3 GLTF.Math.Vector3::Zero
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___Zero_0;
	// GLTF.Math.Vector3 GLTF.Math.Vector3::One
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___One_1;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C_StaticFields, ___Zero_0)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_Zero_0() const { return ___Zero_0; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___Zero_0 = value;
	}

	inline static int32_t get_offset_of_One_1() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C_StaticFields, ___One_1)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_One_1() const { return ___One_1; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_One_1() { return &___One_1; }
	inline void set_One_1(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___One_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T23F8955612D53E66C78FE37F87B1C09E9D78B28C_H
#ifndef VECTOR4_T239657374664B132BBB44122F237F461D91809ED_H
#define VECTOR4_T239657374664B132BBB44122F237F461D91809ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Vector4
struct  Vector4_t239657374664B132BBB44122F237F461D91809ED 
{
public:
	// System.Single GLTF.Math.Vector4::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_0;
	// System.Single GLTF.Math.Vector4::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_1;
	// System.Single GLTF.Math.Vector4::<Z>k__BackingField
	float ___U3CZU3Ek__BackingField_2;
	// System.Single GLTF.Math.Vector4::<W>k__BackingField
	float ___U3CWU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Vector4_t239657374664B132BBB44122F237F461D91809ED, ___U3CXU3Ek__BackingField_0)); }
	inline float get_U3CXU3Ek__BackingField_0() const { return ___U3CXU3Ek__BackingField_0; }
	inline float* get_address_of_U3CXU3Ek__BackingField_0() { return &___U3CXU3Ek__BackingField_0; }
	inline void set_U3CXU3Ek__BackingField_0(float value)
	{
		___U3CXU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Vector4_t239657374664B132BBB44122F237F461D91809ED, ___U3CYU3Ek__BackingField_1)); }
	inline float get_U3CYU3Ek__BackingField_1() const { return ___U3CYU3Ek__BackingField_1; }
	inline float* get_address_of_U3CYU3Ek__BackingField_1() { return &___U3CYU3Ek__BackingField_1; }
	inline void set_U3CYU3Ek__BackingField_1(float value)
	{
		___U3CYU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CZU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vector4_t239657374664B132BBB44122F237F461D91809ED, ___U3CZU3Ek__BackingField_2)); }
	inline float get_U3CZU3Ek__BackingField_2() const { return ___U3CZU3Ek__BackingField_2; }
	inline float* get_address_of_U3CZU3Ek__BackingField_2() { return &___U3CZU3Ek__BackingField_2; }
	inline void set_U3CZU3Ek__BackingField_2(float value)
	{
		___U3CZU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CWU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Vector4_t239657374664B132BBB44122F237F461D91809ED, ___U3CWU3Ek__BackingField_3)); }
	inline float get_U3CWU3Ek__BackingField_3() const { return ___U3CWU3Ek__BackingField_3; }
	inline float* get_address_of_U3CWU3Ek__BackingField_3() { return &___U3CWU3Ek__BackingField_3; }
	inline void set_U3CWU3Ek__BackingField_3(float value)
	{
		___U3CWU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T239657374664B132BBB44122F237F461D91809ED_H
#ifndef CODEATTRIBUTE_T195605836B5236010C22EFADF825430811E28BF0_H
#define CODEATTRIBUTE_T195605836B5236010C22EFADF825430811E28BF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NatShareU.Docs.CodeAttribute
struct  CodeAttribute_t195605836B5236010C22EFADF825430811E28BF0  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEATTRIBUTE_T195605836B5236010C22EFADF825430811E28BF0_H
#ifndef DOCATTRIBUTE_T26C862396DB47D0D92299AFB0D372A1582779ABB_H
#define DOCATTRIBUTE_T26C862396DB47D0D92299AFB0D372A1582779ABB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NatShareU.Docs.DocAttribute
struct  DocAttribute_t26C862396DB47D0D92299AFB0D372A1582779ABB  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCATTRIBUTE_T26C862396DB47D0D92299AFB0D372A1582779ABB_H
#ifndef REFATTRIBUTE_T153A1C0B02D858F3AB42568E769F7E782112DE50_H
#define REFATTRIBUTE_T153A1C0B02D858F3AB42568E769F7E782112DE50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NatShareU.Docs.RefAttribute
struct  RefAttribute_t153A1C0B02D858F3AB42568E769F7E782112DE50  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFATTRIBUTE_T153A1C0B02D858F3AB42568E769F7E782112DE50_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#define ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_stateMachine_0), value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_defaultContextAction_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultContextAction_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
#endif // ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#ifndef TASKAWAITER_T0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_H
#define TASKAWAITER_T0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter
struct  TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F 
{
public:
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.TaskAwaiter::m_task
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F, ___m_task_0)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_m_task_0() const { return ___m_task_0; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_marshaled_pinvoke
{
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_marshaled_com
{
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;
};
#endif // TASKAWAITER_T0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_H
#ifndef TASKAWAITER_1_T0CD71BC02837B0D53252196D19DABDA73A615B86_H
#define TASKAWAITER_1_T0CD71BC02837B0D53252196D19DABDA73A615B86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter`1<System.Net.Http.HttpResponseMessage>
struct  TaskAwaiter_1_t0CD71BC02837B0D53252196D19DABDA73A615B86 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.TaskAwaiter`1::m_task
	Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_1_t0CD71BC02837B0D53252196D19DABDA73A615B86, ___m_task_0)); }
	inline Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKAWAITER_1_T0CD71BC02837B0D53252196D19DABDA73A615B86_H
#ifndef GCHANDLE_T39FAEE3EA592432C93B574A31DD83B87F1847DE3_H
#define GCHANDLE_T39FAEE3EA592432C93B574A31DD83B87F1847DE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T39FAEE3EA592432C93B574A31DD83B87F1847DE3_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef ASSETBUNDLELOADASSETOPERATIONFULL_T999E5BC7463088826907F2CEFB7FB10A35EA96A0_H
#define ASSETBUNDLELOADASSETOPERATIONFULL_T999E5BC7463088826907F2CEFB7FB10A35EA96A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleLoadAssetOperationFull
struct  AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0  : public AssetBundleLoadAssetOperation_t5A1401110A453E5BC738720B9A4E596235815463
{
public:
	// System.String AssetBundles.AssetBundleLoadAssetOperationFull::m_AssetBundleName
	String_t* ___m_AssetBundleName_0;
	// System.String AssetBundles.AssetBundleLoadAssetOperationFull::m_AssetName
	String_t* ___m_AssetName_1;
	// System.String AssetBundles.AssetBundleLoadAssetOperationFull::m_DownloadingError
	String_t* ___m_DownloadingError_2;
	// System.Type AssetBundles.AssetBundleLoadAssetOperationFull::m_Type
	Type_t * ___m_Type_3;
	// UnityEngine.AssetBundleRequest AssetBundles.AssetBundleLoadAssetOperationFull::m_Request
	AssetBundleRequest_tF8897443AF1FC6C3E04911FAA9EDAE89B0A0A962 * ___m_Request_4;

public:
	inline static int32_t get_offset_of_m_AssetBundleName_0() { return static_cast<int32_t>(offsetof(AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0, ___m_AssetBundleName_0)); }
	inline String_t* get_m_AssetBundleName_0() const { return ___m_AssetBundleName_0; }
	inline String_t** get_address_of_m_AssetBundleName_0() { return &___m_AssetBundleName_0; }
	inline void set_m_AssetBundleName_0(String_t* value)
	{
		___m_AssetBundleName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_AssetBundleName_0), value);
	}

	inline static int32_t get_offset_of_m_AssetName_1() { return static_cast<int32_t>(offsetof(AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0, ___m_AssetName_1)); }
	inline String_t* get_m_AssetName_1() const { return ___m_AssetName_1; }
	inline String_t** get_address_of_m_AssetName_1() { return &___m_AssetName_1; }
	inline void set_m_AssetName_1(String_t* value)
	{
		___m_AssetName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_AssetName_1), value);
	}

	inline static int32_t get_offset_of_m_DownloadingError_2() { return static_cast<int32_t>(offsetof(AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0, ___m_DownloadingError_2)); }
	inline String_t* get_m_DownloadingError_2() const { return ___m_DownloadingError_2; }
	inline String_t** get_address_of_m_DownloadingError_2() { return &___m_DownloadingError_2; }
	inline void set_m_DownloadingError_2(String_t* value)
	{
		___m_DownloadingError_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DownloadingError_2), value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0, ___m_Type_3)); }
	inline Type_t * get_m_Type_3() const { return ___m_Type_3; }
	inline Type_t ** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(Type_t * value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_3), value);
	}

	inline static int32_t get_offset_of_m_Request_4() { return static_cast<int32_t>(offsetof(AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0, ___m_Request_4)); }
	inline AssetBundleRequest_tF8897443AF1FC6C3E04911FAA9EDAE89B0A0A962 * get_m_Request_4() const { return ___m_Request_4; }
	inline AssetBundleRequest_tF8897443AF1FC6C3E04911FAA9EDAE89B0A0A962 ** get_address_of_m_Request_4() { return &___m_Request_4; }
	inline void set_m_Request_4(AssetBundleRequest_tF8897443AF1FC6C3E04911FAA9EDAE89B0A0A962 * value)
	{
		___m_Request_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Request_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLELOADASSETOPERATIONFULL_T999E5BC7463088826907F2CEFB7FB10A35EA96A0_H
#ifndef ASSETBUNDLELOADASSETOPERATIONSIMULATION_T7F7FFFC464147B063BF894A9FD38488F4FAEE540_H
#define ASSETBUNDLELOADASSETOPERATIONSIMULATION_T7F7FFFC464147B063BF894A9FD38488F4FAEE540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleLoadAssetOperationSimulation
struct  AssetBundleLoadAssetOperationSimulation_t7F7FFFC464147B063BF894A9FD38488F4FAEE540  : public AssetBundleLoadAssetOperation_t5A1401110A453E5BC738720B9A4E596235815463
{
public:
	// UnityEngine.Object AssetBundles.AssetBundleLoadAssetOperationSimulation::m_SimulatedObject
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___m_SimulatedObject_0;

public:
	inline static int32_t get_offset_of_m_SimulatedObject_0() { return static_cast<int32_t>(offsetof(AssetBundleLoadAssetOperationSimulation_t7F7FFFC464147B063BF894A9FD38488F4FAEE540, ___m_SimulatedObject_0)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_m_SimulatedObject_0() const { return ___m_SimulatedObject_0; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_m_SimulatedObject_0() { return &___m_SimulatedObject_0; }
	inline void set_m_SimulatedObject_0(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___m_SimulatedObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_SimulatedObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLELOADASSETOPERATIONSIMULATION_T7F7FFFC464147B063BF894A9FD38488F4FAEE540_H
#ifndef SCANNERSTATUS_T9AC239CF3598752D1294F8CFBB52C7C97C13691E_H
#define SCANNERSTATUS_T9AC239CF3598752D1294F8CFBB52C7C97C13691E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarcodeScanner.Scanner.ScannerStatus
struct  ScannerStatus_t9AC239CF3598752D1294F8CFBB52C7C97C13691E 
{
public:
	// System.Int32 BarcodeScanner.Scanner.ScannerStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScannerStatus_t9AC239CF3598752D1294F8CFBB52C7C97C13691E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANNERSTATUS_T9AC239CF3598752D1294F8CFBB52C7C97C13691E_H
#ifndef ALPHAMODE_T41015BEC6E2BB18C0A2688E6CF441F148BA768E6_H
#define ALPHAMODE_T41015BEC6E2BB18C0A2688E6CF441F148BA768E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AlphaMode
struct  AlphaMode_t41015BEC6E2BB18C0A2688E6CF441F148BA768E6 
{
public:
	// System.Int32 GLTF.Schema.AlphaMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AlphaMode_t41015BEC6E2BB18C0A2688E6CF441F148BA768E6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALPHAMODE_T41015BEC6E2BB18C0A2688E6CF441F148BA768E6_H
#ifndef INTERPOLATIONTYPE_T31C5CDC2D3A825B4A0287213B304384CC34CEB28_H
#define INTERPOLATIONTYPE_T31C5CDC2D3A825B4A0287213B304384CC34CEB28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.InterpolationType
struct  InterpolationType_t31C5CDC2D3A825B4A0287213B304384CC34CEB28 
{
public:
	// System.Int32 GLTF.Schema.InterpolationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InterpolationType_t31C5CDC2D3A825B4A0287213B304384CC34CEB28, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATIONTYPE_T31C5CDC2D3A825B4A0287213B304384CC34CEB28_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_TB47FCDA2F094FE229473C417AC822A75ABBC508C_H
#define U3CU3EC__DISPLAYCLASS6_0_TB47FCDA2F094FE229473C417AC822A75ABBC508C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NatShareU.Platforms.NatShareWebGL/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_tB47FCDA2F094FE229473C417AC822A75ABBC508C  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.GCHandle NatShareU.Platforms.NatShareWebGL/<>c__DisplayClass6_0::thumbnailHandle
	GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  ___thumbnailHandle_0;

public:
	inline static int32_t get_offset_of_thumbnailHandle_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tB47FCDA2F094FE229473C417AC822A75ABBC508C, ___thumbnailHandle_0)); }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  get_thumbnailHandle_0() const { return ___thumbnailHandle_0; }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 * get_address_of_thumbnailHandle_0() { return &___thumbnailHandle_0; }
	inline void set_thumbnailHandle_0(GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  value)
	{
		___thumbnailHandle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_TB47FCDA2F094FE229473C417AC822A75ABBC508C_H
#ifndef U3CGETTHUMBNAILU3ED__6_T4AA1BAB99FB936EC43A488990FCC1F4D83B36718_H
#define U3CGETTHUMBNAILU3ED__6_T4AA1BAB99FB936EC43A488990FCC1F4D83B36718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NatShareU.Platforms.NatShareWebGL/<GetThumbnail>d__6
struct  U3CGetThumbnailU3Ed__6_t4AA1BAB99FB936EC43A488990FCC1F4D83B36718  : public RuntimeObject
{
public:
	// System.Int32 NatShareU.Platforms.NatShareWebGL/<GetThumbnail>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NatShareU.Platforms.NatShareWebGL/<GetThumbnail>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Runtime.InteropServices.GCHandle NatShareU.Platforms.NatShareWebGL/<GetThumbnail>d__6::thumbnailHandle
	GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  ___thumbnailHandle_2;
	// NatShareU.Platforms.NatShareWebGL/ThumbnailDelegate NatShareU.Platforms.NatShareWebGL/<GetThumbnail>d__6::callbackObject
	ThumbnailDelegate_t407F2620090BC25C8932333B96C8498F1C76A716 * ___callbackObject_3;
	// NatShareU.Platforms.NatShareWebGL/<>c__DisplayClass6_0 NatShareU.Platforms.NatShareWebGL/<GetThumbnail>d__6::<>8__1
	U3CU3Ec__DisplayClass6_0_tB47FCDA2F094FE229473C417AC822A75ABBC508C * ___U3CU3E8__1_4;
	// System.Action`1<UnityEngine.Texture2D> NatShareU.Platforms.NatShareWebGL/<GetThumbnail>d__6::callback
	Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * ___callback_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetThumbnailU3Ed__6_t4AA1BAB99FB936EC43A488990FCC1F4D83B36718, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetThumbnailU3Ed__6_t4AA1BAB99FB936EC43A488990FCC1F4D83B36718, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_thumbnailHandle_2() { return static_cast<int32_t>(offsetof(U3CGetThumbnailU3Ed__6_t4AA1BAB99FB936EC43A488990FCC1F4D83B36718, ___thumbnailHandle_2)); }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  get_thumbnailHandle_2() const { return ___thumbnailHandle_2; }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 * get_address_of_thumbnailHandle_2() { return &___thumbnailHandle_2; }
	inline void set_thumbnailHandle_2(GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  value)
	{
		___thumbnailHandle_2 = value;
	}

	inline static int32_t get_offset_of_callbackObject_3() { return static_cast<int32_t>(offsetof(U3CGetThumbnailU3Ed__6_t4AA1BAB99FB936EC43A488990FCC1F4D83B36718, ___callbackObject_3)); }
	inline ThumbnailDelegate_t407F2620090BC25C8932333B96C8498F1C76A716 * get_callbackObject_3() const { return ___callbackObject_3; }
	inline ThumbnailDelegate_t407F2620090BC25C8932333B96C8498F1C76A716 ** get_address_of_callbackObject_3() { return &___callbackObject_3; }
	inline void set_callbackObject_3(ThumbnailDelegate_t407F2620090BC25C8932333B96C8498F1C76A716 * value)
	{
		___callbackObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___callbackObject_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_4() { return static_cast<int32_t>(offsetof(U3CGetThumbnailU3Ed__6_t4AA1BAB99FB936EC43A488990FCC1F4D83B36718, ___U3CU3E8__1_4)); }
	inline U3CU3Ec__DisplayClass6_0_tB47FCDA2F094FE229473C417AC822A75ABBC508C * get_U3CU3E8__1_4() const { return ___U3CU3E8__1_4; }
	inline U3CU3Ec__DisplayClass6_0_tB47FCDA2F094FE229473C417AC822A75ABBC508C ** get_address_of_U3CU3E8__1_4() { return &___U3CU3E8__1_4; }
	inline void set_U3CU3E8__1_4(U3CU3Ec__DisplayClass6_0_tB47FCDA2F094FE229473C417AC822A75ABBC508C * value)
	{
		___U3CU3E8__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CGetThumbnailU3Ed__6_t4AA1BAB99FB936EC43A488990FCC1F4D83B36718, ___callback_5)); }
	inline Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * get_callback_5() const { return ___callback_5; }
	inline Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETTHUMBNAILU3ED__6_T4AA1BAB99FB936EC43A488990FCC1F4D83B36718_H
#ifndef ANTIALIASINGLEVEL_TE5C5DB0035E55592FAF3559DD0221C0D31F385F7_H
#define ANTIALIASINGLEVEL_TE5C5DB0035E55592FAF3559DD0221C0D31F385F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureBase/AntiAliasingLevel
struct  AntiAliasingLevel_tE5C5DB0035E55592FAF3559DD0221C0D31F385F7 
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase/AntiAliasingLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AntiAliasingLevel_tE5C5DB0035E55592FAF3559DD0221C0D31F385F7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANTIALIASINGLEVEL_TE5C5DB0035E55592FAF3559DD0221C0D31F385F7_H
#ifndef CUBEMAPDEPTH_T4B2F58CBF8F0FD56A4FAB72521FB9497D3CF8DA2_H
#define CUBEMAPDEPTH_T4B2F58CBF8F0FD56A4FAB72521FB9497D3CF8DA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureBase/CubemapDepth
struct  CubemapDepth_t4B2F58CBF8F0FD56A4FAB72521FB9497D3CF8DA2 
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase/CubemapDepth::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CubemapDepth_t4B2F58CBF8F0FD56A4FAB72521FB9497D3CF8DA2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBEMAPDEPTH_T4B2F58CBF8F0FD56A4FAB72521FB9497D3CF8DA2_H
#ifndef CUBEMAPRESOLUTION_T33EB7201E196B88DEC1F1D654910F80883BDAFF5_H
#define CUBEMAPRESOLUTION_T33EB7201E196B88DEC1F1D654910F80883BDAFF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureBase/CubemapResolution
struct  CubemapResolution_t33EB7201E196B88DEC1F1D654910F80883BDAFF5 
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase/CubemapResolution::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CubemapResolution_t33EB7201E196B88DEC1F1D654910F80883BDAFF5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBEMAPRESOLUTION_T33EB7201E196B88DEC1F1D654910F80883BDAFF5_H
#ifndef DOWNSCALE_T0FA203D04AA8A2B141415F5CDE4B6C64FC9C8EE2_H
#define DOWNSCALE_T0FA203D04AA8A2B141415F5CDE4B6C64FC9C8EE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureBase/DownScale
struct  DownScale_t0FA203D04AA8A2B141415F5CDE4B6C64FC9C8EE2 
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase/DownScale::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DownScale_t0FA203D04AA8A2B141415F5CDE4B6C64FC9C8EE2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNSCALE_T0FA203D04AA8A2B141415F5CDE4B6C64FC9C8EE2_H
#ifndef FRAMERATE_TF456D79FE7ABFF53E6E1F0062D230523FE80B8ED_H
#define FRAMERATE_TF456D79FE7ABFF53E6E1F0062D230523FE80B8ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureBase/FrameRate
struct  FrameRate_tF456D79FE7ABFF53E6E1F0062D230523FE80B8ED 
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase/FrameRate::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FrameRate_tF456D79FE7ABFF53E6E1F0062D230523FE80B8ED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMERATE_TF456D79FE7ABFF53E6E1F0062D230523FE80B8ED_H
#ifndef OUTPUTEXTENSION_T564D5B605AF44511D757076642D75E8DE8A2AC62_H
#define OUTPUTEXTENSION_T564D5B605AF44511D757076642D75E8DE8A2AC62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureBase/OutputExtension
struct  OutputExtension_t564D5B605AF44511D757076642D75E8DE8A2AC62 
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase/OutputExtension::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OutputExtension_t564D5B605AF44511D757076642D75E8DE8A2AC62, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPUTEXTENSION_T564D5B605AF44511D757076642D75E8DE8A2AC62_H
#ifndef OUTPUTPATH_T349CC657290608E49919172C43113E04BB5FB92A_H
#define OUTPUTPATH_T349CC657290608E49919172C43113E04BB5FB92A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureBase/OutputPath
struct  OutputPath_t349CC657290608E49919172C43113E04BB5FB92A 
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase/OutputPath::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OutputPath_t349CC657290608E49919172C43113E04BB5FB92A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPUTPATH_T349CC657290608E49919172C43113E04BB5FB92A_H
#ifndef OUTPUTTYPE_T28F9E99ED1790FF6AEF268F541EA007E1E5B907A_H
#define OUTPUTTYPE_T28F9E99ED1790FF6AEF268F541EA007E1E5B907A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureBase/OutputType
struct  OutputType_t28F9E99ED1790FF6AEF268F541EA007E1E5B907A 
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase/OutputType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OutputType_t28F9E99ED1790FF6AEF268F541EA007E1E5B907A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPUTTYPE_T28F9E99ED1790FF6AEF268F541EA007E1E5B907A_H
#ifndef RESOLUTION_TC45EEF57F3A8B62D2D5B55E526EDF1921A327CED_H
#define RESOLUTION_TC45EEF57F3A8B62D2D5B55E526EDF1921A327CED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureBase/Resolution
struct  Resolution_tC45EEF57F3A8B62D2D5B55E526EDF1921A327CED 
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase/Resolution::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Resolution_tC45EEF57F3A8B62D2D5B55E526EDF1921A327CED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTION_TC45EEF57F3A8B62D2D5B55E526EDF1921A327CED_H
#ifndef PIXELFORMAT_T9C6FFBEE3E5AF45D60A375FB66926BD5922FB693_H
#define PIXELFORMAT_T9C6FFBEE3E5AF45D60A375FB66926BD5922FB693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.NativePlugin/PixelFormat
struct  PixelFormat_t9C6FFBEE3E5AF45D60A375FB66926BD5922FB693 
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.NativePlugin/PixelFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PixelFormat_t9C6FFBEE3E5AF45D60A375FB66926BD5922FB693, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXELFORMAT_T9C6FFBEE3E5AF45D60A375FB66926BD5922FB693_H
#ifndef PLUGINEVENT_TC1A4A889A972A7635EDED3226C603681CFD1A494_H
#define PLUGINEVENT_TC1A4A889A972A7635EDED3226C603681CFD1A494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.NativePlugin/PluginEvent
struct  PluginEvent_tC1A4A889A972A7635EDED3226C603681CFD1A494 
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.NativePlugin/PluginEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PluginEvent_tC1A4A889A972A7635EDED3226C603681CFD1A494, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLUGINEVENT_TC1A4A889A972A7635EDED3226C603681CFD1A494_H
#ifndef STEREOPACKING_T3AB63B2DABBF5E68A9F08C8BFC0920105D7278C3_H
#define STEREOPACKING_T3AB63B2DABBF5E68A9F08C8BFC0920105D7278C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.StereoPacking
struct  StereoPacking_t3AB63B2DABBF5E68A9F08C8BFC0920105D7278C3 
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.StereoPacking::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StereoPacking_t3AB63B2DABBF5E68A9F08C8BFC0920105D7278C3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREOPACKING_T3AB63B2DABBF5E68A9F08C8BFC0920105D7278C3_H
#ifndef STOPMODE_TC9B6C8D23F29AF22F2E101E21A94D6E12BA986B9_H
#define STOPMODE_TC9B6C8D23F29AF22F2E101E21A94D6E12BA986B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.StopMode
struct  StopMode_tC9B6C8D23F29AF22F2E101E21A94D6E12BA986B9 
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.StopMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StopMode_tC9B6C8D23F29AF22F2E101E21A94D6E12BA986B9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOPMODE_TC9B6C8D23F29AF22F2E101E21A94D6E12BA986B9_H
#ifndef ASYNCTASKMETHODBUILDER_1_T66ED1808B26B8081A2804D6A750D13386E360BD9_H
#define ASYNCTASKMETHODBUILDER_1_T66ED1808B26B8081A2804D6A750D13386E360BD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult>
struct  AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9, ___m_task_2)); }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T66ED1808B26B8081A2804D6A750D13386E360BD9_H
#ifndef ASYNCVOIDMETHODBUILDER_T44E3C9B52B019BB5BDCC0E1BB83188B536161CFF_H
#define ASYNCVOIDMETHODBUILDER_T44E3C9B52B019BB5BDCC0E1BB83188B536161CFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct  AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF 
{
public:
	// System.Threading.SynchronizationContext System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_synchronizationContext
	SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * ___m_synchronizationContext_0;
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_coreState
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  ___m_coreState_1;
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_task
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_synchronizationContext_0() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF, ___m_synchronizationContext_0)); }
	inline SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * get_m_synchronizationContext_0() const { return ___m_synchronizationContext_0; }
	inline SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 ** get_address_of_m_synchronizationContext_0() { return &___m_synchronizationContext_0; }
	inline void set_m_synchronizationContext_0(SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * value)
	{
		___m_synchronizationContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_synchronizationContext_0), value);
	}

	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF, ___m_task_2)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_m_task_2() const { return ___m_task_2; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF_marshaled_pinvoke
{
	SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_pinvoke ___m_coreState_1;
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_2;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF_marshaled_com
{
	SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_com ___m_coreState_1;
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_2;
};
#endif // ASYNCVOIDMETHODBUILDER_T44E3C9B52B019BB5BDCC0E1BB83188B536161CFF_H
#ifndef CAMERACLEARFLAGS_TAC22BD22D12708CBDC63F6CFB31109E5E17CF239_H
#define CAMERACLEARFLAGS_TAC22BD22D12708CBDC63F6CFB31109E5E17CF239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CameraClearFlags
struct  CameraClearFlags_tAC22BD22D12708CBDC63F6CFB31109E5E17CF239 
{
public:
	// System.Int32 UnityEngine.CameraClearFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraClearFlags_tAC22BD22D12708CBDC63F6CFB31109E5E17CF239, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACLEARFLAGS_TAC22BD22D12708CBDC63F6CFB31109E5E17CF239_H
#ifndef FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#define FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterMode_t6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RENDERTEXTUREFORMAT_T2AB1B77FBD247648292FBBE1182F12B5FC47AF85_H
#define RENDERTEXTUREFORMAT_T2AB1B77FBD247648292FBBE1182F12B5FC47AF85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t2AB1B77FBD247648292FBBE1182F12B5FC47AF85 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t2AB1B77FBD247648292FBBE1182F12B5FC47AF85, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T2AB1B77FBD247648292FBBE1182F12B5FC47AF85_H
#ifndef SCHEMAEXTENSIONS_TAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_H
#define SCHEMAEXTENSIONS_TAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Extensions.SchemaExtensions
struct  SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2  : public RuntimeObject
{
public:

public:
};

struct SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_StaticFields
{
public:
	// GLTF.Math.Vector3 UnityGLTF.Extensions.SchemaExtensions::CoordinateSpaceConversionScale
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___CoordinateSpaceConversionScale_0;
	// GLTF.Math.Vector4 UnityGLTF.Extensions.SchemaExtensions::TangentSpaceConversionScale
	Vector4_t239657374664B132BBB44122F237F461D91809ED  ___TangentSpaceConversionScale_1;

public:
	inline static int32_t get_offset_of_CoordinateSpaceConversionScale_0() { return static_cast<int32_t>(offsetof(SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_StaticFields, ___CoordinateSpaceConversionScale_0)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_CoordinateSpaceConversionScale_0() const { return ___CoordinateSpaceConversionScale_0; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_CoordinateSpaceConversionScale_0() { return &___CoordinateSpaceConversionScale_0; }
	inline void set_CoordinateSpaceConversionScale_0(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___CoordinateSpaceConversionScale_0 = value;
	}

	inline static int32_t get_offset_of_TangentSpaceConversionScale_1() { return static_cast<int32_t>(offsetof(SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_StaticFields, ___TangentSpaceConversionScale_1)); }
	inline Vector4_t239657374664B132BBB44122F237F461D91809ED  get_TangentSpaceConversionScale_1() const { return ___TangentSpaceConversionScale_1; }
	inline Vector4_t239657374664B132BBB44122F237F461D91809ED * get_address_of_TangentSpaceConversionScale_1() { return &___TangentSpaceConversionScale_1; }
	inline void set_TangentSpaceConversionScale_1(Vector4_t239657374664B132BBB44122F237F461D91809ED  value)
	{
		___TangentSpaceConversionScale_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMAEXTENSIONS_TAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_H
#ifndef LOGLEVEL_T16EBCC8E5519DEF124927B19C3FA125CD6C5F079_H
#define LOGLEVEL_T16EBCC8E5519DEF124927B19C3FA125CD6C5F079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wizcorp.Utils.Logger.LogLevel
struct  LogLevel_t16EBCC8E5519DEF124927B19C3FA125CD6C5F079 
{
public:
	// System.Int32 Wizcorp.Utils.Logger.LogLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogLevel_t16EBCC8E5519DEF124927B19C3FA125CD6C5F079, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_T16EBCC8E5519DEF124927B19C3FA125CD6C5F079_H
#ifndef SCANNER_T7EEB3BF43250D48CBD72CE0B36748443406D642D_H
#define SCANNER_T7EEB3BF43250D48CBD72CE0B36748443406D642D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarcodeScanner.Scanner.Scanner
struct  Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D  : public RuntimeObject
{
public:
	// System.EventHandler BarcodeScanner.Scanner.Scanner::OnReady
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___OnReady_0;
	// System.EventHandler BarcodeScanner.Scanner.Scanner::StatusChanged
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___StatusChanged_1;
	// BarcodeScanner.IWebcam BarcodeScanner.Scanner.Scanner::<Camera>k__BackingField
	RuntimeObject* ___U3CCameraU3Ek__BackingField_2;
	// BarcodeScanner.IParser BarcodeScanner.Scanner.Scanner::<Parser>k__BackingField
	RuntimeObject* ___U3CParserU3Ek__BackingField_3;
	// BarcodeScanner.ScannerSettings BarcodeScanner.Scanner.Scanner::<Settings>k__BackingField
	ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69 * ___U3CSettingsU3Ek__BackingField_4;
	// BarcodeScanner.Scanner.ScannerStatus BarcodeScanner.Scanner.Scanner::status
	int32_t ___status_5;
	// UnityEngine.Color32[] BarcodeScanner.Scanner.Scanner::pixels
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___pixels_6;
	// System.Action`2<System.String,System.String> BarcodeScanner.Scanner.Scanner::Callback
	Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * ___Callback_7;
	// BarcodeScanner.Parser.ParserResult BarcodeScanner.Scanner.Scanner::Result
	ParserResult_t92EB97AE2A35D224A2C3624D2A7BD88D61431239 * ___Result_8;
	// System.Boolean BarcodeScanner.Scanner.Scanner::parserPixelAvailable
	bool ___parserPixelAvailable_9;
	// System.Single BarcodeScanner.Scanner.Scanner::mainThreadLastDecode
	float ___mainThreadLastDecode_10;
	// System.Int32 BarcodeScanner.Scanner.Scanner::webcamFrameDelayed
	int32_t ___webcamFrameDelayed_11;
	// System.Int32 BarcodeScanner.Scanner.Scanner::webcamLastChecksum
	int32_t ___webcamLastChecksum_12;
	// System.Boolean BarcodeScanner.Scanner.Scanner::decodeInterrupted
	bool ___decodeInterrupted_13;
	// System.Threading.Thread BarcodeScanner.Scanner.Scanner::CodeScannerThread
	Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * ___CodeScannerThread_14;

public:
	inline static int32_t get_offset_of_OnReady_0() { return static_cast<int32_t>(offsetof(Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D, ___OnReady_0)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_OnReady_0() const { return ___OnReady_0; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_OnReady_0() { return &___OnReady_0; }
	inline void set_OnReady_0(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___OnReady_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnReady_0), value);
	}

	inline static int32_t get_offset_of_StatusChanged_1() { return static_cast<int32_t>(offsetof(Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D, ___StatusChanged_1)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_StatusChanged_1() const { return ___StatusChanged_1; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_StatusChanged_1() { return &___StatusChanged_1; }
	inline void set_StatusChanged_1(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___StatusChanged_1 = value;
		Il2CppCodeGenWriteBarrier((&___StatusChanged_1), value);
	}

	inline static int32_t get_offset_of_U3CCameraU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D, ___U3CCameraU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CCameraU3Ek__BackingField_2() const { return ___U3CCameraU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CCameraU3Ek__BackingField_2() { return &___U3CCameraU3Ek__BackingField_2; }
	inline void set_U3CCameraU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CCameraU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCameraU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CParserU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D, ___U3CParserU3Ek__BackingField_3)); }
	inline RuntimeObject* get_U3CParserU3Ek__BackingField_3() const { return ___U3CParserU3Ek__BackingField_3; }
	inline RuntimeObject** get_address_of_U3CParserU3Ek__BackingField_3() { return &___U3CParserU3Ek__BackingField_3; }
	inline void set_U3CParserU3Ek__BackingField_3(RuntimeObject* value)
	{
		___U3CParserU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParserU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CSettingsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D, ___U3CSettingsU3Ek__BackingField_4)); }
	inline ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69 * get_U3CSettingsU3Ek__BackingField_4() const { return ___U3CSettingsU3Ek__BackingField_4; }
	inline ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69 ** get_address_of_U3CSettingsU3Ek__BackingField_4() { return &___U3CSettingsU3Ek__BackingField_4; }
	inline void set_U3CSettingsU3Ek__BackingField_4(ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69 * value)
	{
		___U3CSettingsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSettingsU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_status_5() { return static_cast<int32_t>(offsetof(Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D, ___status_5)); }
	inline int32_t get_status_5() const { return ___status_5; }
	inline int32_t* get_address_of_status_5() { return &___status_5; }
	inline void set_status_5(int32_t value)
	{
		___status_5 = value;
	}

	inline static int32_t get_offset_of_pixels_6() { return static_cast<int32_t>(offsetof(Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D, ___pixels_6)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_pixels_6() const { return ___pixels_6; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_pixels_6() { return &___pixels_6; }
	inline void set_pixels_6(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___pixels_6 = value;
		Il2CppCodeGenWriteBarrier((&___pixels_6), value);
	}

	inline static int32_t get_offset_of_Callback_7() { return static_cast<int32_t>(offsetof(Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D, ___Callback_7)); }
	inline Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * get_Callback_7() const { return ___Callback_7; }
	inline Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 ** get_address_of_Callback_7() { return &___Callback_7; }
	inline void set_Callback_7(Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * value)
	{
		___Callback_7 = value;
		Il2CppCodeGenWriteBarrier((&___Callback_7), value);
	}

	inline static int32_t get_offset_of_Result_8() { return static_cast<int32_t>(offsetof(Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D, ___Result_8)); }
	inline ParserResult_t92EB97AE2A35D224A2C3624D2A7BD88D61431239 * get_Result_8() const { return ___Result_8; }
	inline ParserResult_t92EB97AE2A35D224A2C3624D2A7BD88D61431239 ** get_address_of_Result_8() { return &___Result_8; }
	inline void set_Result_8(ParserResult_t92EB97AE2A35D224A2C3624D2A7BD88D61431239 * value)
	{
		___Result_8 = value;
		Il2CppCodeGenWriteBarrier((&___Result_8), value);
	}

	inline static int32_t get_offset_of_parserPixelAvailable_9() { return static_cast<int32_t>(offsetof(Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D, ___parserPixelAvailable_9)); }
	inline bool get_parserPixelAvailable_9() const { return ___parserPixelAvailable_9; }
	inline bool* get_address_of_parserPixelAvailable_9() { return &___parserPixelAvailable_9; }
	inline void set_parserPixelAvailable_9(bool value)
	{
		___parserPixelAvailable_9 = value;
	}

	inline static int32_t get_offset_of_mainThreadLastDecode_10() { return static_cast<int32_t>(offsetof(Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D, ___mainThreadLastDecode_10)); }
	inline float get_mainThreadLastDecode_10() const { return ___mainThreadLastDecode_10; }
	inline float* get_address_of_mainThreadLastDecode_10() { return &___mainThreadLastDecode_10; }
	inline void set_mainThreadLastDecode_10(float value)
	{
		___mainThreadLastDecode_10 = value;
	}

	inline static int32_t get_offset_of_webcamFrameDelayed_11() { return static_cast<int32_t>(offsetof(Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D, ___webcamFrameDelayed_11)); }
	inline int32_t get_webcamFrameDelayed_11() const { return ___webcamFrameDelayed_11; }
	inline int32_t* get_address_of_webcamFrameDelayed_11() { return &___webcamFrameDelayed_11; }
	inline void set_webcamFrameDelayed_11(int32_t value)
	{
		___webcamFrameDelayed_11 = value;
	}

	inline static int32_t get_offset_of_webcamLastChecksum_12() { return static_cast<int32_t>(offsetof(Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D, ___webcamLastChecksum_12)); }
	inline int32_t get_webcamLastChecksum_12() const { return ___webcamLastChecksum_12; }
	inline int32_t* get_address_of_webcamLastChecksum_12() { return &___webcamLastChecksum_12; }
	inline void set_webcamLastChecksum_12(int32_t value)
	{
		___webcamLastChecksum_12 = value;
	}

	inline static int32_t get_offset_of_decodeInterrupted_13() { return static_cast<int32_t>(offsetof(Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D, ___decodeInterrupted_13)); }
	inline bool get_decodeInterrupted_13() const { return ___decodeInterrupted_13; }
	inline bool* get_address_of_decodeInterrupted_13() { return &___decodeInterrupted_13; }
	inline void set_decodeInterrupted_13(bool value)
	{
		___decodeInterrupted_13 = value;
	}

	inline static int32_t get_offset_of_CodeScannerThread_14() { return static_cast<int32_t>(offsetof(Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D, ___CodeScannerThread_14)); }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * get_CodeScannerThread_14() const { return ___CodeScannerThread_14; }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 ** get_address_of_CodeScannerThread_14() { return &___CodeScannerThread_14; }
	inline void set_CodeScannerThread_14(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * value)
	{
		___CodeScannerThread_14 = value;
		Il2CppCodeGenWriteBarrier((&___CodeScannerThread_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANNER_T7EEB3BF43250D48CBD72CE0B36748443406D642D_H
#ifndef SCANNERSETTINGS_TE6DCE89EE74AECF5500B1B02595F84F8E7505B69_H
#define SCANNERSETTINGS_TE6DCE89EE74AECF5500B1B02595F84F8E7505B69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarcodeScanner.ScannerSettings
struct  ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69  : public RuntimeObject
{
public:
	// System.Boolean BarcodeScanner.ScannerSettings::<ScannerBackgroundThread>k__BackingField
	bool ___U3CScannerBackgroundThreadU3Ek__BackingField_0;
	// System.Int32 BarcodeScanner.ScannerSettings::<ScannerDelayFrameMin>k__BackingField
	int32_t ___U3CScannerDelayFrameMinU3Ek__BackingField_1;
	// System.Single BarcodeScanner.ScannerSettings::<ScannerDecodeInterval>k__BackingField
	float ___U3CScannerDecodeIntervalU3Ek__BackingField_2;
	// System.Boolean BarcodeScanner.ScannerSettings::<ParserAutoRotate>k__BackingField
	bool ___U3CParserAutoRotateU3Ek__BackingField_3;
	// System.Boolean BarcodeScanner.ScannerSettings::<ParserTryInverted>k__BackingField
	bool ___U3CParserTryInvertedU3Ek__BackingField_4;
	// System.Boolean BarcodeScanner.ScannerSettings::<ParserTryHarder>k__BackingField
	bool ___U3CParserTryHarderU3Ek__BackingField_5;
	// System.String BarcodeScanner.ScannerSettings::<WebcamDefaultDeviceName>k__BackingField
	String_t* ___U3CWebcamDefaultDeviceNameU3Ek__BackingField_6;
	// System.Int32 BarcodeScanner.ScannerSettings::<WebcamRequestedWidth>k__BackingField
	int32_t ___U3CWebcamRequestedWidthU3Ek__BackingField_7;
	// System.Int32 BarcodeScanner.ScannerSettings::<WebcamRequestedHeight>k__BackingField
	int32_t ___U3CWebcamRequestedHeightU3Ek__BackingField_8;
	// UnityEngine.FilterMode BarcodeScanner.ScannerSettings::<WebcamFilterMode>k__BackingField
	int32_t ___U3CWebcamFilterModeU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CScannerBackgroundThreadU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69, ___U3CScannerBackgroundThreadU3Ek__BackingField_0)); }
	inline bool get_U3CScannerBackgroundThreadU3Ek__BackingField_0() const { return ___U3CScannerBackgroundThreadU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CScannerBackgroundThreadU3Ek__BackingField_0() { return &___U3CScannerBackgroundThreadU3Ek__BackingField_0; }
	inline void set_U3CScannerBackgroundThreadU3Ek__BackingField_0(bool value)
	{
		___U3CScannerBackgroundThreadU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CScannerDelayFrameMinU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69, ___U3CScannerDelayFrameMinU3Ek__BackingField_1)); }
	inline int32_t get_U3CScannerDelayFrameMinU3Ek__BackingField_1() const { return ___U3CScannerDelayFrameMinU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CScannerDelayFrameMinU3Ek__BackingField_1() { return &___U3CScannerDelayFrameMinU3Ek__BackingField_1; }
	inline void set_U3CScannerDelayFrameMinU3Ek__BackingField_1(int32_t value)
	{
		___U3CScannerDelayFrameMinU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CScannerDecodeIntervalU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69, ___U3CScannerDecodeIntervalU3Ek__BackingField_2)); }
	inline float get_U3CScannerDecodeIntervalU3Ek__BackingField_2() const { return ___U3CScannerDecodeIntervalU3Ek__BackingField_2; }
	inline float* get_address_of_U3CScannerDecodeIntervalU3Ek__BackingField_2() { return &___U3CScannerDecodeIntervalU3Ek__BackingField_2; }
	inline void set_U3CScannerDecodeIntervalU3Ek__BackingField_2(float value)
	{
		___U3CScannerDecodeIntervalU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CParserAutoRotateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69, ___U3CParserAutoRotateU3Ek__BackingField_3)); }
	inline bool get_U3CParserAutoRotateU3Ek__BackingField_3() const { return ___U3CParserAutoRotateU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CParserAutoRotateU3Ek__BackingField_3() { return &___U3CParserAutoRotateU3Ek__BackingField_3; }
	inline void set_U3CParserAutoRotateU3Ek__BackingField_3(bool value)
	{
		___U3CParserAutoRotateU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CParserTryInvertedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69, ___U3CParserTryInvertedU3Ek__BackingField_4)); }
	inline bool get_U3CParserTryInvertedU3Ek__BackingField_4() const { return ___U3CParserTryInvertedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CParserTryInvertedU3Ek__BackingField_4() { return &___U3CParserTryInvertedU3Ek__BackingField_4; }
	inline void set_U3CParserTryInvertedU3Ek__BackingField_4(bool value)
	{
		___U3CParserTryInvertedU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CParserTryHarderU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69, ___U3CParserTryHarderU3Ek__BackingField_5)); }
	inline bool get_U3CParserTryHarderU3Ek__BackingField_5() const { return ___U3CParserTryHarderU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CParserTryHarderU3Ek__BackingField_5() { return &___U3CParserTryHarderU3Ek__BackingField_5; }
	inline void set_U3CParserTryHarderU3Ek__BackingField_5(bool value)
	{
		___U3CParserTryHarderU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CWebcamDefaultDeviceNameU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69, ___U3CWebcamDefaultDeviceNameU3Ek__BackingField_6)); }
	inline String_t* get_U3CWebcamDefaultDeviceNameU3Ek__BackingField_6() const { return ___U3CWebcamDefaultDeviceNameU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CWebcamDefaultDeviceNameU3Ek__BackingField_6() { return &___U3CWebcamDefaultDeviceNameU3Ek__BackingField_6; }
	inline void set_U3CWebcamDefaultDeviceNameU3Ek__BackingField_6(String_t* value)
	{
		___U3CWebcamDefaultDeviceNameU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWebcamDefaultDeviceNameU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CWebcamRequestedWidthU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69, ___U3CWebcamRequestedWidthU3Ek__BackingField_7)); }
	inline int32_t get_U3CWebcamRequestedWidthU3Ek__BackingField_7() const { return ___U3CWebcamRequestedWidthU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CWebcamRequestedWidthU3Ek__BackingField_7() { return &___U3CWebcamRequestedWidthU3Ek__BackingField_7; }
	inline void set_U3CWebcamRequestedWidthU3Ek__BackingField_7(int32_t value)
	{
		___U3CWebcamRequestedWidthU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CWebcamRequestedHeightU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69, ___U3CWebcamRequestedHeightU3Ek__BackingField_8)); }
	inline int32_t get_U3CWebcamRequestedHeightU3Ek__BackingField_8() const { return ___U3CWebcamRequestedHeightU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CWebcamRequestedHeightU3Ek__BackingField_8() { return &___U3CWebcamRequestedHeightU3Ek__BackingField_8; }
	inline void set_U3CWebcamRequestedHeightU3Ek__BackingField_8(int32_t value)
	{
		___U3CWebcamRequestedHeightU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CWebcamFilterModeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69, ___U3CWebcamFilterModeU3Ek__BackingField_9)); }
	inline int32_t get_U3CWebcamFilterModeU3Ek__BackingField_9() const { return ___U3CWebcamFilterModeU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CWebcamFilterModeU3Ek__BackingField_9() { return &___U3CWebcamFilterModeU3Ek__BackingField_9; }
	inline void set_U3CWebcamFilterModeU3Ek__BackingField_9(int32_t value)
	{
		___U3CWebcamFilterModeU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANNERSETTINGS_TE6DCE89EE74AECF5500B1B02595F84F8E7505B69_H
#ifndef SETTINGS_T6192A73E99B86F492E75C148531AF0BD42C0560E_H
#define SETTINGS_T6192A73E99B86F492E75C148531AF0BD42C0560E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS/Settings
struct  Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E  : public RuntimeObject
{
public:
	// UnityEngine.Camera RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS/Settings::camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___camera_0;
	// System.Single RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS/Settings::ipd
	float ___ipd_1;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS/Settings::pixelSliceSize
	int32_t ___pixelSliceSize_2;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS/Settings::paddingSize
	int32_t ___paddingSize_3;
	// UnityEngine.CameraClearFlags RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS/Settings::cameraClearMode
	int32_t ___cameraClearMode_4;
	// UnityEngine.Color RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS/Settings::cameraClearColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___cameraClearColor_5;
	// UnityEngine.Behaviour[] RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS/Settings::cameraImageEffects
	BehaviourU5BU5D_tB5A90D63D29342B8A4C5C3777683F6B9217C4317* ___cameraImageEffects_6;

public:
	inline static int32_t get_offset_of_camera_0() { return static_cast<int32_t>(offsetof(Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E, ___camera_0)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_camera_0() const { return ___camera_0; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_camera_0() { return &___camera_0; }
	inline void set_camera_0(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___camera_0), value);
	}

	inline static int32_t get_offset_of_ipd_1() { return static_cast<int32_t>(offsetof(Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E, ___ipd_1)); }
	inline float get_ipd_1() const { return ___ipd_1; }
	inline float* get_address_of_ipd_1() { return &___ipd_1; }
	inline void set_ipd_1(float value)
	{
		___ipd_1 = value;
	}

	inline static int32_t get_offset_of_pixelSliceSize_2() { return static_cast<int32_t>(offsetof(Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E, ___pixelSliceSize_2)); }
	inline int32_t get_pixelSliceSize_2() const { return ___pixelSliceSize_2; }
	inline int32_t* get_address_of_pixelSliceSize_2() { return &___pixelSliceSize_2; }
	inline void set_pixelSliceSize_2(int32_t value)
	{
		___pixelSliceSize_2 = value;
	}

	inline static int32_t get_offset_of_paddingSize_3() { return static_cast<int32_t>(offsetof(Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E, ___paddingSize_3)); }
	inline int32_t get_paddingSize_3() const { return ___paddingSize_3; }
	inline int32_t* get_address_of_paddingSize_3() { return &___paddingSize_3; }
	inline void set_paddingSize_3(int32_t value)
	{
		___paddingSize_3 = value;
	}

	inline static int32_t get_offset_of_cameraClearMode_4() { return static_cast<int32_t>(offsetof(Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E, ___cameraClearMode_4)); }
	inline int32_t get_cameraClearMode_4() const { return ___cameraClearMode_4; }
	inline int32_t* get_address_of_cameraClearMode_4() { return &___cameraClearMode_4; }
	inline void set_cameraClearMode_4(int32_t value)
	{
		___cameraClearMode_4 = value;
	}

	inline static int32_t get_offset_of_cameraClearColor_5() { return static_cast<int32_t>(offsetof(Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E, ___cameraClearColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_cameraClearColor_5() const { return ___cameraClearColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_cameraClearColor_5() { return &___cameraClearColor_5; }
	inline void set_cameraClearColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___cameraClearColor_5 = value;
	}

	inline static int32_t get_offset_of_cameraImageEffects_6() { return static_cast<int32_t>(offsetof(Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E, ___cameraImageEffects_6)); }
	inline BehaviourU5BU5D_tB5A90D63D29342B8A4C5C3777683F6B9217C4317* get_cameraImageEffects_6() const { return ___cameraImageEffects_6; }
	inline BehaviourU5BU5D_tB5A90D63D29342B8A4C5C3777683F6B9217C4317** get_address_of_cameraImageEffects_6() { return &___cameraImageEffects_6; }
	inline void set_cameraImageEffects_6(BehaviourU5BU5D_tB5A90D63D29342B8A4C5C3777683F6B9217C4317* value)
	{
		___cameraImageEffects_6 = value;
		Il2CppCodeGenWriteBarrier((&___cameraImageEffects_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T6192A73E99B86F492E75C148531AF0BD42C0560E_H
#ifndef ASYNCTASKMETHODBUILDER_T0CD1893D670405BED201BE8CA6F2E811F2C0F487_H
#define ASYNCTASKMETHODBUILDER_T0CD1893D670405BED201BE8CA6F2E811F2C0F487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct  AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 
{
public:
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::m_builder
	AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  ___m_builder_1;

public:
	inline static int32_t get_offset_of_m_builder_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487, ___m_builder_1)); }
	inline AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  get_m_builder_1() const { return ___m_builder_1; }
	inline AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9 * get_address_of_m_builder_1() { return &___m_builder_1; }
	inline void set_m_builder_1(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  value)
	{
		___m_builder_1 = value;
	}
};

struct AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::s_cachedCompleted
	Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * ___s_cachedCompleted_0;

public:
	inline static int32_t get_offset_of_s_cachedCompleted_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_StaticFields, ___s_cachedCompleted_0)); }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * get_s_cachedCompleted_0() const { return ___s_cachedCompleted_0; }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 ** get_address_of_s_cachedCompleted_0() { return &___s_cachedCompleted_0; }
	inline void set_s_cachedCompleted_0(Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * value)
	{
		___s_cachedCompleted_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_cachedCompleted_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_marshaled_pinvoke
{
	AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  ___m_builder_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_marshaled_com
{
	AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  ___m_builder_1;
};
#endif // ASYNCTASKMETHODBUILDER_T0CD1893D670405BED201BE8CA6F2E811F2C0F487_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef ANIMATIONSAMPLERCACHEDATA_TC0014F4E8B8B7E9E1442D8A0BBBDE38DAFE339D9_H
#define ANIMATIONSAMPLERCACHEDATA_TC0014F4E8B8B7E9E1442D8A0BBBDE38DAFE339D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Cache.AnimationSamplerCacheData
struct  AnimationSamplerCacheData_tC0014F4E8B8B7E9E1442D8A0BBBDE38DAFE339D9 
{
public:
	// GLTF.AttributeAccessor UnityGLTF.Cache.AnimationSamplerCacheData::Input
	AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C * ___Input_0;
	// GLTF.AttributeAccessor UnityGLTF.Cache.AnimationSamplerCacheData::Output
	AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C * ___Output_1;
	// GLTF.Schema.InterpolationType UnityGLTF.Cache.AnimationSamplerCacheData::Interpolation
	int32_t ___Interpolation_2;

public:
	inline static int32_t get_offset_of_Input_0() { return static_cast<int32_t>(offsetof(AnimationSamplerCacheData_tC0014F4E8B8B7E9E1442D8A0BBBDE38DAFE339D9, ___Input_0)); }
	inline AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C * get_Input_0() const { return ___Input_0; }
	inline AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C ** get_address_of_Input_0() { return &___Input_0; }
	inline void set_Input_0(AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C * value)
	{
		___Input_0 = value;
		Il2CppCodeGenWriteBarrier((&___Input_0), value);
	}

	inline static int32_t get_offset_of_Output_1() { return static_cast<int32_t>(offsetof(AnimationSamplerCacheData_tC0014F4E8B8B7E9E1442D8A0BBBDE38DAFE339D9, ___Output_1)); }
	inline AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C * get_Output_1() const { return ___Output_1; }
	inline AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C ** get_address_of_Output_1() { return &___Output_1; }
	inline void set_Output_1(AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C * value)
	{
		___Output_1 = value;
		Il2CppCodeGenWriteBarrier((&___Output_1), value);
	}

	inline static int32_t get_offset_of_Interpolation_2() { return static_cast<int32_t>(offsetof(AnimationSamplerCacheData_tC0014F4E8B8B7E9E1442D8A0BBBDE38DAFE339D9, ___Interpolation_2)); }
	inline int32_t get_Interpolation_2() const { return ___Interpolation_2; }
	inline int32_t* get_address_of_Interpolation_2() { return &___Interpolation_2; }
	inline void set_Interpolation_2(int32_t value)
	{
		___Interpolation_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityGLTF.Cache.AnimationSamplerCacheData
struct AnimationSamplerCacheData_tC0014F4E8B8B7E9E1442D8A0BBBDE38DAFE339D9_marshaled_pinvoke
{
	AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C * ___Input_0;
	AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C * ___Output_1;
	int32_t ___Interpolation_2;
};
// Native definition for COM marshalling of UnityGLTF.Cache.AnimationSamplerCacheData
struct AnimationSamplerCacheData_tC0014F4E8B8B7E9E1442D8A0BBBDE38DAFE339D9_marshaled_com
{
	AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C * ___Input_0;
	AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C * ___Output_1;
	int32_t ___Interpolation_2;
};
#endif // ANIMATIONSAMPLERCACHEDATA_TC0014F4E8B8B7E9E1442D8A0BBBDE38DAFE339D9_H
#ifndef U3CLOADSCENEU3ED__8_TEB650CADA6924E88ADA570F99B9138D00CA509FA_H
#define U3CLOADSCENEU3ED__8_TEB650CADA6924E88ADA570F99B9138D00CA509FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Examples.MultiSceneComponent/<LoadScene>d__8
struct  U3CLoadSceneU3Ed__8_tEB650CADA6924E88ADA570F99B9138D00CA509FA 
{
public:
	// System.Int32 UnityGLTF.Examples.MultiSceneComponent/<LoadScene>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncVoidMethodBuilder UnityGLTF.Examples.MultiSceneComponent/<LoadScene>d__8::<>t__builder
	AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF  ___U3CU3Et__builder_1;
	// UnityGLTF.Examples.MultiSceneComponent UnityGLTF.Examples.MultiSceneComponent/<LoadScene>d__8::<>4__this
	MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A * ___U3CU3E4__this_2;
	// System.Int32 UnityGLTF.Examples.MultiSceneComponent/<LoadScene>d__8::SceneIndex
	int32_t ___SceneIndex_3;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.Examples.MultiSceneComponent/<LoadScene>d__8::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__8_tEB650CADA6924E88ADA570F99B9138D00CA509FA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__8_tEB650CADA6924E88ADA570F99B9138D00CA509FA, ___U3CU3Et__builder_1)); }
	inline AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__8_tEB650CADA6924E88ADA570F99B9138D00CA509FA, ___U3CU3E4__this_2)); }
	inline MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_SceneIndex_3() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__8_tEB650CADA6924E88ADA570F99B9138D00CA509FA, ___SceneIndex_3)); }
	inline int32_t get_SceneIndex_3() const { return ___SceneIndex_3; }
	inline int32_t* get_address_of_SceneIndex_3() { return &___SceneIndex_3; }
	inline void set_SceneIndex_3(int32_t value)
	{
		___SceneIndex_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_4() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__8_tEB650CADA6924E88ADA570F99B9138D00CA509FA, ___U3CU3Eu__1_4)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_4() const { return ___U3CU3Eu__1_4; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_4() { return &___U3CU3Eu__1_4; }
	inline void set_U3CU3Eu__1_4(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCENEU3ED__8_TEB650CADA6924E88ADA570F99B9138D00CA509FA_H
#ifndef STANDARDMAP_T21748F03AFF649249878667EEC817DE852AEEB69_H
#define STANDARDMAP_T21748F03AFF649249878667EEC817DE852AEEB69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.StandardMap
struct  StandardMap_t21748F03AFF649249878667EEC817DE852AEEB69  : public RuntimeObject
{
public:
	// UnityEngine.Material UnityGLTF.StandardMap::_material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____material_0;
	// GLTF.Schema.AlphaMode UnityGLTF.StandardMap::_alphaMode
	int32_t ____alphaMode_1;
	// System.Double UnityGLTF.StandardMap::_alphaCutoff
	double ____alphaCutoff_2;

public:
	inline static int32_t get_offset_of__material_0() { return static_cast<int32_t>(offsetof(StandardMap_t21748F03AFF649249878667EEC817DE852AEEB69, ____material_0)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__material_0() const { return ____material_0; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__material_0() { return &____material_0; }
	inline void set__material_0(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____material_0 = value;
		Il2CppCodeGenWriteBarrier((&____material_0), value);
	}

	inline static int32_t get_offset_of__alphaMode_1() { return static_cast<int32_t>(offsetof(StandardMap_t21748F03AFF649249878667EEC817DE852AEEB69, ____alphaMode_1)); }
	inline int32_t get__alphaMode_1() const { return ____alphaMode_1; }
	inline int32_t* get_address_of__alphaMode_1() { return &____alphaMode_1; }
	inline void set__alphaMode_1(int32_t value)
	{
		____alphaMode_1 = value;
	}

	inline static int32_t get_offset_of__alphaCutoff_2() { return static_cast<int32_t>(offsetof(StandardMap_t21748F03AFF649249878667EEC817DE852AEEB69, ____alphaCutoff_2)); }
	inline double get__alphaCutoff_2() const { return ____alphaCutoff_2; }
	inline double* get_address_of__alphaCutoff_2() { return &____alphaCutoff_2; }
	inline void set__alphaCutoff_2(double value)
	{
		____alphaCutoff_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDMAP_T21748F03AFF649249878667EEC817DE852AEEB69_H
#ifndef CONSOLESERVICE_T04B92B015881DE18D734818F7A6F50412805D0A5_H
#define CONSOLESERVICE_T04B92B015881DE18D734818F7A6F50412805D0A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wizcorp.Utils.Logger.Service.ConsoleService
struct  ConsoleService_t04B92B015881DE18D734818F7A6F50412805D0A5  : public RuntimeObject
{
public:
	// Wizcorp.Utils.Logger.LogLevel Wizcorp.Utils.Logger.Service.ConsoleService::minLevel
	int32_t ___minLevel_0;

public:
	inline static int32_t get_offset_of_minLevel_0() { return static_cast<int32_t>(offsetof(ConsoleService_t04B92B015881DE18D734818F7A6F50412805D0A5, ___minLevel_0)); }
	inline int32_t get_minLevel_0() const { return ___minLevel_0; }
	inline int32_t* get_address_of_minLevel_0() { return &___minLevel_0; }
	inline void set_minLevel_0(int32_t value)
	{
		___minLevel_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSOLESERVICE_T04B92B015881DE18D734818F7A6F50412805D0A5_H
#ifndef FILESERVICE_TF81532934EAC45C16137B3C6E869BAB4AE444921_H
#define FILESERVICE_TF81532934EAC45C16137B3C6E869BAB4AE444921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wizcorp.Utils.Logger.Service.FileService
struct  FileService_tF81532934EAC45C16137B3C6E869BAB4AE444921  : public RuntimeObject
{
public:
	// Wizcorp.Utils.Logger.LogLevel Wizcorp.Utils.Logger.Service.FileService::minLevel
	int32_t ___minLevel_0;
	// System.String Wizcorp.Utils.Logger.Service.FileService::pathLog
	String_t* ___pathLog_1;

public:
	inline static int32_t get_offset_of_minLevel_0() { return static_cast<int32_t>(offsetof(FileService_tF81532934EAC45C16137B3C6E869BAB4AE444921, ___minLevel_0)); }
	inline int32_t get_minLevel_0() const { return ___minLevel_0; }
	inline int32_t* get_address_of_minLevel_0() { return &___minLevel_0; }
	inline void set_minLevel_0(int32_t value)
	{
		___minLevel_0 = value;
	}

	inline static int32_t get_offset_of_pathLog_1() { return static_cast<int32_t>(offsetof(FileService_tF81532934EAC45C16137B3C6E869BAB4AE444921, ___pathLog_1)); }
	inline String_t* get_pathLog_1() const { return ___pathLog_1; }
	inline String_t** get_address_of_pathLog_1() { return &___pathLog_1; }
	inline void set_pathLog_1(String_t* value)
	{
		___pathLog_1 = value;
		Il2CppCodeGenWriteBarrier((&___pathLog_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESERVICE_TF81532934EAC45C16137B3C6E869BAB4AE444921_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef U3CCONSTRUCTTEXTUREU3ED__82_T16404C09FA6C0B2619FBC744338CC75FF1AC9EC7_H
#define U3CCONSTRUCTTEXTUREU3ED__82_T16404C09FA6C0B2619FBC744338CC75FF1AC9EC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<ConstructTexture>d__82
struct  U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructTexture>d__82::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<ConstructTexture>d__82::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<ConstructTexture>d__82::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructTexture>d__82::textureIndex
	int32_t ___textureIndex_3;
	// GLTF.Schema.GLTFTexture UnityGLTF.GLTFSceneImporter/<ConstructTexture>d__82::texture
	GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 * ___texture_4;
	// System.Boolean UnityGLTF.GLTFSceneImporter/<ConstructTexture>d__82::markGpuOnly
	bool ___markGpuOnly_5;
	// System.Boolean UnityGLTF.GLTFSceneImporter/<ConstructTexture>d__82::isLinear
	bool ___isLinear_6;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructTexture>d__82::<sourceId>5__2
	int32_t ___U3CsourceIdU3E5__2_7;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<ConstructTexture>d__82::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_textureIndex_3() { return static_cast<int32_t>(offsetof(U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7, ___textureIndex_3)); }
	inline int32_t get_textureIndex_3() const { return ___textureIndex_3; }
	inline int32_t* get_address_of_textureIndex_3() { return &___textureIndex_3; }
	inline void set_textureIndex_3(int32_t value)
	{
		___textureIndex_3 = value;
	}

	inline static int32_t get_offset_of_texture_4() { return static_cast<int32_t>(offsetof(U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7, ___texture_4)); }
	inline GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 * get_texture_4() const { return ___texture_4; }
	inline GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 ** get_address_of_texture_4() { return &___texture_4; }
	inline void set_texture_4(GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 * value)
	{
		___texture_4 = value;
		Il2CppCodeGenWriteBarrier((&___texture_4), value);
	}

	inline static int32_t get_offset_of_markGpuOnly_5() { return static_cast<int32_t>(offsetof(U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7, ___markGpuOnly_5)); }
	inline bool get_markGpuOnly_5() const { return ___markGpuOnly_5; }
	inline bool* get_address_of_markGpuOnly_5() { return &___markGpuOnly_5; }
	inline void set_markGpuOnly_5(bool value)
	{
		___markGpuOnly_5 = value;
	}

	inline static int32_t get_offset_of_isLinear_6() { return static_cast<int32_t>(offsetof(U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7, ___isLinear_6)); }
	inline bool get_isLinear_6() const { return ___isLinear_6; }
	inline bool* get_address_of_isLinear_6() { return &___isLinear_6; }
	inline void set_isLinear_6(bool value)
	{
		___isLinear_6 = value;
	}

	inline static int32_t get_offset_of_U3CsourceIdU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7, ___U3CsourceIdU3E5__2_7)); }
	inline int32_t get_U3CsourceIdU3E5__2_7() const { return ___U3CsourceIdU3E5__2_7; }
	inline int32_t* get_address_of_U3CsourceIdU3E5__2_7() { return &___U3CsourceIdU3E5__2_7; }
	inline void set_U3CsourceIdU3E5__2_7(int32_t value)
	{
		___U3CsourceIdU3E5__2_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_8() { return static_cast<int32_t>(offsetof(U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7, ___U3CU3Eu__1_8)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_8() const { return ___U3CU3Eu__1_8; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_8() { return &___U3CU3Eu__1_8; }
	inline void set_U3CU3Eu__1_8(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONSTRUCTTEXTUREU3ED__82_T16404C09FA6C0B2619FBC744338CC75FF1AC9EC7_H
#ifndef U3CLOADTEXTUREASYNCU3ED__80_T179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128_H
#define U3CLOADTEXTUREASYNCU3ED__80_T179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<LoadTextureAsync>d__80
struct  U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<LoadTextureAsync>d__80::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<LoadTextureAsync>d__80::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<LoadTextureAsync>d__80::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// GLTF.Schema.GLTFTexture UnityGLTF.GLTFSceneImporter/<LoadTextureAsync>d__80::texture
	GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 * ___texture_3;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<LoadTextureAsync>d__80::textureIndex
	int32_t ___textureIndex_4;
	// System.Boolean UnityGLTF.GLTFSceneImporter/<LoadTextureAsync>d__80::markGpuOnly
	bool ___markGpuOnly_5;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<LoadTextureAsync>d__80::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_texture_3() { return static_cast<int32_t>(offsetof(U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128, ___texture_3)); }
	inline GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 * get_texture_3() const { return ___texture_3; }
	inline GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 ** get_address_of_texture_3() { return &___texture_3; }
	inline void set_texture_3(GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 * value)
	{
		___texture_3 = value;
		Il2CppCodeGenWriteBarrier((&___texture_3), value);
	}

	inline static int32_t get_offset_of_textureIndex_4() { return static_cast<int32_t>(offsetof(U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128, ___textureIndex_4)); }
	inline int32_t get_textureIndex_4() const { return ___textureIndex_4; }
	inline int32_t* get_address_of_textureIndex_4() { return &___textureIndex_4; }
	inline void set_textureIndex_4(int32_t value)
	{
		___textureIndex_4 = value;
	}

	inline static int32_t get_offset_of_markGpuOnly_5() { return static_cast<int32_t>(offsetof(U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128, ___markGpuOnly_5)); }
	inline bool get_markGpuOnly_5() const { return ___markGpuOnly_5; }
	inline bool* get_address_of_markGpuOnly_5() { return &___markGpuOnly_5; }
	inline void set_markGpuOnly_5(bool value)
	{
		___markGpuOnly_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_6() { return static_cast<int32_t>(offsetof(U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128, ___U3CU3Eu__1_6)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_6() const { return ___U3CU3Eu__1_6; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_6() { return &___U3CU3Eu__1_6; }
	inline void set_U3CU3Eu__1_6(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADTEXTUREASYNCU3ED__80_T179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128_H
#ifndef U3CLOADSTREAMU3ED__9_T9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE_H
#define U3CLOADSTREAMU3ED__9_T9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Loader.WebRequestLoader/<LoadStream>d__9
struct  U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE 
{
public:
	// System.Int32 UnityGLTF.Loader.WebRequestLoader/<LoadStream>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.Loader.WebRequestLoader/<LoadStream>d__9::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// System.String UnityGLTF.Loader.WebRequestLoader/<LoadStream>d__9::gltfFilePath
	String_t* ___gltfFilePath_2;
	// UnityGLTF.Loader.WebRequestLoader UnityGLTF.Loader.WebRequestLoader/<LoadStream>d__9::<>4__this
	WebRequestLoader_t09E04D05D3AEA5847FB8314469A2D4AC3E6234CB * ___U3CU3E4__this_3;
	// System.Net.Http.HttpResponseMessage UnityGLTF.Loader.WebRequestLoader/<LoadStream>d__9::<response>5__2
	HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904 * ___U3CresponseU3E5__2_4;
	// System.Runtime.CompilerServices.TaskAwaiter`1<System.Net.Http.HttpResponseMessage> UnityGLTF.Loader.WebRequestLoader/<LoadStream>d__9::<>u__1
	TaskAwaiter_1_t0CD71BC02837B0D53252196D19DABDA73A615B86  ___U3CU3Eu__1_5;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.Loader.WebRequestLoader/<LoadStream>d__9::<>u__2
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_gltfFilePath_2() { return static_cast<int32_t>(offsetof(U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE, ___gltfFilePath_2)); }
	inline String_t* get_gltfFilePath_2() const { return ___gltfFilePath_2; }
	inline String_t** get_address_of_gltfFilePath_2() { return &___gltfFilePath_2; }
	inline void set_gltfFilePath_2(String_t* value)
	{
		___gltfFilePath_2 = value;
		Il2CppCodeGenWriteBarrier((&___gltfFilePath_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE, ___U3CU3E4__this_3)); }
	inline WebRequestLoader_t09E04D05D3AEA5847FB8314469A2D4AC3E6234CB * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline WebRequestLoader_t09E04D05D3AEA5847FB8314469A2D4AC3E6234CB ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(WebRequestLoader_t09E04D05D3AEA5847FB8314469A2D4AC3E6234CB * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CresponseU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE, ___U3CresponseU3E5__2_4)); }
	inline HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904 * get_U3CresponseU3E5__2_4() const { return ___U3CresponseU3E5__2_4; }
	inline HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904 ** get_address_of_U3CresponseU3E5__2_4() { return &___U3CresponseU3E5__2_4; }
	inline void set_U3CresponseU3E5__2_4(HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904 * value)
	{
		___U3CresponseU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresponseU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_1_t0CD71BC02837B0D53252196D19DABDA73A615B86  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_1_t0CD71BC02837B0D53252196D19DABDA73A615B86 * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_1_t0CD71BC02837B0D53252196D19DABDA73A615B86  value)
	{
		___U3CU3Eu__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__2_6() { return static_cast<int32_t>(offsetof(U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE, ___U3CU3Eu__2_6)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__2_6() const { return ___U3CU3Eu__2_6; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__2_6() { return &___U3CU3Eu__2_6; }
	inline void set_U3CU3Eu__2_6(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__2_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSTREAMU3ED__9_T9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE_H
#ifndef METALROUGH2STANDARDMAP_T888CB2F5E48EAC8F465DC34E32B2ABDF5EE51DA5_H
#define METALROUGH2STANDARDMAP_T888CB2F5E48EAC8F465DC34E32B2ABDF5EE51DA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.MetalRough2StandardMap
struct  MetalRough2StandardMap_t888CB2F5E48EAC8F465DC34E32B2ABDF5EE51DA5  : public StandardMap_t21748F03AFF649249878667EEC817DE852AEEB69
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METALROUGH2STANDARDMAP_T888CB2F5E48EAC8F465DC34E32B2ABDF5EE51DA5_H
#ifndef SPECGLOSS2STANDARDMAP_TCA41C38F51292ED96B810DDCE84269AE7E566F9F_H
#define SPECGLOSS2STANDARDMAP_TCA41C38F51292ED96B810DDCE84269AE7E566F9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.SpecGloss2StandardMap
struct  SpecGloss2StandardMap_tCA41C38F51292ED96B810DDCE84269AE7E566F9F  : public StandardMap_t21748F03AFF649249878667EEC817DE852AEEB69
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECGLOSS2STANDARDMAP_TCA41C38F51292ED96B810DDCE84269AE7E566F9F_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef METALROUGHMAP_TD33910BDB1A6B5425ABAE3634D6CD7A2445AE0E6_H
#define METALROUGHMAP_TD33910BDB1A6B5425ABAE3634D6CD7A2445AE0E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.MetalRoughMap
struct  MetalRoughMap_tD33910BDB1A6B5425ABAE3634D6CD7A2445AE0E6  : public MetalRough2StandardMap_t888CB2F5E48EAC8F465DC34E32B2ABDF5EE51DA5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METALROUGHMAP_TD33910BDB1A6B5425ABAE3634D6CD7A2445AE0E6_H
#ifndef SPECGLOSSMAP_T078BF490CDB90B40DB9795C5858AACBE2CB0209A_H
#define SPECGLOSSMAP_T078BF490CDB90B40DB9795C5858AACBE2CB0209A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.SpecGlossMap
struct  SpecGlossMap_t078BF490CDB90B40DB9795C5858AACBE2CB0209A  : public SpecGloss2StandardMap_tCA41C38F51292ED96B810DDCE84269AE7E566F9F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECGLOSSMAP_T078BF490CDB90B40DB9795C5858AACBE2CB0209A_H
#ifndef THUMBNAILDELEGATE_T407F2620090BC25C8932333B96C8498F1C76A716_H
#define THUMBNAILDELEGATE_T407F2620090BC25C8932333B96C8498F1C76A716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NatShareU.Platforms.NatShareWebGL/ThumbnailDelegate
struct  ThumbnailDelegate_t407F2620090BC25C8932333B96C8498F1C76A716  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THUMBNAILDELEGATE_T407F2620090BC25C8932333B96C8498F1C76A716_H
#ifndef CAPTUREBASE_T4AA58B983F72B37942873222FDA0DFAEAE76C110_H
#define CAPTUREBASE_T4AA58B983F72B37942873222FDA0DFAEAE76C110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureBase
struct  CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.KeyCode RenderHeads.Media.AVProMovieCapture.CaptureBase::_captureKey
	int32_t ____captureKey_4;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_captureOnStart
	bool ____captureOnStart_5;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_startPaused
	bool ____startPaused_6;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_listVideoCodecsOnStart
	bool ____listVideoCodecsOnStart_7;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_isRealTime
	bool ____isRealTime_8;
	// RenderHeads.Media.AVProMovieCapture.StopMode RenderHeads.Media.AVProMovieCapture.CaptureBase::_stopMode
	int32_t ____stopMode_9;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_stopFrames
	int32_t ____stopFrames_10;
	// System.Single RenderHeads.Media.AVProMovieCapture.CaptureBase::_stopSeconds
	float ____stopSeconds_11;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_useMediaFoundationH264
	bool ____useMediaFoundationH264_12;
	// System.String[] RenderHeads.Media.AVProMovieCapture.CaptureBase::_videoCodecPriority
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____videoCodecPriority_13;
	// RenderHeads.Media.AVProMovieCapture.CaptureBase/FrameRate RenderHeads.Media.AVProMovieCapture.CaptureBase::_frameRate
	int32_t ____frameRate_14;
	// RenderHeads.Media.AVProMovieCapture.CaptureBase/DownScale RenderHeads.Media.AVProMovieCapture.CaptureBase::_downScale
	int32_t ____downScale_15;
	// UnityEngine.Vector2 RenderHeads.Media.AVProMovieCapture.CaptureBase::_maxVideoSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____maxVideoSize_16;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_forceVideoCodecIndex
	int32_t ____forceVideoCodecIndex_17;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_flipVertically
	bool ____flipVertically_18;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_supportAlpha
	bool ____supportAlpha_19;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_noAudio
	bool ____noAudio_20;
	// System.String[] RenderHeads.Media.AVProMovieCapture.CaptureBase::_audioCodecPriority
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____audioCodecPriority_21;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_forceAudioCodecIndex
	int32_t ____forceAudioCodecIndex_22;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_forceAudioDeviceIndex
	int32_t ____forceAudioDeviceIndex_23;
	// RenderHeads.Media.AVProMovieCapture.UnityAudioCapture RenderHeads.Media.AVProMovieCapture.CaptureBase::_audioCapture
	UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5 * ____audioCapture_24;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_autoGenerateFilename
	bool ____autoGenerateFilename_25;
	// RenderHeads.Media.AVProMovieCapture.CaptureBase/OutputPath RenderHeads.Media.AVProMovieCapture.CaptureBase::_outputFolderType
	int32_t ____outputFolderType_26;
	// System.String RenderHeads.Media.AVProMovieCapture.CaptureBase::_outputFolderPath
	String_t* ____outputFolderPath_27;
	// System.String RenderHeads.Media.AVProMovieCapture.CaptureBase::_autoFilenamePrefix
	String_t* ____autoFilenamePrefix_28;
	// System.String RenderHeads.Media.AVProMovieCapture.CaptureBase::_autoFilenameExtension
	String_t* ____autoFilenameExtension_29;
	// System.String RenderHeads.Media.AVProMovieCapture.CaptureBase::_forceFilename
	String_t* ____forceFilename_30;
	// RenderHeads.Media.AVProMovieCapture.CaptureBase/OutputType RenderHeads.Media.AVProMovieCapture.CaptureBase::_outputType
	int32_t ____outputType_31;
	// RenderHeads.Media.AVProMovieCapture.CaptureBase/Resolution RenderHeads.Media.AVProMovieCapture.CaptureBase::_renderResolution
	int32_t ____renderResolution_32;
	// UnityEngine.Vector2 RenderHeads.Media.AVProMovieCapture.CaptureBase::_renderSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____renderSize_33;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_renderAntiAliasing
	int32_t ____renderAntiAliasing_34;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_useMotionBlur
	bool ____useMotionBlur_35;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_motionBlurSamples
	int32_t ____motionBlurSamples_36;
	// UnityEngine.Camera[] RenderHeads.Media.AVProMovieCapture.CaptureBase::_motionBlurCameras
	CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9* ____motionBlurCameras_37;
	// RenderHeads.Media.AVProMovieCapture.MotionBlur RenderHeads.Media.AVProMovieCapture.CaptureBase::_motionBlur
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7 * ____motionBlur_38;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_allowVSyncDisable
	bool ____allowVSyncDisable_39;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_supportTextureRecreate
	bool ____supportTextureRecreate_40;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_captureMouseCursor
	bool ____captureMouseCursor_41;
	// RenderHeads.Media.AVProMovieCapture.MouseCursor RenderHeads.Media.AVProMovieCapture.CaptureBase::_mouseCursor
	MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD * ____mouseCursor_42;
	// System.String RenderHeads.Media.AVProMovieCapture.CaptureBase::_codecName
	String_t* ____codecName_43;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_codecIndex
	int32_t ____codecIndex_44;
	// System.String RenderHeads.Media.AVProMovieCapture.CaptureBase::_audioCodecName
	String_t* ____audioCodecName_45;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_audioCodecIndex
	int32_t ____audioCodecIndex_46;
	// System.String RenderHeads.Media.AVProMovieCapture.CaptureBase::_audioDeviceName
	String_t* ____audioDeviceName_47;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_audioDeviceIndex
	int32_t ____audioDeviceIndex_48;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_unityAudioSampleRate
	int32_t ____unityAudioSampleRate_49;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_unityAudioChannelCount
	int32_t ____unityAudioChannelCount_50;
	// UnityEngine.Texture2D RenderHeads.Media.AVProMovieCapture.CaptureBase::_texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ____texture_51;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_handle
	int32_t ____handle_52;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_targetWidth
	int32_t ____targetWidth_53;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_targetHeight
	int32_t ____targetHeight_54;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_capturing
	bool ____capturing_55;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_paused
	bool ____paused_56;
	// System.String RenderHeads.Media.AVProMovieCapture.CaptureBase::_filePath
	String_t* ____filePath_57;
	// System.IO.FileInfo RenderHeads.Media.AVProMovieCapture.CaptureBase::_fileInfo
	FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * ____fileInfo_58;
	// RenderHeads.Media.AVProMovieCapture.NativePlugin/PixelFormat RenderHeads.Media.AVProMovieCapture.CaptureBase::_pixelFormat
	int32_t ____pixelFormat_59;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_oldVSyncCount
	int32_t ____oldVSyncCount_60;
	// System.Single RenderHeads.Media.AVProMovieCapture.CaptureBase::_oldFixedDeltaTime
	float ____oldFixedDeltaTime_61;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_isTopDown
	bool ____isTopDown_62;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_isDirectX11
	bool ____isDirectX11_63;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_queuedStartCapture
	bool ____queuedStartCapture_64;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureBase::_queuedStopCapture
	bool ____queuedStopCapture_65;
	// System.Single RenderHeads.Media.AVProMovieCapture.CaptureBase::_captureStartTime
	float ____captureStartTime_66;
	// System.Single RenderHeads.Media.AVProMovieCapture.CaptureBase::_timeSinceLastFrame
	float ____timeSinceLastFrame_67;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_minimumDiskSpaceMB
	int32_t ____minimumDiskSpaceMB_68;
	// System.Int64 RenderHeads.Media.AVProMovieCapture.CaptureBase::_freeDiskSpaceMB
	int64_t ____freeDiskSpaceMB_69;
	// System.UInt32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_numDroppedFrames
	uint32_t ____numDroppedFrames_70;
	// System.UInt32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_numDroppedEncoderFrames
	uint32_t ____numDroppedEncoderFrames_71;
	// System.UInt32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_numEncodedFrames
	uint32_t ____numEncodedFrames_72;
	// System.UInt32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_totalEncodedSeconds
	uint32_t ____totalEncodedSeconds_73;
	// System.IntPtr RenderHeads.Media.AVProMovieCapture.CaptureBase::_renderEventFunction
	intptr_t ____renderEventFunction_74;
	// System.IntPtr RenderHeads.Media.AVProMovieCapture.CaptureBase::_freeEventFunction
	intptr_t ____freeEventFunction_75;
	// System.Single RenderHeads.Media.AVProMovieCapture.CaptureBase::_fps
	float ____fps_76;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_frameTotal
	int32_t ____frameTotal_77;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureBase::_frameCount
	int32_t ____frameCount_78;
	// System.Single RenderHeads.Media.AVProMovieCapture.CaptureBase::_startFrameTime
	float ____startFrameTime_79;

public:
	inline static int32_t get_offset_of__captureKey_4() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____captureKey_4)); }
	inline int32_t get__captureKey_4() const { return ____captureKey_4; }
	inline int32_t* get_address_of__captureKey_4() { return &____captureKey_4; }
	inline void set__captureKey_4(int32_t value)
	{
		____captureKey_4 = value;
	}

	inline static int32_t get_offset_of__captureOnStart_5() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____captureOnStart_5)); }
	inline bool get__captureOnStart_5() const { return ____captureOnStart_5; }
	inline bool* get_address_of__captureOnStart_5() { return &____captureOnStart_5; }
	inline void set__captureOnStart_5(bool value)
	{
		____captureOnStart_5 = value;
	}

	inline static int32_t get_offset_of__startPaused_6() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____startPaused_6)); }
	inline bool get__startPaused_6() const { return ____startPaused_6; }
	inline bool* get_address_of__startPaused_6() { return &____startPaused_6; }
	inline void set__startPaused_6(bool value)
	{
		____startPaused_6 = value;
	}

	inline static int32_t get_offset_of__listVideoCodecsOnStart_7() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____listVideoCodecsOnStart_7)); }
	inline bool get__listVideoCodecsOnStart_7() const { return ____listVideoCodecsOnStart_7; }
	inline bool* get_address_of__listVideoCodecsOnStart_7() { return &____listVideoCodecsOnStart_7; }
	inline void set__listVideoCodecsOnStart_7(bool value)
	{
		____listVideoCodecsOnStart_7 = value;
	}

	inline static int32_t get_offset_of__isRealTime_8() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____isRealTime_8)); }
	inline bool get__isRealTime_8() const { return ____isRealTime_8; }
	inline bool* get_address_of__isRealTime_8() { return &____isRealTime_8; }
	inline void set__isRealTime_8(bool value)
	{
		____isRealTime_8 = value;
	}

	inline static int32_t get_offset_of__stopMode_9() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____stopMode_9)); }
	inline int32_t get__stopMode_9() const { return ____stopMode_9; }
	inline int32_t* get_address_of__stopMode_9() { return &____stopMode_9; }
	inline void set__stopMode_9(int32_t value)
	{
		____stopMode_9 = value;
	}

	inline static int32_t get_offset_of__stopFrames_10() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____stopFrames_10)); }
	inline int32_t get__stopFrames_10() const { return ____stopFrames_10; }
	inline int32_t* get_address_of__stopFrames_10() { return &____stopFrames_10; }
	inline void set__stopFrames_10(int32_t value)
	{
		____stopFrames_10 = value;
	}

	inline static int32_t get_offset_of__stopSeconds_11() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____stopSeconds_11)); }
	inline float get__stopSeconds_11() const { return ____stopSeconds_11; }
	inline float* get_address_of__stopSeconds_11() { return &____stopSeconds_11; }
	inline void set__stopSeconds_11(float value)
	{
		____stopSeconds_11 = value;
	}

	inline static int32_t get_offset_of__useMediaFoundationH264_12() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____useMediaFoundationH264_12)); }
	inline bool get__useMediaFoundationH264_12() const { return ____useMediaFoundationH264_12; }
	inline bool* get_address_of__useMediaFoundationH264_12() { return &____useMediaFoundationH264_12; }
	inline void set__useMediaFoundationH264_12(bool value)
	{
		____useMediaFoundationH264_12 = value;
	}

	inline static int32_t get_offset_of__videoCodecPriority_13() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____videoCodecPriority_13)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__videoCodecPriority_13() const { return ____videoCodecPriority_13; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__videoCodecPriority_13() { return &____videoCodecPriority_13; }
	inline void set__videoCodecPriority_13(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____videoCodecPriority_13 = value;
		Il2CppCodeGenWriteBarrier((&____videoCodecPriority_13), value);
	}

	inline static int32_t get_offset_of__frameRate_14() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____frameRate_14)); }
	inline int32_t get__frameRate_14() const { return ____frameRate_14; }
	inline int32_t* get_address_of__frameRate_14() { return &____frameRate_14; }
	inline void set__frameRate_14(int32_t value)
	{
		____frameRate_14 = value;
	}

	inline static int32_t get_offset_of__downScale_15() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____downScale_15)); }
	inline int32_t get__downScale_15() const { return ____downScale_15; }
	inline int32_t* get_address_of__downScale_15() { return &____downScale_15; }
	inline void set__downScale_15(int32_t value)
	{
		____downScale_15 = value;
	}

	inline static int32_t get_offset_of__maxVideoSize_16() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____maxVideoSize_16)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__maxVideoSize_16() const { return ____maxVideoSize_16; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__maxVideoSize_16() { return &____maxVideoSize_16; }
	inline void set__maxVideoSize_16(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____maxVideoSize_16 = value;
	}

	inline static int32_t get_offset_of__forceVideoCodecIndex_17() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____forceVideoCodecIndex_17)); }
	inline int32_t get__forceVideoCodecIndex_17() const { return ____forceVideoCodecIndex_17; }
	inline int32_t* get_address_of__forceVideoCodecIndex_17() { return &____forceVideoCodecIndex_17; }
	inline void set__forceVideoCodecIndex_17(int32_t value)
	{
		____forceVideoCodecIndex_17 = value;
	}

	inline static int32_t get_offset_of__flipVertically_18() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____flipVertically_18)); }
	inline bool get__flipVertically_18() const { return ____flipVertically_18; }
	inline bool* get_address_of__flipVertically_18() { return &____flipVertically_18; }
	inline void set__flipVertically_18(bool value)
	{
		____flipVertically_18 = value;
	}

	inline static int32_t get_offset_of__supportAlpha_19() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____supportAlpha_19)); }
	inline bool get__supportAlpha_19() const { return ____supportAlpha_19; }
	inline bool* get_address_of__supportAlpha_19() { return &____supportAlpha_19; }
	inline void set__supportAlpha_19(bool value)
	{
		____supportAlpha_19 = value;
	}

	inline static int32_t get_offset_of__noAudio_20() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____noAudio_20)); }
	inline bool get__noAudio_20() const { return ____noAudio_20; }
	inline bool* get_address_of__noAudio_20() { return &____noAudio_20; }
	inline void set__noAudio_20(bool value)
	{
		____noAudio_20 = value;
	}

	inline static int32_t get_offset_of__audioCodecPriority_21() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____audioCodecPriority_21)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__audioCodecPriority_21() const { return ____audioCodecPriority_21; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__audioCodecPriority_21() { return &____audioCodecPriority_21; }
	inline void set__audioCodecPriority_21(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____audioCodecPriority_21 = value;
		Il2CppCodeGenWriteBarrier((&____audioCodecPriority_21), value);
	}

	inline static int32_t get_offset_of__forceAudioCodecIndex_22() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____forceAudioCodecIndex_22)); }
	inline int32_t get__forceAudioCodecIndex_22() const { return ____forceAudioCodecIndex_22; }
	inline int32_t* get_address_of__forceAudioCodecIndex_22() { return &____forceAudioCodecIndex_22; }
	inline void set__forceAudioCodecIndex_22(int32_t value)
	{
		____forceAudioCodecIndex_22 = value;
	}

	inline static int32_t get_offset_of__forceAudioDeviceIndex_23() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____forceAudioDeviceIndex_23)); }
	inline int32_t get__forceAudioDeviceIndex_23() const { return ____forceAudioDeviceIndex_23; }
	inline int32_t* get_address_of__forceAudioDeviceIndex_23() { return &____forceAudioDeviceIndex_23; }
	inline void set__forceAudioDeviceIndex_23(int32_t value)
	{
		____forceAudioDeviceIndex_23 = value;
	}

	inline static int32_t get_offset_of__audioCapture_24() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____audioCapture_24)); }
	inline UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5 * get__audioCapture_24() const { return ____audioCapture_24; }
	inline UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5 ** get_address_of__audioCapture_24() { return &____audioCapture_24; }
	inline void set__audioCapture_24(UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5 * value)
	{
		____audioCapture_24 = value;
		Il2CppCodeGenWriteBarrier((&____audioCapture_24), value);
	}

	inline static int32_t get_offset_of__autoGenerateFilename_25() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____autoGenerateFilename_25)); }
	inline bool get__autoGenerateFilename_25() const { return ____autoGenerateFilename_25; }
	inline bool* get_address_of__autoGenerateFilename_25() { return &____autoGenerateFilename_25; }
	inline void set__autoGenerateFilename_25(bool value)
	{
		____autoGenerateFilename_25 = value;
	}

	inline static int32_t get_offset_of__outputFolderType_26() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____outputFolderType_26)); }
	inline int32_t get__outputFolderType_26() const { return ____outputFolderType_26; }
	inline int32_t* get_address_of__outputFolderType_26() { return &____outputFolderType_26; }
	inline void set__outputFolderType_26(int32_t value)
	{
		____outputFolderType_26 = value;
	}

	inline static int32_t get_offset_of__outputFolderPath_27() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____outputFolderPath_27)); }
	inline String_t* get__outputFolderPath_27() const { return ____outputFolderPath_27; }
	inline String_t** get_address_of__outputFolderPath_27() { return &____outputFolderPath_27; }
	inline void set__outputFolderPath_27(String_t* value)
	{
		____outputFolderPath_27 = value;
		Il2CppCodeGenWriteBarrier((&____outputFolderPath_27), value);
	}

	inline static int32_t get_offset_of__autoFilenamePrefix_28() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____autoFilenamePrefix_28)); }
	inline String_t* get__autoFilenamePrefix_28() const { return ____autoFilenamePrefix_28; }
	inline String_t** get_address_of__autoFilenamePrefix_28() { return &____autoFilenamePrefix_28; }
	inline void set__autoFilenamePrefix_28(String_t* value)
	{
		____autoFilenamePrefix_28 = value;
		Il2CppCodeGenWriteBarrier((&____autoFilenamePrefix_28), value);
	}

	inline static int32_t get_offset_of__autoFilenameExtension_29() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____autoFilenameExtension_29)); }
	inline String_t* get__autoFilenameExtension_29() const { return ____autoFilenameExtension_29; }
	inline String_t** get_address_of__autoFilenameExtension_29() { return &____autoFilenameExtension_29; }
	inline void set__autoFilenameExtension_29(String_t* value)
	{
		____autoFilenameExtension_29 = value;
		Il2CppCodeGenWriteBarrier((&____autoFilenameExtension_29), value);
	}

	inline static int32_t get_offset_of__forceFilename_30() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____forceFilename_30)); }
	inline String_t* get__forceFilename_30() const { return ____forceFilename_30; }
	inline String_t** get_address_of__forceFilename_30() { return &____forceFilename_30; }
	inline void set__forceFilename_30(String_t* value)
	{
		____forceFilename_30 = value;
		Il2CppCodeGenWriteBarrier((&____forceFilename_30), value);
	}

	inline static int32_t get_offset_of__outputType_31() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____outputType_31)); }
	inline int32_t get__outputType_31() const { return ____outputType_31; }
	inline int32_t* get_address_of__outputType_31() { return &____outputType_31; }
	inline void set__outputType_31(int32_t value)
	{
		____outputType_31 = value;
	}

	inline static int32_t get_offset_of__renderResolution_32() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____renderResolution_32)); }
	inline int32_t get__renderResolution_32() const { return ____renderResolution_32; }
	inline int32_t* get_address_of__renderResolution_32() { return &____renderResolution_32; }
	inline void set__renderResolution_32(int32_t value)
	{
		____renderResolution_32 = value;
	}

	inline static int32_t get_offset_of__renderSize_33() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____renderSize_33)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__renderSize_33() const { return ____renderSize_33; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__renderSize_33() { return &____renderSize_33; }
	inline void set__renderSize_33(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____renderSize_33 = value;
	}

	inline static int32_t get_offset_of__renderAntiAliasing_34() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____renderAntiAliasing_34)); }
	inline int32_t get__renderAntiAliasing_34() const { return ____renderAntiAliasing_34; }
	inline int32_t* get_address_of__renderAntiAliasing_34() { return &____renderAntiAliasing_34; }
	inline void set__renderAntiAliasing_34(int32_t value)
	{
		____renderAntiAliasing_34 = value;
	}

	inline static int32_t get_offset_of__useMotionBlur_35() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____useMotionBlur_35)); }
	inline bool get__useMotionBlur_35() const { return ____useMotionBlur_35; }
	inline bool* get_address_of__useMotionBlur_35() { return &____useMotionBlur_35; }
	inline void set__useMotionBlur_35(bool value)
	{
		____useMotionBlur_35 = value;
	}

	inline static int32_t get_offset_of__motionBlurSamples_36() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____motionBlurSamples_36)); }
	inline int32_t get__motionBlurSamples_36() const { return ____motionBlurSamples_36; }
	inline int32_t* get_address_of__motionBlurSamples_36() { return &____motionBlurSamples_36; }
	inline void set__motionBlurSamples_36(int32_t value)
	{
		____motionBlurSamples_36 = value;
	}

	inline static int32_t get_offset_of__motionBlurCameras_37() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____motionBlurCameras_37)); }
	inline CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9* get__motionBlurCameras_37() const { return ____motionBlurCameras_37; }
	inline CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9** get_address_of__motionBlurCameras_37() { return &____motionBlurCameras_37; }
	inline void set__motionBlurCameras_37(CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9* value)
	{
		____motionBlurCameras_37 = value;
		Il2CppCodeGenWriteBarrier((&____motionBlurCameras_37), value);
	}

	inline static int32_t get_offset_of__motionBlur_38() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____motionBlur_38)); }
	inline MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7 * get__motionBlur_38() const { return ____motionBlur_38; }
	inline MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7 ** get_address_of__motionBlur_38() { return &____motionBlur_38; }
	inline void set__motionBlur_38(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7 * value)
	{
		____motionBlur_38 = value;
		Il2CppCodeGenWriteBarrier((&____motionBlur_38), value);
	}

	inline static int32_t get_offset_of__allowVSyncDisable_39() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____allowVSyncDisable_39)); }
	inline bool get__allowVSyncDisable_39() const { return ____allowVSyncDisable_39; }
	inline bool* get_address_of__allowVSyncDisable_39() { return &____allowVSyncDisable_39; }
	inline void set__allowVSyncDisable_39(bool value)
	{
		____allowVSyncDisable_39 = value;
	}

	inline static int32_t get_offset_of__supportTextureRecreate_40() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____supportTextureRecreate_40)); }
	inline bool get__supportTextureRecreate_40() const { return ____supportTextureRecreate_40; }
	inline bool* get_address_of__supportTextureRecreate_40() { return &____supportTextureRecreate_40; }
	inline void set__supportTextureRecreate_40(bool value)
	{
		____supportTextureRecreate_40 = value;
	}

	inline static int32_t get_offset_of__captureMouseCursor_41() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____captureMouseCursor_41)); }
	inline bool get__captureMouseCursor_41() const { return ____captureMouseCursor_41; }
	inline bool* get_address_of__captureMouseCursor_41() { return &____captureMouseCursor_41; }
	inline void set__captureMouseCursor_41(bool value)
	{
		____captureMouseCursor_41 = value;
	}

	inline static int32_t get_offset_of__mouseCursor_42() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____mouseCursor_42)); }
	inline MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD * get__mouseCursor_42() const { return ____mouseCursor_42; }
	inline MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD ** get_address_of__mouseCursor_42() { return &____mouseCursor_42; }
	inline void set__mouseCursor_42(MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD * value)
	{
		____mouseCursor_42 = value;
		Il2CppCodeGenWriteBarrier((&____mouseCursor_42), value);
	}

	inline static int32_t get_offset_of__codecName_43() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____codecName_43)); }
	inline String_t* get__codecName_43() const { return ____codecName_43; }
	inline String_t** get_address_of__codecName_43() { return &____codecName_43; }
	inline void set__codecName_43(String_t* value)
	{
		____codecName_43 = value;
		Il2CppCodeGenWriteBarrier((&____codecName_43), value);
	}

	inline static int32_t get_offset_of__codecIndex_44() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____codecIndex_44)); }
	inline int32_t get__codecIndex_44() const { return ____codecIndex_44; }
	inline int32_t* get_address_of__codecIndex_44() { return &____codecIndex_44; }
	inline void set__codecIndex_44(int32_t value)
	{
		____codecIndex_44 = value;
	}

	inline static int32_t get_offset_of__audioCodecName_45() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____audioCodecName_45)); }
	inline String_t* get__audioCodecName_45() const { return ____audioCodecName_45; }
	inline String_t** get_address_of__audioCodecName_45() { return &____audioCodecName_45; }
	inline void set__audioCodecName_45(String_t* value)
	{
		____audioCodecName_45 = value;
		Il2CppCodeGenWriteBarrier((&____audioCodecName_45), value);
	}

	inline static int32_t get_offset_of__audioCodecIndex_46() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____audioCodecIndex_46)); }
	inline int32_t get__audioCodecIndex_46() const { return ____audioCodecIndex_46; }
	inline int32_t* get_address_of__audioCodecIndex_46() { return &____audioCodecIndex_46; }
	inline void set__audioCodecIndex_46(int32_t value)
	{
		____audioCodecIndex_46 = value;
	}

	inline static int32_t get_offset_of__audioDeviceName_47() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____audioDeviceName_47)); }
	inline String_t* get__audioDeviceName_47() const { return ____audioDeviceName_47; }
	inline String_t** get_address_of__audioDeviceName_47() { return &____audioDeviceName_47; }
	inline void set__audioDeviceName_47(String_t* value)
	{
		____audioDeviceName_47 = value;
		Il2CppCodeGenWriteBarrier((&____audioDeviceName_47), value);
	}

	inline static int32_t get_offset_of__audioDeviceIndex_48() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____audioDeviceIndex_48)); }
	inline int32_t get__audioDeviceIndex_48() const { return ____audioDeviceIndex_48; }
	inline int32_t* get_address_of__audioDeviceIndex_48() { return &____audioDeviceIndex_48; }
	inline void set__audioDeviceIndex_48(int32_t value)
	{
		____audioDeviceIndex_48 = value;
	}

	inline static int32_t get_offset_of__unityAudioSampleRate_49() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____unityAudioSampleRate_49)); }
	inline int32_t get__unityAudioSampleRate_49() const { return ____unityAudioSampleRate_49; }
	inline int32_t* get_address_of__unityAudioSampleRate_49() { return &____unityAudioSampleRate_49; }
	inline void set__unityAudioSampleRate_49(int32_t value)
	{
		____unityAudioSampleRate_49 = value;
	}

	inline static int32_t get_offset_of__unityAudioChannelCount_50() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____unityAudioChannelCount_50)); }
	inline int32_t get__unityAudioChannelCount_50() const { return ____unityAudioChannelCount_50; }
	inline int32_t* get_address_of__unityAudioChannelCount_50() { return &____unityAudioChannelCount_50; }
	inline void set__unityAudioChannelCount_50(int32_t value)
	{
		____unityAudioChannelCount_50 = value;
	}

	inline static int32_t get_offset_of__texture_51() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____texture_51)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get__texture_51() const { return ____texture_51; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of__texture_51() { return &____texture_51; }
	inline void set__texture_51(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		____texture_51 = value;
		Il2CppCodeGenWriteBarrier((&____texture_51), value);
	}

	inline static int32_t get_offset_of__handle_52() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____handle_52)); }
	inline int32_t get__handle_52() const { return ____handle_52; }
	inline int32_t* get_address_of__handle_52() { return &____handle_52; }
	inline void set__handle_52(int32_t value)
	{
		____handle_52 = value;
	}

	inline static int32_t get_offset_of__targetWidth_53() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____targetWidth_53)); }
	inline int32_t get__targetWidth_53() const { return ____targetWidth_53; }
	inline int32_t* get_address_of__targetWidth_53() { return &____targetWidth_53; }
	inline void set__targetWidth_53(int32_t value)
	{
		____targetWidth_53 = value;
	}

	inline static int32_t get_offset_of__targetHeight_54() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____targetHeight_54)); }
	inline int32_t get__targetHeight_54() const { return ____targetHeight_54; }
	inline int32_t* get_address_of__targetHeight_54() { return &____targetHeight_54; }
	inline void set__targetHeight_54(int32_t value)
	{
		____targetHeight_54 = value;
	}

	inline static int32_t get_offset_of__capturing_55() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____capturing_55)); }
	inline bool get__capturing_55() const { return ____capturing_55; }
	inline bool* get_address_of__capturing_55() { return &____capturing_55; }
	inline void set__capturing_55(bool value)
	{
		____capturing_55 = value;
	}

	inline static int32_t get_offset_of__paused_56() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____paused_56)); }
	inline bool get__paused_56() const { return ____paused_56; }
	inline bool* get_address_of__paused_56() { return &____paused_56; }
	inline void set__paused_56(bool value)
	{
		____paused_56 = value;
	}

	inline static int32_t get_offset_of__filePath_57() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____filePath_57)); }
	inline String_t* get__filePath_57() const { return ____filePath_57; }
	inline String_t** get_address_of__filePath_57() { return &____filePath_57; }
	inline void set__filePath_57(String_t* value)
	{
		____filePath_57 = value;
		Il2CppCodeGenWriteBarrier((&____filePath_57), value);
	}

	inline static int32_t get_offset_of__fileInfo_58() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____fileInfo_58)); }
	inline FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * get__fileInfo_58() const { return ____fileInfo_58; }
	inline FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C ** get_address_of__fileInfo_58() { return &____fileInfo_58; }
	inline void set__fileInfo_58(FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * value)
	{
		____fileInfo_58 = value;
		Il2CppCodeGenWriteBarrier((&____fileInfo_58), value);
	}

	inline static int32_t get_offset_of__pixelFormat_59() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____pixelFormat_59)); }
	inline int32_t get__pixelFormat_59() const { return ____pixelFormat_59; }
	inline int32_t* get_address_of__pixelFormat_59() { return &____pixelFormat_59; }
	inline void set__pixelFormat_59(int32_t value)
	{
		____pixelFormat_59 = value;
	}

	inline static int32_t get_offset_of__oldVSyncCount_60() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____oldVSyncCount_60)); }
	inline int32_t get__oldVSyncCount_60() const { return ____oldVSyncCount_60; }
	inline int32_t* get_address_of__oldVSyncCount_60() { return &____oldVSyncCount_60; }
	inline void set__oldVSyncCount_60(int32_t value)
	{
		____oldVSyncCount_60 = value;
	}

	inline static int32_t get_offset_of__oldFixedDeltaTime_61() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____oldFixedDeltaTime_61)); }
	inline float get__oldFixedDeltaTime_61() const { return ____oldFixedDeltaTime_61; }
	inline float* get_address_of__oldFixedDeltaTime_61() { return &____oldFixedDeltaTime_61; }
	inline void set__oldFixedDeltaTime_61(float value)
	{
		____oldFixedDeltaTime_61 = value;
	}

	inline static int32_t get_offset_of__isTopDown_62() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____isTopDown_62)); }
	inline bool get__isTopDown_62() const { return ____isTopDown_62; }
	inline bool* get_address_of__isTopDown_62() { return &____isTopDown_62; }
	inline void set__isTopDown_62(bool value)
	{
		____isTopDown_62 = value;
	}

	inline static int32_t get_offset_of__isDirectX11_63() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____isDirectX11_63)); }
	inline bool get__isDirectX11_63() const { return ____isDirectX11_63; }
	inline bool* get_address_of__isDirectX11_63() { return &____isDirectX11_63; }
	inline void set__isDirectX11_63(bool value)
	{
		____isDirectX11_63 = value;
	}

	inline static int32_t get_offset_of__queuedStartCapture_64() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____queuedStartCapture_64)); }
	inline bool get__queuedStartCapture_64() const { return ____queuedStartCapture_64; }
	inline bool* get_address_of__queuedStartCapture_64() { return &____queuedStartCapture_64; }
	inline void set__queuedStartCapture_64(bool value)
	{
		____queuedStartCapture_64 = value;
	}

	inline static int32_t get_offset_of__queuedStopCapture_65() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____queuedStopCapture_65)); }
	inline bool get__queuedStopCapture_65() const { return ____queuedStopCapture_65; }
	inline bool* get_address_of__queuedStopCapture_65() { return &____queuedStopCapture_65; }
	inline void set__queuedStopCapture_65(bool value)
	{
		____queuedStopCapture_65 = value;
	}

	inline static int32_t get_offset_of__captureStartTime_66() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____captureStartTime_66)); }
	inline float get__captureStartTime_66() const { return ____captureStartTime_66; }
	inline float* get_address_of__captureStartTime_66() { return &____captureStartTime_66; }
	inline void set__captureStartTime_66(float value)
	{
		____captureStartTime_66 = value;
	}

	inline static int32_t get_offset_of__timeSinceLastFrame_67() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____timeSinceLastFrame_67)); }
	inline float get__timeSinceLastFrame_67() const { return ____timeSinceLastFrame_67; }
	inline float* get_address_of__timeSinceLastFrame_67() { return &____timeSinceLastFrame_67; }
	inline void set__timeSinceLastFrame_67(float value)
	{
		____timeSinceLastFrame_67 = value;
	}

	inline static int32_t get_offset_of__minimumDiskSpaceMB_68() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____minimumDiskSpaceMB_68)); }
	inline int32_t get__minimumDiskSpaceMB_68() const { return ____minimumDiskSpaceMB_68; }
	inline int32_t* get_address_of__minimumDiskSpaceMB_68() { return &____minimumDiskSpaceMB_68; }
	inline void set__minimumDiskSpaceMB_68(int32_t value)
	{
		____minimumDiskSpaceMB_68 = value;
	}

	inline static int32_t get_offset_of__freeDiskSpaceMB_69() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____freeDiskSpaceMB_69)); }
	inline int64_t get__freeDiskSpaceMB_69() const { return ____freeDiskSpaceMB_69; }
	inline int64_t* get_address_of__freeDiskSpaceMB_69() { return &____freeDiskSpaceMB_69; }
	inline void set__freeDiskSpaceMB_69(int64_t value)
	{
		____freeDiskSpaceMB_69 = value;
	}

	inline static int32_t get_offset_of__numDroppedFrames_70() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____numDroppedFrames_70)); }
	inline uint32_t get__numDroppedFrames_70() const { return ____numDroppedFrames_70; }
	inline uint32_t* get_address_of__numDroppedFrames_70() { return &____numDroppedFrames_70; }
	inline void set__numDroppedFrames_70(uint32_t value)
	{
		____numDroppedFrames_70 = value;
	}

	inline static int32_t get_offset_of__numDroppedEncoderFrames_71() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____numDroppedEncoderFrames_71)); }
	inline uint32_t get__numDroppedEncoderFrames_71() const { return ____numDroppedEncoderFrames_71; }
	inline uint32_t* get_address_of__numDroppedEncoderFrames_71() { return &____numDroppedEncoderFrames_71; }
	inline void set__numDroppedEncoderFrames_71(uint32_t value)
	{
		____numDroppedEncoderFrames_71 = value;
	}

	inline static int32_t get_offset_of__numEncodedFrames_72() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____numEncodedFrames_72)); }
	inline uint32_t get__numEncodedFrames_72() const { return ____numEncodedFrames_72; }
	inline uint32_t* get_address_of__numEncodedFrames_72() { return &____numEncodedFrames_72; }
	inline void set__numEncodedFrames_72(uint32_t value)
	{
		____numEncodedFrames_72 = value;
	}

	inline static int32_t get_offset_of__totalEncodedSeconds_73() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____totalEncodedSeconds_73)); }
	inline uint32_t get__totalEncodedSeconds_73() const { return ____totalEncodedSeconds_73; }
	inline uint32_t* get_address_of__totalEncodedSeconds_73() { return &____totalEncodedSeconds_73; }
	inline void set__totalEncodedSeconds_73(uint32_t value)
	{
		____totalEncodedSeconds_73 = value;
	}

	inline static int32_t get_offset_of__renderEventFunction_74() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____renderEventFunction_74)); }
	inline intptr_t get__renderEventFunction_74() const { return ____renderEventFunction_74; }
	inline intptr_t* get_address_of__renderEventFunction_74() { return &____renderEventFunction_74; }
	inline void set__renderEventFunction_74(intptr_t value)
	{
		____renderEventFunction_74 = value;
	}

	inline static int32_t get_offset_of__freeEventFunction_75() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____freeEventFunction_75)); }
	inline intptr_t get__freeEventFunction_75() const { return ____freeEventFunction_75; }
	inline intptr_t* get_address_of__freeEventFunction_75() { return &____freeEventFunction_75; }
	inline void set__freeEventFunction_75(intptr_t value)
	{
		____freeEventFunction_75 = value;
	}

	inline static int32_t get_offset_of__fps_76() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____fps_76)); }
	inline float get__fps_76() const { return ____fps_76; }
	inline float* get_address_of__fps_76() { return &____fps_76; }
	inline void set__fps_76(float value)
	{
		____fps_76 = value;
	}

	inline static int32_t get_offset_of__frameTotal_77() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____frameTotal_77)); }
	inline int32_t get__frameTotal_77() const { return ____frameTotal_77; }
	inline int32_t* get_address_of__frameTotal_77() { return &____frameTotal_77; }
	inline void set__frameTotal_77(int32_t value)
	{
		____frameTotal_77 = value;
	}

	inline static int32_t get_offset_of__frameCount_78() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____frameCount_78)); }
	inline int32_t get__frameCount_78() const { return ____frameCount_78; }
	inline int32_t* get_address_of__frameCount_78() { return &____frameCount_78; }
	inline void set__frameCount_78(int32_t value)
	{
		____frameCount_78 = value;
	}

	inline static int32_t get_offset_of__startFrameTime_79() { return static_cast<int32_t>(offsetof(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110, ____startFrameTime_79)); }
	inline float get__startFrameTime_79() const { return ____startFrameTime_79; }
	inline float* get_address_of__startFrameTime_79() { return &____startFrameTime_79; }
	inline void set__startFrameTime_79(float value)
	{
		____startFrameTime_79 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREBASE_T4AA58B983F72B37942873222FDA0DFAEAE76C110_H
#ifndef CAPTUREGUI_T215A684C1FDA02B7B0827810C8C01F75E4022A21_H
#define CAPTUREGUI_T215A684C1FDA02B7B0827810C8C01F75E4022A21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureGUI
struct  CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// RenderHeads.Media.AVProMovieCapture.CaptureBase RenderHeads.Media.AVProMovieCapture.CaptureGUI::_movieCapture
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110 * ____movieCapture_4;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureGUI::_showUI
	bool ____showUI_5;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureGUI::_whenRecordingAutoHideUI
	bool ____whenRecordingAutoHideUI_6;
	// UnityEngine.GUISkin RenderHeads.Media.AVProMovieCapture.CaptureGUI::_guiSkin
	GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * ____guiSkin_7;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureGUI::_shownSection
	int32_t ____shownSection_8;
	// System.String[] RenderHeads.Media.AVProMovieCapture.CaptureGUI::_videoCodecNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____videoCodecNames_9;
	// System.String[] RenderHeads.Media.AVProMovieCapture.CaptureGUI::_audioCodecNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____audioCodecNames_10;
	// System.Boolean[] RenderHeads.Media.AVProMovieCapture.CaptureGUI::_videoCodecConfigurable
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ____videoCodecConfigurable_11;
	// System.Boolean[] RenderHeads.Media.AVProMovieCapture.CaptureGUI::_audioCodecConfigurable
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ____audioCodecConfigurable_12;
	// System.String[] RenderHeads.Media.AVProMovieCapture.CaptureGUI::_audioDeviceNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____audioDeviceNames_13;
	// System.String[] RenderHeads.Media.AVProMovieCapture.CaptureGUI::_downScales
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____downScales_14;
	// System.String[] RenderHeads.Media.AVProMovieCapture.CaptureGUI::_frameRates
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____frameRates_15;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureGUI::_downScaleIndex
	int32_t ____downScaleIndex_16;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureGUI::_frameRateIndex
	int32_t ____frameRateIndex_17;
	// UnityEngine.Vector2 RenderHeads.Media.AVProMovieCapture.CaptureGUI::_videoPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____videoPos_18;
	// UnityEngine.Vector2 RenderHeads.Media.AVProMovieCapture.CaptureGUI::_audioPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____audioPos_19;
	// UnityEngine.Vector2 RenderHeads.Media.AVProMovieCapture.CaptureGUI::_audioCodecPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____audioCodecPos_20;
	// System.Int64 RenderHeads.Media.AVProMovieCapture.CaptureGUI::_lastFileSize
	int64_t ____lastFileSize_21;
	// System.UInt32 RenderHeads.Media.AVProMovieCapture.CaptureGUI::_lastEncodedMinutes
	uint32_t ____lastEncodedMinutes_22;
	// System.UInt32 RenderHeads.Media.AVProMovieCapture.CaptureGUI::_lastEncodedSeconds
	uint32_t ____lastEncodedSeconds_23;
	// System.UInt32 RenderHeads.Media.AVProMovieCapture.CaptureGUI::_lastEncodedFrame
	uint32_t ____lastEncodedFrame_24;

public:
	inline static int32_t get_offset_of__movieCapture_4() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____movieCapture_4)); }
	inline CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110 * get__movieCapture_4() const { return ____movieCapture_4; }
	inline CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110 ** get_address_of__movieCapture_4() { return &____movieCapture_4; }
	inline void set__movieCapture_4(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110 * value)
	{
		____movieCapture_4 = value;
		Il2CppCodeGenWriteBarrier((&____movieCapture_4), value);
	}

	inline static int32_t get_offset_of__showUI_5() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____showUI_5)); }
	inline bool get__showUI_5() const { return ____showUI_5; }
	inline bool* get_address_of__showUI_5() { return &____showUI_5; }
	inline void set__showUI_5(bool value)
	{
		____showUI_5 = value;
	}

	inline static int32_t get_offset_of__whenRecordingAutoHideUI_6() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____whenRecordingAutoHideUI_6)); }
	inline bool get__whenRecordingAutoHideUI_6() const { return ____whenRecordingAutoHideUI_6; }
	inline bool* get_address_of__whenRecordingAutoHideUI_6() { return &____whenRecordingAutoHideUI_6; }
	inline void set__whenRecordingAutoHideUI_6(bool value)
	{
		____whenRecordingAutoHideUI_6 = value;
	}

	inline static int32_t get_offset_of__guiSkin_7() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____guiSkin_7)); }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * get__guiSkin_7() const { return ____guiSkin_7; }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 ** get_address_of__guiSkin_7() { return &____guiSkin_7; }
	inline void set__guiSkin_7(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * value)
	{
		____guiSkin_7 = value;
		Il2CppCodeGenWriteBarrier((&____guiSkin_7), value);
	}

	inline static int32_t get_offset_of__shownSection_8() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____shownSection_8)); }
	inline int32_t get__shownSection_8() const { return ____shownSection_8; }
	inline int32_t* get_address_of__shownSection_8() { return &____shownSection_8; }
	inline void set__shownSection_8(int32_t value)
	{
		____shownSection_8 = value;
	}

	inline static int32_t get_offset_of__videoCodecNames_9() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____videoCodecNames_9)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__videoCodecNames_9() const { return ____videoCodecNames_9; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__videoCodecNames_9() { return &____videoCodecNames_9; }
	inline void set__videoCodecNames_9(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____videoCodecNames_9 = value;
		Il2CppCodeGenWriteBarrier((&____videoCodecNames_9), value);
	}

	inline static int32_t get_offset_of__audioCodecNames_10() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____audioCodecNames_10)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__audioCodecNames_10() const { return ____audioCodecNames_10; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__audioCodecNames_10() { return &____audioCodecNames_10; }
	inline void set__audioCodecNames_10(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____audioCodecNames_10 = value;
		Il2CppCodeGenWriteBarrier((&____audioCodecNames_10), value);
	}

	inline static int32_t get_offset_of__videoCodecConfigurable_11() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____videoCodecConfigurable_11)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get__videoCodecConfigurable_11() const { return ____videoCodecConfigurable_11; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of__videoCodecConfigurable_11() { return &____videoCodecConfigurable_11; }
	inline void set__videoCodecConfigurable_11(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		____videoCodecConfigurable_11 = value;
		Il2CppCodeGenWriteBarrier((&____videoCodecConfigurable_11), value);
	}

	inline static int32_t get_offset_of__audioCodecConfigurable_12() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____audioCodecConfigurable_12)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get__audioCodecConfigurable_12() const { return ____audioCodecConfigurable_12; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of__audioCodecConfigurable_12() { return &____audioCodecConfigurable_12; }
	inline void set__audioCodecConfigurable_12(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		____audioCodecConfigurable_12 = value;
		Il2CppCodeGenWriteBarrier((&____audioCodecConfigurable_12), value);
	}

	inline static int32_t get_offset_of__audioDeviceNames_13() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____audioDeviceNames_13)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__audioDeviceNames_13() const { return ____audioDeviceNames_13; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__audioDeviceNames_13() { return &____audioDeviceNames_13; }
	inline void set__audioDeviceNames_13(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____audioDeviceNames_13 = value;
		Il2CppCodeGenWriteBarrier((&____audioDeviceNames_13), value);
	}

	inline static int32_t get_offset_of__downScales_14() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____downScales_14)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__downScales_14() const { return ____downScales_14; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__downScales_14() { return &____downScales_14; }
	inline void set__downScales_14(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____downScales_14 = value;
		Il2CppCodeGenWriteBarrier((&____downScales_14), value);
	}

	inline static int32_t get_offset_of__frameRates_15() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____frameRates_15)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__frameRates_15() const { return ____frameRates_15; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__frameRates_15() { return &____frameRates_15; }
	inline void set__frameRates_15(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____frameRates_15 = value;
		Il2CppCodeGenWriteBarrier((&____frameRates_15), value);
	}

	inline static int32_t get_offset_of__downScaleIndex_16() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____downScaleIndex_16)); }
	inline int32_t get__downScaleIndex_16() const { return ____downScaleIndex_16; }
	inline int32_t* get_address_of__downScaleIndex_16() { return &____downScaleIndex_16; }
	inline void set__downScaleIndex_16(int32_t value)
	{
		____downScaleIndex_16 = value;
	}

	inline static int32_t get_offset_of__frameRateIndex_17() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____frameRateIndex_17)); }
	inline int32_t get__frameRateIndex_17() const { return ____frameRateIndex_17; }
	inline int32_t* get_address_of__frameRateIndex_17() { return &____frameRateIndex_17; }
	inline void set__frameRateIndex_17(int32_t value)
	{
		____frameRateIndex_17 = value;
	}

	inline static int32_t get_offset_of__videoPos_18() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____videoPos_18)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__videoPos_18() const { return ____videoPos_18; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__videoPos_18() { return &____videoPos_18; }
	inline void set__videoPos_18(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____videoPos_18 = value;
	}

	inline static int32_t get_offset_of__audioPos_19() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____audioPos_19)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__audioPos_19() const { return ____audioPos_19; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__audioPos_19() { return &____audioPos_19; }
	inline void set__audioPos_19(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____audioPos_19 = value;
	}

	inline static int32_t get_offset_of__audioCodecPos_20() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____audioCodecPos_20)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__audioCodecPos_20() const { return ____audioCodecPos_20; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__audioCodecPos_20() { return &____audioCodecPos_20; }
	inline void set__audioCodecPos_20(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____audioCodecPos_20 = value;
	}

	inline static int32_t get_offset_of__lastFileSize_21() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____lastFileSize_21)); }
	inline int64_t get__lastFileSize_21() const { return ____lastFileSize_21; }
	inline int64_t* get_address_of__lastFileSize_21() { return &____lastFileSize_21; }
	inline void set__lastFileSize_21(int64_t value)
	{
		____lastFileSize_21 = value;
	}

	inline static int32_t get_offset_of__lastEncodedMinutes_22() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____lastEncodedMinutes_22)); }
	inline uint32_t get__lastEncodedMinutes_22() const { return ____lastEncodedMinutes_22; }
	inline uint32_t* get_address_of__lastEncodedMinutes_22() { return &____lastEncodedMinutes_22; }
	inline void set__lastEncodedMinutes_22(uint32_t value)
	{
		____lastEncodedMinutes_22 = value;
	}

	inline static int32_t get_offset_of__lastEncodedSeconds_23() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____lastEncodedSeconds_23)); }
	inline uint32_t get__lastEncodedSeconds_23() const { return ____lastEncodedSeconds_23; }
	inline uint32_t* get_address_of__lastEncodedSeconds_23() { return &____lastEncodedSeconds_23; }
	inline void set__lastEncodedSeconds_23(uint32_t value)
	{
		____lastEncodedSeconds_23 = value;
	}

	inline static int32_t get_offset_of__lastEncodedFrame_24() { return static_cast<int32_t>(offsetof(CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21, ____lastEncodedFrame_24)); }
	inline uint32_t get__lastEncodedFrame_24() const { return ____lastEncodedFrame_24; }
	inline uint32_t* get_address_of__lastEncodedFrame_24() { return &____lastEncodedFrame_24; }
	inline void set__lastEncodedFrame_24(uint32_t value)
	{
		____lastEncodedFrame_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREGUI_T215A684C1FDA02B7B0827810C8C01F75E4022A21_H
#ifndef SCREENCAPTUREDEMO_T706C9CC513D34F472E4BCB2621D68DD61AB849B6_H
#define SCREENCAPTUREDEMO_T706C9CC513D34F472E4BCB2621D68DD61AB849B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.Demos.ScreenCaptureDemo
struct  ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioClip RenderHeads.Media.AVProMovieCapture.Demos.ScreenCaptureDemo::_audioBG
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ____audioBG_4;
	// UnityEngine.AudioClip RenderHeads.Media.AVProMovieCapture.Demos.ScreenCaptureDemo::_audioHit
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ____audioHit_5;
	// System.Single RenderHeads.Media.AVProMovieCapture.Demos.ScreenCaptureDemo::_speed
	float ____speed_6;
	// RenderHeads.Media.AVProMovieCapture.CaptureBase RenderHeads.Media.AVProMovieCapture.Demos.ScreenCaptureDemo::_capture
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110 * ____capture_7;
	// UnityEngine.GUISkin RenderHeads.Media.AVProMovieCapture.Demos.ScreenCaptureDemo::_guiSkin
	GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * ____guiSkin_8;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.Demos.ScreenCaptureDemo::_spinCamera
	bool ____spinCamera_9;
	// System.Single RenderHeads.Media.AVProMovieCapture.Demos.ScreenCaptureDemo::_timer
	float ____timer_10;

public:
	inline static int32_t get_offset_of__audioBG_4() { return static_cast<int32_t>(offsetof(ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6, ____audioBG_4)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get__audioBG_4() const { return ____audioBG_4; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of__audioBG_4() { return &____audioBG_4; }
	inline void set__audioBG_4(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		____audioBG_4 = value;
		Il2CppCodeGenWriteBarrier((&____audioBG_4), value);
	}

	inline static int32_t get_offset_of__audioHit_5() { return static_cast<int32_t>(offsetof(ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6, ____audioHit_5)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get__audioHit_5() const { return ____audioHit_5; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of__audioHit_5() { return &____audioHit_5; }
	inline void set__audioHit_5(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		____audioHit_5 = value;
		Il2CppCodeGenWriteBarrier((&____audioHit_5), value);
	}

	inline static int32_t get_offset_of__speed_6() { return static_cast<int32_t>(offsetof(ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6, ____speed_6)); }
	inline float get__speed_6() const { return ____speed_6; }
	inline float* get_address_of__speed_6() { return &____speed_6; }
	inline void set__speed_6(float value)
	{
		____speed_6 = value;
	}

	inline static int32_t get_offset_of__capture_7() { return static_cast<int32_t>(offsetof(ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6, ____capture_7)); }
	inline CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110 * get__capture_7() const { return ____capture_7; }
	inline CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110 ** get_address_of__capture_7() { return &____capture_7; }
	inline void set__capture_7(CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110 * value)
	{
		____capture_7 = value;
		Il2CppCodeGenWriteBarrier((&____capture_7), value);
	}

	inline static int32_t get_offset_of__guiSkin_8() { return static_cast<int32_t>(offsetof(ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6, ____guiSkin_8)); }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * get__guiSkin_8() const { return ____guiSkin_8; }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 ** get_address_of__guiSkin_8() { return &____guiSkin_8; }
	inline void set__guiSkin_8(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * value)
	{
		____guiSkin_8 = value;
		Il2CppCodeGenWriteBarrier((&____guiSkin_8), value);
	}

	inline static int32_t get_offset_of__spinCamera_9() { return static_cast<int32_t>(offsetof(ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6, ____spinCamera_9)); }
	inline bool get__spinCamera_9() const { return ____spinCamera_9; }
	inline bool* get_address_of__spinCamera_9() { return &____spinCamera_9; }
	inline void set__spinCamera_9(bool value)
	{
		____spinCamera_9 = value;
	}

	inline static int32_t get_offset_of__timer_10() { return static_cast<int32_t>(offsetof(ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6, ____timer_10)); }
	inline float get__timer_10() const { return ____timer_10; }
	inline float* get_address_of__timer_10() { return &____timer_10; }
	inline void set__timer_10(float value)
	{
		____timer_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENCAPTUREDEMO_T706C9CC513D34F472E4BCB2621D68DD61AB849B6_H
#ifndef SCRIPTCAPTUREDEMO_T4D14A89238E78B8AD3C0BC618A21D6342873C8A2_H
#define SCRIPTCAPTUREDEMO_T4D14A89238E78B8AD3C0BC618A21D6342873C8A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.Demos.ScriptCaptureDemo
struct  ScriptCaptureDemo_t4D14A89238E78B8AD3C0BC618A21D6342873C8A2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.Demos.ScriptCaptureDemo::_videoCodecIndex
	int32_t ____videoCodecIndex_5;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.Demos.ScriptCaptureDemo::_encoderHandle
	int32_t ____encoderHandle_6;

public:
	inline static int32_t get_offset_of__videoCodecIndex_5() { return static_cast<int32_t>(offsetof(ScriptCaptureDemo_t4D14A89238E78B8AD3C0BC618A21D6342873C8A2, ____videoCodecIndex_5)); }
	inline int32_t get__videoCodecIndex_5() const { return ____videoCodecIndex_5; }
	inline int32_t* get_address_of__videoCodecIndex_5() { return &____videoCodecIndex_5; }
	inline void set__videoCodecIndex_5(int32_t value)
	{
		____videoCodecIndex_5 = value;
	}

	inline static int32_t get_offset_of__encoderHandle_6() { return static_cast<int32_t>(offsetof(ScriptCaptureDemo_t4D14A89238E78B8AD3C0BC618A21D6342873C8A2, ____encoderHandle_6)); }
	inline int32_t get__encoderHandle_6() const { return ____encoderHandle_6; }
	inline int32_t* get_address_of__encoderHandle_6() { return &____encoderHandle_6; }
	inline void set__encoderHandle_6(int32_t value)
	{
		____encoderHandle_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTCAPTUREDEMO_T4D14A89238E78B8AD3C0BC618A21D6342873C8A2_H
#ifndef SURROUNDCAPTUREDEMO_TB198F0F3C3D917DCF75BF34B70C22318F1765EE6_H
#define SURROUNDCAPTUREDEMO_TB198F0F3C3D917DCF75BF34B70C22318F1765EE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.Demos.SurroundCaptureDemo
struct  SurroundCaptureDemo_tB198F0F3C3D917DCF75BF34B70C22318F1765EE6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform RenderHeads.Media.AVProMovieCapture.Demos.SurroundCaptureDemo::_spawnPoint
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____spawnPoint_4;
	// UnityEngine.GameObject RenderHeads.Media.AVProMovieCapture.Demos.SurroundCaptureDemo::_cubePrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____cubePrefab_5;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.Demos.SurroundCaptureDemo::_spawn
	bool ____spawn_6;
	// System.Single RenderHeads.Media.AVProMovieCapture.Demos.SurroundCaptureDemo::_timer
	float ____timer_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> RenderHeads.Media.AVProMovieCapture.Demos.SurroundCaptureDemo::_cubes
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ____cubes_10;

public:
	inline static int32_t get_offset_of__spawnPoint_4() { return static_cast<int32_t>(offsetof(SurroundCaptureDemo_tB198F0F3C3D917DCF75BF34B70C22318F1765EE6, ____spawnPoint_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__spawnPoint_4() const { return ____spawnPoint_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__spawnPoint_4() { return &____spawnPoint_4; }
	inline void set__spawnPoint_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____spawnPoint_4 = value;
		Il2CppCodeGenWriteBarrier((&____spawnPoint_4), value);
	}

	inline static int32_t get_offset_of__cubePrefab_5() { return static_cast<int32_t>(offsetof(SurroundCaptureDemo_tB198F0F3C3D917DCF75BF34B70C22318F1765EE6, ____cubePrefab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__cubePrefab_5() const { return ____cubePrefab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__cubePrefab_5() { return &____cubePrefab_5; }
	inline void set__cubePrefab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____cubePrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&____cubePrefab_5), value);
	}

	inline static int32_t get_offset_of__spawn_6() { return static_cast<int32_t>(offsetof(SurroundCaptureDemo_tB198F0F3C3D917DCF75BF34B70C22318F1765EE6, ____spawn_6)); }
	inline bool get__spawn_6() const { return ____spawn_6; }
	inline bool* get_address_of__spawn_6() { return &____spawn_6; }
	inline void set__spawn_6(bool value)
	{
		____spawn_6 = value;
	}

	inline static int32_t get_offset_of__timer_9() { return static_cast<int32_t>(offsetof(SurroundCaptureDemo_tB198F0F3C3D917DCF75BF34B70C22318F1765EE6, ____timer_9)); }
	inline float get__timer_9() const { return ____timer_9; }
	inline float* get_address_of__timer_9() { return &____timer_9; }
	inline void set__timer_9(float value)
	{
		____timer_9 = value;
	}

	inline static int32_t get_offset_of__cubes_10() { return static_cast<int32_t>(offsetof(SurroundCaptureDemo_tB198F0F3C3D917DCF75BF34B70C22318F1765EE6, ____cubes_10)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get__cubes_10() const { return ____cubes_10; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of__cubes_10() { return &____cubes_10; }
	inline void set__cubes_10(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		____cubes_10 = value;
		Il2CppCodeGenWriteBarrier((&____cubes_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURROUNDCAPTUREDEMO_TB198F0F3C3D917DCF75BF34B70C22318F1765EE6_H
#ifndef TEXTURECAPTUREDEMO_T5FC711E8029DADD159F9138461CB8DBA42263236_H
#define TEXTURECAPTUREDEMO_T5FC711E8029DADD159F9138461CB8DBA42263236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.Demos.TextureCaptureDemo
struct  TextureCaptureDemo_t5FC711E8029DADD159F9138461CB8DBA42263236  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.Demos.TextureCaptureDemo::_textureWidth
	int32_t ____textureWidth_4;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.Demos.TextureCaptureDemo::_textureHeight
	int32_t ____textureHeight_5;
	// RenderHeads.Media.AVProMovieCapture.CaptureFromTexture RenderHeads.Media.AVProMovieCapture.Demos.TextureCaptureDemo::_movieCapture
	CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A * ____movieCapture_6;
	// UnityEngine.Material RenderHeads.Media.AVProMovieCapture.Demos.TextureCaptureDemo::_material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____material_7;
	// UnityEngine.RenderTexture RenderHeads.Media.AVProMovieCapture.Demos.TextureCaptureDemo::_texture
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____texture_8;

public:
	inline static int32_t get_offset_of__textureWidth_4() { return static_cast<int32_t>(offsetof(TextureCaptureDemo_t5FC711E8029DADD159F9138461CB8DBA42263236, ____textureWidth_4)); }
	inline int32_t get__textureWidth_4() const { return ____textureWidth_4; }
	inline int32_t* get_address_of__textureWidth_4() { return &____textureWidth_4; }
	inline void set__textureWidth_4(int32_t value)
	{
		____textureWidth_4 = value;
	}

	inline static int32_t get_offset_of__textureHeight_5() { return static_cast<int32_t>(offsetof(TextureCaptureDemo_t5FC711E8029DADD159F9138461CB8DBA42263236, ____textureHeight_5)); }
	inline int32_t get__textureHeight_5() const { return ____textureHeight_5; }
	inline int32_t* get_address_of__textureHeight_5() { return &____textureHeight_5; }
	inline void set__textureHeight_5(int32_t value)
	{
		____textureHeight_5 = value;
	}

	inline static int32_t get_offset_of__movieCapture_6() { return static_cast<int32_t>(offsetof(TextureCaptureDemo_t5FC711E8029DADD159F9138461CB8DBA42263236, ____movieCapture_6)); }
	inline CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A * get__movieCapture_6() const { return ____movieCapture_6; }
	inline CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A ** get_address_of__movieCapture_6() { return &____movieCapture_6; }
	inline void set__movieCapture_6(CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A * value)
	{
		____movieCapture_6 = value;
		Il2CppCodeGenWriteBarrier((&____movieCapture_6), value);
	}

	inline static int32_t get_offset_of__material_7() { return static_cast<int32_t>(offsetof(TextureCaptureDemo_t5FC711E8029DADD159F9138461CB8DBA42263236, ____material_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__material_7() const { return ____material_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__material_7() { return &____material_7; }
	inline void set__material_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____material_7 = value;
		Il2CppCodeGenWriteBarrier((&____material_7), value);
	}

	inline static int32_t get_offset_of__texture_8() { return static_cast<int32_t>(offsetof(TextureCaptureDemo_t5FC711E8029DADD159F9138461CB8DBA42263236, ____texture_8)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__texture_8() const { return ____texture_8; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__texture_8() { return &____texture_8; }
	inline void set__texture_8(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____texture_8 = value;
		Il2CppCodeGenWriteBarrier((&____texture_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURECAPTUREDEMO_T5FC711E8029DADD159F9138461CB8DBA42263236_H
#ifndef WEBCAMCAPTUREDEMO_TFB43EB9D0B2473CED686BF577E354D2DB32567DE_H
#define WEBCAMCAPTUREDEMO_TFB43EB9D0B2473CED686BF577E354D2DB32567DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.Demos.WebcamCaptureDemo
struct  WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GUISkin RenderHeads.Media.AVProMovieCapture.Demos.WebcamCaptureDemo::_skin
	GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * ____skin_4;
	// UnityEngine.GameObject RenderHeads.Media.AVProMovieCapture.Demos.WebcamCaptureDemo::_prefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____prefab_5;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.Demos.WebcamCaptureDemo::_webcamResolutionWidth
	int32_t ____webcamResolutionWidth_6;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.Demos.WebcamCaptureDemo::_webcamResolutionHeight
	int32_t ____webcamResolutionHeight_7;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.Demos.WebcamCaptureDemo::_webcamFrameRate
	int32_t ____webcamFrameRate_8;
	// RenderHeads.Media.AVProMovieCapture.Demos.WebcamCaptureDemo/Instance[] RenderHeads.Media.AVProMovieCapture.Demos.WebcamCaptureDemo::_instances
	InstanceU5BU5D_tB835FF8E7053C7FFABF84B76C0C085AF13CBBF47* ____instances_9;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.Demos.WebcamCaptureDemo::_selectedWebcamIndex
	int32_t ____selectedWebcamIndex_10;

public:
	inline static int32_t get_offset_of__skin_4() { return static_cast<int32_t>(offsetof(WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE, ____skin_4)); }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * get__skin_4() const { return ____skin_4; }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 ** get_address_of__skin_4() { return &____skin_4; }
	inline void set__skin_4(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * value)
	{
		____skin_4 = value;
		Il2CppCodeGenWriteBarrier((&____skin_4), value);
	}

	inline static int32_t get_offset_of__prefab_5() { return static_cast<int32_t>(offsetof(WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE, ____prefab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__prefab_5() const { return ____prefab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__prefab_5() { return &____prefab_5; }
	inline void set__prefab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____prefab_5 = value;
		Il2CppCodeGenWriteBarrier((&____prefab_5), value);
	}

	inline static int32_t get_offset_of__webcamResolutionWidth_6() { return static_cast<int32_t>(offsetof(WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE, ____webcamResolutionWidth_6)); }
	inline int32_t get__webcamResolutionWidth_6() const { return ____webcamResolutionWidth_6; }
	inline int32_t* get_address_of__webcamResolutionWidth_6() { return &____webcamResolutionWidth_6; }
	inline void set__webcamResolutionWidth_6(int32_t value)
	{
		____webcamResolutionWidth_6 = value;
	}

	inline static int32_t get_offset_of__webcamResolutionHeight_7() { return static_cast<int32_t>(offsetof(WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE, ____webcamResolutionHeight_7)); }
	inline int32_t get__webcamResolutionHeight_7() const { return ____webcamResolutionHeight_7; }
	inline int32_t* get_address_of__webcamResolutionHeight_7() { return &____webcamResolutionHeight_7; }
	inline void set__webcamResolutionHeight_7(int32_t value)
	{
		____webcamResolutionHeight_7 = value;
	}

	inline static int32_t get_offset_of__webcamFrameRate_8() { return static_cast<int32_t>(offsetof(WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE, ____webcamFrameRate_8)); }
	inline int32_t get__webcamFrameRate_8() const { return ____webcamFrameRate_8; }
	inline int32_t* get_address_of__webcamFrameRate_8() { return &____webcamFrameRate_8; }
	inline void set__webcamFrameRate_8(int32_t value)
	{
		____webcamFrameRate_8 = value;
	}

	inline static int32_t get_offset_of__instances_9() { return static_cast<int32_t>(offsetof(WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE, ____instances_9)); }
	inline InstanceU5BU5D_tB835FF8E7053C7FFABF84B76C0C085AF13CBBF47* get__instances_9() const { return ____instances_9; }
	inline InstanceU5BU5D_tB835FF8E7053C7FFABF84B76C0C085AF13CBBF47** get_address_of__instances_9() { return &____instances_9; }
	inline void set__instances_9(InstanceU5BU5D_tB835FF8E7053C7FFABF84B76C0C085AF13CBBF47* value)
	{
		____instances_9 = value;
		Il2CppCodeGenWriteBarrier((&____instances_9), value);
	}

	inline static int32_t get_offset_of__selectedWebcamIndex_10() { return static_cast<int32_t>(offsetof(WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE, ____selectedWebcamIndex_10)); }
	inline int32_t get__selectedWebcamIndex_10() const { return ____selectedWebcamIndex_10; }
	inline int32_t* get_address_of__selectedWebcamIndex_10() { return &____selectedWebcamIndex_10; }
	inline void set__selectedWebcamIndex_10(int32_t value)
	{
		____selectedWebcamIndex_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMCAPTUREDEMO_TFB43EB9D0B2473CED686BF577E354D2DB32567DE_H
#ifndef MOTIONBLUR_TC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7_H
#define MOTIONBLUR_TC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.MotionBlur
struct  MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RenderTextureFormat RenderHeads.Media.AVProMovieCapture.MotionBlur::_format
	int32_t ____format_4;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.MotionBlur::_numSamples
	int32_t ____numSamples_5;
	// UnityEngine.RenderTexture RenderHeads.Media.AVProMovieCapture.MotionBlur::_accum
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____accum_6;
	// UnityEngine.RenderTexture RenderHeads.Media.AVProMovieCapture.MotionBlur::_lastComp
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____lastComp_7;
	// UnityEngine.Material RenderHeads.Media.AVProMovieCapture.MotionBlur::_addMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____addMaterial_8;
	// UnityEngine.Material RenderHeads.Media.AVProMovieCapture.MotionBlur::_divMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____divMaterial_9;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.MotionBlur::_frameCount
	int32_t ____frameCount_10;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.MotionBlur::_targetWidth
	int32_t ____targetWidth_11;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.MotionBlur::_targetHeight
	int32_t ____targetHeight_12;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.MotionBlur::_isDirty
	bool ____isDirty_13;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.MotionBlur::<IsFrameAccumulated>k__BackingField
	bool ___U3CIsFrameAccumulatedU3Ek__BackingField_16;
	// System.Single RenderHeads.Media.AVProMovieCapture.MotionBlur::_bias
	float ____bias_17;
	// System.Single RenderHeads.Media.AVProMovieCapture.MotionBlur::_total
	float ____total_18;

public:
	inline static int32_t get_offset_of__format_4() { return static_cast<int32_t>(offsetof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7, ____format_4)); }
	inline int32_t get__format_4() const { return ____format_4; }
	inline int32_t* get_address_of__format_4() { return &____format_4; }
	inline void set__format_4(int32_t value)
	{
		____format_4 = value;
	}

	inline static int32_t get_offset_of__numSamples_5() { return static_cast<int32_t>(offsetof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7, ____numSamples_5)); }
	inline int32_t get__numSamples_5() const { return ____numSamples_5; }
	inline int32_t* get_address_of__numSamples_5() { return &____numSamples_5; }
	inline void set__numSamples_5(int32_t value)
	{
		____numSamples_5 = value;
	}

	inline static int32_t get_offset_of__accum_6() { return static_cast<int32_t>(offsetof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7, ____accum_6)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__accum_6() const { return ____accum_6; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__accum_6() { return &____accum_6; }
	inline void set__accum_6(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____accum_6 = value;
		Il2CppCodeGenWriteBarrier((&____accum_6), value);
	}

	inline static int32_t get_offset_of__lastComp_7() { return static_cast<int32_t>(offsetof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7, ____lastComp_7)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__lastComp_7() const { return ____lastComp_7; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__lastComp_7() { return &____lastComp_7; }
	inline void set__lastComp_7(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____lastComp_7 = value;
		Il2CppCodeGenWriteBarrier((&____lastComp_7), value);
	}

	inline static int32_t get_offset_of__addMaterial_8() { return static_cast<int32_t>(offsetof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7, ____addMaterial_8)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__addMaterial_8() const { return ____addMaterial_8; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__addMaterial_8() { return &____addMaterial_8; }
	inline void set__addMaterial_8(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____addMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&____addMaterial_8), value);
	}

	inline static int32_t get_offset_of__divMaterial_9() { return static_cast<int32_t>(offsetof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7, ____divMaterial_9)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__divMaterial_9() const { return ____divMaterial_9; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__divMaterial_9() { return &____divMaterial_9; }
	inline void set__divMaterial_9(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____divMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&____divMaterial_9), value);
	}

	inline static int32_t get_offset_of__frameCount_10() { return static_cast<int32_t>(offsetof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7, ____frameCount_10)); }
	inline int32_t get__frameCount_10() const { return ____frameCount_10; }
	inline int32_t* get_address_of__frameCount_10() { return &____frameCount_10; }
	inline void set__frameCount_10(int32_t value)
	{
		____frameCount_10 = value;
	}

	inline static int32_t get_offset_of__targetWidth_11() { return static_cast<int32_t>(offsetof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7, ____targetWidth_11)); }
	inline int32_t get__targetWidth_11() const { return ____targetWidth_11; }
	inline int32_t* get_address_of__targetWidth_11() { return &____targetWidth_11; }
	inline void set__targetWidth_11(int32_t value)
	{
		____targetWidth_11 = value;
	}

	inline static int32_t get_offset_of__targetHeight_12() { return static_cast<int32_t>(offsetof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7, ____targetHeight_12)); }
	inline int32_t get__targetHeight_12() const { return ____targetHeight_12; }
	inline int32_t* get_address_of__targetHeight_12() { return &____targetHeight_12; }
	inline void set__targetHeight_12(int32_t value)
	{
		____targetHeight_12 = value;
	}

	inline static int32_t get_offset_of__isDirty_13() { return static_cast<int32_t>(offsetof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7, ____isDirty_13)); }
	inline bool get__isDirty_13() const { return ____isDirty_13; }
	inline bool* get_address_of__isDirty_13() { return &____isDirty_13; }
	inline void set__isDirty_13(bool value)
	{
		____isDirty_13 = value;
	}

	inline static int32_t get_offset_of_U3CIsFrameAccumulatedU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7, ___U3CIsFrameAccumulatedU3Ek__BackingField_16)); }
	inline bool get_U3CIsFrameAccumulatedU3Ek__BackingField_16() const { return ___U3CIsFrameAccumulatedU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CIsFrameAccumulatedU3Ek__BackingField_16() { return &___U3CIsFrameAccumulatedU3Ek__BackingField_16; }
	inline void set_U3CIsFrameAccumulatedU3Ek__BackingField_16(bool value)
	{
		___U3CIsFrameAccumulatedU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of__bias_17() { return static_cast<int32_t>(offsetof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7, ____bias_17)); }
	inline float get__bias_17() const { return ____bias_17; }
	inline float* get_address_of__bias_17() { return &____bias_17; }
	inline void set__bias_17(float value)
	{
		____bias_17 = value;
	}

	inline static int32_t get_offset_of__total_18() { return static_cast<int32_t>(offsetof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7, ____total_18)); }
	inline float get__total_18() const { return ____total_18; }
	inline float* get_address_of__total_18() { return &____total_18; }
	inline void set__total_18(float value)
	{
		____total_18 = value;
	}
};

struct MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7_StaticFields
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.MotionBlur::_propNumSamples
	int32_t ____propNumSamples_14;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.MotionBlur::_propWeight
	int32_t ____propWeight_15;

public:
	inline static int32_t get_offset_of__propNumSamples_14() { return static_cast<int32_t>(offsetof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7_StaticFields, ____propNumSamples_14)); }
	inline int32_t get__propNumSamples_14() const { return ____propNumSamples_14; }
	inline int32_t* get_address_of__propNumSamples_14() { return &____propNumSamples_14; }
	inline void set__propNumSamples_14(int32_t value)
	{
		____propNumSamples_14 = value;
	}

	inline static int32_t get_offset_of__propWeight_15() { return static_cast<int32_t>(offsetof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7_StaticFields, ____propWeight_15)); }
	inline int32_t get__propWeight_15() const { return ____propWeight_15; }
	inline int32_t* get_address_of__propWeight_15() { return &____propWeight_15; }
	inline void set__propWeight_15(int32_t value)
	{
		____propWeight_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLUR_TC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7_H
#ifndef MOUSECURSOR_T2D59C0E32A66F12984B60C09F6FECBAB53D490DD_H
#define MOUSECURSOR_T2D59C0E32A66F12984B60C09F6FECBAB53D490DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.MouseCursor
struct  MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture2D RenderHeads.Media.AVProMovieCapture.MouseCursor::_texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ____texture_4;
	// UnityEngine.Vector2 RenderHeads.Media.AVProMovieCapture.MouseCursor::_hotspotOffset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____hotspotOffset_5;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.MouseCursor::_sizeScale
	int32_t ____sizeScale_6;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.MouseCursor::_depth
	int32_t ____depth_7;
	// UnityEngine.GUIContent RenderHeads.Media.AVProMovieCapture.MouseCursor::_content
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ____content_8;

public:
	inline static int32_t get_offset_of__texture_4() { return static_cast<int32_t>(offsetof(MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD, ____texture_4)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get__texture_4() const { return ____texture_4; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of__texture_4() { return &____texture_4; }
	inline void set__texture_4(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		____texture_4 = value;
		Il2CppCodeGenWriteBarrier((&____texture_4), value);
	}

	inline static int32_t get_offset_of__hotspotOffset_5() { return static_cast<int32_t>(offsetof(MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD, ____hotspotOffset_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__hotspotOffset_5() const { return ____hotspotOffset_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__hotspotOffset_5() { return &____hotspotOffset_5; }
	inline void set__hotspotOffset_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____hotspotOffset_5 = value;
	}

	inline static int32_t get_offset_of__sizeScale_6() { return static_cast<int32_t>(offsetof(MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD, ____sizeScale_6)); }
	inline int32_t get__sizeScale_6() const { return ____sizeScale_6; }
	inline int32_t* get_address_of__sizeScale_6() { return &____sizeScale_6; }
	inline void set__sizeScale_6(int32_t value)
	{
		____sizeScale_6 = value;
	}

	inline static int32_t get_offset_of__depth_7() { return static_cast<int32_t>(offsetof(MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD, ____depth_7)); }
	inline int32_t get__depth_7() const { return ____depth_7; }
	inline int32_t* get_address_of__depth_7() { return &____depth_7; }
	inline void set__depth_7(int32_t value)
	{
		____depth_7 = value;
	}

	inline static int32_t get_offset_of__content_8() { return static_cast<int32_t>(offsetof(MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD, ____content_8)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get__content_8() const { return ____content_8; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of__content_8() { return &____content_8; }
	inline void set__content_8(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		____content_8 = value;
		Il2CppCodeGenWriteBarrier((&____content_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSECURSOR_T2D59C0E32A66F12984B60C09F6FECBAB53D490DD_H
#ifndef UNITYAUDIOCAPTURE_T6C1B0B749EAB3336BABE138B59C0391C29BCFCB5_H
#define UNITYAUDIOCAPTURE_T6C1B0B749EAB3336BABE138B59C0391C29BCFCB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.UnityAudioCapture
struct  UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean RenderHeads.Media.AVProMovieCapture.UnityAudioCapture::_debugLogging
	bool ____debugLogging_4;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.UnityAudioCapture::_muteAudio
	bool ____muteAudio_5;
	// System.Single[] RenderHeads.Media.AVProMovieCapture.UnityAudioCapture::_buffer
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____buffer_7;
	// System.Single[] RenderHeads.Media.AVProMovieCapture.UnityAudioCapture::_readBuffer
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____readBuffer_8;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.UnityAudioCapture::_bufferIndex
	int32_t ____bufferIndex_9;
	// System.Runtime.InteropServices.GCHandle RenderHeads.Media.AVProMovieCapture.UnityAudioCapture::_bufferHandle
	GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  ____bufferHandle_10;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.UnityAudioCapture::_numChannels
	int32_t ____numChannels_11;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.UnityAudioCapture::_overflowCount
	int32_t ____overflowCount_12;
	// System.Object RenderHeads.Media.AVProMovieCapture.UnityAudioCapture::_lockObject
	RuntimeObject * ____lockObject_13;

public:
	inline static int32_t get_offset_of__debugLogging_4() { return static_cast<int32_t>(offsetof(UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5, ____debugLogging_4)); }
	inline bool get__debugLogging_4() const { return ____debugLogging_4; }
	inline bool* get_address_of__debugLogging_4() { return &____debugLogging_4; }
	inline void set__debugLogging_4(bool value)
	{
		____debugLogging_4 = value;
	}

	inline static int32_t get_offset_of__muteAudio_5() { return static_cast<int32_t>(offsetof(UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5, ____muteAudio_5)); }
	inline bool get__muteAudio_5() const { return ____muteAudio_5; }
	inline bool* get_address_of__muteAudio_5() { return &____muteAudio_5; }
	inline void set__muteAudio_5(bool value)
	{
		____muteAudio_5 = value;
	}

	inline static int32_t get_offset_of__buffer_7() { return static_cast<int32_t>(offsetof(UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5, ____buffer_7)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__buffer_7() const { return ____buffer_7; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__buffer_7() { return &____buffer_7; }
	inline void set__buffer_7(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____buffer_7 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_7), value);
	}

	inline static int32_t get_offset_of__readBuffer_8() { return static_cast<int32_t>(offsetof(UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5, ____readBuffer_8)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__readBuffer_8() const { return ____readBuffer_8; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__readBuffer_8() { return &____readBuffer_8; }
	inline void set__readBuffer_8(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____readBuffer_8 = value;
		Il2CppCodeGenWriteBarrier((&____readBuffer_8), value);
	}

	inline static int32_t get_offset_of__bufferIndex_9() { return static_cast<int32_t>(offsetof(UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5, ____bufferIndex_9)); }
	inline int32_t get__bufferIndex_9() const { return ____bufferIndex_9; }
	inline int32_t* get_address_of__bufferIndex_9() { return &____bufferIndex_9; }
	inline void set__bufferIndex_9(int32_t value)
	{
		____bufferIndex_9 = value;
	}

	inline static int32_t get_offset_of__bufferHandle_10() { return static_cast<int32_t>(offsetof(UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5, ____bufferHandle_10)); }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  get__bufferHandle_10() const { return ____bufferHandle_10; }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 * get_address_of__bufferHandle_10() { return &____bufferHandle_10; }
	inline void set__bufferHandle_10(GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  value)
	{
		____bufferHandle_10 = value;
	}

	inline static int32_t get_offset_of__numChannels_11() { return static_cast<int32_t>(offsetof(UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5, ____numChannels_11)); }
	inline int32_t get__numChannels_11() const { return ____numChannels_11; }
	inline int32_t* get_address_of__numChannels_11() { return &____numChannels_11; }
	inline void set__numChannels_11(int32_t value)
	{
		____numChannels_11 = value;
	}

	inline static int32_t get_offset_of__overflowCount_12() { return static_cast<int32_t>(offsetof(UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5, ____overflowCount_12)); }
	inline int32_t get__overflowCount_12() const { return ____overflowCount_12; }
	inline int32_t* get_address_of__overflowCount_12() { return &____overflowCount_12; }
	inline void set__overflowCount_12(int32_t value)
	{
		____overflowCount_12 = value;
	}

	inline static int32_t get_offset_of__lockObject_13() { return static_cast<int32_t>(offsetof(UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5, ____lockObject_13)); }
	inline RuntimeObject * get__lockObject_13() const { return ____lockObject_13; }
	inline RuntimeObject ** get_address_of__lockObject_13() { return &____lockObject_13; }
	inline void set__lockObject_13(RuntimeObject * value)
	{
		____lockObject_13 = value;
		Il2CppCodeGenWriteBarrier((&____lockObject_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYAUDIOCAPTURE_T6C1B0B749EAB3336BABE138B59C0391C29BCFCB5_H
#ifndef GLTFEXPORTERTEST_TCF2E0E259A8A633C82CDB2C99F65BF845BDA292D_H
#define GLTFEXPORTERTEST_TCF2E0E259A8A633C82CDB2C99F65BF845BDA292D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Examples.GLTFExporterTest
struct  GLTFExporterTest_tCF2E0E259A8A633C82CDB2C99F65BF845BDA292D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFEXPORTERTEST_TCF2E0E259A8A633C82CDB2C99F65BF845BDA292D_H
#ifndef MULTISCENECOMPONENT_TEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A_H
#define MULTISCENECOMPONENT_TEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Examples.MultiSceneComponent
struct  MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 UnityGLTF.Examples.MultiSceneComponent::SceneIndex
	int32_t ___SceneIndex_4;
	// System.String UnityGLTF.Examples.MultiSceneComponent::Url
	String_t* ___Url_5;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.Examples.MultiSceneComponent::_importer
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ____importer_6;
	// UnityGLTF.AsyncCoroutineHelper UnityGLTF.Examples.MultiSceneComponent::_asyncCoroutineHelper
	AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1 * ____asyncCoroutineHelper_7;
	// UnityGLTF.Loader.ILoader UnityGLTF.Examples.MultiSceneComponent::_loader
	RuntimeObject* ____loader_8;
	// System.String UnityGLTF.Examples.MultiSceneComponent::_fileName
	String_t* ____fileName_9;

public:
	inline static int32_t get_offset_of_SceneIndex_4() { return static_cast<int32_t>(offsetof(MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A, ___SceneIndex_4)); }
	inline int32_t get_SceneIndex_4() const { return ___SceneIndex_4; }
	inline int32_t* get_address_of_SceneIndex_4() { return &___SceneIndex_4; }
	inline void set_SceneIndex_4(int32_t value)
	{
		___SceneIndex_4 = value;
	}

	inline static int32_t get_offset_of_Url_5() { return static_cast<int32_t>(offsetof(MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A, ___Url_5)); }
	inline String_t* get_Url_5() const { return ___Url_5; }
	inline String_t** get_address_of_Url_5() { return &___Url_5; }
	inline void set_Url_5(String_t* value)
	{
		___Url_5 = value;
		Il2CppCodeGenWriteBarrier((&___Url_5), value);
	}

	inline static int32_t get_offset_of__importer_6() { return static_cast<int32_t>(offsetof(MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A, ____importer_6)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get__importer_6() const { return ____importer_6; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of__importer_6() { return &____importer_6; }
	inline void set__importer_6(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		____importer_6 = value;
		Il2CppCodeGenWriteBarrier((&____importer_6), value);
	}

	inline static int32_t get_offset_of__asyncCoroutineHelper_7() { return static_cast<int32_t>(offsetof(MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A, ____asyncCoroutineHelper_7)); }
	inline AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1 * get__asyncCoroutineHelper_7() const { return ____asyncCoroutineHelper_7; }
	inline AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1 ** get_address_of__asyncCoroutineHelper_7() { return &____asyncCoroutineHelper_7; }
	inline void set__asyncCoroutineHelper_7(AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1 * value)
	{
		____asyncCoroutineHelper_7 = value;
		Il2CppCodeGenWriteBarrier((&____asyncCoroutineHelper_7), value);
	}

	inline static int32_t get_offset_of__loader_8() { return static_cast<int32_t>(offsetof(MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A, ____loader_8)); }
	inline RuntimeObject* get__loader_8() const { return ____loader_8; }
	inline RuntimeObject** get_address_of__loader_8() { return &____loader_8; }
	inline void set__loader_8(RuntimeObject* value)
	{
		____loader_8 = value;
		Il2CppCodeGenWriteBarrier((&____loader_8), value);
	}

	inline static int32_t get_offset_of__fileName_9() { return static_cast<int32_t>(offsetof(MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A, ____fileName_9)); }
	inline String_t* get__fileName_9() const { return ____fileName_9; }
	inline String_t** get_address_of__fileName_9() { return &____fileName_9; }
	inline void set__fileName_9(String_t* value)
	{
		____fileName_9 = value;
		Il2CppCodeGenWriteBarrier((&____fileName_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTISCENECOMPONENT_TEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A_H
#ifndef ORBITCAMERACONTROLLER_T69B5196542BC4F2CE282AF8A903F95405CE127E8_H
#define ORBITCAMERACONTROLLER_T69B5196542BC4F2CE282AF8A903F95405CE127E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Examples.OrbitCameraController
struct  OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform UnityGLTF.Examples.OrbitCameraController::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_4;
	// System.Single UnityGLTF.Examples.OrbitCameraController::distance
	float ___distance_5;
	// System.Single UnityGLTF.Examples.OrbitCameraController::xSpeed
	float ___xSpeed_6;
	// System.Single UnityGLTF.Examples.OrbitCameraController::ySpeed
	float ___ySpeed_7;
	// System.Single UnityGLTF.Examples.OrbitCameraController::yMinLimit
	float ___yMinLimit_8;
	// System.Single UnityGLTF.Examples.OrbitCameraController::yMaxLimit
	float ___yMaxLimit_9;
	// System.Single UnityGLTF.Examples.OrbitCameraController::distanceMin
	float ___distanceMin_10;
	// System.Single UnityGLTF.Examples.OrbitCameraController::distanceMax
	float ___distanceMax_11;
	// UnityEngine.Rigidbody UnityGLTF.Examples.OrbitCameraController::cameraRigidBody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___cameraRigidBody_12;
	// System.Single UnityGLTF.Examples.OrbitCameraController::x
	float ___x_13;
	// System.Single UnityGLTF.Examples.OrbitCameraController::y
	float ___y_14;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8, ___target_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_4() const { return ___target_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8, ___distance_5)); }
	inline float get_distance_5() const { return ___distance_5; }
	inline float* get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(float value)
	{
		___distance_5 = value;
	}

	inline static int32_t get_offset_of_xSpeed_6() { return static_cast<int32_t>(offsetof(OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8, ___xSpeed_6)); }
	inline float get_xSpeed_6() const { return ___xSpeed_6; }
	inline float* get_address_of_xSpeed_6() { return &___xSpeed_6; }
	inline void set_xSpeed_6(float value)
	{
		___xSpeed_6 = value;
	}

	inline static int32_t get_offset_of_ySpeed_7() { return static_cast<int32_t>(offsetof(OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8, ___ySpeed_7)); }
	inline float get_ySpeed_7() const { return ___ySpeed_7; }
	inline float* get_address_of_ySpeed_7() { return &___ySpeed_7; }
	inline void set_ySpeed_7(float value)
	{
		___ySpeed_7 = value;
	}

	inline static int32_t get_offset_of_yMinLimit_8() { return static_cast<int32_t>(offsetof(OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8, ___yMinLimit_8)); }
	inline float get_yMinLimit_8() const { return ___yMinLimit_8; }
	inline float* get_address_of_yMinLimit_8() { return &___yMinLimit_8; }
	inline void set_yMinLimit_8(float value)
	{
		___yMinLimit_8 = value;
	}

	inline static int32_t get_offset_of_yMaxLimit_9() { return static_cast<int32_t>(offsetof(OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8, ___yMaxLimit_9)); }
	inline float get_yMaxLimit_9() const { return ___yMaxLimit_9; }
	inline float* get_address_of_yMaxLimit_9() { return &___yMaxLimit_9; }
	inline void set_yMaxLimit_9(float value)
	{
		___yMaxLimit_9 = value;
	}

	inline static int32_t get_offset_of_distanceMin_10() { return static_cast<int32_t>(offsetof(OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8, ___distanceMin_10)); }
	inline float get_distanceMin_10() const { return ___distanceMin_10; }
	inline float* get_address_of_distanceMin_10() { return &___distanceMin_10; }
	inline void set_distanceMin_10(float value)
	{
		___distanceMin_10 = value;
	}

	inline static int32_t get_offset_of_distanceMax_11() { return static_cast<int32_t>(offsetof(OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8, ___distanceMax_11)); }
	inline float get_distanceMax_11() const { return ___distanceMax_11; }
	inline float* get_address_of_distanceMax_11() { return &___distanceMax_11; }
	inline void set_distanceMax_11(float value)
	{
		___distanceMax_11 = value;
	}

	inline static int32_t get_offset_of_cameraRigidBody_12() { return static_cast<int32_t>(offsetof(OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8, ___cameraRigidBody_12)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_cameraRigidBody_12() const { return ___cameraRigidBody_12; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_cameraRigidBody_12() { return &___cameraRigidBody_12; }
	inline void set_cameraRigidBody_12(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___cameraRigidBody_12 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRigidBody_12), value);
	}

	inline static int32_t get_offset_of_x_13() { return static_cast<int32_t>(offsetof(OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8, ___x_13)); }
	inline float get_x_13() const { return ___x_13; }
	inline float* get_address_of_x_13() { return &___x_13; }
	inline void set_x_13(float value)
	{
		___x_13 = value;
	}

	inline static int32_t get_offset_of_y_14() { return static_cast<int32_t>(offsetof(OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8, ___y_14)); }
	inline float get_y_14() const { return ___y_14; }
	inline float* get_address_of_y_14() { return &___y_14; }
	inline void set_y_14(float value)
	{
		___y_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORBITCAMERACONTROLLER_T69B5196542BC4F2CE282AF8A903F95405CE127E8_H
#ifndef VISUALIZEMESHATTRIBUTES_T45AB97CFD9146F783AA179220C931B8B65B31498_H
#define VISUALIZEMESHATTRIBUTES_T45AB97CFD9146F783AA179220C931B8B65B31498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Examples.VisualizeMeshAttributes
struct  VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.MeshFilter UnityGLTF.Examples.VisualizeMeshAttributes::Mesh
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___Mesh_4;
	// System.Single UnityGLTF.Examples.VisualizeMeshAttributes::NormalScale
	float ___NormalScale_5;
	// System.Single UnityGLTF.Examples.VisualizeMeshAttributes::TangentScale
	float ___TangentScale_6;
	// System.Boolean UnityGLTF.Examples.VisualizeMeshAttributes::VisualizeTangents
	bool ___VisualizeTangents_7;
	// System.Boolean UnityGLTF.Examples.VisualizeMeshAttributes::VisualizeNormals
	bool ___VisualizeNormals_8;
	// UnityEngine.Vector3[] UnityGLTF.Examples.VisualizeMeshAttributes::vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___vertices_9;
	// UnityEngine.Vector3[] UnityGLTF.Examples.VisualizeMeshAttributes::normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___normals_10;
	// UnityEngine.Vector4[] UnityGLTF.Examples.VisualizeMeshAttributes::tangents
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___tangents_11;

public:
	inline static int32_t get_offset_of_Mesh_4() { return static_cast<int32_t>(offsetof(VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498, ___Mesh_4)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_Mesh_4() const { return ___Mesh_4; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_Mesh_4() { return &___Mesh_4; }
	inline void set_Mesh_4(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___Mesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___Mesh_4), value);
	}

	inline static int32_t get_offset_of_NormalScale_5() { return static_cast<int32_t>(offsetof(VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498, ___NormalScale_5)); }
	inline float get_NormalScale_5() const { return ___NormalScale_5; }
	inline float* get_address_of_NormalScale_5() { return &___NormalScale_5; }
	inline void set_NormalScale_5(float value)
	{
		___NormalScale_5 = value;
	}

	inline static int32_t get_offset_of_TangentScale_6() { return static_cast<int32_t>(offsetof(VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498, ___TangentScale_6)); }
	inline float get_TangentScale_6() const { return ___TangentScale_6; }
	inline float* get_address_of_TangentScale_6() { return &___TangentScale_6; }
	inline void set_TangentScale_6(float value)
	{
		___TangentScale_6 = value;
	}

	inline static int32_t get_offset_of_VisualizeTangents_7() { return static_cast<int32_t>(offsetof(VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498, ___VisualizeTangents_7)); }
	inline bool get_VisualizeTangents_7() const { return ___VisualizeTangents_7; }
	inline bool* get_address_of_VisualizeTangents_7() { return &___VisualizeTangents_7; }
	inline void set_VisualizeTangents_7(bool value)
	{
		___VisualizeTangents_7 = value;
	}

	inline static int32_t get_offset_of_VisualizeNormals_8() { return static_cast<int32_t>(offsetof(VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498, ___VisualizeNormals_8)); }
	inline bool get_VisualizeNormals_8() const { return ___VisualizeNormals_8; }
	inline bool* get_address_of_VisualizeNormals_8() { return &___VisualizeNormals_8; }
	inline void set_VisualizeNormals_8(bool value)
	{
		___VisualizeNormals_8 = value;
	}

	inline static int32_t get_offset_of_vertices_9() { return static_cast<int32_t>(offsetof(VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498, ___vertices_9)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_vertices_9() const { return ___vertices_9; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_vertices_9() { return &___vertices_9; }
	inline void set_vertices_9(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___vertices_9 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_9), value);
	}

	inline static int32_t get_offset_of_normals_10() { return static_cast<int32_t>(offsetof(VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498, ___normals_10)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_normals_10() const { return ___normals_10; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_normals_10() { return &___normals_10; }
	inline void set_normals_10(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___normals_10 = value;
		Il2CppCodeGenWriteBarrier((&___normals_10), value);
	}

	inline static int32_t get_offset_of_tangents_11() { return static_cast<int32_t>(offsetof(VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498, ___tangents_11)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_tangents_11() const { return ___tangents_11; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_tangents_11() { return &___tangents_11; }
	inline void set_tangents_11(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___tangents_11 = value;
		Il2CppCodeGenWriteBarrier((&___tangents_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISUALIZEMESHATTRIBUTES_T45AB97CFD9146F783AA179220C931B8B65B31498_H
#ifndef WEBSERVERCOMPONENT_T7A46F6D96CD661321551B51D34A54738B8DEA4AE_H
#define WEBSERVERCOMPONENT_T7A46F6D96CD661321551B51D34A54738B8DEA4AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Examples.WebServerComponent
struct  WebServerComponent_t7A46F6D96CD661321551B51D34A54738B8DEA4AE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityGLTF.Examples.SimpleHTTPServer UnityGLTF.Examples.WebServerComponent::_server
	SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A * ____server_4;

public:
	inline static int32_t get_offset_of__server_4() { return static_cast<int32_t>(offsetof(WebServerComponent_t7A46F6D96CD661321551B51D34A54738B8DEA4AE, ____server_4)); }
	inline SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A * get__server_4() const { return ____server_4; }
	inline SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A ** get_address_of__server_4() { return &____server_4; }
	inline void set__server_4(SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A * value)
	{
		____server_4 = value;
		Il2CppCodeGenWriteBarrier((&____server_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSERVERCOMPONENT_T7A46F6D96CD661321551B51D34A54738B8DEA4AE_H
#ifndef INSTANTIATEDGLTFOBJECT_T4581743C1D92EE3AD3C85A33000CEB647068E440_H
#define INSTANTIATEDGLTFOBJECT_T4581743C1D92EE3AD3C85A33000CEB647068E440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.InstantiatedGLTFObject
struct  InstantiatedGLTFObject_t4581743C1D92EE3AD3C85A33000CEB647068E440  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityGLTF.Cache.RefCountedCacheData UnityGLTF.InstantiatedGLTFObject::cachedData
	RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313 * ___cachedData_4;

public:
	inline static int32_t get_offset_of_cachedData_4() { return static_cast<int32_t>(offsetof(InstantiatedGLTFObject_t4581743C1D92EE3AD3C85A33000CEB647068E440, ___cachedData_4)); }
	inline RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313 * get_cachedData_4() const { return ___cachedData_4; }
	inline RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313 ** get_address_of_cachedData_4() { return &___cachedData_4; }
	inline void set_cachedData_4(RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313 * value)
	{
		___cachedData_4 = value;
		Il2CppCodeGenWriteBarrier((&___cachedData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTIATEDGLTFOBJECT_T4581743C1D92EE3AD3C85A33000CEB647068E440_H
#ifndef CAPTUREFROMCAMERA_T23E07AE5656F95A80984C97BACFEF42487521C01_H
#define CAPTUREFROMCAMERA_T23E07AE5656F95A80984C97BACFEF42487521C01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureFromCamera
struct  CaptureFromCamera_t23E07AE5656F95A80984C97BACFEF42487521C01  : public CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110
{
public:
	// UnityEngine.Camera RenderHeads.Media.AVProMovieCapture.CaptureFromCamera::_lastCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ____lastCamera_80;
	// UnityEngine.Camera[] RenderHeads.Media.AVProMovieCapture.CaptureFromCamera::_contribCameras
	CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9* ____contribCameras_81;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureFromCamera::_useContributingCameras
	bool ____useContributingCameras_82;
	// UnityEngine.RenderTexture RenderHeads.Media.AVProMovieCapture.CaptureFromCamera::_target
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____target_83;
	// System.IntPtr RenderHeads.Media.AVProMovieCapture.CaptureFromCamera::_targetNativePointer
	intptr_t ____targetNativePointer_84;

public:
	inline static int32_t get_offset_of__lastCamera_80() { return static_cast<int32_t>(offsetof(CaptureFromCamera_t23E07AE5656F95A80984C97BACFEF42487521C01, ____lastCamera_80)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get__lastCamera_80() const { return ____lastCamera_80; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of__lastCamera_80() { return &____lastCamera_80; }
	inline void set__lastCamera_80(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		____lastCamera_80 = value;
		Il2CppCodeGenWriteBarrier((&____lastCamera_80), value);
	}

	inline static int32_t get_offset_of__contribCameras_81() { return static_cast<int32_t>(offsetof(CaptureFromCamera_t23E07AE5656F95A80984C97BACFEF42487521C01, ____contribCameras_81)); }
	inline CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9* get__contribCameras_81() const { return ____contribCameras_81; }
	inline CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9** get_address_of__contribCameras_81() { return &____contribCameras_81; }
	inline void set__contribCameras_81(CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9* value)
	{
		____contribCameras_81 = value;
		Il2CppCodeGenWriteBarrier((&____contribCameras_81), value);
	}

	inline static int32_t get_offset_of__useContributingCameras_82() { return static_cast<int32_t>(offsetof(CaptureFromCamera_t23E07AE5656F95A80984C97BACFEF42487521C01, ____useContributingCameras_82)); }
	inline bool get__useContributingCameras_82() const { return ____useContributingCameras_82; }
	inline bool* get_address_of__useContributingCameras_82() { return &____useContributingCameras_82; }
	inline void set__useContributingCameras_82(bool value)
	{
		____useContributingCameras_82 = value;
	}

	inline static int32_t get_offset_of__target_83() { return static_cast<int32_t>(offsetof(CaptureFromCamera_t23E07AE5656F95A80984C97BACFEF42487521C01, ____target_83)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__target_83() const { return ____target_83; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__target_83() { return &____target_83; }
	inline void set__target_83(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____target_83 = value;
		Il2CppCodeGenWriteBarrier((&____target_83), value);
	}

	inline static int32_t get_offset_of__targetNativePointer_84() { return static_cast<int32_t>(offsetof(CaptureFromCamera_t23E07AE5656F95A80984C97BACFEF42487521C01, ____targetNativePointer_84)); }
	inline intptr_t get__targetNativePointer_84() const { return ____targetNativePointer_84; }
	inline intptr_t* get_address_of__targetNativePointer_84() { return &____targetNativePointer_84; }
	inline void set__targetNativePointer_84(intptr_t value)
	{
		____targetNativePointer_84 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREFROMCAMERA_T23E07AE5656F95A80984C97BACFEF42487521C01_H
#ifndef CAPTUREFROMCAMERA360_TF44D0F180EE13527D6280B1C97255FA7920C9751_H
#define CAPTUREFROMCAMERA360_TF44D0F180EE13527D6280B1C97255FA7920C9751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360
struct  CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751  : public CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110
{
public:
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360::_cubemapResolution
	int32_t ____cubemapResolution_80;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360::_cubemapDepth
	int32_t ____cubemapDepth_81;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360::_supportGUI
	bool ____supportGUI_82;
	// System.Boolean RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360::_supportCameraRotation
	bool ____supportCameraRotation_83;
	// RenderHeads.Media.AVProMovieCapture.StereoPacking RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360::_stereoRendering
	int32_t ____stereoRendering_84;
	// System.Single RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360::_ipd
	float ____ipd_85;
	// UnityEngine.Camera RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360::_camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ____camera_86;
	// UnityEngine.RenderTexture RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360::_faceTarget
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____faceTarget_87;
	// UnityEngine.Material RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360::_blitMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____blitMaterial_88;
	// UnityEngine.Material RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360::_cubemapToEquirectangularMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____cubemapToEquirectangularMaterial_89;
	// UnityEngine.RenderTexture RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360::_cubeTarget
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____cubeTarget_90;
	// UnityEngine.RenderTexture RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360::_finalTarget
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____finalTarget_91;
	// System.IntPtr RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360::_targetNativePointer
	intptr_t ____targetNativePointer_92;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360::_propFlipX
	int32_t ____propFlipX_93;

public:
	inline static int32_t get_offset_of__cubemapResolution_80() { return static_cast<int32_t>(offsetof(CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751, ____cubemapResolution_80)); }
	inline int32_t get__cubemapResolution_80() const { return ____cubemapResolution_80; }
	inline int32_t* get_address_of__cubemapResolution_80() { return &____cubemapResolution_80; }
	inline void set__cubemapResolution_80(int32_t value)
	{
		____cubemapResolution_80 = value;
	}

	inline static int32_t get_offset_of__cubemapDepth_81() { return static_cast<int32_t>(offsetof(CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751, ____cubemapDepth_81)); }
	inline int32_t get__cubemapDepth_81() const { return ____cubemapDepth_81; }
	inline int32_t* get_address_of__cubemapDepth_81() { return &____cubemapDepth_81; }
	inline void set__cubemapDepth_81(int32_t value)
	{
		____cubemapDepth_81 = value;
	}

	inline static int32_t get_offset_of__supportGUI_82() { return static_cast<int32_t>(offsetof(CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751, ____supportGUI_82)); }
	inline bool get__supportGUI_82() const { return ____supportGUI_82; }
	inline bool* get_address_of__supportGUI_82() { return &____supportGUI_82; }
	inline void set__supportGUI_82(bool value)
	{
		____supportGUI_82 = value;
	}

	inline static int32_t get_offset_of__supportCameraRotation_83() { return static_cast<int32_t>(offsetof(CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751, ____supportCameraRotation_83)); }
	inline bool get__supportCameraRotation_83() const { return ____supportCameraRotation_83; }
	inline bool* get_address_of__supportCameraRotation_83() { return &____supportCameraRotation_83; }
	inline void set__supportCameraRotation_83(bool value)
	{
		____supportCameraRotation_83 = value;
	}

	inline static int32_t get_offset_of__stereoRendering_84() { return static_cast<int32_t>(offsetof(CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751, ____stereoRendering_84)); }
	inline int32_t get__stereoRendering_84() const { return ____stereoRendering_84; }
	inline int32_t* get_address_of__stereoRendering_84() { return &____stereoRendering_84; }
	inline void set__stereoRendering_84(int32_t value)
	{
		____stereoRendering_84 = value;
	}

	inline static int32_t get_offset_of__ipd_85() { return static_cast<int32_t>(offsetof(CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751, ____ipd_85)); }
	inline float get__ipd_85() const { return ____ipd_85; }
	inline float* get_address_of__ipd_85() { return &____ipd_85; }
	inline void set__ipd_85(float value)
	{
		____ipd_85 = value;
	}

	inline static int32_t get_offset_of__camera_86() { return static_cast<int32_t>(offsetof(CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751, ____camera_86)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get__camera_86() const { return ____camera_86; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of__camera_86() { return &____camera_86; }
	inline void set__camera_86(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		____camera_86 = value;
		Il2CppCodeGenWriteBarrier((&____camera_86), value);
	}

	inline static int32_t get_offset_of__faceTarget_87() { return static_cast<int32_t>(offsetof(CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751, ____faceTarget_87)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__faceTarget_87() const { return ____faceTarget_87; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__faceTarget_87() { return &____faceTarget_87; }
	inline void set__faceTarget_87(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____faceTarget_87 = value;
		Il2CppCodeGenWriteBarrier((&____faceTarget_87), value);
	}

	inline static int32_t get_offset_of__blitMaterial_88() { return static_cast<int32_t>(offsetof(CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751, ____blitMaterial_88)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__blitMaterial_88() const { return ____blitMaterial_88; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__blitMaterial_88() { return &____blitMaterial_88; }
	inline void set__blitMaterial_88(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____blitMaterial_88 = value;
		Il2CppCodeGenWriteBarrier((&____blitMaterial_88), value);
	}

	inline static int32_t get_offset_of__cubemapToEquirectangularMaterial_89() { return static_cast<int32_t>(offsetof(CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751, ____cubemapToEquirectangularMaterial_89)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__cubemapToEquirectangularMaterial_89() const { return ____cubemapToEquirectangularMaterial_89; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__cubemapToEquirectangularMaterial_89() { return &____cubemapToEquirectangularMaterial_89; }
	inline void set__cubemapToEquirectangularMaterial_89(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____cubemapToEquirectangularMaterial_89 = value;
		Il2CppCodeGenWriteBarrier((&____cubemapToEquirectangularMaterial_89), value);
	}

	inline static int32_t get_offset_of__cubeTarget_90() { return static_cast<int32_t>(offsetof(CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751, ____cubeTarget_90)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__cubeTarget_90() const { return ____cubeTarget_90; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__cubeTarget_90() { return &____cubeTarget_90; }
	inline void set__cubeTarget_90(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____cubeTarget_90 = value;
		Il2CppCodeGenWriteBarrier((&____cubeTarget_90), value);
	}

	inline static int32_t get_offset_of__finalTarget_91() { return static_cast<int32_t>(offsetof(CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751, ____finalTarget_91)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__finalTarget_91() const { return ____finalTarget_91; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__finalTarget_91() { return &____finalTarget_91; }
	inline void set__finalTarget_91(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____finalTarget_91 = value;
		Il2CppCodeGenWriteBarrier((&____finalTarget_91), value);
	}

	inline static int32_t get_offset_of__targetNativePointer_92() { return static_cast<int32_t>(offsetof(CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751, ____targetNativePointer_92)); }
	inline intptr_t get__targetNativePointer_92() const { return ____targetNativePointer_92; }
	inline intptr_t* get_address_of__targetNativePointer_92() { return &____targetNativePointer_92; }
	inline void set__targetNativePointer_92(intptr_t value)
	{
		____targetNativePointer_92 = value;
	}

	inline static int32_t get_offset_of__propFlipX_93() { return static_cast<int32_t>(offsetof(CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751, ____propFlipX_93)); }
	inline int32_t get__propFlipX_93() const { return ____propFlipX_93; }
	inline int32_t* get_address_of__propFlipX_93() { return &____propFlipX_93; }
	inline void set__propFlipX_93(int32_t value)
	{
		____propFlipX_93 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREFROMCAMERA360_TF44D0F180EE13527D6280B1C97255FA7920C9751_H
#ifndef CAPTUREFROMCAMERA360ODS_T0FE75290B51243A85A0E723850267B9348DA5C79_H
#define CAPTUREFROMCAMERA360ODS_T0FE75290B51243A85A0E723850267B9348DA5C79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS
struct  CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79  : public CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110
{
public:
	// RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS/Settings RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS::_settings
	Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E * ____settings_80;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS::_eyeWidth
	int32_t ____eyeWidth_81;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS::_eyeHeight
	int32_t ____eyeHeight_82;
	// UnityEngine.Transform RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS::_cameraGroup
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____cameraGroup_83;
	// UnityEngine.Camera RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS::_leftCameraTop
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ____leftCameraTop_84;
	// UnityEngine.Camera RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS::_leftCameraBot
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ____leftCameraBot_85;
	// UnityEngine.Camera RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS::_rightCameraTop
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ____rightCameraTop_86;
	// UnityEngine.Camera RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS::_rightCameraBot
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ____rightCameraBot_87;
	// UnityEngine.RenderTexture RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS::_final
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____final_88;
	// System.IntPtr RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS::_targetNativePointer
	intptr_t ____targetNativePointer_89;
	// UnityEngine.Material RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS::_finalMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____finalMaterial_90;
	// System.Int32 RenderHeads.Media.AVProMovieCapture.CaptureFromCamera360ODS::_propSliceCenter
	int32_t ____propSliceCenter_91;

public:
	inline static int32_t get_offset_of__settings_80() { return static_cast<int32_t>(offsetof(CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79, ____settings_80)); }
	inline Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E * get__settings_80() const { return ____settings_80; }
	inline Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E ** get_address_of__settings_80() { return &____settings_80; }
	inline void set__settings_80(Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E * value)
	{
		____settings_80 = value;
		Il2CppCodeGenWriteBarrier((&____settings_80), value);
	}

	inline static int32_t get_offset_of__eyeWidth_81() { return static_cast<int32_t>(offsetof(CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79, ____eyeWidth_81)); }
	inline int32_t get__eyeWidth_81() const { return ____eyeWidth_81; }
	inline int32_t* get_address_of__eyeWidth_81() { return &____eyeWidth_81; }
	inline void set__eyeWidth_81(int32_t value)
	{
		____eyeWidth_81 = value;
	}

	inline static int32_t get_offset_of__eyeHeight_82() { return static_cast<int32_t>(offsetof(CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79, ____eyeHeight_82)); }
	inline int32_t get__eyeHeight_82() const { return ____eyeHeight_82; }
	inline int32_t* get_address_of__eyeHeight_82() { return &____eyeHeight_82; }
	inline void set__eyeHeight_82(int32_t value)
	{
		____eyeHeight_82 = value;
	}

	inline static int32_t get_offset_of__cameraGroup_83() { return static_cast<int32_t>(offsetof(CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79, ____cameraGroup_83)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__cameraGroup_83() const { return ____cameraGroup_83; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__cameraGroup_83() { return &____cameraGroup_83; }
	inline void set__cameraGroup_83(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____cameraGroup_83 = value;
		Il2CppCodeGenWriteBarrier((&____cameraGroup_83), value);
	}

	inline static int32_t get_offset_of__leftCameraTop_84() { return static_cast<int32_t>(offsetof(CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79, ____leftCameraTop_84)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get__leftCameraTop_84() const { return ____leftCameraTop_84; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of__leftCameraTop_84() { return &____leftCameraTop_84; }
	inline void set__leftCameraTop_84(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		____leftCameraTop_84 = value;
		Il2CppCodeGenWriteBarrier((&____leftCameraTop_84), value);
	}

	inline static int32_t get_offset_of__leftCameraBot_85() { return static_cast<int32_t>(offsetof(CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79, ____leftCameraBot_85)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get__leftCameraBot_85() const { return ____leftCameraBot_85; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of__leftCameraBot_85() { return &____leftCameraBot_85; }
	inline void set__leftCameraBot_85(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		____leftCameraBot_85 = value;
		Il2CppCodeGenWriteBarrier((&____leftCameraBot_85), value);
	}

	inline static int32_t get_offset_of__rightCameraTop_86() { return static_cast<int32_t>(offsetof(CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79, ____rightCameraTop_86)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get__rightCameraTop_86() const { return ____rightCameraTop_86; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of__rightCameraTop_86() { return &____rightCameraTop_86; }
	inline void set__rightCameraTop_86(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		____rightCameraTop_86 = value;
		Il2CppCodeGenWriteBarrier((&____rightCameraTop_86), value);
	}

	inline static int32_t get_offset_of__rightCameraBot_87() { return static_cast<int32_t>(offsetof(CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79, ____rightCameraBot_87)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get__rightCameraBot_87() const { return ____rightCameraBot_87; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of__rightCameraBot_87() { return &____rightCameraBot_87; }
	inline void set__rightCameraBot_87(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		____rightCameraBot_87 = value;
		Il2CppCodeGenWriteBarrier((&____rightCameraBot_87), value);
	}

	inline static int32_t get_offset_of__final_88() { return static_cast<int32_t>(offsetof(CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79, ____final_88)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__final_88() const { return ____final_88; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__final_88() { return &____final_88; }
	inline void set__final_88(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____final_88 = value;
		Il2CppCodeGenWriteBarrier((&____final_88), value);
	}

	inline static int32_t get_offset_of__targetNativePointer_89() { return static_cast<int32_t>(offsetof(CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79, ____targetNativePointer_89)); }
	inline intptr_t get__targetNativePointer_89() const { return ____targetNativePointer_89; }
	inline intptr_t* get_address_of__targetNativePointer_89() { return &____targetNativePointer_89; }
	inline void set__targetNativePointer_89(intptr_t value)
	{
		____targetNativePointer_89 = value;
	}

	inline static int32_t get_offset_of__finalMaterial_90() { return static_cast<int32_t>(offsetof(CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79, ____finalMaterial_90)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__finalMaterial_90() const { return ____finalMaterial_90; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__finalMaterial_90() { return &____finalMaterial_90; }
	inline void set__finalMaterial_90(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____finalMaterial_90 = value;
		Il2CppCodeGenWriteBarrier((&____finalMaterial_90), value);
	}

	inline static int32_t get_offset_of__propSliceCenter_91() { return static_cast<int32_t>(offsetof(CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79, ____propSliceCenter_91)); }
	inline int32_t get__propSliceCenter_91() const { return ____propSliceCenter_91; }
	inline int32_t* get_address_of__propSliceCenter_91() { return &____propSliceCenter_91; }
	inline void set__propSliceCenter_91(int32_t value)
	{
		____propSliceCenter_91 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREFROMCAMERA360ODS_T0FE75290B51243A85A0E723850267B9348DA5C79_H
#ifndef CAPTUREFROMSCREEN_TEB468CAE6962D15FD6A3F718B595BDF64D70007D_H
#define CAPTUREFROMSCREEN_TEB468CAE6962D15FD6A3F718B595BDF64D70007D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureFromScreen
struct  CaptureFromScreen_tEB468CAE6962D15FD6A3F718B595BDF64D70007D  : public CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110
{
public:
	// UnityEngine.YieldInstruction RenderHeads.Media.AVProMovieCapture.CaptureFromScreen::_waitForEndOfFrame
	YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44 * ____waitForEndOfFrame_80;

public:
	inline static int32_t get_offset_of__waitForEndOfFrame_80() { return static_cast<int32_t>(offsetof(CaptureFromScreen_tEB468CAE6962D15FD6A3F718B595BDF64D70007D, ____waitForEndOfFrame_80)); }
	inline YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44 * get__waitForEndOfFrame_80() const { return ____waitForEndOfFrame_80; }
	inline YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44 ** get_address_of__waitForEndOfFrame_80() { return &____waitForEndOfFrame_80; }
	inline void set__waitForEndOfFrame_80(YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44 * value)
	{
		____waitForEndOfFrame_80 = value;
		Il2CppCodeGenWriteBarrier((&____waitForEndOfFrame_80), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREFROMSCREEN_TEB468CAE6962D15FD6A3F718B595BDF64D70007D_H
#ifndef CAPTUREFROMTEXTURE_T733D44D62743C52FA1410E45358967160B0C227A_H
#define CAPTUREFROMTEXTURE_T733D44D62743C52FA1410E45358967160B0C227A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderHeads.Media.AVProMovieCapture.CaptureFromTexture
struct  CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A  : public CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110
{
public:
	// UnityEngine.Texture RenderHeads.Media.AVProMovieCapture.CaptureFromTexture::_sourceTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ____sourceTexture_80;
	// UnityEngine.RenderTexture RenderHeads.Media.AVProMovieCapture.CaptureFromTexture::_renderTexture
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____renderTexture_81;
	// System.IntPtr RenderHeads.Media.AVProMovieCapture.CaptureFromTexture::_targetNativePointer
	intptr_t ____targetNativePointer_82;

public:
	inline static int32_t get_offset_of__sourceTexture_80() { return static_cast<int32_t>(offsetof(CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A, ____sourceTexture_80)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get__sourceTexture_80() const { return ____sourceTexture_80; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of__sourceTexture_80() { return &____sourceTexture_80; }
	inline void set__sourceTexture_80(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		____sourceTexture_80 = value;
		Il2CppCodeGenWriteBarrier((&____sourceTexture_80), value);
	}

	inline static int32_t get_offset_of__renderTexture_81() { return static_cast<int32_t>(offsetof(CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A, ____renderTexture_81)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__renderTexture_81() const { return ____renderTexture_81; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__renderTexture_81() { return &____renderTexture_81; }
	inline void set__renderTexture_81(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____renderTexture_81 = value;
		Il2CppCodeGenWriteBarrier((&____renderTexture_81), value);
	}

	inline static int32_t get_offset_of__targetNativePointer_82() { return static_cast<int32_t>(offsetof(CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A, ____targetNativePointer_82)); }
	inline intptr_t get__targetNativePointer_82() const { return ____targetNativePointer_82; }
	inline intptr_t* get_address_of__targetNativePointer_82() { return &____targetNativePointer_82; }
	inline void set__targetNativePointer_82(intptr_t value)
	{
		____targetNativePointer_82 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREFROMTEXTURE_T733D44D62743C52FA1410E45358967160B0C227A_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7800 = { sizeof (U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7800[7] = 
{
	U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128::get_offset_of_texture_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128::get_offset_of_textureIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128::get_offset_of_markGpuOnly_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadTextureAsyncU3Ed__80_t179C9BE3BDA17DD2EF8C4FEEE9BBADE22768E128::get_offset_of_U3CU3Eu__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7801 = { sizeof (U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7801[9] = 
{
	U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7::get_offset_of_textureIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7::get_offset_of_texture_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7::get_offset_of_markGpuOnly_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7::get_offset_of_isLinear_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7::get_offset_of_U3CsourceIdU3E5__2_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructTextureU3Ed__82_t16404C09FA6C0B2619FBC744338CC75FF1AC9EC7::get_offset_of_U3CU3Eu__1_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7802 = { sizeof (InstantiatedGLTFObject_t4581743C1D92EE3AD3C85A33000CEB647068E440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7802[1] = 
{
	InstantiatedGLTFObject_t4581743C1D92EE3AD3C85A33000CEB647068E440::get_offset_of_cachedData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7803 = { sizeof (MetalRough2StandardMap_t888CB2F5E48EAC8F465DC34E32B2ABDF5EE51DA5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7804 = { sizeof (MetalRoughMap_tD33910BDB1A6B5425ABAE3634D6CD7A2445AE0E6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7805 = { sizeof (SpecGloss2StandardMap_tCA41C38F51292ED96B810DDCE84269AE7E566F9F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7806 = { sizeof (SpecGlossMap_t078BF490CDB90B40DB9795C5858AACBE2CB0209A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7807 = { sizeof (StandardMap_t21748F03AFF649249878667EEC817DE852AEEB69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7807[3] = 
{
	StandardMap_t21748F03AFF649249878667EEC817DE852AEEB69::get_offset_of__material_0(),
	StandardMap_t21748F03AFF649249878667EEC817DE852AEEB69::get_offset_of__alphaMode_1(),
	StandardMap_t21748F03AFF649249878667EEC817DE852AEEB69::get_offset_of__alphaCutoff_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7808 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7809 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7810 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7811 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7812 = { sizeof (FileLoader_t67D17B03BB535CB6735C669B564A5355BA21ADE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7812[3] = 
{
	FileLoader_t67D17B03BB535CB6735C669B564A5355BA21ADE6::get_offset_of__rootDirectoryPath_0(),
	FileLoader_t67D17B03BB535CB6735C669B564A5355BA21ADE6::get_offset_of_U3CLoadedStreamU3Ek__BackingField_1(),
	FileLoader_t67D17B03BB535CB6735C669B564A5355BA21ADE6::get_offset_of_U3CHasSyncLoadMethodU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7813 = { sizeof (U3CU3Ec__DisplayClass11_0_t6E835C75DA5F3031633DE840D6459AB56787E0D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7813[2] = 
{
	U3CU3Ec__DisplayClass11_0_t6E835C75DA5F3031633DE840D6459AB56787E0D9::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass11_0_t6E835C75DA5F3031633DE840D6459AB56787E0D9::get_offset_of_pathToLoad_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7814 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7815 = { sizeof (WebRequestLoader_t09E04D05D3AEA5847FB8314469A2D4AC3E6234CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7815[3] = 
{
	WebRequestLoader_t09E04D05D3AEA5847FB8314469A2D4AC3E6234CB::get_offset_of_U3CLoadedStreamU3Ek__BackingField_0(),
	WebRequestLoader_t09E04D05D3AEA5847FB8314469A2D4AC3E6234CB::get_offset_of_httpClient_1(),
	WebRequestLoader_t09E04D05D3AEA5847FB8314469A2D4AC3E6234CB::get_offset_of_baseAddress_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7816 = { sizeof (U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7816[7] = 
{
	U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE::get_offset_of_gltfFilePath_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE::get_offset_of_U3CU3E4__this_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE::get_offset_of_U3CresponseU3E5__2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadStreamU3Ed__9_t9B3574D8F92DE7D67762E2D3AB9AA94AD7A17DEE::get_offset_of_U3CU3Eu__2_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7817 = { sizeof (SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2), -1, sizeof(SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7817[2] = 
{
	SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_StaticFields::get_offset_of_CoordinateSpaceConversionScale_0(),
	SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_StaticFields::get_offset_of_TangentSpaceConversionScale_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7818 = { sizeof (AnimationSamplerCacheData_tC0014F4E8B8B7E9E1442D8A0BBBDE38DAFE339D9)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7818[3] = 
{
	AnimationSamplerCacheData_tC0014F4E8B8B7E9E1442D8A0BBBDE38DAFE339D9::get_offset_of_Input_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationSamplerCacheData_tC0014F4E8B8B7E9E1442D8A0BBBDE38DAFE339D9::get_offset_of_Output_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationSamplerCacheData_tC0014F4E8B8B7E9E1442D8A0BBBDE38DAFE339D9::get_offset_of_Interpolation_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7819 = { sizeof (AnimationCacheData_tF59B339794F2D645D67DD3BBC4F2B75D81985486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7819[2] = 
{
	AnimationCacheData_tF59B339794F2D645D67DD3BBC4F2B75D81985486::get_offset_of_U3CLoadedAnimationClipU3Ek__BackingField_0(),
	AnimationCacheData_tF59B339794F2D645D67DD3BBC4F2B75D81985486::get_offset_of_U3CSamplersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7820 = { sizeof (AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7820[8] = 
{
	AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350::get_offset_of_U3CImageStreamCacheU3Ek__BackingField_0(),
	AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350::get_offset_of_U3CImageCacheU3Ek__BackingField_1(),
	AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350::get_offset_of_U3CTextureCacheU3Ek__BackingField_2(),
	AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350::get_offset_of_U3CMaterialCacheU3Ek__BackingField_3(),
	AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350::get_offset_of_U3CBufferCacheU3Ek__BackingField_4(),
	AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350::get_offset_of_U3CMeshCacheU3Ek__BackingField_5(),
	AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350::get_offset_of_U3CAnimationCacheU3Ek__BackingField_6(),
	AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350::get_offset_of_U3CNodeCacheU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7821 = { sizeof (BufferCacheData_t4013B91D6EF6FD8E4915E9B533EF479351F01F7D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7821[2] = 
{
	BufferCacheData_t4013B91D6EF6FD8E4915E9B533EF479351F01F7D::get_offset_of_U3CChunkOffsetU3Ek__BackingField_0(),
	BufferCacheData_t4013B91D6EF6FD8E4915E9B533EF479351F01F7D::get_offset_of_U3CStreamU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7822 = { sizeof (MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7822[3] = 
{
	MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1::get_offset_of_U3CUnityMaterialU3Ek__BackingField_0(),
	MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1::get_offset_of_U3CUnityMaterialWithVertexColorU3Ek__BackingField_1(),
	MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1::get_offset_of_U3CGLTFMaterialU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7823 = { sizeof (MeshCacheData_t8E3920989E4AF559033301071A663BCD94671213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7823[3] = 
{
	MeshCacheData_t8E3920989E4AF559033301071A663BCD94671213::get_offset_of_U3CLoadedMeshU3Ek__BackingField_0(),
	MeshCacheData_t8E3920989E4AF559033301071A663BCD94671213::get_offset_of_U3CMeshAttributesU3Ek__BackingField_1(),
	MeshCacheData_t8E3920989E4AF559033301071A663BCD94671213::get_offset_of_U3CPrimitiveGOU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7824 = { sizeof (RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7824[6] = 
{
	RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313::get_offset_of__isDisposed_0(),
	RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313::get_offset_of__refCount_1(),
	RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313::get_offset_of__refCountLock_2(),
	RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313::get_offset_of_U3CMeshCacheU3Ek__BackingField_3(),
	RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313::get_offset_of_U3CMaterialCacheU3Ek__BackingField_4(),
	RefCountedCacheData_t68F587D716850E3080A86210BDD8C96EFF7ED313::get_offset_of_U3CTextureCacheU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7825 = { sizeof (TextureCacheData_t7D959BB98FA30695BC896C7F667ACB97EA0A99CF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7825[2] = 
{
	TextureCacheData_t7D959BB98FA30695BC896C7F667ACB97EA0A99CF::get_offset_of_TextureDefinition_0(),
	TextureCacheData_t7D959BB98FA30695BC896C7F667ACB97EA0A99CF::get_offset_of_Texture_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7826 = { sizeof (GLTFExporterTest_tCF2E0E259A8A633C82CDB2C99F65BF845BDA292D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7827 = { sizeof (MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7827[6] = 
{
	MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A::get_offset_of_SceneIndex_4(),
	MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A::get_offset_of_Url_5(),
	MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A::get_offset_of__importer_6(),
	MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A::get_offset_of__asyncCoroutineHelper_7(),
	MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A::get_offset_of__loader_8(),
	MultiSceneComponent_tEF62B0E5AB992C1170D0EDB43A8AE24B0F29C56A::get_offset_of__fileName_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7828 = { sizeof (U3CLoadSceneU3Ed__8_tEB650CADA6924E88ADA570F99B9138D00CA509FA)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7828[5] = 
{
	U3CLoadSceneU3Ed__8_tEB650CADA6924E88ADA570F99B9138D00CA509FA::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadSceneU3Ed__8_tEB650CADA6924E88ADA570F99B9138D00CA509FA::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadSceneU3Ed__8_tEB650CADA6924E88ADA570F99B9138D00CA509FA::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadSceneU3Ed__8_tEB650CADA6924E88ADA570F99B9138D00CA509FA::get_offset_of_SceneIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadSceneU3Ed__8_tEB650CADA6924E88ADA570F99B9138D00CA509FA::get_offset_of_U3CU3Eu__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7829 = { sizeof (OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7829[11] = 
{
	OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8::get_offset_of_target_4(),
	OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8::get_offset_of_distance_5(),
	OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8::get_offset_of_xSpeed_6(),
	OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8::get_offset_of_ySpeed_7(),
	OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8::get_offset_of_yMinLimit_8(),
	OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8::get_offset_of_yMaxLimit_9(),
	OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8::get_offset_of_distanceMin_10(),
	OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8::get_offset_of_distanceMax_11(),
	OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8::get_offset_of_cameraRigidBody_12(),
	OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8::get_offset_of_x_13(),
	OrbitCameraController_t69B5196542BC4F2CE282AF8A903F95405CE127E8::get_offset_of_y_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7830 = { sizeof (SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A), -1, sizeof(SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7830[6] = 
{
	SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A::get_offset_of__indexFiles_0(),
	SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A_StaticFields::get_offset_of__mimeTypeMappings_1(),
	SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A::get_offset_of__serverThread_2(),
	SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A::get_offset_of__rootDirectory_3(),
	SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A::get_offset_of__listener_4(),
	SimpleHTTPServer_t255E5CE6E870E03DD3D9F5D98EEB0D238BB4881A::get_offset_of__port_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7831 = { sizeof (VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7831[8] = 
{
	VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498::get_offset_of_Mesh_4(),
	VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498::get_offset_of_NormalScale_5(),
	VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498::get_offset_of_TangentScale_6(),
	VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498::get_offset_of_VisualizeTangents_7(),
	VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498::get_offset_of_VisualizeNormals_8(),
	VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498::get_offset_of_vertices_9(),
	VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498::get_offset_of_normals_10(),
	VisualizeMeshAttributes_t45AB97CFD9146F783AA179220C931B8B65B31498::get_offset_of_tangents_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7832 = { sizeof (WebServerComponent_t7A46F6D96CD661321551B51D34A54738B8DEA4AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7832[1] = 
{
	WebServerComponent_t7A46F6D96CD661321551B51D34A54738B8DEA4AE::get_offset_of__server_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7833 = { sizeof (CaptureFromCamera_t23E07AE5656F95A80984C97BACFEF42487521C01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7833[5] = 
{
	CaptureFromCamera_t23E07AE5656F95A80984C97BACFEF42487521C01::get_offset_of__lastCamera_80(),
	CaptureFromCamera_t23E07AE5656F95A80984C97BACFEF42487521C01::get_offset_of__contribCameras_81(),
	CaptureFromCamera_t23E07AE5656F95A80984C97BACFEF42487521C01::get_offset_of__useContributingCameras_82(),
	CaptureFromCamera_t23E07AE5656F95A80984C97BACFEF42487521C01::get_offset_of__target_83(),
	CaptureFromCamera_t23E07AE5656F95A80984C97BACFEF42487521C01::get_offset_of__targetNativePointer_84(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7834 = { sizeof (CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7834[14] = 
{
	CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751::get_offset_of__cubemapResolution_80(),
	CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751::get_offset_of__cubemapDepth_81(),
	CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751::get_offset_of__supportGUI_82(),
	CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751::get_offset_of__supportCameraRotation_83(),
	CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751::get_offset_of__stereoRendering_84(),
	CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751::get_offset_of__ipd_85(),
	CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751::get_offset_of__camera_86(),
	CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751::get_offset_of__faceTarget_87(),
	CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751::get_offset_of__blitMaterial_88(),
	CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751::get_offset_of__cubemapToEquirectangularMaterial_89(),
	CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751::get_offset_of__cubeTarget_90(),
	CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751::get_offset_of__finalTarget_91(),
	CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751::get_offset_of__targetNativePointer_92(),
	CaptureFromCamera360_tF44D0F180EE13527D6280B1C97255FA7920C9751::get_offset_of__propFlipX_93(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7835 = { sizeof (CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7835[12] = 
{
	CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79::get_offset_of__settings_80(),
	CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79::get_offset_of__eyeWidth_81(),
	CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79::get_offset_of__eyeHeight_82(),
	CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79::get_offset_of__cameraGroup_83(),
	CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79::get_offset_of__leftCameraTop_84(),
	CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79::get_offset_of__leftCameraBot_85(),
	CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79::get_offset_of__rightCameraTop_86(),
	CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79::get_offset_of__rightCameraBot_87(),
	CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79::get_offset_of__final_88(),
	CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79::get_offset_of__targetNativePointer_89(),
	CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79::get_offset_of__finalMaterial_90(),
	CaptureFromCamera360ODS_t0FE75290B51243A85A0E723850267B9348DA5C79::get_offset_of__propSliceCenter_91(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7836 = { sizeof (Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7836[7] = 
{
	Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E::get_offset_of_camera_0(),
	Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E::get_offset_of_ipd_1(),
	Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E::get_offset_of_pixelSliceSize_2(),
	Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E::get_offset_of_paddingSize_3(),
	Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E::get_offset_of_cameraClearMode_4(),
	Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E::get_offset_of_cameraClearColor_5(),
	Settings_t6192A73E99B86F492E75C148531AF0BD42C0560E::get_offset_of_cameraImageEffects_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7837 = { sizeof (CaptureFromScreen_tEB468CAE6962D15FD6A3F718B595BDF64D70007D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7837[1] = 
{
	CaptureFromScreen_tEB468CAE6962D15FD6A3F718B595BDF64D70007D::get_offset_of__waitForEndOfFrame_80(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7838 = { sizeof (U3CFinalRenderCaptureU3Ed__3_tA3C60BC9D1528B7467BCC9B6DDF74F0797C40457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7838[3] = 
{
	U3CFinalRenderCaptureU3Ed__3_tA3C60BC9D1528B7467BCC9B6DDF74F0797C40457::get_offset_of_U3CU3E1__state_0(),
	U3CFinalRenderCaptureU3Ed__3_tA3C60BC9D1528B7467BCC9B6DDF74F0797C40457::get_offset_of_U3CU3E2__current_1(),
	U3CFinalRenderCaptureU3Ed__3_tA3C60BC9D1528B7467BCC9B6DDF74F0797C40457::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7839 = { sizeof (CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7839[3] = 
{
	CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A::get_offset_of__sourceTexture_80(),
	CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A::get_offset_of__renderTexture_81(),
	CaptureFromTexture_t733D44D62743C52FA1410E45358967160B0C227A::get_offset_of__targetNativePointer_82(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7840 = { sizeof (CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7840[21] = 
{
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__movieCapture_4(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__showUI_5(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__whenRecordingAutoHideUI_6(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__guiSkin_7(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__shownSection_8(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__videoCodecNames_9(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__audioCodecNames_10(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__videoCodecConfigurable_11(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__audioCodecConfigurable_12(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__audioDeviceNames_13(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__downScales_14(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__frameRates_15(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__downScaleIndex_16(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__frameRateIndex_17(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__videoPos_18(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__audioPos_19(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__audioCodecPos_20(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__lastFileSize_21(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__lastEncodedMinutes_22(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__lastEncodedSeconds_23(),
	CaptureGUI_t215A684C1FDA02B7B0827810C8C01F75E4022A21::get_offset_of__lastEncodedFrame_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7841 = { sizeof (MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7), -1, sizeof(MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7841[15] = 
{
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7::get_offset_of__format_4(),
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7::get_offset_of__numSamples_5(),
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7::get_offset_of__accum_6(),
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7::get_offset_of__lastComp_7(),
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7::get_offset_of__addMaterial_8(),
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7::get_offset_of__divMaterial_9(),
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7::get_offset_of__frameCount_10(),
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7::get_offset_of__targetWidth_11(),
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7::get_offset_of__targetHeight_12(),
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7::get_offset_of__isDirty_13(),
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7_StaticFields::get_offset_of__propNumSamples_14(),
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7_StaticFields::get_offset_of__propWeight_15(),
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7::get_offset_of_U3CIsFrameAccumulatedU3Ek__BackingField_16(),
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7::get_offset_of__bias_17(),
	MotionBlur_tC2E712A32DE33FA3D93CE5C33D3C22973E9F92A7::get_offset_of__total_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7842 = { sizeof (MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7842[5] = 
{
	MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD::get_offset_of__texture_4(),
	MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD::get_offset_of__hotspotOffset_5(),
	MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD::get_offset_of__sizeScale_6(),
	MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD::get_offset_of__depth_7(),
	MouseCursor_t2D59C0E32A66F12984B60C09F6FECBAB53D490DD::get_offset_of__content_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7843 = { sizeof (UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7843[10] = 
{
	UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5::get_offset_of__debugLogging_4(),
	UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5::get_offset_of__muteAudio_5(),
	0,
	UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5::get_offset_of__buffer_7(),
	UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5::get_offset_of__readBuffer_8(),
	UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5::get_offset_of__bufferIndex_9(),
	UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5::get_offset_of__bufferHandle_10(),
	UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5::get_offset_of__numChannels_11(),
	UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5::get_offset_of__overflowCount_12(),
	UnityAudioCapture_t6C1B0B749EAB3336BABE138B59C0391C29BCFCB5::get_offset_of__lockObject_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7844 = { sizeof (CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7844[76] = 
{
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__captureKey_4(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__captureOnStart_5(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__startPaused_6(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__listVideoCodecsOnStart_7(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__isRealTime_8(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__stopMode_9(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__stopFrames_10(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__stopSeconds_11(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__useMediaFoundationH264_12(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__videoCodecPriority_13(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__frameRate_14(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__downScale_15(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__maxVideoSize_16(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__forceVideoCodecIndex_17(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__flipVertically_18(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__supportAlpha_19(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__noAudio_20(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__audioCodecPriority_21(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__forceAudioCodecIndex_22(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__forceAudioDeviceIndex_23(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__audioCapture_24(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__autoGenerateFilename_25(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__outputFolderType_26(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__outputFolderPath_27(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__autoFilenamePrefix_28(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__autoFilenameExtension_29(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__forceFilename_30(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__outputType_31(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__renderResolution_32(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__renderSize_33(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__renderAntiAliasing_34(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__useMotionBlur_35(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__motionBlurSamples_36(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__motionBlurCameras_37(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__motionBlur_38(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__allowVSyncDisable_39(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__supportTextureRecreate_40(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__captureMouseCursor_41(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__mouseCursor_42(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__codecName_43(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__codecIndex_44(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__audioCodecName_45(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__audioCodecIndex_46(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__audioDeviceName_47(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__audioDeviceIndex_48(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__unityAudioSampleRate_49(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__unityAudioChannelCount_50(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__texture_51(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__handle_52(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__targetWidth_53(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__targetHeight_54(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__capturing_55(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__paused_56(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__filePath_57(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__fileInfo_58(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__pixelFormat_59(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__oldVSyncCount_60(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__oldFixedDeltaTime_61(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__isTopDown_62(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__isDirectX11_63(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__queuedStartCapture_64(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__queuedStopCapture_65(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__captureStartTime_66(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__timeSinceLastFrame_67(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__minimumDiskSpaceMB_68(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__freeDiskSpaceMB_69(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__numDroppedFrames_70(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__numDroppedEncoderFrames_71(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__numEncodedFrames_72(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__totalEncodedSeconds_73(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__renderEventFunction_74(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__freeEventFunction_75(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__fps_76(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__frameTotal_77(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__frameCount_78(),
	CaptureBase_t4AA58B983F72B37942873222FDA0DFAEAE76C110::get_offset_of__startFrameTime_79(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7845 = { sizeof (FrameRate_tF456D79FE7ABFF53E6E1F0062D230523FE80B8ED)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7845[12] = 
{
	FrameRate_tF456D79FE7ABFF53E6E1F0062D230523FE80B8ED::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7846 = { sizeof (Resolution_tC45EEF57F3A8B62D2D5B55E526EDF1921A327CED)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7846[21] = 
{
	Resolution_tC45EEF57F3A8B62D2D5B55E526EDF1921A327CED::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7847 = { sizeof (CubemapDepth_t4B2F58CBF8F0FD56A4FAB72521FB9497D3CF8DA2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7847[4] = 
{
	CubemapDepth_t4B2F58CBF8F0FD56A4FAB72521FB9497D3CF8DA2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7848 = { sizeof (CubemapResolution_t33EB7201E196B88DEC1F1D654910F80883BDAFF5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7848[6] = 
{
	CubemapResolution_t33EB7201E196B88DEC1F1D654910F80883BDAFF5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7849 = { sizeof (AntiAliasingLevel_tE5C5DB0035E55592FAF3559DD0221C0D31F385F7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7849[6] = 
{
	AntiAliasingLevel_tE5C5DB0035E55592FAF3559DD0221C0D31F385F7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7850 = { sizeof (DownScale_t0FA203D04AA8A2B141415F5CDE4B6C64FC9C8EE2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7850[7] = 
{
	DownScale_t0FA203D04AA8A2B141415F5CDE4B6C64FC9C8EE2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7851 = { sizeof (OutputPath_t349CC657290608E49919172C43113E04BB5FB92A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7851[4] = 
{
	OutputPath_t349CC657290608E49919172C43113E04BB5FB92A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7852 = { sizeof (OutputExtension_t564D5B605AF44511D757076642D75E8DE8A2AC62)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7852[5] = 
{
	OutputExtension_t564D5B605AF44511D757076642D75E8DE8A2AC62::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7853 = { sizeof (OutputType_t28F9E99ED1790FF6AEF268F541EA007E1E5B907A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7853[3] = 
{
	OutputType_t28F9E99ED1790FF6AEF268F541EA007E1E5B907A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7854 = { sizeof (StereoPacking_t3AB63B2DABBF5E68A9F08C8BFC0920105D7278C3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7854[4] = 
{
	StereoPacking_t3AB63B2DABBF5E68A9F08C8BFC0920105D7278C3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7855 = { sizeof (StopMode_tC9B6C8D23F29AF22F2E101E21A94D6E12BA986B9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7855[5] = 
{
	StopMode_tC9B6C8D23F29AF22F2E101E21A94D6E12BA986B9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7856 = { sizeof (NativePlugin_tD0BE1E9473BBD3CD8A3F7C990F19D582270BD293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7856[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7857 = { sizeof (PixelFormat_t9C6FFBEE3E5AF45D60A375FB66926BD5922FB693)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7857[6] = 
{
	PixelFormat_t9C6FFBEE3E5AF45D60A375FB66926BD5922FB693::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7858 = { sizeof (PluginEvent_tC1A4A889A972A7635EDED3226C603681CFD1A494)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7858[3] = 
{
	PluginEvent_tC1A4A889A972A7635EDED3226C603681CFD1A494::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7859 = { sizeof (Utils_t432AAACD1A54848334280545542E16BC10339FB3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7860 = { sizeof (U3CU3Ec_tED58CC381B12285A5523E8913E006FB866934B2E), -1, sizeof(U3CU3Ec_tED58CC381B12285A5523E8913E006FB866934B2E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7860[2] = 
{
	U3CU3Ec_tED58CC381B12285A5523E8913E006FB866934B2E_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tED58CC381B12285A5523E8913E006FB866934B2E_StaticFields::get_offset_of_U3CU3E9__2_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7861 = { sizeof (ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7861[7] = 
{
	ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6::get_offset_of__audioBG_4(),
	ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6::get_offset_of__audioHit_5(),
	ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6::get_offset_of__speed_6(),
	ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6::get_offset_of__capture_7(),
	ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6::get_offset_of__guiSkin_8(),
	ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6::get_offset_of__spinCamera_9(),
	ScreenCaptureDemo_t706C9CC513D34F472E4BCB2621D68DD61AB849B6::get_offset_of__timer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7862 = { sizeof (ScriptCaptureDemo_t4D14A89238E78B8AD3C0BC618A21D6342873C8A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7862[3] = 
{
	0,
	ScriptCaptureDemo_t4D14A89238E78B8AD3C0BC618A21D6342873C8A2::get_offset_of__videoCodecIndex_5(),
	ScriptCaptureDemo_t4D14A89238E78B8AD3C0BC618A21D6342873C8A2::get_offset_of__encoderHandle_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7863 = { sizeof (SurroundCaptureDemo_tB198F0F3C3D917DCF75BF34B70C22318F1765EE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7863[7] = 
{
	SurroundCaptureDemo_tB198F0F3C3D917DCF75BF34B70C22318F1765EE6::get_offset_of__spawnPoint_4(),
	SurroundCaptureDemo_tB198F0F3C3D917DCF75BF34B70C22318F1765EE6::get_offset_of__cubePrefab_5(),
	SurroundCaptureDemo_tB198F0F3C3D917DCF75BF34B70C22318F1765EE6::get_offset_of__spawn_6(),
	0,
	0,
	SurroundCaptureDemo_tB198F0F3C3D917DCF75BF34B70C22318F1765EE6::get_offset_of__timer_9(),
	SurroundCaptureDemo_tB198F0F3C3D917DCF75BF34B70C22318F1765EE6::get_offset_of__cubes_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7864 = { sizeof (U3CKillCubeU3Ed__10_tF0E4FE27186E2A9C445D85FCD0001E0290014949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7864[3] = 
{
	U3CKillCubeU3Ed__10_tF0E4FE27186E2A9C445D85FCD0001E0290014949::get_offset_of_U3CU3E1__state_0(),
	U3CKillCubeU3Ed__10_tF0E4FE27186E2A9C445D85FCD0001E0290014949::get_offset_of_U3CU3E2__current_1(),
	U3CKillCubeU3Ed__10_tF0E4FE27186E2A9C445D85FCD0001E0290014949::get_offset_of_go_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7865 = { sizeof (TextureCaptureDemo_t5FC711E8029DADD159F9138461CB8DBA42263236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7865[5] = 
{
	TextureCaptureDemo_t5FC711E8029DADD159F9138461CB8DBA42263236::get_offset_of__textureWidth_4(),
	TextureCaptureDemo_t5FC711E8029DADD159F9138461CB8DBA42263236::get_offset_of__textureHeight_5(),
	TextureCaptureDemo_t5FC711E8029DADD159F9138461CB8DBA42263236::get_offset_of__movieCapture_6(),
	TextureCaptureDemo_t5FC711E8029DADD159F9138461CB8DBA42263236::get_offset_of__material_7(),
	TextureCaptureDemo_t5FC711E8029DADD159F9138461CB8DBA42263236::get_offset_of__texture_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7866 = { sizeof (WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7866[7] = 
{
	WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE::get_offset_of__skin_4(),
	WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE::get_offset_of__prefab_5(),
	WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE::get_offset_of__webcamResolutionWidth_6(),
	WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE::get_offset_of__webcamResolutionHeight_7(),
	WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE::get_offset_of__webcamFrameRate_8(),
	WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE::get_offset_of__instances_9(),
	WebcamCaptureDemo_tFB43EB9D0B2473CED686BF577E354D2DB32567DE::get_offset_of__selectedWebcamIndex_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7867 = { sizeof (Instance_tE55BE73BDE833127C4D2104E58CCB2E5EBEF78AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7867[4] = 
{
	Instance_tE55BE73BDE833127C4D2104E58CCB2E5EBEF78AE::get_offset_of_name_0(),
	Instance_tE55BE73BDE833127C4D2104E58CCB2E5EBEF78AE::get_offset_of_texture_1(),
	Instance_tE55BE73BDE833127C4D2104E58CCB2E5EBEF78AE::get_offset_of_capture_2(),
	Instance_tE55BE73BDE833127C4D2104E58CCB2E5EBEF78AE::get_offset_of_gui_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7868 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7869 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7870 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7871 = { sizeof (ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7871[10] = 
{
	ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69::get_offset_of_U3CScannerBackgroundThreadU3Ek__BackingField_0(),
	ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69::get_offset_of_U3CScannerDelayFrameMinU3Ek__BackingField_1(),
	ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69::get_offset_of_U3CScannerDecodeIntervalU3Ek__BackingField_2(),
	ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69::get_offset_of_U3CParserAutoRotateU3Ek__BackingField_3(),
	ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69::get_offset_of_U3CParserTryInvertedU3Ek__BackingField_4(),
	ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69::get_offset_of_U3CParserTryHarderU3Ek__BackingField_5(),
	ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69::get_offset_of_U3CWebcamDefaultDeviceNameU3Ek__BackingField_6(),
	ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69::get_offset_of_U3CWebcamRequestedWidthU3Ek__BackingField_7(),
	ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69::get_offset_of_U3CWebcamRequestedHeightU3Ek__BackingField_8(),
	ScannerSettings_tE6DCE89EE74AECF5500B1B02595F84F8E7505B69::get_offset_of_U3CWebcamFilterModeU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7872 = { sizeof (UnityWebcam_t3E74C79DAEF401347835D65AD9D25B86C1E36313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7872[3] = 
{
	UnityWebcam_t3E74C79DAEF401347835D65AD9D25B86C1E36313::get_offset_of_U3CWebcamU3Ek__BackingField_0(),
	UnityWebcam_t3E74C79DAEF401347835D65AD9D25B86C1E36313::get_offset_of_U3CWidthU3Ek__BackingField_1(),
	UnityWebcam_t3E74C79DAEF401347835D65AD9D25B86C1E36313::get_offset_of_U3CHeightU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7873 = { sizeof (Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7873[15] = 
{
	Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D::get_offset_of_OnReady_0(),
	Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D::get_offset_of_StatusChanged_1(),
	Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D::get_offset_of_U3CCameraU3Ek__BackingField_2(),
	Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D::get_offset_of_U3CParserU3Ek__BackingField_3(),
	Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D::get_offset_of_U3CSettingsU3Ek__BackingField_4(),
	Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D::get_offset_of_status_5(),
	Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D::get_offset_of_pixels_6(),
	Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D::get_offset_of_Callback_7(),
	Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D::get_offset_of_Result_8(),
	Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D::get_offset_of_parserPixelAvailable_9(),
	Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D::get_offset_of_mainThreadLastDecode_10(),
	Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D::get_offset_of_webcamFrameDelayed_11(),
	Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D::get_offset_of_webcamLastChecksum_12(),
	Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D::get_offset_of_decodeInterrupted_13(),
	Scanner_t7EEB3BF43250D48CBD72CE0B36748443406D642D::get_offset_of_CodeScannerThread_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7874 = { sizeof (ScannerStatus_t9AC239CF3598752D1294F8CFBB52C7C97C13691E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7874[4] = 
{
	ScannerStatus_t9AC239CF3598752D1294F8CFBB52C7C97C13691E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7875 = { sizeof (ParserResult_t92EB97AE2A35D224A2C3624D2A7BD88D61431239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7875[2] = 
{
	ParserResult_t92EB97AE2A35D224A2C3624D2A7BD88D61431239::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	ParserResult_t92EB97AE2A35D224A2C3624D2A7BD88D61431239::get_offset_of_U3CValueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7876 = { sizeof (ZXingParser_t7B9AC8160C3A1565FA96F8714D30B9C7E852DE26), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7876[1] = 
{
	ZXingParser_t7B9AC8160C3A1565FA96F8714D30B9C7E852DE26::get_offset_of_U3CScannerU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7877 = { sizeof (Log_tA0BCB64FBB15B914060CBCDCAC37D04ED3FBA372), -1, sizeof(Log_tA0BCB64FBB15B914060CBCDCAC37D04ED3FBA372_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7877[2] = 
{
	Log_tA0BCB64FBB15B914060CBCDCAC37D04ED3FBA372_StaticFields::get_offset_of_Instance_0(),
	Log_tA0BCB64FBB15B914060CBCDCAC37D04ED3FBA372::get_offset_of_services_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7878 = { sizeof (LogLevel_t16EBCC8E5519DEF124927B19C3FA125CD6C5F079)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7878[7] = 
{
	LogLevel_t16EBCC8E5519DEF124927B19C3FA125CD6C5F079::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7879 = { sizeof (ConsoleService_t04B92B015881DE18D734818F7A6F50412805D0A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7879[1] = 
{
	ConsoleService_t04B92B015881DE18D734818F7A6F50412805D0A5::get_offset_of_minLevel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7880 = { sizeof (FileService_tF81532934EAC45C16137B3C6E869BAB4AE444921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7880[2] = 
{
	FileService_tF81532934EAC45C16137B3C6E869BAB4AE444921::get_offset_of_minLevel_0(),
	FileService_tF81532934EAC45C16137B3C6E869BAB4AE444921::get_offset_of_pathLog_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7881 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7882 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7883 = { sizeof (NatShare_t57B1B3F8122D1A2445260CBDA02BB51194C80442), -1, sizeof(NatShare_t57B1B3F8122D1A2445260CBDA02BB51194C80442_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7883[1] = 
{
	NatShare_t57B1B3F8122D1A2445260CBDA02BB51194C80442_StaticFields::get_offset_of_Implementation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7884 = { sizeof (DocAttribute_t26C862396DB47D0D92299AFB0D372A1582779ABB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7885 = { sizeof (CodeAttribute_t195605836B5236010C22EFADF825430811E28BF0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7886 = { sizeof (RefAttribute_t153A1C0B02D858F3AB42568E769F7E782112DE50), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7887 = { sizeof (NatShareAndroid_t4038EB6E58E38F1959DCB6762FF4128B853F4AE7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7887[1] = 
{
	NatShareAndroid_t4038EB6E58E38F1959DCB6762FF4128B853F4AE7::get_offset_of_natshare_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7888 = { sizeof (NatShareBridge_t2A766E7C6E2043B7A97C213AD0EE5B37E7123977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7888[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7889 = { sizeof (NatShareiOS_tEF7F52531CD9F1C108A99CAD9A9D0A5516CCB25D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7890 = { sizeof (NatShareNull_t9B8BBA5A7C8CF157E2E1D53FD5DADE79595DCB94), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7891 = { sizeof (NatShareWebGL_t4E822D25A7510CBFB9CDC3D24D0C09C2D3B264BE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7892 = { sizeof (ThumbnailDelegate_t407F2620090BC25C8932333B96C8498F1C76A716), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7893 = { sizeof (U3CU3Ec__DisplayClass6_0_tB47FCDA2F094FE229473C417AC822A75ABBC508C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7893[1] = 
{
	U3CU3Ec__DisplayClass6_0_tB47FCDA2F094FE229473C417AC822A75ABBC508C::get_offset_of_thumbnailHandle_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7894 = { sizeof (U3CGetThumbnailU3Ed__6_t4AA1BAB99FB936EC43A488990FCC1F4D83B36718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7894[6] = 
{
	U3CGetThumbnailU3Ed__6_t4AA1BAB99FB936EC43A488990FCC1F4D83B36718::get_offset_of_U3CU3E1__state_0(),
	U3CGetThumbnailU3Ed__6_t4AA1BAB99FB936EC43A488990FCC1F4D83B36718::get_offset_of_U3CU3E2__current_1(),
	U3CGetThumbnailU3Ed__6_t4AA1BAB99FB936EC43A488990FCC1F4D83B36718::get_offset_of_thumbnailHandle_2(),
	U3CGetThumbnailU3Ed__6_t4AA1BAB99FB936EC43A488990FCC1F4D83B36718::get_offset_of_callbackObject_3(),
	U3CGetThumbnailU3Ed__6_t4AA1BAB99FB936EC43A488990FCC1F4D83B36718::get_offset_of_U3CU3E8__1_4(),
	U3CGetThumbnailU3Ed__6_t4AA1BAB99FB936EC43A488990FCC1F4D83B36718::get_offset_of_callback_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7895 = { sizeof (AssetBundleLoadOperation_t4E376230379EB84B5F4A6D7F957B791348E72141), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7896 = { sizeof (AssetBundleLoadLevelOperation_t65E30AD7A5E27BDE9FA80E81AA8A99E14C397DD4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7896[5] = 
{
	AssetBundleLoadLevelOperation_t65E30AD7A5E27BDE9FA80E81AA8A99E14C397DD4::get_offset_of_m_AssetBundleName_0(),
	AssetBundleLoadLevelOperation_t65E30AD7A5E27BDE9FA80E81AA8A99E14C397DD4::get_offset_of_m_LevelName_1(),
	AssetBundleLoadLevelOperation_t65E30AD7A5E27BDE9FA80E81AA8A99E14C397DD4::get_offset_of_m_IsAdditive_2(),
	AssetBundleLoadLevelOperation_t65E30AD7A5E27BDE9FA80E81AA8A99E14C397DD4::get_offset_of_m_DownloadingError_3(),
	AssetBundleLoadLevelOperation_t65E30AD7A5E27BDE9FA80E81AA8A99E14C397DD4::get_offset_of_m_Request_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7897 = { sizeof (AssetBundleLoadAssetOperation_t5A1401110A453E5BC738720B9A4E596235815463), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7898 = { sizeof (AssetBundleLoadAssetOperationSimulation_t7F7FFFC464147B063BF894A9FD38488F4FAEE540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7898[1] = 
{
	AssetBundleLoadAssetOperationSimulation_t7F7FFFC464147B063BF894A9FD38488F4FAEE540::get_offset_of_m_SimulatedObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7899 = { sizeof (AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7899[5] = 
{
	AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0::get_offset_of_m_AssetBundleName_0(),
	AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0::get_offset_of_m_AssetName_1(),
	AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0::get_offset_of_m_DownloadingError_2(),
	AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0::get_offset_of_m_Type_3(),
	AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0::get_offset_of_m_Request_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
