﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct EntryU5BU5D_t794F8731FC1D54FCF80C5E4F0F384277288B9DE6;
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct KeyCollection_t9FFA1CD996863941D040375CDA32CB066FB6F314;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct ValueCollection_tEE610AF4936FFF247E852E276393507326A42062;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32Enum>
struct Dictionary_2_t15935BA59D5EDF22B5075E957C7C05DEE12E3B57;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB;
// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Event>
struct IEqualityComparer_1_t99D463EEE5FD1A90B13F1A4851691BA1AC2F54B4;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Event
struct Event_t187FF6A6B357447B83EC2064823EE0AEC5263210;
// UnityEngine.GUIContent
struct GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.GUIStyleState
struct GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5;
// UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A;
// UnityEngine.SliderState
struct SliderState_t6081D6F2CF6D0F1A13B2A2D255671B4053EC52CB;
// UnityEngine.TextEditor
struct TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90;

extern RuntimeClass* Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319_il2cpp_TypeInfo_var;
extern RuntimeClass* GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_il2cpp_TypeInfo_var;
extern RuntimeClass* GUIStyle_t671F175A201A19166385EE3392292A5F50070572_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* TextEditOp_t197E102C6DE6BA0F6A2871DB17FA260B6E01E151_il2cpp_TypeInfo_var;
extern RuntimeClass* TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral03BB5E3AD422D654676A16373CB6767BDD1EC096;
extern String_t* _stringLiteral03C170628B9070A1EE6D3EF4C543678D9EE67097;
extern String_t* _stringLiteral0E33543BDBE560B7A172E26B076C221830E48A23;
extern String_t* _stringLiteral12C0F1FBADC4046B5F2BB9E063B227EF8750D9D6;
extern String_t* _stringLiteral131D06FD63FDF2386A3D1762E3405DFF94BE55C6;
extern String_t* _stringLiteral15E6F46C6ABAA57A101887F62558D6558BDAF0AC;
extern String_t* _stringLiteral19E7EC0CA525399DB1C8D777D61D6FF53DCC5C0C;
extern String_t* _stringLiteral24078727E760F609BA9D460B38695EB78018866F;
extern String_t* _stringLiteral286BC08C2F4EED0826A70058A345427BAD61B929;
extern String_t* _stringLiteral2D3E82BE1E73866269EA97C6D02040FE73E7AB54;
extern String_t* _stringLiteral32588D5CD9AD74EC3ECEDFA1041A987A5D99069C;
extern String_t* _stringLiteral37E2609EE8EAC3E9833495CCBF1141F79B24270C;
extern String_t* _stringLiteral38132A51EE9A97A4511377264A0779C0861CE815;
extern String_t* _stringLiteral3D68429D3570BB774AB8CA78687D0FC0F48FAC04;
extern String_t* _stringLiteral423D5CEC9E85A686DEF7682D66F54C301C77C171;
extern String_t* _stringLiteral4756CFCA8EEFEAD20B5992EAC6EF5748E1775527;
extern String_t* _stringLiteral4930EB3F60DF3A6A61E8D44178FEB2B8F820A153;
extern String_t* _stringLiteral56B2C39D4D5C71E523F34B8C45A6D4DCBE7EF18C;
extern String_t* _stringLiteral58876305D454A861801BDE1F2125E1368C231E4A;
extern String_t* _stringLiteral59F9DAB91E232359595FB7E7B346D4C636DD29E4;
extern String_t* _stringLiteral5B2662EFB47DC70EB0DD0468A0429C080DE0EA3C;
extern String_t* _stringLiteral6102DC14FA0F0C73D3C99FB1892929691937BD25;
extern String_t* _stringLiteral62847E519D4ADA28164250829049B9A919B2AE01;
extern String_t* _stringLiteral67F7C0B892F47A0F86D1D66FE6BD734EE4F7C1A6;
extern String_t* _stringLiteral6FFAD0F1514E6E22A0D256A7C7B31FAABD73ED63;
extern String_t* _stringLiteral740377EEA64FD0E2703021FAD7C8612402ADFF92;
extern String_t* _stringLiteral763389B097CFDA9576DBD4046F2ECC69E0D29B85;
extern String_t* _stringLiteral77346D0447DAFF959358A0ECBEEC83BFD9EC86BB;
extern String_t* _stringLiteral77DB7EB9136D91C5901F452F84527E54526FAC83;
extern String_t* _stringLiteral7A92F3D26362D6557D5701DE77A63A01DF61E57F;
extern String_t* _stringLiteral7C0A25C06EA30BAE50E39A37A5997E31A1A96E20;
extern String_t* _stringLiteral7CE12B4417E103A0BD80EB14458AE1A104BF6D6B;
extern String_t* _stringLiteral7FA2D4DD6FB5DD5CDF7FFCB9E09C0871C2C63452;
extern String_t* _stringLiteral901C620F7ECC0984C3DD8E8735EFBA866392BA6F;
extern String_t* _stringLiteral9485989FF514B5106B7738850FD73C23E8C1E3F7;
extern String_t* _stringLiteral968767E065572AA7E887F179AEE12CA552A5AC0C;
extern String_t* _stringLiteral982F849E4755453B885083FA0F7C4B4462B2E6B3;
extern String_t* _stringLiteral9A5F708763FE0E3D92F572DBE11DCFB8F2F824EA;
extern String_t* _stringLiteral9E31EA478E6422888A2A833CD072E72DC0543E06;
extern String_t* _stringLiteral9F5BE511E1E437CA95D5B55F8F8A88906FC1AEE6;
extern String_t* _stringLiteralAD56C2BC3EC9BD41B3074B5A8914F35632534E49;
extern String_t* _stringLiteralADDA35B979E6F63F030A7F51D1FC60F0785079DF;
extern String_t* _stringLiteralB83393C5BFF0AEB7C3364909B97AC7186EC0331F;
extern String_t* _stringLiteralB858CB282617FB0956D960215C8E84D1CCF909C6;
extern String_t* _stringLiteralBA841F6B049E05B23489E3235B0729E4E5D5C627;
extern String_t* _stringLiteralBA8AB5A0280B953AA97435FF8946CBCBB2755A27;
extern String_t* _stringLiteralBAD94ADF967C08A44711C5A7B859873FB1B0F065;
extern String_t* _stringLiteralBDE895DBC95DC311972A9ADA8028849FED24437B;
extern String_t* _stringLiteralBEED3BF9F8706DB26270FA0878A81526874ECB0C;
extern String_t* _stringLiteralC470A513564DCA6343DE982775D5451483E70B53;
extern String_t* _stringLiteralC9EBD785A19C1BFEFD62B3B2A4FC1A6D6963AEE1;
extern String_t* _stringLiteralCBECD5D022A1A5C503555604D30C3EAF1C10299E;
extern String_t* _stringLiteralD12BE11B6C6387EF9D43CD28DBA22C79D86FDFFB;
extern String_t* _stringLiteralD27A1F11771200949714B1AF99F048A416F5D6F4;
extern String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
extern String_t* _stringLiteralDC29F2233DCB721EDC0D3E34D64543CCF3403270;
extern String_t* _stringLiteralDF4EC500FDA53C7919CEB5C27504F045909CEB4A;
extern String_t* _stringLiteralDF7A8F3088FC2D7221820E99BAF39A1BCB2DD203;
extern String_t* _stringLiteralE83249BD3BA79932E16FB1FB5100DAFADE9954C2;
extern String_t* _stringLiteralEB36FC6F33913BF1B2F1305630A3023799A75AA7;
extern String_t* _stringLiteralF122C7E167718794E9F9BC54786C86A7D99579E6;
extern String_t* _stringLiteralF7D71AD6A293C739FDB380F7E3761BF9CCF1933B;
extern String_t* _stringLiteralF87FB192D2661A8C26E07D692244897AB190B2FC;
extern String_t* _stringLiteralFD41A55936F9E25124F5243F3AC6F8312A602912;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m886A3ED9D9AB620984BF26DF4B63571D37E2AE41_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_mD6F5EEE24289730ADCFD7DE5DC90DC2E6B0BFF2F_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m15C699577EFD717DEEDFA3D6138A6BDC4C2B43E5_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m461003B498B59A8F07384C0C472BC6AB93545D2C_RuntimeMethod_var;
extern const uint32_t TextEditor_ClampTextIndex_m3EC3F291912021B79AF56CDD804B8795191A629B_MetadataUsageId;
extern const uint32_t TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD_MetadataUsageId;
extern const uint32_t TextEditor_DrawCursor_mF5281E018BE231150E23B748802E69A950ADAAF0_MetadataUsageId;
extern const uint32_t TextEditor_HandleKeyEvent_m40E63B226145A1FA73891E38403E444D4F395750_MetadataUsageId;
extern const uint32_t TextEditor_InitKeyActions_m724C451AA81FE63C7DC95DECD4AC681080AFCC11_MetadataUsageId;
extern const uint32_t TextEditor_IsValidCodePointIndex_m0366054C7FF170C5FBE08469412517D10199AF47_MetadataUsageId;
extern const uint32_t TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305_MetadataUsageId;
extern const uint32_t TextEditor_MoveCursorToPosition_Internal_mD66C2285C2B5F6EC3516820193DCB64936797704_MetadataUsageId;
extern const uint32_t TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40_MetadataUsageId;
extern const uint32_t TextEditor_OnLostFocus_m792C976FAC66B6C5576E49406A2963F1D19DD507_MetadataUsageId;
extern const uint32_t TextEditor_Paste_m551DF86401B71947857EC27860AE3470C33B860B_MetadataUsageId;
extern const uint32_t TextEditor_PerformOperation_m6A900A3FA32AC396A0674649EC52C6456476EA89_MetadataUsageId;
extern const uint32_t TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9_MetadataUsageId;
extern const uint32_t TextEditor_ReplaceNewlinesWithSpaces_mDEEE760A469D8700A24D803C0F184BFC8EDC4B1D_MetadataUsageId;
extern const uint32_t TextEditor_SelectToPosition_m2EE616963923B9561A7C3CFFA392A0C4FE8D8C93_MetadataUsageId;
extern const uint32_t TextEditor__ctor_m279158A237B393882E3CC2834C1F7CA7679F79CC_MetadataUsageId;
extern const uint32_t TextEditor_set_text_mFF167B3EFD4D6F6260126315BE09CAD6BF5F04D0_MetadataUsageId;
struct GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com;
struct GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke;
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef DICTIONARY_2_T146200EEAB2E66010EC2A0DA98A2E0738F4C8319_H
#define DICTIONARY_2_T146200EEAB2E66010EC2A0DA98A2E0738F4C8319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct  Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t794F8731FC1D54FCF80C5E4F0F384277288B9DE6* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t9FFA1CD996863941D040375CDA32CB066FB6F314 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_tEE610AF4936FFF247E852E276393507326A42062 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319, ___entries_1)); }
	inline EntryU5BU5D_t794F8731FC1D54FCF80C5E4F0F384277288B9DE6* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t794F8731FC1D54FCF80C5E4F0F384277288B9DE6** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t794F8731FC1D54FCF80C5E4F0F384277288B9DE6* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319, ___keys_7)); }
	inline KeyCollection_t9FFA1CD996863941D040375CDA32CB066FB6F314 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t9FFA1CD996863941D040375CDA32CB066FB6F314 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t9FFA1CD996863941D040375CDA32CB066FB6F314 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319, ___values_8)); }
	inline ValueCollection_tEE610AF4936FFF247E852E276393507326A42062 * get_values_8() const { return ___values_8; }
	inline ValueCollection_tEE610AF4936FFF247E852E276393507326A42062 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_tEE610AF4936FFF247E852E276393507326A42062 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T146200EEAB2E66010EC2A0DA98A2E0738F4C8319_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef GUICONTENT_T2A00F8961C69C0A382168840CFB2111FB00B5EA0_H
#define GUICONTENT_T2A00F8961C69C0A382168840CFB2111FB00B5EA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIContent
struct  GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0  : public RuntimeObject
{
public:
	// System.String UnityEngine.GUIContent::m_Text
	String_t* ___m_Text_0;
	// UnityEngine.Texture UnityEngine.GUIContent::m_Image
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_Image_1;
	// System.String UnityEngine.GUIContent::m_Tooltip
	String_t* ___m_Tooltip_2;

public:
	inline static int32_t get_offset_of_m_Text_0() { return static_cast<int32_t>(offsetof(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0, ___m_Text_0)); }
	inline String_t* get_m_Text_0() const { return ___m_Text_0; }
	inline String_t** get_address_of_m_Text_0() { return &___m_Text_0; }
	inline void set_m_Text_0(String_t* value)
	{
		___m_Text_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_0), value);
	}

	inline static int32_t get_offset_of_m_Image_1() { return static_cast<int32_t>(offsetof(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0, ___m_Image_1)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_m_Image_1() const { return ___m_Image_1; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_m_Image_1() { return &___m_Image_1; }
	inline void set_m_Image_1(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___m_Image_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_1), value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tooltip_2), value);
	}
};

struct GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_StaticFields
{
public:
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Text
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___s_Text_3;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Image
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___s_Image_4;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_TextImage
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___s_TextImage_5;
	// UnityEngine.GUIContent UnityEngine.GUIContent::none
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___none_6;

public:
	inline static int32_t get_offset_of_s_Text_3() { return static_cast<int32_t>(offsetof(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_StaticFields, ___s_Text_3)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_s_Text_3() const { return ___s_Text_3; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_s_Text_3() { return &___s_Text_3; }
	inline void set_s_Text_3(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___s_Text_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Text_3), value);
	}

	inline static int32_t get_offset_of_s_Image_4() { return static_cast<int32_t>(offsetof(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_StaticFields, ___s_Image_4)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_s_Image_4() const { return ___s_Image_4; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_s_Image_4() { return &___s_Image_4; }
	inline void set_s_Image_4(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___s_Image_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Image_4), value);
	}

	inline static int32_t get_offset_of_s_TextImage_5() { return static_cast<int32_t>(offsetof(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_StaticFields, ___s_TextImage_5)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_s_TextImage_5() const { return ___s_TextImage_5; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_s_TextImage_5() { return &___s_TextImage_5; }
	inline void set_s_TextImage_5(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___s_TextImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_TextImage_5), value);
	}

	inline static int32_t get_offset_of_none_6() { return static_cast<int32_t>(offsetof(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_StaticFields, ___none_6)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_none_6() const { return ___none_6; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_none_6() { return &___none_6; }
	inline void set_none_6(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___none_6 = value;
		Il2CppCodeGenWriteBarrier((&___none_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIContent
struct GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_marshaled_pinvoke
{
	char* ___m_Text_0;
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_Image_1;
	char* ___m_Tooltip_2;
};
// Native definition for COM marshalling of UnityEngine.GUIContent
struct GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_marshaled_com
{
	Il2CppChar* ___m_Text_0;
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_Image_1;
	Il2CppChar* ___m_Tooltip_2;
};
#endif // GUICONTENT_T2A00F8961C69C0A382168840CFB2111FB00B5EA0_H
#ifndef SLIDERSTATE_T6081D6F2CF6D0F1A13B2A2D255671B4053EC52CB_H
#define SLIDERSTATE_T6081D6F2CF6D0F1A13B2A2D255671B4053EC52CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SliderState
struct  SliderState_t6081D6F2CF6D0F1A13B2A2D255671B4053EC52CB  : public RuntimeObject
{
public:
	// System.Single UnityEngine.SliderState::dragStartPos
	float ___dragStartPos_0;
	// System.Single UnityEngine.SliderState::dragStartValue
	float ___dragStartValue_1;
	// System.Boolean UnityEngine.SliderState::isDragging
	bool ___isDragging_2;

public:
	inline static int32_t get_offset_of_dragStartPos_0() { return static_cast<int32_t>(offsetof(SliderState_t6081D6F2CF6D0F1A13B2A2D255671B4053EC52CB, ___dragStartPos_0)); }
	inline float get_dragStartPos_0() const { return ___dragStartPos_0; }
	inline float* get_address_of_dragStartPos_0() { return &___dragStartPos_0; }
	inline void set_dragStartPos_0(float value)
	{
		___dragStartPos_0 = value;
	}

	inline static int32_t get_offset_of_dragStartValue_1() { return static_cast<int32_t>(offsetof(SliderState_t6081D6F2CF6D0F1A13B2A2D255671B4053EC52CB, ___dragStartValue_1)); }
	inline float get_dragStartValue_1() const { return ___dragStartValue_1; }
	inline float* get_address_of_dragStartValue_1() { return &___dragStartValue_1; }
	inline void set_dragStartValue_1(float value)
	{
		___dragStartValue_1 = value;
	}

	inline static int32_t get_offset_of_isDragging_2() { return static_cast<int32_t>(offsetof(SliderState_t6081D6F2CF6D0F1A13B2A2D255671B4053EC52CB, ___isDragging_2)); }
	inline bool get_isDragging_2() const { return ___isDragging_2; }
	inline bool* get_address_of_isDragging_2() { return &___isDragging_2; }
	inline void set_isDragging_2(bool value)
	{
		___isDragging_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIDERSTATE_T6081D6F2CF6D0F1A13B2A2D255671B4053EC52CB_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef CHAR_TBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_H
#define CHAR_TBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((&___categoryForLatin1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_TBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef INT32ENUM_T6312CE4586C17FE2E2E513D2E7655B574F10FDCD_H
#define INT32ENUM_T6312CE4586C17FE2E2E513D2E7655B574F10FDCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32Enum
struct  Int32Enum_t6312CE4586C17FE2E2E513D2E7655B574F10FDCD 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t6312CE4586C17FE2E2E513D2E7655B574F10FDCD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32ENUM_T6312CE4586C17FE2E2E513D2E7655B574F10FDCD_H
#ifndef EVENT_T187FF6A6B357447B83EC2064823EE0AEC5263210_H
#define EVENT_T187FF6A6B357447B83EC2064823EE0AEC5263210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Event
struct  Event_t187FF6A6B357447B83EC2064823EE0AEC5263210  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Event::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Event_t187FF6A6B357447B83EC2064823EE0AEC5263210, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

struct Event_t187FF6A6B357447B83EC2064823EE0AEC5263210_StaticFields
{
public:
	// UnityEngine.Event UnityEngine.Event::s_Current
	Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * ___s_Current_1;
	// UnityEngine.Event UnityEngine.Event::s_MasterEvent
	Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * ___s_MasterEvent_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UnityEngine.Event::<>f__switch$map0
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___U3CU3Ef__switchU24map0_3;

public:
	inline static int32_t get_offset_of_s_Current_1() { return static_cast<int32_t>(offsetof(Event_t187FF6A6B357447B83EC2064823EE0AEC5263210_StaticFields, ___s_Current_1)); }
	inline Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * get_s_Current_1() const { return ___s_Current_1; }
	inline Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 ** get_address_of_s_Current_1() { return &___s_Current_1; }
	inline void set_s_Current_1(Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * value)
	{
		___s_Current_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Current_1), value);
	}

	inline static int32_t get_offset_of_s_MasterEvent_2() { return static_cast<int32_t>(offsetof(Event_t187FF6A6B357447B83EC2064823EE0AEC5263210_StaticFields, ___s_MasterEvent_2)); }
	inline Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * get_s_MasterEvent_2() const { return ___s_MasterEvent_2; }
	inline Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 ** get_address_of_s_MasterEvent_2() { return &___s_MasterEvent_2; }
	inline void set_s_MasterEvent_2(Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * value)
	{
		___s_MasterEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_MasterEvent_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_3() { return static_cast<int32_t>(offsetof(Event_t187FF6A6B357447B83EC2064823EE0AEC5263210_StaticFields, ___U3CU3Ef__switchU24map0_3)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_U3CU3Ef__switchU24map0_3() const { return ___U3CU3Ef__switchU24map0_3; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_U3CU3Ef__switchU24map0_3() { return &___U3CU3Ef__switchU24map0_3; }
	inline void set_U3CU3Ef__switchU24map0_3(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___U3CU3Ef__switchU24map0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Event
struct Event_t187FF6A6B357447B83EC2064823EE0AEC5263210_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Event
struct Event_t187FF6A6B357447B83EC2064823EE0AEC5263210_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // EVENT_T187FF6A6B357447B83EC2064823EE0AEC5263210_H
#ifndef EVENTMODIFIERS_TC34E3018F3697001F894187AF6E9E63D7E203061_H
#define EVENTMODIFIERS_TC34E3018F3697001F894187AF6E9E63D7E203061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventModifiers
struct  EventModifiers_tC34E3018F3697001F894187AF6E9E63D7E203061 
{
public:
	// System.Int32 UnityEngine.EventModifiers::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventModifiers_tC34E3018F3697001F894187AF6E9E63D7E203061, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTMODIFIERS_TC34E3018F3697001F894187AF6E9E63D7E203061_H
#ifndef EVENTTYPE_T3D3937E705A4506226002DAB22071B7B181DA57B_H
#define EVENTTYPE_T3D3937E705A4506226002DAB22071B7B181DA57B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventType
struct  EventType_t3D3937E705A4506226002DAB22071B7B181DA57B 
{
public:
	// System.Int32 UnityEngine.EventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventType_t3D3937E705A4506226002DAB22071B7B181DA57B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTYPE_T3D3937E705A4506226002DAB22071B7B181DA57B_H
#ifndef OPERATINGSYSTEMFAMILY_TB10B95DB611852B942F4B31CCD63B9955350F2EE_H
#define OPERATINGSYSTEMFAMILY_TB10B95DB611852B942F4B31CCD63B9955350F2EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.OperatingSystemFamily
struct  OperatingSystemFamily_tB10B95DB611852B942F4B31CCD63B9955350F2EE 
{
public:
	// System.Int32 UnityEngine.OperatingSystemFamily::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OperatingSystemFamily_tB10B95DB611852B942F4B31CCD63B9955350F2EE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATINGSYSTEMFAMILY_TB10B95DB611852B942F4B31CCD63B9955350F2EE_H
#ifndef RECTOFFSET_TED44B1176E93501050480416699D1F11BAE8C87A_H
#define RECTOFFSET_TED44B1176E93501050480416699D1F11BAE8C87A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectOffset
struct  RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject * ___m_SourceStyle_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A, ___m_SourceStyle_1)); }
	inline RuntimeObject * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline RuntimeObject ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(RuntimeObject * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
#endif // RECTOFFSET_TED44B1176E93501050480416699D1F11BAE8C87A_H
#ifndef CHARACTERTYPE_T8F81A6C20A3599F1ED24D754F6CA748CB8A53B9B_H
#define CHARACTERTYPE_T8F81A6C20A3599F1ED24D754F6CA748CB8A53B9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextEditor/CharacterType
struct  CharacterType_t8F81A6C20A3599F1ED24D754F6CA748CB8A53B9B 
{
public:
	// System.Int32 UnityEngine.TextEditor/CharacterType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CharacterType_t8F81A6C20A3599F1ED24D754F6CA748CB8A53B9B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERTYPE_T8F81A6C20A3599F1ED24D754F6CA748CB8A53B9B_H
#ifndef DBLCLICKSNAPPING_T82D31F14587749755F9CB1FF9E975DDBEF7630CE_H
#define DBLCLICKSNAPPING_T82D31F14587749755F9CB1FF9E975DDBEF7630CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextEditor/DblClickSnapping
struct  DblClickSnapping_t82D31F14587749755F9CB1FF9E975DDBEF7630CE 
{
public:
	// System.Byte UnityEngine.TextEditor/DblClickSnapping::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DblClickSnapping_t82D31F14587749755F9CB1FF9E975DDBEF7630CE, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DBLCLICKSNAPPING_T82D31F14587749755F9CB1FF9E975DDBEF7630CE_H
#ifndef DIRECTION_T037B7E74F9AA6F88B7D885230EFF56443547B9BD_H
#define DIRECTION_T037B7E74F9AA6F88B7D885230EFF56443547B9BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextEditor/Direction
struct  Direction_t037B7E74F9AA6F88B7D885230EFF56443547B9BD 
{
public:
	// System.Int32 UnityEngine.TextEditor/Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_t037B7E74F9AA6F88B7D885230EFF56443547B9BD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T037B7E74F9AA6F88B7D885230EFF56443547B9BD_H
#ifndef TEXTEDITOP_T197E102C6DE6BA0F6A2871DB17FA260B6E01E151_H
#define TEXTEDITOP_T197E102C6DE6BA0F6A2871DB17FA260B6E01E151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextEditor/TextEditOp
struct  TextEditOp_t197E102C6DE6BA0F6A2871DB17FA260B6E01E151 
{
public:
	// System.Int32 UnityEngine.TextEditor/TextEditOp::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextEditOp_t197E102C6DE6BA0F6A2871DB17FA260B6E01E151, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTEDITOP_T197E102C6DE6BA0F6A2871DB17FA260B6E01E151_H
#ifndef TOUCHSCREENKEYBOARDTYPE_TDD21D45735F3021BF4C6C7C1A660ABF03EBCE602_H
#define TOUCHSCREENKEYBOARDTYPE_TDD21D45735F3021BF4C6C7C1A660ABF03EBCE602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboardType
struct  TouchScreenKeyboardType_tDD21D45735F3021BF4C6C7C1A660ABF03EBCE602 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_tDD21D45735F3021BF4C6C7C1A660ABF03EBCE602, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARDTYPE_TDD21D45735F3021BF4C6C7C1A660ABF03EBCE602_H
#ifndef GUISTYLE_T671F175A201A19166385EE3392292A5F50070572_H
#define GUISTYLE_T671F175A201A19166385EE3392292A5F50070572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIStyle
struct  GUIStyle_t671F175A201A19166385EE3392292A5F50070572  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.GUIStyle::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Normal
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_Normal_1;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Hover
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_Hover_2;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Active
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_Active_3;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Focused
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_Focused_4;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnNormal
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_OnNormal_5;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnHover
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_OnHover_6;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnActive
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_OnActive_7;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnFocused
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_OnFocused_8;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Border
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Border_9;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Padding
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Padding_10;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Margin
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Margin_11;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Overflow
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Overflow_12;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Normal_1)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_Normal_1() const { return ___m_Normal_1; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_Normal_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normal_1), value);
	}

	inline static int32_t get_offset_of_m_Hover_2() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Hover_2)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_Hover_2() const { return ___m_Hover_2; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_Hover_2() { return &___m_Hover_2; }
	inline void set_m_Hover_2(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_Hover_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hover_2), value);
	}

	inline static int32_t get_offset_of_m_Active_3() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Active_3)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_Active_3() const { return ___m_Active_3; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_Active_3() { return &___m_Active_3; }
	inline void set_m_Active_3(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_Active_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Active_3), value);
	}

	inline static int32_t get_offset_of_m_Focused_4() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Focused_4)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_Focused_4() const { return ___m_Focused_4; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_Focused_4() { return &___m_Focused_4; }
	inline void set_m_Focused_4(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_Focused_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Focused_4), value);
	}

	inline static int32_t get_offset_of_m_OnNormal_5() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_OnNormal_5)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_OnNormal_5() const { return ___m_OnNormal_5; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_OnNormal_5() { return &___m_OnNormal_5; }
	inline void set_m_OnNormal_5(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_OnNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnNormal_5), value);
	}

	inline static int32_t get_offset_of_m_OnHover_6() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_OnHover_6)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_OnHover_6() const { return ___m_OnHover_6; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_OnHover_6() { return &___m_OnHover_6; }
	inline void set_m_OnHover_6(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_OnHover_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnHover_6), value);
	}

	inline static int32_t get_offset_of_m_OnActive_7() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_OnActive_7)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_OnActive_7() const { return ___m_OnActive_7; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_OnActive_7() { return &___m_OnActive_7; }
	inline void set_m_OnActive_7(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_OnActive_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnActive_7), value);
	}

	inline static int32_t get_offset_of_m_OnFocused_8() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_OnFocused_8)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_OnFocused_8() const { return ___m_OnFocused_8; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_OnFocused_8() { return &___m_OnFocused_8; }
	inline void set_m_OnFocused_8(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_OnFocused_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnFocused_8), value);
	}

	inline static int32_t get_offset_of_m_Border_9() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Border_9)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Border_9() const { return ___m_Border_9; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Border_9() { return &___m_Border_9; }
	inline void set_m_Border_9(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Border_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Border_9), value);
	}

	inline static int32_t get_offset_of_m_Padding_10() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Padding_10)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Padding_10() const { return ___m_Padding_10; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Padding_10() { return &___m_Padding_10; }
	inline void set_m_Padding_10(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Padding_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_10), value);
	}

	inline static int32_t get_offset_of_m_Margin_11() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Margin_11)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Margin_11() const { return ___m_Margin_11; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Margin_11() { return &___m_Margin_11; }
	inline void set_m_Margin_11(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Margin_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Margin_11), value);
	}

	inline static int32_t get_offset_of_m_Overflow_12() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Overflow_12)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Overflow_12() const { return ___m_Overflow_12; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Overflow_12() { return &___m_Overflow_12; }
	inline void set_m_Overflow_12(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Overflow_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Overflow_12), value);
	}
};

struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572_StaticFields
{
public:
	// System.Boolean UnityEngine.GUIStyle::showKeyboardFocus
	bool ___showKeyboardFocus_13;
	// UnityEngine.GUIStyle UnityEngine.GUIStyle::s_None
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___s_None_14;

public:
	inline static int32_t get_offset_of_showKeyboardFocus_13() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572_StaticFields, ___showKeyboardFocus_13)); }
	inline bool get_showKeyboardFocus_13() const { return ___showKeyboardFocus_13; }
	inline bool* get_address_of_showKeyboardFocus_13() { return &___showKeyboardFocus_13; }
	inline void set_showKeyboardFocus_13(bool value)
	{
		___showKeyboardFocus_13 = value;
	}

	inline static int32_t get_offset_of_s_None_14() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572_StaticFields, ___s_None_14)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_s_None_14() const { return ___s_None_14; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_s_None_14() { return &___s_None_14; }
	inline void set_s_None_14(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___s_None_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_None_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_Normal_1;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_Hover_2;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_Active_3;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_Focused_4;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_OnNormal_5;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_OnHover_6;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_OnActive_7;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_OnFocused_8;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_pinvoke ___m_Border_9;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_pinvoke ___m_Padding_10;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_pinvoke ___m_Margin_11;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_pinvoke ___m_Overflow_12;
};
// Native definition for COM marshalling of UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_Normal_1;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_Hover_2;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_Active_3;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_Focused_4;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_OnNormal_5;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_OnHover_6;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_OnActive_7;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_OnFocused_8;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com* ___m_Border_9;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com* ___m_Padding_10;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com* ___m_Margin_11;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com* ___m_Overflow_12;
};
#endif // GUISTYLE_T671F175A201A19166385EE3392292A5F50070572_H
#ifndef TEXTEDITOR_T72CB6095A5C38226E08CD8073D5B6AD98579D440_H
#define TEXTEDITOR_T72CB6095A5C38226E08CD8073D5B6AD98579D440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextEditor
struct  TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440  : public RuntimeObject
{
public:
	// UnityEngine.TouchScreenKeyboard UnityEngine.TextEditor::keyboardOnScreen
	TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90 * ___keyboardOnScreen_0;
	// System.Int32 UnityEngine.TextEditor::controlID
	int32_t ___controlID_1;
	// UnityEngine.GUIStyle UnityEngine.TextEditor::style
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___style_2;
	// System.Boolean UnityEngine.TextEditor::multiline
	bool ___multiline_3;
	// System.Boolean UnityEngine.TextEditor::hasHorizontalCursorPos
	bool ___hasHorizontalCursorPos_4;
	// System.Boolean UnityEngine.TextEditor::isPasswordField
	bool ___isPasswordField_5;
	// System.Boolean UnityEngine.TextEditor::m_HasFocus
	bool ___m_HasFocus_6;
	// UnityEngine.Vector2 UnityEngine.TextEditor::scrollOffset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___scrollOffset_7;
	// UnityEngine.GUIContent UnityEngine.TextEditor::m_Content
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___m_Content_8;
	// UnityEngine.Rect UnityEngine.TextEditor::m_Position
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___m_Position_9;
	// System.Int32 UnityEngine.TextEditor::m_CursorIndex
	int32_t ___m_CursorIndex_10;
	// System.Int32 UnityEngine.TextEditor::m_SelectIndex
	int32_t ___m_SelectIndex_11;
	// System.Boolean UnityEngine.TextEditor::m_RevealCursor
	bool ___m_RevealCursor_12;
	// UnityEngine.Vector2 UnityEngine.TextEditor::graphicalCursorPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___graphicalCursorPos_13;
	// UnityEngine.Vector2 UnityEngine.TextEditor::graphicalSelectCursorPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___graphicalSelectCursorPos_14;
	// System.Boolean UnityEngine.TextEditor::m_MouseDragSelectsWholeWords
	bool ___m_MouseDragSelectsWholeWords_15;
	// System.Int32 UnityEngine.TextEditor::m_DblClickInitPos
	int32_t ___m_DblClickInitPos_16;
	// UnityEngine.TextEditor/DblClickSnapping UnityEngine.TextEditor::m_DblClickSnap
	uint8_t ___m_DblClickSnap_17;
	// System.Boolean UnityEngine.TextEditor::m_bJustSelected
	bool ___m_bJustSelected_18;
	// System.Int32 UnityEngine.TextEditor::m_iAltCursorPos
	int32_t ___m_iAltCursorPos_19;
	// System.String UnityEngine.TextEditor::oldText
	String_t* ___oldText_20;
	// System.Int32 UnityEngine.TextEditor::oldPos
	int32_t ___oldPos_21;
	// System.Int32 UnityEngine.TextEditor::oldSelectPos
	int32_t ___oldSelectPos_22;

public:
	inline static int32_t get_offset_of_keyboardOnScreen_0() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___keyboardOnScreen_0)); }
	inline TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90 * get_keyboardOnScreen_0() const { return ___keyboardOnScreen_0; }
	inline TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90 ** get_address_of_keyboardOnScreen_0() { return &___keyboardOnScreen_0; }
	inline void set_keyboardOnScreen_0(TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90 * value)
	{
		___keyboardOnScreen_0 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardOnScreen_0), value);
	}

	inline static int32_t get_offset_of_controlID_1() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___controlID_1)); }
	inline int32_t get_controlID_1() const { return ___controlID_1; }
	inline int32_t* get_address_of_controlID_1() { return &___controlID_1; }
	inline void set_controlID_1(int32_t value)
	{
		___controlID_1 = value;
	}

	inline static int32_t get_offset_of_style_2() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___style_2)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_style_2() const { return ___style_2; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_style_2() { return &___style_2; }
	inline void set_style_2(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___style_2 = value;
		Il2CppCodeGenWriteBarrier((&___style_2), value);
	}

	inline static int32_t get_offset_of_multiline_3() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___multiline_3)); }
	inline bool get_multiline_3() const { return ___multiline_3; }
	inline bool* get_address_of_multiline_3() { return &___multiline_3; }
	inline void set_multiline_3(bool value)
	{
		___multiline_3 = value;
	}

	inline static int32_t get_offset_of_hasHorizontalCursorPos_4() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___hasHorizontalCursorPos_4)); }
	inline bool get_hasHorizontalCursorPos_4() const { return ___hasHorizontalCursorPos_4; }
	inline bool* get_address_of_hasHorizontalCursorPos_4() { return &___hasHorizontalCursorPos_4; }
	inline void set_hasHorizontalCursorPos_4(bool value)
	{
		___hasHorizontalCursorPos_4 = value;
	}

	inline static int32_t get_offset_of_isPasswordField_5() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___isPasswordField_5)); }
	inline bool get_isPasswordField_5() const { return ___isPasswordField_5; }
	inline bool* get_address_of_isPasswordField_5() { return &___isPasswordField_5; }
	inline void set_isPasswordField_5(bool value)
	{
		___isPasswordField_5 = value;
	}

	inline static int32_t get_offset_of_m_HasFocus_6() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___m_HasFocus_6)); }
	inline bool get_m_HasFocus_6() const { return ___m_HasFocus_6; }
	inline bool* get_address_of_m_HasFocus_6() { return &___m_HasFocus_6; }
	inline void set_m_HasFocus_6(bool value)
	{
		___m_HasFocus_6 = value;
	}

	inline static int32_t get_offset_of_scrollOffset_7() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___scrollOffset_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_scrollOffset_7() const { return ___scrollOffset_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_scrollOffset_7() { return &___scrollOffset_7; }
	inline void set_scrollOffset_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___scrollOffset_7 = value;
	}

	inline static int32_t get_offset_of_m_Content_8() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___m_Content_8)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_m_Content_8() const { return ___m_Content_8; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_m_Content_8() { return &___m_Content_8; }
	inline void set_m_Content_8(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___m_Content_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_8), value);
	}

	inline static int32_t get_offset_of_m_Position_9() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___m_Position_9)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_m_Position_9() const { return ___m_Position_9; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_m_Position_9() { return &___m_Position_9; }
	inline void set_m_Position_9(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___m_Position_9 = value;
	}

	inline static int32_t get_offset_of_m_CursorIndex_10() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___m_CursorIndex_10)); }
	inline int32_t get_m_CursorIndex_10() const { return ___m_CursorIndex_10; }
	inline int32_t* get_address_of_m_CursorIndex_10() { return &___m_CursorIndex_10; }
	inline void set_m_CursorIndex_10(int32_t value)
	{
		___m_CursorIndex_10 = value;
	}

	inline static int32_t get_offset_of_m_SelectIndex_11() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___m_SelectIndex_11)); }
	inline int32_t get_m_SelectIndex_11() const { return ___m_SelectIndex_11; }
	inline int32_t* get_address_of_m_SelectIndex_11() { return &___m_SelectIndex_11; }
	inline void set_m_SelectIndex_11(int32_t value)
	{
		___m_SelectIndex_11 = value;
	}

	inline static int32_t get_offset_of_m_RevealCursor_12() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___m_RevealCursor_12)); }
	inline bool get_m_RevealCursor_12() const { return ___m_RevealCursor_12; }
	inline bool* get_address_of_m_RevealCursor_12() { return &___m_RevealCursor_12; }
	inline void set_m_RevealCursor_12(bool value)
	{
		___m_RevealCursor_12 = value;
	}

	inline static int32_t get_offset_of_graphicalCursorPos_13() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___graphicalCursorPos_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_graphicalCursorPos_13() const { return ___graphicalCursorPos_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_graphicalCursorPos_13() { return &___graphicalCursorPos_13; }
	inline void set_graphicalCursorPos_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___graphicalCursorPos_13 = value;
	}

	inline static int32_t get_offset_of_graphicalSelectCursorPos_14() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___graphicalSelectCursorPos_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_graphicalSelectCursorPos_14() const { return ___graphicalSelectCursorPos_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_graphicalSelectCursorPos_14() { return &___graphicalSelectCursorPos_14; }
	inline void set_graphicalSelectCursorPos_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___graphicalSelectCursorPos_14 = value;
	}

	inline static int32_t get_offset_of_m_MouseDragSelectsWholeWords_15() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___m_MouseDragSelectsWholeWords_15)); }
	inline bool get_m_MouseDragSelectsWholeWords_15() const { return ___m_MouseDragSelectsWholeWords_15; }
	inline bool* get_address_of_m_MouseDragSelectsWholeWords_15() { return &___m_MouseDragSelectsWholeWords_15; }
	inline void set_m_MouseDragSelectsWholeWords_15(bool value)
	{
		___m_MouseDragSelectsWholeWords_15 = value;
	}

	inline static int32_t get_offset_of_m_DblClickInitPos_16() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___m_DblClickInitPos_16)); }
	inline int32_t get_m_DblClickInitPos_16() const { return ___m_DblClickInitPos_16; }
	inline int32_t* get_address_of_m_DblClickInitPos_16() { return &___m_DblClickInitPos_16; }
	inline void set_m_DblClickInitPos_16(int32_t value)
	{
		___m_DblClickInitPos_16 = value;
	}

	inline static int32_t get_offset_of_m_DblClickSnap_17() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___m_DblClickSnap_17)); }
	inline uint8_t get_m_DblClickSnap_17() const { return ___m_DblClickSnap_17; }
	inline uint8_t* get_address_of_m_DblClickSnap_17() { return &___m_DblClickSnap_17; }
	inline void set_m_DblClickSnap_17(uint8_t value)
	{
		___m_DblClickSnap_17 = value;
	}

	inline static int32_t get_offset_of_m_bJustSelected_18() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___m_bJustSelected_18)); }
	inline bool get_m_bJustSelected_18() const { return ___m_bJustSelected_18; }
	inline bool* get_address_of_m_bJustSelected_18() { return &___m_bJustSelected_18; }
	inline void set_m_bJustSelected_18(bool value)
	{
		___m_bJustSelected_18 = value;
	}

	inline static int32_t get_offset_of_m_iAltCursorPos_19() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___m_iAltCursorPos_19)); }
	inline int32_t get_m_iAltCursorPos_19() const { return ___m_iAltCursorPos_19; }
	inline int32_t* get_address_of_m_iAltCursorPos_19() { return &___m_iAltCursorPos_19; }
	inline void set_m_iAltCursorPos_19(int32_t value)
	{
		___m_iAltCursorPos_19 = value;
	}

	inline static int32_t get_offset_of_oldText_20() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___oldText_20)); }
	inline String_t* get_oldText_20() const { return ___oldText_20; }
	inline String_t** get_address_of_oldText_20() { return &___oldText_20; }
	inline void set_oldText_20(String_t* value)
	{
		___oldText_20 = value;
		Il2CppCodeGenWriteBarrier((&___oldText_20), value);
	}

	inline static int32_t get_offset_of_oldPos_21() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___oldPos_21)); }
	inline int32_t get_oldPos_21() const { return ___oldPos_21; }
	inline int32_t* get_address_of_oldPos_21() { return &___oldPos_21; }
	inline void set_oldPos_21(int32_t value)
	{
		___oldPos_21 = value;
	}

	inline static int32_t get_offset_of_oldSelectPos_22() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440, ___oldSelectPos_22)); }
	inline int32_t get_oldSelectPos_22() const { return ___oldSelectPos_22; }
	inline int32_t* get_address_of_oldSelectPos_22() { return &___oldSelectPos_22; }
	inline void set_oldSelectPos_22(int32_t value)
	{
		___oldSelectPos_22 = value;
	}
};

struct TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp> UnityEngine.TextEditor::s_Keyactions
	Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 * ___s_Keyactions_23;

public:
	inline static int32_t get_offset_of_s_Keyactions_23() { return static_cast<int32_t>(offsetof(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440_StaticFields, ___s_Keyactions_23)); }
	inline Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 * get_s_Keyactions_23() const { return ___s_Keyactions_23; }
	inline Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 ** get_address_of_s_Keyactions_23() { return &___s_Keyactions_23; }
	inline void set_s_Keyactions_23(Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 * value)
	{
		___s_Keyactions_23 = value;
		Il2CppCodeGenWriteBarrier((&___s_Keyactions_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTEDITOR_T72CB6095A5C38226E08CD8073D5B6AD98579D440_H
#ifndef TOUCHSCREENKEYBOARD_T2A69F85698E9780470181532D3F2BC903623FD90_H
#define TOUCHSCREENKEYBOARD_T2A69F85698E9780470181532D3F2BC903623FD90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboard
struct  TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.TouchScreenKeyboard::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Boolean UnityEngine.TouchScreenKeyboard::<canGetSelection>k__BackingField
	bool ___U3CcanGetSelectionU3Ek__BackingField_1;
	// System.Boolean UnityEngine.TouchScreenKeyboard::<canSetSelection>k__BackingField
	bool ___U3CcanSetSelectionU3Ek__BackingField_2;
	// UnityEngine.TouchScreenKeyboardType UnityEngine.TouchScreenKeyboard::<type>k__BackingField
	int32_t ___U3CtypeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_U3CcanGetSelectionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90, ___U3CcanGetSelectionU3Ek__BackingField_1)); }
	inline bool get_U3CcanGetSelectionU3Ek__BackingField_1() const { return ___U3CcanGetSelectionU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CcanGetSelectionU3Ek__BackingField_1() { return &___U3CcanGetSelectionU3Ek__BackingField_1; }
	inline void set_U3CcanGetSelectionU3Ek__BackingField_1(bool value)
	{
		___U3CcanGetSelectionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CcanSetSelectionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90, ___U3CcanSetSelectionU3Ek__BackingField_2)); }
	inline bool get_U3CcanSetSelectionU3Ek__BackingField_2() const { return ___U3CcanSetSelectionU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CcanSetSelectionU3Ek__BackingField_2() { return &___U3CcanSetSelectionU3Ek__BackingField_2; }
	inline void set_U3CcanSetSelectionU3Ek__BackingField_2(bool value)
	{
		___U3CcanSetSelectionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CtypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90, ___U3CtypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CtypeU3Ek__BackingField_3() const { return ___U3CtypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CtypeU3Ek__BackingField_3() { return &___U3CtypeU3Ek__BackingField_3; }
	inline void set_U3CtypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CtypeU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARD_T2A69F85698E9780470181532D3F2BC903623FD90_H


// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32Enum>::ContainsKey(!0)
extern "C" IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_mFA8A7C3AE75554E70838FCF41A5DF89B9B6FC0DB_gshared (Dictionary_2_t15935BA59D5EDF22B5075E957C7C05DEE12E3B57 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Int32Enum>::get_Item(!0)
extern "C" IL2CPP_METHOD_ATTR int32_t Dictionary_2_get_Item_m762CB847133E2354776C0BB968D07190DC136A22_gshared (Dictionary_2_t15935BA59D5EDF22B5075E957C7C05DEE12E3B57 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32Enum>::set_Item(!0,!1)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_m4A0DA06D7DEC22F36AE25DC7630B32A76C0E1749_gshared (Dictionary_2_t15935BA59D5EDF22B5075E957C7C05DEE12E3B57 * __this, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32Enum>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m1C8D9B685C8406F92F3C063FCA11D9AB5F56493E_gshared (Dictionary_2_t15935BA59D5EDF22B5075E957C7C05DEE12E3B57 * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// UnityEngine.GUIStyle UnityEngine.GUIStyle::get_none()
extern "C" IL2CPP_METHOD_ATTR GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * GUIStyle_get_none_m2B48FA4E2FA2B9BB7A458DF25B1391F9C7BB8B7B (const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8 (const RuntimeMethod* method);
// System.Void UnityEngine.GUIContent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GUIContent__ctor_m939CDEFF44061020E997A0F0640ADDF2740B0C38 (GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * __this, const RuntimeMethod* method);
// System.String UnityEngine.GUIContent::get_text()
extern "C" IL2CPP_METHOD_ATTR String_t* GUIContent_get_text_mAAFFFB0278FCB4F7A7661BE595BA42CD093E38C5 (GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GUIContent::set_text(System.String)
extern "C" IL2CPP_METHOD_ATTR void GUIContent_set_text_m866E0C5690119816D87D83124C81BDC0A0ED4316 (GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::EnsureValidCodePointIndex(System.Int32&)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_EnsureValidCodePointIndex_m5354F0315238BEAC78516935288D8BA67AE15C60 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t* ___index0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C" IL2CPP_METHOD_ATTR bool Rect_op_Equality_mFBE3505CEDD6B73F66276E782C1B02E0E5633563 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE  p0, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  p1, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_UpdateScrollOffset_mC49895B4151C725237B45F82FA3CB3E29BDDB02B (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// UnityEngine.Rect UnityEngine.TextEditor::get_position()
extern "C" IL2CPP_METHOD_ATTR Rect_t35B976DE901B5423C11705E156938EA27AB402CE  TextEditor_get_position_m4C5447E79B4B01068C4C6A47A757C4CF6483423F (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::set_selectIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::set_cursorIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectAll()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectAll_m33196916F8C32522554E30567291CF967CFD6EF9 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.TextEditor::get_cursorIndex()
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.GUIStyle::GetCursorPixelPosition(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GUIStyle_GetCursorPixelPosition_m028A8E1540C0E9EB435A289146B090EAA96CF1F0 (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * __this, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___position0, GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___content1, int32_t ___cursorStringIndex2, const RuntimeMethod* method);
// System.Int32 UnityEngine.TextEditor::get_selectIndex()
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::InitKeyActions()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_InitKeyActions_m724C451AA81FE63C7DC95DECD4AC681080AFCC11 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
extern "C" IL2CPP_METHOD_ATTR int32_t Event_get_modifiers_m4D1BDE843A9379F50C3F32CB78CCDAD84B779108 (Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)
extern "C" IL2CPP_METHOD_ATTR void Event_set_modifiers_mF9604DECDDDD4CC598E53E7A09C851F64CB84196 (Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_m886A3ED9D9AB620984BF26DF4B63571D37E2AE41 (Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 * __this, Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 *, Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 *, const RuntimeMethod*))Dictionary_2_ContainsKey_mFA8A7C3AE75554E70838FCF41A5DF89B9B6FC0DB_gshared)(__this, p0, method);
}
// !1 System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Item(!0)
inline int32_t Dictionary_2_get_Item_m15C699577EFD717DEEDFA3D6138A6BDC4C2B43E5 (Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 * __this, Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * p0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 *, Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 *, const RuntimeMethod*))Dictionary_2_get_Item_m762CB847133E2354776C0BB968D07190DC136A22_gshared)(__this, p0, method);
}
// System.Boolean UnityEngine.TextEditor::PerformOperation(UnityEngine.TextEditor/TextEditOp)
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_PerformOperation_m6A900A3FA32AC396A0674649EC52C6456476EA89 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___operation0, const RuntimeMethod* method);
// System.Boolean UnityEngine.TextEditor::get_hasSelection()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_get_hasSelection_mB7A3FE7A641A5DCB7E96D3FBB12A0304B8ACFD06 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_DeleteSelection_m74DE4B4D997A9F88D31A24E8B97484FE9FA77D0D (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.String UnityEngine.TextEditor::get_text()
extern "C" IL2CPP_METHOD_ATTR String_t* TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Char System.String::get_Chars(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96 (String_t* __this, int32_t p0, const RuntimeMethod* method);
// System.String System.String::Remove(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Remove_m54FD37F2B9CA7DBFE440B0CB8503640A2CFF00FF (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.TextEditor::FindEndOfPreviousWord(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_FindEndOfPreviousWord_m63E76A65AC4616A74902F9CF00BC5C20F2174F97 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.TextEditor::FindStartOfNextWord(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_FindStartOfNextWord_m31C0620A516CB2C598AA67A9D0931D562ED37828 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___p0, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018 (String_t* __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.TextEditor::NextCodePointIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 UnityEngine.TextEditor::PreviousCodePointIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::ClearCursorPos()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.String System.String::Insert(System.Int32,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Insert_m2525FE6F79C96A359A588C8FA764419EBD811749 (String_t* __this, int32_t p0, String_t* p1, const RuntimeMethod* method);
// System.String System.Char::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Char_ToString_mA42A88FEBA41B72D48BB24373E3101B7A91B6FD8 (Il2CppChar* __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_ReplaceSelection_m9534876530D3A70E2358F347A8F10096343BD456 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, String_t* ___replace0, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::DetectFocusChange()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_DetectFocusChange_mE73EABD5C139A2851856E4E039842F6AA66D0824 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::GrabGraphicalCursorPos()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_GrabGraphicalCursorPos_m19AF9FD7089329EAEC363D0646F95FC0F4D8A03F (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.GUIStyle::GetCursorStringIndex(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR int32_t GUIStyle_GetCursorStringIndex_m9824718B33E4BB17B1CD8A2C7D32D06C15CCAFAF (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * __this, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___position0, GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___content1, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___cursorPixelPosition2, const RuntimeMethod* method);
// System.Single UnityEngine.GUIStyle::get_lineHeight()
extern "C" IL2CPP_METHOD_ATTR float GUIStyle_get_lineHeight_mF1F02FEF06EAD632D35889F6396416EB50ADAB4C (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.TextEditor::GetGraphicalLineStart(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_GetGraphicalLineStart_m5F7F5A73CDDF5B3BD16E61F09AAAF14A0CF140AB (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.TextEditor::GetGraphicalLineEnd(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_GetGraphicalLineEnd_m55161AA566216C1A1D1A59E724356660F936B99D (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___p0, const RuntimeMethod* method);
// System.Int32 System.String::IndexOf(System.Char,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t String_IndexOf_m66F6178DB4B2F61F4FAFD8B75787D0AB142ADD7D (String_t* __this, Il2CppChar p0, int32_t p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.TextEditor::IndexOfEndOfLine(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_IndexOfEndOfLine_m2429A2AD71CA478CF6C17C578F4B1F0E65303BA1 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___startIndex0, const RuntimeMethod* method);
// System.Int32 System.String::LastIndexOf(System.Char,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t String_LastIndexOf_mC378BF6308D9D6A19AFA397CBD7046B2ED9B340B (String_t* __this, Il2CppChar p0, int32_t p1, const RuntimeMethod* method);
// UnityEngine.Event UnityEngine.Event::get_current()
extern "C" IL2CPP_METHOD_ATTR Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * Event_get_current_m072CEE599D11B8A9A916697E57C0F561188CDB2B (const RuntimeMethod* method);
// System.Boolean UnityEngine.Event::get_shift()
extern "C" IL2CPP_METHOD_ATTR bool Event_get_shift_m74FCE61864B9A7AD13623FA2E8F87FC4A9DBD7A9 (Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveCursorToPosition_Internal(UnityEngine.Vector2,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveCursorToPosition_Internal_mD66C2285C2B5F6EC3516820193DCB64936797704 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___cursorPosition0, bool ___shift1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.TextEditor::FindEndOfClassification(System.Int32,UnityEngine.TextEditor/Direction)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_FindEndOfClassification_m6B5731CDADA3B2AF8AEE067CA37690A77725E761 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___p0, int32_t ___dir1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Mathf_Max_mBDE4C6F1883EE3215CD7AE62550B2AC90592BC3F (int32_t p0, int32_t p1, const RuntimeMethod* method);
// UnityEngine.TextEditor/CharacterType UnityEngine.TextEditor::ClassifyChar(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 UnityEngine.TextEditor::FindNextSeperator(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_FindNextSeperator_m019CE71A55764D7325CDE64E83D3D5A49BE92184 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___startPos0, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveRight()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveRight_m59C55388AA97B5BBF3BD7EA1242A414D699200B7 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveLeft()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveLeft_mB89B657A867A5BDDD5C7C29746883BE2654A2C65 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Boolean System.Char::IsWhiteSpace(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool Char_IsWhiteSpace_m8E25F5E52D932FBA7D4D87A5A740128FA098DBE7 (String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Boolean System.Char::IsLetterOrDigit(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool Char_IsLetterOrDigit_m99BA6814DA5E508FAA97139C4DFE6368B655E2EB (String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.TextEditor::FindPrevSeperator(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_FindPrevSeperator_m46051223DF3979C42C9497D991B7C0D68A6CAA85 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___startPos0, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveWordRight()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveWordRight_m682304844C44DB8749BF5FCA4DB321682D1CEFDB (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveWordLeft()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveWordLeft_m615D5BF427B7E37C700C434B8630F6F9BCDCAA1C (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// UnityEngine.EventType UnityEngine.Event::get_type()
extern "C" IL2CPP_METHOD_ATTR int32_t Event_get_type_mAABE4A35E5658E0079A1518D318AF2592F51D6FA (Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_width()
extern "C" IL2CPP_METHOD_ATTR float Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_height()
extern "C" IL2CPP_METHOD_ATTR float Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// UnityEngine.RectOffset UnityEngine.GUIStyle::get_padding()
extern "C" IL2CPP_METHOD_ATTR RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * GUIStyle_get_padding_m5DF76AA03A313366D0DD8D577731DAC4FB83053A (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * __this, const RuntimeMethod* method);
// UnityEngine.Rect UnityEngine.RectOffset::Remove(UnityEngine.Rect)
extern "C" IL2CPP_METHOD_ATTR Rect_t35B976DE901B5423C11705E156938EA27AB402CE  RectOffset_Remove_m1A74003F469E4CE1B62D5BEC3FA1C1CA7122C85E (RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * __this, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.GUIStyle::CalcSize(UnityEngine.GUIContent)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GUIStyle_CalcSize_m122C915B2050F60D120BDDDBD84433F26EC21E9F (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * __this, GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___content0, const RuntimeMethod* method);
// System.Single UnityEngine.GUIStyle::CalcHeight(UnityEngine.GUIContent,System.Single)
extern "C" IL2CPP_METHOD_ATTR float GUIStyle_CalcHeight_m6F102200768409D1B2184A7FE1A56747AB7B59B5 (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * __this, GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___content0, float ___width1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, float p0, float p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C" IL2CPP_METHOD_ATTR int32_t RectOffset_get_left_mA86EC00866C1940134873E3A1565A1F700DE67AD (RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C" IL2CPP_METHOD_ATTR int32_t RectOffset_get_top_mBA813D4147BFBC079933054018437F411B6B41E1 (RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C" IL2CPP_METHOD_ATTR int32_t RectOffset_get_bottom_mE5162CADD266B59539E3EE1967EE9A74705E5632 (RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * __this, const RuntimeMethod* method);
// System.String UnityEngine.Input::get_compositionString()
extern "C" IL2CPP_METHOD_ATTR String_t* Input_get_compositionString_mAE7E520D248E55E8C99827380E1AB586C03C757E (const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Substring_m2C4AFF5E79DD8BADFD2DFBCF156BF728FBB8E1AE (String_t* __this, int32_t p0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.GUIStyle::get_contentOffset()
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GUIStyle_get_contentOffset_m9764FD32FFAD60090B0E0D6ABBE610A64AE8DAB2 (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p1, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::set_contentOffset(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle_set_contentOffset_mDE7B72F911E033B193DFBCD8A72902A9B8CAF0DA (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::set_Internal_clipOffset(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle_set_Internal_clipOffset_m2374646917D20BFA059BCFEF3E473C1AD95902B2 (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_x()
extern "C" IL2CPP_METHOD_ATTR float Rect_get_x_mC51A461F546D14832EB96B11A7198DADDE2597B7 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_y()
extern "C" IL2CPP_METHOD_ATTR float Rect_get_y_m53E3E4F62D9840FBEA751A66293038F1F5D1D45C (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Input::set_compositionCursorPos(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void Input_set_compositionCursorPos_m2FBB434ABF1961EE12CD4CE79CF0C4C6B477BF66 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::DrawWithTextSelection(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Int32,System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle_DrawWithTextSelection_m60783EB261EBD814D7FFC6131A87E675290BAEFC (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * __this, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___position0, GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___content1, int32_t ___controlID2, int32_t ___firstSelectedCharacter3, int32_t ___lastSelectedCharacter4, bool ___drawSelectionAsComposition5, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::DrawWithTextSelection(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle_DrawWithTextSelection_mF72D3C928F0A7DC73F11121858D040972B1A9807 (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * __this, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___position0, GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___content1, int32_t ___controlID2, int32_t ___firstSelectedCharacter3, int32_t ___lastSelectedCharacter4, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::DrawCursor(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle_DrawCursor_m2CE940C9FF3E2691960F1DE97D39CB1B3E0D2EE4 (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * __this, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___position0, GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___content1, int32_t ___controlID2, int32_t ___character3, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveUp()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveUp_m33A336334EE094B374C79D384742C10EE3F87CE9 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveDown()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveDown_mF8326F5B90AD880828693C184F97D02D1EC15FEE (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveLineStart()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveLineStart_m01C387F3CAD4B192C343E1FBD81D3D8C1A26BFE1 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveLineEnd()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveLineEnd_m6CD2F02FF6859D1465DB9FB6E805DBBB5EFE1485 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveToStartOfNextWord()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveToStartOfNextWord_mF8E0AF826783F4C751E47EB39558892E0D280911 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveToEndOfPreviousWord()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveToEndOfPreviousWord_mA1D6F6C37728A27568A2308F6033544BE41CC053 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveTextStart()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveTextStart_m154B56BBD4408FFA335AF28B8AC712D0D64ACEAD (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveTextEnd()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveTextEnd_mA4E9806443211054DBAD3A8809FF54E78BD14F16 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveParagraphForward()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveParagraphForward_mDCDEBAB394A840A65A97C676BD3A5FEFF5D6D323 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveParagraphBackward()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveParagraphBackward_m0E0D4995F67E1A93171351AC59D0C9D0F12E2ED1 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveGraphicalLineStart()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveGraphicalLineStart_m17B1028964B11E29FC4F400FD7737A7490989154 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::MoveGraphicalLineEnd()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveGraphicalLineEnd_m37A229D49BD65C1C6EBF036769E20332F65B9C12 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectLeft()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectLeft_m7B2639E4B74273199C4DD7B19D22A1469F92067F (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectRight()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectRight_m269F9770E5FD0525B55776C07E9971D6C66A78DE (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectUp()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectUp_mB4ED2C2AF3A2ACCDF5B3526AD833A579918F57AC (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectDown()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectDown_mDB4866E1E2147CF2E3A8A112089DBF39DC51E0EC (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectWordRight()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectWordRight_mC181E8E66228F232CF3B5F9799AC15DC8C44C585 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectWordLeft()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectWordLeft_m1094CAC46C35D29D80B3A086B4D5E874BE37C7D3 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectToEndOfPreviousWord()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectToEndOfPreviousWord_mDBD58F16716BF35033E3E942889A17BB8A7AEB41 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectToStartOfNextWord()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectToStartOfNextWord_m16F1549DC820938AB680CBC76D3B3B816BEC0037 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectTextStart()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectTextStart_m8562BCC674CDA850E3DBE8455E92B3CBB590436D (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectTextEnd()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectTextEnd_m18F2785E8502B02F3ED4B0C0E1C68CCD9B8408FD (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::ExpandSelectGraphicalLineStart()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_ExpandSelectGraphicalLineStart_mF13A47F89B658B025FBEAB8CDA30B121E4BAF474 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::ExpandSelectGraphicalLineEnd()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_ExpandSelectGraphicalLineEnd_m948098D63AB53DAA3AED6F864A00559B325E8D2C (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectParagraphForward()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectParagraphForward_m38A2B0EEF2DFB8B81A681EEC34E704FF90AB80E1 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectParagraphBackward()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectParagraphBackward_m72FEDA47BEDFBC046ED21F80768FD422FA63766E (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectGraphicalLineStart()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectGraphicalLineStart_m1C6C0A1E5D2E196FBD005D5815CF8CD4E923C8C0 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectGraphicalLineEnd()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectGraphicalLineEnd_mEE048AE3578021287AF9017356913423E4D2CA0F (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.TextEditor::Delete()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_Delete_m4B7A14DD74D23305D71ECF58B078EB1C99864FDC (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.TextEditor::Backspace()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_Backspace_m256A202133B82F0A43563BEEAC463CF808229CC7 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.TextEditor::Cut()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_Cut_mE1FCFFF9B77690296C732A0FE7183A69249DB819 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::Copy()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_Copy_mF21F9BE54FD96EEC2CAA33EB3595ACD3894BA4E2 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.TextEditor::Paste()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_Paste_m551DF86401B71947857EC27860AE3470C33B860B (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::SelectNone()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectNone_m4CDD0831BB48D920C158779FDF404EDABD56763D (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.TextEditor::DeleteWordBack()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_DeleteWordBack_mB48FCF1FBFABBC70C1EB7EA184B387BE12F16B50 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.TextEditor::DeleteLineBack()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_DeleteLineBack_mDFF1F1E2ABA2EBC1ECF020D63784467CD392F9A2 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.TextEditor::DeleteWordForward()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_DeleteWordForward_m94D25A0E9E8746E8B419E5F2D041305744734FCF (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * p0, const RuntimeMethod* method);
// System.Void UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)
extern "C" IL2CPP_METHOD_ATTR void GUIUtility_set_systemCopyBuffer_m8C87AFD05D32AB0C30A2203005A64A86DFE18BE6 (String_t* ___value0, const RuntimeMethod* method);
// System.String System.String::Replace(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Replace_m970DFB0A280952FA7D3BA20AB7A8FB9F80CF6470 (String_t* __this, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.String System.String::Replace(System.Char,System.Char)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Replace_m276641366A463205C185A9B3DC0E24ECB95122C9 (String_t* __this, Il2CppChar p0, Il2CppChar p1, const RuntimeMethod* method);
// System.String UnityEngine.GUIUtility::get_systemCopyBuffer()
extern "C" IL2CPP_METHOD_ATTR String_t* GUIUtility_get_systemCopyBuffer_m5C4EE0A0FDE696D4A1337480B20AF300E6A5624E (const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.String UnityEngine.TextEditor::ReplaceNewlinesWithSpaces(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* TextEditor_ReplaceNewlinesWithSpaces_mDEEE760A469D8700A24D803C0F184BFC8EDC4B1D (String_t* ___value0, const RuntimeMethod* method);
// UnityEngine.Event UnityEngine.Event::KeyboardEvent(System.String)
extern "C" IL2CPP_METHOD_ATTR Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * Event_KeyboardEvent_m42755FC3AFA26F90EBE7A428F7845C42BBF1A97D (String_t* ___key0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Item(!0,!1)
inline void Dictionary_2_set_Item_m461003B498B59A8F07384C0C472BC6AB93545D2C (Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 * __this, Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * p0, int32_t p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 *, Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 *, int32_t, const RuntimeMethod*))Dictionary_2_set_Item_m4A0DA06D7DEC22F36AE25DC7630B32A76C0E1749_gshared)(__this, p0, p1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor()
inline void Dictionary_2__ctor_mD6F5EEE24289730ADCFD7DE5DC90DC2E6B0BFF2F (Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 *, const RuntimeMethod*))Dictionary_2__ctor_m1C8D9B685C8406F92F3C063FCA11D9AB5F56493E_gshared)(__this, method);
}
// System.Void UnityEngine.TextEditor::MapKey(System.String,UnityEngine.TextEditor/TextEditOp)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305 (String_t* ___key0, int32_t ___action1, const RuntimeMethod* method);
// UnityEngine.OperatingSystemFamily UnityEngine.SystemInfo::get_operatingSystemFamily()
extern "C" IL2CPP_METHOD_ATTR int32_t SystemInfo_get_operatingSystemFamily_mA35FE1FF2DD6240B2880DC5F642D4A0CC2B58D8D (const RuntimeMethod* method);
// System.Int32 UnityEngine.GUIUtility::get_keyboardControl()
extern "C" IL2CPP_METHOD_ATTR int32_t GUIUtility_get_keyboardControl_mB580042A41CB6E9D3AB2E2CF0CEC00F64B2FC55F (const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::OnLostFocus()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_OnLostFocus_m792C976FAC66B6C5576E49406A2963F1D19DD507 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::OnFocus()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_OnFocus_mE285A1E80DF51FED810C9438DF15835BD1120A84 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Mathf_Clamp_mE1EA15D719BF2F632741D42DF96F0BC797A20389 (int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.TextEditor::ClampTextIndex(System.Int32&)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_ClampTextIndex_m3EC3F291912021B79AF56CDD804B8795191A629B (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t* ___index0, const RuntimeMethod* method);
// System.Boolean UnityEngine.TextEditor::IsValidCodePointIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_IsValidCodePointIndex_m0366054C7FF170C5FBE08469412517D10199AF47 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Boolean System.Char::IsLowSurrogate(System.Char)
extern "C" IL2CPP_METHOD_ATTR bool Char_IsLowSurrogate_m11EF790BE9683BDF04022FD055104AE7A22A6A9C (Il2CppChar p0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.SliderState::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SliderState__ctor_m8CBFEE8D20C2D8EB0B68B1B171D9396DE84B605E (SliderState_t6081D6F2CF6D0F1A13B2A2D255671B4053EC52CB * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.TextEditor::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TextEditor__ctor_m279158A237B393882E3CC2834C1F7CA7679F79CC (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor__ctor_m279158A237B393882E3CC2834C1F7CA7679F79CC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_keyboardOnScreen_0((TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90 *)NULL);
		__this->set_controlID_1(0);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t671F175A201A19166385EE3392292A5F50070572_il2cpp_TypeInfo_var);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_0 = GUIStyle_get_none_m2B48FA4E2FA2B9BB7A458DF25B1391F9C7BB8B7B(/*hidden argument*/NULL);
		__this->set_style_2(L_0);
		__this->set_multiline_3((bool)0);
		__this->set_hasHorizontalCursorPos_4((bool)0);
		__this->set_isPasswordField_5((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		__this->set_scrollOffset_7(L_1);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_2 = (GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 *)il2cpp_codegen_object_new(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_il2cpp_TypeInfo_var);
		GUIContent__ctor_m939CDEFF44061020E997A0F0640ADDF2740B0C38(L_2, /*hidden argument*/NULL);
		__this->set_m_Content_8(L_2);
		__this->set_m_CursorIndex_10(0);
		__this->set_m_SelectIndex_11(0);
		__this->set_m_RevealCursor_12((bool)0);
		__this->set_m_MouseDragSelectsWholeWords_15((bool)0);
		__this->set_m_DblClickInitPos_16(0);
		__this->set_m_DblClickSnap_17(0);
		__this->set_m_bJustSelected_18((bool)0);
		__this->set_m_iAltCursorPos_19((-1));
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.TextEditor::get_text()
extern "C" IL2CPP_METHOD_ATTR String_t* TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_0 = __this->get_m_Content_8();
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_mAAFFFB0278FCB4F7A7661BE595BA42CD093E38C5(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.TextEditor::set_text(System.String)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_set_text_mFF167B3EFD4D6F6260126315BE09CAD6BF5F04D0 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_set_text_mFF167B3EFD4D6F6260126315BE09CAD6BF5F04D0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * G_B1_1 = NULL;
	{
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_0 = __this->get_m_Content_8();
		String_t* L_1 = ___value0;
		String_t* L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			goto IL_0014;
		}
	}
	{
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0014:
	{
		NullCheck(G_B2_1);
		GUIContent_set_text_m866E0C5690119816D87D83124C81BDC0A0ED4316(G_B2_1, G_B2_0, /*hidden argument*/NULL);
		int32_t* L_4 = __this->get_address_of_m_CursorIndex_10();
		TextEditor_EnsureValidCodePointIndex_m5354F0315238BEAC78516935288D8BA67AE15C60(__this, (int32_t*)L_4, /*hidden argument*/NULL);
		int32_t* L_5 = __this->get_address_of_m_SelectIndex_11();
		TextEditor_EnsureValidCodePointIndex_m5354F0315238BEAC78516935288D8BA67AE15C60(__this, (int32_t*)L_5, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rect UnityEngine.TextEditor::get_position()
extern "C" IL2CPP_METHOD_ATTR Rect_t35B976DE901B5423C11705E156938EA27AB402CE  TextEditor_get_position_m4C5447E79B4B01068C4C6A47A757C4CF6483423F (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_0 = __this->get_m_Position_9();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.TextEditor::set_position(UnityEngine.Rect)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_set_position_mA051BD36CA4B264CC97CC739DF3702349E9617D2 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___value0, const RuntimeMethod* method)
{
	{
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_0 = __this->get_m_Position_9();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_1 = ___value0;
		bool L_2 = Rect_op_Equality_mFBE3505CEDD6B73F66276E782C1B02E0E5633563(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0024;
	}

IL_0017:
	{
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_3 = ___value0;
		__this->set_m_Position_9(L_3);
		TextEditor_UpdateScrollOffset_mC49895B4151C725237B45F82FA3CB3E29BDDB02B(__this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// UnityEngine.Rect UnityEngine.TextEditor::get_localPosition()
extern "C" IL2CPP_METHOD_ATTR Rect_t35B976DE901B5423C11705E156938EA27AB402CE  TextEditor_get_localPosition_m7640D023C7C3E646ECBE54E06EB5189A9724CA0C (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_0 = TextEditor_get_position_m4C5447E79B4B01068C4C6A47A757C4CF6483423F(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.TextEditor::get_cursorIndex()
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_CursorIndex_10();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.TextEditor::set_cursorIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_CursorIndex_10();
		V_0 = L_0;
		int32_t L_1 = ___value0;
		__this->set_m_CursorIndex_10(L_1);
		int32_t* L_2 = __this->get_address_of_m_CursorIndex_10();
		TextEditor_EnsureValidCodePointIndex_m5354F0315238BEAC78516935288D8BA67AE15C60(__this, (int32_t*)L_2, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_m_CursorIndex_10();
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0036;
		}
	}
	{
		__this->set_m_RevealCursor_12((bool)1);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.TextEditor::OnCursorIndexChange() */, __this);
	}

IL_0036:
	{
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::get_selectIndex()
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_SelectIndex_11();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.TextEditor::set_selectIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_SelectIndex_11();
		V_0 = L_0;
		int32_t L_1 = ___value0;
		__this->set_m_SelectIndex_11(L_1);
		int32_t* L_2 = __this->get_address_of_m_SelectIndex_11();
		TextEditor_EnsureValidCodePointIndex_m5354F0315238BEAC78516935288D8BA67AE15C60(__this, (int32_t*)L_2, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_m_SelectIndex_11();
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_002d;
		}
	}
	{
		VirtActionInvoker0::Invoke(7 /* System.Void UnityEngine.TextEditor::OnSelectIndexChange() */, __this);
	}

IL_002d:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::ClearCursorPos()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		__this->set_hasHorizontalCursorPos_4((bool)0);
		__this->set_m_iAltCursorPos_19((-1));
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnFocus()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_OnFocus_mE285A1E80DF51FED810C9438DF15835BD1120A84 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_multiline_3();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		V_0 = 0;
		int32_t L_1 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_2, /*hidden argument*/NULL);
		goto IL_0027;
	}

IL_0021:
	{
		TextEditor_SelectAll_m33196916F8C32522554E30567291CF967CFD6EF9(__this, /*hidden argument*/NULL);
	}

IL_0027:
	{
		__this->set_m_HasFocus_6((bool)1);
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnLostFocus()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_OnLostFocus_m792C976FAC66B6C5576E49406A2963F1D19DD507 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_OnLostFocus_m792C976FAC66B6C5576E49406A2963F1D19DD507_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_HasFocus_6((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		__this->set_scrollOffset_7(L_0);
		return;
	}
}
// System.Void UnityEngine.TextEditor::GrabGraphicalCursorPos()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_GrabGraphicalCursorPos_m19AF9FD7089329EAEC363D0646F95FC0F4D8A03F (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasHorizontalCursorPos_4();
		if (L_0)
		{
			goto IL_005b;
		}
	}
	{
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_1 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_2 = VirtFuncInvoker0< Rect_t35B976DE901B5423C11705E156938EA27AB402CE  >::Invoke(4 /* UnityEngine.Rect UnityEngine.TextEditor::get_localPosition() */, __this);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_3 = __this->get_m_Content_8();
		int32_t L_4 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = GUIStyle_GetCursorPixelPosition_m028A8E1540C0E9EB435A289146B090EAA96CF1F0(L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		__this->set_graphicalCursorPos_13(L_5);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_6 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_7 = VirtFuncInvoker0< Rect_t35B976DE901B5423C11705E156938EA27AB402CE  >::Invoke(4 /* UnityEngine.Rect UnityEngine.TextEditor::get_localPosition() */, __this);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_8 = __this->get_m_Content_8();
		int32_t L_9 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = GUIStyle_GetCursorPixelPosition_m028A8E1540C0E9EB435A289146B090EAA96CF1F0(L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		__this->set_graphicalSelectCursorPos_14(L_10);
		__this->set_hasHorizontalCursorPos_4((bool)0);
	}

IL_005b:
	{
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::HandleKeyEvent(UnityEngine.Event)
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_HandleKeyEvent_m40E63B226145A1FA73891E38403E444D4F395750 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * ___e0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_HandleKeyEvent_m40E63B226145A1FA73891E38403E444D4F395750_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		TextEditor_InitKeyActions_m724C451AA81FE63C7DC95DECD4AC681080AFCC11(__this, /*hidden argument*/NULL);
		Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * L_0 = ___e0;
		NullCheck(L_0);
		int32_t L_1 = Event_get_modifiers_m4D1BDE843A9379F50C3F32CB78CCDAD84B779108(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * L_2 = ___e0;
		Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4 = Event_get_modifiers_m4D1BDE843A9379F50C3F32CB78CCDAD84B779108(L_3, /*hidden argument*/NULL);
		NullCheck(L_3);
		Event_set_modifiers_mF9604DECDDDD4CC598E53E7A09C851F64CB84196(L_3, ((int32_t)((int32_t)L_4&(int32_t)((int32_t)-33))), /*hidden argument*/NULL);
		Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 * L_5 = ((TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440_StaticFields*)il2cpp_codegen_static_fields_for(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440_il2cpp_TypeInfo_var))->get_s_Keyactions_23();
		Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * L_6 = ___e0;
		NullCheck(L_5);
		bool L_7 = Dictionary_2_ContainsKey_m886A3ED9D9AB620984BF26DF4B63571D37E2AE41(L_5, L_6, /*hidden argument*/Dictionary_2_ContainsKey_m886A3ED9D9AB620984BF26DF4B63571D37E2AE41_RuntimeMethod_var);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 * L_8 = ((TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440_StaticFields*)il2cpp_codegen_static_fields_for(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440_il2cpp_TypeInfo_var))->get_s_Keyactions_23();
		Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * L_9 = ___e0;
		NullCheck(L_8);
		int32_t L_10 = Dictionary_2_get_Item_m15C699577EFD717DEEDFA3D6138A6BDC4C2B43E5(L_8, L_9, /*hidden argument*/Dictionary_2_get_Item_m15C699577EFD717DEEDFA3D6138A6BDC4C2B43E5_RuntimeMethod_var);
		V_1 = L_10;
		int32_t L_11 = V_1;
		TextEditor_PerformOperation_m6A900A3FA32AC396A0674649EC52C6456476EA89(__this, L_11, /*hidden argument*/NULL);
		Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * L_12 = ___e0;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		Event_set_modifiers_mF9604DECDDDD4CC598E53E7A09C851F64CB84196(L_12, L_13, /*hidden argument*/NULL);
		V_2 = (bool)1;
		goto IL_005e;
	}

IL_0050:
	{
		Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * L_14 = ___e0;
		int32_t L_15 = V_0;
		NullCheck(L_14);
		Event_set_modifiers_mF9604DECDDDD4CC598E53E7A09C851F64CB84196(L_14, L_15, /*hidden argument*/NULL);
		V_2 = (bool)0;
		goto IL_005e;
	}

IL_005e:
	{
		bool L_16 = V_2;
		return L_16;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteLineBack()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_DeleteLineBack_mDFF1F1E2ABA2EBC1ECF020D63784467CD392F9A2 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_mB7A3FE7A641A5DCB7E96D3FBB12A0304B8ACFD06(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		TextEditor_DeleteSelection_m74DE4B4D997A9F88D31A24E8B97484FE9FA77D0D(__this, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_00a3;
	}

IL_001b:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_1;
		V_2 = L_2;
		goto IL_0046;
	}

IL_0029:
	{
		String_t* L_3 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_2;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_3, L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_6 = V_2;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
		goto IL_0050;
	}

IL_0046:
	{
		int32_t L_7 = V_2;
		int32_t L_8 = L_7;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1));
		if (L_8)
		{
			goto IL_0029;
		}
	}

IL_0050:
	{
		int32_t L_9 = V_2;
		if ((!(((uint32_t)L_9) == ((uint32_t)(-1)))))
		{
			goto IL_0059;
		}
	}
	{
		V_1 = 0;
	}

IL_0059:
	{
		int32_t L_10 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) == ((int32_t)L_11)))
		{
			goto IL_009c;
		}
	}
	{
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_12 = __this->get_m_Content_8();
		String_t* L_13 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_14 = V_1;
		int32_t L_15 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_16 = V_1;
		NullCheck(L_13);
		String_t* L_17 = String_Remove_m54FD37F2B9CA7DBFE440B0CB8503640A2CFF00FF(L_13, L_14, ((int32_t)il2cpp_codegen_subtract((int32_t)L_15, (int32_t)L_16)), /*hidden argument*/NULL);
		NullCheck(L_12);
		GUIContent_set_text_m866E0C5690119816D87D83124C81BDC0A0ED4316(L_12, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_1;
		V_3 = L_18;
		int32_t L_19 = V_3;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_19, /*hidden argument*/NULL);
		int32_t L_20 = V_3;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_20, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_00a3;
	}

IL_009c:
	{
		V_0 = (bool)0;
		goto IL_00a3;
	}

IL_00a3:
	{
		bool L_21 = V_0;
		return L_21;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteWordBack()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_DeleteWordBack_mB48FCF1FBFABBC70C1EB7EA184B387BE12F16B50 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_mB7A3FE7A641A5DCB7E96D3FBB12A0304B8ACFD06(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		TextEditor_DeleteSelection_m74DE4B4D997A9F88D31A24E8B97484FE9FA77D0D(__this, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_0072;
	}

IL_001b:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_FindEndOfPreviousWord_m63E76A65AC4616A74902F9CF00BC5C20F2174F97(__this, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_1;
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_006b;
		}
	}
	{
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_5 = __this->get_m_Content_8();
		String_t* L_6 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		int32_t L_8 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		NullCheck(L_6);
		String_t* L_10 = String_Remove_m54FD37F2B9CA7DBFE440B0CB8503640A2CFF00FF(L_6, L_7, ((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)L_9)), /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIContent_set_text_m866E0C5690119816D87D83124C81BDC0A0ED4316(L_5, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_1;
		V_2 = L_11;
		int32_t L_12 = V_2;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_2;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_13, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_0072;
	}

IL_006b:
	{
		V_0 = (bool)0;
		goto IL_0072;
	}

IL_0072:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteWordForward()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_DeleteWordForward_m94D25A0E9E8746E8B419E5F2D041305744734FCF (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_mB7A3FE7A641A5DCB7E96D3FBB12A0304B8ACFD06(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		TextEditor_DeleteSelection_m74DE4B4D997A9F88D31A24E8B97484FE9FA77D0D(__this, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_0071;
	}

IL_001b:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_FindStartOfNextWord_m31C0620A516CB2C598AA67A9D0931D562ED37828(__this, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		String_t* L_4 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_3) >= ((int32_t)L_5)))
		{
			goto IL_006a;
		}
	}
	{
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_6 = __this->get_m_Content_8();
		String_t* L_7 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		int32_t L_10 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_11 = String_Remove_m54FD37F2B9CA7DBFE440B0CB8503640A2CFF00FF(L_7, L_8, ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)L_10)), /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIContent_set_text_m866E0C5690119816D87D83124C81BDC0A0ED4316(L_6, L_11, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_0071;
	}

IL_006a:
	{
		V_0 = (bool)0;
		goto IL_0071;
	}

IL_0071:
	{
		bool L_12 = V_0;
		return L_12;
	}
}
// System.Boolean UnityEngine.TextEditor::Delete()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_Delete_m4B7A14DD74D23305D71ECF58B078EB1C99864FDC (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = TextEditor_get_hasSelection_mB7A3FE7A641A5DCB7E96D3FBB12A0304B8ACFD06(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		TextEditor_DeleteSelection_m74DE4B4D997A9F88D31A24E8B97484FE9FA77D0D(__this, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_006f;
	}

IL_001b:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		String_t* L_2 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0068;
		}
	}
	{
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_4 = __this->get_m_Content_8();
		String_t* L_5 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_7 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40(__this, L_7, /*hidden argument*/NULL);
		int32_t L_9 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_10 = String_Remove_m54FD37F2B9CA7DBFE440B0CB8503640A2CFF00FF(L_5, L_6, ((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)L_9)), /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIContent_set_text_m866E0C5690119816D87D83124C81BDC0A0ED4316(L_4, L_10, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_006f;
	}

IL_0068:
	{
		V_0 = (bool)0;
		goto IL_006f;
	}

IL_006f:
	{
		bool L_11 = V_0;
		return L_11;
	}
}
// System.Boolean UnityEngine.TextEditor::Backspace()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_Backspace_m256A202133B82F0A43563BEEAC463CF808229CC7 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_mB7A3FE7A641A5DCB7E96D3FBB12A0304B8ACFD06(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		TextEditor_DeleteSelection_m74DE4B4D997A9F88D31A24E8B97484FE9FA77D0D(__this, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_0078;
	}

IL_001b:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9(__this, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_4 = __this->get_m_Content_8();
		String_t* L_5 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		int32_t L_7 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		NullCheck(L_5);
		String_t* L_9 = String_Remove_m54FD37F2B9CA7DBFE440B0CB8503640A2CFF00FF(L_5, L_6, ((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)L_8)), /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIContent_set_text_m866E0C5690119816D87D83124C81BDC0A0ED4316(L_4, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		V_2 = L_10;
		int32_t L_11 = V_2;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_2;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_12, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_0078;
	}

IL_0071:
	{
		V_0 = (bool)0;
		goto IL_0078;
	}

IL_0078:
	{
		bool L_13 = V_0;
		return L_13;
	}
}
// System.Void UnityEngine.TextEditor::SelectAll()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectAll_m33196916F8C32522554E30567291CF967CFD6EF9 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, 0, /*hidden argument*/NULL);
		String_t* L_0 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_0, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_1, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectNone()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectNone_m4CDD0831BB48D920C158779FDF404EDABD56763D (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_0, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::get_hasSelection()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_get_hasSelection_mB7A3FE7A641A5DCB7E96D3FBB12A0304B8ACFD06 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0018;
	}

IL_0018:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_DeleteSelection_m74DE4B4D997A9F88D31A24E8B97484FE9FA77D0D (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0019;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_00e2;
	}

IL_0019:
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_0082;
		}
	}
	{
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_4 = __this->get_m_Content_8();
		String_t* L_5 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_7 = String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB(L_5, 0, L_6, /*hidden argument*/NULL);
		String_t* L_8 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_9 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		String_t* L_10 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_10, /*hidden argument*/NULL);
		int32_t L_12 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_13 = String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB(L_8, L_9, ((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)L_12)), /*hidden argument*/NULL);
		String_t* L_14 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_7, L_13, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIContent_set_text_m866E0C5690119816D87D83124C81BDC0A0ED4316(L_4, L_14, /*hidden argument*/NULL);
		int32_t L_15 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_15, /*hidden argument*/NULL);
		goto IL_00d5;
	}

IL_0082:
	{
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_16 = __this->get_m_Content_8();
		String_t* L_17 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_18 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_19 = String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB(L_17, 0, L_18, /*hidden argument*/NULL);
		String_t* L_20 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_21 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		String_t* L_22 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_22, /*hidden argument*/NULL);
		int32_t L_24 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_25 = String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB(L_20, L_21, ((int32_t)il2cpp_codegen_subtract((int32_t)L_23, (int32_t)L_24)), /*hidden argument*/NULL);
		String_t* L_26 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_19, L_25, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUIContent_set_text_m866E0C5690119816D87D83124C81BDC0A0ED4316(L_16, L_26, /*hidden argument*/NULL);
		int32_t L_27 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_27, /*hidden argument*/NULL);
	}

IL_00d5:
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_00e2;
	}

IL_00e2:
	{
		bool L_28 = V_0;
		return L_28;
	}
}
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_ReplaceSelection_m9534876530D3A70E2358F347A8F10096343BD456 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, String_t* ___replace0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_DeleteSelection_m74DE4B4D997A9F88D31A24E8B97484FE9FA77D0D(__this, /*hidden argument*/NULL);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_0 = __this->get_m_Content_8();
		String_t* L_1 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		String_t* L_3 = ___replace0;
		NullCheck(L_1);
		String_t* L_4 = String_Insert_m2525FE6F79C96A359A588C8FA764419EBD811749(L_1, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIContent_set_text_m866E0C5690119816D87D83124C81BDC0A0ED4316(L_0, L_4, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		String_t* L_6 = ___replace0;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_6, /*hidden argument*/NULL);
		int32_t L_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_7));
		V_0 = L_8;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_9, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::Insert(System.Char)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_Insert_m541F3AEBE598F6A82C56195A47BF712C9D65DEF3 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, Il2CppChar ___c0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = Char_ToString_mA42A88FEBA41B72D48BB24373E3101B7A91B6FD8((Il2CppChar*)(&___c0), /*hidden argument*/NULL);
		TextEditor_ReplaceSelection_m9534876530D3A70E2358F347A8F10096343BD456(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveRight()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveRight_m59C55388AA97B5BBF3BD7EA1242A414D699200B7 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40(__this, L_2, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_3, /*hidden argument*/NULL);
		TextEditor_DetectFocusChange_mE73EABD5C139A2851856E4E039842F6AA66D0824(__this, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_4, /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0043:
	{
		int32_t L_5 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)L_6)))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_7 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_7, /*hidden argument*/NULL);
		goto IL_0072;
	}

IL_0066:
	{
		int32_t L_8 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_8, /*hidden argument*/NULL);
	}

IL_0072:
	{
	}

IL_0073:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveLeft()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveLeft_mB89B657A867A5BDDD5C7C29746883BE2654A2C65 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9(__this, L_2, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_4, /*hidden argument*/NULL);
		goto IL_0067;
	}

IL_0037:
	{
		int32_t L_5 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)L_6)))
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_7, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_005a:
	{
		int32_t L_8 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_8, /*hidden argument*/NULL);
	}

IL_0066:
	{
	}

IL_0067:
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveUp()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveUp_m33A336334EE094B374C79D384742C10EE3F87CE9 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_2, /*hidden argument*/NULL);
		goto IL_002f;
	}

IL_0023:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_3, /*hidden argument*/NULL);
	}

IL_002f:
	{
		TextEditor_GrabGraphicalCursorPos_m19AF9FD7089329EAEC363D0646F95FC0F4D8A03F(__this, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_4 = __this->get_address_of_graphicalCursorPos_13();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_5 = L_4;
		float L_6 = L_5->get_y_1();
		L_5->set_y_1(((float)il2cpp_codegen_subtract((float)L_6, (float)(1.0f))));
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_7 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_8 = VirtFuncInvoker0< Rect_t35B976DE901B5423C11705E156938EA27AB402CE  >::Invoke(4 /* UnityEngine.Rect UnityEngine.TextEditor::get_localPosition() */, __this);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_9 = __this->get_m_Content_8();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = __this->get_graphicalCursorPos_13();
		NullCheck(L_7);
		int32_t L_11 = GUIStyle_GetCursorStringIndex_m9824718B33E4BB17B1CD8A2C7D32D06C15CCAFAF(L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		int32_t L_12 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_14) > ((int32_t)0)))
		{
			goto IL_008a;
		}
	}
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
	}

IL_008a:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveDown()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveDown_mF8326F5B90AD880828693C184F97D02D1EC15FEE (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_2, /*hidden argument*/NULL);
		goto IL_002f;
	}

IL_0023:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_3, /*hidden argument*/NULL);
	}

IL_002f:
	{
		TextEditor_GrabGraphicalCursorPos_m19AF9FD7089329EAEC363D0646F95FC0F4D8A03F(__this, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_4 = __this->get_address_of_graphicalCursorPos_13();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_5 = L_4;
		float L_6 = L_5->get_y_1();
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_7 = __this->get_style_2();
		NullCheck(L_7);
		float L_8 = GUIStyle_get_lineHeight_mF1F02FEF06EAD632D35889F6396416EB50ADAB4C(L_7, /*hidden argument*/NULL);
		L_5->set_y_1(((float)il2cpp_codegen_add((float)L_6, (float)((float)il2cpp_codegen_add((float)L_8, (float)(5.0f))))));
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_9 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_10 = VirtFuncInvoker0< Rect_t35B976DE901B5423C11705E156938EA27AB402CE  >::Invoke(4 /* UnityEngine.Rect UnityEngine.TextEditor::get_localPosition() */, __this);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_11 = __this->get_m_Content_8();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12 = __this->get_graphicalCursorPos_13();
		NullCheck(L_9);
		int32_t L_13 = GUIStyle_GetCursorStringIndex_m9824718B33E4BB17B1CD8A2C7D32D06C15CCAFAF(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_0;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_15, /*hidden argument*/NULL);
		int32_t L_16 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		String_t* L_17 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		int32_t L_18 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_17, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)L_18))))
		{
			goto IL_00a0;
		}
	}
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
	}

IL_00a0:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveLineStart()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveLineStart_m01C387F3CAD4B192C343E1FBD81D3D8C1A26BFE1 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0023:
	{
		V_0 = G_B3_0;
		int32_t L_4 = V_0;
		V_1 = L_4;
		goto IL_0056;
	}

IL_002b:
	{
		String_t* L_5 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_5, L_6, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_8 = V_1;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		int32_t L_9 = V_2;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_2;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_10, /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0056:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = L_11;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)1));
		if (L_12)
		{
			goto IL_002b;
		}
	}
	{
		V_2 = 0;
		int32_t L_13 = V_2;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_14, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveLineEnd()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveLineEnd_m6CD2F02FF6859D1465DB9FB6E805DBBB5EFE1485 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0023:
	{
		V_0 = G_B3_0;
		int32_t L_4 = V_0;
		V_1 = L_4;
		String_t* L_5 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		goto IL_0066;
	}

IL_0037:
	{
		String_t* L_7 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_7, L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_10 = V_1;
		V_3 = L_10;
		int32_t L_11 = V_3;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_3;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_12, /*hidden argument*/NULL);
		goto IL_007d;
	}

IL_0061:
	{
		int32_t L_13 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0066:
	{
		int32_t L_14 = V_1;
		int32_t L_15 = V_2;
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_16 = V_2;
		V_3 = L_16;
		int32_t L_17 = V_3;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_3;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_18, /*hidden argument*/NULL);
	}

IL_007d:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveGraphicalLineStart()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveGraphicalLineStart_m17B1028964B11E29FC4F400FD7737A7490989154 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B2_0 = NULL;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B2_1 = NULL;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B1_0 = NULL;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B3_1 = NULL;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B3_2 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		G_B1_1 = __this;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			G_B2_1 = __this;
			goto IL_001f;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0025;
	}

IL_001f:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0025:
	{
		NullCheck(G_B3_1);
		int32_t L_4 = TextEditor_GetGraphicalLineStart_m5F7F5A73CDDF5B3BD16E61F09AAAF14A0CF140AB(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		NullCheck(G_B3_2);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(G_B3_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveGraphicalLineEnd()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveGraphicalLineEnd_m37A229D49BD65C1C6EBF036769E20332F65B9C12 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B2_0 = NULL;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B2_1 = NULL;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B1_0 = NULL;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B3_1 = NULL;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B3_2 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		G_B1_1 = __this;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			G_B2_1 = __this;
			goto IL_001f;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0025;
	}

IL_001f:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0025:
	{
		NullCheck(G_B3_1);
		int32_t L_4 = TextEditor_GetGraphicalLineEnd_m55161AA566216C1A1D1A59E724356660F936B99D(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		NullCheck(G_B3_2);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(G_B3_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveTextStart()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveTextStart_m154B56BBD4408FFA335AF28B8AC712D0D64ACEAD (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveTextEnd()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveTextEnd_mA4E9806443211054DBAD3A8809FF54E78BD14F16 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::IndexOfEndOfLine(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_IndexOfEndOfLine_m2429A2AD71CA478CF6C17C578F4B1F0E65303BA1 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___startIndex0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___startIndex0;
		NullCheck(L_0);
		int32_t L_2 = String_IndexOf_m66F6178DB4B2F61F4FAFD8B75787D0AB142ADD7D(L_0, ((int32_t)10), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_4 = V_0;
		G_B3_0 = L_4;
		goto IL_0028;
	}

IL_001d:
	{
		String_t* L_5 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
	}

IL_0028:
	{
		V_1 = G_B3_0;
		goto IL_002e;
	}

IL_002e:
	{
		int32_t L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.TextEditor::MoveParagraphForward()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveParagraphForward_mDCDEBAB394A840A65A97C676BD3A5FEFF5D6D323 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B2_0 = NULL;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B3_1 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0024;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0024:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		String_t* L_5 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) >= ((int32_t)L_6)))
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_IndexOfEndOfLine_m2429A2AD71CA478CF6C17C578F4B1F0E65303BA1(__this, ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_8;
		int32_t L_9 = V_0;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_10, /*hidden argument*/NULL);
	}

IL_005e:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveParagraphBackward()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveParagraphBackward_m0E0D4995F67E1A93171351AC59D0C9D0F12E2ED1 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B2_0 = NULL;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B3_1 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0024;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0024:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)1)))
		{
			goto IL_0062;
		}
	}
	{
		String_t* L_5 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_7 = String_LastIndexOf_mC378BF6308D9D6A19AFA397CBD7046B2ED9B340B(L_5, ((int32_t)10), ((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)2)), /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
		int32_t L_8 = V_0;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_9, /*hidden argument*/NULL);
		goto IL_0072;
	}

IL_0062:
	{
		V_0 = 0;
		int32_t L_10 = V_0;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_11, /*hidden argument*/NULL);
	}

IL_0072:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveCursorToPosition(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveCursorToPosition_m71D34646EB403858B7DFE794825DA71DC9CAEF7E (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___cursorPosition0, const RuntimeMethod* method)
{
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___cursorPosition0;
		Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * L_1 = Event_get_current_m072CEE599D11B8A9A916697E57C0F561188CDB2B(/*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = Event_get_shift_m74FCE61864B9A7AD13623FA2E8F87FC4A9DBD7A9(L_1, /*hidden argument*/NULL);
		TextEditor_MoveCursorToPosition_Internal_mD66C2285C2B5F6EC3516820193DCB64936797704(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveCursorToPosition_Internal(UnityEngine.Vector2,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveCursorToPosition_Internal_mD66C2285C2B5F6EC3516820193DCB64936797704 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___cursorPosition0, bool ___shift1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_MoveCursorToPosition_Internal_mD66C2285C2B5F6EC3516820193DCB64936797704_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_0 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_1 = VirtFuncInvoker0< Rect_t35B976DE901B5423C11705E156938EA27AB402CE  >::Invoke(4 /* UnityEngine.Rect UnityEngine.TextEditor::get_localPosition() */, __this);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_2 = __this->get_m_Content_8();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = ___cursorPosition0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = __this->get_scrollOffset_7();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_6 = GUIStyle_GetCursorStringIndex_m9824718B33E4BB17B1CD8A2C7D32D06C15CCAFAF(L_0, L_1, L_2, L_5, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_6, /*hidden argument*/NULL);
		bool L_7 = ___shift1;
		if (L_7)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_8 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_8, /*hidden argument*/NULL);
	}

IL_003e:
	{
		TextEditor_DetectFocusChange_mE73EABD5C139A2851856E4E039842F6AA66D0824(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectToPosition(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectToPosition_m2EE616963923B9561A7C3CFFA392A0C4FE8D8C93 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___cursorPosition0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_SelectToPosition_m2EE616963923B9561A7C3CFFA392A0C4FE8D8C93_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_MouseDragSelectsWholeWords_15();
		if (L_0)
		{
			goto IL_003a;
		}
	}
	{
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_1 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_2 = VirtFuncInvoker0< Rect_t35B976DE901B5423C11705E156938EA27AB402CE  >::Invoke(4 /* UnityEngine.Rect UnityEngine.TextEditor::get_localPosition() */, __this);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_3 = __this->get_m_Content_8();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = ___cursorPosition0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = __this->get_scrollOffset_7();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_6 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_7 = GUIStyle_GetCursorStringIndex_m9824718B33E4BB17B1CD8A2C7D32D06C15CCAFAF(L_1, L_2, L_3, L_6, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_7, /*hidden argument*/NULL);
		goto IL_0197;
	}

IL_003a:
	{
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_8 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_9 = VirtFuncInvoker0< Rect_t35B976DE901B5423C11705E156938EA27AB402CE  >::Invoke(4 /* UnityEngine.Rect UnityEngine.TextEditor::get_localPosition() */, __this);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_10 = __this->get_m_Content_8();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_11 = ___cursorPosition0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12 = __this->get_scrollOffset_7();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_13 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_14 = GUIStyle_GetCursorStringIndex_m9824718B33E4BB17B1CD8A2C7D32D06C15CCAFAF(L_8, L_9, L_10, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		TextEditor_EnsureValidCodePointIndex_m5354F0315238BEAC78516935288D8BA67AE15C60(__this, (int32_t*)(&V_0), /*hidden argument*/NULL);
		int32_t* L_15 = __this->get_address_of_m_DblClickInitPos_16();
		TextEditor_EnsureValidCodePointIndex_m5354F0315238BEAC78516935288D8BA67AE15C60(__this, (int32_t*)L_15, /*hidden argument*/NULL);
		uint8_t L_16 = __this->get_m_DblClickSnap_17();
		if (L_16)
		{
			goto IL_00dc;
		}
	}
	{
		int32_t L_17 = V_0;
		int32_t L_18 = __this->get_m_DblClickInitPos_16();
		if ((((int32_t)L_17) >= ((int32_t)L_18)))
		{
			goto IL_00b3;
		}
	}
	{
		int32_t L_19 = V_0;
		int32_t L_20 = TextEditor_FindEndOfClassification_m6B5731CDADA3B2AF8AEE067CA37690A77725E761(__this, L_19, 1, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_20, /*hidden argument*/NULL);
		int32_t L_21 = __this->get_m_DblClickInitPos_16();
		int32_t L_22 = TextEditor_FindEndOfClassification_m6B5731CDADA3B2AF8AEE067CA37690A77725E761(__this, L_21, 0, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_22, /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_00b3:
	{
		int32_t L_23 = V_0;
		int32_t L_24 = TextEditor_FindEndOfClassification_m6B5731CDADA3B2AF8AEE067CA37690A77725E761(__this, L_23, 0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_24, /*hidden argument*/NULL);
		int32_t L_25 = __this->get_m_DblClickInitPos_16();
		int32_t L_26 = TextEditor_FindEndOfClassification_m6B5731CDADA3B2AF8AEE067CA37690A77725E761(__this, L_25, 1, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_26, /*hidden argument*/NULL);
	}

IL_00d6:
	{
		goto IL_0196;
	}

IL_00dc:
	{
		int32_t L_27 = V_0;
		int32_t L_28 = __this->get_m_DblClickInitPos_16();
		if ((((int32_t)L_27) >= ((int32_t)L_28)))
		{
			goto IL_013a;
		}
	}
	{
		int32_t L_29 = V_0;
		if ((((int32_t)L_29) <= ((int32_t)0)))
		{
			goto IL_0114;
		}
	}
	{
		String_t* L_30 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_31 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		int32_t L_32 = Mathf_Max_mBDE4C6F1883EE3215CD7AE62550B2AC90592BC3F(0, ((int32_t)il2cpp_codegen_subtract((int32_t)L_31, (int32_t)2)), /*hidden argument*/NULL);
		NullCheck(L_30);
		int32_t L_33 = String_LastIndexOf_mC378BF6308D9D6A19AFA397CBD7046B2ED9B340B(L_30, ((int32_t)10), L_32, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1)), /*hidden argument*/NULL);
		goto IL_011b;
	}

IL_0114:
	{
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, 0, /*hidden argument*/NULL);
	}

IL_011b:
	{
		String_t* L_34 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_35 = __this->get_m_DblClickInitPos_16();
		NullCheck(L_34);
		int32_t L_36 = String_LastIndexOf_mC378BF6308D9D6A19AFA397CBD7046B2ED9B340B(L_34, ((int32_t)10), L_35, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_36, /*hidden argument*/NULL);
		goto IL_0195;
	}

IL_013a:
	{
		int32_t L_37 = V_0;
		String_t* L_38 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_38);
		int32_t L_39 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_38, /*hidden argument*/NULL);
		if ((((int32_t)L_37) >= ((int32_t)L_39)))
		{
			goto IL_0160;
		}
	}
	{
		int32_t L_40 = V_0;
		int32_t L_41 = TextEditor_IndexOfEndOfLine_m2429A2AD71CA478CF6C17C578F4B1F0E65303BA1(__this, L_40, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_41, /*hidden argument*/NULL);
		goto IL_0171;
	}

IL_0160:
	{
		String_t* L_42 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_42, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_43, /*hidden argument*/NULL);
	}

IL_0171:
	{
		String_t* L_44 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_45 = __this->get_m_DblClickInitPos_16();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		int32_t L_46 = Mathf_Max_mBDE4C6F1883EE3215CD7AE62550B2AC90592BC3F(0, ((int32_t)il2cpp_codegen_subtract((int32_t)L_45, (int32_t)2)), /*hidden argument*/NULL);
		NullCheck(L_44);
		int32_t L_47 = String_LastIndexOf_mC378BF6308D9D6A19AFA397CBD7046B2ED9B340B(L_44, ((int32_t)10), L_46, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, ((int32_t)il2cpp_codegen_add((int32_t)L_47, (int32_t)1)), /*hidden argument*/NULL);
	}

IL_0195:
	{
	}

IL_0196:
	{
	}

IL_0197:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectLeft()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectLeft_m7B2639E4B74273199C4DD7B19D22A1469F92067F (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_bJustSelected_18();
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		__this->set_m_bJustSelected_18((bool)0);
		int32_t L_6 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_7 = TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9(__this, L_6, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectRight()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectRight_m269F9770E5FD0525B55776C07E9971D6C66A78DE (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_bJustSelected_18();
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		__this->set_m_bJustSelected_18((bool)0);
		int32_t L_6 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_7 = TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40(__this, L_6, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectUp()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectUp_mB4ED2C2AF3A2ACCDF5B3526AD833A579918F57AC (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		TextEditor_GrabGraphicalCursorPos_m19AF9FD7089329EAEC363D0646F95FC0F4D8A03F(__this, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_0 = __this->get_address_of_graphicalCursorPos_13();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_1 = L_0;
		float L_2 = L_1->get_y_1();
		L_1->set_y_1(((float)il2cpp_codegen_subtract((float)L_2, (float)(1.0f))));
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_3 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_4 = VirtFuncInvoker0< Rect_t35B976DE901B5423C11705E156938EA27AB402CE  >::Invoke(4 /* UnityEngine.Rect UnityEngine.TextEditor::get_localPosition() */, __this);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_5 = __this->get_m_Content_8();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_6 = __this->get_graphicalCursorPos_13();
		NullCheck(L_3);
		int32_t L_7 = GUIStyle_GetCursorStringIndex_m9824718B33E4BB17B1CD8A2C7D32D06C15CCAFAF(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectDown()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectDown_mDB4866E1E2147CF2E3A8A112089DBF39DC51E0EC (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		TextEditor_GrabGraphicalCursorPos_m19AF9FD7089329EAEC363D0646F95FC0F4D8A03F(__this, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_0 = __this->get_address_of_graphicalCursorPos_13();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_1 = L_0;
		float L_2 = L_1->get_y_1();
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_3 = __this->get_style_2();
		NullCheck(L_3);
		float L_4 = GUIStyle_get_lineHeight_mF1F02FEF06EAD632D35889F6396416EB50ADAB4C(L_3, /*hidden argument*/NULL);
		L_1->set_y_1(((float)il2cpp_codegen_add((float)L_2, (float)((float)il2cpp_codegen_add((float)L_4, (float)(5.0f))))));
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_5 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_6 = VirtFuncInvoker0< Rect_t35B976DE901B5423C11705E156938EA27AB402CE  >::Invoke(4 /* UnityEngine.Rect UnityEngine.TextEditor::get_localPosition() */, __this);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_7 = __this->get_m_Content_8();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = __this->get_graphicalCursorPos_13();
		NullCheck(L_5);
		int32_t L_9 = GUIStyle_GetCursorStringIndex_m9824718B33E4BB17B1CD8A2C7D32D06C15CCAFAF(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectTextEnd()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectTextEnd_m18F2785E8502B02F3ED4B0C0E1C68CCD9B8408FD (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectTextStart()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectTextStart_m8562BCC674CDA850E3DBE8455E92B3CBB590436D (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MouseDragSelectsWholeWords(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MouseDragSelectsWholeWords_m7F93A5149B5B5E78852BC1C5321CD7B7EE98F3DA (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, bool ___on0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___on0;
		__this->set_m_MouseDragSelectsWholeWords_15(L_0);
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		__this->set_m_DblClickInitPos_16(L_1);
		return;
	}
}
// System.Void UnityEngine.TextEditor::DblClickSnap(UnityEngine.TextEditor/DblClickSnapping)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_DblClickSnap_mC20D59EAA328DA3D0A74352F94E71B426655DF8D (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, uint8_t ___snapping0, const RuntimeMethod* method)
{
	{
		uint8_t L_0 = ___snapping0;
		__this->set_m_DblClickSnap_17(L_0);
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::GetGraphicalLineStart(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_GetGraphicalLineStart_m5F7F5A73CDDF5B3BD16E61F09AAAF14A0CF140AB (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___p0, const RuntimeMethod* method)
{
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_0 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_1 = VirtFuncInvoker0< Rect_t35B976DE901B5423C11705E156938EA27AB402CE  >::Invoke(4 /* UnityEngine.Rect UnityEngine.TextEditor::get_localPosition() */, __this);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_2 = __this->get_m_Content_8();
		int32_t L_3 = ___p0;
		NullCheck(L_0);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = GUIStyle_GetCursorPixelPosition_m028A8E1540C0E9EB435A289146B090EAA96CF1F0(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		(&V_0)->set_x_0((0.0f));
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_5 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_6 = VirtFuncInvoker0< Rect_t35B976DE901B5423C11705E156938EA27AB402CE  >::Invoke(4 /* UnityEngine.Rect UnityEngine.TextEditor::get_localPosition() */, __this);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_7 = __this->get_m_Content_8();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = GUIStyle_GetCursorStringIndex_m9824718B33E4BB17B1CD8A2C7D32D06C15CCAFAF(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		goto IL_0044;
	}

IL_0044:
	{
		int32_t L_10 = V_1;
		return L_10;
	}
}
// System.Int32 UnityEngine.TextEditor::GetGraphicalLineEnd(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_GetGraphicalLineEnd_m55161AA566216C1A1D1A59E724356660F936B99D (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___p0, const RuntimeMethod* method)
{
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_0 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_1 = VirtFuncInvoker0< Rect_t35B976DE901B5423C11705E156938EA27AB402CE  >::Invoke(4 /* UnityEngine.Rect UnityEngine.TextEditor::get_localPosition() */, __this);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_2 = __this->get_m_Content_8();
		int32_t L_3 = ___p0;
		NullCheck(L_0);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = GUIStyle_GetCursorPixelPosition_m028A8E1540C0E9EB435A289146B090EAA96CF1F0(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_5 = (&V_0);
		float L_6 = L_5->get_x_0();
		L_5->set_x_0(((float)il2cpp_codegen_add((float)L_6, (float)(5000.0f))));
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_7 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_8 = VirtFuncInvoker0< Rect_t35B976DE901B5423C11705E156938EA27AB402CE  >::Invoke(4 /* UnityEngine.Rect UnityEngine.TextEditor::get_localPosition() */, __this);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_9 = __this->get_m_Content_8();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = V_0;
		NullCheck(L_7);
		int32_t L_11 = GUIStyle_GetCursorStringIndex_m9824718B33E4BB17B1CD8A2C7D32D06C15CCAFAF(L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		goto IL_004b;
	}

IL_004b:
	{
		int32_t L_12 = V_1;
		return L_12;
	}
}
// System.Int32 UnityEngine.TextEditor::FindNextSeperator(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_FindNextSeperator_m019CE71A55764D7325CDE64E83D3D5A49BE92184 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___startPos0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_001b;
	}

IL_0012:
	{
		int32_t L_2 = ___startPos0;
		int32_t L_3 = TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40(__this, L_2, /*hidden argument*/NULL);
		___startPos0 = L_3;
	}

IL_001b:
	{
		int32_t L_4 = ___startPos0;
		int32_t L_5 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_6 = ___startPos0;
		int32_t L_7 = TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD(__this, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0012;
		}
	}

IL_002e:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_8 = ___startPos0;
		int32_t L_9 = TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40(__this, L_8, /*hidden argument*/NULL);
		___startPos0 = L_9;
	}

IL_003c:
	{
		int32_t L_10 = ___startPos0;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) >= ((int32_t)L_11)))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_12 = ___startPos0;
		int32_t L_13 = TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD(__this, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0033;
		}
	}

IL_004f:
	{
		int32_t L_14 = ___startPos0;
		V_1 = L_14;
		goto IL_0056;
	}

IL_0056:
	{
		int32_t L_15 = V_1;
		return L_15;
	}
}
// System.Int32 UnityEngine.TextEditor::FindPrevSeperator(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_FindPrevSeperator_m46051223DF3979C42C9497D991B7C0D68A6CAA85 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___startPos0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___startPos0;
		int32_t L_1 = TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9(__this, L_0, /*hidden argument*/NULL);
		___startPos0 = L_1;
		goto IL_0018;
	}

IL_000f:
	{
		int32_t L_2 = ___startPos0;
		int32_t L_3 = TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9(__this, L_2, /*hidden argument*/NULL);
		___startPos0 = L_3;
	}

IL_0018:
	{
		int32_t L_4 = ___startPos0;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_5 = ___startPos0;
		int32_t L_6 = TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD(__this, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_000f;
		}
	}

IL_002b:
	{
		int32_t L_7 = ___startPos0;
		if (L_7)
		{
			goto IL_0038;
		}
	}
	{
		V_0 = 0;
		goto IL_0079;
	}

IL_0038:
	{
		goto IL_0046;
	}

IL_003d:
	{
		int32_t L_8 = ___startPos0;
		int32_t L_9 = TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9(__this, L_8, /*hidden argument*/NULL);
		___startPos0 = L_9;
	}

IL_0046:
	{
		int32_t L_10 = ___startPos0;
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_11 = ___startPos0;
		int32_t L_12 = TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD(__this, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003d;
		}
	}

IL_0059:
	{
		int32_t L_13 = ___startPos0;
		int32_t L_14 = TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD(__this, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_006c;
		}
	}
	{
		int32_t L_15 = ___startPos0;
		V_0 = L_15;
		goto IL_0079;
	}

IL_006c:
	{
		int32_t L_16 = ___startPos0;
		int32_t L_17 = TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40(__this, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		goto IL_0079;
	}

IL_0079:
	{
		int32_t L_18 = V_0;
		return L_18;
	}
}
// System.Void UnityEngine.TextEditor::MoveWordRight()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveWordRight_m682304844C44DB8749BF5FCA4DB321682D1CEFDB (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B2_0 = NULL;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B3_1 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0024;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0024:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_FindNextSeperator_m019CE71A55764D7325CDE64E83D3D5A49BE92184(__this, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_7, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveToStartOfNextWord()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveToStartOfNextWord_mF8E0AF826783F4C751E47EB39558892E0D280911 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0024;
		}
	}
	{
		TextEditor_MoveRight_m59C55388AA97B5BBF3BD7EA1242A414D699200B7(__this, /*hidden argument*/NULL);
		goto IL_003f;
	}

IL_0024:
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_FindStartOfNextWord_m31C0620A516CB2C598AA67A9D0931D562ED37828(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_5, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveToEndOfPreviousWord()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveToEndOfPreviousWord_mA1D6F6C37728A27568A2308F6033544BE41CC053 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0024;
		}
	}
	{
		TextEditor_MoveLeft_mB89B657A867A5BDDD5C7C29746883BE2654A2C65(__this, /*hidden argument*/NULL);
		goto IL_003f;
	}

IL_0024:
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_FindEndOfPreviousWord_m63E76A65AC4616A74902F9CF00BC5C20F2174F97(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_5, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectToStartOfNextWord()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectToStartOfNextWord_m16F1549DC820938AB680CBC76D3B3B816BEC0037 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_FindStartOfNextWord_m31C0620A516CB2C598AA67A9D0931D562ED37828(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectToEndOfPreviousWord()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectToEndOfPreviousWord_mDBD58F16716BF35033E3E942889A17BB8A7AEB41 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_FindEndOfPreviousWord_m63E76A65AC4616A74902F9CF00BC5C20F2174F97(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.TextEditor/CharacterType UnityEngine.TextEditor::ClassifyChar(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		IL2CPP_RUNTIME_CLASS_INIT(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_il2cpp_TypeInfo_var);
		bool L_2 = Char_IsWhiteSpace_m8E25F5E52D932FBA7D4D87A5A740128FA098DBE7(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		V_0 = 3;
		goto IL_004b;
	}

IL_0019:
	{
		String_t* L_3 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_4 = ___index0;
		IL2CPP_RUNTIME_CLASS_INIT(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_il2cpp_TypeInfo_var);
		bool L_5 = Char_IsLetterOrDigit_m99BA6814DA5E508FAA97139C4DFE6368B655E2EB(L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003d;
		}
	}
	{
		String_t* L_6 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_7 = ___index0;
		NullCheck(L_6);
		Il2CppChar L_8 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_6, L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)39)))))
		{
			goto IL_0044;
		}
	}

IL_003d:
	{
		V_0 = 0;
		goto IL_004b;
	}

IL_0044:
	{
		V_0 = 1;
		goto IL_004b;
	}

IL_004b:
	{
		int32_t L_9 = V_0;
		return L_9;
	}
}
// System.Int32 UnityEngine.TextEditor::FindStartOfNextWord(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_FindStartOfNextWord_m31C0620A516CB2C598AA67A9D0931D562ED37828 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___p0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___p0;
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_4 = ___p0;
		V_1 = L_4;
		goto IL_0110;
	}

IL_001b:
	{
		int32_t L_5 = ___p0;
		int32_t L_6 = TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD(__this, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		int32_t L_7 = V_2;
		if ((((int32_t)L_7) == ((int32_t)3)))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_8 = ___p0;
		int32_t L_9 = TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40(__this, L_8, /*hidden argument*/NULL);
		___p0 = L_9;
		goto IL_0042;
	}

IL_0039:
	{
		int32_t L_10 = ___p0;
		int32_t L_11 = TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40(__this, L_10, /*hidden argument*/NULL);
		___p0 = L_11;
	}

IL_0042:
	{
		int32_t L_12 = ___p0;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) >= ((int32_t)L_13)))
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_14 = ___p0;
		int32_t L_15 = TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD(__this, L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_2;
		if ((((int32_t)L_15) == ((int32_t)L_16)))
		{
			goto IL_0039;
		}
	}

IL_0056:
	{
		goto IL_0091;
	}

IL_005c:
	{
		String_t* L_17 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_18 = ___p0;
		NullCheck(L_17);
		Il2CppChar L_19 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_17, L_18, /*hidden argument*/NULL);
		if ((((int32_t)L_19) == ((int32_t)((int32_t)9))))
		{
			goto IL_0083;
		}
	}
	{
		String_t* L_20 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_21 = ___p0;
		NullCheck(L_20);
		Il2CppChar L_22 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_20, L_21, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0090;
		}
	}

IL_0083:
	{
		int32_t L_23 = ___p0;
		int32_t L_24 = TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40(__this, L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		goto IL_0110;
	}

IL_0090:
	{
	}

IL_0091:
	{
		int32_t L_25 = ___p0;
		int32_t L_26 = V_0;
		if ((!(((uint32_t)L_25) == ((uint32_t)L_26))))
		{
			goto IL_009f;
		}
	}
	{
		int32_t L_27 = ___p0;
		V_1 = L_27;
		goto IL_0110;
	}

IL_009f:
	{
		String_t* L_28 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_29 = ___p0;
		NullCheck(L_28);
		Il2CppChar L_30 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_28, L_29, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)32)))))
		{
			goto IL_00db;
		}
	}
	{
		goto IL_00c1;
	}

IL_00b8:
	{
		int32_t L_31 = ___p0;
		int32_t L_32 = TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40(__this, L_31, /*hidden argument*/NULL);
		___p0 = L_32;
	}

IL_00c1:
	{
		int32_t L_33 = ___p0;
		int32_t L_34 = V_0;
		if ((((int32_t)L_33) >= ((int32_t)L_34)))
		{
			goto IL_00d5;
		}
	}
	{
		int32_t L_35 = ___p0;
		int32_t L_36 = TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD(__this, L_35, /*hidden argument*/NULL);
		if ((((int32_t)L_36) == ((int32_t)3)))
		{
			goto IL_00b8;
		}
	}

IL_00d5:
	{
		goto IL_0109;
	}

IL_00db:
	{
		String_t* L_37 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_38 = ___p0;
		NullCheck(L_37);
		Il2CppChar L_39 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_37, L_38, /*hidden argument*/NULL);
		if ((((int32_t)L_39) == ((int32_t)((int32_t)9))))
		{
			goto IL_0101;
		}
	}
	{
		String_t* L_40 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_41 = ___p0;
		NullCheck(L_40);
		Il2CppChar L_42 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_40, L_41, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_42) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0109;
		}
	}

IL_0101:
	{
		int32_t L_43 = ___p0;
		V_1 = L_43;
		goto IL_0110;
	}

IL_0109:
	{
		int32_t L_44 = ___p0;
		V_1 = L_44;
		goto IL_0110;
	}

IL_0110:
	{
		int32_t L_45 = V_1;
		return L_45;
	}
}
// System.Int32 UnityEngine.TextEditor::FindEndOfPreviousWord(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_FindEndOfPreviousWord_m63E76A65AC4616A74902F9CF00BC5C20F2174F97 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___p0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___p0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___p0;
		V_0 = L_1;
		goto IL_007f;
	}

IL_000e:
	{
		int32_t L_2 = ___p0;
		int32_t L_3 = TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9(__this, L_2, /*hidden argument*/NULL);
		___p0 = L_3;
		goto IL_0025;
	}

IL_001c:
	{
		int32_t L_4 = ___p0;
		int32_t L_5 = TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9(__this, L_4, /*hidden argument*/NULL);
		___p0 = L_5;
	}

IL_0025:
	{
		int32_t L_6 = ___p0;
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_7 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_8 = ___p0;
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_7, L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)((int32_t)32))))
		{
			goto IL_001c;
		}
	}

IL_003f:
	{
		int32_t L_10 = ___p0;
		int32_t L_11 = TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD(__this, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		int32_t L_12 = V_1;
		if ((((int32_t)L_12) == ((int32_t)3)))
		{
			goto IL_0078;
		}
	}
	{
		goto IL_005d;
	}

IL_0054:
	{
		int32_t L_13 = ___p0;
		int32_t L_14 = TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9(__this, L_13, /*hidden argument*/NULL);
		___p0 = L_14;
	}

IL_005d:
	{
		int32_t L_15 = ___p0;
		if ((((int32_t)L_15) <= ((int32_t)0)))
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_16 = ___p0;
		int32_t L_17 = TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9(__this, L_16, /*hidden argument*/NULL);
		int32_t L_18 = TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD(__this, L_17, /*hidden argument*/NULL);
		int32_t L_19 = V_1;
		if ((((int32_t)L_18) == ((int32_t)L_19)))
		{
			goto IL_0054;
		}
	}

IL_0077:
	{
	}

IL_0078:
	{
		int32_t L_20 = ___p0;
		V_0 = L_20;
		goto IL_007f;
	}

IL_007f:
	{
		int32_t L_21 = V_0;
		return L_21;
	}
}
// System.Void UnityEngine.TextEditor::MoveWordLeft()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MoveWordLeft_m615D5BF427B7E37C700C434B8630F6F9BCDCAA1C (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B2_0 = NULL;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B3_1 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0024;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0024:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_FindPrevSeperator_m46051223DF3979C42C9497D991B7C0D68A6CAA85(__this, L_4, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectWordRight()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectWordRight_mC181E8E66228F232CF3B5F9799AC15DC8C44C585 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B3_0 = NULL;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B4_1 = NULL;
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_3, /*hidden argument*/NULL);
		TextEditor_MoveWordRight_m682304844C44DB8749BF5FCA4DB321682D1CEFDB(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if ((((int32_t)L_5) >= ((int32_t)L_6)))
		{
			G_B3_0 = __this;
			goto IL_0056;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		goto IL_005c;
	}

IL_0056:
	{
		int32_t L_8 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
	}

IL_005c:
	{
		NullCheck(G_B4_1);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_0066:
	{
		int32_t L_9 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_9, /*hidden argument*/NULL);
		TextEditor_MoveWordRight_m682304844C44DB8749BF5FCA4DB321682D1CEFDB(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_10, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectWordLeft()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectWordLeft_m1094CAC46C35D29D80B3A086B4D5E874BE37C7D3 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B3_0 = NULL;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * G_B4_1 = NULL;
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_3, /*hidden argument*/NULL);
		TextEditor_MoveWordLeft_m615D5BF427B7E37C700C434B8630F6F9BCDCAA1C(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if ((((int32_t)L_5) <= ((int32_t)L_6)))
		{
			G_B3_0 = __this;
			goto IL_0056;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		goto IL_005c;
	}

IL_0056:
	{
		int32_t L_8 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
	}

IL_005c:
	{
		NullCheck(G_B4_1);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_0066:
	{
		int32_t L_9 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_9, /*hidden argument*/NULL);
		TextEditor_MoveWordLeft_m615D5BF427B7E37C700C434B8630F6F9BCDCAA1C(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_10, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::ExpandSelectGraphicalLineStart()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_ExpandSelectGraphicalLineStart_mF13A47F89B658B025FBEAB8CDA30B121E4BAF474 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_002f;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_GetGraphicalLineStart_m5F7F5A73CDDF5B3BD16E61F09AAAF14A0CF140AB(__this, L_2, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_3, /*hidden argument*/NULL);
		goto IL_0051;
	}

IL_002f:
	{
		int32_t L_4 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_GetGraphicalLineStart_m5F7F5A73CDDF5B3BD16E61F09AAAF14A0CF140AB(__this, L_5, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_7, /*hidden argument*/NULL);
	}

IL_0051:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::ExpandSelectGraphicalLineEnd()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_ExpandSelectGraphicalLineEnd_m948098D63AB53DAA3AED6F864A00559B325E8D2C (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_002f;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_GetGraphicalLineEnd_m55161AA566216C1A1D1A59E724356660F936B99D(__this, L_2, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_3, /*hidden argument*/NULL);
		goto IL_0051;
	}

IL_002f:
	{
		int32_t L_4 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_GetGraphicalLineEnd_m55161AA566216C1A1D1A59E724356660F936B99D(__this, L_5, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_7, /*hidden argument*/NULL);
	}

IL_0051:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectGraphicalLineStart()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectGraphicalLineStart_m1C6C0A1E5D2E196FBD005D5815CF8CD4E923C8C0 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_GetGraphicalLineStart_m5F7F5A73CDDF5B3BD16E61F09AAAF14A0CF140AB(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectGraphicalLineEnd()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectGraphicalLineEnd_mEE048AE3578021287AF9017356913423E4D2CA0F (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_GetGraphicalLineEnd_m55161AA566216C1A1D1A59E724356660F936B99D(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectParagraphForward()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectParagraphForward_m38A2B0EEF2DFB8B81A681EEC34E704FF90AB80E1 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_0) < ((int32_t)L_1))? 1 : 0);
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		String_t* L_3 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_2) >= ((int32_t)L_4)))
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_5 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_IndexOfEndOfLine_m2429A2AD71CA478CF6C17C578F4B1F0E65303BA1(__this, ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_6, /*hidden argument*/NULL);
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_8 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_9 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_8) <= ((int32_t)L_9)))
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_10 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_10, /*hidden argument*/NULL);
	}

IL_0064:
	{
	}

IL_0065:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectParagraphBackward()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectParagraphBackward_m72FEDA47BEDFBC046ED21F80768FD422FA63766E (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_0) > ((int32_t)L_1))? 1 : 0);
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		String_t* L_3 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_5 = String_LastIndexOf_mC378BF6308D9D6A19AFA397CBD7046B2ED9B340B(L_3, ((int32_t)10), ((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)2)), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)), /*hidden argument*/NULL);
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0063;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_7) >= ((int32_t)L_8)))
		{
			goto IL_0063;
		}
	}
	{
		int32_t L_9 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_9, /*hidden argument*/NULL);
	}

IL_0063:
	{
		goto IL_0079;
	}

IL_0069:
	{
		V_1 = 0;
		int32_t L_10 = V_1;
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_1;
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_11, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectCurrentWord()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectCurrentWord_m8DA526CC839A25CDD6E36FC411C2598C58362082 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_3 = V_0;
		int32_t L_4 = TextEditor_FindEndOfClassification_m6B5731CDADA3B2AF8AEE067CA37690A77725E761(__this, L_3, 1, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		int32_t L_6 = TextEditor_FindEndOfClassification_m6B5731CDADA3B2AF8AEE067CA37690A77725E761(__this, L_5, 0, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_6, /*hidden argument*/NULL);
		goto IL_005a;
	}

IL_003c:
	{
		int32_t L_7 = V_0;
		int32_t L_8 = TextEditor_FindEndOfClassification_m6B5731CDADA3B2AF8AEE067CA37690A77725E761(__this, L_7, 0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		int32_t L_10 = TextEditor_FindEndOfClassification_m6B5731CDADA3B2AF8AEE067CA37690A77725E761(__this, L_9, 1, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, L_10, /*hidden argument*/NULL);
	}

IL_005a:
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		__this->set_m_bJustSelected_18((bool)1);
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::FindEndOfClassification(System.Int32,UnityEngine.TextEditor/Direction)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_FindEndOfClassification_m6B5731CDADA3B2AF8AEE067CA37690A77725E761 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___p0, int32_t ___dir1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B12_0 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		V_0 = 0;
		goto IL_00d9;
	}

IL_0018:
	{
		int32_t L_2 = ___p0;
		String_t* L_3 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)L_4))))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_5 = ___p0;
		int32_t L_6 = TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9(__this, L_5, /*hidden argument*/NULL);
		___p0 = L_6;
	}

IL_0032:
	{
		int32_t L_7 = ___p0;
		int32_t L_8 = TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD(__this, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
	}

IL_003a:
	{
		int32_t L_9 = ___dir1;
		if ((((int32_t)L_9) == ((int32_t)1)))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_10 = ___dir1;
		if (!L_10)
		{
			goto IL_0081;
		}
	}
	{
		goto IL_00b1;
	}

IL_004d:
	{
		int32_t L_11 = ___p0;
		int32_t L_12 = TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9(__this, L_11, /*hidden argument*/NULL);
		___p0 = L_12;
		int32_t L_13 = ___p0;
		if (L_13)
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_14 = TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD(__this, 0, /*hidden argument*/NULL);
		int32_t L_15 = V_1;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_006f;
		}
	}
	{
		G_B12_0 = 0;
		goto IL_0076;
	}

IL_006f:
	{
		int32_t L_16 = TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40(__this, 0, /*hidden argument*/NULL);
		G_B12_0 = L_16;
	}

IL_0076:
	{
		V_0 = G_B12_0;
		goto IL_00d9;
	}

IL_007c:
	{
		goto IL_00b1;
	}

IL_0081:
	{
		int32_t L_17 = ___p0;
		int32_t L_18 = TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40(__this, L_17, /*hidden argument*/NULL);
		___p0 = L_18;
		int32_t L_19 = ___p0;
		String_t* L_20 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		int32_t L_21 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_20, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_19) == ((uint32_t)L_21))))
		{
			goto IL_00ac;
		}
	}
	{
		String_t* L_22 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_22, /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_00d9;
	}

IL_00ac:
	{
		goto IL_00b1;
	}

IL_00b1:
	{
		int32_t L_24 = ___p0;
		int32_t L_25 = TextEditor_ClassifyChar_mB0AD6C21D1A57F6979B994B754B92C9CA6734CDD(__this, L_24, /*hidden argument*/NULL);
		int32_t L_26 = V_1;
		if ((((int32_t)L_25) == ((int32_t)L_26)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_27 = ___dir1;
		if (L_27)
		{
			goto IL_00cc;
		}
	}
	{
		int32_t L_28 = ___p0;
		V_0 = L_28;
		goto IL_00d9;
	}

IL_00cc:
	{
		int32_t L_29 = ___p0;
		int32_t L_30 = TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40(__this, L_29, /*hidden argument*/NULL);
		V_0 = L_30;
		goto IL_00d9;
	}

IL_00d9:
	{
		int32_t L_31 = V_0;
		return L_31;
	}
}
// System.Void UnityEngine.TextEditor::SelectCurrentParagraph()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SelectCurrentParagraph_m5AD142E67B292CDC97299A100CC9F42F9C91CA86 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m28ECFE51A6B5604A5D3B0D5424D746512C146FFC(__this, /*hidden argument*/NULL);
		String_t* L_0 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_4 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_IndexOfEndOfLine_m2429A2AD71CA478CF6C17C578F4B1F0E65303BA1(__this, L_4, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m175414D37AEC8ED5B835A80063F9BA5B4A9B14D0(__this, ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_6 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_005d;
		}
	}
	{
		String_t* L_7 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_9 = String_LastIndexOf_mC378BF6308D9D6A19AFA397CBD7046B2ED9B340B(L_7, ((int32_t)10), ((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1)), /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m0496DFBB1C7C3C148A0F2BC81A0EADDF506C0B31(__this, ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::UpdateScrollOffsetIfNeeded(UnityEngine.Event)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_UpdateScrollOffsetIfNeeded_m43C5FA605EBC97A762B62C7B27CA31D8DF43561F (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * ___evt0, const RuntimeMethod* method)
{
	{
		Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * L_0 = ___evt0;
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_mAABE4A35E5658E0079A1518D318AF2592F51D6FA(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)7)))
		{
			goto IL_0021;
		}
	}
	{
		Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * L_2 = ___evt0;
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_mAABE4A35E5658E0079A1518D318AF2592F51D6FA(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)8)))
		{
			goto IL_0021;
		}
	}
	{
		TextEditor_UpdateScrollOffset_mC49895B4151C725237B45F82FA3CB3E29BDDB02B(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_UpdateScrollOffset_mC49895B4151C725237B45F82FA3CB3E29BDDB02B (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * G_B21_0 = NULL;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * G_B20_0 = NULL;
	float G_B22_0 = 0.0f;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * G_B22_1 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_1 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_2 = TextEditor_get_position_m4C5447E79B4B01068C4C6A47A757C4CF6483423F(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_1), /*hidden argument*/NULL);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_4 = TextEditor_get_position_m4C5447E79B4B01068C4C6A47A757C4CF6483423F(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_2), /*hidden argument*/NULL);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280((&L_6), (0.0f), (0.0f), L_3, L_5, /*hidden argument*/NULL);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_7 = __this->get_m_Content_8();
		int32_t L_8 = V_0;
		NullCheck(L_1);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9 = GUIStyle_GetCursorPixelPosition_m028A8E1540C0E9EB435A289146B090EAA96CF1F0(L_1, L_6, L_7, L_8, /*hidden argument*/NULL);
		__this->set_graphicalCursorPos_13(L_9);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_10 = __this->get_style_2();
		NullCheck(L_10);
		RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * L_11 = GUIStyle_get_padding_m5DF76AA03A313366D0DD8D577731DAC4FB83053A(L_10, /*hidden argument*/NULL);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_12 = TextEditor_get_position_m4C5447E79B4B01068C4C6A47A757C4CF6483423F(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_13 = RectOffset_Remove_m1A74003F469E4CE1B62D5BEC3FA1C1CA7122C85E(L_11, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_14 = __this->get_style_2();
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_15 = __this->get_m_Content_8();
		NullCheck(L_14);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_16 = GUIStyle_CalcSize_m122C915B2050F60D120BDDDBD84433F26EC21E9F(L_14, L_15, /*hidden argument*/NULL);
		V_5 = L_16;
		float L_17 = (&V_5)->get_x_0();
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_18 = __this->get_style_2();
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_19 = __this->get_m_Content_8();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_20 = TextEditor_get_position_m4C5447E79B4B01068C4C6A47A757C4CF6483423F(__this, /*hidden argument*/NULL);
		V_6 = L_20;
		float L_21 = Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_6), /*hidden argument*/NULL);
		NullCheck(L_18);
		float L_22 = GUIStyle_CalcHeight_m6F102200768409D1B2184A7FE1A56747AB7B59B5(L_18, L_19, L_21, /*hidden argument*/NULL);
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_4), L_17, L_22, /*hidden argument*/NULL);
		float L_23 = (&V_4)->get_x_0();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_24 = TextEditor_get_position_m4C5447E79B4B01068C4C6A47A757C4CF6483423F(__this, /*hidden argument*/NULL);
		V_7 = L_24;
		float L_25 = Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_7), /*hidden argument*/NULL);
		if ((!(((float)L_23) < ((float)L_25))))
		{
			goto IL_00d5;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_26 = __this->get_address_of_scrollOffset_7();
		L_26->set_x_0((0.0f));
		goto IL_017e;
	}

IL_00d5:
	{
		bool L_27 = __this->get_m_RevealCursor_12();
		if (!L_27)
		{
			goto IL_017e;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_28 = __this->get_address_of_graphicalCursorPos_13();
		float L_29 = L_28->get_x_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_30 = __this->get_address_of_scrollOffset_7();
		float L_31 = L_30->get_x_0();
		float L_32 = Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_3), /*hidden argument*/NULL);
		if ((!(((float)((float)il2cpp_codegen_add((float)L_29, (float)(1.0f)))) > ((float)((float)il2cpp_codegen_add((float)L_31, (float)L_32))))))
		{
			goto IL_0128;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_33 = __this->get_address_of_scrollOffset_7();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_34 = __this->get_address_of_graphicalCursorPos_13();
		float L_35 = L_34->get_x_0();
		float L_36 = Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_3), /*hidden argument*/NULL);
		L_33->set_x_0(((float)il2cpp_codegen_subtract((float)L_35, (float)L_36)));
	}

IL_0128:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_37 = __this->get_address_of_graphicalCursorPos_13();
		float L_38 = L_37->get_x_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_39 = __this->get_address_of_scrollOffset_7();
		float L_40 = L_39->get_x_0();
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_41 = __this->get_style_2();
		NullCheck(L_41);
		RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * L_42 = GUIStyle_get_padding_m5DF76AA03A313366D0DD8D577731DAC4FB83053A(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = RectOffset_get_left_mA86EC00866C1940134873E3A1565A1F700DE67AD(L_42, /*hidden argument*/NULL);
		if ((!(((float)L_38) < ((float)((float)il2cpp_codegen_add((float)L_40, (float)(((float)((float)L_43)))))))))
		{
			goto IL_017d;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_44 = __this->get_address_of_scrollOffset_7();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_45 = __this->get_address_of_graphicalCursorPos_13();
		float L_46 = L_45->get_x_0();
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_47 = __this->get_style_2();
		NullCheck(L_47);
		RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * L_48 = GUIStyle_get_padding_m5DF76AA03A313366D0DD8D577731DAC4FB83053A(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		int32_t L_49 = RectOffset_get_left_mA86EC00866C1940134873E3A1565A1F700DE67AD(L_48, /*hidden argument*/NULL);
		L_44->set_x_0(((float)il2cpp_codegen_subtract((float)L_46, (float)(((float)((float)L_49))))));
	}

IL_017d:
	{
	}

IL_017e:
	{
		float L_50 = (&V_4)->get_y_1();
		float L_51 = Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_3), /*hidden argument*/NULL);
		if ((!(((float)L_50) < ((float)L_51))))
		{
			goto IL_01a8;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_52 = __this->get_address_of_scrollOffset_7();
		L_52->set_y_1((0.0f));
		goto IL_0287;
	}

IL_01a8:
	{
		bool L_53 = __this->get_m_RevealCursor_12();
		if (!L_53)
		{
			goto IL_0287;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_54 = __this->get_address_of_graphicalCursorPos_13();
		float L_55 = L_54->get_y_1();
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_56 = __this->get_style_2();
		NullCheck(L_56);
		float L_57 = GUIStyle_get_lineHeight_mF1F02FEF06EAD632D35889F6396416EB50ADAB4C(L_56, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_58 = __this->get_address_of_scrollOffset_7();
		float L_59 = L_58->get_y_1();
		float L_60 = Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_3), /*hidden argument*/NULL);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_61 = __this->get_style_2();
		NullCheck(L_61);
		RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * L_62 = GUIStyle_get_padding_m5DF76AA03A313366D0DD8D577731DAC4FB83053A(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63 = RectOffset_get_top_mBA813D4147BFBC079933054018437F411B6B41E1(L_62, /*hidden argument*/NULL);
		if ((!(((float)((float)il2cpp_codegen_add((float)L_55, (float)L_57))) > ((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_59, (float)L_60)), (float)(((float)((float)L_63)))))))))
		{
			goto IL_0231;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_64 = __this->get_address_of_scrollOffset_7();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_65 = __this->get_address_of_graphicalCursorPos_13();
		float L_66 = L_65->get_y_1();
		float L_67 = Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_3), /*hidden argument*/NULL);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_68 = __this->get_style_2();
		NullCheck(L_68);
		RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * L_69 = GUIStyle_get_padding_m5DF76AA03A313366D0DD8D577731DAC4FB83053A(L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		int32_t L_70 = RectOffset_get_top_mBA813D4147BFBC079933054018437F411B6B41E1(L_69, /*hidden argument*/NULL);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_71 = __this->get_style_2();
		NullCheck(L_71);
		float L_72 = GUIStyle_get_lineHeight_mF1F02FEF06EAD632D35889F6396416EB50ADAB4C(L_71, /*hidden argument*/NULL);
		L_64->set_y_1(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)L_66, (float)L_67)), (float)(((float)((float)L_70))))), (float)L_72)));
	}

IL_0231:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_73 = __this->get_address_of_graphicalCursorPos_13();
		float L_74 = L_73->get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_75 = __this->get_address_of_scrollOffset_7();
		float L_76 = L_75->get_y_1();
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_77 = __this->get_style_2();
		NullCheck(L_77);
		RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * L_78 = GUIStyle_get_padding_m5DF76AA03A313366D0DD8D577731DAC4FB83053A(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		int32_t L_79 = RectOffset_get_top_mBA813D4147BFBC079933054018437F411B6B41E1(L_78, /*hidden argument*/NULL);
		if ((!(((float)L_74) < ((float)((float)il2cpp_codegen_add((float)L_76, (float)(((float)((float)L_79)))))))))
		{
			goto IL_0286;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_80 = __this->get_address_of_scrollOffset_7();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_81 = __this->get_address_of_graphicalCursorPos_13();
		float L_82 = L_81->get_y_1();
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_83 = __this->get_style_2();
		NullCheck(L_83);
		RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * L_84 = GUIStyle_get_padding_m5DF76AA03A313366D0DD8D577731DAC4FB83053A(L_83, /*hidden argument*/NULL);
		NullCheck(L_84);
		int32_t L_85 = RectOffset_get_top_mBA813D4147BFBC079933054018437F411B6B41E1(L_84, /*hidden argument*/NULL);
		L_80->set_y_1(((float)il2cpp_codegen_subtract((float)L_82, (float)(((float)((float)L_85))))));
	}

IL_0286:
	{
	}

IL_0287:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_86 = __this->get_address_of_scrollOffset_7();
		float L_87 = L_86->get_y_1();
		if ((!(((float)L_87) > ((float)(0.0f)))))
		{
			goto IL_031d;
		}
	}
	{
		float L_88 = (&V_4)->get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_89 = __this->get_address_of_scrollOffset_7();
		float L_90 = L_89->get_y_1();
		float L_91 = Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_3), /*hidden argument*/NULL);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_92 = __this->get_style_2();
		NullCheck(L_92);
		RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * L_93 = GUIStyle_get_padding_m5DF76AA03A313366D0DD8D577731DAC4FB83053A(L_92, /*hidden argument*/NULL);
		NullCheck(L_93);
		int32_t L_94 = RectOffset_get_top_mBA813D4147BFBC079933054018437F411B6B41E1(L_93, /*hidden argument*/NULL);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_95 = __this->get_style_2();
		NullCheck(L_95);
		RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * L_96 = GUIStyle_get_padding_m5DF76AA03A313366D0DD8D577731DAC4FB83053A(L_95, /*hidden argument*/NULL);
		NullCheck(L_96);
		int32_t L_97 = RectOffset_get_bottom_mE5162CADD266B59539E3EE1967EE9A74705E5632(L_96, /*hidden argument*/NULL);
		if ((!(((float)((float)il2cpp_codegen_subtract((float)L_88, (float)L_90))) < ((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_91, (float)(((float)((float)L_94))))), (float)(((float)((float)L_97)))))))))
		{
			goto IL_031d;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_98 = __this->get_address_of_scrollOffset_7();
		float L_99 = (&V_4)->get_y_1();
		float L_100 = Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_3), /*hidden argument*/NULL);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_101 = __this->get_style_2();
		NullCheck(L_101);
		RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * L_102 = GUIStyle_get_padding_m5DF76AA03A313366D0DD8D577731DAC4FB83053A(L_101, /*hidden argument*/NULL);
		NullCheck(L_102);
		int32_t L_103 = RectOffset_get_top_mBA813D4147BFBC079933054018437F411B6B41E1(L_102, /*hidden argument*/NULL);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_104 = __this->get_style_2();
		NullCheck(L_104);
		RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * L_105 = GUIStyle_get_padding_m5DF76AA03A313366D0DD8D577731DAC4FB83053A(L_104, /*hidden argument*/NULL);
		NullCheck(L_105);
		int32_t L_106 = RectOffset_get_bottom_mE5162CADD266B59539E3EE1967EE9A74705E5632(L_105, /*hidden argument*/NULL);
		L_98->set_y_1(((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)L_99, (float)L_100)), (float)(((float)((float)L_103))))), (float)(((float)((float)L_106))))));
	}

IL_031d:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_107 = __this->get_address_of_scrollOffset_7();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_108 = __this->get_address_of_scrollOffset_7();
		float L_109 = L_108->get_y_1();
		G_B20_0 = L_107;
		if ((!(((float)L_109) < ((float)(0.0f)))))
		{
			G_B21_0 = L_107;
			goto IL_0342;
		}
	}
	{
		G_B22_0 = (0.0f);
		G_B22_1 = G_B20_0;
		goto IL_034d;
	}

IL_0342:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_110 = __this->get_address_of_scrollOffset_7();
		float L_111 = L_110->get_y_1();
		G_B22_0 = L_111;
		G_B22_1 = G_B21_0;
	}

IL_034d:
	{
		G_B22_1->set_y_1(G_B22_0);
		__this->set_m_RevealCursor_12((bool)0);
		return;
	}
}
// System.Void UnityEngine.TextEditor::DrawCursor(System.String)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_DrawCursor_mF5281E018BE231150E23B748802E69A950ADAAF0 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, String_t* ___newText0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_DrawCursor_mF5281E018BE231150E23B748802E69A950ADAAF0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		String_t* L_0 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		String_t* L_2 = Input_get_compositionString_mAE7E520D248E55E8C99827380E1AB586C03C757E(/*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_4 = __this->get_m_Content_8();
		String_t* L_5 = ___newText0;
		int32_t L_6 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_7 = String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB(L_5, 0, L_6, /*hidden argument*/NULL);
		String_t* L_8 = Input_get_compositionString_mAE7E520D248E55E8C99827380E1AB586C03C757E(/*hidden argument*/NULL);
		String_t* L_9 = ___newText0;
		int32_t L_10 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m2C4AFF5E79DD8BADFD2DFBCF156BF728FBB8E1AE(L_9, L_10, /*hidden argument*/NULL);
		String_t* L_12 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(L_7, L_8, L_11, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIContent_set_text_m866E0C5690119816D87D83124C81BDC0A0ED4316(L_4, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		String_t* L_14 = Input_get_compositionString_mAE7E520D248E55E8C99827380E1AB586C03C757E(/*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_14, /*hidden argument*/NULL);
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)L_15));
		goto IL_006d;
	}

IL_0061:
	{
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_16 = __this->get_m_Content_8();
		String_t* L_17 = ___newText0;
		NullCheck(L_16);
		GUIContent_set_text_m866E0C5690119816D87D83124C81BDC0A0ED4316(L_16, L_17, /*hidden argument*/NULL);
	}

IL_006d:
	{
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_18 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_19 = TextEditor_get_position_m4C5447E79B4B01068C4C6A47A757C4CF6483423F(__this, /*hidden argument*/NULL);
		V_2 = L_19;
		float L_20 = Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_2), /*hidden argument*/NULL);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_21 = TextEditor_get_position_m4C5447E79B4B01068C4C6A47A757C4CF6483423F(__this, /*hidden argument*/NULL);
		V_3 = L_21;
		float L_22 = Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_3), /*hidden argument*/NULL);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280((&L_23), (0.0f), (0.0f), L_20, L_22, /*hidden argument*/NULL);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_24 = __this->get_m_Content_8();
		int32_t L_25 = V_1;
		NullCheck(L_18);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_26 = GUIStyle_GetCursorPixelPosition_m028A8E1540C0E9EB435A289146B090EAA96CF1F0(L_18, L_23, L_24, L_25, /*hidden argument*/NULL);
		__this->set_graphicalCursorPos_13(L_26);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_27 = __this->get_style_2();
		NullCheck(L_27);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_28 = GUIStyle_get_contentOffset_m9764FD32FFAD60090B0E0D6ABBE610A64AE8DAB2(L_27, /*hidden argument*/NULL);
		V_4 = L_28;
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_29 = __this->get_style_2();
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_30 = L_29;
		NullCheck(L_30);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_31 = GUIStyle_get_contentOffset_m9764FD32FFAD60090B0E0D6ABBE610A64AE8DAB2(L_30, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_32 = __this->get_scrollOffset_7();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_33 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		GUIStyle_set_contentOffset_mDE7B72F911E033B193DFBCD8A72902A9B8CAF0DA(L_30, L_33, /*hidden argument*/NULL);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_34 = __this->get_style_2();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_35 = __this->get_scrollOffset_7();
		NullCheck(L_34);
		GUIStyle_set_Internal_clipOffset_m2374646917D20BFA059BCFEF3E473C1AD95902B2(L_34, L_35, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_36 = __this->get_graphicalCursorPos_13();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_37 = TextEditor_get_position_m4C5447E79B4B01068C4C6A47A757C4CF6483423F(__this, /*hidden argument*/NULL);
		V_5 = L_37;
		float L_38 = Rect_get_x_mC51A461F546D14832EB96B11A7198DADDE2597B7((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_5), /*hidden argument*/NULL);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_39 = TextEditor_get_position_m4C5447E79B4B01068C4C6A47A757C4CF6483423F(__this, /*hidden argument*/NULL);
		V_6 = L_39;
		float L_40 = Rect_get_y_m53E3E4F62D9840FBEA751A66293038F1F5D1D45C((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_6), /*hidden argument*/NULL);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_41 = __this->get_style_2();
		NullCheck(L_41);
		float L_42 = GUIStyle_get_lineHeight_mF1F02FEF06EAD632D35889F6396416EB50ADAB4C(L_41, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_43), L_38, ((float)il2cpp_codegen_add((float)L_40, (float)L_42)), /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_44 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_36, L_43, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_45 = __this->get_scrollOffset_7();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_46 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_44, L_45, /*hidden argument*/NULL);
		Input_set_compositionCursorPos_m2FBB434ABF1961EE12CD4CE79CF0C4C6B477BF66(L_46, /*hidden argument*/NULL);
		String_t* L_47 = Input_get_compositionString_mAE7E520D248E55E8C99827380E1AB586C03C757E(/*hidden argument*/NULL);
		NullCheck(L_47);
		int32_t L_48 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_47, /*hidden argument*/NULL);
		if ((((int32_t)L_48) <= ((int32_t)0)))
		{
			goto IL_017e;
		}
	}
	{
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_49 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_50 = TextEditor_get_position_m4C5447E79B4B01068C4C6A47A757C4CF6483423F(__this, /*hidden argument*/NULL);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_51 = __this->get_m_Content_8();
		int32_t L_52 = __this->get_controlID_1();
		int32_t L_53 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_54 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		String_t* L_55 = Input_get_compositionString_mAE7E520D248E55E8C99827380E1AB586C03C757E(/*hidden argument*/NULL);
		NullCheck(L_55);
		int32_t L_56 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_55, /*hidden argument*/NULL);
		NullCheck(L_49);
		GUIStyle_DrawWithTextSelection_m60783EB261EBD814D7FFC6131A87E675290BAEFC(L_49, L_50, L_51, L_52, L_53, ((int32_t)il2cpp_codegen_add((int32_t)L_54, (int32_t)L_56)), (bool)1, /*hidden argument*/NULL);
		goto IL_01a7;
	}

IL_017e:
	{
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_57 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_58 = TextEditor_get_position_m4C5447E79B4B01068C4C6A47A757C4CF6483423F(__this, /*hidden argument*/NULL);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_59 = __this->get_m_Content_8();
		int32_t L_60 = __this->get_controlID_1();
		int32_t L_61 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_62 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		NullCheck(L_57);
		GUIStyle_DrawWithTextSelection_mF72D3C928F0A7DC73F11121858D040972B1A9807(L_57, L_58, L_59, L_60, L_61, L_62, /*hidden argument*/NULL);
	}

IL_01a7:
	{
		int32_t L_63 = __this->get_m_iAltCursorPos_19();
		if ((((int32_t)L_63) == ((int32_t)(-1))))
		{
			goto IL_01d6;
		}
	}
	{
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_64 = __this->get_style_2();
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_65 = TextEditor_get_position_m4C5447E79B4B01068C4C6A47A757C4CF6483423F(__this, /*hidden argument*/NULL);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_66 = __this->get_m_Content_8();
		int32_t L_67 = __this->get_controlID_1();
		int32_t L_68 = __this->get_m_iAltCursorPos_19();
		NullCheck(L_64);
		GUIStyle_DrawCursor_m2CE940C9FF3E2691960F1DE97D39CB1B3E0D2EE4(L_64, L_65, L_66, L_67, L_68, /*hidden argument*/NULL);
	}

IL_01d6:
	{
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_69 = __this->get_style_2();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_70 = V_4;
		NullCheck(L_69);
		GUIStyle_set_contentOffset_mDE7B72F911E033B193DFBCD8A72902A9B8CAF0DA(L_69, L_70, /*hidden argument*/NULL);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_71 = __this->get_style_2();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_72 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		NullCheck(L_71);
		GUIStyle_set_Internal_clipOffset_m2374646917D20BFA059BCFEF3E473C1AD95902B2(L_71, L_72, /*hidden argument*/NULL);
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_73 = __this->get_m_Content_8();
		String_t* L_74 = V_0;
		NullCheck(L_73);
		GUIContent_set_text_m866E0C5690119816D87D83124C81BDC0A0ED4316(L_73, L_74, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::PerformOperation(UnityEngine.TextEditor/TextEditOp)
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_PerformOperation_m6A900A3FA32AC396A0674649EC52C6456476EA89 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___operation0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_PerformOperation_m6A900A3FA32AC396A0674649EC52C6456476EA89_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		__this->set_m_RevealCursor_12((bool)1);
		int32_t L_0 = ___operation0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_00cb;
			}
			case 1:
			{
				goto IL_00d6;
			}
			case 2:
			{
				goto IL_00e1;
			}
			case 3:
			{
				goto IL_00ec;
			}
			case 4:
			{
				goto IL_00f7;
			}
			case 5:
			{
				goto IL_0102;
			}
			case 6:
			{
				goto IL_0139;
			}
			case 7:
			{
				goto IL_0144;
			}
			case 8:
			{
				goto IL_02a0;
			}
			case 9:
			{
				goto IL_02a0;
			}
			case 10:
			{
				goto IL_0165;
			}
			case 11:
			{
				goto IL_0170;
			}
			case 12:
			{
				goto IL_012e;
			}
			case 13:
			{
				goto IL_010d;
			}
			case 14:
			{
				goto IL_014f;
			}
			case 15:
			{
				goto IL_015a;
			}
			case 16:
			{
				goto IL_0118;
			}
			case 17:
			{
				goto IL_0123;
			}
			case 18:
			{
				goto IL_017b;
			}
			case 19:
			{
				goto IL_0186;
			}
			case 20:
			{
				goto IL_0191;
			}
			case 21:
			{
				goto IL_019c;
			}
			case 22:
			{
				goto IL_01d3;
			}
			case 23:
			{
				goto IL_01de;
			}
			case 24:
			{
				goto IL_02a0;
			}
			case 25:
			{
				goto IL_02a0;
			}
			case 26:
			{
				goto IL_01e9;
			}
			case 27:
			{
				goto IL_01f4;
			}
			case 28:
			{
				goto IL_0215;
			}
			case 29:
			{
				goto IL_0220;
			}
			case 30:
			{
				goto IL_01b2;
			}
			case 31:
			{
				goto IL_01a7;
			}
			case 32:
			{
				goto IL_01bd;
			}
			case 33:
			{
				goto IL_01c8;
			}
			case 34:
			{
				goto IL_020a;
			}
			case 35:
			{
				goto IL_01ff;
			}
			case 36:
			{
				goto IL_022b;
			}
			case 37:
			{
				goto IL_0237;
			}
			case 38:
			{
				goto IL_027c;
			}
			case 39:
			{
				goto IL_0294;
			}
			case 40:
			{
				goto IL_0288;
			}
			case 41:
			{
				goto IL_0243;
			}
			case 42:
			{
				goto IL_024f;
			}
			case 43:
			{
				goto IL_025a;
			}
			case 44:
			{
				goto IL_0266;
			}
			case 45:
			{
				goto IL_0271;
			}
		}
	}
	{
		goto IL_02a0;
	}

IL_00cb:
	{
		TextEditor_MoveLeft_mB89B657A867A5BDDD5C7C29746883BE2654A2C65(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_00d6:
	{
		TextEditor_MoveRight_m59C55388AA97B5BBF3BD7EA1242A414D699200B7(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_00e1:
	{
		TextEditor_MoveUp_m33A336334EE094B374C79D384742C10EE3F87CE9(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_00ec:
	{
		TextEditor_MoveDown_mF8326F5B90AD880828693C184F97D02D1EC15FEE(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_00f7:
	{
		TextEditor_MoveLineStart_m01C387F3CAD4B192C343E1FBD81D3D8C1A26BFE1(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_0102:
	{
		TextEditor_MoveLineEnd_m6CD2F02FF6859D1465DB9FB6E805DBBB5EFE1485(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_010d:
	{
		TextEditor_MoveWordRight_m682304844C44DB8749BF5FCA4DB321682D1CEFDB(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_0118:
	{
		TextEditor_MoveToStartOfNextWord_mF8E0AF826783F4C751E47EB39558892E0D280911(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_0123:
	{
		TextEditor_MoveToEndOfPreviousWord_mA1D6F6C37728A27568A2308F6033544BE41CC053(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_012e:
	{
		TextEditor_MoveWordLeft_m615D5BF427B7E37C700C434B8630F6F9BCDCAA1C(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_0139:
	{
		TextEditor_MoveTextStart_m154B56BBD4408FFA335AF28B8AC712D0D64ACEAD(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_0144:
	{
		TextEditor_MoveTextEnd_mA4E9806443211054DBAD3A8809FF54E78BD14F16(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_014f:
	{
		TextEditor_MoveParagraphForward_mDCDEBAB394A840A65A97C676BD3A5FEFF5D6D323(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_015a:
	{
		TextEditor_MoveParagraphBackward_m0E0D4995F67E1A93171351AC59D0C9D0F12E2ED1(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_0165:
	{
		TextEditor_MoveGraphicalLineStart_m17B1028964B11E29FC4F400FD7737A7490989154(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_0170:
	{
		TextEditor_MoveGraphicalLineEnd_m37A229D49BD65C1C6EBF036769E20332F65B9C12(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_017b:
	{
		TextEditor_SelectLeft_m7B2639E4B74273199C4DD7B19D22A1469F92067F(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_0186:
	{
		TextEditor_SelectRight_m269F9770E5FD0525B55776C07E9971D6C66A78DE(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_0191:
	{
		TextEditor_SelectUp_mB4ED2C2AF3A2ACCDF5B3526AD833A579918F57AC(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_019c:
	{
		TextEditor_SelectDown_mDB4866E1E2147CF2E3A8A112089DBF39DC51E0EC(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_01a7:
	{
		TextEditor_SelectWordRight_mC181E8E66228F232CF3B5F9799AC15DC8C44C585(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_01b2:
	{
		TextEditor_SelectWordLeft_m1094CAC46C35D29D80B3A086B4D5E874BE37C7D3(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_01bd:
	{
		TextEditor_SelectToEndOfPreviousWord_mDBD58F16716BF35033E3E942889A17BB8A7AEB41(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_01c8:
	{
		TextEditor_SelectToStartOfNextWord_m16F1549DC820938AB680CBC76D3B3B816BEC0037(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_01d3:
	{
		TextEditor_SelectTextStart_m8562BCC674CDA850E3DBE8455E92B3CBB590436D(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_01de:
	{
		TextEditor_SelectTextEnd_m18F2785E8502B02F3ED4B0C0E1C68CCD9B8408FD(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_01e9:
	{
		TextEditor_ExpandSelectGraphicalLineStart_mF13A47F89B658B025FBEAB8CDA30B121E4BAF474(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_01f4:
	{
		TextEditor_ExpandSelectGraphicalLineEnd_m948098D63AB53DAA3AED6F864A00559B325E8D2C(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_01ff:
	{
		TextEditor_SelectParagraphForward_m38A2B0EEF2DFB8B81A681EEC34E704FF90AB80E1(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_020a:
	{
		TextEditor_SelectParagraphBackward_m72FEDA47BEDFBC046ED21F80768FD422FA63766E(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_0215:
	{
		TextEditor_SelectGraphicalLineStart_m1C6C0A1E5D2E196FBD005D5815CF8CD4E923C8C0(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_0220:
	{
		TextEditor_SelectGraphicalLineEnd_mEE048AE3578021287AF9017356913423E4D2CA0F(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_022b:
	{
		bool L_1 = TextEditor_Delete_m4B7A14DD74D23305D71ECF58B078EB1C99864FDC(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_02c1;
	}

IL_0237:
	{
		bool L_2 = TextEditor_Backspace_m256A202133B82F0A43563BEEAC463CF808229CC7(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_02c1;
	}

IL_0243:
	{
		bool L_3 = TextEditor_Cut_mE1FCFFF9B77690296C732A0FE7183A69249DB819(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_02c1;
	}

IL_024f:
	{
		TextEditor_Copy_mF21F9BE54FD96EEC2CAA33EB3595ACD3894BA4E2(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_025a:
	{
		bool L_4 = TextEditor_Paste_m551DF86401B71947857EC27860AE3470C33B860B(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_02c1;
	}

IL_0266:
	{
		TextEditor_SelectAll_m33196916F8C32522554E30567291CF967CFD6EF9(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_0271:
	{
		TextEditor_SelectNone_m4CDD0831BB48D920C158779FDF404EDABD56763D(__this, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_027c:
	{
		bool L_5 = TextEditor_DeleteWordBack_mB48FCF1FBFABBC70C1EB7EA184B387BE12F16B50(__this, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_02c1;
	}

IL_0288:
	{
		bool L_6 = TextEditor_DeleteLineBack_mDFF1F1E2ABA2EBC1ECF020D63784467CD392F9A2(__this, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_02c1;
	}

IL_0294:
	{
		bool L_7 = TextEditor_DeleteWordForward_m94D25A0E9E8746E8B419E5F2D041305744734FCF(__this, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_02c1;
	}

IL_02a0:
	{
		int32_t L_8 = ___operation0;
		int32_t L_9 = L_8;
		RuntimeObject * L_10 = Box(TextEditOp_t197E102C6DE6BA0F6A2871DB17FA260B6E01E151_il2cpp_TypeInfo_var, &L_9);
		String_t* L_11 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral968767E065572AA7E887F179AEE12CA552A5AC0C, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_11, /*hidden argument*/NULL);
		goto IL_02ba;
	}

IL_02ba:
	{
		V_0 = (bool)0;
		goto IL_02c1;
	}

IL_02c1:
	{
		bool L_12 = V_0;
		return L_12;
	}
}
// System.Void UnityEngine.TextEditor::SaveBackup()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_SaveBackup_m7339CCE5F14572F00CCE1222B6B674E6A0394CA9 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		__this->set_oldText_20(L_0);
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		__this->set_oldPos_21(L_1);
		int32_t L_2 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		__this->set_oldSelectPos_22(L_2);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::Cut()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_Cut_mE1FCFFF9B77690296C732A0FE7183A69249DB819 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_isPasswordField_5();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0025;
	}

IL_0013:
	{
		TextEditor_Copy_mF21F9BE54FD96EEC2CAA33EB3595ACD3894BA4E2(__this, /*hidden argument*/NULL);
		bool L_1 = TextEditor_DeleteSelection_m74DE4B4D997A9F88D31A24E8B97484FE9FA77D0D(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0025;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.TextEditor::Copy()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_Copy_mF21F9BE54FD96EEC2CAA33EB3595ACD3894BA4E2 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0081;
	}

IL_0017:
	{
		bool L_2 = __this->get_isPasswordField_5();
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		goto IL_0081;
	}

IL_0027:
	{
		int32_t L_3 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_005c;
		}
	}
	{
		String_t* L_5 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_7 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_9 = String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB(L_5, L_6, ((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_007b;
	}

IL_005c:
	{
		String_t* L_10 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_11 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		int32_t L_12 = TextEditor_get_cursorIndex_mE4D6D2380232B9BAC44FB7151AD5D03AF9CE973C(__this, /*hidden argument*/NULL);
		int32_t L_13 = TextEditor_get_selectIndex_m20CF98C3C600C974EC6BF59B541182AE8CB95123(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_14 = String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB(L_10, L_11, ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)L_13)), /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_007b:
	{
		String_t* L_15 = V_0;
		GUIUtility_set_systemCopyBuffer_m8C87AFD05D32AB0C30A2203005A64A86DFE18BE6(L_15, /*hidden argument*/NULL);
	}

IL_0081:
	{
		return;
	}
}
// System.String UnityEngine.TextEditor::ReplaceNewlinesWithSpaces(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* TextEditor_ReplaceNewlinesWithSpaces_mDEEE760A469D8700A24D803C0F184BFC8EDC4B1D (String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_ReplaceNewlinesWithSpaces_mDEEE760A469D8700A24D803C0F184BFC8EDC4B1D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___value0;
		NullCheck(L_0);
		String_t* L_1 = String_Replace_m970DFB0A280952FA7D3BA20AB7A8FB9F80CF6470(L_0, _stringLiteralBA8AB5A0280B953AA97435FF8946CBCBB2755A27, _stringLiteralB858CB282617FB0956D960215C8E84D1CCF909C6, /*hidden argument*/NULL);
		___value0 = L_1;
		String_t* L_2 = ___value0;
		NullCheck(L_2);
		String_t* L_3 = String_Replace_m276641366A463205C185A9B3DC0E24ECB95122C9(L_2, ((int32_t)10), ((int32_t)32), /*hidden argument*/NULL);
		___value0 = L_3;
		String_t* L_4 = ___value0;
		NullCheck(L_4);
		String_t* L_5 = String_Replace_m276641366A463205C185A9B3DC0E24ECB95122C9(L_4, ((int32_t)13), ((int32_t)32), /*hidden argument*/NULL);
		___value0 = L_5;
		String_t* L_6 = ___value0;
		V_0 = L_6;
		goto IL_0032;
	}

IL_0032:
	{
		String_t* L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.TextEditor::Paste()
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_Paste_m551DF86401B71947857EC27860AE3470C33B860B (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_Paste_m551DF86401B71947857EC27860AE3470C33B860B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	{
		String_t* L_0 = GUIUtility_get_systemCopyBuffer_m5C4EE0A0FDE696D4A1337480B20AF300E6A5624E(/*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		bool L_2 = String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E(L_1, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0038;
		}
	}
	{
		bool L_3 = __this->get_multiline_3();
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_4 = V_0;
		String_t* L_5 = TextEditor_ReplaceNewlinesWithSpaces_mDEEE760A469D8700A24D803C0F184BFC8EDC4B1D(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_002a:
	{
		String_t* L_6 = V_0;
		TextEditor_ReplaceSelection_m9534876530D3A70E2358F347A8F10096343BD456(__this, L_6, /*hidden argument*/NULL);
		V_1 = (bool)1;
		goto IL_003f;
	}

IL_0038:
	{
		V_1 = (bool)0;
		goto IL_003f;
	}

IL_003f:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.TextEditor::MapKey(System.String,UnityEngine.TextEditor/TextEditOp)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305 (String_t* ___key0, int32_t ___action1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 * L_0 = ((TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440_StaticFields*)il2cpp_codegen_static_fields_for(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440_il2cpp_TypeInfo_var))->get_s_Keyactions_23();
		String_t* L_1 = ___key0;
		Event_t187FF6A6B357447B83EC2064823EE0AEC5263210 * L_2 = Event_KeyboardEvent_m42755FC3AFA26F90EBE7A428F7845C42BBF1A97D(L_1, /*hidden argument*/NULL);
		int32_t L_3 = ___action1;
		NullCheck(L_0);
		Dictionary_2_set_Item_m461003B498B59A8F07384C0C472BC6AB93545D2C(L_0, L_2, L_3, /*hidden argument*/Dictionary_2_set_Item_m461003B498B59A8F07384C0C472BC6AB93545D2C_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.TextEditor::InitKeyActions()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_InitKeyActions_m724C451AA81FE63C7DC95DECD4AC681080AFCC11 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_InitKeyActions_m724C451AA81FE63C7DC95DECD4AC681080AFCC11_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 * L_0 = ((TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440_StaticFields*)il2cpp_codegen_static_fields_for(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440_il2cpp_TypeInfo_var))->get_s_Keyactions_23();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		goto IL_039c;
	}

IL_0010:
	{
		Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 * L_1 = (Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319 *)il2cpp_codegen_object_new(Dictionary_2_t146200EEAB2E66010EC2A0DA98A2E0738F4C8319_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mD6F5EEE24289730ADCFD7DE5DC90DC2E6B0BFF2F(L_1, /*hidden argument*/Dictionary_2__ctor_mD6F5EEE24289730ADCFD7DE5DC90DC2E6B0BFF2F_RuntimeMethod_var);
		((TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440_StaticFields*)il2cpp_codegen_static_fields_for(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440_il2cpp_TypeInfo_var))->set_s_Keyactions_23(L_1);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral12C0F1FBADC4046B5F2BB9E063B227EF8750D9D6, 0, /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralD27A1F11771200949714B1AF99F048A416F5D6F4, 1, /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral7C0A25C06EA30BAE50E39A37A5997E31A1A96E20, 2, /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral77346D0447DAFF959358A0ECBEEC83BFD9EC86BB, 3, /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral6102DC14FA0F0C73D3C99FB1892929691937BD25, ((int32_t)18), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralAD56C2BC3EC9BD41B3074B5A8914F35632534E49, ((int32_t)19), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral131D06FD63FDF2386A3D1762E3405DFF94BE55C6, ((int32_t)20), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral740377EEA64FD0E2703021FAD7C8612402ADFF92, ((int32_t)21), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral9485989FF514B5106B7738850FD73C23E8C1E3F7, ((int32_t)36), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral4930EB3F60DF3A6A61E8D44178FEB2B8F820A153, ((int32_t)37), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral6FFAD0F1514E6E22A0D256A7C7B31FAABD73ED63, ((int32_t)37), /*hidden argument*/NULL);
		int32_t L_2 = SystemInfo_get_operatingSystemFamily_mA35FE1FF2DD6240B2880DC5F642D4A0CC2B58D8D(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0262;
		}
	}
	{
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral59F9DAB91E232359595FB7E7B346D4C636DD29E4, ((int32_t)10), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralD12BE11B6C6387EF9D43CD28DBA22C79D86FDFFB, ((int32_t)11), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral5B2662EFB47DC70EB0DD0468A0429C080DE0EA3C, ((int32_t)12), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral286BC08C2F4EED0826A70058A345427BAD61B929, ((int32_t)13), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral0E33543BDBE560B7A172E26B076C221830E48A23, ((int32_t)15), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral24078727E760F609BA9D460B38695EB78018866F, ((int32_t)14), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral9A5F708763FE0E3D92F572DBE11DCFB8F2F824EA, ((int32_t)10), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral7CE12B4417E103A0BD80EB14458AE1A104BF6D6B, ((int32_t)11), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral37E2609EE8EAC3E9833495CCBF1141F79B24270C, 6, /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralBDE895DBC95DC311972A9ADA8028849FED24437B, 7, /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral763389B097CFDA9576DBD4046F2ECC69E0D29B85, ((int32_t)22), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral58876305D454A861801BDE1F2125E1368C231E4A, ((int32_t)23), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralBEED3BF9F8706DB26270FA0878A81526874ECB0C, ((int32_t)26), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral3D68429D3570BB774AB8CA78687D0FC0F48FAC04, ((int32_t)27), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral62847E519D4ADA28164250829049B9A919B2AE01, ((int32_t)34), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral9F5BE511E1E437CA95D5B55F8F8A88906FC1AEE6, ((int32_t)35), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralB83393C5BFF0AEB7C3364909B97AC7186EC0331F, ((int32_t)30), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralDC29F2233DCB721EDC0D3E34D64543CCF3403270, ((int32_t)31), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral9E31EA478E6422888A2A833CD072E72DC0543E06, ((int32_t)34), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral901C620F7ECC0984C3DD8E8735EFBA866392BA6F, ((int32_t)35), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralBAD94ADF967C08A44711C5A7B859873FB1B0F065, ((int32_t)26), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral4756CFCA8EEFEAD20B5992EAC6EF5748E1775527, ((int32_t)27), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral67F7C0B892F47A0F86D1D66FE6BD734EE4F7C1A6, ((int32_t)22), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralDF7A8F3088FC2D7221820E99BAF39A1BCB2DD203, ((int32_t)23), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral7FA2D4DD6FB5DD5CDF7FFCB9E09C0871C2C63452, ((int32_t)44), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralF7D71AD6A293C739FDB380F7E3761BF9CCF1933B, ((int32_t)41), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral38132A51EE9A97A4511377264A0779C0861CE815, ((int32_t)42), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral2D3E82BE1E73866269EA97C6D02040FE73E7AB54, ((int32_t)43), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral423D5CEC9E85A686DEF7682D66F54C301C77C171, ((int32_t)36), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralF122C7E167718794E9F9BC54786C86A7D99579E6, ((int32_t)37), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral32588D5CD9AD74EC3ECEDFA1041A987A5D99069C, 0, /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralBA841F6B049E05B23489E3235B0729E4E5D5C627, 1, /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralF87FB192D2661A8C26E07D692244897AB190B2FC, 4, /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral982F849E4755453B885083FA0F7C4B4462B2E6B3, 5, /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralC9EBD785A19C1BFEFD62B3B2A4FC1A6D6963AEE1, ((int32_t)39), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralFD41A55936F9E25124F5243F3AC6F8312A602912, ((int32_t)38), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralCBECD5D022A1A5C503555604D30C3EAF1C10299E, ((int32_t)40), /*hidden argument*/NULL);
		goto IL_039c;
	}

IL_0262:
	{
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralE83249BD3BA79932E16FB1FB5100DAFADE9954C2, ((int32_t)10), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral7A92F3D26362D6557D5701DE77A63A01DF61E57F, ((int32_t)11), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral9A5F708763FE0E3D92F572DBE11DCFB8F2F824EA, ((int32_t)12), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral7CE12B4417E103A0BD80EB14458AE1A104BF6D6B, ((int32_t)13), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral37E2609EE8EAC3E9833495CCBF1141F79B24270C, ((int32_t)15), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralBDE895DBC95DC311972A9ADA8028849FED24437B, ((int32_t)14), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral59F9DAB91E232359595FB7E7B346D4C636DD29E4, ((int32_t)17), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralD12BE11B6C6387EF9D43CD28DBA22C79D86FDFFB, ((int32_t)16), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral15E6F46C6ABAA57A101887F62558D6558BDAF0AC, ((int32_t)15), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralEB36FC6F33913BF1B2F1305630A3023799A75AA7, ((int32_t)14), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralBEED3BF9F8706DB26270FA0878A81526874ECB0C, ((int32_t)32), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral3D68429D3570BB774AB8CA78687D0FC0F48FAC04, ((int32_t)33), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral62847E519D4ADA28164250829049B9A919B2AE01, ((int32_t)34), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral9F5BE511E1E437CA95D5B55F8F8A88906FC1AEE6, ((int32_t)35), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral763389B097CFDA9576DBD4046F2ECC69E0D29B85, ((int32_t)28), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral58876305D454A861801BDE1F2125E1368C231E4A, ((int32_t)29), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral56B2C39D4D5C71E523F34B8C45A6D4DCBE7EF18C, ((int32_t)39), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral19E7EC0CA525399DB1C8D777D61D6FF53DCC5C0C, ((int32_t)38), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralCBECD5D022A1A5C503555604D30C3EAF1C10299E, ((int32_t)40), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralF87FB192D2661A8C26E07D692244897AB190B2FC, ((int32_t)44), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralC470A513564DCA6343DE982775D5451483E70B53, ((int32_t)41), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralDF4EC500FDA53C7919CEB5C27504F045909CEB4A, ((int32_t)42), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteralADDA35B979E6F63F030A7F51D1FC60F0785079DF, ((int32_t)43), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral03C170628B9070A1EE6D3EF4C543678D9EE67097, ((int32_t)41), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral03BB5E3AD422D654676A16373CB6767BDD1EC096, ((int32_t)42), /*hidden argument*/NULL);
		TextEditor_MapKey_m51A0F7783D2A7BDDF52E3A76B851ECD3D1E1C305(_stringLiteral77DB7EB9136D91C5901F452F84527E54526FAC83, ((int32_t)43), /*hidden argument*/NULL);
	}

IL_039c:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::DetectFocusChange()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_DetectFocusChange_mE73EABD5C139A2851856E4E039842F6AA66D0824 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.TextEditor::OnDetectFocusChange() */, __this);
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnDetectFocusChange()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_OnDetectFocusChange_m8FA664ED49A3104688B99010987A67CD8C8ABE91 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_HasFocus_6();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_controlID_1();
		int32_t L_2 = GUIUtility_get_keyboardControl_mB580042A41CB6E9D3AB2E2CF0CEC00F64B2FC55F(/*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}
	{
		TextEditor_OnLostFocus_m792C976FAC66B6C5576E49406A2963F1D19DD507(__this, /*hidden argument*/NULL);
	}

IL_0022:
	{
		bool L_3 = __this->get_m_HasFocus_6();
		if (L_3)
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = __this->get_controlID_1();
		int32_t L_5 = GUIUtility_get_keyboardControl_mB580042A41CB6E9D3AB2E2CF0CEC00F64B2FC55F(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0043;
		}
	}
	{
		TextEditor_OnFocus_mE285A1E80DF51FED810C9438DF15835BD1120A84(__this, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnCursorIndexChange()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_OnCursorIndexChange_m77C1647414D629E6E72DCE4D556DBA95A27B59D2 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnSelectIndexChange()
extern "C" IL2CPP_METHOD_ATTR void TextEditor_OnSelectIndexChange_mA096B8B572516C59578A402F38BDF2CB4BE4DAF1 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::ClampTextIndex(System.Int32&)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_ClampTextIndex_m3EC3F291912021B79AF56CDD804B8795191A629B (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t* ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_ClampTextIndex_m3EC3F291912021B79AF56CDD804B8795191A629B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t* L_0 = ___index0;
		int32_t* L_1 = ___index0;
		int32_t L_2 = *((int32_t*)L_1);
		String_t* L_3 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Clamp_mE1EA15D719BF2F632741D42DF96F0BC797A20389(L_2, 0, L_4, /*hidden argument*/NULL);
		*((int32_t*)L_0) = (int32_t)L_5;
		return;
	}
}
// System.Void UnityEngine.TextEditor::EnsureValidCodePointIndex(System.Int32&)
extern "C" IL2CPP_METHOD_ATTR void TextEditor_EnsureValidCodePointIndex_m5354F0315238BEAC78516935288D8BA67AE15C60 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t* ___index0, const RuntimeMethod* method)
{
	{
		int32_t* L_0 = ___index0;
		TextEditor_ClampTextIndex_m3EC3F291912021B79AF56CDD804B8795191A629B(__this, (int32_t*)L_0, /*hidden argument*/NULL);
		int32_t* L_1 = ___index0;
		int32_t L_2 = *((int32_t*)L_1);
		bool L_3 = TextEditor_IsValidCodePointIndex_m0366054C7FF170C5FBE08469412517D10199AF47(__this, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_001f;
		}
	}
	{
		int32_t* L_4 = ___index0;
		int32_t* L_5 = ___index0;
		int32_t L_6 = *((int32_t*)L_5);
		int32_t L_7 = TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40(__this, L_6, /*hidden argument*/NULL);
		*((int32_t*)L_4) = (int32_t)L_7;
	}

IL_001f:
	{
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::IsValidCodePointIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool TextEditor_IsValidCodePointIndex_m0366054C7FF170C5FBE08469412517D10199AF47 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_IsValidCodePointIndex_m0366054C7FF170C5FBE08469412517D10199AF47_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_1 = ___index0;
		String_t* L_2 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_3)))
		{
			goto IL_0020;
		}
	}

IL_0019:
	{
		V_0 = (bool)0;
		goto IL_0058;
	}

IL_0020:
	{
		int32_t L_4 = ___index0;
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_5 = ___index0;
		String_t* L_6 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_6, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)L_7))))
		{
			goto IL_003e;
		}
	}

IL_0037:
	{
		V_0 = (bool)1;
		goto IL_0058;
	}

IL_003e:
	{
		String_t* L_8 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_9 = ___index0;
		NullCheck(L_8);
		Il2CppChar L_10 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_8, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_il2cpp_TypeInfo_var);
		bool L_11 = Char_IsLowSurrogate_m11EF790BE9683BDF04022FD055104AE7A22A6A9C(L_10, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_11) == ((int32_t)0))? 1 : 0);
		goto IL_0058;
	}

IL_0058:
	{
		bool L_12 = V_0;
		return L_12;
	}
}
// System.Int32 UnityEngine.TextEditor::PreviousCodePointIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_PreviousCodePointIndex_mC4DF9BAE596823A08B99AB8C79C1FA78145B47E9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___index0;
		___index0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1));
	}

IL_000d:
	{
		goto IL_0017;
	}

IL_0012:
	{
		int32_t L_2 = ___index0;
		___index0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1));
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_4 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_5 = ___index0;
		NullCheck(L_4);
		Il2CppChar L_6 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_il2cpp_TypeInfo_var);
		bool L_7 = Char_IsLowSurrogate_m11EF790BE9683BDF04022FD055104AE7A22A6A9C(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0012;
		}
	}

IL_0034:
	{
		int32_t L_8 = ___index0;
		V_0 = L_8;
		goto IL_003b;
	}

IL_003b:
	{
		int32_t L_9 = V_0;
		return L_9;
	}
}
// System.Int32 UnityEngine.TextEditor::NextCodePointIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40 (TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_NextCodePointIndex_mB145570559432C3C7E481DC6A38DDC34223F9C40_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		String_t* L_1 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_2)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = ___index0;
		___index0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
	}

IL_0017:
	{
		goto IL_0021;
	}

IL_001c:
	{
		int32_t L_4 = ___index0;
		___index0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0021:
	{
		int32_t L_5 = ___index0;
		String_t* L_6 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) >= ((int32_t)L_7)))
		{
			goto IL_0048;
		}
	}
	{
		String_t* L_8 = TextEditor_get_text_m1D8D3C908E35F9373070528D3183586E91D0129D(__this, /*hidden argument*/NULL);
		int32_t L_9 = ___index0;
		NullCheck(L_8);
		Il2CppChar L_10 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_8, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_il2cpp_TypeInfo_var);
		bool L_11 = Char_IsLowSurrogate_m11EF790BE9683BDF04022FD055104AE7A22A6A9C(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_001c;
		}
	}

IL_0048:
	{
		int32_t L_12 = ___index0;
		V_0 = L_12;
		goto IL_004f;
	}

IL_004f:
	{
		int32_t L_13 = V_0;
		return L_13;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
