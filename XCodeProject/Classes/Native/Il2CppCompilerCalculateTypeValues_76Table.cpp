﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Core.Camera.CameraRig
struct CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4;
// Core.Health.IAlignmentProvider
struct IAlignmentProvider_t2D743EB90EDB9AA7B214CF0DADA5767CC0022D5E;
// Core.Health.SerializableIAlignmentProvider
struct SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61;
// Core.Input.PointerInfo
struct PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB;
// Core.Utilities.Pool`1<Core.Utilities.Poolable>
struct Pool_1_tA688FE7A8096EF8D7EADA8644CA3B87A9356A2D7;
// Core.Utilities.RepeatingTimer
struct RepeatingTimer_tFEABA0EAF35D64F27E55652FD50AF98799EDC468;
// Core.Utilities.Timer
struct Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B;
// Economy.Currency
struct Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05;
// Economy.CurrencyManager
struct CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497;
// Health
struct Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D;
// Pathfinding.Agents.Agent
struct Agent_t0C7954C99BD6D63C1824532878279573DE904927;
// Pathfinding.MeshCreator.AreaMeshCreator
struct AreaMeshCreator_tE22D06EA3837CB117F16849084D4074A0A6F01D7;
// Pathfinding.MeshCreator.MeshObject
struct MeshObject_t3BA586F00E133946864FFCB15DE54A59D4E5E2B2;
// Pathfinding.Nodes.Node
struct Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7;
// Placement.IPlacementArea
struct IPlacementArea_t9FCC029582EBBBD107146874926E17F29D7D765F;
// Placement.PlacementTile
struct PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107;
// Placement.PlacementTile[0...,0...]
struct PlacementTileU5BU2CU5D_t61562B1B8B671E98D123F8F58F5D2AFB9EBB752E;
// SimpleHealthBar
struct SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5;
// SimpleHealthBar_SpaceshipExample.AsteroidController
struct AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB;
// SimpleHealthBar_SpaceshipExample.GameManager
struct GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC;
// SimpleHealthBar_SpaceshipExample.HealthPickupController
struct HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A;
// SimpleHealthBar_SpaceshipExample.PlayerController
struct PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2;
// SimpleHealthBar_SpaceshipExample.PlayerHealth
struct PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Economy.CurrencyChangeInfo>
struct Action_1_t1C2B006BB1030E36B3A42A958F7832065CAC6582;
// System.Action`1<Health>
struct Action_1_t001094C4FAC15A904C20F73F81BD57C81EBC71C4;
// System.Action`1<Pathfinding.Nodes.Node>
struct Action_1_t80C0367B25255284C38F37E49F3BA4F972110C44;
// System.Action`1<Towers.Tower>
struct Action_1_t7149A4E24DCDE38351BFE018FCC6DE62E88D5D12;
// System.Action`2<Towers.UI.Builder/State,Towers.UI.Builder/State>
struct Action_2_t202C402486F1480CE5A702E273710C3EF1006EDF;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[0...,0...]
struct BooleanU5BU2CU5D_tCD5854E1A55166EC93998DDED119F97FF5A5ECDC;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,Towers.Tower>
struct Dictionary_2_tC007A39DEED395F16A0D5539A314506782DF5092;
// System.Collections.Generic.List`1<Pathfinding.MeshCreator.Triangle>
struct List_1_tB20600C5F27F61A121E4F1B216435E032CA9DE32;
// System.Collections.Generic.List`1<Pathfinding.Nodes.Node>
struct List_1_t7306888B53570444822982D6E369D7C947D755BB;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,UnityEngine.Vector3>>
struct List_1_tB4C90384CD860CA4134CB5A40A69318313715E49;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<Towers.Tower>
struct List_1_tF9EC1C6B2834B01EDC824378E295B95026E2F32F;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0;
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_tFA660ACCBB75B7DE99651E0CED2A469BEEEF76CE;
// System.Collections.Generic.List`1<UnityEngine.TrailRenderer>
struct List_1_tE0D9DFFB62228152DD6534EEA186AFCF5BC079CC;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.Queue`1<UnityThreading.Task>
struct Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`2<Pathfinding.MeshCreator.Triangle,System.Single>
struct Func_2_t480B5D7B86240B0AA17CE63C1B28FAC99E8B64F5;
// System.Func`2<Pathfinding.Nodes.Node,System.Int32>
struct Func_2_t1D4CD12E8113CDF351DA2144EF3ECAD7A7E3BF0B;
// System.Func`2<Towers.Tower,System.String>
struct Func_2_t685175F73E65E811AD0DFB52202D871B7225642D;
// System.Func`2<UnityThreading.Task,System.Int32>
struct Func_2_tA39F4AF9333F296F37EC21B52181F6BF6082DCFF;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Random
struct Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Threading.ManualResetEvent
struct ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// Towers.Data.TowerLevelData
struct TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9;
// Towers.Tower
struct Tower_tCB73BE25139856154FE23524412B9114BA6260DF;
// Towers.TowerLevel
struct TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9;
// Towers.TowerLevel[]
struct TowerLevelU5BU5D_tE0775FCFD7723EC0E0794AEC8D999328F3E2F1FA;
// Towers.TowerPlacementGhost
struct TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7;
// Towers.UI.Builder
struct Builder_tF7414C0E077162F0CE605E009B1013AD98732805;
// Towers.UI.HUD.BuildInfoUI
struct BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF;
// Towers.UI.HUD.TowerInfoDisplay
struct TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6;
// Towers.UI.HUD.TowerSpawnButton[]
struct TowerSpawnButtonU5BU5D_tEC7065232C15DE6D9589F8404EEE4601C16AF697;
// Towers.UI.HUD.TowerUI
struct TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF;
// Towers.UI.RadiusVisualizerController
struct RadiusVisualizerController_tC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75;
// Turret
struct Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_tD93BAD8854B394AA1D372193F21154E94CA84BEB;
// UnityEngine.Animation
struct Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.LineRenderer
struct LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.MeshRenderer[]
struct MeshRendererU5BU5D_t5FE99DFF4D53B490C79494B8DCAA661F1721E0D3;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityThreading.Task
struct Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F;
// UnityThreading.TaskEndedEventHandler
struct TaskEndedEventHandler_t00BAD98C5606F4CC601A12E95E473B7B19B4C611;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef GAMEDATASTOREBASE_TEE7B21A3C806384FAE75290BCE97E0A9EE7255A6_H
#define GAMEDATASTOREBASE_TEE7B21A3C806384FAE75290BCE97E0A9EE7255A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Data.GameDataStoreBase
struct  GameDataStoreBase_tEE7B21A3C806384FAE75290BCE97E0A9EE7255A6  : public RuntimeObject
{
public:
	// System.Single Core.Data.GameDataStoreBase::masterVolume
	float ___masterVolume_0;
	// System.Single Core.Data.GameDataStoreBase::sfxVolume
	float ___sfxVolume_1;
	// System.Single Core.Data.GameDataStoreBase::musicVolume
	float ___musicVolume_2;

public:
	inline static int32_t get_offset_of_masterVolume_0() { return static_cast<int32_t>(offsetof(GameDataStoreBase_tEE7B21A3C806384FAE75290BCE97E0A9EE7255A6, ___masterVolume_0)); }
	inline float get_masterVolume_0() const { return ___masterVolume_0; }
	inline float* get_address_of_masterVolume_0() { return &___masterVolume_0; }
	inline void set_masterVolume_0(float value)
	{
		___masterVolume_0 = value;
	}

	inline static int32_t get_offset_of_sfxVolume_1() { return static_cast<int32_t>(offsetof(GameDataStoreBase_tEE7B21A3C806384FAE75290BCE97E0A9EE7255A6, ___sfxVolume_1)); }
	inline float get_sfxVolume_1() const { return ___sfxVolume_1; }
	inline float* get_address_of_sfxVolume_1() { return &___sfxVolume_1; }
	inline void set_sfxVolume_1(float value)
	{
		___sfxVolume_1 = value;
	}

	inline static int32_t get_offset_of_musicVolume_2() { return static_cast<int32_t>(offsetof(GameDataStoreBase_tEE7B21A3C806384FAE75290BCE97E0A9EE7255A6, ___musicVolume_2)); }
	inline float get_musicVolume_2() const { return ___musicVolume_2; }
	inline float* get_address_of_musicVolume_2() { return &___musicVolume_2; }
	inline void set_musicVolume_2(float value)
	{
		___musicVolume_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEDATASTOREBASE_TEE7B21A3C806384FAE75290BCE97E0A9EE7255A6_H
#ifndef ILISTEXTENSIONS_T3B5F7401DDFDCFB2A4C70AE498ED97D8F7090C2C_H
#define ILISTEXTENSIONS_T3B5F7401DDFDCFB2A4C70AE498ED97D8F7090C2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Extensions.IListExtensions
struct  IListExtensions_t3B5F7401DDFDCFB2A4C70AE498ED97D8F7090C2C  : public RuntimeObject
{
public:

public:
};

struct IListExtensions_t3B5F7401DDFDCFB2A4C70AE498ED97D8F7090C2C_StaticFields
{
public:
	// System.Random Core.Extensions.IListExtensions::s_SharedRandom
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ___s_SharedRandom_0;

public:
	inline static int32_t get_offset_of_s_SharedRandom_0() { return static_cast<int32_t>(offsetof(IListExtensions_t3B5F7401DDFDCFB2A4C70AE498ED97D8F7090C2C_StaticFields, ___s_SharedRandom_0)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get_s_SharedRandom_0() const { return ___s_SharedRandom_0; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of_s_SharedRandom_0() { return &___s_SharedRandom_0; }
	inline void set_s_SharedRandom_0(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		___s_SharedRandom_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_SharedRandom_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ILISTEXTENSIONS_T3B5F7401DDFDCFB2A4C70AE498ED97D8F7090C2C_H
#ifndef CURRENCY_T579EAFB2245C03FCD16C5F77C9D9EA23195F3E05_H
#define CURRENCY_T579EAFB2245C03FCD16C5F77C9D9EA23195F3E05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Economy.Currency
struct  Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05  : public RuntimeObject
{
public:
	// System.Int32 Economy.Currency::<currentCurrency>k__BackingField
	int32_t ___U3CcurrentCurrencyU3Ek__BackingField_0;
	// System.Action Economy.Currency::currencyChanged
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___currencyChanged_1;

public:
	inline static int32_t get_offset_of_U3CcurrentCurrencyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05, ___U3CcurrentCurrencyU3Ek__BackingField_0)); }
	inline int32_t get_U3CcurrentCurrencyU3Ek__BackingField_0() const { return ___U3CcurrentCurrencyU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CcurrentCurrencyU3Ek__BackingField_0() { return &___U3CcurrentCurrencyU3Ek__BackingField_0; }
	inline void set_U3CcurrentCurrencyU3Ek__BackingField_0(int32_t value)
	{
		___U3CcurrentCurrencyU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_currencyChanged_1() { return static_cast<int32_t>(offsetof(Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05, ___currencyChanged_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_currencyChanged_1() const { return ___currencyChanged_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_currencyChanged_1() { return &___currencyChanged_1; }
	inline void set_currencyChanged_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___currencyChanged_1 = value;
		Il2CppCodeGenWriteBarrier((&___currencyChanged_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENCY_T579EAFB2245C03FCD16C5F77C9D9EA23195F3E05_H
#ifndef MESHOBJECT_T3BA586F00E133946864FFCB15DE54A59D4E5E2B2_H
#define MESHOBJECT_T3BA586F00E133946864FFCB15DE54A59D4E5E2B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.MeshCreator.MeshObject
struct  MeshObject_t3BA586F00E133946864FFCB15DE54A59D4E5E2B2  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Pathfinding.MeshCreator.Triangle> Pathfinding.MeshCreator.MeshObject::triangles
	List_1_tB20600C5F27F61A121E4F1B216435E032CA9DE32 * ___triangles_0;
	// System.Single Pathfinding.MeshCreator.MeshObject::completeArea
	float ___completeArea_1;

public:
	inline static int32_t get_offset_of_triangles_0() { return static_cast<int32_t>(offsetof(MeshObject_t3BA586F00E133946864FFCB15DE54A59D4E5E2B2, ___triangles_0)); }
	inline List_1_tB20600C5F27F61A121E4F1B216435E032CA9DE32 * get_triangles_0() const { return ___triangles_0; }
	inline List_1_tB20600C5F27F61A121E4F1B216435E032CA9DE32 ** get_address_of_triangles_0() { return &___triangles_0; }
	inline void set_triangles_0(List_1_tB20600C5F27F61A121E4F1B216435E032CA9DE32 * value)
	{
		___triangles_0 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_0), value);
	}

	inline static int32_t get_offset_of_completeArea_1() { return static_cast<int32_t>(offsetof(MeshObject_t3BA586F00E133946864FFCB15DE54A59D4E5E2B2, ___completeArea_1)); }
	inline float get_completeArea_1() const { return ___completeArea_1; }
	inline float* get_address_of_completeArea_1() { return &___completeArea_1; }
	inline void set_completeArea_1(float value)
	{
		___completeArea_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHOBJECT_T3BA586F00E133946864FFCB15DE54A59D4E5E2B2_H
#ifndef U3CU3EC_TFD6DBEF54E6D28B6F7BD616D509265D264B445F8_H
#define U3CU3EC_TFD6DBEF54E6D28B6F7BD616D509265D264B445F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.MeshCreator.MeshObject/<>c
struct  U3CU3Ec_tFD6DBEF54E6D28B6F7BD616D509265D264B445F8  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tFD6DBEF54E6D28B6F7BD616D509265D264B445F8_StaticFields
{
public:
	// Pathfinding.MeshCreator.MeshObject/<>c Pathfinding.MeshCreator.MeshObject/<>c::<>9
	U3CU3Ec_tFD6DBEF54E6D28B6F7BD616D509265D264B445F8 * ___U3CU3E9_0;
	// System.Func`2<Pathfinding.MeshCreator.Triangle,System.Single> Pathfinding.MeshCreator.MeshObject/<>c::<>9__2_0
	Func_2_t480B5D7B86240B0AA17CE63C1B28FAC99E8B64F5 * ___U3CU3E9__2_0_1;
	// System.Func`2<Pathfinding.MeshCreator.Triangle,System.Single> Pathfinding.MeshCreator.MeshObject/<>c::<>9__3_0
	Func_2_t480B5D7B86240B0AA17CE63C1B28FAC99E8B64F5 * ___U3CU3E9__3_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFD6DBEF54E6D28B6F7BD616D509265D264B445F8_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tFD6DBEF54E6D28B6F7BD616D509265D264B445F8 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tFD6DBEF54E6D28B6F7BD616D509265D264B445F8 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tFD6DBEF54E6D28B6F7BD616D509265D264B445F8 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFD6DBEF54E6D28B6F7BD616D509265D264B445F8_StaticFields, ___U3CU3E9__2_0_1)); }
	inline Func_2_t480B5D7B86240B0AA17CE63C1B28FAC99E8B64F5 * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline Func_2_t480B5D7B86240B0AA17CE63C1B28FAC99E8B64F5 ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(Func_2_t480B5D7B86240B0AA17CE63C1B28FAC99E8B64F5 * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFD6DBEF54E6D28B6F7BD616D509265D264B445F8_StaticFields, ___U3CU3E9__3_0_2)); }
	inline Func_2_t480B5D7B86240B0AA17CE63C1B28FAC99E8B64F5 * get_U3CU3E9__3_0_2() const { return ___U3CU3E9__3_0_2; }
	inline Func_2_t480B5D7B86240B0AA17CE63C1B28FAC99E8B64F5 ** get_address_of_U3CU3E9__3_0_2() { return &___U3CU3E9__3_0_2; }
	inline void set_U3CU3E9__3_0_2(Func_2_t480B5D7B86240B0AA17CE63C1B28FAC99E8B64F5 * value)
	{
		___U3CU3E9__3_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TFD6DBEF54E6D28B6F7BD616D509265D264B445F8_H
#ifndef TRIANGULATOR_TE822FC04EEB8D0343FF299D8F7B14E508C55AD4E_H
#define TRIANGULATOR_TE822FC04EEB8D0343FF299D8F7B14E508C55AD4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.MeshCreator.Triangulator
struct  Triangulator_tE822FC04EEB8D0343FF299D8F7B14E508C55AD4E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Pathfinding.MeshCreator.Triangulator::m_Points
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Points_0;

public:
	inline static int32_t get_offset_of_m_Points_0() { return static_cast<int32_t>(offsetof(Triangulator_tE822FC04EEB8D0343FF299D8F7B14E508C55AD4E, ___m_Points_0)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Points_0() const { return ___m_Points_0; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Points_0() { return &___m_Points_0; }
	inline void set_m_Points_0(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Points_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Points_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATOR_TE822FC04EEB8D0343FF299D8F7B14E508C55AD4E_H
#ifndef U3CU3EC_T28270A5A54390944ABF6943F0A6A82B812BA4842_H
#define U3CU3EC_T28270A5A54390944ABF6943F0A6A82B812BA4842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Nodes.RandomNodeSelector/<>c
struct  U3CU3Ec_t28270A5A54390944ABF6943F0A6A82B812BA4842  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t28270A5A54390944ABF6943F0A6A82B812BA4842_StaticFields
{
public:
	// Pathfinding.Nodes.RandomNodeSelector/<>c Pathfinding.Nodes.RandomNodeSelector/<>c::<>9
	U3CU3Ec_t28270A5A54390944ABF6943F0A6A82B812BA4842 * ___U3CU3E9_0;
	// System.Func`2<Pathfinding.Nodes.Node,System.Int32> Pathfinding.Nodes.RandomNodeSelector/<>c::<>9__1_0
	Func_2_t1D4CD12E8113CDF351DA2144EF3ECAD7A7E3BF0B * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t28270A5A54390944ABF6943F0A6A82B812BA4842_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t28270A5A54390944ABF6943F0A6A82B812BA4842 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t28270A5A54390944ABF6943F0A6A82B812BA4842 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t28270A5A54390944ABF6943F0A6A82B812BA4842 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t28270A5A54390944ABF6943F0A6A82B812BA4842_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_t1D4CD12E8113CDF351DA2144EF3ECAD7A7E3BF0B * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_t1D4CD12E8113CDF351DA2144EF3ECAD7A7E3BF0B ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_t1D4CD12E8113CDF351DA2144EF3ECAD7A7E3BF0B * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T28270A5A54390944ABF6943F0A6A82B812BA4842_H
#ifndef PLACEMENTAREAEXTENSIONS_T8B49828B8AD5EFBF9E04C5B08CE30AA73B2F19EC_H
#define PLACEMENTAREAEXTENSIONS_T8B49828B8AD5EFBF9E04C5B08CE30AA73B2F19EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Placement.PlacementAreaExtensions
struct  PlacementAreaExtensions_t8B49828B8AD5EFBF9E04C5B08CE30AA73B2F19EC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEMENTAREAEXTENSIONS_T8B49828B8AD5EFBF9E04C5B08CE30AA73B2F19EC_H
#ifndef U3CDELAYINITIALDESTRUCTIONU3ED__7_T9FAE7563F4CEF6E366A82C07A45594A0DBF2699A_H
#define U3CDELAYINITIALDESTRUCTIONU3ED__7_T9FAE7563F4CEF6E366A82C07A45594A0DBF2699A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>d__7
struct  U3CDelayInitialDestructionU3Ed__7_t9FAE7563F4CEF6E366A82C07A45594A0DBF2699A  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>d__7::delayTime
	float ___delayTime_2;
	// SimpleHealthBar_SpaceshipExample.AsteroidController SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>d__7::<>4__this
	AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__7_t9FAE7563F4CEF6E366A82C07A45594A0DBF2699A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__7_t9FAE7563F4CEF6E366A82C07A45594A0DBF2699A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delayTime_2() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__7_t9FAE7563F4CEF6E366A82C07A45594A0DBF2699A, ___delayTime_2)); }
	inline float get_delayTime_2() const { return ___delayTime_2; }
	inline float* get_address_of_delayTime_2() { return &___delayTime_2; }
	inline void set_delayTime_2(float value)
	{
		___delayTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__7_t9FAE7563F4CEF6E366A82C07A45594A0DBF2699A, ___U3CU3E4__this_3)); }
	inline AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYINITIALDESTRUCTIONU3ED__7_T9FAE7563F4CEF6E366A82C07A45594A0DBF2699A_H
#ifndef U3CSPAWNHEALTHTIMERU3ED__25_TCFD0C835D21022468D481CBBCE79E73CF9EF5E69_H
#define U3CSPAWNHEALTHTIMERU3ED__25_TCFD0C835D21022468D481CBBCE79E73CF9EF5E69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.GameManager/<SpawnHealthTimer>d__25
struct  U3CSpawnHealthTimerU3Ed__25_tCFD0C835D21022468D481CBBCE79E73CF9EF5E69  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager/<SpawnHealthTimer>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.GameManager/<SpawnHealthTimer>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleHealthBar_SpaceshipExample.GameManager SimpleHealthBar_SpaceshipExample.GameManager/<SpawnHealthTimer>d__25::<>4__this
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSpawnHealthTimerU3Ed__25_tCFD0C835D21022468D481CBBCE79E73CF9EF5E69, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSpawnHealthTimerU3Ed__25_tCFD0C835D21022468D481CBBCE79E73CF9EF5E69, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSpawnHealthTimerU3Ed__25_tCFD0C835D21022468D481CBBCE79E73CF9EF5E69, ___U3CU3E4__this_2)); }
	inline GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPAWNHEALTHTIMERU3ED__25_TCFD0C835D21022468D481CBBCE79E73CF9EF5E69_H
#ifndef U3CSPAWNTIMERU3ED__27_TBA95B4D843AFFA5C8B5492DA62BEA4B2D2A9B33D_H
#define U3CSPAWNTIMERU3ED__27_TBA95B4D843AFFA5C8B5492DA62BEA4B2D2A9B33D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.GameManager/<SpawnTimer>d__27
struct  U3CSpawnTimerU3Ed__27_tBA95B4D843AFFA5C8B5492DA62BEA4B2D2A9B33D  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager/<SpawnTimer>d__27::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.GameManager/<SpawnTimer>d__27::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleHealthBar_SpaceshipExample.GameManager SimpleHealthBar_SpaceshipExample.GameManager/<SpawnTimer>d__27::<>4__this
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSpawnTimerU3Ed__27_tBA95B4D843AFFA5C8B5492DA62BEA4B2D2A9B33D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSpawnTimerU3Ed__27_tBA95B4D843AFFA5C8B5492DA62BEA4B2D2A9B33D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSpawnTimerU3Ed__27_tBA95B4D843AFFA5C8B5492DA62BEA4B2D2A9B33D, ___U3CU3E4__this_2)); }
	inline GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPAWNTIMERU3ED__27_TBA95B4D843AFFA5C8B5492DA62BEA4B2D2A9B33D_H
#ifndef U3CDELAYINITIALDESTRUCTIONU3ED__6_TAD05859DEE753EE3C15A1245A4FF5A3511DE6E02_H
#define U3CDELAYINITIALDESTRUCTIONU3ED__6_TAD05859DEE753EE3C15A1245A4FF5A3511DE6E02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>d__6
struct  U3CDelayInitialDestructionU3Ed__6_tAD05859DEE753EE3C15A1245A4FF5A3511DE6E02  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>d__6::delayTime
	float ___delayTime_2;
	// SimpleHealthBar_SpaceshipExample.HealthPickupController SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>d__6::<>4__this
	HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__6_tAD05859DEE753EE3C15A1245A4FF5A3511DE6E02, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__6_tAD05859DEE753EE3C15A1245A4FF5A3511DE6E02, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delayTime_2() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__6_tAD05859DEE753EE3C15A1245A4FF5A3511DE6E02, ___delayTime_2)); }
	inline float get_delayTime_2() const { return ___delayTime_2; }
	inline float* get_address_of_delayTime_2() { return &___delayTime_2; }
	inline void set_delayTime_2(float value)
	{
		___delayTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__6_tAD05859DEE753EE3C15A1245A4FF5A3511DE6E02, ___U3CU3E4__this_3)); }
	inline HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYINITIALDESTRUCTIONU3ED__6_TAD05859DEE753EE3C15A1245A4FF5A3511DE6E02_H
#ifndef U3CDELAYOVERHEATU3ED__29_T4214B957649EDFFD363CA17E9571FC8DD35BD919_H
#define U3CDELAYOVERHEATU3ED__29_T4214B957649EDFFD363CA17E9571FC8DD35BD919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>d__29
struct  U3CDelayOverheatU3Ed__29_t4214B957649EDFFD363CA17E9571FC8DD35BD919  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>d__29::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>d__29::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleHealthBar_SpaceshipExample.PlayerController SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>d__29::<>4__this
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2 * ___U3CU3E4__this_2;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>d__29::<t>5__2
	float ___U3CtU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayOverheatU3Ed__29_t4214B957649EDFFD363CA17E9571FC8DD35BD919, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayOverheatU3Ed__29_t4214B957649EDFFD363CA17E9571FC8DD35BD919, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDelayOverheatU3Ed__29_t4214B957649EDFFD363CA17E9571FC8DD35BD919, ___U3CU3E4__this_2)); }
	inline PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDelayOverheatU3Ed__29_t4214B957649EDFFD363CA17E9571FC8DD35BD919, ___U3CtU3E5__2_3)); }
	inline float get_U3CtU3E5__2_3() const { return ___U3CtU3E5__2_3; }
	inline float* get_address_of_U3CtU3E5__2_3() { return &___U3CtU3E5__2_3; }
	inline void set_U3CtU3E5__2_3(float value)
	{
		___U3CtU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYOVERHEATU3ED__29_T4214B957649EDFFD363CA17E9571FC8DD35BD919_H
#ifndef U3CINVULNERABLILTYU3ED__20_T167CD2A0C46BEF46F258FE0C37DE2238F0B966B2_H
#define U3CINVULNERABLILTYU3ED__20_T167CD2A0C46BEF46F258FE0C37DE2238F0B966B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.PlayerHealth/<Invulnerablilty>d__20
struct  U3CInvulnerabliltyU3Ed__20_t167CD2A0C46BEF46F258FE0C37DE2238F0B966B2  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.PlayerHealth/<Invulnerablilty>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.PlayerHealth/<Invulnerablilty>d__20::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleHealthBar_SpaceshipExample.PlayerHealth SimpleHealthBar_SpaceshipExample.PlayerHealth/<Invulnerablilty>d__20::<>4__this
	PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInvulnerabliltyU3Ed__20_t167CD2A0C46BEF46F258FE0C37DE2238F0B966B2, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInvulnerabliltyU3Ed__20_t167CD2A0C46BEF46F258FE0C37DE2238F0B966B2, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInvulnerabliltyU3Ed__20_t167CD2A0C46BEF46F258FE0C37DE2238F0B966B2, ___U3CU3E4__this_2)); }
	inline PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINVULNERABLILTYU3ED__20_T167CD2A0C46BEF46F258FE0C37DE2238F0B966B2_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef U3CU3EC_T604A742CF42B9D19303EA45C08CA95CBDECB2603_H
#define U3CU3EC_T604A742CF42B9D19303EA45C08CA95CBDECB2603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Towers.Data.TowerLibrary/<>c
struct  U3CU3Ec_t604A742CF42B9D19303EA45C08CA95CBDECB2603  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t604A742CF42B9D19303EA45C08CA95CBDECB2603_StaticFields
{
public:
	// Towers.Data.TowerLibrary/<>c Towers.Data.TowerLibrary/<>c::<>9
	U3CU3Ec_t604A742CF42B9D19303EA45C08CA95CBDECB2603 * ___U3CU3E9_0;
	// System.Func`2<Towers.Tower,System.String> Towers.Data.TowerLibrary/<>c::<>9__5_0
	Func_2_t685175F73E65E811AD0DFB52202D871B7225642D * ___U3CU3E9__5_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t604A742CF42B9D19303EA45C08CA95CBDECB2603_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t604A742CF42B9D19303EA45C08CA95CBDECB2603 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t604A742CF42B9D19303EA45C08CA95CBDECB2603 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t604A742CF42B9D19303EA45C08CA95CBDECB2603 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t604A742CF42B9D19303EA45C08CA95CBDECB2603_StaticFields, ___U3CU3E9__5_0_1)); }
	inline Func_2_t685175F73E65E811AD0DFB52202D871B7225642D * get_U3CU3E9__5_0_1() const { return ___U3CU3E9__5_0_1; }
	inline Func_2_t685175F73E65E811AD0DFB52202D871B7225642D ** get_address_of_U3CU3E9__5_0_1() { return &___U3CU3E9__5_0_1; }
	inline void set_U3CU3E9__5_0_1(Func_2_t685175F73E65E811AD0DFB52202D871B7225642D * value)
	{
		___U3CU3E9__5_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T604A742CF42B9D19303EA45C08CA95CBDECB2603_H
#ifndef ACTIONEXTENSION_T5F73C2D473CA1CC346E9ECB67783A82AD915956F_H
#define ACTIONEXTENSION_T5F73C2D473CA1CC346E9ECB67783A82AD915956F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.ActionExtension
struct  ActionExtension_t5F73C2D473CA1CC346E9ECB67783A82AD915956F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONEXTENSION_T5F73C2D473CA1CC346E9ECB67783A82AD915956F_H
#ifndef CHANNEL_1_T0339EDC239D726A81C161D498FAF9B9CEC352B87_H
#define CHANNEL_1_T0339EDC239D726A81C161D498FAF9B9CEC352B87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.Channel`1<System.Object>
struct  Channel_1_t0339EDC239D726A81C161D498FAF9B9CEC352B87  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> UnityThreading.Channel`1::buffer
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___buffer_0;
	// System.Object UnityThreading.Channel`1::setSyncRoot
	RuntimeObject * ___setSyncRoot_1;
	// System.Object UnityThreading.Channel`1::getSyncRoot
	RuntimeObject * ___getSyncRoot_2;
	// System.Object UnityThreading.Channel`1::disposeRoot
	RuntimeObject * ___disposeRoot_3;
	// System.Threading.ManualResetEvent UnityThreading.Channel`1::setEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___setEvent_4;
	// System.Threading.ManualResetEvent UnityThreading.Channel`1::getEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___getEvent_5;
	// System.Threading.ManualResetEvent UnityThreading.Channel`1::exitEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___exitEvent_6;
	// System.Boolean UnityThreading.Channel`1::disposed
	bool ___disposed_7;
	// System.Int32 UnityThreading.Channel`1::<BufferSize>k__BackingField
	int32_t ___U3CBufferSizeU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(Channel_1_t0339EDC239D726A81C161D498FAF9B9CEC352B87, ___buffer_0)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_buffer_0() const { return ___buffer_0; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_setSyncRoot_1() { return static_cast<int32_t>(offsetof(Channel_1_t0339EDC239D726A81C161D498FAF9B9CEC352B87, ___setSyncRoot_1)); }
	inline RuntimeObject * get_setSyncRoot_1() const { return ___setSyncRoot_1; }
	inline RuntimeObject ** get_address_of_setSyncRoot_1() { return &___setSyncRoot_1; }
	inline void set_setSyncRoot_1(RuntimeObject * value)
	{
		___setSyncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&___setSyncRoot_1), value);
	}

	inline static int32_t get_offset_of_getSyncRoot_2() { return static_cast<int32_t>(offsetof(Channel_1_t0339EDC239D726A81C161D498FAF9B9CEC352B87, ___getSyncRoot_2)); }
	inline RuntimeObject * get_getSyncRoot_2() const { return ___getSyncRoot_2; }
	inline RuntimeObject ** get_address_of_getSyncRoot_2() { return &___getSyncRoot_2; }
	inline void set_getSyncRoot_2(RuntimeObject * value)
	{
		___getSyncRoot_2 = value;
		Il2CppCodeGenWriteBarrier((&___getSyncRoot_2), value);
	}

	inline static int32_t get_offset_of_disposeRoot_3() { return static_cast<int32_t>(offsetof(Channel_1_t0339EDC239D726A81C161D498FAF9B9CEC352B87, ___disposeRoot_3)); }
	inline RuntimeObject * get_disposeRoot_3() const { return ___disposeRoot_3; }
	inline RuntimeObject ** get_address_of_disposeRoot_3() { return &___disposeRoot_3; }
	inline void set_disposeRoot_3(RuntimeObject * value)
	{
		___disposeRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&___disposeRoot_3), value);
	}

	inline static int32_t get_offset_of_setEvent_4() { return static_cast<int32_t>(offsetof(Channel_1_t0339EDC239D726A81C161D498FAF9B9CEC352B87, ___setEvent_4)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_setEvent_4() const { return ___setEvent_4; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_setEvent_4() { return &___setEvent_4; }
	inline void set_setEvent_4(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___setEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___setEvent_4), value);
	}

	inline static int32_t get_offset_of_getEvent_5() { return static_cast<int32_t>(offsetof(Channel_1_t0339EDC239D726A81C161D498FAF9B9CEC352B87, ___getEvent_5)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_getEvent_5() const { return ___getEvent_5; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_getEvent_5() { return &___getEvent_5; }
	inline void set_getEvent_5(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___getEvent_5 = value;
		Il2CppCodeGenWriteBarrier((&___getEvent_5), value);
	}

	inline static int32_t get_offset_of_exitEvent_6() { return static_cast<int32_t>(offsetof(Channel_1_t0339EDC239D726A81C161D498FAF9B9CEC352B87, ___exitEvent_6)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_exitEvent_6() const { return ___exitEvent_6; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_exitEvent_6() { return &___exitEvent_6; }
	inline void set_exitEvent_6(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___exitEvent_6 = value;
		Il2CppCodeGenWriteBarrier((&___exitEvent_6), value);
	}

	inline static int32_t get_offset_of_disposed_7() { return static_cast<int32_t>(offsetof(Channel_1_t0339EDC239D726A81C161D498FAF9B9CEC352B87, ___disposed_7)); }
	inline bool get_disposed_7() const { return ___disposed_7; }
	inline bool* get_address_of_disposed_7() { return &___disposed_7; }
	inline void set_disposed_7(bool value)
	{
		___disposed_7 = value;
	}

	inline static int32_t get_offset_of_U3CBufferSizeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Channel_1_t0339EDC239D726A81C161D498FAF9B9CEC352B87, ___U3CBufferSizeU3Ek__BackingField_8)); }
	inline int32_t get_U3CBufferSizeU3Ek__BackingField_8() const { return ___U3CBufferSizeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CBufferSizeU3Ek__BackingField_8() { return &___U3CBufferSizeU3Ek__BackingField_8; }
	inline void set_U3CBufferSizeU3Ek__BackingField_8(int32_t value)
	{
		___U3CBufferSizeU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNEL_1_T0339EDC239D726A81C161D498FAF9B9CEC352B87_H
#ifndef U3CU3EC_T2F4A834ED9678DF4E594B0DFE0366030E796CB55_H
#define U3CU3EC_T2F4A834ED9678DF4E594B0DFE0366030E796CB55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.DispatcherBase/<>c
struct  U3CU3Ec_t2F4A834ED9678DF4E594B0DFE0366030E796CB55  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2F4A834ED9678DF4E594B0DFE0366030E796CB55_StaticFields
{
public:
	// UnityThreading.DispatcherBase/<>c UnityThreading.DispatcherBase/<>c::<>9
	U3CU3Ec_t2F4A834ED9678DF4E594B0DFE0366030E796CB55 * ___U3CU3E9_0;
	// System.Func`2<UnityThreading.Task,System.Int32> UnityThreading.DispatcherBase/<>c::<>9__20_0
	Func_2_tA39F4AF9333F296F37EC21B52181F6BF6082DCFF * ___U3CU3E9__20_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2F4A834ED9678DF4E594B0DFE0366030E796CB55_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2F4A834ED9678DF4E594B0DFE0366030E796CB55 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2F4A834ED9678DF4E594B0DFE0366030E796CB55 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2F4A834ED9678DF4E594B0DFE0366030E796CB55 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__20_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2F4A834ED9678DF4E594B0DFE0366030E796CB55_StaticFields, ___U3CU3E9__20_0_1)); }
	inline Func_2_tA39F4AF9333F296F37EC21B52181F6BF6082DCFF * get_U3CU3E9__20_0_1() const { return ___U3CU3E9__20_0_1; }
	inline Func_2_tA39F4AF9333F296F37EC21B52181F6BF6082DCFF ** get_address_of_U3CU3E9__20_0_1() { return &___U3CU3E9__20_0_1; }
	inline void set_U3CU3E9__20_0_1(Func_2_tA39F4AF9333F296F37EC21B52181F6BF6082DCFF * value)
	{
		___U3CU3E9__20_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__20_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2F4A834ED9678DF4E594B0DFE0366030E796CB55_H
#ifndef ENUMERABLEEXTENSION_T4C8E385903C82D244863B9A6A36944EC45BBF3E6_H
#define ENUMERABLEEXTENSION_T4C8E385903C82D244863B9A6A36944EC45BBF3E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.EnumerableExtension
struct  EnumerableExtension_t4C8E385903C82D244863B9A6A36944EC45BBF3E6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERABLEEXTENSION_T4C8E385903C82D244863B9A6A36944EC45BBF3E6_H
#ifndef ENUMERATOREXTENSION_T9A5A9C5C5108631CECEE49E3B4797F8764F76719_H
#define ENUMERATOREXTENSION_T9A5A9C5C5108631CECEE49E3B4797F8764F76719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.EnumeratorExtension
struct  EnumeratorExtension_t9A5A9C5C5108631CECEE49E3B4797F8764F76719  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOREXTENSION_T9A5A9C5C5108631CECEE49E3B4797F8764F76719_H
#ifndef OBJECTEXTENSION_T3BD2FE20BD963D62D315A19A17748809208FB3C8_H
#define OBJECTEXTENSION_T3BD2FE20BD963D62D315A19A17748809208FB3C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.ObjectExtension
struct  ObjectExtension_t3BD2FE20BD963D62D315A19A17748809208FB3C8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTEXTENSION_T3BD2FE20BD963D62D315A19A17748809208FB3C8_H
#ifndef U3CU3EC__DISPLAYCLASS43_0_T6E9F076F4B7B65FD7A272546C7262408006196A8_H
#define U3CU3EC__DISPLAYCLASS43_0_T6E9F076F4B7B65FD7A272546C7262408006196A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.Task/<>c__DisplayClass43_0
struct  U3CU3Ec__DisplayClass43_0_t6E9F076F4B7B65FD7A272546C7262408006196A8  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator UnityThreading.Task/<>c__DisplayClass43_0::enumerator
	RuntimeObject* ___enumerator_0;
	// UnityThreading.Task UnityThreading.Task/<>c__DisplayClass43_0::<>4__this
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * ___U3CU3E4__this_1;
	// System.Action UnityThreading.Task/<>c__DisplayClass43_0::<>9__0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__0_2;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_0_t6E9F076F4B7B65FD7A272546C7262408006196A8, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_0_t6E9F076F4B7B65FD7A272546C7262408006196A8, ___U3CU3E4__this_1)); }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_0_t6E9F076F4B7B65FD7A272546C7262408006196A8, ___U3CU3E9__0_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS43_0_T6E9F076F4B7B65FD7A272546C7262408006196A8_H
#ifndef INTVECTOR2_TB8D88944661CECEA9BC4360526A64507248EDFA9_H
#define INTVECTOR2_TB8D88944661CECEA9BC4360526A64507248EDFA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.IntVector2
struct  IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9 
{
public:
	// System.Int32 Core.Utilities.IntVector2::x
	int32_t ___x_2;
	// System.Int32 Core.Utilities.IntVector2::y
	int32_t ___y_3;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9, ___x_2)); }
	inline int32_t get_x_2() const { return ___x_2; }
	inline int32_t* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(int32_t value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9, ___y_3)); }
	inline int32_t get_y_3() const { return ___y_3; }
	inline int32_t* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(int32_t value)
	{
		___y_3 = value;
	}
};

struct IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9_StaticFields
{
public:
	// Core.Utilities.IntVector2 Core.Utilities.IntVector2::one
	IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  ___one_0;
	// Core.Utilities.IntVector2 Core.Utilities.IntVector2::zero
	IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  ___zero_1;

public:
	inline static int32_t get_offset_of_one_0() { return static_cast<int32_t>(offsetof(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9_StaticFields, ___one_0)); }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  get_one_0() const { return ___one_0; }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9 * get_address_of_one_0() { return &___one_0; }
	inline void set_one_0(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  value)
	{
		___one_0 = value;
	}

	inline static int32_t get_offset_of_zero_1() { return static_cast<int32_t>(offsetof(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9_StaticFields, ___zero_1)); }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  get_zero_1() const { return ___zero_1; }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9 * get_address_of_zero_1() { return &___zero_1; }
	inline void set_zero_1(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  value)
	{
		___zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTVECTOR2_TB8D88944661CECEA9BC4360526A64507248EDFA9_H
#ifndef CURRENCYCHANGEINFO_T9FD7557727D54B624C05DA83849209C8E6C548E0_H
#define CURRENCYCHANGEINFO_T9FD7557727D54B624C05DA83849209C8E6C548E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Economy.CurrencyChangeInfo
struct  CurrencyChangeInfo_t9FD7557727D54B624C05DA83849209C8E6C548E0 
{
public:
	// System.Int32 Economy.CurrencyChangeInfo::previousCurrency
	int32_t ___previousCurrency_0;
	// System.Int32 Economy.CurrencyChangeInfo::currentCurrency
	int32_t ___currentCurrency_1;
	// System.Int32 Economy.CurrencyChangeInfo::difference
	int32_t ___difference_2;
	// System.Int32 Economy.CurrencyChangeInfo::absoluteDifference
	int32_t ___absoluteDifference_3;

public:
	inline static int32_t get_offset_of_previousCurrency_0() { return static_cast<int32_t>(offsetof(CurrencyChangeInfo_t9FD7557727D54B624C05DA83849209C8E6C548E0, ___previousCurrency_0)); }
	inline int32_t get_previousCurrency_0() const { return ___previousCurrency_0; }
	inline int32_t* get_address_of_previousCurrency_0() { return &___previousCurrency_0; }
	inline void set_previousCurrency_0(int32_t value)
	{
		___previousCurrency_0 = value;
	}

	inline static int32_t get_offset_of_currentCurrency_1() { return static_cast<int32_t>(offsetof(CurrencyChangeInfo_t9FD7557727D54B624C05DA83849209C8E6C548E0, ___currentCurrency_1)); }
	inline int32_t get_currentCurrency_1() const { return ___currentCurrency_1; }
	inline int32_t* get_address_of_currentCurrency_1() { return &___currentCurrency_1; }
	inline void set_currentCurrency_1(int32_t value)
	{
		___currentCurrency_1 = value;
	}

	inline static int32_t get_offset_of_difference_2() { return static_cast<int32_t>(offsetof(CurrencyChangeInfo_t9FD7557727D54B624C05DA83849209C8E6C548E0, ___difference_2)); }
	inline int32_t get_difference_2() const { return ___difference_2; }
	inline int32_t* get_address_of_difference_2() { return &___difference_2; }
	inline void set_difference_2(int32_t value)
	{
		___difference_2 = value;
	}

	inline static int32_t get_offset_of_absoluteDifference_3() { return static_cast<int32_t>(offsetof(CurrencyChangeInfo_t9FD7557727D54B624C05DA83849209C8E6C548E0, ___absoluteDifference_3)); }
	inline int32_t get_absoluteDifference_3() const { return ___absoluteDifference_3; }
	inline int32_t* get_address_of_absoluteDifference_3() { return &___absoluteDifference_3; }
	inline void set_absoluteDifference_3(int32_t value)
	{
		___absoluteDifference_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENCYCHANGEINFO_T9FD7557727D54B624C05DA83849209C8E6C548E0_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#define LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef CHANNEL_T8FA7A794BA08A27A818FB3D8E68B435E7738E536_H
#define CHANNEL_T8FA7A794BA08A27A818FB3D8E68B435E7738E536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.Channel
struct  Channel_t8FA7A794BA08A27A818FB3D8E68B435E7738E536  : public Channel_1_t0339EDC239D726A81C161D498FAF9B9CEC352B87
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNEL_T8FA7A794BA08A27A818FB3D8E68B435E7738E536_H
#ifndef UNIT_T3749682E243E386DEAEF1418B5180C6E0E00F84D_H
#define UNIT_T3749682E243E386DEAEF1418B5180C6E0E00F84D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.Task/Unit
struct  Unit_t3749682E243E386DEAEF1418B5180C6E0E00F84D 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Unit_t3749682E243E386DEAEF1418B5180C6E0E00F84D__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIT_T3749682E243E386DEAEF1418B5180C6E0E00F84D_H
#ifndef STARTZOOMMODE_T66CB6AACCF501BD91E38B85FA4A11EEDBF56B2D1_H
#define STARTZOOMMODE_T66CB6AACCF501BD91E38B85FA4A11EEDBF56B2D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Camera.CameraInitialState/StartZoomMode
struct  StartZoomMode_t66CB6AACCF501BD91E38B85FA4A11EEDBF56B2D1 
{
public:
	// System.Int32 Core.Camera.CameraInitialState/StartZoomMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StartZoomMode_t66CB6AACCF501BD91E38B85FA4A11EEDBF56B2D1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTZOOMMODE_T66CB6AACCF501BD91E38B85FA4A11EEDBF56B2D1_H
#ifndef LIGHTNINGBOLTANIMATIONMODE_TEED68806DD3934973F957DB59EBBED0142C57EE7_H
#define LIGHTNINGBOLTANIMATIONMODE_TEED68806DD3934973F957DB59EBBED0142C57EE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.LightningBolt.LightningBoltAnimationMode
struct  LightningBoltAnimationMode_tEED68806DD3934973F957DB59EBBED0142C57EE7 
{
public:
	// System.Int32 DigitalRuby.LightningBolt.LightningBoltAnimationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LightningBoltAnimationMode_tEED68806DD3934973F957DB59EBBED0142C57EE7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTNINGBOLTANIMATIONMODE_TEED68806DD3934973F957DB59EBBED0142C57EE7_H
#ifndef STATE_T5D7DF387DD891673A309CB96CA602DB9652EC5CC_H
#define STATE_T5D7DF387DD891673A309CB96CA602DB9652EC5CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Agents.Agent/State
struct  State_t5D7DF387DD891673A309CB96CA602DB9652EC5CC 
{
public:
	// System.Int32 Pathfinding.Agents.Agent/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t5D7DF387DD891673A309CB96CA602DB9652EC5CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T5D7DF387DD891673A309CB96CA602DB9652EC5CC_H
#ifndef TRIANGLE_T5CDAC049E48591B7FA7637C0185B5DF87DEAD231_H
#define TRIANGLE_T5CDAC049E48591B7FA7637C0185B5DF87DEAD231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.MeshCreator.Triangle
struct  Triangle_t5CDAC049E48591B7FA7637C0185B5DF87DEAD231  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Pathfinding.MeshCreator.Triangle::v0
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___v0_0;
	// UnityEngine.Vector3 Pathfinding.MeshCreator.Triangle::v1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___v1_1;
	// UnityEngine.Vector3 Pathfinding.MeshCreator.Triangle::v2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___v2_2;
	// System.Single Pathfinding.MeshCreator.Triangle::area
	float ___area_3;

public:
	inline static int32_t get_offset_of_v0_0() { return static_cast<int32_t>(offsetof(Triangle_t5CDAC049E48591B7FA7637C0185B5DF87DEAD231, ___v0_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_v0_0() const { return ___v0_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_v0_0() { return &___v0_0; }
	inline void set_v0_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___v0_0 = value;
	}

	inline static int32_t get_offset_of_v1_1() { return static_cast<int32_t>(offsetof(Triangle_t5CDAC049E48591B7FA7637C0185B5DF87DEAD231, ___v1_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_v1_1() const { return ___v1_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_v1_1() { return &___v1_1; }
	inline void set_v1_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___v1_1 = value;
	}

	inline static int32_t get_offset_of_v2_2() { return static_cast<int32_t>(offsetof(Triangle_t5CDAC049E48591B7FA7637C0185B5DF87DEAD231, ___v2_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_v2_2() const { return ___v2_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_v2_2() { return &___v2_2; }
	inline void set_v2_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___v2_2 = value;
	}

	inline static int32_t get_offset_of_area_3() { return static_cast<int32_t>(offsetof(Triangle_t5CDAC049E48591B7FA7637C0185B5DF87DEAD231, ___area_3)); }
	inline float get_area_3() const { return ___area_3; }
	inline float* get_address_of_area_3() { return &___area_3; }
	inline void set_area_3(float value)
	{
		___area_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGLE_T5CDAC049E48591B7FA7637C0185B5DF87DEAD231_H
#ifndef OBJECTFITSTATUS_T18139825A4B0C476F24EBA127E1735F2A03C1D50_H
#define OBJECTFITSTATUS_T18139825A4B0C476F24EBA127E1735F2A03C1D50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Placement.ObjectFitStatus
struct  ObjectFitStatus_t18139825A4B0C476F24EBA127E1735F2A03C1D50 
{
public:
	// System.Int32 Placement.ObjectFitStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectFitStatus_t18139825A4B0C476F24EBA127E1735F2A03C1D50, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTFITSTATUS_T18139825A4B0C476F24EBA127E1735F2A03C1D50_H
#ifndef PLACEMENTTILESTATE_T815C7EABFA640F06EA4F26BDCD0487B0E47CC584_H
#define PLACEMENTTILESTATE_T815C7EABFA640F06EA4F26BDCD0487B0E47CC584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Placement.PlacementTileState
struct  PlacementTileState_t815C7EABFA640F06EA4F26BDCD0487B0E47CC584 
{
public:
	// System.Int32 Placement.PlacementTileState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlacementTileState_t815C7EABFA640F06EA4F26BDCD0487B0E47CC584, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEMENTTILESTATE_T815C7EABFA640F06EA4F26BDCD0487B0E47CC584_H
#ifndef U3CFADEDEATHSCREENU3ED__32_T343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2_H
#define U3CFADEDEATHSCREENU3ED__32_T343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32
struct  U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleHealthBar_SpaceshipExample.GameManager SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32::<>4__this
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC * ___U3CU3E4__this_2;
	// UnityEngine.Color SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32::<imageColor>5__2
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___U3CimageColorU3E5__2_3;
	// UnityEngine.Color SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32::<textColor>5__3
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___U3CtextColorU3E5__3_4;
	// UnityEngine.Color SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32::<finalScoreTextColor>5__4
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___U3CfinalScoreTextColorU3E5__4_5;
	// System.Single SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32::<t>5__5
	float ___U3CtU3E5__5_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2, ___U3CU3E4__this_2)); }
	inline GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CimageColorU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2, ___U3CimageColorU3E5__2_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_U3CimageColorU3E5__2_3() const { return ___U3CimageColorU3E5__2_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_U3CimageColorU3E5__2_3() { return &___U3CimageColorU3E5__2_3; }
	inline void set_U3CimageColorU3E5__2_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___U3CimageColorU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CtextColorU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2, ___U3CtextColorU3E5__3_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_U3CtextColorU3E5__3_4() const { return ___U3CtextColorU3E5__3_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_U3CtextColorU3E5__3_4() { return &___U3CtextColorU3E5__3_4; }
	inline void set_U3CtextColorU3E5__3_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___U3CtextColorU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CfinalScoreTextColorU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2, ___U3CfinalScoreTextColorU3E5__4_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_U3CfinalScoreTextColorU3E5__4_5() const { return ___U3CfinalScoreTextColorU3E5__4_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_U3CfinalScoreTextColorU3E5__4_5() { return &___U3CfinalScoreTextColorU3E5__4_5; }
	inline void set_U3CfinalScoreTextColorU3E5__4_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___U3CfinalScoreTextColorU3E5__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2, ___U3CtU3E5__5_6)); }
	inline float get_U3CtU3E5__5_6() const { return ___U3CtU3E5__5_6; }
	inline float* get_address_of_U3CtU3E5__5_6() { return &___U3CtU3E5__5_6; }
	inline void set_U3CtU3E5__5_6(float value)
	{
		___U3CtU3E5__5_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEDEATHSCREENU3ED__32_T343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2_H
#ifndef U3CSHAKECAMERAU3ED__21_T09EFFA8ACEA55E8BEFDC29E8061AEBA20F1A1CCA_H
#define U3CSHAKECAMERAU3ED__21_T09EFFA8ACEA55E8BEFDC29E8061AEBA20F1A1CCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>d__21
struct  U3CShakeCameraU3Ed__21_t09EFFA8ACEA55E8BEFDC29E8061AEBA20F1A1CCA  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>d__21::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Vector2 SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>d__21::<origPos>5__2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CorigPosU3E5__2_2;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>d__21::<t>5__3
	float ___U3CtU3E5__3_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShakeCameraU3Ed__21_t09EFFA8ACEA55E8BEFDC29E8061AEBA20F1A1CCA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CShakeCameraU3Ed__21_t09EFFA8ACEA55E8BEFDC29E8061AEBA20F1A1CCA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CorigPosU3E5__2_2() { return static_cast<int32_t>(offsetof(U3CShakeCameraU3Ed__21_t09EFFA8ACEA55E8BEFDC29E8061AEBA20F1A1CCA, ___U3CorigPosU3E5__2_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CorigPosU3E5__2_2() const { return ___U3CorigPosU3E5__2_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CorigPosU3E5__2_2() { return &___U3CorigPosU3E5__2_2; }
	inline void set_U3CorigPosU3E5__2_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CorigPosU3E5__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__3_3() { return static_cast<int32_t>(offsetof(U3CShakeCameraU3Ed__21_t09EFFA8ACEA55E8BEFDC29E8061AEBA20F1A1CCA, ___U3CtU3E5__3_3)); }
	inline float get_U3CtU3E5__3_3() const { return ___U3CtU3E5__3_3; }
	inline float* get_address_of_U3CtU3E5__3_3() { return &___U3CtU3E5__3_3; }
	inline void set_U3CtU3E5__3_3(float value)
	{
		___U3CtU3E5__3_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHAKECAMERAU3ED__21_T09EFFA8ACEA55E8BEFDC29E8061AEBA20F1A1CCA_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef LEVELSTATE_T52F9A6FD8458DFF7F387C7F57210D9842A2FFAFE_H
#define LEVELSTATE_T52F9A6FD8458DFF7F387C7F57210D9842A2FFAFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerDefense.Level.LevelState
struct  LevelState_t52F9A6FD8458DFF7F387C7F57210D9842A2FFAFE 
{
public:
	// System.Int32 TowerDefense.Level.LevelState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LevelState_t52F9A6FD8458DFF7F387C7F57210D9842A2FFAFE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSTATE_T52F9A6FD8458DFF7F387C7F57210D9842A2FFAFE_H
#ifndef STATE_T8F92AE8C7A2512F09D0216DA21E50391B17AE1BC_H
#define STATE_T8F92AE8C7A2512F09D0216DA21E50391B17AE1BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Towers.UI.Builder/State
struct  State_t8F92AE8C7A2512F09D0216DA21E50391B17AE1BC 
{
public:
	// System.Int32 Towers.UI.Builder/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t8F92AE8C7A2512F09D0216DA21E50391B17AE1BC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T8F92AE8C7A2512F09D0216DA21E50391B17AE1BC_H
#ifndef ANIMATIONSTATE_T55EC65D7C3704BE87F1DB1AF06EB62BAB5264F2B_H
#define ANIMATIONSTATE_T55EC65D7C3704BE87F1DB1AF06EB62BAB5264F2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Towers.UI.HUD.BuildInfoUI/AnimationState
struct  AnimationState_t55EC65D7C3704BE87F1DB1AF06EB62BAB5264F2B 
{
public:
	// System.Int32 Towers.UI.HUD.BuildInfoUI/AnimationState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnimationState_t55EC65D7C3704BE87F1DB1AF06EB62BAB5264F2B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTATE_T55EC65D7C3704BE87F1DB1AF06EB62BAB5264F2B_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef PLANE_T0903921088DEEDE1BCDEA5BF279EDBCFC9679AED_H
#define PLANE_T0903921088DEEDE1BCDEA5BF279EDBCFC9679AED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED, ___m_Normal_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T0903921088DEEDE1BCDEA5BF279EDBCFC9679AED_H
#ifndef RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#define RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Origin_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Direction_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifndef RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#define RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifndef TARGETTYPE_T0D7AB4ABEFBCFB09F254BD3EF3CDCCEC31107907_H
#define TARGETTYPE_T0D7AB4ABEFBCFB09F254BD3EF3CDCCEC31107907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.SwitchTo/TargetType
struct  TargetType_t0D7AB4ABEFBCFB09F254BD3EF3CDCCEC31107907 
{
public:
	// System.Int32 UnityThreading.SwitchTo/TargetType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TargetType_t0D7AB4ABEFBCFB09F254BD3EF3CDCCEC31107907, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETTYPE_T0D7AB4ABEFBCFB09F254BD3EF3CDCCEC31107907_H
#ifndef TASK_T5886E986429BB4D23327D2F8D6B94A4DB454B15F_H
#define TASK_T5886E986429BB4D23327D2F8D6B94A4DB454B15F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.Task
struct  Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F  : public RuntimeObject
{
public:
	// System.Object UnityThreading.Task::syncRoot
	RuntimeObject * ___syncRoot_0;
	// UnityThreading.TaskEndedEventHandler UnityThreading.Task::taskEnded
	TaskEndedEventHandler_t00BAD98C5606F4CC601A12E95E473B7B19B4C611 * ___taskEnded_1;
	// System.Boolean UnityThreading.Task::hasEnded
	bool ___hasEnded_2;
	// System.String UnityThreading.Task::Name
	String_t* ___Name_3;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) UnityThreading.Task::Priority
	int32_t ___Priority_4;
	// System.Threading.ManualResetEvent UnityThreading.Task::abortEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___abortEvent_5;
	// System.Threading.ManualResetEvent UnityThreading.Task::endedEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___endedEvent_6;
	// System.Threading.ManualResetEvent UnityThreading.Task::endingEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___endingEvent_7;
	// System.Boolean UnityThreading.Task::hasStarted
	bool ___hasStarted_8;
	// System.Boolean UnityThreading.Task::disposed
	bool ___disposed_10;

public:
	inline static int32_t get_offset_of_syncRoot_0() { return static_cast<int32_t>(offsetof(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F, ___syncRoot_0)); }
	inline RuntimeObject * get_syncRoot_0() const { return ___syncRoot_0; }
	inline RuntimeObject ** get_address_of_syncRoot_0() { return &___syncRoot_0; }
	inline void set_syncRoot_0(RuntimeObject * value)
	{
		___syncRoot_0 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_0), value);
	}

	inline static int32_t get_offset_of_taskEnded_1() { return static_cast<int32_t>(offsetof(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F, ___taskEnded_1)); }
	inline TaskEndedEventHandler_t00BAD98C5606F4CC601A12E95E473B7B19B4C611 * get_taskEnded_1() const { return ___taskEnded_1; }
	inline TaskEndedEventHandler_t00BAD98C5606F4CC601A12E95E473B7B19B4C611 ** get_address_of_taskEnded_1() { return &___taskEnded_1; }
	inline void set_taskEnded_1(TaskEndedEventHandler_t00BAD98C5606F4CC601A12E95E473B7B19B4C611 * value)
	{
		___taskEnded_1 = value;
		Il2CppCodeGenWriteBarrier((&___taskEnded_1), value);
	}

	inline static int32_t get_offset_of_hasEnded_2() { return static_cast<int32_t>(offsetof(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F, ___hasEnded_2)); }
	inline bool get_hasEnded_2() const { return ___hasEnded_2; }
	inline bool* get_address_of_hasEnded_2() { return &___hasEnded_2; }
	inline void set_hasEnded_2(bool value)
	{
		___hasEnded_2 = value;
	}

	inline static int32_t get_offset_of_Name_3() { return static_cast<int32_t>(offsetof(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F, ___Name_3)); }
	inline String_t* get_Name_3() const { return ___Name_3; }
	inline String_t** get_address_of_Name_3() { return &___Name_3; }
	inline void set_Name_3(String_t* value)
	{
		___Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___Name_3), value);
	}

	inline static int32_t get_offset_of_Priority_4() { return static_cast<int32_t>(offsetof(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F, ___Priority_4)); }
	inline int32_t get_Priority_4() const { return ___Priority_4; }
	inline int32_t* get_address_of_Priority_4() { return &___Priority_4; }
	inline void set_Priority_4(int32_t value)
	{
		___Priority_4 = value;
	}

	inline static int32_t get_offset_of_abortEvent_5() { return static_cast<int32_t>(offsetof(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F, ___abortEvent_5)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_abortEvent_5() const { return ___abortEvent_5; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_abortEvent_5() { return &___abortEvent_5; }
	inline void set_abortEvent_5(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___abortEvent_5 = value;
		Il2CppCodeGenWriteBarrier((&___abortEvent_5), value);
	}

	inline static int32_t get_offset_of_endedEvent_6() { return static_cast<int32_t>(offsetof(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F, ___endedEvent_6)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_endedEvent_6() const { return ___endedEvent_6; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_endedEvent_6() { return &___endedEvent_6; }
	inline void set_endedEvent_6(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___endedEvent_6 = value;
		Il2CppCodeGenWriteBarrier((&___endedEvent_6), value);
	}

	inline static int32_t get_offset_of_endingEvent_7() { return static_cast<int32_t>(offsetof(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F, ___endingEvent_7)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_endingEvent_7() const { return ___endingEvent_7; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_endingEvent_7() { return &___endingEvent_7; }
	inline void set_endingEvent_7(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___endingEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___endingEvent_7), value);
	}

	inline static int32_t get_offset_of_hasStarted_8() { return static_cast<int32_t>(offsetof(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F, ___hasStarted_8)); }
	inline bool get_hasStarted_8() const { return ___hasStarted_8; }
	inline bool* get_address_of_hasStarted_8() { return &___hasStarted_8; }
	inline void set_hasStarted_8(bool value)
	{
		___hasStarted_8 = value;
	}

	inline static int32_t get_offset_of_disposed_10() { return static_cast<int32_t>(offsetof(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F, ___disposed_10)); }
	inline bool get_disposed_10() const { return ___disposed_10; }
	inline bool* get_address_of_disposed_10() { return &___disposed_10; }
	inline void set_disposed_10(bool value)
	{
		___disposed_10 = value;
	}
};

struct Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F_ThreadStaticFields
{
public:
	// UnityThreading.Task UnityThreading.Task::current
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * ___current_9;

public:
	inline static int32_t get_offset_of_current_9() { return static_cast<int32_t>(offsetof(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F_ThreadStaticFields, ___current_9)); }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * get_current_9() const { return ___current_9; }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F ** get_address_of_current_9() { return &___current_9; }
	inline void set_current_9(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * value)
	{
		___current_9 = value;
		Il2CppCodeGenWriteBarrier((&___current_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASK_T5886E986429BB4D23327D2F8D6B94A4DB454B15F_H
#ifndef TASKSORTINGSYSTEM_T69AB27974F2B4DEF923567D25F773D47B3DD0F22_H
#define TASKSORTINGSYSTEM_T69AB27974F2B4DEF923567D25F773D47B3DD0F22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskSortingSystem
struct  TaskSortingSystem_t69AB27974F2B4DEF923567D25F773D47B3DD0F22 
{
public:
	// System.Int32 UnityThreading.TaskSortingSystem::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TaskSortingSystem_t69AB27974F2B4DEF923567D25F773D47B3DD0F22, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKSORTINGSYSTEM_T69AB27974F2B4DEF923567D25F773D47B3DD0F22_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef NULLABLE_1_T6224B732028631256932FA4C4176E10D171EFE29_H
#define NULLABLE_1_T6224B732028631256932FA4C4176E10D171EFE29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.RaycastHit>
struct  Nullable_1_t6224B732028631256932FA4C4176E10D171EFE29 
{
public:
	// T System.Nullable`1::value
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t6224B732028631256932FA4C4176E10D171EFE29, ___value_0)); }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  get_value_0() const { return ___value_0; }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t6224B732028631256932FA4C4176E10D171EFE29, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T6224B732028631256932FA4C4176E10D171EFE29_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef DISPATCHERBASE_TB856AF22CD8042F30BC3EA0B35779BBF104A4578_H
#define DISPATCHERBASE_TB856AF22CD8042F30BC3EA0B35779BBF104A4578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.DispatcherBase
struct  DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578  : public RuntimeObject
{
public:
	// System.Int32 UnityThreading.DispatcherBase::lockCount
	int32_t ___lockCount_0;
	// System.Object UnityThreading.DispatcherBase::taskListSyncRoot
	RuntimeObject * ___taskListSyncRoot_1;
	// System.Collections.Generic.Queue`1<UnityThreading.Task> UnityThreading.DispatcherBase::taskList
	Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 * ___taskList_2;
	// System.Collections.Generic.Queue`1<UnityThreading.Task> UnityThreading.DispatcherBase::delayedTaskList
	Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 * ___delayedTaskList_3;
	// System.Threading.ManualResetEvent UnityThreading.DispatcherBase::dataEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___dataEvent_4;
	// System.Boolean UnityThreading.DispatcherBase::AllowAccessLimitationChecks
	bool ___AllowAccessLimitationChecks_5;
	// UnityThreading.TaskSortingSystem UnityThreading.DispatcherBase::TaskSortingSystem
	int32_t ___TaskSortingSystem_6;

public:
	inline static int32_t get_offset_of_lockCount_0() { return static_cast<int32_t>(offsetof(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578, ___lockCount_0)); }
	inline int32_t get_lockCount_0() const { return ___lockCount_0; }
	inline int32_t* get_address_of_lockCount_0() { return &___lockCount_0; }
	inline void set_lockCount_0(int32_t value)
	{
		___lockCount_0 = value;
	}

	inline static int32_t get_offset_of_taskListSyncRoot_1() { return static_cast<int32_t>(offsetof(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578, ___taskListSyncRoot_1)); }
	inline RuntimeObject * get_taskListSyncRoot_1() const { return ___taskListSyncRoot_1; }
	inline RuntimeObject ** get_address_of_taskListSyncRoot_1() { return &___taskListSyncRoot_1; }
	inline void set_taskListSyncRoot_1(RuntimeObject * value)
	{
		___taskListSyncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&___taskListSyncRoot_1), value);
	}

	inline static int32_t get_offset_of_taskList_2() { return static_cast<int32_t>(offsetof(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578, ___taskList_2)); }
	inline Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 * get_taskList_2() const { return ___taskList_2; }
	inline Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 ** get_address_of_taskList_2() { return &___taskList_2; }
	inline void set_taskList_2(Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 * value)
	{
		___taskList_2 = value;
		Il2CppCodeGenWriteBarrier((&___taskList_2), value);
	}

	inline static int32_t get_offset_of_delayedTaskList_3() { return static_cast<int32_t>(offsetof(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578, ___delayedTaskList_3)); }
	inline Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 * get_delayedTaskList_3() const { return ___delayedTaskList_3; }
	inline Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 ** get_address_of_delayedTaskList_3() { return &___delayedTaskList_3; }
	inline void set_delayedTaskList_3(Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 * value)
	{
		___delayedTaskList_3 = value;
		Il2CppCodeGenWriteBarrier((&___delayedTaskList_3), value);
	}

	inline static int32_t get_offset_of_dataEvent_4() { return static_cast<int32_t>(offsetof(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578, ___dataEvent_4)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_dataEvent_4() const { return ___dataEvent_4; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_dataEvent_4() { return &___dataEvent_4; }
	inline void set_dataEvent_4(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___dataEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___dataEvent_4), value);
	}

	inline static int32_t get_offset_of_AllowAccessLimitationChecks_5() { return static_cast<int32_t>(offsetof(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578, ___AllowAccessLimitationChecks_5)); }
	inline bool get_AllowAccessLimitationChecks_5() const { return ___AllowAccessLimitationChecks_5; }
	inline bool* get_address_of_AllowAccessLimitationChecks_5() { return &___AllowAccessLimitationChecks_5; }
	inline void set_AllowAccessLimitationChecks_5(bool value)
	{
		___AllowAccessLimitationChecks_5 = value;
	}

	inline static int32_t get_offset_of_TaskSortingSystem_6() { return static_cast<int32_t>(offsetof(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578, ___TaskSortingSystem_6)); }
	inline int32_t get_TaskSortingSystem_6() const { return ___TaskSortingSystem_6; }
	inline int32_t* get_address_of_TaskSortingSystem_6() { return &___TaskSortingSystem_6; }
	inline void set_TaskSortingSystem_6(int32_t value)
	{
		___TaskSortingSystem_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHERBASE_TB856AF22CD8042F30BC3EA0B35779BBF104A4578_H
#ifndef SWITCHTO_T546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89_H
#define SWITCHTO_T546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.SwitchTo
struct  SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89  : public RuntimeObject
{
public:
	// UnityThreading.SwitchTo/TargetType UnityThreading.SwitchTo::<Target>k__BackingField
	int32_t ___U3CTargetU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CTargetU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89, ___U3CTargetU3Ek__BackingField_0)); }
	inline int32_t get_U3CTargetU3Ek__BackingField_0() const { return ___U3CTargetU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CTargetU3Ek__BackingField_0() { return &___U3CTargetU3Ek__BackingField_0; }
	inline void set_U3CTargetU3Ek__BackingField_0(int32_t value)
	{
		___U3CTargetU3Ek__BackingField_0 = value;
	}
};

struct SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89_StaticFields
{
public:
	// UnityThreading.SwitchTo UnityThreading.SwitchTo::MainThread
	SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89 * ___MainThread_1;
	// UnityThreading.SwitchTo UnityThreading.SwitchTo::Thread
	SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89 * ___Thread_2;

public:
	inline static int32_t get_offset_of_MainThread_1() { return static_cast<int32_t>(offsetof(SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89_StaticFields, ___MainThread_1)); }
	inline SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89 * get_MainThread_1() const { return ___MainThread_1; }
	inline SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89 ** get_address_of_MainThread_1() { return &___MainThread_1; }
	inline void set_MainThread_1(SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89 * value)
	{
		___MainThread_1 = value;
		Il2CppCodeGenWriteBarrier((&___MainThread_1), value);
	}

	inline static int32_t get_offset_of_Thread_2() { return static_cast<int32_t>(offsetof(SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89_StaticFields, ___Thread_2)); }
	inline SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89 * get_Thread_2() const { return ___Thread_2; }
	inline SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89 ** get_address_of_Thread_2() { return &___Thread_2; }
	inline void set_Thread_2(SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89 * value)
	{
		___Thread_2 = value;
		Il2CppCodeGenWriteBarrier((&___Thread_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCHTO_T546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89_H
#ifndef AGENTCONFIGURATION_TB707ADEB3898212CA6B3C134B11AE9E9F440A8EC_H
#define AGENTCONFIGURATION_TB707ADEB3898212CA6B3C134B11AE9E9F440A8EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Agents.Data.AgentConfiguration
struct  AgentConfiguration_tB707ADEB3898212CA6B3C134B11AE9E9F440A8EC  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String Pathfinding.Agents.Data.AgentConfiguration::agentName
	String_t* ___agentName_4;
	// System.String Pathfinding.Agents.Data.AgentConfiguration::agentDescription
	String_t* ___agentDescription_5;
	// Pathfinding.Agents.Agent Pathfinding.Agents.Data.AgentConfiguration::agentPrefab
	Agent_t0C7954C99BD6D63C1824532878279573DE904927 * ___agentPrefab_6;

public:
	inline static int32_t get_offset_of_agentName_4() { return static_cast<int32_t>(offsetof(AgentConfiguration_tB707ADEB3898212CA6B3C134B11AE9E9F440A8EC, ___agentName_4)); }
	inline String_t* get_agentName_4() const { return ___agentName_4; }
	inline String_t** get_address_of_agentName_4() { return &___agentName_4; }
	inline void set_agentName_4(String_t* value)
	{
		___agentName_4 = value;
		Il2CppCodeGenWriteBarrier((&___agentName_4), value);
	}

	inline static int32_t get_offset_of_agentDescription_5() { return static_cast<int32_t>(offsetof(AgentConfiguration_tB707ADEB3898212CA6B3C134B11AE9E9F440A8EC, ___agentDescription_5)); }
	inline String_t* get_agentDescription_5() const { return ___agentDescription_5; }
	inline String_t** get_address_of_agentDescription_5() { return &___agentDescription_5; }
	inline void set_agentDescription_5(String_t* value)
	{
		___agentDescription_5 = value;
		Il2CppCodeGenWriteBarrier((&___agentDescription_5), value);
	}

	inline static int32_t get_offset_of_agentPrefab_6() { return static_cast<int32_t>(offsetof(AgentConfiguration_tB707ADEB3898212CA6B3C134B11AE9E9F440A8EC, ___agentPrefab_6)); }
	inline Agent_t0C7954C99BD6D63C1824532878279573DE904927 * get_agentPrefab_6() const { return ___agentPrefab_6; }
	inline Agent_t0C7954C99BD6D63C1824532878279573DE904927 ** get_address_of_agentPrefab_6() { return &___agentPrefab_6; }
	inline void set_agentPrefab_6(Agent_t0C7954C99BD6D63C1824532878279573DE904927 * value)
	{
		___agentPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___agentPrefab_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGENTCONFIGURATION_TB707ADEB3898212CA6B3C134B11AE9E9F440A8EC_H
#ifndef TOWERLEVELDATA_TFAF65E7202275569B6848DFB6FA816701E6AB2C9_H
#define TOWERLEVELDATA_TFAF65E7202275569B6848DFB6FA816701E6AB2C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Towers.Data.TowerLevelData
struct  TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String Towers.Data.TowerLevelData::description
	String_t* ___description_4;
	// System.String Towers.Data.TowerLevelData::upgradeDescription
	String_t* ___upgradeDescription_5;
	// System.Int32 Towers.Data.TowerLevelData::cost
	int32_t ___cost_6;
	// System.Int32 Towers.Data.TowerLevelData::sell
	int32_t ___sell_7;
	// System.Int32 Towers.Data.TowerLevelData::maxHealth
	int32_t ___maxHealth_8;
	// System.Int32 Towers.Data.TowerLevelData::startingHealth
	int32_t ___startingHealth_9;
	// UnityEngine.Sprite Towers.Data.TowerLevelData::icon
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___icon_10;

public:
	inline static int32_t get_offset_of_description_4() { return static_cast<int32_t>(offsetof(TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9, ___description_4)); }
	inline String_t* get_description_4() const { return ___description_4; }
	inline String_t** get_address_of_description_4() { return &___description_4; }
	inline void set_description_4(String_t* value)
	{
		___description_4 = value;
		Il2CppCodeGenWriteBarrier((&___description_4), value);
	}

	inline static int32_t get_offset_of_upgradeDescription_5() { return static_cast<int32_t>(offsetof(TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9, ___upgradeDescription_5)); }
	inline String_t* get_upgradeDescription_5() const { return ___upgradeDescription_5; }
	inline String_t** get_address_of_upgradeDescription_5() { return &___upgradeDescription_5; }
	inline void set_upgradeDescription_5(String_t* value)
	{
		___upgradeDescription_5 = value;
		Il2CppCodeGenWriteBarrier((&___upgradeDescription_5), value);
	}

	inline static int32_t get_offset_of_cost_6() { return static_cast<int32_t>(offsetof(TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9, ___cost_6)); }
	inline int32_t get_cost_6() const { return ___cost_6; }
	inline int32_t* get_address_of_cost_6() { return &___cost_6; }
	inline void set_cost_6(int32_t value)
	{
		___cost_6 = value;
	}

	inline static int32_t get_offset_of_sell_7() { return static_cast<int32_t>(offsetof(TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9, ___sell_7)); }
	inline int32_t get_sell_7() const { return ___sell_7; }
	inline int32_t* get_address_of_sell_7() { return &___sell_7; }
	inline void set_sell_7(int32_t value)
	{
		___sell_7 = value;
	}

	inline static int32_t get_offset_of_maxHealth_8() { return static_cast<int32_t>(offsetof(TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9, ___maxHealth_8)); }
	inline int32_t get_maxHealth_8() const { return ___maxHealth_8; }
	inline int32_t* get_address_of_maxHealth_8() { return &___maxHealth_8; }
	inline void set_maxHealth_8(int32_t value)
	{
		___maxHealth_8 = value;
	}

	inline static int32_t get_offset_of_startingHealth_9() { return static_cast<int32_t>(offsetof(TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9, ___startingHealth_9)); }
	inline int32_t get_startingHealth_9() const { return ___startingHealth_9; }
	inline int32_t* get_address_of_startingHealth_9() { return &___startingHealth_9; }
	inline void set_startingHealth_9(int32_t value)
	{
		___startingHealth_9 = value;
	}

	inline static int32_t get_offset_of_icon_10() { return static_cast<int32_t>(offsetof(TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9, ___icon_10)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_icon_10() const { return ___icon_10; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_icon_10() { return &___icon_10; }
	inline void set_icon_10(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___icon_10 = value;
		Il2CppCodeGenWriteBarrier((&___icon_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERLEVELDATA_TFAF65E7202275569B6848DFB6FA816701E6AB2C9_H
#ifndef TOWERLIBRARY_T6E601F3B2DB60D4823E7FD85CDBEEE11896F9A7C_H
#define TOWERLIBRARY_T6E601F3B2DB60D4823E7FD85CDBEEE11896F9A7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Towers.Data.TowerLibrary
struct  TowerLibrary_t6E601F3B2DB60D4823E7FD85CDBEEE11896F9A7C  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<Towers.Tower> Towers.Data.TowerLibrary::configurations
	List_1_tF9EC1C6B2834B01EDC824378E295B95026E2F32F * ___configurations_4;
	// System.Collections.Generic.Dictionary`2<System.String,Towers.Tower> Towers.Data.TowerLibrary::m_ConfigurationDictionary
	Dictionary_2_tC007A39DEED395F16A0D5539A314506782DF5092 * ___m_ConfigurationDictionary_5;

public:
	inline static int32_t get_offset_of_configurations_4() { return static_cast<int32_t>(offsetof(TowerLibrary_t6E601F3B2DB60D4823E7FD85CDBEEE11896F9A7C, ___configurations_4)); }
	inline List_1_tF9EC1C6B2834B01EDC824378E295B95026E2F32F * get_configurations_4() const { return ___configurations_4; }
	inline List_1_tF9EC1C6B2834B01EDC824378E295B95026E2F32F ** get_address_of_configurations_4() { return &___configurations_4; }
	inline void set_configurations_4(List_1_tF9EC1C6B2834B01EDC824378E295B95026E2F32F * value)
	{
		___configurations_4 = value;
		Il2CppCodeGenWriteBarrier((&___configurations_4), value);
	}

	inline static int32_t get_offset_of_m_ConfigurationDictionary_5() { return static_cast<int32_t>(offsetof(TowerLibrary_t6E601F3B2DB60D4823E7FD85CDBEEE11896F9A7C, ___m_ConfigurationDictionary_5)); }
	inline Dictionary_2_tC007A39DEED395F16A0D5539A314506782DF5092 * get_m_ConfigurationDictionary_5() const { return ___m_ConfigurationDictionary_5; }
	inline Dictionary_2_tC007A39DEED395F16A0D5539A314506782DF5092 ** get_address_of_m_ConfigurationDictionary_5() { return &___m_ConfigurationDictionary_5; }
	inline void set_m_ConfigurationDictionary_5(Dictionary_2_tC007A39DEED395F16A0D5539A314506782DF5092 * value)
	{
		___m_ConfigurationDictionary_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConfigurationDictionary_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERLIBRARY_T6E601F3B2DB60D4823E7FD85CDBEEE11896F9A7C_H
#ifndef UIPOINTER_T0FC948F506BE29990B0FF40D59141947F4B0EF38_H
#define UIPOINTER_T0FC948F506BE29990B0FF40D59141947F4B0EF38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Towers.UI.Builder/UIPointer
struct  UIPointer_t0FC948F506BE29990B0FF40D59141947F4B0EF38 
{
public:
	// Core.Input.PointerInfo Towers.UI.Builder/UIPointer::pointer
	PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB * ___pointer_0;
	// UnityEngine.Ray Towers.UI.Builder/UIPointer::ray
	Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray_1;
	// System.Nullable`1<UnityEngine.RaycastHit> Towers.UI.Builder/UIPointer::raycast
	Nullable_1_t6224B732028631256932FA4C4176E10D171EFE29  ___raycast_2;
	// System.Boolean Towers.UI.Builder/UIPointer::overUI
	bool ___overUI_3;

public:
	inline static int32_t get_offset_of_pointer_0() { return static_cast<int32_t>(offsetof(UIPointer_t0FC948F506BE29990B0FF40D59141947F4B0EF38, ___pointer_0)); }
	inline PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB * get_pointer_0() const { return ___pointer_0; }
	inline PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB ** get_address_of_pointer_0() { return &___pointer_0; }
	inline void set_pointer_0(PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB * value)
	{
		___pointer_0 = value;
		Il2CppCodeGenWriteBarrier((&___pointer_0), value);
	}

	inline static int32_t get_offset_of_ray_1() { return static_cast<int32_t>(offsetof(UIPointer_t0FC948F506BE29990B0FF40D59141947F4B0EF38, ___ray_1)); }
	inline Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  get_ray_1() const { return ___ray_1; }
	inline Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * get_address_of_ray_1() { return &___ray_1; }
	inline void set_ray_1(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  value)
	{
		___ray_1 = value;
	}

	inline static int32_t get_offset_of_raycast_2() { return static_cast<int32_t>(offsetof(UIPointer_t0FC948F506BE29990B0FF40D59141947F4B0EF38, ___raycast_2)); }
	inline Nullable_1_t6224B732028631256932FA4C4176E10D171EFE29  get_raycast_2() const { return ___raycast_2; }
	inline Nullable_1_t6224B732028631256932FA4C4176E10D171EFE29 * get_address_of_raycast_2() { return &___raycast_2; }
	inline void set_raycast_2(Nullable_1_t6224B732028631256932FA4C4176E10D171EFE29  value)
	{
		___raycast_2 = value;
	}

	inline static int32_t get_offset_of_overUI_3() { return static_cast<int32_t>(offsetof(UIPointer_t0FC948F506BE29990B0FF40D59141947F4B0EF38, ___overUI_3)); }
	inline bool get_overUI_3() const { return ___overUI_3; }
	inline bool* get_address_of_overUI_3() { return &___overUI_3; }
	inline void set_overUI_3(bool value)
	{
		___overUI_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Towers.UI.Builder/UIPointer
struct UIPointer_t0FC948F506BE29990B0FF40D59141947F4B0EF38_marshaled_pinvoke
{
	PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB * ___pointer_0;
	Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray_1;
	Nullable_1_t6224B732028631256932FA4C4176E10D171EFE29  ___raycast_2;
	int32_t ___overUI_3;
};
// Native definition for COM marshalling of Towers.UI.Builder/UIPointer
struct UIPointer_t0FC948F506BE29990B0FF40D59141947F4B0EF38_marshaled_com
{
	PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB * ___pointer_0;
	Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray_1;
	Nullable_1_t6224B732028631256932FA4C4176E10D171EFE29  ___raycast_2;
	int32_t ___overUI_3;
};
#endif // UIPOINTER_T0FC948F506BE29990B0FF40D59141947F4B0EF38_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef DISPATCHER_TD5EE49BC8AEFD030FF77F95C8A029588E07F0297_H
#define DISPATCHER_TD5EE49BC8AEFD030FF77F95C8A029588E07F0297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.Dispatcher
struct  Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297  : public DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578
{
public:

public:
};

struct Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297_StaticFields
{
public:
	// UnityThreading.Dispatcher UnityThreading.Dispatcher::mainDispatcher
	Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 * ___mainDispatcher_9;

public:
	inline static int32_t get_offset_of_mainDispatcher_9() { return static_cast<int32_t>(offsetof(Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297_StaticFields, ___mainDispatcher_9)); }
	inline Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 * get_mainDispatcher_9() const { return ___mainDispatcher_9; }
	inline Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 ** get_address_of_mainDispatcher_9() { return &___mainDispatcher_9; }
	inline void set_mainDispatcher_9(Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 * value)
	{
		___mainDispatcher_9 = value;
		Il2CppCodeGenWriteBarrier((&___mainDispatcher_9), value);
	}
};

struct Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297_ThreadStaticFields
{
public:
	// UnityThreading.Task UnityThreading.Dispatcher::currentTask
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * ___currentTask_7;
	// UnityThreading.Dispatcher UnityThreading.Dispatcher::currentDispatcher
	Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 * ___currentDispatcher_8;

public:
	inline static int32_t get_offset_of_currentTask_7() { return static_cast<int32_t>(offsetof(Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297_ThreadStaticFields, ___currentTask_7)); }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * get_currentTask_7() const { return ___currentTask_7; }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F ** get_address_of_currentTask_7() { return &___currentTask_7; }
	inline void set_currentTask_7(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * value)
	{
		___currentTask_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentTask_7), value);
	}

	inline static int32_t get_offset_of_currentDispatcher_8() { return static_cast<int32_t>(offsetof(Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297_ThreadStaticFields, ___currentDispatcher_8)); }
	inline Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 * get_currentDispatcher_8() const { return ___currentDispatcher_8; }
	inline Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 ** get_address_of_currentDispatcher_8() { return &___currentDispatcher_8; }
	inline void set_currentDispatcher_8(Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 * value)
	{
		___currentDispatcher_8 = value;
		Il2CppCodeGenWriteBarrier((&___currentDispatcher_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHER_TD5EE49BC8AEFD030FF77F95C8A029588E07F0297_H
#ifndef NULLDISPATCHER_TE297A13D35B57D87B3AFA60E38331CBEF62B73F5_H
#define NULLDISPATCHER_TE297A13D35B57D87B3AFA60E38331CBEF62B73F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.NullDispatcher
struct  NullDispatcher_tE297A13D35B57D87B3AFA60E38331CBEF62B73F5  : public DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578
{
public:

public:
};

struct NullDispatcher_tE297A13D35B57D87B3AFA60E38331CBEF62B73F5_StaticFields
{
public:
	// UnityThreading.NullDispatcher UnityThreading.NullDispatcher::Null
	NullDispatcher_tE297A13D35B57D87B3AFA60E38331CBEF62B73F5 * ___Null_7;

public:
	inline static int32_t get_offset_of_Null_7() { return static_cast<int32_t>(offsetof(NullDispatcher_tE297A13D35B57D87B3AFA60E38331CBEF62B73F5_StaticFields, ___Null_7)); }
	inline NullDispatcher_tE297A13D35B57D87B3AFA60E38331CBEF62B73F5 * get_Null_7() const { return ___Null_7; }
	inline NullDispatcher_tE297A13D35B57D87B3AFA60E38331CBEF62B73F5 ** get_address_of_Null_7() { return &___Null_7; }
	inline void set_Null_7(NullDispatcher_tE297A13D35B57D87B3AFA60E38331CBEF62B73F5 * value)
	{
		___Null_7 = value;
		Il2CppCodeGenWriteBarrier((&___Null_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLDISPATCHER_TE297A13D35B57D87B3AFA60E38331CBEF62B73F5_H
#ifndef TASKENDEDEVENTHANDLER_T00BAD98C5606F4CC601A12E95E473B7B19B4C611_H
#define TASKENDEDEVENTHANDLER_T00BAD98C5606F4CC601A12E95E473B7B19B4C611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskEndedEventHandler
struct  TaskEndedEventHandler_t00BAD98C5606F4CC601A12E95E473B7B19B4C611  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKENDEDEVENTHANDLER_T00BAD98C5606F4CC601A12E95E473B7B19B4C611_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef CAMERAINITIALSTATE_T7A8610673FEC75147BAD732A6A4B64B1B3AB917C_H
#define CAMERAINITIALSTATE_T7A8610673FEC75147BAD732A6A4B64B1B3AB917C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Camera.CameraInitialState
struct  CameraInitialState_t7A8610673FEC75147BAD732A6A4B64B1B3AB917C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Core.Camera.CameraInitialState/StartZoomMode Core.Camera.CameraInitialState::startZoomMode
	int32_t ___startZoomMode_4;
	// UnityEngine.Transform Core.Camera.CameraInitialState::initialLookAt
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___initialLookAt_5;

public:
	inline static int32_t get_offset_of_startZoomMode_4() { return static_cast<int32_t>(offsetof(CameraInitialState_t7A8610673FEC75147BAD732A6A4B64B1B3AB917C, ___startZoomMode_4)); }
	inline int32_t get_startZoomMode_4() const { return ___startZoomMode_4; }
	inline int32_t* get_address_of_startZoomMode_4() { return &___startZoomMode_4; }
	inline void set_startZoomMode_4(int32_t value)
	{
		___startZoomMode_4 = value;
	}

	inline static int32_t get_offset_of_initialLookAt_5() { return static_cast<int32_t>(offsetof(CameraInitialState_t7A8610673FEC75147BAD732A6A4B64B1B3AB917C, ___initialLookAt_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_initialLookAt_5() const { return ___initialLookAt_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_initialLookAt_5() { return &___initialLookAt_5; }
	inline void set_initialLookAt_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___initialLookAt_5 = value;
		Il2CppCodeGenWriteBarrier((&___initialLookAt_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAINITIALSTATE_T7A8610673FEC75147BAD732A6A4B64B1B3AB917C_H
#ifndef CAMERARIG_T283698A4EB169762A3342D3E48419021467FB3D4_H
#define CAMERARIG_T283698A4EB169762A3342D3E48419021467FB3D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Camera.CameraRig
struct  CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Core.Camera.CameraRig::lookDampFactor
	float ___lookDampFactor_4;
	// System.Single Core.Camera.CameraRig::movementDampFactor
	float ___movementDampFactor_5;
	// System.Single Core.Camera.CameraRig::nearestZoom
	float ___nearestZoom_6;
	// System.Single Core.Camera.CameraRig::furthestZoom
	float ___furthestZoom_7;
	// System.Single Core.Camera.CameraRig::maxZoom
	float ___maxZoom_8;
	// System.Single Core.Camera.CameraRig::zoomLogFactor
	float ___zoomLogFactor_9;
	// System.Single Core.Camera.CameraRig::zoomRecoverSpeed
	float ___zoomRecoverSpeed_10;
	// System.Single Core.Camera.CameraRig::floorY
	float ___floorY_11;
	// UnityEngine.Transform Core.Camera.CameraRig::zoomedCamAngle
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___zoomedCamAngle_12;
	// UnityEngine.Rect Core.Camera.CameraRig::mapSize
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___mapSize_13;
	// System.Boolean Core.Camera.CameraRig::springyZoom
	bool ___springyZoom_14;
	// UnityEngine.Vector3 Core.Camera.CameraRig::m_CurrentLookVelocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_CurrentLookVelocity_15;
	// UnityEngine.Quaternion Core.Camera.CameraRig::m_MinZoomRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_MinZoomRotation_16;
	// UnityEngine.Quaternion Core.Camera.CameraRig::m_MaxZoomRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_MaxZoomRotation_17;
	// UnityEngine.Vector3 Core.Camera.CameraRig::m_CurrentCamVelocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_CurrentCamVelocity_18;
	// UnityEngine.Plane Core.Camera.CameraRig::m_FloorPlane
	Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  ___m_FloorPlane_19;
	// UnityEngine.Vector3 Core.Camera.CameraRig::<lookPosition>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3ClookPositionU3Ek__BackingField_20;
	// UnityEngine.Vector3 Core.Camera.CameraRig::<currentLookPosition>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CcurrentLookPositionU3Ek__BackingField_21;
	// UnityEngine.Vector3 Core.Camera.CameraRig::<cameraPosition>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CcameraPositionU3Ek__BackingField_22;
	// UnityEngine.Rect Core.Camera.CameraRig::<lookBounds>k__BackingField
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___U3ClookBoundsU3Ek__BackingField_23;
	// System.Single Core.Camera.CameraRig::<zoomDist>k__BackingField
	float ___U3CzoomDistU3Ek__BackingField_24;
	// System.Single Core.Camera.CameraRig::<rawZoomDist>k__BackingField
	float ___U3CrawZoomDistU3Ek__BackingField_25;
	// UnityEngine.GameObject Core.Camera.CameraRig::<trackingObject>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CtrackingObjectU3Ek__BackingField_26;
	// UnityEngine.Camera Core.Camera.CameraRig::<cachedCamera>k__BackingField
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___U3CcachedCameraU3Ek__BackingField_27;

public:
	inline static int32_t get_offset_of_lookDampFactor_4() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___lookDampFactor_4)); }
	inline float get_lookDampFactor_4() const { return ___lookDampFactor_4; }
	inline float* get_address_of_lookDampFactor_4() { return &___lookDampFactor_4; }
	inline void set_lookDampFactor_4(float value)
	{
		___lookDampFactor_4 = value;
	}

	inline static int32_t get_offset_of_movementDampFactor_5() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___movementDampFactor_5)); }
	inline float get_movementDampFactor_5() const { return ___movementDampFactor_5; }
	inline float* get_address_of_movementDampFactor_5() { return &___movementDampFactor_5; }
	inline void set_movementDampFactor_5(float value)
	{
		___movementDampFactor_5 = value;
	}

	inline static int32_t get_offset_of_nearestZoom_6() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___nearestZoom_6)); }
	inline float get_nearestZoom_6() const { return ___nearestZoom_6; }
	inline float* get_address_of_nearestZoom_6() { return &___nearestZoom_6; }
	inline void set_nearestZoom_6(float value)
	{
		___nearestZoom_6 = value;
	}

	inline static int32_t get_offset_of_furthestZoom_7() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___furthestZoom_7)); }
	inline float get_furthestZoom_7() const { return ___furthestZoom_7; }
	inline float* get_address_of_furthestZoom_7() { return &___furthestZoom_7; }
	inline void set_furthestZoom_7(float value)
	{
		___furthestZoom_7 = value;
	}

	inline static int32_t get_offset_of_maxZoom_8() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___maxZoom_8)); }
	inline float get_maxZoom_8() const { return ___maxZoom_8; }
	inline float* get_address_of_maxZoom_8() { return &___maxZoom_8; }
	inline void set_maxZoom_8(float value)
	{
		___maxZoom_8 = value;
	}

	inline static int32_t get_offset_of_zoomLogFactor_9() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___zoomLogFactor_9)); }
	inline float get_zoomLogFactor_9() const { return ___zoomLogFactor_9; }
	inline float* get_address_of_zoomLogFactor_9() { return &___zoomLogFactor_9; }
	inline void set_zoomLogFactor_9(float value)
	{
		___zoomLogFactor_9 = value;
	}

	inline static int32_t get_offset_of_zoomRecoverSpeed_10() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___zoomRecoverSpeed_10)); }
	inline float get_zoomRecoverSpeed_10() const { return ___zoomRecoverSpeed_10; }
	inline float* get_address_of_zoomRecoverSpeed_10() { return &___zoomRecoverSpeed_10; }
	inline void set_zoomRecoverSpeed_10(float value)
	{
		___zoomRecoverSpeed_10 = value;
	}

	inline static int32_t get_offset_of_floorY_11() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___floorY_11)); }
	inline float get_floorY_11() const { return ___floorY_11; }
	inline float* get_address_of_floorY_11() { return &___floorY_11; }
	inline void set_floorY_11(float value)
	{
		___floorY_11 = value;
	}

	inline static int32_t get_offset_of_zoomedCamAngle_12() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___zoomedCamAngle_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_zoomedCamAngle_12() const { return ___zoomedCamAngle_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_zoomedCamAngle_12() { return &___zoomedCamAngle_12; }
	inline void set_zoomedCamAngle_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___zoomedCamAngle_12 = value;
		Il2CppCodeGenWriteBarrier((&___zoomedCamAngle_12), value);
	}

	inline static int32_t get_offset_of_mapSize_13() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___mapSize_13)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_mapSize_13() const { return ___mapSize_13; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_mapSize_13() { return &___mapSize_13; }
	inline void set_mapSize_13(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___mapSize_13 = value;
	}

	inline static int32_t get_offset_of_springyZoom_14() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___springyZoom_14)); }
	inline bool get_springyZoom_14() const { return ___springyZoom_14; }
	inline bool* get_address_of_springyZoom_14() { return &___springyZoom_14; }
	inline void set_springyZoom_14(bool value)
	{
		___springyZoom_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentLookVelocity_15() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___m_CurrentLookVelocity_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_CurrentLookVelocity_15() const { return ___m_CurrentLookVelocity_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_CurrentLookVelocity_15() { return &___m_CurrentLookVelocity_15; }
	inline void set_m_CurrentLookVelocity_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_CurrentLookVelocity_15 = value;
	}

	inline static int32_t get_offset_of_m_MinZoomRotation_16() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___m_MinZoomRotation_16)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_MinZoomRotation_16() const { return ___m_MinZoomRotation_16; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_MinZoomRotation_16() { return &___m_MinZoomRotation_16; }
	inline void set_m_MinZoomRotation_16(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_MinZoomRotation_16 = value;
	}

	inline static int32_t get_offset_of_m_MaxZoomRotation_17() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___m_MaxZoomRotation_17)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_MaxZoomRotation_17() const { return ___m_MaxZoomRotation_17; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_MaxZoomRotation_17() { return &___m_MaxZoomRotation_17; }
	inline void set_m_MaxZoomRotation_17(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_MaxZoomRotation_17 = value;
	}

	inline static int32_t get_offset_of_m_CurrentCamVelocity_18() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___m_CurrentCamVelocity_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_CurrentCamVelocity_18() const { return ___m_CurrentCamVelocity_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_CurrentCamVelocity_18() { return &___m_CurrentCamVelocity_18; }
	inline void set_m_CurrentCamVelocity_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_CurrentCamVelocity_18 = value;
	}

	inline static int32_t get_offset_of_m_FloorPlane_19() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___m_FloorPlane_19)); }
	inline Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  get_m_FloorPlane_19() const { return ___m_FloorPlane_19; }
	inline Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED * get_address_of_m_FloorPlane_19() { return &___m_FloorPlane_19; }
	inline void set_m_FloorPlane_19(Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  value)
	{
		___m_FloorPlane_19 = value;
	}

	inline static int32_t get_offset_of_U3ClookPositionU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___U3ClookPositionU3Ek__BackingField_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3ClookPositionU3Ek__BackingField_20() const { return ___U3ClookPositionU3Ek__BackingField_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3ClookPositionU3Ek__BackingField_20() { return &___U3ClookPositionU3Ek__BackingField_20; }
	inline void set_U3ClookPositionU3Ek__BackingField_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3ClookPositionU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentLookPositionU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___U3CcurrentLookPositionU3Ek__BackingField_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CcurrentLookPositionU3Ek__BackingField_21() const { return ___U3CcurrentLookPositionU3Ek__BackingField_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CcurrentLookPositionU3Ek__BackingField_21() { return &___U3CcurrentLookPositionU3Ek__BackingField_21; }
	inline void set_U3CcurrentLookPositionU3Ek__BackingField_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CcurrentLookPositionU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CcameraPositionU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___U3CcameraPositionU3Ek__BackingField_22)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CcameraPositionU3Ek__BackingField_22() const { return ___U3CcameraPositionU3Ek__BackingField_22; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CcameraPositionU3Ek__BackingField_22() { return &___U3CcameraPositionU3Ek__BackingField_22; }
	inline void set_U3CcameraPositionU3Ek__BackingField_22(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CcameraPositionU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3ClookBoundsU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___U3ClookBoundsU3Ek__BackingField_23)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_U3ClookBoundsU3Ek__BackingField_23() const { return ___U3ClookBoundsU3Ek__BackingField_23; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_U3ClookBoundsU3Ek__BackingField_23() { return &___U3ClookBoundsU3Ek__BackingField_23; }
	inline void set_U3ClookBoundsU3Ek__BackingField_23(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___U3ClookBoundsU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CzoomDistU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___U3CzoomDistU3Ek__BackingField_24)); }
	inline float get_U3CzoomDistU3Ek__BackingField_24() const { return ___U3CzoomDistU3Ek__BackingField_24; }
	inline float* get_address_of_U3CzoomDistU3Ek__BackingField_24() { return &___U3CzoomDistU3Ek__BackingField_24; }
	inline void set_U3CzoomDistU3Ek__BackingField_24(float value)
	{
		___U3CzoomDistU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CrawZoomDistU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___U3CrawZoomDistU3Ek__BackingField_25)); }
	inline float get_U3CrawZoomDistU3Ek__BackingField_25() const { return ___U3CrawZoomDistU3Ek__BackingField_25; }
	inline float* get_address_of_U3CrawZoomDistU3Ek__BackingField_25() { return &___U3CrawZoomDistU3Ek__BackingField_25; }
	inline void set_U3CrawZoomDistU3Ek__BackingField_25(float value)
	{
		___U3CrawZoomDistU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CtrackingObjectU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___U3CtrackingObjectU3Ek__BackingField_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CtrackingObjectU3Ek__BackingField_26() const { return ___U3CtrackingObjectU3Ek__BackingField_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CtrackingObjectU3Ek__BackingField_26() { return &___U3CtrackingObjectU3Ek__BackingField_26; }
	inline void set_U3CtrackingObjectU3Ek__BackingField_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CtrackingObjectU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtrackingObjectU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CcachedCameraU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4, ___U3CcachedCameraU3Ek__BackingField_27)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_U3CcachedCameraU3Ek__BackingField_27() const { return ___U3CcachedCameraU3Ek__BackingField_27; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_U3CcachedCameraU3Ek__BackingField_27() { return &___U3CcachedCameraU3Ek__BackingField_27; }
	inline void set_U3CcachedCameraU3Ek__BackingField_27(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___U3CcachedCameraU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcachedCameraU3Ek__BackingField_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERARIG_T283698A4EB169762A3342D3E48419021467FB3D4_H
#ifndef INPUTSCHEME_T78F140C9CD80326F0D459CE65156EF66E3218678_H
#define INPUTSCHEME_T78F140C9CD80326F0D459CE65156EF66E3218678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.InputScheme
struct  InputScheme_t78F140C9CD80326F0D459CE65156EF66E3218678  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSCHEME_T78F140C9CD80326F0D459CE65156EF66E3218678_H
#ifndef POOLABLE_TBBCA15FD94FB330CD5A30C65692500ED9BA65D2F_H
#define POOLABLE_TBBCA15FD94FB330CD5A30C65692500ED9BA65D2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Poolable
struct  Poolable_tBBCA15FD94FB330CD5A30C65692500ED9BA65D2F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Core.Utilities.Poolable::initialPoolCapacity
	int32_t ___initialPoolCapacity_4;
	// Core.Utilities.Pool`1<Core.Utilities.Poolable> Core.Utilities.Poolable::pool
	Pool_1_tA688FE7A8096EF8D7EADA8644CA3B87A9356A2D7 * ___pool_5;

public:
	inline static int32_t get_offset_of_initialPoolCapacity_4() { return static_cast<int32_t>(offsetof(Poolable_tBBCA15FD94FB330CD5A30C65692500ED9BA65D2F, ___initialPoolCapacity_4)); }
	inline int32_t get_initialPoolCapacity_4() const { return ___initialPoolCapacity_4; }
	inline int32_t* get_address_of_initialPoolCapacity_4() { return &___initialPoolCapacity_4; }
	inline void set_initialPoolCapacity_4(int32_t value)
	{
		___initialPoolCapacity_4 = value;
	}

	inline static int32_t get_offset_of_pool_5() { return static_cast<int32_t>(offsetof(Poolable_tBBCA15FD94FB330CD5A30C65692500ED9BA65D2F, ___pool_5)); }
	inline Pool_1_tA688FE7A8096EF8D7EADA8644CA3B87A9356A2D7 * get_pool_5() const { return ___pool_5; }
	inline Pool_1_tA688FE7A8096EF8D7EADA8644CA3B87A9356A2D7 ** get_address_of_pool_5() { return &___pool_5; }
	inline void set_pool_5(Pool_1_tA688FE7A8096EF8D7EADA8644CA3B87A9356A2D7 * value)
	{
		___pool_5 = value;
		Il2CppCodeGenWriteBarrier((&___pool_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLABLE_TBBCA15FD94FB330CD5A30C65692500ED9BA65D2F_H
#ifndef SINGLETON_1_TB8BC3FEE6CD31F56AF6C63704CA5A47016487BB1_H
#define SINGLETON_1_TB8BC3FEE6CD31F56AF6C63704CA5A47016487BB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<Economy.CurrencyManager>
struct  Singleton_1_tB8BC3FEE6CD31F56AF6C63704CA5A47016487BB1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_tB8BC3FEE6CD31F56AF6C63704CA5A47016487BB1_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_tB8BC3FEE6CD31F56AF6C63704CA5A47016487BB1_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_TB8BC3FEE6CD31F56AF6C63704CA5A47016487BB1_H
#ifndef SINGLETON_1_T676EF679D4D9890C1966EDB733B76F47314FB958_H
#define SINGLETON_1_T676EF679D4D9890C1966EDB733B76F47314FB958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<Towers.UI.Builder>
struct  Singleton_1_t676EF679D4D9890C1966EDB733B76F47314FB958  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t676EF679D4D9890C1966EDB733B76F47314FB958_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_t676EF679D4D9890C1966EDB733B76F47314FB958_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline Builder_tF7414C0E077162F0CE605E009B1013AD98732805 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline Builder_tF7414C0E077162F0CE605E009B1013AD98732805 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(Builder_tF7414C0E077162F0CE605E009B1013AD98732805 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T676EF679D4D9890C1966EDB733B76F47314FB958_H
#ifndef SINGLETON_1_T9E84971219B4F5C6D76B382E6432C7ED72BE346C_H
#define SINGLETON_1_T9E84971219B4F5C6D76B382E6432C7ED72BE346C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<Towers.UI.RadiusVisualizerController>
struct  Singleton_1_t9E84971219B4F5C6D76B382E6432C7ED72BE346C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t9E84971219B4F5C6D76B382E6432C7ED72BE346C_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	RadiusVisualizerController_tC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_t9E84971219B4F5C6D76B382E6432C7ED72BE346C_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline RadiusVisualizerController_tC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline RadiusVisualizerController_tC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(RadiusVisualizerController_tC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T9E84971219B4F5C6D76B382E6432C7ED72BE346C_H
#ifndef LIGHTNINGBOLTSCRIPT_T9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B_H
#define LIGHTNINGBOLTSCRIPT_T9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.LightningBolt.LightningBoltScript
struct  LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject DigitalRuby.LightningBolt.LightningBoltScript::StartObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___StartObject_4;
	// UnityEngine.Vector3 DigitalRuby.LightningBolt.LightningBoltScript::StartPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartPosition_5;
	// UnityEngine.GameObject DigitalRuby.LightningBolt.LightningBoltScript::EndObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___EndObject_6;
	// UnityEngine.Vector3 DigitalRuby.LightningBolt.LightningBoltScript::EndPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___EndPosition_7;
	// System.Int32 DigitalRuby.LightningBolt.LightningBoltScript::Generations
	int32_t ___Generations_8;
	// System.Single DigitalRuby.LightningBolt.LightningBoltScript::Duration
	float ___Duration_9;
	// System.Single DigitalRuby.LightningBolt.LightningBoltScript::timer
	float ___timer_10;
	// System.Single DigitalRuby.LightningBolt.LightningBoltScript::ChaosFactor
	float ___ChaosFactor_11;
	// System.Boolean DigitalRuby.LightningBolt.LightningBoltScript::ManualMode
	bool ___ManualMode_12;
	// System.Int32 DigitalRuby.LightningBolt.LightningBoltScript::Rows
	int32_t ___Rows_13;
	// System.Int32 DigitalRuby.LightningBolt.LightningBoltScript::Columns
	int32_t ___Columns_14;
	// DigitalRuby.LightningBolt.LightningBoltAnimationMode DigitalRuby.LightningBolt.LightningBoltScript::AnimationMode
	int32_t ___AnimationMode_15;
	// System.Random DigitalRuby.LightningBolt.LightningBoltScript::RandomGenerator
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ___RandomGenerator_16;
	// UnityEngine.LineRenderer DigitalRuby.LightningBolt.LightningBoltScript::lineRenderer
	LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * ___lineRenderer_17;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,UnityEngine.Vector3>> DigitalRuby.LightningBolt.LightningBoltScript::segments
	List_1_tB4C90384CD860CA4134CB5A40A69318313715E49 * ___segments_18;
	// System.Int32 DigitalRuby.LightningBolt.LightningBoltScript::startIndex
	int32_t ___startIndex_19;
	// UnityEngine.Vector2 DigitalRuby.LightningBolt.LightningBoltScript::size
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___size_20;
	// UnityEngine.Vector2[] DigitalRuby.LightningBolt.LightningBoltScript::offsets
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___offsets_21;
	// System.Int32 DigitalRuby.LightningBolt.LightningBoltScript::animationOffsetIndex
	int32_t ___animationOffsetIndex_22;
	// System.Int32 DigitalRuby.LightningBolt.LightningBoltScript::animationPingPongDirection
	int32_t ___animationPingPongDirection_23;
	// System.Boolean DigitalRuby.LightningBolt.LightningBoltScript::orthographic
	bool ___orthographic_24;

public:
	inline static int32_t get_offset_of_StartObject_4() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___StartObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_StartObject_4() const { return ___StartObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_StartObject_4() { return &___StartObject_4; }
	inline void set_StartObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___StartObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___StartObject_4), value);
	}

	inline static int32_t get_offset_of_StartPosition_5() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___StartPosition_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartPosition_5() const { return ___StartPosition_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartPosition_5() { return &___StartPosition_5; }
	inline void set_StartPosition_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartPosition_5 = value;
	}

	inline static int32_t get_offset_of_EndObject_6() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___EndObject_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_EndObject_6() const { return ___EndObject_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_EndObject_6() { return &___EndObject_6; }
	inline void set_EndObject_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___EndObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___EndObject_6), value);
	}

	inline static int32_t get_offset_of_EndPosition_7() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___EndPosition_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_EndPosition_7() const { return ___EndPosition_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_EndPosition_7() { return &___EndPosition_7; }
	inline void set_EndPosition_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___EndPosition_7 = value;
	}

	inline static int32_t get_offset_of_Generations_8() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___Generations_8)); }
	inline int32_t get_Generations_8() const { return ___Generations_8; }
	inline int32_t* get_address_of_Generations_8() { return &___Generations_8; }
	inline void set_Generations_8(int32_t value)
	{
		___Generations_8 = value;
	}

	inline static int32_t get_offset_of_Duration_9() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___Duration_9)); }
	inline float get_Duration_9() const { return ___Duration_9; }
	inline float* get_address_of_Duration_9() { return &___Duration_9; }
	inline void set_Duration_9(float value)
	{
		___Duration_9 = value;
	}

	inline static int32_t get_offset_of_timer_10() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___timer_10)); }
	inline float get_timer_10() const { return ___timer_10; }
	inline float* get_address_of_timer_10() { return &___timer_10; }
	inline void set_timer_10(float value)
	{
		___timer_10 = value;
	}

	inline static int32_t get_offset_of_ChaosFactor_11() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___ChaosFactor_11)); }
	inline float get_ChaosFactor_11() const { return ___ChaosFactor_11; }
	inline float* get_address_of_ChaosFactor_11() { return &___ChaosFactor_11; }
	inline void set_ChaosFactor_11(float value)
	{
		___ChaosFactor_11 = value;
	}

	inline static int32_t get_offset_of_ManualMode_12() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___ManualMode_12)); }
	inline bool get_ManualMode_12() const { return ___ManualMode_12; }
	inline bool* get_address_of_ManualMode_12() { return &___ManualMode_12; }
	inline void set_ManualMode_12(bool value)
	{
		___ManualMode_12 = value;
	}

	inline static int32_t get_offset_of_Rows_13() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___Rows_13)); }
	inline int32_t get_Rows_13() const { return ___Rows_13; }
	inline int32_t* get_address_of_Rows_13() { return &___Rows_13; }
	inline void set_Rows_13(int32_t value)
	{
		___Rows_13 = value;
	}

	inline static int32_t get_offset_of_Columns_14() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___Columns_14)); }
	inline int32_t get_Columns_14() const { return ___Columns_14; }
	inline int32_t* get_address_of_Columns_14() { return &___Columns_14; }
	inline void set_Columns_14(int32_t value)
	{
		___Columns_14 = value;
	}

	inline static int32_t get_offset_of_AnimationMode_15() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___AnimationMode_15)); }
	inline int32_t get_AnimationMode_15() const { return ___AnimationMode_15; }
	inline int32_t* get_address_of_AnimationMode_15() { return &___AnimationMode_15; }
	inline void set_AnimationMode_15(int32_t value)
	{
		___AnimationMode_15 = value;
	}

	inline static int32_t get_offset_of_RandomGenerator_16() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___RandomGenerator_16)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get_RandomGenerator_16() const { return ___RandomGenerator_16; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of_RandomGenerator_16() { return &___RandomGenerator_16; }
	inline void set_RandomGenerator_16(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		___RandomGenerator_16 = value;
		Il2CppCodeGenWriteBarrier((&___RandomGenerator_16), value);
	}

	inline static int32_t get_offset_of_lineRenderer_17() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___lineRenderer_17)); }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * get_lineRenderer_17() const { return ___lineRenderer_17; }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 ** get_address_of_lineRenderer_17() { return &___lineRenderer_17; }
	inline void set_lineRenderer_17(LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * value)
	{
		___lineRenderer_17 = value;
		Il2CppCodeGenWriteBarrier((&___lineRenderer_17), value);
	}

	inline static int32_t get_offset_of_segments_18() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___segments_18)); }
	inline List_1_tB4C90384CD860CA4134CB5A40A69318313715E49 * get_segments_18() const { return ___segments_18; }
	inline List_1_tB4C90384CD860CA4134CB5A40A69318313715E49 ** get_address_of_segments_18() { return &___segments_18; }
	inline void set_segments_18(List_1_tB4C90384CD860CA4134CB5A40A69318313715E49 * value)
	{
		___segments_18 = value;
		Il2CppCodeGenWriteBarrier((&___segments_18), value);
	}

	inline static int32_t get_offset_of_startIndex_19() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___startIndex_19)); }
	inline int32_t get_startIndex_19() const { return ___startIndex_19; }
	inline int32_t* get_address_of_startIndex_19() { return &___startIndex_19; }
	inline void set_startIndex_19(int32_t value)
	{
		___startIndex_19 = value;
	}

	inline static int32_t get_offset_of_size_20() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___size_20)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_size_20() const { return ___size_20; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_size_20() { return &___size_20; }
	inline void set_size_20(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___size_20 = value;
	}

	inline static int32_t get_offset_of_offsets_21() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___offsets_21)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_offsets_21() const { return ___offsets_21; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_offsets_21() { return &___offsets_21; }
	inline void set_offsets_21(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___offsets_21 = value;
		Il2CppCodeGenWriteBarrier((&___offsets_21), value);
	}

	inline static int32_t get_offset_of_animationOffsetIndex_22() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___animationOffsetIndex_22)); }
	inline int32_t get_animationOffsetIndex_22() const { return ___animationOffsetIndex_22; }
	inline int32_t* get_address_of_animationOffsetIndex_22() { return &___animationOffsetIndex_22; }
	inline void set_animationOffsetIndex_22(int32_t value)
	{
		___animationOffsetIndex_22 = value;
	}

	inline static int32_t get_offset_of_animationPingPongDirection_23() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___animationPingPongDirection_23)); }
	inline int32_t get_animationPingPongDirection_23() const { return ___animationPingPongDirection_23; }
	inline int32_t* get_address_of_animationPingPongDirection_23() { return &___animationPingPongDirection_23; }
	inline void set_animationPingPongDirection_23(int32_t value)
	{
		___animationPingPongDirection_23 = value;
	}

	inline static int32_t get_offset_of_orthographic_24() { return static_cast<int32_t>(offsetof(LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B, ___orthographic_24)); }
	inline bool get_orthographic_24() const { return ___orthographic_24; }
	inline bool* get_address_of_orthographic_24() { return &___orthographic_24; }
	inline void set_orthographic_24(bool value)
	{
		___orthographic_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTNINGBOLTSCRIPT_T9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B_H
#ifndef LOOTDROP_T3A4E9B0E2E4BB7E65EAF3C68E2DA76F1B142E8E6_H
#define LOOTDROP_T3A4E9B0E2E4BB7E65EAF3C68E2DA76F1B142E8E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Economy.LootDrop
struct  LootDrop_t3A4E9B0E2E4BB7E65EAF3C68E2DA76F1B142E8E6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Economy.LootDrop::lootDropped
	int32_t ___lootDropped_4;
	// Health Economy.LootDrop::health
	Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * ___health_5;

public:
	inline static int32_t get_offset_of_lootDropped_4() { return static_cast<int32_t>(offsetof(LootDrop_t3A4E9B0E2E4BB7E65EAF3C68E2DA76F1B142E8E6, ___lootDropped_4)); }
	inline int32_t get_lootDropped_4() const { return ___lootDropped_4; }
	inline int32_t* get_address_of_lootDropped_4() { return &___lootDropped_4; }
	inline void set_lootDropped_4(int32_t value)
	{
		___lootDropped_4 = value;
	}

	inline static int32_t get_offset_of_health_5() { return static_cast<int32_t>(offsetof(LootDrop_t3A4E9B0E2E4BB7E65EAF3C68E2DA76F1B142E8E6, ___health_5)); }
	inline Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * get_health_5() const { return ___health_5; }
	inline Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D ** get_address_of_health_5() { return &___health_5; }
	inline void set_health_5(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * value)
	{
		___health_5 = value;
		Il2CppCodeGenWriteBarrier((&___health_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOTDROP_T3A4E9B0E2E4BB7E65EAF3C68E2DA76F1B142E8E6_H
#ifndef CURRENCYUI_T651099D3DC0E8F1703B66575C0536EF4CF5A0E5D_H
#define CURRENCYUI_T651099D3DC0E8F1703B66575C0536EF4CF5A0E5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Economy.UI.CurrencyUI
struct  CurrencyUI_t651099D3DC0E8F1703B66575C0536EF4CF5A0E5D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Economy.UI.CurrencyUI::display
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___display_4;
	// System.String Economy.UI.CurrencyUI::currencySymbol
	String_t* ___currencySymbol_5;
	// Economy.Currency Economy.UI.CurrencyUI::m_Currency
	Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05 * ___m_Currency_6;

public:
	inline static int32_t get_offset_of_display_4() { return static_cast<int32_t>(offsetof(CurrencyUI_t651099D3DC0E8F1703B66575C0536EF4CF5A0E5D, ___display_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_display_4() const { return ___display_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_display_4() { return &___display_4; }
	inline void set_display_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___display_4 = value;
		Il2CppCodeGenWriteBarrier((&___display_4), value);
	}

	inline static int32_t get_offset_of_currencySymbol_5() { return static_cast<int32_t>(offsetof(CurrencyUI_t651099D3DC0E8F1703B66575C0536EF4CF5A0E5D, ___currencySymbol_5)); }
	inline String_t* get_currencySymbol_5() const { return ___currencySymbol_5; }
	inline String_t** get_address_of_currencySymbol_5() { return &___currencySymbol_5; }
	inline void set_currencySymbol_5(String_t* value)
	{
		___currencySymbol_5 = value;
		Il2CppCodeGenWriteBarrier((&___currencySymbol_5), value);
	}

	inline static int32_t get_offset_of_m_Currency_6() { return static_cast<int32_t>(offsetof(CurrencyUI_t651099D3DC0E8F1703B66575C0536EF4CF5A0E5D, ___m_Currency_6)); }
	inline Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05 * get_m_Currency_6() const { return ___m_Currency_6; }
	inline Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05 ** get_address_of_m_Currency_6() { return &___m_Currency_6; }
	inline void set_m_Currency_6(Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05 * value)
	{
		___m_Currency_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Currency_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENCYUI_T651099D3DC0E8F1703B66575C0536EF4CF5A0E5D_H
#ifndef HEALTH_TBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D_H
#define HEALTH_TBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Health
struct  Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Core.Health.SerializableIAlignmentProvider Health::alignment
	SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 * ___alignment_4;
	// Core.Health.IAlignmentProvider Health::alignmentProvider
	RuntimeObject* ___alignmentProvider_5;
	// System.Single Health::MaxHealth
	float ___MaxHealth_6;
	// System.Single Health::CurrentHealth
	float ___CurrentHealth_7;
	// System.Action`1<Health> Health::removed
	Action_1_t001094C4FAC15A904C20F73F81BD57C81EBC71C4 * ___removed_8;
	// System.Action Health::healthChanged
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___healthChanged_9;
	// System.Action Health::died
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___died_10;

public:
	inline static int32_t get_offset_of_alignment_4() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___alignment_4)); }
	inline SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 * get_alignment_4() const { return ___alignment_4; }
	inline SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 ** get_address_of_alignment_4() { return &___alignment_4; }
	inline void set_alignment_4(SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 * value)
	{
		___alignment_4 = value;
		Il2CppCodeGenWriteBarrier((&___alignment_4), value);
	}

	inline static int32_t get_offset_of_alignmentProvider_5() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___alignmentProvider_5)); }
	inline RuntimeObject* get_alignmentProvider_5() const { return ___alignmentProvider_5; }
	inline RuntimeObject** get_address_of_alignmentProvider_5() { return &___alignmentProvider_5; }
	inline void set_alignmentProvider_5(RuntimeObject* value)
	{
		___alignmentProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___alignmentProvider_5), value);
	}

	inline static int32_t get_offset_of_MaxHealth_6() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___MaxHealth_6)); }
	inline float get_MaxHealth_6() const { return ___MaxHealth_6; }
	inline float* get_address_of_MaxHealth_6() { return &___MaxHealth_6; }
	inline void set_MaxHealth_6(float value)
	{
		___MaxHealth_6 = value;
	}

	inline static int32_t get_offset_of_CurrentHealth_7() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___CurrentHealth_7)); }
	inline float get_CurrentHealth_7() const { return ___CurrentHealth_7; }
	inline float* get_address_of_CurrentHealth_7() { return &___CurrentHealth_7; }
	inline void set_CurrentHealth_7(float value)
	{
		___CurrentHealth_7 = value;
	}

	inline static int32_t get_offset_of_removed_8() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___removed_8)); }
	inline Action_1_t001094C4FAC15A904C20F73F81BD57C81EBC71C4 * get_removed_8() const { return ___removed_8; }
	inline Action_1_t001094C4FAC15A904C20F73F81BD57C81EBC71C4 ** get_address_of_removed_8() { return &___removed_8; }
	inline void set_removed_8(Action_1_t001094C4FAC15A904C20F73F81BD57C81EBC71C4 * value)
	{
		___removed_8 = value;
		Il2CppCodeGenWriteBarrier((&___removed_8), value);
	}

	inline static int32_t get_offset_of_healthChanged_9() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___healthChanged_9)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_healthChanged_9() const { return ___healthChanged_9; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_healthChanged_9() { return &___healthChanged_9; }
	inline void set_healthChanged_9(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___healthChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___healthChanged_9), value);
	}

	inline static int32_t get_offset_of_died_10() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___died_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_died_10() const { return ___died_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_died_10() { return &___died_10; }
	inline void set_died_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___died_10 = value;
		Il2CppCodeGenWriteBarrier((&___died_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEALTH_TBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D_H
#ifndef AREAMESHCREATOR_TE22D06EA3837CB117F16849084D4074A0A6F01D7_H
#define AREAMESHCREATOR_TE22D06EA3837CB117F16849084D4074A0A6F01D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.MeshCreator.AreaMeshCreator
struct  AreaMeshCreator_tE22D06EA3837CB117F16849084D4074A0A6F01D7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Pathfinding.MeshCreator.MeshObject Pathfinding.MeshCreator.AreaMeshCreator::meshObject
	MeshObject_t3BA586F00E133946864FFCB15DE54A59D4E5E2B2 * ___meshObject_4;
	// UnityEngine.Transform Pathfinding.MeshCreator.AreaMeshCreator::outSidePointsParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___outSidePointsParent_5;

public:
	inline static int32_t get_offset_of_meshObject_4() { return static_cast<int32_t>(offsetof(AreaMeshCreator_tE22D06EA3837CB117F16849084D4074A0A6F01D7, ___meshObject_4)); }
	inline MeshObject_t3BA586F00E133946864FFCB15DE54A59D4E5E2B2 * get_meshObject_4() const { return ___meshObject_4; }
	inline MeshObject_t3BA586F00E133946864FFCB15DE54A59D4E5E2B2 ** get_address_of_meshObject_4() { return &___meshObject_4; }
	inline void set_meshObject_4(MeshObject_t3BA586F00E133946864FFCB15DE54A59D4E5E2B2 * value)
	{
		___meshObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___meshObject_4), value);
	}

	inline static int32_t get_offset_of_outSidePointsParent_5() { return static_cast<int32_t>(offsetof(AreaMeshCreator_tE22D06EA3837CB117F16849084D4074A0A6F01D7, ___outSidePointsParent_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_outSidePointsParent_5() const { return ___outSidePointsParent_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_outSidePointsParent_5() { return &___outSidePointsParent_5; }
	inline void set_outSidePointsParent_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___outSidePointsParent_5 = value;
		Il2CppCodeGenWriteBarrier((&___outSidePointsParent_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AREAMESHCREATOR_TE22D06EA3837CB117F16849084D4074A0A6F01D7_H
#ifndef NODE_TB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7_H
#define NODE_TB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Nodes.Node
struct  Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Pathfinding.MeshCreator.AreaMeshCreator Pathfinding.Nodes.Node::areaMesh
	AreaMeshCreator_tE22D06EA3837CB117F16849084D4074A0A6F01D7 * ___areaMesh_4;
	// System.Int32 Pathfinding.Nodes.Node::weight
	int32_t ___weight_5;

public:
	inline static int32_t get_offset_of_areaMesh_4() { return static_cast<int32_t>(offsetof(Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7, ___areaMesh_4)); }
	inline AreaMeshCreator_tE22D06EA3837CB117F16849084D4074A0A6F01D7 * get_areaMesh_4() const { return ___areaMesh_4; }
	inline AreaMeshCreator_tE22D06EA3837CB117F16849084D4074A0A6F01D7 ** get_address_of_areaMesh_4() { return &___areaMesh_4; }
	inline void set_areaMesh_4(AreaMeshCreator_tE22D06EA3837CB117F16849084D4074A0A6F01D7 * value)
	{
		___areaMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___areaMesh_4), value);
	}

	inline static int32_t get_offset_of_weight_5() { return static_cast<int32_t>(offsetof(Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7, ___weight_5)); }
	inline int32_t get_weight_5() const { return ___weight_5; }
	inline int32_t* get_address_of_weight_5() { return &___weight_5; }
	inline void set_weight_5(int32_t value)
	{
		___weight_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODE_TB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7_H
#ifndef NODESELECTOR_TFEA86AE0908D36229D2BBA57FDF7AEDEC84304D9_H
#define NODESELECTOR_TFEA86AE0908D36229D2BBA57FDF7AEDEC84304D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Nodes.NodeSelector
struct  NodeSelector_tFEA86AE0908D36229D2BBA57FDF7AEDEC84304D9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<Pathfinding.Nodes.Node> Pathfinding.Nodes.NodeSelector::linkedNodes
	List_1_t7306888B53570444822982D6E369D7C947D755BB * ___linkedNodes_4;

public:
	inline static int32_t get_offset_of_linkedNodes_4() { return static_cast<int32_t>(offsetof(NodeSelector_tFEA86AE0908D36229D2BBA57FDF7AEDEC84304D9, ___linkedNodes_4)); }
	inline List_1_t7306888B53570444822982D6E369D7C947D755BB * get_linkedNodes_4() const { return ___linkedNodes_4; }
	inline List_1_t7306888B53570444822982D6E369D7C947D755BB ** get_address_of_linkedNodes_4() { return &___linkedNodes_4; }
	inline void set_linkedNodes_4(List_1_t7306888B53570444822982D6E369D7C947D755BB * value)
	{
		___linkedNodes_4 = value;
		Il2CppCodeGenWriteBarrier((&___linkedNodes_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODESELECTOR_TFEA86AE0908D36229D2BBA57FDF7AEDEC84304D9_H
#ifndef OBJECTPLACEMENTGRID_TACB77AFD3305F152CC7D3C321E439CC41815D91E_H
#define OBJECTPLACEMENTGRID_TACB77AFD3305F152CC7D3C321E439CC41815D91E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Placement.ObjectPlacementGrid
struct  ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Placement.PlacementTile Placement.ObjectPlacementGrid::placementTilePrefab
	PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 * ___placementTilePrefab_4;
	// Placement.PlacementTile Placement.ObjectPlacementGrid::placementTilePrefabMobile
	PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 * ___placementTilePrefabMobile_5;
	// Core.Utilities.IntVector2 Placement.ObjectPlacementGrid::dimensions
	IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  ___dimensions_6;
	// System.Single Placement.ObjectPlacementGrid::gridSize
	float ___gridSize_7;
	// System.Single Placement.ObjectPlacementGrid::m_InvGridSize
	float ___m_InvGridSize_8;
	// System.Boolean[0...,0...] Placement.ObjectPlacementGrid::m_AvailableCells
	BooleanU5BU2CU5D_tCD5854E1A55166EC93998DDED119F97FF5A5ECDC* ___m_AvailableCells_9;
	// Placement.PlacementTile[0...,0...] Placement.ObjectPlacementGrid::m_Tiles
	PlacementTileU5BU2CU5D_t61562B1B8B671E98D123F8F58F5D2AFB9EBB752E* ___m_Tiles_10;

public:
	inline static int32_t get_offset_of_placementTilePrefab_4() { return static_cast<int32_t>(offsetof(ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E, ___placementTilePrefab_4)); }
	inline PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 * get_placementTilePrefab_4() const { return ___placementTilePrefab_4; }
	inline PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 ** get_address_of_placementTilePrefab_4() { return &___placementTilePrefab_4; }
	inline void set_placementTilePrefab_4(PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 * value)
	{
		___placementTilePrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___placementTilePrefab_4), value);
	}

	inline static int32_t get_offset_of_placementTilePrefabMobile_5() { return static_cast<int32_t>(offsetof(ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E, ___placementTilePrefabMobile_5)); }
	inline PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 * get_placementTilePrefabMobile_5() const { return ___placementTilePrefabMobile_5; }
	inline PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 ** get_address_of_placementTilePrefabMobile_5() { return &___placementTilePrefabMobile_5; }
	inline void set_placementTilePrefabMobile_5(PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 * value)
	{
		___placementTilePrefabMobile_5 = value;
		Il2CppCodeGenWriteBarrier((&___placementTilePrefabMobile_5), value);
	}

	inline static int32_t get_offset_of_dimensions_6() { return static_cast<int32_t>(offsetof(ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E, ___dimensions_6)); }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  get_dimensions_6() const { return ___dimensions_6; }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9 * get_address_of_dimensions_6() { return &___dimensions_6; }
	inline void set_dimensions_6(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  value)
	{
		___dimensions_6 = value;
	}

	inline static int32_t get_offset_of_gridSize_7() { return static_cast<int32_t>(offsetof(ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E, ___gridSize_7)); }
	inline float get_gridSize_7() const { return ___gridSize_7; }
	inline float* get_address_of_gridSize_7() { return &___gridSize_7; }
	inline void set_gridSize_7(float value)
	{
		___gridSize_7 = value;
	}

	inline static int32_t get_offset_of_m_InvGridSize_8() { return static_cast<int32_t>(offsetof(ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E, ___m_InvGridSize_8)); }
	inline float get_m_InvGridSize_8() const { return ___m_InvGridSize_8; }
	inline float* get_address_of_m_InvGridSize_8() { return &___m_InvGridSize_8; }
	inline void set_m_InvGridSize_8(float value)
	{
		___m_InvGridSize_8 = value;
	}

	inline static int32_t get_offset_of_m_AvailableCells_9() { return static_cast<int32_t>(offsetof(ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E, ___m_AvailableCells_9)); }
	inline BooleanU5BU2CU5D_tCD5854E1A55166EC93998DDED119F97FF5A5ECDC* get_m_AvailableCells_9() const { return ___m_AvailableCells_9; }
	inline BooleanU5BU2CU5D_tCD5854E1A55166EC93998DDED119F97FF5A5ECDC** get_address_of_m_AvailableCells_9() { return &___m_AvailableCells_9; }
	inline void set_m_AvailableCells_9(BooleanU5BU2CU5D_tCD5854E1A55166EC93998DDED119F97FF5A5ECDC* value)
	{
		___m_AvailableCells_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AvailableCells_9), value);
	}

	inline static int32_t get_offset_of_m_Tiles_10() { return static_cast<int32_t>(offsetof(ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E, ___m_Tiles_10)); }
	inline PlacementTileU5BU2CU5D_t61562B1B8B671E98D123F8F58F5D2AFB9EBB752E* get_m_Tiles_10() const { return ___m_Tiles_10; }
	inline PlacementTileU5BU2CU5D_t61562B1B8B671E98D123F8F58F5D2AFB9EBB752E** get_address_of_m_Tiles_10() { return &___m_Tiles_10; }
	inline void set_m_Tiles_10(PlacementTileU5BU2CU5D_t61562B1B8B671E98D123F8F58F5D2AFB9EBB752E* value)
	{
		___m_Tiles_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tiles_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPLACEMENTGRID_TACB77AFD3305F152CC7D3C321E439CC41815D91E_H
#ifndef PLACEMENTTILE_T20F49A42F6A5E00D37AD0AC1D50E07FAD0223107_H
#define PLACEMENTTILE_T20F49A42F6A5E00D37AD0AC1D50E07FAD0223107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Placement.PlacementTile
struct  PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Material Placement.PlacementTile::emptyMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___emptyMaterial_4;
	// UnityEngine.Material Placement.PlacementTile::filledMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___filledMaterial_5;
	// UnityEngine.Renderer Placement.PlacementTile::tileRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___tileRenderer_6;

public:
	inline static int32_t get_offset_of_emptyMaterial_4() { return static_cast<int32_t>(offsetof(PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107, ___emptyMaterial_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_emptyMaterial_4() const { return ___emptyMaterial_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_emptyMaterial_4() { return &___emptyMaterial_4; }
	inline void set_emptyMaterial_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___emptyMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___emptyMaterial_4), value);
	}

	inline static int32_t get_offset_of_filledMaterial_5() { return static_cast<int32_t>(offsetof(PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107, ___filledMaterial_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_filledMaterial_5() const { return ___filledMaterial_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_filledMaterial_5() { return &___filledMaterial_5; }
	inline void set_filledMaterial_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___filledMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___filledMaterial_5), value);
	}

	inline static int32_t get_offset_of_tileRenderer_6() { return static_cast<int32_t>(offsetof(PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107, ___tileRenderer_6)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_tileRenderer_6() const { return ___tileRenderer_6; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_tileRenderer_6() { return &___tileRenderer_6; }
	inline void set_tileRenderer_6(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___tileRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&___tileRenderer_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEMENTTILE_T20F49A42F6A5E00D37AD0AC1D50E07FAD0223107_H
#ifndef SINGLEOBJECTPLACEMENTAREA_T78A74C01D07BB007432A1D4D2F677E08250E19BA_H
#define SINGLEOBJECTPLACEMENTAREA_T78A74C01D07BB007432A1D4D2F677E08250E19BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Placement.SingleObjectPlacementArea
struct  SingleObjectPlacementArea_t78A74C01D07BB007432A1D4D2F677E08250E19BA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Placement.PlacementTile Placement.SingleObjectPlacementArea::placementTilePrefab
	PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 * ___placementTilePrefab_4;
	// Placement.PlacementTile Placement.SingleObjectPlacementArea::placementTilePrefabMobile
	PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 * ___placementTilePrefabMobile_5;
	// Placement.PlacementTile Placement.SingleObjectPlacementArea::m_SpawnedTile
	PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 * ___m_SpawnedTile_6;
	// System.Boolean Placement.SingleObjectPlacementArea::m_IsOccupied
	bool ___m_IsOccupied_7;

public:
	inline static int32_t get_offset_of_placementTilePrefab_4() { return static_cast<int32_t>(offsetof(SingleObjectPlacementArea_t78A74C01D07BB007432A1D4D2F677E08250E19BA, ___placementTilePrefab_4)); }
	inline PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 * get_placementTilePrefab_4() const { return ___placementTilePrefab_4; }
	inline PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 ** get_address_of_placementTilePrefab_4() { return &___placementTilePrefab_4; }
	inline void set_placementTilePrefab_4(PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 * value)
	{
		___placementTilePrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___placementTilePrefab_4), value);
	}

	inline static int32_t get_offset_of_placementTilePrefabMobile_5() { return static_cast<int32_t>(offsetof(SingleObjectPlacementArea_t78A74C01D07BB007432A1D4D2F677E08250E19BA, ___placementTilePrefabMobile_5)); }
	inline PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 * get_placementTilePrefabMobile_5() const { return ___placementTilePrefabMobile_5; }
	inline PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 ** get_address_of_placementTilePrefabMobile_5() { return &___placementTilePrefabMobile_5; }
	inline void set_placementTilePrefabMobile_5(PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 * value)
	{
		___placementTilePrefabMobile_5 = value;
		Il2CppCodeGenWriteBarrier((&___placementTilePrefabMobile_5), value);
	}

	inline static int32_t get_offset_of_m_SpawnedTile_6() { return static_cast<int32_t>(offsetof(SingleObjectPlacementArea_t78A74C01D07BB007432A1D4D2F677E08250E19BA, ___m_SpawnedTile_6)); }
	inline PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 * get_m_SpawnedTile_6() const { return ___m_SpawnedTile_6; }
	inline PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 ** get_address_of_m_SpawnedTile_6() { return &___m_SpawnedTile_6; }
	inline void set_m_SpawnedTile_6(PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107 * value)
	{
		___m_SpawnedTile_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpawnedTile_6), value);
	}

	inline static int32_t get_offset_of_m_IsOccupied_7() { return static_cast<int32_t>(offsetof(SingleObjectPlacementArea_t78A74C01D07BB007432A1D4D2F677E08250E19BA, ___m_IsOccupied_7)); }
	inline bool get_m_IsOccupied_7() const { return ___m_IsOccupied_7; }
	inline bool* get_address_of_m_IsOccupied_7() { return &___m_IsOccupied_7; }
	inline void set_m_IsOccupied_7(bool value)
	{
		___m_IsOccupied_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLEOBJECTPLACEMENTAREA_T78A74C01D07BB007432A1D4D2F677E08250E19BA_H
#ifndef ASTEROIDCONTROLLER_T6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB_H
#define ASTEROIDCONTROLLER_T6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.AsteroidController
struct  AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Rigidbody SimpleHealthBar_SpaceshipExample.AsteroidController::myRigidbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___myRigidbody_4;
	// System.Boolean SimpleHealthBar_SpaceshipExample.AsteroidController::canDestroy
	bool ___canDestroy_5;
	// System.Boolean SimpleHealthBar_SpaceshipExample.AsteroidController::isDestroyed
	bool ___isDestroyed_6;
	// System.Boolean SimpleHealthBar_SpaceshipExample.AsteroidController::isDebris
	bool ___isDebris_7;
	// System.Single SimpleHealthBar_SpaceshipExample.AsteroidController::health
	float ___health_8;
	// System.Single SimpleHealthBar_SpaceshipExample.AsteroidController::maxHealth
	float ___maxHealth_9;

public:
	inline static int32_t get_offset_of_myRigidbody_4() { return static_cast<int32_t>(offsetof(AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB, ___myRigidbody_4)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_myRigidbody_4() const { return ___myRigidbody_4; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_myRigidbody_4() { return &___myRigidbody_4; }
	inline void set_myRigidbody_4(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___myRigidbody_4 = value;
		Il2CppCodeGenWriteBarrier((&___myRigidbody_4), value);
	}

	inline static int32_t get_offset_of_canDestroy_5() { return static_cast<int32_t>(offsetof(AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB, ___canDestroy_5)); }
	inline bool get_canDestroy_5() const { return ___canDestroy_5; }
	inline bool* get_address_of_canDestroy_5() { return &___canDestroy_5; }
	inline void set_canDestroy_5(bool value)
	{
		___canDestroy_5 = value;
	}

	inline static int32_t get_offset_of_isDestroyed_6() { return static_cast<int32_t>(offsetof(AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB, ___isDestroyed_6)); }
	inline bool get_isDestroyed_6() const { return ___isDestroyed_6; }
	inline bool* get_address_of_isDestroyed_6() { return &___isDestroyed_6; }
	inline void set_isDestroyed_6(bool value)
	{
		___isDestroyed_6 = value;
	}

	inline static int32_t get_offset_of_isDebris_7() { return static_cast<int32_t>(offsetof(AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB, ___isDebris_7)); }
	inline bool get_isDebris_7() const { return ___isDebris_7; }
	inline bool* get_address_of_isDebris_7() { return &___isDebris_7; }
	inline void set_isDebris_7(bool value)
	{
		___isDebris_7 = value;
	}

	inline static int32_t get_offset_of_health_8() { return static_cast<int32_t>(offsetof(AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB, ___health_8)); }
	inline float get_health_8() const { return ___health_8; }
	inline float* get_address_of_health_8() { return &___health_8; }
	inline void set_health_8(float value)
	{
		___health_8 = value;
	}

	inline static int32_t get_offset_of_maxHealth_9() { return static_cast<int32_t>(offsetof(AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB, ___maxHealth_9)); }
	inline float get_maxHealth_9() const { return ___maxHealth_9; }
	inline float* get_address_of_maxHealth_9() { return &___maxHealth_9; }
	inline void set_maxHealth_9(float value)
	{
		___maxHealth_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTEROIDCONTROLLER_T6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB_H
#ifndef GAMEMANAGER_T57B571316ACCF8579D9C669E2CBB93B1D8E748BC_H
#define GAMEMANAGER_T57B571316ACCF8579D9C669E2CBB93B1D8E748BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.GameManager
struct  GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.GameManager::astroidPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___astroidPrefab_5;
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.GameManager::debrisPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___debrisPrefab_6;
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.GameManager::explosionPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___explosionPrefab_7;
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.GameManager::healthPickupPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___healthPickupPrefab_8;
	// System.Boolean SimpleHealthBar_SpaceshipExample.GameManager::spawning
	bool ___spawning_9;
	// System.Single SimpleHealthBar_SpaceshipExample.GameManager::spawnTimeMin
	float ___spawnTimeMin_10;
	// System.Single SimpleHealthBar_SpaceshipExample.GameManager::spawnTimeMax
	float ___spawnTimeMax_11;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::startingAsteroids
	int32_t ___startingAsteroids_12;
	// System.Single SimpleHealthBar_SpaceshipExample.GameManager::healthSpawnTimeMin
	float ___healthSpawnTimeMin_13;
	// System.Single SimpleHealthBar_SpaceshipExample.GameManager::healthSpawnTimeMax
	float ___healthSpawnTimeMax_14;
	// UnityEngine.UI.Text SimpleHealthBar_SpaceshipExample.GameManager::scoreText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___scoreText_15;
	// UnityEngine.UI.Text SimpleHealthBar_SpaceshipExample.GameManager::finalScoreText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___finalScoreText_16;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::score
	int32_t ___score_17;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::asteroidPoints
	int32_t ___asteroidPoints_18;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::debrisPoints
	int32_t ___debrisPoints_19;
	// UnityEngine.UI.Image SimpleHealthBar_SpaceshipExample.GameManager::gameOverScreen
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___gameOverScreen_20;
	// UnityEngine.UI.Text SimpleHealthBar_SpaceshipExample.GameManager::gameOverText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gameOverText_21;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::asteroidHealth
	int32_t ___asteroidHealth_22;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::debrisHealth
	int32_t ___debrisHealth_23;
	// System.Boolean SimpleHealthBar_SpaceshipExample.GameManager::hasLost
	bool ___hasLost_24;

public:
	inline static int32_t get_offset_of_astroidPrefab_5() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___astroidPrefab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_astroidPrefab_5() const { return ___astroidPrefab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_astroidPrefab_5() { return &___astroidPrefab_5; }
	inline void set_astroidPrefab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___astroidPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___astroidPrefab_5), value);
	}

	inline static int32_t get_offset_of_debrisPrefab_6() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___debrisPrefab_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_debrisPrefab_6() const { return ___debrisPrefab_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_debrisPrefab_6() { return &___debrisPrefab_6; }
	inline void set_debrisPrefab_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___debrisPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___debrisPrefab_6), value);
	}

	inline static int32_t get_offset_of_explosionPrefab_7() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___explosionPrefab_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_explosionPrefab_7() const { return ___explosionPrefab_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_explosionPrefab_7() { return &___explosionPrefab_7; }
	inline void set_explosionPrefab_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___explosionPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___explosionPrefab_7), value);
	}

	inline static int32_t get_offset_of_healthPickupPrefab_8() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___healthPickupPrefab_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_healthPickupPrefab_8() const { return ___healthPickupPrefab_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_healthPickupPrefab_8() { return &___healthPickupPrefab_8; }
	inline void set_healthPickupPrefab_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___healthPickupPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___healthPickupPrefab_8), value);
	}

	inline static int32_t get_offset_of_spawning_9() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___spawning_9)); }
	inline bool get_spawning_9() const { return ___spawning_9; }
	inline bool* get_address_of_spawning_9() { return &___spawning_9; }
	inline void set_spawning_9(bool value)
	{
		___spawning_9 = value;
	}

	inline static int32_t get_offset_of_spawnTimeMin_10() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___spawnTimeMin_10)); }
	inline float get_spawnTimeMin_10() const { return ___spawnTimeMin_10; }
	inline float* get_address_of_spawnTimeMin_10() { return &___spawnTimeMin_10; }
	inline void set_spawnTimeMin_10(float value)
	{
		___spawnTimeMin_10 = value;
	}

	inline static int32_t get_offset_of_spawnTimeMax_11() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___spawnTimeMax_11)); }
	inline float get_spawnTimeMax_11() const { return ___spawnTimeMax_11; }
	inline float* get_address_of_spawnTimeMax_11() { return &___spawnTimeMax_11; }
	inline void set_spawnTimeMax_11(float value)
	{
		___spawnTimeMax_11 = value;
	}

	inline static int32_t get_offset_of_startingAsteroids_12() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___startingAsteroids_12)); }
	inline int32_t get_startingAsteroids_12() const { return ___startingAsteroids_12; }
	inline int32_t* get_address_of_startingAsteroids_12() { return &___startingAsteroids_12; }
	inline void set_startingAsteroids_12(int32_t value)
	{
		___startingAsteroids_12 = value;
	}

	inline static int32_t get_offset_of_healthSpawnTimeMin_13() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___healthSpawnTimeMin_13)); }
	inline float get_healthSpawnTimeMin_13() const { return ___healthSpawnTimeMin_13; }
	inline float* get_address_of_healthSpawnTimeMin_13() { return &___healthSpawnTimeMin_13; }
	inline void set_healthSpawnTimeMin_13(float value)
	{
		___healthSpawnTimeMin_13 = value;
	}

	inline static int32_t get_offset_of_healthSpawnTimeMax_14() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___healthSpawnTimeMax_14)); }
	inline float get_healthSpawnTimeMax_14() const { return ___healthSpawnTimeMax_14; }
	inline float* get_address_of_healthSpawnTimeMax_14() { return &___healthSpawnTimeMax_14; }
	inline void set_healthSpawnTimeMax_14(float value)
	{
		___healthSpawnTimeMax_14 = value;
	}

	inline static int32_t get_offset_of_scoreText_15() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___scoreText_15)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_scoreText_15() const { return ___scoreText_15; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_scoreText_15() { return &___scoreText_15; }
	inline void set_scoreText_15(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___scoreText_15 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_15), value);
	}

	inline static int32_t get_offset_of_finalScoreText_16() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___finalScoreText_16)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_finalScoreText_16() const { return ___finalScoreText_16; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_finalScoreText_16() { return &___finalScoreText_16; }
	inline void set_finalScoreText_16(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___finalScoreText_16 = value;
		Il2CppCodeGenWriteBarrier((&___finalScoreText_16), value);
	}

	inline static int32_t get_offset_of_score_17() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___score_17)); }
	inline int32_t get_score_17() const { return ___score_17; }
	inline int32_t* get_address_of_score_17() { return &___score_17; }
	inline void set_score_17(int32_t value)
	{
		___score_17 = value;
	}

	inline static int32_t get_offset_of_asteroidPoints_18() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___asteroidPoints_18)); }
	inline int32_t get_asteroidPoints_18() const { return ___asteroidPoints_18; }
	inline int32_t* get_address_of_asteroidPoints_18() { return &___asteroidPoints_18; }
	inline void set_asteroidPoints_18(int32_t value)
	{
		___asteroidPoints_18 = value;
	}

	inline static int32_t get_offset_of_debrisPoints_19() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___debrisPoints_19)); }
	inline int32_t get_debrisPoints_19() const { return ___debrisPoints_19; }
	inline int32_t* get_address_of_debrisPoints_19() { return &___debrisPoints_19; }
	inline void set_debrisPoints_19(int32_t value)
	{
		___debrisPoints_19 = value;
	}

	inline static int32_t get_offset_of_gameOverScreen_20() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___gameOverScreen_20)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_gameOverScreen_20() const { return ___gameOverScreen_20; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_gameOverScreen_20() { return &___gameOverScreen_20; }
	inline void set_gameOverScreen_20(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___gameOverScreen_20 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverScreen_20), value);
	}

	inline static int32_t get_offset_of_gameOverText_21() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___gameOverText_21)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gameOverText_21() const { return ___gameOverText_21; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gameOverText_21() { return &___gameOverText_21; }
	inline void set_gameOverText_21(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gameOverText_21 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverText_21), value);
	}

	inline static int32_t get_offset_of_asteroidHealth_22() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___asteroidHealth_22)); }
	inline int32_t get_asteroidHealth_22() const { return ___asteroidHealth_22; }
	inline int32_t* get_address_of_asteroidHealth_22() { return &___asteroidHealth_22; }
	inline void set_asteroidHealth_22(int32_t value)
	{
		___asteroidHealth_22 = value;
	}

	inline static int32_t get_offset_of_debrisHealth_23() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___debrisHealth_23)); }
	inline int32_t get_debrisHealth_23() const { return ___debrisHealth_23; }
	inline int32_t* get_address_of_debrisHealth_23() { return &___debrisHealth_23; }
	inline void set_debrisHealth_23(int32_t value)
	{
		___debrisHealth_23 = value;
	}

	inline static int32_t get_offset_of_hasLost_24() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC, ___hasLost_24)); }
	inline bool get_hasLost_24() const { return ___hasLost_24; }
	inline bool* get_address_of_hasLost_24() { return &___hasLost_24; }
	inline void set_hasLost_24(bool value)
	{
		___hasLost_24 = value;
	}
};

struct GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC_StaticFields
{
public:
	// SimpleHealthBar_SpaceshipExample.GameManager SimpleHealthBar_SpaceshipExample.GameManager::instance
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC_StaticFields, ___instance_4)); }
	inline GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC * get_instance_4() const { return ___instance_4; }
	inline GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T57B571316ACCF8579D9C669E2CBB93B1D8E748BC_H
#ifndef HEALTHPICKUPCONTROLLER_T33A8F52AD069679E26AD051B9A1BE56A58C5121A_H
#define HEALTHPICKUPCONTROLLER_T33A8F52AD069679E26AD051B9A1BE56A58C5121A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.HealthPickupController
struct  HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Rigidbody SimpleHealthBar_SpaceshipExample.HealthPickupController::myRigidbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___myRigidbody_4;
	// UnityEngine.ParticleSystem SimpleHealthBar_SpaceshipExample.HealthPickupController::particles
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___particles_5;
	// UnityEngine.SpriteRenderer SimpleHealthBar_SpaceshipExample.HealthPickupController::mySprite
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___mySprite_6;
	// System.Boolean SimpleHealthBar_SpaceshipExample.HealthPickupController::canDestroy
	bool ___canDestroy_7;
	// System.Boolean SimpleHealthBar_SpaceshipExample.HealthPickupController::canPickup
	bool ___canPickup_8;

public:
	inline static int32_t get_offset_of_myRigidbody_4() { return static_cast<int32_t>(offsetof(HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A, ___myRigidbody_4)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_myRigidbody_4() const { return ___myRigidbody_4; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_myRigidbody_4() { return &___myRigidbody_4; }
	inline void set_myRigidbody_4(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___myRigidbody_4 = value;
		Il2CppCodeGenWriteBarrier((&___myRigidbody_4), value);
	}

	inline static int32_t get_offset_of_particles_5() { return static_cast<int32_t>(offsetof(HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A, ___particles_5)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_particles_5() const { return ___particles_5; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_particles_5() { return &___particles_5; }
	inline void set_particles_5(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___particles_5 = value;
		Il2CppCodeGenWriteBarrier((&___particles_5), value);
	}

	inline static int32_t get_offset_of_mySprite_6() { return static_cast<int32_t>(offsetof(HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A, ___mySprite_6)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_mySprite_6() const { return ___mySprite_6; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_mySprite_6() { return &___mySprite_6; }
	inline void set_mySprite_6(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___mySprite_6 = value;
		Il2CppCodeGenWriteBarrier((&___mySprite_6), value);
	}

	inline static int32_t get_offset_of_canDestroy_7() { return static_cast<int32_t>(offsetof(HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A, ___canDestroy_7)); }
	inline bool get_canDestroy_7() const { return ___canDestroy_7; }
	inline bool* get_address_of_canDestroy_7() { return &___canDestroy_7; }
	inline void set_canDestroy_7(bool value)
	{
		___canDestroy_7 = value;
	}

	inline static int32_t get_offset_of_canPickup_8() { return static_cast<int32_t>(offsetof(HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A, ___canPickup_8)); }
	inline bool get_canPickup_8() const { return ___canPickup_8; }
	inline bool* get_address_of_canPickup_8() { return &___canPickup_8; }
	inline void set_canPickup_8(bool value)
	{
		___canPickup_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEALTHPICKUPCONTROLLER_T33A8F52AD069679E26AD051B9A1BE56A58C5121A_H
#ifndef PLAYERCONTROLLER_TE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2_H
#define PLAYERCONTROLLER_TE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.PlayerController
struct  PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::rotationSpeed
	float ___rotationSpeed_5;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::accelerationSpeed
	float ___accelerationSpeed_6;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::maxSpeed
	float ___maxSpeed_7;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::shootingCooldown
	float ___shootingCooldown_8;
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.PlayerController::bulletPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bulletPrefab_9;
	// UnityEngine.Rigidbody SimpleHealthBar_SpaceshipExample.PlayerController::myRigidbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___myRigidbody_10;
	// UnityEngine.Transform SimpleHealthBar_SpaceshipExample.PlayerController::gunTrans
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___gunTrans_11;
	// UnityEngine.Transform SimpleHealthBar_SpaceshipExample.PlayerController::bulletSpawnPos
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bulletSpawnPos_12;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::shootingTimer
	float ___shootingTimer_13;
	// System.Boolean SimpleHealthBar_SpaceshipExample.PlayerController::canControl
	bool ___canControl_14;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::rotation
	float ___rotation_15;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::acceleration
	float ___acceleration_16;
	// UnityEngine.RectTransform SimpleHealthBar_SpaceshipExample.PlayerController::overheatVisual
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___overheatVisual_17;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::overheatTimer
	float ___overheatTimer_18;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::overheatTimerMax
	float ___overheatTimerMax_19;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::cooldownSpeed
	float ___cooldownSpeed_20;
	// System.Boolean SimpleHealthBar_SpaceshipExample.PlayerController::canShoot
	bool ___canShoot_21;
	// SimpleHealthBar SimpleHealthBar_SpaceshipExample.PlayerController::gunHeatBar
	SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 * ___gunHeatBar_22;

public:
	inline static int32_t get_offset_of_rotationSpeed_5() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___rotationSpeed_5)); }
	inline float get_rotationSpeed_5() const { return ___rotationSpeed_5; }
	inline float* get_address_of_rotationSpeed_5() { return &___rotationSpeed_5; }
	inline void set_rotationSpeed_5(float value)
	{
		___rotationSpeed_5 = value;
	}

	inline static int32_t get_offset_of_accelerationSpeed_6() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___accelerationSpeed_6)); }
	inline float get_accelerationSpeed_6() const { return ___accelerationSpeed_6; }
	inline float* get_address_of_accelerationSpeed_6() { return &___accelerationSpeed_6; }
	inline void set_accelerationSpeed_6(float value)
	{
		___accelerationSpeed_6 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_7() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___maxSpeed_7)); }
	inline float get_maxSpeed_7() const { return ___maxSpeed_7; }
	inline float* get_address_of_maxSpeed_7() { return &___maxSpeed_7; }
	inline void set_maxSpeed_7(float value)
	{
		___maxSpeed_7 = value;
	}

	inline static int32_t get_offset_of_shootingCooldown_8() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___shootingCooldown_8)); }
	inline float get_shootingCooldown_8() const { return ___shootingCooldown_8; }
	inline float* get_address_of_shootingCooldown_8() { return &___shootingCooldown_8; }
	inline void set_shootingCooldown_8(float value)
	{
		___shootingCooldown_8 = value;
	}

	inline static int32_t get_offset_of_bulletPrefab_9() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___bulletPrefab_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bulletPrefab_9() const { return ___bulletPrefab_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bulletPrefab_9() { return &___bulletPrefab_9; }
	inline void set_bulletPrefab_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bulletPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___bulletPrefab_9), value);
	}

	inline static int32_t get_offset_of_myRigidbody_10() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___myRigidbody_10)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_myRigidbody_10() const { return ___myRigidbody_10; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_myRigidbody_10() { return &___myRigidbody_10; }
	inline void set_myRigidbody_10(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___myRigidbody_10 = value;
		Il2CppCodeGenWriteBarrier((&___myRigidbody_10), value);
	}

	inline static int32_t get_offset_of_gunTrans_11() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___gunTrans_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_gunTrans_11() const { return ___gunTrans_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_gunTrans_11() { return &___gunTrans_11; }
	inline void set_gunTrans_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___gunTrans_11 = value;
		Il2CppCodeGenWriteBarrier((&___gunTrans_11), value);
	}

	inline static int32_t get_offset_of_bulletSpawnPos_12() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___bulletSpawnPos_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bulletSpawnPos_12() const { return ___bulletSpawnPos_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bulletSpawnPos_12() { return &___bulletSpawnPos_12; }
	inline void set_bulletSpawnPos_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bulletSpawnPos_12 = value;
		Il2CppCodeGenWriteBarrier((&___bulletSpawnPos_12), value);
	}

	inline static int32_t get_offset_of_shootingTimer_13() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___shootingTimer_13)); }
	inline float get_shootingTimer_13() const { return ___shootingTimer_13; }
	inline float* get_address_of_shootingTimer_13() { return &___shootingTimer_13; }
	inline void set_shootingTimer_13(float value)
	{
		___shootingTimer_13 = value;
	}

	inline static int32_t get_offset_of_canControl_14() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___canControl_14)); }
	inline bool get_canControl_14() const { return ___canControl_14; }
	inline bool* get_address_of_canControl_14() { return &___canControl_14; }
	inline void set_canControl_14(bool value)
	{
		___canControl_14 = value;
	}

	inline static int32_t get_offset_of_rotation_15() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___rotation_15)); }
	inline float get_rotation_15() const { return ___rotation_15; }
	inline float* get_address_of_rotation_15() { return &___rotation_15; }
	inline void set_rotation_15(float value)
	{
		___rotation_15 = value;
	}

	inline static int32_t get_offset_of_acceleration_16() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___acceleration_16)); }
	inline float get_acceleration_16() const { return ___acceleration_16; }
	inline float* get_address_of_acceleration_16() { return &___acceleration_16; }
	inline void set_acceleration_16(float value)
	{
		___acceleration_16 = value;
	}

	inline static int32_t get_offset_of_overheatVisual_17() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___overheatVisual_17)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_overheatVisual_17() const { return ___overheatVisual_17; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_overheatVisual_17() { return &___overheatVisual_17; }
	inline void set_overheatVisual_17(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___overheatVisual_17 = value;
		Il2CppCodeGenWriteBarrier((&___overheatVisual_17), value);
	}

	inline static int32_t get_offset_of_overheatTimer_18() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___overheatTimer_18)); }
	inline float get_overheatTimer_18() const { return ___overheatTimer_18; }
	inline float* get_address_of_overheatTimer_18() { return &___overheatTimer_18; }
	inline void set_overheatTimer_18(float value)
	{
		___overheatTimer_18 = value;
	}

	inline static int32_t get_offset_of_overheatTimerMax_19() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___overheatTimerMax_19)); }
	inline float get_overheatTimerMax_19() const { return ___overheatTimerMax_19; }
	inline float* get_address_of_overheatTimerMax_19() { return &___overheatTimerMax_19; }
	inline void set_overheatTimerMax_19(float value)
	{
		___overheatTimerMax_19 = value;
	}

	inline static int32_t get_offset_of_cooldownSpeed_20() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___cooldownSpeed_20)); }
	inline float get_cooldownSpeed_20() const { return ___cooldownSpeed_20; }
	inline float* get_address_of_cooldownSpeed_20() { return &___cooldownSpeed_20; }
	inline void set_cooldownSpeed_20(float value)
	{
		___cooldownSpeed_20 = value;
	}

	inline static int32_t get_offset_of_canShoot_21() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___canShoot_21)); }
	inline bool get_canShoot_21() const { return ___canShoot_21; }
	inline bool* get_address_of_canShoot_21() { return &___canShoot_21; }
	inline void set_canShoot_21(bool value)
	{
		___canShoot_21 = value;
	}

	inline static int32_t get_offset_of_gunHeatBar_22() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2, ___gunHeatBar_22)); }
	inline SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 * get_gunHeatBar_22() const { return ___gunHeatBar_22; }
	inline SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 ** get_address_of_gunHeatBar_22() { return &___gunHeatBar_22; }
	inline void set_gunHeatBar_22(SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 * value)
	{
		___gunHeatBar_22 = value;
		Il2CppCodeGenWriteBarrier((&___gunHeatBar_22), value);
	}
};

struct PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2_StaticFields
{
public:
	// SimpleHealthBar_SpaceshipExample.PlayerController SimpleHealthBar_SpaceshipExample.PlayerController::instance
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2_StaticFields, ___instance_4)); }
	inline PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2 * get_instance_4() const { return ___instance_4; }
	inline PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_TE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2_H
#ifndef PLAYERHEALTH_TA3F94E28DC22C88ACD17777530BD9FEA631EBBC1_H
#define PLAYERHEALTH_TA3F94E28DC22C88ACD17777530BD9FEA631EBBC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.PlayerHealth
struct  PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean SimpleHealthBar_SpaceshipExample.PlayerHealth::canTakeDamage
	bool ___canTakeDamage_5;
	// System.Int32 SimpleHealthBar_SpaceshipExample.PlayerHealth::maxHealth
	int32_t ___maxHealth_6;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth::currentHealth
	float ___currentHealth_7;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth::invulnerabilityTime
	float ___invulnerabilityTime_8;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth::currentShield
	float ___currentShield_9;
	// System.Int32 SimpleHealthBar_SpaceshipExample.PlayerHealth::maxShield
	int32_t ___maxShield_10;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth::regenShieldTimer
	float ___regenShieldTimer_11;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth::regenShieldTimerMax
	float ___regenShieldTimerMax_12;
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.PlayerHealth::explosionParticles
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___explosionParticles_13;
	// SimpleHealthBar SimpleHealthBar_SpaceshipExample.PlayerHealth::healthBar
	SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 * ___healthBar_14;
	// SimpleHealthBar SimpleHealthBar_SpaceshipExample.PlayerHealth::shieldBar
	SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 * ___shieldBar_15;

public:
	inline static int32_t get_offset_of_canTakeDamage_5() { return static_cast<int32_t>(offsetof(PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1, ___canTakeDamage_5)); }
	inline bool get_canTakeDamage_5() const { return ___canTakeDamage_5; }
	inline bool* get_address_of_canTakeDamage_5() { return &___canTakeDamage_5; }
	inline void set_canTakeDamage_5(bool value)
	{
		___canTakeDamage_5 = value;
	}

	inline static int32_t get_offset_of_maxHealth_6() { return static_cast<int32_t>(offsetof(PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1, ___maxHealth_6)); }
	inline int32_t get_maxHealth_6() const { return ___maxHealth_6; }
	inline int32_t* get_address_of_maxHealth_6() { return &___maxHealth_6; }
	inline void set_maxHealth_6(int32_t value)
	{
		___maxHealth_6 = value;
	}

	inline static int32_t get_offset_of_currentHealth_7() { return static_cast<int32_t>(offsetof(PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1, ___currentHealth_7)); }
	inline float get_currentHealth_7() const { return ___currentHealth_7; }
	inline float* get_address_of_currentHealth_7() { return &___currentHealth_7; }
	inline void set_currentHealth_7(float value)
	{
		___currentHealth_7 = value;
	}

	inline static int32_t get_offset_of_invulnerabilityTime_8() { return static_cast<int32_t>(offsetof(PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1, ___invulnerabilityTime_8)); }
	inline float get_invulnerabilityTime_8() const { return ___invulnerabilityTime_8; }
	inline float* get_address_of_invulnerabilityTime_8() { return &___invulnerabilityTime_8; }
	inline void set_invulnerabilityTime_8(float value)
	{
		___invulnerabilityTime_8 = value;
	}

	inline static int32_t get_offset_of_currentShield_9() { return static_cast<int32_t>(offsetof(PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1, ___currentShield_9)); }
	inline float get_currentShield_9() const { return ___currentShield_9; }
	inline float* get_address_of_currentShield_9() { return &___currentShield_9; }
	inline void set_currentShield_9(float value)
	{
		___currentShield_9 = value;
	}

	inline static int32_t get_offset_of_maxShield_10() { return static_cast<int32_t>(offsetof(PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1, ___maxShield_10)); }
	inline int32_t get_maxShield_10() const { return ___maxShield_10; }
	inline int32_t* get_address_of_maxShield_10() { return &___maxShield_10; }
	inline void set_maxShield_10(int32_t value)
	{
		___maxShield_10 = value;
	}

	inline static int32_t get_offset_of_regenShieldTimer_11() { return static_cast<int32_t>(offsetof(PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1, ___regenShieldTimer_11)); }
	inline float get_regenShieldTimer_11() const { return ___regenShieldTimer_11; }
	inline float* get_address_of_regenShieldTimer_11() { return &___regenShieldTimer_11; }
	inline void set_regenShieldTimer_11(float value)
	{
		___regenShieldTimer_11 = value;
	}

	inline static int32_t get_offset_of_regenShieldTimerMax_12() { return static_cast<int32_t>(offsetof(PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1, ___regenShieldTimerMax_12)); }
	inline float get_regenShieldTimerMax_12() const { return ___regenShieldTimerMax_12; }
	inline float* get_address_of_regenShieldTimerMax_12() { return &___regenShieldTimerMax_12; }
	inline void set_regenShieldTimerMax_12(float value)
	{
		___regenShieldTimerMax_12 = value;
	}

	inline static int32_t get_offset_of_explosionParticles_13() { return static_cast<int32_t>(offsetof(PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1, ___explosionParticles_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_explosionParticles_13() const { return ___explosionParticles_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_explosionParticles_13() { return &___explosionParticles_13; }
	inline void set_explosionParticles_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___explosionParticles_13 = value;
		Il2CppCodeGenWriteBarrier((&___explosionParticles_13), value);
	}

	inline static int32_t get_offset_of_healthBar_14() { return static_cast<int32_t>(offsetof(PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1, ___healthBar_14)); }
	inline SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 * get_healthBar_14() const { return ___healthBar_14; }
	inline SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 ** get_address_of_healthBar_14() { return &___healthBar_14; }
	inline void set_healthBar_14(SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 * value)
	{
		___healthBar_14 = value;
		Il2CppCodeGenWriteBarrier((&___healthBar_14), value);
	}

	inline static int32_t get_offset_of_shieldBar_15() { return static_cast<int32_t>(offsetof(PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1, ___shieldBar_15)); }
	inline SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 * get_shieldBar_15() const { return ___shieldBar_15; }
	inline SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 ** get_address_of_shieldBar_15() { return &___shieldBar_15; }
	inline void set_shieldBar_15(SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 * value)
	{
		___shieldBar_15 = value;
		Il2CppCodeGenWriteBarrier((&___shieldBar_15), value);
	}
};

struct PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1_StaticFields
{
public:
	// SimpleHealthBar_SpaceshipExample.PlayerHealth SimpleHealthBar_SpaceshipExample.PlayerHealth::instance
	PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1_StaticFields, ___instance_4)); }
	inline PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1 * get_instance_4() const { return ___instance_4; }
	inline PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERHEALTH_TA3F94E28DC22C88ACD17777530BD9FEA631EBBC1_H
#ifndef SELFDESTROYTIMER_T8C6B5648246F50B10AB7BF98D77A8E6D4D3EB476_H
#define SELFDESTROYTIMER_T8C6B5648246F50B10AB7BF98D77A8E6D4D3EB476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerDefense.Towers.SelfDestroyTimer
struct  SelfDestroyTimer_t8C6B5648246F50B10AB7BF98D77A8E6D4D3EB476  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single TowerDefense.Towers.SelfDestroyTimer::time
	float ___time_4;
	// Core.Utilities.Timer TowerDefense.Towers.SelfDestroyTimer::timer
	Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B * ___timer_5;
	// UnityEngine.Events.UnityEvent TowerDefense.Towers.SelfDestroyTimer::death
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___death_6;

public:
	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(SelfDestroyTimer_t8C6B5648246F50B10AB7BF98D77A8E6D4D3EB476, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}

	inline static int32_t get_offset_of_timer_5() { return static_cast<int32_t>(offsetof(SelfDestroyTimer_t8C6B5648246F50B10AB7BF98D77A8E6D4D3EB476, ___timer_5)); }
	inline Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B * get_timer_5() const { return ___timer_5; }
	inline Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B ** get_address_of_timer_5() { return &___timer_5; }
	inline void set_timer_5(Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B * value)
	{
		___timer_5 = value;
		Il2CppCodeGenWriteBarrier((&___timer_5), value);
	}

	inline static int32_t get_offset_of_death_6() { return static_cast<int32_t>(offsetof(SelfDestroyTimer_t8C6B5648246F50B10AB7BF98D77A8E6D4D3EB476, ___death_6)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_death_6() const { return ___death_6; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_death_6() { return &___death_6; }
	inline void set_death_6(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___death_6 = value;
		Il2CppCodeGenWriteBarrier((&___death_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELFDESTROYTIMER_T8C6B5648246F50B10AB7BF98D77A8E6D4D3EB476_H
#ifndef TOWERLEVEL_T483DF43339AC44420BE580ADEC7F31C71C18ADB9_H
#define TOWERLEVEL_T483DF43339AC44420BE580ADEC7F31C71C18ADB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Towers.TowerLevel
struct  TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Towers.TowerPlacementGhost Towers.TowerLevel::towerGhostPrefab
	TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7 * ___towerGhostPrefab_4;
	// Towers.Data.TowerLevelData Towers.TowerLevel::levelData
	TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9 * ___levelData_5;
	// Towers.Tower Towers.TowerLevel::m_ParentTower
	Tower_tCB73BE25139856154FE23524412B9114BA6260DF * ___m_ParentTower_6;
	// Turret Towers.TowerLevel::m_Turret
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E * ___m_Turret_7;
	// UnityEngine.LayerMask Towers.TowerLevel::<mask>k__BackingField
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___U3CmaskU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_towerGhostPrefab_4() { return static_cast<int32_t>(offsetof(TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9, ___towerGhostPrefab_4)); }
	inline TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7 * get_towerGhostPrefab_4() const { return ___towerGhostPrefab_4; }
	inline TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7 ** get_address_of_towerGhostPrefab_4() { return &___towerGhostPrefab_4; }
	inline void set_towerGhostPrefab_4(TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7 * value)
	{
		___towerGhostPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___towerGhostPrefab_4), value);
	}

	inline static int32_t get_offset_of_levelData_5() { return static_cast<int32_t>(offsetof(TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9, ___levelData_5)); }
	inline TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9 * get_levelData_5() const { return ___levelData_5; }
	inline TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9 ** get_address_of_levelData_5() { return &___levelData_5; }
	inline void set_levelData_5(TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9 * value)
	{
		___levelData_5 = value;
		Il2CppCodeGenWriteBarrier((&___levelData_5), value);
	}

	inline static int32_t get_offset_of_m_ParentTower_6() { return static_cast<int32_t>(offsetof(TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9, ___m_ParentTower_6)); }
	inline Tower_tCB73BE25139856154FE23524412B9114BA6260DF * get_m_ParentTower_6() const { return ___m_ParentTower_6; }
	inline Tower_tCB73BE25139856154FE23524412B9114BA6260DF ** get_address_of_m_ParentTower_6() { return &___m_ParentTower_6; }
	inline void set_m_ParentTower_6(Tower_tCB73BE25139856154FE23524412B9114BA6260DF * value)
	{
		___m_ParentTower_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentTower_6), value);
	}

	inline static int32_t get_offset_of_m_Turret_7() { return static_cast<int32_t>(offsetof(TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9, ___m_Turret_7)); }
	inline Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E * get_m_Turret_7() const { return ___m_Turret_7; }
	inline Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E ** get_address_of_m_Turret_7() { return &___m_Turret_7; }
	inline void set_m_Turret_7(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E * value)
	{
		___m_Turret_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Turret_7), value);
	}

	inline static int32_t get_offset_of_U3CmaskU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9, ___U3CmaskU3Ek__BackingField_8)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_U3CmaskU3Ek__BackingField_8() const { return ___U3CmaskU3Ek__BackingField_8; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_U3CmaskU3Ek__BackingField_8() { return &___U3CmaskU3Ek__BackingField_8; }
	inline void set_U3CmaskU3Ek__BackingField_8(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___U3CmaskU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERLEVEL_T483DF43339AC44420BE580ADEC7F31C71C18ADB9_H
#ifndef TOWERPLACEMENTGHOST_T07837449CA42E96133BDBB23D88A06371310E1B7_H
#define TOWERPLACEMENTGHOST_T07837449CA42E96133BDBB23D88A06371310E1B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Towers.TowerPlacementGhost
struct  TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Towers.Tower Towers.TowerPlacementGhost::<controller>k__BackingField
	Tower_tCB73BE25139856154FE23524412B9114BA6260DF * ___U3CcontrollerU3Ek__BackingField_4;
	// UnityEngine.GameObject Towers.TowerPlacementGhost::radiusVisualizer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___radiusVisualizer_5;
	// System.Single Towers.TowerPlacementGhost::radiusVisualizerHeight
	float ___radiusVisualizerHeight_6;
	// System.Single Towers.TowerPlacementGhost::dampSpeed
	float ___dampSpeed_7;
	// UnityEngine.Material Towers.TowerPlacementGhost::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_8;
	// UnityEngine.Material Towers.TowerPlacementGhost::invalidPositionMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___invalidPositionMaterial_9;
	// UnityEngine.MeshRenderer[] Towers.TowerPlacementGhost::m_MeshRenderers
	MeshRendererU5BU5D_t5FE99DFF4D53B490C79494B8DCAA661F1721E0D3* ___m_MeshRenderers_10;
	// UnityEngine.Vector3 Towers.TowerPlacementGhost::m_MoveVel
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_MoveVel_11;
	// UnityEngine.Vector3 Towers.TowerPlacementGhost::m_TargetPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_TargetPosition_12;
	// System.Boolean Towers.TowerPlacementGhost::m_ValidPos
	bool ___m_ValidPos_13;
	// UnityEngine.Collider Towers.TowerPlacementGhost::<ghostCollider>k__BackingField
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___U3CghostColliderU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CcontrollerU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7, ___U3CcontrollerU3Ek__BackingField_4)); }
	inline Tower_tCB73BE25139856154FE23524412B9114BA6260DF * get_U3CcontrollerU3Ek__BackingField_4() const { return ___U3CcontrollerU3Ek__BackingField_4; }
	inline Tower_tCB73BE25139856154FE23524412B9114BA6260DF ** get_address_of_U3CcontrollerU3Ek__BackingField_4() { return &___U3CcontrollerU3Ek__BackingField_4; }
	inline void set_U3CcontrollerU3Ek__BackingField_4(Tower_tCB73BE25139856154FE23524412B9114BA6260DF * value)
	{
		___U3CcontrollerU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontrollerU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_radiusVisualizer_5() { return static_cast<int32_t>(offsetof(TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7, ___radiusVisualizer_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_radiusVisualizer_5() const { return ___radiusVisualizer_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_radiusVisualizer_5() { return &___radiusVisualizer_5; }
	inline void set_radiusVisualizer_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___radiusVisualizer_5 = value;
		Il2CppCodeGenWriteBarrier((&___radiusVisualizer_5), value);
	}

	inline static int32_t get_offset_of_radiusVisualizerHeight_6() { return static_cast<int32_t>(offsetof(TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7, ___radiusVisualizerHeight_6)); }
	inline float get_radiusVisualizerHeight_6() const { return ___radiusVisualizerHeight_6; }
	inline float* get_address_of_radiusVisualizerHeight_6() { return &___radiusVisualizerHeight_6; }
	inline void set_radiusVisualizerHeight_6(float value)
	{
		___radiusVisualizerHeight_6 = value;
	}

	inline static int32_t get_offset_of_dampSpeed_7() { return static_cast<int32_t>(offsetof(TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7, ___dampSpeed_7)); }
	inline float get_dampSpeed_7() const { return ___dampSpeed_7; }
	inline float* get_address_of_dampSpeed_7() { return &___dampSpeed_7; }
	inline void set_dampSpeed_7(float value)
	{
		___dampSpeed_7 = value;
	}

	inline static int32_t get_offset_of_material_8() { return static_cast<int32_t>(offsetof(TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7, ___material_8)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_8() const { return ___material_8; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_8() { return &___material_8; }
	inline void set_material_8(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_8 = value;
		Il2CppCodeGenWriteBarrier((&___material_8), value);
	}

	inline static int32_t get_offset_of_invalidPositionMaterial_9() { return static_cast<int32_t>(offsetof(TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7, ___invalidPositionMaterial_9)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_invalidPositionMaterial_9() const { return ___invalidPositionMaterial_9; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_invalidPositionMaterial_9() { return &___invalidPositionMaterial_9; }
	inline void set_invalidPositionMaterial_9(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___invalidPositionMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___invalidPositionMaterial_9), value);
	}

	inline static int32_t get_offset_of_m_MeshRenderers_10() { return static_cast<int32_t>(offsetof(TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7, ___m_MeshRenderers_10)); }
	inline MeshRendererU5BU5D_t5FE99DFF4D53B490C79494B8DCAA661F1721E0D3* get_m_MeshRenderers_10() const { return ___m_MeshRenderers_10; }
	inline MeshRendererU5BU5D_t5FE99DFF4D53B490C79494B8DCAA661F1721E0D3** get_address_of_m_MeshRenderers_10() { return &___m_MeshRenderers_10; }
	inline void set_m_MeshRenderers_10(MeshRendererU5BU5D_t5FE99DFF4D53B490C79494B8DCAA661F1721E0D3* value)
	{
		___m_MeshRenderers_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_MeshRenderers_10), value);
	}

	inline static int32_t get_offset_of_m_MoveVel_11() { return static_cast<int32_t>(offsetof(TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7, ___m_MoveVel_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_MoveVel_11() const { return ___m_MoveVel_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_MoveVel_11() { return &___m_MoveVel_11; }
	inline void set_m_MoveVel_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_MoveVel_11 = value;
	}

	inline static int32_t get_offset_of_m_TargetPosition_12() { return static_cast<int32_t>(offsetof(TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7, ___m_TargetPosition_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_TargetPosition_12() const { return ___m_TargetPosition_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_TargetPosition_12() { return &___m_TargetPosition_12; }
	inline void set_m_TargetPosition_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_TargetPosition_12 = value;
	}

	inline static int32_t get_offset_of_m_ValidPos_13() { return static_cast<int32_t>(offsetof(TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7, ___m_ValidPos_13)); }
	inline bool get_m_ValidPos_13() const { return ___m_ValidPos_13; }
	inline bool* get_address_of_m_ValidPos_13() { return &___m_ValidPos_13; }
	inline void set_m_ValidPos_13(bool value)
	{
		___m_ValidPos_13 = value;
	}

	inline static int32_t get_offset_of_U3CghostColliderU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7, ___U3CghostColliderU3Ek__BackingField_14)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_U3CghostColliderU3Ek__BackingField_14() const { return ___U3CghostColliderU3Ek__BackingField_14; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_U3CghostColliderU3Ek__BackingField_14() { return &___U3CghostColliderU3Ek__BackingField_14; }
	inline void set_U3CghostColliderU3Ek__BackingField_14(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___U3CghostColliderU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CghostColliderU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERPLACEMENTGHOST_T07837449CA42E96133BDBB23D88A06371310E1B7_H
#ifndef BUILDINFOUI_TD9CF97A89D59E29BDDE2C47795495FA06EEF28AF_H
#define BUILDINFOUI_TD9CF97A89D59E29BDDE2C47795495FA06EEF28AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Towers.UI.HUD.BuildInfoUI
struct  BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Animation Towers.UI.HUD.BuildInfoUI::anim
	Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * ___anim_4;
	// System.String Towers.UI.HUD.BuildInfoUI::showClipName
	String_t* ___showClipName_5;
	// System.String Towers.UI.HUD.BuildInfoUI::hideClipName
	String_t* ___hideClipName_6;
	// Towers.UI.HUD.TowerUI Towers.UI.HUD.BuildInfoUI::m_TowerUI
	TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF * ___m_TowerUI_7;
	// UnityEngine.Canvas Towers.UI.HUD.BuildInfoUI::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_8;
	// Towers.UI.HUD.BuildInfoUI/AnimationState Towers.UI.HUD.BuildInfoUI::m_State
	int32_t ___m_State_9;

public:
	inline static int32_t get_offset_of_anim_4() { return static_cast<int32_t>(offsetof(BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF, ___anim_4)); }
	inline Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * get_anim_4() const { return ___anim_4; }
	inline Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C ** get_address_of_anim_4() { return &___anim_4; }
	inline void set_anim_4(Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * value)
	{
		___anim_4 = value;
		Il2CppCodeGenWriteBarrier((&___anim_4), value);
	}

	inline static int32_t get_offset_of_showClipName_5() { return static_cast<int32_t>(offsetof(BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF, ___showClipName_5)); }
	inline String_t* get_showClipName_5() const { return ___showClipName_5; }
	inline String_t** get_address_of_showClipName_5() { return &___showClipName_5; }
	inline void set_showClipName_5(String_t* value)
	{
		___showClipName_5 = value;
		Il2CppCodeGenWriteBarrier((&___showClipName_5), value);
	}

	inline static int32_t get_offset_of_hideClipName_6() { return static_cast<int32_t>(offsetof(BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF, ___hideClipName_6)); }
	inline String_t* get_hideClipName_6() const { return ___hideClipName_6; }
	inline String_t** get_address_of_hideClipName_6() { return &___hideClipName_6; }
	inline void set_hideClipName_6(String_t* value)
	{
		___hideClipName_6 = value;
		Il2CppCodeGenWriteBarrier((&___hideClipName_6), value);
	}

	inline static int32_t get_offset_of_m_TowerUI_7() { return static_cast<int32_t>(offsetof(BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF, ___m_TowerUI_7)); }
	inline TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF * get_m_TowerUI_7() const { return ___m_TowerUI_7; }
	inline TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF ** get_address_of_m_TowerUI_7() { return &___m_TowerUI_7; }
	inline void set_m_TowerUI_7(TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF * value)
	{
		___m_TowerUI_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TowerUI_7), value);
	}

	inline static int32_t get_offset_of_m_Canvas_8() { return static_cast<int32_t>(offsetof(BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF, ___m_Canvas_8)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_8() const { return ___m_Canvas_8; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_8() { return &___m_Canvas_8; }
	inline void set_m_Canvas_8(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_8), value);
	}

	inline static int32_t get_offset_of_m_State_9() { return static_cast<int32_t>(offsetof(BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF, ___m_State_9)); }
	inline int32_t get_m_State_9() const { return ___m_State_9; }
	inline int32_t* get_address_of_m_State_9() { return &___m_State_9; }
	inline void set_m_State_9(int32_t value)
	{
		___m_State_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDINFOUI_TD9CF97A89D59E29BDDE2C47795495FA06EEF28AF_H
#ifndef TOWERINFODISPLAY_T2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6_H
#define TOWERINFODISPLAY_T2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Towers.UI.HUD.TowerInfoDisplay
struct  TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Towers.UI.HUD.TowerInfoDisplay::towerName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___towerName_4;
	// UnityEngine.UI.Text Towers.UI.HUD.TowerInfoDisplay::description
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___description_5;
	// UnityEngine.UI.Text Towers.UI.HUD.TowerInfoDisplay::dps
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___dps_6;
	// UnityEngine.UI.Text Towers.UI.HUD.TowerInfoDisplay::level
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___level_7;
	// UnityEngine.UI.Text Towers.UI.HUD.TowerInfoDisplay::health
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___health_8;
	// UnityEngine.UI.Text Towers.UI.HUD.TowerInfoDisplay::dimensions
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___dimensions_9;
	// UnityEngine.UI.Text Towers.UI.HUD.TowerInfoDisplay::upgradeCost
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___upgradeCost_10;
	// UnityEngine.UI.Text Towers.UI.HUD.TowerInfoDisplay::sellPrice
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___sellPrice_11;

public:
	inline static int32_t get_offset_of_towerName_4() { return static_cast<int32_t>(offsetof(TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6, ___towerName_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_towerName_4() const { return ___towerName_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_towerName_4() { return &___towerName_4; }
	inline void set_towerName_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___towerName_4 = value;
		Il2CppCodeGenWriteBarrier((&___towerName_4), value);
	}

	inline static int32_t get_offset_of_description_5() { return static_cast<int32_t>(offsetof(TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6, ___description_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_description_5() const { return ___description_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_description_5() { return &___description_5; }
	inline void set_description_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___description_5 = value;
		Il2CppCodeGenWriteBarrier((&___description_5), value);
	}

	inline static int32_t get_offset_of_dps_6() { return static_cast<int32_t>(offsetof(TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6, ___dps_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_dps_6() const { return ___dps_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_dps_6() { return &___dps_6; }
	inline void set_dps_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___dps_6 = value;
		Il2CppCodeGenWriteBarrier((&___dps_6), value);
	}

	inline static int32_t get_offset_of_level_7() { return static_cast<int32_t>(offsetof(TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6, ___level_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_level_7() const { return ___level_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_level_7() { return &___level_7; }
	inline void set_level_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___level_7 = value;
		Il2CppCodeGenWriteBarrier((&___level_7), value);
	}

	inline static int32_t get_offset_of_health_8() { return static_cast<int32_t>(offsetof(TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6, ___health_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_health_8() const { return ___health_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_health_8() { return &___health_8; }
	inline void set_health_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___health_8 = value;
		Il2CppCodeGenWriteBarrier((&___health_8), value);
	}

	inline static int32_t get_offset_of_dimensions_9() { return static_cast<int32_t>(offsetof(TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6, ___dimensions_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_dimensions_9() const { return ___dimensions_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_dimensions_9() { return &___dimensions_9; }
	inline void set_dimensions_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___dimensions_9 = value;
		Il2CppCodeGenWriteBarrier((&___dimensions_9), value);
	}

	inline static int32_t get_offset_of_upgradeCost_10() { return static_cast<int32_t>(offsetof(TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6, ___upgradeCost_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_upgradeCost_10() const { return ___upgradeCost_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_upgradeCost_10() { return &___upgradeCost_10; }
	inline void set_upgradeCost_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___upgradeCost_10 = value;
		Il2CppCodeGenWriteBarrier((&___upgradeCost_10), value);
	}

	inline static int32_t get_offset_of_sellPrice_11() { return static_cast<int32_t>(offsetof(TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6, ___sellPrice_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_sellPrice_11() const { return ___sellPrice_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_sellPrice_11() { return &___sellPrice_11; }
	inline void set_sellPrice_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___sellPrice_11 = value;
		Il2CppCodeGenWriteBarrier((&___sellPrice_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERINFODISPLAY_T2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6_H
#ifndef TOWERSPAWNBUTTON_T980372E20A7E18067070D7209BF454D083553E6C_H
#define TOWERSPAWNBUTTON_T980372E20A7E18067070D7209BF454D083553E6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Towers.UI.HUD.TowerSpawnButton
struct  TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Towers.UI.HUD.TowerSpawnButton::buttonText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___buttonText_4;
	// UnityEngine.UI.Image Towers.UI.HUD.TowerSpawnButton::towerIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___towerIcon_5;
	// UnityEngine.UI.Button Towers.UI.HUD.TowerSpawnButton::buyButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___buyButton_6;
	// UnityEngine.UI.Image Towers.UI.HUD.TowerSpawnButton::energyIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___energyIcon_7;
	// UnityEngine.Color Towers.UI.HUD.TowerSpawnButton::energyDefaultColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___energyDefaultColor_8;
	// UnityEngine.Color Towers.UI.HUD.TowerSpawnButton::energyInvalidColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___energyInvalidColor_9;
	// System.Action`1<Towers.Tower> Towers.UI.HUD.TowerSpawnButton::buttonTapped
	Action_1_t7149A4E24DCDE38351BFE018FCC6DE62E88D5D12 * ___buttonTapped_10;
	// System.Action`1<Towers.Tower> Towers.UI.HUD.TowerSpawnButton::draggedOff
	Action_1_t7149A4E24DCDE38351BFE018FCC6DE62E88D5D12 * ___draggedOff_11;
	// Towers.Tower Towers.UI.HUD.TowerSpawnButton::m_Tower
	Tower_tCB73BE25139856154FE23524412B9114BA6260DF * ___m_Tower_12;
	// Economy.Currency Towers.UI.HUD.TowerSpawnButton::m_Currency
	Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05 * ___m_Currency_13;
	// UnityEngine.RectTransform Towers.UI.HUD.TowerSpawnButton::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_14;

public:
	inline static int32_t get_offset_of_buttonText_4() { return static_cast<int32_t>(offsetof(TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C, ___buttonText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_buttonText_4() const { return ___buttonText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_buttonText_4() { return &___buttonText_4; }
	inline void set_buttonText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___buttonText_4 = value;
		Il2CppCodeGenWriteBarrier((&___buttonText_4), value);
	}

	inline static int32_t get_offset_of_towerIcon_5() { return static_cast<int32_t>(offsetof(TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C, ___towerIcon_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_towerIcon_5() const { return ___towerIcon_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_towerIcon_5() { return &___towerIcon_5; }
	inline void set_towerIcon_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___towerIcon_5 = value;
		Il2CppCodeGenWriteBarrier((&___towerIcon_5), value);
	}

	inline static int32_t get_offset_of_buyButton_6() { return static_cast<int32_t>(offsetof(TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C, ___buyButton_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_buyButton_6() const { return ___buyButton_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_buyButton_6() { return &___buyButton_6; }
	inline void set_buyButton_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___buyButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___buyButton_6), value);
	}

	inline static int32_t get_offset_of_energyIcon_7() { return static_cast<int32_t>(offsetof(TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C, ___energyIcon_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_energyIcon_7() const { return ___energyIcon_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_energyIcon_7() { return &___energyIcon_7; }
	inline void set_energyIcon_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___energyIcon_7 = value;
		Il2CppCodeGenWriteBarrier((&___energyIcon_7), value);
	}

	inline static int32_t get_offset_of_energyDefaultColor_8() { return static_cast<int32_t>(offsetof(TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C, ___energyDefaultColor_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_energyDefaultColor_8() const { return ___energyDefaultColor_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_energyDefaultColor_8() { return &___energyDefaultColor_8; }
	inline void set_energyDefaultColor_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___energyDefaultColor_8 = value;
	}

	inline static int32_t get_offset_of_energyInvalidColor_9() { return static_cast<int32_t>(offsetof(TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C, ___energyInvalidColor_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_energyInvalidColor_9() const { return ___energyInvalidColor_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_energyInvalidColor_9() { return &___energyInvalidColor_9; }
	inline void set_energyInvalidColor_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___energyInvalidColor_9 = value;
	}

	inline static int32_t get_offset_of_buttonTapped_10() { return static_cast<int32_t>(offsetof(TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C, ___buttonTapped_10)); }
	inline Action_1_t7149A4E24DCDE38351BFE018FCC6DE62E88D5D12 * get_buttonTapped_10() const { return ___buttonTapped_10; }
	inline Action_1_t7149A4E24DCDE38351BFE018FCC6DE62E88D5D12 ** get_address_of_buttonTapped_10() { return &___buttonTapped_10; }
	inline void set_buttonTapped_10(Action_1_t7149A4E24DCDE38351BFE018FCC6DE62E88D5D12 * value)
	{
		___buttonTapped_10 = value;
		Il2CppCodeGenWriteBarrier((&___buttonTapped_10), value);
	}

	inline static int32_t get_offset_of_draggedOff_11() { return static_cast<int32_t>(offsetof(TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C, ___draggedOff_11)); }
	inline Action_1_t7149A4E24DCDE38351BFE018FCC6DE62E88D5D12 * get_draggedOff_11() const { return ___draggedOff_11; }
	inline Action_1_t7149A4E24DCDE38351BFE018FCC6DE62E88D5D12 ** get_address_of_draggedOff_11() { return &___draggedOff_11; }
	inline void set_draggedOff_11(Action_1_t7149A4E24DCDE38351BFE018FCC6DE62E88D5D12 * value)
	{
		___draggedOff_11 = value;
		Il2CppCodeGenWriteBarrier((&___draggedOff_11), value);
	}

	inline static int32_t get_offset_of_m_Tower_12() { return static_cast<int32_t>(offsetof(TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C, ___m_Tower_12)); }
	inline Tower_tCB73BE25139856154FE23524412B9114BA6260DF * get_m_Tower_12() const { return ___m_Tower_12; }
	inline Tower_tCB73BE25139856154FE23524412B9114BA6260DF ** get_address_of_m_Tower_12() { return &___m_Tower_12; }
	inline void set_m_Tower_12(Tower_tCB73BE25139856154FE23524412B9114BA6260DF * value)
	{
		___m_Tower_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tower_12), value);
	}

	inline static int32_t get_offset_of_m_Currency_13() { return static_cast<int32_t>(offsetof(TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C, ___m_Currency_13)); }
	inline Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05 * get_m_Currency_13() const { return ___m_Currency_13; }
	inline Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05 ** get_address_of_m_Currency_13() { return &___m_Currency_13; }
	inline void set_m_Currency_13(Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05 * value)
	{
		___m_Currency_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Currency_13), value);
	}

	inline static int32_t get_offset_of_m_RectTransform_14() { return static_cast<int32_t>(offsetof(TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C, ___m_RectTransform_14)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_14() const { return ___m_RectTransform_14; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_14() { return &___m_RectTransform_14; }
	inline void set_m_RectTransform_14(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERSPAWNBUTTON_T980372E20A7E18067070D7209BF454D083553E6C_H
#ifndef TOWERUI_T9E9DD8F174D01D378E2DC3292DE761996C32CDEF_H
#define TOWERUI_T9E9DD8F174D01D378E2DC3292DE761996C32CDEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Towers.UI.HUD.TowerUI
struct  TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Towers.UI.HUD.TowerUI::towerName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___towerName_4;
	// UnityEngine.UI.Text Towers.UI.HUD.TowerUI::description
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___description_5;
	// UnityEngine.UI.Text Towers.UI.HUD.TowerUI::upgradeDescription
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___upgradeDescription_6;
	// UnityEngine.UI.Button Towers.UI.HUD.TowerUI::sellButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___sellButton_7;
	// UnityEngine.UI.Button Towers.UI.HUD.TowerUI::upgradeButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___upgradeButton_8;
	// Towers.UI.HUD.TowerInfoDisplay Towers.UI.HUD.TowerUI::towerInfoDisplay
	TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6 * ___towerInfoDisplay_9;
	// UnityEngine.RectTransform Towers.UI.HUD.TowerUI::panelRectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___panelRectTransform_10;
	// UnityEngine.GameObject[] Towers.UI.HUD.TowerUI::confirmationButtons
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___confirmationButtons_11;
	// UnityEngine.Camera Towers.UI.HUD.TowerUI::m_GameCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_GameCamera_12;
	// Towers.Tower Towers.UI.HUD.TowerUI::m_Tower
	Tower_tCB73BE25139856154FE23524412B9114BA6260DF * ___m_Tower_13;
	// UnityEngine.Canvas Towers.UI.HUD.TowerUI::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_14;

public:
	inline static int32_t get_offset_of_towerName_4() { return static_cast<int32_t>(offsetof(TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF, ___towerName_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_towerName_4() const { return ___towerName_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_towerName_4() { return &___towerName_4; }
	inline void set_towerName_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___towerName_4 = value;
		Il2CppCodeGenWriteBarrier((&___towerName_4), value);
	}

	inline static int32_t get_offset_of_description_5() { return static_cast<int32_t>(offsetof(TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF, ___description_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_description_5() const { return ___description_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_description_5() { return &___description_5; }
	inline void set_description_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___description_5 = value;
		Il2CppCodeGenWriteBarrier((&___description_5), value);
	}

	inline static int32_t get_offset_of_upgradeDescription_6() { return static_cast<int32_t>(offsetof(TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF, ___upgradeDescription_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_upgradeDescription_6() const { return ___upgradeDescription_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_upgradeDescription_6() { return &___upgradeDescription_6; }
	inline void set_upgradeDescription_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___upgradeDescription_6 = value;
		Il2CppCodeGenWriteBarrier((&___upgradeDescription_6), value);
	}

	inline static int32_t get_offset_of_sellButton_7() { return static_cast<int32_t>(offsetof(TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF, ___sellButton_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_sellButton_7() const { return ___sellButton_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_sellButton_7() { return &___sellButton_7; }
	inline void set_sellButton_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___sellButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___sellButton_7), value);
	}

	inline static int32_t get_offset_of_upgradeButton_8() { return static_cast<int32_t>(offsetof(TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF, ___upgradeButton_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_upgradeButton_8() const { return ___upgradeButton_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_upgradeButton_8() { return &___upgradeButton_8; }
	inline void set_upgradeButton_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___upgradeButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___upgradeButton_8), value);
	}

	inline static int32_t get_offset_of_towerInfoDisplay_9() { return static_cast<int32_t>(offsetof(TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF, ___towerInfoDisplay_9)); }
	inline TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6 * get_towerInfoDisplay_9() const { return ___towerInfoDisplay_9; }
	inline TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6 ** get_address_of_towerInfoDisplay_9() { return &___towerInfoDisplay_9; }
	inline void set_towerInfoDisplay_9(TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6 * value)
	{
		___towerInfoDisplay_9 = value;
		Il2CppCodeGenWriteBarrier((&___towerInfoDisplay_9), value);
	}

	inline static int32_t get_offset_of_panelRectTransform_10() { return static_cast<int32_t>(offsetof(TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF, ___panelRectTransform_10)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_panelRectTransform_10() const { return ___panelRectTransform_10; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_panelRectTransform_10() { return &___panelRectTransform_10; }
	inline void set_panelRectTransform_10(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___panelRectTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___panelRectTransform_10), value);
	}

	inline static int32_t get_offset_of_confirmationButtons_11() { return static_cast<int32_t>(offsetof(TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF, ___confirmationButtons_11)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_confirmationButtons_11() const { return ___confirmationButtons_11; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_confirmationButtons_11() { return &___confirmationButtons_11; }
	inline void set_confirmationButtons_11(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___confirmationButtons_11 = value;
		Il2CppCodeGenWriteBarrier((&___confirmationButtons_11), value);
	}

	inline static int32_t get_offset_of_m_GameCamera_12() { return static_cast<int32_t>(offsetof(TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF, ___m_GameCamera_12)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_GameCamera_12() const { return ___m_GameCamera_12; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_GameCamera_12() { return &___m_GameCamera_12; }
	inline void set_m_GameCamera_12(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_GameCamera_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameCamera_12), value);
	}

	inline static int32_t get_offset_of_m_Tower_13() { return static_cast<int32_t>(offsetof(TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF, ___m_Tower_13)); }
	inline Tower_tCB73BE25139856154FE23524412B9114BA6260DF * get_m_Tower_13() const { return ___m_Tower_13; }
	inline Tower_tCB73BE25139856154FE23524412B9114BA6260DF ** get_address_of_m_Tower_13() { return &___m_Tower_13; }
	inline void set_m_Tower_13(Tower_tCB73BE25139856154FE23524412B9114BA6260DF * value)
	{
		___m_Tower_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tower_13), value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF, ___m_Canvas_14)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERUI_T9E9DD8F174D01D378E2DC3292DE761996C32CDEF_H
#ifndef CANTARGET_T2CFF066CBA8508F76446938BE3B126AF6DDA8AF0_H
#define CANTARGET_T2CFF066CBA8508F76446938BE3B126AF6DDA8AF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CanTarget
struct  CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0  : public Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D
{
public:
	// UnityEngine.Transform CanTarget::targetTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___targetTransform_11;
	// UnityEngine.Vector3 CanTarget::m_CurrentPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_CurrentPosition_12;
	// UnityEngine.Vector3 CanTarget::m_PreviousPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_PreviousPosition_13;
	// UnityEngine.Vector3 CanTarget::<velocity>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CvelocityU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_targetTransform_11() { return static_cast<int32_t>(offsetof(CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0, ___targetTransform_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_targetTransform_11() const { return ___targetTransform_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_targetTransform_11() { return &___targetTransform_11; }
	inline void set_targetTransform_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___targetTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___targetTransform_11), value);
	}

	inline static int32_t get_offset_of_m_CurrentPosition_12() { return static_cast<int32_t>(offsetof(CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0, ___m_CurrentPosition_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_CurrentPosition_12() const { return ___m_CurrentPosition_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_CurrentPosition_12() { return &___m_CurrentPosition_12; }
	inline void set_m_CurrentPosition_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_CurrentPosition_12 = value;
	}

	inline static int32_t get_offset_of_m_PreviousPosition_13() { return static_cast<int32_t>(offsetof(CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0, ___m_PreviousPosition_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_PreviousPosition_13() const { return ___m_PreviousPosition_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_PreviousPosition_13() { return &___m_PreviousPosition_13; }
	inline void set_m_PreviousPosition_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_PreviousPosition_13 = value;
	}

	inline static int32_t get_offset_of_U3CvelocityU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0, ___U3CvelocityU3Ek__BackingField_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CvelocityU3Ek__BackingField_14() const { return ___U3CvelocityU3Ek__BackingField_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CvelocityU3Ek__BackingField_14() { return &___U3CvelocityU3Ek__BackingField_14; }
	inline void set_U3CvelocityU3Ek__BackingField_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CvelocityU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANTARGET_T2CFF066CBA8508F76446938BE3B126AF6DDA8AF0_H
#ifndef CAMERAINPUTSCHEME_TEB16782DA7575673162607368A520FB02D2739E5_H
#define CAMERAINPUTSCHEME_TEB16782DA7575673162607368A520FB02D2739E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.CameraInputScheme
struct  CameraInputScheme_tEB16782DA7575673162607368A520FB02D2739E5  : public InputScheme_t78F140C9CD80326F0D459CE65156EF66E3218678
{
public:
	// Core.Camera.CameraRig Core.Input.CameraInputScheme::cameraRig
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4 * ___cameraRig_4;
	// System.Single Core.Input.CameraInputScheme::nearZoomPanSpeedModifier
	float ___nearZoomPanSpeedModifier_5;

public:
	inline static int32_t get_offset_of_cameraRig_4() { return static_cast<int32_t>(offsetof(CameraInputScheme_tEB16782DA7575673162607368A520FB02D2739E5, ___cameraRig_4)); }
	inline CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4 * get_cameraRig_4() const { return ___cameraRig_4; }
	inline CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4 ** get_address_of_cameraRig_4() { return &___cameraRig_4; }
	inline void set_cameraRig_4(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4 * value)
	{
		___cameraRig_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRig_4), value);
	}

	inline static int32_t get_offset_of_nearZoomPanSpeedModifier_5() { return static_cast<int32_t>(offsetof(CameraInputScheme_tEB16782DA7575673162607368A520FB02D2739E5, ___nearZoomPanSpeedModifier_5)); }
	inline float get_nearZoomPanSpeedModifier_5() const { return ___nearZoomPanSpeedModifier_5; }
	inline float* get_address_of_nearZoomPanSpeedModifier_5() { return &___nearZoomPanSpeedModifier_5; }
	inline void set_nearZoomPanSpeedModifier_5(float value)
	{
		___nearZoomPanSpeedModifier_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAINPUTSCHEME_TEB16782DA7575673162607368A520FB02D2739E5_H
#ifndef CURRENCYMANAGER_TB78B180A3E420C837BD37D7F480D2A1678CD4497_H
#define CURRENCYMANAGER_TB78B180A3E420C837BD37D7F480D2A1678CD4497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Economy.CurrencyManager
struct  CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497  : public Singleton_1_tB8BC3FEE6CD31F56AF6C63704CA5A47016487BB1
{
public:
	// System.Int32 Economy.CurrencyManager::startingCurrency
	int32_t ___startingCurrency_5;
	// System.Int32 Economy.CurrencyManager::constantCurrencyAddition
	int32_t ___constantCurrencyAddition_6;
	// System.Single Economy.CurrencyManager::constantCurrencyGainRate
	float ___constantCurrencyGainRate_7;
	// System.Action`1<Economy.CurrencyChangeInfo> Economy.CurrencyManager::currencyChanged
	Action_1_t1C2B006BB1030E36B3A42A958F7832065CAC6582 * ___currencyChanged_8;
	// Core.Utilities.RepeatingTimer Economy.CurrencyManager::m_GainTimer
	RepeatingTimer_tFEABA0EAF35D64F27E55652FD50AF98799EDC468 * ___m_GainTimer_9;
	// Economy.Currency Economy.CurrencyManager::<currency>k__BackingField
	Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05 * ___U3CcurrencyU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_startingCurrency_5() { return static_cast<int32_t>(offsetof(CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497, ___startingCurrency_5)); }
	inline int32_t get_startingCurrency_5() const { return ___startingCurrency_5; }
	inline int32_t* get_address_of_startingCurrency_5() { return &___startingCurrency_5; }
	inline void set_startingCurrency_5(int32_t value)
	{
		___startingCurrency_5 = value;
	}

	inline static int32_t get_offset_of_constantCurrencyAddition_6() { return static_cast<int32_t>(offsetof(CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497, ___constantCurrencyAddition_6)); }
	inline int32_t get_constantCurrencyAddition_6() const { return ___constantCurrencyAddition_6; }
	inline int32_t* get_address_of_constantCurrencyAddition_6() { return &___constantCurrencyAddition_6; }
	inline void set_constantCurrencyAddition_6(int32_t value)
	{
		___constantCurrencyAddition_6 = value;
	}

	inline static int32_t get_offset_of_constantCurrencyGainRate_7() { return static_cast<int32_t>(offsetof(CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497, ___constantCurrencyGainRate_7)); }
	inline float get_constantCurrencyGainRate_7() const { return ___constantCurrencyGainRate_7; }
	inline float* get_address_of_constantCurrencyGainRate_7() { return &___constantCurrencyGainRate_7; }
	inline void set_constantCurrencyGainRate_7(float value)
	{
		___constantCurrencyGainRate_7 = value;
	}

	inline static int32_t get_offset_of_currencyChanged_8() { return static_cast<int32_t>(offsetof(CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497, ___currencyChanged_8)); }
	inline Action_1_t1C2B006BB1030E36B3A42A958F7832065CAC6582 * get_currencyChanged_8() const { return ___currencyChanged_8; }
	inline Action_1_t1C2B006BB1030E36B3A42A958F7832065CAC6582 ** get_address_of_currencyChanged_8() { return &___currencyChanged_8; }
	inline void set_currencyChanged_8(Action_1_t1C2B006BB1030E36B3A42A958F7832065CAC6582 * value)
	{
		___currencyChanged_8 = value;
		Il2CppCodeGenWriteBarrier((&___currencyChanged_8), value);
	}

	inline static int32_t get_offset_of_m_GainTimer_9() { return static_cast<int32_t>(offsetof(CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497, ___m_GainTimer_9)); }
	inline RepeatingTimer_tFEABA0EAF35D64F27E55652FD50AF98799EDC468 * get_m_GainTimer_9() const { return ___m_GainTimer_9; }
	inline RepeatingTimer_tFEABA0EAF35D64F27E55652FD50AF98799EDC468 ** get_address_of_m_GainTimer_9() { return &___m_GainTimer_9; }
	inline void set_m_GainTimer_9(RepeatingTimer_tFEABA0EAF35D64F27E55652FD50AF98799EDC468 * value)
	{
		___m_GainTimer_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_GainTimer_9), value);
	}

	inline static int32_t get_offset_of_U3CcurrencyU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497, ___U3CcurrencyU3Ek__BackingField_10)); }
	inline Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05 * get_U3CcurrencyU3Ek__BackingField_10() const { return ___U3CcurrencyU3Ek__BackingField_10; }
	inline Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05 ** get_address_of_U3CcurrencyU3Ek__BackingField_10() { return &___U3CcurrencyU3Ek__BackingField_10; }
	inline void set_U3CcurrencyU3Ek__BackingField_10(Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05 * value)
	{
		___U3CcurrencyU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrencyU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENCYMANAGER_TB78B180A3E420C837BD37D7F480D2A1678CD4497_H
#ifndef FIXEDNODESELECTOR_TA44D55626F235F2A427889E2F7F968E76EEC7A43_H
#define FIXEDNODESELECTOR_TA44D55626F235F2A427889E2F7F968E76EEC7A43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Nodes.FixedNodeSelector
struct  FixedNodeSelector_tA44D55626F235F2A427889E2F7F968E76EEC7A43  : public NodeSelector_tFEA86AE0908D36229D2BBA57FDF7AEDEC84304D9
{
public:
	// System.Int32 Pathfinding.Nodes.FixedNodeSelector::m_NodeIndex
	int32_t ___m_NodeIndex_5;

public:
	inline static int32_t get_offset_of_m_NodeIndex_5() { return static_cast<int32_t>(offsetof(FixedNodeSelector_tA44D55626F235F2A427889E2F7F968E76EEC7A43, ___m_NodeIndex_5)); }
	inline int32_t get_m_NodeIndex_5() const { return ___m_NodeIndex_5; }
	inline int32_t* get_address_of_m_NodeIndex_5() { return &___m_NodeIndex_5; }
	inline void set_m_NodeIndex_5(int32_t value)
	{
		___m_NodeIndex_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDNODESELECTOR_TA44D55626F235F2A427889E2F7F968E76EEC7A43_H
#ifndef RANDOMNODESELECTOR_TA96B6649EC217A254C3A87C8E808C86D5E3F0AD2_H
#define RANDOMNODESELECTOR_TA96B6649EC217A254C3A87C8E808C86D5E3F0AD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Nodes.RandomNodeSelector
struct  RandomNodeSelector_tA96B6649EC217A254C3A87C8E808C86D5E3F0AD2  : public NodeSelector_tFEA86AE0908D36229D2BBA57FDF7AEDEC84304D9
{
public:
	// System.Int32 Pathfinding.Nodes.RandomNodeSelector::m_WeightSum
	int32_t ___m_WeightSum_5;

public:
	inline static int32_t get_offset_of_m_WeightSum_5() { return static_cast<int32_t>(offsetof(RandomNodeSelector_tA96B6649EC217A254C3A87C8E808C86D5E3F0AD2, ___m_WeightSum_5)); }
	inline int32_t get_m_WeightSum_5() const { return ___m_WeightSum_5; }
	inline int32_t* get_address_of_m_WeightSum_5() { return &___m_WeightSum_5; }
	inline void set_m_WeightSum_5(int32_t value)
	{
		___m_WeightSum_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMNODESELECTOR_TA96B6649EC217A254C3A87C8E808C86D5E3F0AD2_H
#ifndef POOLABLEEFFECT_T345C497F0CDBEA3D2D5DB85CBBCAB596C6E5C66B_H
#define POOLABLEEFFECT_T345C497F0CDBEA3D2D5DB85CBBCAB596C6E5C66B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerDefense.Effects.PoolableEffect
struct  PoolableEffect_t345C497F0CDBEA3D2D5DB85CBBCAB596C6E5C66B  : public Poolable_tBBCA15FD94FB330CD5A30C65692500ED9BA65D2F
{
public:
	// System.Collections.Generic.List`1<UnityEngine.ParticleSystem> TowerDefense.Effects.PoolableEffect::m_Systems
	List_1_tFA660ACCBB75B7DE99651E0CED2A469BEEEF76CE * ___m_Systems_6;
	// System.Collections.Generic.List`1<UnityEngine.TrailRenderer> TowerDefense.Effects.PoolableEffect::m_Trails
	List_1_tE0D9DFFB62228152DD6534EEA186AFCF5BC079CC * ___m_Trails_7;
	// System.Boolean TowerDefense.Effects.PoolableEffect::m_EffectsEnabled
	bool ___m_EffectsEnabled_8;

public:
	inline static int32_t get_offset_of_m_Systems_6() { return static_cast<int32_t>(offsetof(PoolableEffect_t345C497F0CDBEA3D2D5DB85CBBCAB596C6E5C66B, ___m_Systems_6)); }
	inline List_1_tFA660ACCBB75B7DE99651E0CED2A469BEEEF76CE * get_m_Systems_6() const { return ___m_Systems_6; }
	inline List_1_tFA660ACCBB75B7DE99651E0CED2A469BEEEF76CE ** get_address_of_m_Systems_6() { return &___m_Systems_6; }
	inline void set_m_Systems_6(List_1_tFA660ACCBB75B7DE99651E0CED2A469BEEEF76CE * value)
	{
		___m_Systems_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Systems_6), value);
	}

	inline static int32_t get_offset_of_m_Trails_7() { return static_cast<int32_t>(offsetof(PoolableEffect_t345C497F0CDBEA3D2D5DB85CBBCAB596C6E5C66B, ___m_Trails_7)); }
	inline List_1_tE0D9DFFB62228152DD6534EEA186AFCF5BC079CC * get_m_Trails_7() const { return ___m_Trails_7; }
	inline List_1_tE0D9DFFB62228152DD6534EEA186AFCF5BC079CC ** get_address_of_m_Trails_7() { return &___m_Trails_7; }
	inline void set_m_Trails_7(List_1_tE0D9DFFB62228152DD6534EEA186AFCF5BC079CC * value)
	{
		___m_Trails_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Trails_7), value);
	}

	inline static int32_t get_offset_of_m_EffectsEnabled_8() { return static_cast<int32_t>(offsetof(PoolableEffect_t345C497F0CDBEA3D2D5DB85CBBCAB596C6E5C66B, ___m_EffectsEnabled_8)); }
	inline bool get_m_EffectsEnabled_8() const { return ___m_EffectsEnabled_8; }
	inline bool* get_address_of_m_EffectsEnabled_8() { return &___m_EffectsEnabled_8; }
	inline void set_m_EffectsEnabled_8(bool value)
	{
		___m_EffectsEnabled_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLABLEEFFECT_T345C497F0CDBEA3D2D5DB85CBBCAB596C6E5C66B_H
#ifndef BUILDER_TF7414C0E077162F0CE605E009B1013AD98732805_H
#define BUILDER_TF7414C0E077162F0CE605E009B1013AD98732805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Towers.UI.Builder
struct  Builder_tF7414C0E077162F0CE605E009B1013AD98732805  : public Singleton_1_t676EF679D4D9890C1966EDB733B76F47314FB958
{
public:
	// Towers.UI.Builder/State Towers.UI.Builder::<state>k__BackingField
	int32_t ___U3CstateU3Ek__BackingField_5;
	// UnityEngine.LayerMask Towers.UI.Builder::placementAreaMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___placementAreaMask_6;
	// UnityEngine.LayerMask Towers.UI.Builder::towerSelectionLayer
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___towerSelectionLayer_7;
	// UnityEngine.LayerMask Towers.UI.Builder::ghostWorldPlacementMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___ghostWorldPlacementMask_8;
	// System.Single Towers.UI.Builder::sphereCastRadius
	float ___sphereCastRadius_9;
	// Towers.UI.HUD.TowerSpawnButton[] Towers.UI.Builder::towerSpawnButtons
	TowerSpawnButtonU5BU5D_tEC7065232C15DE6D9589F8404EEE4601C16AF697* ___towerSpawnButtons_10;
	// Towers.UI.HUD.TowerUI Towers.UI.Builder::towerUI
	TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF * ___towerUI_11;
	// Towers.UI.HUD.BuildInfoUI Towers.UI.Builder::buildInfoUI
	BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF * ___buildInfoUI_12;
	// System.Action`2<Towers.UI.Builder/State,Towers.UI.Builder/State> Towers.UI.Builder::stateChanged
	Action_2_t202C402486F1480CE5A702E273710C3EF1006EDF * ___stateChanged_13;
	// System.Action Towers.UI.Builder::ghostBecameValid
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ghostBecameValid_14;
	// System.Action`1<Towers.Tower> Towers.UI.Builder::selectionChanged
	Action_1_t7149A4E24DCDE38351BFE018FCC6DE62E88D5D12 * ___selectionChanged_15;
	// Placement.IPlacementArea Towers.UI.Builder::m_CurrentArea
	RuntimeObject* ___m_CurrentArea_16;
	// Core.Utilities.IntVector2 Towers.UI.Builder::m_GridPosition
	IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  ___m_GridPosition_17;
	// UnityEngine.Camera Towers.UI.Builder::m_Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_Camera_18;
	// Towers.TowerPlacementGhost Towers.UI.Builder::m_CurrentTower
	TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7 * ___m_CurrentTower_19;
	// System.Boolean Towers.UI.Builder::m_GhostPlacementPossible
	bool ___m_GhostPlacementPossible_20;
	// Towers.Tower Towers.UI.Builder::<currentSelectedTower>k__BackingField
	Tower_tCB73BE25139856154FE23524412B9114BA6260DF * ___U3CcurrentSelectedTowerU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_U3CstateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___U3CstateU3Ek__BackingField_5)); }
	inline int32_t get_U3CstateU3Ek__BackingField_5() const { return ___U3CstateU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CstateU3Ek__BackingField_5() { return &___U3CstateU3Ek__BackingField_5; }
	inline void set_U3CstateU3Ek__BackingField_5(int32_t value)
	{
		___U3CstateU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_placementAreaMask_6() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___placementAreaMask_6)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_placementAreaMask_6() const { return ___placementAreaMask_6; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_placementAreaMask_6() { return &___placementAreaMask_6; }
	inline void set_placementAreaMask_6(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___placementAreaMask_6 = value;
	}

	inline static int32_t get_offset_of_towerSelectionLayer_7() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___towerSelectionLayer_7)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_towerSelectionLayer_7() const { return ___towerSelectionLayer_7; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_towerSelectionLayer_7() { return &___towerSelectionLayer_7; }
	inline void set_towerSelectionLayer_7(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___towerSelectionLayer_7 = value;
	}

	inline static int32_t get_offset_of_ghostWorldPlacementMask_8() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___ghostWorldPlacementMask_8)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_ghostWorldPlacementMask_8() const { return ___ghostWorldPlacementMask_8; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_ghostWorldPlacementMask_8() { return &___ghostWorldPlacementMask_8; }
	inline void set_ghostWorldPlacementMask_8(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___ghostWorldPlacementMask_8 = value;
	}

	inline static int32_t get_offset_of_sphereCastRadius_9() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___sphereCastRadius_9)); }
	inline float get_sphereCastRadius_9() const { return ___sphereCastRadius_9; }
	inline float* get_address_of_sphereCastRadius_9() { return &___sphereCastRadius_9; }
	inline void set_sphereCastRadius_9(float value)
	{
		___sphereCastRadius_9 = value;
	}

	inline static int32_t get_offset_of_towerSpawnButtons_10() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___towerSpawnButtons_10)); }
	inline TowerSpawnButtonU5BU5D_tEC7065232C15DE6D9589F8404EEE4601C16AF697* get_towerSpawnButtons_10() const { return ___towerSpawnButtons_10; }
	inline TowerSpawnButtonU5BU5D_tEC7065232C15DE6D9589F8404EEE4601C16AF697** get_address_of_towerSpawnButtons_10() { return &___towerSpawnButtons_10; }
	inline void set_towerSpawnButtons_10(TowerSpawnButtonU5BU5D_tEC7065232C15DE6D9589F8404EEE4601C16AF697* value)
	{
		___towerSpawnButtons_10 = value;
		Il2CppCodeGenWriteBarrier((&___towerSpawnButtons_10), value);
	}

	inline static int32_t get_offset_of_towerUI_11() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___towerUI_11)); }
	inline TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF * get_towerUI_11() const { return ___towerUI_11; }
	inline TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF ** get_address_of_towerUI_11() { return &___towerUI_11; }
	inline void set_towerUI_11(TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF * value)
	{
		___towerUI_11 = value;
		Il2CppCodeGenWriteBarrier((&___towerUI_11), value);
	}

	inline static int32_t get_offset_of_buildInfoUI_12() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___buildInfoUI_12)); }
	inline BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF * get_buildInfoUI_12() const { return ___buildInfoUI_12; }
	inline BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF ** get_address_of_buildInfoUI_12() { return &___buildInfoUI_12; }
	inline void set_buildInfoUI_12(BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF * value)
	{
		___buildInfoUI_12 = value;
		Il2CppCodeGenWriteBarrier((&___buildInfoUI_12), value);
	}

	inline static int32_t get_offset_of_stateChanged_13() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___stateChanged_13)); }
	inline Action_2_t202C402486F1480CE5A702E273710C3EF1006EDF * get_stateChanged_13() const { return ___stateChanged_13; }
	inline Action_2_t202C402486F1480CE5A702E273710C3EF1006EDF ** get_address_of_stateChanged_13() { return &___stateChanged_13; }
	inline void set_stateChanged_13(Action_2_t202C402486F1480CE5A702E273710C3EF1006EDF * value)
	{
		___stateChanged_13 = value;
		Il2CppCodeGenWriteBarrier((&___stateChanged_13), value);
	}

	inline static int32_t get_offset_of_ghostBecameValid_14() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___ghostBecameValid_14)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ghostBecameValid_14() const { return ___ghostBecameValid_14; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ghostBecameValid_14() { return &___ghostBecameValid_14; }
	inline void set_ghostBecameValid_14(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ghostBecameValid_14 = value;
		Il2CppCodeGenWriteBarrier((&___ghostBecameValid_14), value);
	}

	inline static int32_t get_offset_of_selectionChanged_15() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___selectionChanged_15)); }
	inline Action_1_t7149A4E24DCDE38351BFE018FCC6DE62E88D5D12 * get_selectionChanged_15() const { return ___selectionChanged_15; }
	inline Action_1_t7149A4E24DCDE38351BFE018FCC6DE62E88D5D12 ** get_address_of_selectionChanged_15() { return &___selectionChanged_15; }
	inline void set_selectionChanged_15(Action_1_t7149A4E24DCDE38351BFE018FCC6DE62E88D5D12 * value)
	{
		___selectionChanged_15 = value;
		Il2CppCodeGenWriteBarrier((&___selectionChanged_15), value);
	}

	inline static int32_t get_offset_of_m_CurrentArea_16() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___m_CurrentArea_16)); }
	inline RuntimeObject* get_m_CurrentArea_16() const { return ___m_CurrentArea_16; }
	inline RuntimeObject** get_address_of_m_CurrentArea_16() { return &___m_CurrentArea_16; }
	inline void set_m_CurrentArea_16(RuntimeObject* value)
	{
		___m_CurrentArea_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentArea_16), value);
	}

	inline static int32_t get_offset_of_m_GridPosition_17() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___m_GridPosition_17)); }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  get_m_GridPosition_17() const { return ___m_GridPosition_17; }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9 * get_address_of_m_GridPosition_17() { return &___m_GridPosition_17; }
	inline void set_m_GridPosition_17(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  value)
	{
		___m_GridPosition_17 = value;
	}

	inline static int32_t get_offset_of_m_Camera_18() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___m_Camera_18)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_Camera_18() const { return ___m_Camera_18; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_Camera_18() { return &___m_Camera_18; }
	inline void set_m_Camera_18(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_Camera_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_18), value);
	}

	inline static int32_t get_offset_of_m_CurrentTower_19() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___m_CurrentTower_19)); }
	inline TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7 * get_m_CurrentTower_19() const { return ___m_CurrentTower_19; }
	inline TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7 ** get_address_of_m_CurrentTower_19() { return &___m_CurrentTower_19; }
	inline void set_m_CurrentTower_19(TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7 * value)
	{
		___m_CurrentTower_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentTower_19), value);
	}

	inline static int32_t get_offset_of_m_GhostPlacementPossible_20() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___m_GhostPlacementPossible_20)); }
	inline bool get_m_GhostPlacementPossible_20() const { return ___m_GhostPlacementPossible_20; }
	inline bool* get_address_of_m_GhostPlacementPossible_20() { return &___m_GhostPlacementPossible_20; }
	inline void set_m_GhostPlacementPossible_20(bool value)
	{
		___m_GhostPlacementPossible_20 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentSelectedTowerU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(Builder_tF7414C0E077162F0CE605E009B1013AD98732805, ___U3CcurrentSelectedTowerU3Ek__BackingField_21)); }
	inline Tower_tCB73BE25139856154FE23524412B9114BA6260DF * get_U3CcurrentSelectedTowerU3Ek__BackingField_21() const { return ___U3CcurrentSelectedTowerU3Ek__BackingField_21; }
	inline Tower_tCB73BE25139856154FE23524412B9114BA6260DF ** get_address_of_U3CcurrentSelectedTowerU3Ek__BackingField_21() { return &___U3CcurrentSelectedTowerU3Ek__BackingField_21; }
	inline void set_U3CcurrentSelectedTowerU3Ek__BackingField_21(Tower_tCB73BE25139856154FE23524412B9114BA6260DF * value)
	{
		___U3CcurrentSelectedTowerU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentSelectedTowerU3Ek__BackingField_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_TF7414C0E077162F0CE605E009B1013AD98732805_H
#ifndef RADIUSVISUALIZERCONTROLLER_TC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75_H
#define RADIUSVISUALIZERCONTROLLER_TC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Towers.UI.RadiusVisualizerController
struct  RadiusVisualizerController_tC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75  : public Singleton_1_t9E84971219B4F5C6D76B382E6432C7ED72BE346C
{
public:
	// UnityEngine.GameObject Towers.UI.RadiusVisualizerController::radiusVisualizerPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___radiusVisualizerPrefab_5;
	// System.Single Towers.UI.RadiusVisualizerController::radiusVisualizerHeight
	float ___radiusVisualizerHeight_6;
	// UnityEngine.Vector3 Towers.UI.RadiusVisualizerController::localEuler
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___localEuler_7;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Towers.UI.RadiusVisualizerController::m_RadiusVisualizers
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ___m_RadiusVisualizers_8;

public:
	inline static int32_t get_offset_of_radiusVisualizerPrefab_5() { return static_cast<int32_t>(offsetof(RadiusVisualizerController_tC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75, ___radiusVisualizerPrefab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_radiusVisualizerPrefab_5() const { return ___radiusVisualizerPrefab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_radiusVisualizerPrefab_5() { return &___radiusVisualizerPrefab_5; }
	inline void set_radiusVisualizerPrefab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___radiusVisualizerPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___radiusVisualizerPrefab_5), value);
	}

	inline static int32_t get_offset_of_radiusVisualizerHeight_6() { return static_cast<int32_t>(offsetof(RadiusVisualizerController_tC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75, ___radiusVisualizerHeight_6)); }
	inline float get_radiusVisualizerHeight_6() const { return ___radiusVisualizerHeight_6; }
	inline float* get_address_of_radiusVisualizerHeight_6() { return &___radiusVisualizerHeight_6; }
	inline void set_radiusVisualizerHeight_6(float value)
	{
		___radiusVisualizerHeight_6 = value;
	}

	inline static int32_t get_offset_of_localEuler_7() { return static_cast<int32_t>(offsetof(RadiusVisualizerController_tC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75, ___localEuler_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_localEuler_7() const { return ___localEuler_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_localEuler_7() { return &___localEuler_7; }
	inline void set_localEuler_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___localEuler_7 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVisualizers_8() { return static_cast<int32_t>(offsetof(RadiusVisualizerController_tC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75, ___m_RadiusVisualizers_8)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get_m_RadiusVisualizers_8() const { return ___m_RadiusVisualizers_8; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of_m_RadiusVisualizers_8() { return &___m_RadiusVisualizers_8; }
	inline void set_m_RadiusVisualizers_8(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		___m_RadiusVisualizers_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_RadiusVisualizers_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIUSVISUALIZERCONTROLLER_TC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75_H
#ifndef KEYBOARDMOUSEINPUT_T695D0A615AD59B9BBB1C8FBA24F5236117D9813E_H
#define KEYBOARDMOUSEINPUT_T695D0A615AD59B9BBB1C8FBA24F5236117D9813E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.KeyboardMouseInput
struct  KeyboardMouseInput_t695D0A615AD59B9BBB1C8FBA24F5236117D9813E  : public CameraInputScheme_tEB16782DA7575673162607368A520FB02D2739E5
{
public:
	// System.Single Core.Input.KeyboardMouseInput::screenPanThreshold
	float ___screenPanThreshold_6;
	// System.Single Core.Input.KeyboardMouseInput::mouseEdgePanSpeed
	float ___mouseEdgePanSpeed_7;
	// System.Single Core.Input.KeyboardMouseInput::mouseRmbPanSpeed
	float ___mouseRmbPanSpeed_8;

public:
	inline static int32_t get_offset_of_screenPanThreshold_6() { return static_cast<int32_t>(offsetof(KeyboardMouseInput_t695D0A615AD59B9BBB1C8FBA24F5236117D9813E, ___screenPanThreshold_6)); }
	inline float get_screenPanThreshold_6() const { return ___screenPanThreshold_6; }
	inline float* get_address_of_screenPanThreshold_6() { return &___screenPanThreshold_6; }
	inline void set_screenPanThreshold_6(float value)
	{
		___screenPanThreshold_6 = value;
	}

	inline static int32_t get_offset_of_mouseEdgePanSpeed_7() { return static_cast<int32_t>(offsetof(KeyboardMouseInput_t695D0A615AD59B9BBB1C8FBA24F5236117D9813E, ___mouseEdgePanSpeed_7)); }
	inline float get_mouseEdgePanSpeed_7() const { return ___mouseEdgePanSpeed_7; }
	inline float* get_address_of_mouseEdgePanSpeed_7() { return &___mouseEdgePanSpeed_7; }
	inline void set_mouseEdgePanSpeed_7(float value)
	{
		___mouseEdgePanSpeed_7 = value;
	}

	inline static int32_t get_offset_of_mouseRmbPanSpeed_8() { return static_cast<int32_t>(offsetof(KeyboardMouseInput_t695D0A615AD59B9BBB1C8FBA24F5236117D9813E, ___mouseRmbPanSpeed_8)); }
	inline float get_mouseRmbPanSpeed_8() const { return ___mouseRmbPanSpeed_8; }
	inline float* get_address_of_mouseRmbPanSpeed_8() { return &___mouseRmbPanSpeed_8; }
	inline void set_mouseRmbPanSpeed_8(float value)
	{
		___mouseRmbPanSpeed_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDMOUSEINPUT_T695D0A615AD59B9BBB1C8FBA24F5236117D9813E_H
#ifndef AGENT_T0C7954C99BD6D63C1824532878279573DE904927_H
#define AGENT_T0C7954C99BD6D63C1824532878279573DE904927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Agents.Agent
struct  Agent_t0C7954C99BD6D63C1824532878279573DE904927  : public CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0
{
public:
	// System.Action`1<Pathfinding.Nodes.Node> Pathfinding.Agents.Agent::destinationReached
	Action_1_t80C0367B25255284C38F37E49F3BA4F972110C44 * ___destinationReached_15;
	// UnityEngine.Vector3 Pathfinding.Agents.Agent::appliedEffectOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___appliedEffectOffset_16;
	// System.Single Pathfinding.Agents.Agent::appliedEffectScale
	float ___appliedEffectScale_17;
	// UnityEngine.AI.NavMeshAgent Pathfinding.Agents.Agent::m_NavMeshAgent
	NavMeshAgent_tD93BAD8854B394AA1D372193F21154E94CA84BEB * ___m_NavMeshAgent_18;
	// Pathfinding.Nodes.Node Pathfinding.Agents.Agent::m_CurrentNode
	Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 * ___m_CurrentNode_19;
	// UnityEngine.Vector3 Pathfinding.Agents.Agent::m_Destination
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Destination_20;
	// Pathfinding.Agents.Agent/State Pathfinding.Agents.Agent::<state>k__BackingField
	int32_t ___U3CstateU3Ek__BackingField_21;
	// System.Single Pathfinding.Agents.Agent::<originalMovementSpeed>k__BackingField
	float ___U3CoriginalMovementSpeedU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_destinationReached_15() { return static_cast<int32_t>(offsetof(Agent_t0C7954C99BD6D63C1824532878279573DE904927, ___destinationReached_15)); }
	inline Action_1_t80C0367B25255284C38F37E49F3BA4F972110C44 * get_destinationReached_15() const { return ___destinationReached_15; }
	inline Action_1_t80C0367B25255284C38F37E49F3BA4F972110C44 ** get_address_of_destinationReached_15() { return &___destinationReached_15; }
	inline void set_destinationReached_15(Action_1_t80C0367B25255284C38F37E49F3BA4F972110C44 * value)
	{
		___destinationReached_15 = value;
		Il2CppCodeGenWriteBarrier((&___destinationReached_15), value);
	}

	inline static int32_t get_offset_of_appliedEffectOffset_16() { return static_cast<int32_t>(offsetof(Agent_t0C7954C99BD6D63C1824532878279573DE904927, ___appliedEffectOffset_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_appliedEffectOffset_16() const { return ___appliedEffectOffset_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_appliedEffectOffset_16() { return &___appliedEffectOffset_16; }
	inline void set_appliedEffectOffset_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___appliedEffectOffset_16 = value;
	}

	inline static int32_t get_offset_of_appliedEffectScale_17() { return static_cast<int32_t>(offsetof(Agent_t0C7954C99BD6D63C1824532878279573DE904927, ___appliedEffectScale_17)); }
	inline float get_appliedEffectScale_17() const { return ___appliedEffectScale_17; }
	inline float* get_address_of_appliedEffectScale_17() { return &___appliedEffectScale_17; }
	inline void set_appliedEffectScale_17(float value)
	{
		___appliedEffectScale_17 = value;
	}

	inline static int32_t get_offset_of_m_NavMeshAgent_18() { return static_cast<int32_t>(offsetof(Agent_t0C7954C99BD6D63C1824532878279573DE904927, ___m_NavMeshAgent_18)); }
	inline NavMeshAgent_tD93BAD8854B394AA1D372193F21154E94CA84BEB * get_m_NavMeshAgent_18() const { return ___m_NavMeshAgent_18; }
	inline NavMeshAgent_tD93BAD8854B394AA1D372193F21154E94CA84BEB ** get_address_of_m_NavMeshAgent_18() { return &___m_NavMeshAgent_18; }
	inline void set_m_NavMeshAgent_18(NavMeshAgent_tD93BAD8854B394AA1D372193F21154E94CA84BEB * value)
	{
		___m_NavMeshAgent_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_NavMeshAgent_18), value);
	}

	inline static int32_t get_offset_of_m_CurrentNode_19() { return static_cast<int32_t>(offsetof(Agent_t0C7954C99BD6D63C1824532878279573DE904927, ___m_CurrentNode_19)); }
	inline Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 * get_m_CurrentNode_19() const { return ___m_CurrentNode_19; }
	inline Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 ** get_address_of_m_CurrentNode_19() { return &___m_CurrentNode_19; }
	inline void set_m_CurrentNode_19(Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 * value)
	{
		___m_CurrentNode_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentNode_19), value);
	}

	inline static int32_t get_offset_of_m_Destination_20() { return static_cast<int32_t>(offsetof(Agent_t0C7954C99BD6D63C1824532878279573DE904927, ___m_Destination_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Destination_20() const { return ___m_Destination_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Destination_20() { return &___m_Destination_20; }
	inline void set_m_Destination_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Destination_20 = value;
	}

	inline static int32_t get_offset_of_U3CstateU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(Agent_t0C7954C99BD6D63C1824532878279573DE904927, ___U3CstateU3Ek__BackingField_21)); }
	inline int32_t get_U3CstateU3Ek__BackingField_21() const { return ___U3CstateU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CstateU3Ek__BackingField_21() { return &___U3CstateU3Ek__BackingField_21; }
	inline void set_U3CstateU3Ek__BackingField_21(int32_t value)
	{
		___U3CstateU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CoriginalMovementSpeedU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(Agent_t0C7954C99BD6D63C1824532878279573DE904927, ___U3CoriginalMovementSpeedU3Ek__BackingField_22)); }
	inline float get_U3CoriginalMovementSpeedU3Ek__BackingField_22() const { return ___U3CoriginalMovementSpeedU3Ek__BackingField_22; }
	inline float* get_address_of_U3CoriginalMovementSpeedU3Ek__BackingField_22() { return &___U3CoriginalMovementSpeedU3Ek__BackingField_22; }
	inline void set_U3CoriginalMovementSpeedU3Ek__BackingField_22(float value)
	{
		___U3CoriginalMovementSpeedU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGENT_T0C7954C99BD6D63C1824532878279573DE904927_H
#ifndef TOWER_TCB73BE25139856154FE23524412B9114BA6260DF_H
#define TOWER_TCB73BE25139856154FE23524412B9114BA6260DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Towers.Tower
struct  Tower_tCB73BE25139856154FE23524412B9114BA6260DF  : public CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0
{
public:
	// Towers.TowerLevel[] Towers.Tower::levels
	TowerLevelU5BU5D_tE0775FCFD7723EC0E0794AEC8D999328F3E2F1FA* ___levels_15;
	// System.String Towers.Tower::towerName
	String_t* ___towerName_16;
	// Core.Utilities.IntVector2 Towers.Tower::dimensions
	IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  ___dimensions_17;
	// UnityEngine.LayerMask Towers.Tower::enemyLayerMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___enemyLayerMask_18;
	// System.Int32 Towers.Tower::<currentLevel>k__BackingField
	int32_t ___U3CcurrentLevelU3Ek__BackingField_19;
	// Towers.TowerLevel Towers.Tower::<currentTowerLevel>k__BackingField
	TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9 * ___U3CcurrentTowerLevelU3Ek__BackingField_20;
	// Core.Utilities.IntVector2 Towers.Tower::<gridPosition>k__BackingField
	IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  ___U3CgridPositionU3Ek__BackingField_21;
	// Placement.IPlacementArea Towers.Tower::<placementArea>k__BackingField
	RuntimeObject* ___U3CplacementAreaU3Ek__BackingField_22;
	// System.Action Towers.Tower::towerDeleted
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___towerDeleted_23;
	// System.Action Towers.Tower::towerDestroyed
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___towerDestroyed_24;

public:
	inline static int32_t get_offset_of_levels_15() { return static_cast<int32_t>(offsetof(Tower_tCB73BE25139856154FE23524412B9114BA6260DF, ___levels_15)); }
	inline TowerLevelU5BU5D_tE0775FCFD7723EC0E0794AEC8D999328F3E2F1FA* get_levels_15() const { return ___levels_15; }
	inline TowerLevelU5BU5D_tE0775FCFD7723EC0E0794AEC8D999328F3E2F1FA** get_address_of_levels_15() { return &___levels_15; }
	inline void set_levels_15(TowerLevelU5BU5D_tE0775FCFD7723EC0E0794AEC8D999328F3E2F1FA* value)
	{
		___levels_15 = value;
		Il2CppCodeGenWriteBarrier((&___levels_15), value);
	}

	inline static int32_t get_offset_of_towerName_16() { return static_cast<int32_t>(offsetof(Tower_tCB73BE25139856154FE23524412B9114BA6260DF, ___towerName_16)); }
	inline String_t* get_towerName_16() const { return ___towerName_16; }
	inline String_t** get_address_of_towerName_16() { return &___towerName_16; }
	inline void set_towerName_16(String_t* value)
	{
		___towerName_16 = value;
		Il2CppCodeGenWriteBarrier((&___towerName_16), value);
	}

	inline static int32_t get_offset_of_dimensions_17() { return static_cast<int32_t>(offsetof(Tower_tCB73BE25139856154FE23524412B9114BA6260DF, ___dimensions_17)); }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  get_dimensions_17() const { return ___dimensions_17; }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9 * get_address_of_dimensions_17() { return &___dimensions_17; }
	inline void set_dimensions_17(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  value)
	{
		___dimensions_17 = value;
	}

	inline static int32_t get_offset_of_enemyLayerMask_18() { return static_cast<int32_t>(offsetof(Tower_tCB73BE25139856154FE23524412B9114BA6260DF, ___enemyLayerMask_18)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_enemyLayerMask_18() const { return ___enemyLayerMask_18; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_enemyLayerMask_18() { return &___enemyLayerMask_18; }
	inline void set_enemyLayerMask_18(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___enemyLayerMask_18 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentLevelU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(Tower_tCB73BE25139856154FE23524412B9114BA6260DF, ___U3CcurrentLevelU3Ek__BackingField_19)); }
	inline int32_t get_U3CcurrentLevelU3Ek__BackingField_19() const { return ___U3CcurrentLevelU3Ek__BackingField_19; }
	inline int32_t* get_address_of_U3CcurrentLevelU3Ek__BackingField_19() { return &___U3CcurrentLevelU3Ek__BackingField_19; }
	inline void set_U3CcurrentLevelU3Ek__BackingField_19(int32_t value)
	{
		___U3CcurrentLevelU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentTowerLevelU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Tower_tCB73BE25139856154FE23524412B9114BA6260DF, ___U3CcurrentTowerLevelU3Ek__BackingField_20)); }
	inline TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9 * get_U3CcurrentTowerLevelU3Ek__BackingField_20() const { return ___U3CcurrentTowerLevelU3Ek__BackingField_20; }
	inline TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9 ** get_address_of_U3CcurrentTowerLevelU3Ek__BackingField_20() { return &___U3CcurrentTowerLevelU3Ek__BackingField_20; }
	inline void set_U3CcurrentTowerLevelU3Ek__BackingField_20(TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9 * value)
	{
		___U3CcurrentTowerLevelU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentTowerLevelU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CgridPositionU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(Tower_tCB73BE25139856154FE23524412B9114BA6260DF, ___U3CgridPositionU3Ek__BackingField_21)); }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  get_U3CgridPositionU3Ek__BackingField_21() const { return ___U3CgridPositionU3Ek__BackingField_21; }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9 * get_address_of_U3CgridPositionU3Ek__BackingField_21() { return &___U3CgridPositionU3Ek__BackingField_21; }
	inline void set_U3CgridPositionU3Ek__BackingField_21(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  value)
	{
		___U3CgridPositionU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CplacementAreaU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(Tower_tCB73BE25139856154FE23524412B9114BA6260DF, ___U3CplacementAreaU3Ek__BackingField_22)); }
	inline RuntimeObject* get_U3CplacementAreaU3Ek__BackingField_22() const { return ___U3CplacementAreaU3Ek__BackingField_22; }
	inline RuntimeObject** get_address_of_U3CplacementAreaU3Ek__BackingField_22() { return &___U3CplacementAreaU3Ek__BackingField_22; }
	inline void set_U3CplacementAreaU3Ek__BackingField_22(RuntimeObject* value)
	{
		___U3CplacementAreaU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplacementAreaU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_towerDeleted_23() { return static_cast<int32_t>(offsetof(Tower_tCB73BE25139856154FE23524412B9114BA6260DF, ___towerDeleted_23)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_towerDeleted_23() const { return ___towerDeleted_23; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_towerDeleted_23() { return &___towerDeleted_23; }
	inline void set_towerDeleted_23(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___towerDeleted_23 = value;
		Il2CppCodeGenWriteBarrier((&___towerDeleted_23), value);
	}

	inline static int32_t get_offset_of_towerDestroyed_24() { return static_cast<int32_t>(offsetof(Tower_tCB73BE25139856154FE23524412B9114BA6260DF, ___towerDestroyed_24)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_towerDestroyed_24() const { return ___towerDestroyed_24; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_towerDestroyed_24() { return &___towerDestroyed_24; }
	inline void set_towerDestroyed_24(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___towerDestroyed_24 = value;
		Il2CppCodeGenWriteBarrier((&___towerDestroyed_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWER_TCB73BE25139856154FE23524412B9114BA6260DF_H
#ifndef MOVINGAGENT_T0359422B389B0779219F0921D0D8EF1AC67686CC_H
#define MOVINGAGENT_T0359422B389B0779219F0921D0D8EF1AC67686CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Agents.MovingAgent
struct  MovingAgent_t0359422B389B0779219F0921D0D8EF1AC67686CC  : public Agent_t0C7954C99BD6D63C1824532878279573DE904927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVINGAGENT_T0359422B389B0779219F0921D0D8EF1AC67686CC_H
#ifndef TOWERDEFENSEKEYBOARDMOUSEINPUT_T55B4E6FECB880B0CCD7642AC94DD2ED25CAD2358_H
#define TOWERDEFENSEKEYBOARDMOUSEINPUT_T55B4E6FECB880B0CCD7642AC94DD2ED25CAD2358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerDefense.Input.TowerDefenseKeyboardMouseInput
struct  TowerDefenseKeyboardMouseInput_t55B4E6FECB880B0CCD7642AC94DD2ED25CAD2358  : public KeyboardMouseInput_t695D0A615AD59B9BBB1C8FBA24F5236117D9813E
{
public:
	// Towers.UI.Builder TowerDefense.Input.TowerDefenseKeyboardMouseInput::m_Builder
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805 * ___m_Builder_9;

public:
	inline static int32_t get_offset_of_m_Builder_9() { return static_cast<int32_t>(offsetof(TowerDefenseKeyboardMouseInput_t55B4E6FECB880B0CCD7642AC94DD2ED25CAD2358, ___m_Builder_9)); }
	inline Builder_tF7414C0E077162F0CE605E009B1013AD98732805 * get_m_Builder_9() const { return ___m_Builder_9; }
	inline Builder_tF7414C0E077162F0CE605E009B1013AD98732805 ** get_address_of_m_Builder_9() { return &___m_Builder_9; }
	inline void set_m_Builder_9(Builder_tF7414C0E077162F0CE605E009B1013AD98732805 * value)
	{
		___m_Builder_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Builder_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERDEFENSEKEYBOARDMOUSEINPUT_T55B4E6FECB880B0CCD7642AC94DD2ED25CAD2358_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7600 = { sizeof (IListExtensions_t3B5F7401DDFDCFB2A4C70AE498ED97D8F7090C2C), -1, sizeof(IListExtensions_t3B5F7401DDFDCFB2A4C70AE498ED97D8F7090C2C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7600[1] = 
{
	IListExtensions_t3B5F7401DDFDCFB2A4C70AE498ED97D8F7090C2C_StaticFields::get_offset_of_s_SharedRandom_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7601 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7601[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7602 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7602[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7603 = { sizeof (GameDataStoreBase_tEE7B21A3C806384FAE75290BCE97E0A9EE7255A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7603[3] = 
{
	GameDataStoreBase_tEE7B21A3C806384FAE75290BCE97E0A9EE7255A6::get_offset_of_masterVolume_0(),
	GameDataStoreBase_tEE7B21A3C806384FAE75290BCE97E0A9EE7255A6::get_offset_of_sfxVolume_1(),
	GameDataStoreBase_tEE7B21A3C806384FAE75290BCE97E0A9EE7255A6::get_offset_of_musicVolume_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7604 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7604[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7605 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7606 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7607 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7608 = { sizeof (CameraInitialState_t7A8610673FEC75147BAD732A6A4B64B1B3AB917C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7608[2] = 
{
	CameraInitialState_t7A8610673FEC75147BAD732A6A4B64B1B3AB917C::get_offset_of_startZoomMode_4(),
	CameraInitialState_t7A8610673FEC75147BAD732A6A4B64B1B3AB917C::get_offset_of_initialLookAt_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7609 = { sizeof (StartZoomMode_t66CB6AACCF501BD91E38B85FA4A11EEDBF56B2D1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7609[4] = 
{
	StartZoomMode_t66CB6AACCF501BD91E38B85FA4A11EEDBF56B2D1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7610 = { sizeof (CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7610[24] = 
{
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_lookDampFactor_4(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_movementDampFactor_5(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_nearestZoom_6(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_furthestZoom_7(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_maxZoom_8(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_zoomLogFactor_9(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_zoomRecoverSpeed_10(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_floorY_11(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_zoomedCamAngle_12(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_mapSize_13(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_springyZoom_14(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_m_CurrentLookVelocity_15(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_m_MinZoomRotation_16(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_m_MaxZoomRotation_17(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_m_CurrentCamVelocity_18(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_m_FloorPlane_19(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_U3ClookPositionU3Ek__BackingField_20(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_U3CcurrentLookPositionU3Ek__BackingField_21(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_U3CcameraPositionU3Ek__BackingField_22(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_U3ClookBoundsU3Ek__BackingField_23(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_U3CzoomDistU3Ek__BackingField_24(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_U3CrawZoomDistU3Ek__BackingField_25(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_U3CtrackingObjectU3Ek__BackingField_26(),
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4::get_offset_of_U3CcachedCameraU3Ek__BackingField_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7611 = { sizeof (TowerDefenseKeyboardMouseInput_t55B4E6FECB880B0CCD7642AC94DD2ED25CAD2358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7611[1] = 
{
	TowerDefenseKeyboardMouseInput_t55B4E6FECB880B0CCD7642AC94DD2ED25CAD2358::get_offset_of_m_Builder_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7612 = { sizeof (LevelState_t52F9A6FD8458DFF7F387C7F57210D9842A2FFAFE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7612[7] = 
{
	LevelState_t52F9A6FD8458DFF7F387C7F57210D9842A2FFAFE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7613 = { sizeof (PoolableEffect_t345C497F0CDBEA3D2D5DB85CBBCAB596C6E5C66B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7613[3] = 
{
	PoolableEffect_t345C497F0CDBEA3D2D5DB85CBBCAB596C6E5C66B::get_offset_of_m_Systems_6(),
	PoolableEffect_t345C497F0CDBEA3D2D5DB85CBBCAB596C6E5C66B::get_offset_of_m_Trails_7(),
	PoolableEffect_t345C497F0CDBEA3D2D5DB85CBBCAB596C6E5C66B::get_offset_of_m_EffectsEnabled_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7614 = { sizeof (SelfDestroyTimer_t8C6B5648246F50B10AB7BF98D77A8E6D4D3EB476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7614[3] = 
{
	SelfDestroyTimer_t8C6B5648246F50B10AB7BF98D77A8E6D4D3EB476::get_offset_of_time_4(),
	SelfDestroyTimer_t8C6B5648246F50B10AB7BF98D77A8E6D4D3EB476::get_offset_of_timer_5(),
	SelfDestroyTimer_t8C6B5648246F50B10AB7BF98D77A8E6D4D3EB476::get_offset_of_death_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7615 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7616 = { sizeof (Tower_tCB73BE25139856154FE23524412B9114BA6260DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7616[10] = 
{
	Tower_tCB73BE25139856154FE23524412B9114BA6260DF::get_offset_of_levels_15(),
	Tower_tCB73BE25139856154FE23524412B9114BA6260DF::get_offset_of_towerName_16(),
	Tower_tCB73BE25139856154FE23524412B9114BA6260DF::get_offset_of_dimensions_17(),
	Tower_tCB73BE25139856154FE23524412B9114BA6260DF::get_offset_of_enemyLayerMask_18(),
	Tower_tCB73BE25139856154FE23524412B9114BA6260DF::get_offset_of_U3CcurrentLevelU3Ek__BackingField_19(),
	Tower_tCB73BE25139856154FE23524412B9114BA6260DF::get_offset_of_U3CcurrentTowerLevelU3Ek__BackingField_20(),
	Tower_tCB73BE25139856154FE23524412B9114BA6260DF::get_offset_of_U3CgridPositionU3Ek__BackingField_21(),
	Tower_tCB73BE25139856154FE23524412B9114BA6260DF::get_offset_of_U3CplacementAreaU3Ek__BackingField_22(),
	Tower_tCB73BE25139856154FE23524412B9114BA6260DF::get_offset_of_towerDeleted_23(),
	Tower_tCB73BE25139856154FE23524412B9114BA6260DF::get_offset_of_towerDestroyed_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7617 = { sizeof (TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7617[5] = 
{
	TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9::get_offset_of_towerGhostPrefab_4(),
	TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9::get_offset_of_levelData_5(),
	TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9::get_offset_of_m_ParentTower_6(),
	TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9::get_offset_of_m_Turret_7(),
	TowerLevel_t483DF43339AC44420BE580ADEC7F31C71C18ADB9::get_offset_of_U3CmaskU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7618 = { sizeof (TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7618[11] = 
{
	TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7::get_offset_of_U3CcontrollerU3Ek__BackingField_4(),
	TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7::get_offset_of_radiusVisualizer_5(),
	TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7::get_offset_of_radiusVisualizerHeight_6(),
	TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7::get_offset_of_dampSpeed_7(),
	TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7::get_offset_of_material_8(),
	TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7::get_offset_of_invalidPositionMaterial_9(),
	TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7::get_offset_of_m_MeshRenderers_10(),
	TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7::get_offset_of_m_MoveVel_11(),
	TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7::get_offset_of_m_TargetPosition_12(),
	TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7::get_offset_of_m_ValidPos_13(),
	TowerPlacementGhost_t07837449CA42E96133BDBB23D88A06371310E1B7::get_offset_of_U3CghostColliderU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7619 = { sizeof (TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7619[7] = 
{
	TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9::get_offset_of_description_4(),
	TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9::get_offset_of_upgradeDescription_5(),
	TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9::get_offset_of_cost_6(),
	TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9::get_offset_of_sell_7(),
	TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9::get_offset_of_maxHealth_8(),
	TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9::get_offset_of_startingHealth_9(),
	TowerLevelData_tFAF65E7202275569B6848DFB6FA816701E6AB2C9::get_offset_of_icon_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7620 = { sizeof (TowerLibrary_t6E601F3B2DB60D4823E7FD85CDBEEE11896F9A7C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7620[2] = 
{
	TowerLibrary_t6E601F3B2DB60D4823E7FD85CDBEEE11896F9A7C::get_offset_of_configurations_4(),
	TowerLibrary_t6E601F3B2DB60D4823E7FD85CDBEEE11896F9A7C::get_offset_of_m_ConfigurationDictionary_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7621 = { sizeof (U3CU3Ec_t604A742CF42B9D19303EA45C08CA95CBDECB2603), -1, sizeof(U3CU3Ec_t604A742CF42B9D19303EA45C08CA95CBDECB2603_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7621[2] = 
{
	U3CU3Ec_t604A742CF42B9D19303EA45C08CA95CBDECB2603_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t604A742CF42B9D19303EA45C08CA95CBDECB2603_StaticFields::get_offset_of_U3CU3E9__5_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7622 = { sizeof (Builder_tF7414C0E077162F0CE605E009B1013AD98732805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7622[17] = 
{
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_U3CstateU3Ek__BackingField_5(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_placementAreaMask_6(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_towerSelectionLayer_7(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_ghostWorldPlacementMask_8(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_sphereCastRadius_9(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_towerSpawnButtons_10(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_towerUI_11(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_buildInfoUI_12(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_stateChanged_13(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_ghostBecameValid_14(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_selectionChanged_15(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_m_CurrentArea_16(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_m_GridPosition_17(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_m_Camera_18(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_m_CurrentTower_19(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_m_GhostPlacementPossible_20(),
	Builder_tF7414C0E077162F0CE605E009B1013AD98732805::get_offset_of_U3CcurrentSelectedTowerU3Ek__BackingField_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7623 = { sizeof (UIPointer_t0FC948F506BE29990B0FF40D59141947F4B0EF38)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7623[4] = 
{
	UIPointer_t0FC948F506BE29990B0FF40D59141947F4B0EF38::get_offset_of_pointer_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIPointer_t0FC948F506BE29990B0FF40D59141947F4B0EF38::get_offset_of_ray_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIPointer_t0FC948F506BE29990B0FF40D59141947F4B0EF38::get_offset_of_raycast_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIPointer_t0FC948F506BE29990B0FF40D59141947F4B0EF38::get_offset_of_overUI_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7624 = { sizeof (State_t8F92AE8C7A2512F09D0216DA21E50391B17AE1BC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7624[4] = 
{
	State_t8F92AE8C7A2512F09D0216DA21E50391B17AE1BC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7625 = { sizeof (RadiusVisualizerController_tC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7625[4] = 
{
	RadiusVisualizerController_tC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75::get_offset_of_radiusVisualizerPrefab_5(),
	RadiusVisualizerController_tC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75::get_offset_of_radiusVisualizerHeight_6(),
	RadiusVisualizerController_tC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75::get_offset_of_localEuler_7(),
	RadiusVisualizerController_tC38F2BF1AFDE8F6F7B2F30E4DF73F80427EA7B75::get_offset_of_m_RadiusVisualizers_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7626 = { sizeof (TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7626[11] = 
{
	TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C::get_offset_of_buttonText_4(),
	TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C::get_offset_of_towerIcon_5(),
	TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C::get_offset_of_buyButton_6(),
	TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C::get_offset_of_energyIcon_7(),
	TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C::get_offset_of_energyDefaultColor_8(),
	TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C::get_offset_of_energyInvalidColor_9(),
	TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C::get_offset_of_buttonTapped_10(),
	TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C::get_offset_of_draggedOff_11(),
	TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C::get_offset_of_m_Tower_12(),
	TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C::get_offset_of_m_Currency_13(),
	TowerSpawnButton_t980372E20A7E18067070D7209BF454D083553E6C::get_offset_of_m_RectTransform_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7627 = { sizeof (BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7627[6] = 
{
	BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF::get_offset_of_anim_4(),
	BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF::get_offset_of_showClipName_5(),
	BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF::get_offset_of_hideClipName_6(),
	BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF::get_offset_of_m_TowerUI_7(),
	BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF::get_offset_of_m_Canvas_8(),
	BuildInfoUI_tD9CF97A89D59E29BDDE2C47795495FA06EEF28AF::get_offset_of_m_State_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7628 = { sizeof (AnimationState_t55EC65D7C3704BE87F1DB1AF06EB62BAB5264F2B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7628[5] = 
{
	AnimationState_t55EC65D7C3704BE87F1DB1AF06EB62BAB5264F2B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7629 = { sizeof (TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7629[8] = 
{
	TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6::get_offset_of_towerName_4(),
	TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6::get_offset_of_description_5(),
	TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6::get_offset_of_dps_6(),
	TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6::get_offset_of_level_7(),
	TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6::get_offset_of_health_8(),
	TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6::get_offset_of_dimensions_9(),
	TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6::get_offset_of_upgradeCost_10(),
	TowerInfoDisplay_t2F2B23CCAEDA8EAAD5ACBA9F18B1498423C858E6::get_offset_of_sellPrice_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7630 = { sizeof (TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7630[11] = 
{
	TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF::get_offset_of_towerName_4(),
	TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF::get_offset_of_description_5(),
	TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF::get_offset_of_upgradeDescription_6(),
	TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF::get_offset_of_sellButton_7(),
	TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF::get_offset_of_upgradeButton_8(),
	TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF::get_offset_of_towerInfoDisplay_9(),
	TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF::get_offset_of_panelRectTransform_10(),
	TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF::get_offset_of_confirmationButtons_11(),
	TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF::get_offset_of_m_GameCamera_12(),
	TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF::get_offset_of_m_Tower_13(),
	TowerUI_t9E9DD8F174D01D378E2DC3292DE761996C32CDEF::get_offset_of_m_Canvas_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7631 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7632 = { sizeof (PlacementAreaExtensions_t8B49828B8AD5EFBF9E04C5B08CE30AA73B2F19EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7633 = { sizeof (ObjectFitStatus_t18139825A4B0C476F24EBA127E1735F2A03C1D50)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7633[4] = 
{
	ObjectFitStatus_t18139825A4B0C476F24EBA127E1735F2A03C1D50::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7634 = { sizeof (ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7634[7] = 
{
	ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E::get_offset_of_placementTilePrefab_4(),
	ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E::get_offset_of_placementTilePrefabMobile_5(),
	ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E::get_offset_of_dimensions_6(),
	ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E::get_offset_of_gridSize_7(),
	ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E::get_offset_of_m_InvGridSize_8(),
	ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E::get_offset_of_m_AvailableCells_9(),
	ObjectPlacementGrid_tACB77AFD3305F152CC7D3C321E439CC41815D91E::get_offset_of_m_Tiles_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7635 = { sizeof (PlacementTileState_t815C7EABFA640F06EA4F26BDCD0487B0E47CC584)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7635[3] = 
{
	PlacementTileState_t815C7EABFA640F06EA4F26BDCD0487B0E47CC584::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7636 = { sizeof (PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7636[3] = 
{
	PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107::get_offset_of_emptyMaterial_4(),
	PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107::get_offset_of_filledMaterial_5(),
	PlacementTile_t20F49A42F6A5E00D37AD0AC1D50E07FAD0223107::get_offset_of_tileRenderer_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7637 = { sizeof (SingleObjectPlacementArea_t78A74C01D07BB007432A1D4D2F677E08250E19BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7637[4] = 
{
	SingleObjectPlacementArea_t78A74C01D07BB007432A1D4D2F677E08250E19BA::get_offset_of_placementTilePrefab_4(),
	SingleObjectPlacementArea_t78A74C01D07BB007432A1D4D2F677E08250E19BA::get_offset_of_placementTilePrefabMobile_5(),
	SingleObjectPlacementArea_t78A74C01D07BB007432A1D4D2F677E08250E19BA::get_offset_of_m_SpawnedTile_6(),
	SingleObjectPlacementArea_t78A74C01D07BB007432A1D4D2F677E08250E19BA::get_offset_of_m_IsOccupied_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7638 = { sizeof (FixedNodeSelector_tA44D55626F235F2A427889E2F7F968E76EEC7A43), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7638[1] = 
{
	FixedNodeSelector_tA44D55626F235F2A427889E2F7F968E76EEC7A43::get_offset_of_m_NodeIndex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7639 = { sizeof (Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7639[2] = 
{
	Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7::get_offset_of_areaMesh_4(),
	Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7::get_offset_of_weight_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7640 = { sizeof (NodeSelector_tFEA86AE0908D36229D2BBA57FDF7AEDEC84304D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7640[1] = 
{
	NodeSelector_tFEA86AE0908D36229D2BBA57FDF7AEDEC84304D9::get_offset_of_linkedNodes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7641 = { sizeof (RandomNodeSelector_tA96B6649EC217A254C3A87C8E808C86D5E3F0AD2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7641[1] = 
{
	RandomNodeSelector_tA96B6649EC217A254C3A87C8E808C86D5E3F0AD2::get_offset_of_m_WeightSum_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7642 = { sizeof (U3CU3Ec_t28270A5A54390944ABF6943F0A6A82B812BA4842), -1, sizeof(U3CU3Ec_t28270A5A54390944ABF6943F0A6A82B812BA4842_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7642[2] = 
{
	U3CU3Ec_t28270A5A54390944ABF6943F0A6A82B812BA4842_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t28270A5A54390944ABF6943F0A6A82B812BA4842_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7643 = { sizeof (AreaMeshCreator_tE22D06EA3837CB117F16849084D4074A0A6F01D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7643[2] = 
{
	AreaMeshCreator_tE22D06EA3837CB117F16849084D4074A0A6F01D7::get_offset_of_meshObject_4(),
	AreaMeshCreator_tE22D06EA3837CB117F16849084D4074A0A6F01D7::get_offset_of_outSidePointsParent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7644 = { sizeof (Triangle_t5CDAC049E48591B7FA7637C0185B5DF87DEAD231), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7644[4] = 
{
	Triangle_t5CDAC049E48591B7FA7637C0185B5DF87DEAD231::get_offset_of_v0_0(),
	Triangle_t5CDAC049E48591B7FA7637C0185B5DF87DEAD231::get_offset_of_v1_1(),
	Triangle_t5CDAC049E48591B7FA7637C0185B5DF87DEAD231::get_offset_of_v2_2(),
	Triangle_t5CDAC049E48591B7FA7637C0185B5DF87DEAD231::get_offset_of_area_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7645 = { sizeof (MeshObject_t3BA586F00E133946864FFCB15DE54A59D4E5E2B2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7645[2] = 
{
	MeshObject_t3BA586F00E133946864FFCB15DE54A59D4E5E2B2::get_offset_of_triangles_0(),
	MeshObject_t3BA586F00E133946864FFCB15DE54A59D4E5E2B2::get_offset_of_completeArea_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7646 = { sizeof (U3CU3Ec_tFD6DBEF54E6D28B6F7BD616D509265D264B445F8), -1, sizeof(U3CU3Ec_tFD6DBEF54E6D28B6F7BD616D509265D264B445F8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7646[3] = 
{
	U3CU3Ec_tFD6DBEF54E6D28B6F7BD616D509265D264B445F8_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tFD6DBEF54E6D28B6F7BD616D509265D264B445F8_StaticFields::get_offset_of_U3CU3E9__2_0_1(),
	U3CU3Ec_tFD6DBEF54E6D28B6F7BD616D509265D264B445F8_StaticFields::get_offset_of_U3CU3E9__3_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7647 = { sizeof (Triangulator_tE822FC04EEB8D0343FF299D8F7B14E508C55AD4E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7647[1] = 
{
	Triangulator_tE822FC04EEB8D0343FF299D8F7B14E508C55AD4E::get_offset_of_m_Points_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7648 = { sizeof (Agent_t0C7954C99BD6D63C1824532878279573DE904927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7648[8] = 
{
	Agent_t0C7954C99BD6D63C1824532878279573DE904927::get_offset_of_destinationReached_15(),
	Agent_t0C7954C99BD6D63C1824532878279573DE904927::get_offset_of_appliedEffectOffset_16(),
	Agent_t0C7954C99BD6D63C1824532878279573DE904927::get_offset_of_appliedEffectScale_17(),
	Agent_t0C7954C99BD6D63C1824532878279573DE904927::get_offset_of_m_NavMeshAgent_18(),
	Agent_t0C7954C99BD6D63C1824532878279573DE904927::get_offset_of_m_CurrentNode_19(),
	Agent_t0C7954C99BD6D63C1824532878279573DE904927::get_offset_of_m_Destination_20(),
	Agent_t0C7954C99BD6D63C1824532878279573DE904927::get_offset_of_U3CstateU3Ek__BackingField_21(),
	Agent_t0C7954C99BD6D63C1824532878279573DE904927::get_offset_of_U3CoriginalMovementSpeedU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7649 = { sizeof (State_t5D7DF387DD891673A309CB96CA602DB9652EC5CC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7649[6] = 
{
	State_t5D7DF387DD891673A309CB96CA602DB9652EC5CC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7650 = { sizeof (MovingAgent_t0359422B389B0779219F0921D0D8EF1AC67686CC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7651 = { sizeof (AgentConfiguration_tB707ADEB3898212CA6B3C134B11AE9E9F440A8EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7651[3] = 
{
	AgentConfiguration_tB707ADEB3898212CA6B3C134B11AE9E9F440A8EC::get_offset_of_agentName_4(),
	AgentConfiguration_tB707ADEB3898212CA6B3C134B11AE9E9F440A8EC::get_offset_of_agentDescription_5(),
	AgentConfiguration_tB707ADEB3898212CA6B3C134B11AE9E9F440A8EC::get_offset_of_agentPrefab_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7652 = { sizeof (Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7652[2] = 
{
	Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05::get_offset_of_U3CcurrentCurrencyU3Ek__BackingField_0(),
	Currency_t579EAFB2245C03FCD16C5F77C9D9EA23195F3E05::get_offset_of_currencyChanged_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7653 = { sizeof (CurrencyChangeInfo_t9FD7557727D54B624C05DA83849209C8E6C548E0)+ sizeof (RuntimeObject), sizeof(CurrencyChangeInfo_t9FD7557727D54B624C05DA83849209C8E6C548E0 ), 0, 0 };
extern const int32_t g_FieldOffsetTable7653[4] = 
{
	CurrencyChangeInfo_t9FD7557727D54B624C05DA83849209C8E6C548E0::get_offset_of_previousCurrency_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurrencyChangeInfo_t9FD7557727D54B624C05DA83849209C8E6C548E0::get_offset_of_currentCurrency_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurrencyChangeInfo_t9FD7557727D54B624C05DA83849209C8E6C548E0::get_offset_of_difference_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurrencyChangeInfo_t9FD7557727D54B624C05DA83849209C8E6C548E0::get_offset_of_absoluteDifference_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7654 = { sizeof (CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7654[6] = 
{
	CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497::get_offset_of_startingCurrency_5(),
	CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497::get_offset_of_constantCurrencyAddition_6(),
	CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497::get_offset_of_constantCurrencyGainRate_7(),
	CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497::get_offset_of_currencyChanged_8(),
	CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497::get_offset_of_m_GainTimer_9(),
	CurrencyManager_tB78B180A3E420C837BD37D7F480D2A1678CD4497::get_offset_of_U3CcurrencyU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7655 = { sizeof (LootDrop_t3A4E9B0E2E4BB7E65EAF3C68E2DA76F1B142E8E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7655[2] = 
{
	LootDrop_t3A4E9B0E2E4BB7E65EAF3C68E2DA76F1B142E8E6::get_offset_of_lootDropped_4(),
	LootDrop_t3A4E9B0E2E4BB7E65EAF3C68E2DA76F1B142E8E6::get_offset_of_health_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7656 = { sizeof (CurrencyUI_t651099D3DC0E8F1703B66575C0536EF4CF5A0E5D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7656[3] = 
{
	CurrencyUI_t651099D3DC0E8F1703B66575C0536EF4CF5A0E5D::get_offset_of_display_4(),
	CurrencyUI_t651099D3DC0E8F1703B66575C0536EF4CF5A0E5D::get_offset_of_currencySymbol_5(),
	CurrencyUI_t651099D3DC0E8F1703B66575C0536EF4CF5A0E5D::get_offset_of_m_Currency_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7657 = { sizeof (AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7657[6] = 
{
	AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB::get_offset_of_myRigidbody_4(),
	AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB::get_offset_of_canDestroy_5(),
	AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB::get_offset_of_isDestroyed_6(),
	AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB::get_offset_of_isDebris_7(),
	AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB::get_offset_of_health_8(),
	AsteroidController_t6B5CEB21D7C18DE9BF67A7D97DCE17E0880A50FB::get_offset_of_maxHealth_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7658 = { sizeof (U3CDelayInitialDestructionU3Ed__7_t9FAE7563F4CEF6E366A82C07A45594A0DBF2699A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7658[4] = 
{
	U3CDelayInitialDestructionU3Ed__7_t9FAE7563F4CEF6E366A82C07A45594A0DBF2699A::get_offset_of_U3CU3E1__state_0(),
	U3CDelayInitialDestructionU3Ed__7_t9FAE7563F4CEF6E366A82C07A45594A0DBF2699A::get_offset_of_U3CU3E2__current_1(),
	U3CDelayInitialDestructionU3Ed__7_t9FAE7563F4CEF6E366A82C07A45594A0DBF2699A::get_offset_of_delayTime_2(),
	U3CDelayInitialDestructionU3Ed__7_t9FAE7563F4CEF6E366A82C07A45594A0DBF2699A::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7659 = { sizeof (GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC), -1, sizeof(GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7659[21] = 
{
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC_StaticFields::get_offset_of_instance_4(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_astroidPrefab_5(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_debrisPrefab_6(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_explosionPrefab_7(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_healthPickupPrefab_8(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_spawning_9(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_spawnTimeMin_10(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_spawnTimeMax_11(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_startingAsteroids_12(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_healthSpawnTimeMin_13(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_healthSpawnTimeMax_14(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_scoreText_15(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_finalScoreText_16(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_score_17(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_asteroidPoints_18(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_debrisPoints_19(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_gameOverScreen_20(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_gameOverText_21(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_asteroidHealth_22(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_debrisHealth_23(),
	GameManager_t57B571316ACCF8579D9C669E2CBB93B1D8E748BC::get_offset_of_hasLost_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7660 = { sizeof (U3CSpawnHealthTimerU3Ed__25_tCFD0C835D21022468D481CBBCE79E73CF9EF5E69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7660[3] = 
{
	U3CSpawnHealthTimerU3Ed__25_tCFD0C835D21022468D481CBBCE79E73CF9EF5E69::get_offset_of_U3CU3E1__state_0(),
	U3CSpawnHealthTimerU3Ed__25_tCFD0C835D21022468D481CBBCE79E73CF9EF5E69::get_offset_of_U3CU3E2__current_1(),
	U3CSpawnHealthTimerU3Ed__25_tCFD0C835D21022468D481CBBCE79E73CF9EF5E69::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7661 = { sizeof (U3CSpawnTimerU3Ed__27_tBA95B4D843AFFA5C8B5492DA62BEA4B2D2A9B33D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7661[3] = 
{
	U3CSpawnTimerU3Ed__27_tBA95B4D843AFFA5C8B5492DA62BEA4B2D2A9B33D::get_offset_of_U3CU3E1__state_0(),
	U3CSpawnTimerU3Ed__27_tBA95B4D843AFFA5C8B5492DA62BEA4B2D2A9B33D::get_offset_of_U3CU3E2__current_1(),
	U3CSpawnTimerU3Ed__27_tBA95B4D843AFFA5C8B5492DA62BEA4B2D2A9B33D::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7662 = { sizeof (U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7662[7] = 
{
	U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2::get_offset_of_U3CU3E1__state_0(),
	U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2::get_offset_of_U3CU3E2__current_1(),
	U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2::get_offset_of_U3CU3E4__this_2(),
	U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2::get_offset_of_U3CimageColorU3E5__2_3(),
	U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2::get_offset_of_U3CtextColorU3E5__3_4(),
	U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2::get_offset_of_U3CfinalScoreTextColorU3E5__4_5(),
	U3CFadeDeathScreenU3Ed__32_t343C8655DAA2E77E42667C9C6DF4EA4A51B4DCB2::get_offset_of_U3CtU3E5__5_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7663 = { sizeof (HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7663[5] = 
{
	HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A::get_offset_of_myRigidbody_4(),
	HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A::get_offset_of_particles_5(),
	HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A::get_offset_of_mySprite_6(),
	HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A::get_offset_of_canDestroy_7(),
	HealthPickupController_t33A8F52AD069679E26AD051B9A1BE56A58C5121A::get_offset_of_canPickup_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7664 = { sizeof (U3CDelayInitialDestructionU3Ed__6_tAD05859DEE753EE3C15A1245A4FF5A3511DE6E02), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7664[4] = 
{
	U3CDelayInitialDestructionU3Ed__6_tAD05859DEE753EE3C15A1245A4FF5A3511DE6E02::get_offset_of_U3CU3E1__state_0(),
	U3CDelayInitialDestructionU3Ed__6_tAD05859DEE753EE3C15A1245A4FF5A3511DE6E02::get_offset_of_U3CU3E2__current_1(),
	U3CDelayInitialDestructionU3Ed__6_tAD05859DEE753EE3C15A1245A4FF5A3511DE6E02::get_offset_of_delayTime_2(),
	U3CDelayInitialDestructionU3Ed__6_tAD05859DEE753EE3C15A1245A4FF5A3511DE6E02::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7665 = { sizeof (PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2), -1, sizeof(PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7665[19] = 
{
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2_StaticFields::get_offset_of_instance_4(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_rotationSpeed_5(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_accelerationSpeed_6(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_maxSpeed_7(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_shootingCooldown_8(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_bulletPrefab_9(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_myRigidbody_10(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_gunTrans_11(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_bulletSpawnPos_12(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_shootingTimer_13(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_canControl_14(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_rotation_15(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_acceleration_16(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_overheatVisual_17(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_overheatTimer_18(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_overheatTimerMax_19(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_cooldownSpeed_20(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_canShoot_21(),
	PlayerController_tE41FC109B1AA1C1BB2FB0F958F04ADA1CB010EC2::get_offset_of_gunHeatBar_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7666 = { sizeof (U3CDelayOverheatU3Ed__29_t4214B957649EDFFD363CA17E9571FC8DD35BD919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7666[4] = 
{
	U3CDelayOverheatU3Ed__29_t4214B957649EDFFD363CA17E9571FC8DD35BD919::get_offset_of_U3CU3E1__state_0(),
	U3CDelayOverheatU3Ed__29_t4214B957649EDFFD363CA17E9571FC8DD35BD919::get_offset_of_U3CU3E2__current_1(),
	U3CDelayOverheatU3Ed__29_t4214B957649EDFFD363CA17E9571FC8DD35BD919::get_offset_of_U3CU3E4__this_2(),
	U3CDelayOverheatU3Ed__29_t4214B957649EDFFD363CA17E9571FC8DD35BD919::get_offset_of_U3CtU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7667 = { sizeof (PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1), -1, sizeof(PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7667[12] = 
{
	PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1_StaticFields::get_offset_of_instance_4(),
	PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1::get_offset_of_canTakeDamage_5(),
	PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1::get_offset_of_maxHealth_6(),
	PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1::get_offset_of_currentHealth_7(),
	PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1::get_offset_of_invulnerabilityTime_8(),
	PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1::get_offset_of_currentShield_9(),
	PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1::get_offset_of_maxShield_10(),
	PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1::get_offset_of_regenShieldTimer_11(),
	PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1::get_offset_of_regenShieldTimerMax_12(),
	PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1::get_offset_of_explosionParticles_13(),
	PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1::get_offset_of_healthBar_14(),
	PlayerHealth_tA3F94E28DC22C88ACD17777530BD9FEA631EBBC1::get_offset_of_shieldBar_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7668 = { sizeof (U3CInvulnerabliltyU3Ed__20_t167CD2A0C46BEF46F258FE0C37DE2238F0B966B2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7668[3] = 
{
	U3CInvulnerabliltyU3Ed__20_t167CD2A0C46BEF46F258FE0C37DE2238F0B966B2::get_offset_of_U3CU3E1__state_0(),
	U3CInvulnerabliltyU3Ed__20_t167CD2A0C46BEF46F258FE0C37DE2238F0B966B2::get_offset_of_U3CU3E2__current_1(),
	U3CInvulnerabliltyU3Ed__20_t167CD2A0C46BEF46F258FE0C37DE2238F0B966B2::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7669 = { sizeof (U3CShakeCameraU3Ed__21_t09EFFA8ACEA55E8BEFDC29E8061AEBA20F1A1CCA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7669[4] = 
{
	U3CShakeCameraU3Ed__21_t09EFFA8ACEA55E8BEFDC29E8061AEBA20F1A1CCA::get_offset_of_U3CU3E1__state_0(),
	U3CShakeCameraU3Ed__21_t09EFFA8ACEA55E8BEFDC29E8061AEBA20F1A1CCA::get_offset_of_U3CU3E2__current_1(),
	U3CShakeCameraU3Ed__21_t09EFFA8ACEA55E8BEFDC29E8061AEBA20F1A1CCA::get_offset_of_U3CorigPosU3E5__2_2(),
	U3CShakeCameraU3Ed__21_t09EFFA8ACEA55E8BEFDC29E8061AEBA20F1A1CCA::get_offset_of_U3CtU3E5__3_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7670 = { sizeof (LightningBoltAnimationMode_tEED68806DD3934973F957DB59EBBED0142C57EE7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7670[5] = 
{
	LightningBoltAnimationMode_tEED68806DD3934973F957DB59EBBED0142C57EE7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7671 = { sizeof (LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7671[21] = 
{
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_StartObject_4(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_StartPosition_5(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_EndObject_6(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_EndPosition_7(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_Generations_8(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_Duration_9(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_timer_10(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_ChaosFactor_11(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_ManualMode_12(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_Rows_13(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_Columns_14(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_AnimationMode_15(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_RandomGenerator_16(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_lineRenderer_17(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_segments_18(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_startIndex_19(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_size_20(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_offsets_21(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_animationOffsetIndex_22(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_animationPingPongDirection_23(),
	LightningBoltScript_t9D8EF3E3AA2FCF42201CB1B97494C474E2D75C3B::get_offset_of_orthographic_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7672 = { sizeof (ActionExtension_t5F73C2D473CA1CC346E9ECB67783A82AD915956F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7673 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7673[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7674 = { sizeof (Channel_t8FA7A794BA08A27A818FB3D8E68B435E7738E536), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7675 = { sizeof (DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7675[7] = 
{
	DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578::get_offset_of_lockCount_0(),
	DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578::get_offset_of_taskListSyncRoot_1(),
	DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578::get_offset_of_taskList_2(),
	DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578::get_offset_of_delayedTaskList_3(),
	DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578::get_offset_of_dataEvent_4(),
	DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578::get_offset_of_AllowAccessLimitationChecks_5(),
	DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578::get_offset_of_TaskSortingSystem_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7676 = { sizeof (U3CU3Ec_t2F4A834ED9678DF4E594B0DFE0366030E796CB55), -1, sizeof(U3CU3Ec_t2F4A834ED9678DF4E594B0DFE0366030E796CB55_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7676[2] = 
{
	U3CU3Ec_t2F4A834ED9678DF4E594B0DFE0366030E796CB55_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2F4A834ED9678DF4E594B0DFE0366030E796CB55_StaticFields::get_offset_of_U3CU3E9__20_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7677 = { sizeof (NullDispatcher_tE297A13D35B57D87B3AFA60E38331CBEF62B73F5), -1, sizeof(NullDispatcher_tE297A13D35B57D87B3AFA60E38331CBEF62B73F5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7677[1] = 
{
	NullDispatcher_tE297A13D35B57D87B3AFA60E38331CBEF62B73F5_StaticFields::get_offset_of_Null_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7678 = { sizeof (Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297), -1, sizeof(Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297_StaticFields), sizeof(Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable7678[3] = 
{
	Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297_ThreadStaticFields::get_offset_of_currentTask_7() | THREAD_LOCAL_STATIC_MASK,
	Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297_ThreadStaticFields::get_offset_of_currentDispatcher_8() | THREAD_LOCAL_STATIC_MASK,
	Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297_StaticFields::get_offset_of_mainDispatcher_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7679 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7679[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7680 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7680[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7681 = { sizeof (EnumerableExtension_t4C8E385903C82D244863B9A6A36944EC45BBF3E6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7682 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7682[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7683 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7683[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7684 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7684[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7685 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7685[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7686 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7686[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7687 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7687[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7688 = { sizeof (EnumeratorExtension_t9A5A9C5C5108631CECEE49E3B4797F8764F76719), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7689 = { sizeof (ObjectExtension_t3BD2FE20BD963D62D315A19A17748809208FB3C8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7690 = { sizeof (SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89), -1, sizeof(SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7690[3] = 
{
	SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89::get_offset_of_U3CTargetU3Ek__BackingField_0(),
	SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89_StaticFields::get_offset_of_MainThread_1(),
	SwitchTo_t546E8C14B0EEE621E9D0CA517B4EA4B7D4CFBE89_StaticFields::get_offset_of_Thread_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7691 = { sizeof (TargetType_t0D7AB4ABEFBCFB09F254BD3EF3CDCCEC31107907)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7691[3] = 
{
	TargetType_t0D7AB4ABEFBCFB09F254BD3EF3CDCCEC31107907::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7692 = { sizeof (TaskSortingSystem_t69AB27974F2B4DEF923567D25F773D47B3DD0F22)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7692[4] = 
{
	TaskSortingSystem_t69AB27974F2B4DEF923567D25F773D47B3DD0F22::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7693 = { sizeof (TaskEndedEventHandler_t00BAD98C5606F4CC601A12E95E473B7B19B4C611), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7694 = { sizeof (Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F), -1, 0, sizeof(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable7694[11] = 
{
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F::get_offset_of_syncRoot_0(),
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F::get_offset_of_taskEnded_1(),
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F::get_offset_of_hasEnded_2(),
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F::get_offset_of_Name_3(),
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F::get_offset_of_Priority_4(),
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F::get_offset_of_abortEvent_5(),
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F::get_offset_of_endedEvent_6(),
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F::get_offset_of_endingEvent_7(),
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F::get_offset_of_hasStarted_8(),
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F_ThreadStaticFields::get_offset_of_current_9() | THREAD_LOCAL_STATIC_MASK,
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F::get_offset_of_disposed_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7695 = { sizeof (Unit_t3749682E243E386DEAEF1418B5180C6E0E00F84D)+ sizeof (RuntimeObject), sizeof(Unit_t3749682E243E386DEAEF1418B5180C6E0E00F84D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7696 = { sizeof (U3CU3Ec__DisplayClass43_0_t6E9F076F4B7B65FD7A272546C7262408006196A8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7696[3] = 
{
	U3CU3Ec__DisplayClass43_0_t6E9F076F4B7B65FD7A272546C7262408006196A8::get_offset_of_enumerator_0(),
	U3CU3Ec__DisplayClass43_0_t6E9F076F4B7B65FD7A272546C7262408006196A8::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass43_0_t6E9F076F4B7B65FD7A272546C7262408006196A8::get_offset_of_U3CU3E9__0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7697 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7697[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7698 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7698[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7699 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7699[1] = 
{
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
