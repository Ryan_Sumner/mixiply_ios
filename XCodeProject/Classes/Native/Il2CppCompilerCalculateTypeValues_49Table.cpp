﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ICSharpCode.SharpZipLib.Checksums.Adler32
struct Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033;
// ICSharpCode.SharpZipLib.Checksums.Crc32
struct Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F;
// ICSharpCode.SharpZipLib.Core.INameTransform
struct INameTransform_t3427F87035EE4BD38B68C3015D253A1A08FB7F29;
// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform
struct ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195;
// ICSharpCode.SharpZipLib.Tar.ProgressMessageHandler
struct ProgressMessageHandler_tCB79C5F0AE44A5C4FA7F7D8363992A25DDEDC4EF;
// ICSharpCode.SharpZipLib.Tar.TarArchive
struct TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726;
// ICSharpCode.SharpZipLib.Tar.TarBuffer
struct TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C;
// ICSharpCode.SharpZipLib.Tar.TarEntry
struct TarEntry_tDE18EDBB29A64DBD744028FA5BAF5026519398BE;
// ICSharpCode.SharpZipLib.Tar.TarHeader
struct TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74;
// ICSharpCode.SharpZipLib.Tar.TarInputStream
struct TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E;
// ICSharpCode.SharpZipLib.Tar.TarInputStream/IEntryFactory
struct IEntryFactory_tC2393E9B7A0536B33821164FC2D6D2173A6B797C;
// ICSharpCode.SharpZipLib.Tar.TarOutputStream
struct TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838;
// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader
struct InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree
struct InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer
struct InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow
struct OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator
struct StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5;
// ICSharpCode.SharpZipLib.Zip.IArchiveStorage
struct IArchiveStorage_tA26244F967901B1B7831DF183DDBAEB6EE787B5C;
// ICSharpCode.SharpZipLib.Zip.IDynamicDataSource
struct IDynamicDataSource_tDF2EF6F650885419EC0DE3BC16B51ED5CE1E2AE8;
// ICSharpCode.SharpZipLib.Zip.IEntryFactory
struct IEntryFactory_t9FAA37B862F9CF44DD8AF1FB148406E1A10C3BEF;
// ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs
struct KeysRequiredEventArgs_t333068E27532BA6462A1C84D7D3CCE8904FC7148;
// ICSharpCode.SharpZipLib.Zip.ZipEntry[]
struct ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917;
// ICSharpCode.SharpZipLib.Zip.ZipFile
struct ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F;
// ICSharpCode.SharpZipLib.Zip.ZipFile/KeysRequiredEventHandler
struct KeysRequiredEventHandler_t01BA6F773E29C468C7FD85BE6B93EE5043A521D9;
// MS.Internal.Xml.Linq.ComponentModel.XDeferredAxis`1<System.Xml.Linq.XElement>
struct XDeferredAxis_1_t09F31BE4F91C49ACD687B068AA30BE5949D83E3E;
// MS.Internal.Xml.Linq.ComponentModel.XDeferredSingleton`1<System.Xml.Linq.XAttribute>
struct XDeferredSingleton_1_t7C8C8735370E05490AD1578B4C1A58CE50766E84;
// MS.Internal.Xml.Linq.ComponentModel.XDeferredSingleton`1<System.Xml.Linq.XElement>
struct XDeferredSingleton_1_tC860FD4576C6582D4FE5320A114A68E992162504;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Attribute[]
struct AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement>
struct IEnumerable_1_t7CE0C1128FF206142728C6F2878581A28330D757;
// System.Collections.Generic.List`1<System.Xml.Linq.XAttribute>
struct List_1_t086F7CFD3949A2E3FFB2410304205A5C947629CA;
// System.Collections.Generic.List`1<UnityEngine.Experimental.SubsystemDescriptor>
struct List_1_tDC5945E3E5C2531BBD805BC13C39514143200C98;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.ComponentModel.AttributeCollection
struct AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE;
// System.ComponentModel.TypeConverter
struct TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`3<System.Xml.Linq.XElement,System.Xml.Linq.XName,System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement>>
struct Func_3_t036A925D26033259BAD0288212362AD3B832B326;
// System.Func`3<System.Xml.Linq.XElement,System.Xml.Linq.XName,System.Xml.Linq.XAttribute>
struct Func_3_t6BC35E42DCF60365A4FC21EB230A592B00043787;
// System.Func`3<System.Xml.Linq.XElement,System.Xml.Linq.XName,System.Xml.Linq.XElement>
struct Func_3_tBD8132E09E03DDDFAAFA827439082F6743D6C0AA;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.MemoryStream
struct MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int16[]
struct Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Cryptography.HMACSHA1
struct HMACSHA1_t4E10C259BBC525A8E0ABEAE9EF01C8589F51FEE5;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t43C29A7F3A8C2DDAC9F3BF9BF739B03E4D5DE9A9;
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E;
// System.String
struct String_t;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.Xml.IDtdInfo
struct IDtdInfo_t5971A8C09914EDB816FE7A86A38288FDC4B6F80C;
// System.Xml.Linq.NamespaceResolver/NamespaceDeclaration
struct NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF;
// System.Xml.Linq.XAttribute
struct XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015;
// System.Xml.Linq.XContainer
struct XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599;
// System.Xml.Linq.XDeclaration
struct XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512;
// System.Xml.Linq.XElement
struct XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95;
// System.Xml.Linq.XName
struct XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A;
// System.Xml.Linq.XNamespace
struct XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D;
// System.Xml.Linq.XNode
struct XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751;
// System.Xml.Linq.XStreamingElement
struct XStreamingElement_t6B73236C8904BD25EBD29C8A18941012D8DA6D45;
// System.Xml.XmlWriter
struct XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_TAA90CDF50306A16DF6C29043AB49497B8E0C9DA6_H
#define U3CMODULEU3E_TAA90CDF50306A16DF6C29043AB49497B8E0C9DA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tAA90CDF50306A16DF6C29043AB49497B8E0C9DA6 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TAA90CDF50306A16DF6C29043AB49497B8E0C9DA6_H
#ifndef U3CMODULEU3E_T93DCC9DD64AD0C36686B88FC177EB2212081BDB5_H
#define U3CMODULEU3E_T93DCC9DD64AD0C36686B88FC177EB2212081BDB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t93DCC9DD64AD0C36686B88FC177EB2212081BDB5 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T93DCC9DD64AD0C36686B88FC177EB2212081BDB5_H
#ifndef U3CMODULEU3E_T54CCF983E499A2643FC67544116B427DC6EB5C39_H
#define U3CMODULEU3E_T54CCF983E499A2643FC67544116B427DC6EB5C39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t54CCF983E499A2643FC67544116B427DC6EB5C39 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T54CCF983E499A2643FC67544116B427DC6EB5C39_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T07F5E93AED2CB4162F2DD7883F51F5E5FD89E88E_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T07F5E93AED2CB4162F2DD7883F51F5E5FD89E88E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t07F5E93AED2CB4162F2DD7883F51F5E5FD89E88E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T07F5E93AED2CB4162F2DD7883F51F5E5FD89E88E_H
#ifndef ADLER32_T1764266FFA97C6DE074ACBE8444B9BC93C9F2033_H
#define ADLER32_T1764266FFA97C6DE074ACBE8444B9BC93C9F2033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Checksums.Adler32
struct  Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033  : public RuntimeObject
{
public:
	// System.UInt32 ICSharpCode.SharpZipLib.Checksums.Adler32::checksum
	uint32_t ___checksum_0;

public:
	inline static int32_t get_offset_of_checksum_0() { return static_cast<int32_t>(offsetof(Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033, ___checksum_0)); }
	inline uint32_t get_checksum_0() const { return ___checksum_0; }
	inline uint32_t* get_address_of_checksum_0() { return &___checksum_0; }
	inline void set_checksum_0(uint32_t value)
	{
		___checksum_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADLER32_T1764266FFA97C6DE074ACBE8444B9BC93C9F2033_H
#ifndef CRC32_T92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F_H
#define CRC32_T92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Checksums.Crc32
struct  Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F  : public RuntimeObject
{
public:
	// System.UInt32 ICSharpCode.SharpZipLib.Checksums.Crc32::crc
	uint32_t ___crc_1;

public:
	inline static int32_t get_offset_of_crc_1() { return static_cast<int32_t>(offsetof(Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F, ___crc_1)); }
	inline uint32_t get_crc_1() const { return ___crc_1; }
	inline uint32_t* get_address_of_crc_1() { return &___crc_1; }
	inline void set_crc_1(uint32_t value)
	{
		___crc_1 = value;
	}
};

struct Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F_StaticFields
{
public:
	// System.UInt32[] ICSharpCode.SharpZipLib.Checksums.Crc32::CrcTable
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___CrcTable_0;

public:
	inline static int32_t get_offset_of_CrcTable_0() { return static_cast<int32_t>(offsetof(Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F_StaticFields, ___CrcTable_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_CrcTable_0() const { return ___CrcTable_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_CrcTable_0() { return &___CrcTable_0; }
	inline void set_CrcTable_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___CrcTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___CrcTable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRC32_T92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F_H
#ifndef STREAMUTILS_TC8B2EC2CC286B4E36E2D78C3D96D64770DC3D6F4_H
#define STREAMUTILS_TC8B2EC2CC286B4E36E2D78C3D96D64770DC3D6F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Core.StreamUtils
struct  StreamUtils_tC8B2EC2CC286B4E36E2D78C3D96D64770DC3D6F4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMUTILS_TC8B2EC2CC286B4E36E2D78C3D96D64770DC3D6F4_H
#ifndef PKZIPCLASSICCRYPTOBASE_T82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B_H
#define PKZIPCLASSICCRYPTOBASE_T82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase
struct  PkzipClassicCryptoBase_t82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B  : public RuntimeObject
{
public:
	// System.UInt32[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::keys
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___keys_0;

public:
	inline static int32_t get_offset_of_keys_0() { return static_cast<int32_t>(offsetof(PkzipClassicCryptoBase_t82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B, ___keys_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_keys_0() const { return ___keys_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_keys_0() { return &___keys_0; }
	inline void set_keys_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___keys_0 = value;
		Il2CppCodeGenWriteBarrier((&___keys_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKZIPCLASSICCRYPTOBASE_T82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B_H
#ifndef ZIPAESTRANSFORM_TD2F237B71B8532EC36AAF6E45E4FFAB185301195_H
#define ZIPAESTRANSFORM_TD2F237B71B8532EC36AAF6E45E4FFAB185301195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform
struct  ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195  : public RuntimeObject
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_blockSize
	int32_t ____blockSize_0;
	// System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_encryptor
	RuntimeObject* ____encryptor_1;
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_counterNonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____counterNonce_2;
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_encryptBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____encryptBuffer_3;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_encrPos
	int32_t ____encrPos_4;
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_pwdVerifier
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____pwdVerifier_5;
	// System.Security.Cryptography.HMACSHA1 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_hmacsha1
	HMACSHA1_t4E10C259BBC525A8E0ABEAE9EF01C8589F51FEE5 * ____hmacsha1_6;
	// System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_finalised
	bool ____finalised_7;
	// System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_writeMode
	bool ____writeMode_8;

public:
	inline static int32_t get_offset_of__blockSize_0() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____blockSize_0)); }
	inline int32_t get__blockSize_0() const { return ____blockSize_0; }
	inline int32_t* get_address_of__blockSize_0() { return &____blockSize_0; }
	inline void set__blockSize_0(int32_t value)
	{
		____blockSize_0 = value;
	}

	inline static int32_t get_offset_of__encryptor_1() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____encryptor_1)); }
	inline RuntimeObject* get__encryptor_1() const { return ____encryptor_1; }
	inline RuntimeObject** get_address_of__encryptor_1() { return &____encryptor_1; }
	inline void set__encryptor_1(RuntimeObject* value)
	{
		____encryptor_1 = value;
		Il2CppCodeGenWriteBarrier((&____encryptor_1), value);
	}

	inline static int32_t get_offset_of__counterNonce_2() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____counterNonce_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__counterNonce_2() const { return ____counterNonce_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__counterNonce_2() { return &____counterNonce_2; }
	inline void set__counterNonce_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____counterNonce_2 = value;
		Il2CppCodeGenWriteBarrier((&____counterNonce_2), value);
	}

	inline static int32_t get_offset_of__encryptBuffer_3() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____encryptBuffer_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__encryptBuffer_3() const { return ____encryptBuffer_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__encryptBuffer_3() { return &____encryptBuffer_3; }
	inline void set__encryptBuffer_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____encryptBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&____encryptBuffer_3), value);
	}

	inline static int32_t get_offset_of__encrPos_4() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____encrPos_4)); }
	inline int32_t get__encrPos_4() const { return ____encrPos_4; }
	inline int32_t* get_address_of__encrPos_4() { return &____encrPos_4; }
	inline void set__encrPos_4(int32_t value)
	{
		____encrPos_4 = value;
	}

	inline static int32_t get_offset_of__pwdVerifier_5() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____pwdVerifier_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__pwdVerifier_5() const { return ____pwdVerifier_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__pwdVerifier_5() { return &____pwdVerifier_5; }
	inline void set__pwdVerifier_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____pwdVerifier_5 = value;
		Il2CppCodeGenWriteBarrier((&____pwdVerifier_5), value);
	}

	inline static int32_t get_offset_of__hmacsha1_6() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____hmacsha1_6)); }
	inline HMACSHA1_t4E10C259BBC525A8E0ABEAE9EF01C8589F51FEE5 * get__hmacsha1_6() const { return ____hmacsha1_6; }
	inline HMACSHA1_t4E10C259BBC525A8E0ABEAE9EF01C8589F51FEE5 ** get_address_of__hmacsha1_6() { return &____hmacsha1_6; }
	inline void set__hmacsha1_6(HMACSHA1_t4E10C259BBC525A8E0ABEAE9EF01C8589F51FEE5 * value)
	{
		____hmacsha1_6 = value;
		Il2CppCodeGenWriteBarrier((&____hmacsha1_6), value);
	}

	inline static int32_t get_offset_of__finalised_7() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____finalised_7)); }
	inline bool get__finalised_7() const { return ____finalised_7; }
	inline bool* get_address_of__finalised_7() { return &____finalised_7; }
	inline void set__finalised_7(bool value)
	{
		____finalised_7 = value;
	}

	inline static int32_t get_offset_of__writeMode_8() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____writeMode_8)); }
	inline bool get__writeMode_8() const { return ____writeMode_8; }
	inline bool* get_address_of__writeMode_8() { return &____writeMode_8; }
	inline void set__writeMode_8(bool value)
	{
		____writeMode_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPAESTRANSFORM_TD2F237B71B8532EC36AAF6E45E4FFAB185301195_H
#ifndef TARARCHIVE_T788C674DE76F953D2C2B6CC4DF838FCE0ED22726_H
#define TARARCHIVE_T788C674DE76F953D2C2B6CC4DF838FCE0ED22726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.TarArchive
struct  TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726  : public RuntimeObject
{
public:
	// ICSharpCode.SharpZipLib.Tar.ProgressMessageHandler ICSharpCode.SharpZipLib.Tar.TarArchive::ProgressMessageEvent
	ProgressMessageHandler_tCB79C5F0AE44A5C4FA7F7D8363992A25DDEDC4EF * ___ProgressMessageEvent_0;
	// System.Boolean ICSharpCode.SharpZipLib.Tar.TarArchive::keepOldFiles
	bool ___keepOldFiles_1;
	// System.Boolean ICSharpCode.SharpZipLib.Tar.TarArchive::asciiTranslate
	bool ___asciiTranslate_2;
	// System.String ICSharpCode.SharpZipLib.Tar.TarArchive::userName
	String_t* ___userName_3;
	// System.String ICSharpCode.SharpZipLib.Tar.TarArchive::groupName
	String_t* ___groupName_4;
	// ICSharpCode.SharpZipLib.Tar.TarInputStream ICSharpCode.SharpZipLib.Tar.TarArchive::tarIn
	TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E * ___tarIn_5;
	// ICSharpCode.SharpZipLib.Tar.TarOutputStream ICSharpCode.SharpZipLib.Tar.TarArchive::tarOut
	TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838 * ___tarOut_6;
	// System.Boolean ICSharpCode.SharpZipLib.Tar.TarArchive::isDisposed
	bool ___isDisposed_7;

public:
	inline static int32_t get_offset_of_ProgressMessageEvent_0() { return static_cast<int32_t>(offsetof(TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726, ___ProgressMessageEvent_0)); }
	inline ProgressMessageHandler_tCB79C5F0AE44A5C4FA7F7D8363992A25DDEDC4EF * get_ProgressMessageEvent_0() const { return ___ProgressMessageEvent_0; }
	inline ProgressMessageHandler_tCB79C5F0AE44A5C4FA7F7D8363992A25DDEDC4EF ** get_address_of_ProgressMessageEvent_0() { return &___ProgressMessageEvent_0; }
	inline void set_ProgressMessageEvent_0(ProgressMessageHandler_tCB79C5F0AE44A5C4FA7F7D8363992A25DDEDC4EF * value)
	{
		___ProgressMessageEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___ProgressMessageEvent_0), value);
	}

	inline static int32_t get_offset_of_keepOldFiles_1() { return static_cast<int32_t>(offsetof(TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726, ___keepOldFiles_1)); }
	inline bool get_keepOldFiles_1() const { return ___keepOldFiles_1; }
	inline bool* get_address_of_keepOldFiles_1() { return &___keepOldFiles_1; }
	inline void set_keepOldFiles_1(bool value)
	{
		___keepOldFiles_1 = value;
	}

	inline static int32_t get_offset_of_asciiTranslate_2() { return static_cast<int32_t>(offsetof(TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726, ___asciiTranslate_2)); }
	inline bool get_asciiTranslate_2() const { return ___asciiTranslate_2; }
	inline bool* get_address_of_asciiTranslate_2() { return &___asciiTranslate_2; }
	inline void set_asciiTranslate_2(bool value)
	{
		___asciiTranslate_2 = value;
	}

	inline static int32_t get_offset_of_userName_3() { return static_cast<int32_t>(offsetof(TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726, ___userName_3)); }
	inline String_t* get_userName_3() const { return ___userName_3; }
	inline String_t** get_address_of_userName_3() { return &___userName_3; }
	inline void set_userName_3(String_t* value)
	{
		___userName_3 = value;
		Il2CppCodeGenWriteBarrier((&___userName_3), value);
	}

	inline static int32_t get_offset_of_groupName_4() { return static_cast<int32_t>(offsetof(TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726, ___groupName_4)); }
	inline String_t* get_groupName_4() const { return ___groupName_4; }
	inline String_t** get_address_of_groupName_4() { return &___groupName_4; }
	inline void set_groupName_4(String_t* value)
	{
		___groupName_4 = value;
		Il2CppCodeGenWriteBarrier((&___groupName_4), value);
	}

	inline static int32_t get_offset_of_tarIn_5() { return static_cast<int32_t>(offsetof(TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726, ___tarIn_5)); }
	inline TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E * get_tarIn_5() const { return ___tarIn_5; }
	inline TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E ** get_address_of_tarIn_5() { return &___tarIn_5; }
	inline void set_tarIn_5(TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E * value)
	{
		___tarIn_5 = value;
		Il2CppCodeGenWriteBarrier((&___tarIn_5), value);
	}

	inline static int32_t get_offset_of_tarOut_6() { return static_cast<int32_t>(offsetof(TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726, ___tarOut_6)); }
	inline TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838 * get_tarOut_6() const { return ___tarOut_6; }
	inline TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838 ** get_address_of_tarOut_6() { return &___tarOut_6; }
	inline void set_tarOut_6(TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838 * value)
	{
		___tarOut_6 = value;
		Il2CppCodeGenWriteBarrier((&___tarOut_6), value);
	}

	inline static int32_t get_offset_of_isDisposed_7() { return static_cast<int32_t>(offsetof(TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726, ___isDisposed_7)); }
	inline bool get_isDisposed_7() const { return ___isDisposed_7; }
	inline bool* get_address_of_isDisposed_7() { return &___isDisposed_7; }
	inline void set_isDisposed_7(bool value)
	{
		___isDisposed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARARCHIVE_T788C674DE76F953D2C2B6CC4DF838FCE0ED22726_H
#ifndef TARBUFFER_T96EE7BB130E846A706DBC5D6049D057FC1A3701C_H
#define TARBUFFER_T96EE7BB130E846A706DBC5D6049D057FC1A3701C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.TarBuffer
struct  TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C  : public RuntimeObject
{
public:
	// System.IO.Stream ICSharpCode.SharpZipLib.Tar.TarBuffer::inputStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___inputStream_0;
	// System.IO.Stream ICSharpCode.SharpZipLib.Tar.TarBuffer::outputStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___outputStream_1;
	// System.Byte[] ICSharpCode.SharpZipLib.Tar.TarBuffer::recordBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___recordBuffer_2;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::currentBlockIndex
	int32_t ___currentBlockIndex_3;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::currentRecordIndex
	int32_t ___currentRecordIndex_4;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::recordSize
	int32_t ___recordSize_5;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::blockFactor
	int32_t ___blockFactor_6;
	// System.Boolean ICSharpCode.SharpZipLib.Tar.TarBuffer::isStreamOwner_
	bool ___isStreamOwner__7;

public:
	inline static int32_t get_offset_of_inputStream_0() { return static_cast<int32_t>(offsetof(TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C, ___inputStream_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_inputStream_0() const { return ___inputStream_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_inputStream_0() { return &___inputStream_0; }
	inline void set_inputStream_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___inputStream_0 = value;
		Il2CppCodeGenWriteBarrier((&___inputStream_0), value);
	}

	inline static int32_t get_offset_of_outputStream_1() { return static_cast<int32_t>(offsetof(TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C, ___outputStream_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_outputStream_1() const { return ___outputStream_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_outputStream_1() { return &___outputStream_1; }
	inline void set_outputStream_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___outputStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___outputStream_1), value);
	}

	inline static int32_t get_offset_of_recordBuffer_2() { return static_cast<int32_t>(offsetof(TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C, ___recordBuffer_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_recordBuffer_2() const { return ___recordBuffer_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_recordBuffer_2() { return &___recordBuffer_2; }
	inline void set_recordBuffer_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___recordBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___recordBuffer_2), value);
	}

	inline static int32_t get_offset_of_currentBlockIndex_3() { return static_cast<int32_t>(offsetof(TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C, ___currentBlockIndex_3)); }
	inline int32_t get_currentBlockIndex_3() const { return ___currentBlockIndex_3; }
	inline int32_t* get_address_of_currentBlockIndex_3() { return &___currentBlockIndex_3; }
	inline void set_currentBlockIndex_3(int32_t value)
	{
		___currentBlockIndex_3 = value;
	}

	inline static int32_t get_offset_of_currentRecordIndex_4() { return static_cast<int32_t>(offsetof(TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C, ___currentRecordIndex_4)); }
	inline int32_t get_currentRecordIndex_4() const { return ___currentRecordIndex_4; }
	inline int32_t* get_address_of_currentRecordIndex_4() { return &___currentRecordIndex_4; }
	inline void set_currentRecordIndex_4(int32_t value)
	{
		___currentRecordIndex_4 = value;
	}

	inline static int32_t get_offset_of_recordSize_5() { return static_cast<int32_t>(offsetof(TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C, ___recordSize_5)); }
	inline int32_t get_recordSize_5() const { return ___recordSize_5; }
	inline int32_t* get_address_of_recordSize_5() { return &___recordSize_5; }
	inline void set_recordSize_5(int32_t value)
	{
		___recordSize_5 = value;
	}

	inline static int32_t get_offset_of_blockFactor_6() { return static_cast<int32_t>(offsetof(TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C, ___blockFactor_6)); }
	inline int32_t get_blockFactor_6() const { return ___blockFactor_6; }
	inline int32_t* get_address_of_blockFactor_6() { return &___blockFactor_6; }
	inline void set_blockFactor_6(int32_t value)
	{
		___blockFactor_6 = value;
	}

	inline static int32_t get_offset_of_isStreamOwner__7() { return static_cast<int32_t>(offsetof(TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C, ___isStreamOwner__7)); }
	inline bool get_isStreamOwner__7() const { return ___isStreamOwner__7; }
	inline bool* get_address_of_isStreamOwner__7() { return &___isStreamOwner__7; }
	inline void set_isStreamOwner__7(bool value)
	{
		___isStreamOwner__7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARBUFFER_T96EE7BB130E846A706DBC5D6049D057FC1A3701C_H
#ifndef TARENTRY_TDE18EDBB29A64DBD744028FA5BAF5026519398BE_H
#define TARENTRY_TDE18EDBB29A64DBD744028FA5BAF5026519398BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.TarEntry
struct  TarEntry_tDE18EDBB29A64DBD744028FA5BAF5026519398BE  : public RuntimeObject
{
public:
	// System.String ICSharpCode.SharpZipLib.Tar.TarEntry::file
	String_t* ___file_0;
	// ICSharpCode.SharpZipLib.Tar.TarHeader ICSharpCode.SharpZipLib.Tar.TarEntry::header
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74 * ___header_1;

public:
	inline static int32_t get_offset_of_file_0() { return static_cast<int32_t>(offsetof(TarEntry_tDE18EDBB29A64DBD744028FA5BAF5026519398BE, ___file_0)); }
	inline String_t* get_file_0() const { return ___file_0; }
	inline String_t** get_address_of_file_0() { return &___file_0; }
	inline void set_file_0(String_t* value)
	{
		___file_0 = value;
		Il2CppCodeGenWriteBarrier((&___file_0), value);
	}

	inline static int32_t get_offset_of_header_1() { return static_cast<int32_t>(offsetof(TarEntry_tDE18EDBB29A64DBD744028FA5BAF5026519398BE, ___header_1)); }
	inline TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74 * get_header_1() const { return ___header_1; }
	inline TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74 ** get_address_of_header_1() { return &___header_1; }
	inline void set_header_1(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74 * value)
	{
		___header_1 = value;
		Il2CppCodeGenWriteBarrier((&___header_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARENTRY_TDE18EDBB29A64DBD744028FA5BAF5026519398BE_H
#ifndef DEFLATERHUFFMAN_T07ABC2467D43109DA82829D4D0476554059A12BD_H
#define DEFLATERHUFFMAN_T07ABC2467D43109DA82829D4D0476554059A12BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman
struct  DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD  : public RuntimeObject
{
public:

public:
};

struct DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields
{
public:
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::BL_ORDER
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___BL_ORDER_0;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::bit4Reverse
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bit4Reverse_1;
	// System.Int16[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::staticLCodes
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___staticLCodes_2;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::staticLLength
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___staticLLength_3;
	// System.Int16[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::staticDCodes
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___staticDCodes_4;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::staticDLength
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___staticDLength_5;

public:
	inline static int32_t get_offset_of_BL_ORDER_0() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields, ___BL_ORDER_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_BL_ORDER_0() const { return ___BL_ORDER_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_BL_ORDER_0() { return &___BL_ORDER_0; }
	inline void set_BL_ORDER_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___BL_ORDER_0 = value;
		Il2CppCodeGenWriteBarrier((&___BL_ORDER_0), value);
	}

	inline static int32_t get_offset_of_bit4Reverse_1() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields, ___bit4Reverse_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bit4Reverse_1() const { return ___bit4Reverse_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bit4Reverse_1() { return &___bit4Reverse_1; }
	inline void set_bit4Reverse_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bit4Reverse_1 = value;
		Il2CppCodeGenWriteBarrier((&___bit4Reverse_1), value);
	}

	inline static int32_t get_offset_of_staticLCodes_2() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields, ___staticLCodes_2)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_staticLCodes_2() const { return ___staticLCodes_2; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_staticLCodes_2() { return &___staticLCodes_2; }
	inline void set_staticLCodes_2(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___staticLCodes_2 = value;
		Il2CppCodeGenWriteBarrier((&___staticLCodes_2), value);
	}

	inline static int32_t get_offset_of_staticLLength_3() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields, ___staticLLength_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_staticLLength_3() const { return ___staticLLength_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_staticLLength_3() { return &___staticLLength_3; }
	inline void set_staticLLength_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___staticLLength_3 = value;
		Il2CppCodeGenWriteBarrier((&___staticLLength_3), value);
	}

	inline static int32_t get_offset_of_staticDCodes_4() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields, ___staticDCodes_4)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_staticDCodes_4() const { return ___staticDCodes_4; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_staticDCodes_4() { return &___staticDCodes_4; }
	inline void set_staticDCodes_4(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___staticDCodes_4 = value;
		Il2CppCodeGenWriteBarrier((&___staticDCodes_4), value);
	}

	inline static int32_t get_offset_of_staticDLength_5() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields, ___staticDLength_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_staticDLength_5() const { return ___staticDLength_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_staticDLength_5() { return &___staticDLength_5; }
	inline void set_staticDLength_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___staticDLength_5 = value;
		Il2CppCodeGenWriteBarrier((&___staticDLength_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATERHUFFMAN_T07ABC2467D43109DA82829D4D0476554059A12BD_H
#ifndef INFLATER_T69AF9807C58C78BD7FCF1A0372F416961C1C73CB_H
#define INFLATER_T69AF9807C58C78BD7FCF1A0372F416961C1C73CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct  Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB  : public RuntimeObject
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::mode
	int32_t ___mode_4;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::readAdler
	int32_t ___readAdler_5;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::neededBits
	int32_t ___neededBits_6;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::repLength
	int32_t ___repLength_7;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::repDist
	int32_t ___repDist_8;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::uncomprLen
	int32_t ___uncomprLen_9;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::isLastBlock
	bool ___isLastBlock_10;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::totalOut
	int64_t ___totalOut_11;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::totalIn
	int64_t ___totalIn_12;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::noHeader
	bool ___noHeader_13;
	// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator ICSharpCode.SharpZipLib.Zip.Compression.Inflater::input
	StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5 * ___input_14;
	// ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow ICSharpCode.SharpZipLib.Zip.Compression.Inflater::outputWindow
	OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581 * ___outputWindow_15;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader ICSharpCode.SharpZipLib.Zip.Compression.Inflater::dynHeader
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3 * ___dynHeader_16;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.Inflater::litlenTree
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * ___litlenTree_17;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.Inflater::distTree
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * ___distTree_18;
	// ICSharpCode.SharpZipLib.Checksums.Adler32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::adler
	Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033 * ___adler_19;

public:
	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_readAdler_5() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___readAdler_5)); }
	inline int32_t get_readAdler_5() const { return ___readAdler_5; }
	inline int32_t* get_address_of_readAdler_5() { return &___readAdler_5; }
	inline void set_readAdler_5(int32_t value)
	{
		___readAdler_5 = value;
	}

	inline static int32_t get_offset_of_neededBits_6() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___neededBits_6)); }
	inline int32_t get_neededBits_6() const { return ___neededBits_6; }
	inline int32_t* get_address_of_neededBits_6() { return &___neededBits_6; }
	inline void set_neededBits_6(int32_t value)
	{
		___neededBits_6 = value;
	}

	inline static int32_t get_offset_of_repLength_7() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___repLength_7)); }
	inline int32_t get_repLength_7() const { return ___repLength_7; }
	inline int32_t* get_address_of_repLength_7() { return &___repLength_7; }
	inline void set_repLength_7(int32_t value)
	{
		___repLength_7 = value;
	}

	inline static int32_t get_offset_of_repDist_8() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___repDist_8)); }
	inline int32_t get_repDist_8() const { return ___repDist_8; }
	inline int32_t* get_address_of_repDist_8() { return &___repDist_8; }
	inline void set_repDist_8(int32_t value)
	{
		___repDist_8 = value;
	}

	inline static int32_t get_offset_of_uncomprLen_9() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___uncomprLen_9)); }
	inline int32_t get_uncomprLen_9() const { return ___uncomprLen_9; }
	inline int32_t* get_address_of_uncomprLen_9() { return &___uncomprLen_9; }
	inline void set_uncomprLen_9(int32_t value)
	{
		___uncomprLen_9 = value;
	}

	inline static int32_t get_offset_of_isLastBlock_10() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___isLastBlock_10)); }
	inline bool get_isLastBlock_10() const { return ___isLastBlock_10; }
	inline bool* get_address_of_isLastBlock_10() { return &___isLastBlock_10; }
	inline void set_isLastBlock_10(bool value)
	{
		___isLastBlock_10 = value;
	}

	inline static int32_t get_offset_of_totalOut_11() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___totalOut_11)); }
	inline int64_t get_totalOut_11() const { return ___totalOut_11; }
	inline int64_t* get_address_of_totalOut_11() { return &___totalOut_11; }
	inline void set_totalOut_11(int64_t value)
	{
		___totalOut_11 = value;
	}

	inline static int32_t get_offset_of_totalIn_12() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___totalIn_12)); }
	inline int64_t get_totalIn_12() const { return ___totalIn_12; }
	inline int64_t* get_address_of_totalIn_12() { return &___totalIn_12; }
	inline void set_totalIn_12(int64_t value)
	{
		___totalIn_12 = value;
	}

	inline static int32_t get_offset_of_noHeader_13() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___noHeader_13)); }
	inline bool get_noHeader_13() const { return ___noHeader_13; }
	inline bool* get_address_of_noHeader_13() { return &___noHeader_13; }
	inline void set_noHeader_13(bool value)
	{
		___noHeader_13 = value;
	}

	inline static int32_t get_offset_of_input_14() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___input_14)); }
	inline StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5 * get_input_14() const { return ___input_14; }
	inline StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5 ** get_address_of_input_14() { return &___input_14; }
	inline void set_input_14(StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5 * value)
	{
		___input_14 = value;
		Il2CppCodeGenWriteBarrier((&___input_14), value);
	}

	inline static int32_t get_offset_of_outputWindow_15() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___outputWindow_15)); }
	inline OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581 * get_outputWindow_15() const { return ___outputWindow_15; }
	inline OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581 ** get_address_of_outputWindow_15() { return &___outputWindow_15; }
	inline void set_outputWindow_15(OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581 * value)
	{
		___outputWindow_15 = value;
		Il2CppCodeGenWriteBarrier((&___outputWindow_15), value);
	}

	inline static int32_t get_offset_of_dynHeader_16() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___dynHeader_16)); }
	inline InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3 * get_dynHeader_16() const { return ___dynHeader_16; }
	inline InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3 ** get_address_of_dynHeader_16() { return &___dynHeader_16; }
	inline void set_dynHeader_16(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3 * value)
	{
		___dynHeader_16 = value;
		Il2CppCodeGenWriteBarrier((&___dynHeader_16), value);
	}

	inline static int32_t get_offset_of_litlenTree_17() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___litlenTree_17)); }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * get_litlenTree_17() const { return ___litlenTree_17; }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B ** get_address_of_litlenTree_17() { return &___litlenTree_17; }
	inline void set_litlenTree_17(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * value)
	{
		___litlenTree_17 = value;
		Il2CppCodeGenWriteBarrier((&___litlenTree_17), value);
	}

	inline static int32_t get_offset_of_distTree_18() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___distTree_18)); }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * get_distTree_18() const { return ___distTree_18; }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B ** get_address_of_distTree_18() { return &___distTree_18; }
	inline void set_distTree_18(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * value)
	{
		___distTree_18 = value;
		Il2CppCodeGenWriteBarrier((&___distTree_18), value);
	}

	inline static int32_t get_offset_of_adler_19() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___adler_19)); }
	inline Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033 * get_adler_19() const { return ___adler_19; }
	inline Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033 ** get_address_of_adler_19() { return &___adler_19; }
	inline void set_adler_19(Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033 * value)
	{
		___adler_19 = value;
		Il2CppCodeGenWriteBarrier((&___adler_19), value);
	}
};

struct Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields
{
public:
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPLENS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___CPLENS_0;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPLEXT
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___CPLEXT_1;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPDIST
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___CPDIST_2;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPDEXT
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___CPDEXT_3;

public:
	inline static int32_t get_offset_of_CPLENS_0() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields, ___CPLENS_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_CPLENS_0() const { return ___CPLENS_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_CPLENS_0() { return &___CPLENS_0; }
	inline void set_CPLENS_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___CPLENS_0 = value;
		Il2CppCodeGenWriteBarrier((&___CPLENS_0), value);
	}

	inline static int32_t get_offset_of_CPLEXT_1() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields, ___CPLEXT_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_CPLEXT_1() const { return ___CPLEXT_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_CPLEXT_1() { return &___CPLEXT_1; }
	inline void set_CPLEXT_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___CPLEXT_1 = value;
		Il2CppCodeGenWriteBarrier((&___CPLEXT_1), value);
	}

	inline static int32_t get_offset_of_CPDIST_2() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields, ___CPDIST_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_CPDIST_2() const { return ___CPDIST_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_CPDIST_2() { return &___CPDIST_2; }
	inline void set_CPDIST_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___CPDIST_2 = value;
		Il2CppCodeGenWriteBarrier((&___CPDIST_2), value);
	}

	inline static int32_t get_offset_of_CPDEXT_3() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields, ___CPDEXT_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_CPDEXT_3() const { return ___CPDEXT_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_CPDEXT_3() { return &___CPDEXT_3; }
	inline void set_CPDEXT_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___CPDEXT_3 = value;
		Il2CppCodeGenWriteBarrier((&___CPDEXT_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATER_T69AF9807C58C78BD7FCF1A0372F416961C1C73CB_H
#ifndef INFLATERDYNHEADER_T4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_H
#define INFLATERDYNHEADER_T4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader
struct  InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3  : public RuntimeObject
{
public:
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::blLens
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___blLens_3;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::litdistLens
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___litdistLens_4;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::blTree
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * ___blTree_5;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::mode
	int32_t ___mode_6;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::lnum
	int32_t ___lnum_7;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::dnum
	int32_t ___dnum_8;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::blnum
	int32_t ___blnum_9;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::num
	int32_t ___num_10;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::repSymbol
	int32_t ___repSymbol_11;
	// System.Byte ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::lastLen
	uint8_t ___lastLen_12;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::ptr
	int32_t ___ptr_13;

public:
	inline static int32_t get_offset_of_blLens_3() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___blLens_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_blLens_3() const { return ___blLens_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_blLens_3() { return &___blLens_3; }
	inline void set_blLens_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___blLens_3 = value;
		Il2CppCodeGenWriteBarrier((&___blLens_3), value);
	}

	inline static int32_t get_offset_of_litdistLens_4() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___litdistLens_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_litdistLens_4() const { return ___litdistLens_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_litdistLens_4() { return &___litdistLens_4; }
	inline void set_litdistLens_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___litdistLens_4 = value;
		Il2CppCodeGenWriteBarrier((&___litdistLens_4), value);
	}

	inline static int32_t get_offset_of_blTree_5() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___blTree_5)); }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * get_blTree_5() const { return ___blTree_5; }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B ** get_address_of_blTree_5() { return &___blTree_5; }
	inline void set_blTree_5(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * value)
	{
		___blTree_5 = value;
		Il2CppCodeGenWriteBarrier((&___blTree_5), value);
	}

	inline static int32_t get_offset_of_mode_6() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___mode_6)); }
	inline int32_t get_mode_6() const { return ___mode_6; }
	inline int32_t* get_address_of_mode_6() { return &___mode_6; }
	inline void set_mode_6(int32_t value)
	{
		___mode_6 = value;
	}

	inline static int32_t get_offset_of_lnum_7() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___lnum_7)); }
	inline int32_t get_lnum_7() const { return ___lnum_7; }
	inline int32_t* get_address_of_lnum_7() { return &___lnum_7; }
	inline void set_lnum_7(int32_t value)
	{
		___lnum_7 = value;
	}

	inline static int32_t get_offset_of_dnum_8() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___dnum_8)); }
	inline int32_t get_dnum_8() const { return ___dnum_8; }
	inline int32_t* get_address_of_dnum_8() { return &___dnum_8; }
	inline void set_dnum_8(int32_t value)
	{
		___dnum_8 = value;
	}

	inline static int32_t get_offset_of_blnum_9() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___blnum_9)); }
	inline int32_t get_blnum_9() const { return ___blnum_9; }
	inline int32_t* get_address_of_blnum_9() { return &___blnum_9; }
	inline void set_blnum_9(int32_t value)
	{
		___blnum_9 = value;
	}

	inline static int32_t get_offset_of_num_10() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___num_10)); }
	inline int32_t get_num_10() const { return ___num_10; }
	inline int32_t* get_address_of_num_10() { return &___num_10; }
	inline void set_num_10(int32_t value)
	{
		___num_10 = value;
	}

	inline static int32_t get_offset_of_repSymbol_11() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___repSymbol_11)); }
	inline int32_t get_repSymbol_11() const { return ___repSymbol_11; }
	inline int32_t* get_address_of_repSymbol_11() { return &___repSymbol_11; }
	inline void set_repSymbol_11(int32_t value)
	{
		___repSymbol_11 = value;
	}

	inline static int32_t get_offset_of_lastLen_12() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___lastLen_12)); }
	inline uint8_t get_lastLen_12() const { return ___lastLen_12; }
	inline uint8_t* get_address_of_lastLen_12() { return &___lastLen_12; }
	inline void set_lastLen_12(uint8_t value)
	{
		___lastLen_12 = value;
	}

	inline static int32_t get_offset_of_ptr_13() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___ptr_13)); }
	inline int32_t get_ptr_13() const { return ___ptr_13; }
	inline int32_t* get_address_of_ptr_13() { return &___ptr_13; }
	inline void set_ptr_13(int32_t value)
	{
		___ptr_13 = value;
	}
};

struct InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields
{
public:
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::repMin
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___repMin_0;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::repBits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___repBits_1;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::BL_ORDER
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___BL_ORDER_2;

public:
	inline static int32_t get_offset_of_repMin_0() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields, ___repMin_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_repMin_0() const { return ___repMin_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_repMin_0() { return &___repMin_0; }
	inline void set_repMin_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___repMin_0 = value;
		Il2CppCodeGenWriteBarrier((&___repMin_0), value);
	}

	inline static int32_t get_offset_of_repBits_1() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields, ___repBits_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_repBits_1() const { return ___repBits_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_repBits_1() { return &___repBits_1; }
	inline void set_repBits_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___repBits_1 = value;
		Il2CppCodeGenWriteBarrier((&___repBits_1), value);
	}

	inline static int32_t get_offset_of_BL_ORDER_2() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields, ___BL_ORDER_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_BL_ORDER_2() const { return ___BL_ORDER_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_BL_ORDER_2() { return &___BL_ORDER_2; }
	inline void set_BL_ORDER_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___BL_ORDER_2 = value;
		Il2CppCodeGenWriteBarrier((&___BL_ORDER_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATERDYNHEADER_T4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_H
#ifndef INFLATERHUFFMANTREE_T3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_H
#define INFLATERHUFFMANTREE_T3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree
struct  InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B  : public RuntimeObject
{
public:
	// System.Int16[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::tree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___tree_0;

public:
	inline static int32_t get_offset_of_tree_0() { return static_cast<int32_t>(offsetof(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B, ___tree_0)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_tree_0() const { return ___tree_0; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_tree_0() { return &___tree_0; }
	inline void set_tree_0(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___tree_0 = value;
		Il2CppCodeGenWriteBarrier((&___tree_0), value);
	}
};

struct InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_StaticFields
{
public:
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::defLitLenTree
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * ___defLitLenTree_1;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::defDistTree
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * ___defDistTree_2;

public:
	inline static int32_t get_offset_of_defLitLenTree_1() { return static_cast<int32_t>(offsetof(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_StaticFields, ___defLitLenTree_1)); }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * get_defLitLenTree_1() const { return ___defLitLenTree_1; }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B ** get_address_of_defLitLenTree_1() { return &___defLitLenTree_1; }
	inline void set_defLitLenTree_1(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * value)
	{
		___defLitLenTree_1 = value;
		Il2CppCodeGenWriteBarrier((&___defLitLenTree_1), value);
	}

	inline static int32_t get_offset_of_defDistTree_2() { return static_cast<int32_t>(offsetof(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_StaticFields, ___defDistTree_2)); }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * get_defDistTree_2() const { return ___defDistTree_2; }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B ** get_address_of_defDistTree_2() { return &___defDistTree_2; }
	inline void set_defDistTree_2(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * value)
	{
		___defDistTree_2 = value;
		Il2CppCodeGenWriteBarrier((&___defDistTree_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATERHUFFMANTREE_T3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_H
#ifndef INFLATERINPUTBUFFER_T7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3_H
#define INFLATERINPUTBUFFER_T7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer
struct  InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3  : public RuntimeObject
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::rawLength
	int32_t ___rawLength_0;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::rawData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___rawData_1;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::clearTextLength
	int32_t ___clearTextLength_2;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::clearText
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___clearText_3;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::available
	int32_t ___available_4;
	// System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::cryptoTransform
	RuntimeObject* ___cryptoTransform_5;
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::inputStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___inputStream_6;

public:
	inline static int32_t get_offset_of_rawLength_0() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3, ___rawLength_0)); }
	inline int32_t get_rawLength_0() const { return ___rawLength_0; }
	inline int32_t* get_address_of_rawLength_0() { return &___rawLength_0; }
	inline void set_rawLength_0(int32_t value)
	{
		___rawLength_0 = value;
	}

	inline static int32_t get_offset_of_rawData_1() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3, ___rawData_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_rawData_1() const { return ___rawData_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_rawData_1() { return &___rawData_1; }
	inline void set_rawData_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___rawData_1 = value;
		Il2CppCodeGenWriteBarrier((&___rawData_1), value);
	}

	inline static int32_t get_offset_of_clearTextLength_2() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3, ___clearTextLength_2)); }
	inline int32_t get_clearTextLength_2() const { return ___clearTextLength_2; }
	inline int32_t* get_address_of_clearTextLength_2() { return &___clearTextLength_2; }
	inline void set_clearTextLength_2(int32_t value)
	{
		___clearTextLength_2 = value;
	}

	inline static int32_t get_offset_of_clearText_3() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3, ___clearText_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_clearText_3() const { return ___clearText_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_clearText_3() { return &___clearText_3; }
	inline void set_clearText_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___clearText_3 = value;
		Il2CppCodeGenWriteBarrier((&___clearText_3), value);
	}

	inline static int32_t get_offset_of_available_4() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3, ___available_4)); }
	inline int32_t get_available_4() const { return ___available_4; }
	inline int32_t* get_address_of_available_4() { return &___available_4; }
	inline void set_available_4(int32_t value)
	{
		___available_4 = value;
	}

	inline static int32_t get_offset_of_cryptoTransform_5() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3, ___cryptoTransform_5)); }
	inline RuntimeObject* get_cryptoTransform_5() const { return ___cryptoTransform_5; }
	inline RuntimeObject** get_address_of_cryptoTransform_5() { return &___cryptoTransform_5; }
	inline void set_cryptoTransform_5(RuntimeObject* value)
	{
		___cryptoTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___cryptoTransform_5), value);
	}

	inline static int32_t get_offset_of_inputStream_6() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3, ___inputStream_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_inputStream_6() const { return ___inputStream_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_inputStream_6() { return &___inputStream_6; }
	inline void set_inputStream_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___inputStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___inputStream_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATERINPUTBUFFER_T7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3_H
#ifndef OUTPUTWINDOW_T7CB804720F02D63ECB8018477FF479C596859581_H
#define OUTPUTWINDOW_T7CB804720F02D63ECB8018477FF479C596859581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow
struct  OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581  : public RuntimeObject
{
public:
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::window
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___window_0;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::windowEnd
	int32_t ___windowEnd_1;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::windowFilled
	int32_t ___windowFilled_2;

public:
	inline static int32_t get_offset_of_window_0() { return static_cast<int32_t>(offsetof(OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581, ___window_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_window_0() const { return ___window_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_window_0() { return &___window_0; }
	inline void set_window_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___window_0 = value;
		Il2CppCodeGenWriteBarrier((&___window_0), value);
	}

	inline static int32_t get_offset_of_windowEnd_1() { return static_cast<int32_t>(offsetof(OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581, ___windowEnd_1)); }
	inline int32_t get_windowEnd_1() const { return ___windowEnd_1; }
	inline int32_t* get_address_of_windowEnd_1() { return &___windowEnd_1; }
	inline void set_windowEnd_1(int32_t value)
	{
		___windowEnd_1 = value;
	}

	inline static int32_t get_offset_of_windowFilled_2() { return static_cast<int32_t>(offsetof(OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581, ___windowFilled_2)); }
	inline int32_t get_windowFilled_2() const { return ___windowFilled_2; }
	inline int32_t* get_address_of_windowFilled_2() { return &___windowFilled_2; }
	inline void set_windowFilled_2(int32_t value)
	{
		___windowFilled_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPUTWINDOW_T7CB804720F02D63ECB8018477FF479C596859581_H
#ifndef STREAMMANIPULATOR_TBAD267162B2327BE4350A4407E38A40C613CFAB5_H
#define STREAMMANIPULATOR_TBAD267162B2327BE4350A4407E38A40C613CFAB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator
struct  StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5  : public RuntimeObject
{
public:
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::window_
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___window__0;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::windowStart_
	int32_t ___windowStart__1;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::windowEnd_
	int32_t ___windowEnd__2;
	// System.UInt32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::buffer_
	uint32_t ___buffer__3;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::bitsInBuffer_
	int32_t ___bitsInBuffer__4;

public:
	inline static int32_t get_offset_of_window__0() { return static_cast<int32_t>(offsetof(StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5, ___window__0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_window__0() const { return ___window__0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_window__0() { return &___window__0; }
	inline void set_window__0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___window__0 = value;
		Il2CppCodeGenWriteBarrier((&___window__0), value);
	}

	inline static int32_t get_offset_of_windowStart__1() { return static_cast<int32_t>(offsetof(StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5, ___windowStart__1)); }
	inline int32_t get_windowStart__1() const { return ___windowStart__1; }
	inline int32_t* get_address_of_windowStart__1() { return &___windowStart__1; }
	inline void set_windowStart__1(int32_t value)
	{
		___windowStart__1 = value;
	}

	inline static int32_t get_offset_of_windowEnd__2() { return static_cast<int32_t>(offsetof(StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5, ___windowEnd__2)); }
	inline int32_t get_windowEnd__2() const { return ___windowEnd__2; }
	inline int32_t* get_address_of_windowEnd__2() { return &___windowEnd__2; }
	inline void set_windowEnd__2(int32_t value)
	{
		___windowEnd__2 = value;
	}

	inline static int32_t get_offset_of_buffer__3() { return static_cast<int32_t>(offsetof(StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5, ___buffer__3)); }
	inline uint32_t get_buffer__3() const { return ___buffer__3; }
	inline uint32_t* get_address_of_buffer__3() { return &___buffer__3; }
	inline void set_buffer__3(uint32_t value)
	{
		___buffer__3 = value;
	}

	inline static int32_t get_offset_of_bitsInBuffer__4() { return static_cast<int32_t>(offsetof(StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5, ___bitsInBuffer__4)); }
	inline int32_t get_bitsInBuffer__4() const { return ___bitsInBuffer__4; }
	inline int32_t* get_address_of_bitsInBuffer__4() { return &___bitsInBuffer__4; }
	inline void set_bitsInBuffer__4(int32_t value)
	{
		___bitsInBuffer__4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMMANIPULATOR_TBAD267162B2327BE4350A4407E38A40C613CFAB5_H
#ifndef ZIPCONSTANTS_T346D2EB237FE5E5068BED97F246531E08A363863_H
#define ZIPCONSTANTS_T346D2EB237FE5E5068BED97F246531E08A363863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipConstants
struct  ZipConstants_t346D2EB237FE5E5068BED97F246531E08A363863  : public RuntimeObject
{
public:

public:
};

struct ZipConstants_t346D2EB237FE5E5068BED97F246531E08A363863_StaticFields
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipConstants::defaultCodePage
	int32_t ___defaultCodePage_0;

public:
	inline static int32_t get_offset_of_defaultCodePage_0() { return static_cast<int32_t>(offsetof(ZipConstants_t346D2EB237FE5E5068BED97F246531E08A363863_StaticFields, ___defaultCodePage_0)); }
	inline int32_t get_defaultCodePage_0() const { return ___defaultCodePage_0; }
	inline int32_t* get_address_of_defaultCodePage_0() { return &___defaultCodePage_0; }
	inline void set_defaultCodePage_0(int32_t value)
	{
		___defaultCodePage_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPCONSTANTS_T346D2EB237FE5E5068BED97F246531E08A363863_H
#ifndef ZIPEXTRADATA_T2BE422C50624DC89A6D09B5207221310DC3C82A0_H
#define ZIPEXTRADATA_T2BE422C50624DC89A6D09B5207221310DC3C82A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipExtraData
struct  ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0  : public RuntimeObject
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::_index
	int32_t ____index_0;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::_readValueStart
	int32_t ____readValueStart_1;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::_readValueLength
	int32_t ____readValueLength_2;
	// System.IO.MemoryStream ICSharpCode.SharpZipLib.Zip.ZipExtraData::_newEntry
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ____newEntry_3;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipExtraData::_data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____data_4;

public:
	inline static int32_t get_offset_of__index_0() { return static_cast<int32_t>(offsetof(ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0, ____index_0)); }
	inline int32_t get__index_0() const { return ____index_0; }
	inline int32_t* get_address_of__index_0() { return &____index_0; }
	inline void set__index_0(int32_t value)
	{
		____index_0 = value;
	}

	inline static int32_t get_offset_of__readValueStart_1() { return static_cast<int32_t>(offsetof(ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0, ____readValueStart_1)); }
	inline int32_t get__readValueStart_1() const { return ____readValueStart_1; }
	inline int32_t* get_address_of__readValueStart_1() { return &____readValueStart_1; }
	inline void set__readValueStart_1(int32_t value)
	{
		____readValueStart_1 = value;
	}

	inline static int32_t get_offset_of__readValueLength_2() { return static_cast<int32_t>(offsetof(ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0, ____readValueLength_2)); }
	inline int32_t get__readValueLength_2() const { return ____readValueLength_2; }
	inline int32_t* get_address_of__readValueLength_2() { return &____readValueLength_2; }
	inline void set__readValueLength_2(int32_t value)
	{
		____readValueLength_2 = value;
	}

	inline static int32_t get_offset_of__newEntry_3() { return static_cast<int32_t>(offsetof(ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0, ____newEntry_3)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get__newEntry_3() const { return ____newEntry_3; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of__newEntry_3() { return &____newEntry_3; }
	inline void set__newEntry_3(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		____newEntry_3 = value;
		Il2CppCodeGenWriteBarrier((&____newEntry_3), value);
	}

	inline static int32_t get_offset_of__data_4() { return static_cast<int32_t>(offsetof(ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0, ____data_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__data_4() const { return ____data_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__data_4() { return &____data_4; }
	inline void set__data_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____data_4 = value;
		Il2CppCodeGenWriteBarrier((&____data_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPEXTRADATA_T2BE422C50624DC89A6D09B5207221310DC3C82A0_H
#ifndef ZIPENTRYENUMERATOR_TD1C0F596AD8074627C711424B3F64891F3B2037F_H
#define ZIPENTRYENUMERATOR_TD1C0F596AD8074627C711424B3F64891F3B2037F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipFile/ZipEntryEnumerator
struct  ZipEntryEnumerator_tD1C0F596AD8074627C711424B3F64891F3B2037F  : public RuntimeObject
{
public:
	// ICSharpCode.SharpZipLib.Zip.ZipEntry[] ICSharpCode.SharpZipLib.Zip.ZipFile/ZipEntryEnumerator::array
	ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917* ___array_0;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile/ZipEntryEnumerator::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_array_0() { return static_cast<int32_t>(offsetof(ZipEntryEnumerator_tD1C0F596AD8074627C711424B3F64891F3B2037F, ___array_0)); }
	inline ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917* get_array_0() const { return ___array_0; }
	inline ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917** get_address_of_array_0() { return &___array_0; }
	inline void set_array_0(ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917* value)
	{
		___array_0 = value;
		Il2CppCodeGenWriteBarrier((&___array_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(ZipEntryEnumerator_tD1C0F596AD8074627C711424B3F64891F3B2037F, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPENTRYENUMERATOR_TD1C0F596AD8074627C711424B3F64891F3B2037F_H
#ifndef ZIPNAMETRANSFORM_T8016B5EE6B0807E685F47F38135D7F000D18BAEF_H
#define ZIPNAMETRANSFORM_T8016B5EE6B0807E685F47F38135D7F000D18BAEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipNameTransform
struct  ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF  : public RuntimeObject
{
public:

public:
};

struct ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF_StaticFields
{
public:
	// System.Char[] ICSharpCode.SharpZipLib.Zip.ZipNameTransform::InvalidEntryChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___InvalidEntryChars_0;
	// System.Char[] ICSharpCode.SharpZipLib.Zip.ZipNameTransform::InvalidEntryCharsRelaxed
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___InvalidEntryCharsRelaxed_1;

public:
	inline static int32_t get_offset_of_InvalidEntryChars_0() { return static_cast<int32_t>(offsetof(ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF_StaticFields, ___InvalidEntryChars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_InvalidEntryChars_0() const { return ___InvalidEntryChars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_InvalidEntryChars_0() { return &___InvalidEntryChars_0; }
	inline void set_InvalidEntryChars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___InvalidEntryChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidEntryChars_0), value);
	}

	inline static int32_t get_offset_of_InvalidEntryCharsRelaxed_1() { return static_cast<int32_t>(offsetof(ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF_StaticFields, ___InvalidEntryCharsRelaxed_1)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_InvalidEntryCharsRelaxed_1() const { return ___InvalidEntryCharsRelaxed_1; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_InvalidEntryCharsRelaxed_1() { return &___InvalidEntryCharsRelaxed_1; }
	inline void set_InvalidEntryCharsRelaxed_1(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___InvalidEntryCharsRelaxed_1 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidEntryCharsRelaxed_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPNAMETRANSFORM_T8016B5EE6B0807E685F47F38135D7F000D18BAEF_H
#ifndef U3CU3EC_TC4AD9CB09E765F9D3B76E4C1A9BA2D8DB47CEC7A_H
#define U3CU3EC_TC4AD9CB09E765F9D3B76E4C1A9BA2D8DB47CEC7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Linq.ComponentModel.XElementAttributePropertyDescriptor/<>c
struct  U3CU3Ec_tC4AD9CB09E765F9D3B76E4C1A9BA2D8DB47CEC7A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tC4AD9CB09E765F9D3B76E4C1A9BA2D8DB47CEC7A_StaticFields
{
public:
	// MS.Internal.Xml.Linq.ComponentModel.XElementAttributePropertyDescriptor/<>c MS.Internal.Xml.Linq.ComponentModel.XElementAttributePropertyDescriptor/<>c::<>9
	U3CU3Ec_tC4AD9CB09E765F9D3B76E4C1A9BA2D8DB47CEC7A * ___U3CU3E9_0;
	// System.Func`3<System.Xml.Linq.XElement,System.Xml.Linq.XName,System.Xml.Linq.XAttribute> MS.Internal.Xml.Linq.ComponentModel.XElementAttributePropertyDescriptor/<>c::<>9__3_0
	Func_3_t6BC35E42DCF60365A4FC21EB230A592B00043787 * ___U3CU3E9__3_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC4AD9CB09E765F9D3B76E4C1A9BA2D8DB47CEC7A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tC4AD9CB09E765F9D3B76E4C1A9BA2D8DB47CEC7A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tC4AD9CB09E765F9D3B76E4C1A9BA2D8DB47CEC7A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tC4AD9CB09E765F9D3B76E4C1A9BA2D8DB47CEC7A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC4AD9CB09E765F9D3B76E4C1A9BA2D8DB47CEC7A_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_3_t6BC35E42DCF60365A4FC21EB230A592B00043787 * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_3_t6BC35E42DCF60365A4FC21EB230A592B00043787 ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_3_t6BC35E42DCF60365A4FC21EB230A592B00043787 * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TC4AD9CB09E765F9D3B76E4C1A9BA2D8DB47CEC7A_H
#ifndef U3CU3EC_T8F9160D463F532068C1A4675D8C1E1B8C4E008C3_H
#define U3CU3EC_T8F9160D463F532068C1A4675D8C1E1B8C4E008C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Linq.ComponentModel.XElementDescendantsPropertyDescriptor/<>c
struct  U3CU3Ec_t8F9160D463F532068C1A4675D8C1E1B8C4E008C3  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t8F9160D463F532068C1A4675D8C1E1B8C4E008C3_StaticFields
{
public:
	// MS.Internal.Xml.Linq.ComponentModel.XElementDescendantsPropertyDescriptor/<>c MS.Internal.Xml.Linq.ComponentModel.XElementDescendantsPropertyDescriptor/<>c::<>9
	U3CU3Ec_t8F9160D463F532068C1A4675D8C1E1B8C4E008C3 * ___U3CU3E9_0;
	// System.Func`3<System.Xml.Linq.XElement,System.Xml.Linq.XName,System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement>> MS.Internal.Xml.Linq.ComponentModel.XElementDescendantsPropertyDescriptor/<>c::<>9__3_0
	Func_3_t036A925D26033259BAD0288212362AD3B832B326 * ___U3CU3E9__3_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8F9160D463F532068C1A4675D8C1E1B8C4E008C3_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t8F9160D463F532068C1A4675D8C1E1B8C4E008C3 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t8F9160D463F532068C1A4675D8C1E1B8C4E008C3 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t8F9160D463F532068C1A4675D8C1E1B8C4E008C3 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8F9160D463F532068C1A4675D8C1E1B8C4E008C3_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_3_t036A925D26033259BAD0288212362AD3B832B326 * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_3_t036A925D26033259BAD0288212362AD3B832B326 ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_3_t036A925D26033259BAD0288212362AD3B832B326 * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T8F9160D463F532068C1A4675D8C1E1B8C4E008C3_H
#ifndef U3CU3EC_T4E705D19DC0E84DCB74017D6083C8FD98D9B0385_H
#define U3CU3EC_T4E705D19DC0E84DCB74017D6083C8FD98D9B0385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Linq.ComponentModel.XElementElementPropertyDescriptor/<>c
struct  U3CU3Ec_t4E705D19DC0E84DCB74017D6083C8FD98D9B0385  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4E705D19DC0E84DCB74017D6083C8FD98D9B0385_StaticFields
{
public:
	// MS.Internal.Xml.Linq.ComponentModel.XElementElementPropertyDescriptor/<>c MS.Internal.Xml.Linq.ComponentModel.XElementElementPropertyDescriptor/<>c::<>9
	U3CU3Ec_t4E705D19DC0E84DCB74017D6083C8FD98D9B0385 * ___U3CU3E9_0;
	// System.Func`3<System.Xml.Linq.XElement,System.Xml.Linq.XName,System.Xml.Linq.XElement> MS.Internal.Xml.Linq.ComponentModel.XElementElementPropertyDescriptor/<>c::<>9__3_0
	Func_3_tBD8132E09E03DDDFAAFA827439082F6743D6C0AA * ___U3CU3E9__3_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4E705D19DC0E84DCB74017D6083C8FD98D9B0385_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4E705D19DC0E84DCB74017D6083C8FD98D9B0385 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4E705D19DC0E84DCB74017D6083C8FD98D9B0385 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4E705D19DC0E84DCB74017D6083C8FD98D9B0385 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4E705D19DC0E84DCB74017D6083C8FD98D9B0385_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_3_tBD8132E09E03DDDFAAFA827439082F6743D6C0AA * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_3_tBD8132E09E03DDDFAAFA827439082F6743D6C0AA ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_3_tBD8132E09E03DDDFAAFA827439082F6743D6C0AA * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4E705D19DC0E84DCB74017D6083C8FD98D9B0385_H
#ifndef U3CU3EC_T9962754B7C58EB75F881DB6A6D958A857648D0A3_H
#define U3CU3EC_T9962754B7C58EB75F881DB6A6D958A857648D0A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Linq.ComponentModel.XElementElementsPropertyDescriptor/<>c
struct  U3CU3Ec_t9962754B7C58EB75F881DB6A6D958A857648D0A3  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t9962754B7C58EB75F881DB6A6D958A857648D0A3_StaticFields
{
public:
	// MS.Internal.Xml.Linq.ComponentModel.XElementElementsPropertyDescriptor/<>c MS.Internal.Xml.Linq.ComponentModel.XElementElementsPropertyDescriptor/<>c::<>9
	U3CU3Ec_t9962754B7C58EB75F881DB6A6D958A857648D0A3 * ___U3CU3E9_0;
	// System.Func`3<System.Xml.Linq.XElement,System.Xml.Linq.XName,System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement>> MS.Internal.Xml.Linq.ComponentModel.XElementElementsPropertyDescriptor/<>c::<>9__3_0
	Func_3_t036A925D26033259BAD0288212362AD3B832B326 * ___U3CU3E9__3_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9962754B7C58EB75F881DB6A6D958A857648D0A3_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t9962754B7C58EB75F881DB6A6D958A857648D0A3 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t9962754B7C58EB75F881DB6A6D958A857648D0A3 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t9962754B7C58EB75F881DB6A6D958A857648D0A3 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9962754B7C58EB75F881DB6A6D958A857648D0A3_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_3_t036A925D26033259BAD0288212362AD3B832B326 * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_3_t036A925D26033259BAD0288212362AD3B832B326 ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_3_t036A925D26033259BAD0288212362AD3B832B326 * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T9962754B7C58EB75F881DB6A6D958A857648D0A3_H
#ifndef MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#define MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MemberDescriptor
struct  MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8  : public RuntimeObject
{
public:
	// System.String System.ComponentModel.MemberDescriptor::name
	String_t* ___name_0;
	// System.String System.ComponentModel.MemberDescriptor::displayName
	String_t* ___displayName_1;
	// System.Int32 System.ComponentModel.MemberDescriptor::nameHash
	int32_t ___nameHash_2;
	// System.ComponentModel.AttributeCollection System.ComponentModel.MemberDescriptor::attributeCollection
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * ___attributeCollection_3;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::attributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___attributes_4;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::originalAttributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___originalAttributes_5;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFiltered
	bool ___attributesFiltered_6;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFilled
	bool ___attributesFilled_7;
	// System.Int32 System.ComponentModel.MemberDescriptor::metadataVersion
	int32_t ___metadataVersion_8;
	// System.String System.ComponentModel.MemberDescriptor::category
	String_t* ___category_9;
	// System.String System.ComponentModel.MemberDescriptor::description
	String_t* ___description_10;
	// System.Object System.ComponentModel.MemberDescriptor::lockCookie
	RuntimeObject * ___lockCookie_11;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_displayName_1() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___displayName_1)); }
	inline String_t* get_displayName_1() const { return ___displayName_1; }
	inline String_t** get_address_of_displayName_1() { return &___displayName_1; }
	inline void set_displayName_1(String_t* value)
	{
		___displayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_1), value);
	}

	inline static int32_t get_offset_of_nameHash_2() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___nameHash_2)); }
	inline int32_t get_nameHash_2() const { return ___nameHash_2; }
	inline int32_t* get_address_of_nameHash_2() { return &___nameHash_2; }
	inline void set_nameHash_2(int32_t value)
	{
		___nameHash_2 = value;
	}

	inline static int32_t get_offset_of_attributeCollection_3() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributeCollection_3)); }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * get_attributeCollection_3() const { return ___attributeCollection_3; }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE ** get_address_of_attributeCollection_3() { return &___attributeCollection_3; }
	inline void set_attributeCollection_3(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * value)
	{
		___attributeCollection_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributeCollection_3), value);
	}

	inline static int32_t get_offset_of_attributes_4() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributes_4)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_attributes_4() const { return ___attributes_4; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_attributes_4() { return &___attributes_4; }
	inline void set_attributes_4(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___attributes_4 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_4), value);
	}

	inline static int32_t get_offset_of_originalAttributes_5() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___originalAttributes_5)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_originalAttributes_5() const { return ___originalAttributes_5; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_originalAttributes_5() { return &___originalAttributes_5; }
	inline void set_originalAttributes_5(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___originalAttributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___originalAttributes_5), value);
	}

	inline static int32_t get_offset_of_attributesFiltered_6() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFiltered_6)); }
	inline bool get_attributesFiltered_6() const { return ___attributesFiltered_6; }
	inline bool* get_address_of_attributesFiltered_6() { return &___attributesFiltered_6; }
	inline void set_attributesFiltered_6(bool value)
	{
		___attributesFiltered_6 = value;
	}

	inline static int32_t get_offset_of_attributesFilled_7() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFilled_7)); }
	inline bool get_attributesFilled_7() const { return ___attributesFilled_7; }
	inline bool* get_address_of_attributesFilled_7() { return &___attributesFilled_7; }
	inline void set_attributesFilled_7(bool value)
	{
		___attributesFilled_7 = value;
	}

	inline static int32_t get_offset_of_metadataVersion_8() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___metadataVersion_8)); }
	inline int32_t get_metadataVersion_8() const { return ___metadataVersion_8; }
	inline int32_t* get_address_of_metadataVersion_8() { return &___metadataVersion_8; }
	inline void set_metadataVersion_8(int32_t value)
	{
		___metadataVersion_8 = value;
	}

	inline static int32_t get_offset_of_category_9() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___category_9)); }
	inline String_t* get_category_9() const { return ___category_9; }
	inline String_t** get_address_of_category_9() { return &___category_9; }
	inline void set_category_9(String_t* value)
	{
		___category_9 = value;
		Il2CppCodeGenWriteBarrier((&___category_9), value);
	}

	inline static int32_t get_offset_of_description_10() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___description_10)); }
	inline String_t* get_description_10() const { return ___description_10; }
	inline String_t** get_address_of_description_10() { return &___description_10; }
	inline void set_description_10(String_t* value)
	{
		___description_10 = value;
		Il2CppCodeGenWriteBarrier((&___description_10), value);
	}

	inline static int32_t get_offset_of_lockCookie_11() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___lockCookie_11)); }
	inline RuntimeObject * get_lockCookie_11() const { return ___lockCookie_11; }
	inline RuntimeObject ** get_address_of_lockCookie_11() { return &___lockCookie_11; }
	inline void set_lockCookie_11(RuntimeObject * value)
	{
		___lockCookie_11 = value;
		Il2CppCodeGenWriteBarrier((&___lockCookie_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef NAMESPACEDECLARATION_T8CC10C4791174893FA9467DE2F8E41D1097236DF_H
#define NAMESPACEDECLARATION_T8CC10C4791174893FA9467DE2F8E41D1097236DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.NamespaceResolver/NamespaceDeclaration
struct  NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF  : public RuntimeObject
{
public:
	// System.String System.Xml.Linq.NamespaceResolver/NamespaceDeclaration::prefix
	String_t* ___prefix_0;
	// System.Xml.Linq.XNamespace System.Xml.Linq.NamespaceResolver/NamespaceDeclaration::ns
	XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D * ___ns_1;
	// System.Int32 System.Xml.Linq.NamespaceResolver/NamespaceDeclaration::scope
	int32_t ___scope_2;
	// System.Xml.Linq.NamespaceResolver/NamespaceDeclaration System.Xml.Linq.NamespaceResolver/NamespaceDeclaration::prev
	NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF * ___prev_3;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF, ___ns_1)); }
	inline XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D * get_ns_1() const { return ___ns_1; }
	inline XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D ** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D * value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier((&___ns_1), value);
	}

	inline static int32_t get_offset_of_scope_2() { return static_cast<int32_t>(offsetof(NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF, ___scope_2)); }
	inline int32_t get_scope_2() const { return ___scope_2; }
	inline int32_t* get_address_of_scope_2() { return &___scope_2; }
	inline void set_scope_2(int32_t value)
	{
		___scope_2 = value;
	}

	inline static int32_t get_offset_of_prev_3() { return static_cast<int32_t>(offsetof(NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF, ___prev_3)); }
	inline NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF * get_prev_3() const { return ___prev_3; }
	inline NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF ** get_address_of_prev_3() { return &___prev_3; }
	inline void set_prev_3(NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF * value)
	{
		___prev_3 = value;
		Il2CppCodeGenWriteBarrier((&___prev_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEDECLARATION_T8CC10C4791174893FA9467DE2F8E41D1097236DF_H
#ifndef RES_TD7FC17E62C176207785D3F44C92954026A90B16B_H
#define RES_TD7FC17E62C176207785D3F44C92954026A90B16B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.Res
struct  Res_tD7FC17E62C176207785D3F44C92954026A90B16B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RES_TD7FC17E62C176207785D3F44C92954026A90B16B_H
#ifndef U3CGETDESCENDANTSU3ED__39_TA691FA9408A12703768386BC37C6C9B973E8EF59_H
#define U3CGETDESCENDANTSU3ED__39_TA691FA9408A12703768386BC37C6C9B973E8EF59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XContainer/<GetDescendants>d__39
struct  U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Linq.XContainer/<GetDescendants>d__39::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Xml.Linq.XElement System.Xml.Linq.XContainer/<GetDescendants>d__39::<>2__current
	XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * ___U3CU3E2__current_1;
	// System.Int32 System.Xml.Linq.XContainer/<GetDescendants>d__39::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Boolean System.Xml.Linq.XContainer/<GetDescendants>d__39::self
	bool ___self_3;
	// System.Boolean System.Xml.Linq.XContainer/<GetDescendants>d__39::<>3__self
	bool ___U3CU3E3__self_4;
	// System.Xml.Linq.XContainer System.Xml.Linq.XContainer/<GetDescendants>d__39::<>4__this
	XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 * ___U3CU3E4__this_5;
	// System.Xml.Linq.XName System.Xml.Linq.XContainer/<GetDescendants>d__39::name
	XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * ___name_6;
	// System.Xml.Linq.XName System.Xml.Linq.XContainer/<GetDescendants>d__39::<>3__name
	XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * ___U3CU3E3__name_7;
	// System.Xml.Linq.XNode System.Xml.Linq.XContainer/<GetDescendants>d__39::<n>5__1
	XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751 * ___U3CnU3E5__1_8;
	// System.Xml.Linq.XElement System.Xml.Linq.XContainer/<GetDescendants>d__39::<e>5__2
	XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * ___U3CeU3E5__2_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59, ___U3CU3E2__current_1)); }
	inline XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_self_3() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59, ___self_3)); }
	inline bool get_self_3() const { return ___self_3; }
	inline bool* get_address_of_self_3() { return &___self_3; }
	inline void set_self_3(bool value)
	{
		___self_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__self_4() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59, ___U3CU3E3__self_4)); }
	inline bool get_U3CU3E3__self_4() const { return ___U3CU3E3__self_4; }
	inline bool* get_address_of_U3CU3E3__self_4() { return &___U3CU3E3__self_4; }
	inline void set_U3CU3E3__self_4(bool value)
	{
		___U3CU3E3__self_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59, ___U3CU3E4__this_5)); }
	inline XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_name_6() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59, ___name_6)); }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * get_name_6() const { return ___name_6; }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A ** get_address_of_name_6() { return &___name_6; }
	inline void set_name_6(XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * value)
	{
		___name_6 = value;
		Il2CppCodeGenWriteBarrier((&___name_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__name_7() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59, ___U3CU3E3__name_7)); }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * get_U3CU3E3__name_7() const { return ___U3CU3E3__name_7; }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A ** get_address_of_U3CU3E3__name_7() { return &___U3CU3E3__name_7; }
	inline void set_U3CU3E3__name_7(XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * value)
	{
		___U3CU3E3__name_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__name_7), value);
	}

	inline static int32_t get_offset_of_U3CnU3E5__1_8() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59, ___U3CnU3E5__1_8)); }
	inline XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751 * get_U3CnU3E5__1_8() const { return ___U3CnU3E5__1_8; }
	inline XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751 ** get_address_of_U3CnU3E5__1_8() { return &___U3CnU3E5__1_8; }
	inline void set_U3CnU3E5__1_8(XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751 * value)
	{
		___U3CnU3E5__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnU3E5__1_8), value);
	}

	inline static int32_t get_offset_of_U3CeU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59, ___U3CeU3E5__2_9)); }
	inline XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * get_U3CeU3E5__2_9() const { return ___U3CeU3E5__2_9; }
	inline XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 ** get_address_of_U3CeU3E5__2_9() { return &___U3CeU3E5__2_9; }
	inline void set_U3CeU3E5__2_9(XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * value)
	{
		___U3CeU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CeU3E5__2_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETDESCENDANTSU3ED__39_TA691FA9408A12703768386BC37C6C9B973E8EF59_H
#ifndef U3CGETELEMENTSU3ED__40_T2C252163A201D4084C359A465846CBCDB6AE5927_H
#define U3CGETELEMENTSU3ED__40_T2C252163A201D4084C359A465846CBCDB6AE5927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XContainer/<GetElements>d__40
struct  U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Linq.XContainer/<GetElements>d__40::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Xml.Linq.XElement System.Xml.Linq.XContainer/<GetElements>d__40::<>2__current
	XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * ___U3CU3E2__current_1;
	// System.Int32 System.Xml.Linq.XContainer/<GetElements>d__40::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Xml.Linq.XContainer System.Xml.Linq.XContainer/<GetElements>d__40::<>4__this
	XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 * ___U3CU3E4__this_3;
	// System.Xml.Linq.XNode System.Xml.Linq.XContainer/<GetElements>d__40::<n>5__1
	XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751 * ___U3CnU3E5__1_4;
	// System.Xml.Linq.XName System.Xml.Linq.XContainer/<GetElements>d__40::name
	XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * ___name_5;
	// System.Xml.Linq.XName System.Xml.Linq.XContainer/<GetElements>d__40::<>3__name
	XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * ___U3CU3E3__name_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927, ___U3CU3E2__current_1)); }
	inline XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927, ___U3CU3E4__this_3)); }
	inline XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CnU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927, ___U3CnU3E5__1_4)); }
	inline XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751 * get_U3CnU3E5__1_4() const { return ___U3CnU3E5__1_4; }
	inline XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751 ** get_address_of_U3CnU3E5__1_4() { return &___U3CnU3E5__1_4; }
	inline void set_U3CnU3E5__1_4(XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751 * value)
	{
		___U3CnU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnU3E5__1_4), value);
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927, ___name_5)); }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * get_name_5() const { return ___name_5; }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A ** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__name_6() { return static_cast<int32_t>(offsetof(U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927, ___U3CU3E3__name_6)); }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * get_U3CU3E3__name_6() const { return ___U3CU3E3__name_6; }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A ** get_address_of_U3CU3E3__name_6() { return &___U3CU3E3__name_6; }
	inline void set_U3CU3E3__name_6(XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * value)
	{
		___U3CU3E3__name_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__name_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETELEMENTSU3ED__40_T2C252163A201D4084C359A465846CBCDB6AE5927_H
#ifndef XDECLARATION_TB17C002AEFF0D8802A09677E246F555FDD1F8512_H
#define XDECLARATION_TB17C002AEFF0D8802A09677E246F555FDD1F8512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XDeclaration
struct  XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512  : public RuntimeObject
{
public:
	// System.String System.Xml.Linq.XDeclaration::version
	String_t* ___version_0;
	// System.String System.Xml.Linq.XDeclaration::encoding
	String_t* ___encoding_1;
	// System.String System.Xml.Linq.XDeclaration::standalone
	String_t* ___standalone_2;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512, ___version_0)); }
	inline String_t* get_version_0() const { return ___version_0; }
	inline String_t** get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(String_t* value)
	{
		___version_0 = value;
		Il2CppCodeGenWriteBarrier((&___version_0), value);
	}

	inline static int32_t get_offset_of_encoding_1() { return static_cast<int32_t>(offsetof(XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512, ___encoding_1)); }
	inline String_t* get_encoding_1() const { return ___encoding_1; }
	inline String_t** get_address_of_encoding_1() { return &___encoding_1; }
	inline void set_encoding_1(String_t* value)
	{
		___encoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_1), value);
	}

	inline static int32_t get_offset_of_standalone_2() { return static_cast<int32_t>(offsetof(XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512, ___standalone_2)); }
	inline String_t* get_standalone_2() const { return ___standalone_2; }
	inline String_t** get_address_of_standalone_2() { return &___standalone_2; }
	inline void set_standalone_2(String_t* value)
	{
		___standalone_2 = value;
		Il2CppCodeGenWriteBarrier((&___standalone_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDECLARATION_TB17C002AEFF0D8802A09677E246F555FDD1F8512_H
#ifndef U3CGETATTRIBUTESU3ED__105_TC7FE5089390F02011E9B6F1AFD90DE16A5B78157_H
#define U3CGETATTRIBUTESU3ED__105_TC7FE5089390F02011E9B6F1AFD90DE16A5B78157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XElement/<GetAttributes>d__105
struct  U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Linq.XElement/<GetAttributes>d__105::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Xml.Linq.XAttribute System.Xml.Linq.XElement/<GetAttributes>d__105::<>2__current
	XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 * ___U3CU3E2__current_1;
	// System.Int32 System.Xml.Linq.XElement/<GetAttributes>d__105::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Xml.Linq.XElement System.Xml.Linq.XElement/<GetAttributes>d__105::<>4__this
	XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * ___U3CU3E4__this_3;
	// System.Xml.Linq.XAttribute System.Xml.Linq.XElement/<GetAttributes>d__105::<a>5__1
	XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 * ___U3CaU3E5__1_4;
	// System.Xml.Linq.XName System.Xml.Linq.XElement/<GetAttributes>d__105::name
	XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * ___name_5;
	// System.Xml.Linq.XName System.Xml.Linq.XElement/<GetAttributes>d__105::<>3__name
	XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * ___U3CU3E3__name_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157, ___U3CU3E2__current_1)); }
	inline XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157, ___U3CU3E4__this_3)); }
	inline XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CaU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157, ___U3CaU3E5__1_4)); }
	inline XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 * get_U3CaU3E5__1_4() const { return ___U3CaU3E5__1_4; }
	inline XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 ** get_address_of_U3CaU3E5__1_4() { return &___U3CaU3E5__1_4; }
	inline void set_U3CaU3E5__1_4(XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 * value)
	{
		___U3CaU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaU3E5__1_4), value);
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157, ___name_5)); }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * get_name_5() const { return ___name_5; }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A ** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__name_6() { return static_cast<int32_t>(offsetof(U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157, ___U3CU3E3__name_6)); }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * get_U3CU3E3__name_6() const { return ___U3CU3E3__name_6; }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A ** get_address_of_U3CU3E3__name_6() { return &___U3CU3E3__name_6; }
	inline void set_U3CU3E3__name_6(XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * value)
	{
		___U3CU3E3__name_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__name_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETATTRIBUTESU3ED__105_TC7FE5089390F02011E9B6F1AFD90DE16A5B78157_H
#ifndef XOBJECT_T0DA241208A0EC65CA16AEA485DD80F21A693EDEF_H
#define XOBJECT_T0DA241208A0EC65CA16AEA485DD80F21A693EDEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XObject
struct  XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF  : public RuntimeObject
{
public:
	// System.Xml.Linq.XContainer System.Xml.Linq.XObject::parent
	XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 * ___parent_0;
	// System.Object System.Xml.Linq.XObject::annotations
	RuntimeObject * ___annotations_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF, ___parent_0)); }
	inline XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 * get_parent_0() const { return ___parent_0; }
	inline XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_annotations_1() { return static_cast<int32_t>(offsetof(XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF, ___annotations_1)); }
	inline RuntimeObject * get_annotations_1() const { return ___annotations_1; }
	inline RuntimeObject ** get_address_of_annotations_1() { return &___annotations_1; }
	inline void set_annotations_1(RuntimeObject * value)
	{
		___annotations_1 = value;
		Il2CppCodeGenWriteBarrier((&___annotations_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XOBJECT_T0DA241208A0EC65CA16AEA485DD80F21A693EDEF_H
#ifndef XSTREAMINGELEMENT_T6B73236C8904BD25EBD29C8A18941012D8DA6D45_H
#define XSTREAMINGELEMENT_T6B73236C8904BD25EBD29C8A18941012D8DA6D45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XStreamingElement
struct  XStreamingElement_t6B73236C8904BD25EBD29C8A18941012D8DA6D45  : public RuntimeObject
{
public:
	// System.Xml.Linq.XName System.Xml.Linq.XStreamingElement::name
	XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * ___name_0;
	// System.Object System.Xml.Linq.XStreamingElement::content
	RuntimeObject * ___content_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(XStreamingElement_t6B73236C8904BD25EBD29C8A18941012D8DA6D45, ___name_0)); }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * get_name_0() const { return ___name_0; }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A ** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(XStreamingElement_t6B73236C8904BD25EBD29C8A18941012D8DA6D45, ___content_1)); }
	inline RuntimeObject * get_content_1() const { return ___content_1; }
	inline RuntimeObject ** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(RuntimeObject * value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSTREAMINGELEMENT_T6B73236C8904BD25EBD29C8A18941012D8DA6D45_H
#ifndef SUBSYSTEMREGISTRATION_T0A22FECC46483ABBFFC039449407F73FF11F5A1A_H
#define SUBSYSTEMREGISTRATION_T0A22FECC46483ABBFFC039449407F73FF11F5A1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SubsystemRegistration
struct  SubsystemRegistration_t0A22FECC46483ABBFFC039449407F73FF11F5A1A  : public RuntimeObject
{
public:

public:
};

struct SubsystemRegistration_t0A22FECC46483ABBFFC039449407F73FF11F5A1A_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Experimental.SubsystemDescriptor> UnityEngine.SubsystemRegistration::k_SubsystemDescriptors
	List_1_tDC5945E3E5C2531BBD805BC13C39514143200C98 * ___k_SubsystemDescriptors_0;

public:
	inline static int32_t get_offset_of_k_SubsystemDescriptors_0() { return static_cast<int32_t>(offsetof(SubsystemRegistration_t0A22FECC46483ABBFFC039449407F73FF11F5A1A_StaticFields, ___k_SubsystemDescriptors_0)); }
	inline List_1_tDC5945E3E5C2531BBD805BC13C39514143200C98 * get_k_SubsystemDescriptors_0() const { return ___k_SubsystemDescriptors_0; }
	inline List_1_tDC5945E3E5C2531BBD805BC13C39514143200C98 ** get_address_of_k_SubsystemDescriptors_0() { return &___k_SubsystemDescriptors_0; }
	inline void set_k_SubsystemDescriptors_0(List_1_tDC5945E3E5C2531BBD805BC13C39514143200C98 * value)
	{
		___k_SubsystemDescriptors_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_SubsystemDescriptors_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEMREGISTRATION_T0A22FECC46483ABBFFC039449407F73FF11F5A1A_H
#ifndef __STATICARRAYINITTYPESIZEU3D1024_TA2012153EE60F5C9CA38878077580795956DBCD1_H
#define __STATICARRAYINITTYPESIZEU3D1024_TA2012153EE60F5C9CA38878077580795956DBCD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=1024
struct  __StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1__padding[1024];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D1024_TA2012153EE60F5C9CA38878077580795956DBCD1_H
#ifndef __STATICARRAYINITTYPESIZEU3D116_TDF00214C4AC7272026E840470B881841013B7244_H
#define __STATICARRAYINITTYPESIZEU3D116_TDF00214C4AC7272026E840470B881841013B7244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=116
struct  __StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244__padding[116];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D116_TDF00214C4AC7272026E840470B881841013B7244_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1_H
#define __STATICARRAYINITTYPESIZEU3D12_T3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1_H
#ifndef __STATICARRAYINITTYPESIZEU3D120_T8DDD6C9514870A994B892ACC8E5E40366D37C155_H
#define __STATICARRAYINITTYPESIZEU3D120_T8DDD6C9514870A994B892ACC8E5E40366D37C155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=120
struct  __StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155__padding[120];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D120_T8DDD6C9514870A994B892ACC8E5E40366D37C155_H
#ifndef __STATICARRAYINITTYPESIZEU3D16_TA74794CC2DB67F8B4B9BCE862B22BC6D2497E777_H
#define __STATICARRAYINITTYPESIZEU3D16_TA74794CC2DB67F8B4B9BCE862B22BC6D2497E777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=16
struct  __StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777__padding[16];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D16_TA74794CC2DB67F8B4B9BCE862B22BC6D2497E777_H
#ifndef __STATICARRAYINITTYPESIZEU3D76_T06243B400D517E4BAA79958C008A9C655F63F936_H
#define __STATICARRAYINITTYPESIZEU3D76_T06243B400D517E4BAA79958C008A9C655F63F936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=76
struct  __StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936__padding[76];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D76_T06243B400D517E4BAA79958C008A9C655F63F936_H
#ifndef PKZIPCLASSICDECRYPTCRYPTOTRANSFORM_TBB3B4209A63C60CB64240640E0CBA21626ACA93E_H
#define PKZIPCLASSICDECRYPTCRYPTOTRANSFORM_TBB3B4209A63C60CB64240640E0CBA21626ACA93E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform
struct  PkzipClassicDecryptCryptoTransform_tBB3B4209A63C60CB64240640E0CBA21626ACA93E  : public PkzipClassicCryptoBase_t82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKZIPCLASSICDECRYPTCRYPTOTRANSFORM_TBB3B4209A63C60CB64240640E0CBA21626ACA93E_H
#ifndef PKZIPCLASSICENCRYPTCRYPTOTRANSFORM_T050C6231405F6F498F285DF8D3FCB2DCD6EB2EE0_H
#define PKZIPCLASSICENCRYPTCRYPTOTRANSFORM_T050C6231405F6F498F285DF8D3FCB2DCD6EB2EE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform
struct  PkzipClassicEncryptCryptoTransform_t050C6231405F6F498F285DF8D3FCB2DCD6EB2EE0  : public PkzipClassicCryptoBase_t82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKZIPCLASSICENCRYPTCRYPTOTRANSFORM_T050C6231405F6F498F285DF8D3FCB2DCD6EB2EE0_H
#ifndef KEYSREQUIREDEVENTARGS_T333068E27532BA6462A1C84D7D3CCE8904FC7148_H
#define KEYSREQUIREDEVENTARGS_T333068E27532BA6462A1C84D7D3CCE8904FC7148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs
struct  KeysRequiredEventArgs_t333068E27532BA6462A1C84D7D3CCE8904FC7148  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs::fileName
	String_t* ___fileName_1;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs::key
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key_2;

public:
	inline static int32_t get_offset_of_fileName_1() { return static_cast<int32_t>(offsetof(KeysRequiredEventArgs_t333068E27532BA6462A1C84D7D3CCE8904FC7148, ___fileName_1)); }
	inline String_t* get_fileName_1() const { return ___fileName_1; }
	inline String_t** get_address_of_fileName_1() { return &___fileName_1; }
	inline void set_fileName_1(String_t* value)
	{
		___fileName_1 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_1), value);
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(KeysRequiredEventArgs_t333068E27532BA6462A1C84D7D3CCE8904FC7148, ___key_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_key_2() const { return ___key_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYSREQUIREDEVENTARGS_T333068E27532BA6462A1C84D7D3CCE8904FC7148_H
#ifndef APPLICATIONEXCEPTION_T664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74_H
#define APPLICATIONEXCEPTION_T664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ApplicationException
struct  ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONEXCEPTION_T664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74_H
#ifndef PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#define PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyDescriptor
struct  PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D  : public MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8
{
public:
	// System.ComponentModel.TypeConverter System.ComponentModel.PropertyDescriptor::converter
	TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * ___converter_12;
	// System.Collections.Hashtable System.ComponentModel.PropertyDescriptor::valueChangedHandlers
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___valueChangedHandlers_13;
	// System.Object[] System.ComponentModel.PropertyDescriptor::editors
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___editors_14;
	// System.Type[] System.ComponentModel.PropertyDescriptor::editorTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___editorTypes_15;
	// System.Int32 System.ComponentModel.PropertyDescriptor::editorCount
	int32_t ___editorCount_16;

public:
	inline static int32_t get_offset_of_converter_12() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___converter_12)); }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * get_converter_12() const { return ___converter_12; }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB ** get_address_of_converter_12() { return &___converter_12; }
	inline void set_converter_12(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * value)
	{
		___converter_12 = value;
		Il2CppCodeGenWriteBarrier((&___converter_12), value);
	}

	inline static int32_t get_offset_of_valueChangedHandlers_13() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___valueChangedHandlers_13)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_valueChangedHandlers_13() const { return ___valueChangedHandlers_13; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_valueChangedHandlers_13() { return &___valueChangedHandlers_13; }
	inline void set_valueChangedHandlers_13(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___valueChangedHandlers_13 = value;
		Il2CppCodeGenWriteBarrier((&___valueChangedHandlers_13), value);
	}

	inline static int32_t get_offset_of_editors_14() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editors_14)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_editors_14() const { return ___editors_14; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_editors_14() { return &___editors_14; }
	inline void set_editors_14(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___editors_14 = value;
		Il2CppCodeGenWriteBarrier((&___editors_14), value);
	}

	inline static int32_t get_offset_of_editorTypes_15() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorTypes_15)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_editorTypes_15() const { return ___editorTypes_15; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_editorTypes_15() { return &___editorTypes_15; }
	inline void set_editorTypes_15(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___editorTypes_15 = value;
		Il2CppCodeGenWriteBarrier((&___editorTypes_15), value);
	}

	inline static int32_t get_offset_of_editorCount_16() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorCount_16)); }
	inline int32_t get_editorCount_16() const { return ___editorCount_16; }
	inline int32_t* get_address_of_editorCount_16() { return &___editorCount_16; }
	inline void set_editorCount_16(int32_t value)
	{
		___editorCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_2), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_3), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef NAMESPACECACHE_T0647B5D31D12BE681FD6B177432FE09BD092AD70_H
#define NAMESPACECACHE_T0647B5D31D12BE681FD6B177432FE09BD092AD70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.NamespaceCache
struct  NamespaceCache_t0647B5D31D12BE681FD6B177432FE09BD092AD70 
{
public:
	// System.Xml.Linq.XNamespace System.Xml.Linq.NamespaceCache::ns
	XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D * ___ns_0;
	// System.String System.Xml.Linq.NamespaceCache::namespaceName
	String_t* ___namespaceName_1;

public:
	inline static int32_t get_offset_of_ns_0() { return static_cast<int32_t>(offsetof(NamespaceCache_t0647B5D31D12BE681FD6B177432FE09BD092AD70, ___ns_0)); }
	inline XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D * get_ns_0() const { return ___ns_0; }
	inline XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D ** get_address_of_ns_0() { return &___ns_0; }
	inline void set_ns_0(XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D * value)
	{
		___ns_0 = value;
		Il2CppCodeGenWriteBarrier((&___ns_0), value);
	}

	inline static int32_t get_offset_of_namespaceName_1() { return static_cast<int32_t>(offsetof(NamespaceCache_t0647B5D31D12BE681FD6B177432FE09BD092AD70, ___namespaceName_1)); }
	inline String_t* get_namespaceName_1() const { return ___namespaceName_1; }
	inline String_t** get_address_of_namespaceName_1() { return &___namespaceName_1; }
	inline void set_namespaceName_1(String_t* value)
	{
		___namespaceName_1 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Linq.NamespaceCache
struct NamespaceCache_t0647B5D31D12BE681FD6B177432FE09BD092AD70_marshaled_pinvoke
{
	XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D * ___ns_0;
	char* ___namespaceName_1;
};
// Native definition for COM marshalling of System.Xml.Linq.NamespaceCache
struct NamespaceCache_t0647B5D31D12BE681FD6B177432FE09BD092AD70_marshaled_com
{
	XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D * ___ns_0;
	Il2CppChar* ___namespaceName_1;
};
#endif // NAMESPACECACHE_T0647B5D31D12BE681FD6B177432FE09BD092AD70_H
#ifndef NAMESPACERESOLVER_T51606A2FFD4C06A168414A26FBD1541856EA049D_H
#define NAMESPACERESOLVER_T51606A2FFD4C06A168414A26FBD1541856EA049D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.NamespaceResolver
struct  NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D 
{
public:
	// System.Int32 System.Xml.Linq.NamespaceResolver::scope
	int32_t ___scope_0;
	// System.Xml.Linq.NamespaceResolver/NamespaceDeclaration System.Xml.Linq.NamespaceResolver::declaration
	NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF * ___declaration_1;
	// System.Xml.Linq.NamespaceResolver/NamespaceDeclaration System.Xml.Linq.NamespaceResolver::rover
	NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF * ___rover_2;

public:
	inline static int32_t get_offset_of_scope_0() { return static_cast<int32_t>(offsetof(NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D, ___scope_0)); }
	inline int32_t get_scope_0() const { return ___scope_0; }
	inline int32_t* get_address_of_scope_0() { return &___scope_0; }
	inline void set_scope_0(int32_t value)
	{
		___scope_0 = value;
	}

	inline static int32_t get_offset_of_declaration_1() { return static_cast<int32_t>(offsetof(NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D, ___declaration_1)); }
	inline NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF * get_declaration_1() const { return ___declaration_1; }
	inline NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF ** get_address_of_declaration_1() { return &___declaration_1; }
	inline void set_declaration_1(NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF * value)
	{
		___declaration_1 = value;
		Il2CppCodeGenWriteBarrier((&___declaration_1), value);
	}

	inline static int32_t get_offset_of_rover_2() { return static_cast<int32_t>(offsetof(NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D, ___rover_2)); }
	inline NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF * get_rover_2() const { return ___rover_2; }
	inline NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF ** get_address_of_rover_2() { return &___rover_2; }
	inline void set_rover_2(NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF * value)
	{
		___rover_2 = value;
		Il2CppCodeGenWriteBarrier((&___rover_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Linq.NamespaceResolver
struct NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D_marshaled_pinvoke
{
	int32_t ___scope_0;
	NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF * ___declaration_1;
	NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF * ___rover_2;
};
// Native definition for COM marshalling of System.Xml.Linq.NamespaceResolver
struct NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D_marshaled_com
{
	int32_t ___scope_0;
	NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF * ___declaration_1;
	NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF * ___rover_2;
};
#endif // NAMESPACERESOLVER_T51606A2FFD4C06A168414A26FBD1541856EA049D_H
#ifndef XATTRIBUTE_TE59FB05CB3017A75CD81C84F5E4FE613B8A8F015_H
#define XATTRIBUTE_TE59FB05CB3017A75CD81C84F5E4FE613B8A8F015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XAttribute
struct  XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015  : public XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF
{
public:
	// System.Xml.Linq.XAttribute System.Xml.Linq.XAttribute::next
	XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 * ___next_2;
	// System.Xml.Linq.XName System.Xml.Linq.XAttribute::name
	XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * ___name_3;
	// System.String System.Xml.Linq.XAttribute::value
	String_t* ___value_4;

public:
	inline static int32_t get_offset_of_next_2() { return static_cast<int32_t>(offsetof(XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015, ___next_2)); }
	inline XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 * get_next_2() const { return ___next_2; }
	inline XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 ** get_address_of_next_2() { return &___next_2; }
	inline void set_next_2(XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 * value)
	{
		___next_2 = value;
		Il2CppCodeGenWriteBarrier((&___next_2), value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015, ___name_3)); }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * get_name_3() const { return ___name_3; }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A ** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015, ___value_4)); }
	inline String_t* get_value_4() const { return ___value_4; }
	inline String_t** get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(String_t* value)
	{
		___value_4 = value;
		Il2CppCodeGenWriteBarrier((&___value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XATTRIBUTE_TE59FB05CB3017A75CD81C84F5E4FE613B8A8F015_H
#ifndef XNODE_TC1E0A039E17CD7048FD925F35FB0413D3D292751_H
#define XNODE_TC1E0A039E17CD7048FD925F35FB0413D3D292751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XNode
struct  XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751  : public XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF
{
public:
	// System.Xml.Linq.XNode System.Xml.Linq.XNode::next
	XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751 * ___next_2;

public:
	inline static int32_t get_offset_of_next_2() { return static_cast<int32_t>(offsetof(XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751, ___next_2)); }
	inline XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751 * get_next_2() const { return ___next_2; }
	inline XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751 ** get_address_of_next_2() { return &___next_2; }
	inline void set_next_2(XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751 * value)
	{
		___next_2 = value;
		Il2CppCodeGenWriteBarrier((&___next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XNODE_TC1E0A039E17CD7048FD925F35FB0413D3D292751_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_TF556D54546BC2FC0638B99AF7BAF448A985AB3EC_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_TF556D54546BC2FC0638B99AF7BAF448A985AB3EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}
struct  U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields
{
public:
	// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=1024 <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}::$$method0x60004f9-1
	__StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1  ___U24U24method0x60004f9U2D1_0;
	// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}::$$method0x60000dd-1
	__StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1  ___U24U24method0x60000ddU2D1_1;
	// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=76 <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}::$$method0x600029d-1
	__StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936  ___U24U24method0x600029dU2D1_2;
	// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}::$$method0x600029d-2
	__StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777  ___U24U24method0x600029dU2D2_3;
	// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=116 <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}::$$method0x60004fd-1
	__StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244  ___U24U24method0x60004fdU2D1_4;
	// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=116 <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}::$$method0x60004fd-2
	__StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244  ___U24U24method0x60004fdU2D2_5;
	// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}::$$method0x60004fd-3
	__StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155  ___U24U24method0x60004fdU2D3_6;
	// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}::$$method0x60004fd-4
	__StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155  ___U24U24method0x60004fdU2D4_7;
	// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}::$$method0x60004fe-1
	__StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1  ___U24U24method0x60004feU2D1_8;
	// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}::$$method0x60004fe-2
	__StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1  ___U24U24method0x60004feU2D2_9;
	// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=76 <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}::$$method0x60004fe-3
	__StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936  ___U24U24method0x60004feU2D3_10;

public:
	inline static int32_t get_offset_of_U24U24method0x60004f9U2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004f9U2D1_0)); }
	inline __StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1  get_U24U24method0x60004f9U2D1_0() const { return ___U24U24method0x60004f9U2D1_0; }
	inline __StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1 * get_address_of_U24U24method0x60004f9U2D1_0() { return &___U24U24method0x60004f9U2D1_0; }
	inline void set_U24U24method0x60004f9U2D1_0(__StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1  value)
	{
		___U24U24method0x60004f9U2D1_0 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60000ddU2D1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60000ddU2D1_1)); }
	inline __StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1  get_U24U24method0x60000ddU2D1_1() const { return ___U24U24method0x60000ddU2D1_1; }
	inline __StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1 * get_address_of_U24U24method0x60000ddU2D1_1() { return &___U24U24method0x60000ddU2D1_1; }
	inline void set_U24U24method0x60000ddU2D1_1(__StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1  value)
	{
		___U24U24method0x60000ddU2D1_1 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x600029dU2D1_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x600029dU2D1_2)); }
	inline __StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936  get_U24U24method0x600029dU2D1_2() const { return ___U24U24method0x600029dU2D1_2; }
	inline __StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936 * get_address_of_U24U24method0x600029dU2D1_2() { return &___U24U24method0x600029dU2D1_2; }
	inline void set_U24U24method0x600029dU2D1_2(__StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936  value)
	{
		___U24U24method0x600029dU2D1_2 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x600029dU2D2_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x600029dU2D2_3)); }
	inline __StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777  get_U24U24method0x600029dU2D2_3() const { return ___U24U24method0x600029dU2D2_3; }
	inline __StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777 * get_address_of_U24U24method0x600029dU2D2_3() { return &___U24U24method0x600029dU2D2_3; }
	inline void set_U24U24method0x600029dU2D2_3(__StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777  value)
	{
		___U24U24method0x600029dU2D2_3 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60004fdU2D1_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004fdU2D1_4)); }
	inline __StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244  get_U24U24method0x60004fdU2D1_4() const { return ___U24U24method0x60004fdU2D1_4; }
	inline __StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244 * get_address_of_U24U24method0x60004fdU2D1_4() { return &___U24U24method0x60004fdU2D1_4; }
	inline void set_U24U24method0x60004fdU2D1_4(__StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244  value)
	{
		___U24U24method0x60004fdU2D1_4 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60004fdU2D2_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004fdU2D2_5)); }
	inline __StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244  get_U24U24method0x60004fdU2D2_5() const { return ___U24U24method0x60004fdU2D2_5; }
	inline __StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244 * get_address_of_U24U24method0x60004fdU2D2_5() { return &___U24U24method0x60004fdU2D2_5; }
	inline void set_U24U24method0x60004fdU2D2_5(__StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244  value)
	{
		___U24U24method0x60004fdU2D2_5 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60004fdU2D3_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004fdU2D3_6)); }
	inline __StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155  get_U24U24method0x60004fdU2D3_6() const { return ___U24U24method0x60004fdU2D3_6; }
	inline __StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155 * get_address_of_U24U24method0x60004fdU2D3_6() { return &___U24U24method0x60004fdU2D3_6; }
	inline void set_U24U24method0x60004fdU2D3_6(__StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155  value)
	{
		___U24U24method0x60004fdU2D3_6 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60004fdU2D4_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004fdU2D4_7)); }
	inline __StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155  get_U24U24method0x60004fdU2D4_7() const { return ___U24U24method0x60004fdU2D4_7; }
	inline __StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155 * get_address_of_U24U24method0x60004fdU2D4_7() { return &___U24U24method0x60004fdU2D4_7; }
	inline void set_U24U24method0x60004fdU2D4_7(__StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155  value)
	{
		___U24U24method0x60004fdU2D4_7 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60004feU2D1_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004feU2D1_8)); }
	inline __StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1  get_U24U24method0x60004feU2D1_8() const { return ___U24U24method0x60004feU2D1_8; }
	inline __StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1 * get_address_of_U24U24method0x60004feU2D1_8() { return &___U24U24method0x60004feU2D1_8; }
	inline void set_U24U24method0x60004feU2D1_8(__StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1  value)
	{
		___U24U24method0x60004feU2D1_8 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60004feU2D2_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004feU2D2_9)); }
	inline __StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1  get_U24U24method0x60004feU2D2_9() const { return ___U24U24method0x60004feU2D2_9; }
	inline __StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1 * get_address_of_U24U24method0x60004feU2D2_9() { return &___U24U24method0x60004feU2D2_9; }
	inline void set_U24U24method0x60004feU2D2_9(__StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1  value)
	{
		___U24U24method0x60004feU2D2_9 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60004feU2D3_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004feU2D3_10)); }
	inline __StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936  get_U24U24method0x60004feU2D3_10() const { return ___U24U24method0x60004feU2D3_10; }
	inline __StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936 * get_address_of_U24U24method0x60004feU2D3_10() { return &___U24U24method0x60004feU2D3_10; }
	inline void set_U24U24method0x60004feU2D3_10(__StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936  value)
	{
		___U24U24method0x60004feU2D3_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_TF556D54546BC2FC0638B99AF7BAF448A985AB3EC_H
#ifndef SHARPZIPBASEEXCEPTION_T822541D5F0EC9FCA3550F00DCF0883E987E5045F_H
#define SHARPZIPBASEEXCEPTION_T822541D5F0EC9FCA3550F00DCF0883E987E5045F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.SharpZipBaseException
struct  SharpZipBaseException_t822541D5F0EC9FCA3550F00DCF0883E987E5045F  : public ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARPZIPBASEEXCEPTION_T822541D5F0EC9FCA3550F00DCF0883E987E5045F_H
#ifndef TARHEADER_TE84CDB48A8D910B6485D7391D6E49A778332AA74_H
#define TARHEADER_TE84CDB48A8D910B6485D7391D6E49A778332AA74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.TarHeader
struct  TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74  : public RuntimeObject
{
public:
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::name
	String_t* ___name_1;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::mode
	int32_t ___mode_2;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::userId
	int32_t ___userId_3;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::groupId
	int32_t ___groupId_4;
	// System.Int64 ICSharpCode.SharpZipLib.Tar.TarHeader::size
	int64_t ___size_5;
	// System.DateTime ICSharpCode.SharpZipLib.Tar.TarHeader::modTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___modTime_6;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::checksum
	int32_t ___checksum_7;
	// System.Boolean ICSharpCode.SharpZipLib.Tar.TarHeader::isChecksumValid
	bool ___isChecksumValid_8;
	// System.Byte ICSharpCode.SharpZipLib.Tar.TarHeader::typeFlag
	uint8_t ___typeFlag_9;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::linkName
	String_t* ___linkName_10;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::magic
	String_t* ___magic_11;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::version
	String_t* ___version_12;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::userName
	String_t* ___userName_13;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::groupName
	String_t* ___groupName_14;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::devMajor
	int32_t ___devMajor_15;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::devMinor
	int32_t ___devMinor_16;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_mode_2() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___mode_2)); }
	inline int32_t get_mode_2() const { return ___mode_2; }
	inline int32_t* get_address_of_mode_2() { return &___mode_2; }
	inline void set_mode_2(int32_t value)
	{
		___mode_2 = value;
	}

	inline static int32_t get_offset_of_userId_3() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___userId_3)); }
	inline int32_t get_userId_3() const { return ___userId_3; }
	inline int32_t* get_address_of_userId_3() { return &___userId_3; }
	inline void set_userId_3(int32_t value)
	{
		___userId_3 = value;
	}

	inline static int32_t get_offset_of_groupId_4() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___groupId_4)); }
	inline int32_t get_groupId_4() const { return ___groupId_4; }
	inline int32_t* get_address_of_groupId_4() { return &___groupId_4; }
	inline void set_groupId_4(int32_t value)
	{
		___groupId_4 = value;
	}

	inline static int32_t get_offset_of_size_5() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___size_5)); }
	inline int64_t get_size_5() const { return ___size_5; }
	inline int64_t* get_address_of_size_5() { return &___size_5; }
	inline void set_size_5(int64_t value)
	{
		___size_5 = value;
	}

	inline static int32_t get_offset_of_modTime_6() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___modTime_6)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_modTime_6() const { return ___modTime_6; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_modTime_6() { return &___modTime_6; }
	inline void set_modTime_6(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___modTime_6 = value;
	}

	inline static int32_t get_offset_of_checksum_7() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___checksum_7)); }
	inline int32_t get_checksum_7() const { return ___checksum_7; }
	inline int32_t* get_address_of_checksum_7() { return &___checksum_7; }
	inline void set_checksum_7(int32_t value)
	{
		___checksum_7 = value;
	}

	inline static int32_t get_offset_of_isChecksumValid_8() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___isChecksumValid_8)); }
	inline bool get_isChecksumValid_8() const { return ___isChecksumValid_8; }
	inline bool* get_address_of_isChecksumValid_8() { return &___isChecksumValid_8; }
	inline void set_isChecksumValid_8(bool value)
	{
		___isChecksumValid_8 = value;
	}

	inline static int32_t get_offset_of_typeFlag_9() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___typeFlag_9)); }
	inline uint8_t get_typeFlag_9() const { return ___typeFlag_9; }
	inline uint8_t* get_address_of_typeFlag_9() { return &___typeFlag_9; }
	inline void set_typeFlag_9(uint8_t value)
	{
		___typeFlag_9 = value;
	}

	inline static int32_t get_offset_of_linkName_10() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___linkName_10)); }
	inline String_t* get_linkName_10() const { return ___linkName_10; }
	inline String_t** get_address_of_linkName_10() { return &___linkName_10; }
	inline void set_linkName_10(String_t* value)
	{
		___linkName_10 = value;
		Il2CppCodeGenWriteBarrier((&___linkName_10), value);
	}

	inline static int32_t get_offset_of_magic_11() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___magic_11)); }
	inline String_t* get_magic_11() const { return ___magic_11; }
	inline String_t** get_address_of_magic_11() { return &___magic_11; }
	inline void set_magic_11(String_t* value)
	{
		___magic_11 = value;
		Il2CppCodeGenWriteBarrier((&___magic_11), value);
	}

	inline static int32_t get_offset_of_version_12() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___version_12)); }
	inline String_t* get_version_12() const { return ___version_12; }
	inline String_t** get_address_of_version_12() { return &___version_12; }
	inline void set_version_12(String_t* value)
	{
		___version_12 = value;
		Il2CppCodeGenWriteBarrier((&___version_12), value);
	}

	inline static int32_t get_offset_of_userName_13() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___userName_13)); }
	inline String_t* get_userName_13() const { return ___userName_13; }
	inline String_t** get_address_of_userName_13() { return &___userName_13; }
	inline void set_userName_13(String_t* value)
	{
		___userName_13 = value;
		Il2CppCodeGenWriteBarrier((&___userName_13), value);
	}

	inline static int32_t get_offset_of_groupName_14() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___groupName_14)); }
	inline String_t* get_groupName_14() const { return ___groupName_14; }
	inline String_t** get_address_of_groupName_14() { return &___groupName_14; }
	inline void set_groupName_14(String_t* value)
	{
		___groupName_14 = value;
		Il2CppCodeGenWriteBarrier((&___groupName_14), value);
	}

	inline static int32_t get_offset_of_devMajor_15() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___devMajor_15)); }
	inline int32_t get_devMajor_15() const { return ___devMajor_15; }
	inline int32_t* get_address_of_devMajor_15() { return &___devMajor_15; }
	inline void set_devMajor_15(int32_t value)
	{
		___devMajor_15 = value;
	}

	inline static int32_t get_offset_of_devMinor_16() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74, ___devMinor_16)); }
	inline int32_t get_devMinor_16() const { return ___devMinor_16; }
	inline int32_t* get_address_of_devMinor_16() { return &___devMinor_16; }
	inline void set_devMinor_16(int32_t value)
	{
		___devMinor_16 = value;
	}
};

struct TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74_StaticFields
{
public:
	// System.DateTime ICSharpCode.SharpZipLib.Tar.TarHeader::dateTime1970
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___dateTime1970_0;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::groupNameAsSet
	String_t* ___groupNameAsSet_17;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::defaultUserId
	int32_t ___defaultUserId_18;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::defaultGroupId
	int32_t ___defaultGroupId_19;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::defaultGroupName
	String_t* ___defaultGroupName_20;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::defaultUser
	String_t* ___defaultUser_21;

public:
	inline static int32_t get_offset_of_dateTime1970_0() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74_StaticFields, ___dateTime1970_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_dateTime1970_0() const { return ___dateTime1970_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_dateTime1970_0() { return &___dateTime1970_0; }
	inline void set_dateTime1970_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___dateTime1970_0 = value;
	}

	inline static int32_t get_offset_of_groupNameAsSet_17() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74_StaticFields, ___groupNameAsSet_17)); }
	inline String_t* get_groupNameAsSet_17() const { return ___groupNameAsSet_17; }
	inline String_t** get_address_of_groupNameAsSet_17() { return &___groupNameAsSet_17; }
	inline void set_groupNameAsSet_17(String_t* value)
	{
		___groupNameAsSet_17 = value;
		Il2CppCodeGenWriteBarrier((&___groupNameAsSet_17), value);
	}

	inline static int32_t get_offset_of_defaultUserId_18() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74_StaticFields, ___defaultUserId_18)); }
	inline int32_t get_defaultUserId_18() const { return ___defaultUserId_18; }
	inline int32_t* get_address_of_defaultUserId_18() { return &___defaultUserId_18; }
	inline void set_defaultUserId_18(int32_t value)
	{
		___defaultUserId_18 = value;
	}

	inline static int32_t get_offset_of_defaultGroupId_19() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74_StaticFields, ___defaultGroupId_19)); }
	inline int32_t get_defaultGroupId_19() const { return ___defaultGroupId_19; }
	inline int32_t* get_address_of_defaultGroupId_19() { return &___defaultGroupId_19; }
	inline void set_defaultGroupId_19(int32_t value)
	{
		___defaultGroupId_19 = value;
	}

	inline static int32_t get_offset_of_defaultGroupName_20() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74_StaticFields, ___defaultGroupName_20)); }
	inline String_t* get_defaultGroupName_20() const { return ___defaultGroupName_20; }
	inline String_t** get_address_of_defaultGroupName_20() { return &___defaultGroupName_20; }
	inline void set_defaultGroupName_20(String_t* value)
	{
		___defaultGroupName_20 = value;
		Il2CppCodeGenWriteBarrier((&___defaultGroupName_20), value);
	}

	inline static int32_t get_offset_of_defaultUser_21() { return static_cast<int32_t>(offsetof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74_StaticFields, ___defaultUser_21)); }
	inline String_t* get_defaultUser_21() const { return ___defaultUser_21; }
	inline String_t** get_address_of_defaultUser_21() { return &___defaultUser_21; }
	inline void set_defaultUser_21(String_t* value)
	{
		___defaultUser_21 = value;
		Il2CppCodeGenWriteBarrier((&___defaultUser_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARHEADER_TE84CDB48A8D910B6485D7391D6E49A778332AA74_H
#ifndef TARINPUTSTREAM_TB97EB07332E378AD3848EC09413E8425190CAC3E_H
#define TARINPUTSTREAM_TB97EB07332E378AD3848EC09413E8425190CAC3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.TarInputStream
struct  TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean ICSharpCode.SharpZipLib.Tar.TarInputStream::hasHitEOF
	bool ___hasHitEOF_4;
	// System.Int64 ICSharpCode.SharpZipLib.Tar.TarInputStream::entrySize
	int64_t ___entrySize_5;
	// System.Int64 ICSharpCode.SharpZipLib.Tar.TarInputStream::entryOffset
	int64_t ___entryOffset_6;
	// System.Byte[] ICSharpCode.SharpZipLib.Tar.TarInputStream::readBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___readBuffer_7;
	// ICSharpCode.SharpZipLib.Tar.TarBuffer ICSharpCode.SharpZipLib.Tar.TarInputStream::tarBuffer
	TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C * ___tarBuffer_8;
	// ICSharpCode.SharpZipLib.Tar.TarEntry ICSharpCode.SharpZipLib.Tar.TarInputStream::currentEntry
	TarEntry_tDE18EDBB29A64DBD744028FA5BAF5026519398BE * ___currentEntry_9;
	// ICSharpCode.SharpZipLib.Tar.TarInputStream/IEntryFactory ICSharpCode.SharpZipLib.Tar.TarInputStream::entryFactory
	RuntimeObject* ___entryFactory_10;
	// System.IO.Stream ICSharpCode.SharpZipLib.Tar.TarInputStream::inputStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___inputStream_11;

public:
	inline static int32_t get_offset_of_hasHitEOF_4() { return static_cast<int32_t>(offsetof(TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E, ___hasHitEOF_4)); }
	inline bool get_hasHitEOF_4() const { return ___hasHitEOF_4; }
	inline bool* get_address_of_hasHitEOF_4() { return &___hasHitEOF_4; }
	inline void set_hasHitEOF_4(bool value)
	{
		___hasHitEOF_4 = value;
	}

	inline static int32_t get_offset_of_entrySize_5() { return static_cast<int32_t>(offsetof(TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E, ___entrySize_5)); }
	inline int64_t get_entrySize_5() const { return ___entrySize_5; }
	inline int64_t* get_address_of_entrySize_5() { return &___entrySize_5; }
	inline void set_entrySize_5(int64_t value)
	{
		___entrySize_5 = value;
	}

	inline static int32_t get_offset_of_entryOffset_6() { return static_cast<int32_t>(offsetof(TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E, ___entryOffset_6)); }
	inline int64_t get_entryOffset_6() const { return ___entryOffset_6; }
	inline int64_t* get_address_of_entryOffset_6() { return &___entryOffset_6; }
	inline void set_entryOffset_6(int64_t value)
	{
		___entryOffset_6 = value;
	}

	inline static int32_t get_offset_of_readBuffer_7() { return static_cast<int32_t>(offsetof(TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E, ___readBuffer_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_readBuffer_7() const { return ___readBuffer_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_readBuffer_7() { return &___readBuffer_7; }
	inline void set_readBuffer_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___readBuffer_7 = value;
		Il2CppCodeGenWriteBarrier((&___readBuffer_7), value);
	}

	inline static int32_t get_offset_of_tarBuffer_8() { return static_cast<int32_t>(offsetof(TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E, ___tarBuffer_8)); }
	inline TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C * get_tarBuffer_8() const { return ___tarBuffer_8; }
	inline TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C ** get_address_of_tarBuffer_8() { return &___tarBuffer_8; }
	inline void set_tarBuffer_8(TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C * value)
	{
		___tarBuffer_8 = value;
		Il2CppCodeGenWriteBarrier((&___tarBuffer_8), value);
	}

	inline static int32_t get_offset_of_currentEntry_9() { return static_cast<int32_t>(offsetof(TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E, ___currentEntry_9)); }
	inline TarEntry_tDE18EDBB29A64DBD744028FA5BAF5026519398BE * get_currentEntry_9() const { return ___currentEntry_9; }
	inline TarEntry_tDE18EDBB29A64DBD744028FA5BAF5026519398BE ** get_address_of_currentEntry_9() { return &___currentEntry_9; }
	inline void set_currentEntry_9(TarEntry_tDE18EDBB29A64DBD744028FA5BAF5026519398BE * value)
	{
		___currentEntry_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentEntry_9), value);
	}

	inline static int32_t get_offset_of_entryFactory_10() { return static_cast<int32_t>(offsetof(TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E, ___entryFactory_10)); }
	inline RuntimeObject* get_entryFactory_10() const { return ___entryFactory_10; }
	inline RuntimeObject** get_address_of_entryFactory_10() { return &___entryFactory_10; }
	inline void set_entryFactory_10(RuntimeObject* value)
	{
		___entryFactory_10 = value;
		Il2CppCodeGenWriteBarrier((&___entryFactory_10), value);
	}

	inline static int32_t get_offset_of_inputStream_11() { return static_cast<int32_t>(offsetof(TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E, ___inputStream_11)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_inputStream_11() const { return ___inputStream_11; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_inputStream_11() { return &___inputStream_11; }
	inline void set_inputStream_11(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___inputStream_11 = value;
		Il2CppCodeGenWriteBarrier((&___inputStream_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARINPUTSTREAM_TB97EB07332E378AD3848EC09413E8425190CAC3E_H
#ifndef TAROUTPUTSTREAM_T5327E699157D91D4FD74D62ACCD54C2C539C8838_H
#define TAROUTPUTSTREAM_T5327E699157D91D4FD74D62ACCD54C2C539C8838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.TarOutputStream
struct  TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Int64 ICSharpCode.SharpZipLib.Tar.TarOutputStream::currBytes
	int64_t ___currBytes_4;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarOutputStream::assemblyBufferLength
	int32_t ___assemblyBufferLength_5;
	// System.Boolean ICSharpCode.SharpZipLib.Tar.TarOutputStream::isClosed
	bool ___isClosed_6;
	// System.Int64 ICSharpCode.SharpZipLib.Tar.TarOutputStream::currSize
	int64_t ___currSize_7;
	// System.Byte[] ICSharpCode.SharpZipLib.Tar.TarOutputStream::blockBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___blockBuffer_8;
	// System.Byte[] ICSharpCode.SharpZipLib.Tar.TarOutputStream::assemblyBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___assemblyBuffer_9;
	// ICSharpCode.SharpZipLib.Tar.TarBuffer ICSharpCode.SharpZipLib.Tar.TarOutputStream::buffer
	TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C * ___buffer_10;
	// System.IO.Stream ICSharpCode.SharpZipLib.Tar.TarOutputStream::outputStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___outputStream_11;

public:
	inline static int32_t get_offset_of_currBytes_4() { return static_cast<int32_t>(offsetof(TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838, ___currBytes_4)); }
	inline int64_t get_currBytes_4() const { return ___currBytes_4; }
	inline int64_t* get_address_of_currBytes_4() { return &___currBytes_4; }
	inline void set_currBytes_4(int64_t value)
	{
		___currBytes_4 = value;
	}

	inline static int32_t get_offset_of_assemblyBufferLength_5() { return static_cast<int32_t>(offsetof(TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838, ___assemblyBufferLength_5)); }
	inline int32_t get_assemblyBufferLength_5() const { return ___assemblyBufferLength_5; }
	inline int32_t* get_address_of_assemblyBufferLength_5() { return &___assemblyBufferLength_5; }
	inline void set_assemblyBufferLength_5(int32_t value)
	{
		___assemblyBufferLength_5 = value;
	}

	inline static int32_t get_offset_of_isClosed_6() { return static_cast<int32_t>(offsetof(TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838, ___isClosed_6)); }
	inline bool get_isClosed_6() const { return ___isClosed_6; }
	inline bool* get_address_of_isClosed_6() { return &___isClosed_6; }
	inline void set_isClosed_6(bool value)
	{
		___isClosed_6 = value;
	}

	inline static int32_t get_offset_of_currSize_7() { return static_cast<int32_t>(offsetof(TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838, ___currSize_7)); }
	inline int64_t get_currSize_7() const { return ___currSize_7; }
	inline int64_t* get_address_of_currSize_7() { return &___currSize_7; }
	inline void set_currSize_7(int64_t value)
	{
		___currSize_7 = value;
	}

	inline static int32_t get_offset_of_blockBuffer_8() { return static_cast<int32_t>(offsetof(TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838, ___blockBuffer_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_blockBuffer_8() const { return ___blockBuffer_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_blockBuffer_8() { return &___blockBuffer_8; }
	inline void set_blockBuffer_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___blockBuffer_8 = value;
		Il2CppCodeGenWriteBarrier((&___blockBuffer_8), value);
	}

	inline static int32_t get_offset_of_assemblyBuffer_9() { return static_cast<int32_t>(offsetof(TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838, ___assemblyBuffer_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_assemblyBuffer_9() const { return ___assemblyBuffer_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_assemblyBuffer_9() { return &___assemblyBuffer_9; }
	inline void set_assemblyBuffer_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___assemblyBuffer_9 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyBuffer_9), value);
	}

	inline static int32_t get_offset_of_buffer_10() { return static_cast<int32_t>(offsetof(TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838, ___buffer_10)); }
	inline TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C * get_buffer_10() const { return ___buffer_10; }
	inline TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C ** get_address_of_buffer_10() { return &___buffer_10; }
	inline void set_buffer_10(TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C * value)
	{
		___buffer_10 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_10), value);
	}

	inline static int32_t get_offset_of_outputStream_11() { return static_cast<int32_t>(offsetof(TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838, ___outputStream_11)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_outputStream_11() const { return ___outputStream_11; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_outputStream_11() { return &___outputStream_11; }
	inline void set_outputStream_11(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___outputStream_11 = value;
		Il2CppCodeGenWriteBarrier((&___outputStream_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAROUTPUTSTREAM_T5327E699157D91D4FD74D62ACCD54C2C539C8838_H
#ifndef INFLATERINPUTSTREAM_T73956344292D849DFE9A530C35B00DBE415C7DF6_H
#define INFLATERINPUTSTREAM_T73956344292D849DFE9A530C35B00DBE415C7DF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream
struct  InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// ICSharpCode.SharpZipLib.Zip.Compression.Inflater ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::inf
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB * ___inf_4;
	// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::inputBuffer
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3 * ___inputBuffer_5;
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::baseInputStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___baseInputStream_6;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::isClosed
	bool ___isClosed_7;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::isStreamOwner
	bool ___isStreamOwner_8;

public:
	inline static int32_t get_offset_of_inf_4() { return static_cast<int32_t>(offsetof(InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6, ___inf_4)); }
	inline Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB * get_inf_4() const { return ___inf_4; }
	inline Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB ** get_address_of_inf_4() { return &___inf_4; }
	inline void set_inf_4(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB * value)
	{
		___inf_4 = value;
		Il2CppCodeGenWriteBarrier((&___inf_4), value);
	}

	inline static int32_t get_offset_of_inputBuffer_5() { return static_cast<int32_t>(offsetof(InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6, ___inputBuffer_5)); }
	inline InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3 * get_inputBuffer_5() const { return ___inputBuffer_5; }
	inline InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3 ** get_address_of_inputBuffer_5() { return &___inputBuffer_5; }
	inline void set_inputBuffer_5(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3 * value)
	{
		___inputBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___inputBuffer_5), value);
	}

	inline static int32_t get_offset_of_baseInputStream_6() { return static_cast<int32_t>(offsetof(InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6, ___baseInputStream_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_baseInputStream_6() const { return ___baseInputStream_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_baseInputStream_6() { return &___baseInputStream_6; }
	inline void set_baseInputStream_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___baseInputStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___baseInputStream_6), value);
	}

	inline static int32_t get_offset_of_isClosed_7() { return static_cast<int32_t>(offsetof(InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6, ___isClosed_7)); }
	inline bool get_isClosed_7() const { return ___isClosed_7; }
	inline bool* get_address_of_isClosed_7() { return &___isClosed_7; }
	inline void set_isClosed_7(bool value)
	{
		___isClosed_7 = value;
	}

	inline static int32_t get_offset_of_isStreamOwner_8() { return static_cast<int32_t>(offsetof(InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6, ___isStreamOwner_8)); }
	inline bool get_isStreamOwner_8() const { return ___isStreamOwner_8; }
	inline bool* get_address_of_isStreamOwner_8() { return &___isStreamOwner_8; }
	inline void set_isStreamOwner_8(bool value)
	{
		___isStreamOwner_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATERINPUTSTREAM_T73956344292D849DFE9A530C35B00DBE415C7DF6_H
#ifndef COMPRESSIONMETHOD_T1F31EE4DB3B1958C77654ED6CDA048854A028ECD_H
#define COMPRESSIONMETHOD_T1F31EE4DB3B1958C77654ED6CDA048854A028ECD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.CompressionMethod
struct  CompressionMethod_t1F31EE4DB3B1958C77654ED6CDA048854A028ECD 
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.CompressionMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompressionMethod_t1F31EE4DB3B1958C77654ED6CDA048854A028ECD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONMETHOD_T1F31EE4DB3B1958C77654ED6CDA048854A028ECD_H
#ifndef USEZIP64_T64A23EE068A11F6879053B3C4EFBF67F75D3B884_H
#define USEZIP64_T64A23EE068A11F6879053B3C4EFBF67F75D3B884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.UseZip64
struct  UseZip64_t64A23EE068A11F6879053B3C4EFBF67F75D3B884 
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.UseZip64::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UseZip64_t64A23EE068A11F6879053B3C4EFBF67F75D3B884, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USEZIP64_T64A23EE068A11F6879053B3C4EFBF67F75D3B884_H
#ifndef KNOWN_TC640ED74F2497140C21D74C29A9E194AC89EF2FB_H
#define KNOWN_TC640ED74F2497140C21D74C29A9E194AC89EF2FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipEntry/Known
struct  Known_tC640ED74F2497140C21D74C29A9E194AC89EF2FB 
{
public:
	// System.Byte ICSharpCode.SharpZipLib.Zip.ZipEntry/Known::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Known_tC640ED74F2497140C21D74C29A9E194AC89EF2FB, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KNOWN_TC640ED74F2497140C21D74C29A9E194AC89EF2FB_H
#ifndef ZIPENTRYFACTORY_TD953786FCD19BFC64F0A014807EB7B46B0BEF929_H
#define ZIPENTRYFACTORY_TD953786FCD19BFC64F0A014807EB7B46B0BEF929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipEntryFactory
struct  ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929  : public RuntimeObject
{
public:
	// ICSharpCode.SharpZipLib.Core.INameTransform ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::nameTransform_
	RuntimeObject* ___nameTransform__0;
	// System.DateTime ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::fixedDateTime_
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___fixedDateTime__1;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::getAttributes_
	int32_t ___getAttributes__2;

public:
	inline static int32_t get_offset_of_nameTransform__0() { return static_cast<int32_t>(offsetof(ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929, ___nameTransform__0)); }
	inline RuntimeObject* get_nameTransform__0() const { return ___nameTransform__0; }
	inline RuntimeObject** get_address_of_nameTransform__0() { return &___nameTransform__0; }
	inline void set_nameTransform__0(RuntimeObject* value)
	{
		___nameTransform__0 = value;
		Il2CppCodeGenWriteBarrier((&___nameTransform__0), value);
	}

	inline static int32_t get_offset_of_fixedDateTime__1() { return static_cast<int32_t>(offsetof(ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929, ___fixedDateTime__1)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_fixedDateTime__1() const { return ___fixedDateTime__1; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_fixedDateTime__1() { return &___fixedDateTime__1; }
	inline void set_fixedDateTime__1(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___fixedDateTime__1 = value;
	}

	inline static int32_t get_offset_of_getAttributes__2() { return static_cast<int32_t>(offsetof(ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929, ___getAttributes__2)); }
	inline int32_t get_getAttributes__2() const { return ___getAttributes__2; }
	inline int32_t* get_address_of_getAttributes__2() { return &___getAttributes__2; }
	inline void set_getAttributes__2(int32_t value)
	{
		___getAttributes__2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPENTRYFACTORY_TD953786FCD19BFC64F0A014807EB7B46B0BEF929_H
#ifndef HEADERTEST_T98D72CE7316D175D3364E46D294E73256D1532D8_H
#define HEADERTEST_T98D72CE7316D175D3364E46D294E73256D1532D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipFile/HeaderTest
struct  HeaderTest_t98D72CE7316D175D3364E46D294E73256D1532D8 
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile/HeaderTest::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HeaderTest_t98D72CE7316D175D3364E46D294E73256D1532D8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERTEST_T98D72CE7316D175D3364E46D294E73256D1532D8_H
#ifndef PARTIALINPUTSTREAM_T5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195_H
#define PARTIALINPUTSTREAM_T5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream
struct  PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// ICSharpCode.SharpZipLib.Zip.ZipFile ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::zipFile_
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F * ___zipFile__4;
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::baseStream_
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___baseStream__5;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::start_
	int64_t ___start__6;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::length_
	int64_t ___length__7;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::readPos_
	int64_t ___readPos__8;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::end_
	int64_t ___end__9;

public:
	inline static int32_t get_offset_of_zipFile__4() { return static_cast<int32_t>(offsetof(PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195, ___zipFile__4)); }
	inline ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F * get_zipFile__4() const { return ___zipFile__4; }
	inline ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F ** get_address_of_zipFile__4() { return &___zipFile__4; }
	inline void set_zipFile__4(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F * value)
	{
		___zipFile__4 = value;
		Il2CppCodeGenWriteBarrier((&___zipFile__4), value);
	}

	inline static int32_t get_offset_of_baseStream__5() { return static_cast<int32_t>(offsetof(PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195, ___baseStream__5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_baseStream__5() const { return ___baseStream__5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_baseStream__5() { return &___baseStream__5; }
	inline void set_baseStream__5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___baseStream__5 = value;
		Il2CppCodeGenWriteBarrier((&___baseStream__5), value);
	}

	inline static int32_t get_offset_of_start__6() { return static_cast<int32_t>(offsetof(PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195, ___start__6)); }
	inline int64_t get_start__6() const { return ___start__6; }
	inline int64_t* get_address_of_start__6() { return &___start__6; }
	inline void set_start__6(int64_t value)
	{
		___start__6 = value;
	}

	inline static int32_t get_offset_of_length__7() { return static_cast<int32_t>(offsetof(PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195, ___length__7)); }
	inline int64_t get_length__7() const { return ___length__7; }
	inline int64_t* get_address_of_length__7() { return &___length__7; }
	inline void set_length__7(int64_t value)
	{
		___length__7 = value;
	}

	inline static int32_t get_offset_of_readPos__8() { return static_cast<int32_t>(offsetof(PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195, ___readPos__8)); }
	inline int64_t get_readPos__8() const { return ___readPos__8; }
	inline int64_t* get_address_of_readPos__8() { return &___readPos__8; }
	inline void set_readPos__8(int64_t value)
	{
		___readPos__8 = value;
	}

	inline static int32_t get_offset_of_end__9() { return static_cast<int32_t>(offsetof(PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195, ___end__9)); }
	inline int64_t get_end__9() const { return ___end__9; }
	inline int64_t* get_address_of_end__9() { return &___end__9; }
	inline void set_end__9(int64_t value)
	{
		___end__9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTIALINPUTSTREAM_T5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195_H
#ifndef ZIPHELPERSTREAM_T5EBF734E826C8FAB30C920728A06E3291AB40B12_H
#define ZIPHELPERSTREAM_T5EBF734E826C8FAB30C920728A06E3291AB40B12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipHelperStream
struct  ZipHelperStream_t5EBF734E826C8FAB30C920728A06E3291AB40B12  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::isOwner_
	bool ___isOwner__4;
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipHelperStream::stream_
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream__5;

public:
	inline static int32_t get_offset_of_isOwner__4() { return static_cast<int32_t>(offsetof(ZipHelperStream_t5EBF734E826C8FAB30C920728A06E3291AB40B12, ___isOwner__4)); }
	inline bool get_isOwner__4() const { return ___isOwner__4; }
	inline bool* get_address_of_isOwner__4() { return &___isOwner__4; }
	inline void set_isOwner__4(bool value)
	{
		___isOwner__4 = value;
	}

	inline static int32_t get_offset_of_stream__5() { return static_cast<int32_t>(offsetof(ZipHelperStream_t5EBF734E826C8FAB30C920728A06E3291AB40B12, ___stream__5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream__5() const { return ___stream__5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream__5() { return &___stream__5; }
	inline void set_stream__5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream__5 = value;
		Il2CppCodeGenWriteBarrier((&___stream__5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPHELPERSTREAM_T5EBF734E826C8FAB30C920728A06E3291AB40B12_H
#ifndef XPROPERTYDESCRIPTOR_2_TB243420F73078FF83B4901B34B4FEFC2F260686E_H
#define XPROPERTYDESCRIPTOR_2_TB243420F73078FF83B4901B34B4FEFC2F260686E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Linq.ComponentModel.XPropertyDescriptor`2<System.Xml.Linq.XAttribute,System.String>
struct  XPropertyDescriptor_2_tB243420F73078FF83B4901B34B4FEFC2F260686E  : public PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPROPERTYDESCRIPTOR_2_TB243420F73078FF83B4901B34B4FEFC2F260686E_H
#ifndef XPROPERTYDESCRIPTOR_2_T197028EA1CE689F52D9E0368C60D4DF321C65CCE_H
#define XPROPERTYDESCRIPTOR_2_T197028EA1CE689F52D9E0368C60D4DF321C65CCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Linq.ComponentModel.XPropertyDescriptor`2<System.Xml.Linq.XElement,System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement>>
struct  XPropertyDescriptor_2_t197028EA1CE689F52D9E0368C60D4DF321C65CCE  : public PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPROPERTYDESCRIPTOR_2_T197028EA1CE689F52D9E0368C60D4DF321C65CCE_H
#ifndef XPROPERTYDESCRIPTOR_2_T08258A32886D9FF6C631A406809DBE40C0A7680B_H
#define XPROPERTYDESCRIPTOR_2_T08258A32886D9FF6C631A406809DBE40C0A7680B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Linq.ComponentModel.XPropertyDescriptor`2<System.Xml.Linq.XElement,System.Object>
struct  XPropertyDescriptor_2_t08258A32886D9FF6C631A406809DBE40C0A7680B  : public PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPROPERTYDESCRIPTOR_2_T08258A32886D9FF6C631A406809DBE40C0A7680B_H
#ifndef XPROPERTYDESCRIPTOR_2_T686B10FB0E5822D980C88672E12AE9C310DBDBEF_H
#define XPROPERTYDESCRIPTOR_2_T686B10FB0E5822D980C88672E12AE9C310DBDBEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Linq.ComponentModel.XPropertyDescriptor`2<System.Xml.Linq.XElement,System.String>
struct  XPropertyDescriptor_2_t686B10FB0E5822D980C88672E12AE9C310DBDBEF  : public PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPROPERTYDESCRIPTOR_2_T686B10FB0E5822D980C88672E12AE9C310DBDBEF_H
#ifndef CONSTRUCTORHANDLING_T24063773444D2C4382EB581D086B8F078E533371_H
#define CONSTRUCTORHANDLING_T24063773444D2C4382EB581D086B8F078E533371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ConstructorHandling
struct  ConstructorHandling_t24063773444D2C4382EB581D086B8F078E533371 
{
public:
	// System.Int32 Newtonsoft.Json.ConstructorHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConstructorHandling_t24063773444D2C4382EB581D086B8F078E533371, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORHANDLING_T24063773444D2C4382EB581D086B8F078E533371_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef CIPHERMODE_T1DC3069D617AC3D17A2608F5BB36C0F115D229DF_H
#define CIPHERMODE_T1DC3069D617AC3D17A2608F5BB36C0F115D229DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CipherMode
struct  CipherMode_t1DC3069D617AC3D17A2608F5BB36C0F115D229DF 
{
public:
	// System.Int32 System.Security.Cryptography.CipherMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherMode_t1DC3069D617AC3D17A2608F5BB36C0F115D229DF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERMODE_T1DC3069D617AC3D17A2608F5BB36C0F115D229DF_H
#ifndef CRYPTOSTREAMMODE_TDA87D3C6D96B2FCC35184B3B4A00923266ADB2D5_H
#define CRYPTOSTREAMMODE_TDA87D3C6D96B2FCC35184B3B4A00923266ADB2D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CryptoStreamMode
struct  CryptoStreamMode_tDA87D3C6D96B2FCC35184B3B4A00923266ADB2D5 
{
public:
	// System.Int32 System.Security.Cryptography.CryptoStreamMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CryptoStreamMode_tDA87D3C6D96B2FCC35184B3B4A00923266ADB2D5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOSTREAMMODE_TDA87D3C6D96B2FCC35184B3B4A00923266ADB2D5_H
#ifndef PADDINGMODE_TA6F228B2795D29C9554F2D6824DB9FF67519A0E0_H
#define PADDINGMODE_TA6F228B2795D29C9554F2D6824DB9FF67519A0E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.PaddingMode
struct  PaddingMode_tA6F228B2795D29C9554F2D6824DB9FF67519A0E0 
{
public:
	// System.Int32 System.Security.Cryptography.PaddingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PaddingMode_tA6F228B2795D29C9554F2D6824DB9FF67519A0E0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADDINGMODE_TA6F228B2795D29C9554F2D6824DB9FF67519A0E0_H
#ifndef ELEMENTWRITER_T9469E888D0CE4D9B706DC16A3917D16984561552_H
#define ELEMENTWRITER_T9469E888D0CE4D9B706DC16A3917D16984561552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.ElementWriter
struct  ElementWriter_t9469E888D0CE4D9B706DC16A3917D16984561552 
{
public:
	// System.Xml.XmlWriter System.Xml.Linq.ElementWriter::writer
	XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * ___writer_0;
	// System.Xml.Linq.NamespaceResolver System.Xml.Linq.ElementWriter::resolver
	NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D  ___resolver_1;

public:
	inline static int32_t get_offset_of_writer_0() { return static_cast<int32_t>(offsetof(ElementWriter_t9469E888D0CE4D9B706DC16A3917D16984561552, ___writer_0)); }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * get_writer_0() const { return ___writer_0; }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 ** get_address_of_writer_0() { return &___writer_0; }
	inline void set_writer_0(XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * value)
	{
		___writer_0 = value;
		Il2CppCodeGenWriteBarrier((&___writer_0), value);
	}

	inline static int32_t get_offset_of_resolver_1() { return static_cast<int32_t>(offsetof(ElementWriter_t9469E888D0CE4D9B706DC16A3917D16984561552, ___resolver_1)); }
	inline NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D  get_resolver_1() const { return ___resolver_1; }
	inline NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D * get_address_of_resolver_1() { return &___resolver_1; }
	inline void set_resolver_1(NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D  value)
	{
		___resolver_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Linq.ElementWriter
struct ElementWriter_t9469E888D0CE4D9B706DC16A3917D16984561552_marshaled_pinvoke
{
	XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * ___writer_0;
	NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D_marshaled_pinvoke ___resolver_1;
};
// Native definition for COM marshalling of System.Xml.Linq.ElementWriter
struct ElementWriter_t9469E888D0CE4D9B706DC16A3917D16984561552_marshaled_com
{
	XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * ___writer_0;
	NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D_marshaled_com ___resolver_1;
};
#endif // ELEMENTWRITER_T9469E888D0CE4D9B706DC16A3917D16984561552_H
#ifndef LOADOPTIONS_T351EDF45A682830EA523299A126655C72CF76643_H
#define LOADOPTIONS_T351EDF45A682830EA523299A126655C72CF76643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.LoadOptions
struct  LoadOptions_t351EDF45A682830EA523299A126655C72CF76643 
{
public:
	// System.Int32 System.Xml.Linq.LoadOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadOptions_t351EDF45A682830EA523299A126655C72CF76643, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADOPTIONS_T351EDF45A682830EA523299A126655C72CF76643_H
#ifndef SAVEOPTIONS_T3EA240C0E910B76C6D762B139C6E03F0496DD3BC_H
#define SAVEOPTIONS_T3EA240C0E910B76C6D762B139C6E03F0496DD3BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.SaveOptions
struct  SaveOptions_t3EA240C0E910B76C6D762B139C6E03F0496DD3BC 
{
public:
	// System.Int32 System.Xml.Linq.SaveOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SaveOptions_t3EA240C0E910B76C6D762B139C6E03F0496DD3BC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEOPTIONS_T3EA240C0E910B76C6D762B139C6E03F0496DD3BC_H
#ifndef STREAMINGELEMENTWRITER_T3A8F2AEFE076A10FB4D09D511F4D568A1E931205_H
#define STREAMINGELEMENTWRITER_T3A8F2AEFE076A10FB4D09D511F4D568A1E931205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.StreamingElementWriter
struct  StreamingElementWriter_t3A8F2AEFE076A10FB4D09D511F4D568A1E931205 
{
public:
	// System.Xml.XmlWriter System.Xml.Linq.StreamingElementWriter::writer
	XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * ___writer_0;
	// System.Xml.Linq.XStreamingElement System.Xml.Linq.StreamingElementWriter::element
	XStreamingElement_t6B73236C8904BD25EBD29C8A18941012D8DA6D45 * ___element_1;
	// System.Collections.Generic.List`1<System.Xml.Linq.XAttribute> System.Xml.Linq.StreamingElementWriter::attributes
	List_1_t086F7CFD3949A2E3FFB2410304205A5C947629CA * ___attributes_2;
	// System.Xml.Linq.NamespaceResolver System.Xml.Linq.StreamingElementWriter::resolver
	NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D  ___resolver_3;

public:
	inline static int32_t get_offset_of_writer_0() { return static_cast<int32_t>(offsetof(StreamingElementWriter_t3A8F2AEFE076A10FB4D09D511F4D568A1E931205, ___writer_0)); }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * get_writer_0() const { return ___writer_0; }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 ** get_address_of_writer_0() { return &___writer_0; }
	inline void set_writer_0(XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * value)
	{
		___writer_0 = value;
		Il2CppCodeGenWriteBarrier((&___writer_0), value);
	}

	inline static int32_t get_offset_of_element_1() { return static_cast<int32_t>(offsetof(StreamingElementWriter_t3A8F2AEFE076A10FB4D09D511F4D568A1E931205, ___element_1)); }
	inline XStreamingElement_t6B73236C8904BD25EBD29C8A18941012D8DA6D45 * get_element_1() const { return ___element_1; }
	inline XStreamingElement_t6B73236C8904BD25EBD29C8A18941012D8DA6D45 ** get_address_of_element_1() { return &___element_1; }
	inline void set_element_1(XStreamingElement_t6B73236C8904BD25EBD29C8A18941012D8DA6D45 * value)
	{
		___element_1 = value;
		Il2CppCodeGenWriteBarrier((&___element_1), value);
	}

	inline static int32_t get_offset_of_attributes_2() { return static_cast<int32_t>(offsetof(StreamingElementWriter_t3A8F2AEFE076A10FB4D09D511F4D568A1E931205, ___attributes_2)); }
	inline List_1_t086F7CFD3949A2E3FFB2410304205A5C947629CA * get_attributes_2() const { return ___attributes_2; }
	inline List_1_t086F7CFD3949A2E3FFB2410304205A5C947629CA ** get_address_of_attributes_2() { return &___attributes_2; }
	inline void set_attributes_2(List_1_t086F7CFD3949A2E3FFB2410304205A5C947629CA * value)
	{
		___attributes_2 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_2), value);
	}

	inline static int32_t get_offset_of_resolver_3() { return static_cast<int32_t>(offsetof(StreamingElementWriter_t3A8F2AEFE076A10FB4D09D511F4D568A1E931205, ___resolver_3)); }
	inline NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D  get_resolver_3() const { return ___resolver_3; }
	inline NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D * get_address_of_resolver_3() { return &___resolver_3; }
	inline void set_resolver_3(NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D  value)
	{
		___resolver_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Linq.StreamingElementWriter
struct StreamingElementWriter_t3A8F2AEFE076A10FB4D09D511F4D568A1E931205_marshaled_pinvoke
{
	XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * ___writer_0;
	XStreamingElement_t6B73236C8904BD25EBD29C8A18941012D8DA6D45 * ___element_1;
	List_1_t086F7CFD3949A2E3FFB2410304205A5C947629CA * ___attributes_2;
	NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D_marshaled_pinvoke ___resolver_3;
};
// Native definition for COM marshalling of System.Xml.Linq.StreamingElementWriter
struct StreamingElementWriter_t3A8F2AEFE076A10FB4D09D511F4D568A1E931205_marshaled_com
{
	XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * ___writer_0;
	XStreamingElement_t6B73236C8904BD25EBD29C8A18941012D8DA6D45 * ___element_1;
	List_1_t086F7CFD3949A2E3FFB2410304205A5C947629CA * ___attributes_2;
	NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D_marshaled_com ___resolver_3;
};
#endif // STREAMINGELEMENTWRITER_T3A8F2AEFE076A10FB4D09D511F4D568A1E931205_H
#ifndef XCOMMENT_T4A3C0B2424F442F0C1F76C33354476FFD90788AD_H
#define XCOMMENT_T4A3C0B2424F442F0C1F76C33354476FFD90788AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XComment
struct  XComment_t4A3C0B2424F442F0C1F76C33354476FFD90788AD  : public XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751
{
public:
	// System.String System.Xml.Linq.XComment::value
	String_t* ___value_3;

public:
	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(XComment_t4A3C0B2424F442F0C1F76C33354476FFD90788AD, ___value_3)); }
	inline String_t* get_value_3() const { return ___value_3; }
	inline String_t** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(String_t* value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCOMMENT_T4A3C0B2424F442F0C1F76C33354476FFD90788AD_H
#ifndef XCONTAINER_T17E75C21493AAC7D12D6159A3A8C5308B3DBC599_H
#define XCONTAINER_T17E75C21493AAC7D12D6159A3A8C5308B3DBC599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XContainer
struct  XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599  : public XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751
{
public:
	// System.Object System.Xml.Linq.XContainer::content
	RuntimeObject * ___content_3;

public:
	inline static int32_t get_offset_of_content_3() { return static_cast<int32_t>(offsetof(XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599, ___content_3)); }
	inline RuntimeObject * get_content_3() const { return ___content_3; }
	inline RuntimeObject ** get_address_of_content_3() { return &___content_3; }
	inline void set_content_3(RuntimeObject * value)
	{
		___content_3 = value;
		Il2CppCodeGenWriteBarrier((&___content_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCONTAINER_T17E75C21493AAC7D12D6159A3A8C5308B3DBC599_H
#ifndef XDOCUMENTTYPE_TB3120C99DFF0207A8DDAF94868BA84AD2296E444_H
#define XDOCUMENTTYPE_TB3120C99DFF0207A8DDAF94868BA84AD2296E444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XDocumentType
struct  XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444  : public XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751
{
public:
	// System.String System.Xml.Linq.XDocumentType::name
	String_t* ___name_3;
	// System.String System.Xml.Linq.XDocumentType::publicId
	String_t* ___publicId_4;
	// System.String System.Xml.Linq.XDocumentType::systemId
	String_t* ___systemId_5;
	// System.String System.Xml.Linq.XDocumentType::internalSubset
	String_t* ___internalSubset_6;
	// System.Xml.IDtdInfo System.Xml.Linq.XDocumentType::dtdInfo
	RuntimeObject* ___dtdInfo_7;

public:
	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_publicId_4() { return static_cast<int32_t>(offsetof(XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444, ___publicId_4)); }
	inline String_t* get_publicId_4() const { return ___publicId_4; }
	inline String_t** get_address_of_publicId_4() { return &___publicId_4; }
	inline void set_publicId_4(String_t* value)
	{
		___publicId_4 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_4), value);
	}

	inline static int32_t get_offset_of_systemId_5() { return static_cast<int32_t>(offsetof(XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444, ___systemId_5)); }
	inline String_t* get_systemId_5() const { return ___systemId_5; }
	inline String_t** get_address_of_systemId_5() { return &___systemId_5; }
	inline void set_systemId_5(String_t* value)
	{
		___systemId_5 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_5), value);
	}

	inline static int32_t get_offset_of_internalSubset_6() { return static_cast<int32_t>(offsetof(XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444, ___internalSubset_6)); }
	inline String_t* get_internalSubset_6() const { return ___internalSubset_6; }
	inline String_t** get_address_of_internalSubset_6() { return &___internalSubset_6; }
	inline void set_internalSubset_6(String_t* value)
	{
		___internalSubset_6 = value;
		Il2CppCodeGenWriteBarrier((&___internalSubset_6), value);
	}

	inline static int32_t get_offset_of_dtdInfo_7() { return static_cast<int32_t>(offsetof(XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444, ___dtdInfo_7)); }
	inline RuntimeObject* get_dtdInfo_7() const { return ___dtdInfo_7; }
	inline RuntimeObject** get_address_of_dtdInfo_7() { return &___dtdInfo_7; }
	inline void set_dtdInfo_7(RuntimeObject* value)
	{
		___dtdInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___dtdInfo_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDOCUMENTTYPE_TB3120C99DFF0207A8DDAF94868BA84AD2296E444_H
#ifndef XPROCESSINGINSTRUCTION_T9136E2BCE4B8BBE51093AAC9AEC92F05E577A213_H
#define XPROCESSINGINSTRUCTION_T9136E2BCE4B8BBE51093AAC9AEC92F05E577A213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XProcessingInstruction
struct  XProcessingInstruction_t9136E2BCE4B8BBE51093AAC9AEC92F05E577A213  : public XNode_tC1E0A039E17CD7048FD925F35FB0413D3D292751
{
public:
	// System.String System.Xml.Linq.XProcessingInstruction::target
	String_t* ___target_3;
	// System.String System.Xml.Linq.XProcessingInstruction::data
	String_t* ___data_4;

public:
	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(XProcessingInstruction_t9136E2BCE4B8BBE51093AAC9AEC92F05E577A213, ___target_3)); }
	inline String_t* get_target_3() const { return ___target_3; }
	inline String_t** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(String_t* value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(XProcessingInstruction_t9136E2BCE4B8BBE51093AAC9AEC92F05E577A213, ___data_4)); }
	inline String_t* get_data_4() const { return ___data_4; }
	inline String_t** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(String_t* value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((&___data_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPROCESSINGINSTRUCTION_T9136E2BCE4B8BBE51093AAC9AEC92F05E577A213_H
#ifndef GZIPEXCEPTION_T414765B7EF634182FC2C85E0AB1FA929E3A591C4_H
#define GZIPEXCEPTION_T414765B7EF634182FC2C85E0AB1FA929E3A591C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.GZip.GZipException
struct  GZipException_t414765B7EF634182FC2C85E0AB1FA929E3A591C4  : public SharpZipBaseException_t822541D5F0EC9FCA3550F00DCF0883E987E5045F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GZIPEXCEPTION_T414765B7EF634182FC2C85E0AB1FA929E3A591C4_H
#ifndef GZIPINPUTSTREAM_T56798E4A16A18A8EC793908E2E815DF63BF9289A_H
#define GZIPINPUTSTREAM_T56798E4A16A18A8EC793908E2E815DF63BF9289A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.GZip.GZipInputStream
struct  GZipInputStream_t56798E4A16A18A8EC793908E2E815DF63BF9289A  : public InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6
{
public:
	// ICSharpCode.SharpZipLib.Checksums.Crc32 ICSharpCode.SharpZipLib.GZip.GZipInputStream::crc
	Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F * ___crc_9;
	// System.Boolean ICSharpCode.SharpZipLib.GZip.GZipInputStream::readGZIPHeader
	bool ___readGZIPHeader_10;

public:
	inline static int32_t get_offset_of_crc_9() { return static_cast<int32_t>(offsetof(GZipInputStream_t56798E4A16A18A8EC793908E2E815DF63BF9289A, ___crc_9)); }
	inline Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F * get_crc_9() const { return ___crc_9; }
	inline Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F ** get_address_of_crc_9() { return &___crc_9; }
	inline void set_crc_9(Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F * value)
	{
		___crc_9 = value;
		Il2CppCodeGenWriteBarrier((&___crc_9), value);
	}

	inline static int32_t get_offset_of_readGZIPHeader_10() { return static_cast<int32_t>(offsetof(GZipInputStream_t56798E4A16A18A8EC793908E2E815DF63BF9289A, ___readGZIPHeader_10)); }
	inline bool get_readGZIPHeader_10() const { return ___readGZIPHeader_10; }
	inline bool* get_address_of_readGZIPHeader_10() { return &___readGZIPHeader_10; }
	inline void set_readGZIPHeader_10(bool value)
	{
		___readGZIPHeader_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GZIPINPUTSTREAM_T56798E4A16A18A8EC793908E2E815DF63BF9289A_H
#ifndef TAREXCEPTION_T7ADB01E2D320C153961C7EDAF801B73E3A9B1D66_H
#define TAREXCEPTION_T7ADB01E2D320C153961C7EDAF801B73E3A9B1D66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.TarException
struct  TarException_t7ADB01E2D320C153961C7EDAF801B73E3A9B1D66  : public SharpZipBaseException_t822541D5F0EC9FCA3550F00DCF0883E987E5045F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAREXCEPTION_T7ADB01E2D320C153961C7EDAF801B73E3A9B1D66_H
#ifndef ZIPENTRY_T0C490E8147650DD0F764C17C5D545E633E38E9E5_H
#define ZIPENTRY_T0C490E8147650DD0F764C17C5D545E633E38E9E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct  ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5  : public RuntimeObject
{
public:
	// ICSharpCode.SharpZipLib.Zip.ZipEntry/Known ICSharpCode.SharpZipLib.Zip.ZipEntry::known
	uint8_t ___known_0;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::externalFileAttributes
	int32_t ___externalFileAttributes_1;
	// System.UInt16 ICSharpCode.SharpZipLib.Zip.ZipEntry::versionMadeBy
	uint16_t ___versionMadeBy_2;
	// System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::name
	String_t* ___name_3;
	// System.UInt64 ICSharpCode.SharpZipLib.Zip.ZipEntry::size
	uint64_t ___size_4;
	// System.UInt64 ICSharpCode.SharpZipLib.Zip.ZipEntry::compressedSize
	uint64_t ___compressedSize_5;
	// System.UInt16 ICSharpCode.SharpZipLib.Zip.ZipEntry::versionToExtract
	uint16_t ___versionToExtract_6;
	// System.UInt32 ICSharpCode.SharpZipLib.Zip.ZipEntry::crc
	uint32_t ___crc_7;
	// System.UInt32 ICSharpCode.SharpZipLib.Zip.ZipEntry::dosTime
	uint32_t ___dosTime_8;
	// ICSharpCode.SharpZipLib.Zip.CompressionMethod ICSharpCode.SharpZipLib.Zip.ZipEntry::method
	int32_t ___method_9;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipEntry::extra
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___extra_10;
	// System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::comment
	String_t* ___comment_11;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::flags
	int32_t ___flags_12;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::zipFileIndex
	int64_t ___zipFileIndex_13;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::offset
	int64_t ___offset_14;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::forceZip64_
	bool ___forceZip64__15;
	// System.Byte ICSharpCode.SharpZipLib.Zip.ZipEntry::cryptoCheckValue_
	uint8_t ___cryptoCheckValue__16;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::_aesVer
	int32_t ____aesVer_17;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::_aesEncryptionStrength
	int32_t ____aesEncryptionStrength_18;

public:
	inline static int32_t get_offset_of_known_0() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___known_0)); }
	inline uint8_t get_known_0() const { return ___known_0; }
	inline uint8_t* get_address_of_known_0() { return &___known_0; }
	inline void set_known_0(uint8_t value)
	{
		___known_0 = value;
	}

	inline static int32_t get_offset_of_externalFileAttributes_1() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___externalFileAttributes_1)); }
	inline int32_t get_externalFileAttributes_1() const { return ___externalFileAttributes_1; }
	inline int32_t* get_address_of_externalFileAttributes_1() { return &___externalFileAttributes_1; }
	inline void set_externalFileAttributes_1(int32_t value)
	{
		___externalFileAttributes_1 = value;
	}

	inline static int32_t get_offset_of_versionMadeBy_2() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___versionMadeBy_2)); }
	inline uint16_t get_versionMadeBy_2() const { return ___versionMadeBy_2; }
	inline uint16_t* get_address_of_versionMadeBy_2() { return &___versionMadeBy_2; }
	inline void set_versionMadeBy_2(uint16_t value)
	{
		___versionMadeBy_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_size_4() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___size_4)); }
	inline uint64_t get_size_4() const { return ___size_4; }
	inline uint64_t* get_address_of_size_4() { return &___size_4; }
	inline void set_size_4(uint64_t value)
	{
		___size_4 = value;
	}

	inline static int32_t get_offset_of_compressedSize_5() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___compressedSize_5)); }
	inline uint64_t get_compressedSize_5() const { return ___compressedSize_5; }
	inline uint64_t* get_address_of_compressedSize_5() { return &___compressedSize_5; }
	inline void set_compressedSize_5(uint64_t value)
	{
		___compressedSize_5 = value;
	}

	inline static int32_t get_offset_of_versionToExtract_6() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___versionToExtract_6)); }
	inline uint16_t get_versionToExtract_6() const { return ___versionToExtract_6; }
	inline uint16_t* get_address_of_versionToExtract_6() { return &___versionToExtract_6; }
	inline void set_versionToExtract_6(uint16_t value)
	{
		___versionToExtract_6 = value;
	}

	inline static int32_t get_offset_of_crc_7() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___crc_7)); }
	inline uint32_t get_crc_7() const { return ___crc_7; }
	inline uint32_t* get_address_of_crc_7() { return &___crc_7; }
	inline void set_crc_7(uint32_t value)
	{
		___crc_7 = value;
	}

	inline static int32_t get_offset_of_dosTime_8() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___dosTime_8)); }
	inline uint32_t get_dosTime_8() const { return ___dosTime_8; }
	inline uint32_t* get_address_of_dosTime_8() { return &___dosTime_8; }
	inline void set_dosTime_8(uint32_t value)
	{
		___dosTime_8 = value;
	}

	inline static int32_t get_offset_of_method_9() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___method_9)); }
	inline int32_t get_method_9() const { return ___method_9; }
	inline int32_t* get_address_of_method_9() { return &___method_9; }
	inline void set_method_9(int32_t value)
	{
		___method_9 = value;
	}

	inline static int32_t get_offset_of_extra_10() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___extra_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_extra_10() const { return ___extra_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_extra_10() { return &___extra_10; }
	inline void set_extra_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___extra_10 = value;
		Il2CppCodeGenWriteBarrier((&___extra_10), value);
	}

	inline static int32_t get_offset_of_comment_11() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___comment_11)); }
	inline String_t* get_comment_11() const { return ___comment_11; }
	inline String_t** get_address_of_comment_11() { return &___comment_11; }
	inline void set_comment_11(String_t* value)
	{
		___comment_11 = value;
		Il2CppCodeGenWriteBarrier((&___comment_11), value);
	}

	inline static int32_t get_offset_of_flags_12() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___flags_12)); }
	inline int32_t get_flags_12() const { return ___flags_12; }
	inline int32_t* get_address_of_flags_12() { return &___flags_12; }
	inline void set_flags_12(int32_t value)
	{
		___flags_12 = value;
	}

	inline static int32_t get_offset_of_zipFileIndex_13() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___zipFileIndex_13)); }
	inline int64_t get_zipFileIndex_13() const { return ___zipFileIndex_13; }
	inline int64_t* get_address_of_zipFileIndex_13() { return &___zipFileIndex_13; }
	inline void set_zipFileIndex_13(int64_t value)
	{
		___zipFileIndex_13 = value;
	}

	inline static int32_t get_offset_of_offset_14() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___offset_14)); }
	inline int64_t get_offset_14() const { return ___offset_14; }
	inline int64_t* get_address_of_offset_14() { return &___offset_14; }
	inline void set_offset_14(int64_t value)
	{
		___offset_14 = value;
	}

	inline static int32_t get_offset_of_forceZip64__15() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___forceZip64__15)); }
	inline bool get_forceZip64__15() const { return ___forceZip64__15; }
	inline bool* get_address_of_forceZip64__15() { return &___forceZip64__15; }
	inline void set_forceZip64__15(bool value)
	{
		___forceZip64__15 = value;
	}

	inline static int32_t get_offset_of_cryptoCheckValue__16() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___cryptoCheckValue__16)); }
	inline uint8_t get_cryptoCheckValue__16() const { return ___cryptoCheckValue__16; }
	inline uint8_t* get_address_of_cryptoCheckValue__16() { return &___cryptoCheckValue__16; }
	inline void set_cryptoCheckValue__16(uint8_t value)
	{
		___cryptoCheckValue__16 = value;
	}

	inline static int32_t get_offset_of__aesVer_17() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ____aesVer_17)); }
	inline int32_t get__aesVer_17() const { return ____aesVer_17; }
	inline int32_t* get_address_of__aesVer_17() { return &____aesVer_17; }
	inline void set__aesVer_17(int32_t value)
	{
		____aesVer_17 = value;
	}

	inline static int32_t get_offset_of__aesEncryptionStrength_18() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ____aesEncryptionStrength_18)); }
	inline int32_t get__aesEncryptionStrength_18() const { return ____aesEncryptionStrength_18; }
	inline int32_t* get_address_of__aesEncryptionStrength_18() { return &____aesEncryptionStrength_18; }
	inline void set__aesEncryptionStrength_18(int32_t value)
	{
		____aesEncryptionStrength_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPENTRY_T0C490E8147650DD0F764C17C5D545E633E38E9E5_H
#ifndef ZIPEXCEPTION_T555EB5B9EA2BB8DF941C19C19F3BBA466EE7D944_H
#define ZIPEXCEPTION_T555EB5B9EA2BB8DF941C19C19F3BBA466EE7D944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipException
struct  ZipException_t555EB5B9EA2BB8DF941C19C19F3BBA466EE7D944  : public SharpZipBaseException_t822541D5F0EC9FCA3550F00DCF0883E987E5045F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPEXCEPTION_T555EB5B9EA2BB8DF941C19C19F3BBA466EE7D944_H
#ifndef ZIPFILE_TEFDC6C532094F9A5F6185F4E69E340602984E28F_H
#define ZIPFILE_TEFDC6C532094F9A5F6185F4E69E340602984E28F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipFile
struct  ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F  : public RuntimeObject
{
public:
	// ICSharpCode.SharpZipLib.Zip.ZipFile/KeysRequiredEventHandler ICSharpCode.SharpZipLib.Zip.ZipFile::KeysRequired
	KeysRequiredEventHandler_t01BA6F773E29C468C7FD85BE6B93EE5043A521D9 * ___KeysRequired_0;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::isDisposed_
	bool ___isDisposed__1;
	// System.String ICSharpCode.SharpZipLib.Zip.ZipFile::name_
	String_t* ___name__2;
	// System.String ICSharpCode.SharpZipLib.Zip.ZipFile::comment_
	String_t* ___comment__3;
	// System.String ICSharpCode.SharpZipLib.Zip.ZipFile::rawPassword_
	String_t* ___rawPassword__4;
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipFile::baseStream_
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___baseStream__5;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::isStreamOwner
	bool ___isStreamOwner_6;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile::offsetOfFirstEntry
	int64_t ___offsetOfFirstEntry_7;
	// ICSharpCode.SharpZipLib.Zip.ZipEntry[] ICSharpCode.SharpZipLib.Zip.ZipFile::entries_
	ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917* ___entries__8;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipFile::key
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key_9;
	// ICSharpCode.SharpZipLib.Zip.UseZip64 ICSharpCode.SharpZipLib.Zip.ZipFile::useZip64_
	int32_t ___useZip64__10;
	// System.Collections.ArrayList ICSharpCode.SharpZipLib.Zip.ZipFile::updates_
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___updates__11;
	// System.Collections.Hashtable ICSharpCode.SharpZipLib.Zip.ZipFile::updateIndex_
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___updateIndex__12;
	// ICSharpCode.SharpZipLib.Zip.IArchiveStorage ICSharpCode.SharpZipLib.Zip.ZipFile::archiveStorage_
	RuntimeObject* ___archiveStorage__13;
	// ICSharpCode.SharpZipLib.Zip.IDynamicDataSource ICSharpCode.SharpZipLib.Zip.ZipFile::updateDataSource_
	RuntimeObject* ___updateDataSource__14;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile::bufferSize_
	int32_t ___bufferSize__15;
	// ICSharpCode.SharpZipLib.Zip.IEntryFactory ICSharpCode.SharpZipLib.Zip.ZipFile::updateEntryFactory_
	RuntimeObject* ___updateEntryFactory__16;

public:
	inline static int32_t get_offset_of_KeysRequired_0() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___KeysRequired_0)); }
	inline KeysRequiredEventHandler_t01BA6F773E29C468C7FD85BE6B93EE5043A521D9 * get_KeysRequired_0() const { return ___KeysRequired_0; }
	inline KeysRequiredEventHandler_t01BA6F773E29C468C7FD85BE6B93EE5043A521D9 ** get_address_of_KeysRequired_0() { return &___KeysRequired_0; }
	inline void set_KeysRequired_0(KeysRequiredEventHandler_t01BA6F773E29C468C7FD85BE6B93EE5043A521D9 * value)
	{
		___KeysRequired_0 = value;
		Il2CppCodeGenWriteBarrier((&___KeysRequired_0), value);
	}

	inline static int32_t get_offset_of_isDisposed__1() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___isDisposed__1)); }
	inline bool get_isDisposed__1() const { return ___isDisposed__1; }
	inline bool* get_address_of_isDisposed__1() { return &___isDisposed__1; }
	inline void set_isDisposed__1(bool value)
	{
		___isDisposed__1 = value;
	}

	inline static int32_t get_offset_of_name__2() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___name__2)); }
	inline String_t* get_name__2() const { return ___name__2; }
	inline String_t** get_address_of_name__2() { return &___name__2; }
	inline void set_name__2(String_t* value)
	{
		___name__2 = value;
		Il2CppCodeGenWriteBarrier((&___name__2), value);
	}

	inline static int32_t get_offset_of_comment__3() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___comment__3)); }
	inline String_t* get_comment__3() const { return ___comment__3; }
	inline String_t** get_address_of_comment__3() { return &___comment__3; }
	inline void set_comment__3(String_t* value)
	{
		___comment__3 = value;
		Il2CppCodeGenWriteBarrier((&___comment__3), value);
	}

	inline static int32_t get_offset_of_rawPassword__4() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___rawPassword__4)); }
	inline String_t* get_rawPassword__4() const { return ___rawPassword__4; }
	inline String_t** get_address_of_rawPassword__4() { return &___rawPassword__4; }
	inline void set_rawPassword__4(String_t* value)
	{
		___rawPassword__4 = value;
		Il2CppCodeGenWriteBarrier((&___rawPassword__4), value);
	}

	inline static int32_t get_offset_of_baseStream__5() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___baseStream__5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_baseStream__5() const { return ___baseStream__5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_baseStream__5() { return &___baseStream__5; }
	inline void set_baseStream__5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___baseStream__5 = value;
		Il2CppCodeGenWriteBarrier((&___baseStream__5), value);
	}

	inline static int32_t get_offset_of_isStreamOwner_6() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___isStreamOwner_6)); }
	inline bool get_isStreamOwner_6() const { return ___isStreamOwner_6; }
	inline bool* get_address_of_isStreamOwner_6() { return &___isStreamOwner_6; }
	inline void set_isStreamOwner_6(bool value)
	{
		___isStreamOwner_6 = value;
	}

	inline static int32_t get_offset_of_offsetOfFirstEntry_7() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___offsetOfFirstEntry_7)); }
	inline int64_t get_offsetOfFirstEntry_7() const { return ___offsetOfFirstEntry_7; }
	inline int64_t* get_address_of_offsetOfFirstEntry_7() { return &___offsetOfFirstEntry_7; }
	inline void set_offsetOfFirstEntry_7(int64_t value)
	{
		___offsetOfFirstEntry_7 = value;
	}

	inline static int32_t get_offset_of_entries__8() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___entries__8)); }
	inline ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917* get_entries__8() const { return ___entries__8; }
	inline ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917** get_address_of_entries__8() { return &___entries__8; }
	inline void set_entries__8(ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917* value)
	{
		___entries__8 = value;
		Il2CppCodeGenWriteBarrier((&___entries__8), value);
	}

	inline static int32_t get_offset_of_key_9() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___key_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_key_9() const { return ___key_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_key_9() { return &___key_9; }
	inline void set_key_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___key_9 = value;
		Il2CppCodeGenWriteBarrier((&___key_9), value);
	}

	inline static int32_t get_offset_of_useZip64__10() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___useZip64__10)); }
	inline int32_t get_useZip64__10() const { return ___useZip64__10; }
	inline int32_t* get_address_of_useZip64__10() { return &___useZip64__10; }
	inline void set_useZip64__10(int32_t value)
	{
		___useZip64__10 = value;
	}

	inline static int32_t get_offset_of_updates__11() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___updates__11)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_updates__11() const { return ___updates__11; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_updates__11() { return &___updates__11; }
	inline void set_updates__11(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___updates__11 = value;
		Il2CppCodeGenWriteBarrier((&___updates__11), value);
	}

	inline static int32_t get_offset_of_updateIndex__12() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___updateIndex__12)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_updateIndex__12() const { return ___updateIndex__12; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_updateIndex__12() { return &___updateIndex__12; }
	inline void set_updateIndex__12(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___updateIndex__12 = value;
		Il2CppCodeGenWriteBarrier((&___updateIndex__12), value);
	}

	inline static int32_t get_offset_of_archiveStorage__13() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___archiveStorage__13)); }
	inline RuntimeObject* get_archiveStorage__13() const { return ___archiveStorage__13; }
	inline RuntimeObject** get_address_of_archiveStorage__13() { return &___archiveStorage__13; }
	inline void set_archiveStorage__13(RuntimeObject* value)
	{
		___archiveStorage__13 = value;
		Il2CppCodeGenWriteBarrier((&___archiveStorage__13), value);
	}

	inline static int32_t get_offset_of_updateDataSource__14() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___updateDataSource__14)); }
	inline RuntimeObject* get_updateDataSource__14() const { return ___updateDataSource__14; }
	inline RuntimeObject** get_address_of_updateDataSource__14() { return &___updateDataSource__14; }
	inline void set_updateDataSource__14(RuntimeObject* value)
	{
		___updateDataSource__14 = value;
		Il2CppCodeGenWriteBarrier((&___updateDataSource__14), value);
	}

	inline static int32_t get_offset_of_bufferSize__15() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___bufferSize__15)); }
	inline int32_t get_bufferSize__15() const { return ___bufferSize__15; }
	inline int32_t* get_address_of_bufferSize__15() { return &___bufferSize__15; }
	inline void set_bufferSize__15(int32_t value)
	{
		___bufferSize__15 = value;
	}

	inline static int32_t get_offset_of_updateEntryFactory__16() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___updateEntryFactory__16)); }
	inline RuntimeObject* get_updateEntryFactory__16() const { return ___updateEntryFactory__16; }
	inline RuntimeObject** get_address_of_updateEntryFactory__16() { return &___updateEntryFactory__16; }
	inline void set_updateEntryFactory__16(RuntimeObject* value)
	{
		___updateEntryFactory__16 = value;
		Il2CppCodeGenWriteBarrier((&___updateEntryFactory__16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPFILE_TEFDC6C532094F9A5F6185F4E69E340602984E28F_H
#ifndef XATTRIBUTEVALUEPROPERTYDESCRIPTOR_T15FD64FDD0562E6EBD938EBB49D6BDB4188F5D45_H
#define XATTRIBUTEVALUEPROPERTYDESCRIPTOR_T15FD64FDD0562E6EBD938EBB49D6BDB4188F5D45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Linq.ComponentModel.XAttributeValuePropertyDescriptor
struct  XAttributeValuePropertyDescriptor_t15FD64FDD0562E6EBD938EBB49D6BDB4188F5D45  : public XPropertyDescriptor_2_tB243420F73078FF83B4901B34B4FEFC2F260686E
{
public:
	// System.Xml.Linq.XAttribute MS.Internal.Xml.Linq.ComponentModel.XAttributeValuePropertyDescriptor::attribute
	XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 * ___attribute_17;

public:
	inline static int32_t get_offset_of_attribute_17() { return static_cast<int32_t>(offsetof(XAttributeValuePropertyDescriptor_t15FD64FDD0562E6EBD938EBB49D6BDB4188F5D45, ___attribute_17)); }
	inline XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 * get_attribute_17() const { return ___attribute_17; }
	inline XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 ** get_address_of_attribute_17() { return &___attribute_17; }
	inline void set_attribute_17(XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 * value)
	{
		___attribute_17 = value;
		Il2CppCodeGenWriteBarrier((&___attribute_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XATTRIBUTEVALUEPROPERTYDESCRIPTOR_T15FD64FDD0562E6EBD938EBB49D6BDB4188F5D45_H
#ifndef XELEMENTATTRIBUTEPROPERTYDESCRIPTOR_TD36B88FA52DD02B3B6BAC5B90E098BFDD99B6781_H
#define XELEMENTATTRIBUTEPROPERTYDESCRIPTOR_TD36B88FA52DD02B3B6BAC5B90E098BFDD99B6781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Linq.ComponentModel.XElementAttributePropertyDescriptor
struct  XElementAttributePropertyDescriptor_tD36B88FA52DD02B3B6BAC5B90E098BFDD99B6781  : public XPropertyDescriptor_2_t08258A32886D9FF6C631A406809DBE40C0A7680B
{
public:
	// MS.Internal.Xml.Linq.ComponentModel.XDeferredSingleton`1<System.Xml.Linq.XAttribute> MS.Internal.Xml.Linq.ComponentModel.XElementAttributePropertyDescriptor::value
	XDeferredSingleton_1_t7C8C8735370E05490AD1578B4C1A58CE50766E84 * ___value_17;

public:
	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(XElementAttributePropertyDescriptor_tD36B88FA52DD02B3B6BAC5B90E098BFDD99B6781, ___value_17)); }
	inline XDeferredSingleton_1_t7C8C8735370E05490AD1578B4C1A58CE50766E84 * get_value_17() const { return ___value_17; }
	inline XDeferredSingleton_1_t7C8C8735370E05490AD1578B4C1A58CE50766E84 ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(XDeferredSingleton_1_t7C8C8735370E05490AD1578B4C1A58CE50766E84 * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XELEMENTATTRIBUTEPROPERTYDESCRIPTOR_TD36B88FA52DD02B3B6BAC5B90E098BFDD99B6781_H
#ifndef XELEMENTDESCENDANTSPROPERTYDESCRIPTOR_T6FD271B29477DECB6D1753E7EAB9938BCCEEFD67_H
#define XELEMENTDESCENDANTSPROPERTYDESCRIPTOR_T6FD271B29477DECB6D1753E7EAB9938BCCEEFD67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Linq.ComponentModel.XElementDescendantsPropertyDescriptor
struct  XElementDescendantsPropertyDescriptor_t6FD271B29477DECB6D1753E7EAB9938BCCEEFD67  : public XPropertyDescriptor_2_t197028EA1CE689F52D9E0368C60D4DF321C65CCE
{
public:
	// MS.Internal.Xml.Linq.ComponentModel.XDeferredAxis`1<System.Xml.Linq.XElement> MS.Internal.Xml.Linq.ComponentModel.XElementDescendantsPropertyDescriptor::value
	XDeferredAxis_1_t09F31BE4F91C49ACD687B068AA30BE5949D83E3E * ___value_17;

public:
	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(XElementDescendantsPropertyDescriptor_t6FD271B29477DECB6D1753E7EAB9938BCCEEFD67, ___value_17)); }
	inline XDeferredAxis_1_t09F31BE4F91C49ACD687B068AA30BE5949D83E3E * get_value_17() const { return ___value_17; }
	inline XDeferredAxis_1_t09F31BE4F91C49ACD687B068AA30BE5949D83E3E ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(XDeferredAxis_1_t09F31BE4F91C49ACD687B068AA30BE5949D83E3E * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XELEMENTDESCENDANTSPROPERTYDESCRIPTOR_T6FD271B29477DECB6D1753E7EAB9938BCCEEFD67_H
#ifndef XELEMENTELEMENTPROPERTYDESCRIPTOR_T70E40E5AC03112F32900811619B7FA09B354429C_H
#define XELEMENTELEMENTPROPERTYDESCRIPTOR_T70E40E5AC03112F32900811619B7FA09B354429C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Linq.ComponentModel.XElementElementPropertyDescriptor
struct  XElementElementPropertyDescriptor_t70E40E5AC03112F32900811619B7FA09B354429C  : public XPropertyDescriptor_2_t08258A32886D9FF6C631A406809DBE40C0A7680B
{
public:
	// MS.Internal.Xml.Linq.ComponentModel.XDeferredSingleton`1<System.Xml.Linq.XElement> MS.Internal.Xml.Linq.ComponentModel.XElementElementPropertyDescriptor::value
	XDeferredSingleton_1_tC860FD4576C6582D4FE5320A114A68E992162504 * ___value_17;

public:
	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(XElementElementPropertyDescriptor_t70E40E5AC03112F32900811619B7FA09B354429C, ___value_17)); }
	inline XDeferredSingleton_1_tC860FD4576C6582D4FE5320A114A68E992162504 * get_value_17() const { return ___value_17; }
	inline XDeferredSingleton_1_tC860FD4576C6582D4FE5320A114A68E992162504 ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(XDeferredSingleton_1_tC860FD4576C6582D4FE5320A114A68E992162504 * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XELEMENTELEMENTPROPERTYDESCRIPTOR_T70E40E5AC03112F32900811619B7FA09B354429C_H
#ifndef XELEMENTELEMENTSPROPERTYDESCRIPTOR_TDC9447874F6F704F58D0E2B9DD63A4B0479D0AF0_H
#define XELEMENTELEMENTSPROPERTYDESCRIPTOR_TDC9447874F6F704F58D0E2B9DD63A4B0479D0AF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Linq.ComponentModel.XElementElementsPropertyDescriptor
struct  XElementElementsPropertyDescriptor_tDC9447874F6F704F58D0E2B9DD63A4B0479D0AF0  : public XPropertyDescriptor_2_t197028EA1CE689F52D9E0368C60D4DF321C65CCE
{
public:
	// MS.Internal.Xml.Linq.ComponentModel.XDeferredAxis`1<System.Xml.Linq.XElement> MS.Internal.Xml.Linq.ComponentModel.XElementElementsPropertyDescriptor::value
	XDeferredAxis_1_t09F31BE4F91C49ACD687B068AA30BE5949D83E3E * ___value_17;

public:
	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(XElementElementsPropertyDescriptor_tDC9447874F6F704F58D0E2B9DD63A4B0479D0AF0, ___value_17)); }
	inline XDeferredAxis_1_t09F31BE4F91C49ACD687B068AA30BE5949D83E3E * get_value_17() const { return ___value_17; }
	inline XDeferredAxis_1_t09F31BE4F91C49ACD687B068AA30BE5949D83E3E ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(XDeferredAxis_1_t09F31BE4F91C49ACD687B068AA30BE5949D83E3E * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XELEMENTELEMENTSPROPERTYDESCRIPTOR_TDC9447874F6F704F58D0E2B9DD63A4B0479D0AF0_H
#ifndef XELEMENTVALUEPROPERTYDESCRIPTOR_T0A248B7DFC84B57E056F1252B056B38C4F552DA6_H
#define XELEMENTVALUEPROPERTYDESCRIPTOR_T0A248B7DFC84B57E056F1252B056B38C4F552DA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Linq.ComponentModel.XElementValuePropertyDescriptor
struct  XElementValuePropertyDescriptor_t0A248B7DFC84B57E056F1252B056B38C4F552DA6  : public XPropertyDescriptor_2_t686B10FB0E5822D980C88672E12AE9C310DBDBEF
{
public:
	// System.Xml.Linq.XElement MS.Internal.Xml.Linq.ComponentModel.XElementValuePropertyDescriptor::element
	XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * ___element_17;

public:
	inline static int32_t get_offset_of_element_17() { return static_cast<int32_t>(offsetof(XElementValuePropertyDescriptor_t0A248B7DFC84B57E056F1252B056B38C4F552DA6, ___element_17)); }
	inline XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * get_element_17() const { return ___element_17; }
	inline XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 ** get_address_of_element_17() { return &___element_17; }
	inline void set_element_17(XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * value)
	{
		___element_17 = value;
		Il2CppCodeGenWriteBarrier((&___element_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XELEMENTVALUEPROPERTYDESCRIPTOR_T0A248B7DFC84B57E056F1252B056B38C4F552DA6_H
#ifndef XELEMENTXMLPROPERTYDESCRIPTOR_T9A1BD1AFD52CF5A598D2BBDA004071CDB76437D7_H
#define XELEMENTXMLPROPERTYDESCRIPTOR_T9A1BD1AFD52CF5A598D2BBDA004071CDB76437D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Linq.ComponentModel.XElementXmlPropertyDescriptor
struct  XElementXmlPropertyDescriptor_t9A1BD1AFD52CF5A598D2BBDA004071CDB76437D7  : public XPropertyDescriptor_2_t686B10FB0E5822D980C88672E12AE9C310DBDBEF
{
public:
	// System.Xml.Linq.XElement MS.Internal.Xml.Linq.ComponentModel.XElementXmlPropertyDescriptor::element
	XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * ___element_17;

public:
	inline static int32_t get_offset_of_element_17() { return static_cast<int32_t>(offsetof(XElementXmlPropertyDescriptor_t9A1BD1AFD52CF5A598D2BBDA004071CDB76437D7, ___element_17)); }
	inline XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * get_element_17() const { return ___element_17; }
	inline XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 ** get_address_of_element_17() { return &___element_17; }
	inline void set_element_17(XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95 * value)
	{
		___element_17 = value;
		Il2CppCodeGenWriteBarrier((&___element_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XELEMENTXMLPROPERTYDESCRIPTOR_T9A1BD1AFD52CF5A598D2BBDA004071CDB76437D7_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef CRYPTOSTREAM_T02F19D004439DD03FD7FA81169B58FC3AA05C9FC_H
#define CRYPTOSTREAM_T02F19D004439DD03FD7FA81169B58FC3AA05C9FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CryptoStream
struct  CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream System.Security.Cryptography.CryptoStream::_stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____stream_4;
	// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.CryptoStream::_Transform
	RuntimeObject* ____Transform_5;
	// System.Byte[] System.Security.Cryptography.CryptoStream::_InputBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____InputBuffer_6;
	// System.Int32 System.Security.Cryptography.CryptoStream::_InputBufferIndex
	int32_t ____InputBufferIndex_7;
	// System.Int32 System.Security.Cryptography.CryptoStream::_InputBlockSize
	int32_t ____InputBlockSize_8;
	// System.Byte[] System.Security.Cryptography.CryptoStream::_OutputBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____OutputBuffer_9;
	// System.Int32 System.Security.Cryptography.CryptoStream::_OutputBufferIndex
	int32_t ____OutputBufferIndex_10;
	// System.Int32 System.Security.Cryptography.CryptoStream::_OutputBlockSize
	int32_t ____OutputBlockSize_11;
	// System.Security.Cryptography.CryptoStreamMode System.Security.Cryptography.CryptoStream::_transformMode
	int32_t ____transformMode_12;
	// System.Boolean System.Security.Cryptography.CryptoStream::_canRead
	bool ____canRead_13;
	// System.Boolean System.Security.Cryptography.CryptoStream::_canWrite
	bool ____canWrite_14;
	// System.Boolean System.Security.Cryptography.CryptoStream::_finalBlockTransformed
	bool ____finalBlockTransformed_15;

public:
	inline static int32_t get_offset_of__stream_4() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____stream_4)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__stream_4() const { return ____stream_4; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__stream_4() { return &____stream_4; }
	inline void set__stream_4(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____stream_4 = value;
		Il2CppCodeGenWriteBarrier((&____stream_4), value);
	}

	inline static int32_t get_offset_of__Transform_5() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____Transform_5)); }
	inline RuntimeObject* get__Transform_5() const { return ____Transform_5; }
	inline RuntimeObject** get_address_of__Transform_5() { return &____Transform_5; }
	inline void set__Transform_5(RuntimeObject* value)
	{
		____Transform_5 = value;
		Il2CppCodeGenWriteBarrier((&____Transform_5), value);
	}

	inline static int32_t get_offset_of__InputBuffer_6() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____InputBuffer_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__InputBuffer_6() const { return ____InputBuffer_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__InputBuffer_6() { return &____InputBuffer_6; }
	inline void set__InputBuffer_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____InputBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((&____InputBuffer_6), value);
	}

	inline static int32_t get_offset_of__InputBufferIndex_7() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____InputBufferIndex_7)); }
	inline int32_t get__InputBufferIndex_7() const { return ____InputBufferIndex_7; }
	inline int32_t* get_address_of__InputBufferIndex_7() { return &____InputBufferIndex_7; }
	inline void set__InputBufferIndex_7(int32_t value)
	{
		____InputBufferIndex_7 = value;
	}

	inline static int32_t get_offset_of__InputBlockSize_8() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____InputBlockSize_8)); }
	inline int32_t get__InputBlockSize_8() const { return ____InputBlockSize_8; }
	inline int32_t* get_address_of__InputBlockSize_8() { return &____InputBlockSize_8; }
	inline void set__InputBlockSize_8(int32_t value)
	{
		____InputBlockSize_8 = value;
	}

	inline static int32_t get_offset_of__OutputBuffer_9() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____OutputBuffer_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__OutputBuffer_9() const { return ____OutputBuffer_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__OutputBuffer_9() { return &____OutputBuffer_9; }
	inline void set__OutputBuffer_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____OutputBuffer_9 = value;
		Il2CppCodeGenWriteBarrier((&____OutputBuffer_9), value);
	}

	inline static int32_t get_offset_of__OutputBufferIndex_10() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____OutputBufferIndex_10)); }
	inline int32_t get__OutputBufferIndex_10() const { return ____OutputBufferIndex_10; }
	inline int32_t* get_address_of__OutputBufferIndex_10() { return &____OutputBufferIndex_10; }
	inline void set__OutputBufferIndex_10(int32_t value)
	{
		____OutputBufferIndex_10 = value;
	}

	inline static int32_t get_offset_of__OutputBlockSize_11() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____OutputBlockSize_11)); }
	inline int32_t get__OutputBlockSize_11() const { return ____OutputBlockSize_11; }
	inline int32_t* get_address_of__OutputBlockSize_11() { return &____OutputBlockSize_11; }
	inline void set__OutputBlockSize_11(int32_t value)
	{
		____OutputBlockSize_11 = value;
	}

	inline static int32_t get_offset_of__transformMode_12() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____transformMode_12)); }
	inline int32_t get__transformMode_12() const { return ____transformMode_12; }
	inline int32_t* get_address_of__transformMode_12() { return &____transformMode_12; }
	inline void set__transformMode_12(int32_t value)
	{
		____transformMode_12 = value;
	}

	inline static int32_t get_offset_of__canRead_13() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____canRead_13)); }
	inline bool get__canRead_13() const { return ____canRead_13; }
	inline bool* get_address_of__canRead_13() { return &____canRead_13; }
	inline void set__canRead_13(bool value)
	{
		____canRead_13 = value;
	}

	inline static int32_t get_offset_of__canWrite_14() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____canWrite_14)); }
	inline bool get__canWrite_14() const { return ____canWrite_14; }
	inline bool* get_address_of__canWrite_14() { return &____canWrite_14; }
	inline void set__canWrite_14(bool value)
	{
		____canWrite_14 = value;
	}

	inline static int32_t get_offset_of__finalBlockTransformed_15() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____finalBlockTransformed_15)); }
	inline bool get__finalBlockTransformed_15() const { return ____finalBlockTransformed_15; }
	inline bool* get_address_of__finalBlockTransformed_15() { return &____finalBlockTransformed_15; }
	inline void set__finalBlockTransformed_15(bool value)
	{
		____finalBlockTransformed_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOSTREAM_T02F19D004439DD03FD7FA81169B58FC3AA05C9FC_H
#ifndef SYMMETRICALGORITHM_T0A2EC7E7AD8B8976832B4F0AC432B691F862E789_H
#define SYMMETRICALGORITHM_T0A2EC7E7AD8B8976832B4F0AC432B691F862E789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SymmetricAlgorithm
struct  SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::BlockSizeValue
	int32_t ___BlockSizeValue_0;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::FeedbackSizeValue
	int32_t ___FeedbackSizeValue_1;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::IVValue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IVValue_2;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::KeyValue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___KeyValue_3;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalBlockSizesValue
	KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* ___LegalBlockSizesValue_4;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* ___LegalKeySizesValue_5;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_6;
	// System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::ModeValue
	int32_t ___ModeValue_7;
	// System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::PaddingValue
	int32_t ___PaddingValue_8;

public:
	inline static int32_t get_offset_of_BlockSizeValue_0() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___BlockSizeValue_0)); }
	inline int32_t get_BlockSizeValue_0() const { return ___BlockSizeValue_0; }
	inline int32_t* get_address_of_BlockSizeValue_0() { return &___BlockSizeValue_0; }
	inline void set_BlockSizeValue_0(int32_t value)
	{
		___BlockSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_FeedbackSizeValue_1() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___FeedbackSizeValue_1)); }
	inline int32_t get_FeedbackSizeValue_1() const { return ___FeedbackSizeValue_1; }
	inline int32_t* get_address_of_FeedbackSizeValue_1() { return &___FeedbackSizeValue_1; }
	inline void set_FeedbackSizeValue_1(int32_t value)
	{
		___FeedbackSizeValue_1 = value;
	}

	inline static int32_t get_offset_of_IVValue_2() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___IVValue_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IVValue_2() const { return ___IVValue_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IVValue_2() { return &___IVValue_2; }
	inline void set_IVValue_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IVValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___IVValue_2), value);
	}

	inline static int32_t get_offset_of_KeyValue_3() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___KeyValue_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_KeyValue_3() const { return ___KeyValue_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_KeyValue_3() { return &___KeyValue_3; }
	inline void set_KeyValue_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___KeyValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_3), value);
	}

	inline static int32_t get_offset_of_LegalBlockSizesValue_4() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___LegalBlockSizesValue_4)); }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* get_LegalBlockSizesValue_4() const { return ___LegalBlockSizesValue_4; }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E** get_address_of_LegalBlockSizesValue_4() { return &___LegalBlockSizesValue_4; }
	inline void set_LegalBlockSizesValue_4(KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* value)
	{
		___LegalBlockSizesValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___LegalBlockSizesValue_4), value);
	}

	inline static int32_t get_offset_of_LegalKeySizesValue_5() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___LegalKeySizesValue_5)); }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* get_LegalKeySizesValue_5() const { return ___LegalKeySizesValue_5; }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E** get_address_of_LegalKeySizesValue_5() { return &___LegalKeySizesValue_5; }
	inline void set_LegalKeySizesValue_5(KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* value)
	{
		___LegalKeySizesValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___LegalKeySizesValue_5), value);
	}

	inline static int32_t get_offset_of_KeySizeValue_6() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___KeySizeValue_6)); }
	inline int32_t get_KeySizeValue_6() const { return ___KeySizeValue_6; }
	inline int32_t* get_address_of_KeySizeValue_6() { return &___KeySizeValue_6; }
	inline void set_KeySizeValue_6(int32_t value)
	{
		___KeySizeValue_6 = value;
	}

	inline static int32_t get_offset_of_ModeValue_7() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___ModeValue_7)); }
	inline int32_t get_ModeValue_7() const { return ___ModeValue_7; }
	inline int32_t* get_address_of_ModeValue_7() { return &___ModeValue_7; }
	inline void set_ModeValue_7(int32_t value)
	{
		___ModeValue_7 = value;
	}

	inline static int32_t get_offset_of_PaddingValue_8() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___PaddingValue_8)); }
	inline int32_t get_PaddingValue_8() const { return ___PaddingValue_8; }
	inline int32_t* get_address_of_PaddingValue_8() { return &___PaddingValue_8; }
	inline void set_PaddingValue_8(int32_t value)
	{
		___PaddingValue_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMMETRICALGORITHM_T0A2EC7E7AD8B8976832B4F0AC432B691F862E789_H
#ifndef XDOCUMENT_TDB43DAB669CB833AF350FA40A38F709A9D674B0F_H
#define XDOCUMENT_TDB43DAB669CB833AF350FA40A38F709A9D674B0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XDocument
struct  XDocument_tDB43DAB669CB833AF350FA40A38F709A9D674B0F  : public XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599
{
public:
	// System.Xml.Linq.XDeclaration System.Xml.Linq.XDocument::declaration
	XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 * ___declaration_4;

public:
	inline static int32_t get_offset_of_declaration_4() { return static_cast<int32_t>(offsetof(XDocument_tDB43DAB669CB833AF350FA40A38F709A9D674B0F, ___declaration_4)); }
	inline XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 * get_declaration_4() const { return ___declaration_4; }
	inline XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 ** get_address_of_declaration_4() { return &___declaration_4; }
	inline void set_declaration_4(XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 * value)
	{
		___declaration_4 = value;
		Il2CppCodeGenWriteBarrier((&___declaration_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDOCUMENT_TDB43DAB669CB833AF350FA40A38F709A9D674B0F_H
#ifndef XELEMENT_T19F18C253C1E4E22F010C470A5181AB7BD0B5E95_H
#define XELEMENT_T19F18C253C1E4E22F010C470A5181AB7BD0B5E95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XElement
struct  XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95  : public XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599
{
public:
	// System.Xml.Linq.XName System.Xml.Linq.XElement::name
	XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * ___name_5;
	// System.Xml.Linq.XAttribute System.Xml.Linq.XElement::lastAttr
	XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 * ___lastAttr_6;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95, ___name_5)); }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * get_name_5() const { return ___name_5; }
	inline XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A ** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A * value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_lastAttr_6() { return static_cast<int32_t>(offsetof(XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95, ___lastAttr_6)); }
	inline XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 * get_lastAttr_6() const { return ___lastAttr_6; }
	inline XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 ** get_address_of_lastAttr_6() { return &___lastAttr_6; }
	inline void set_lastAttr_6(XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015 * value)
	{
		___lastAttr_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastAttr_6), value);
	}
};

struct XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95_StaticFields
{
public:
	// System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement> System.Xml.Linq.XElement::emptySequence
	RuntimeObject* ___emptySequence_4;

public:
	inline static int32_t get_offset_of_emptySequence_4() { return static_cast<int32_t>(offsetof(XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95_StaticFields, ___emptySequence_4)); }
	inline RuntimeObject* get_emptySequence_4() const { return ___emptySequence_4; }
	inline RuntimeObject** get_address_of_emptySequence_4() { return &___emptySequence_4; }
	inline void set_emptySequence_4(RuntimeObject* value)
	{
		___emptySequence_4 = value;
		Il2CppCodeGenWriteBarrier((&___emptySequence_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XELEMENT_T19F18C253C1E4E22F010C470A5181AB7BD0B5E95_H
#ifndef PKZIPCLASSIC_TDAD4BF22079888AFE3944F718304C54DD2EEDF7E_H
#define PKZIPCLASSIC_TDAD4BF22079888AFE3944F718304C54DD2EEDF7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.PkzipClassic
struct  PkzipClassic_tDAD4BF22079888AFE3944F718304C54DD2EEDF7E  : public SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKZIPCLASSIC_TDAD4BF22079888AFE3944F718304C54DD2EEDF7E_H
#ifndef ZIPAESSTREAM_TCD59113BEAB7E3B63907D92AD46F7503A03B2CB2_H
#define ZIPAESSTREAM_TCD59113BEAB7E3B63907D92AD46F7503A03B2CB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.ZipAESStream
struct  ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2  : public CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC
{
public:
	// System.IO.Stream ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____stream_16;
	// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_transform
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195 * ____transform_17;
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_slideBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____slideBuffer_18;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_slideBufStartPos
	int32_t ____slideBufStartPos_19;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_slideBufFreePos
	int32_t ____slideBufFreePos_20;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_blockAndAuth
	int32_t ____blockAndAuth_21;

public:
	inline static int32_t get_offset_of__stream_16() { return static_cast<int32_t>(offsetof(ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2, ____stream_16)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__stream_16() const { return ____stream_16; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__stream_16() { return &____stream_16; }
	inline void set__stream_16(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____stream_16 = value;
		Il2CppCodeGenWriteBarrier((&____stream_16), value);
	}

	inline static int32_t get_offset_of__transform_17() { return static_cast<int32_t>(offsetof(ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2, ____transform_17)); }
	inline ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195 * get__transform_17() const { return ____transform_17; }
	inline ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195 ** get_address_of__transform_17() { return &____transform_17; }
	inline void set__transform_17(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195 * value)
	{
		____transform_17 = value;
		Il2CppCodeGenWriteBarrier((&____transform_17), value);
	}

	inline static int32_t get_offset_of__slideBuffer_18() { return static_cast<int32_t>(offsetof(ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2, ____slideBuffer_18)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__slideBuffer_18() const { return ____slideBuffer_18; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__slideBuffer_18() { return &____slideBuffer_18; }
	inline void set__slideBuffer_18(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____slideBuffer_18 = value;
		Il2CppCodeGenWriteBarrier((&____slideBuffer_18), value);
	}

	inline static int32_t get_offset_of__slideBufStartPos_19() { return static_cast<int32_t>(offsetof(ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2, ____slideBufStartPos_19)); }
	inline int32_t get__slideBufStartPos_19() const { return ____slideBufStartPos_19; }
	inline int32_t* get_address_of__slideBufStartPos_19() { return &____slideBufStartPos_19; }
	inline void set__slideBufStartPos_19(int32_t value)
	{
		____slideBufStartPos_19 = value;
	}

	inline static int32_t get_offset_of__slideBufFreePos_20() { return static_cast<int32_t>(offsetof(ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2, ____slideBufFreePos_20)); }
	inline int32_t get__slideBufFreePos_20() const { return ____slideBufFreePos_20; }
	inline int32_t* get_address_of__slideBufFreePos_20() { return &____slideBufFreePos_20; }
	inline void set__slideBufFreePos_20(int32_t value)
	{
		____slideBufFreePos_20 = value;
	}

	inline static int32_t get_offset_of__blockAndAuth_21() { return static_cast<int32_t>(offsetof(ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2, ____blockAndAuth_21)); }
	inline int32_t get__blockAndAuth_21() const { return ____blockAndAuth_21; }
	inline int32_t* get_address_of__blockAndAuth_21() { return &____blockAndAuth_21; }
	inline void set__blockAndAuth_21(int32_t value)
	{
		____blockAndAuth_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPAESSTREAM_TCD59113BEAB7E3B63907D92AD46F7503A03B2CB2_H
#ifndef INVALIDHEADEREXCEPTION_T8EEEB622B5F672B4B04882668C92C150A2F9BB67_H
#define INVALIDHEADEREXCEPTION_T8EEEB622B5F672B4B04882668C92C150A2F9BB67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.InvalidHeaderException
struct  InvalidHeaderException_t8EEEB622B5F672B4B04882668C92C150A2F9BB67  : public TarException_t7ADB01E2D320C153961C7EDAF801B73E3A9B1D66
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDHEADEREXCEPTION_T8EEEB622B5F672B4B04882668C92C150A2F9BB67_H
#ifndef PROGRESSMESSAGEHANDLER_TCB79C5F0AE44A5C4FA7F7D8363992A25DDEDC4EF_H
#define PROGRESSMESSAGEHANDLER_TCB79C5F0AE44A5C4FA7F7D8363992A25DDEDC4EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.ProgressMessageHandler
struct  ProgressMessageHandler_tCB79C5F0AE44A5C4FA7F7D8363992A25DDEDC4EF  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSMESSAGEHANDLER_TCB79C5F0AE44A5C4FA7F7D8363992A25DDEDC4EF_H
#ifndef KEYSREQUIREDEVENTHANDLER_T01BA6F773E29C468C7FD85BE6B93EE5043A521D9_H
#define KEYSREQUIREDEVENTHANDLER_T01BA6F773E29C468C7FD85BE6B93EE5043A521D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipFile/KeysRequiredEventHandler
struct  KeysRequiredEventHandler_t01BA6F773E29C468C7FD85BE6B93EE5043A521D9  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYSREQUIREDEVENTHANDLER_T01BA6F773E29C468C7FD85BE6B93EE5043A521D9_H
#ifndef PKZIPCLASSICMANAGED_TE9CA988C413530D0AE504C4B2E14A9543E505DA4_H
#define PKZIPCLASSICMANAGED_TE9CA988C413530D0AE504C4B2E14A9543E505DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged
struct  PkzipClassicManaged_tE9CA988C413530D0AE504C4B2E14A9543E505DA4  : public PkzipClassic_tDAD4BF22079888AFE3944F718304C54DD2EEDF7E
{
public:
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::key_
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key__9;

public:
	inline static int32_t get_offset_of_key__9() { return static_cast<int32_t>(offsetof(PkzipClassicManaged_tE9CA988C413530D0AE504C4B2E14A9543E505DA4, ___key__9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_key__9() const { return ___key__9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_key__9() { return &___key__9; }
	inline void set_key__9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___key__9 = value;
		Il2CppCodeGenWriteBarrier((&___key__9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKZIPCLASSICMANAGED_TE9CA988C413530D0AE504C4B2E14A9543E505DA4_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4900 = { sizeof (U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4900[10] = 
{
	U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59::get_offset_of_U3CU3E1__state_0(),
	U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59::get_offset_of_U3CU3E2__current_1(),
	U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59::get_offset_of_self_3(),
	U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59::get_offset_of_U3CU3E3__self_4(),
	U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59::get_offset_of_U3CU3E4__this_5(),
	U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59::get_offset_of_name_6(),
	U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59::get_offset_of_U3CU3E3__name_7(),
	U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59::get_offset_of_U3CnU3E5__1_8(),
	U3CGetDescendantsU3Ed__39_tA691FA9408A12703768386BC37C6C9B973E8EF59::get_offset_of_U3CeU3E5__2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4901 = { sizeof (U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4901[7] = 
{
	U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927::get_offset_of_U3CU3E1__state_0(),
	U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927::get_offset_of_U3CU3E2__current_1(),
	U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927::get_offset_of_U3CU3E4__this_3(),
	U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927::get_offset_of_U3CnU3E5__1_4(),
	U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927::get_offset_of_name_5(),
	U3CGetElementsU3Ed__40_t2C252163A201D4084C359A465846CBCDB6AE5927::get_offset_of_U3CU3E3__name_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4902 = { sizeof (NamespaceCache_t0647B5D31D12BE681FD6B177432FE09BD092AD70)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4902[2] = 
{
	NamespaceCache_t0647B5D31D12BE681FD6B177432FE09BD092AD70::get_offset_of_ns_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NamespaceCache_t0647B5D31D12BE681FD6B177432FE09BD092AD70::get_offset_of_namespaceName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4903 = { sizeof (XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95), -1, sizeof(XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4903[3] = 
{
	XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95_StaticFields::get_offset_of_emptySequence_4(),
	XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95::get_offset_of_name_5(),
	XElement_t19F18C253C1E4E22F010C470A5181AB7BD0B5E95::get_offset_of_lastAttr_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4904 = { sizeof (U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4904[7] = 
{
	U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157::get_offset_of_U3CU3E1__state_0(),
	U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157::get_offset_of_U3CU3E2__current_1(),
	U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157::get_offset_of_U3CU3E4__this_3(),
	U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157::get_offset_of_U3CaU3E5__1_4(),
	U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157::get_offset_of_name_5(),
	U3CGetAttributesU3Ed__105_tC7FE5089390F02011E9B6F1AFD90DE16A5B78157::get_offset_of_U3CU3E3__name_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4905 = { sizeof (ElementWriter_t9469E888D0CE4D9B706DC16A3917D16984561552)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4905[2] = 
{
	ElementWriter_t9469E888D0CE4D9B706DC16A3917D16984561552::get_offset_of_writer_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementWriter_t9469E888D0CE4D9B706DC16A3917D16984561552::get_offset_of_resolver_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4906 = { sizeof (NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4906[3] = 
{
	NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D::get_offset_of_scope_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D::get_offset_of_declaration_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NamespaceResolver_t51606A2FFD4C06A168414A26FBD1541856EA049D::get_offset_of_rover_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4907 = { sizeof (NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4907[4] = 
{
	NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF::get_offset_of_prefix_0(),
	NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF::get_offset_of_ns_1(),
	NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF::get_offset_of_scope_2(),
	NamespaceDeclaration_t8CC10C4791174893FA9467DE2F8E41D1097236DF::get_offset_of_prev_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4908 = { sizeof (LoadOptions_t351EDF45A682830EA523299A126655C72CF76643)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4908[5] = 
{
	LoadOptions_t351EDF45A682830EA523299A126655C72CF76643::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4909 = { sizeof (SaveOptions_t3EA240C0E910B76C6D762B139C6E03F0496DD3BC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4909[4] = 
{
	SaveOptions_t3EA240C0E910B76C6D762B139C6E03F0496DD3BC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4910 = { sizeof (XDocument_tDB43DAB669CB833AF350FA40A38F709A9D674B0F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4910[1] = 
{
	XDocument_tDB43DAB669CB833AF350FA40A38F709A9D674B0F::get_offset_of_declaration_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4911 = { sizeof (XComment_t4A3C0B2424F442F0C1F76C33354476FFD90788AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4911[1] = 
{
	XComment_t4A3C0B2424F442F0C1F76C33354476FFD90788AD::get_offset_of_value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4912 = { sizeof (XProcessingInstruction_t9136E2BCE4B8BBE51093AAC9AEC92F05E577A213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4912[2] = 
{
	XProcessingInstruction_t9136E2BCE4B8BBE51093AAC9AEC92F05E577A213::get_offset_of_target_3(),
	XProcessingInstruction_t9136E2BCE4B8BBE51093AAC9AEC92F05E577A213::get_offset_of_data_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4913 = { sizeof (XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4913[3] = 
{
	XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512::get_offset_of_version_0(),
	XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512::get_offset_of_encoding_1(),
	XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512::get_offset_of_standalone_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4914 = { sizeof (XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4914[5] = 
{
	XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444::get_offset_of_name_3(),
	XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444::get_offset_of_publicId_4(),
	XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444::get_offset_of_systemId_5(),
	XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444::get_offset_of_internalSubset_6(),
	XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444::get_offset_of_dtdInfo_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4915 = { sizeof (XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4915[3] = 
{
	XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015::get_offset_of_next_2(),
	XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015::get_offset_of_name_3(),
	XAttribute_tE59FB05CB3017A75CD81C84F5E4FE613B8A8F015::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4916 = { sizeof (XStreamingElement_t6B73236C8904BD25EBD29C8A18941012D8DA6D45), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4916[2] = 
{
	XStreamingElement_t6B73236C8904BD25EBD29C8A18941012D8DA6D45::get_offset_of_name_0(),
	XStreamingElement_t6B73236C8904BD25EBD29C8A18941012D8DA6D45::get_offset_of_content_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4917 = { sizeof (StreamingElementWriter_t3A8F2AEFE076A10FB4D09D511F4D568A1E931205)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4917[4] = 
{
	StreamingElementWriter_t3A8F2AEFE076A10FB4D09D511F4D568A1E931205::get_offset_of_writer_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StreamingElementWriter_t3A8F2AEFE076A10FB4D09D511F4D568A1E931205::get_offset_of_element_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StreamingElementWriter_t3A8F2AEFE076A10FB4D09D511F4D568A1E931205::get_offset_of_attributes_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StreamingElementWriter_t3A8F2AEFE076A10FB4D09D511F4D568A1E931205::get_offset_of_resolver_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4918 = { sizeof (Res_tD7FC17E62C176207785D3F44C92954026A90B16B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4919 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4920 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4921 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4922 = { sizeof (XElementAttributePropertyDescriptor_tD36B88FA52DD02B3B6BAC5B90E098BFDD99B6781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4922[1] = 
{
	XElementAttributePropertyDescriptor_tD36B88FA52DD02B3B6BAC5B90E098BFDD99B6781::get_offset_of_value_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4923 = { sizeof (U3CU3Ec_tC4AD9CB09E765F9D3B76E4C1A9BA2D8DB47CEC7A), -1, sizeof(U3CU3Ec_tC4AD9CB09E765F9D3B76E4C1A9BA2D8DB47CEC7A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4923[2] = 
{
	U3CU3Ec_tC4AD9CB09E765F9D3B76E4C1A9BA2D8DB47CEC7A_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tC4AD9CB09E765F9D3B76E4C1A9BA2D8DB47CEC7A_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4924 = { sizeof (XElementDescendantsPropertyDescriptor_t6FD271B29477DECB6D1753E7EAB9938BCCEEFD67), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4924[1] = 
{
	XElementDescendantsPropertyDescriptor_t6FD271B29477DECB6D1753E7EAB9938BCCEEFD67::get_offset_of_value_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4925 = { sizeof (U3CU3Ec_t8F9160D463F532068C1A4675D8C1E1B8C4E008C3), -1, sizeof(U3CU3Ec_t8F9160D463F532068C1A4675D8C1E1B8C4E008C3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4925[2] = 
{
	U3CU3Ec_t8F9160D463F532068C1A4675D8C1E1B8C4E008C3_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t8F9160D463F532068C1A4675D8C1E1B8C4E008C3_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4926 = { sizeof (XElementElementPropertyDescriptor_t70E40E5AC03112F32900811619B7FA09B354429C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4926[1] = 
{
	XElementElementPropertyDescriptor_t70E40E5AC03112F32900811619B7FA09B354429C::get_offset_of_value_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4927 = { sizeof (U3CU3Ec_t4E705D19DC0E84DCB74017D6083C8FD98D9B0385), -1, sizeof(U3CU3Ec_t4E705D19DC0E84DCB74017D6083C8FD98D9B0385_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4927[2] = 
{
	U3CU3Ec_t4E705D19DC0E84DCB74017D6083C8FD98D9B0385_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4E705D19DC0E84DCB74017D6083C8FD98D9B0385_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4928 = { sizeof (XElementElementsPropertyDescriptor_tDC9447874F6F704F58D0E2B9DD63A4B0479D0AF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4928[1] = 
{
	XElementElementsPropertyDescriptor_tDC9447874F6F704F58D0E2B9DD63A4B0479D0AF0::get_offset_of_value_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4929 = { sizeof (U3CU3Ec_t9962754B7C58EB75F881DB6A6D958A857648D0A3), -1, sizeof(U3CU3Ec_t9962754B7C58EB75F881DB6A6D958A857648D0A3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4929[2] = 
{
	U3CU3Ec_t9962754B7C58EB75F881DB6A6D958A857648D0A3_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t9962754B7C58EB75F881DB6A6D958A857648D0A3_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4930 = { sizeof (XElementValuePropertyDescriptor_t0A248B7DFC84B57E056F1252B056B38C4F552DA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4930[1] = 
{
	XElementValuePropertyDescriptor_t0A248B7DFC84B57E056F1252B056B38C4F552DA6::get_offset_of_element_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4931 = { sizeof (XElementXmlPropertyDescriptor_t9A1BD1AFD52CF5A598D2BBDA004071CDB76437D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4931[1] = 
{
	XElementXmlPropertyDescriptor_t9A1BD1AFD52CF5A598D2BBDA004071CDB76437D7::get_offset_of_element_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4932 = { sizeof (XAttributeValuePropertyDescriptor_t15FD64FDD0562E6EBD938EBB49D6BDB4188F5D45), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4932[1] = 
{
	XAttributeValuePropertyDescriptor_t15FD64FDD0562E6EBD938EBB49D6BDB4188F5D45::get_offset_of_attribute_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4933 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4933[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4934 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4934[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4935 = { sizeof (U3CPrivateImplementationDetailsU3E_t07F5E93AED2CB4162F2DD7883F51F5E5FD89E88E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4936 = { sizeof (U3CModuleU3E_t93DCC9DD64AD0C36686B88FC177EB2212081BDB5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4937 = { sizeof (SubsystemRegistration_t0A22FECC46483ABBFFC039449407F73FF11F5A1A), -1, sizeof(SubsystemRegistration_t0A22FECC46483ABBFFC039449407F73FF11F5A1A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4937[1] = 
{
	SubsystemRegistration_t0A22FECC46483ABBFFC039449407F73FF11F5A1A_StaticFields::get_offset_of_k_SubsystemDescriptors_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4938 = { sizeof (U3CModuleU3E_tAA90CDF50306A16DF6C29043AB49497B8E0C9DA6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4939 = { sizeof (SharpZipBaseException_t822541D5F0EC9FCA3550F00DCF0883E987E5045F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4940 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4941 = { sizeof (Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4941[1] = 
{
	Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033::get_offset_of_checksum_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4942 = { sizeof (Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F), -1, sizeof(Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4942[2] = 
{
	Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F_StaticFields::get_offset_of_CrcTable_0(),
	Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F::get_offset_of_crc_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4943 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4944 = { sizeof (StreamUtils_tC8B2EC2CC286B4E36E2D78C3D96D64770DC3D6F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4945 = { sizeof (PkzipClassic_tDAD4BF22079888AFE3944F718304C54DD2EEDF7E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4946 = { sizeof (PkzipClassicCryptoBase_t82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4946[1] = 
{
	PkzipClassicCryptoBase_t82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B::get_offset_of_keys_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4947 = { sizeof (PkzipClassicEncryptCryptoTransform_t050C6231405F6F498F285DF8D3FCB2DCD6EB2EE0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4948 = { sizeof (PkzipClassicDecryptCryptoTransform_tBB3B4209A63C60CB64240640E0CBA21626ACA93E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4949 = { sizeof (PkzipClassicManaged_tE9CA988C413530D0AE504C4B2E14A9543E505DA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4949[1] = 
{
	PkzipClassicManaged_tE9CA988C413530D0AE504C4B2E14A9543E505DA4::get_offset_of_key__9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4950 = { sizeof (ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4950[6] = 
{
	ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2::get_offset_of__stream_16(),
	ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2::get_offset_of__transform_17(),
	ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2::get_offset_of__slideBuffer_18(),
	ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2::get_offset_of__slideBufStartPos_19(),
	ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2::get_offset_of__slideBufFreePos_20(),
	ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2::get_offset_of__blockAndAuth_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4951 = { sizeof (ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4951[9] = 
{
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__blockSize_0(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__encryptor_1(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__counterNonce_2(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__encryptBuffer_3(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__encrPos_4(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__pwdVerifier_5(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__hmacsha1_6(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__finalised_7(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__writeMode_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4952 = { sizeof (GZipException_t414765B7EF634182FC2C85E0AB1FA929E3A591C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4953 = { sizeof (InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4953[5] = 
{
	InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6::get_offset_of_inf_4(),
	InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6::get_offset_of_inputBuffer_5(),
	InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6::get_offset_of_baseInputStream_6(),
	InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6::get_offset_of_isClosed_7(),
	InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6::get_offset_of_isStreamOwner_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4954 = { sizeof (GZipInputStream_t56798E4A16A18A8EC793908E2E815DF63BF9289A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4954[2] = 
{
	GZipInputStream_t56798E4A16A18A8EC793908E2E815DF63BF9289A::get_offset_of_crc_9(),
	GZipInputStream_t56798E4A16A18A8EC793908E2E815DF63BF9289A::get_offset_of_readGZIPHeader_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4955 = { sizeof (TarException_t7ADB01E2D320C153961C7EDAF801B73E3A9B1D66), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4956 = { sizeof (InvalidHeaderException_t8EEEB622B5F672B4B04882668C92C150A2F9BB67), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4957 = { sizeof (ProgressMessageHandler_tCB79C5F0AE44A5C4FA7F7D8363992A25DDEDC4EF), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4958 = { sizeof (TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4958[8] = 
{
	TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726::get_offset_of_ProgressMessageEvent_0(),
	TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726::get_offset_of_keepOldFiles_1(),
	TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726::get_offset_of_asciiTranslate_2(),
	TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726::get_offset_of_userName_3(),
	TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726::get_offset_of_groupName_4(),
	TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726::get_offset_of_tarIn_5(),
	TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726::get_offset_of_tarOut_6(),
	TarArchive_t788C674DE76F953D2C2B6CC4DF838FCE0ED22726::get_offset_of_isDisposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4959 = { sizeof (TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4959[8] = 
{
	TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C::get_offset_of_inputStream_0(),
	TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C::get_offset_of_outputStream_1(),
	TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C::get_offset_of_recordBuffer_2(),
	TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C::get_offset_of_currentBlockIndex_3(),
	TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C::get_offset_of_currentRecordIndex_4(),
	TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C::get_offset_of_recordSize_5(),
	TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C::get_offset_of_blockFactor_6(),
	TarBuffer_t96EE7BB130E846A706DBC5D6049D057FC1A3701C::get_offset_of_isStreamOwner__7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4960 = { sizeof (TarEntry_tDE18EDBB29A64DBD744028FA5BAF5026519398BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4960[2] = 
{
	TarEntry_tDE18EDBB29A64DBD744028FA5BAF5026519398BE::get_offset_of_file_0(),
	TarEntry_tDE18EDBB29A64DBD744028FA5BAF5026519398BE::get_offset_of_header_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4961 = { sizeof (TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74), -1, sizeof(TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4961[22] = 
{
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74_StaticFields::get_offset_of_dateTime1970_0(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_name_1(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_mode_2(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_userId_3(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_groupId_4(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_size_5(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_modTime_6(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_checksum_7(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_isChecksumValid_8(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_typeFlag_9(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_linkName_10(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_magic_11(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_version_12(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_userName_13(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_groupName_14(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_devMajor_15(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74::get_offset_of_devMinor_16(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74_StaticFields::get_offset_of_groupNameAsSet_17(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74_StaticFields::get_offset_of_defaultUserId_18(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74_StaticFields::get_offset_of_defaultGroupId_19(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74_StaticFields::get_offset_of_defaultGroupName_20(),
	TarHeader_tE84CDB48A8D910B6485D7391D6E49A778332AA74_StaticFields::get_offset_of_defaultUser_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4962 = { sizeof (TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4962[8] = 
{
	TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E::get_offset_of_hasHitEOF_4(),
	TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E::get_offset_of_entrySize_5(),
	TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E::get_offset_of_entryOffset_6(),
	TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E::get_offset_of_readBuffer_7(),
	TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E::get_offset_of_tarBuffer_8(),
	TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E::get_offset_of_currentEntry_9(),
	TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E::get_offset_of_entryFactory_10(),
	TarInputStream_tB97EB07332E378AD3848EC09413E8425190CAC3E::get_offset_of_inputStream_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4963 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4964 = { sizeof (TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4964[8] = 
{
	TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838::get_offset_of_currBytes_4(),
	TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838::get_offset_of_assemblyBufferLength_5(),
	TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838::get_offset_of_isClosed_6(),
	TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838::get_offset_of_currSize_7(),
	TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838::get_offset_of_blockBuffer_8(),
	TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838::get_offset_of_assemblyBuffer_9(),
	TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838::get_offset_of_buffer_10(),
	TarOutputStream_t5327E699157D91D4FD74D62ACCD54C2C539C8838::get_offset_of_outputStream_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4965 = { sizeof (InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4965[7] = 
{
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3::get_offset_of_rawLength_0(),
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3::get_offset_of_rawData_1(),
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3::get_offset_of_clearTextLength_2(),
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3::get_offset_of_clearText_3(),
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3::get_offset_of_available_4(),
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3::get_offset_of_cryptoTransform_5(),
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3::get_offset_of_inputStream_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4966 = { sizeof (OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4966[3] = 
{
	OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581::get_offset_of_window_0(),
	OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581::get_offset_of_windowEnd_1(),
	OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581::get_offset_of_windowFilled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4967 = { sizeof (StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4967[5] = 
{
	StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5::get_offset_of_window__0(),
	StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5::get_offset_of_windowStart__1(),
	StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5::get_offset_of_windowEnd__2(),
	StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5::get_offset_of_buffer__3(),
	StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5::get_offset_of_bitsInBuffer__4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4968 = { sizeof (DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD), -1, sizeof(DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4968[6] = 
{
	DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields::get_offset_of_BL_ORDER_0(),
	DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields::get_offset_of_bit4Reverse_1(),
	DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields::get_offset_of_staticLCodes_2(),
	DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields::get_offset_of_staticLLength_3(),
	DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields::get_offset_of_staticDCodes_4(),
	DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields::get_offset_of_staticDLength_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4969 = { sizeof (Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB), -1, sizeof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4969[20] = 
{
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields::get_offset_of_CPLENS_0(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields::get_offset_of_CPLEXT_1(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields::get_offset_of_CPDIST_2(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields::get_offset_of_CPDEXT_3(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_mode_4(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_readAdler_5(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_neededBits_6(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_repLength_7(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_repDist_8(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_uncomprLen_9(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_isLastBlock_10(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_totalOut_11(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_totalIn_12(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_noHeader_13(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_input_14(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_outputWindow_15(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_dynHeader_16(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_litlenTree_17(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_distTree_18(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_adler_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4970 = { sizeof (InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3), -1, sizeof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4970[14] = 
{
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields::get_offset_of_repMin_0(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields::get_offset_of_repBits_1(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields::get_offset_of_BL_ORDER_2(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_blLens_3(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_litdistLens_4(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_blTree_5(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_mode_6(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_lnum_7(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_dnum_8(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_blnum_9(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_num_10(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_repSymbol_11(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_lastLen_12(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_ptr_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4971 = { sizeof (InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B), -1, sizeof(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4971[3] = 
{
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B::get_offset_of_tree_0(),
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_StaticFields::get_offset_of_defLitLenTree_1(),
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_StaticFields::get_offset_of_defDistTree_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4972 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4973 = { sizeof (UseZip64_t64A23EE068A11F6879053B3C4EFBF67F75D3B884)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4973[4] = 
{
	UseZip64_t64A23EE068A11F6879053B3C4EFBF67F75D3B884::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4974 = { sizeof (CompressionMethod_t1F31EE4DB3B1958C77654ED6CDA048854A028ECD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4974[6] = 
{
	CompressionMethod_t1F31EE4DB3B1958C77654ED6CDA048854A028ECD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4975 = { sizeof (ZipConstants_t346D2EB237FE5E5068BED97F246531E08A363863), -1, sizeof(ZipConstants_t346D2EB237FE5E5068BED97F246531E08A363863_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4975[1] = 
{
	ZipConstants_t346D2EB237FE5E5068BED97F246531E08A363863_StaticFields::get_offset_of_defaultCodePage_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4976 = { sizeof (ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4976[19] = 
{
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_known_0(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_externalFileAttributes_1(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_versionMadeBy_2(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_name_3(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_size_4(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_compressedSize_5(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_versionToExtract_6(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_crc_7(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_dosTime_8(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_method_9(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_extra_10(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_comment_11(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_flags_12(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_zipFileIndex_13(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_offset_14(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_forceZip64__15(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_cryptoCheckValue__16(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of__aesVer_17(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of__aesEncryptionStrength_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4977 = { sizeof (Known_tC640ED74F2497140C21D74C29A9E194AC89EF2FB)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4977[7] = 
{
	Known_tC640ED74F2497140C21D74C29A9E194AC89EF2FB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4978 = { sizeof (ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4978[3] = 
{
	ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929::get_offset_of_nameTransform__0(),
	ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929::get_offset_of_fixedDateTime__1(),
	ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929::get_offset_of_getAttributes__2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4979 = { sizeof (ZipException_t555EB5B9EA2BB8DF941C19C19F3BBA466EE7D944), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4980 = { sizeof (ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4980[5] = 
{
	ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0::get_offset_of__index_0(),
	ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0::get_offset_of__readValueStart_1(),
	ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0::get_offset_of__readValueLength_2(),
	ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0::get_offset_of__newEntry_3(),
	ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0::get_offset_of__data_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4981 = { sizeof (KeysRequiredEventArgs_t333068E27532BA6462A1C84D7D3CCE8904FC7148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4981[2] = 
{
	KeysRequiredEventArgs_t333068E27532BA6462A1C84D7D3CCE8904FC7148::get_offset_of_fileName_1(),
	KeysRequiredEventArgs_t333068E27532BA6462A1C84D7D3CCE8904FC7148::get_offset_of_key_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4982 = { sizeof (ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4982[17] = 
{
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_KeysRequired_0(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_isDisposed__1(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_name__2(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_comment__3(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_rawPassword__4(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_baseStream__5(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_isStreamOwner_6(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_offsetOfFirstEntry_7(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_entries__8(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_key_9(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_useZip64__10(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_updates__11(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_updateIndex__12(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_archiveStorage__13(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_updateDataSource__14(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_bufferSize__15(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_updateEntryFactory__16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4983 = { sizeof (KeysRequiredEventHandler_t01BA6F773E29C468C7FD85BE6B93EE5043A521D9), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4984 = { sizeof (HeaderTest_t98D72CE7316D175D3364E46D294E73256D1532D8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4984[3] = 
{
	HeaderTest_t98D72CE7316D175D3364E46D294E73256D1532D8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4985 = { sizeof (ZipEntryEnumerator_tD1C0F596AD8074627C711424B3F64891F3B2037F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4985[2] = 
{
	ZipEntryEnumerator_tD1C0F596AD8074627C711424B3F64891F3B2037F::get_offset_of_array_0(),
	ZipEntryEnumerator_tD1C0F596AD8074627C711424B3F64891F3B2037F::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4986 = { sizeof (PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4986[6] = 
{
	PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195::get_offset_of_zipFile__4(),
	PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195::get_offset_of_baseStream__5(),
	PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195::get_offset_of_start__6(),
	PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195::get_offset_of_length__7(),
	PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195::get_offset_of_readPos__8(),
	PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195::get_offset_of_end__9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4987 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4988 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4989 = { sizeof (ZipHelperStream_t5EBF734E826C8FAB30C920728A06E3291AB40B12), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4989[2] = 
{
	ZipHelperStream_t5EBF734E826C8FAB30C920728A06E3291AB40B12::get_offset_of_isOwner__4(),
	ZipHelperStream_t5EBF734E826C8FAB30C920728A06E3291AB40B12::get_offset_of_stream__5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4990 = { sizeof (ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF), -1, sizeof(ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4990[2] = 
{
	ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF_StaticFields::get_offset_of_InvalidEntryChars_0(),
	ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF_StaticFields::get_offset_of_InvalidEntryCharsRelaxed_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4991 = { sizeof (U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC), -1, sizeof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4991[11] = 
{
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004f9U2D1_0(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60000ddU2D1_1(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x600029dU2D1_2(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x600029dU2D2_3(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004fdU2D1_4(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004fdU2D2_5(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004fdU2D3_6(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004fdU2D4_7(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004feU2D1_8(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004feU2D2_9(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004feU2D3_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4992 = { sizeof (__StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4993 = { sizeof (__StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4994 = { sizeof (__StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4995 = { sizeof (__StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4996 = { sizeof (__StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4997 = { sizeof (__StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4998 = { sizeof (U3CModuleU3E_t54CCF983E499A2643FC67544116B427DC6EB5C39), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4999 = { sizeof (ConstructorHandling_t24063773444D2C4382EB581D086B8F078E533371)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4999[3] = 
{
	ConstructorHandling_t24063773444D2C4382EB581D086B8F078E533371::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
