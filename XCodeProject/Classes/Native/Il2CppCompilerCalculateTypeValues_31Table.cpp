﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A;
// System.Collections.Generic.IReadOnlyList`1<System.Linq.Expressions.Expression>
struct IReadOnlyList_1_tE0AAC21D2B77CF522F220413F6DFF6258C3BFFEB;
// System.Collections.Generic.IReadOnlyList`1<System.Linq.Expressions.ParameterExpression>
struct IReadOnlyList_1_t5DFB50E0BE5A6629C58BDB8962A4E8D4464B7BE6;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.Expression>
struct ReadOnlyCollection_1_tD7D717CF64D4DE35D57DF90C1D84E345FDA66B3E;
// System.Dynamic.Utils.CacheDict`2<System.Type,System.Func`5<System.Linq.Expressions.Expression,System.String,System.Boolean,System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ParameterExpression>,System.Linq.Expressions.LambdaExpression>>
struct CacheDict_2_tD9E815C55FFD40FDBB492D33789DC1AE2CC23F16;
// System.Dynamic.Utils.CacheDict`2<System.Type,System.Reflection.MethodInfo>
struct CacheDict_2_tF8A461B212165E766A927A935F261E445B76AADB;
// System.Linq.Expressions.BlockExpression
struct BlockExpression_t5613E40B381138E108C2CE3A3B4A53A793460DC0;
// System.Linq.Expressions.BlockExpressionList
struct BlockExpressionList_t08F678D1B58A8042C6F0855C640B36006A44D55A;
// System.Linq.Expressions.Expression
struct Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F;
// System.Linq.Expressions.LambdaExpression
struct LambdaExpression_t75634233B2F65FAB049A8F4AEB44836CF14F87B4;
// System.Linq.Expressions.ParameterExpression
struct ParameterExpression_t616ADBE354C741C2F0ABA5856714FC0607DEC217;
// System.Linq.Expressions.SymbolDocumentInfo
struct SymbolDocumentInfo_t860DD18CE6D7420C3D23B30589F174BD655B3190;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.CompilerServices.CallSiteBinder
struct CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889;
// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Linq.Expressions.Expression,System.Linq.Expressions.Expression/ExtensionInfo>
struct ConditionalWeakTable_2_t535A4A4D56280720C9EBCEE7884741E0241A6B55;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CRANGEITERATORU3ED__115_TF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450_H
#define U3CRANGEITERATORU3ED__115_TF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/<RangeIterator>d__115
struct  U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/<RangeIterator>d__115::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Int32 System.Linq.Enumerable/<RangeIterator>d__115::<>2__current
	int32_t ___U3CU3E2__current_1;
	// System.Int32 System.Linq.Enumerable/<RangeIterator>d__115::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Int32 System.Linq.Enumerable/<RangeIterator>d__115::start
	int32_t ___start_3;
	// System.Int32 System.Linq.Enumerable/<RangeIterator>d__115::<>3__start
	int32_t ___U3CU3E3__start_4;
	// System.Int32 System.Linq.Enumerable/<RangeIterator>d__115::<i>5__1
	int32_t ___U3CiU3E5__1_5;
	// System.Int32 System.Linq.Enumerable/<RangeIterator>d__115::count
	int32_t ___count_6;
	// System.Int32 System.Linq.Enumerable/<RangeIterator>d__115::<>3__count
	int32_t ___U3CU3E3__count_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450, ___U3CU3E2__current_1)); }
	inline int32_t get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline int32_t* get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(int32_t value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_start_3() { return static_cast<int32_t>(offsetof(U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450, ___start_3)); }
	inline int32_t get_start_3() const { return ___start_3; }
	inline int32_t* get_address_of_start_3() { return &___start_3; }
	inline void set_start_3(int32_t value)
	{
		___start_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__start_4() { return static_cast<int32_t>(offsetof(U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450, ___U3CU3E3__start_4)); }
	inline int32_t get_U3CU3E3__start_4() const { return ___U3CU3E3__start_4; }
	inline int32_t* get_address_of_U3CU3E3__start_4() { return &___U3CU3E3__start_4; }
	inline void set_U3CU3E3__start_4(int32_t value)
	{
		___U3CU3E3__start_4 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450, ___U3CiU3E5__1_5)); }
	inline int32_t get_U3CiU3E5__1_5() const { return ___U3CiU3E5__1_5; }
	inline int32_t* get_address_of_U3CiU3E5__1_5() { return &___U3CiU3E5__1_5; }
	inline void set_U3CiU3E5__1_5(int32_t value)
	{
		___U3CiU3E5__1_5 = value;
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450, ___count_6)); }
	inline int32_t get_count_6() const { return ___count_6; }
	inline int32_t* get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(int32_t value)
	{
		___count_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__count_7() { return static_cast<int32_t>(offsetof(U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450, ___U3CU3E3__count_7)); }
	inline int32_t get_U3CU3E3__count_7() const { return ___U3CU3E3__count_7; }
	inline int32_t* get_address_of_U3CU3E3__count_7() { return &___U3CU3E3__count_7; }
	inline void set_U3CU3E3__count_7(int32_t value)
	{
		___U3CU3E3__count_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRANGEITERATORU3ED__115_TF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450_H
#ifndef ARRAYBUILDEREXTENSIONS_TFD672AC010C3B563A707E8D154317702B571E8BF_H
#define ARRAYBUILDEREXTENSIONS_TFD672AC010C3B563A707E8D154317702B571E8BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ArrayBuilderExtensions
struct  ArrayBuilderExtensions_tFD672AC010C3B563A707E8D154317702B571E8BF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYBUILDEREXTENSIONS_TFD672AC010C3B563A707E8D154317702B571E8BF_H
#ifndef BLOCKEXPRESSIONLIST_T08F678D1B58A8042C6F0855C640B36006A44D55A_H
#define BLOCKEXPRESSIONLIST_T08F678D1B58A8042C6F0855C640B36006A44D55A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.BlockExpressionList
struct  BlockExpressionList_t08F678D1B58A8042C6F0855C640B36006A44D55A  : public RuntimeObject
{
public:
	// System.Linq.Expressions.BlockExpression System.Linq.Expressions.BlockExpressionList::_block
	BlockExpression_t5613E40B381138E108C2CE3A3B4A53A793460DC0 * ____block_0;
	// System.Linq.Expressions.Expression System.Linq.Expressions.BlockExpressionList::_arg0
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg0_1;

public:
	inline static int32_t get_offset_of__block_0() { return static_cast<int32_t>(offsetof(BlockExpressionList_t08F678D1B58A8042C6F0855C640B36006A44D55A, ____block_0)); }
	inline BlockExpression_t5613E40B381138E108C2CE3A3B4A53A793460DC0 * get__block_0() const { return ____block_0; }
	inline BlockExpression_t5613E40B381138E108C2CE3A3B4A53A793460DC0 ** get_address_of__block_0() { return &____block_0; }
	inline void set__block_0(BlockExpression_t5613E40B381138E108C2CE3A3B4A53A793460DC0 * value)
	{
		____block_0 = value;
		Il2CppCodeGenWriteBarrier((&____block_0), value);
	}

	inline static int32_t get_offset_of__arg0_1() { return static_cast<int32_t>(offsetof(BlockExpressionList_t08F678D1B58A8042C6F0855C640B36006A44D55A, ____arg0_1)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg0_1() const { return ____arg0_1; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg0_1() { return &____arg0_1; }
	inline void set__arg0_1(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg0_1 = value;
		Il2CppCodeGenWriteBarrier((&____arg0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKEXPRESSIONLIST_T08F678D1B58A8042C6F0855C640B36006A44D55A_H
#ifndef U3CGETENUMERATORU3ED__18_TF7F7D15CFAB0C836E345FA8DD20482647A01C49C_H
#define U3CGETENUMERATORU3ED__18_TF7F7D15CFAB0C836E345FA8DD20482647A01C49C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.BlockExpressionList/<GetEnumerator>d__18
struct  U3CGetEnumeratorU3Ed__18_tF7F7D15CFAB0C836E345FA8DD20482647A01C49C  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Expressions.BlockExpressionList/<GetEnumerator>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Linq.Expressions.Expression System.Linq.Expressions.BlockExpressionList/<GetEnumerator>d__18::<>2__current
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___U3CU3E2__current_1;
	// System.Linq.Expressions.BlockExpressionList System.Linq.Expressions.BlockExpressionList/<GetEnumerator>d__18::<>4__this
	BlockExpressionList_t08F678D1B58A8042C6F0855C640B36006A44D55A * ___U3CU3E4__this_2;
	// System.Int32 System.Linq.Expressions.BlockExpressionList/<GetEnumerator>d__18::<i>5__1
	int32_t ___U3CiU3E5__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_tF7F7D15CFAB0C836E345FA8DD20482647A01C49C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_tF7F7D15CFAB0C836E345FA8DD20482647A01C49C, ___U3CU3E2__current_1)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_tF7F7D15CFAB0C836E345FA8DD20482647A01C49C, ___U3CU3E4__this_2)); }
	inline BlockExpressionList_t08F678D1B58A8042C6F0855C640B36006A44D55A * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BlockExpressionList_t08F678D1B58A8042C6F0855C640B36006A44D55A ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BlockExpressionList_t08F678D1B58A8042C6F0855C640B36006A44D55A * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_tF7F7D15CFAB0C836E345FA8DD20482647A01C49C, ___U3CiU3E5__1_3)); }
	inline int32_t get_U3CiU3E5__1_3() const { return ___U3CiU3E5__1_3; }
	inline int32_t* get_address_of_U3CiU3E5__1_3() { return &___U3CiU3E5__1_3; }
	inline void set_U3CiU3E5__1_3(int32_t value)
	{
		___U3CiU3E5__1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__18_TF7F7D15CFAB0C836E345FA8DD20482647A01C49C_H
#ifndef CACHEDREFLECTIONINFO_TE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_H
#define CACHEDREFLECTIONINFO_TE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.CachedReflectionInfo
struct  CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14  : public RuntimeObject
{
public:

public:
};

struct CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields
{
public:
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_SetNotMatched
	MethodInfo_t * ___s_CallSiteOps_SetNotMatched_0;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_CreateMatchmaker
	MethodInfo_t * ___s_CallSiteOps_CreateMatchmaker_1;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_GetMatch
	MethodInfo_t * ___s_CallSiteOps_GetMatch_2;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_ClearMatch
	MethodInfo_t * ___s_CallSiteOps_ClearMatch_3;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_UpdateRules
	MethodInfo_t * ___s_CallSiteOps_UpdateRules_4;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_GetRules
	MethodInfo_t * ___s_CallSiteOps_GetRules_5;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_GetRuleCache
	MethodInfo_t * ___s_CallSiteOps_GetRuleCache_6;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_GetCachedRules
	MethodInfo_t * ___s_CallSiteOps_GetCachedRules_7;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_AddRule
	MethodInfo_t * ___s_CallSiteOps_AddRule_8;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_MoveRule
	MethodInfo_t * ___s_CallSiteOps_MoveRule_9;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_Bind
	MethodInfo_t * ___s_CallSiteOps_Bind_10;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_String_op_Equality_String_String
	MethodInfo_t * ___s_String_op_Equality_String_String_11;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_Math_Pow_Double_Double
	MethodInfo_t * ___s_Math_Pow_Double_Double_12;

public:
	inline static int32_t get_offset_of_s_CallSiteOps_SetNotMatched_0() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields, ___s_CallSiteOps_SetNotMatched_0)); }
	inline MethodInfo_t * get_s_CallSiteOps_SetNotMatched_0() const { return ___s_CallSiteOps_SetNotMatched_0; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_SetNotMatched_0() { return &___s_CallSiteOps_SetNotMatched_0; }
	inline void set_s_CallSiteOps_SetNotMatched_0(MethodInfo_t * value)
	{
		___s_CallSiteOps_SetNotMatched_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_SetNotMatched_0), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_CreateMatchmaker_1() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields, ___s_CallSiteOps_CreateMatchmaker_1)); }
	inline MethodInfo_t * get_s_CallSiteOps_CreateMatchmaker_1() const { return ___s_CallSiteOps_CreateMatchmaker_1; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_CreateMatchmaker_1() { return &___s_CallSiteOps_CreateMatchmaker_1; }
	inline void set_s_CallSiteOps_CreateMatchmaker_1(MethodInfo_t * value)
	{
		___s_CallSiteOps_CreateMatchmaker_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_CreateMatchmaker_1), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_GetMatch_2() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields, ___s_CallSiteOps_GetMatch_2)); }
	inline MethodInfo_t * get_s_CallSiteOps_GetMatch_2() const { return ___s_CallSiteOps_GetMatch_2; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_GetMatch_2() { return &___s_CallSiteOps_GetMatch_2; }
	inline void set_s_CallSiteOps_GetMatch_2(MethodInfo_t * value)
	{
		___s_CallSiteOps_GetMatch_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_GetMatch_2), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_ClearMatch_3() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields, ___s_CallSiteOps_ClearMatch_3)); }
	inline MethodInfo_t * get_s_CallSiteOps_ClearMatch_3() const { return ___s_CallSiteOps_ClearMatch_3; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_ClearMatch_3() { return &___s_CallSiteOps_ClearMatch_3; }
	inline void set_s_CallSiteOps_ClearMatch_3(MethodInfo_t * value)
	{
		___s_CallSiteOps_ClearMatch_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_ClearMatch_3), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_UpdateRules_4() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields, ___s_CallSiteOps_UpdateRules_4)); }
	inline MethodInfo_t * get_s_CallSiteOps_UpdateRules_4() const { return ___s_CallSiteOps_UpdateRules_4; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_UpdateRules_4() { return &___s_CallSiteOps_UpdateRules_4; }
	inline void set_s_CallSiteOps_UpdateRules_4(MethodInfo_t * value)
	{
		___s_CallSiteOps_UpdateRules_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_UpdateRules_4), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_GetRules_5() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields, ___s_CallSiteOps_GetRules_5)); }
	inline MethodInfo_t * get_s_CallSiteOps_GetRules_5() const { return ___s_CallSiteOps_GetRules_5; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_GetRules_5() { return &___s_CallSiteOps_GetRules_5; }
	inline void set_s_CallSiteOps_GetRules_5(MethodInfo_t * value)
	{
		___s_CallSiteOps_GetRules_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_GetRules_5), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_GetRuleCache_6() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields, ___s_CallSiteOps_GetRuleCache_6)); }
	inline MethodInfo_t * get_s_CallSiteOps_GetRuleCache_6() const { return ___s_CallSiteOps_GetRuleCache_6; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_GetRuleCache_6() { return &___s_CallSiteOps_GetRuleCache_6; }
	inline void set_s_CallSiteOps_GetRuleCache_6(MethodInfo_t * value)
	{
		___s_CallSiteOps_GetRuleCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_GetRuleCache_6), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_GetCachedRules_7() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields, ___s_CallSiteOps_GetCachedRules_7)); }
	inline MethodInfo_t * get_s_CallSiteOps_GetCachedRules_7() const { return ___s_CallSiteOps_GetCachedRules_7; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_GetCachedRules_7() { return &___s_CallSiteOps_GetCachedRules_7; }
	inline void set_s_CallSiteOps_GetCachedRules_7(MethodInfo_t * value)
	{
		___s_CallSiteOps_GetCachedRules_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_GetCachedRules_7), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_AddRule_8() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields, ___s_CallSiteOps_AddRule_8)); }
	inline MethodInfo_t * get_s_CallSiteOps_AddRule_8() const { return ___s_CallSiteOps_AddRule_8; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_AddRule_8() { return &___s_CallSiteOps_AddRule_8; }
	inline void set_s_CallSiteOps_AddRule_8(MethodInfo_t * value)
	{
		___s_CallSiteOps_AddRule_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_AddRule_8), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_MoveRule_9() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields, ___s_CallSiteOps_MoveRule_9)); }
	inline MethodInfo_t * get_s_CallSiteOps_MoveRule_9() const { return ___s_CallSiteOps_MoveRule_9; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_MoveRule_9() { return &___s_CallSiteOps_MoveRule_9; }
	inline void set_s_CallSiteOps_MoveRule_9(MethodInfo_t * value)
	{
		___s_CallSiteOps_MoveRule_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_MoveRule_9), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_Bind_10() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields, ___s_CallSiteOps_Bind_10)); }
	inline MethodInfo_t * get_s_CallSiteOps_Bind_10() const { return ___s_CallSiteOps_Bind_10; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_Bind_10() { return &___s_CallSiteOps_Bind_10; }
	inline void set_s_CallSiteOps_Bind_10(MethodInfo_t * value)
	{
		___s_CallSiteOps_Bind_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_Bind_10), value);
	}

	inline static int32_t get_offset_of_s_String_op_Equality_String_String_11() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields, ___s_String_op_Equality_String_String_11)); }
	inline MethodInfo_t * get_s_String_op_Equality_String_String_11() const { return ___s_String_op_Equality_String_String_11; }
	inline MethodInfo_t ** get_address_of_s_String_op_Equality_String_String_11() { return &___s_String_op_Equality_String_String_11; }
	inline void set_s_String_op_Equality_String_String_11(MethodInfo_t * value)
	{
		___s_String_op_Equality_String_String_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_String_op_Equality_String_String_11), value);
	}

	inline static int32_t get_offset_of_s_Math_Pow_Double_Double_12() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields, ___s_Math_Pow_Double_Double_12)); }
	inline MethodInfo_t * get_s_Math_Pow_Double_Double_12() const { return ___s_Math_Pow_Double_Double_12; }
	inline MethodInfo_t ** get_address_of_s_Math_Pow_Double_Double_12() { return &___s_Math_Pow_Double_Double_12; }
	inline void set_s_Math_Pow_Double_Double_12(MethodInfo_t * value)
	{
		___s_Math_Pow_Double_Double_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_Math_Pow_Double_Double_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDREFLECTIONINFO_TE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_H
#ifndef CATCHBLOCK_T56D3C700B21707DF484608E619F354C25B2D7E64_H
#define CATCHBLOCK_T56D3C700B21707DF484608E619F354C25B2D7E64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.CatchBlock
struct  CatchBlock_t56D3C700B21707DF484608E619F354C25B2D7E64  : public RuntimeObject
{
public:
	// System.Linq.Expressions.ParameterExpression System.Linq.Expressions.CatchBlock::<Variable>k__BackingField
	ParameterExpression_t616ADBE354C741C2F0ABA5856714FC0607DEC217 * ___U3CVariableU3Ek__BackingField_0;
	// System.Type System.Linq.Expressions.CatchBlock::<Test>k__BackingField
	Type_t * ___U3CTestU3Ek__BackingField_1;
	// System.Linq.Expressions.Expression System.Linq.Expressions.CatchBlock::<Body>k__BackingField
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___U3CBodyU3Ek__BackingField_2;
	// System.Linq.Expressions.Expression System.Linq.Expressions.CatchBlock::<Filter>k__BackingField
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___U3CFilterU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CVariableU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CatchBlock_t56D3C700B21707DF484608E619F354C25B2D7E64, ___U3CVariableU3Ek__BackingField_0)); }
	inline ParameterExpression_t616ADBE354C741C2F0ABA5856714FC0607DEC217 * get_U3CVariableU3Ek__BackingField_0() const { return ___U3CVariableU3Ek__BackingField_0; }
	inline ParameterExpression_t616ADBE354C741C2F0ABA5856714FC0607DEC217 ** get_address_of_U3CVariableU3Ek__BackingField_0() { return &___U3CVariableU3Ek__BackingField_0; }
	inline void set_U3CVariableU3Ek__BackingField_0(ParameterExpression_t616ADBE354C741C2F0ABA5856714FC0607DEC217 * value)
	{
		___U3CVariableU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVariableU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTestU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CatchBlock_t56D3C700B21707DF484608E619F354C25B2D7E64, ___U3CTestU3Ek__BackingField_1)); }
	inline Type_t * get_U3CTestU3Ek__BackingField_1() const { return ___U3CTestU3Ek__BackingField_1; }
	inline Type_t ** get_address_of_U3CTestU3Ek__BackingField_1() { return &___U3CTestU3Ek__BackingField_1; }
	inline void set_U3CTestU3Ek__BackingField_1(Type_t * value)
	{
		___U3CTestU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTestU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CBodyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CatchBlock_t56D3C700B21707DF484608E619F354C25B2D7E64, ___U3CBodyU3Ek__BackingField_2)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get_U3CBodyU3Ek__BackingField_2() const { return ___U3CBodyU3Ek__BackingField_2; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of_U3CBodyU3Ek__BackingField_2() { return &___U3CBodyU3Ek__BackingField_2; }
	inline void set_U3CBodyU3Ek__BackingField_2(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		___U3CBodyU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBodyU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CFilterU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CatchBlock_t56D3C700B21707DF484608E619F354C25B2D7E64, ___U3CFilterU3Ek__BackingField_3)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get_U3CFilterU3Ek__BackingField_3() const { return ___U3CFilterU3Ek__BackingField_3; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of_U3CFilterU3Ek__BackingField_3() { return &___U3CFilterU3Ek__BackingField_3; }
	inline void set_U3CFilterU3Ek__BackingField_3(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		___U3CFilterU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFilterU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATCHBLOCK_T56D3C700B21707DF484608E619F354C25B2D7E64_H
#ifndef CONSTANTCHECK_TFFE62F1408427AE7354DC3E4AA7017FCAAFEFE8D_H
#define CONSTANTCHECK_TFFE62F1408427AE7354DC3E4AA7017FCAAFEFE8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ConstantCheck
struct  ConstantCheck_tFFE62F1408427AE7354DC3E4AA7017FCAAFEFE8D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTCHECK_TFFE62F1408427AE7354DC3E4AA7017FCAAFEFE8D_H
#ifndef ELEMENTINIT_TE52EF678BD23E3A9448F99710E4AF84E6A0F544B_H
#define ELEMENTINIT_TE52EF678BD23E3A9448F99710E4AF84E6A0F544B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ElementInit
struct  ElementInit_tE52EF678BD23E3A9448F99710E4AF84E6A0F544B  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo System.Linq.Expressions.ElementInit::<AddMethod>k__BackingField
	MethodInfo_t * ___U3CAddMethodU3Ek__BackingField_0;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.Expression> System.Linq.Expressions.ElementInit::<Arguments>k__BackingField
	ReadOnlyCollection_1_tD7D717CF64D4DE35D57DF90C1D84E345FDA66B3E * ___U3CArgumentsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CAddMethodU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ElementInit_tE52EF678BD23E3A9448F99710E4AF84E6A0F544B, ___U3CAddMethodU3Ek__BackingField_0)); }
	inline MethodInfo_t * get_U3CAddMethodU3Ek__BackingField_0() const { return ___U3CAddMethodU3Ek__BackingField_0; }
	inline MethodInfo_t ** get_address_of_U3CAddMethodU3Ek__BackingField_0() { return &___U3CAddMethodU3Ek__BackingField_0; }
	inline void set_U3CAddMethodU3Ek__BackingField_0(MethodInfo_t * value)
	{
		___U3CAddMethodU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAddMethodU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CArgumentsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ElementInit_tE52EF678BD23E3A9448F99710E4AF84E6A0F544B, ___U3CArgumentsU3Ek__BackingField_1)); }
	inline ReadOnlyCollection_1_tD7D717CF64D4DE35D57DF90C1D84E345FDA66B3E * get_U3CArgumentsU3Ek__BackingField_1() const { return ___U3CArgumentsU3Ek__BackingField_1; }
	inline ReadOnlyCollection_1_tD7D717CF64D4DE35D57DF90C1D84E345FDA66B3E ** get_address_of_U3CArgumentsU3Ek__BackingField_1() { return &___U3CArgumentsU3Ek__BackingField_1; }
	inline void set_U3CArgumentsU3Ek__BackingField_1(ReadOnlyCollection_1_tD7D717CF64D4DE35D57DF90C1D84E345FDA66B3E * value)
	{
		___U3CArgumentsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgumentsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTINIT_TE52EF678BD23E3A9448F99710E4AF84E6A0F544B_H
#ifndef ERROR_T15851665677E2192F92E4241B725BBEB2A51EFBB_H
#define ERROR_T15851665677E2192F92E4241B725BBEB2A51EFBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Error
struct  Error_t15851665677E2192F92E4241B725BBEB2A51EFBB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROR_T15851665677E2192F92E4241B725BBEB2A51EFBB_H
#ifndef EXPRESSION_T13144FD397DCFCFABDC7D6B513CD5F32A1C2833F_H
#define EXPRESSION_T13144FD397DCFCFABDC7D6B513CD5F32A1C2833F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression
struct  Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F  : public RuntimeObject
{
public:

public:
};

struct Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F_StaticFields
{
public:
	// System.Dynamic.Utils.CacheDict`2<System.Type,System.Reflection.MethodInfo> System.Linq.Expressions.Expression::s_lambdaDelegateCache
	CacheDict_2_tF8A461B212165E766A927A935F261E445B76AADB * ___s_lambdaDelegateCache_0;
	// System.Dynamic.Utils.CacheDict`2<System.Type,System.Func`5<System.Linq.Expressions.Expression,System.String,System.Boolean,System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ParameterExpression>,System.Linq.Expressions.LambdaExpression>> modreq(System.Runtime.CompilerServices.IsVolatile) System.Linq.Expressions.Expression::s_lambdaFactories
	CacheDict_2_tD9E815C55FFD40FDBB492D33789DC1AE2CC23F16 * ___s_lambdaFactories_1;
	// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Linq.Expressions.Expression,System.Linq.Expressions.Expression/ExtensionInfo> System.Linq.Expressions.Expression::s_legacyCtorSupportTable
	ConditionalWeakTable_2_t535A4A4D56280720C9EBCEE7884741E0241A6B55 * ___s_legacyCtorSupportTable_2;

public:
	inline static int32_t get_offset_of_s_lambdaDelegateCache_0() { return static_cast<int32_t>(offsetof(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F_StaticFields, ___s_lambdaDelegateCache_0)); }
	inline CacheDict_2_tF8A461B212165E766A927A935F261E445B76AADB * get_s_lambdaDelegateCache_0() const { return ___s_lambdaDelegateCache_0; }
	inline CacheDict_2_tF8A461B212165E766A927A935F261E445B76AADB ** get_address_of_s_lambdaDelegateCache_0() { return &___s_lambdaDelegateCache_0; }
	inline void set_s_lambdaDelegateCache_0(CacheDict_2_tF8A461B212165E766A927A935F261E445B76AADB * value)
	{
		___s_lambdaDelegateCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_lambdaDelegateCache_0), value);
	}

	inline static int32_t get_offset_of_s_lambdaFactories_1() { return static_cast<int32_t>(offsetof(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F_StaticFields, ___s_lambdaFactories_1)); }
	inline CacheDict_2_tD9E815C55FFD40FDBB492D33789DC1AE2CC23F16 * get_s_lambdaFactories_1() const { return ___s_lambdaFactories_1; }
	inline CacheDict_2_tD9E815C55FFD40FDBB492D33789DC1AE2CC23F16 ** get_address_of_s_lambdaFactories_1() { return &___s_lambdaFactories_1; }
	inline void set_s_lambdaFactories_1(CacheDict_2_tD9E815C55FFD40FDBB492D33789DC1AE2CC23F16 * value)
	{
		___s_lambdaFactories_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_lambdaFactories_1), value);
	}

	inline static int32_t get_offset_of_s_legacyCtorSupportTable_2() { return static_cast<int32_t>(offsetof(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F_StaticFields, ___s_legacyCtorSupportTable_2)); }
	inline ConditionalWeakTable_2_t535A4A4D56280720C9EBCEE7884741E0241A6B55 * get_s_legacyCtorSupportTable_2() const { return ___s_legacyCtorSupportTable_2; }
	inline ConditionalWeakTable_2_t535A4A4D56280720C9EBCEE7884741E0241A6B55 ** get_address_of_s_legacyCtorSupportTable_2() { return &___s_legacyCtorSupportTable_2; }
	inline void set_s_legacyCtorSupportTable_2(ConditionalWeakTable_2_t535A4A4D56280720C9EBCEE7884741E0241A6B55 * value)
	{
		___s_legacyCtorSupportTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_legacyCtorSupportTable_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSION_T13144FD397DCFCFABDC7D6B513CD5F32A1C2833F_H
#ifndef BINARYEXPRESSIONPROXY_T4E56E8621F86F80B1A2CABA5B1CE67B745666B9A_H
#define BINARYEXPRESSIONPROXY_T4E56E8621F86F80B1A2CABA5B1CE67B745666B9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/BinaryExpressionProxy
struct  BinaryExpressionProxy_t4E56E8621F86F80B1A2CABA5B1CE67B745666B9A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYEXPRESSIONPROXY_T4E56E8621F86F80B1A2CABA5B1CE67B745666B9A_H
#ifndef BLOCKEXPRESSIONPROXY_T2BDE714E12A8469B6270A9CA6EA9D6C117008668_H
#define BLOCKEXPRESSIONPROXY_T2BDE714E12A8469B6270A9CA6EA9D6C117008668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/BlockExpressionProxy
struct  BlockExpressionProxy_t2BDE714E12A8469B6270A9CA6EA9D6C117008668  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKEXPRESSIONPROXY_T2BDE714E12A8469B6270A9CA6EA9D6C117008668_H
#ifndef CATCHBLOCKPROXY_TE843ACD1ED306F2731F7E4ECA09E784617E9D191_H
#define CATCHBLOCKPROXY_TE843ACD1ED306F2731F7E4ECA09E784617E9D191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/CatchBlockProxy
struct  CatchBlockProxy_tE843ACD1ED306F2731F7E4ECA09E784617E9D191  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATCHBLOCKPROXY_TE843ACD1ED306F2731F7E4ECA09E784617E9D191_H
#ifndef CONDITIONALEXPRESSIONPROXY_T0A38D650C4FAC80C22E6B4708AB1BDF82702CB07_H
#define CONDITIONALEXPRESSIONPROXY_T0A38D650C4FAC80C22E6B4708AB1BDF82702CB07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/ConditionalExpressionProxy
struct  ConditionalExpressionProxy_t0A38D650C4FAC80C22E6B4708AB1BDF82702CB07  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONALEXPRESSIONPROXY_T0A38D650C4FAC80C22E6B4708AB1BDF82702CB07_H
#ifndef CONSTANTEXPRESSIONPROXY_TF7144753C8EEEE622B9BF8CCAFA6CF6AD2CE30CA_H
#define CONSTANTEXPRESSIONPROXY_TF7144753C8EEEE622B9BF8CCAFA6CF6AD2CE30CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/ConstantExpressionProxy
struct  ConstantExpressionProxy_tF7144753C8EEEE622B9BF8CCAFA6CF6AD2CE30CA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTEXPRESSIONPROXY_TF7144753C8EEEE622B9BF8CCAFA6CF6AD2CE30CA_H
#ifndef DEBUGINFOEXPRESSIONPROXY_T79D7C20C51AC2950AA6745CE0B351F3618EB1845_H
#define DEBUGINFOEXPRESSIONPROXY_T79D7C20C51AC2950AA6745CE0B351F3618EB1845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/DebugInfoExpressionProxy
struct  DebugInfoExpressionProxy_t79D7C20C51AC2950AA6745CE0B351F3618EB1845  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGINFOEXPRESSIONPROXY_T79D7C20C51AC2950AA6745CE0B351F3618EB1845_H
#ifndef DEFAULTEXPRESSIONPROXY_T0AF31C51AE8334247CA292F19A90C69D164C8AD1_H
#define DEFAULTEXPRESSIONPROXY_T0AF31C51AE8334247CA292F19A90C69D164C8AD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/DefaultExpressionProxy
struct  DefaultExpressionProxy_t0AF31C51AE8334247CA292F19A90C69D164C8AD1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTEXPRESSIONPROXY_T0AF31C51AE8334247CA292F19A90C69D164C8AD1_H
#ifndef GOTOEXPRESSIONPROXY_T9E397A25F622DE41C3026F2CF3BC09040D055CDB_H
#define GOTOEXPRESSIONPROXY_T9E397A25F622DE41C3026F2CF3BC09040D055CDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/GotoExpressionProxy
struct  GotoExpressionProxy_t9E397A25F622DE41C3026F2CF3BC09040D055CDB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOTOEXPRESSIONPROXY_T9E397A25F622DE41C3026F2CF3BC09040D055CDB_H
#ifndef INDEXEXPRESSIONPROXY_T73B0DA06C5FCCADF6607D6D0A76D489950C2454A_H
#define INDEXEXPRESSIONPROXY_T73B0DA06C5FCCADF6607D6D0A76D489950C2454A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/IndexExpressionProxy
struct  IndexExpressionProxy_t73B0DA06C5FCCADF6607D6D0A76D489950C2454A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXEXPRESSIONPROXY_T73B0DA06C5FCCADF6607D6D0A76D489950C2454A_H
#ifndef INVOCATIONEXPRESSIONPROXY_TDBD0FCF2EC385080B0D0976D98AE58E7668F8156_H
#define INVOCATIONEXPRESSIONPROXY_TDBD0FCF2EC385080B0D0976D98AE58E7668F8156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/InvocationExpressionProxy
struct  InvocationExpressionProxy_tDBD0FCF2EC385080B0D0976D98AE58E7668F8156  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOCATIONEXPRESSIONPROXY_TDBD0FCF2EC385080B0D0976D98AE58E7668F8156_H
#ifndef LABELEXPRESSIONPROXY_TE5634DF21429942EF34929AAA7D6A1F47886E900_H
#define LABELEXPRESSIONPROXY_TE5634DF21429942EF34929AAA7D6A1F47886E900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/LabelExpressionProxy
struct  LabelExpressionProxy_tE5634DF21429942EF34929AAA7D6A1F47886E900  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LABELEXPRESSIONPROXY_TE5634DF21429942EF34929AAA7D6A1F47886E900_H
#ifndef LAMBDAEXPRESSIONPROXY_T0329823C7F8CB84A18FB01862E7E2329FF609B58_H
#define LAMBDAEXPRESSIONPROXY_T0329823C7F8CB84A18FB01862E7E2329FF609B58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/LambdaExpressionProxy
struct  LambdaExpressionProxy_t0329823C7F8CB84A18FB01862E7E2329FF609B58  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAMBDAEXPRESSIONPROXY_T0329823C7F8CB84A18FB01862E7E2329FF609B58_H
#ifndef LISTINITEXPRESSIONPROXY_T0C544125FB1E83286C918EE26017723365C87DF8_H
#define LISTINITEXPRESSIONPROXY_T0C544125FB1E83286C918EE26017723365C87DF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/ListInitExpressionProxy
struct  ListInitExpressionProxy_t0C544125FB1E83286C918EE26017723365C87DF8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTINITEXPRESSIONPROXY_T0C544125FB1E83286C918EE26017723365C87DF8_H
#ifndef LOOPEXPRESSIONPROXY_T7A0A1B87DB30C79F9CA434BB3EABFBE713FC5DEC_H
#define LOOPEXPRESSIONPROXY_T7A0A1B87DB30C79F9CA434BB3EABFBE713FC5DEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/LoopExpressionProxy
struct  LoopExpressionProxy_t7A0A1B87DB30C79F9CA434BB3EABFBE713FC5DEC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPEXPRESSIONPROXY_T7A0A1B87DB30C79F9CA434BB3EABFBE713FC5DEC_H
#ifndef MEMBEREXPRESSIONPROXY_T6C2D8472505AFBA73202F9FCB520FA1E1ED8D100_H
#define MEMBEREXPRESSIONPROXY_T6C2D8472505AFBA73202F9FCB520FA1E1ED8D100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/MemberExpressionProxy
struct  MemberExpressionProxy_t6C2D8472505AFBA73202F9FCB520FA1E1ED8D100  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBEREXPRESSIONPROXY_T6C2D8472505AFBA73202F9FCB520FA1E1ED8D100_H
#ifndef MEMBERINITEXPRESSIONPROXY_T93AA89F3681DD86FDAAEE48525258AF236B1E955_H
#define MEMBERINITEXPRESSIONPROXY_T93AA89F3681DD86FDAAEE48525258AF236B1E955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/MemberInitExpressionProxy
struct  MemberInitExpressionProxy_t93AA89F3681DD86FDAAEE48525258AF236B1E955  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINITEXPRESSIONPROXY_T93AA89F3681DD86FDAAEE48525258AF236B1E955_H
#ifndef METHODCALLEXPRESSIONPROXY_T114DC6C2F53B7EF7198D540C80056F8EB50C6146_H
#define METHODCALLEXPRESSIONPROXY_T114DC6C2F53B7EF7198D540C80056F8EB50C6146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/MethodCallExpressionProxy
struct  MethodCallExpressionProxy_t114DC6C2F53B7EF7198D540C80056F8EB50C6146  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODCALLEXPRESSIONPROXY_T114DC6C2F53B7EF7198D540C80056F8EB50C6146_H
#ifndef NEWARRAYEXPRESSIONPROXY_TC522CFC96FA9CEDAC4CA25C92E6536E501E1C316_H
#define NEWARRAYEXPRESSIONPROXY_TC522CFC96FA9CEDAC4CA25C92E6536E501E1C316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/NewArrayExpressionProxy
struct  NewArrayExpressionProxy_tC522CFC96FA9CEDAC4CA25C92E6536E501E1C316  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWARRAYEXPRESSIONPROXY_TC522CFC96FA9CEDAC4CA25C92E6536E501E1C316_H
#ifndef NEWEXPRESSIONPROXY_T9B725808DF48067DADF750F253F42CC8D73D95CE_H
#define NEWEXPRESSIONPROXY_T9B725808DF48067DADF750F253F42CC8D73D95CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/NewExpressionProxy
struct  NewExpressionProxy_t9B725808DF48067DADF750F253F42CC8D73D95CE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWEXPRESSIONPROXY_T9B725808DF48067DADF750F253F42CC8D73D95CE_H
#ifndef PARAMETEREXPRESSIONPROXY_TBB89F4DCCB4CF297D8C33D4CF920F0F253C8B689_H
#define PARAMETEREXPRESSIONPROXY_TBB89F4DCCB4CF297D8C33D4CF920F0F253C8B689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/ParameterExpressionProxy
struct  ParameterExpressionProxy_tBB89F4DCCB4CF297D8C33D4CF920F0F253C8B689  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEREXPRESSIONPROXY_TBB89F4DCCB4CF297D8C33D4CF920F0F253C8B689_H
#ifndef RUNTIMEVARIABLESEXPRESSIONPROXY_TE5623A2468F383D7DE36202A05420853F9AA3330_H
#define RUNTIMEVARIABLESEXPRESSIONPROXY_TE5623A2468F383D7DE36202A05420853F9AA3330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/RuntimeVariablesExpressionProxy
struct  RuntimeVariablesExpressionProxy_tE5623A2468F383D7DE36202A05420853F9AA3330  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEVARIABLESEXPRESSIONPROXY_TE5623A2468F383D7DE36202A05420853F9AA3330_H
#ifndef SWITCHCASEPROXY_T2D879E7D31FBC5B5705F02D9C0050BA418A27A32_H
#define SWITCHCASEPROXY_T2D879E7D31FBC5B5705F02D9C0050BA418A27A32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/SwitchCaseProxy
struct  SwitchCaseProxy_t2D879E7D31FBC5B5705F02D9C0050BA418A27A32  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCHCASEPROXY_T2D879E7D31FBC5B5705F02D9C0050BA418A27A32_H
#ifndef SWITCHEXPRESSIONPROXY_T25D440423D0E4BBF67C885BE102DCC9A5F288DE6_H
#define SWITCHEXPRESSIONPROXY_T25D440423D0E4BBF67C885BE102DCC9A5F288DE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/SwitchExpressionProxy
struct  SwitchExpressionProxy_t25D440423D0E4BBF67C885BE102DCC9A5F288DE6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCHEXPRESSIONPROXY_T25D440423D0E4BBF67C885BE102DCC9A5F288DE6_H
#ifndef TRYEXPRESSIONPROXY_T03B4CBC9EB77A07874851B7A65B32A6C5AD8213C_H
#define TRYEXPRESSIONPROXY_T03B4CBC9EB77A07874851B7A65B32A6C5AD8213C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/TryExpressionProxy
struct  TryExpressionProxy_t03B4CBC9EB77A07874851B7A65B32A6C5AD8213C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRYEXPRESSIONPROXY_T03B4CBC9EB77A07874851B7A65B32A6C5AD8213C_H
#ifndef TYPEBINARYEXPRESSIONPROXY_T4CC1003894A2968865169ADCF54A90B4166C434B_H
#define TYPEBINARYEXPRESSIONPROXY_T4CC1003894A2968865169ADCF54A90B4166C434B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/TypeBinaryExpressionProxy
struct  TypeBinaryExpressionProxy_t4CC1003894A2968865169ADCF54A90B4166C434B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEBINARYEXPRESSIONPROXY_T4CC1003894A2968865169ADCF54A90B4166C434B_H
#ifndef UNARYEXPRESSIONPROXY_TC967CC148CB23EF56B38327141C8DDA9825ECA23_H
#define UNARYEXPRESSIONPROXY_TC967CC148CB23EF56B38327141C8DDA9825ECA23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/UnaryExpressionProxy
struct  UnaryExpressionProxy_tC967CC148CB23EF56B38327141C8DDA9825ECA23  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNARYEXPRESSIONPROXY_TC967CC148CB23EF56B38327141C8DDA9825ECA23_H
#ifndef EXPRESSIONEXTENSION_TAA67E9752E30C5AE5608378246B38AA4A29A7580_H
#define EXPRESSIONEXTENSION_TAA67E9752E30C5AE5608378246B38AA4A29A7580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ExpressionExtension
struct  ExpressionExtension_tAA67E9752E30C5AE5608378246B38AA4A29A7580  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONEXTENSION_TAA67E9752E30C5AE5608378246B38AA4A29A7580_H
#ifndef EXPRESSIONVISITOR_T2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF_H
#define EXPRESSIONVISITOR_T2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ExpressionVisitor
struct  ExpressionVisitor_t2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONVISITOR_T2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef BINARYEXPRESSION_T10B903ED77052B1B3CB73F0902051D2C5184C9E9_H
#define BINARYEXPRESSION_T10B903ED77052B1B3CB73F0902051D2C5184C9E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.BinaryExpression
struct  BinaryExpression_t10B903ED77052B1B3CB73F0902051D2C5184C9E9  : public Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F
{
public:
	// System.Linq.Expressions.Expression System.Linq.Expressions.BinaryExpression::<Right>k__BackingField
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___U3CRightU3Ek__BackingField_3;
	// System.Linq.Expressions.Expression System.Linq.Expressions.BinaryExpression::<Left>k__BackingField
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___U3CLeftU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CRightU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BinaryExpression_t10B903ED77052B1B3CB73F0902051D2C5184C9E9, ___U3CRightU3Ek__BackingField_3)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get_U3CRightU3Ek__BackingField_3() const { return ___U3CRightU3Ek__BackingField_3; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of_U3CRightU3Ek__BackingField_3() { return &___U3CRightU3Ek__BackingField_3; }
	inline void set_U3CRightU3Ek__BackingField_3(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		___U3CRightU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRightU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CLeftU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BinaryExpression_t10B903ED77052B1B3CB73F0902051D2C5184C9E9, ___U3CLeftU3Ek__BackingField_4)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get_U3CLeftU3Ek__BackingField_4() const { return ___U3CLeftU3Ek__BackingField_4; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of_U3CLeftU3Ek__BackingField_4() { return &___U3CLeftU3Ek__BackingField_4; }
	inline void set_U3CLeftU3Ek__BackingField_4(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		___U3CLeftU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLeftU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYEXPRESSION_T10B903ED77052B1B3CB73F0902051D2C5184C9E9_H
#ifndef BLOCKEXPRESSION_T5613E40B381138E108C2CE3A3B4A53A793460DC0_H
#define BLOCKEXPRESSION_T5613E40B381138E108C2CE3A3B4A53A793460DC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.BlockExpression
struct  BlockExpression_t5613E40B381138E108C2CE3A3B4A53A793460DC0  : public Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKEXPRESSION_T5613E40B381138E108C2CE3A3B4A53A793460DC0_H
#ifndef CONDITIONALEXPRESSION_TF88CC6FADB5DEF42D521C59962BBB5D343CF6EF4_H
#define CONDITIONALEXPRESSION_TF88CC6FADB5DEF42D521C59962BBB5D343CF6EF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ConditionalExpression
struct  ConditionalExpression_tF88CC6FADB5DEF42D521C59962BBB5D343CF6EF4  : public Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F
{
public:
	// System.Linq.Expressions.Expression System.Linq.Expressions.ConditionalExpression::<Test>k__BackingField
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___U3CTestU3Ek__BackingField_3;
	// System.Linq.Expressions.Expression System.Linq.Expressions.ConditionalExpression::<IfTrue>k__BackingField
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___U3CIfTrueU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CTestU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ConditionalExpression_tF88CC6FADB5DEF42D521C59962BBB5D343CF6EF4, ___U3CTestU3Ek__BackingField_3)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get_U3CTestU3Ek__BackingField_3() const { return ___U3CTestU3Ek__BackingField_3; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of_U3CTestU3Ek__BackingField_3() { return &___U3CTestU3Ek__BackingField_3; }
	inline void set_U3CTestU3Ek__BackingField_3(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		___U3CTestU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTestU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CIfTrueU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ConditionalExpression_tF88CC6FADB5DEF42D521C59962BBB5D343CF6EF4, ___U3CIfTrueU3Ek__BackingField_4)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get_U3CIfTrueU3Ek__BackingField_4() const { return ___U3CIfTrueU3Ek__BackingField_4; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of_U3CIfTrueU3Ek__BackingField_4() { return &___U3CIfTrueU3Ek__BackingField_4; }
	inline void set_U3CIfTrueU3Ek__BackingField_4(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		___U3CIfTrueU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIfTrueU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONALEXPRESSION_TF88CC6FADB5DEF42D521C59962BBB5D343CF6EF4_H
#ifndef CONSTANTEXPRESSION_T48A9B671E0EF5A540C1669033E2C58646D6F72AB_H
#define CONSTANTEXPRESSION_T48A9B671E0EF5A540C1669033E2C58646D6F72AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ConstantExpression
struct  ConstantExpression_t48A9B671E0EF5A540C1669033E2C58646D6F72AB  : public Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F
{
public:
	// System.Object System.Linq.Expressions.ConstantExpression::<Value>k__BackingField
	RuntimeObject * ___U3CValueU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ConstantExpression_t48A9B671E0EF5A540C1669033E2C58646D6F72AB, ___U3CValueU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CValueU3Ek__BackingField_3() const { return ___U3CValueU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CValueU3Ek__BackingField_3() { return &___U3CValueU3Ek__BackingField_3; }
	inline void set_U3CValueU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CValueU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTEXPRESSION_T48A9B671E0EF5A540C1669033E2C58646D6F72AB_H
#ifndef DEBUGINFOEXPRESSION_T48CB8618093FCD434E886962EF166714B45FD013_H
#define DEBUGINFOEXPRESSION_T48CB8618093FCD434E886962EF166714B45FD013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.DebugInfoExpression
struct  DebugInfoExpression_t48CB8618093FCD434E886962EF166714B45FD013  : public Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F
{
public:
	// System.Linq.Expressions.SymbolDocumentInfo System.Linq.Expressions.DebugInfoExpression::<Document>k__BackingField
	SymbolDocumentInfo_t860DD18CE6D7420C3D23B30589F174BD655B3190 * ___U3CDocumentU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CDocumentU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DebugInfoExpression_t48CB8618093FCD434E886962EF166714B45FD013, ___U3CDocumentU3Ek__BackingField_3)); }
	inline SymbolDocumentInfo_t860DD18CE6D7420C3D23B30589F174BD655B3190 * get_U3CDocumentU3Ek__BackingField_3() const { return ___U3CDocumentU3Ek__BackingField_3; }
	inline SymbolDocumentInfo_t860DD18CE6D7420C3D23B30589F174BD655B3190 ** get_address_of_U3CDocumentU3Ek__BackingField_3() { return &___U3CDocumentU3Ek__BackingField_3; }
	inline void set_U3CDocumentU3Ek__BackingField_3(SymbolDocumentInfo_t860DD18CE6D7420C3D23B30589F174BD655B3190 * value)
	{
		___U3CDocumentU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDocumentU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGINFOEXPRESSION_T48CB8618093FCD434E886962EF166714B45FD013_H
#ifndef DEFAULTEXPRESSION_T85FE780BF362882EE1632BB7F9F70C0B3AE52DC8_H
#define DEFAULTEXPRESSION_T85FE780BF362882EE1632BB7F9F70C0B3AE52DC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.DefaultExpression
struct  DefaultExpression_t85FE780BF362882EE1632BB7F9F70C0B3AE52DC8  : public Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F
{
public:
	// System.Type System.Linq.Expressions.DefaultExpression::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DefaultExpression_t85FE780BF362882EE1632BB7F9F70C0B3AE52DC8, ___U3CTypeU3Ek__BackingField_3)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_3() const { return ___U3CTypeU3Ek__BackingField_3; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_3() { return &___U3CTypeU3Ek__BackingField_3; }
	inline void set_U3CTypeU3Ek__BackingField_3(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTEXPRESSION_T85FE780BF362882EE1632BB7F9F70C0B3AE52DC8_H
#ifndef DYNAMICEXPRESSION_TA1D1AA206E166DF9C43B60A150E901F1007D388E_H
#define DYNAMICEXPRESSION_TA1D1AA206E166DF9C43B60A150E901F1007D388E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.DynamicExpression
struct  DynamicExpression_tA1D1AA206E166DF9C43B60A150E901F1007D388E  : public Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F
{
public:
	// System.Runtime.CompilerServices.CallSiteBinder System.Linq.Expressions.DynamicExpression::<Binder>k__BackingField
	CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889 * ___U3CBinderU3Ek__BackingField_3;
	// System.Type System.Linq.Expressions.DynamicExpression::<DelegateType>k__BackingField
	Type_t * ___U3CDelegateTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CBinderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DynamicExpression_tA1D1AA206E166DF9C43B60A150E901F1007D388E, ___U3CBinderU3Ek__BackingField_3)); }
	inline CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889 * get_U3CBinderU3Ek__BackingField_3() const { return ___U3CBinderU3Ek__BackingField_3; }
	inline CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889 ** get_address_of_U3CBinderU3Ek__BackingField_3() { return &___U3CBinderU3Ek__BackingField_3; }
	inline void set_U3CBinderU3Ek__BackingField_3(CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889 * value)
	{
		___U3CBinderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBinderU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CDelegateTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DynamicExpression_tA1D1AA206E166DF9C43B60A150E901F1007D388E, ___U3CDelegateTypeU3Ek__BackingField_4)); }
	inline Type_t * get_U3CDelegateTypeU3Ek__BackingField_4() const { return ___U3CDelegateTypeU3Ek__BackingField_4; }
	inline Type_t ** get_address_of_U3CDelegateTypeU3Ek__BackingField_4() { return &___U3CDelegateTypeU3Ek__BackingField_4; }
	inline void set_U3CDelegateTypeU3Ek__BackingField_4(Type_t * value)
	{
		___U3CDelegateTypeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDelegateTypeU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICEXPRESSION_TA1D1AA206E166DF9C43B60A150E901F1007D388E_H
#ifndef DYNAMICEXPRESSIONVISITOR_T7A3AF91DC4DA314063E3421F634C10F17F992ED3_H
#define DYNAMICEXPRESSIONVISITOR_T7A3AF91DC4DA314063E3421F634C10F17F992ED3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.DynamicExpressionVisitor
struct  DynamicExpressionVisitor_t7A3AF91DC4DA314063E3421F634C10F17F992ED3  : public ExpressionVisitor_t2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICEXPRESSIONVISITOR_T7A3AF91DC4DA314063E3421F634C10F17F992ED3_H
#ifndef EXPRESSIONSTRINGBUILDER_T816963EA447C46FC13627E4980472716FAEA1AD2_H
#define EXPRESSIONSTRINGBUILDER_T816963EA447C46FC13627E4980472716FAEA1AD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ExpressionStringBuilder
struct  ExpressionStringBuilder_t816963EA447C46FC13627E4980472716FAEA1AD2  : public ExpressionVisitor_t2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF
{
public:
	// System.Text.StringBuilder System.Linq.Expressions.ExpressionStringBuilder::_out
	StringBuilder_t * ____out_0;
	// System.Collections.Generic.Dictionary`2<System.Object,System.Int32> System.Linq.Expressions.ExpressionStringBuilder::_ids
	Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * ____ids_1;

public:
	inline static int32_t get_offset_of__out_0() { return static_cast<int32_t>(offsetof(ExpressionStringBuilder_t816963EA447C46FC13627E4980472716FAEA1AD2, ____out_0)); }
	inline StringBuilder_t * get__out_0() const { return ____out_0; }
	inline StringBuilder_t ** get_address_of__out_0() { return &____out_0; }
	inline void set__out_0(StringBuilder_t * value)
	{
		____out_0 = value;
		Il2CppCodeGenWriteBarrier((&____out_0), value);
	}

	inline static int32_t get_offset_of__ids_1() { return static_cast<int32_t>(offsetof(ExpressionStringBuilder_t816963EA447C46FC13627E4980472716FAEA1AD2, ____ids_1)); }
	inline Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * get__ids_1() const { return ____ids_1; }
	inline Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A ** get_address_of__ids_1() { return &____ids_1; }
	inline void set__ids_1(Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * value)
	{
		____ids_1 = value;
		Il2CppCodeGenWriteBarrier((&____ids_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONSTRINGBUILDER_T816963EA447C46FC13627E4980472716FAEA1AD2_H
#ifndef ANALYZETYPEISRESULT_T9DD8DECD3DBF5DEBDA925E0DD71955032DD1C7C7_H
#define ANALYZETYPEISRESULT_T9DD8DECD3DBF5DEBDA925E0DD71955032DD1C7C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.AnalyzeTypeIsResult
struct  AnalyzeTypeIsResult_t9DD8DECD3DBF5DEBDA925E0DD71955032DD1C7C7 
{
public:
	// System.Int32 System.Linq.Expressions.AnalyzeTypeIsResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnalyzeTypeIsResult_t9DD8DECD3DBF5DEBDA925E0DD71955032DD1C7C7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYZETYPEISRESULT_T9DD8DECD3DBF5DEBDA925E0DD71955032DD1C7C7_H
#ifndef ASSIGNBINARYEXPRESSION_TA10C5BB91B59F34C469D11B84A8ACD2098253B12_H
#define ASSIGNBINARYEXPRESSION_TA10C5BB91B59F34C469D11B84A8ACD2098253B12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.AssignBinaryExpression
struct  AssignBinaryExpression_tA10C5BB91B59F34C469D11B84A8ACD2098253B12  : public BinaryExpression_t10B903ED77052B1B3CB73F0902051D2C5184C9E9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIGNBINARYEXPRESSION_TA10C5BB91B59F34C469D11B84A8ACD2098253B12_H
#ifndef BLOCK2_TA48FE3FBF982B1955C8AE937CB1EACB42559BE34_H
#define BLOCK2_TA48FE3FBF982B1955C8AE937CB1EACB42559BE34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Block2
struct  Block2_tA48FE3FBF982B1955C8AE937CB1EACB42559BE34  : public BlockExpression_t5613E40B381138E108C2CE3A3B4A53A793460DC0
{
public:
	// System.Object System.Linq.Expressions.Block2::_arg0
	RuntimeObject * ____arg0_3;
	// System.Linq.Expressions.Expression System.Linq.Expressions.Block2::_arg1
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg1_4;

public:
	inline static int32_t get_offset_of__arg0_3() { return static_cast<int32_t>(offsetof(Block2_tA48FE3FBF982B1955C8AE937CB1EACB42559BE34, ____arg0_3)); }
	inline RuntimeObject * get__arg0_3() const { return ____arg0_3; }
	inline RuntimeObject ** get_address_of__arg0_3() { return &____arg0_3; }
	inline void set__arg0_3(RuntimeObject * value)
	{
		____arg0_3 = value;
		Il2CppCodeGenWriteBarrier((&____arg0_3), value);
	}

	inline static int32_t get_offset_of__arg1_4() { return static_cast<int32_t>(offsetof(Block2_tA48FE3FBF982B1955C8AE937CB1EACB42559BE34, ____arg1_4)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg1_4() const { return ____arg1_4; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg1_4() { return &____arg1_4; }
	inline void set__arg1_4(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg1_4 = value;
		Il2CppCodeGenWriteBarrier((&____arg1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCK2_TA48FE3FBF982B1955C8AE937CB1EACB42559BE34_H
#ifndef BLOCK3_TE0D3BE57795B0F21FD162E820DD7F3B783BDFA52_H
#define BLOCK3_TE0D3BE57795B0F21FD162E820DD7F3B783BDFA52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Block3
struct  Block3_tE0D3BE57795B0F21FD162E820DD7F3B783BDFA52  : public BlockExpression_t5613E40B381138E108C2CE3A3B4A53A793460DC0
{
public:
	// System.Object System.Linq.Expressions.Block3::_arg0
	RuntimeObject * ____arg0_3;
	// System.Linq.Expressions.Expression System.Linq.Expressions.Block3::_arg1
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg1_4;
	// System.Linq.Expressions.Expression System.Linq.Expressions.Block3::_arg2
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg2_5;

public:
	inline static int32_t get_offset_of__arg0_3() { return static_cast<int32_t>(offsetof(Block3_tE0D3BE57795B0F21FD162E820DD7F3B783BDFA52, ____arg0_3)); }
	inline RuntimeObject * get__arg0_3() const { return ____arg0_3; }
	inline RuntimeObject ** get_address_of__arg0_3() { return &____arg0_3; }
	inline void set__arg0_3(RuntimeObject * value)
	{
		____arg0_3 = value;
		Il2CppCodeGenWriteBarrier((&____arg0_3), value);
	}

	inline static int32_t get_offset_of__arg1_4() { return static_cast<int32_t>(offsetof(Block3_tE0D3BE57795B0F21FD162E820DD7F3B783BDFA52, ____arg1_4)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg1_4() const { return ____arg1_4; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg1_4() { return &____arg1_4; }
	inline void set__arg1_4(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg1_4 = value;
		Il2CppCodeGenWriteBarrier((&____arg1_4), value);
	}

	inline static int32_t get_offset_of__arg2_5() { return static_cast<int32_t>(offsetof(Block3_tE0D3BE57795B0F21FD162E820DD7F3B783BDFA52, ____arg2_5)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg2_5() const { return ____arg2_5; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg2_5() { return &____arg2_5; }
	inline void set__arg2_5(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg2_5 = value;
		Il2CppCodeGenWriteBarrier((&____arg2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCK3_TE0D3BE57795B0F21FD162E820DD7F3B783BDFA52_H
#ifndef BLOCK4_T9E4E20F6BEC4BAA3E8FC0C736E96901B77DB7820_H
#define BLOCK4_T9E4E20F6BEC4BAA3E8FC0C736E96901B77DB7820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Block4
struct  Block4_t9E4E20F6BEC4BAA3E8FC0C736E96901B77DB7820  : public BlockExpression_t5613E40B381138E108C2CE3A3B4A53A793460DC0
{
public:
	// System.Object System.Linq.Expressions.Block4::_arg0
	RuntimeObject * ____arg0_3;
	// System.Linq.Expressions.Expression System.Linq.Expressions.Block4::_arg1
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg1_4;
	// System.Linq.Expressions.Expression System.Linq.Expressions.Block4::_arg2
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg2_5;
	// System.Linq.Expressions.Expression System.Linq.Expressions.Block4::_arg3
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg3_6;

public:
	inline static int32_t get_offset_of__arg0_3() { return static_cast<int32_t>(offsetof(Block4_t9E4E20F6BEC4BAA3E8FC0C736E96901B77DB7820, ____arg0_3)); }
	inline RuntimeObject * get__arg0_3() const { return ____arg0_3; }
	inline RuntimeObject ** get_address_of__arg0_3() { return &____arg0_3; }
	inline void set__arg0_3(RuntimeObject * value)
	{
		____arg0_3 = value;
		Il2CppCodeGenWriteBarrier((&____arg0_3), value);
	}

	inline static int32_t get_offset_of__arg1_4() { return static_cast<int32_t>(offsetof(Block4_t9E4E20F6BEC4BAA3E8FC0C736E96901B77DB7820, ____arg1_4)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg1_4() const { return ____arg1_4; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg1_4() { return &____arg1_4; }
	inline void set__arg1_4(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg1_4 = value;
		Il2CppCodeGenWriteBarrier((&____arg1_4), value);
	}

	inline static int32_t get_offset_of__arg2_5() { return static_cast<int32_t>(offsetof(Block4_t9E4E20F6BEC4BAA3E8FC0C736E96901B77DB7820, ____arg2_5)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg2_5() const { return ____arg2_5; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg2_5() { return &____arg2_5; }
	inline void set__arg2_5(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg2_5 = value;
		Il2CppCodeGenWriteBarrier((&____arg2_5), value);
	}

	inline static int32_t get_offset_of__arg3_6() { return static_cast<int32_t>(offsetof(Block4_t9E4E20F6BEC4BAA3E8FC0C736E96901B77DB7820, ____arg3_6)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg3_6() const { return ____arg3_6; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg3_6() { return &____arg3_6; }
	inline void set__arg3_6(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg3_6 = value;
		Il2CppCodeGenWriteBarrier((&____arg3_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCK4_T9E4E20F6BEC4BAA3E8FC0C736E96901B77DB7820_H
#ifndef BLOCK5_T629F0CE779BAC80C617115897BF4A40868FDBA6B_H
#define BLOCK5_T629F0CE779BAC80C617115897BF4A40868FDBA6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Block5
struct  Block5_t629F0CE779BAC80C617115897BF4A40868FDBA6B  : public BlockExpression_t5613E40B381138E108C2CE3A3B4A53A793460DC0
{
public:
	// System.Object System.Linq.Expressions.Block5::_arg0
	RuntimeObject * ____arg0_3;
	// System.Linq.Expressions.Expression System.Linq.Expressions.Block5::_arg1
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg1_4;
	// System.Linq.Expressions.Expression System.Linq.Expressions.Block5::_arg2
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg2_5;
	// System.Linq.Expressions.Expression System.Linq.Expressions.Block5::_arg3
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg3_6;
	// System.Linq.Expressions.Expression System.Linq.Expressions.Block5::_arg4
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg4_7;

public:
	inline static int32_t get_offset_of__arg0_3() { return static_cast<int32_t>(offsetof(Block5_t629F0CE779BAC80C617115897BF4A40868FDBA6B, ____arg0_3)); }
	inline RuntimeObject * get__arg0_3() const { return ____arg0_3; }
	inline RuntimeObject ** get_address_of__arg0_3() { return &____arg0_3; }
	inline void set__arg0_3(RuntimeObject * value)
	{
		____arg0_3 = value;
		Il2CppCodeGenWriteBarrier((&____arg0_3), value);
	}

	inline static int32_t get_offset_of__arg1_4() { return static_cast<int32_t>(offsetof(Block5_t629F0CE779BAC80C617115897BF4A40868FDBA6B, ____arg1_4)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg1_4() const { return ____arg1_4; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg1_4() { return &____arg1_4; }
	inline void set__arg1_4(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg1_4 = value;
		Il2CppCodeGenWriteBarrier((&____arg1_4), value);
	}

	inline static int32_t get_offset_of__arg2_5() { return static_cast<int32_t>(offsetof(Block5_t629F0CE779BAC80C617115897BF4A40868FDBA6B, ____arg2_5)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg2_5() const { return ____arg2_5; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg2_5() { return &____arg2_5; }
	inline void set__arg2_5(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg2_5 = value;
		Il2CppCodeGenWriteBarrier((&____arg2_5), value);
	}

	inline static int32_t get_offset_of__arg3_6() { return static_cast<int32_t>(offsetof(Block5_t629F0CE779BAC80C617115897BF4A40868FDBA6B, ____arg3_6)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg3_6() const { return ____arg3_6; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg3_6() { return &____arg3_6; }
	inline void set__arg3_6(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg3_6 = value;
		Il2CppCodeGenWriteBarrier((&____arg3_6), value);
	}

	inline static int32_t get_offset_of__arg4_7() { return static_cast<int32_t>(offsetof(Block5_t629F0CE779BAC80C617115897BF4A40868FDBA6B, ____arg4_7)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg4_7() const { return ____arg4_7; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg4_7() { return &____arg4_7; }
	inline void set__arg4_7(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg4_7 = value;
		Il2CppCodeGenWriteBarrier((&____arg4_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCK5_T629F0CE779BAC80C617115897BF4A40868FDBA6B_H
#ifndef BLOCKN_TC8C8344042ADB359E3EF5647D9DAAC410B587062_H
#define BLOCKN_TC8C8344042ADB359E3EF5647D9DAAC410B587062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.BlockN
struct  BlockN_tC8C8344042ADB359E3EF5647D9DAAC410B587062  : public BlockExpression_t5613E40B381138E108C2CE3A3B4A53A793460DC0
{
public:
	// System.Collections.Generic.IReadOnlyList`1<System.Linq.Expressions.Expression> System.Linq.Expressions.BlockN::_expressions
	RuntimeObject* ____expressions_3;

public:
	inline static int32_t get_offset_of__expressions_3() { return static_cast<int32_t>(offsetof(BlockN_tC8C8344042ADB359E3EF5647D9DAAC410B587062, ____expressions_3)); }
	inline RuntimeObject* get__expressions_3() const { return ____expressions_3; }
	inline RuntimeObject** get_address_of__expressions_3() { return &____expressions_3; }
	inline void set__expressions_3(RuntimeObject* value)
	{
		____expressions_3 = value;
		Il2CppCodeGenWriteBarrier((&____expressions_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKN_TC8C8344042ADB359E3EF5647D9DAAC410B587062_H
#ifndef COALESCECONVERSIONBINARYEXPRESSION_T52C23C0B5C136F115C4D6AB563B9880593444CDB_H
#define COALESCECONVERSIONBINARYEXPRESSION_T52C23C0B5C136F115C4D6AB563B9880593444CDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.CoalesceConversionBinaryExpression
struct  CoalesceConversionBinaryExpression_t52C23C0B5C136F115C4D6AB563B9880593444CDB  : public BinaryExpression_t10B903ED77052B1B3CB73F0902051D2C5184C9E9
{
public:
	// System.Linq.Expressions.LambdaExpression System.Linq.Expressions.CoalesceConversionBinaryExpression::_conversion
	LambdaExpression_t75634233B2F65FAB049A8F4AEB44836CF14F87B4 * ____conversion_5;

public:
	inline static int32_t get_offset_of__conversion_5() { return static_cast<int32_t>(offsetof(CoalesceConversionBinaryExpression_t52C23C0B5C136F115C4D6AB563B9880593444CDB, ____conversion_5)); }
	inline LambdaExpression_t75634233B2F65FAB049A8F4AEB44836CF14F87B4 * get__conversion_5() const { return ____conversion_5; }
	inline LambdaExpression_t75634233B2F65FAB049A8F4AEB44836CF14F87B4 ** get_address_of__conversion_5() { return &____conversion_5; }
	inline void set__conversion_5(LambdaExpression_t75634233B2F65FAB049A8F4AEB44836CF14F87B4 * value)
	{
		____conversion_5 = value;
		Il2CppCodeGenWriteBarrier((&____conversion_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COALESCECONVERSIONBINARYEXPRESSION_T52C23C0B5C136F115C4D6AB563B9880593444CDB_H
#ifndef DYNAMICEXPRESSION1_TBE462E1F453791FAAB35156246407D3DFBCA9712_H
#define DYNAMICEXPRESSION1_TBE462E1F453791FAAB35156246407D3DFBCA9712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.DynamicExpression1
struct  DynamicExpression1_tBE462E1F453791FAAB35156246407D3DFBCA9712  : public DynamicExpression_tA1D1AA206E166DF9C43B60A150E901F1007D388E
{
public:
	// System.Object System.Linq.Expressions.DynamicExpression1::_arg0
	RuntimeObject * ____arg0_5;

public:
	inline static int32_t get_offset_of__arg0_5() { return static_cast<int32_t>(offsetof(DynamicExpression1_tBE462E1F453791FAAB35156246407D3DFBCA9712, ____arg0_5)); }
	inline RuntimeObject * get__arg0_5() const { return ____arg0_5; }
	inline RuntimeObject ** get_address_of__arg0_5() { return &____arg0_5; }
	inline void set__arg0_5(RuntimeObject * value)
	{
		____arg0_5 = value;
		Il2CppCodeGenWriteBarrier((&____arg0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICEXPRESSION1_TBE462E1F453791FAAB35156246407D3DFBCA9712_H
#ifndef DYNAMICEXPRESSION2_T7AFA91A3BE4D76649EDBEE96DE228DCF5C86081E_H
#define DYNAMICEXPRESSION2_T7AFA91A3BE4D76649EDBEE96DE228DCF5C86081E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.DynamicExpression2
struct  DynamicExpression2_t7AFA91A3BE4D76649EDBEE96DE228DCF5C86081E  : public DynamicExpression_tA1D1AA206E166DF9C43B60A150E901F1007D388E
{
public:
	// System.Object System.Linq.Expressions.DynamicExpression2::_arg0
	RuntimeObject * ____arg0_5;
	// System.Linq.Expressions.Expression System.Linq.Expressions.DynamicExpression2::_arg1
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg1_6;

public:
	inline static int32_t get_offset_of__arg0_5() { return static_cast<int32_t>(offsetof(DynamicExpression2_t7AFA91A3BE4D76649EDBEE96DE228DCF5C86081E, ____arg0_5)); }
	inline RuntimeObject * get__arg0_5() const { return ____arg0_5; }
	inline RuntimeObject ** get_address_of__arg0_5() { return &____arg0_5; }
	inline void set__arg0_5(RuntimeObject * value)
	{
		____arg0_5 = value;
		Il2CppCodeGenWriteBarrier((&____arg0_5), value);
	}

	inline static int32_t get_offset_of__arg1_6() { return static_cast<int32_t>(offsetof(DynamicExpression2_t7AFA91A3BE4D76649EDBEE96DE228DCF5C86081E, ____arg1_6)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg1_6() const { return ____arg1_6; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg1_6() { return &____arg1_6; }
	inline void set__arg1_6(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg1_6 = value;
		Il2CppCodeGenWriteBarrier((&____arg1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICEXPRESSION2_T7AFA91A3BE4D76649EDBEE96DE228DCF5C86081E_H
#ifndef DYNAMICEXPRESSION3_TE2F9B99DF93202729C4816B554255FD6980FD30D_H
#define DYNAMICEXPRESSION3_TE2F9B99DF93202729C4816B554255FD6980FD30D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.DynamicExpression3
struct  DynamicExpression3_tE2F9B99DF93202729C4816B554255FD6980FD30D  : public DynamicExpression_tA1D1AA206E166DF9C43B60A150E901F1007D388E
{
public:
	// System.Object System.Linq.Expressions.DynamicExpression3::_arg0
	RuntimeObject * ____arg0_5;
	// System.Linq.Expressions.Expression System.Linq.Expressions.DynamicExpression3::_arg1
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg1_6;
	// System.Linq.Expressions.Expression System.Linq.Expressions.DynamicExpression3::_arg2
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg2_7;

public:
	inline static int32_t get_offset_of__arg0_5() { return static_cast<int32_t>(offsetof(DynamicExpression3_tE2F9B99DF93202729C4816B554255FD6980FD30D, ____arg0_5)); }
	inline RuntimeObject * get__arg0_5() const { return ____arg0_5; }
	inline RuntimeObject ** get_address_of__arg0_5() { return &____arg0_5; }
	inline void set__arg0_5(RuntimeObject * value)
	{
		____arg0_5 = value;
		Il2CppCodeGenWriteBarrier((&____arg0_5), value);
	}

	inline static int32_t get_offset_of__arg1_6() { return static_cast<int32_t>(offsetof(DynamicExpression3_tE2F9B99DF93202729C4816B554255FD6980FD30D, ____arg1_6)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg1_6() const { return ____arg1_6; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg1_6() { return &____arg1_6; }
	inline void set__arg1_6(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg1_6 = value;
		Il2CppCodeGenWriteBarrier((&____arg1_6), value);
	}

	inline static int32_t get_offset_of__arg2_7() { return static_cast<int32_t>(offsetof(DynamicExpression3_tE2F9B99DF93202729C4816B554255FD6980FD30D, ____arg2_7)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg2_7() const { return ____arg2_7; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg2_7() { return &____arg2_7; }
	inline void set__arg2_7(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg2_7 = value;
		Il2CppCodeGenWriteBarrier((&____arg2_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICEXPRESSION3_TE2F9B99DF93202729C4816B554255FD6980FD30D_H
#ifndef DYNAMICEXPRESSION4_T3978F09387B840D1976292109243E5B4A77A200E_H
#define DYNAMICEXPRESSION4_T3978F09387B840D1976292109243E5B4A77A200E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.DynamicExpression4
struct  DynamicExpression4_t3978F09387B840D1976292109243E5B4A77A200E  : public DynamicExpression_tA1D1AA206E166DF9C43B60A150E901F1007D388E
{
public:
	// System.Object System.Linq.Expressions.DynamicExpression4::_arg0
	RuntimeObject * ____arg0_5;
	// System.Linq.Expressions.Expression System.Linq.Expressions.DynamicExpression4::_arg1
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg1_6;
	// System.Linq.Expressions.Expression System.Linq.Expressions.DynamicExpression4::_arg2
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg2_7;
	// System.Linq.Expressions.Expression System.Linq.Expressions.DynamicExpression4::_arg3
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____arg3_8;

public:
	inline static int32_t get_offset_of__arg0_5() { return static_cast<int32_t>(offsetof(DynamicExpression4_t3978F09387B840D1976292109243E5B4A77A200E, ____arg0_5)); }
	inline RuntimeObject * get__arg0_5() const { return ____arg0_5; }
	inline RuntimeObject ** get_address_of__arg0_5() { return &____arg0_5; }
	inline void set__arg0_5(RuntimeObject * value)
	{
		____arg0_5 = value;
		Il2CppCodeGenWriteBarrier((&____arg0_5), value);
	}

	inline static int32_t get_offset_of__arg1_6() { return static_cast<int32_t>(offsetof(DynamicExpression4_t3978F09387B840D1976292109243E5B4A77A200E, ____arg1_6)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg1_6() const { return ____arg1_6; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg1_6() { return &____arg1_6; }
	inline void set__arg1_6(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg1_6 = value;
		Il2CppCodeGenWriteBarrier((&____arg1_6), value);
	}

	inline static int32_t get_offset_of__arg2_7() { return static_cast<int32_t>(offsetof(DynamicExpression4_t3978F09387B840D1976292109243E5B4A77A200E, ____arg2_7)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg2_7() const { return ____arg2_7; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg2_7() { return &____arg2_7; }
	inline void set__arg2_7(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg2_7 = value;
		Il2CppCodeGenWriteBarrier((&____arg2_7), value);
	}

	inline static int32_t get_offset_of__arg3_8() { return static_cast<int32_t>(offsetof(DynamicExpression4_t3978F09387B840D1976292109243E5B4A77A200E, ____arg3_8)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__arg3_8() const { return ____arg3_8; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__arg3_8() { return &____arg3_8; }
	inline void set__arg3_8(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____arg3_8 = value;
		Il2CppCodeGenWriteBarrier((&____arg3_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICEXPRESSION4_T3978F09387B840D1976292109243E5B4A77A200E_H
#ifndef DYNAMICEXPRESSIONN_T36C5CBF6E3265E7F65FDF490BBC25FA793E60284_H
#define DYNAMICEXPRESSIONN_T36C5CBF6E3265E7F65FDF490BBC25FA793E60284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.DynamicExpressionN
struct  DynamicExpressionN_t36C5CBF6E3265E7F65FDF490BBC25FA793E60284  : public DynamicExpression_tA1D1AA206E166DF9C43B60A150E901F1007D388E
{
public:
	// System.Collections.Generic.IReadOnlyList`1<System.Linq.Expressions.Expression> System.Linq.Expressions.DynamicExpressionN::_arguments
	RuntimeObject* ____arguments_5;

public:
	inline static int32_t get_offset_of__arguments_5() { return static_cast<int32_t>(offsetof(DynamicExpressionN_t36C5CBF6E3265E7F65FDF490BBC25FA793E60284, ____arguments_5)); }
	inline RuntimeObject* get__arguments_5() const { return ____arguments_5; }
	inline RuntimeObject** get_address_of__arguments_5() { return &____arguments_5; }
	inline void set__arguments_5(RuntimeObject* value)
	{
		____arguments_5 = value;
		Il2CppCodeGenWriteBarrier((&____arguments_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICEXPRESSIONN_T36C5CBF6E3265E7F65FDF490BBC25FA793E60284_H
#ifndef TRYGETFUNCACTIONARGSRESULT_T67E4ADF583263395AA60BC3E1F60AD45AD3021CA_H
#define TRYGETFUNCACTIONARGSRESULT_T67E4ADF583263395AA60BC3E1F60AD45AD3021CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/TryGetFuncActionArgsResult
struct  TryGetFuncActionArgsResult_t67E4ADF583263395AA60BC3E1F60AD45AD3021CA 
{
public:
	// System.Int32 System.Linq.Expressions.Expression/TryGetFuncActionArgsResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TryGetFuncActionArgsResult_t67E4ADF583263395AA60BC3E1F60AD45AD3021CA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRYGETFUNCACTIONARGSRESULT_T67E4ADF583263395AA60BC3E1F60AD45AD3021CA_H
#ifndef EXPRESSIONTYPE_TA0C19CAF997A8592C84F0EBFE791BC19BF5AC556_H
#define EXPRESSIONTYPE_TA0C19CAF997A8592C84F0EBFE791BC19BF5AC556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ExpressionType
struct  ExpressionType_tA0C19CAF997A8592C84F0EBFE791BC19BF5AC556 
{
public:
	// System.Int32 System.Linq.Expressions.ExpressionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExpressionType_tA0C19CAF997A8592C84F0EBFE791BC19BF5AC556, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONTYPE_TA0C19CAF997A8592C84F0EBFE791BC19BF5AC556_H
#ifndef FULLCONDITIONALEXPRESSION_T979D04DB2166ED9CE05ABAA9B42AB5CFEADB4E42_H
#define FULLCONDITIONALEXPRESSION_T979D04DB2166ED9CE05ABAA9B42AB5CFEADB4E42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.FullConditionalExpression
struct  FullConditionalExpression_t979D04DB2166ED9CE05ABAA9B42AB5CFEADB4E42  : public ConditionalExpression_tF88CC6FADB5DEF42D521C59962BBB5D343CF6EF4
{
public:
	// System.Linq.Expressions.Expression System.Linq.Expressions.FullConditionalExpression::_false
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____false_5;

public:
	inline static int32_t get_offset_of__false_5() { return static_cast<int32_t>(offsetof(FullConditionalExpression_t979D04DB2166ED9CE05ABAA9B42AB5CFEADB4E42, ____false_5)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__false_5() const { return ____false_5; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__false_5() { return &____false_5; }
	inline void set__false_5(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____false_5 = value;
		Il2CppCodeGenWriteBarrier((&____false_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FULLCONDITIONALEXPRESSION_T979D04DB2166ED9CE05ABAA9B42AB5CFEADB4E42_H
#ifndef GOTOEXPRESSIONKIND_T08D7AD2E460A35E36B1F72FFECFF744DE69A469C_H
#define GOTOEXPRESSIONKIND_T08D7AD2E460A35E36B1F72FFECFF744DE69A469C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.GotoExpressionKind
struct  GotoExpressionKind_t08D7AD2E460A35E36B1F72FFECFF744DE69A469C 
{
public:
	// System.Int32 System.Linq.Expressions.GotoExpressionKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GotoExpressionKind_t08D7AD2E460A35E36B1F72FFECFF744DE69A469C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOTOEXPRESSIONKIND_T08D7AD2E460A35E36B1F72FFECFF744DE69A469C_H
#ifndef SCOPEEXPRESSION_TD6E0D12EF687ABE0F3F53511A05F37EF0A48BDC9_H
#define SCOPEEXPRESSION_TD6E0D12EF687ABE0F3F53511A05F37EF0A48BDC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ScopeExpression
struct  ScopeExpression_tD6E0D12EF687ABE0F3F53511A05F37EF0A48BDC9  : public BlockExpression_t5613E40B381138E108C2CE3A3B4A53A793460DC0
{
public:
	// System.Collections.Generic.IReadOnlyList`1<System.Linq.Expressions.ParameterExpression> System.Linq.Expressions.ScopeExpression::_variables
	RuntimeObject* ____variables_3;

public:
	inline static int32_t get_offset_of__variables_3() { return static_cast<int32_t>(offsetof(ScopeExpression_tD6E0D12EF687ABE0F3F53511A05F37EF0A48BDC9, ____variables_3)); }
	inline RuntimeObject* get__variables_3() const { return ____variables_3; }
	inline RuntimeObject** get_address_of__variables_3() { return &____variables_3; }
	inline void set__variables_3(RuntimeObject* value)
	{
		____variables_3 = value;
		Il2CppCodeGenWriteBarrier((&____variables_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPEEXPRESSION_TD6E0D12EF687ABE0F3F53511A05F37EF0A48BDC9_H
#ifndef TYPEDCONSTANTEXPRESSION_TA9579152BD160DCD089AF605E33C55995603AE6E_H
#define TYPEDCONSTANTEXPRESSION_TA9579152BD160DCD089AF605E33C55995603AE6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.TypedConstantExpression
struct  TypedConstantExpression_tA9579152BD160DCD089AF605E33C55995603AE6E  : public ConstantExpression_t48A9B671E0EF5A540C1669033E2C58646D6F72AB
{
public:
	// System.Type System.Linq.Expressions.TypedConstantExpression::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TypedConstantExpression_tA9579152BD160DCD089AF605E33C55995603AE6E, ___U3CTypeU3Ek__BackingField_4)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_4() const { return ___U3CTypeU3Ek__BackingField_4; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_4() { return &___U3CTypeU3Ek__BackingField_4; }
	inline void set_U3CTypeU3Ek__BackingField_4(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDCONSTANTEXPRESSION_TA9579152BD160DCD089AF605E33C55995603AE6E_H
#ifndef EXTENSIONINFO_TCF69A9771A34F70573764381BB67710C64C210EE_H
#define EXTENSIONINFO_TCF69A9771A34F70573764381BB67710C64C210EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/ExtensionInfo
struct  ExtensionInfo_tCF69A9771A34F70573764381BB67710C64C210EE  : public RuntimeObject
{
public:
	// System.Linq.Expressions.ExpressionType System.Linq.Expressions.Expression/ExtensionInfo::NodeType
	int32_t ___NodeType_0;
	// System.Type System.Linq.Expressions.Expression/ExtensionInfo::Type
	Type_t * ___Type_1;

public:
	inline static int32_t get_offset_of_NodeType_0() { return static_cast<int32_t>(offsetof(ExtensionInfo_tCF69A9771A34F70573764381BB67710C64C210EE, ___NodeType_0)); }
	inline int32_t get_NodeType_0() const { return ___NodeType_0; }
	inline int32_t* get_address_of_NodeType_0() { return &___NodeType_0; }
	inline void set_NodeType_0(int32_t value)
	{
		___NodeType_0 = value;
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(ExtensionInfo_tCF69A9771A34F70573764381BB67710C64C210EE, ___Type_1)); }
	inline Type_t * get_Type_1() const { return ___Type_1; }
	inline Type_t ** get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(Type_t * value)
	{
		___Type_1 = value;
		Il2CppCodeGenWriteBarrier((&___Type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONINFO_TCF69A9771A34F70573764381BB67710C64C210EE_H
#ifndef FULLCONDITIONALEXPRESSIONWITHTYPE_TDAD020052F826C0FEC8E514FAE5F301AC86CBBA2_H
#define FULLCONDITIONALEXPRESSIONWITHTYPE_TDAD020052F826C0FEC8E514FAE5F301AC86CBBA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.FullConditionalExpressionWithType
struct  FullConditionalExpressionWithType_tDAD020052F826C0FEC8E514FAE5F301AC86CBBA2  : public FullConditionalExpression_t979D04DB2166ED9CE05ABAA9B42AB5CFEADB4E42
{
public:
	// System.Type System.Linq.Expressions.FullConditionalExpressionWithType::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FullConditionalExpressionWithType_tDAD020052F826C0FEC8E514FAE5F301AC86CBBA2, ___U3CTypeU3Ek__BackingField_6)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_6() const { return ___U3CTypeU3Ek__BackingField_6; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_6() { return &___U3CTypeU3Ek__BackingField_6; }
	inline void set_U3CTypeU3Ek__BackingField_6(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FULLCONDITIONALEXPRESSIONWITHTYPE_TDAD020052F826C0FEC8E514FAE5F301AC86CBBA2_H
#ifndef LOGICALBINARYEXPRESSION_T38BDD4B5DFFC68697CDC74A9DCE2ED26EEB2430F_H
#define LOGICALBINARYEXPRESSION_T38BDD4B5DFFC68697CDC74A9DCE2ED26EEB2430F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.LogicalBinaryExpression
struct  LogicalBinaryExpression_t38BDD4B5DFFC68697CDC74A9DCE2ED26EEB2430F  : public BinaryExpression_t10B903ED77052B1B3CB73F0902051D2C5184C9E9
{
public:
	// System.Linq.Expressions.ExpressionType System.Linq.Expressions.LogicalBinaryExpression::<NodeType>k__BackingField
	int32_t ___U3CNodeTypeU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CNodeTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LogicalBinaryExpression_t38BDD4B5DFFC68697CDC74A9DCE2ED26EEB2430F, ___U3CNodeTypeU3Ek__BackingField_5)); }
	inline int32_t get_U3CNodeTypeU3Ek__BackingField_5() const { return ___U3CNodeTypeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CNodeTypeU3Ek__BackingField_5() { return &___U3CNodeTypeU3Ek__BackingField_5; }
	inline void set_U3CNodeTypeU3Ek__BackingField_5(int32_t value)
	{
		___U3CNodeTypeU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGICALBINARYEXPRESSION_T38BDD4B5DFFC68697CDC74A9DCE2ED26EEB2430F_H
#ifndef SCOPE1_T9CBA8E80F40CE5572A9179EDA3332E46CCFB4B47_H
#define SCOPE1_T9CBA8E80F40CE5572A9179EDA3332E46CCFB4B47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Scope1
struct  Scope1_t9CBA8E80F40CE5572A9179EDA3332E46CCFB4B47  : public ScopeExpression_tD6E0D12EF687ABE0F3F53511A05F37EF0A48BDC9
{
public:
	// System.Object System.Linq.Expressions.Scope1::_body
	RuntimeObject * ____body_4;

public:
	inline static int32_t get_offset_of__body_4() { return static_cast<int32_t>(offsetof(Scope1_t9CBA8E80F40CE5572A9179EDA3332E46CCFB4B47, ____body_4)); }
	inline RuntimeObject * get__body_4() const { return ____body_4; }
	inline RuntimeObject ** get_address_of__body_4() { return &____body_4; }
	inline void set__body_4(RuntimeObject * value)
	{
		____body_4 = value;
		Il2CppCodeGenWriteBarrier((&____body_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPE1_T9CBA8E80F40CE5572A9179EDA3332E46CCFB4B47_H
#ifndef SCOPEN_TB43D682920BD7ACC0659E6FB2C57788B2DFE05AC_H
#define SCOPEN_TB43D682920BD7ACC0659E6FB2C57788B2DFE05AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ScopeN
struct  ScopeN_tB43D682920BD7ACC0659E6FB2C57788B2DFE05AC  : public ScopeExpression_tD6E0D12EF687ABE0F3F53511A05F37EF0A48BDC9
{
public:
	// System.Collections.Generic.IReadOnlyList`1<System.Linq.Expressions.Expression> System.Linq.Expressions.ScopeN::_body
	RuntimeObject* ____body_4;

public:
	inline static int32_t get_offset_of__body_4() { return static_cast<int32_t>(offsetof(ScopeN_tB43D682920BD7ACC0659E6FB2C57788B2DFE05AC, ____body_4)); }
	inline RuntimeObject* get__body_4() const { return ____body_4; }
	inline RuntimeObject** get_address_of__body_4() { return &____body_4; }
	inline void set__body_4(RuntimeObject* value)
	{
		____body_4 = value;
		Il2CppCodeGenWriteBarrier((&____body_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPEN_TB43D682920BD7ACC0659E6FB2C57788B2DFE05AC_H
#ifndef SIMPLEBINARYEXPRESSION_TA0B2CEF87A6757C15E7491F83BDFF2E3A2509137_H
#define SIMPLEBINARYEXPRESSION_TA0B2CEF87A6757C15E7491F83BDFF2E3A2509137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.SimpleBinaryExpression
struct  SimpleBinaryExpression_tA0B2CEF87A6757C15E7491F83BDFF2E3A2509137  : public BinaryExpression_t10B903ED77052B1B3CB73F0902051D2C5184C9E9
{
public:
	// System.Linq.Expressions.ExpressionType System.Linq.Expressions.SimpleBinaryExpression::<NodeType>k__BackingField
	int32_t ___U3CNodeTypeU3Ek__BackingField_5;
	// System.Type System.Linq.Expressions.SimpleBinaryExpression::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CNodeTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SimpleBinaryExpression_tA0B2CEF87A6757C15E7491F83BDFF2E3A2509137, ___U3CNodeTypeU3Ek__BackingField_5)); }
	inline int32_t get_U3CNodeTypeU3Ek__BackingField_5() const { return ___U3CNodeTypeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CNodeTypeU3Ek__BackingField_5() { return &___U3CNodeTypeU3Ek__BackingField_5; }
	inline void set_U3CNodeTypeU3Ek__BackingField_5(int32_t value)
	{
		___U3CNodeTypeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SimpleBinaryExpression_tA0B2CEF87A6757C15E7491F83BDFF2E3A2509137, ___U3CTypeU3Ek__BackingField_6)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_6() const { return ___U3CTypeU3Ek__BackingField_6; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_6() { return &___U3CTypeU3Ek__BackingField_6; }
	inline void set_U3CTypeU3Ek__BackingField_6(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEBINARYEXPRESSION_TA0B2CEF87A6757C15E7491F83BDFF2E3A2509137_H
#ifndef TYPEDDYNAMICEXPRESSION1_TC7F19F798B67838B12778AC72E3192C5157D5975_H
#define TYPEDDYNAMICEXPRESSION1_TC7F19F798B67838B12778AC72E3192C5157D5975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.TypedDynamicExpression1
struct  TypedDynamicExpression1_tC7F19F798B67838B12778AC72E3192C5157D5975  : public DynamicExpression1_tBE462E1F453791FAAB35156246407D3DFBCA9712
{
public:
	// System.Type System.Linq.Expressions.TypedDynamicExpression1::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TypedDynamicExpression1_tC7F19F798B67838B12778AC72E3192C5157D5975, ___U3CTypeU3Ek__BackingField_6)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_6() const { return ___U3CTypeU3Ek__BackingField_6; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_6() { return &___U3CTypeU3Ek__BackingField_6; }
	inline void set_U3CTypeU3Ek__BackingField_6(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDDYNAMICEXPRESSION1_TC7F19F798B67838B12778AC72E3192C5157D5975_H
#ifndef TYPEDDYNAMICEXPRESSION2_TB87FA130890912873FBB78503F63A45C6CD66A2E_H
#define TYPEDDYNAMICEXPRESSION2_TB87FA130890912873FBB78503F63A45C6CD66A2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.TypedDynamicExpression2
struct  TypedDynamicExpression2_tB87FA130890912873FBB78503F63A45C6CD66A2E  : public DynamicExpression2_t7AFA91A3BE4D76649EDBEE96DE228DCF5C86081E
{
public:
	// System.Type System.Linq.Expressions.TypedDynamicExpression2::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TypedDynamicExpression2_tB87FA130890912873FBB78503F63A45C6CD66A2E, ___U3CTypeU3Ek__BackingField_7)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_7() const { return ___U3CTypeU3Ek__BackingField_7; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_7() { return &___U3CTypeU3Ek__BackingField_7; }
	inline void set_U3CTypeU3Ek__BackingField_7(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDDYNAMICEXPRESSION2_TB87FA130890912873FBB78503F63A45C6CD66A2E_H
#ifndef TYPEDDYNAMICEXPRESSION3_TA9DBA3E00A96B7F0B5150B7697023553B27FCD23_H
#define TYPEDDYNAMICEXPRESSION3_TA9DBA3E00A96B7F0B5150B7697023553B27FCD23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.TypedDynamicExpression3
struct  TypedDynamicExpression3_tA9DBA3E00A96B7F0B5150B7697023553B27FCD23  : public DynamicExpression3_tE2F9B99DF93202729C4816B554255FD6980FD30D
{
public:
	// System.Type System.Linq.Expressions.TypedDynamicExpression3::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TypedDynamicExpression3_tA9DBA3E00A96B7F0B5150B7697023553B27FCD23, ___U3CTypeU3Ek__BackingField_8)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_8() const { return ___U3CTypeU3Ek__BackingField_8; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_8() { return &___U3CTypeU3Ek__BackingField_8; }
	inline void set_U3CTypeU3Ek__BackingField_8(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDDYNAMICEXPRESSION3_TA9DBA3E00A96B7F0B5150B7697023553B27FCD23_H
#ifndef TYPEDDYNAMICEXPRESSION4_T065E0D4637723B1E0FEA1351E7B146AE5B158F3C_H
#define TYPEDDYNAMICEXPRESSION4_T065E0D4637723B1E0FEA1351E7B146AE5B158F3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.TypedDynamicExpression4
struct  TypedDynamicExpression4_t065E0D4637723B1E0FEA1351E7B146AE5B158F3C  : public DynamicExpression4_t3978F09387B840D1976292109243E5B4A77A200E
{
public:
	// System.Type System.Linq.Expressions.TypedDynamicExpression4::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TypedDynamicExpression4_t065E0D4637723B1E0FEA1351E7B146AE5B158F3C, ___U3CTypeU3Ek__BackingField_9)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_9() const { return ___U3CTypeU3Ek__BackingField_9; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_9() { return &___U3CTypeU3Ek__BackingField_9; }
	inline void set_U3CTypeU3Ek__BackingField_9(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDDYNAMICEXPRESSION4_T065E0D4637723B1E0FEA1351E7B146AE5B158F3C_H
#ifndef TYPEDDYNAMICEXPRESSIONN_T0FB1AE8CA308EF208C2FD0F992B234A9AA7B9AA9_H
#define TYPEDDYNAMICEXPRESSIONN_T0FB1AE8CA308EF208C2FD0F992B234A9AA7B9AA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.TypedDynamicExpressionN
struct  TypedDynamicExpressionN_t0FB1AE8CA308EF208C2FD0F992B234A9AA7B9AA9  : public DynamicExpressionN_t36C5CBF6E3265E7F65FDF490BBC25FA793E60284
{
public:
	// System.Type System.Linq.Expressions.TypedDynamicExpressionN::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TypedDynamicExpressionN_t0FB1AE8CA308EF208C2FD0F992B234A9AA7B9AA9, ___U3CTypeU3Ek__BackingField_6)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_6() const { return ___U3CTypeU3Ek__BackingField_6; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_6() { return &___U3CTypeU3Ek__BackingField_6; }
	inline void set_U3CTypeU3Ek__BackingField_6(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDDYNAMICEXPRESSIONN_T0FB1AE8CA308EF208C2FD0F992B234A9AA7B9AA9_H
#ifndef METHODBINARYEXPRESSION_T5F8B65E7C0CEE266C90492E1FF7E96E6F4FB0794_H
#define METHODBINARYEXPRESSION_T5F8B65E7C0CEE266C90492E1FF7E96E6F4FB0794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.MethodBinaryExpression
struct  MethodBinaryExpression_t5F8B65E7C0CEE266C90492E1FF7E96E6F4FB0794  : public SimpleBinaryExpression_tA0B2CEF87A6757C15E7491F83BDFF2E3A2509137
{
public:
	// System.Reflection.MethodInfo System.Linq.Expressions.MethodBinaryExpression::_method
	MethodInfo_t * ____method_7;

public:
	inline static int32_t get_offset_of__method_7() { return static_cast<int32_t>(offsetof(MethodBinaryExpression_t5F8B65E7C0CEE266C90492E1FF7E96E6F4FB0794, ____method_7)); }
	inline MethodInfo_t * get__method_7() const { return ____method_7; }
	inline MethodInfo_t ** get_address_of__method_7() { return &____method_7; }
	inline void set__method_7(MethodInfo_t * value)
	{
		____method_7 = value;
		Il2CppCodeGenWriteBarrier((&____method_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBINARYEXPRESSION_T5F8B65E7C0CEE266C90492E1FF7E96E6F4FB0794_H
#ifndef SCOPEWITHTYPE_T37D938AB6A4DB40DB8B0E696D153ABB09F4B5830_H
#define SCOPEWITHTYPE_T37D938AB6A4DB40DB8B0E696D153ABB09F4B5830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ScopeWithType
struct  ScopeWithType_t37D938AB6A4DB40DB8B0E696D153ABB09F4B5830  : public ScopeN_tB43D682920BD7ACC0659E6FB2C57788B2DFE05AC
{
public:
	// System.Type System.Linq.Expressions.ScopeWithType::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ScopeWithType_t37D938AB6A4DB40DB8B0E696D153ABB09F4B5830, ___U3CTypeU3Ek__BackingField_5)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_5() const { return ___U3CTypeU3Ek__BackingField_5; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_5() { return &___U3CTypeU3Ek__BackingField_5; }
	inline void set_U3CTypeU3Ek__BackingField_5(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPEWITHTYPE_T37D938AB6A4DB40DB8B0E696D153ABB09F4B5830_H
#ifndef OPASSIGNMETHODCONVERSIONBINARYEXPRESSION_TE0D8CA16C6C7D92D5792D1127703B268A29D3DF7_H
#define OPASSIGNMETHODCONVERSIONBINARYEXPRESSION_TE0D8CA16C6C7D92D5792D1127703B268A29D3DF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.OpAssignMethodConversionBinaryExpression
struct  OpAssignMethodConversionBinaryExpression_tE0D8CA16C6C7D92D5792D1127703B268A29D3DF7  : public MethodBinaryExpression_t5F8B65E7C0CEE266C90492E1FF7E96E6F4FB0794
{
public:
	// System.Linq.Expressions.LambdaExpression System.Linq.Expressions.OpAssignMethodConversionBinaryExpression::_conversion
	LambdaExpression_t75634233B2F65FAB049A8F4AEB44836CF14F87B4 * ____conversion_8;

public:
	inline static int32_t get_offset_of__conversion_8() { return static_cast<int32_t>(offsetof(OpAssignMethodConversionBinaryExpression_tE0D8CA16C6C7D92D5792D1127703B268A29D3DF7, ____conversion_8)); }
	inline LambdaExpression_t75634233B2F65FAB049A8F4AEB44836CF14F87B4 * get__conversion_8() const { return ____conversion_8; }
	inline LambdaExpression_t75634233B2F65FAB049A8F4AEB44836CF14F87B4 ** get_address_of__conversion_8() { return &____conversion_8; }
	inline void set__conversion_8(LambdaExpression_t75634233B2F65FAB049A8F4AEB44836CF14F87B4 * value)
	{
		____conversion_8 = value;
		Il2CppCodeGenWriteBarrier((&____conversion_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPASSIGNMETHODCONVERSIONBINARYEXPRESSION_TE0D8CA16C6C7D92D5792D1127703B268A29D3DF7_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3100[8] = 
{
	U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450::get_offset_of_U3CU3E1__state_0(),
	U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450::get_offset_of_U3CU3E2__current_1(),
	U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450::get_offset_of_start_3(),
	U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450::get_offset_of_U3CU3E3__start_4(),
	U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450::get_offset_of_U3CiU3E5__1_5(),
	U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450::get_offset_of_count_6(),
	U3CRangeIteratorU3Ed__115_tF33889E2B64BDD3CCCF967BEFB8E2C7BBF10C450::get_offset_of_U3CU3E3__count_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3101[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3102[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3104[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3108[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3109[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3110[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3111[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3112[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3113[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3114[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3115[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3116[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3117[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3119[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3120[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14), -1, sizeof(CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3121[13] = 
{
	CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields::get_offset_of_s_CallSiteOps_SetNotMatched_0(),
	CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields::get_offset_of_s_CallSiteOps_CreateMatchmaker_1(),
	CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields::get_offset_of_s_CallSiteOps_GetMatch_2(),
	CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields::get_offset_of_s_CallSiteOps_ClearMatch_3(),
	CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields::get_offset_of_s_CallSiteOps_UpdateRules_4(),
	CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields::get_offset_of_s_CallSiteOps_GetRules_5(),
	CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields::get_offset_of_s_CallSiteOps_GetRuleCache_6(),
	CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields::get_offset_of_s_CallSiteOps_GetCachedRules_7(),
	CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields::get_offset_of_s_CallSiteOps_AddRule_8(),
	CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields::get_offset_of_s_CallSiteOps_MoveRule_9(),
	CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields::get_offset_of_s_CallSiteOps_Bind_10(),
	CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields::get_offset_of_s_String_op_Equality_String_String_11(),
	CachedReflectionInfo_tE1112A2DFD32FA5E64F83C4BCC993D8D85399E14_StaticFields::get_offset_of_s_Math_Pow_Double_Double_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (BinaryExpression_t10B903ED77052B1B3CB73F0902051D2C5184C9E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3122[2] = 
{
	BinaryExpression_t10B903ED77052B1B3CB73F0902051D2C5184C9E9::get_offset_of_U3CRightU3Ek__BackingField_3(),
	BinaryExpression_t10B903ED77052B1B3CB73F0902051D2C5184C9E9::get_offset_of_U3CLeftU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (LogicalBinaryExpression_t38BDD4B5DFFC68697CDC74A9DCE2ED26EEB2430F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3123[1] = 
{
	LogicalBinaryExpression_t38BDD4B5DFFC68697CDC74A9DCE2ED26EEB2430F::get_offset_of_U3CNodeTypeU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (AssignBinaryExpression_tA10C5BB91B59F34C469D11B84A8ACD2098253B12), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (CoalesceConversionBinaryExpression_t52C23C0B5C136F115C4D6AB563B9880593444CDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3125[1] = 
{
	CoalesceConversionBinaryExpression_t52C23C0B5C136F115C4D6AB563B9880593444CDB::get_offset_of__conversion_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (OpAssignMethodConversionBinaryExpression_tE0D8CA16C6C7D92D5792D1127703B268A29D3DF7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3126[1] = 
{
	OpAssignMethodConversionBinaryExpression_tE0D8CA16C6C7D92D5792D1127703B268A29D3DF7::get_offset_of__conversion_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (SimpleBinaryExpression_tA0B2CEF87A6757C15E7491F83BDFF2E3A2509137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3127[2] = 
{
	SimpleBinaryExpression_tA0B2CEF87A6757C15E7491F83BDFF2E3A2509137::get_offset_of_U3CNodeTypeU3Ek__BackingField_5(),
	SimpleBinaryExpression_tA0B2CEF87A6757C15E7491F83BDFF2E3A2509137::get_offset_of_U3CTypeU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (MethodBinaryExpression_t5F8B65E7C0CEE266C90492E1FF7E96E6F4FB0794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3128[1] = 
{
	MethodBinaryExpression_t5F8B65E7C0CEE266C90492E1FF7E96E6F4FB0794::get_offset_of__method_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F), -1, sizeof(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3129[3] = 
{
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F_StaticFields::get_offset_of_s_lambdaDelegateCache_0(),
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F_StaticFields::get_offset_of_s_lambdaFactories_1(),
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F_StaticFields::get_offset_of_s_legacyCtorSupportTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (BinaryExpressionProxy_t4E56E8621F86F80B1A2CABA5B1CE67B745666B9A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (BlockExpressionProxy_t2BDE714E12A8469B6270A9CA6EA9D6C117008668), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { sizeof (CatchBlockProxy_tE843ACD1ED306F2731F7E4ECA09E784617E9D191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (ConditionalExpressionProxy_t0A38D650C4FAC80C22E6B4708AB1BDF82702CB07), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { sizeof (ConstantExpressionProxy_tF7144753C8EEEE622B9BF8CCAFA6CF6AD2CE30CA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (DebugInfoExpressionProxy_t79D7C20C51AC2950AA6745CE0B351F3618EB1845), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { sizeof (DefaultExpressionProxy_t0AF31C51AE8334247CA292F19A90C69D164C8AD1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { sizeof (GotoExpressionProxy_t9E397A25F622DE41C3026F2CF3BC09040D055CDB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (IndexExpressionProxy_t73B0DA06C5FCCADF6607D6D0A76D489950C2454A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (InvocationExpressionProxy_tDBD0FCF2EC385080B0D0976D98AE58E7668F8156), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (LabelExpressionProxy_tE5634DF21429942EF34929AAA7D6A1F47886E900), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (LambdaExpressionProxy_t0329823C7F8CB84A18FB01862E7E2329FF609B58), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (ListInitExpressionProxy_t0C544125FB1E83286C918EE26017723365C87DF8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (LoopExpressionProxy_t7A0A1B87DB30C79F9CA434BB3EABFBE713FC5DEC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (MemberExpressionProxy_t6C2D8472505AFBA73202F9FCB520FA1E1ED8D100), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (MemberInitExpressionProxy_t93AA89F3681DD86FDAAEE48525258AF236B1E955), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (MethodCallExpressionProxy_t114DC6C2F53B7EF7198D540C80056F8EB50C6146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (NewArrayExpressionProxy_tC522CFC96FA9CEDAC4CA25C92E6536E501E1C316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (NewExpressionProxy_t9B725808DF48067DADF750F253F42CC8D73D95CE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (ParameterExpressionProxy_tBB89F4DCCB4CF297D8C33D4CF920F0F253C8B689), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (RuntimeVariablesExpressionProxy_tE5623A2468F383D7DE36202A05420853F9AA3330), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (SwitchCaseProxy_t2D879E7D31FBC5B5705F02D9C0050BA418A27A32), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (SwitchExpressionProxy_t25D440423D0E4BBF67C885BE102DCC9A5F288DE6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { sizeof (TryExpressionProxy_t03B4CBC9EB77A07874851B7A65B32A6C5AD8213C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { sizeof (TypeBinaryExpressionProxy_t4CC1003894A2968865169ADCF54A90B4166C434B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { sizeof (UnaryExpressionProxy_tC967CC148CB23EF56B38327141C8DDA9825ECA23), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { sizeof (ExtensionInfo_tCF69A9771A34F70573764381BB67710C64C210EE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3156[2] = 
{
	ExtensionInfo_tCF69A9771A34F70573764381BB67710C64C210EE::get_offset_of_NodeType_0(),
	ExtensionInfo_tCF69A9771A34F70573764381BB67710C64C210EE::get_offset_of_Type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { sizeof (TryGetFuncActionArgsResult_t67E4ADF583263395AA60BC3E1F60AD45AD3021CA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3157[5] = 
{
	TryGetFuncActionArgsResult_t67E4ADF583263395AA60BC3E1F60AD45AD3021CA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { sizeof (BlockExpression_t5613E40B381138E108C2CE3A3B4A53A793460DC0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { sizeof (Block2_tA48FE3FBF982B1955C8AE937CB1EACB42559BE34), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3159[2] = 
{
	Block2_tA48FE3FBF982B1955C8AE937CB1EACB42559BE34::get_offset_of__arg0_3(),
	Block2_tA48FE3FBF982B1955C8AE937CB1EACB42559BE34::get_offset_of__arg1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (Block3_tE0D3BE57795B0F21FD162E820DD7F3B783BDFA52), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3160[3] = 
{
	Block3_tE0D3BE57795B0F21FD162E820DD7F3B783BDFA52::get_offset_of__arg0_3(),
	Block3_tE0D3BE57795B0F21FD162E820DD7F3B783BDFA52::get_offset_of__arg1_4(),
	Block3_tE0D3BE57795B0F21FD162E820DD7F3B783BDFA52::get_offset_of__arg2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (Block4_t9E4E20F6BEC4BAA3E8FC0C736E96901B77DB7820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3161[4] = 
{
	Block4_t9E4E20F6BEC4BAA3E8FC0C736E96901B77DB7820::get_offset_of__arg0_3(),
	Block4_t9E4E20F6BEC4BAA3E8FC0C736E96901B77DB7820::get_offset_of__arg1_4(),
	Block4_t9E4E20F6BEC4BAA3E8FC0C736E96901B77DB7820::get_offset_of__arg2_5(),
	Block4_t9E4E20F6BEC4BAA3E8FC0C736E96901B77DB7820::get_offset_of__arg3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (Block5_t629F0CE779BAC80C617115897BF4A40868FDBA6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3162[5] = 
{
	Block5_t629F0CE779BAC80C617115897BF4A40868FDBA6B::get_offset_of__arg0_3(),
	Block5_t629F0CE779BAC80C617115897BF4A40868FDBA6B::get_offset_of__arg1_4(),
	Block5_t629F0CE779BAC80C617115897BF4A40868FDBA6B::get_offset_of__arg2_5(),
	Block5_t629F0CE779BAC80C617115897BF4A40868FDBA6B::get_offset_of__arg3_6(),
	Block5_t629F0CE779BAC80C617115897BF4A40868FDBA6B::get_offset_of__arg4_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { sizeof (BlockN_tC8C8344042ADB359E3EF5647D9DAAC410B587062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3163[1] = 
{
	BlockN_tC8C8344042ADB359E3EF5647D9DAAC410B587062::get_offset_of__expressions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (ScopeExpression_tD6E0D12EF687ABE0F3F53511A05F37EF0A48BDC9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3164[1] = 
{
	ScopeExpression_tD6E0D12EF687ABE0F3F53511A05F37EF0A48BDC9::get_offset_of__variables_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (Scope1_t9CBA8E80F40CE5572A9179EDA3332E46CCFB4B47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3165[1] = 
{
	Scope1_t9CBA8E80F40CE5572A9179EDA3332E46CCFB4B47::get_offset_of__body_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { sizeof (ScopeN_tB43D682920BD7ACC0659E6FB2C57788B2DFE05AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3166[1] = 
{
	ScopeN_tB43D682920BD7ACC0659E6FB2C57788B2DFE05AC::get_offset_of__body_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { sizeof (ScopeWithType_t37D938AB6A4DB40DB8B0E696D153ABB09F4B5830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3167[1] = 
{
	ScopeWithType_t37D938AB6A4DB40DB8B0E696D153ABB09F4B5830::get_offset_of_U3CTypeU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { sizeof (BlockExpressionList_t08F678D1B58A8042C6F0855C640B36006A44D55A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3168[2] = 
{
	BlockExpressionList_t08F678D1B58A8042C6F0855C640B36006A44D55A::get_offset_of__block_0(),
	BlockExpressionList_t08F678D1B58A8042C6F0855C640B36006A44D55A::get_offset_of__arg0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { sizeof (U3CGetEnumeratorU3Ed__18_tF7F7D15CFAB0C836E345FA8DD20482647A01C49C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3169[4] = 
{
	U3CGetEnumeratorU3Ed__18_tF7F7D15CFAB0C836E345FA8DD20482647A01C49C::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__18_tF7F7D15CFAB0C836E345FA8DD20482647A01C49C::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__18_tF7F7D15CFAB0C836E345FA8DD20482647A01C49C::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__18_tF7F7D15CFAB0C836E345FA8DD20482647A01C49C::get_offset_of_U3CiU3E5__1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { sizeof (CatchBlock_t56D3C700B21707DF484608E619F354C25B2D7E64), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3170[4] = 
{
	CatchBlock_t56D3C700B21707DF484608E619F354C25B2D7E64::get_offset_of_U3CVariableU3Ek__BackingField_0(),
	CatchBlock_t56D3C700B21707DF484608E619F354C25B2D7E64::get_offset_of_U3CTestU3Ek__BackingField_1(),
	CatchBlock_t56D3C700B21707DF484608E619F354C25B2D7E64::get_offset_of_U3CBodyU3Ek__BackingField_2(),
	CatchBlock_t56D3C700B21707DF484608E619F354C25B2D7E64::get_offset_of_U3CFilterU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { sizeof (ArrayBuilderExtensions_tFD672AC010C3B563A707E8D154317702B571E8BF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { sizeof (AnalyzeTypeIsResult_t9DD8DECD3DBF5DEBDA925E0DD71955032DD1C7C7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3172[5] = 
{
	AnalyzeTypeIsResult_t9DD8DECD3DBF5DEBDA925E0DD71955032DD1C7C7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (ConstantCheck_tFFE62F1408427AE7354DC3E4AA7017FCAAFEFE8D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (ConditionalExpression_tF88CC6FADB5DEF42D521C59962BBB5D343CF6EF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3174[2] = 
{
	ConditionalExpression_tF88CC6FADB5DEF42D521C59962BBB5D343CF6EF4::get_offset_of_U3CTestU3Ek__BackingField_3(),
	ConditionalExpression_tF88CC6FADB5DEF42D521C59962BBB5D343CF6EF4::get_offset_of_U3CIfTrueU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { sizeof (FullConditionalExpression_t979D04DB2166ED9CE05ABAA9B42AB5CFEADB4E42), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3175[1] = 
{
	FullConditionalExpression_t979D04DB2166ED9CE05ABAA9B42AB5CFEADB4E42::get_offset_of__false_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { sizeof (FullConditionalExpressionWithType_tDAD020052F826C0FEC8E514FAE5F301AC86CBBA2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3176[1] = 
{
	FullConditionalExpressionWithType_tDAD020052F826C0FEC8E514FAE5F301AC86CBBA2::get_offset_of_U3CTypeU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { sizeof (ConstantExpression_t48A9B671E0EF5A540C1669033E2C58646D6F72AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3177[1] = 
{
	ConstantExpression_t48A9B671E0EF5A540C1669033E2C58646D6F72AB::get_offset_of_U3CValueU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { sizeof (TypedConstantExpression_tA9579152BD160DCD089AF605E33C55995603AE6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3178[1] = 
{
	TypedConstantExpression_tA9579152BD160DCD089AF605E33C55995603AE6E::get_offset_of_U3CTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { sizeof (DebugInfoExpression_t48CB8618093FCD434E886962EF166714B45FD013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3179[1] = 
{
	DebugInfoExpression_t48CB8618093FCD434E886962EF166714B45FD013::get_offset_of_U3CDocumentU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { sizeof (DefaultExpression_t85FE780BF362882EE1632BB7F9F70C0B3AE52DC8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3180[1] = 
{
	DefaultExpression_t85FE780BF362882EE1632BB7F9F70C0B3AE52DC8::get_offset_of_U3CTypeU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { sizeof (DynamicExpression_tA1D1AA206E166DF9C43B60A150E901F1007D388E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3181[2] = 
{
	DynamicExpression_tA1D1AA206E166DF9C43B60A150E901F1007D388E::get_offset_of_U3CBinderU3Ek__BackingField_3(),
	DynamicExpression_tA1D1AA206E166DF9C43B60A150E901F1007D388E::get_offset_of_U3CDelegateTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { sizeof (DynamicExpressionN_t36C5CBF6E3265E7F65FDF490BBC25FA793E60284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3182[1] = 
{
	DynamicExpressionN_t36C5CBF6E3265E7F65FDF490BBC25FA793E60284::get_offset_of__arguments_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { sizeof (TypedDynamicExpressionN_t0FB1AE8CA308EF208C2FD0F992B234A9AA7B9AA9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3183[1] = 
{
	TypedDynamicExpressionN_t0FB1AE8CA308EF208C2FD0F992B234A9AA7B9AA9::get_offset_of_U3CTypeU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { sizeof (DynamicExpression1_tBE462E1F453791FAAB35156246407D3DFBCA9712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3184[1] = 
{
	DynamicExpression1_tBE462E1F453791FAAB35156246407D3DFBCA9712::get_offset_of__arg0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { sizeof (TypedDynamicExpression1_tC7F19F798B67838B12778AC72E3192C5157D5975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3185[1] = 
{
	TypedDynamicExpression1_tC7F19F798B67838B12778AC72E3192C5157D5975::get_offset_of_U3CTypeU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { sizeof (DynamicExpression2_t7AFA91A3BE4D76649EDBEE96DE228DCF5C86081E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3186[2] = 
{
	DynamicExpression2_t7AFA91A3BE4D76649EDBEE96DE228DCF5C86081E::get_offset_of__arg0_5(),
	DynamicExpression2_t7AFA91A3BE4D76649EDBEE96DE228DCF5C86081E::get_offset_of__arg1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { sizeof (TypedDynamicExpression2_tB87FA130890912873FBB78503F63A45C6CD66A2E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3187[1] = 
{
	TypedDynamicExpression2_tB87FA130890912873FBB78503F63A45C6CD66A2E::get_offset_of_U3CTypeU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { sizeof (DynamicExpression3_tE2F9B99DF93202729C4816B554255FD6980FD30D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3188[3] = 
{
	DynamicExpression3_tE2F9B99DF93202729C4816B554255FD6980FD30D::get_offset_of__arg0_5(),
	DynamicExpression3_tE2F9B99DF93202729C4816B554255FD6980FD30D::get_offset_of__arg1_6(),
	DynamicExpression3_tE2F9B99DF93202729C4816B554255FD6980FD30D::get_offset_of__arg2_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { sizeof (TypedDynamicExpression3_tA9DBA3E00A96B7F0B5150B7697023553B27FCD23), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3189[1] = 
{
	TypedDynamicExpression3_tA9DBA3E00A96B7F0B5150B7697023553B27FCD23::get_offset_of_U3CTypeU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { sizeof (DynamicExpression4_t3978F09387B840D1976292109243E5B4A77A200E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3190[4] = 
{
	DynamicExpression4_t3978F09387B840D1976292109243E5B4A77A200E::get_offset_of__arg0_5(),
	DynamicExpression4_t3978F09387B840D1976292109243E5B4A77A200E::get_offset_of__arg1_6(),
	DynamicExpression4_t3978F09387B840D1976292109243E5B4A77A200E::get_offset_of__arg2_7(),
	DynamicExpression4_t3978F09387B840D1976292109243E5B4A77A200E::get_offset_of__arg3_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { sizeof (TypedDynamicExpression4_t065E0D4637723B1E0FEA1351E7B146AE5B158F3C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3191[1] = 
{
	TypedDynamicExpression4_t065E0D4637723B1E0FEA1351E7B146AE5B158F3C::get_offset_of_U3CTypeU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { sizeof (ExpressionExtension_tAA67E9752E30C5AE5608378246B38AA4A29A7580), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { sizeof (DynamicExpressionVisitor_t7A3AF91DC4DA314063E3421F634C10F17F992ED3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { sizeof (ElementInit_tE52EF678BD23E3A9448F99710E4AF84E6A0F544B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3194[2] = 
{
	ElementInit_tE52EF678BD23E3A9448F99710E4AF84E6A0F544B::get_offset_of_U3CAddMethodU3Ek__BackingField_0(),
	ElementInit_tE52EF678BD23E3A9448F99710E4AF84E6A0F544B::get_offset_of_U3CArgumentsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { sizeof (Error_t15851665677E2192F92E4241B725BBEB2A51EFBB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { sizeof (ExpressionStringBuilder_t816963EA447C46FC13627E4980472716FAEA1AD2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3196[2] = 
{
	ExpressionStringBuilder_t816963EA447C46FC13627E4980472716FAEA1AD2::get_offset_of__out_0(),
	ExpressionStringBuilder_t816963EA447C46FC13627E4980472716FAEA1AD2::get_offset_of__ids_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { sizeof (ExpressionType_tA0C19CAF997A8592C84F0EBFE791BC19BF5AC556)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3197[86] = 
{
	ExpressionType_tA0C19CAF997A8592C84F0EBFE791BC19BF5AC556::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { sizeof (ExpressionVisitor_t2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { sizeof (GotoExpressionKind_t08D7AD2E460A35E36B1F72FFECFF744DE69A469C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3199[5] = 
{
	GotoExpressionKind_t08D7AD2E460A35E36B1F72FFECFF744DE69A469C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
