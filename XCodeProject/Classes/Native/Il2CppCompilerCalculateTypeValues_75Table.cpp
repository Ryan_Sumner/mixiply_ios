﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Core.Camera.CameraRig
struct CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4;
// Core.Game.LevelItem[]
struct LevelItemU5BU5D_t270D9F8FC942934C2EE86D06CD1617071D9796E7;
// Core.Health.Damageable
struct Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A;
// Core.Health.DamageableBehaviour
struct DamageableBehaviour_tA04944F5DEB3599E963255A91160F9074060329F;
// Core.Health.HealthChangeEvent
struct HealthChangeEvent_t7A0991C3BA39B6630C4A93CD11890F01767308BE;
// Core.Health.HitEvent
struct HitEvent_tA814F77C3A27C8D4A9A6EBE40269D28D13288921;
// Core.Health.IAlignmentProvider
struct IAlignmentProvider_t2D743EB90EDB9AA7B214CF0DADA5767CC0022D5E;
// Core.Health.SerializableIAlignmentProvider
struct SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61;
// Core.Input.InputController
struct InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45;
// Core.Input.InputScheme
struct InputScheme_t78F140C9CD80326F0D459CE65156EF66E3218678;
// Core.Input.InputScheme[]
struct InputSchemeU5BU5D_t4C31A62BE86FB155B6597242B14B4A8E709A4BB9;
// Core.Input.PointerInfo
struct PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB;
// Core.Input.TouchInfo
struct TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2;
// Core.UI.IMainMenuPage
struct IMainMenuPage_t0A88EB3BFE31F4875D5C7A3D764AED5AA5AD472B;
// Core.Utilities.PoolManager
struct PoolManager_tC4AA76A7B5FF355D068EEC877E2680E2DC9E771F;
// Core.Utilities.Pool`1<Core.Utilities.Poolable>
struct Pool_1_tA688FE7A8096EF8D7EADA8644CA3B87A9356A2D7;
// Core.Utilities.RepeatingTimer
struct RepeatingTimer_tFEABA0EAF35D64F27E55652FD50AF98799EDC468;
// Core.Utilities.Timer
struct Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B;
// Level.Waves.WaveManager
struct WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800;
// Pathfinding.Agents.Data.AgentConfiguration
struct AgentConfiguration_tB707ADEB3898212CA6B3C134B11AE9E9F440A8EC;
// Pathfinding.Nodes.Node
struct Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Core.Health.DamageableBehaviour>
struct Action_1_t771534850E52AB2204D871260BDCC1D862F5F599;
// System.Action`1<Core.Health.HealthChangeInfo>
struct Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0;
// System.Action`1<Core.Health.HitInfo>
struct Action_1_tAC22707664B02B17BD3236A855C78B87B86BE22D;
// System.Action`1<Core.Input.PinchInfo>
struct Action_1_t8F2FA388F2027FB226797F1A92127EA304C1B90B;
// System.Action`1<Core.Input.PointerActionInfo>
struct Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1;
// System.Action`1<Core.Input.PointerInfo>
struct Action_1_t07687568CEAACB78656500EE24DE97612CAC8085;
// System.Action`1<Core.Input.WheelInfo>
struct Action_1_t481C0EDC6AC5AB041D11A7E7B09B193CCDCAEDA4;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_t9715959F24DDA308D1D281316A802C4107FC7725;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Core.Utilities.Poolable,Core.Utilities.AutoComponentPrefabPool`1<Core.Utilities.Poolable>>
struct Dictionary_2_tAFB69BEC9DD355D9F4D7A6DA8507756D019398EF;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
struct Dictionary_2_t2945E29AF5C8B98DCF5C7EB04CC31078A8DBC7E9;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>
struct Dictionary_2_t321449D6ED7F1095DE0E435E70AE766EE3327455;
// System.Collections.Generic.IDictionary`2<System.String,Core.Game.LevelItem>
struct IDictionary_2_t3EC77DCA141B3E8B88896D7B58E49D3A81629652;
// System.Collections.Generic.List`1<Core.Health.SimpleAlignment>
struct List_1_tE750D17233AFE3F19B812157798E916EE6DC7721;
// System.Collections.Generic.List`1<Core.Input.MouseButtonInfo>
struct List_1_t7B2B84536A834129E63FA96130202D797A2D624A;
// System.Collections.Generic.List`1<Core.Input.TouchInfo>
struct List_1_tB2F89C3237A8F9BC195D8869C618C1621829EAA4;
// System.Collections.Generic.List`1<Core.Utilities.Poolable>
struct List_1_t20B9B8A4495121AF636424D41BC08F7E75DAF87D;
// System.Collections.Generic.List`1<Core.Utilities.Timer>
struct List_1_tE20E361D7E2F208DA9910E19F3009930FB383D85;
// System.Collections.Generic.List`1<Level.SpawnInstruction>
struct List_1_t87DB7F5E86C53503A5ADE7BE5C57DBB891DA7ADD;
// System.Collections.Generic.List`1<Level.Waves.Wave>
struct List_1_t804D7EE5940213EA47FF7D51872C5B9845A63218;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0;
// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>
struct List_1_t2762C811E470D336E31761384C6E5382164DA4C7;
// System.Collections.Generic.Stack`1<Core.UI.IMainMenuPage>
struct Stack_1_t7E9CC30355F0C75A11303BCC137AB31F0CFEB510;
// System.Func`1<UnityEngine.GameObject>
struct Func_1_t2DB1284E02C7B50129AA58C05B4E23DE4642FF89;
// System.Func`2<Core.Game.LevelItem,System.String>
struct Func_2_t114CAEED5141E4B87EEF10AF6DAE3C98CEB061BD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasGroup
struct CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252;
// UnityEngine.Collision
struct Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Light
struct Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.SphereCollider
struct SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Transform[]
struct TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2;
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct AxisMapping_tD681DA8C80391B51E5F8AB14A8F9DD0B62F61ABD;
// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44;
// UnityStandardAssets.Effects.ExplosionFireAndDebris
struct ExplosionFireAndDebris_tE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA;
// UnityStandardAssets.Effects.ExplosionPhysicsForce
struct ExplosionPhysicsForce_t2BC67AEA4386688129F43B43EB9927BE57A384A8;
// UnityStandardAssets.Effects.Explosive
struct Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40;
// UnityStandardAssets.Utility.ObjectResetter
struct ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry[]
struct EntryU5BU5D_t1FB7802B3ACA3175B98D7F3897D6E0728B50CAF1;
// UnityStandardAssets.Utility.WaypointCircuit
struct WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE;
// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct WaypointList_t28075B7540AC2B9E2507C0E6175C6D5B31F7364D;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef GAMEOBJECTEXTENSIONS_T562EC0C76CE41B4A7BFE03B524AA44C9B4389001_H
#define GAMEOBJECTEXTENSIONS_T562EC0C76CE41B4A7BFE03B524AA44C9B4389001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Extensions.GameObjectExtensions
struct  GameObjectExtensions_t562EC0C76CE41B4A7BFE03B524AA44C9B4389001  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTEXTENSIONS_T562EC0C76CE41B4A7BFE03B524AA44C9B4389001_H
#ifndef LEVELITEM_TBE03F0DC7DCC0F0F4844E719363782132F9BCDC2_H
#define LEVELITEM_TBE03F0DC7DCC0F0F4844E719363782132F9BCDC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Game.LevelItem
struct  LevelItem_tBE03F0DC7DCC0F0F4844E719363782132F9BCDC2  : public RuntimeObject
{
public:
	// System.String Core.Game.LevelItem::id
	String_t* ___id_0;
	// System.String Core.Game.LevelItem::name
	String_t* ___name_1;
	// System.String Core.Game.LevelItem::description
	String_t* ___description_2;
	// System.String Core.Game.LevelItem::sceneName
	String_t* ___sceneName_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(LevelItem_tBE03F0DC7DCC0F0F4844E719363782132F9BCDC2, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(LevelItem_tBE03F0DC7DCC0F0F4844E719363782132F9BCDC2, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_description_2() { return static_cast<int32_t>(offsetof(LevelItem_tBE03F0DC7DCC0F0F4844E719363782132F9BCDC2, ___description_2)); }
	inline String_t* get_description_2() const { return ___description_2; }
	inline String_t** get_address_of_description_2() { return &___description_2; }
	inline void set_description_2(String_t* value)
	{
		___description_2 = value;
		Il2CppCodeGenWriteBarrier((&___description_2), value);
	}

	inline static int32_t get_offset_of_sceneName_3() { return static_cast<int32_t>(offsetof(LevelItem_tBE03F0DC7DCC0F0F4844E719363782132F9BCDC2, ___sceneName_3)); }
	inline String_t* get_sceneName_3() const { return ___sceneName_3; }
	inline String_t** get_address_of_sceneName_3() { return &___sceneName_3; }
	inline void set_sceneName_3(String_t* value)
	{
		___sceneName_3 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELITEM_TBE03F0DC7DCC0F0F4844E719363782132F9BCDC2_H
#ifndef U3CU3EC_T1A65C2DF9B3F8829C11776D23B8FC1964D2C5862_H
#define U3CU3EC_T1A65C2DF9B3F8829C11776D23B8FC1964D2C5862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Game.LevelList/<>c
struct  U3CU3Ec_t1A65C2DF9B3F8829C11776D23B8FC1964D2C5862  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1A65C2DF9B3F8829C11776D23B8FC1964D2C5862_StaticFields
{
public:
	// Core.Game.LevelList/<>c Core.Game.LevelList/<>c::<>9
	U3CU3Ec_t1A65C2DF9B3F8829C11776D23B8FC1964D2C5862 * ___U3CU3E9_0;
	// System.Func`2<Core.Game.LevelItem,System.String> Core.Game.LevelList/<>c::<>9__18_0
	Func_2_t114CAEED5141E4B87EEF10AF6DAE3C98CEB061BD * ___U3CU3E9__18_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1A65C2DF9B3F8829C11776D23B8FC1964D2C5862_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1A65C2DF9B3F8829C11776D23B8FC1964D2C5862 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1A65C2DF9B3F8829C11776D23B8FC1964D2C5862 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1A65C2DF9B3F8829C11776D23B8FC1964D2C5862 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1A65C2DF9B3F8829C11776D23B8FC1964D2C5862_StaticFields, ___U3CU3E9__18_0_1)); }
	inline Func_2_t114CAEED5141E4B87EEF10AF6DAE3C98CEB061BD * get_U3CU3E9__18_0_1() const { return ___U3CU3E9__18_0_1; }
	inline Func_2_t114CAEED5141E4B87EEF10AF6DAE3C98CEB061BD ** get_address_of_U3CU3E9__18_0_1() { return &___U3CU3E9__18_0_1; }
	inline void set_U3CU3E9__18_0_1(Func_2_t114CAEED5141E4B87EEF10AF6DAE3C98CEB061BD * value)
	{
		___U3CU3E9__18_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1A65C2DF9B3F8829C11776D23B8FC1964D2C5862_H
#ifndef DAMAGEABLE_T352EB38C33DFADA9737BAA68D89A675CEA2BF23A_H
#define DAMAGEABLE_T352EB38C33DFADA9737BAA68D89A675CEA2BF23A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Health.Damageable
struct  Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A  : public RuntimeObject
{
public:
	// System.Single Core.Health.Damageable::maxHealth
	float ___maxHealth_0;
	// System.Single Core.Health.Damageable::startingHealth
	float ___startingHealth_1;
	// Core.Health.SerializableIAlignmentProvider Core.Health.Damageable::alignment
	SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 * ___alignment_2;
	// System.Single Core.Health.Damageable::<currentHealth>k__BackingField
	float ___U3CcurrentHealthU3Ek__BackingField_3;
	// System.Action Core.Health.Damageable::reachedMaxHealth
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___reachedMaxHealth_4;
	// System.Action`1<Core.Health.HealthChangeInfo> Core.Health.Damageable::damaged
	Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 * ___damaged_5;
	// System.Action`1<Core.Health.HealthChangeInfo> Core.Health.Damageable::healed
	Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 * ___healed_6;
	// System.Action`1<Core.Health.HealthChangeInfo> Core.Health.Damageable::died
	Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 * ___died_7;
	// System.Action`1<Core.Health.HealthChangeInfo> Core.Health.Damageable::healthChanged
	Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 * ___healthChanged_8;

public:
	inline static int32_t get_offset_of_maxHealth_0() { return static_cast<int32_t>(offsetof(Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A, ___maxHealth_0)); }
	inline float get_maxHealth_0() const { return ___maxHealth_0; }
	inline float* get_address_of_maxHealth_0() { return &___maxHealth_0; }
	inline void set_maxHealth_0(float value)
	{
		___maxHealth_0 = value;
	}

	inline static int32_t get_offset_of_startingHealth_1() { return static_cast<int32_t>(offsetof(Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A, ___startingHealth_1)); }
	inline float get_startingHealth_1() const { return ___startingHealth_1; }
	inline float* get_address_of_startingHealth_1() { return &___startingHealth_1; }
	inline void set_startingHealth_1(float value)
	{
		___startingHealth_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A, ___alignment_2)); }
	inline SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 * get_alignment_2() const { return ___alignment_2; }
	inline SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 ** get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 * value)
	{
		___alignment_2 = value;
		Il2CppCodeGenWriteBarrier((&___alignment_2), value);
	}

	inline static int32_t get_offset_of_U3CcurrentHealthU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A, ___U3CcurrentHealthU3Ek__BackingField_3)); }
	inline float get_U3CcurrentHealthU3Ek__BackingField_3() const { return ___U3CcurrentHealthU3Ek__BackingField_3; }
	inline float* get_address_of_U3CcurrentHealthU3Ek__BackingField_3() { return &___U3CcurrentHealthU3Ek__BackingField_3; }
	inline void set_U3CcurrentHealthU3Ek__BackingField_3(float value)
	{
		___U3CcurrentHealthU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_reachedMaxHealth_4() { return static_cast<int32_t>(offsetof(Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A, ___reachedMaxHealth_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_reachedMaxHealth_4() const { return ___reachedMaxHealth_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_reachedMaxHealth_4() { return &___reachedMaxHealth_4; }
	inline void set_reachedMaxHealth_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___reachedMaxHealth_4 = value;
		Il2CppCodeGenWriteBarrier((&___reachedMaxHealth_4), value);
	}

	inline static int32_t get_offset_of_damaged_5() { return static_cast<int32_t>(offsetof(Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A, ___damaged_5)); }
	inline Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 * get_damaged_5() const { return ___damaged_5; }
	inline Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 ** get_address_of_damaged_5() { return &___damaged_5; }
	inline void set_damaged_5(Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 * value)
	{
		___damaged_5 = value;
		Il2CppCodeGenWriteBarrier((&___damaged_5), value);
	}

	inline static int32_t get_offset_of_healed_6() { return static_cast<int32_t>(offsetof(Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A, ___healed_6)); }
	inline Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 * get_healed_6() const { return ___healed_6; }
	inline Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 ** get_address_of_healed_6() { return &___healed_6; }
	inline void set_healed_6(Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 * value)
	{
		___healed_6 = value;
		Il2CppCodeGenWriteBarrier((&___healed_6), value);
	}

	inline static int32_t get_offset_of_died_7() { return static_cast<int32_t>(offsetof(Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A, ___died_7)); }
	inline Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 * get_died_7() const { return ___died_7; }
	inline Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 ** get_address_of_died_7() { return &___died_7; }
	inline void set_died_7(Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 * value)
	{
		___died_7 = value;
		Il2CppCodeGenWriteBarrier((&___died_7), value);
	}

	inline static int32_t get_offset_of_healthChanged_8() { return static_cast<int32_t>(offsetof(Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A, ___healthChanged_8)); }
	inline Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 * get_healthChanged_8() const { return ___healthChanged_8; }
	inline Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 ** get_address_of_healthChanged_8() { return &___healthChanged_8; }
	inline void set_healthChanged_8(Action_1_t6582A9CC88A82F872FFF3E522BCAF59BA5F764E0 * value)
	{
		___healthChanged_8 = value;
		Il2CppCodeGenWriteBarrier((&___healthChanged_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAMAGEABLE_T352EB38C33DFADA9737BAA68D89A675CEA2BF23A_H
#ifndef POOL_1_T67DD965EB2EB501D49914B0624CBE59393FFF7CA_H
#define POOL_1_T67DD965EB2EB501D49914B0624CBE59393FFF7CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Pool`1<UnityEngine.GameObject>
struct  Pool_1_t67DD965EB2EB501D49914B0624CBE59393FFF7CA  : public RuntimeObject
{
public:
	// System.Func`1<T> Core.Utilities.Pool`1::m_Factory
	Func_1_t2DB1284E02C7B50129AA58C05B4E23DE4642FF89 * ___m_Factory_0;
	// System.Action`1<T> Core.Utilities.Pool`1::m_Reset
	Action_1_t9715959F24DDA308D1D281316A802C4107FC7725 * ___m_Reset_1;
	// System.Collections.Generic.List`1<T> Core.Utilities.Pool`1::m_Available
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ___m_Available_2;
	// System.Collections.Generic.List`1<T> Core.Utilities.Pool`1::m_All
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ___m_All_3;

public:
	inline static int32_t get_offset_of_m_Factory_0() { return static_cast<int32_t>(offsetof(Pool_1_t67DD965EB2EB501D49914B0624CBE59393FFF7CA, ___m_Factory_0)); }
	inline Func_1_t2DB1284E02C7B50129AA58C05B4E23DE4642FF89 * get_m_Factory_0() const { return ___m_Factory_0; }
	inline Func_1_t2DB1284E02C7B50129AA58C05B4E23DE4642FF89 ** get_address_of_m_Factory_0() { return &___m_Factory_0; }
	inline void set_m_Factory_0(Func_1_t2DB1284E02C7B50129AA58C05B4E23DE4642FF89 * value)
	{
		___m_Factory_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Factory_0), value);
	}

	inline static int32_t get_offset_of_m_Reset_1() { return static_cast<int32_t>(offsetof(Pool_1_t67DD965EB2EB501D49914B0624CBE59393FFF7CA, ___m_Reset_1)); }
	inline Action_1_t9715959F24DDA308D1D281316A802C4107FC7725 * get_m_Reset_1() const { return ___m_Reset_1; }
	inline Action_1_t9715959F24DDA308D1D281316A802C4107FC7725 ** get_address_of_m_Reset_1() { return &___m_Reset_1; }
	inline void set_m_Reset_1(Action_1_t9715959F24DDA308D1D281316A802C4107FC7725 * value)
	{
		___m_Reset_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Reset_1), value);
	}

	inline static int32_t get_offset_of_m_Available_2() { return static_cast<int32_t>(offsetof(Pool_1_t67DD965EB2EB501D49914B0624CBE59393FFF7CA, ___m_Available_2)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get_m_Available_2() const { return ___m_Available_2; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of_m_Available_2() { return &___m_Available_2; }
	inline void set_m_Available_2(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		___m_Available_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Available_2), value);
	}

	inline static int32_t get_offset_of_m_All_3() { return static_cast<int32_t>(offsetof(Pool_1_t67DD965EB2EB501D49914B0624CBE59393FFF7CA, ___m_All_3)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get_m_All_3() const { return ___m_All_3; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of_m_All_3() { return &___m_All_3; }
	inline void set_m_All_3(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		___m_All_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_All_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOL_1_T67DD965EB2EB501D49914B0624CBE59393FFF7CA_H
#ifndef SERIALIZABLEINTERFACE_TB2B751B1CAB73F4FE07575DB97D4A6BCA607F9A7_H
#define SERIALIZABLEINTERFACE_TB2B751B1CAB73F4FE07575DB97D4A6BCA607F9A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.SerializableInterface
struct  SerializableInterface_tB2B751B1CAB73F4FE07575DB97D4A6BCA607F9A7  : public RuntimeObject
{
public:
	// UnityEngine.Object Core.Utilities.SerializableInterface::unityObjectReference
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___unityObjectReference_0;

public:
	inline static int32_t get_offset_of_unityObjectReference_0() { return static_cast<int32_t>(offsetof(SerializableInterface_tB2B751B1CAB73F4FE07575DB97D4A6BCA607F9A7, ___unityObjectReference_0)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_unityObjectReference_0() const { return ___unityObjectReference_0; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_unityObjectReference_0() { return &___unityObjectReference_0; }
	inline void set_unityObjectReference_0(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___unityObjectReference_0 = value;
		Il2CppCodeGenWriteBarrier((&___unityObjectReference_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEINTERFACE_TB2B751B1CAB73F4FE07575DB97D4A6BCA607F9A7_H
#ifndef TIMER_TAA66D657EFD530564D7F0ABC86482FF91BC89C1B_H
#define TIMER_TAA66D657EFD530564D7F0ABC86482FF91BC89C1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Timer
struct  Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B  : public RuntimeObject
{
public:
	// System.Action Core.Utilities.Timer::m_Callback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___m_Callback_0;
	// System.Single Core.Utilities.Timer::m_Time
	float ___m_Time_1;
	// System.Single Core.Utilities.Timer::m_CurrentTime
	float ___m_CurrentTime_2;

public:
	inline static int32_t get_offset_of_m_Callback_0() { return static_cast<int32_t>(offsetof(Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B, ___m_Callback_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_m_Callback_0() const { return ___m_Callback_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_m_Callback_0() { return &___m_Callback_0; }
	inline void set_m_Callback_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___m_Callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_0), value);
	}

	inline static int32_t get_offset_of_m_Time_1() { return static_cast<int32_t>(offsetof(Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B, ___m_Time_1)); }
	inline float get_m_Time_1() const { return ___m_Time_1; }
	inline float* get_address_of_m_Time_1() { return &___m_Time_1; }
	inline void set_m_Time_1(float value)
	{
		___m_Time_1 = value;
	}

	inline static int32_t get_offset_of_m_CurrentTime_2() { return static_cast<int32_t>(offsetof(Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B, ___m_CurrentTime_2)); }
	inline float get_m_CurrentTime_2() const { return ___m_CurrentTime_2; }
	inline float* get_address_of_m_CurrentTime_2() { return &___m_CurrentTime_2; }
	inline void set_m_CurrentTime_2(float value)
	{
		___m_CurrentTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMER_TAA66D657EFD530564D7F0ABC86482FF91BC89C1B_H
#ifndef VECTORHELPER_T2C792229B88A0DEB52177324404C0BB495152786_H
#define VECTORHELPER_T2C792229B88A0DEB52177324404C0BB495152786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.VectorHelper
struct  VectorHelper_t2C792229B88A0DEB52177324404C0BB495152786  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORHELPER_T2C792229B88A0DEB52177324404C0BB495152786_H
#ifndef SPAWNINSTRUCTION_TB217FE64A2A52EC6DF45A5A8685389B46B56105F_H
#define SPAWNINSTRUCTION_TB217FE64A2A52EC6DF45A5A8685389B46B56105F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level.SpawnInstruction
struct  SpawnInstruction_tB217FE64A2A52EC6DF45A5A8685389B46B56105F  : public RuntimeObject
{
public:
	// Pathfinding.Agents.Data.AgentConfiguration Level.SpawnInstruction::agentConfiguration
	AgentConfiguration_tB707ADEB3898212CA6B3C134B11AE9E9F440A8EC * ___agentConfiguration_0;
	// System.Single Level.SpawnInstruction::delayToSpawn
	float ___delayToSpawn_1;
	// Pathfinding.Nodes.Node Level.SpawnInstruction::startingNode
	Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 * ___startingNode_2;

public:
	inline static int32_t get_offset_of_agentConfiguration_0() { return static_cast<int32_t>(offsetof(SpawnInstruction_tB217FE64A2A52EC6DF45A5A8685389B46B56105F, ___agentConfiguration_0)); }
	inline AgentConfiguration_tB707ADEB3898212CA6B3C134B11AE9E9F440A8EC * get_agentConfiguration_0() const { return ___agentConfiguration_0; }
	inline AgentConfiguration_tB707ADEB3898212CA6B3C134B11AE9E9F440A8EC ** get_address_of_agentConfiguration_0() { return &___agentConfiguration_0; }
	inline void set_agentConfiguration_0(AgentConfiguration_tB707ADEB3898212CA6B3C134B11AE9E9F440A8EC * value)
	{
		___agentConfiguration_0 = value;
		Il2CppCodeGenWriteBarrier((&___agentConfiguration_0), value);
	}

	inline static int32_t get_offset_of_delayToSpawn_1() { return static_cast<int32_t>(offsetof(SpawnInstruction_tB217FE64A2A52EC6DF45A5A8685389B46B56105F, ___delayToSpawn_1)); }
	inline float get_delayToSpawn_1() const { return ___delayToSpawn_1; }
	inline float* get_address_of_delayToSpawn_1() { return &___delayToSpawn_1; }
	inline void set_delayToSpawn_1(float value)
	{
		___delayToSpawn_1 = value;
	}

	inline static int32_t get_offset_of_startingNode_2() { return static_cast<int32_t>(offsetof(SpawnInstruction_tB217FE64A2A52EC6DF45A5A8685389B46B56105F, ___startingNode_2)); }
	inline Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 * get_startingNode_2() const { return ___startingNode_2; }
	inline Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 ** get_address_of_startingNode_2() { return &___startingNode_2; }
	inline void set_startingNode_2(Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 * value)
	{
		___startingNode_2 = value;
		Il2CppCodeGenWriteBarrier((&___startingNode_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWNINSTRUCTION_TB217FE64A2A52EC6DF45A5A8685389B46B56105F_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef CROSSPLATFORMINPUTMANAGER_T13D200EB7A01CDDE2A4EADCDB5306E3F525EFE0B_H
#define CROSSPLATFORMINPUTMANAGER_T13D200EB7A01CDDE2A4EADCDB5306E3F525EFE0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
struct  CrossPlatformInputManager_t13D200EB7A01CDDE2A4EADCDB5306E3F525EFE0B  : public RuntimeObject
{
public:

public:
};

struct CrossPlatformInputManager_t13D200EB7A01CDDE2A4EADCDB5306E3F525EFE0B_StaticFields
{
public:
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::activeInput
	VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44 * ___activeInput_0;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_TouchInput
	VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44 * ___s_TouchInput_1;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_HardwareInput
	VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44 * ___s_HardwareInput_2;

public:
	inline static int32_t get_offset_of_activeInput_0() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t13D200EB7A01CDDE2A4EADCDB5306E3F525EFE0B_StaticFields, ___activeInput_0)); }
	inline VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44 * get_activeInput_0() const { return ___activeInput_0; }
	inline VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44 ** get_address_of_activeInput_0() { return &___activeInput_0; }
	inline void set_activeInput_0(VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44 * value)
	{
		___activeInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___activeInput_0), value);
	}

	inline static int32_t get_offset_of_s_TouchInput_1() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t13D200EB7A01CDDE2A4EADCDB5306E3F525EFE0B_StaticFields, ___s_TouchInput_1)); }
	inline VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44 * get_s_TouchInput_1() const { return ___s_TouchInput_1; }
	inline VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44 ** get_address_of_s_TouchInput_1() { return &___s_TouchInput_1; }
	inline void set_s_TouchInput_1(VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44 * value)
	{
		___s_TouchInput_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_TouchInput_1), value);
	}

	inline static int32_t get_offset_of_s_HardwareInput_2() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t13D200EB7A01CDDE2A4EADCDB5306E3F525EFE0B_StaticFields, ___s_HardwareInput_2)); }
	inline VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44 * get_s_HardwareInput_2() const { return ___s_HardwareInput_2; }
	inline VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44 ** get_address_of_s_HardwareInput_2() { return &___s_HardwareInput_2; }
	inline void set_s_HardwareInput_2(VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44 * value)
	{
		___s_HardwareInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_HardwareInput_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSPLATFORMINPUTMANAGER_T13D200EB7A01CDDE2A4EADCDB5306E3F525EFE0B_H
#ifndef VIRTUALAXIS_TC5D5D2ECC42057E501B294C4032369D0AFEC50F2_H
#define VIRTUALAXIS_TC5D5D2ECC42057E501B294C4032369D0AFEC50F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct  VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::m_Value
	float ___m_Value_1;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2, ___U3CmatchWithInputManagerU3Ek__BackingField_2)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_2() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return &___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_2(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALAXIS_TC5D5D2ECC42057E501B294C4032369D0AFEC50F2_H
#ifndef VIRTUALBUTTON_TB612D966F284C10B236197C0B236E1C7DCBA9B91_H
#define VIRTUALBUTTON_TB612D966F284C10B236197C0B236E1C7DCBA9B91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
struct  VirtualButton_tB612D966F284C10B236197C0B236E1C7DCBA9B91  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_1;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_LastPressedFrame
	int32_t ___m_LastPressedFrame_2;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_ReleasedFrame
	int32_t ___m_ReleasedFrame_3;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_Pressed
	bool ___m_Pressed_4;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualButton_tB612D966F284C10B236197C0B236E1C7DCBA9B91, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VirtualButton_tB612D966F284C10B236197C0B236E1C7DCBA9B91, ___U3CmatchWithInputManagerU3Ek__BackingField_1)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_1() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return &___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_1(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_m_LastPressedFrame_2() { return static_cast<int32_t>(offsetof(VirtualButton_tB612D966F284C10B236197C0B236E1C7DCBA9B91, ___m_LastPressedFrame_2)); }
	inline int32_t get_m_LastPressedFrame_2() const { return ___m_LastPressedFrame_2; }
	inline int32_t* get_address_of_m_LastPressedFrame_2() { return &___m_LastPressedFrame_2; }
	inline void set_m_LastPressedFrame_2(int32_t value)
	{
		___m_LastPressedFrame_2 = value;
	}

	inline static int32_t get_offset_of_m_ReleasedFrame_3() { return static_cast<int32_t>(offsetof(VirtualButton_tB612D966F284C10B236197C0B236E1C7DCBA9B91, ___m_ReleasedFrame_3)); }
	inline int32_t get_m_ReleasedFrame_3() const { return ___m_ReleasedFrame_3; }
	inline int32_t* get_address_of_m_ReleasedFrame_3() { return &___m_ReleasedFrame_3; }
	inline void set_m_ReleasedFrame_3(int32_t value)
	{
		___m_ReleasedFrame_3 = value;
	}

	inline static int32_t get_offset_of_m_Pressed_4() { return static_cast<int32_t>(offsetof(VirtualButton_tB612D966F284C10B236197C0B236E1C7DCBA9B91, ___m_Pressed_4)); }
	inline bool get_m_Pressed_4() const { return ___m_Pressed_4; }
	inline bool* get_address_of_m_Pressed_4() { return &___m_Pressed_4; }
	inline void set_m_Pressed_4(bool value)
	{
		___m_Pressed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_TB612D966F284C10B236197C0B236E1C7DCBA9B91_H
#ifndef U3CSTARTU3ED__4_T8F787342BA2EF55D1F910EC7C170C59B1B6AB17F_H
#define U3CSTARTU3ED__4_T8F787342BA2EF55D1F910EC7C170C59B1B6AB17F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4
struct  U3CStartU3Ed__4_t8F787342BA2EF55D1F910EC7C170C59B1B6AB17F  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Effects.ExplosionFireAndDebris UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::<>4__this
	ExplosionFireAndDebris_tE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA * ___U3CU3E4__this_2;
	// System.Single UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::<multiplier>5__2
	float ___U3CmultiplierU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t8F787342BA2EF55D1F910EC7C170C59B1B6AB17F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t8F787342BA2EF55D1F910EC7C170C59B1B6AB17F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t8F787342BA2EF55D1F910EC7C170C59B1B6AB17F, ___U3CU3E4__this_2)); }
	inline ExplosionFireAndDebris_tE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ExplosionFireAndDebris_tE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ExplosionFireAndDebris_tE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CmultiplierU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t8F787342BA2EF55D1F910EC7C170C59B1B6AB17F, ___U3CmultiplierU3E5__2_3)); }
	inline float get_U3CmultiplierU3E5__2_3() const { return ___U3CmultiplierU3E5__2_3; }
	inline float* get_address_of_U3CmultiplierU3E5__2_3() { return &___U3CmultiplierU3E5__2_3; }
	inline void set_U3CmultiplierU3E5__2_3(float value)
	{
		___U3CmultiplierU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__4_T8F787342BA2EF55D1F910EC7C170C59B1B6AB17F_H
#ifndef U3CSTARTU3ED__1_TBA16B8AACF91AA6BA165D5587233B3B3BC159B2F_H
#define U3CSTARTU3ED__1_TBA16B8AACF91AA6BA165D5587233B3B3BC159B2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1
struct  U3CStartU3Ed__1_tBA16B8AACF91AA6BA165D5587233B3B3BC159B2F  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Effects.ExplosionPhysicsForce UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::<>4__this
	ExplosionPhysicsForce_t2BC67AEA4386688129F43B43EB9927BE57A384A8 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__1_tBA16B8AACF91AA6BA165D5587233B3B3BC159B2F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__1_tBA16B8AACF91AA6BA165D5587233B3B3BC159B2F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__1_tBA16B8AACF91AA6BA165D5587233B3B3BC159B2F, ___U3CU3E4__this_2)); }
	inline ExplosionPhysicsForce_t2BC67AEA4386688129F43B43EB9927BE57A384A8 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ExplosionPhysicsForce_t2BC67AEA4386688129F43B43EB9927BE57A384A8 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ExplosionPhysicsForce_t2BC67AEA4386688129F43B43EB9927BE57A384A8 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__1_TBA16B8AACF91AA6BA165D5587233B3B3BC159B2F_H
#ifndef U3CONCOLLISIONENTERU3ED__8_T9CC677DD20DFE89A30A2E5E4A99CF9EA59CAB3E3_H
#define U3CONCOLLISIONENTERU3ED__8_T9CC677DD20DFE89A30A2E5E4A99CF9EA59CAB3E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8
struct  U3COnCollisionEnterU3Ed__8_t9CC677DD20DFE89A30A2E5E4A99CF9EA59CAB3E3  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Effects.Explosive UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8::<>4__this
	Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40 * ___U3CU3E4__this_2;
	// UnityEngine.Collision UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8::col
	Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C * ___col_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnCollisionEnterU3Ed__8_t9CC677DD20DFE89A30A2E5E4A99CF9EA59CAB3E3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3COnCollisionEnterU3Ed__8_t9CC677DD20DFE89A30A2E5E4A99CF9EA59CAB3E3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3COnCollisionEnterU3Ed__8_t9CC677DD20DFE89A30A2E5E4A99CF9EA59CAB3E3, ___U3CU3E4__this_2)); }
	inline Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_col_3() { return static_cast<int32_t>(offsetof(U3COnCollisionEnterU3Ed__8_t9CC677DD20DFE89A30A2E5E4A99CF9EA59CAB3E3, ___col_3)); }
	inline Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C * get_col_3() const { return ___col_3; }
	inline Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C ** get_address_of_col_3() { return &___col_3; }
	inline void set_col_3(Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C * value)
	{
		___col_3 = value;
		Il2CppCodeGenWriteBarrier((&___col_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONCOLLISIONENTERU3ED__8_T9CC677DD20DFE89A30A2E5E4A99CF9EA59CAB3E3_H
#ifndef U3CACTIVATEU3ED__5_TABE89194BB953D1E0A8B14D2BF1E92D09509958F_H
#define U3CACTIVATEU3ED__5_TABE89194BB953D1E0A8B14D2BF1E92D09509958F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5
struct  U3CActivateU3Ed__5_tABE89194BB953D1E0A8B14D2BF1E92D09509958F  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5::entry
	Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864 * ___entry_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CActivateU3Ed__5_tABE89194BB953D1E0A8B14D2BF1E92D09509958F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CActivateU3Ed__5_tABE89194BB953D1E0A8B14D2BF1E92D09509958F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_entry_2() { return static_cast<int32_t>(offsetof(U3CActivateU3Ed__5_tABE89194BB953D1E0A8B14D2BF1E92D09509958F, ___entry_2)); }
	inline Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864 * get_entry_2() const { return ___entry_2; }
	inline Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864 ** get_address_of_entry_2() { return &___entry_2; }
	inline void set_entry_2(Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864 * value)
	{
		___entry_2 = value;
		Il2CppCodeGenWriteBarrier((&___entry_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACTIVATEU3ED__5_TABE89194BB953D1E0A8B14D2BF1E92D09509958F_H
#ifndef U3CDEACTIVATEU3ED__6_TA37ED97A195669F022F3A77008A29533A5CCEEC1_H
#define U3CDEACTIVATEU3ED__6_TA37ED97A195669F022F3A77008A29533A5CCEEC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6
struct  U3CDeactivateU3Ed__6_tA37ED97A195669F022F3A77008A29533A5CCEEC1  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6::entry
	Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864 * ___entry_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ed__6_tA37ED97A195669F022F3A77008A29533A5CCEEC1, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ed__6_tA37ED97A195669F022F3A77008A29533A5CCEEC1, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_entry_2() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ed__6_tA37ED97A195669F022F3A77008A29533A5CCEEC1, ___entry_2)); }
	inline Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864 * get_entry_2() const { return ___entry_2; }
	inline Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864 ** get_address_of_entry_2() { return &___entry_2; }
	inline void set_entry_2(Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864 * value)
	{
		___entry_2 = value;
		Il2CppCodeGenWriteBarrier((&___entry_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDEACTIVATEU3ED__6_TA37ED97A195669F022F3A77008A29533A5CCEEC1_H
#ifndef U3CRELOADLEVELU3ED__7_TA802367CF3ABCAA4BFD1F0DE841A813AF26B0386_H
#define U3CRELOADLEVELU3ED__7_TA802367CF3ABCAA4BFD1F0DE841A813AF26B0386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7
struct  U3CReloadLevelU3Ed__7_tA802367CF3ABCAA4BFD1F0DE841A813AF26B0386  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7::entry
	Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864 * ___entry_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ed__7_tA802367CF3ABCAA4BFD1F0DE841A813AF26B0386, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ed__7_tA802367CF3ABCAA4BFD1F0DE841A813AF26B0386, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_entry_2() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ed__7_tA802367CF3ABCAA4BFD1F0DE841A813AF26B0386, ___entry_2)); }
	inline Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864 * get_entry_2() const { return ___entry_2; }
	inline Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864 ** get_address_of_entry_2() { return &___entry_2; }
	inline void set_entry_2(Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864 * value)
	{
		___entry_2 = value;
		Il2CppCodeGenWriteBarrier((&___entry_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRELOADLEVELU3ED__7_TA802367CF3ABCAA4BFD1F0DE841A813AF26B0386_H
#ifndef ENTRIES_T0337CB356ADCAF3563885A2117B6C5DA4DCB9E3E_H
#define ENTRIES_T0337CB356ADCAF3563885A2117B6C5DA4DCB9E3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct  Entries_t0337CB356ADCAF3563885A2117B6C5DA4DCB9E3E  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry[] UnityStandardAssets.Utility.TimedObjectActivator/Entries::entries
	EntryU5BU5D_t1FB7802B3ACA3175B98D7F3897D6E0728B50CAF1* ___entries_0;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(Entries_t0337CB356ADCAF3563885A2117B6C5DA4DCB9E3E, ___entries_0)); }
	inline EntryU5BU5D_t1FB7802B3ACA3175B98D7F3897D6E0728B50CAF1* get_entries_0() const { return ___entries_0; }
	inline EntryU5BU5D_t1FB7802B3ACA3175B98D7F3897D6E0728B50CAF1** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(EntryU5BU5D_t1FB7802B3ACA3175B98D7F3897D6E0728B50CAF1* value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier((&___entries_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRIES_T0337CB356ADCAF3563885A2117B6C5DA4DCB9E3E_H
#ifndef WAYPOINTLIST_T28075B7540AC2B9E2507C0E6175C6D5B31F7364D_H
#define WAYPOINTLIST_T28075B7540AC2B9E2507C0E6175C6D5B31F7364D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct  WaypointList_t28075B7540AC2B9E2507C0E6175C6D5B31F7364D  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit UnityStandardAssets.Utility.WaypointCircuit/WaypointList::circuit
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE * ___circuit_0;
	// UnityEngine.Transform[] UnityStandardAssets.Utility.WaypointCircuit/WaypointList::items
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* ___items_1;

public:
	inline static int32_t get_offset_of_circuit_0() { return static_cast<int32_t>(offsetof(WaypointList_t28075B7540AC2B9E2507C0E6175C6D5B31F7364D, ___circuit_0)); }
	inline WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE * get_circuit_0() const { return ___circuit_0; }
	inline WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE ** get_address_of_circuit_0() { return &___circuit_0; }
	inline void set_circuit_0(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE * value)
	{
		___circuit_0 = value;
		Il2CppCodeGenWriteBarrier((&___circuit_0), value);
	}

	inline static int32_t get_offset_of_items_1() { return static_cast<int32_t>(offsetof(WaypointList_t28075B7540AC2B9E2507C0E6175C6D5B31F7364D, ___items_1)); }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* get_items_1() const { return ___items_1; }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804** get_address_of_items_1() { return &___items_1; }
	inline void set_items_1(TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* value)
	{
		___items_1 = value;
		Il2CppCodeGenWriteBarrier((&___items_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTLIST_T28075B7540AC2B9E2507C0E6175C6D5B31F7364D_H
#ifndef HEALTHCHANGEINFO_TA440FC3535F0441F2500BBE0099909FB97DA21D7_H
#define HEALTHCHANGEINFO_TA440FC3535F0441F2500BBE0099909FB97DA21D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Health.HealthChangeInfo
struct  HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7 
{
public:
	// Core.Health.Damageable Core.Health.HealthChangeInfo::damageable
	Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A * ___damageable_0;
	// System.Single Core.Health.HealthChangeInfo::oldHealth
	float ___oldHealth_1;
	// System.Single Core.Health.HealthChangeInfo::newHealth
	float ___newHealth_2;
	// Core.Health.IAlignmentProvider Core.Health.HealthChangeInfo::damageAlignment
	RuntimeObject* ___damageAlignment_3;

public:
	inline static int32_t get_offset_of_damageable_0() { return static_cast<int32_t>(offsetof(HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7, ___damageable_0)); }
	inline Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A * get_damageable_0() const { return ___damageable_0; }
	inline Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A ** get_address_of_damageable_0() { return &___damageable_0; }
	inline void set_damageable_0(Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A * value)
	{
		___damageable_0 = value;
		Il2CppCodeGenWriteBarrier((&___damageable_0), value);
	}

	inline static int32_t get_offset_of_oldHealth_1() { return static_cast<int32_t>(offsetof(HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7, ___oldHealth_1)); }
	inline float get_oldHealth_1() const { return ___oldHealth_1; }
	inline float* get_address_of_oldHealth_1() { return &___oldHealth_1; }
	inline void set_oldHealth_1(float value)
	{
		___oldHealth_1 = value;
	}

	inline static int32_t get_offset_of_newHealth_2() { return static_cast<int32_t>(offsetof(HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7, ___newHealth_2)); }
	inline float get_newHealth_2() const { return ___newHealth_2; }
	inline float* get_address_of_newHealth_2() { return &___newHealth_2; }
	inline void set_newHealth_2(float value)
	{
		___newHealth_2 = value;
	}

	inline static int32_t get_offset_of_damageAlignment_3() { return static_cast<int32_t>(offsetof(HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7, ___damageAlignment_3)); }
	inline RuntimeObject* get_damageAlignment_3() const { return ___damageAlignment_3; }
	inline RuntimeObject** get_address_of_damageAlignment_3() { return &___damageAlignment_3; }
	inline void set_damageAlignment_3(RuntimeObject* value)
	{
		___damageAlignment_3 = value;
		Il2CppCodeGenWriteBarrier((&___damageAlignment_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Core.Health.HealthChangeInfo
struct HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7_marshaled_pinvoke
{
	Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A * ___damageable_0;
	float ___oldHealth_1;
	float ___newHealth_2;
	RuntimeObject* ___damageAlignment_3;
};
// Native definition for COM marshalling of Core.Health.HealthChangeInfo
struct HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7_marshaled_com
{
	Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A * ___damageable_0;
	float ___oldHealth_1;
	float ___newHealth_2;
	RuntimeObject* ___damageAlignment_3;
};
#endif // HEALTHCHANGEINFO_TA440FC3535F0441F2500BBE0099909FB97DA21D7_H
#ifndef PINCHINFO_T094B9BE005110F688D0F3E91452E7710B4BFFDB9_H
#define PINCHINFO_T094B9BE005110F688D0F3E91452E7710B4BFFDB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.PinchInfo
struct  PinchInfo_t094B9BE005110F688D0F3E91452E7710B4BFFDB9 
{
public:
	// Core.Input.TouchInfo Core.Input.PinchInfo::touch1
	TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2 * ___touch1_0;
	// Core.Input.TouchInfo Core.Input.PinchInfo::touch2
	TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2 * ___touch2_1;

public:
	inline static int32_t get_offset_of_touch1_0() { return static_cast<int32_t>(offsetof(PinchInfo_t094B9BE005110F688D0F3E91452E7710B4BFFDB9, ___touch1_0)); }
	inline TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2 * get_touch1_0() const { return ___touch1_0; }
	inline TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2 ** get_address_of_touch1_0() { return &___touch1_0; }
	inline void set_touch1_0(TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2 * value)
	{
		___touch1_0 = value;
		Il2CppCodeGenWriteBarrier((&___touch1_0), value);
	}

	inline static int32_t get_offset_of_touch2_1() { return static_cast<int32_t>(offsetof(PinchInfo_t094B9BE005110F688D0F3E91452E7710B4BFFDB9, ___touch2_1)); }
	inline TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2 * get_touch2_1() const { return ___touch2_1; }
	inline TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2 ** get_address_of_touch2_1() { return &___touch2_1; }
	inline void set_touch2_1(TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2 * value)
	{
		___touch2_1 = value;
		Il2CppCodeGenWriteBarrier((&___touch2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Core.Input.PinchInfo
struct PinchInfo_t094B9BE005110F688D0F3E91452E7710B4BFFDB9_marshaled_pinvoke
{
	TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2 * ___touch1_0;
	TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2 * ___touch2_1;
};
// Native definition for COM marshalling of Core.Input.PinchInfo
struct PinchInfo_t094B9BE005110F688D0F3E91452E7710B4BFFDB9_marshaled_com
{
	TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2 * ___touch1_0;
	TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2 * ___touch2_1;
};
#endif // PINCHINFO_T094B9BE005110F688D0F3E91452E7710B4BFFDB9_H
#ifndef WHEELINFO_T4290340AD427B74E506E228DC368CEEE742D6F0A_H
#define WHEELINFO_T4290340AD427B74E506E228DC368CEEE742D6F0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.WheelInfo
struct  WheelInfo_t4290340AD427B74E506E228DC368CEEE742D6F0A 
{
public:
	// System.Single Core.Input.WheelInfo::zoomAmount
	float ___zoomAmount_0;

public:
	inline static int32_t get_offset_of_zoomAmount_0() { return static_cast<int32_t>(offsetof(WheelInfo_t4290340AD427B74E506E228DC368CEEE742D6F0A, ___zoomAmount_0)); }
	inline float get_zoomAmount_0() const { return ___zoomAmount_0; }
	inline float* get_address_of_zoomAmount_0() { return &___zoomAmount_0; }
	inline void set_zoomAmount_0(float value)
	{
		___zoomAmount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHEELINFO_T4290340AD427B74E506E228DC368CEEE742D6F0A_H
#ifndef GAMEOBJECTPOOL_T3C29EB821D23EC3ED655342B2218FFFCD437CF8C_H
#define GAMEOBJECTPOOL_T3C29EB821D23EC3ED655342B2218FFFCD437CF8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.GameObjectPool
struct  GameObjectPool_t3C29EB821D23EC3ED655342B2218FFFCD437CF8C  : public Pool_1_t67DD965EB2EB501D49914B0624CBE59393FFF7CA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTPOOL_T3C29EB821D23EC3ED655342B2218FFFCD437CF8C_H
#ifndef HEXPOINT_TFDE14AF0E958CABA7D037DF3F087469BCF0265E4_H
#define HEXPOINT_TFDE14AF0E958CABA7D037DF3F087469BCF0265E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.HexPoint
struct  HexPoint_tFDE14AF0E958CABA7D037DF3F087469BCF0265E4 
{
public:
	// System.Int32 Core.Utilities.HexPoint::x
	int32_t ___x_0;
	// System.Int32 Core.Utilities.HexPoint::y
	int32_t ___y_1;
	// System.Int32 Core.Utilities.HexPoint::z
	int32_t ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(HexPoint_tFDE14AF0E958CABA7D037DF3F087469BCF0265E4, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(HexPoint_tFDE14AF0E958CABA7D037DF3F087469BCF0265E4, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(HexPoint_tFDE14AF0E958CABA7D037DF3F087469BCF0265E4, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEXPOINT_TFDE14AF0E958CABA7D037DF3F087469BCF0265E4_H
#ifndef INTVECTOR2_TB8D88944661CECEA9BC4360526A64507248EDFA9_H
#define INTVECTOR2_TB8D88944661CECEA9BC4360526A64507248EDFA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.IntVector2
struct  IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9 
{
public:
	// System.Int32 Core.Utilities.IntVector2::x
	int32_t ___x_2;
	// System.Int32 Core.Utilities.IntVector2::y
	int32_t ___y_3;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9, ___x_2)); }
	inline int32_t get_x_2() const { return ___x_2; }
	inline int32_t* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(int32_t value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9, ___y_3)); }
	inline int32_t get_y_3() const { return ___y_3; }
	inline int32_t* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(int32_t value)
	{
		___y_3 = value;
	}
};

struct IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9_StaticFields
{
public:
	// Core.Utilities.IntVector2 Core.Utilities.IntVector2::one
	IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  ___one_0;
	// Core.Utilities.IntVector2 Core.Utilities.IntVector2::zero
	IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  ___zero_1;

public:
	inline static int32_t get_offset_of_one_0() { return static_cast<int32_t>(offsetof(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9_StaticFields, ___one_0)); }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  get_one_0() const { return ___one_0; }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9 * get_address_of_one_0() { return &___one_0; }
	inline void set_one_0(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  value)
	{
		___one_0 = value;
	}

	inline static int32_t get_offset_of_zero_1() { return static_cast<int32_t>(offsetof(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9_StaticFields, ___zero_1)); }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  get_zero_1() const { return ___zero_1; }
	inline IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9 * get_address_of_zero_1() { return &___zero_1; }
	inline void set_zero_1(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9  value)
	{
		___zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTVECTOR2_TB8D88944661CECEA9BC4360526A64507248EDFA9_H
#ifndef REPEATINGTIMER_TFEABA0EAF35D64F27E55652FD50AF98799EDC468_H
#define REPEATINGTIMER_TFEABA0EAF35D64F27E55652FD50AF98799EDC468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.RepeatingTimer
struct  RepeatingTimer_tFEABA0EAF35D64F27E55652FD50AF98799EDC468  : public Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPEATINGTIMER_TFEABA0EAF35D64F27E55652FD50AF98799EDC468_H
#ifndef SERIALIZABLEINTERFACE_1_T2C787DCC17132CD1E14699594854C9A79A9F835A_H
#define SERIALIZABLEINTERFACE_1_T2C787DCC17132CD1E14699594854C9A79A9F835A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.SerializableInterface`1<Core.Health.IAlignmentProvider>
struct  SerializableInterface_1_t2C787DCC17132CD1E14699594854C9A79A9F835A  : public SerializableInterface_tB2B751B1CAB73F4FE07575DB97D4A6BCA607F9A7
{
public:
	// T Core.Utilities.SerializableInterface`1::m_InterfaceReference
	RuntimeObject* ___m_InterfaceReference_1;

public:
	inline static int32_t get_offset_of_m_InterfaceReference_1() { return static_cast<int32_t>(offsetof(SerializableInterface_1_t2C787DCC17132CD1E14699594854C9A79A9F835A, ___m_InterfaceReference_1)); }
	inline RuntimeObject* get_m_InterfaceReference_1() const { return ___m_InterfaceReference_1; }
	inline RuntimeObject** get_address_of_m_InterfaceReference_1() { return &___m_InterfaceReference_1; }
	inline void set_m_InterfaceReference_1(RuntimeObject* value)
	{
		___m_InterfaceReference_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_InterfaceReference_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEINTERFACE_1_T2C787DCC17132CD1E14699594854C9A79A9F835A_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UNITYEVENT_1_T106423E0BBFC76C9F9269229342F10D8E0BDD049_H
#define UNITYEVENT_1_T106423E0BBFC76C9F9269229342F10D8E0BDD049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Core.Health.HealthChangeInfo>
struct  UnityEvent_1_t106423E0BBFC76C9F9269229342F10D8E0BDD049  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t106423E0BBFC76C9F9269229342F10D8E0BDD049, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T106423E0BBFC76C9F9269229342F10D8E0BDD049_H
#ifndef UNITYEVENT_1_T1BBE0CB91E7FD66230443FC2358AFD28E1E40D2C_H
#define UNITYEVENT_1_T1BBE0CB91E7FD66230443FC2358AFD28E1E40D2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Core.Health.HitInfo>
struct  UnityEvent_1_t1BBE0CB91E7FD66230443FC2358AFD28E1E40D2C  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1BBE0CB91E7FD66230443FC2358AFD28E1E40D2C, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1BBE0CB91E7FD66230443FC2358AFD28E1E40D2C_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef HEALTHCHANGEEVENT_T7A0991C3BA39B6630C4A93CD11890F01767308BE_H
#define HEALTHCHANGEEVENT_T7A0991C3BA39B6630C4A93CD11890F01767308BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Health.HealthChangeEvent
struct  HealthChangeEvent_t7A0991C3BA39B6630C4A93CD11890F01767308BE  : public UnityEvent_1_t106423E0BBFC76C9F9269229342F10D8E0BDD049
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEALTHCHANGEEVENT_T7A0991C3BA39B6630C4A93CD11890F01767308BE_H
#ifndef HITEVENT_TA814F77C3A27C8D4A9A6EBE40269D28D13288921_H
#define HITEVENT_TA814F77C3A27C8D4A9A6EBE40269D28D13288921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Health.HitEvent
struct  HitEvent_tA814F77C3A27C8D4A9A6EBE40269D28D13288921  : public UnityEvent_1_t1BBE0CB91E7FD66230443FC2358AFD28E1E40D2C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITEVENT_TA814F77C3A27C8D4A9A6EBE40269D28D13288921_H
#ifndef HITINFO_T290C7732A0A53F1933D0D7A416B01FFB79185D2B_H
#define HITINFO_T290C7732A0A53F1933D0D7A416B01FFB79185D2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Health.HitInfo
struct  HitInfo_t290C7732A0A53F1933D0D7A416B01FFB79185D2B 
{
public:
	// Core.Health.HealthChangeInfo Core.Health.HitInfo::m_HealthChangeInfo
	HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7  ___m_HealthChangeInfo_0;
	// UnityEngine.Vector3 Core.Health.HitInfo::m_DamagePoint
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_DamagePoint_1;

public:
	inline static int32_t get_offset_of_m_HealthChangeInfo_0() { return static_cast<int32_t>(offsetof(HitInfo_t290C7732A0A53F1933D0D7A416B01FFB79185D2B, ___m_HealthChangeInfo_0)); }
	inline HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7  get_m_HealthChangeInfo_0() const { return ___m_HealthChangeInfo_0; }
	inline HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7 * get_address_of_m_HealthChangeInfo_0() { return &___m_HealthChangeInfo_0; }
	inline void set_m_HealthChangeInfo_0(HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7  value)
	{
		___m_HealthChangeInfo_0 = value;
	}

	inline static int32_t get_offset_of_m_DamagePoint_1() { return static_cast<int32_t>(offsetof(HitInfo_t290C7732A0A53F1933D0D7A416B01FFB79185D2B, ___m_DamagePoint_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_DamagePoint_1() const { return ___m_DamagePoint_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_DamagePoint_1() { return &___m_DamagePoint_1; }
	inline void set_m_DamagePoint_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_DamagePoint_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Core.Health.HitInfo
struct HitInfo_t290C7732A0A53F1933D0D7A416B01FFB79185D2B_marshaled_pinvoke
{
	HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7_marshaled_pinvoke ___m_HealthChangeInfo_0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_DamagePoint_1;
};
// Native definition for COM marshalling of Core.Health.HitInfo
struct HitInfo_t290C7732A0A53F1933D0D7A416B01FFB79185D2B_marshaled_com
{
	HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7_marshaled_com ___m_HealthChangeInfo_0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_DamagePoint_1;
};
#endif // HITINFO_T290C7732A0A53F1933D0D7A416B01FFB79185D2B_H
#ifndef SERIALIZABLEIALIGNMENTPROVIDER_TD55C30A32481580774C690CC7BFD52BA27CE6C61_H
#define SERIALIZABLEIALIGNMENTPROVIDER_TD55C30A32481580774C690CC7BFD52BA27CE6C61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Health.SerializableIAlignmentProvider
struct  SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61  : public SerializableInterface_1_t2C787DCC17132CD1E14699594854C9A79A9F835A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEIALIGNMENTPROVIDER_TD55C30A32481580774C690CC7BFD52BA27CE6C61_H
#ifndef POINTERINFO_T9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB_H
#define POINTERINFO_T9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.PointerInfo
struct  PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB  : public RuntimeObject
{
public:
	// UnityEngine.Vector2 Core.Input.PointerInfo::currentPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___currentPosition_0;
	// UnityEngine.Vector2 Core.Input.PointerInfo::previousPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___previousPosition_1;
	// UnityEngine.Vector2 Core.Input.PointerInfo::delta
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___delta_2;
	// System.Boolean Core.Input.PointerInfo::startedOverUI
	bool ___startedOverUI_3;

public:
	inline static int32_t get_offset_of_currentPosition_0() { return static_cast<int32_t>(offsetof(PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB, ___currentPosition_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_currentPosition_0() const { return ___currentPosition_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_currentPosition_0() { return &___currentPosition_0; }
	inline void set_currentPosition_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___currentPosition_0 = value;
	}

	inline static int32_t get_offset_of_previousPosition_1() { return static_cast<int32_t>(offsetof(PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB, ___previousPosition_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_previousPosition_1() const { return ___previousPosition_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_previousPosition_1() { return &___previousPosition_1; }
	inline void set_previousPosition_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___previousPosition_1 = value;
	}

	inline static int32_t get_offset_of_delta_2() { return static_cast<int32_t>(offsetof(PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB, ___delta_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_delta_2() const { return ___delta_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_delta_2() { return &___delta_2; }
	inline void set_delta_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___delta_2 = value;
	}

	inline static int32_t get_offset_of_startedOverUI_3() { return static_cast<int32_t>(offsetof(PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB, ___startedOverUI_3)); }
	inline bool get_startedOverUI_3() const { return ___startedOverUI_3; }
	inline bool* get_address_of_startedOverUI_3() { return &___startedOverUI_3; }
	inline void set_startedOverUI_3(bool value)
	{
		___startedOverUI_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERINFO_T9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB_H
#ifndef AUTOGAMEOBJECTPREFABPOOL_T229E733F0B10B86446DB1171D197E02B5B42FBEE_H
#define AUTOGAMEOBJECTPREFABPOOL_T229E733F0B10B86446DB1171D197E02B5B42FBEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.AutoGameObjectPrefabPool
struct  AutoGameObjectPrefabPool_t229E733F0B10B86446DB1171D197E02B5B42FBEE  : public GameObjectPool_t3C29EB821D23EC3ED655342B2218FFFCD437CF8C
{
public:
	// UnityEngine.GameObject Core.Utilities.AutoGameObjectPrefabPool::m_Prefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_Prefab_4;
	// System.Action`1<UnityEngine.GameObject> Core.Utilities.AutoGameObjectPrefabPool::m_Initialize
	Action_1_t9715959F24DDA308D1D281316A802C4107FC7725 * ___m_Initialize_5;

public:
	inline static int32_t get_offset_of_m_Prefab_4() { return static_cast<int32_t>(offsetof(AutoGameObjectPrefabPool_t229E733F0B10B86446DB1171D197E02B5B42FBEE, ___m_Prefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_Prefab_4() const { return ___m_Prefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_Prefab_4() { return &___m_Prefab_4; }
	inline void set_m_Prefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_Prefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Prefab_4), value);
	}

	inline static int32_t get_offset_of_m_Initialize_5() { return static_cast<int32_t>(offsetof(AutoGameObjectPrefabPool_t229E733F0B10B86446DB1171D197E02B5B42FBEE, ___m_Initialize_5)); }
	inline Action_1_t9715959F24DDA308D1D281316A802C4107FC7725 * get_m_Initialize_5() const { return ___m_Initialize_5; }
	inline Action_1_t9715959F24DDA308D1D281316A802C4107FC7725 ** get_address_of_m_Initialize_5() { return &___m_Initialize_5; }
	inline void set_m_Initialize_5(Action_1_t9715959F24DDA308D1D281316A802C4107FC7725 * value)
	{
		___m_Initialize_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Initialize_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOGAMEOBJECTPREFABPOOL_T229E733F0B10B86446DB1171D197E02B5B42FBEE_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#define TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_tD902305F0B673116C42548A58E8BEED50177A33D 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchPhase_tD902305F0B673116C42548A58E8BEED50177A33D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#ifndef TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#define TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t27DBEAB2242247A15EDE96D740F7EB73ACC938DB 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchType_t27DBEAB2242247A15EDE96D740F7EB73ACC938DB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#ifndef ACTIVEINPUTMETHOD_T85F98F3660FBD40776DF0060C53A612634AF8C35_H
#define ACTIVEINPUTMETHOD_T85F98F3660FBD40776DF0060C53A612634AF8C35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod
struct  ActiveInputMethod_t85F98F3660FBD40776DF0060C53A612634AF8C35 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActiveInputMethod_t85F98F3660FBD40776DF0060C53A612634AF8C35, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEINPUTMETHOD_T85F98F3660FBD40776DF0060C53A612634AF8C35_H
#ifndef AXISOPTION_TB37A2B637C3AD16E57FA96263DBEF759EAA95778_H
#define AXISOPTION_TB37A2B637C3AD16E57FA96263DBEF759EAA95778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption
struct  AxisOption_tB37A2B637C3AD16E57FA96263DBEF759EAA95778 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisOption_tB37A2B637C3AD16E57FA96263DBEF759EAA95778, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_TB37A2B637C3AD16E57FA96263DBEF759EAA95778_H
#ifndef MAPPINGTYPE_TEDB7A23D56E34B76DB3E0CB661EE732290967973_H
#define MAPPINGTYPE_TEDB7A23D56E34B76DB3E0CB661EE732290967973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType
struct  MappingType_tEDB7A23D56E34B76DB3E0CB661EE732290967973 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MappingType_tEDB7A23D56E34B76DB3E0CB661EE732290967973, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGTYPE_TEDB7A23D56E34B76DB3E0CB661EE732290967973_H
#ifndef AXISOPTIONS_T4D76D2E6C294064845318452078036E3A1538AD6_H
#define AXISOPTIONS_T4D76D2E6C294064845318452078036E3A1538AD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions
struct  AxisOptions_t4D76D2E6C294064845318452078036E3A1538AD6 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisOptions_t4D76D2E6C294064845318452078036E3A1538AD6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTIONS_T4D76D2E6C294064845318452078036E3A1538AD6_H
#ifndef AXISOPTION_T3B4AED7486BB5A566F38A787A8DCE08E9B08F6CC_H
#define AXISOPTION_T3B4AED7486BB5A566F38A787A8DCE08E9B08F6CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption
struct  AxisOption_t3B4AED7486BB5A566F38A787A8DCE08E9B08F6CC 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisOption_t3B4AED7486BB5A566F38A787A8DCE08E9B08F6CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T3B4AED7486BB5A566F38A787A8DCE08E9B08F6CC_H
#ifndef CONTROLSTYLE_T8B77B7EAA734BCFA3CB594B7E973839AA0CB5975_H
#define CONTROLSTYLE_T8B77B7EAA734BCFA3CB594B7E973839AA0CB5975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle
struct  ControlStyle_t8B77B7EAA734BCFA3CB594B7E973839AA0CB5975 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControlStyle_t8B77B7EAA734BCFA3CB594B7E973839AA0CB5975, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSTYLE_T8B77B7EAA734BCFA3CB594B7E973839AA0CB5975_H
#ifndef VIRTUALINPUT_T139865CDFDEC27280DCAFD0A8116DCED6AB51C44_H
#define VIRTUALINPUT_T139865CDFDEC27280DCAFD0A8116DCED6AB51C44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct  VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::<virtualMousePosition>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CvirtualMousePositionU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualAxes
	Dictionary_2_t2945E29AF5C8B98DCF5C7EB04CC31078A8DBC7E9 * ___m_VirtualAxes_1;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualButtons
	Dictionary_2_t321449D6ED7F1095DE0E435E70AE766EE3327455 * ___m_VirtualButtons_2;
	// System.Collections.Generic.List`1<System.String> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_AlwaysUseVirtual
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___m_AlwaysUseVirtual_3;

public:
	inline static int32_t get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44, ___U3CvirtualMousePositionU3Ek__BackingField_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CvirtualMousePositionU3Ek__BackingField_0() const { return ___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return &___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline void set_U3CvirtualMousePositionU3Ek__BackingField_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CvirtualMousePositionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_VirtualAxes_1() { return static_cast<int32_t>(offsetof(VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44, ___m_VirtualAxes_1)); }
	inline Dictionary_2_t2945E29AF5C8B98DCF5C7EB04CC31078A8DBC7E9 * get_m_VirtualAxes_1() const { return ___m_VirtualAxes_1; }
	inline Dictionary_2_t2945E29AF5C8B98DCF5C7EB04CC31078A8DBC7E9 ** get_address_of_m_VirtualAxes_1() { return &___m_VirtualAxes_1; }
	inline void set_m_VirtualAxes_1(Dictionary_2_t2945E29AF5C8B98DCF5C7EB04CC31078A8DBC7E9 * value)
	{
		___m_VirtualAxes_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualAxes_1), value);
	}

	inline static int32_t get_offset_of_m_VirtualButtons_2() { return static_cast<int32_t>(offsetof(VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44, ___m_VirtualButtons_2)); }
	inline Dictionary_2_t321449D6ED7F1095DE0E435E70AE766EE3327455 * get_m_VirtualButtons_2() const { return ___m_VirtualButtons_2; }
	inline Dictionary_2_t321449D6ED7F1095DE0E435E70AE766EE3327455 ** get_address_of_m_VirtualButtons_2() { return &___m_VirtualButtons_2; }
	inline void set_m_VirtualButtons_2(Dictionary_2_t321449D6ED7F1095DE0E435E70AE766EE3327455 * value)
	{
		___m_VirtualButtons_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualButtons_2), value);
	}

	inline static int32_t get_offset_of_m_AlwaysUseVirtual_3() { return static_cast<int32_t>(offsetof(VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44, ___m_AlwaysUseVirtual_3)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_m_AlwaysUseVirtual_3() const { return ___m_AlwaysUseVirtual_3; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_m_AlwaysUseVirtual_3() { return &___m_AlwaysUseVirtual_3; }
	inline void set_m_AlwaysUseVirtual_3(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___m_AlwaysUseVirtual_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlwaysUseVirtual_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALINPUT_T139865CDFDEC27280DCAFD0A8116DCED6AB51C44_H
#ifndef ACTION_T555B81FF41842ECAE14AABF88618DE8D3189FFCD_H
#define ACTION_T555B81FF41842ECAE14AABF88618DE8D3189FFCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Action
struct  Action_t555B81FF41842ECAE14AABF88618DE8D3189FFCD 
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/Action::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Action_t555B81FF41842ECAE14AABF88618DE8D3189FFCD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T555B81FF41842ECAE14AABF88618DE8D3189FFCD_H
#ifndef ROUTEPOINT_T6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16_H
#define ROUTEPOINT_T6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint
struct  RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16 
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_direction_1() { return static_cast<int32_t>(offsetof(RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16, ___direction_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_direction_1() const { return ___direction_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_direction_1() { return &___direction_1; }
	inline void set_direction_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROUTEPOINT_T6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16_H
#ifndef PROGRESSSTYLE_TB2C07787FC973F9972DF4CDC1A266C420440B108_H
#define PROGRESSSTYLE_TB2C07787FC973F9972DF4CDC1A266C420440B108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle
struct  ProgressStyle_tB2C07787FC973F9972DF4CDC1A266C420440B108 
{
public:
	// System.Int32 UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProgressStyle_tB2C07787FC973F9972DF4CDC1A266C420440B108, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSSTYLE_TB2C07787FC973F9972DF4CDC1A266C420440B108_H
#ifndef MOUSECURSORINFO_TEBCC3A46B40FF3535F41C88F909902DCB99C0A09_H
#define MOUSECURSORINFO_TEBCC3A46B40FF3535F41C88F909902DCB99C0A09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.MouseCursorInfo
struct  MouseCursorInfo_tEBCC3A46B40FF3535F41C88F909902DCB99C0A09  : public PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSECURSORINFO_TEBCC3A46B40FF3535F41C88F909902DCB99C0A09_H
#ifndef POINTERACTIONINFO_T2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8_H
#define POINTERACTIONINFO_T2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.PointerActionInfo
struct  PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8  : public PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB
{
public:
	// UnityEngine.Vector2 Core.Input.PointerActionInfo::startPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___startPosition_4;
	// UnityEngine.Vector2 Core.Input.PointerActionInfo::flickVelocity
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___flickVelocity_5;
	// System.Single Core.Input.PointerActionInfo::totalMovement
	float ___totalMovement_6;
	// System.Single Core.Input.PointerActionInfo::startTime
	float ___startTime_7;
	// System.Boolean Core.Input.PointerActionInfo::isDrag
	bool ___isDrag_8;
	// System.Boolean Core.Input.PointerActionInfo::isHold
	bool ___isHold_9;
	// System.Boolean Core.Input.PointerActionInfo::wasHold
	bool ___wasHold_10;

public:
	inline static int32_t get_offset_of_startPosition_4() { return static_cast<int32_t>(offsetof(PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8, ___startPosition_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_startPosition_4() const { return ___startPosition_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_startPosition_4() { return &___startPosition_4; }
	inline void set_startPosition_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___startPosition_4 = value;
	}

	inline static int32_t get_offset_of_flickVelocity_5() { return static_cast<int32_t>(offsetof(PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8, ___flickVelocity_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_flickVelocity_5() const { return ___flickVelocity_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_flickVelocity_5() { return &___flickVelocity_5; }
	inline void set_flickVelocity_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___flickVelocity_5 = value;
	}

	inline static int32_t get_offset_of_totalMovement_6() { return static_cast<int32_t>(offsetof(PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8, ___totalMovement_6)); }
	inline float get_totalMovement_6() const { return ___totalMovement_6; }
	inline float* get_address_of_totalMovement_6() { return &___totalMovement_6; }
	inline void set_totalMovement_6(float value)
	{
		___totalMovement_6 = value;
	}

	inline static int32_t get_offset_of_startTime_7() { return static_cast<int32_t>(offsetof(PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8, ___startTime_7)); }
	inline float get_startTime_7() const { return ___startTime_7; }
	inline float* get_address_of_startTime_7() { return &___startTime_7; }
	inline void set_startTime_7(float value)
	{
		___startTime_7 = value;
	}

	inline static int32_t get_offset_of_isDrag_8() { return static_cast<int32_t>(offsetof(PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8, ___isDrag_8)); }
	inline bool get_isDrag_8() const { return ___isDrag_8; }
	inline bool* get_address_of_isDrag_8() { return &___isDrag_8; }
	inline void set_isDrag_8(bool value)
	{
		___isDrag_8 = value;
	}

	inline static int32_t get_offset_of_isHold_9() { return static_cast<int32_t>(offsetof(PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8, ___isHold_9)); }
	inline bool get_isHold_9() const { return ___isHold_9; }
	inline bool* get_address_of_isHold_9() { return &___isHold_9; }
	inline void set_isHold_9(bool value)
	{
		___isHold_9 = value;
	}

	inline static int32_t get_offset_of_wasHold_10() { return static_cast<int32_t>(offsetof(PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8, ___wasHold_10)); }
	inline bool get_wasHold_10() const { return ___wasHold_10; }
	inline bool* get_address_of_wasHold_10() { return &___wasHold_10; }
	inline void set_wasHold_10(bool value)
	{
		___wasHold_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERACTIONINFO_T2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef TOUCH_T806752C775BA713A91B6588A07CA98417CABC003_H
#define TOUCH_T806752C775BA713A91B6588A07CA98417CABC003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t806752C775BA713A91B6588A07CA98417CABC003 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Position_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_RawPosition_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_PositionDelta_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T806752C775BA713A91B6588A07CA98417CABC003_H
#ifndef MOBILEINPUT_T049CC24BD98748DE8E06ADDB30005E0182C41A13_H
#define MOBILEINPUT_T049CC24BD98748DE8E06ADDB30005E0182C41A13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
struct  MobileInput_t049CC24BD98748DE8E06ADDB30005E0182C41A13  : public VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEINPUT_T049CC24BD98748DE8E06ADDB30005E0182C41A13_H
#ifndef STANDALONEINPUT_T1FD6E11131D40042C4EBA9CFB3AAEA7695644A2A_H
#define STANDALONEINPUT_T1FD6E11131D40042C4EBA9CFB3AAEA7695644A2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
struct  StandaloneInput_t1FD6E11131D40042C4EBA9CFB3AAEA7695644A2A  : public VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEINPUT_T1FD6E11131D40042C4EBA9CFB3AAEA7695644A2A_H
#ifndef AXISMAPPING_TD681DA8C80391B51E5F8AB14A8F9DD0B62F61ABD_H
#define AXISMAPPING_TD681DA8C80391B51E5F8AB14A8F9DD0B62F61ABD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct  AxisMapping_tD681DA8C80391B51E5F8AB14A8F9DD0B62F61ABD  : public RuntimeObject
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::type
	int32_t ___type_0;
	// System.String UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::axisName
	String_t* ___axisName_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(AxisMapping_tD681DA8C80391B51E5F8AB14A8F9DD0B62F61ABD, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_axisName_1() { return static_cast<int32_t>(offsetof(AxisMapping_tD681DA8C80391B51E5F8AB14A8F9DD0B62F61ABD, ___axisName_1)); }
	inline String_t* get_axisName_1() const { return ___axisName_1; }
	inline String_t** get_address_of_axisName_1() { return &___axisName_1; }
	inline void set_axisName_1(String_t* value)
	{
		___axisName_1 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISMAPPING_TD681DA8C80391B51E5F8AB14A8F9DD0B62F61ABD_H
#ifndef ENTRY_TD15B328556DA14DB79346DCBC40047DB82C2A864_H
#define ENTRY_TD15B328556DA14DB79346DCBC40047DB82C2A864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct  Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityStandardAssets.Utility.TimedObjectActivator/Entry::target
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___target_0;
	// UnityStandardAssets.Utility.TimedObjectActivator/Action UnityStandardAssets.Utility.TimedObjectActivator/Entry::action
	int32_t ___action_1;
	// System.Single UnityStandardAssets.Utility.TimedObjectActivator/Entry::delay
	float ___delay_2;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864, ___target_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_target_0() const { return ___target_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864, ___action_1)); }
	inline int32_t get_action_1() const { return ___action_1; }
	inline int32_t* get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(int32_t value)
	{
		___action_1 = value;
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_TD15B328556DA14DB79346DCBC40047DB82C2A864_H
#ifndef LEVELLIST_TAB7E8C7790A45E96D59254664D7C1B38406882E7_H
#define LEVELLIST_TAB7E8C7790A45E96D59254664D7C1B38406882E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Game.LevelList
struct  LevelList_tAB7E8C7790A45E96D59254664D7C1B38406882E7  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Core.Game.LevelItem[] Core.Game.LevelList::levels
	LevelItemU5BU5D_t270D9F8FC942934C2EE86D06CD1617071D9796E7* ___levels_4;
	// System.Collections.Generic.IDictionary`2<System.String,Core.Game.LevelItem> Core.Game.LevelList::m_LevelDictionary
	RuntimeObject* ___m_LevelDictionary_5;

public:
	inline static int32_t get_offset_of_levels_4() { return static_cast<int32_t>(offsetof(LevelList_tAB7E8C7790A45E96D59254664D7C1B38406882E7, ___levels_4)); }
	inline LevelItemU5BU5D_t270D9F8FC942934C2EE86D06CD1617071D9796E7* get_levels_4() const { return ___levels_4; }
	inline LevelItemU5BU5D_t270D9F8FC942934C2EE86D06CD1617071D9796E7** get_address_of_levels_4() { return &___levels_4; }
	inline void set_levels_4(LevelItemU5BU5D_t270D9F8FC942934C2EE86D06CD1617071D9796E7* value)
	{
		___levels_4 = value;
		Il2CppCodeGenWriteBarrier((&___levels_4), value);
	}

	inline static int32_t get_offset_of_m_LevelDictionary_5() { return static_cast<int32_t>(offsetof(LevelList_tAB7E8C7790A45E96D59254664D7C1B38406882E7, ___m_LevelDictionary_5)); }
	inline RuntimeObject* get_m_LevelDictionary_5() const { return ___m_LevelDictionary_5; }
	inline RuntimeObject** get_address_of_m_LevelDictionary_5() { return &___m_LevelDictionary_5; }
	inline void set_m_LevelDictionary_5(RuntimeObject* value)
	{
		___m_LevelDictionary_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_LevelDictionary_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELLIST_TAB7E8C7790A45E96D59254664D7C1B38406882E7_H
#ifndef SIMPLEALIGNMENT_T5D6492537FFFDB3F1989DEE50A3F598560931296_H
#define SIMPLEALIGNMENT_T5D6492537FFFDB3F1989DEE50A3F598560931296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Health.SimpleAlignment
struct  SimpleAlignment_t5D6492537FFFDB3F1989DEE50A3F598560931296  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<Core.Health.SimpleAlignment> Core.Health.SimpleAlignment::opponents
	List_1_tE750D17233AFE3F19B812157798E916EE6DC7721 * ___opponents_4;

public:
	inline static int32_t get_offset_of_opponents_4() { return static_cast<int32_t>(offsetof(SimpleAlignment_t5D6492537FFFDB3F1989DEE50A3F598560931296, ___opponents_4)); }
	inline List_1_tE750D17233AFE3F19B812157798E916EE6DC7721 * get_opponents_4() const { return ___opponents_4; }
	inline List_1_tE750D17233AFE3F19B812157798E916EE6DC7721 ** get_address_of_opponents_4() { return &___opponents_4; }
	inline void set_opponents_4(List_1_tE750D17233AFE3F19B812157798E916EE6DC7721 * value)
	{
		___opponents_4 = value;
		Il2CppCodeGenWriteBarrier((&___opponents_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEALIGNMENT_T5D6492537FFFDB3F1989DEE50A3F598560931296_H
#ifndef U3CU3EC__DISPLAYCLASS58_0_T18E011E4AF0EB1B5A829735E5210A320161F81E4_H
#define U3CU3EC__DISPLAYCLASS58_0_T18E011E4AF0EB1B5A829735E5210A320161F81E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.InputController/<>c__DisplayClass58_0
struct  U3CU3Ec__DisplayClass58_0_t18E011E4AF0EB1B5A829735E5210A320161F81E4  : public RuntimeObject
{
public:
	// UnityEngine.Touch Core.Input.InputController/<>c__DisplayClass58_0::touch
	Touch_t806752C775BA713A91B6588A07CA98417CABC003  ___touch_0;

public:
	inline static int32_t get_offset_of_touch_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass58_0_t18E011E4AF0EB1B5A829735E5210A320161F81E4, ___touch_0)); }
	inline Touch_t806752C775BA713A91B6588A07CA98417CABC003  get_touch_0() const { return ___touch_0; }
	inline Touch_t806752C775BA713A91B6588A07CA98417CABC003 * get_address_of_touch_0() { return &___touch_0; }
	inline void set_touch_0(Touch_t806752C775BA713A91B6588A07CA98417CABC003  value)
	{
		___touch_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS58_0_T18E011E4AF0EB1B5A829735E5210A320161F81E4_H
#ifndef MOUSEBUTTONINFO_T4FD58E460FD4C794A9F215BD79301752CBF01C0A_H
#define MOUSEBUTTONINFO_T4FD58E460FD4C794A9F215BD79301752CBF01C0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.MouseButtonInfo
struct  MouseButtonInfo_t4FD58E460FD4C794A9F215BD79301752CBF01C0A  : public PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8
{
public:
	// System.Boolean Core.Input.MouseButtonInfo::isDown
	bool ___isDown_11;
	// System.Int32 Core.Input.MouseButtonInfo::mouseButtonId
	int32_t ___mouseButtonId_12;

public:
	inline static int32_t get_offset_of_isDown_11() { return static_cast<int32_t>(offsetof(MouseButtonInfo_t4FD58E460FD4C794A9F215BD79301752CBF01C0A, ___isDown_11)); }
	inline bool get_isDown_11() const { return ___isDown_11; }
	inline bool* get_address_of_isDown_11() { return &___isDown_11; }
	inline void set_isDown_11(bool value)
	{
		___isDown_11 = value;
	}

	inline static int32_t get_offset_of_mouseButtonId_12() { return static_cast<int32_t>(offsetof(MouseButtonInfo_t4FD58E460FD4C794A9F215BD79301752CBF01C0A, ___mouseButtonId_12)); }
	inline int32_t get_mouseButtonId_12() const { return ___mouseButtonId_12; }
	inline int32_t* get_address_of_mouseButtonId_12() { return &___mouseButtonId_12; }
	inline void set_mouseButtonId_12(int32_t value)
	{
		___mouseButtonId_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTONINFO_T4FD58E460FD4C794A9F215BD79301752CBF01C0A_H
#ifndef TOUCHINFO_T134CF8ECAE6843279CC8FDA89E8488650636FDA2_H
#define TOUCHINFO_T134CF8ECAE6843279CC8FDA89E8488650636FDA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.TouchInfo
struct  TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2  : public PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8
{
public:
	// System.Int32 Core.Input.TouchInfo::touchId
	int32_t ___touchId_11;

public:
	inline static int32_t get_offset_of_touchId_11() { return static_cast<int32_t>(offsetof(TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2, ___touchId_11)); }
	inline int32_t get_touchId_11() const { return ___touchId_11; }
	inline int32_t* get_address_of_touchId_11() { return &___touchId_11; }
	inline void set_touchId_11(int32_t value)
	{
		___touchId_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHINFO_T134CF8ECAE6843279CC8FDA89E8488650636FDA2_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef DAMAGEABLEBEHAVIOUR_TA04944F5DEB3599E963255A91160F9074060329F_H
#define DAMAGEABLEBEHAVIOUR_TA04944F5DEB3599E963255A91160F9074060329F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Health.DamageableBehaviour
struct  DamageableBehaviour_tA04944F5DEB3599E963255A91160F9074060329F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Core.Health.Damageable Core.Health.DamageableBehaviour::configuration
	Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A * ___configuration_4;
	// System.Action`1<Core.Health.HitInfo> Core.Health.DamageableBehaviour::hit
	Action_1_tAC22707664B02B17BD3236A855C78B87B86BE22D * ___hit_5;
	// System.Action`1<Core.Health.DamageableBehaviour> Core.Health.DamageableBehaviour::removed
	Action_1_t771534850E52AB2204D871260BDCC1D862F5F599 * ___removed_6;
	// System.Action`1<Core.Health.DamageableBehaviour> Core.Health.DamageableBehaviour::died
	Action_1_t771534850E52AB2204D871260BDCC1D862F5F599 * ___died_7;

public:
	inline static int32_t get_offset_of_configuration_4() { return static_cast<int32_t>(offsetof(DamageableBehaviour_tA04944F5DEB3599E963255A91160F9074060329F, ___configuration_4)); }
	inline Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A * get_configuration_4() const { return ___configuration_4; }
	inline Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A ** get_address_of_configuration_4() { return &___configuration_4; }
	inline void set_configuration_4(Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A * value)
	{
		___configuration_4 = value;
		Il2CppCodeGenWriteBarrier((&___configuration_4), value);
	}

	inline static int32_t get_offset_of_hit_5() { return static_cast<int32_t>(offsetof(DamageableBehaviour_tA04944F5DEB3599E963255A91160F9074060329F, ___hit_5)); }
	inline Action_1_tAC22707664B02B17BD3236A855C78B87B86BE22D * get_hit_5() const { return ___hit_5; }
	inline Action_1_tAC22707664B02B17BD3236A855C78B87B86BE22D ** get_address_of_hit_5() { return &___hit_5; }
	inline void set_hit_5(Action_1_tAC22707664B02B17BD3236A855C78B87B86BE22D * value)
	{
		___hit_5 = value;
		Il2CppCodeGenWriteBarrier((&___hit_5), value);
	}

	inline static int32_t get_offset_of_removed_6() { return static_cast<int32_t>(offsetof(DamageableBehaviour_tA04944F5DEB3599E963255A91160F9074060329F, ___removed_6)); }
	inline Action_1_t771534850E52AB2204D871260BDCC1D862F5F599 * get_removed_6() const { return ___removed_6; }
	inline Action_1_t771534850E52AB2204D871260BDCC1D862F5F599 ** get_address_of_removed_6() { return &___removed_6; }
	inline void set_removed_6(Action_1_t771534850E52AB2204D871260BDCC1D862F5F599 * value)
	{
		___removed_6 = value;
		Il2CppCodeGenWriteBarrier((&___removed_6), value);
	}

	inline static int32_t get_offset_of_died_7() { return static_cast<int32_t>(offsetof(DamageableBehaviour_tA04944F5DEB3599E963255A91160F9074060329F, ___died_7)); }
	inline Action_1_t771534850E52AB2204D871260BDCC1D862F5F599 * get_died_7() const { return ___died_7; }
	inline Action_1_t771534850E52AB2204D871260BDCC1D862F5F599 ** get_address_of_died_7() { return &___died_7; }
	inline void set_died_7(Action_1_t771534850E52AB2204D871260BDCC1D862F5F599 * value)
	{
		___died_7 = value;
		Il2CppCodeGenWriteBarrier((&___died_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAMAGEABLEBEHAVIOUR_TA04944F5DEB3599E963255A91160F9074060329F_H
#ifndef DAMAGEABLELISTENER_T858DEFF680028F0BF08F1CC84A50058A555EABC6_H
#define DAMAGEABLELISTENER_T858DEFF680028F0BF08F1CC84A50058A555EABC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Health.DamageableListener
struct  DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Core.Health.DamageableBehaviour Core.Health.DamageableListener::damageableBehaviour
	DamageableBehaviour_tA04944F5DEB3599E963255A91160F9074060329F * ___damageableBehaviour_4;
	// Core.Health.HealthChangeEvent Core.Health.DamageableListener::damaged
	HealthChangeEvent_t7A0991C3BA39B6630C4A93CD11890F01767308BE * ___damaged_5;
	// Core.Health.HealthChangeEvent Core.Health.DamageableListener::healed
	HealthChangeEvent_t7A0991C3BA39B6630C4A93CD11890F01767308BE * ___healed_6;
	// UnityEngine.Events.UnityEvent Core.Health.DamageableListener::died
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___died_7;
	// UnityEngine.Events.UnityEvent Core.Health.DamageableListener::reachedMaxHealth
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___reachedMaxHealth_8;
	// Core.Health.HealthChangeEvent Core.Health.DamageableListener::healthChanged
	HealthChangeEvent_t7A0991C3BA39B6630C4A93CD11890F01767308BE * ___healthChanged_9;
	// Core.Health.HitEvent Core.Health.DamageableListener::hit
	HitEvent_tA814F77C3A27C8D4A9A6EBE40269D28D13288921 * ___hit_10;

public:
	inline static int32_t get_offset_of_damageableBehaviour_4() { return static_cast<int32_t>(offsetof(DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6, ___damageableBehaviour_4)); }
	inline DamageableBehaviour_tA04944F5DEB3599E963255A91160F9074060329F * get_damageableBehaviour_4() const { return ___damageableBehaviour_4; }
	inline DamageableBehaviour_tA04944F5DEB3599E963255A91160F9074060329F ** get_address_of_damageableBehaviour_4() { return &___damageableBehaviour_4; }
	inline void set_damageableBehaviour_4(DamageableBehaviour_tA04944F5DEB3599E963255A91160F9074060329F * value)
	{
		___damageableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___damageableBehaviour_4), value);
	}

	inline static int32_t get_offset_of_damaged_5() { return static_cast<int32_t>(offsetof(DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6, ___damaged_5)); }
	inline HealthChangeEvent_t7A0991C3BA39B6630C4A93CD11890F01767308BE * get_damaged_5() const { return ___damaged_5; }
	inline HealthChangeEvent_t7A0991C3BA39B6630C4A93CD11890F01767308BE ** get_address_of_damaged_5() { return &___damaged_5; }
	inline void set_damaged_5(HealthChangeEvent_t7A0991C3BA39B6630C4A93CD11890F01767308BE * value)
	{
		___damaged_5 = value;
		Il2CppCodeGenWriteBarrier((&___damaged_5), value);
	}

	inline static int32_t get_offset_of_healed_6() { return static_cast<int32_t>(offsetof(DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6, ___healed_6)); }
	inline HealthChangeEvent_t7A0991C3BA39B6630C4A93CD11890F01767308BE * get_healed_6() const { return ___healed_6; }
	inline HealthChangeEvent_t7A0991C3BA39B6630C4A93CD11890F01767308BE ** get_address_of_healed_6() { return &___healed_6; }
	inline void set_healed_6(HealthChangeEvent_t7A0991C3BA39B6630C4A93CD11890F01767308BE * value)
	{
		___healed_6 = value;
		Il2CppCodeGenWriteBarrier((&___healed_6), value);
	}

	inline static int32_t get_offset_of_died_7() { return static_cast<int32_t>(offsetof(DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6, ___died_7)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_died_7() const { return ___died_7; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_died_7() { return &___died_7; }
	inline void set_died_7(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___died_7 = value;
		Il2CppCodeGenWriteBarrier((&___died_7), value);
	}

	inline static int32_t get_offset_of_reachedMaxHealth_8() { return static_cast<int32_t>(offsetof(DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6, ___reachedMaxHealth_8)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_reachedMaxHealth_8() const { return ___reachedMaxHealth_8; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_reachedMaxHealth_8() { return &___reachedMaxHealth_8; }
	inline void set_reachedMaxHealth_8(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___reachedMaxHealth_8 = value;
		Il2CppCodeGenWriteBarrier((&___reachedMaxHealth_8), value);
	}

	inline static int32_t get_offset_of_healthChanged_9() { return static_cast<int32_t>(offsetof(DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6, ___healthChanged_9)); }
	inline HealthChangeEvent_t7A0991C3BA39B6630C4A93CD11890F01767308BE * get_healthChanged_9() const { return ___healthChanged_9; }
	inline HealthChangeEvent_t7A0991C3BA39B6630C4A93CD11890F01767308BE ** get_address_of_healthChanged_9() { return &___healthChanged_9; }
	inline void set_healthChanged_9(HealthChangeEvent_t7A0991C3BA39B6630C4A93CD11890F01767308BE * value)
	{
		___healthChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___healthChanged_9), value);
	}

	inline static int32_t get_offset_of_hit_10() { return static_cast<int32_t>(offsetof(DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6, ___hit_10)); }
	inline HitEvent_tA814F77C3A27C8D4A9A6EBE40269D28D13288921 * get_hit_10() const { return ___hit_10; }
	inline HitEvent_tA814F77C3A27C8D4A9A6EBE40269D28D13288921 ** get_address_of_hit_10() { return &___hit_10; }
	inline void set_hit_10(HitEvent_tA814F77C3A27C8D4A9A6EBE40269D28D13288921 * value)
	{
		___hit_10 = value;
		Il2CppCodeGenWriteBarrier((&___hit_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAMAGEABLELISTENER_T858DEFF680028F0BF08F1CC84A50058A555EABC6_H
#ifndef INPUTSCHEME_T78F140C9CD80326F0D459CE65156EF66E3218678_H
#define INPUTSCHEME_T78F140C9CD80326F0D459CE65156EF66E3218678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.InputScheme
struct  InputScheme_t78F140C9CD80326F0D459CE65156EF66E3218678  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSCHEME_T78F140C9CD80326F0D459CE65156EF66E3218678_H
#ifndef INPUTSCHEMESWITCHER_T0B26D609BCBC926A38D267CD7B1E22394E809270_H
#define INPUTSCHEMESWITCHER_T0B26D609BCBC926A38D267CD7B1E22394E809270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.InputSchemeSwitcher
struct  InputSchemeSwitcher_t0B26D609BCBC926A38D267CD7B1E22394E809270  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Core.Input.InputScheme[] Core.Input.InputSchemeSwitcher::m_InputSchemes
	InputSchemeU5BU5D_t4C31A62BE86FB155B6597242B14B4A8E709A4BB9* ___m_InputSchemes_4;
	// Core.Input.InputScheme Core.Input.InputSchemeSwitcher::m_DefaultScheme
	InputScheme_t78F140C9CD80326F0D459CE65156EF66E3218678 * ___m_DefaultScheme_5;
	// Core.Input.InputScheme Core.Input.InputSchemeSwitcher::m_CurrentScheme
	InputScheme_t78F140C9CD80326F0D459CE65156EF66E3218678 * ___m_CurrentScheme_6;

public:
	inline static int32_t get_offset_of_m_InputSchemes_4() { return static_cast<int32_t>(offsetof(InputSchemeSwitcher_t0B26D609BCBC926A38D267CD7B1E22394E809270, ___m_InputSchemes_4)); }
	inline InputSchemeU5BU5D_t4C31A62BE86FB155B6597242B14B4A8E709A4BB9* get_m_InputSchemes_4() const { return ___m_InputSchemes_4; }
	inline InputSchemeU5BU5D_t4C31A62BE86FB155B6597242B14B4A8E709A4BB9** get_address_of_m_InputSchemes_4() { return &___m_InputSchemes_4; }
	inline void set_m_InputSchemes_4(InputSchemeU5BU5D_t4C31A62BE86FB155B6597242B14B4A8E709A4BB9* value)
	{
		___m_InputSchemes_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputSchemes_4), value);
	}

	inline static int32_t get_offset_of_m_DefaultScheme_5() { return static_cast<int32_t>(offsetof(InputSchemeSwitcher_t0B26D609BCBC926A38D267CD7B1E22394E809270, ___m_DefaultScheme_5)); }
	inline InputScheme_t78F140C9CD80326F0D459CE65156EF66E3218678 * get_m_DefaultScheme_5() const { return ___m_DefaultScheme_5; }
	inline InputScheme_t78F140C9CD80326F0D459CE65156EF66E3218678 ** get_address_of_m_DefaultScheme_5() { return &___m_DefaultScheme_5; }
	inline void set_m_DefaultScheme_5(InputScheme_t78F140C9CD80326F0D459CE65156EF66E3218678 * value)
	{
		___m_DefaultScheme_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultScheme_5), value);
	}

	inline static int32_t get_offset_of_m_CurrentScheme_6() { return static_cast<int32_t>(offsetof(InputSchemeSwitcher_t0B26D609BCBC926A38D267CD7B1E22394E809270, ___m_CurrentScheme_6)); }
	inline InputScheme_t78F140C9CD80326F0D459CE65156EF66E3218678 * get_m_CurrentScheme_6() const { return ___m_CurrentScheme_6; }
	inline InputScheme_t78F140C9CD80326F0D459CE65156EF66E3218678 ** get_address_of_m_CurrentScheme_6() { return &___m_CurrentScheme_6; }
	inline void set_m_CurrentScheme_6(InputScheme_t78F140C9CD80326F0D459CE65156EF66E3218678 * value)
	{
		___m_CurrentScheme_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentScheme_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSCHEMESWITCHER_T0B26D609BCBC926A38D267CD7B1E22394E809270_H
#ifndef ANIMATINGMAINMENUPAGE_TB189461182DB6C25EB964EA89022715D731EFDA0_H
#define ANIMATINGMAINMENUPAGE_TB189461182DB6C25EB964EA89022715D731EFDA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.UI.AnimatingMainMenuPage
struct  AnimatingMainMenuPage_tB189461182DB6C25EB964EA89022715D731EFDA0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Canvas Core.UI.AnimatingMainMenuPage::canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___canvas_4;

public:
	inline static int32_t get_offset_of_canvas_4() { return static_cast<int32_t>(offsetof(AnimatingMainMenuPage_tB189461182DB6C25EB964EA89022715D731EFDA0, ___canvas_4)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_canvas_4() const { return ___canvas_4; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_canvas_4() { return &___canvas_4; }
	inline void set_canvas_4(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___canvas_4 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATINGMAINMENUPAGE_TB189461182DB6C25EB964EA89022715D731EFDA0_H
#ifndef MAINMENU_TC48FC5C59167013D720749CAE27A82E55C2DEB42_H
#define MAINMENU_TC48FC5C59167013D720749CAE27A82E55C2DEB42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.UI.MainMenu
struct  MainMenu_tC48FC5C59167013D720749CAE27A82E55C2DEB42  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Core.UI.IMainMenuPage Core.UI.MainMenu::m_CurrentPage
	RuntimeObject* ___m_CurrentPage_4;
	// System.Collections.Generic.Stack`1<Core.UI.IMainMenuPage> Core.UI.MainMenu::m_PageStack
	Stack_1_t7E9CC30355F0C75A11303BCC137AB31F0CFEB510 * ___m_PageStack_5;

public:
	inline static int32_t get_offset_of_m_CurrentPage_4() { return static_cast<int32_t>(offsetof(MainMenu_tC48FC5C59167013D720749CAE27A82E55C2DEB42, ___m_CurrentPage_4)); }
	inline RuntimeObject* get_m_CurrentPage_4() const { return ___m_CurrentPage_4; }
	inline RuntimeObject** get_address_of_m_CurrentPage_4() { return &___m_CurrentPage_4; }
	inline void set_m_CurrentPage_4(RuntimeObject* value)
	{
		___m_CurrentPage_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentPage_4), value);
	}

	inline static int32_t get_offset_of_m_PageStack_5() { return static_cast<int32_t>(offsetof(MainMenu_tC48FC5C59167013D720749CAE27A82E55C2DEB42, ___m_PageStack_5)); }
	inline Stack_1_t7E9CC30355F0C75A11303BCC137AB31F0CFEB510 * get_m_PageStack_5() const { return ___m_PageStack_5; }
	inline Stack_1_t7E9CC30355F0C75A11303BCC137AB31F0CFEB510 ** get_address_of_m_PageStack_5() { return &___m_PageStack_5; }
	inline void set_m_PageStack_5(Stack_1_t7E9CC30355F0C75A11303BCC137AB31F0CFEB510 * value)
	{
		___m_PageStack_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PageStack_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENU_TC48FC5C59167013D720749CAE27A82E55C2DEB42_H
#ifndef MODAL_TC8DF509CFEBAC4C21E59851EB18C17C4C74118A1_H
#define MODAL_TC8DF509CFEBAC4C21E59851EB18C17C4C74118A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.UI.Modal
struct  Modal_tC8DF509CFEBAC4C21E59851EB18C17C4C74118A1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.CanvasGroup Core.UI.Modal::canvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___canvasGroup_4;

public:
	inline static int32_t get_offset_of_canvasGroup_4() { return static_cast<int32_t>(offsetof(Modal_tC8DF509CFEBAC4C21E59851EB18C17C4C74118A1, ___canvasGroup_4)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_canvasGroup_4() const { return ___canvasGroup_4; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_canvasGroup_4() { return &___canvasGroup_4; }
	inline void set_canvasGroup_4(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___canvasGroup_4 = value;
		Il2CppCodeGenWriteBarrier((&___canvasGroup_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODAL_TC8DF509CFEBAC4C21E59851EB18C17C4C74118A1_H
#ifndef SIMPLEMAINMENUPAGE_TFE65ADBB869DBE12172A97EB12C2669E767098AC_H
#define SIMPLEMAINMENUPAGE_TFE65ADBB869DBE12172A97EB12C2669E767098AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.UI.SimpleMainMenuPage
struct  SimpleMainMenuPage_tFE65ADBB869DBE12172A97EB12C2669E767098AC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Canvas Core.UI.SimpleMainMenuPage::canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___canvas_4;

public:
	inline static int32_t get_offset_of_canvas_4() { return static_cast<int32_t>(offsetof(SimpleMainMenuPage_tFE65ADBB869DBE12172A97EB12C2669E767098AC, ___canvas_4)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_canvas_4() const { return ___canvas_4; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_canvas_4() { return &___canvas_4; }
	inline void set_canvas_4(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___canvas_4 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEMAINMENUPAGE_TFE65ADBB869DBE12172A97EB12C2669E767098AC_H
#ifndef POOLABLE_TBBCA15FD94FB330CD5A30C65692500ED9BA65D2F_H
#define POOLABLE_TBBCA15FD94FB330CD5A30C65692500ED9BA65D2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Poolable
struct  Poolable_tBBCA15FD94FB330CD5A30C65692500ED9BA65D2F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Core.Utilities.Poolable::initialPoolCapacity
	int32_t ___initialPoolCapacity_4;
	// Core.Utilities.Pool`1<Core.Utilities.Poolable> Core.Utilities.Poolable::pool
	Pool_1_tA688FE7A8096EF8D7EADA8644CA3B87A9356A2D7 * ___pool_5;

public:
	inline static int32_t get_offset_of_initialPoolCapacity_4() { return static_cast<int32_t>(offsetof(Poolable_tBBCA15FD94FB330CD5A30C65692500ED9BA65D2F, ___initialPoolCapacity_4)); }
	inline int32_t get_initialPoolCapacity_4() const { return ___initialPoolCapacity_4; }
	inline int32_t* get_address_of_initialPoolCapacity_4() { return &___initialPoolCapacity_4; }
	inline void set_initialPoolCapacity_4(int32_t value)
	{
		___initialPoolCapacity_4 = value;
	}

	inline static int32_t get_offset_of_pool_5() { return static_cast<int32_t>(offsetof(Poolable_tBBCA15FD94FB330CD5A30C65692500ED9BA65D2F, ___pool_5)); }
	inline Pool_1_tA688FE7A8096EF8D7EADA8644CA3B87A9356A2D7 * get_pool_5() const { return ___pool_5; }
	inline Pool_1_tA688FE7A8096EF8D7EADA8644CA3B87A9356A2D7 ** get_address_of_pool_5() { return &___pool_5; }
	inline void set_pool_5(Pool_1_tA688FE7A8096EF8D7EADA8644CA3B87A9356A2D7 * value)
	{
		___pool_5 = value;
		Il2CppCodeGenWriteBarrier((&___pool_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLABLE_TBBCA15FD94FB330CD5A30C65692500ED9BA65D2F_H
#ifndef SINGLETON_1_TD9F825EC34DD92F207C776721B0BB94C86D58536_H
#define SINGLETON_1_TD9F825EC34DD92F207C776721B0BB94C86D58536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<Core.Input.InputController>
struct  Singleton_1_tD9F825EC34DD92F207C776721B0BB94C86D58536  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_tD9F825EC34DD92F207C776721B0BB94C86D58536_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_tD9F825EC34DD92F207C776721B0BB94C86D58536_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_TD9F825EC34DD92F207C776721B0BB94C86D58536_H
#ifndef SINGLETON_1_T0A072F12096883D64D8936C7E8E499271D434D08_H
#define SINGLETON_1_T0A072F12096883D64D8936C7E8E499271D434D08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<Core.Utilities.PoolManager>
struct  Singleton_1_t0A072F12096883D64D8936C7E8E499271D434D08  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t0A072F12096883D64D8936C7E8E499271D434D08_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	PoolManager_tC4AA76A7B5FF355D068EEC877E2680E2DC9E771F * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_t0A072F12096883D64D8936C7E8E499271D434D08_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline PoolManager_tC4AA76A7B5FF355D068EEC877E2680E2DC9E771F * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline PoolManager_tC4AA76A7B5FF355D068EEC877E2680E2DC9E771F ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(PoolManager_tC4AA76A7B5FF355D068EEC877E2680E2DC9E771F * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T0A072F12096883D64D8936C7E8E499271D434D08_H
#ifndef SINGLETON_1_TCCDC37ABE94E8327269A8C833E467BD733005DCE_H
#define SINGLETON_1_TCCDC37ABE94E8327269A8C833E467BD733005DCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<Level.Waves.WaveManager>
struct  Singleton_1_tCCDC37ABE94E8327269A8C833E467BD733005DCE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_tCCDC37ABE94E8327269A8C833E467BD733005DCE_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_tCCDC37ABE94E8327269A8C833E467BD733005DCE_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_TCCDC37ABE94E8327269A8C833E467BD733005DCE_H
#ifndef TIMEDBEHAVIOUR_T4297CA025FC6BC8794A1151BC41DE0ACB076CCCA_H
#define TIMEDBEHAVIOUR_T4297CA025FC6BC8794A1151BC41DE0ACB076CCCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.TimedBehaviour
struct  TimedBehaviour_t4297CA025FC6BC8794A1151BC41DE0ACB076CCCA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<Core.Utilities.Timer> Core.Utilities.TimedBehaviour::m_ActiveTimers
	List_1_tE20E361D7E2F208DA9910E19F3009930FB383D85 * ___m_ActiveTimers_4;

public:
	inline static int32_t get_offset_of_m_ActiveTimers_4() { return static_cast<int32_t>(offsetof(TimedBehaviour_t4297CA025FC6BC8794A1151BC41DE0ACB076CCCA, ___m_ActiveTimers_4)); }
	inline List_1_tE20E361D7E2F208DA9910E19F3009930FB383D85 * get_m_ActiveTimers_4() const { return ___m_ActiveTimers_4; }
	inline List_1_tE20E361D7E2F208DA9910E19F3009930FB383D85 ** get_address_of_m_ActiveTimers_4() { return &___m_ActiveTimers_4; }
	inline void set_m_ActiveTimers_4(List_1_tE20E361D7E2F208DA9910E19F3009930FB383D85 * value)
	{
		___m_ActiveTimers_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActiveTimers_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDBEHAVIOUR_T4297CA025FC6BC8794A1151BC41DE0ACB076CCCA_H
#ifndef AXISTOUCHBUTTON_TD2C8F0EA8741A7896A784CEABB1B2434F080C33D_H
#define AXISTOUCHBUTTON_TD2C8F0EA8741A7896A784CEABB1B2434F080C33D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
struct  AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisName
	String_t* ___axisName_4;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisValue
	float ___axisValue_5;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::responseSpeed
	float ___responseSpeed_6;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::returnToCentreSpeed
	float ___returnToCentreSpeed_7;
	// UnityStandardAssets.CrossPlatformInput.AxisTouchButton UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_PairedWith
	AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D * ___m_PairedWith_8;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_Axis
	VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * ___m_Axis_9;

public:
	inline static int32_t get_offset_of_axisName_4() { return static_cast<int32_t>(offsetof(AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D, ___axisName_4)); }
	inline String_t* get_axisName_4() const { return ___axisName_4; }
	inline String_t** get_address_of_axisName_4() { return &___axisName_4; }
	inline void set_axisName_4(String_t* value)
	{
		___axisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_4), value);
	}

	inline static int32_t get_offset_of_axisValue_5() { return static_cast<int32_t>(offsetof(AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D, ___axisValue_5)); }
	inline float get_axisValue_5() const { return ___axisValue_5; }
	inline float* get_address_of_axisValue_5() { return &___axisValue_5; }
	inline void set_axisValue_5(float value)
	{
		___axisValue_5 = value;
	}

	inline static int32_t get_offset_of_responseSpeed_6() { return static_cast<int32_t>(offsetof(AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D, ___responseSpeed_6)); }
	inline float get_responseSpeed_6() const { return ___responseSpeed_6; }
	inline float* get_address_of_responseSpeed_6() { return &___responseSpeed_6; }
	inline void set_responseSpeed_6(float value)
	{
		___responseSpeed_6 = value;
	}

	inline static int32_t get_offset_of_returnToCentreSpeed_7() { return static_cast<int32_t>(offsetof(AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D, ___returnToCentreSpeed_7)); }
	inline float get_returnToCentreSpeed_7() const { return ___returnToCentreSpeed_7; }
	inline float* get_address_of_returnToCentreSpeed_7() { return &___returnToCentreSpeed_7; }
	inline void set_returnToCentreSpeed_7(float value)
	{
		___returnToCentreSpeed_7 = value;
	}

	inline static int32_t get_offset_of_m_PairedWith_8() { return static_cast<int32_t>(offsetof(AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D, ___m_PairedWith_8)); }
	inline AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D * get_m_PairedWith_8() const { return ___m_PairedWith_8; }
	inline AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D ** get_address_of_m_PairedWith_8() { return &___m_PairedWith_8; }
	inline void set_m_PairedWith_8(AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D * value)
	{
		___m_PairedWith_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_PairedWith_8), value);
	}

	inline static int32_t get_offset_of_m_Axis_9() { return static_cast<int32_t>(offsetof(AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D, ___m_Axis_9)); }
	inline VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * get_m_Axis_9() const { return ___m_Axis_9; }
	inline VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 ** get_address_of_m_Axis_9() { return &___m_Axis_9; }
	inline void set_m_Axis_9(VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * value)
	{
		___m_Axis_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Axis_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTOUCHBUTTON_TD2C8F0EA8741A7896A784CEABB1B2434F080C33D_H
#ifndef BUTTONHANDLER_TF3AE341BBD2737A5A0E4F0736532CC2DA07F022B_H
#define BUTTONHANDLER_TF3AE341BBD2737A5A0E4F0736532CC2DA07F022B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.ButtonHandler
struct  ButtonHandler_tF3AE341BBD2737A5A0E4F0736532CC2DA07F022B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.ButtonHandler::Name
	String_t* ___Name_4;

public:
	inline static int32_t get_offset_of_Name_4() { return static_cast<int32_t>(offsetof(ButtonHandler_tF3AE341BBD2737A5A0E4F0736532CC2DA07F022B, ___Name_4)); }
	inline String_t* get_Name_4() const { return ___Name_4; }
	inline String_t** get_address_of_Name_4() { return &___Name_4; }
	inline void set_Name_4(String_t* value)
	{
		___Name_4 = value;
		Il2CppCodeGenWriteBarrier((&___Name_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONHANDLER_TF3AE341BBD2737A5A0E4F0736532CC2DA07F022B_H
#ifndef INPUTAXISSCROLLBAR_T22CF1CCDEDC91CD0B02149CB598719AB3313015E_H
#define INPUTAXISSCROLLBAR_T22CF1CCDEDC91CD0B02149CB598719AB3313015E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
struct  InputAxisScrollbar_t22CF1CCDEDC91CD0B02149CB598719AB3313015E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::axis
	String_t* ___axis_4;

public:
	inline static int32_t get_offset_of_axis_4() { return static_cast<int32_t>(offsetof(InputAxisScrollbar_t22CF1CCDEDC91CD0B02149CB598719AB3313015E, ___axis_4)); }
	inline String_t* get_axis_4() const { return ___axis_4; }
	inline String_t** get_address_of_axis_4() { return &___axis_4; }
	inline void set_axis_4(String_t* value)
	{
		___axis_4 = value;
		Il2CppCodeGenWriteBarrier((&___axis_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTAXISSCROLLBAR_T22CF1CCDEDC91CD0B02149CB598719AB3313015E_H
#ifndef JOYSTICK_T5B6E095A016AA630E6D613AFFAF403C73601C148_H
#define JOYSTICK_T5B6E095A016AA630E6D613AFFAF403C73601C148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick
struct  Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick::MovementRange
	int32_t ___MovementRange_4;
	// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption UnityStandardAssets.CrossPlatformInput.Joystick::axesToUse
	int32_t ___axesToUse_5;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::horizontalAxisName
	String_t* ___horizontalAxisName_6;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::verticalAxisName
	String_t* ___verticalAxisName_7;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.Joystick::m_StartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_StartPos_8;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseX
	bool ___m_UseX_9;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseY
	bool ___m_UseY_10;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_HorizontalVirtualAxis
	VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * ___m_HorizontalVirtualAxis_11;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_VerticalVirtualAxis
	VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * ___m_VerticalVirtualAxis_12;

public:
	inline static int32_t get_offset_of_MovementRange_4() { return static_cast<int32_t>(offsetof(Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148, ___MovementRange_4)); }
	inline int32_t get_MovementRange_4() const { return ___MovementRange_4; }
	inline int32_t* get_address_of_MovementRange_4() { return &___MovementRange_4; }
	inline void set_MovementRange_4(int32_t value)
	{
		___MovementRange_4 = value;
	}

	inline static int32_t get_offset_of_axesToUse_5() { return static_cast<int32_t>(offsetof(Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148, ___axesToUse_5)); }
	inline int32_t get_axesToUse_5() const { return ___axesToUse_5; }
	inline int32_t* get_address_of_axesToUse_5() { return &___axesToUse_5; }
	inline void set_axesToUse_5(int32_t value)
	{
		___axesToUse_5 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_6() { return static_cast<int32_t>(offsetof(Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148, ___horizontalAxisName_6)); }
	inline String_t* get_horizontalAxisName_6() const { return ___horizontalAxisName_6; }
	inline String_t** get_address_of_horizontalAxisName_6() { return &___horizontalAxisName_6; }
	inline void set_horizontalAxisName_6(String_t* value)
	{
		___horizontalAxisName_6 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_6), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_7() { return static_cast<int32_t>(offsetof(Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148, ___verticalAxisName_7)); }
	inline String_t* get_verticalAxisName_7() const { return ___verticalAxisName_7; }
	inline String_t** get_address_of_verticalAxisName_7() { return &___verticalAxisName_7; }
	inline void set_verticalAxisName_7(String_t* value)
	{
		___verticalAxisName_7 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_7), value);
	}

	inline static int32_t get_offset_of_m_StartPos_8() { return static_cast<int32_t>(offsetof(Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148, ___m_StartPos_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_StartPos_8() const { return ___m_StartPos_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_StartPos_8() { return &___m_StartPos_8; }
	inline void set_m_StartPos_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_StartPos_8 = value;
	}

	inline static int32_t get_offset_of_m_UseX_9() { return static_cast<int32_t>(offsetof(Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148, ___m_UseX_9)); }
	inline bool get_m_UseX_9() const { return ___m_UseX_9; }
	inline bool* get_address_of_m_UseX_9() { return &___m_UseX_9; }
	inline void set_m_UseX_9(bool value)
	{
		___m_UseX_9 = value;
	}

	inline static int32_t get_offset_of_m_UseY_10() { return static_cast<int32_t>(offsetof(Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148, ___m_UseY_10)); }
	inline bool get_m_UseY_10() const { return ___m_UseY_10; }
	inline bool* get_address_of_m_UseY_10() { return &___m_UseY_10; }
	inline void set_m_UseY_10(bool value)
	{
		___m_UseY_10 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_11() { return static_cast<int32_t>(offsetof(Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148, ___m_HorizontalVirtualAxis_11)); }
	inline VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * get_m_HorizontalVirtualAxis_11() const { return ___m_HorizontalVirtualAxis_11; }
	inline VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 ** get_address_of_m_HorizontalVirtualAxis_11() { return &___m_HorizontalVirtualAxis_11; }
	inline void set_m_HorizontalVirtualAxis_11(VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * value)
	{
		___m_HorizontalVirtualAxis_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_11), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_12() { return static_cast<int32_t>(offsetof(Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148, ___m_VerticalVirtualAxis_12)); }
	inline VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * get_m_VerticalVirtualAxis_12() const { return ___m_VerticalVirtualAxis_12; }
	inline VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 ** get_address_of_m_VerticalVirtualAxis_12() { return &___m_VerticalVirtualAxis_12; }
	inline void set_m_VerticalVirtualAxis_12(VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * value)
	{
		___m_VerticalVirtualAxis_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T5B6E095A016AA630E6D613AFFAF403C73601C148_H
#ifndef MOBILECONTROLRIG_TA47FA373A5534CAB0AE37FEF5DB990FAC290A89B_H
#define MOBILECONTROLRIG_TA47FA373A5534CAB0AE37FEF5DB990FAC290A89B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.MobileControlRig
struct  MobileControlRig_tA47FA373A5534CAB0AE37FEF5DB990FAC290A89B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILECONTROLRIG_TA47FA373A5534CAB0AE37FEF5DB990FAC290A89B_H
#ifndef TILTINPUT_TD86B151ED46EA7E5A6FEF6AF2C5C48E7014EF9F2_H
#define TILTINPUT_TD86B151ED46EA7E5A6FEF6AF2C5C48E7014EF9F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput
struct  TiltInput_tD86B151ED46EA7E5A6FEF6AF2C5C48E7014EF9F2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping UnityStandardAssets.CrossPlatformInput.TiltInput::mapping
	AxisMapping_tD681DA8C80391B51E5F8AB14A8F9DD0B62F61ABD * ___mapping_4;
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions UnityStandardAssets.CrossPlatformInput.TiltInput::tiltAroundAxis
	int32_t ___tiltAroundAxis_5;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::fullTiltAngle
	float ___fullTiltAngle_6;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::centreAngleOffset
	float ___centreAngleOffset_7;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TiltInput::m_SteerAxis
	VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * ___m_SteerAxis_8;

public:
	inline static int32_t get_offset_of_mapping_4() { return static_cast<int32_t>(offsetof(TiltInput_tD86B151ED46EA7E5A6FEF6AF2C5C48E7014EF9F2, ___mapping_4)); }
	inline AxisMapping_tD681DA8C80391B51E5F8AB14A8F9DD0B62F61ABD * get_mapping_4() const { return ___mapping_4; }
	inline AxisMapping_tD681DA8C80391B51E5F8AB14A8F9DD0B62F61ABD ** get_address_of_mapping_4() { return &___mapping_4; }
	inline void set_mapping_4(AxisMapping_tD681DA8C80391B51E5F8AB14A8F9DD0B62F61ABD * value)
	{
		___mapping_4 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_4), value);
	}

	inline static int32_t get_offset_of_tiltAroundAxis_5() { return static_cast<int32_t>(offsetof(TiltInput_tD86B151ED46EA7E5A6FEF6AF2C5C48E7014EF9F2, ___tiltAroundAxis_5)); }
	inline int32_t get_tiltAroundAxis_5() const { return ___tiltAroundAxis_5; }
	inline int32_t* get_address_of_tiltAroundAxis_5() { return &___tiltAroundAxis_5; }
	inline void set_tiltAroundAxis_5(int32_t value)
	{
		___tiltAroundAxis_5 = value;
	}

	inline static int32_t get_offset_of_fullTiltAngle_6() { return static_cast<int32_t>(offsetof(TiltInput_tD86B151ED46EA7E5A6FEF6AF2C5C48E7014EF9F2, ___fullTiltAngle_6)); }
	inline float get_fullTiltAngle_6() const { return ___fullTiltAngle_6; }
	inline float* get_address_of_fullTiltAngle_6() { return &___fullTiltAngle_6; }
	inline void set_fullTiltAngle_6(float value)
	{
		___fullTiltAngle_6 = value;
	}

	inline static int32_t get_offset_of_centreAngleOffset_7() { return static_cast<int32_t>(offsetof(TiltInput_tD86B151ED46EA7E5A6FEF6AF2C5C48E7014EF9F2, ___centreAngleOffset_7)); }
	inline float get_centreAngleOffset_7() const { return ___centreAngleOffset_7; }
	inline float* get_address_of_centreAngleOffset_7() { return &___centreAngleOffset_7; }
	inline void set_centreAngleOffset_7(float value)
	{
		___centreAngleOffset_7 = value;
	}

	inline static int32_t get_offset_of_m_SteerAxis_8() { return static_cast<int32_t>(offsetof(TiltInput_tD86B151ED46EA7E5A6FEF6AF2C5C48E7014EF9F2, ___m_SteerAxis_8)); }
	inline VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * get_m_SteerAxis_8() const { return ___m_SteerAxis_8; }
	inline VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 ** get_address_of_m_SteerAxis_8() { return &___m_SteerAxis_8; }
	inline void set_m_SteerAxis_8(VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * value)
	{
		___m_SteerAxis_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SteerAxis_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTINPUT_TD86B151ED46EA7E5A6FEF6AF2C5C48E7014EF9F2_H
#ifndef TOUCHPAD_TA8387389C0CA557CD9BF6A1D40C7DC6D4804E930_H
#define TOUCHPAD_TA8387389C0CA557CD9BF6A1D40C7DC6D4804E930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad
struct  TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption UnityStandardAssets.CrossPlatformInput.TouchPad::axesToUse
	int32_t ___axesToUse_4;
	// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle UnityStandardAssets.CrossPlatformInput.TouchPad::controlStyle
	int32_t ___controlStyle_5;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::horizontalAxisName
	String_t* ___horizontalAxisName_6;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::verticalAxisName
	String_t* ___verticalAxisName_7;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Xsensitivity
	float ___Xsensitivity_8;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Ysensitivity
	float ___Ysensitivity_9;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_StartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_StartPos_10;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousDelta
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PreviousDelta_11;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_JoytickOutput
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_JoytickOutput_12;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseX
	bool ___m_UseX_13;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseY
	bool ___m_UseY_14;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_HorizontalVirtualAxis
	VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * ___m_HorizontalVirtualAxis_15;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_VerticalVirtualAxis
	VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * ___m_VerticalVirtualAxis_16;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_Dragging
	bool ___m_Dragging_17;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Id
	int32_t ___m_Id_18;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousTouchPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PreviousTouchPos_19;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_20;
	// UnityEngine.UI.Image UnityStandardAssets.CrossPlatformInput.TouchPad::m_Image
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_Image_21;

public:
	inline static int32_t get_offset_of_axesToUse_4() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___axesToUse_4)); }
	inline int32_t get_axesToUse_4() const { return ___axesToUse_4; }
	inline int32_t* get_address_of_axesToUse_4() { return &___axesToUse_4; }
	inline void set_axesToUse_4(int32_t value)
	{
		___axesToUse_4 = value;
	}

	inline static int32_t get_offset_of_controlStyle_5() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___controlStyle_5)); }
	inline int32_t get_controlStyle_5() const { return ___controlStyle_5; }
	inline int32_t* get_address_of_controlStyle_5() { return &___controlStyle_5; }
	inline void set_controlStyle_5(int32_t value)
	{
		___controlStyle_5 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_6() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___horizontalAxisName_6)); }
	inline String_t* get_horizontalAxisName_6() const { return ___horizontalAxisName_6; }
	inline String_t** get_address_of_horizontalAxisName_6() { return &___horizontalAxisName_6; }
	inline void set_horizontalAxisName_6(String_t* value)
	{
		___horizontalAxisName_6 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_6), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_7() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___verticalAxisName_7)); }
	inline String_t* get_verticalAxisName_7() const { return ___verticalAxisName_7; }
	inline String_t** get_address_of_verticalAxisName_7() { return &___verticalAxisName_7; }
	inline void set_verticalAxisName_7(String_t* value)
	{
		___verticalAxisName_7 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_7), value);
	}

	inline static int32_t get_offset_of_Xsensitivity_8() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___Xsensitivity_8)); }
	inline float get_Xsensitivity_8() const { return ___Xsensitivity_8; }
	inline float* get_address_of_Xsensitivity_8() { return &___Xsensitivity_8; }
	inline void set_Xsensitivity_8(float value)
	{
		___Xsensitivity_8 = value;
	}

	inline static int32_t get_offset_of_Ysensitivity_9() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___Ysensitivity_9)); }
	inline float get_Ysensitivity_9() const { return ___Ysensitivity_9; }
	inline float* get_address_of_Ysensitivity_9() { return &___Ysensitivity_9; }
	inline void set_Ysensitivity_9(float value)
	{
		___Ysensitivity_9 = value;
	}

	inline static int32_t get_offset_of_m_StartPos_10() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___m_StartPos_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_StartPos_10() const { return ___m_StartPos_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_StartPos_10() { return &___m_StartPos_10; }
	inline void set_m_StartPos_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_StartPos_10 = value;
	}

	inline static int32_t get_offset_of_m_PreviousDelta_11() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___m_PreviousDelta_11)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PreviousDelta_11() const { return ___m_PreviousDelta_11; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PreviousDelta_11() { return &___m_PreviousDelta_11; }
	inline void set_m_PreviousDelta_11(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PreviousDelta_11 = value;
	}

	inline static int32_t get_offset_of_m_JoytickOutput_12() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___m_JoytickOutput_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_JoytickOutput_12() const { return ___m_JoytickOutput_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_JoytickOutput_12() { return &___m_JoytickOutput_12; }
	inline void set_m_JoytickOutput_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_JoytickOutput_12 = value;
	}

	inline static int32_t get_offset_of_m_UseX_13() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___m_UseX_13)); }
	inline bool get_m_UseX_13() const { return ___m_UseX_13; }
	inline bool* get_address_of_m_UseX_13() { return &___m_UseX_13; }
	inline void set_m_UseX_13(bool value)
	{
		___m_UseX_13 = value;
	}

	inline static int32_t get_offset_of_m_UseY_14() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___m_UseY_14)); }
	inline bool get_m_UseY_14() const { return ___m_UseY_14; }
	inline bool* get_address_of_m_UseY_14() { return &___m_UseY_14; }
	inline void set_m_UseY_14(bool value)
	{
		___m_UseY_14 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_15() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___m_HorizontalVirtualAxis_15)); }
	inline VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * get_m_HorizontalVirtualAxis_15() const { return ___m_HorizontalVirtualAxis_15; }
	inline VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 ** get_address_of_m_HorizontalVirtualAxis_15() { return &___m_HorizontalVirtualAxis_15; }
	inline void set_m_HorizontalVirtualAxis_15(VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * value)
	{
		___m_HorizontalVirtualAxis_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_15), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_16() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___m_VerticalVirtualAxis_16)); }
	inline VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * get_m_VerticalVirtualAxis_16() const { return ___m_VerticalVirtualAxis_16; }
	inline VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 ** get_address_of_m_VerticalVirtualAxis_16() { return &___m_VerticalVirtualAxis_16; }
	inline void set_m_VerticalVirtualAxis_16(VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2 * value)
	{
		___m_VerticalVirtualAxis_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_16), value);
	}

	inline static int32_t get_offset_of_m_Dragging_17() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___m_Dragging_17)); }
	inline bool get_m_Dragging_17() const { return ___m_Dragging_17; }
	inline bool* get_address_of_m_Dragging_17() { return &___m_Dragging_17; }
	inline void set_m_Dragging_17(bool value)
	{
		___m_Dragging_17 = value;
	}

	inline static int32_t get_offset_of_m_Id_18() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___m_Id_18)); }
	inline int32_t get_m_Id_18() const { return ___m_Id_18; }
	inline int32_t* get_address_of_m_Id_18() { return &___m_Id_18; }
	inline void set_m_Id_18(int32_t value)
	{
		___m_Id_18 = value;
	}

	inline static int32_t get_offset_of_m_PreviousTouchPos_19() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___m_PreviousTouchPos_19)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PreviousTouchPos_19() const { return ___m_PreviousTouchPos_19; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PreviousTouchPos_19() { return &___m_PreviousTouchPos_19; }
	inline void set_m_PreviousTouchPos_19(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PreviousTouchPos_19 = value;
	}

	inline static int32_t get_offset_of_m_Center_20() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___m_Center_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_20() const { return ___m_Center_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_20() { return &___m_Center_20; }
	inline void set_m_Center_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_20 = value;
	}

	inline static int32_t get_offset_of_m_Image_21() { return static_cast<int32_t>(offsetof(TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930, ___m_Image_21)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_Image_21() const { return ___m_Image_21; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_Image_21() { return &___m_Image_21; }
	inline void set_m_Image_21(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_Image_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPAD_TA8387389C0CA557CD9BF6A1D40C7DC6D4804E930_H
#ifndef AFTERBURNERPHYSICSFORCE_T9F0D1071F45FDA99D798A1C46886A26A592F8408_H
#define AFTERBURNERPHYSICSFORCE_T9F0D1071F45FDA99D798A1C46886A26A592F8408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.AfterburnerPhysicsForce
struct  AfterburnerPhysicsForce_t9F0D1071F45FDA99D798A1C46886A26A592F8408  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::effectAngle
	float ___effectAngle_4;
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::effectWidth
	float ___effectWidth_5;
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::effectDistance
	float ___effectDistance_6;
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::force
	float ___force_7;
	// UnityEngine.Collider[] UnityStandardAssets.Effects.AfterburnerPhysicsForce::m_Cols
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* ___m_Cols_8;
	// UnityEngine.SphereCollider UnityStandardAssets.Effects.AfterburnerPhysicsForce::m_Sphere
	SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ___m_Sphere_9;

public:
	inline static int32_t get_offset_of_effectAngle_4() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t9F0D1071F45FDA99D798A1C46886A26A592F8408, ___effectAngle_4)); }
	inline float get_effectAngle_4() const { return ___effectAngle_4; }
	inline float* get_address_of_effectAngle_4() { return &___effectAngle_4; }
	inline void set_effectAngle_4(float value)
	{
		___effectAngle_4 = value;
	}

	inline static int32_t get_offset_of_effectWidth_5() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t9F0D1071F45FDA99D798A1C46886A26A592F8408, ___effectWidth_5)); }
	inline float get_effectWidth_5() const { return ___effectWidth_5; }
	inline float* get_address_of_effectWidth_5() { return &___effectWidth_5; }
	inline void set_effectWidth_5(float value)
	{
		___effectWidth_5 = value;
	}

	inline static int32_t get_offset_of_effectDistance_6() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t9F0D1071F45FDA99D798A1C46886A26A592F8408, ___effectDistance_6)); }
	inline float get_effectDistance_6() const { return ___effectDistance_6; }
	inline float* get_address_of_effectDistance_6() { return &___effectDistance_6; }
	inline void set_effectDistance_6(float value)
	{
		___effectDistance_6 = value;
	}

	inline static int32_t get_offset_of_force_7() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t9F0D1071F45FDA99D798A1C46886A26A592F8408, ___force_7)); }
	inline float get_force_7() const { return ___force_7; }
	inline float* get_address_of_force_7() { return &___force_7; }
	inline void set_force_7(float value)
	{
		___force_7 = value;
	}

	inline static int32_t get_offset_of_m_Cols_8() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t9F0D1071F45FDA99D798A1C46886A26A592F8408, ___m_Cols_8)); }
	inline ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* get_m_Cols_8() const { return ___m_Cols_8; }
	inline ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252** get_address_of_m_Cols_8() { return &___m_Cols_8; }
	inline void set_m_Cols_8(ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* value)
	{
		___m_Cols_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cols_8), value);
	}

	inline static int32_t get_offset_of_m_Sphere_9() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t9F0D1071F45FDA99D798A1C46886A26A592F8408, ___m_Sphere_9)); }
	inline SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * get_m_Sphere_9() const { return ___m_Sphere_9; }
	inline SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F ** get_address_of_m_Sphere_9() { return &___m_Sphere_9; }
	inline void set_m_Sphere_9(SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * value)
	{
		___m_Sphere_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sphere_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AFTERBURNERPHYSICSFORCE_T9F0D1071F45FDA99D798A1C46886A26A592F8408_H
#ifndef EXPLOSIONFIREANDDEBRIS_TE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA_H
#define EXPLOSIONFIREANDDEBRIS_TE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ExplosionFireAndDebris
struct  ExplosionFireAndDebris_tE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform[] UnityStandardAssets.Effects.ExplosionFireAndDebris::debrisPrefabs
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* ___debrisPrefabs_4;
	// UnityEngine.Transform UnityStandardAssets.Effects.ExplosionFireAndDebris::firePrefab
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___firePrefab_5;
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris::numDebrisPieces
	int32_t ___numDebrisPieces_6;
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris::numFires
	int32_t ___numFires_7;

public:
	inline static int32_t get_offset_of_debrisPrefabs_4() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_tE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA, ___debrisPrefabs_4)); }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* get_debrisPrefabs_4() const { return ___debrisPrefabs_4; }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804** get_address_of_debrisPrefabs_4() { return &___debrisPrefabs_4; }
	inline void set_debrisPrefabs_4(TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* value)
	{
		___debrisPrefabs_4 = value;
		Il2CppCodeGenWriteBarrier((&___debrisPrefabs_4), value);
	}

	inline static int32_t get_offset_of_firePrefab_5() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_tE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA, ___firePrefab_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_firePrefab_5() const { return ___firePrefab_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_firePrefab_5() { return &___firePrefab_5; }
	inline void set_firePrefab_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___firePrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___firePrefab_5), value);
	}

	inline static int32_t get_offset_of_numDebrisPieces_6() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_tE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA, ___numDebrisPieces_6)); }
	inline int32_t get_numDebrisPieces_6() const { return ___numDebrisPieces_6; }
	inline int32_t* get_address_of_numDebrisPieces_6() { return &___numDebrisPieces_6; }
	inline void set_numDebrisPieces_6(int32_t value)
	{
		___numDebrisPieces_6 = value;
	}

	inline static int32_t get_offset_of_numFires_7() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_tE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA, ___numFires_7)); }
	inline int32_t get_numFires_7() const { return ___numFires_7; }
	inline int32_t* get_address_of_numFires_7() { return &___numFires_7; }
	inline void set_numFires_7(int32_t value)
	{
		___numFires_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPLOSIONFIREANDDEBRIS_TE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA_H
#ifndef EXPLOSIONPHYSICSFORCE_T2BC67AEA4386688129F43B43EB9927BE57A384A8_H
#define EXPLOSIONPHYSICSFORCE_T2BC67AEA4386688129F43B43EB9927BE57A384A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ExplosionPhysicsForce
struct  ExplosionPhysicsForce_t2BC67AEA4386688129F43B43EB9927BE57A384A8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityStandardAssets.Effects.ExplosionPhysicsForce::explosionForce
	float ___explosionForce_4;

public:
	inline static int32_t get_offset_of_explosionForce_4() { return static_cast<int32_t>(offsetof(ExplosionPhysicsForce_t2BC67AEA4386688129F43B43EB9927BE57A384A8, ___explosionForce_4)); }
	inline float get_explosionForce_4() const { return ___explosionForce_4; }
	inline float* get_address_of_explosionForce_4() { return &___explosionForce_4; }
	inline void set_explosionForce_4(float value)
	{
		___explosionForce_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPLOSIONPHYSICSFORCE_T2BC67AEA4386688129F43B43EB9927BE57A384A8_H
#ifndef EXPLOSIVE_TDE008C99033356E9A6400EBF52C9F532AC681E40_H
#define EXPLOSIVE_TDE008C99033356E9A6400EBF52C9F532AC681E40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.Explosive
struct  Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform UnityStandardAssets.Effects.Explosive::explosionPrefab
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___explosionPrefab_4;
	// System.Single UnityStandardAssets.Effects.Explosive::detonationImpactVelocity
	float ___detonationImpactVelocity_5;
	// System.Single UnityStandardAssets.Effects.Explosive::sizeMultiplier
	float ___sizeMultiplier_6;
	// System.Boolean UnityStandardAssets.Effects.Explosive::reset
	bool ___reset_7;
	// System.Single UnityStandardAssets.Effects.Explosive::resetTimeDelay
	float ___resetTimeDelay_8;
	// System.Boolean UnityStandardAssets.Effects.Explosive::m_Exploded
	bool ___m_Exploded_9;
	// UnityStandardAssets.Utility.ObjectResetter UnityStandardAssets.Effects.Explosive::m_ObjectResetter
	ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9 * ___m_ObjectResetter_10;

public:
	inline static int32_t get_offset_of_explosionPrefab_4() { return static_cast<int32_t>(offsetof(Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40, ___explosionPrefab_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_explosionPrefab_4() const { return ___explosionPrefab_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_explosionPrefab_4() { return &___explosionPrefab_4; }
	inline void set_explosionPrefab_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___explosionPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___explosionPrefab_4), value);
	}

	inline static int32_t get_offset_of_detonationImpactVelocity_5() { return static_cast<int32_t>(offsetof(Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40, ___detonationImpactVelocity_5)); }
	inline float get_detonationImpactVelocity_5() const { return ___detonationImpactVelocity_5; }
	inline float* get_address_of_detonationImpactVelocity_5() { return &___detonationImpactVelocity_5; }
	inline void set_detonationImpactVelocity_5(float value)
	{
		___detonationImpactVelocity_5 = value;
	}

	inline static int32_t get_offset_of_sizeMultiplier_6() { return static_cast<int32_t>(offsetof(Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40, ___sizeMultiplier_6)); }
	inline float get_sizeMultiplier_6() const { return ___sizeMultiplier_6; }
	inline float* get_address_of_sizeMultiplier_6() { return &___sizeMultiplier_6; }
	inline void set_sizeMultiplier_6(float value)
	{
		___sizeMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_reset_7() { return static_cast<int32_t>(offsetof(Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40, ___reset_7)); }
	inline bool get_reset_7() const { return ___reset_7; }
	inline bool* get_address_of_reset_7() { return &___reset_7; }
	inline void set_reset_7(bool value)
	{
		___reset_7 = value;
	}

	inline static int32_t get_offset_of_resetTimeDelay_8() { return static_cast<int32_t>(offsetof(Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40, ___resetTimeDelay_8)); }
	inline float get_resetTimeDelay_8() const { return ___resetTimeDelay_8; }
	inline float* get_address_of_resetTimeDelay_8() { return &___resetTimeDelay_8; }
	inline void set_resetTimeDelay_8(float value)
	{
		___resetTimeDelay_8 = value;
	}

	inline static int32_t get_offset_of_m_Exploded_9() { return static_cast<int32_t>(offsetof(Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40, ___m_Exploded_9)); }
	inline bool get_m_Exploded_9() const { return ___m_Exploded_9; }
	inline bool* get_address_of_m_Exploded_9() { return &___m_Exploded_9; }
	inline void set_m_Exploded_9(bool value)
	{
		___m_Exploded_9 = value;
	}

	inline static int32_t get_offset_of_m_ObjectResetter_10() { return static_cast<int32_t>(offsetof(Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40, ___m_ObjectResetter_10)); }
	inline ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9 * get_m_ObjectResetter_10() const { return ___m_ObjectResetter_10; }
	inline ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9 ** get_address_of_m_ObjectResetter_10() { return &___m_ObjectResetter_10; }
	inline void set_m_ObjectResetter_10(ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9 * value)
	{
		___m_ObjectResetter_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectResetter_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPLOSIVE_TDE008C99033356E9A6400EBF52C9F532AC681E40_H
#ifndef EXTINGUISHABLEPARTICLESYSTEM_TDF266922C9D4176B2247949390DFDB17BA233130_H
#define EXTINGUISHABLEPARTICLESYSTEM_TDF266922C9D4176B2247949390DFDB17BA233130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ExtinguishableParticleSystem
struct  ExtinguishableParticleSystem_tDF266922C9D4176B2247949390DFDB17BA233130  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityStandardAssets.Effects.ExtinguishableParticleSystem::multiplier
	float ___multiplier_4;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Effects.ExtinguishableParticleSystem::m_Systems
	ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* ___m_Systems_5;

public:
	inline static int32_t get_offset_of_multiplier_4() { return static_cast<int32_t>(offsetof(ExtinguishableParticleSystem_tDF266922C9D4176B2247949390DFDB17BA233130, ___multiplier_4)); }
	inline float get_multiplier_4() const { return ___multiplier_4; }
	inline float* get_address_of_multiplier_4() { return &___multiplier_4; }
	inline void set_multiplier_4(float value)
	{
		___multiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_Systems_5() { return static_cast<int32_t>(offsetof(ExtinguishableParticleSystem_tDF266922C9D4176B2247949390DFDB17BA233130, ___m_Systems_5)); }
	inline ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* get_m_Systems_5() const { return ___m_Systems_5; }
	inline ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9** get_address_of_m_Systems_5() { return &___m_Systems_5; }
	inline void set_m_Systems_5(ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* value)
	{
		___m_Systems_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Systems_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTINGUISHABLEPARTICLESYSTEM_TDF266922C9D4176B2247949390DFDB17BA233130_H
#ifndef FIRELIGHT_T4C0EE4FFBB8F12021C447A53FA704029761639C0_H
#define FIRELIGHT_T4C0EE4FFBB8F12021C447A53FA704029761639C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.FireLight
struct  FireLight_t4C0EE4FFBB8F12021C447A53FA704029761639C0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityStandardAssets.Effects.FireLight::m_Rnd
	float ___m_Rnd_4;
	// System.Boolean UnityStandardAssets.Effects.FireLight::m_Burning
	bool ___m_Burning_5;
	// UnityEngine.Light UnityStandardAssets.Effects.FireLight::m_Light
	Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * ___m_Light_6;

public:
	inline static int32_t get_offset_of_m_Rnd_4() { return static_cast<int32_t>(offsetof(FireLight_t4C0EE4FFBB8F12021C447A53FA704029761639C0, ___m_Rnd_4)); }
	inline float get_m_Rnd_4() const { return ___m_Rnd_4; }
	inline float* get_address_of_m_Rnd_4() { return &___m_Rnd_4; }
	inline void set_m_Rnd_4(float value)
	{
		___m_Rnd_4 = value;
	}

	inline static int32_t get_offset_of_m_Burning_5() { return static_cast<int32_t>(offsetof(FireLight_t4C0EE4FFBB8F12021C447A53FA704029761639C0, ___m_Burning_5)); }
	inline bool get_m_Burning_5() const { return ___m_Burning_5; }
	inline bool* get_address_of_m_Burning_5() { return &___m_Burning_5; }
	inline void set_m_Burning_5(bool value)
	{
		___m_Burning_5 = value;
	}

	inline static int32_t get_offset_of_m_Light_6() { return static_cast<int32_t>(offsetof(FireLight_t4C0EE4FFBB8F12021C447A53FA704029761639C0, ___m_Light_6)); }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * get_m_Light_6() const { return ___m_Light_6; }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C ** get_address_of_m_Light_6() { return &___m_Light_6; }
	inline void set_m_Light_6(Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * value)
	{
		___m_Light_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Light_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIRELIGHT_T4C0EE4FFBB8F12021C447A53FA704029761639C0_H
#ifndef HOSE_T27B33777B005A1CE8639E4CD14A7B33AB5966CB7_H
#define HOSE_T27B33777B005A1CE8639E4CD14A7B33AB5966CB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.Hose
struct  Hose_t27B33777B005A1CE8639E4CD14A7B33AB5966CB7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityStandardAssets.Effects.Hose::maxPower
	float ___maxPower_4;
	// System.Single UnityStandardAssets.Effects.Hose::minPower
	float ___minPower_5;
	// System.Single UnityStandardAssets.Effects.Hose::changeSpeed
	float ___changeSpeed_6;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Effects.Hose::hoseWaterSystems
	ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* ___hoseWaterSystems_7;
	// UnityEngine.Renderer UnityStandardAssets.Effects.Hose::systemRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___systemRenderer_8;
	// System.Single UnityStandardAssets.Effects.Hose::m_Power
	float ___m_Power_9;

public:
	inline static int32_t get_offset_of_maxPower_4() { return static_cast<int32_t>(offsetof(Hose_t27B33777B005A1CE8639E4CD14A7B33AB5966CB7, ___maxPower_4)); }
	inline float get_maxPower_4() const { return ___maxPower_4; }
	inline float* get_address_of_maxPower_4() { return &___maxPower_4; }
	inline void set_maxPower_4(float value)
	{
		___maxPower_4 = value;
	}

	inline static int32_t get_offset_of_minPower_5() { return static_cast<int32_t>(offsetof(Hose_t27B33777B005A1CE8639E4CD14A7B33AB5966CB7, ___minPower_5)); }
	inline float get_minPower_5() const { return ___minPower_5; }
	inline float* get_address_of_minPower_5() { return &___minPower_5; }
	inline void set_minPower_5(float value)
	{
		___minPower_5 = value;
	}

	inline static int32_t get_offset_of_changeSpeed_6() { return static_cast<int32_t>(offsetof(Hose_t27B33777B005A1CE8639E4CD14A7B33AB5966CB7, ___changeSpeed_6)); }
	inline float get_changeSpeed_6() const { return ___changeSpeed_6; }
	inline float* get_address_of_changeSpeed_6() { return &___changeSpeed_6; }
	inline void set_changeSpeed_6(float value)
	{
		___changeSpeed_6 = value;
	}

	inline static int32_t get_offset_of_hoseWaterSystems_7() { return static_cast<int32_t>(offsetof(Hose_t27B33777B005A1CE8639E4CD14A7B33AB5966CB7, ___hoseWaterSystems_7)); }
	inline ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* get_hoseWaterSystems_7() const { return ___hoseWaterSystems_7; }
	inline ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9** get_address_of_hoseWaterSystems_7() { return &___hoseWaterSystems_7; }
	inline void set_hoseWaterSystems_7(ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* value)
	{
		___hoseWaterSystems_7 = value;
		Il2CppCodeGenWriteBarrier((&___hoseWaterSystems_7), value);
	}

	inline static int32_t get_offset_of_systemRenderer_8() { return static_cast<int32_t>(offsetof(Hose_t27B33777B005A1CE8639E4CD14A7B33AB5966CB7, ___systemRenderer_8)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_systemRenderer_8() const { return ___systemRenderer_8; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_systemRenderer_8() { return &___systemRenderer_8; }
	inline void set_systemRenderer_8(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___systemRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___systemRenderer_8), value);
	}

	inline static int32_t get_offset_of_m_Power_9() { return static_cast<int32_t>(offsetof(Hose_t27B33777B005A1CE8639E4CD14A7B33AB5966CB7, ___m_Power_9)); }
	inline float get_m_Power_9() const { return ___m_Power_9; }
	inline float* get_address_of_m_Power_9() { return &___m_Power_9; }
	inline void set_m_Power_9(float value)
	{
		___m_Power_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOSE_T27B33777B005A1CE8639E4CD14A7B33AB5966CB7_H
#ifndef PARTICLESYSTEMMULTIPLIER_T4B02B754B0A06C562C5B1B52DCD68D1BAEDCD5CE_H
#define PARTICLESYSTEMMULTIPLIER_T4B02B754B0A06C562C5B1B52DCD68D1BAEDCD5CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ParticleSystemMultiplier
struct  ParticleSystemMultiplier_t4B02B754B0A06C562C5B1B52DCD68D1BAEDCD5CE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityStandardAssets.Effects.ParticleSystemMultiplier::multiplier
	float ___multiplier_4;

public:
	inline static int32_t get_offset_of_multiplier_4() { return static_cast<int32_t>(offsetof(ParticleSystemMultiplier_t4B02B754B0A06C562C5B1B52DCD68D1BAEDCD5CE, ___multiplier_4)); }
	inline float get_multiplier_4() const { return ___multiplier_4; }
	inline float* get_address_of_multiplier_4() { return &___multiplier_4; }
	inline void set_multiplier_4(float value)
	{
		___multiplier_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMMULTIPLIER_T4B02B754B0A06C562C5B1B52DCD68D1BAEDCD5CE_H
#ifndef SMOKEPARTICLES_T8AF64C59BD42E84E783834AFDBDD84A262FCB28F_H
#define SMOKEPARTICLES_T8AF64C59BD42E84E783834AFDBDD84A262FCB28F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.SmokeParticles
struct  SmokeParticles_t8AF64C59BD42E84E783834AFDBDD84A262FCB28F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioClip[] UnityStandardAssets.Effects.SmokeParticles::extinguishSounds
	AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* ___extinguishSounds_4;

public:
	inline static int32_t get_offset_of_extinguishSounds_4() { return static_cast<int32_t>(offsetof(SmokeParticles_t8AF64C59BD42E84E783834AFDBDD84A262FCB28F, ___extinguishSounds_4)); }
	inline AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* get_extinguishSounds_4() const { return ___extinguishSounds_4; }
	inline AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2** get_address_of_extinguishSounds_4() { return &___extinguishSounds_4; }
	inline void set_extinguishSounds_4(AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* value)
	{
		___extinguishSounds_4 = value;
		Il2CppCodeGenWriteBarrier((&___extinguishSounds_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOKEPARTICLES_T8AF64C59BD42E84E783834AFDBDD84A262FCB28F_H
#ifndef WATERHOSEPARTICLES_TD058F19E51943F4EFA58450B7E3F7FCEB1902179_H
#define WATERHOSEPARTICLES_TD058F19E51943F4EFA58450B7E3F7FCEB1902179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.WaterHoseParticles
struct  WaterHoseParticles_tD058F19E51943F4EFA58450B7E3F7FCEB1902179  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityStandardAssets.Effects.WaterHoseParticles::force
	float ___force_5;
	// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent> UnityStandardAssets.Effects.WaterHoseParticles::m_CollisionEvents
	List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * ___m_CollisionEvents_6;
	// UnityEngine.ParticleSystem UnityStandardAssets.Effects.WaterHoseParticles::m_ParticleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___m_ParticleSystem_7;

public:
	inline static int32_t get_offset_of_force_5() { return static_cast<int32_t>(offsetof(WaterHoseParticles_tD058F19E51943F4EFA58450B7E3F7FCEB1902179, ___force_5)); }
	inline float get_force_5() const { return ___force_5; }
	inline float* get_address_of_force_5() { return &___force_5; }
	inline void set_force_5(float value)
	{
		___force_5 = value;
	}

	inline static int32_t get_offset_of_m_CollisionEvents_6() { return static_cast<int32_t>(offsetof(WaterHoseParticles_tD058F19E51943F4EFA58450B7E3F7FCEB1902179, ___m_CollisionEvents_6)); }
	inline List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * get_m_CollisionEvents_6() const { return ___m_CollisionEvents_6; }
	inline List_1_t2762C811E470D336E31761384C6E5382164DA4C7 ** get_address_of_m_CollisionEvents_6() { return &___m_CollisionEvents_6; }
	inline void set_m_CollisionEvents_6(List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * value)
	{
		___m_CollisionEvents_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CollisionEvents_6), value);
	}

	inline static int32_t get_offset_of_m_ParticleSystem_7() { return static_cast<int32_t>(offsetof(WaterHoseParticles_tD058F19E51943F4EFA58450B7E3F7FCEB1902179, ___m_ParticleSystem_7)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_m_ParticleSystem_7() const { return ___m_ParticleSystem_7; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_m_ParticleSystem_7() { return &___m_ParticleSystem_7; }
	inline void set_m_ParticleSystem_7(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___m_ParticleSystem_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_7), value);
	}
};

struct WaterHoseParticles_tD058F19E51943F4EFA58450B7E3F7FCEB1902179_StaticFields
{
public:
	// System.Single UnityStandardAssets.Effects.WaterHoseParticles::lastSoundTime
	float ___lastSoundTime_4;

public:
	inline static int32_t get_offset_of_lastSoundTime_4() { return static_cast<int32_t>(offsetof(WaterHoseParticles_tD058F19E51943F4EFA58450B7E3F7FCEB1902179_StaticFields, ___lastSoundTime_4)); }
	inline float get_lastSoundTime_4() const { return ___lastSoundTime_4; }
	inline float* get_address_of_lastSoundTime_4() { return &___lastSoundTime_4; }
	inline void set_lastSoundTime_4(float value)
	{
		___lastSoundTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERHOSEPARTICLES_TD058F19E51943F4EFA58450B7E3F7FCEB1902179_H
#ifndef TIMEDOBJECTDESTRUCTOR_T8783E91456633F1C86444708432B962621D06B5D_H
#define TIMEDOBJECTDESTRUCTOR_T8783E91456633F1C86444708432B962621D06B5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectDestructor
struct  TimedObjectDestructor_t8783E91456633F1C86444708432B962621D06B5D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityStandardAssets.Utility.TimedObjectDestructor::m_TimeOut
	float ___m_TimeOut_4;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectDestructor::m_DetachChildren
	bool ___m_DetachChildren_5;

public:
	inline static int32_t get_offset_of_m_TimeOut_4() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t8783E91456633F1C86444708432B962621D06B5D, ___m_TimeOut_4)); }
	inline float get_m_TimeOut_4() const { return ___m_TimeOut_4; }
	inline float* get_address_of_m_TimeOut_4() { return &___m_TimeOut_4; }
	inline void set_m_TimeOut_4(float value)
	{
		___m_TimeOut_4 = value;
	}

	inline static int32_t get_offset_of_m_DetachChildren_5() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t8783E91456633F1C86444708432B962621D06B5D, ___m_DetachChildren_5)); }
	inline bool get_m_DetachChildren_5() const { return ___m_DetachChildren_5; }
	inline bool* get_address_of_m_DetachChildren_5() { return &___m_DetachChildren_5; }
	inline void set_m_DetachChildren_5(bool value)
	{
		___m_DetachChildren_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTDESTRUCTOR_T8783E91456633F1C86444708432B962621D06B5D_H
#ifndef WAYPOINTCIRCUIT_TE92057604A54283CA3531B3F8A316D7F520FC8FE_H
#define WAYPOINTCIRCUIT_TE92057604A54283CA3531B3F8A316D7F520FC8FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit
struct  WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit/WaypointList UnityStandardAssets.Utility.WaypointCircuit::waypointList
	WaypointList_t28075B7540AC2B9E2507C0E6175C6D5B31F7364D * ___waypointList_4;
	// System.Boolean UnityStandardAssets.Utility.WaypointCircuit::smoothRoute
	bool ___smoothRoute_5;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::numPoints
	int32_t ___numPoints_6;
	// UnityEngine.Vector3[] UnityStandardAssets.Utility.WaypointCircuit::points
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___points_7;
	// System.Single[] UnityStandardAssets.Utility.WaypointCircuit::distances
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___distances_8;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::editorVisualisationSubsteps
	float ___editorVisualisationSubsteps_9;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::<Length>k__BackingField
	float ___U3CLengthU3Ek__BackingField_10;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p0n
	int32_t ___p0n_11;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p1n
	int32_t ___p1n_12;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p2n
	int32_t ___p2n_13;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p3n
	int32_t ___p3n_14;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::i
	float ___i_15;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P0
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___P0_16;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___P1_17;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___P2_18;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___P3_19;

public:
	inline static int32_t get_offset_of_waypointList_4() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___waypointList_4)); }
	inline WaypointList_t28075B7540AC2B9E2507C0E6175C6D5B31F7364D * get_waypointList_4() const { return ___waypointList_4; }
	inline WaypointList_t28075B7540AC2B9E2507C0E6175C6D5B31F7364D ** get_address_of_waypointList_4() { return &___waypointList_4; }
	inline void set_waypointList_4(WaypointList_t28075B7540AC2B9E2507C0E6175C6D5B31F7364D * value)
	{
		___waypointList_4 = value;
		Il2CppCodeGenWriteBarrier((&___waypointList_4), value);
	}

	inline static int32_t get_offset_of_smoothRoute_5() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___smoothRoute_5)); }
	inline bool get_smoothRoute_5() const { return ___smoothRoute_5; }
	inline bool* get_address_of_smoothRoute_5() { return &___smoothRoute_5; }
	inline void set_smoothRoute_5(bool value)
	{
		___smoothRoute_5 = value;
	}

	inline static int32_t get_offset_of_numPoints_6() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___numPoints_6)); }
	inline int32_t get_numPoints_6() const { return ___numPoints_6; }
	inline int32_t* get_address_of_numPoints_6() { return &___numPoints_6; }
	inline void set_numPoints_6(int32_t value)
	{
		___numPoints_6 = value;
	}

	inline static int32_t get_offset_of_points_7() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___points_7)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_points_7() const { return ___points_7; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_points_7() { return &___points_7; }
	inline void set_points_7(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___points_7 = value;
		Il2CppCodeGenWriteBarrier((&___points_7), value);
	}

	inline static int32_t get_offset_of_distances_8() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___distances_8)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_distances_8() const { return ___distances_8; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_distances_8() { return &___distances_8; }
	inline void set_distances_8(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___distances_8 = value;
		Il2CppCodeGenWriteBarrier((&___distances_8), value);
	}

	inline static int32_t get_offset_of_editorVisualisationSubsteps_9() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___editorVisualisationSubsteps_9)); }
	inline float get_editorVisualisationSubsteps_9() const { return ___editorVisualisationSubsteps_9; }
	inline float* get_address_of_editorVisualisationSubsteps_9() { return &___editorVisualisationSubsteps_9; }
	inline void set_editorVisualisationSubsteps_9(float value)
	{
		___editorVisualisationSubsteps_9 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___U3CLengthU3Ek__BackingField_10)); }
	inline float get_U3CLengthU3Ek__BackingField_10() const { return ___U3CLengthU3Ek__BackingField_10; }
	inline float* get_address_of_U3CLengthU3Ek__BackingField_10() { return &___U3CLengthU3Ek__BackingField_10; }
	inline void set_U3CLengthU3Ek__BackingField_10(float value)
	{
		___U3CLengthU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_p0n_11() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___p0n_11)); }
	inline int32_t get_p0n_11() const { return ___p0n_11; }
	inline int32_t* get_address_of_p0n_11() { return &___p0n_11; }
	inline void set_p0n_11(int32_t value)
	{
		___p0n_11 = value;
	}

	inline static int32_t get_offset_of_p1n_12() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___p1n_12)); }
	inline int32_t get_p1n_12() const { return ___p1n_12; }
	inline int32_t* get_address_of_p1n_12() { return &___p1n_12; }
	inline void set_p1n_12(int32_t value)
	{
		___p1n_12 = value;
	}

	inline static int32_t get_offset_of_p2n_13() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___p2n_13)); }
	inline int32_t get_p2n_13() const { return ___p2n_13; }
	inline int32_t* get_address_of_p2n_13() { return &___p2n_13; }
	inline void set_p2n_13(int32_t value)
	{
		___p2n_13 = value;
	}

	inline static int32_t get_offset_of_p3n_14() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___p3n_14)); }
	inline int32_t get_p3n_14() const { return ___p3n_14; }
	inline int32_t* get_address_of_p3n_14() { return &___p3n_14; }
	inline void set_p3n_14(int32_t value)
	{
		___p3n_14 = value;
	}

	inline static int32_t get_offset_of_i_15() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___i_15)); }
	inline float get_i_15() const { return ___i_15; }
	inline float* get_address_of_i_15() { return &___i_15; }
	inline void set_i_15(float value)
	{
		___i_15 = value;
	}

	inline static int32_t get_offset_of_P0_16() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___P0_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_P0_16() const { return ___P0_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_P0_16() { return &___P0_16; }
	inline void set_P0_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___P0_16 = value;
	}

	inline static int32_t get_offset_of_P1_17() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___P1_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_P1_17() const { return ___P1_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_P1_17() { return &___P1_17; }
	inline void set_P1_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___P1_17 = value;
	}

	inline static int32_t get_offset_of_P2_18() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___P2_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_P2_18() const { return ___P2_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_P2_18() { return &___P2_18; }
	inline void set_P2_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___P2_18 = value;
	}

	inline static int32_t get_offset_of_P3_19() { return static_cast<int32_t>(offsetof(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE, ___P3_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_P3_19() const { return ___P3_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_P3_19() { return &___P3_19; }
	inline void set_P3_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___P3_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTCIRCUIT_TE92057604A54283CA3531B3F8A316D7F520FC8FE_H
#ifndef WAYPOINTPROGRESSTRACKER_TDDB26AC32CC6ECD02D2301A148750F07695C1AF9_H
#define WAYPOINTPROGRESSTRACKER_TDDB26AC32CC6ECD02D2301A148750F07695C1AF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointProgressTracker
struct  WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit UnityStandardAssets.Utility.WaypointProgressTracker::circuit
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE * ___circuit_4;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForTargetOffset
	float ___lookAheadForTargetOffset_5;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForTargetFactor
	float ___lookAheadForTargetFactor_6;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForSpeedOffset
	float ___lookAheadForSpeedOffset_7;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForSpeedFactor
	float ___lookAheadForSpeedFactor_8;
	// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle UnityStandardAssets.Utility.WaypointProgressTracker::progressStyle
	int32_t ___progressStyle_9;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::pointToPointThreshold
	float ___pointToPointThreshold_10;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<targetPoint>k__BackingField
	RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16  ___U3CtargetPointU3Ek__BackingField_11;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<speedPoint>k__BackingField
	RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16  ___U3CspeedPointU3Ek__BackingField_12;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<progressPoint>k__BackingField
	RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16  ___U3CprogressPointU3Ek__BackingField_13;
	// UnityEngine.Transform UnityStandardAssets.Utility.WaypointProgressTracker::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_14;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::progressDistance
	float ___progressDistance_15;
	// System.Int32 UnityStandardAssets.Utility.WaypointProgressTracker::progressNum
	int32_t ___progressNum_16;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointProgressTracker::lastPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___lastPosition_17;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::speed
	float ___speed_18;

public:
	inline static int32_t get_offset_of_circuit_4() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9, ___circuit_4)); }
	inline WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE * get_circuit_4() const { return ___circuit_4; }
	inline WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE ** get_address_of_circuit_4() { return &___circuit_4; }
	inline void set_circuit_4(WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE * value)
	{
		___circuit_4 = value;
		Il2CppCodeGenWriteBarrier((&___circuit_4), value);
	}

	inline static int32_t get_offset_of_lookAheadForTargetOffset_5() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9, ___lookAheadForTargetOffset_5)); }
	inline float get_lookAheadForTargetOffset_5() const { return ___lookAheadForTargetOffset_5; }
	inline float* get_address_of_lookAheadForTargetOffset_5() { return &___lookAheadForTargetOffset_5; }
	inline void set_lookAheadForTargetOffset_5(float value)
	{
		___lookAheadForTargetOffset_5 = value;
	}

	inline static int32_t get_offset_of_lookAheadForTargetFactor_6() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9, ___lookAheadForTargetFactor_6)); }
	inline float get_lookAheadForTargetFactor_6() const { return ___lookAheadForTargetFactor_6; }
	inline float* get_address_of_lookAheadForTargetFactor_6() { return &___lookAheadForTargetFactor_6; }
	inline void set_lookAheadForTargetFactor_6(float value)
	{
		___lookAheadForTargetFactor_6 = value;
	}

	inline static int32_t get_offset_of_lookAheadForSpeedOffset_7() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9, ___lookAheadForSpeedOffset_7)); }
	inline float get_lookAheadForSpeedOffset_7() const { return ___lookAheadForSpeedOffset_7; }
	inline float* get_address_of_lookAheadForSpeedOffset_7() { return &___lookAheadForSpeedOffset_7; }
	inline void set_lookAheadForSpeedOffset_7(float value)
	{
		___lookAheadForSpeedOffset_7 = value;
	}

	inline static int32_t get_offset_of_lookAheadForSpeedFactor_8() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9, ___lookAheadForSpeedFactor_8)); }
	inline float get_lookAheadForSpeedFactor_8() const { return ___lookAheadForSpeedFactor_8; }
	inline float* get_address_of_lookAheadForSpeedFactor_8() { return &___lookAheadForSpeedFactor_8; }
	inline void set_lookAheadForSpeedFactor_8(float value)
	{
		___lookAheadForSpeedFactor_8 = value;
	}

	inline static int32_t get_offset_of_progressStyle_9() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9, ___progressStyle_9)); }
	inline int32_t get_progressStyle_9() const { return ___progressStyle_9; }
	inline int32_t* get_address_of_progressStyle_9() { return &___progressStyle_9; }
	inline void set_progressStyle_9(int32_t value)
	{
		___progressStyle_9 = value;
	}

	inline static int32_t get_offset_of_pointToPointThreshold_10() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9, ___pointToPointThreshold_10)); }
	inline float get_pointToPointThreshold_10() const { return ___pointToPointThreshold_10; }
	inline float* get_address_of_pointToPointThreshold_10() { return &___pointToPointThreshold_10; }
	inline void set_pointToPointThreshold_10(float value)
	{
		___pointToPointThreshold_10 = value;
	}

	inline static int32_t get_offset_of_U3CtargetPointU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9, ___U3CtargetPointU3Ek__BackingField_11)); }
	inline RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16  get_U3CtargetPointU3Ek__BackingField_11() const { return ___U3CtargetPointU3Ek__BackingField_11; }
	inline RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16 * get_address_of_U3CtargetPointU3Ek__BackingField_11() { return &___U3CtargetPointU3Ek__BackingField_11; }
	inline void set_U3CtargetPointU3Ek__BackingField_11(RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16  value)
	{
		___U3CtargetPointU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CspeedPointU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9, ___U3CspeedPointU3Ek__BackingField_12)); }
	inline RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16  get_U3CspeedPointU3Ek__BackingField_12() const { return ___U3CspeedPointU3Ek__BackingField_12; }
	inline RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16 * get_address_of_U3CspeedPointU3Ek__BackingField_12() { return &___U3CspeedPointU3Ek__BackingField_12; }
	inline void set_U3CspeedPointU3Ek__BackingField_12(RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16  value)
	{
		___U3CspeedPointU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CprogressPointU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9, ___U3CprogressPointU3Ek__BackingField_13)); }
	inline RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16  get_U3CprogressPointU3Ek__BackingField_13() const { return ___U3CprogressPointU3Ek__BackingField_13; }
	inline RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16 * get_address_of_U3CprogressPointU3Ek__BackingField_13() { return &___U3CprogressPointU3Ek__BackingField_13; }
	inline void set_U3CprogressPointU3Ek__BackingField_13(RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16  value)
	{
		___U3CprogressPointU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_target_14() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9, ___target_14)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_14() const { return ___target_14; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_14() { return &___target_14; }
	inline void set_target_14(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_14 = value;
		Il2CppCodeGenWriteBarrier((&___target_14), value);
	}

	inline static int32_t get_offset_of_progressDistance_15() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9, ___progressDistance_15)); }
	inline float get_progressDistance_15() const { return ___progressDistance_15; }
	inline float* get_address_of_progressDistance_15() { return &___progressDistance_15; }
	inline void set_progressDistance_15(float value)
	{
		___progressDistance_15 = value;
	}

	inline static int32_t get_offset_of_progressNum_16() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9, ___progressNum_16)); }
	inline int32_t get_progressNum_16() const { return ___progressNum_16; }
	inline int32_t* get_address_of_progressNum_16() { return &___progressNum_16; }
	inline void set_progressNum_16(int32_t value)
	{
		___progressNum_16 = value;
	}

	inline static int32_t get_offset_of_lastPosition_17() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9, ___lastPosition_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_lastPosition_17() const { return ___lastPosition_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_lastPosition_17() { return &___lastPosition_17; }
	inline void set_lastPosition_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___lastPosition_17 = value;
	}

	inline static int32_t get_offset_of_speed_18() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9, ___speed_18)); }
	inline float get_speed_18() const { return ___speed_18; }
	inline float* get_address_of_speed_18() { return &___speed_18; }
	inline void set_speed_18(float value)
	{
		___speed_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTPROGRESSTRACKER_TDDB26AC32CC6ECD02D2301A148750F07695C1AF9_H
#ifndef CAMERAINPUTSCHEME_TEB16782DA7575673162607368A520FB02D2739E5_H
#define CAMERAINPUTSCHEME_TEB16782DA7575673162607368A520FB02D2739E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.CameraInputScheme
struct  CameraInputScheme_tEB16782DA7575673162607368A520FB02D2739E5  : public InputScheme_t78F140C9CD80326F0D459CE65156EF66E3218678
{
public:
	// Core.Camera.CameraRig Core.Input.CameraInputScheme::cameraRig
	CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4 * ___cameraRig_4;
	// System.Single Core.Input.CameraInputScheme::nearZoomPanSpeedModifier
	float ___nearZoomPanSpeedModifier_5;

public:
	inline static int32_t get_offset_of_cameraRig_4() { return static_cast<int32_t>(offsetof(CameraInputScheme_tEB16782DA7575673162607368A520FB02D2739E5, ___cameraRig_4)); }
	inline CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4 * get_cameraRig_4() const { return ___cameraRig_4; }
	inline CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4 ** get_address_of_cameraRig_4() { return &___cameraRig_4; }
	inline void set_cameraRig_4(CameraRig_t283698A4EB169762A3342D3E48419021467FB3D4 * value)
	{
		___cameraRig_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRig_4), value);
	}

	inline static int32_t get_offset_of_nearZoomPanSpeedModifier_5() { return static_cast<int32_t>(offsetof(CameraInputScheme_tEB16782DA7575673162607368A520FB02D2739E5, ___nearZoomPanSpeedModifier_5)); }
	inline float get_nearZoomPanSpeedModifier_5() const { return ___nearZoomPanSpeedModifier_5; }
	inline float* get_address_of_nearZoomPanSpeedModifier_5() { return &___nearZoomPanSpeedModifier_5; }
	inline void set_nearZoomPanSpeedModifier_5(float value)
	{
		___nearZoomPanSpeedModifier_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAINPUTSCHEME_TEB16782DA7575673162607368A520FB02D2739E5_H
#ifndef INPUTCONTROLLER_T7EED1B6916BB59A90A8974D508B8480EA0B12B45_H
#define INPUTCONTROLLER_T7EED1B6916BB59A90A8974D508B8480EA0B12B45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.InputController
struct  InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45  : public Singleton_1_tD9F825EC34DD92F207C776721B0BB94C86D58536
{
public:
	// System.Single Core.Input.InputController::dragThresholdTouch
	float ___dragThresholdTouch_6;
	// System.Single Core.Input.InputController::dragThresholdMouse
	float ___dragThresholdMouse_7;
	// System.Single Core.Input.InputController::tapTime
	float ___tapTime_8;
	// System.Single Core.Input.InputController::holdTime
	float ___holdTime_9;
	// System.Single Core.Input.InputController::mouseWheelSensitivity
	float ___mouseWheelSensitivity_10;
	// System.Int32 Core.Input.InputController::trackMouseButtons
	int32_t ___trackMouseButtons_11;
	// System.Single Core.Input.InputController::flickThreshold
	float ___flickThreshold_12;
	// System.Collections.Generic.List`1<Core.Input.TouchInfo> Core.Input.InputController::m_Touches
	List_1_tB2F89C3237A8F9BC195D8869C618C1621829EAA4 * ___m_Touches_13;
	// System.Collections.Generic.List`1<Core.Input.MouseButtonInfo> Core.Input.InputController::m_MouseInfo
	List_1_t7B2B84536A834129E63FA96130202D797A2D624A * ___m_MouseInfo_14;
	// System.Boolean Core.Input.InputController::<mouseButtonPressedThisFrame>k__BackingField
	bool ___U3CmouseButtonPressedThisFrameU3Ek__BackingField_15;
	// System.Boolean Core.Input.InputController::<mouseMovedOnThisFrame>k__BackingField
	bool ___U3CmouseMovedOnThisFrameU3Ek__BackingField_16;
	// System.Boolean Core.Input.InputController::<touchPressedThisFrame>k__BackingField
	bool ___U3CtouchPressedThisFrameU3Ek__BackingField_17;
	// Core.Input.PointerInfo Core.Input.InputController::<basicMouseInfo>k__BackingField
	PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB * ___U3CbasicMouseInfoU3Ek__BackingField_18;
	// System.Action`1<Core.Input.PointerActionInfo> Core.Input.InputController::pressed
	Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * ___pressed_19;
	// System.Action`1<Core.Input.PointerActionInfo> Core.Input.InputController::released
	Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * ___released_20;
	// System.Action`1<Core.Input.PointerActionInfo> Core.Input.InputController::tapped
	Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * ___tapped_21;
	// System.Action`1<Core.Input.PointerActionInfo> Core.Input.InputController::startedDrag
	Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * ___startedDrag_22;
	// System.Action`1<Core.Input.PointerActionInfo> Core.Input.InputController::dragged
	Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * ___dragged_23;
	// System.Action`1<Core.Input.PointerActionInfo> Core.Input.InputController::startedHold
	Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * ___startedHold_24;
	// System.Action`1<Core.Input.WheelInfo> Core.Input.InputController::spunWheel
	Action_1_t481C0EDC6AC5AB041D11A7E7B09B193CCDCAEDA4 * ___spunWheel_25;
	// System.Action`1<Core.Input.PinchInfo> Core.Input.InputController::pinched
	Action_1_t8F2FA388F2027FB226797F1A92127EA304C1B90B * ___pinched_26;
	// System.Action`1<Core.Input.PointerInfo> Core.Input.InputController::mouseMoved
	Action_1_t07687568CEAACB78656500EE24DE97612CAC8085 * ___mouseMoved_27;

public:
	inline static int32_t get_offset_of_dragThresholdTouch_6() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___dragThresholdTouch_6)); }
	inline float get_dragThresholdTouch_6() const { return ___dragThresholdTouch_6; }
	inline float* get_address_of_dragThresholdTouch_6() { return &___dragThresholdTouch_6; }
	inline void set_dragThresholdTouch_6(float value)
	{
		___dragThresholdTouch_6 = value;
	}

	inline static int32_t get_offset_of_dragThresholdMouse_7() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___dragThresholdMouse_7)); }
	inline float get_dragThresholdMouse_7() const { return ___dragThresholdMouse_7; }
	inline float* get_address_of_dragThresholdMouse_7() { return &___dragThresholdMouse_7; }
	inline void set_dragThresholdMouse_7(float value)
	{
		___dragThresholdMouse_7 = value;
	}

	inline static int32_t get_offset_of_tapTime_8() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___tapTime_8)); }
	inline float get_tapTime_8() const { return ___tapTime_8; }
	inline float* get_address_of_tapTime_8() { return &___tapTime_8; }
	inline void set_tapTime_8(float value)
	{
		___tapTime_8 = value;
	}

	inline static int32_t get_offset_of_holdTime_9() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___holdTime_9)); }
	inline float get_holdTime_9() const { return ___holdTime_9; }
	inline float* get_address_of_holdTime_9() { return &___holdTime_9; }
	inline void set_holdTime_9(float value)
	{
		___holdTime_9 = value;
	}

	inline static int32_t get_offset_of_mouseWheelSensitivity_10() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___mouseWheelSensitivity_10)); }
	inline float get_mouseWheelSensitivity_10() const { return ___mouseWheelSensitivity_10; }
	inline float* get_address_of_mouseWheelSensitivity_10() { return &___mouseWheelSensitivity_10; }
	inline void set_mouseWheelSensitivity_10(float value)
	{
		___mouseWheelSensitivity_10 = value;
	}

	inline static int32_t get_offset_of_trackMouseButtons_11() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___trackMouseButtons_11)); }
	inline int32_t get_trackMouseButtons_11() const { return ___trackMouseButtons_11; }
	inline int32_t* get_address_of_trackMouseButtons_11() { return &___trackMouseButtons_11; }
	inline void set_trackMouseButtons_11(int32_t value)
	{
		___trackMouseButtons_11 = value;
	}

	inline static int32_t get_offset_of_flickThreshold_12() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___flickThreshold_12)); }
	inline float get_flickThreshold_12() const { return ___flickThreshold_12; }
	inline float* get_address_of_flickThreshold_12() { return &___flickThreshold_12; }
	inline void set_flickThreshold_12(float value)
	{
		___flickThreshold_12 = value;
	}

	inline static int32_t get_offset_of_m_Touches_13() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___m_Touches_13)); }
	inline List_1_tB2F89C3237A8F9BC195D8869C618C1621829EAA4 * get_m_Touches_13() const { return ___m_Touches_13; }
	inline List_1_tB2F89C3237A8F9BC195D8869C618C1621829EAA4 ** get_address_of_m_Touches_13() { return &___m_Touches_13; }
	inline void set_m_Touches_13(List_1_tB2F89C3237A8F9BC195D8869C618C1621829EAA4 * value)
	{
		___m_Touches_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Touches_13), value);
	}

	inline static int32_t get_offset_of_m_MouseInfo_14() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___m_MouseInfo_14)); }
	inline List_1_t7B2B84536A834129E63FA96130202D797A2D624A * get_m_MouseInfo_14() const { return ___m_MouseInfo_14; }
	inline List_1_t7B2B84536A834129E63FA96130202D797A2D624A ** get_address_of_m_MouseInfo_14() { return &___m_MouseInfo_14; }
	inline void set_m_MouseInfo_14(List_1_t7B2B84536A834129E63FA96130202D797A2D624A * value)
	{
		___m_MouseInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseInfo_14), value);
	}

	inline static int32_t get_offset_of_U3CmouseButtonPressedThisFrameU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___U3CmouseButtonPressedThisFrameU3Ek__BackingField_15)); }
	inline bool get_U3CmouseButtonPressedThisFrameU3Ek__BackingField_15() const { return ___U3CmouseButtonPressedThisFrameU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CmouseButtonPressedThisFrameU3Ek__BackingField_15() { return &___U3CmouseButtonPressedThisFrameU3Ek__BackingField_15; }
	inline void set_U3CmouseButtonPressedThisFrameU3Ek__BackingField_15(bool value)
	{
		___U3CmouseButtonPressedThisFrameU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CmouseMovedOnThisFrameU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___U3CmouseMovedOnThisFrameU3Ek__BackingField_16)); }
	inline bool get_U3CmouseMovedOnThisFrameU3Ek__BackingField_16() const { return ___U3CmouseMovedOnThisFrameU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CmouseMovedOnThisFrameU3Ek__BackingField_16() { return &___U3CmouseMovedOnThisFrameU3Ek__BackingField_16; }
	inline void set_U3CmouseMovedOnThisFrameU3Ek__BackingField_16(bool value)
	{
		___U3CmouseMovedOnThisFrameU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CtouchPressedThisFrameU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___U3CtouchPressedThisFrameU3Ek__BackingField_17)); }
	inline bool get_U3CtouchPressedThisFrameU3Ek__BackingField_17() const { return ___U3CtouchPressedThisFrameU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CtouchPressedThisFrameU3Ek__BackingField_17() { return &___U3CtouchPressedThisFrameU3Ek__BackingField_17; }
	inline void set_U3CtouchPressedThisFrameU3Ek__BackingField_17(bool value)
	{
		___U3CtouchPressedThisFrameU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CbasicMouseInfoU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___U3CbasicMouseInfoU3Ek__BackingField_18)); }
	inline PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB * get_U3CbasicMouseInfoU3Ek__BackingField_18() const { return ___U3CbasicMouseInfoU3Ek__BackingField_18; }
	inline PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB ** get_address_of_U3CbasicMouseInfoU3Ek__BackingField_18() { return &___U3CbasicMouseInfoU3Ek__BackingField_18; }
	inline void set_U3CbasicMouseInfoU3Ek__BackingField_18(PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB * value)
	{
		___U3CbasicMouseInfoU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbasicMouseInfoU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_pressed_19() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___pressed_19)); }
	inline Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * get_pressed_19() const { return ___pressed_19; }
	inline Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 ** get_address_of_pressed_19() { return &___pressed_19; }
	inline void set_pressed_19(Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * value)
	{
		___pressed_19 = value;
		Il2CppCodeGenWriteBarrier((&___pressed_19), value);
	}

	inline static int32_t get_offset_of_released_20() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___released_20)); }
	inline Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * get_released_20() const { return ___released_20; }
	inline Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 ** get_address_of_released_20() { return &___released_20; }
	inline void set_released_20(Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * value)
	{
		___released_20 = value;
		Il2CppCodeGenWriteBarrier((&___released_20), value);
	}

	inline static int32_t get_offset_of_tapped_21() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___tapped_21)); }
	inline Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * get_tapped_21() const { return ___tapped_21; }
	inline Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 ** get_address_of_tapped_21() { return &___tapped_21; }
	inline void set_tapped_21(Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * value)
	{
		___tapped_21 = value;
		Il2CppCodeGenWriteBarrier((&___tapped_21), value);
	}

	inline static int32_t get_offset_of_startedDrag_22() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___startedDrag_22)); }
	inline Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * get_startedDrag_22() const { return ___startedDrag_22; }
	inline Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 ** get_address_of_startedDrag_22() { return &___startedDrag_22; }
	inline void set_startedDrag_22(Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * value)
	{
		___startedDrag_22 = value;
		Il2CppCodeGenWriteBarrier((&___startedDrag_22), value);
	}

	inline static int32_t get_offset_of_dragged_23() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___dragged_23)); }
	inline Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * get_dragged_23() const { return ___dragged_23; }
	inline Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 ** get_address_of_dragged_23() { return &___dragged_23; }
	inline void set_dragged_23(Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * value)
	{
		___dragged_23 = value;
		Il2CppCodeGenWriteBarrier((&___dragged_23), value);
	}

	inline static int32_t get_offset_of_startedHold_24() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___startedHold_24)); }
	inline Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * get_startedHold_24() const { return ___startedHold_24; }
	inline Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 ** get_address_of_startedHold_24() { return &___startedHold_24; }
	inline void set_startedHold_24(Action_1_t334E803E81D0F678454B9485C5C9DCECB41314B1 * value)
	{
		___startedHold_24 = value;
		Il2CppCodeGenWriteBarrier((&___startedHold_24), value);
	}

	inline static int32_t get_offset_of_spunWheel_25() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___spunWheel_25)); }
	inline Action_1_t481C0EDC6AC5AB041D11A7E7B09B193CCDCAEDA4 * get_spunWheel_25() const { return ___spunWheel_25; }
	inline Action_1_t481C0EDC6AC5AB041D11A7E7B09B193CCDCAEDA4 ** get_address_of_spunWheel_25() { return &___spunWheel_25; }
	inline void set_spunWheel_25(Action_1_t481C0EDC6AC5AB041D11A7E7B09B193CCDCAEDA4 * value)
	{
		___spunWheel_25 = value;
		Il2CppCodeGenWriteBarrier((&___spunWheel_25), value);
	}

	inline static int32_t get_offset_of_pinched_26() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___pinched_26)); }
	inline Action_1_t8F2FA388F2027FB226797F1A92127EA304C1B90B * get_pinched_26() const { return ___pinched_26; }
	inline Action_1_t8F2FA388F2027FB226797F1A92127EA304C1B90B ** get_address_of_pinched_26() { return &___pinched_26; }
	inline void set_pinched_26(Action_1_t8F2FA388F2027FB226797F1A92127EA304C1B90B * value)
	{
		___pinched_26 = value;
		Il2CppCodeGenWriteBarrier((&___pinched_26), value);
	}

	inline static int32_t get_offset_of_mouseMoved_27() { return static_cast<int32_t>(offsetof(InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45, ___mouseMoved_27)); }
	inline Action_1_t07687568CEAACB78656500EE24DE97612CAC8085 * get_mouseMoved_27() const { return ___mouseMoved_27; }
	inline Action_1_t07687568CEAACB78656500EE24DE97612CAC8085 ** get_address_of_mouseMoved_27() { return &___mouseMoved_27; }
	inline void set_mouseMoved_27(Action_1_t07687568CEAACB78656500EE24DE97612CAC8085 * value)
	{
		___mouseMoved_27 = value;
		Il2CppCodeGenWriteBarrier((&___mouseMoved_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTCONTROLLER_T7EED1B6916BB59A90A8974D508B8480EA0B12B45_H
#ifndef BASICANIMATINGMAINMENUPAGE_T232167802C8CC8F286537A9ADA57BDB51B9BAE33_H
#define BASICANIMATINGMAINMENUPAGE_T232167802C8CC8F286537A9ADA57BDB51B9BAE33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.UI.BasicAnimatingMainMenuPage
struct  BasicAnimatingMainMenuPage_t232167802C8CC8F286537A9ADA57BDB51B9BAE33  : public AnimatingMainMenuPage_tB189461182DB6C25EB964EA89022715D731EFDA0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICANIMATINGMAINMENUPAGE_T232167802C8CC8F286537A9ADA57BDB51B9BAE33_H
#ifndef POOLMANAGER_TC4AA76A7B5FF355D068EEC877E2680E2DC9E771F_H
#define POOLMANAGER_TC4AA76A7B5FF355D068EEC877E2680E2DC9E771F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.PoolManager
struct  PoolManager_tC4AA76A7B5FF355D068EEC877E2680E2DC9E771F  : public Singleton_1_t0A072F12096883D64D8936C7E8E499271D434D08
{
public:
	// System.Collections.Generic.List`1<Core.Utilities.Poolable> Core.Utilities.PoolManager::poolables
	List_1_t20B9B8A4495121AF636424D41BC08F7E75DAF87D * ___poolables_5;
	// System.Collections.Generic.Dictionary`2<Core.Utilities.Poolable,Core.Utilities.AutoComponentPrefabPool`1<Core.Utilities.Poolable>> Core.Utilities.PoolManager::m_Pools
	Dictionary_2_tAFB69BEC9DD355D9F4D7A6DA8507756D019398EF * ___m_Pools_6;

public:
	inline static int32_t get_offset_of_poolables_5() { return static_cast<int32_t>(offsetof(PoolManager_tC4AA76A7B5FF355D068EEC877E2680E2DC9E771F, ___poolables_5)); }
	inline List_1_t20B9B8A4495121AF636424D41BC08F7E75DAF87D * get_poolables_5() const { return ___poolables_5; }
	inline List_1_t20B9B8A4495121AF636424D41BC08F7E75DAF87D ** get_address_of_poolables_5() { return &___poolables_5; }
	inline void set_poolables_5(List_1_t20B9B8A4495121AF636424D41BC08F7E75DAF87D * value)
	{
		___poolables_5 = value;
		Il2CppCodeGenWriteBarrier((&___poolables_5), value);
	}

	inline static int32_t get_offset_of_m_Pools_6() { return static_cast<int32_t>(offsetof(PoolManager_tC4AA76A7B5FF355D068EEC877E2680E2DC9E771F, ___m_Pools_6)); }
	inline Dictionary_2_tAFB69BEC9DD355D9F4D7A6DA8507756D019398EF * get_m_Pools_6() const { return ___m_Pools_6; }
	inline Dictionary_2_tAFB69BEC9DD355D9F4D7A6DA8507756D019398EF ** get_address_of_m_Pools_6() { return &___m_Pools_6; }
	inline void set_m_Pools_6(Dictionary_2_tAFB69BEC9DD355D9F4D7A6DA8507756D019398EF * value)
	{
		___m_Pools_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Pools_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLMANAGER_TC4AA76A7B5FF355D068EEC877E2680E2DC9E771F_H
#ifndef WAVE_T7EA46D35418196B830BC891624B786640BB0F586_H
#define WAVE_T7EA46D35418196B830BC891624B786640BB0F586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level.Waves.Wave
struct  Wave_t7EA46D35418196B830BC891624B786640BB0F586  : public TimedBehaviour_t4297CA025FC6BC8794A1151BC41DE0ACB076CCCA
{
public:
	// System.Collections.Generic.List`1<Level.SpawnInstruction> Level.Waves.Wave::spawnInstructions
	List_1_t87DB7F5E86C53503A5ADE7BE5C57DBB891DA7ADD * ___spawnInstructions_5;
	// System.Int32 Level.Waves.Wave::m_CurrentIndex
	int32_t ___m_CurrentIndex_6;
	// Core.Utilities.RepeatingTimer Level.Waves.Wave::m_SpawnTimer
	RepeatingTimer_tFEABA0EAF35D64F27E55652FD50AF98799EDC468 * ___m_SpawnTimer_7;
	// System.Action Level.Waves.Wave::waveCompleted
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___waveCompleted_8;

public:
	inline static int32_t get_offset_of_spawnInstructions_5() { return static_cast<int32_t>(offsetof(Wave_t7EA46D35418196B830BC891624B786640BB0F586, ___spawnInstructions_5)); }
	inline List_1_t87DB7F5E86C53503A5ADE7BE5C57DBB891DA7ADD * get_spawnInstructions_5() const { return ___spawnInstructions_5; }
	inline List_1_t87DB7F5E86C53503A5ADE7BE5C57DBB891DA7ADD ** get_address_of_spawnInstructions_5() { return &___spawnInstructions_5; }
	inline void set_spawnInstructions_5(List_1_t87DB7F5E86C53503A5ADE7BE5C57DBB891DA7ADD * value)
	{
		___spawnInstructions_5 = value;
		Il2CppCodeGenWriteBarrier((&___spawnInstructions_5), value);
	}

	inline static int32_t get_offset_of_m_CurrentIndex_6() { return static_cast<int32_t>(offsetof(Wave_t7EA46D35418196B830BC891624B786640BB0F586, ___m_CurrentIndex_6)); }
	inline int32_t get_m_CurrentIndex_6() const { return ___m_CurrentIndex_6; }
	inline int32_t* get_address_of_m_CurrentIndex_6() { return &___m_CurrentIndex_6; }
	inline void set_m_CurrentIndex_6(int32_t value)
	{
		___m_CurrentIndex_6 = value;
	}

	inline static int32_t get_offset_of_m_SpawnTimer_7() { return static_cast<int32_t>(offsetof(Wave_t7EA46D35418196B830BC891624B786640BB0F586, ___m_SpawnTimer_7)); }
	inline RepeatingTimer_tFEABA0EAF35D64F27E55652FD50AF98799EDC468 * get_m_SpawnTimer_7() const { return ___m_SpawnTimer_7; }
	inline RepeatingTimer_tFEABA0EAF35D64F27E55652FD50AF98799EDC468 ** get_address_of_m_SpawnTimer_7() { return &___m_SpawnTimer_7; }
	inline void set_m_SpawnTimer_7(RepeatingTimer_tFEABA0EAF35D64F27E55652FD50AF98799EDC468 * value)
	{
		___m_SpawnTimer_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpawnTimer_7), value);
	}

	inline static int32_t get_offset_of_waveCompleted_8() { return static_cast<int32_t>(offsetof(Wave_t7EA46D35418196B830BC891624B786640BB0F586, ___waveCompleted_8)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_waveCompleted_8() const { return ___waveCompleted_8; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_waveCompleted_8() { return &___waveCompleted_8; }
	inline void set_waveCompleted_8(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___waveCompleted_8 = value;
		Il2CppCodeGenWriteBarrier((&___waveCompleted_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAVE_T7EA46D35418196B830BC891624B786640BB0F586_H
#ifndef WAVEMANAGER_TBD677215628B517D81FD26BD40FD7A55051AD800_H
#define WAVEMANAGER_TBD677215628B517D81FD26BD40FD7A55051AD800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level.Waves.WaveManager
struct  WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800  : public Singleton_1_tCCDC37ABE94E8327269A8C833E467BD733005DCE
{
public:
	// System.Int32 Level.Waves.WaveManager::m_CurrentIndex
	int32_t ___m_CurrentIndex_5;
	// Pathfinding.Nodes.Node Level.Waves.WaveManager::DefaultStartingNode
	Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 * ___DefaultStartingNode_6;
	// System.Boolean Level.Waves.WaveManager::startWavesOnAwake
	bool ___startWavesOnAwake_7;
	// System.Collections.Generic.List`1<Level.Waves.Wave> Level.Waves.WaveManager::waves
	List_1_t804D7EE5940213EA47FF7D51872C5B9845A63218 * ___waves_8;
	// System.Action Level.Waves.WaveManager::waveChanged
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___waveChanged_9;
	// System.Action Level.Waves.WaveManager::spawningCompleted
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___spawningCompleted_10;

public:
	inline static int32_t get_offset_of_m_CurrentIndex_5() { return static_cast<int32_t>(offsetof(WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800, ___m_CurrentIndex_5)); }
	inline int32_t get_m_CurrentIndex_5() const { return ___m_CurrentIndex_5; }
	inline int32_t* get_address_of_m_CurrentIndex_5() { return &___m_CurrentIndex_5; }
	inline void set_m_CurrentIndex_5(int32_t value)
	{
		___m_CurrentIndex_5 = value;
	}

	inline static int32_t get_offset_of_DefaultStartingNode_6() { return static_cast<int32_t>(offsetof(WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800, ___DefaultStartingNode_6)); }
	inline Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 * get_DefaultStartingNode_6() const { return ___DefaultStartingNode_6; }
	inline Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 ** get_address_of_DefaultStartingNode_6() { return &___DefaultStartingNode_6; }
	inline void set_DefaultStartingNode_6(Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 * value)
	{
		___DefaultStartingNode_6 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultStartingNode_6), value);
	}

	inline static int32_t get_offset_of_startWavesOnAwake_7() { return static_cast<int32_t>(offsetof(WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800, ___startWavesOnAwake_7)); }
	inline bool get_startWavesOnAwake_7() const { return ___startWavesOnAwake_7; }
	inline bool* get_address_of_startWavesOnAwake_7() { return &___startWavesOnAwake_7; }
	inline void set_startWavesOnAwake_7(bool value)
	{
		___startWavesOnAwake_7 = value;
	}

	inline static int32_t get_offset_of_waves_8() { return static_cast<int32_t>(offsetof(WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800, ___waves_8)); }
	inline List_1_t804D7EE5940213EA47FF7D51872C5B9845A63218 * get_waves_8() const { return ___waves_8; }
	inline List_1_t804D7EE5940213EA47FF7D51872C5B9845A63218 ** get_address_of_waves_8() { return &___waves_8; }
	inline void set_waves_8(List_1_t804D7EE5940213EA47FF7D51872C5B9845A63218 * value)
	{
		___waves_8 = value;
		Il2CppCodeGenWriteBarrier((&___waves_8), value);
	}

	inline static int32_t get_offset_of_waveChanged_9() { return static_cast<int32_t>(offsetof(WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800, ___waveChanged_9)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_waveChanged_9() const { return ___waveChanged_9; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_waveChanged_9() { return &___waveChanged_9; }
	inline void set_waveChanged_9(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___waveChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___waveChanged_9), value);
	}

	inline static int32_t get_offset_of_spawningCompleted_10() { return static_cast<int32_t>(offsetof(WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800, ___spawningCompleted_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_spawningCompleted_10() const { return ___spawningCompleted_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_spawningCompleted_10() { return &___spawningCompleted_10; }
	inline void set_spawningCompleted_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___spawningCompleted_10 = value;
		Il2CppCodeGenWriteBarrier((&___spawningCompleted_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAVEMANAGER_TBD677215628B517D81FD26BD40FD7A55051AD800_H
#ifndef KEYBOARDMOUSEINPUT_T695D0A615AD59B9BBB1C8FBA24F5236117D9813E_H
#define KEYBOARDMOUSEINPUT_T695D0A615AD59B9BBB1C8FBA24F5236117D9813E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.KeyboardMouseInput
struct  KeyboardMouseInput_t695D0A615AD59B9BBB1C8FBA24F5236117D9813E  : public CameraInputScheme_tEB16782DA7575673162607368A520FB02D2739E5
{
public:
	// System.Single Core.Input.KeyboardMouseInput::screenPanThreshold
	float ___screenPanThreshold_6;
	// System.Single Core.Input.KeyboardMouseInput::mouseEdgePanSpeed
	float ___mouseEdgePanSpeed_7;
	// System.Single Core.Input.KeyboardMouseInput::mouseRmbPanSpeed
	float ___mouseRmbPanSpeed_8;

public:
	inline static int32_t get_offset_of_screenPanThreshold_6() { return static_cast<int32_t>(offsetof(KeyboardMouseInput_t695D0A615AD59B9BBB1C8FBA24F5236117D9813E, ___screenPanThreshold_6)); }
	inline float get_screenPanThreshold_6() const { return ___screenPanThreshold_6; }
	inline float* get_address_of_screenPanThreshold_6() { return &___screenPanThreshold_6; }
	inline void set_screenPanThreshold_6(float value)
	{
		___screenPanThreshold_6 = value;
	}

	inline static int32_t get_offset_of_mouseEdgePanSpeed_7() { return static_cast<int32_t>(offsetof(KeyboardMouseInput_t695D0A615AD59B9BBB1C8FBA24F5236117D9813E, ___mouseEdgePanSpeed_7)); }
	inline float get_mouseEdgePanSpeed_7() const { return ___mouseEdgePanSpeed_7; }
	inline float* get_address_of_mouseEdgePanSpeed_7() { return &___mouseEdgePanSpeed_7; }
	inline void set_mouseEdgePanSpeed_7(float value)
	{
		___mouseEdgePanSpeed_7 = value;
	}

	inline static int32_t get_offset_of_mouseRmbPanSpeed_8() { return static_cast<int32_t>(offsetof(KeyboardMouseInput_t695D0A615AD59B9BBB1C8FBA24F5236117D9813E, ___mouseRmbPanSpeed_8)); }
	inline float get_mouseRmbPanSpeed_8() const { return ___mouseRmbPanSpeed_8; }
	inline float* get_address_of_mouseRmbPanSpeed_8() { return &___mouseRmbPanSpeed_8; }
	inline void set_mouseRmbPanSpeed_8(float value)
	{
		___mouseRmbPanSpeed_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDMOUSEINPUT_T695D0A615AD59B9BBB1C8FBA24F5236117D9813E_H
#ifndef TOUCHINPUT_TC9037DFA662201899E9B004912AEAA29E81F4F60_H
#define TOUCHINPUT_TC9037DFA662201899E9B004912AEAA29E81F4F60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Input.TouchInput
struct  TouchInput_tC9037DFA662201899E9B004912AEAA29E81F4F60  : public CameraInputScheme_tEB16782DA7575673162607368A520FB02D2739E5
{
public:
	// System.Single Core.Input.TouchInput::panSpeed
	float ___panSpeed_6;
	// System.Single Core.Input.TouchInput::flickDecayFactor
	float ___flickDecayFactor_7;
	// UnityEngine.Vector3 Core.Input.TouchInput::m_FlickDirection
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_FlickDirection_8;

public:
	inline static int32_t get_offset_of_panSpeed_6() { return static_cast<int32_t>(offsetof(TouchInput_tC9037DFA662201899E9B004912AEAA29E81F4F60, ___panSpeed_6)); }
	inline float get_panSpeed_6() const { return ___panSpeed_6; }
	inline float* get_address_of_panSpeed_6() { return &___panSpeed_6; }
	inline void set_panSpeed_6(float value)
	{
		___panSpeed_6 = value;
	}

	inline static int32_t get_offset_of_flickDecayFactor_7() { return static_cast<int32_t>(offsetof(TouchInput_tC9037DFA662201899E9B004912AEAA29E81F4F60, ___flickDecayFactor_7)); }
	inline float get_flickDecayFactor_7() const { return ___flickDecayFactor_7; }
	inline float* get_address_of_flickDecayFactor_7() { return &___flickDecayFactor_7; }
	inline void set_flickDecayFactor_7(float value)
	{
		___flickDecayFactor_7 = value;
	}

	inline static int32_t get_offset_of_m_FlickDirection_8() { return static_cast<int32_t>(offsetof(TouchInput_tC9037DFA662201899E9B004912AEAA29E81F4F60, ___m_FlickDirection_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_FlickDirection_8() const { return ___m_FlickDirection_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_FlickDirection_8() { return &___m_FlickDirection_8; }
	inline void set_m_FlickDirection_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_FlickDirection_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHINPUT_TC9037DFA662201899E9B004912AEAA29E81F4F60_H
#ifndef TIMEDWAVE_TAE5CAECC23693939A6B523208893CB192FF36D8E_H
#define TIMEDWAVE_TAE5CAECC23693939A6B523208893CB192FF36D8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level.Waves.TimedWave
struct  TimedWave_tAE5CAECC23693939A6B523208893CB192FF36D8E  : public Wave_t7EA46D35418196B830BC891624B786640BB0F586
{
public:
	// System.Single Level.Waves.TimedWave::timeToNextWave
	float ___timeToNextWave_9;
	// Core.Utilities.Timer Level.Waves.TimedWave::m_WaveTimer
	Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B * ___m_WaveTimer_10;

public:
	inline static int32_t get_offset_of_timeToNextWave_9() { return static_cast<int32_t>(offsetof(TimedWave_tAE5CAECC23693939A6B523208893CB192FF36D8E, ___timeToNextWave_9)); }
	inline float get_timeToNextWave_9() const { return ___timeToNextWave_9; }
	inline float* get_address_of_timeToNextWave_9() { return &___timeToNextWave_9; }
	inline void set_timeToNextWave_9(float value)
	{
		___timeToNextWave_9 = value;
	}

	inline static int32_t get_offset_of_m_WaveTimer_10() { return static_cast<int32_t>(offsetof(TimedWave_tAE5CAECC23693939A6B523208893CB192FF36D8E, ___m_WaveTimer_10)); }
	inline Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B * get_m_WaveTimer_10() const { return ___m_WaveTimer_10; }
	inline Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B ** get_address_of_m_WaveTimer_10() { return &___m_WaveTimer_10; }
	inline void set_m_WaveTimer_10(Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B * value)
	{
		___m_WaveTimer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_WaveTimer_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDWAVE_TAE5CAECC23693939A6B523208893CB192FF36D8E_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7500 = { sizeof (Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7500[3] = 
{
	Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864::get_offset_of_target_0(),
	Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864::get_offset_of_action_1(),
	Entry_tD15B328556DA14DB79346DCBC40047DB82C2A864::get_offset_of_delay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7501 = { sizeof (Entries_t0337CB356ADCAF3563885A2117B6C5DA4DCB9E3E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7501[1] = 
{
	Entries_t0337CB356ADCAF3563885A2117B6C5DA4DCB9E3E::get_offset_of_entries_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7502 = { sizeof (U3CActivateU3Ed__5_tABE89194BB953D1E0A8B14D2BF1E92D09509958F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7502[3] = 
{
	U3CActivateU3Ed__5_tABE89194BB953D1E0A8B14D2BF1E92D09509958F::get_offset_of_U3CU3E1__state_0(),
	U3CActivateU3Ed__5_tABE89194BB953D1E0A8B14D2BF1E92D09509958F::get_offset_of_U3CU3E2__current_1(),
	U3CActivateU3Ed__5_tABE89194BB953D1E0A8B14D2BF1E92D09509958F::get_offset_of_entry_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7503 = { sizeof (U3CDeactivateU3Ed__6_tA37ED97A195669F022F3A77008A29533A5CCEEC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7503[3] = 
{
	U3CDeactivateU3Ed__6_tA37ED97A195669F022F3A77008A29533A5CCEEC1::get_offset_of_U3CU3E1__state_0(),
	U3CDeactivateU3Ed__6_tA37ED97A195669F022F3A77008A29533A5CCEEC1::get_offset_of_U3CU3E2__current_1(),
	U3CDeactivateU3Ed__6_tA37ED97A195669F022F3A77008A29533A5CCEEC1::get_offset_of_entry_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7504 = { sizeof (U3CReloadLevelU3Ed__7_tA802367CF3ABCAA4BFD1F0DE841A813AF26B0386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7504[3] = 
{
	U3CReloadLevelU3Ed__7_tA802367CF3ABCAA4BFD1F0DE841A813AF26B0386::get_offset_of_U3CU3E1__state_0(),
	U3CReloadLevelU3Ed__7_tA802367CF3ABCAA4BFD1F0DE841A813AF26B0386::get_offset_of_U3CU3E2__current_1(),
	U3CReloadLevelU3Ed__7_tA802367CF3ABCAA4BFD1F0DE841A813AF26B0386::get_offset_of_entry_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7505 = { sizeof (TimedObjectDestructor_t8783E91456633F1C86444708432B962621D06B5D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7505[2] = 
{
	TimedObjectDestructor_t8783E91456633F1C86444708432B962621D06B5D::get_offset_of_m_TimeOut_4(),
	TimedObjectDestructor_t8783E91456633F1C86444708432B962621D06B5D::get_offset_of_m_DetachChildren_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7506 = { sizeof (WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7506[16] = 
{
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_waypointList_4(),
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_smoothRoute_5(),
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_numPoints_6(),
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_points_7(),
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_distances_8(),
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_editorVisualisationSubsteps_9(),
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_U3CLengthU3Ek__BackingField_10(),
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_p0n_11(),
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_p1n_12(),
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_p2n_13(),
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_p3n_14(),
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_i_15(),
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_P0_16(),
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_P1_17(),
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_P2_18(),
	WaypointCircuit_tE92057604A54283CA3531B3F8A316D7F520FC8FE::get_offset_of_P3_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7507 = { sizeof (WaypointList_t28075B7540AC2B9E2507C0E6175C6D5B31F7364D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7507[2] = 
{
	WaypointList_t28075B7540AC2B9E2507C0E6175C6D5B31F7364D::get_offset_of_circuit_0(),
	WaypointList_t28075B7540AC2B9E2507C0E6175C6D5B31F7364D::get_offset_of_items_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7508 = { sizeof (RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16)+ sizeof (RuntimeObject), sizeof(RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16 ), 0, 0 };
extern const int32_t g_FieldOffsetTable7508[2] = 
{
	RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoutePoint_t6EF96F6A79A6B4DA6B9ADB6E40CD542F2DC43A16::get_offset_of_direction_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7509 = { sizeof (WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7509[15] = 
{
	WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9::get_offset_of_circuit_4(),
	WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9::get_offset_of_lookAheadForTargetOffset_5(),
	WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9::get_offset_of_lookAheadForTargetFactor_6(),
	WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9::get_offset_of_lookAheadForSpeedOffset_7(),
	WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9::get_offset_of_lookAheadForSpeedFactor_8(),
	WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9::get_offset_of_progressStyle_9(),
	WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9::get_offset_of_pointToPointThreshold_10(),
	WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9::get_offset_of_U3CtargetPointU3Ek__BackingField_11(),
	WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9::get_offset_of_U3CspeedPointU3Ek__BackingField_12(),
	WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9::get_offset_of_U3CprogressPointU3Ek__BackingField_13(),
	WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9::get_offset_of_target_14(),
	WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9::get_offset_of_progressDistance_15(),
	WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9::get_offset_of_progressNum_16(),
	WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9::get_offset_of_lastPosition_17(),
	WaypointProgressTracker_tDDB26AC32CC6ECD02D2301A148750F07695C1AF9::get_offset_of_speed_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7510 = { sizeof (ProgressStyle_tB2C07787FC973F9972DF4CDC1A266C420440B108)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7510[3] = 
{
	ProgressStyle_tB2C07787FC973F9972DF4CDC1A266C420440B108::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7511 = { sizeof (AfterburnerPhysicsForce_t9F0D1071F45FDA99D798A1C46886A26A592F8408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7511[6] = 
{
	AfterburnerPhysicsForce_t9F0D1071F45FDA99D798A1C46886A26A592F8408::get_offset_of_effectAngle_4(),
	AfterburnerPhysicsForce_t9F0D1071F45FDA99D798A1C46886A26A592F8408::get_offset_of_effectWidth_5(),
	AfterburnerPhysicsForce_t9F0D1071F45FDA99D798A1C46886A26A592F8408::get_offset_of_effectDistance_6(),
	AfterburnerPhysicsForce_t9F0D1071F45FDA99D798A1C46886A26A592F8408::get_offset_of_force_7(),
	AfterburnerPhysicsForce_t9F0D1071F45FDA99D798A1C46886A26A592F8408::get_offset_of_m_Cols_8(),
	AfterburnerPhysicsForce_t9F0D1071F45FDA99D798A1C46886A26A592F8408::get_offset_of_m_Sphere_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7512 = { sizeof (ExplosionFireAndDebris_tE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7512[4] = 
{
	ExplosionFireAndDebris_tE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA::get_offset_of_debrisPrefabs_4(),
	ExplosionFireAndDebris_tE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA::get_offset_of_firePrefab_5(),
	ExplosionFireAndDebris_tE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA::get_offset_of_numDebrisPieces_6(),
	ExplosionFireAndDebris_tE2976CDB7E4452538EFB104C1A3EC124BDA1A7FA::get_offset_of_numFires_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7513 = { sizeof (U3CStartU3Ed__4_t8F787342BA2EF55D1F910EC7C170C59B1B6AB17F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7513[4] = 
{
	U3CStartU3Ed__4_t8F787342BA2EF55D1F910EC7C170C59B1B6AB17F::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__4_t8F787342BA2EF55D1F910EC7C170C59B1B6AB17F::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__4_t8F787342BA2EF55D1F910EC7C170C59B1B6AB17F::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__4_t8F787342BA2EF55D1F910EC7C170C59B1B6AB17F::get_offset_of_U3CmultiplierU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7514 = { sizeof (ExplosionPhysicsForce_t2BC67AEA4386688129F43B43EB9927BE57A384A8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7514[1] = 
{
	ExplosionPhysicsForce_t2BC67AEA4386688129F43B43EB9927BE57A384A8::get_offset_of_explosionForce_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7515 = { sizeof (U3CStartU3Ed__1_tBA16B8AACF91AA6BA165D5587233B3B3BC159B2F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7515[3] = 
{
	U3CStartU3Ed__1_tBA16B8AACF91AA6BA165D5587233B3B3BC159B2F::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__1_tBA16B8AACF91AA6BA165D5587233B3B3BC159B2F::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__1_tBA16B8AACF91AA6BA165D5587233B3B3BC159B2F::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7516 = { sizeof (Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7516[7] = 
{
	Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40::get_offset_of_explosionPrefab_4(),
	Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40::get_offset_of_detonationImpactVelocity_5(),
	Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40::get_offset_of_sizeMultiplier_6(),
	Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40::get_offset_of_reset_7(),
	Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40::get_offset_of_resetTimeDelay_8(),
	Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40::get_offset_of_m_Exploded_9(),
	Explosive_tDE008C99033356E9A6400EBF52C9F532AC681E40::get_offset_of_m_ObjectResetter_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7517 = { sizeof (U3COnCollisionEnterU3Ed__8_t9CC677DD20DFE89A30A2E5E4A99CF9EA59CAB3E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7517[4] = 
{
	U3COnCollisionEnterU3Ed__8_t9CC677DD20DFE89A30A2E5E4A99CF9EA59CAB3E3::get_offset_of_U3CU3E1__state_0(),
	U3COnCollisionEnterU3Ed__8_t9CC677DD20DFE89A30A2E5E4A99CF9EA59CAB3E3::get_offset_of_U3CU3E2__current_1(),
	U3COnCollisionEnterU3Ed__8_t9CC677DD20DFE89A30A2E5E4A99CF9EA59CAB3E3::get_offset_of_U3CU3E4__this_2(),
	U3COnCollisionEnterU3Ed__8_t9CC677DD20DFE89A30A2E5E4A99CF9EA59CAB3E3::get_offset_of_col_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7518 = { sizeof (ExtinguishableParticleSystem_tDF266922C9D4176B2247949390DFDB17BA233130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7518[2] = 
{
	ExtinguishableParticleSystem_tDF266922C9D4176B2247949390DFDB17BA233130::get_offset_of_multiplier_4(),
	ExtinguishableParticleSystem_tDF266922C9D4176B2247949390DFDB17BA233130::get_offset_of_m_Systems_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7519 = { sizeof (FireLight_t4C0EE4FFBB8F12021C447A53FA704029761639C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7519[3] = 
{
	FireLight_t4C0EE4FFBB8F12021C447A53FA704029761639C0::get_offset_of_m_Rnd_4(),
	FireLight_t4C0EE4FFBB8F12021C447A53FA704029761639C0::get_offset_of_m_Burning_5(),
	FireLight_t4C0EE4FFBB8F12021C447A53FA704029761639C0::get_offset_of_m_Light_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7520 = { sizeof (Hose_t27B33777B005A1CE8639E4CD14A7B33AB5966CB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7520[6] = 
{
	Hose_t27B33777B005A1CE8639E4CD14A7B33AB5966CB7::get_offset_of_maxPower_4(),
	Hose_t27B33777B005A1CE8639E4CD14A7B33AB5966CB7::get_offset_of_minPower_5(),
	Hose_t27B33777B005A1CE8639E4CD14A7B33AB5966CB7::get_offset_of_changeSpeed_6(),
	Hose_t27B33777B005A1CE8639E4CD14A7B33AB5966CB7::get_offset_of_hoseWaterSystems_7(),
	Hose_t27B33777B005A1CE8639E4CD14A7B33AB5966CB7::get_offset_of_systemRenderer_8(),
	Hose_t27B33777B005A1CE8639E4CD14A7B33AB5966CB7::get_offset_of_m_Power_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7521 = { sizeof (ParticleSystemMultiplier_t4B02B754B0A06C562C5B1B52DCD68D1BAEDCD5CE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7521[1] = 
{
	ParticleSystemMultiplier_t4B02B754B0A06C562C5B1B52DCD68D1BAEDCD5CE::get_offset_of_multiplier_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7522 = { sizeof (SmokeParticles_t8AF64C59BD42E84E783834AFDBDD84A262FCB28F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7522[1] = 
{
	SmokeParticles_t8AF64C59BD42E84E783834AFDBDD84A262FCB28F::get_offset_of_extinguishSounds_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7523 = { sizeof (WaterHoseParticles_tD058F19E51943F4EFA58450B7E3F7FCEB1902179), -1, sizeof(WaterHoseParticles_tD058F19E51943F4EFA58450B7E3F7FCEB1902179_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7523[4] = 
{
	WaterHoseParticles_tD058F19E51943F4EFA58450B7E3F7FCEB1902179_StaticFields::get_offset_of_lastSoundTime_4(),
	WaterHoseParticles_tD058F19E51943F4EFA58450B7E3F7FCEB1902179::get_offset_of_force_5(),
	WaterHoseParticles_tD058F19E51943F4EFA58450B7E3F7FCEB1902179::get_offset_of_m_CollisionEvents_6(),
	WaterHoseParticles_tD058F19E51943F4EFA58450B7E3F7FCEB1902179::get_offset_of_m_ParticleSystem_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7524 = { sizeof (AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7524[6] = 
{
	AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D::get_offset_of_axisName_4(),
	AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D::get_offset_of_axisValue_5(),
	AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D::get_offset_of_responseSpeed_6(),
	AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D::get_offset_of_returnToCentreSpeed_7(),
	AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D::get_offset_of_m_PairedWith_8(),
	AxisTouchButton_tD2C8F0EA8741A7896A784CEABB1B2434F080C33D::get_offset_of_m_Axis_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7525 = { sizeof (ButtonHandler_tF3AE341BBD2737A5A0E4F0736532CC2DA07F022B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7525[1] = 
{
	ButtonHandler_tF3AE341BBD2737A5A0E4F0736532CC2DA07F022B::get_offset_of_Name_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7526 = { sizeof (CrossPlatformInputManager_t13D200EB7A01CDDE2A4EADCDB5306E3F525EFE0B), -1, sizeof(CrossPlatformInputManager_t13D200EB7A01CDDE2A4EADCDB5306E3F525EFE0B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7526[3] = 
{
	CrossPlatformInputManager_t13D200EB7A01CDDE2A4EADCDB5306E3F525EFE0B_StaticFields::get_offset_of_activeInput_0(),
	CrossPlatformInputManager_t13D200EB7A01CDDE2A4EADCDB5306E3F525EFE0B_StaticFields::get_offset_of_s_TouchInput_1(),
	CrossPlatformInputManager_t13D200EB7A01CDDE2A4EADCDB5306E3F525EFE0B_StaticFields::get_offset_of_s_HardwareInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7527 = { sizeof (ActiveInputMethod_t85F98F3660FBD40776DF0060C53A612634AF8C35)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7527[3] = 
{
	ActiveInputMethod_t85F98F3660FBD40776DF0060C53A612634AF8C35::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7528 = { sizeof (VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7528[3] = 
{
	VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2::get_offset_of_m_Value_1(),
	VirtualAxis_tC5D5D2ECC42057E501B294C4032369D0AFEC50F2::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7529 = { sizeof (VirtualButton_tB612D966F284C10B236197C0B236E1C7DCBA9B91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7529[5] = 
{
	VirtualButton_tB612D966F284C10B236197C0B236E1C7DCBA9B91::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualButton_tB612D966F284C10B236197C0B236E1C7DCBA9B91::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1(),
	VirtualButton_tB612D966F284C10B236197C0B236E1C7DCBA9B91::get_offset_of_m_LastPressedFrame_2(),
	VirtualButton_tB612D966F284C10B236197C0B236E1C7DCBA9B91::get_offset_of_m_ReleasedFrame_3(),
	VirtualButton_tB612D966F284C10B236197C0B236E1C7DCBA9B91::get_offset_of_m_Pressed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7530 = { sizeof (InputAxisScrollbar_t22CF1CCDEDC91CD0B02149CB598719AB3313015E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7530[1] = 
{
	InputAxisScrollbar_t22CF1CCDEDC91CD0B02149CB598719AB3313015E::get_offset_of_axis_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7531 = { sizeof (Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7531[9] = 
{
	Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148::get_offset_of_MovementRange_4(),
	Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148::get_offset_of_axesToUse_5(),
	Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148::get_offset_of_horizontalAxisName_6(),
	Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148::get_offset_of_verticalAxisName_7(),
	Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148::get_offset_of_m_StartPos_8(),
	Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148::get_offset_of_m_UseX_9(),
	Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148::get_offset_of_m_UseY_10(),
	Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148::get_offset_of_m_HorizontalVirtualAxis_11(),
	Joystick_t5B6E095A016AA630E6D613AFFAF403C73601C148::get_offset_of_m_VerticalVirtualAxis_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7532 = { sizeof (AxisOption_tB37A2B637C3AD16E57FA96263DBEF759EAA95778)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7532[4] = 
{
	AxisOption_tB37A2B637C3AD16E57FA96263DBEF759EAA95778::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7533 = { sizeof (MobileControlRig_tA47FA373A5534CAB0AE37FEF5DB990FAC290A89B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7534 = { sizeof (TiltInput_tD86B151ED46EA7E5A6FEF6AF2C5C48E7014EF9F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7534[5] = 
{
	TiltInput_tD86B151ED46EA7E5A6FEF6AF2C5C48E7014EF9F2::get_offset_of_mapping_4(),
	TiltInput_tD86B151ED46EA7E5A6FEF6AF2C5C48E7014EF9F2::get_offset_of_tiltAroundAxis_5(),
	TiltInput_tD86B151ED46EA7E5A6FEF6AF2C5C48E7014EF9F2::get_offset_of_fullTiltAngle_6(),
	TiltInput_tD86B151ED46EA7E5A6FEF6AF2C5C48E7014EF9F2::get_offset_of_centreAngleOffset_7(),
	TiltInput_tD86B151ED46EA7E5A6FEF6AF2C5C48E7014EF9F2::get_offset_of_m_SteerAxis_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7535 = { sizeof (AxisOptions_t4D76D2E6C294064845318452078036E3A1538AD6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7535[3] = 
{
	AxisOptions_t4D76D2E6C294064845318452078036E3A1538AD6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7536 = { sizeof (AxisMapping_tD681DA8C80391B51E5F8AB14A8F9DD0B62F61ABD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7536[2] = 
{
	AxisMapping_tD681DA8C80391B51E5F8AB14A8F9DD0B62F61ABD::get_offset_of_type_0(),
	AxisMapping_tD681DA8C80391B51E5F8AB14A8F9DD0B62F61ABD::get_offset_of_axisName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7537 = { sizeof (MappingType_tEDB7A23D56E34B76DB3E0CB661EE732290967973)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7537[5] = 
{
	MappingType_tEDB7A23D56E34B76DB3E0CB661EE732290967973::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7538 = { sizeof (TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7538[18] = 
{
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_axesToUse_4(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_controlStyle_5(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_horizontalAxisName_6(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_verticalAxisName_7(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_Xsensitivity_8(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_Ysensitivity_9(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_m_StartPos_10(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_m_PreviousDelta_11(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_m_JoytickOutput_12(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_m_UseX_13(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_m_UseY_14(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_m_HorizontalVirtualAxis_15(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_m_VerticalVirtualAxis_16(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_m_Dragging_17(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_m_Id_18(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_m_PreviousTouchPos_19(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_m_Center_20(),
	TouchPad_tA8387389C0CA557CD9BF6A1D40C7DC6D4804E930::get_offset_of_m_Image_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7539 = { sizeof (AxisOption_t3B4AED7486BB5A566F38A787A8DCE08E9B08F6CC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7539[4] = 
{
	AxisOption_t3B4AED7486BB5A566F38A787A8DCE08E9B08F6CC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7540 = { sizeof (ControlStyle_t8B77B7EAA734BCFA3CB594B7E973839AA0CB5975)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7540[4] = 
{
	ControlStyle_t8B77B7EAA734BCFA3CB594B7E973839AA0CB5975::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7541 = { sizeof (VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7541[4] = 
{
	VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44::get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0(),
	VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44::get_offset_of_m_VirtualAxes_1(),
	VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44::get_offset_of_m_VirtualButtons_2(),
	VirtualInput_t139865CDFDEC27280DCAFD0A8116DCED6AB51C44::get_offset_of_m_AlwaysUseVirtual_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7542 = { sizeof (MobileInput_t049CC24BD98748DE8E06ADDB30005E0182C41A13), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7543 = { sizeof (StandaloneInput_t1FD6E11131D40042C4EBA9CFB3AAEA7695644A2A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7544 = { sizeof (SpawnInstruction_tB217FE64A2A52EC6DF45A5A8685389B46B56105F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7544[3] = 
{
	SpawnInstruction_tB217FE64A2A52EC6DF45A5A8685389B46B56105F::get_offset_of_agentConfiguration_0(),
	SpawnInstruction_tB217FE64A2A52EC6DF45A5A8685389B46B56105F::get_offset_of_delayToSpawn_1(),
	SpawnInstruction_tB217FE64A2A52EC6DF45A5A8685389B46B56105F::get_offset_of_startingNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7545 = { sizeof (TimedWave_tAE5CAECC23693939A6B523208893CB192FF36D8E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7545[2] = 
{
	TimedWave_tAE5CAECC23693939A6B523208893CB192FF36D8E::get_offset_of_timeToNextWave_9(),
	TimedWave_tAE5CAECC23693939A6B523208893CB192FF36D8E::get_offset_of_m_WaveTimer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7546 = { sizeof (Wave_t7EA46D35418196B830BC891624B786640BB0F586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7546[4] = 
{
	Wave_t7EA46D35418196B830BC891624B786640BB0F586::get_offset_of_spawnInstructions_5(),
	Wave_t7EA46D35418196B830BC891624B786640BB0F586::get_offset_of_m_CurrentIndex_6(),
	Wave_t7EA46D35418196B830BC891624B786640BB0F586::get_offset_of_m_SpawnTimer_7(),
	Wave_t7EA46D35418196B830BC891624B786640BB0F586::get_offset_of_waveCompleted_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7547 = { sizeof (WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7547[6] = 
{
	WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800::get_offset_of_m_CurrentIndex_5(),
	WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800::get_offset_of_DefaultStartingNode_6(),
	WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800::get_offset_of_startWavesOnAwake_7(),
	WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800::get_offset_of_waves_8(),
	WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800::get_offset_of_waveChanged_9(),
	WaveManager_tBD677215628B517D81FD26BD40FD7A55051AD800::get_offset_of_spawningCompleted_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7548 = { sizeof (HexPoint_tFDE14AF0E958CABA7D037DF3F087469BCF0265E4)+ sizeof (RuntimeObject), sizeof(HexPoint_tFDE14AF0E958CABA7D037DF3F087469BCF0265E4 ), 0, 0 };
extern const int32_t g_FieldOffsetTable7548[3] = 
{
	HexPoint_tFDE14AF0E958CABA7D037DF3F087469BCF0265E4::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HexPoint_tFDE14AF0E958CABA7D037DF3F087469BCF0265E4::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HexPoint_tFDE14AF0E958CABA7D037DF3F087469BCF0265E4::get_offset_of_z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7549 = { sizeof (IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9)+ sizeof (RuntimeObject), sizeof(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9 ), sizeof(IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7549[4] = 
{
	IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9_StaticFields::get_offset_of_one_0(),
	IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9_StaticFields::get_offset_of_zero_1(),
	IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9::get_offset_of_x_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntVector2_tB8D88944661CECEA9BC4360526A64507248EDFA9::get_offset_of_y_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7550 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7551 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7551[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7552 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7553 = { sizeof (GameObjectPool_t3C29EB821D23EC3ED655342B2218FFFCD437CF8C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7554 = { sizeof (AutoGameObjectPrefabPool_t229E733F0B10B86446DB1171D197E02B5B42FBEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7554[2] = 
{
	AutoGameObjectPrefabPool_t229E733F0B10B86446DB1171D197E02B5B42FBEE::get_offset_of_m_Prefab_4(),
	AutoGameObjectPrefabPool_t229E733F0B10B86446DB1171D197E02B5B42FBEE::get_offset_of_m_Initialize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7555 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7555[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7556 = { sizeof (Poolable_tBBCA15FD94FB330CD5A30C65692500ED9BA65D2F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7556[2] = 
{
	Poolable_tBBCA15FD94FB330CD5A30C65692500ED9BA65D2F::get_offset_of_initialPoolCapacity_4(),
	Poolable_tBBCA15FD94FB330CD5A30C65692500ED9BA65D2F::get_offset_of_pool_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7557 = { sizeof (PoolManager_tC4AA76A7B5FF355D068EEC877E2680E2DC9E771F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7557[2] = 
{
	PoolManager_tC4AA76A7B5FF355D068EEC877E2680E2DC9E771F::get_offset_of_poolables_5(),
	PoolManager_tC4AA76A7B5FF355D068EEC877E2680E2DC9E771F::get_offset_of_m_Pools_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7558 = { sizeof (RepeatingTimer_tFEABA0EAF35D64F27E55652FD50AF98799EDC468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7559 = { sizeof (SerializableInterface_tB2B751B1CAB73F4FE07575DB97D4A6BCA607F9A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7559[1] = 
{
	SerializableInterface_tB2B751B1CAB73F4FE07575DB97D4A6BCA607F9A7::get_offset_of_unityObjectReference_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7560 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7560[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7561 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7562 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7562[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7563 = { sizeof (TimedBehaviour_t4297CA025FC6BC8794A1151BC41DE0ACB076CCCA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7563[1] = 
{
	TimedBehaviour_t4297CA025FC6BC8794A1151BC41DE0ACB076CCCA::get_offset_of_m_ActiveTimers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7564 = { sizeof (Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7564[3] = 
{
	Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B::get_offset_of_m_Callback_0(),
	Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B::get_offset_of_m_Time_1(),
	Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B::get_offset_of_m_CurrentTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7565 = { sizeof (VectorHelper_t2C792229B88A0DEB52177324404C0BB495152786), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7566 = { sizeof (AnimatingMainMenuPage_tB189461182DB6C25EB964EA89022715D731EFDA0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7566[1] = 
{
	AnimatingMainMenuPage_tB189461182DB6C25EB964EA89022715D731EFDA0::get_offset_of_canvas_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7567 = { sizeof (BasicAnimatingMainMenuPage_t232167802C8CC8F286537A9ADA57BDB51B9BAE33), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7568 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7569 = { sizeof (MainMenu_tC48FC5C59167013D720749CAE27A82E55C2DEB42), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7569[2] = 
{
	MainMenu_tC48FC5C59167013D720749CAE27A82E55C2DEB42::get_offset_of_m_CurrentPage_4(),
	MainMenu_tC48FC5C59167013D720749CAE27A82E55C2DEB42::get_offset_of_m_PageStack_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7570 = { sizeof (Modal_tC8DF509CFEBAC4C21E59851EB18C17C4C74118A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7570[1] = 
{
	Modal_tC8DF509CFEBAC4C21E59851EB18C17C4C74118A1::get_offset_of_canvasGroup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7571 = { sizeof (SimpleMainMenuPage_tFE65ADBB869DBE12172A97EB12C2669E767098AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7571[1] = 
{
	SimpleMainMenuPage_tFE65ADBB869DBE12172A97EB12C2669E767098AC::get_offset_of_canvas_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7572 = { sizeof (CameraInputScheme_tEB16782DA7575673162607368A520FB02D2739E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7572[2] = 
{
	CameraInputScheme_tEB16782DA7575673162607368A520FB02D2739E5::get_offset_of_cameraRig_4(),
	CameraInputScheme_tEB16782DA7575673162607368A520FB02D2739E5::get_offset_of_nearZoomPanSpeedModifier_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7573 = { sizeof (InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7573[23] = 
{
	0,
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_dragThresholdTouch_6(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_dragThresholdMouse_7(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_tapTime_8(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_holdTime_9(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_mouseWheelSensitivity_10(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_trackMouseButtons_11(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_flickThreshold_12(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_m_Touches_13(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_m_MouseInfo_14(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_U3CmouseButtonPressedThisFrameU3Ek__BackingField_15(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_U3CmouseMovedOnThisFrameU3Ek__BackingField_16(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_U3CtouchPressedThisFrameU3Ek__BackingField_17(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_U3CbasicMouseInfoU3Ek__BackingField_18(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_pressed_19(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_released_20(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_tapped_21(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_startedDrag_22(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_dragged_23(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_startedHold_24(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_spunWheel_25(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_pinched_26(),
	InputController_t7EED1B6916BB59A90A8974D508B8480EA0B12B45::get_offset_of_mouseMoved_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7574 = { sizeof (U3CU3Ec__DisplayClass58_0_t18E011E4AF0EB1B5A829735E5210A320161F81E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7574[1] = 
{
	U3CU3Ec__DisplayClass58_0_t18E011E4AF0EB1B5A829735E5210A320161F81E4::get_offset_of_touch_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7575 = { sizeof (InputScheme_t78F140C9CD80326F0D459CE65156EF66E3218678), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7576 = { sizeof (InputSchemeSwitcher_t0B26D609BCBC926A38D267CD7B1E22394E809270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7576[3] = 
{
	InputSchemeSwitcher_t0B26D609BCBC926A38D267CD7B1E22394E809270::get_offset_of_m_InputSchemes_4(),
	InputSchemeSwitcher_t0B26D609BCBC926A38D267CD7B1E22394E809270::get_offset_of_m_DefaultScheme_5(),
	InputSchemeSwitcher_t0B26D609BCBC926A38D267CD7B1E22394E809270::get_offset_of_m_CurrentScheme_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7577 = { sizeof (KeyboardMouseInput_t695D0A615AD59B9BBB1C8FBA24F5236117D9813E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7577[3] = 
{
	KeyboardMouseInput_t695D0A615AD59B9BBB1C8FBA24F5236117D9813E::get_offset_of_screenPanThreshold_6(),
	KeyboardMouseInput_t695D0A615AD59B9BBB1C8FBA24F5236117D9813E::get_offset_of_mouseEdgePanSpeed_7(),
	KeyboardMouseInput_t695D0A615AD59B9BBB1C8FBA24F5236117D9813E::get_offset_of_mouseRmbPanSpeed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7578 = { sizeof (MouseButtonInfo_t4FD58E460FD4C794A9F215BD79301752CBF01C0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7578[2] = 
{
	MouseButtonInfo_t4FD58E460FD4C794A9F215BD79301752CBF01C0A::get_offset_of_isDown_11(),
	MouseButtonInfo_t4FD58E460FD4C794A9F215BD79301752CBF01C0A::get_offset_of_mouseButtonId_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7579 = { sizeof (MouseCursorInfo_tEBCC3A46B40FF3535F41C88F909902DCB99C0A09), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7580 = { sizeof (PinchInfo_t094B9BE005110F688D0F3E91452E7710B4BFFDB9)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7580[2] = 
{
	PinchInfo_t094B9BE005110F688D0F3E91452E7710B4BFFDB9::get_offset_of_touch1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PinchInfo_t094B9BE005110F688D0F3E91452E7710B4BFFDB9::get_offset_of_touch2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7581 = { sizeof (PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7581[7] = 
{
	PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8::get_offset_of_startPosition_4(),
	PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8::get_offset_of_flickVelocity_5(),
	PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8::get_offset_of_totalMovement_6(),
	PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8::get_offset_of_startTime_7(),
	PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8::get_offset_of_isDrag_8(),
	PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8::get_offset_of_isHold_9(),
	PointerActionInfo_t2D1926AA141A238E54D0FBB7B01A249E1FF6F2F8::get_offset_of_wasHold_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7582 = { sizeof (PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7582[4] = 
{
	PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB::get_offset_of_currentPosition_0(),
	PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB::get_offset_of_previousPosition_1(),
	PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB::get_offset_of_delta_2(),
	PointerInfo_t9C25C1FF22F2B024B8CFC94EEED70B2BE31CF1DB::get_offset_of_startedOverUI_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7583 = { sizeof (TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7583[1] = 
{
	TouchInfo_t134CF8ECAE6843279CC8FDA89E8488650636FDA2::get_offset_of_touchId_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7584 = { sizeof (TouchInput_tC9037DFA662201899E9B004912AEAA29E81F4F60), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7584[3] = 
{
	TouchInput_tC9037DFA662201899E9B004912AEAA29E81F4F60::get_offset_of_panSpeed_6(),
	TouchInput_tC9037DFA662201899E9B004912AEAA29E81F4F60::get_offset_of_flickDecayFactor_7(),
	TouchInput_tC9037DFA662201899E9B004912AEAA29E81F4F60::get_offset_of_m_FlickDirection_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7585 = { sizeof (WheelInfo_t4290340AD427B74E506E228DC368CEEE742D6F0A)+ sizeof (RuntimeObject), sizeof(WheelInfo_t4290340AD427B74E506E228DC368CEEE742D6F0A ), 0, 0 };
extern const int32_t g_FieldOffsetTable7585[1] = 
{
	WheelInfo_t4290340AD427B74E506E228DC368CEEE742D6F0A::get_offset_of_zoomAmount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7586 = { sizeof (Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7586[9] = 
{
	Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A::get_offset_of_maxHealth_0(),
	Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A::get_offset_of_startingHealth_1(),
	Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A::get_offset_of_alignment_2(),
	Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A::get_offset_of_U3CcurrentHealthU3Ek__BackingField_3(),
	Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A::get_offset_of_reachedMaxHealth_4(),
	Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A::get_offset_of_damaged_5(),
	Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A::get_offset_of_healed_6(),
	Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A::get_offset_of_died_7(),
	Damageable_t352EB38C33DFADA9737BAA68D89A675CEA2BF23A::get_offset_of_healthChanged_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7587 = { sizeof (DamageableBehaviour_tA04944F5DEB3599E963255A91160F9074060329F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7587[4] = 
{
	DamageableBehaviour_tA04944F5DEB3599E963255A91160F9074060329F::get_offset_of_configuration_4(),
	DamageableBehaviour_tA04944F5DEB3599E963255A91160F9074060329F::get_offset_of_hit_5(),
	DamageableBehaviour_tA04944F5DEB3599E963255A91160F9074060329F::get_offset_of_removed_6(),
	DamageableBehaviour_tA04944F5DEB3599E963255A91160F9074060329F::get_offset_of_died_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7588 = { sizeof (HealthChangeEvent_t7A0991C3BA39B6630C4A93CD11890F01767308BE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7589 = { sizeof (HitEvent_tA814F77C3A27C8D4A9A6EBE40269D28D13288921), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7590 = { sizeof (DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7590[7] = 
{
	DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6::get_offset_of_damageableBehaviour_4(),
	DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6::get_offset_of_damaged_5(),
	DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6::get_offset_of_healed_6(),
	DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6::get_offset_of_died_7(),
	DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6::get_offset_of_reachedMaxHealth_8(),
	DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6::get_offset_of_healthChanged_9(),
	DamageableListener_t858DEFF680028F0BF08F1CC84A50058A555EABC6::get_offset_of_hit_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7591 = { sizeof (HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7591[4] = 
{
	HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7::get_offset_of_damageable_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7::get_offset_of_oldHealth_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7::get_offset_of_newHealth_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HealthChangeInfo_tA440FC3535F0441F2500BBE0099909FB97DA21D7::get_offset_of_damageAlignment_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7592 = { sizeof (HitInfo_t290C7732A0A53F1933D0D7A416B01FFB79185D2B)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7592[2] = 
{
	HitInfo_t290C7732A0A53F1933D0D7A416B01FFB79185D2B::get_offset_of_m_HealthChangeInfo_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HitInfo_t290C7732A0A53F1933D0D7A416B01FFB79185D2B::get_offset_of_m_DamagePoint_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7593 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7594 = { sizeof (SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7595 = { sizeof (SimpleAlignment_t5D6492537FFFDB3F1989DEE50A3F598560931296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7595[1] = 
{
	SimpleAlignment_t5D6492537FFFDB3F1989DEE50A3F598560931296::get_offset_of_opponents_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7596 = { sizeof (LevelItem_tBE03F0DC7DCC0F0F4844E719363782132F9BCDC2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7596[4] = 
{
	LevelItem_tBE03F0DC7DCC0F0F4844E719363782132F9BCDC2::get_offset_of_id_0(),
	LevelItem_tBE03F0DC7DCC0F0F4844E719363782132F9BCDC2::get_offset_of_name_1(),
	LevelItem_tBE03F0DC7DCC0F0F4844E719363782132F9BCDC2::get_offset_of_description_2(),
	LevelItem_tBE03F0DC7DCC0F0F4844E719363782132F9BCDC2::get_offset_of_sceneName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7597 = { sizeof (LevelList_tAB7E8C7790A45E96D59254664D7C1B38406882E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7597[2] = 
{
	LevelList_tAB7E8C7790A45E96D59254664D7C1B38406882E7::get_offset_of_levels_4(),
	LevelList_tAB7E8C7790A45E96D59254664D7C1B38406882E7::get_offset_of_m_LevelDictionary_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7598 = { sizeof (U3CU3Ec_t1A65C2DF9B3F8829C11776D23B8FC1964D2C5862), -1, sizeof(U3CU3Ec_t1A65C2DF9B3F8829C11776D23B8FC1964D2C5862_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7598[2] = 
{
	U3CU3Ec_t1A65C2DF9B3F8829C11776D23B8FC1964D2C5862_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1A65C2DF9B3F8829C11776D23B8FC1964D2C5862_StaticFields::get_offset_of_U3CU3E9__18_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7599 = { sizeof (GameObjectExtensions_t562EC0C76CE41B4A7BFE03B524AA44C9B4389001), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
