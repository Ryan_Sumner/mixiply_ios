﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Newtonsoft.Json.Bson.BsonBinaryWriter
struct BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2;
// Newtonsoft.Json.Bson.BsonString
struct BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46;
// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973;
// Newtonsoft.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84;
// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>
struct Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B;
// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>
struct Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466;
// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>
struct Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Func`2<UnityEngine.Experimental.XR.XRCameraSubsystem,System.Boolean>>
struct Dictionary_2_t332B381ADD42E7894216EE28B27D48019ED7637B;
// System.Collections.Generic.Dictionary`2<System.String,System.Func`2<UnityEngine.Experimental.XR.XRCameraSubsystem,System.IntPtr>>
struct Dictionary_2_t707989CBAF49C089295D286B11265A8C639A7730;
// System.Collections.Generic.Dictionary`2<System.String,System.Func`2<UnityEngine.Experimental.XR.XRSessionSubsystem,System.IntPtr>>
struct Dictionary_2_tAD560473AB7821B978587ED1305391B72E86B1B7;
// System.Collections.Generic.Dictionary`2<System.String,System.Func`3<UnityEngine.Experimental.XR.XRCameraSubsystem,UnityEngine.XR.ARExtensions.CameraFocusMode,System.Boolean>>
struct Dictionary_2_t03FC742469A2B251FF3680C63D1C74E74C66DECF;
// System.Collections.Generic.Dictionary`2<System.String,System.Func`3<UnityEngine.Experimental.XR.XRPlaneSubsystem,UnityEngine.Experimental.XR.TrackableId,System.IntPtr>>
struct Dictionary_2_t2706E85D313DEF390F17DB6874CDDCEEE2DCB409;
// System.Collections.Generic.Dictionary`2<System.String,System.Func`3<UnityEngine.Experimental.XR.XRPlaneSubsystem,UnityEngine.Experimental.XR.TrackableId,UnityEngine.Experimental.XR.TrackingState>>
struct Dictionary_2_t4A9D89D64890F68FBC43DE2C012EE1C637F5C467;
// System.Collections.Generic.Dictionary`2<System.String,System.Func`3<UnityEngine.Experimental.XR.XRPlaneSubsystem,UnityEngine.XR.ARExtensions.PlaneDetectionFlags,System.Boolean>>
struct Dictionary_2_tE04FB6FCD228835F326ECDF3C5A21B523796BEB6;
// System.Collections.Generic.Dictionary`2<System.String,System.Func`3<UnityEngine.Experimental.XR.XRReferencePointSubsystem,UnityEngine.Experimental.XR.TrackableId,System.IntPtr>>
struct Dictionary_2_tD67186A32F638C55B3D7E5D00AA532859F8B871D;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.ARExtensions.ICameraConfigApi>
struct Dictionary_2_tECE8019B660D66E6B522360B0BF8F94254B823FF;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.ARExtensions.ICameraImageApi>
struct Dictionary_2_t6058911EEE43591076AB7625C4B87296D2B2E94F;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.ARExtensions.XRCameraExtensions/TryGetColorCorrectionDelegate>
struct Dictionary_2_tDBB540258E01DD19F5B248555ADFB91E451BADF0;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.ARExtensions.XRReferencePointExtensions/AttachReferencePointDelegate>
struct Dictionary_2_tF65291F5C17BE4538B0A42E1C7369F27621D2554;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.ARExtensions.XRSessionExtensions/AsyncDelegate`1<UnityEngine.XR.ARExtensions.SessionAvailability>>
struct Dictionary_2_tCA4D6C75E791133AABE5613D1D1A6A554382A344;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.ARExtensions.XRSessionExtensions/AsyncDelegate`1<UnityEngine.XR.ARExtensions.SessionInstallationStatus>>
struct Dictionary_2_t5440C9C92669AAC2A8D32C4BC41523E53DE6A49F;
// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonProperty>
struct List_1_t23B22538D2FA378DA6D9B5BD5B84B060381E4748;
// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonToken>
struct List_1_t8966010566181DE22880EF5DF9C0CB53B4614363;
// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode>
struct List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3;
// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>
struct List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.SpatialTracking.TrackedPoseDriver/TrackedPose>
struct List_1_t894CB1A35FB018A041CA1B49CF3AB136E531995A;
// System.Collections.Generic.List`1<UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData>
struct List_1_t634EA217FAFE0009FF221F59F7952CBEA023049B;
// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>
struct List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A;
// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>
struct List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483;
// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>
struct List_1_tE2C2476479F1A844897C46751D09491B2534EC87;
// System.Collections.Generic.List`1<UnityEngine.XR.XRNodeState>
struct List_1_tDECBF737A96DF978685F6386C44B9284190E43C7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`2<UnityEngine.Experimental.XR.XRCameraSubsystem,System.Boolean>
struct Func_2_t8DD4B98590E9E9D33E80BE6D41F3B325D5F48D78;
// System.Func`2<UnityEngine.Experimental.XR.XRCameraSubsystem,System.IntPtr>
struct Func_2_tD21C0AD1E10C99C0FC7A84082344E25F086499CD;
// System.Func`2<UnityEngine.Experimental.XR.XRSessionSubsystem,System.IntPtr>
struct Func_2_t9B4A2794A15F05B6A5AA4A59CE29B1D3CA8081A7;
// System.Func`3<UnityEngine.Experimental.XR.XRCameraSubsystem,UnityEngine.XR.ARExtensions.CameraFocusMode,System.Boolean>
struct Func_3_t8142E55EC06EB0185AD099830F125CA5E187DDD8;
// System.Func`3<UnityEngine.Experimental.XR.XRPlaneSubsystem,UnityEngine.Experimental.XR.TrackableId,System.IntPtr>
struct Func_3_tDF578AEA147B81628BA611F816D57E306CA287C4;
// System.Func`3<UnityEngine.Experimental.XR.XRPlaneSubsystem,UnityEngine.Experimental.XR.TrackableId,UnityEngine.Experimental.XR.TrackingState>
struct Func_3_tADB96F4F6D4804C360DF07DE8146E2B4631B36B9;
// System.Func`3<UnityEngine.Experimental.XR.XRPlaneSubsystem,UnityEngine.XR.ARExtensions.PlaneDetectionFlags,System.Boolean>
struct Func_3_t1EF1CCE7ECB8A978928DF7EEDA4028100901CB48;
// System.Func`3<UnityEngine.Experimental.XR.XRReferencePointSubsystem,UnityEngine.Experimental.XR.TrackableId,System.IntPtr>
struct Func_3_tE51207E7A242422D97436D398CA19D150FF94581;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.BinaryWriter
struct BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.Xml.Linq.XDeclaration
struct XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512;
// System.Xml.Linq.XDocumentType
struct XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444;
// System.Xml.Linq.XObject
struct XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF;
// System.Xml.XmlDocumentType
struct XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136;
// System.Xml.XmlNode
struct XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB;
// UnityEngine.Experimental.ISubsystemDescriptor
struct ISubsystemDescriptor_tDF5EB3ED639A15690D2CB9993789BB21F24D3934;
// UnityEngine.Experimental.XR.Interaction.BasePoseProvider
struct BasePoseProvider_t951B9DB48DBBA7336FFE70B77529EEF42742B50B;
// UnityEngine.Experimental.XR.XRCameraSubsystem
struct XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701;
// UnityEngine.Experimental.XR.XRReferencePointSubsystem
struct XRReferencePointSubsystem_t9D4A49A2B6580143EF25399812034F001A18D00C;
// UnityEngine.XR.ARExtensions.DefaultCameraConfigApi
struct DefaultCameraConfigApi_t125599F6D1891BEF834E1B20C71C503AB27F4091;
// UnityEngine.XR.ARExtensions.DefaultCameraImageApi
struct DefaultCameraImageApi_t32807EEA82DF3B067158E1DCEA117A577B5E3BCB;
// UnityEngine.XR.ARExtensions.ICameraConfigApi
struct ICameraConfigApi_t5694F28C505D2CA91EF77838138F419261B240BA;
// UnityEngine.XR.ARExtensions.ICameraImageApi
struct ICameraImageApi_tBAC003481E172628D623F3F9E7CFB8D2FD84EEE4;
// UnityEngine.XR.ARExtensions.XRCameraExtensions/OnImageRequestCompleteDelegate
struct OnImageRequestCompleteDelegate_tC602138547507C797A3BE8CC2D7022659FA457B2;
// UnityEngine.XR.ARExtensions.XRCameraExtensions/TryGetColorCorrectionDelegate
struct TryGetColorCorrectionDelegate_t30B930C66D1DCEC1490F1A6624404BA665652BD8;
// UnityEngine.XR.ARExtensions.XRReferencePointExtensions/AttachReferencePointDelegate
struct AttachReferencePointDelegate_t8E14528144FB8B3DE6B44FCD01B3B4C2090236DF;
// UnityEngine.XR.ARExtensions.XRSessionExtensions/AsyncDelegate`1<UnityEngine.XR.ARExtensions.SessionAvailability>
struct AsyncDelegate_1_tC1209225BB9B8AF0FB9384EA8D49347BB4BCC659;
// UnityEngine.XR.ARExtensions.XRSessionExtensions/AsyncDelegate`1<UnityEngine.XR.ARExtensions.SessionInstallationStatus>
struct AsyncDelegate_1_t249767B2886936DE438787AE5D7C959A26DE03C3;
// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem
struct XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86;




#ifndef U3CMODULEU3E_T9235715C8404A6E9A7653F6E7FFDB5E36D9C2D95_H
#define U3CMODULEU3E_T9235715C8404A6E9A7653F6E7FFDB5E36D9C2D95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t9235715C8404A6E9A7653F6E7FFDB5E36D9C2D95 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T9235715C8404A6E9A7653F6E7FFDB5E36D9C2D95_H
#ifndef U3CMODULEU3E_TD7C26C6BCAC76F130F59027F1362DDBCC127637D_H
#define U3CMODULEU3E_TD7C26C6BCAC76F130F59027F1362DDBCC127637D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tD7C26C6BCAC76F130F59027F1362DDBCC127637D 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TD7C26C6BCAC76F130F59027F1362DDBCC127637D_H
#ifndef U3CMODULEU3E_T61085D0352A59B7105C361DB16CFAD5E8986AE2E_H
#define U3CMODULEU3E_T61085D0352A59B7105C361DB16CFAD5E8986AE2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t61085D0352A59B7105C361DB16CFAD5E8986AE2E 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T61085D0352A59B7105C361DB16CFAD5E8986AE2E_H
#ifndef U3CMODULEU3E_T76DD45B11E728799BA16B6E93B81827DD86E5AEE_H
#define U3CMODULEU3E_T76DD45B11E728799BA16B6E93B81827DD86E5AEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t76DD45B11E728799BA16B6E93B81827DD86E5AEE 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T76DD45B11E728799BA16B6E93B81827DD86E5AEE_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BSONOBJECTID_TACBE83AF96B5BCA4A847AD22C826939C821DD2D7_H
#define BSONOBJECTID_TACBE83AF96B5BCA4A847AD22C826939C821DD2D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonObjectId
struct  BsonObjectId_tACBE83AF96B5BCA4A847AD22C826939C821DD2D7  : public RuntimeObject
{
public:
	// System.Byte[] Newtonsoft.Json.Bson.BsonObjectId::<Value>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CValueU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonObjectId_tACBE83AF96B5BCA4A847AD22C826939C821DD2D7, ___U3CValueU3Ek__BackingField_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECTID_TACBE83AF96B5BCA4A847AD22C826939C821DD2D7_H
#ifndef BSONPROPERTY_T4D924C12981551812A0C17ACA98F3D5C0C3C9ECC_H
#define BSONPROPERTY_T4D924C12981551812A0C17ACA98F3D5C0C3C9ECC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonProperty
struct  BsonProperty_t4D924C12981551812A0C17ACA98F3D5C0C3C9ECC  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonProperty::<Name>k__BackingField
	BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46 * ___U3CNameU3Ek__BackingField_0;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonProperty::<Value>k__BackingField
	BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 * ___U3CValueU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonProperty_t4D924C12981551812A0C17ACA98F3D5C0C3C9ECC, ___U3CNameU3Ek__BackingField_0)); }
	inline BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46 * get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46 ** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46 * value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BsonProperty_t4D924C12981551812A0C17ACA98F3D5C0C3C9ECC, ___U3CValueU3Ek__BackingField_1)); }
	inline BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 * get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 ** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 * value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONPROPERTY_T4D924C12981551812A0C17ACA98F3D5C0C3C9ECC_H
#ifndef BSONTOKEN_T3703970749E7450A6A4AA0DBF8DFDE514F518973_H
#define BSONTOKEN_T3703970749E7450A6A4AA0DBF8DFDE514F518973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonToken
struct  BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonToken::<Parent>k__BackingField
	BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 * ___U3CParentU3Ek__BackingField_0;
	// System.Int32 Newtonsoft.Json.Bson.BsonToken::<CalculatedSize>k__BackingField
	int32_t ___U3CCalculatedSizeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CParentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973, ___U3CParentU3Ek__BackingField_0)); }
	inline BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 * get_U3CParentU3Ek__BackingField_0() const { return ___U3CParentU3Ek__BackingField_0; }
	inline BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 ** get_address_of_U3CParentU3Ek__BackingField_0() { return &___U3CParentU3Ek__BackingField_0; }
	inline void set_U3CParentU3Ek__BackingField_0(BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 * value)
	{
		___U3CParentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCalculatedSizeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973, ___U3CCalculatedSizeU3Ek__BackingField_1)); }
	inline int32_t get_U3CCalculatedSizeU3Ek__BackingField_1() const { return ___U3CCalculatedSizeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCalculatedSizeU3Ek__BackingField_1() { return &___U3CCalculatedSizeU3Ek__BackingField_1; }
	inline void set_U3CCalculatedSizeU3Ek__BackingField_1(int32_t value)
	{
		___U3CCalculatedSizeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONTOKEN_T3703970749E7450A6A4AA0DBF8DFDE514F518973_H
#ifndef XOBJECTWRAPPER_T725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A_H
#define XOBJECTWRAPPER_T725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XObjectWrapper
struct  XObjectWrapper_t725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A  : public RuntimeObject
{
public:
	// System.Xml.Linq.XObject Newtonsoft.Json.Converters.XObjectWrapper::_xmlObject
	XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF * ____xmlObject_1;

public:
	inline static int32_t get_offset_of__xmlObject_1() { return static_cast<int32_t>(offsetof(XObjectWrapper_t725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A, ____xmlObject_1)); }
	inline XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF * get__xmlObject_1() const { return ____xmlObject_1; }
	inline XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF ** get_address_of__xmlObject_1() { return &____xmlObject_1; }
	inline void set__xmlObject_1(XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF * value)
	{
		____xmlObject_1 = value;
		Il2CppCodeGenWriteBarrier((&____xmlObject_1), value);
	}
};

struct XObjectWrapper_t725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A_StaticFields
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XObjectWrapper::EmptyChildNodes
	List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * ___EmptyChildNodes_0;

public:
	inline static int32_t get_offset_of_EmptyChildNodes_0() { return static_cast<int32_t>(offsetof(XObjectWrapper_t725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A_StaticFields, ___EmptyChildNodes_0)); }
	inline List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * get_EmptyChildNodes_0() const { return ___EmptyChildNodes_0; }
	inline List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 ** get_address_of_EmptyChildNodes_0() { return &___EmptyChildNodes_0; }
	inline void set_EmptyChildNodes_0(List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * value)
	{
		___EmptyChildNodes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyChildNodes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XOBJECTWRAPPER_T725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A_H
#ifndef XMLNODEWRAPPER_TADE3A83C456747510EF6C4046E6689838DD070F8_H
#define XMLNODEWRAPPER_TADE3A83C456747510EF6C4046E6689838DD070F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlNodeWrapper
struct  XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8  : public RuntimeObject
{
public:
	// System.Xml.XmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::_node
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ____node_0;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::_childNodes
	List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * ____childNodes_1;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::_attributes
	List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * ____attributes_2;

public:
	inline static int32_t get_offset_of__node_0() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8, ____node_0)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get__node_0() const { return ____node_0; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of__node_0() { return &____node_0; }
	inline void set__node_0(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		____node_0 = value;
		Il2CppCodeGenWriteBarrier((&____node_0), value);
	}

	inline static int32_t get_offset_of__childNodes_1() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8, ____childNodes_1)); }
	inline List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * get__childNodes_1() const { return ____childNodes_1; }
	inline List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 ** get_address_of__childNodes_1() { return &____childNodes_1; }
	inline void set__childNodes_1(List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * value)
	{
		____childNodes_1 = value;
		Il2CppCodeGenWriteBarrier((&____childNodes_1), value);
	}

	inline static int32_t get_offset_of__attributes_2() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8, ____attributes_2)); }
	inline List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * get__attributes_2() const { return ____attributes_2; }
	inline List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 ** get_address_of__attributes_2() { return &____attributes_2; }
	inline void set__attributes_2(List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * value)
	{
		____attributes_2 = value;
		Il2CppCodeGenWriteBarrier((&____attributes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEWRAPPER_TADE3A83C456747510EF6C4046E6689838DD070F8_H
#ifndef JSONCONVERTER_T1CC2EC16B3208095A97A3CAE15286A5E16C3DD59_H
#define JSONCONVERTER_T1CC2EC16B3208095A97A3CAE15286A5E16C3DD59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverter
struct  JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTER_T1CC2EC16B3208095A97A3CAE15286A5E16C3DD59_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef SUBSYSTEM_T9D40133A9FEB984E2C3695B077DA4DDFC91CA181_H
#define SUBSYSTEM_T9D40133A9FEB984E2C3695B077DA4DDFC91CA181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Subsystem
struct  Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181  : public RuntimeObject
{
public:
	// UnityEngine.Experimental.ISubsystemDescriptor UnityEngine.Experimental.Subsystem::m_subsystemDescriptor
	RuntimeObject* ___m_subsystemDescriptor_0;

public:
	inline static int32_t get_offset_of_m_subsystemDescriptor_0() { return static_cast<int32_t>(offsetof(Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181, ___m_subsystemDescriptor_0)); }
	inline RuntimeObject* get_m_subsystemDescriptor_0() const { return ___m_subsystemDescriptor_0; }
	inline RuntimeObject** get_address_of_m_subsystemDescriptor_0() { return &___m_subsystemDescriptor_0; }
	inline void set_m_subsystemDescriptor_0(RuntimeObject* value)
	{
		___m_subsystemDescriptor_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_subsystemDescriptor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEM_T9D40133A9FEB984E2C3695B077DA4DDFC91CA181_H
#ifndef SUBSYSTEMDESCRIPTOR_T39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4_H
#define SUBSYSTEMDESCRIPTOR_T39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.SubsystemDescriptor
struct  SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4  : public RuntimeObject
{
public:
	// System.String UnityEngine.Experimental.SubsystemDescriptor::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// System.Type UnityEngine.Experimental.SubsystemDescriptor::<subsystemImplementationType>k__BackingField
	Type_t * ___U3CsubsystemImplementationTypeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsubsystemImplementationTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4, ___U3CsubsystemImplementationTypeU3Ek__BackingField_1)); }
	inline Type_t * get_U3CsubsystemImplementationTypeU3Ek__BackingField_1() const { return ___U3CsubsystemImplementationTypeU3Ek__BackingField_1; }
	inline Type_t ** get_address_of_U3CsubsystemImplementationTypeU3Ek__BackingField_1() { return &___U3CsubsystemImplementationTypeU3Ek__BackingField_1; }
	inline void set_U3CsubsystemImplementationTypeU3Ek__BackingField_1(Type_t * value)
	{
		___U3CsubsystemImplementationTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsubsystemImplementationTypeU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEMDESCRIPTOR_T39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4_H
#ifndef POSEDATASOURCE_T922E6B2F7058F8A78172B8E29AEB0BBD748B4657_H
#define POSEDATASOURCE_T922E6B2F7058F8A78172B8E29AEB0BBD748B4657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpatialTracking.PoseDataSource
struct  PoseDataSource_t922E6B2F7058F8A78172B8E29AEB0BBD748B4657  : public RuntimeObject
{
public:

public:
};

struct PoseDataSource_t922E6B2F7058F8A78172B8E29AEB0BBD748B4657_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.XRNodeState> UnityEngine.SpatialTracking.PoseDataSource::nodeStates
	List_1_tDECBF737A96DF978685F6386C44B9284190E43C7 * ___nodeStates_0;

public:
	inline static int32_t get_offset_of_nodeStates_0() { return static_cast<int32_t>(offsetof(PoseDataSource_t922E6B2F7058F8A78172B8E29AEB0BBD748B4657_StaticFields, ___nodeStates_0)); }
	inline List_1_tDECBF737A96DF978685F6386C44B9284190E43C7 * get_nodeStates_0() const { return ___nodeStates_0; }
	inline List_1_tDECBF737A96DF978685F6386C44B9284190E43C7 ** get_address_of_nodeStates_0() { return &___nodeStates_0; }
	inline void set_nodeStates_0(List_1_tDECBF737A96DF978685F6386C44B9284190E43C7 * value)
	{
		___nodeStates_0 = value;
		Il2CppCodeGenWriteBarrier((&___nodeStates_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSEDATASOURCE_T922E6B2F7058F8A78172B8E29AEB0BBD748B4657_H
#ifndef TRACKEDPOSEDRIVERDATADESCRIPTION_TEA05A7320AED44B790D678CF1B65B48138DA1ECA_H
#define TRACKEDPOSEDRIVERDATADESCRIPTION_TEA05A7320AED44B790D678CF1B65B48138DA1ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription
struct  TrackedPoseDriverDataDescription_tEA05A7320AED44B790D678CF1B65B48138DA1ECA  : public RuntimeObject
{
public:

public:
};

struct TrackedPoseDriverDataDescription_tEA05A7320AED44B790D678CF1B65B48138DA1ECA_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData> UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription::DeviceData
	List_1_t634EA217FAFE0009FF221F59F7952CBEA023049B * ___DeviceData_0;

public:
	inline static int32_t get_offset_of_DeviceData_0() { return static_cast<int32_t>(offsetof(TrackedPoseDriverDataDescription_tEA05A7320AED44B790D678CF1B65B48138DA1ECA_StaticFields, ___DeviceData_0)); }
	inline List_1_t634EA217FAFE0009FF221F59F7952CBEA023049B * get_DeviceData_0() const { return ___DeviceData_0; }
	inline List_1_t634EA217FAFE0009FF221F59F7952CBEA023049B ** get_address_of_DeviceData_0() { return &___DeviceData_0; }
	inline void set_DeviceData_0(List_1_t634EA217FAFE0009FF221F59F7952CBEA023049B * value)
	{
		___DeviceData_0 = value;
		Il2CppCodeGenWriteBarrier((&___DeviceData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKEDPOSEDRIVERDATADESCRIPTION_TEA05A7320AED44B790D678CF1B65B48138DA1ECA_H
#ifndef DEFAULTCAMERACONFIGAPI_T125599F6D1891BEF834E1B20C71C503AB27F4091_H
#define DEFAULTCAMERACONFIGAPI_T125599F6D1891BEF834E1B20C71C503AB27F4091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.DefaultCameraConfigApi
struct  DefaultCameraConfigApi_t125599F6D1891BEF834E1B20C71C503AB27F4091  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCAMERACONFIGAPI_T125599F6D1891BEF834E1B20C71C503AB27F4091_H
#ifndef DEFAULTCAMERAIMAGEAPI_T32807EEA82DF3B067158E1DCEA117A577B5E3BCB_H
#define DEFAULTCAMERAIMAGEAPI_T32807EEA82DF3B067158E1DCEA117A577B5E3BCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.DefaultCameraImageApi
struct  DefaultCameraImageApi_t32807EEA82DF3B067158E1DCEA117A577B5E3BCB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCAMERAIMAGEAPI_T32807EEA82DF3B067158E1DCEA117A577B5E3BCB_H
#ifndef REGISTRATIONHELPER_T0925D2AC89FBDCEB2FA94AEDADCBA81670F5B531_H
#define REGISTRATIONHELPER_T0925D2AC89FBDCEB2FA94AEDADCBA81670F5B531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.RegistrationHelper
struct  RegistrationHelper_t0925D2AC89FBDCEB2FA94AEDADCBA81670F5B531  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGISTRATIONHELPER_T0925D2AC89FBDCEB2FA94AEDADCBA81670F5B531_H
#ifndef SESSIONAVAILABILITYEXTENSIONS_TE9EC580D14C93F2A5732599A5A7F0435EC737929_H
#define SESSIONAVAILABILITYEXTENSIONS_TE9EC580D14C93F2A5732599A5A7F0435EC737929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.SessionAvailabilityExtensions
struct  SessionAvailabilityExtensions_tE9EC580D14C93F2A5732599A5A7F0435EC737929  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONAVAILABILITYEXTENSIONS_TE9EC580D14C93F2A5732599A5A7F0435EC737929_H
#ifndef XRCAMERAEXTENSIONS_T9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_H
#define XRCAMERAEXTENSIONS_T9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.XRCameraExtensions
struct  XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D  : public RuntimeObject
{
public:

public:
};

struct XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields
{
public:
	// System.Func`2<UnityEngine.Experimental.XR.XRCameraSubsystem,System.Boolean> UnityEngine.XR.ARExtensions.XRCameraExtensions::s_IsPermissionGrantedDelegate
	Func_2_t8DD4B98590E9E9D33E80BE6D41F3B325D5F48D78 * ___s_IsPermissionGrantedDelegate_0;
	// UnityEngine.XR.ARExtensions.XRCameraExtensions/TryGetColorCorrectionDelegate UnityEngine.XR.ARExtensions.XRCameraExtensions::s_TryGetColorCorrectionDelegate
	TryGetColorCorrectionDelegate_t30B930C66D1DCEC1490F1A6624404BA665652BD8 * ___s_TryGetColorCorrectionDelegate_1;
	// System.Func`2<UnityEngine.Experimental.XR.XRCameraSubsystem,System.IntPtr> UnityEngine.XR.ARExtensions.XRCameraExtensions::s_GetNativePtrDelegate
	Func_2_tD21C0AD1E10C99C0FC7A84082344E25F086499CD * ___s_GetNativePtrDelegate_2;
	// UnityEngine.XR.ARExtensions.ICameraImageApi UnityEngine.XR.ARExtensions.XRCameraExtensions::s_CameraImageApi
	RuntimeObject* ___s_CameraImageApi_3;
	// UnityEngine.XR.ARExtensions.DefaultCameraImageApi UnityEngine.XR.ARExtensions.XRCameraExtensions::s_DefaultCameraImageApi
	DefaultCameraImageApi_t32807EEA82DF3B067158E1DCEA117A577B5E3BCB * ___s_DefaultCameraImageApi_4;
	// UnityEngine.XR.ARExtensions.ICameraConfigApi UnityEngine.XR.ARExtensions.XRCameraExtensions::s_CameraConfigApi
	RuntimeObject* ___s_CameraConfigApi_5;
	// UnityEngine.XR.ARExtensions.DefaultCameraConfigApi UnityEngine.XR.ARExtensions.XRCameraExtensions::s_DefaultCameraConfigApi
	DefaultCameraConfigApi_t125599F6D1891BEF834E1B20C71C503AB27F4091 * ___s_DefaultCameraConfigApi_6;
	// System.Func`3<UnityEngine.Experimental.XR.XRCameraSubsystem,UnityEngine.XR.ARExtensions.CameraFocusMode,System.Boolean> UnityEngine.XR.ARExtensions.XRCameraExtensions::s_TrySetFocusModeDelegate
	Func_3_t8142E55EC06EB0185AD099830F125CA5E187DDD8 * ___s_TrySetFocusModeDelegate_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Func`2<UnityEngine.Experimental.XR.XRCameraSubsystem,System.Boolean>> UnityEngine.XR.ARExtensions.XRCameraExtensions::s_IsPermissionGrantedDelegates
	Dictionary_2_t332B381ADD42E7894216EE28B27D48019ED7637B * ___s_IsPermissionGrantedDelegates_8;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.ARExtensions.XRCameraExtensions/TryGetColorCorrectionDelegate> UnityEngine.XR.ARExtensions.XRCameraExtensions::s_TryGetColorCorrectionDelegates
	Dictionary_2_tDBB540258E01DD19F5B248555ADFB91E451BADF0 * ___s_TryGetColorCorrectionDelegates_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Func`2<UnityEngine.Experimental.XR.XRCameraSubsystem,System.IntPtr>> UnityEngine.XR.ARExtensions.XRCameraExtensions::s_GetNativePtrDelegates
	Dictionary_2_t707989CBAF49C089295D286B11265A8C639A7730 * ___s_GetNativePtrDelegates_10;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.ARExtensions.ICameraImageApi> UnityEngine.XR.ARExtensions.XRCameraExtensions::s_CameraImageApis
	Dictionary_2_t6058911EEE43591076AB7625C4B87296D2B2E94F * ___s_CameraImageApis_11;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.ARExtensions.ICameraConfigApi> UnityEngine.XR.ARExtensions.XRCameraExtensions::s_CameraConfigApis
	Dictionary_2_tECE8019B660D66E6B522360B0BF8F94254B823FF * ___s_CameraConfigApis_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Func`3<UnityEngine.Experimental.XR.XRCameraSubsystem,UnityEngine.XR.ARExtensions.CameraFocusMode,System.Boolean>> UnityEngine.XR.ARExtensions.XRCameraExtensions::s_TrySetFocusModeDelegates
	Dictionary_2_t03FC742469A2B251FF3680C63D1C74E74C66DECF * ___s_TrySetFocusModeDelegates_13;

public:
	inline static int32_t get_offset_of_s_IsPermissionGrantedDelegate_0() { return static_cast<int32_t>(offsetof(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields, ___s_IsPermissionGrantedDelegate_0)); }
	inline Func_2_t8DD4B98590E9E9D33E80BE6D41F3B325D5F48D78 * get_s_IsPermissionGrantedDelegate_0() const { return ___s_IsPermissionGrantedDelegate_0; }
	inline Func_2_t8DD4B98590E9E9D33E80BE6D41F3B325D5F48D78 ** get_address_of_s_IsPermissionGrantedDelegate_0() { return &___s_IsPermissionGrantedDelegate_0; }
	inline void set_s_IsPermissionGrantedDelegate_0(Func_2_t8DD4B98590E9E9D33E80BE6D41F3B325D5F48D78 * value)
	{
		___s_IsPermissionGrantedDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_IsPermissionGrantedDelegate_0), value);
	}

	inline static int32_t get_offset_of_s_TryGetColorCorrectionDelegate_1() { return static_cast<int32_t>(offsetof(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields, ___s_TryGetColorCorrectionDelegate_1)); }
	inline TryGetColorCorrectionDelegate_t30B930C66D1DCEC1490F1A6624404BA665652BD8 * get_s_TryGetColorCorrectionDelegate_1() const { return ___s_TryGetColorCorrectionDelegate_1; }
	inline TryGetColorCorrectionDelegate_t30B930C66D1DCEC1490F1A6624404BA665652BD8 ** get_address_of_s_TryGetColorCorrectionDelegate_1() { return &___s_TryGetColorCorrectionDelegate_1; }
	inline void set_s_TryGetColorCorrectionDelegate_1(TryGetColorCorrectionDelegate_t30B930C66D1DCEC1490F1A6624404BA665652BD8 * value)
	{
		___s_TryGetColorCorrectionDelegate_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_TryGetColorCorrectionDelegate_1), value);
	}

	inline static int32_t get_offset_of_s_GetNativePtrDelegate_2() { return static_cast<int32_t>(offsetof(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields, ___s_GetNativePtrDelegate_2)); }
	inline Func_2_tD21C0AD1E10C99C0FC7A84082344E25F086499CD * get_s_GetNativePtrDelegate_2() const { return ___s_GetNativePtrDelegate_2; }
	inline Func_2_tD21C0AD1E10C99C0FC7A84082344E25F086499CD ** get_address_of_s_GetNativePtrDelegate_2() { return &___s_GetNativePtrDelegate_2; }
	inline void set_s_GetNativePtrDelegate_2(Func_2_tD21C0AD1E10C99C0FC7A84082344E25F086499CD * value)
	{
		___s_GetNativePtrDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetNativePtrDelegate_2), value);
	}

	inline static int32_t get_offset_of_s_CameraImageApi_3() { return static_cast<int32_t>(offsetof(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields, ___s_CameraImageApi_3)); }
	inline RuntimeObject* get_s_CameraImageApi_3() const { return ___s_CameraImageApi_3; }
	inline RuntimeObject** get_address_of_s_CameraImageApi_3() { return &___s_CameraImageApi_3; }
	inline void set_s_CameraImageApi_3(RuntimeObject* value)
	{
		___s_CameraImageApi_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_CameraImageApi_3), value);
	}

	inline static int32_t get_offset_of_s_DefaultCameraImageApi_4() { return static_cast<int32_t>(offsetof(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields, ___s_DefaultCameraImageApi_4)); }
	inline DefaultCameraImageApi_t32807EEA82DF3B067158E1DCEA117A577B5E3BCB * get_s_DefaultCameraImageApi_4() const { return ___s_DefaultCameraImageApi_4; }
	inline DefaultCameraImageApi_t32807EEA82DF3B067158E1DCEA117A577B5E3BCB ** get_address_of_s_DefaultCameraImageApi_4() { return &___s_DefaultCameraImageApi_4; }
	inline void set_s_DefaultCameraImageApi_4(DefaultCameraImageApi_t32807EEA82DF3B067158E1DCEA117A577B5E3BCB * value)
	{
		___s_DefaultCameraImageApi_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultCameraImageApi_4), value);
	}

	inline static int32_t get_offset_of_s_CameraConfigApi_5() { return static_cast<int32_t>(offsetof(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields, ___s_CameraConfigApi_5)); }
	inline RuntimeObject* get_s_CameraConfigApi_5() const { return ___s_CameraConfigApi_5; }
	inline RuntimeObject** get_address_of_s_CameraConfigApi_5() { return &___s_CameraConfigApi_5; }
	inline void set_s_CameraConfigApi_5(RuntimeObject* value)
	{
		___s_CameraConfigApi_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_CameraConfigApi_5), value);
	}

	inline static int32_t get_offset_of_s_DefaultCameraConfigApi_6() { return static_cast<int32_t>(offsetof(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields, ___s_DefaultCameraConfigApi_6)); }
	inline DefaultCameraConfigApi_t125599F6D1891BEF834E1B20C71C503AB27F4091 * get_s_DefaultCameraConfigApi_6() const { return ___s_DefaultCameraConfigApi_6; }
	inline DefaultCameraConfigApi_t125599F6D1891BEF834E1B20C71C503AB27F4091 ** get_address_of_s_DefaultCameraConfigApi_6() { return &___s_DefaultCameraConfigApi_6; }
	inline void set_s_DefaultCameraConfigApi_6(DefaultCameraConfigApi_t125599F6D1891BEF834E1B20C71C503AB27F4091 * value)
	{
		___s_DefaultCameraConfigApi_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultCameraConfigApi_6), value);
	}

	inline static int32_t get_offset_of_s_TrySetFocusModeDelegate_7() { return static_cast<int32_t>(offsetof(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields, ___s_TrySetFocusModeDelegate_7)); }
	inline Func_3_t8142E55EC06EB0185AD099830F125CA5E187DDD8 * get_s_TrySetFocusModeDelegate_7() const { return ___s_TrySetFocusModeDelegate_7; }
	inline Func_3_t8142E55EC06EB0185AD099830F125CA5E187DDD8 ** get_address_of_s_TrySetFocusModeDelegate_7() { return &___s_TrySetFocusModeDelegate_7; }
	inline void set_s_TrySetFocusModeDelegate_7(Func_3_t8142E55EC06EB0185AD099830F125CA5E187DDD8 * value)
	{
		___s_TrySetFocusModeDelegate_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_TrySetFocusModeDelegate_7), value);
	}

	inline static int32_t get_offset_of_s_IsPermissionGrantedDelegates_8() { return static_cast<int32_t>(offsetof(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields, ___s_IsPermissionGrantedDelegates_8)); }
	inline Dictionary_2_t332B381ADD42E7894216EE28B27D48019ED7637B * get_s_IsPermissionGrantedDelegates_8() const { return ___s_IsPermissionGrantedDelegates_8; }
	inline Dictionary_2_t332B381ADD42E7894216EE28B27D48019ED7637B ** get_address_of_s_IsPermissionGrantedDelegates_8() { return &___s_IsPermissionGrantedDelegates_8; }
	inline void set_s_IsPermissionGrantedDelegates_8(Dictionary_2_t332B381ADD42E7894216EE28B27D48019ED7637B * value)
	{
		___s_IsPermissionGrantedDelegates_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_IsPermissionGrantedDelegates_8), value);
	}

	inline static int32_t get_offset_of_s_TryGetColorCorrectionDelegates_9() { return static_cast<int32_t>(offsetof(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields, ___s_TryGetColorCorrectionDelegates_9)); }
	inline Dictionary_2_tDBB540258E01DD19F5B248555ADFB91E451BADF0 * get_s_TryGetColorCorrectionDelegates_9() const { return ___s_TryGetColorCorrectionDelegates_9; }
	inline Dictionary_2_tDBB540258E01DD19F5B248555ADFB91E451BADF0 ** get_address_of_s_TryGetColorCorrectionDelegates_9() { return &___s_TryGetColorCorrectionDelegates_9; }
	inline void set_s_TryGetColorCorrectionDelegates_9(Dictionary_2_tDBB540258E01DD19F5B248555ADFB91E451BADF0 * value)
	{
		___s_TryGetColorCorrectionDelegates_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_TryGetColorCorrectionDelegates_9), value);
	}

	inline static int32_t get_offset_of_s_GetNativePtrDelegates_10() { return static_cast<int32_t>(offsetof(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields, ___s_GetNativePtrDelegates_10)); }
	inline Dictionary_2_t707989CBAF49C089295D286B11265A8C639A7730 * get_s_GetNativePtrDelegates_10() const { return ___s_GetNativePtrDelegates_10; }
	inline Dictionary_2_t707989CBAF49C089295D286B11265A8C639A7730 ** get_address_of_s_GetNativePtrDelegates_10() { return &___s_GetNativePtrDelegates_10; }
	inline void set_s_GetNativePtrDelegates_10(Dictionary_2_t707989CBAF49C089295D286B11265A8C639A7730 * value)
	{
		___s_GetNativePtrDelegates_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetNativePtrDelegates_10), value);
	}

	inline static int32_t get_offset_of_s_CameraImageApis_11() { return static_cast<int32_t>(offsetof(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields, ___s_CameraImageApis_11)); }
	inline Dictionary_2_t6058911EEE43591076AB7625C4B87296D2B2E94F * get_s_CameraImageApis_11() const { return ___s_CameraImageApis_11; }
	inline Dictionary_2_t6058911EEE43591076AB7625C4B87296D2B2E94F ** get_address_of_s_CameraImageApis_11() { return &___s_CameraImageApis_11; }
	inline void set_s_CameraImageApis_11(Dictionary_2_t6058911EEE43591076AB7625C4B87296D2B2E94F * value)
	{
		___s_CameraImageApis_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_CameraImageApis_11), value);
	}

	inline static int32_t get_offset_of_s_CameraConfigApis_12() { return static_cast<int32_t>(offsetof(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields, ___s_CameraConfigApis_12)); }
	inline Dictionary_2_tECE8019B660D66E6B522360B0BF8F94254B823FF * get_s_CameraConfigApis_12() const { return ___s_CameraConfigApis_12; }
	inline Dictionary_2_tECE8019B660D66E6B522360B0BF8F94254B823FF ** get_address_of_s_CameraConfigApis_12() { return &___s_CameraConfigApis_12; }
	inline void set_s_CameraConfigApis_12(Dictionary_2_tECE8019B660D66E6B522360B0BF8F94254B823FF * value)
	{
		___s_CameraConfigApis_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_CameraConfigApis_12), value);
	}

	inline static int32_t get_offset_of_s_TrySetFocusModeDelegates_13() { return static_cast<int32_t>(offsetof(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields, ___s_TrySetFocusModeDelegates_13)); }
	inline Dictionary_2_t03FC742469A2B251FF3680C63D1C74E74C66DECF * get_s_TrySetFocusModeDelegates_13() const { return ___s_TrySetFocusModeDelegates_13; }
	inline Dictionary_2_t03FC742469A2B251FF3680C63D1C74E74C66DECF ** get_address_of_s_TrySetFocusModeDelegates_13() { return &___s_TrySetFocusModeDelegates_13; }
	inline void set_s_TrySetFocusModeDelegates_13(Dictionary_2_t03FC742469A2B251FF3680C63D1C74E74C66DECF * value)
	{
		___s_TrySetFocusModeDelegates_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_TrySetFocusModeDelegates_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRCAMERAEXTENSIONS_T9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_H
#ifndef XRPLANEEXTENSIONS_TE5907A4D3FC04AB6A65D1F86404095C31362D4BE_H
#define XRPLANEEXTENSIONS_TE5907A4D3FC04AB6A65D1F86404095C31362D4BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.XRPlaneExtensions
struct  XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE  : public RuntimeObject
{
public:

public:
};

struct XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE_StaticFields
{
public:
	// System.Func`3<UnityEngine.Experimental.XR.XRPlaneSubsystem,UnityEngine.Experimental.XR.TrackableId,System.IntPtr> UnityEngine.XR.ARExtensions.XRPlaneExtensions::s_GetNativePtrDelegate
	Func_3_tDF578AEA147B81628BA611F816D57E306CA287C4 * ___s_GetNativePtrDelegate_0;
	// System.Func`3<UnityEngine.Experimental.XR.XRPlaneSubsystem,UnityEngine.Experimental.XR.TrackableId,UnityEngine.Experimental.XR.TrackingState> UnityEngine.XR.ARExtensions.XRPlaneExtensions::s_GetTrackingStateDelegate
	Func_3_tADB96F4F6D4804C360DF07DE8146E2B4631B36B9 * ___s_GetTrackingStateDelegate_1;
	// System.Func`3<UnityEngine.Experimental.XR.XRPlaneSubsystem,UnityEngine.XR.ARExtensions.PlaneDetectionFlags,System.Boolean> UnityEngine.XR.ARExtensions.XRPlaneExtensions::s_TrySetPlaneDetectionFlagsDelegate
	Func_3_t1EF1CCE7ECB8A978928DF7EEDA4028100901CB48 * ___s_TrySetPlaneDetectionFlagsDelegate_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Func`3<UnityEngine.Experimental.XR.XRPlaneSubsystem,UnityEngine.Experimental.XR.TrackableId,System.IntPtr>> UnityEngine.XR.ARExtensions.XRPlaneExtensions::s_GetNativePtrDelegates
	Dictionary_2_t2706E85D313DEF390F17DB6874CDDCEEE2DCB409 * ___s_GetNativePtrDelegates_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Func`3<UnityEngine.Experimental.XR.XRPlaneSubsystem,UnityEngine.Experimental.XR.TrackableId,UnityEngine.Experimental.XR.TrackingState>> UnityEngine.XR.ARExtensions.XRPlaneExtensions::s_GetTrackingStateDelegates
	Dictionary_2_t4A9D89D64890F68FBC43DE2C012EE1C637F5C467 * ___s_GetTrackingStateDelegates_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Func`3<UnityEngine.Experimental.XR.XRPlaneSubsystem,UnityEngine.XR.ARExtensions.PlaneDetectionFlags,System.Boolean>> UnityEngine.XR.ARExtensions.XRPlaneExtensions::s_TrySetPlaneDetectionFlagsDelegates
	Dictionary_2_tE04FB6FCD228835F326ECDF3C5A21B523796BEB6 * ___s_TrySetPlaneDetectionFlagsDelegates_5;

public:
	inline static int32_t get_offset_of_s_GetNativePtrDelegate_0() { return static_cast<int32_t>(offsetof(XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE_StaticFields, ___s_GetNativePtrDelegate_0)); }
	inline Func_3_tDF578AEA147B81628BA611F816D57E306CA287C4 * get_s_GetNativePtrDelegate_0() const { return ___s_GetNativePtrDelegate_0; }
	inline Func_3_tDF578AEA147B81628BA611F816D57E306CA287C4 ** get_address_of_s_GetNativePtrDelegate_0() { return &___s_GetNativePtrDelegate_0; }
	inline void set_s_GetNativePtrDelegate_0(Func_3_tDF578AEA147B81628BA611F816D57E306CA287C4 * value)
	{
		___s_GetNativePtrDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetNativePtrDelegate_0), value);
	}

	inline static int32_t get_offset_of_s_GetTrackingStateDelegate_1() { return static_cast<int32_t>(offsetof(XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE_StaticFields, ___s_GetTrackingStateDelegate_1)); }
	inline Func_3_tADB96F4F6D4804C360DF07DE8146E2B4631B36B9 * get_s_GetTrackingStateDelegate_1() const { return ___s_GetTrackingStateDelegate_1; }
	inline Func_3_tADB96F4F6D4804C360DF07DE8146E2B4631B36B9 ** get_address_of_s_GetTrackingStateDelegate_1() { return &___s_GetTrackingStateDelegate_1; }
	inline void set_s_GetTrackingStateDelegate_1(Func_3_tADB96F4F6D4804C360DF07DE8146E2B4631B36B9 * value)
	{
		___s_GetTrackingStateDelegate_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetTrackingStateDelegate_1), value);
	}

	inline static int32_t get_offset_of_s_TrySetPlaneDetectionFlagsDelegate_2() { return static_cast<int32_t>(offsetof(XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE_StaticFields, ___s_TrySetPlaneDetectionFlagsDelegate_2)); }
	inline Func_3_t1EF1CCE7ECB8A978928DF7EEDA4028100901CB48 * get_s_TrySetPlaneDetectionFlagsDelegate_2() const { return ___s_TrySetPlaneDetectionFlagsDelegate_2; }
	inline Func_3_t1EF1CCE7ECB8A978928DF7EEDA4028100901CB48 ** get_address_of_s_TrySetPlaneDetectionFlagsDelegate_2() { return &___s_TrySetPlaneDetectionFlagsDelegate_2; }
	inline void set_s_TrySetPlaneDetectionFlagsDelegate_2(Func_3_t1EF1CCE7ECB8A978928DF7EEDA4028100901CB48 * value)
	{
		___s_TrySetPlaneDetectionFlagsDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_TrySetPlaneDetectionFlagsDelegate_2), value);
	}

	inline static int32_t get_offset_of_s_GetNativePtrDelegates_3() { return static_cast<int32_t>(offsetof(XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE_StaticFields, ___s_GetNativePtrDelegates_3)); }
	inline Dictionary_2_t2706E85D313DEF390F17DB6874CDDCEEE2DCB409 * get_s_GetNativePtrDelegates_3() const { return ___s_GetNativePtrDelegates_3; }
	inline Dictionary_2_t2706E85D313DEF390F17DB6874CDDCEEE2DCB409 ** get_address_of_s_GetNativePtrDelegates_3() { return &___s_GetNativePtrDelegates_3; }
	inline void set_s_GetNativePtrDelegates_3(Dictionary_2_t2706E85D313DEF390F17DB6874CDDCEEE2DCB409 * value)
	{
		___s_GetNativePtrDelegates_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetNativePtrDelegates_3), value);
	}

	inline static int32_t get_offset_of_s_GetTrackingStateDelegates_4() { return static_cast<int32_t>(offsetof(XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE_StaticFields, ___s_GetTrackingStateDelegates_4)); }
	inline Dictionary_2_t4A9D89D64890F68FBC43DE2C012EE1C637F5C467 * get_s_GetTrackingStateDelegates_4() const { return ___s_GetTrackingStateDelegates_4; }
	inline Dictionary_2_t4A9D89D64890F68FBC43DE2C012EE1C637F5C467 ** get_address_of_s_GetTrackingStateDelegates_4() { return &___s_GetTrackingStateDelegates_4; }
	inline void set_s_GetTrackingStateDelegates_4(Dictionary_2_t4A9D89D64890F68FBC43DE2C012EE1C637F5C467 * value)
	{
		___s_GetTrackingStateDelegates_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetTrackingStateDelegates_4), value);
	}

	inline static int32_t get_offset_of_s_TrySetPlaneDetectionFlagsDelegates_5() { return static_cast<int32_t>(offsetof(XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE_StaticFields, ___s_TrySetPlaneDetectionFlagsDelegates_5)); }
	inline Dictionary_2_tE04FB6FCD228835F326ECDF3C5A21B523796BEB6 * get_s_TrySetPlaneDetectionFlagsDelegates_5() const { return ___s_TrySetPlaneDetectionFlagsDelegates_5; }
	inline Dictionary_2_tE04FB6FCD228835F326ECDF3C5A21B523796BEB6 ** get_address_of_s_TrySetPlaneDetectionFlagsDelegates_5() { return &___s_TrySetPlaneDetectionFlagsDelegates_5; }
	inline void set_s_TrySetPlaneDetectionFlagsDelegates_5(Dictionary_2_tE04FB6FCD228835F326ECDF3C5A21B523796BEB6 * value)
	{
		___s_TrySetPlaneDetectionFlagsDelegates_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_TrySetPlaneDetectionFlagsDelegates_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRPLANEEXTENSIONS_TE5907A4D3FC04AB6A65D1F86404095C31362D4BE_H
#ifndef XRREFERENCEPOINTEXTENSIONS_TDB4598C0A44D28208D387FF166DD81A02D92D74D_H
#define XRREFERENCEPOINTEXTENSIONS_TDB4598C0A44D28208D387FF166DD81A02D92D74D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.XRReferencePointExtensions
struct  XRReferencePointExtensions_tDB4598C0A44D28208D387FF166DD81A02D92D74D  : public RuntimeObject
{
public:

public:
};

struct XRReferencePointExtensions_tDB4598C0A44D28208D387FF166DD81A02D92D74D_StaticFields
{
public:
	// UnityEngine.XR.ARExtensions.XRReferencePointExtensions/AttachReferencePointDelegate UnityEngine.XR.ARExtensions.XRReferencePointExtensions::s_AttachReferencePointDelegate
	AttachReferencePointDelegate_t8E14528144FB8B3DE6B44FCD01B3B4C2090236DF * ___s_AttachReferencePointDelegate_0;
	// System.Func`3<UnityEngine.Experimental.XR.XRReferencePointSubsystem,UnityEngine.Experimental.XR.TrackableId,System.IntPtr> UnityEngine.XR.ARExtensions.XRReferencePointExtensions::s_GetNativePtrDelegate
	Func_3_tE51207E7A242422D97436D398CA19D150FF94581 * ___s_GetNativePtrDelegate_1;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.ARExtensions.XRReferencePointExtensions/AttachReferencePointDelegate> UnityEngine.XR.ARExtensions.XRReferencePointExtensions::s_AttachReferencePointDelegates
	Dictionary_2_tF65291F5C17BE4538B0A42E1C7369F27621D2554 * ___s_AttachReferencePointDelegates_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Func`3<UnityEngine.Experimental.XR.XRReferencePointSubsystem,UnityEngine.Experimental.XR.TrackableId,System.IntPtr>> UnityEngine.XR.ARExtensions.XRReferencePointExtensions::s_GetNativePtrDelegates
	Dictionary_2_tD67186A32F638C55B3D7E5D00AA532859F8B871D * ___s_GetNativePtrDelegates_3;

public:
	inline static int32_t get_offset_of_s_AttachReferencePointDelegate_0() { return static_cast<int32_t>(offsetof(XRReferencePointExtensions_tDB4598C0A44D28208D387FF166DD81A02D92D74D_StaticFields, ___s_AttachReferencePointDelegate_0)); }
	inline AttachReferencePointDelegate_t8E14528144FB8B3DE6B44FCD01B3B4C2090236DF * get_s_AttachReferencePointDelegate_0() const { return ___s_AttachReferencePointDelegate_0; }
	inline AttachReferencePointDelegate_t8E14528144FB8B3DE6B44FCD01B3B4C2090236DF ** get_address_of_s_AttachReferencePointDelegate_0() { return &___s_AttachReferencePointDelegate_0; }
	inline void set_s_AttachReferencePointDelegate_0(AttachReferencePointDelegate_t8E14528144FB8B3DE6B44FCD01B3B4C2090236DF * value)
	{
		___s_AttachReferencePointDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_AttachReferencePointDelegate_0), value);
	}

	inline static int32_t get_offset_of_s_GetNativePtrDelegate_1() { return static_cast<int32_t>(offsetof(XRReferencePointExtensions_tDB4598C0A44D28208D387FF166DD81A02D92D74D_StaticFields, ___s_GetNativePtrDelegate_1)); }
	inline Func_3_tE51207E7A242422D97436D398CA19D150FF94581 * get_s_GetNativePtrDelegate_1() const { return ___s_GetNativePtrDelegate_1; }
	inline Func_3_tE51207E7A242422D97436D398CA19D150FF94581 ** get_address_of_s_GetNativePtrDelegate_1() { return &___s_GetNativePtrDelegate_1; }
	inline void set_s_GetNativePtrDelegate_1(Func_3_tE51207E7A242422D97436D398CA19D150FF94581 * value)
	{
		___s_GetNativePtrDelegate_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetNativePtrDelegate_1), value);
	}

	inline static int32_t get_offset_of_s_AttachReferencePointDelegates_2() { return static_cast<int32_t>(offsetof(XRReferencePointExtensions_tDB4598C0A44D28208D387FF166DD81A02D92D74D_StaticFields, ___s_AttachReferencePointDelegates_2)); }
	inline Dictionary_2_tF65291F5C17BE4538B0A42E1C7369F27621D2554 * get_s_AttachReferencePointDelegates_2() const { return ___s_AttachReferencePointDelegates_2; }
	inline Dictionary_2_tF65291F5C17BE4538B0A42E1C7369F27621D2554 ** get_address_of_s_AttachReferencePointDelegates_2() { return &___s_AttachReferencePointDelegates_2; }
	inline void set_s_AttachReferencePointDelegates_2(Dictionary_2_tF65291F5C17BE4538B0A42E1C7369F27621D2554 * value)
	{
		___s_AttachReferencePointDelegates_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_AttachReferencePointDelegates_2), value);
	}

	inline static int32_t get_offset_of_s_GetNativePtrDelegates_3() { return static_cast<int32_t>(offsetof(XRReferencePointExtensions_tDB4598C0A44D28208D387FF166DD81A02D92D74D_StaticFields, ___s_GetNativePtrDelegates_3)); }
	inline Dictionary_2_tD67186A32F638C55B3D7E5D00AA532859F8B871D * get_s_GetNativePtrDelegates_3() const { return ___s_GetNativePtrDelegates_3; }
	inline Dictionary_2_tD67186A32F638C55B3D7E5D00AA532859F8B871D ** get_address_of_s_GetNativePtrDelegates_3() { return &___s_GetNativePtrDelegates_3; }
	inline void set_s_GetNativePtrDelegates_3(Dictionary_2_tD67186A32F638C55B3D7E5D00AA532859F8B871D * value)
	{
		___s_GetNativePtrDelegates_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetNativePtrDelegates_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRREFERENCEPOINTEXTENSIONS_TDB4598C0A44D28208D387FF166DD81A02D92D74D_H
#ifndef XRSESSIONEXTENSIONS_T90CFFFAE273DEBF123D02B08B529F491CFA1DF49_H
#define XRSESSIONEXTENSIONS_T90CFFFAE273DEBF123D02B08B529F491CFA1DF49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.XRSessionExtensions
struct  XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49  : public RuntimeObject
{
public:

public:
};

struct XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49_StaticFields
{
public:
	// UnityEngine.XR.ARExtensions.XRSessionExtensions/AsyncDelegate`1<UnityEngine.XR.ARExtensions.SessionInstallationStatus> UnityEngine.XR.ARExtensions.XRSessionExtensions::s_InstallAsyncDelegate
	AsyncDelegate_1_t249767B2886936DE438787AE5D7C959A26DE03C3 * ___s_InstallAsyncDelegate_0;
	// UnityEngine.XR.ARExtensions.XRSessionExtensions/AsyncDelegate`1<UnityEngine.XR.ARExtensions.SessionAvailability> UnityEngine.XR.ARExtensions.XRSessionExtensions::s_GetAvailabilityAsyncDelegate
	AsyncDelegate_1_tC1209225BB9B8AF0FB9384EA8D49347BB4BCC659 * ___s_GetAvailabilityAsyncDelegate_1;
	// System.Func`2<UnityEngine.Experimental.XR.XRSessionSubsystem,System.IntPtr> UnityEngine.XR.ARExtensions.XRSessionExtensions::s_GetNativePtrDelegate
	Func_2_t9B4A2794A15F05B6A5AA4A59CE29B1D3CA8081A7 * ___s_GetNativePtrDelegate_2;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.ARExtensions.XRSessionExtensions/AsyncDelegate`1<UnityEngine.XR.ARExtensions.SessionInstallationStatus>> UnityEngine.XR.ARExtensions.XRSessionExtensions::s_InstallAsyncDelegates
	Dictionary_2_t5440C9C92669AAC2A8D32C4BC41523E53DE6A49F * ___s_InstallAsyncDelegates_3;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.ARExtensions.XRSessionExtensions/AsyncDelegate`1<UnityEngine.XR.ARExtensions.SessionAvailability>> UnityEngine.XR.ARExtensions.XRSessionExtensions::s_GetAvailabilityAsyncDelegates
	Dictionary_2_tCA4D6C75E791133AABE5613D1D1A6A554382A344 * ___s_GetAvailabilityAsyncDelegates_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Func`2<UnityEngine.Experimental.XR.XRSessionSubsystem,System.IntPtr>> UnityEngine.XR.ARExtensions.XRSessionExtensions::s_GetNativePtrDelegates
	Dictionary_2_tAD560473AB7821B978587ED1305391B72E86B1B7 * ___s_GetNativePtrDelegates_5;

public:
	inline static int32_t get_offset_of_s_InstallAsyncDelegate_0() { return static_cast<int32_t>(offsetof(XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49_StaticFields, ___s_InstallAsyncDelegate_0)); }
	inline AsyncDelegate_1_t249767B2886936DE438787AE5D7C959A26DE03C3 * get_s_InstallAsyncDelegate_0() const { return ___s_InstallAsyncDelegate_0; }
	inline AsyncDelegate_1_t249767B2886936DE438787AE5D7C959A26DE03C3 ** get_address_of_s_InstallAsyncDelegate_0() { return &___s_InstallAsyncDelegate_0; }
	inline void set_s_InstallAsyncDelegate_0(AsyncDelegate_1_t249767B2886936DE438787AE5D7C959A26DE03C3 * value)
	{
		___s_InstallAsyncDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_InstallAsyncDelegate_0), value);
	}

	inline static int32_t get_offset_of_s_GetAvailabilityAsyncDelegate_1() { return static_cast<int32_t>(offsetof(XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49_StaticFields, ___s_GetAvailabilityAsyncDelegate_1)); }
	inline AsyncDelegate_1_tC1209225BB9B8AF0FB9384EA8D49347BB4BCC659 * get_s_GetAvailabilityAsyncDelegate_1() const { return ___s_GetAvailabilityAsyncDelegate_1; }
	inline AsyncDelegate_1_tC1209225BB9B8AF0FB9384EA8D49347BB4BCC659 ** get_address_of_s_GetAvailabilityAsyncDelegate_1() { return &___s_GetAvailabilityAsyncDelegate_1; }
	inline void set_s_GetAvailabilityAsyncDelegate_1(AsyncDelegate_1_tC1209225BB9B8AF0FB9384EA8D49347BB4BCC659 * value)
	{
		___s_GetAvailabilityAsyncDelegate_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetAvailabilityAsyncDelegate_1), value);
	}

	inline static int32_t get_offset_of_s_GetNativePtrDelegate_2() { return static_cast<int32_t>(offsetof(XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49_StaticFields, ___s_GetNativePtrDelegate_2)); }
	inline Func_2_t9B4A2794A15F05B6A5AA4A59CE29B1D3CA8081A7 * get_s_GetNativePtrDelegate_2() const { return ___s_GetNativePtrDelegate_2; }
	inline Func_2_t9B4A2794A15F05B6A5AA4A59CE29B1D3CA8081A7 ** get_address_of_s_GetNativePtrDelegate_2() { return &___s_GetNativePtrDelegate_2; }
	inline void set_s_GetNativePtrDelegate_2(Func_2_t9B4A2794A15F05B6A5AA4A59CE29B1D3CA8081A7 * value)
	{
		___s_GetNativePtrDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetNativePtrDelegate_2), value);
	}

	inline static int32_t get_offset_of_s_InstallAsyncDelegates_3() { return static_cast<int32_t>(offsetof(XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49_StaticFields, ___s_InstallAsyncDelegates_3)); }
	inline Dictionary_2_t5440C9C92669AAC2A8D32C4BC41523E53DE6A49F * get_s_InstallAsyncDelegates_3() const { return ___s_InstallAsyncDelegates_3; }
	inline Dictionary_2_t5440C9C92669AAC2A8D32C4BC41523E53DE6A49F ** get_address_of_s_InstallAsyncDelegates_3() { return &___s_InstallAsyncDelegates_3; }
	inline void set_s_InstallAsyncDelegates_3(Dictionary_2_t5440C9C92669AAC2A8D32C4BC41523E53DE6A49F * value)
	{
		___s_InstallAsyncDelegates_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_InstallAsyncDelegates_3), value);
	}

	inline static int32_t get_offset_of_s_GetAvailabilityAsyncDelegates_4() { return static_cast<int32_t>(offsetof(XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49_StaticFields, ___s_GetAvailabilityAsyncDelegates_4)); }
	inline Dictionary_2_tCA4D6C75E791133AABE5613D1D1A6A554382A344 * get_s_GetAvailabilityAsyncDelegates_4() const { return ___s_GetAvailabilityAsyncDelegates_4; }
	inline Dictionary_2_tCA4D6C75E791133AABE5613D1D1A6A554382A344 ** get_address_of_s_GetAvailabilityAsyncDelegates_4() { return &___s_GetAvailabilityAsyncDelegates_4; }
	inline void set_s_GetAvailabilityAsyncDelegates_4(Dictionary_2_tCA4D6C75E791133AABE5613D1D1A6A554382A344 * value)
	{
		___s_GetAvailabilityAsyncDelegates_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetAvailabilityAsyncDelegates_4), value);
	}

	inline static int32_t get_offset_of_s_GetNativePtrDelegates_5() { return static_cast<int32_t>(offsetof(XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49_StaticFields, ___s_GetNativePtrDelegates_5)); }
	inline Dictionary_2_tAD560473AB7821B978587ED1305391B72E86B1B7 * get_s_GetNativePtrDelegates_5() const { return ___s_GetNativePtrDelegates_5; }
	inline Dictionary_2_tAD560473AB7821B978587ED1305391B72E86B1B7 ** get_address_of_s_GetNativePtrDelegates_5() { return &___s_GetNativePtrDelegates_5; }
	inline void set_s_GetNativePtrDelegates_5(Dictionary_2_tAD560473AB7821B978587ED1305391B72E86B1B7 * value)
	{
		___s_GetNativePtrDelegates_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetNativePtrDelegates_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRSESSIONEXTENSIONS_T90CFFFAE273DEBF123D02B08B529F491CFA1DF49_H
#ifndef __STATICARRAYINITTYPESIZEU3D10_T2210C3858CD7BBD086B8C29DD7AE0B07CF930F6E_H
#define __STATICARRAYINITTYPESIZEU3D10_T2210C3858CD7BBD086B8C29DD7AE0B07CF930F6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10
struct  __StaticArrayInitTypeSizeU3D10_t2210C3858CD7BBD086B8C29DD7AE0B07CF930F6E 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D10_t2210C3858CD7BBD086B8C29DD7AE0B07CF930F6E__padding[10];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D10_T2210C3858CD7BBD086B8C29DD7AE0B07CF930F6E_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T6A42B5D9C1B6EC123AB21D43DAB4814988F7FE5A_H
#define __STATICARRAYINITTYPESIZEU3D12_T6A42B5D9C1B6EC123AB21D43DAB4814988F7FE5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_t6A42B5D9C1B6EC123AB21D43DAB4814988F7FE5A 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t6A42B5D9C1B6EC123AB21D43DAB4814988F7FE5A__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T6A42B5D9C1B6EC123AB21D43DAB4814988F7FE5A_H
#ifndef __STATICARRAYINITTYPESIZEU3D28_TD95E73C8180D450CB90FBFA3367F31DECB46B9A8_H
#define __STATICARRAYINITTYPESIZEU3D28_TD95E73C8180D450CB90FBFA3367F31DECB46B9A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28
struct  __StaticArrayInitTypeSizeU3D28_tD95E73C8180D450CB90FBFA3367F31DECB46B9A8 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_tD95E73C8180D450CB90FBFA3367F31DECB46B9A8__padding[28];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D28_TD95E73C8180D450CB90FBFA3367F31DECB46B9A8_H
#ifndef __STATICARRAYINITTYPESIZEU3D52_TA3556BE2FDC9141F7C4FB86611890DADE17CDD63_H
#define __STATICARRAYINITTYPESIZEU3D52_TA3556BE2FDC9141F7C4FB86611890DADE17CDD63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52
struct  __StaticArrayInitTypeSizeU3D52_tA3556BE2FDC9141F7C4FB86611890DADE17CDD63 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D52_tA3556BE2FDC9141F7C4FB86611890DADE17CDD63__padding[52];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D52_TA3556BE2FDC9141F7C4FB86611890DADE17CDD63_H
#ifndef BSONARRAY_T409BB62F0CCDED5660D1BABDA0BB2085FB09C915_H
#define BSONARRAY_T409BB62F0CCDED5660D1BABDA0BB2085FB09C915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonArray
struct  BsonArray_t409BB62F0CCDED5660D1BABDA0BB2085FB09C915  : public BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonToken> Newtonsoft.Json.Bson.BsonArray::_children
	List_1_t8966010566181DE22880EF5DF9C0CB53B4614363 * ____children_2;

public:
	inline static int32_t get_offset_of__children_2() { return static_cast<int32_t>(offsetof(BsonArray_t409BB62F0CCDED5660D1BABDA0BB2085FB09C915, ____children_2)); }
	inline List_1_t8966010566181DE22880EF5DF9C0CB53B4614363 * get__children_2() const { return ____children_2; }
	inline List_1_t8966010566181DE22880EF5DF9C0CB53B4614363 ** get_address_of__children_2() { return &____children_2; }
	inline void set__children_2(List_1_t8966010566181DE22880EF5DF9C0CB53B4614363 * value)
	{
		____children_2 = value;
		Il2CppCodeGenWriteBarrier((&____children_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONARRAY_T409BB62F0CCDED5660D1BABDA0BB2085FB09C915_H
#ifndef BSONOBJECT_TCC61A23F42600B1B6CA194FF0E549E688AAA45BB_H
#define BSONOBJECT_TCC61A23F42600B1B6CA194FF0E549E688AAA45BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonObject
struct  BsonObject_tCC61A23F42600B1B6CA194FF0E549E688AAA45BB  : public BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonProperty> Newtonsoft.Json.Bson.BsonObject::_children
	List_1_t23B22538D2FA378DA6D9B5BD5B84B060381E4748 * ____children_2;

public:
	inline static int32_t get_offset_of__children_2() { return static_cast<int32_t>(offsetof(BsonObject_tCC61A23F42600B1B6CA194FF0E549E688AAA45BB, ____children_2)); }
	inline List_1_t23B22538D2FA378DA6D9B5BD5B84B060381E4748 * get__children_2() const { return ____children_2; }
	inline List_1_t23B22538D2FA378DA6D9B5BD5B84B060381E4748 ** get_address_of__children_2() { return &____children_2; }
	inline void set__children_2(List_1_t23B22538D2FA378DA6D9B5BD5B84B060381E4748 * value)
	{
		____children_2 = value;
		Il2CppCodeGenWriteBarrier((&____children_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECT_TCC61A23F42600B1B6CA194FF0E549E688AAA45BB_H
#ifndef BSONREGEX_T4366470059BFDDDD190CAFC4E538DFB38D88468E_H
#define BSONREGEX_T4366470059BFDDDD190CAFC4E538DFB38D88468E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonRegex
struct  BsonRegex_t4366470059BFDDDD190CAFC4E538DFB38D88468E  : public BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973
{
public:
	// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonRegex::<Pattern>k__BackingField
	BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46 * ___U3CPatternU3Ek__BackingField_2;
	// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonRegex::<Options>k__BackingField
	BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46 * ___U3COptionsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CPatternU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BsonRegex_t4366470059BFDDDD190CAFC4E538DFB38D88468E, ___U3CPatternU3Ek__BackingField_2)); }
	inline BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46 * get_U3CPatternU3Ek__BackingField_2() const { return ___U3CPatternU3Ek__BackingField_2; }
	inline BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46 ** get_address_of_U3CPatternU3Ek__BackingField_2() { return &___U3CPatternU3Ek__BackingField_2; }
	inline void set_U3CPatternU3Ek__BackingField_2(BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46 * value)
	{
		___U3CPatternU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPatternU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3COptionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BsonRegex_t4366470059BFDDDD190CAFC4E538DFB38D88468E, ___U3COptionsU3Ek__BackingField_3)); }
	inline BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46 * get_U3COptionsU3Ek__BackingField_3() const { return ___U3COptionsU3Ek__BackingField_3; }
	inline BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46 ** get_address_of_U3COptionsU3Ek__BackingField_3() { return &___U3COptionsU3Ek__BackingField_3; }
	inline void set_U3COptionsU3Ek__BackingField_3(BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46 * value)
	{
		___U3COptionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3COptionsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONREGEX_T4366470059BFDDDD190CAFC4E538DFB38D88468E_H
#ifndef XATTRIBUTEWRAPPER_T560E9EF1F2D11F507C449C41A682293650E5C0D5_H
#define XATTRIBUTEWRAPPER_T560E9EF1F2D11F507C449C41A682293650E5C0D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XAttributeWrapper
struct  XAttributeWrapper_t560E9EF1F2D11F507C449C41A682293650E5C0D5  : public XObjectWrapper_t725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XATTRIBUTEWRAPPER_T560E9EF1F2D11F507C449C41A682293650E5C0D5_H
#ifndef XCOMMENTWRAPPER_TF6D511EEEC8190037957744ABC04D1C17B370A96_H
#define XCOMMENTWRAPPER_TF6D511EEEC8190037957744ABC04D1C17B370A96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XCommentWrapper
struct  XCommentWrapper_tF6D511EEEC8190037957744ABC04D1C17B370A96  : public XObjectWrapper_t725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCOMMENTWRAPPER_TF6D511EEEC8190037957744ABC04D1C17B370A96_H
#ifndef XCONTAINERWRAPPER_TF1D4A2616F408C9F225A2A2F92C75CBA72C347C0_H
#define XCONTAINERWRAPPER_TF1D4A2616F408C9F225A2A2F92C75CBA72C347C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XContainerWrapper
struct  XContainerWrapper_tF1D4A2616F408C9F225A2A2F92C75CBA72C347C0  : public XObjectWrapper_t725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XContainerWrapper::_childNodes
	List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * ____childNodes_2;

public:
	inline static int32_t get_offset_of__childNodes_2() { return static_cast<int32_t>(offsetof(XContainerWrapper_tF1D4A2616F408C9F225A2A2F92C75CBA72C347C0, ____childNodes_2)); }
	inline List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * get__childNodes_2() const { return ____childNodes_2; }
	inline List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 ** get_address_of__childNodes_2() { return &____childNodes_2; }
	inline void set__childNodes_2(List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * value)
	{
		____childNodes_2 = value;
		Il2CppCodeGenWriteBarrier((&____childNodes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCONTAINERWRAPPER_TF1D4A2616F408C9F225A2A2F92C75CBA72C347C0_H
#ifndef XDECLARATIONWRAPPER_T19BEE7F4C333B0A8EEAC25BDA6614C131ADE698B_H
#define XDECLARATIONWRAPPER_T19BEE7F4C333B0A8EEAC25BDA6614C131ADE698B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XDeclarationWrapper
struct  XDeclarationWrapper_t19BEE7F4C333B0A8EEAC25BDA6614C131ADE698B  : public XObjectWrapper_t725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A
{
public:
	// System.Xml.Linq.XDeclaration Newtonsoft.Json.Converters.XDeclarationWrapper::<Declaration>k__BackingField
	XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 * ___U3CDeclarationU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CDeclarationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XDeclarationWrapper_t19BEE7F4C333B0A8EEAC25BDA6614C131ADE698B, ___U3CDeclarationU3Ek__BackingField_2)); }
	inline XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 * get_U3CDeclarationU3Ek__BackingField_2() const { return ___U3CDeclarationU3Ek__BackingField_2; }
	inline XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 ** get_address_of_U3CDeclarationU3Ek__BackingField_2() { return &___U3CDeclarationU3Ek__BackingField_2; }
	inline void set_U3CDeclarationU3Ek__BackingField_2(XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 * value)
	{
		___U3CDeclarationU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeclarationU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDECLARATIONWRAPPER_T19BEE7F4C333B0A8EEAC25BDA6614C131ADE698B_H
#ifndef XDOCUMENTTYPEWRAPPER_T594BC04853709973E3A7CEE68502A1D6256DF7AD_H
#define XDOCUMENTTYPEWRAPPER_T594BC04853709973E3A7CEE68502A1D6256DF7AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XDocumentTypeWrapper
struct  XDocumentTypeWrapper_t594BC04853709973E3A7CEE68502A1D6256DF7AD  : public XObjectWrapper_t725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A
{
public:
	// System.Xml.Linq.XDocumentType Newtonsoft.Json.Converters.XDocumentTypeWrapper::_documentType
	XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444 * ____documentType_2;

public:
	inline static int32_t get_offset_of__documentType_2() { return static_cast<int32_t>(offsetof(XDocumentTypeWrapper_t594BC04853709973E3A7CEE68502A1D6256DF7AD, ____documentType_2)); }
	inline XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444 * get__documentType_2() const { return ____documentType_2; }
	inline XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444 ** get_address_of__documentType_2() { return &____documentType_2; }
	inline void set__documentType_2(XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444 * value)
	{
		____documentType_2 = value;
		Il2CppCodeGenWriteBarrier((&____documentType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDOCUMENTTYPEWRAPPER_T594BC04853709973E3A7CEE68502A1D6256DF7AD_H
#ifndef XPROCESSINGINSTRUCTIONWRAPPER_T6951DEA9256CEE1E71C353D0BB1B20C34E85B4EA_H
#define XPROCESSINGINSTRUCTIONWRAPPER_T6951DEA9256CEE1E71C353D0BB1B20C34E85B4EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XProcessingInstructionWrapper
struct  XProcessingInstructionWrapper_t6951DEA9256CEE1E71C353D0BB1B20C34E85B4EA  : public XObjectWrapper_t725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPROCESSINGINSTRUCTIONWRAPPER_T6951DEA9256CEE1E71C353D0BB1B20C34E85B4EA_H
#ifndef XTEXTWRAPPER_T0C09B42E98EB742AF5813BDEB9230C3DD113CD90_H
#define XTEXTWRAPPER_T0C09B42E98EB742AF5813BDEB9230C3DD113CD90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XTextWrapper
struct  XTextWrapper_t0C09B42E98EB742AF5813BDEB9230C3DD113CD90  : public XObjectWrapper_t725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XTEXTWRAPPER_T0C09B42E98EB742AF5813BDEB9230C3DD113CD90_H
#ifndef XMLDOCUMENTTYPEWRAPPER_T580823554B71FD5205C8D8177B0EB1B339BC0BDF_H
#define XMLDOCUMENTTYPEWRAPPER_T580823554B71FD5205C8D8177B0EB1B339BC0BDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlDocumentTypeWrapper
struct  XmlDocumentTypeWrapper_t580823554B71FD5205C8D8177B0EB1B339BC0BDF  : public XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8
{
public:
	// System.Xml.XmlDocumentType Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::_documentType
	XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136 * ____documentType_3;

public:
	inline static int32_t get_offset_of__documentType_3() { return static_cast<int32_t>(offsetof(XmlDocumentTypeWrapper_t580823554B71FD5205C8D8177B0EB1B339BC0BDF, ____documentType_3)); }
	inline XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136 * get__documentType_3() const { return ____documentType_3; }
	inline XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136 ** get_address_of__documentType_3() { return &____documentType_3; }
	inline void set__documentType_3(XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136 * value)
	{
		____documentType_3 = value;
		Il2CppCodeGenWriteBarrier((&____documentType_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTTYPEWRAPPER_T580823554B71FD5205C8D8177B0EB1B339BC0BDF_H
#ifndef XMLNODECONVERTER_T12A0E794803AB63F65E00D245B7EE3C4C8CEB304_H
#define XMLNODECONVERTER_T12A0E794803AB63F65E00D245B7EE3C4C8CEB304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlNodeConverter
struct  XmlNodeConverter_t12A0E794803AB63F65E00D245B7EE3C4C8CEB304  : public JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59
{
public:
	// System.String Newtonsoft.Json.Converters.XmlNodeConverter::<DeserializeRootElementName>k__BackingField
	String_t* ___U3CDeserializeRootElementNameU3Ek__BackingField_0;
	// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::<WriteArrayAttribute>k__BackingField
	bool ___U3CWriteArrayAttributeU3Ek__BackingField_1;
	// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::<OmitRootObject>k__BackingField
	bool ___U3COmitRootObjectU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CDeserializeRootElementNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t12A0E794803AB63F65E00D245B7EE3C4C8CEB304, ___U3CDeserializeRootElementNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CDeserializeRootElementNameU3Ek__BackingField_0() const { return ___U3CDeserializeRootElementNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CDeserializeRootElementNameU3Ek__BackingField_0() { return &___U3CDeserializeRootElementNameU3Ek__BackingField_0; }
	inline void set_U3CDeserializeRootElementNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CDeserializeRootElementNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeserializeRootElementNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CWriteArrayAttributeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t12A0E794803AB63F65E00D245B7EE3C4C8CEB304, ___U3CWriteArrayAttributeU3Ek__BackingField_1)); }
	inline bool get_U3CWriteArrayAttributeU3Ek__BackingField_1() const { return ___U3CWriteArrayAttributeU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CWriteArrayAttributeU3Ek__BackingField_1() { return &___U3CWriteArrayAttributeU3Ek__BackingField_1; }
	inline void set_U3CWriteArrayAttributeU3Ek__BackingField_1(bool value)
	{
		___U3CWriteArrayAttributeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3COmitRootObjectU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t12A0E794803AB63F65E00D245B7EE3C4C8CEB304, ___U3COmitRootObjectU3Ek__BackingField_2)); }
	inline bool get_U3COmitRootObjectU3Ek__BackingField_2() const { return ___U3COmitRootObjectU3Ek__BackingField_2; }
	inline bool* get_address_of_U3COmitRootObjectU3Ek__BackingField_2() { return &___U3COmitRootObjectU3Ek__BackingField_2; }
	inline void set_U3COmitRootObjectU3Ek__BackingField_2(bool value)
	{
		___U3COmitRootObjectU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECONVERTER_T12A0E794803AB63F65E00D245B7EE3C4C8CEB304_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef SUBSYSTEMDESCRIPTOR_1_T8BC7D01A6BF03D22D133B8FB58E5B2D85675FDD7_H
#define SUBSYSTEMDESCRIPTOR_1_T8BC7D01A6BF03D22D133B8FB58E5B2D85675FDD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.SubsystemDescriptor`1<UnityEngine.XR.FaceSubsystem.XRFaceSubsystem>
struct  SubsystemDescriptor_1_t8BC7D01A6BF03D22D133B8FB58E5B2D85675FDD7  : public SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEMDESCRIPTOR_1_T8BC7D01A6BF03D22D133B8FB58E5B2D85675FDD7_H
#ifndef SUBSYSTEM_1_T7A31187B2E3DFF03501265C781C9A8547A073F0C_H
#define SUBSYSTEM_1_T7A31187B2E3DFF03501265C781C9A8547A073F0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Subsystem`1<UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor>
struct  Subsystem_1_t7A31187B2E3DFF03501265C781C9A8547A073F0C  : public Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEM_1_T7A31187B2E3DFF03501265C781C9A8547A073F0C_H
#ifndef TRACKABLEID_TA539F57E82A04D410FE11E10ACC830CF7CD71F7B_H
#define TRACKABLEID_TA539F57E82A04D410FE11E10ACC830CF7CD71F7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.TrackableId
struct  TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B 
{
public:
	// System.UInt64 UnityEngine.Experimental.XR.TrackableId::m_SubId1
	uint64_t ___m_SubId1_1;
	// System.UInt64 UnityEngine.Experimental.XR.TrackableId::m_SubId2
	uint64_t ___m_SubId2_2;

public:
	inline static int32_t get_offset_of_m_SubId1_1() { return static_cast<int32_t>(offsetof(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B, ___m_SubId1_1)); }
	inline uint64_t get_m_SubId1_1() const { return ___m_SubId1_1; }
	inline uint64_t* get_address_of_m_SubId1_1() { return &___m_SubId1_1; }
	inline void set_m_SubId1_1(uint64_t value)
	{
		___m_SubId1_1 = value;
	}

	inline static int32_t get_offset_of_m_SubId2_2() { return static_cast<int32_t>(offsetof(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B, ___m_SubId2_2)); }
	inline uint64_t get_m_SubId2_2() const { return ___m_SubId2_2; }
	inline uint64_t* get_address_of_m_SubId2_2() { return &___m_SubId2_2; }
	inline void set_m_SubId2_2(uint64_t value)
	{
		___m_SubId2_2 = value;
	}
};

struct TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B_StaticFields
{
public:
	// UnityEngine.Experimental.XR.TrackableId UnityEngine.Experimental.XR.TrackableId::s_InvalidId
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___s_InvalidId_0;

public:
	inline static int32_t get_offset_of_s_InvalidId_0() { return static_cast<int32_t>(offsetof(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B_StaticFields, ___s_InvalidId_0)); }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  get_s_InvalidId_0() const { return ___s_InvalidId_0; }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B * get_address_of_s_InvalidId_0() { return &___s_InvalidId_0; }
	inline void set_s_InvalidId_0(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  value)
	{
		___s_InvalidId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEID_TA539F57E82A04D410FE11E10ACC830CF7CD71F7B_H
#ifndef PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#define PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef RECTINT_T595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A_H
#define RECTINT_T595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectInt
struct  RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A 
{
public:
	// System.Int32 UnityEngine.RectInt::m_XMin
	int32_t ___m_XMin_0;
	// System.Int32 UnityEngine.RectInt::m_YMin
	int32_t ___m_YMin_1;
	// System.Int32 UnityEngine.RectInt::m_Width
	int32_t ___m_Width_2;
	// System.Int32 UnityEngine.RectInt::m_Height
	int32_t ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A, ___m_XMin_0)); }
	inline int32_t get_m_XMin_0() const { return ___m_XMin_0; }
	inline int32_t* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(int32_t value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A, ___m_YMin_1)); }
	inline int32_t get_m_YMin_1() const { return ___m_YMin_1; }
	inline int32_t* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(int32_t value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A, ___m_Width_2)); }
	inline int32_t get_m_Width_2() const { return ___m_Width_2; }
	inline int32_t* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(int32_t value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A, ___m_Height_3)); }
	inline int32_t get_m_Height_3() const { return ___m_Height_3; }
	inline int32_t* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(int32_t value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTINT_T595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A_H
#ifndef POSEDATA_TF79A33767571168AAB333A85A6B6F0F9A8825DFE_H
#define POSEDATA_TF79A33767571168AAB333A85A6B6F0F9A8825DFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData
struct  PoseData_tF79A33767571168AAB333A85A6B6F0F9A8825DFE 
{
public:
	// System.Collections.Generic.List`1<System.String> UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData::PoseNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___PoseNames_0;
	// System.Collections.Generic.List`1<UnityEngine.SpatialTracking.TrackedPoseDriver/TrackedPose> UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData::Poses
	List_1_t894CB1A35FB018A041CA1B49CF3AB136E531995A * ___Poses_1;

public:
	inline static int32_t get_offset_of_PoseNames_0() { return static_cast<int32_t>(offsetof(PoseData_tF79A33767571168AAB333A85A6B6F0F9A8825DFE, ___PoseNames_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_PoseNames_0() const { return ___PoseNames_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_PoseNames_0() { return &___PoseNames_0; }
	inline void set_PoseNames_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___PoseNames_0 = value;
		Il2CppCodeGenWriteBarrier((&___PoseNames_0), value);
	}

	inline static int32_t get_offset_of_Poses_1() { return static_cast<int32_t>(offsetof(PoseData_tF79A33767571168AAB333A85A6B6F0F9A8825DFE, ___Poses_1)); }
	inline List_1_t894CB1A35FB018A041CA1B49CF3AB136E531995A * get_Poses_1() const { return ___Poses_1; }
	inline List_1_t894CB1A35FB018A041CA1B49CF3AB136E531995A ** get_address_of_Poses_1() { return &___Poses_1; }
	inline void set_Poses_1(List_1_t894CB1A35FB018A041CA1B49CF3AB136E531995A * value)
	{
		___Poses_1 = value;
		Il2CppCodeGenWriteBarrier((&___Poses_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData
struct PoseData_tF79A33767571168AAB333A85A6B6F0F9A8825DFE_marshaled_pinvoke
{
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___PoseNames_0;
	List_1_t894CB1A35FB018A041CA1B49CF3AB136E531995A * ___Poses_1;
};
// Native definition for COM marshalling of UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData
struct PoseData_tF79A33767571168AAB333A85A6B6F0F9A8825DFE_marshaled_com
{
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___PoseNames_0;
	List_1_t894CB1A35FB018A041CA1B49CF3AB136E531995A * ___Poses_1;
};
#endif // POSEDATA_TF79A33767571168AAB333A85A6B6F0F9A8825DFE_H
#ifndef VECTOR2INT_T339DA203C037FA6BCFC926C36DC2194D52D5F905_H
#define VECTOR2INT_T339DA203C037FA6BCFC926C36DC2194D52D5F905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2Int
struct  Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 
{
public:
	// System.Int32 UnityEngine.Vector2Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector2Int::m_Y
	int32_t ___m_Y_1;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}
};

struct Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields
{
public:
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Zero
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Zero_2;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_One
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_One_3;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Up
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Up_4;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Down
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Down_5;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Left
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Left_6;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Right
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Right_7;

public:
	inline static int32_t get_offset_of_s_Zero_2() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Zero_2)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Zero_2() const { return ___s_Zero_2; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Zero_2() { return &___s_Zero_2; }
	inline void set_s_Zero_2(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Zero_2 = value;
	}

	inline static int32_t get_offset_of_s_One_3() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_One_3)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_One_3() const { return ___s_One_3; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_One_3() { return &___s_One_3; }
	inline void set_s_One_3(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_One_3 = value;
	}

	inline static int32_t get_offset_of_s_Up_4() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Up_4)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Up_4() const { return ___s_Up_4; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Up_4() { return &___s_Up_4; }
	inline void set_s_Up_4(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Up_4 = value;
	}

	inline static int32_t get_offset_of_s_Down_5() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Down_5)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Down_5() const { return ___s_Down_5; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Down_5() { return &___s_Down_5; }
	inline void set_s_Down_5(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Down_5 = value;
	}

	inline static int32_t get_offset_of_s_Left_6() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Left_6)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Left_6() const { return ___s_Left_6; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Left_6() { return &___s_Left_6; }
	inline void set_s_Left_6(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Left_6 = value;
	}

	inline static int32_t get_offset_of_s_Right_7() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Right_7)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Right_7() const { return ___s_Right_7; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Right_7() { return &___s_Right_7; }
	inline void set_s_Right_7(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Right_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2INT_T339DA203C037FA6BCFC926C36DC2194D52D5F905_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef CAMERACONFIGURATIONCOLLECTION_T32228119EE1DD23BE336BD6F1C49B339B10DB618_H
#define CAMERACONFIGURATIONCOLLECTION_T32228119EE1DD23BE336BD6F1C49B339B10DB618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.CameraConfigurationCollection
struct  CameraConfigurationCollection_t32228119EE1DD23BE336BD6F1C49B339B10DB618 
{
public:
	// UnityEngine.XR.ARExtensions.ICameraConfigApi UnityEngine.XR.ARExtensions.CameraConfigurationCollection::m_CameraConfigApi
	RuntimeObject* ___m_CameraConfigApi_0;

public:
	inline static int32_t get_offset_of_m_CameraConfigApi_0() { return static_cast<int32_t>(offsetof(CameraConfigurationCollection_t32228119EE1DD23BE336BD6F1C49B339B10DB618, ___m_CameraConfigApi_0)); }
	inline RuntimeObject* get_m_CameraConfigApi_0() const { return ___m_CameraConfigApi_0; }
	inline RuntimeObject** get_address_of_m_CameraConfigApi_0() { return &___m_CameraConfigApi_0; }
	inline void set_m_CameraConfigApi_0(RuntimeObject* value)
	{
		___m_CameraConfigApi_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraConfigApi_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.ARExtensions.CameraConfigurationCollection
struct CameraConfigurationCollection_t32228119EE1DD23BE336BD6F1C49B339B10DB618_marshaled_pinvoke
{
	RuntimeObject* ___m_CameraConfigApi_0;
};
// Native definition for COM marshalling of UnityEngine.XR.ARExtensions.CameraConfigurationCollection
struct CameraConfigurationCollection_t32228119EE1DD23BE336BD6F1C49B339B10DB618_marshaled_com
{
	RuntimeObject* ___m_CameraConfigApi_0;
};
#endif // CAMERACONFIGURATIONCOLLECTION_T32228119EE1DD23BE336BD6F1C49B339B10DB618_H
#ifndef CONFIGENUMERATOR_TF8562092447F19D8DC60A0CEC6355037826B652F_H
#define CONFIGENUMERATOR_TF8562092447F19D8DC60A0CEC6355037826B652F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.CameraConfigurationCollection/ConfigEnumerator
struct  ConfigEnumerator_tF8562092447F19D8DC60A0CEC6355037826B652F 
{
public:
	// System.Int32 UnityEngine.XR.ARExtensions.CameraConfigurationCollection/ConfigEnumerator::m_Index
	int32_t ___m_Index_0;
	// System.Int32 UnityEngine.XR.ARExtensions.CameraConfigurationCollection/ConfigEnumerator::m_Count
	int32_t ___m_Count_1;
	// UnityEngine.XR.ARExtensions.ICameraConfigApi UnityEngine.XR.ARExtensions.CameraConfigurationCollection/ConfigEnumerator::m_CameraConfigApi
	RuntimeObject* ___m_CameraConfigApi_2;

public:
	inline static int32_t get_offset_of_m_Index_0() { return static_cast<int32_t>(offsetof(ConfigEnumerator_tF8562092447F19D8DC60A0CEC6355037826B652F, ___m_Index_0)); }
	inline int32_t get_m_Index_0() const { return ___m_Index_0; }
	inline int32_t* get_address_of_m_Index_0() { return &___m_Index_0; }
	inline void set_m_Index_0(int32_t value)
	{
		___m_Index_0 = value;
	}

	inline static int32_t get_offset_of_m_Count_1() { return static_cast<int32_t>(offsetof(ConfigEnumerator_tF8562092447F19D8DC60A0CEC6355037826B652F, ___m_Count_1)); }
	inline int32_t get_m_Count_1() const { return ___m_Count_1; }
	inline int32_t* get_address_of_m_Count_1() { return &___m_Count_1; }
	inline void set_m_Count_1(int32_t value)
	{
		___m_Count_1 = value;
	}

	inline static int32_t get_offset_of_m_CameraConfigApi_2() { return static_cast<int32_t>(offsetof(ConfigEnumerator_tF8562092447F19D8DC60A0CEC6355037826B652F, ___m_CameraConfigApi_2)); }
	inline RuntimeObject* get_m_CameraConfigApi_2() const { return ___m_CameraConfigApi_2; }
	inline RuntimeObject** get_address_of_m_CameraConfigApi_2() { return &___m_CameraConfigApi_2; }
	inline void set_m_CameraConfigApi_2(RuntimeObject* value)
	{
		___m_CameraConfigApi_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraConfigApi_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.ARExtensions.CameraConfigurationCollection/ConfigEnumerator
struct ConfigEnumerator_tF8562092447F19D8DC60A0CEC6355037826B652F_marshaled_pinvoke
{
	int32_t ___m_Index_0;
	int32_t ___m_Count_1;
	RuntimeObject* ___m_CameraConfigApi_2;
};
// Native definition for COM marshalling of UnityEngine.XR.ARExtensions.CameraConfigurationCollection/ConfigEnumerator
struct ConfigEnumerator_tF8562092447F19D8DC60A0CEC6355037826B652F_marshaled_com
{
	int32_t ___m_Index_0;
	int32_t ___m_Count_1;
	RuntimeObject* ___m_CameraConfigApi_2;
};
#endif // CONFIGENUMERATOR_TF8562092447F19D8DC60A0CEC6355037826B652F_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TBBC8E11E0053333620B8C574BE27D9CD62A99591_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TBBC8E11E0053333620B8C574BE27D9CD62A99591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tBBC8E11E0053333620B8C574BE27D9CD62A99591  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tBBC8E11E0053333620B8C574BE27D9CD62A99591_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::9E31F24F64765FCAA589F589324D17C9FCF6A06D
	__StaticArrayInitTypeSizeU3D28_tD95E73C8180D450CB90FBFA3367F31DECB46B9A8  ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::ADFD2E1C801C825415DD53F4F2F72A13B389313C
	__StaticArrayInitTypeSizeU3D12_t6A42B5D9C1B6EC123AB21D43DAB4814988F7FE5A  ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10 <PrivateImplementationDetails>::D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB
	__StaticArrayInitTypeSizeU3D10_t2210C3858CD7BBD086B8C29DD7AE0B07CF930F6E  ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::DD3AEFEADB1CD615F3017763F1568179FEE640B0
	__StaticArrayInitTypeSizeU3D52_tA3556BE2FDC9141F7C4FB86611890DADE17CDD63  ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::E92B39D8233061927D9ACDE54665E68E7535635A
	__StaticArrayInitTypeSizeU3D52_tA3556BE2FDC9141F7C4FB86611890DADE17CDD63  ___E92B39D8233061927D9ACDE54665E68E7535635A_4;

public:
	inline static int32_t get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBBC8E11E0053333620B8C574BE27D9CD62A99591_StaticFields, ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0)); }
	inline __StaticArrayInitTypeSizeU3D28_tD95E73C8180D450CB90FBFA3367F31DECB46B9A8  get_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0() const { return ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0; }
	inline __StaticArrayInitTypeSizeU3D28_tD95E73C8180D450CB90FBFA3367F31DECB46B9A8 * get_address_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0() { return &___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0; }
	inline void set_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0(__StaticArrayInitTypeSizeU3D28_tD95E73C8180D450CB90FBFA3367F31DECB46B9A8  value)
	{
		___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0 = value;
	}

	inline static int32_t get_offset_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBBC8E11E0053333620B8C574BE27D9CD62A99591_StaticFields, ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1)); }
	inline __StaticArrayInitTypeSizeU3D12_t6A42B5D9C1B6EC123AB21D43DAB4814988F7FE5A  get_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1() const { return ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1; }
	inline __StaticArrayInitTypeSizeU3D12_t6A42B5D9C1B6EC123AB21D43DAB4814988F7FE5A * get_address_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1() { return &___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1; }
	inline void set_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1(__StaticArrayInitTypeSizeU3D12_t6A42B5D9C1B6EC123AB21D43DAB4814988F7FE5A  value)
	{
		___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1 = value;
	}

	inline static int32_t get_offset_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBBC8E11E0053333620B8C574BE27D9CD62A99591_StaticFields, ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2)); }
	inline __StaticArrayInitTypeSizeU3D10_t2210C3858CD7BBD086B8C29DD7AE0B07CF930F6E  get_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2() const { return ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2; }
	inline __StaticArrayInitTypeSizeU3D10_t2210C3858CD7BBD086B8C29DD7AE0B07CF930F6E * get_address_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2() { return &___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2; }
	inline void set_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2(__StaticArrayInitTypeSizeU3D10_t2210C3858CD7BBD086B8C29DD7AE0B07CF930F6E  value)
	{
		___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2 = value;
	}

	inline static int32_t get_offset_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBBC8E11E0053333620B8C574BE27D9CD62A99591_StaticFields, ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3)); }
	inline __StaticArrayInitTypeSizeU3D52_tA3556BE2FDC9141F7C4FB86611890DADE17CDD63  get_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3() const { return ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3; }
	inline __StaticArrayInitTypeSizeU3D52_tA3556BE2FDC9141F7C4FB86611890DADE17CDD63 * get_address_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3() { return &___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3; }
	inline void set_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3(__StaticArrayInitTypeSizeU3D52_tA3556BE2FDC9141F7C4FB86611890DADE17CDD63  value)
	{
		___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3 = value;
	}

	inline static int32_t get_offset_of_E92B39D8233061927D9ACDE54665E68E7535635A_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBBC8E11E0053333620B8C574BE27D9CD62A99591_StaticFields, ___E92B39D8233061927D9ACDE54665E68E7535635A_4)); }
	inline __StaticArrayInitTypeSizeU3D52_tA3556BE2FDC9141F7C4FB86611890DADE17CDD63  get_E92B39D8233061927D9ACDE54665E68E7535635A_4() const { return ___E92B39D8233061927D9ACDE54665E68E7535635A_4; }
	inline __StaticArrayInitTypeSizeU3D52_tA3556BE2FDC9141F7C4FB86611890DADE17CDD63 * get_address_of_E92B39D8233061927D9ACDE54665E68E7535635A_4() { return &___E92B39D8233061927D9ACDE54665E68E7535635A_4; }
	inline void set_E92B39D8233061927D9ACDE54665E68E7535635A_4(__StaticArrayInitTypeSizeU3D52_tA3556BE2FDC9141F7C4FB86611890DADE17CDD63  value)
	{
		___E92B39D8233061927D9ACDE54665E68E7535635A_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TBBC8E11E0053333620B8C574BE27D9CD62A99591_H
#ifndef BSONBINARYTYPE_T1FA8FE0696F46BBDF161A97E2AB1D1AF726B9858_H
#define BSONBINARYTYPE_T1FA8FE0696F46BBDF161A97E2AB1D1AF726B9858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBinaryType
struct  BsonBinaryType_t1FA8FE0696F46BBDF161A97E2AB1D1AF726B9858 
{
public:
	// System.Byte Newtonsoft.Json.Bson.BsonBinaryType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BsonBinaryType_t1FA8FE0696F46BBDF161A97E2AB1D1AF726B9858, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARYTYPE_T1FA8FE0696F46BBDF161A97E2AB1D1AF726B9858_H
#ifndef BSONTYPE_T7BD027216E37FF11157FAA220E1E48019CF823D1_H
#define BSONTYPE_T7BD027216E37FF11157FAA220E1E48019CF823D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonType
struct  BsonType_t7BD027216E37FF11157FAA220E1E48019CF823D1 
{
public:
	// System.SByte Newtonsoft.Json.Bson.BsonType::value__
	int8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BsonType_t7BD027216E37FF11157FAA220E1E48019CF823D1, ___value___2)); }
	inline int8_t get_value___2() const { return ___value___2; }
	inline int8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONTYPE_T7BD027216E37FF11157FAA220E1E48019CF823D1_H
#ifndef XDOCUMENTWRAPPER_T715FF948E28684D8586F3AA249CC48E112921363_H
#define XDOCUMENTWRAPPER_T715FF948E28684D8586F3AA249CC48E112921363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XDocumentWrapper
struct  XDocumentWrapper_t715FF948E28684D8586F3AA249CC48E112921363  : public XContainerWrapper_tF1D4A2616F408C9F225A2A2F92C75CBA72C347C0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDOCUMENTWRAPPER_T715FF948E28684D8586F3AA249CC48E112921363_H
#ifndef XELEMENTWRAPPER_T5BDE61B8CE601DD94A50452AFAD7E88A969EA2D7_H
#define XELEMENTWRAPPER_T5BDE61B8CE601DD94A50452AFAD7E88A969EA2D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XElementWrapper
struct  XElementWrapper_t5BDE61B8CE601DD94A50452AFAD7E88A969EA2D7  : public XContainerWrapper_tF1D4A2616F408C9F225A2A2F92C75CBA72C347C0
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XElementWrapper::_attributes
	List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * ____attributes_3;

public:
	inline static int32_t get_offset_of__attributes_3() { return static_cast<int32_t>(offsetof(XElementWrapper_t5BDE61B8CE601DD94A50452AFAD7E88A969EA2D7, ____attributes_3)); }
	inline List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * get__attributes_3() const { return ____attributes_3; }
	inline List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 ** get_address_of__attributes_3() { return &____attributes_3; }
	inline void set__attributes_3(List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * value)
	{
		____attributes_3 = value;
		Il2CppCodeGenWriteBarrier((&____attributes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XELEMENTWRAPPER_T5BDE61B8CE601DD94A50452AFAD7E88A969EA2D7_H
#ifndef DATEFORMATHANDLING_T0D54266C0C7CD81612959E7CB8174F6F8AA21424_H
#define DATEFORMATHANDLING_T0D54266C0C7CD81612959E7CB8174F6F8AA21424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateFormatHandling
struct  DateFormatHandling_t0D54266C0C7CD81612959E7CB8174F6F8AA21424 
{
public:
	// System.Int32 Newtonsoft.Json.DateFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateFormatHandling_t0D54266C0C7CD81612959E7CB8174F6F8AA21424, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEFORMATHANDLING_T0D54266C0C7CD81612959E7CB8174F6F8AA21424_H
#ifndef DATETIMEZONEHANDLING_T4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD_H
#define DATETIMEZONEHANDLING_T4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateTimeZoneHandling
struct  DateTimeZoneHandling_t4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD 
{
public:
	// System.Int32 Newtonsoft.Json.DateTimeZoneHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeZoneHandling_t4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEZONEHANDLING_T4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD_H
#ifndef FLOATFORMATHANDLING_TE78C0C14E4CFE64BDF71370419573A3ED8B68289_H
#define FLOATFORMATHANDLING_TE78C0C14E4CFE64BDF71370419573A3ED8B68289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatFormatHandling
struct  FloatFormatHandling_tE78C0C14E4CFE64BDF71370419573A3ED8B68289 
{
public:
	// System.Int32 Newtonsoft.Json.FloatFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatFormatHandling_tE78C0C14E4CFE64BDF71370419573A3ED8B68289, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFORMATHANDLING_TE78C0C14E4CFE64BDF71370419573A3ED8B68289_H
#ifndef FORMATTING_TE2524DB9105FC27E1B956F5AF5F211981B22E3E1_H
#define FORMATTING_TE2524DB9105FC27E1B956F5AF5F211981B22E3E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Formatting
struct  Formatting_tE2524DB9105FC27E1B956F5AF5F211981B22E3E1 
{
public:
	// System.Int32 Newtonsoft.Json.Formatting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Formatting_tE2524DB9105FC27E1B956F5AF5F211981B22E3E1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_TE2524DB9105FC27E1B956F5AF5F211981B22E3E1_H
#ifndef JSONCONTAINERTYPE_T1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6_H
#define JSONCONTAINERTYPE_T1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerType
struct  JsonContainerType_t1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6 
{
public:
	// System.Int32 Newtonsoft.Json.JsonContainerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonContainerType_t1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERTYPE_T1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6_H
#ifndef STATE_T1DE392B52BABD9BF376B03FE41F672F7113AE11F_H
#define STATE_T1DE392B52BABD9BF376B03FE41F672F7113AE11F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter/State
struct  State_t1DE392B52BABD9BF376B03FE41F672F7113AE11F 
{
public:
	// System.Int32 Newtonsoft.Json.JsonWriter/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t1DE392B52BABD9BF376B03FE41F672F7113AE11F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T1DE392B52BABD9BF376B03FE41F672F7113AE11F_H
#ifndef STRINGESCAPEHANDLING_T3E67D3B0A780B24A2D57DF8921B9438CC507029A_H
#define STRINGESCAPEHANDLING_T3E67D3B0A780B24A2D57DF8921B9438CC507029A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.StringEscapeHandling
struct  StringEscapeHandling_t3E67D3B0A780B24A2D57DF8921B9438CC507029A 
{
public:
	// System.Int32 Newtonsoft.Json.StringEscapeHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StringEscapeHandling_t3E67D3B0A780B24A2D57DF8921B9438CC507029A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGESCAPEHANDLING_T3E67D3B0A780B24A2D57DF8921B9438CC507029A_H
#ifndef DATETIMEKIND_T6BC23532930B812ABFCCEB2B61BC233712B180EE_H
#define DATETIMEKIND_T6BC23532930B812ABFCCEB2B61BC233712B180EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t6BC23532930B812ABFCCEB2B61BC233712B180EE 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeKind_t6BC23532930B812ABFCCEB2B61BC233712B180EE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T6BC23532930B812ABFCCEB2B61BC233712B180EE_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef EVENTHANDLE_TF6428A551850EC70E06F4140A2D3121C4B0DC64E_H
#define EVENTHANDLE_TF6428A551850EC70E06F4140A2D3121C4B0DC64E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventHandle
struct  EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventHandle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLE_TF6428A551850EC70E06F4140A2D3121C4B0DC64E_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#define POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Pose
struct  Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 
{
public:
	// UnityEngine.Vector3 UnityEngine.Pose::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Quaternion UnityEngine.Pose::rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___rotation_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rotation_1() const { return ___rotation_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rotation_1 = value;
	}
};

struct Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields
{
public:
	// UnityEngine.Pose UnityEngine.Pose::k_Identity
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___k_Identity_2;

public:
	inline static int32_t get_offset_of_k_Identity_2() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields, ___k_Identity_2)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_k_Identity_2() const { return ___k_Identity_2; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_k_Identity_2() { return &___k_Identity_2; }
	inline void set_k_Identity_2(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___k_Identity_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#ifndef DEVICETYPE_TC20CDCE75CB9BCB7EEF098A83B16071A0C86D198_H
#define DEVICETYPE_TC20CDCE75CB9BCB7EEF098A83B16071A0C86D198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpatialTracking.TrackedPoseDriver/DeviceType
struct  DeviceType_tC20CDCE75CB9BCB7EEF098A83B16071A0C86D198 
{
public:
	// System.Int32 UnityEngine.SpatialTracking.TrackedPoseDriver/DeviceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeviceType_tC20CDCE75CB9BCB7EEF098A83B16071A0C86D198, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICETYPE_TC20CDCE75CB9BCB7EEF098A83B16071A0C86D198_H
#ifndef TRACKEDPOSE_TBECB0ED5440415EE75C061C0588F16CF0D8E5DFA_H
#define TRACKEDPOSE_TBECB0ED5440415EE75C061C0588F16CF0D8E5DFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpatialTracking.TrackedPoseDriver/TrackedPose
struct  TrackedPose_tBECB0ED5440415EE75C061C0588F16CF0D8E5DFA 
{
public:
	// System.Int32 UnityEngine.SpatialTracking.TrackedPoseDriver/TrackedPose::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackedPose_tBECB0ED5440415EE75C061C0588F16CF0D8E5DFA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKEDPOSE_TBECB0ED5440415EE75C061C0588F16CF0D8E5DFA_H
#ifndef TRACKINGTYPE_T54A99FD46DD997F9BDD94EB51082AEA72640B28A_H
#define TRACKINGTYPE_T54A99FD46DD997F9BDD94EB51082AEA72640B28A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpatialTracking.TrackedPoseDriver/TrackingType
struct  TrackingType_t54A99FD46DD997F9BDD94EB51082AEA72640B28A 
{
public:
	// System.Int32 UnityEngine.SpatialTracking.TrackedPoseDriver/TrackingType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingType_t54A99FD46DD997F9BDD94EB51082AEA72640B28A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGTYPE_T54A99FD46DD997F9BDD94EB51082AEA72640B28A_H
#ifndef UPDATETYPE_T2E40BE3577C3CE3CB868167C75BA759A2B76BB10_H
#define UPDATETYPE_T2E40BE3577C3CE3CB868167C75BA759A2B76BB10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpatialTracking.TrackedPoseDriver/UpdateType
struct  UpdateType_t2E40BE3577C3CE3CB868167C75BA759A2B76BB10 
{
public:
	// System.Int32 UnityEngine.SpatialTracking.TrackedPoseDriver/UpdateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateType_t2E40BE3577C3CE3CB868167C75BA759A2B76BB10, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T2E40BE3577C3CE3CB868167C75BA759A2B76BB10_H
#ifndef TEXTUREFORMAT_T7C6B5101554065C47682E592D1E26079D4EC2DCE_H
#define TEXTUREFORMAT_T7C6B5101554065C47682E592D1E26079D4EC2DCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T7C6B5101554065C47682E592D1E26079D4EC2DCE_H
#ifndef ASYNCCAMERAIMAGECONVERSIONSTATUS_TF869FB2C28DB2729AAC068D746FBDF9076725ECA_H
#define ASYNCCAMERAIMAGECONVERSIONSTATUS_TF869FB2C28DB2729AAC068D746FBDF9076725ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.AsyncCameraImageConversionStatus
struct  AsyncCameraImageConversionStatus_tF869FB2C28DB2729AAC068D746FBDF9076725ECA 
{
public:
	// System.Int32 UnityEngine.XR.ARExtensions.AsyncCameraImageConversionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AsyncCameraImageConversionStatus_tF869FB2C28DB2729AAC068D746FBDF9076725ECA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCAMERAIMAGECONVERSIONSTATUS_TF869FB2C28DB2729AAC068D746FBDF9076725ECA_H
#ifndef CAMERACONFIGURATION_TF2E20F6B31C6014A39C87E6C430247D1B0F3BDD7_H
#define CAMERACONFIGURATION_TF2E20F6B31C6014A39C87E6C430247D1B0F3BDD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.CameraConfiguration
struct  CameraConfiguration_tF2E20F6B31C6014A39C87E6C430247D1B0F3BDD7 
{
public:
	// UnityEngine.Vector2Int UnityEngine.XR.ARExtensions.CameraConfiguration::m_Resolution
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___m_Resolution_0;
	// System.Int32 UnityEngine.XR.ARExtensions.CameraConfiguration::m_Framerate
	int32_t ___m_Framerate_1;

public:
	inline static int32_t get_offset_of_m_Resolution_0() { return static_cast<int32_t>(offsetof(CameraConfiguration_tF2E20F6B31C6014A39C87E6C430247D1B0F3BDD7, ___m_Resolution_0)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_m_Resolution_0() const { return ___m_Resolution_0; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_m_Resolution_0() { return &___m_Resolution_0; }
	inline void set_m_Resolution_0(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___m_Resolution_0 = value;
	}

	inline static int32_t get_offset_of_m_Framerate_1() { return static_cast<int32_t>(offsetof(CameraConfiguration_tF2E20F6B31C6014A39C87E6C430247D1B0F3BDD7, ___m_Framerate_1)); }
	inline int32_t get_m_Framerate_1() const { return ___m_Framerate_1; }
	inline int32_t* get_address_of_m_Framerate_1() { return &___m_Framerate_1; }
	inline void set_m_Framerate_1(int32_t value)
	{
		___m_Framerate_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONFIGURATION_TF2E20F6B31C6014A39C87E6C430247D1B0F3BDD7_H
#ifndef CAMERAFOCUSMODE_T198966DE20F938A7890CE5EBFB00CCA53EAF4ED4_H
#define CAMERAFOCUSMODE_T198966DE20F938A7890CE5EBFB00CCA53EAF4ED4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.CameraFocusMode
struct  CameraFocusMode_t198966DE20F938A7890CE5EBFB00CCA53EAF4ED4 
{
public:
	// System.Int32 UnityEngine.XR.ARExtensions.CameraFocusMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraFocusMode_t198966DE20F938A7890CE5EBFB00CCA53EAF4ED4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAFOCUSMODE_T198966DE20F938A7890CE5EBFB00CCA53EAF4ED4_H
#ifndef CAMERAIMAGEFORMAT_T3B37B8C0CD1FDB32590DE83ABB369EEB65307EBD_H
#define CAMERAIMAGEFORMAT_T3B37B8C0CD1FDB32590DE83ABB369EEB65307EBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.CameraImageFormat
struct  CameraImageFormat_t3B37B8C0CD1FDB32590DE83ABB369EEB65307EBD 
{
public:
	// System.Int32 UnityEngine.XR.ARExtensions.CameraImageFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraImageFormat_t3B37B8C0CD1FDB32590DE83ABB369EEB65307EBD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAIMAGEFORMAT_T3B37B8C0CD1FDB32590DE83ABB369EEB65307EBD_H
#ifndef CAMERAIMAGETRANSFORMATION_T9244C88533E9586E4237E49856EE71A34B60E48D_H
#define CAMERAIMAGETRANSFORMATION_T9244C88533E9586E4237E49856EE71A34B60E48D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.CameraImageTransformation
struct  CameraImageTransformation_t9244C88533E9586E4237E49856EE71A34B60E48D 
{
public:
	// System.Int32 UnityEngine.XR.ARExtensions.CameraImageTransformation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraImageTransformation_t9244C88533E9586E4237E49856EE71A34B60E48D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAIMAGETRANSFORMATION_T9244C88533E9586E4237E49856EE71A34B60E48D_H
#ifndef PLANEDETECTIONFLAGS_T973AF579C70632C3A6345A61C688B125C5864215_H
#define PLANEDETECTIONFLAGS_T973AF579C70632C3A6345A61C688B125C5864215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.PlaneDetectionFlags
struct  PlaneDetectionFlags_t973AF579C70632C3A6345A61C688B125C5864215 
{
public:
	// System.Int32 UnityEngine.XR.ARExtensions.PlaneDetectionFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlaneDetectionFlags_t973AF579C70632C3A6345A61C688B125C5864215, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANEDETECTIONFLAGS_T973AF579C70632C3A6345A61C688B125C5864215_H
#ifndef PLANEDETECTIONFLAGSMASKATTRIBUTE_T891FFF72B896019B6EF9B5BC66B66B7482D85E36_H
#define PLANEDETECTIONFLAGSMASKATTRIBUTE_T891FFF72B896019B6EF9B5BC66B66B7482D85E36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.PlaneDetectionFlagsMaskAttribute
struct  PlaneDetectionFlagsMaskAttribute_t891FFF72B896019B6EF9B5BC66B66B7482D85E36  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANEDETECTIONFLAGSMASKATTRIBUTE_T891FFF72B896019B6EF9B5BC66B66B7482D85E36_H
#ifndef SESSIONAVAILABILITY_T66AE38CEACBF09105F9564006DE37A94C9DB370F_H
#define SESSIONAVAILABILITY_T66AE38CEACBF09105F9564006DE37A94C9DB370F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.SessionAvailability
struct  SessionAvailability_t66AE38CEACBF09105F9564006DE37A94C9DB370F 
{
public:
	// System.Int32 UnityEngine.XR.ARExtensions.SessionAvailability::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SessionAvailability_t66AE38CEACBF09105F9564006DE37A94C9DB370F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONAVAILABILITY_T66AE38CEACBF09105F9564006DE37A94C9DB370F_H
#ifndef SESSIONINSTALLATIONSTATUS_TB8FC754E8CCA38CA2F32E61B354743D33DA3939B_H
#define SESSIONINSTALLATIONSTATUS_TB8FC754E8CCA38CA2F32E61B354743D33DA3939B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.SessionInstallationStatus
struct  SessionInstallationStatus_tB8FC754E8CCA38CA2F32E61B354743D33DA3939B 
{
public:
	// System.Int32 UnityEngine.XR.ARExtensions.SessionInstallationStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SessionInstallationStatus_tB8FC754E8CCA38CA2F32E61B354743D33DA3939B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONINSTALLATIONSTATUS_TB8FC754E8CCA38CA2F32E61B354743D33DA3939B_H
#ifndef FACESUBSYSTEMCAPABILITIES_TA2A2AD15E172EF1B91CE85994931F8C11330BB2C_H
#define FACESUBSYSTEMCAPABILITIES_TA2A2AD15E172EF1B91CE85994931F8C11330BB2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemCapabilities
struct  FaceSubsystemCapabilities_tA2A2AD15E172EF1B91CE85994931F8C11330BB2C 
{
public:
	// System.Int32 UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemCapabilities::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FaceSubsystemCapabilities_tA2A2AD15E172EF1B91CE85994931F8C11330BB2C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACESUBSYSTEMCAPABILITIES_TA2A2AD15E172EF1B91CE85994931F8C11330BB2C_H
#ifndef XRFACESUBSYSTEM_TE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_H
#define XRFACESUBSYSTEM_TE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem
struct  XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86  : public Subsystem_1_t7A31187B2E3DFF03501265C781C9A8547A073F0C
{
public:
	// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::faceAdded
	Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * ___faceAdded_4;
	// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::faceUpdated
	Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * ___faceUpdated_5;
	// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::faceRemoved
	Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * ___faceRemoved_6;

public:
	inline static int32_t get_offset_of_faceAdded_4() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86, ___faceAdded_4)); }
	inline Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * get_faceAdded_4() const { return ___faceAdded_4; }
	inline Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B ** get_address_of_faceAdded_4() { return &___faceAdded_4; }
	inline void set_faceAdded_4(Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * value)
	{
		___faceAdded_4 = value;
		Il2CppCodeGenWriteBarrier((&___faceAdded_4), value);
	}

	inline static int32_t get_offset_of_faceUpdated_5() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86, ___faceUpdated_5)); }
	inline Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * get_faceUpdated_5() const { return ___faceUpdated_5; }
	inline Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 ** get_address_of_faceUpdated_5() { return &___faceUpdated_5; }
	inline void set_faceUpdated_5(Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * value)
	{
		___faceUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___faceUpdated_5), value);
	}

	inline static int32_t get_offset_of_faceRemoved_6() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86, ___faceRemoved_6)); }
	inline Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * get_faceRemoved_6() const { return ___faceRemoved_6; }
	inline Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 ** get_address_of_faceRemoved_6() { return &___faceRemoved_6; }
	inline void set_faceRemoved_6(Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * value)
	{
		___faceRemoved_6 = value;
		Il2CppCodeGenWriteBarrier((&___faceRemoved_6), value);
	}
};

struct XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::s_FacesAddedThisFrame
	List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * ___s_FacesAddedThisFrame_1;
	// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::s_FacesUpdatedThisFrame
	List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * ___s_FacesUpdatedThisFrame_2;
	// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::s_FacesRemovedThisFrame
	List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * ___s_FacesRemovedThisFrame_3;

public:
	inline static int32_t get_offset_of_s_FacesAddedThisFrame_1() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields, ___s_FacesAddedThisFrame_1)); }
	inline List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * get_s_FacesAddedThisFrame_1() const { return ___s_FacesAddedThisFrame_1; }
	inline List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A ** get_address_of_s_FacesAddedThisFrame_1() { return &___s_FacesAddedThisFrame_1; }
	inline void set_s_FacesAddedThisFrame_1(List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * value)
	{
		___s_FacesAddedThisFrame_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_FacesAddedThisFrame_1), value);
	}

	inline static int32_t get_offset_of_s_FacesUpdatedThisFrame_2() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields, ___s_FacesUpdatedThisFrame_2)); }
	inline List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * get_s_FacesUpdatedThisFrame_2() const { return ___s_FacesUpdatedThisFrame_2; }
	inline List_1_tE2C2476479F1A844897C46751D09491B2534EC87 ** get_address_of_s_FacesUpdatedThisFrame_2() { return &___s_FacesUpdatedThisFrame_2; }
	inline void set_s_FacesUpdatedThisFrame_2(List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * value)
	{
		___s_FacesUpdatedThisFrame_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_FacesUpdatedThisFrame_2), value);
	}

	inline static int32_t get_offset_of_s_FacesRemovedThisFrame_3() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields, ___s_FacesRemovedThisFrame_3)); }
	inline List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * get_s_FacesRemovedThisFrame_3() const { return ___s_FacesRemovedThisFrame_3; }
	inline List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 ** get_address_of_s_FacesRemovedThisFrame_3() { return &___s_FacesRemovedThisFrame_3; }
	inline void set_s_FacesRemovedThisFrame_3(List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * value)
	{
		___s_FacesRemovedThisFrame_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_FacesRemovedThisFrame_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRFACESUBSYSTEM_TE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_H
#ifndef XRFACESUBSYSTEMDESCRIPTOR_T6952E974969B7D18CA2047C47FC5EB740507FA19_H
#define XRFACESUBSYSTEMDESCRIPTOR_T6952E974969B7D18CA2047C47FC5EB740507FA19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor
struct  XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19  : public SubsystemDescriptor_1_t8BC7D01A6BF03D22D133B8FB58E5B2D85675FDD7
{
public:
	// System.Boolean UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor::<supportsFacePose>k__BackingField
	bool ___U3CsupportsFacePoseU3Ek__BackingField_2;
	// System.Boolean UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor::<supportsFaceMeshVerticesAndIndices>k__BackingField
	bool ___U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3;
	// System.Boolean UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor::<supportsFaceMeshUVs>k__BackingField
	bool ___U3CsupportsFaceMeshUVsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CsupportsFacePoseU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19, ___U3CsupportsFacePoseU3Ek__BackingField_2)); }
	inline bool get_U3CsupportsFacePoseU3Ek__BackingField_2() const { return ___U3CsupportsFacePoseU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CsupportsFacePoseU3Ek__BackingField_2() { return &___U3CsupportsFacePoseU3Ek__BackingField_2; }
	inline void set_U3CsupportsFacePoseU3Ek__BackingField_2(bool value)
	{
		___U3CsupportsFacePoseU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19, ___U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3)); }
	inline bool get_U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3() const { return ___U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3() { return &___U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3; }
	inline void set_U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3(bool value)
	{
		___U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CsupportsFaceMeshUVsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19, ___U3CsupportsFaceMeshUVsU3Ek__BackingField_4)); }
	inline bool get_U3CsupportsFaceMeshUVsU3Ek__BackingField_4() const { return ___U3CsupportsFaceMeshUVsU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CsupportsFaceMeshUVsU3Ek__BackingField_4() { return &___U3CsupportsFaceMeshUVsU3Ek__BackingField_4; }
	inline void set_U3CsupportsFaceMeshUVsU3Ek__BackingField_4(bool value)
	{
		___U3CsupportsFaceMeshUVsU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRFACESUBSYSTEMDESCRIPTOR_T6952E974969B7D18CA2047C47FC5EB740507FA19_H
#ifndef BSONBINARYWRITER_T39DD2DD462DE9FF693FFEF86A404347C8B060EA2_H
#define BSONBINARYWRITER_T39DD2DD462DE9FF693FFEF86A404347C8B060EA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBinaryWriter
struct  BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2  : public RuntimeObject
{
public:
	// System.IO.BinaryWriter Newtonsoft.Json.Bson.BsonBinaryWriter::_writer
	BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * ____writer_1;
	// System.Byte[] Newtonsoft.Json.Bson.BsonBinaryWriter::_largeByteBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____largeByteBuffer_2;
	// System.DateTimeKind Newtonsoft.Json.Bson.BsonBinaryWriter::<DateTimeKindHandling>k__BackingField
	int32_t ___U3CDateTimeKindHandlingU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__writer_1() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2, ____writer_1)); }
	inline BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * get__writer_1() const { return ____writer_1; }
	inline BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 ** get_address_of__writer_1() { return &____writer_1; }
	inline void set__writer_1(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * value)
	{
		____writer_1 = value;
		Il2CppCodeGenWriteBarrier((&____writer_1), value);
	}

	inline static int32_t get_offset_of__largeByteBuffer_2() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2, ____largeByteBuffer_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__largeByteBuffer_2() const { return ____largeByteBuffer_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__largeByteBuffer_2() { return &____largeByteBuffer_2; }
	inline void set__largeByteBuffer_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____largeByteBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&____largeByteBuffer_2), value);
	}

	inline static int32_t get_offset_of_U3CDateTimeKindHandlingU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2, ___U3CDateTimeKindHandlingU3Ek__BackingField_3)); }
	inline int32_t get_U3CDateTimeKindHandlingU3Ek__BackingField_3() const { return ___U3CDateTimeKindHandlingU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CDateTimeKindHandlingU3Ek__BackingField_3() { return &___U3CDateTimeKindHandlingU3Ek__BackingField_3; }
	inline void set_U3CDateTimeKindHandlingU3Ek__BackingField_3(int32_t value)
	{
		___U3CDateTimeKindHandlingU3Ek__BackingField_3 = value;
	}
};

struct BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2_StaticFields
{
public:
	// System.Text.Encoding Newtonsoft.Json.Bson.BsonBinaryWriter::Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___Encoding_0;

public:
	inline static int32_t get_offset_of_Encoding_0() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2_StaticFields, ___Encoding_0)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_Encoding_0() const { return ___Encoding_0; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_Encoding_0() { return &___Encoding_0; }
	inline void set_Encoding_0(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___Encoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___Encoding_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARYWRITER_T39DD2DD462DE9FF693FFEF86A404347C8B060EA2_H
#ifndef BSONVALUE_T1CADCCC6BD9CB06C512942B38623671052A09262_H
#define BSONVALUE_T1CADCCC6BD9CB06C512942B38623671052A09262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonValue
struct  BsonValue_t1CADCCC6BD9CB06C512942B38623671052A09262  : public BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973
{
public:
	// System.Object Newtonsoft.Json.Bson.BsonValue::_value
	RuntimeObject * ____value_2;
	// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonValue::_type
	int8_t ____type_3;

public:
	inline static int32_t get_offset_of__value_2() { return static_cast<int32_t>(offsetof(BsonValue_t1CADCCC6BD9CB06C512942B38623671052A09262, ____value_2)); }
	inline RuntimeObject * get__value_2() const { return ____value_2; }
	inline RuntimeObject ** get_address_of__value_2() { return &____value_2; }
	inline void set__value_2(RuntimeObject * value)
	{
		____value_2 = value;
		Il2CppCodeGenWriteBarrier((&____value_2), value);
	}

	inline static int32_t get_offset_of__type_3() { return static_cast<int32_t>(offsetof(BsonValue_t1CADCCC6BD9CB06C512942B38623671052A09262, ____type_3)); }
	inline int8_t get__type_3() const { return ____type_3; }
	inline int8_t* get_address_of__type_3() { return &____type_3; }
	inline void set__type_3(int8_t value)
	{
		____type_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONVALUE_T1CADCCC6BD9CB06C512942B38623671052A09262_H
#ifndef JSONPOSITION_T7C381FA9E9591B7782B67363333C5CFA331E8EAD_H
#define JSONPOSITION_T7C381FA9E9591B7782B67363333C5CFA331E8EAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPosition
struct  JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD 
{
public:
	// Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonPosition::Type
	int32_t ___Type_1;
	// System.Int32 Newtonsoft.Json.JsonPosition::Position
	int32_t ___Position_2;
	// System.String Newtonsoft.Json.JsonPosition::PropertyName
	String_t* ___PropertyName_3;
	// System.Boolean Newtonsoft.Json.JsonPosition::HasIndex
	bool ___HasIndex_4;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_HasIndex_4() { return static_cast<int32_t>(offsetof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD, ___HasIndex_4)); }
	inline bool get_HasIndex_4() const { return ___HasIndex_4; }
	inline bool* get_address_of_HasIndex_4() { return &___HasIndex_4; }
	inline void set_HasIndex_4(bool value)
	{
		___HasIndex_4 = value;
	}
};

struct JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD_StaticFields
{
public:
	// System.Char[] Newtonsoft.Json.JsonPosition::SpecialCharacters
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___SpecialCharacters_0;

public:
	inline static int32_t get_offset_of_SpecialCharacters_0() { return static_cast<int32_t>(offsetof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD_StaticFields, ___SpecialCharacters_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_SpecialCharacters_0() const { return ___SpecialCharacters_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_SpecialCharacters_0() { return &___SpecialCharacters_0; }
	inline void set_SpecialCharacters_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___SpecialCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialCharacters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD_marshaled_pinvoke
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	char* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
// Native definition for COM marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD_marshaled_com
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	Il2CppChar* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
#endif // JSONPOSITION_T7C381FA9E9591B7782B67363333C5CFA331E8EAD_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef CAMERAIMAGE_TA347D573CD5319A54DF1A4D73E36B0975970723E_H
#define CAMERAIMAGE_TA347D573CD5319A54DF1A4D73E36B0975970723E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.CameraImage
struct  CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E 
{
public:
	// UnityEngine.Vector2Int UnityEngine.XR.ARExtensions.CameraImage::<dimensions>k__BackingField
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___U3CdimensionsU3Ek__BackingField_0;
	// System.Int32 UnityEngine.XR.ARExtensions.CameraImage::<planeCount>k__BackingField
	int32_t ___U3CplaneCountU3Ek__BackingField_1;
	// UnityEngine.XR.ARExtensions.CameraImageFormat UnityEngine.XR.ARExtensions.CameraImage::<format>k__BackingField
	int32_t ___U3CformatU3Ek__BackingField_2;
	// System.Double UnityEngine.XR.ARExtensions.CameraImage::<timestamp>k__BackingField
	double ___U3CtimestampU3Ek__BackingField_3;
	// System.Int32 UnityEngine.XR.ARExtensions.CameraImage::m_NativeHandle
	int32_t ___m_NativeHandle_4;
	// UnityEngine.XR.ARExtensions.ICameraImageApi UnityEngine.XR.ARExtensions.CameraImage::m_CameraImageApi
	RuntimeObject* ___m_CameraImageApi_5;

public:
	inline static int32_t get_offset_of_U3CdimensionsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E, ___U3CdimensionsU3Ek__BackingField_0)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_U3CdimensionsU3Ek__BackingField_0() const { return ___U3CdimensionsU3Ek__BackingField_0; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_U3CdimensionsU3Ek__BackingField_0() { return &___U3CdimensionsU3Ek__BackingField_0; }
	inline void set_U3CdimensionsU3Ek__BackingField_0(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___U3CdimensionsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CplaneCountU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E, ___U3CplaneCountU3Ek__BackingField_1)); }
	inline int32_t get_U3CplaneCountU3Ek__BackingField_1() const { return ___U3CplaneCountU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CplaneCountU3Ek__BackingField_1() { return &___U3CplaneCountU3Ek__BackingField_1; }
	inline void set_U3CplaneCountU3Ek__BackingField_1(int32_t value)
	{
		___U3CplaneCountU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CformatU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E, ___U3CformatU3Ek__BackingField_2)); }
	inline int32_t get_U3CformatU3Ek__BackingField_2() const { return ___U3CformatU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CformatU3Ek__BackingField_2() { return &___U3CformatU3Ek__BackingField_2; }
	inline void set_U3CformatU3Ek__BackingField_2(int32_t value)
	{
		___U3CformatU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CtimestampU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E, ___U3CtimestampU3Ek__BackingField_3)); }
	inline double get_U3CtimestampU3Ek__BackingField_3() const { return ___U3CtimestampU3Ek__BackingField_3; }
	inline double* get_address_of_U3CtimestampU3Ek__BackingField_3() { return &___U3CtimestampU3Ek__BackingField_3; }
	inline void set_U3CtimestampU3Ek__BackingField_3(double value)
	{
		___U3CtimestampU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_m_NativeHandle_4() { return static_cast<int32_t>(offsetof(CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E, ___m_NativeHandle_4)); }
	inline int32_t get_m_NativeHandle_4() const { return ___m_NativeHandle_4; }
	inline int32_t* get_address_of_m_NativeHandle_4() { return &___m_NativeHandle_4; }
	inline void set_m_NativeHandle_4(int32_t value)
	{
		___m_NativeHandle_4 = value;
	}

	inline static int32_t get_offset_of_m_CameraImageApi_5() { return static_cast<int32_t>(offsetof(CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E, ___m_CameraImageApi_5)); }
	inline RuntimeObject* get_m_CameraImageApi_5() const { return ___m_CameraImageApi_5; }
	inline RuntimeObject** get_address_of_m_CameraImageApi_5() { return &___m_CameraImageApi_5; }
	inline void set_m_CameraImageApi_5(RuntimeObject* value)
	{
		___m_CameraImageApi_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraImageApi_5), value);
	}
};

struct CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E_StaticFields
{
public:
	// UnityEngine.XR.ARExtensions.XRCameraExtensions/OnImageRequestCompleteDelegate UnityEngine.XR.ARExtensions.CameraImage::s_OnAsyncConversionComplete
	OnImageRequestCompleteDelegate_tC602138547507C797A3BE8CC2D7022659FA457B2 * ___s_OnAsyncConversionComplete_6;

public:
	inline static int32_t get_offset_of_s_OnAsyncConversionComplete_6() { return static_cast<int32_t>(offsetof(CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E_StaticFields, ___s_OnAsyncConversionComplete_6)); }
	inline OnImageRequestCompleteDelegate_tC602138547507C797A3BE8CC2D7022659FA457B2 * get_s_OnAsyncConversionComplete_6() const { return ___s_OnAsyncConversionComplete_6; }
	inline OnImageRequestCompleteDelegate_tC602138547507C797A3BE8CC2D7022659FA457B2 ** get_address_of_s_OnAsyncConversionComplete_6() { return &___s_OnAsyncConversionComplete_6; }
	inline void set_s_OnAsyncConversionComplete_6(OnImageRequestCompleteDelegate_tC602138547507C797A3BE8CC2D7022659FA457B2 * value)
	{
		___s_OnAsyncConversionComplete_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_OnAsyncConversionComplete_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.ARExtensions.CameraImage
struct CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E_marshaled_pinvoke
{
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___U3CdimensionsU3Ek__BackingField_0;
	int32_t ___U3CplaneCountU3Ek__BackingField_1;
	int32_t ___U3CformatU3Ek__BackingField_2;
	double ___U3CtimestampU3Ek__BackingField_3;
	int32_t ___m_NativeHandle_4;
	RuntimeObject* ___m_CameraImageApi_5;
};
// Native definition for COM marshalling of UnityEngine.XR.ARExtensions.CameraImage
struct CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E_marshaled_com
{
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___U3CdimensionsU3Ek__BackingField_0;
	int32_t ___U3CplaneCountU3Ek__BackingField_1;
	int32_t ___U3CformatU3Ek__BackingField_2;
	double ___U3CtimestampU3Ek__BackingField_3;
	int32_t ___m_NativeHandle_4;
	RuntimeObject* ___m_CameraImageApi_5;
};
#endif // CAMERAIMAGE_TA347D573CD5319A54DF1A4D73E36B0975970723E_H
#ifndef CAMERAIMAGECONVERSIONPARAMS_TEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1_H
#define CAMERAIMAGECONVERSIONPARAMS_TEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.CameraImageConversionParams
struct  CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1 
{
public:
	// UnityEngine.RectInt UnityEngine.XR.ARExtensions.CameraImageConversionParams::m_InputRect
	RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A  ___m_InputRect_0;
	// UnityEngine.Vector2Int UnityEngine.XR.ARExtensions.CameraImageConversionParams::m_OutputDimensions
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___m_OutputDimensions_1;
	// UnityEngine.TextureFormat UnityEngine.XR.ARExtensions.CameraImageConversionParams::m_Format
	int32_t ___m_Format_2;
	// UnityEngine.XR.ARExtensions.CameraImageTransformation UnityEngine.XR.ARExtensions.CameraImageConversionParams::m_Transformation
	int32_t ___m_Transformation_3;

public:
	inline static int32_t get_offset_of_m_InputRect_0() { return static_cast<int32_t>(offsetof(CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1, ___m_InputRect_0)); }
	inline RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A  get_m_InputRect_0() const { return ___m_InputRect_0; }
	inline RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A * get_address_of_m_InputRect_0() { return &___m_InputRect_0; }
	inline void set_m_InputRect_0(RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A  value)
	{
		___m_InputRect_0 = value;
	}

	inline static int32_t get_offset_of_m_OutputDimensions_1() { return static_cast<int32_t>(offsetof(CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1, ___m_OutputDimensions_1)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_m_OutputDimensions_1() const { return ___m_OutputDimensions_1; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_m_OutputDimensions_1() { return &___m_OutputDimensions_1; }
	inline void set_m_OutputDimensions_1(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___m_OutputDimensions_1 = value;
	}

	inline static int32_t get_offset_of_m_Format_2() { return static_cast<int32_t>(offsetof(CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1, ___m_Format_2)); }
	inline int32_t get_m_Format_2() const { return ___m_Format_2; }
	inline int32_t* get_address_of_m_Format_2() { return &___m_Format_2; }
	inline void set_m_Format_2(int32_t value)
	{
		___m_Format_2 = value;
	}

	inline static int32_t get_offset_of_m_Transformation_3() { return static_cast<int32_t>(offsetof(CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1, ___m_Transformation_3)); }
	inline int32_t get_m_Transformation_3() const { return ___m_Transformation_3; }
	inline int32_t* get_address_of_m_Transformation_3() { return &___m_Transformation_3; }
	inline void set_m_Transformation_3(int32_t value)
	{
		___m_Transformation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAIMAGECONVERSIONPARAMS_TEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1_H
#ifndef FACESUBSYSTEMPARAMS_TA2026F8E1817583A4FCE47A581DD5C29F3015040_H
#define FACESUBSYSTEMPARAMS_TA2026F8E1817583A4FCE47A581DD5C29F3015040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams
struct  FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 
{
public:
	// System.String UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::id
	String_t* ___id_0;
	// System.Type UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::implementationType
	Type_t * ___implementationType_1;
	// UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemCapabilities UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::<capabilities>k__BackingField
	int32_t ___U3CcapabilitiesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_implementationType_1() { return static_cast<int32_t>(offsetof(FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040, ___implementationType_1)); }
	inline Type_t * get_implementationType_1() const { return ___implementationType_1; }
	inline Type_t ** get_address_of_implementationType_1() { return &___implementationType_1; }
	inline void set_implementationType_1(Type_t * value)
	{
		___implementationType_1 = value;
		Il2CppCodeGenWriteBarrier((&___implementationType_1), value);
	}

	inline static int32_t get_offset_of_U3CcapabilitiesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040, ___U3CcapabilitiesU3Ek__BackingField_2)); }
	inline int32_t get_U3CcapabilitiesU3Ek__BackingField_2() const { return ___U3CcapabilitiesU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CcapabilitiesU3Ek__BackingField_2() { return &___U3CcapabilitiesU3Ek__BackingField_2; }
	inline void set_U3CcapabilitiesU3Ek__BackingField_2(int32_t value)
	{
		___U3CcapabilitiesU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams
struct FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshaled_pinvoke
{
	char* ___id_0;
	Type_t * ___implementationType_1;
	int32_t ___U3CcapabilitiesU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams
struct FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshaled_com
{
	Il2CppChar* ___id_0;
	Type_t * ___implementationType_1;
	int32_t ___U3CcapabilitiesU3Ek__BackingField_2;
};
#endif // FACESUBSYSTEMPARAMS_TA2026F8E1817583A4FCE47A581DD5C29F3015040_H
#ifndef XRFACE_TD2DF3125FE693EE9DABE72A78349CB7F07A51246_H
#define XRFACE_TD2DF3125FE693EE9DABE72A78349CB7F07A51246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.XRFace
struct  XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 
{
public:
	// UnityEngine.Experimental.XR.TrackableId UnityEngine.XR.FaceSubsystem.XRFace::m_TrackableId
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___m_TrackableId_0;
	// UnityEngine.Pose UnityEngine.XR.FaceSubsystem.XRFace::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_1;
	// System.Int32 UnityEngine.XR.FaceSubsystem.XRFace::m_WasUpdated
	int32_t ___m_WasUpdated_2;
	// System.Int32 UnityEngine.XR.FaceSubsystem.XRFace::m_IsTracked
	int32_t ___m_IsTracked_3;

public:
	inline static int32_t get_offset_of_m_TrackableId_0() { return static_cast<int32_t>(offsetof(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246, ___m_TrackableId_0)); }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  get_m_TrackableId_0() const { return ___m_TrackableId_0; }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B * get_address_of_m_TrackableId_0() { return &___m_TrackableId_0; }
	inline void set_m_TrackableId_0(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  value)
	{
		___m_TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_m_Pose_1() { return static_cast<int32_t>(offsetof(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246, ___m_Pose_1)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_1() const { return ___m_Pose_1; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_1() { return &___m_Pose_1; }
	inline void set_m_Pose_1(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_1 = value;
	}

	inline static int32_t get_offset_of_m_WasUpdated_2() { return static_cast<int32_t>(offsetof(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246, ___m_WasUpdated_2)); }
	inline int32_t get_m_WasUpdated_2() const { return ___m_WasUpdated_2; }
	inline int32_t* get_address_of_m_WasUpdated_2() { return &___m_WasUpdated_2; }
	inline void set_m_WasUpdated_2(int32_t value)
	{
		___m_WasUpdated_2 = value;
	}

	inline static int32_t get_offset_of_m_IsTracked_3() { return static_cast<int32_t>(offsetof(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246, ___m_IsTracked_3)); }
	inline int32_t get_m_IsTracked_3() const { return ___m_IsTracked_3; }
	inline int32_t* get_address_of_m_IsTracked_3() { return &___m_IsTracked_3; }
	inline void set_m_IsTracked_3(int32_t value)
	{
		___m_IsTracked_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRFACE_TD2DF3125FE693EE9DABE72A78349CB7F07A51246_H
#ifndef BSONBINARY_TE52CAC83857D4FC5A09DBC2954EBCCF54EAAA1AB_H
#define BSONBINARY_TE52CAC83857D4FC5A09DBC2954EBCCF54EAAA1AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBinary
struct  BsonBinary_tE52CAC83857D4FC5A09DBC2954EBCCF54EAAA1AB  : public BsonValue_t1CADCCC6BD9CB06C512942B38623671052A09262
{
public:
	// Newtonsoft.Json.Bson.BsonBinaryType Newtonsoft.Json.Bson.BsonBinary::<BinaryType>k__BackingField
	uint8_t ___U3CBinaryTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CBinaryTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BsonBinary_tE52CAC83857D4FC5A09DBC2954EBCCF54EAAA1AB, ___U3CBinaryTypeU3Ek__BackingField_4)); }
	inline uint8_t get_U3CBinaryTypeU3Ek__BackingField_4() const { return ___U3CBinaryTypeU3Ek__BackingField_4; }
	inline uint8_t* get_address_of_U3CBinaryTypeU3Ek__BackingField_4() { return &___U3CBinaryTypeU3Ek__BackingField_4; }
	inline void set_U3CBinaryTypeU3Ek__BackingField_4(uint8_t value)
	{
		___U3CBinaryTypeU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARY_TE52CAC83857D4FC5A09DBC2954EBCCF54EAAA1AB_H
#ifndef BSONSTRING_T7BA8171A0F1E08A1320F89FD6A4AAC201E402D46_H
#define BSONSTRING_T7BA8171A0F1E08A1320F89FD6A4AAC201E402D46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonString
struct  BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46  : public BsonValue_t1CADCCC6BD9CB06C512942B38623671052A09262
{
public:
	// System.Int32 Newtonsoft.Json.Bson.BsonString::<ByteCount>k__BackingField
	int32_t ___U3CByteCountU3Ek__BackingField_4;
	// System.Boolean Newtonsoft.Json.Bson.BsonString::<IncludeLength>k__BackingField
	bool ___U3CIncludeLengthU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CByteCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46, ___U3CByteCountU3Ek__BackingField_4)); }
	inline int32_t get_U3CByteCountU3Ek__BackingField_4() const { return ___U3CByteCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CByteCountU3Ek__BackingField_4() { return &___U3CByteCountU3Ek__BackingField_4; }
	inline void set_U3CByteCountU3Ek__BackingField_4(int32_t value)
	{
		___U3CByteCountU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIncludeLengthU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46, ___U3CIncludeLengthU3Ek__BackingField_5)); }
	inline bool get_U3CIncludeLengthU3Ek__BackingField_5() const { return ___U3CIncludeLengthU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIncludeLengthU3Ek__BackingField_5() { return &___U3CIncludeLengthU3Ek__BackingField_5; }
	inline void set_U3CIncludeLengthU3Ek__BackingField_5(bool value)
	{
		___U3CIncludeLengthU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONSTRING_T7BA8171A0F1E08A1320F89FD6A4AAC201E402D46_H
#ifndef JSONWRITER_T4CF8C6EC696955A38B904D71B2AB1C6ED6155091_H
#define JSONWRITER_T4CF8C6EC696955A38B904D71B2AB1C6ED6155091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter
struct  JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonWriter::_stack
	List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 * ____stack_2;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonWriter::_currentPosition
	JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD  ____currentPosition_3;
	// Newtonsoft.Json.JsonWriter/State Newtonsoft.Json.JsonWriter::_currentState
	int32_t ____currentState_4;
	// Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::_formatting
	int32_t ____formatting_5;
	// System.Boolean Newtonsoft.Json.JsonWriter::<CloseOutput>k__BackingField
	bool ___U3CCloseOutputU3Ek__BackingField_6;
	// Newtonsoft.Json.DateFormatHandling Newtonsoft.Json.JsonWriter::_dateFormatHandling
	int32_t ____dateFormatHandling_7;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonWriter::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_8;
	// Newtonsoft.Json.StringEscapeHandling Newtonsoft.Json.JsonWriter::_stringEscapeHandling
	int32_t ____stringEscapeHandling_9;
	// Newtonsoft.Json.FloatFormatHandling Newtonsoft.Json.JsonWriter::_floatFormatHandling
	int32_t ____floatFormatHandling_10;
	// System.String Newtonsoft.Json.JsonWriter::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonWriter::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_12;

public:
	inline static int32_t get_offset_of__stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____stack_2)); }
	inline List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 * get__stack_2() const { return ____stack_2; }
	inline List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 ** get_address_of__stack_2() { return &____stack_2; }
	inline void set__stack_2(List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 * value)
	{
		____stack_2 = value;
		Il2CppCodeGenWriteBarrier((&____stack_2), value);
	}

	inline static int32_t get_offset_of__currentPosition_3() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____currentPosition_3)); }
	inline JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD  get__currentPosition_3() const { return ____currentPosition_3; }
	inline JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD * get_address_of__currentPosition_3() { return &____currentPosition_3; }
	inline void set__currentPosition_3(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD  value)
	{
		____currentPosition_3 = value;
	}

	inline static int32_t get_offset_of__currentState_4() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____currentState_4)); }
	inline int32_t get__currentState_4() const { return ____currentState_4; }
	inline int32_t* get_address_of__currentState_4() { return &____currentState_4; }
	inline void set__currentState_4(int32_t value)
	{
		____currentState_4 = value;
	}

	inline static int32_t get_offset_of__formatting_5() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____formatting_5)); }
	inline int32_t get__formatting_5() const { return ____formatting_5; }
	inline int32_t* get_address_of__formatting_5() { return &____formatting_5; }
	inline void set__formatting_5(int32_t value)
	{
		____formatting_5 = value;
	}

	inline static int32_t get_offset_of_U3CCloseOutputU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ___U3CCloseOutputU3Ek__BackingField_6)); }
	inline bool get_U3CCloseOutputU3Ek__BackingField_6() const { return ___U3CCloseOutputU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CCloseOutputU3Ek__BackingField_6() { return &___U3CCloseOutputU3Ek__BackingField_6; }
	inline void set_U3CCloseOutputU3Ek__BackingField_6(bool value)
	{
		___U3CCloseOutputU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_7() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____dateFormatHandling_7)); }
	inline int32_t get__dateFormatHandling_7() const { return ____dateFormatHandling_7; }
	inline int32_t* get_address_of__dateFormatHandling_7() { return &____dateFormatHandling_7; }
	inline void set__dateFormatHandling_7(int32_t value)
	{
		____dateFormatHandling_7 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_8() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____dateTimeZoneHandling_8)); }
	inline int32_t get__dateTimeZoneHandling_8() const { return ____dateTimeZoneHandling_8; }
	inline int32_t* get_address_of__dateTimeZoneHandling_8() { return &____dateTimeZoneHandling_8; }
	inline void set__dateTimeZoneHandling_8(int32_t value)
	{
		____dateTimeZoneHandling_8 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_9() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____stringEscapeHandling_9)); }
	inline int32_t get__stringEscapeHandling_9() const { return ____stringEscapeHandling_9; }
	inline int32_t* get_address_of__stringEscapeHandling_9() { return &____stringEscapeHandling_9; }
	inline void set__stringEscapeHandling_9(int32_t value)
	{
		____stringEscapeHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_10() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____floatFormatHandling_10)); }
	inline int32_t get__floatFormatHandling_10() const { return ____floatFormatHandling_10; }
	inline int32_t* get_address_of__floatFormatHandling_10() { return &____floatFormatHandling_10; }
	inline void set__floatFormatHandling_10(int32_t value)
	{
		____floatFormatHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__culture_12() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____culture_12)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_12() const { return ____culture_12; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_12() { return &____culture_12; }
	inline void set__culture_12(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_12 = value;
		Il2CppCodeGenWriteBarrier((&____culture_12), value);
	}
};

struct JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091_StaticFields
{
public:
	// Newtonsoft.Json.JsonWriter/State[][] Newtonsoft.Json.JsonWriter::StateArray
	StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* ___StateArray_0;
	// Newtonsoft.Json.JsonWriter/State[][] Newtonsoft.Json.JsonWriter::StateArrayTempate
	StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* ___StateArrayTempate_1;

public:
	inline static int32_t get_offset_of_StateArray_0() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091_StaticFields, ___StateArray_0)); }
	inline StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* get_StateArray_0() const { return ___StateArray_0; }
	inline StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84** get_address_of_StateArray_0() { return &___StateArray_0; }
	inline void set_StateArray_0(StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* value)
	{
		___StateArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___StateArray_0), value);
	}

	inline static int32_t get_offset_of_StateArrayTempate_1() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091_StaticFields, ___StateArrayTempate_1)); }
	inline StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* get_StateArrayTempate_1() const { return ___StateArrayTempate_1; }
	inline StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84** get_address_of_StateArrayTempate_1() { return &___StateArrayTempate_1; }
	inline void set_StateArrayTempate_1(StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* value)
	{
		___StateArrayTempate_1 = value;
		Il2CppCodeGenWriteBarrier((&___StateArrayTempate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T4CF8C6EC696955A38B904D71B2AB1C6ED6155091_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef ONIMAGEREQUESTCOMPLETEDELEGATE_TC602138547507C797A3BE8CC2D7022659FA457B2_H
#define ONIMAGEREQUESTCOMPLETEDELEGATE_TC602138547507C797A3BE8CC2D7022659FA457B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.XRCameraExtensions/OnImageRequestCompleteDelegate
struct  OnImageRequestCompleteDelegate_tC602138547507C797A3BE8CC2D7022659FA457B2  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONIMAGEREQUESTCOMPLETEDELEGATE_TC602138547507C797A3BE8CC2D7022659FA457B2_H
#ifndef TRYGETCOLORCORRECTIONDELEGATE_T30B930C66D1DCEC1490F1A6624404BA665652BD8_H
#define TRYGETCOLORCORRECTIONDELEGATE_T30B930C66D1DCEC1490F1A6624404BA665652BD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.XRCameraExtensions/TryGetColorCorrectionDelegate
struct  TryGetColorCorrectionDelegate_t30B930C66D1DCEC1490F1A6624404BA665652BD8  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRYGETCOLORCORRECTIONDELEGATE_T30B930C66D1DCEC1490F1A6624404BA665652BD8_H
#ifndef ATTACHREFERENCEPOINTDELEGATE_T8E14528144FB8B3DE6B44FCD01B3B4C2090236DF_H
#define ATTACHREFERENCEPOINTDELEGATE_T8E14528144FB8B3DE6B44FCD01B3B4C2090236DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.XRReferencePointExtensions/AttachReferencePointDelegate
struct  AttachReferencePointDelegate_t8E14528144FB8B3DE6B44FCD01B3B4C2090236DF  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHREFERENCEPOINTDELEGATE_T8E14528144FB8B3DE6B44FCD01B3B4C2090236DF_H
#ifndef FACEADDEDEVENTARGS_T9D33653893D8E72028F38DD5820FE6E488491B0C_H
#define FACEADDEDEVENTARGS_T9D33653893D8E72028F38DD5820FE6E488491B0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs
struct  FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C 
{
public:
	// UnityEngine.XR.FaceSubsystem.XRFace UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::<xrFace>k__BackingField
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::<xrFaceSubsystem>k__BackingField
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CxrFaceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C, ___U3CxrFaceU3Ek__BackingField_0)); }
	inline XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  get_U3CxrFaceU3Ek__BackingField_0() const { return ___U3CxrFaceU3Ek__BackingField_0; }
	inline XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * get_address_of_U3CxrFaceU3Ek__BackingField_0() { return &___U3CxrFaceU3Ek__BackingField_0; }
	inline void set_U3CxrFaceU3Ek__BackingField_0(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  value)
	{
		___U3CxrFaceU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CxrFaceSubsystemU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C, ___U3CxrFaceSubsystemU3Ek__BackingField_1)); }
	inline XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * get_U3CxrFaceSubsystemU3Ek__BackingField_1() const { return ___U3CxrFaceSubsystemU3Ek__BackingField_1; }
	inline XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 ** get_address_of_U3CxrFaceSubsystemU3Ek__BackingField_1() { return &___U3CxrFaceSubsystemU3Ek__BackingField_1; }
	inline void set_U3CxrFaceSubsystemU3Ek__BackingField_1(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * value)
	{
		___U3CxrFaceSubsystemU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CxrFaceSubsystemU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs
struct FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshaled_pinvoke
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs
struct FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshaled_com
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;
};
#endif // FACEADDEDEVENTARGS_T9D33653893D8E72028F38DD5820FE6E488491B0C_H
#ifndef FACEREMOVEDEVENTARGS_T2D9E4A417A63098195E12566E34160103841D651_H
#define FACEREMOVEDEVENTARGS_T2D9E4A417A63098195E12566E34160103841D651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs
struct  FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 
{
public:
	// UnityEngine.XR.FaceSubsystem.XRFace UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::<xrFace>k__BackingField
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::<xrFaceSubsystem>k__BackingField
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CxrFaceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651, ___U3CxrFaceU3Ek__BackingField_0)); }
	inline XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  get_U3CxrFaceU3Ek__BackingField_0() const { return ___U3CxrFaceU3Ek__BackingField_0; }
	inline XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * get_address_of_U3CxrFaceU3Ek__BackingField_0() { return &___U3CxrFaceU3Ek__BackingField_0; }
	inline void set_U3CxrFaceU3Ek__BackingField_0(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  value)
	{
		___U3CxrFaceU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CxrFaceSubsystemU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651, ___U3CxrFaceSubsystemU3Ek__BackingField_1)); }
	inline XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * get_U3CxrFaceSubsystemU3Ek__BackingField_1() const { return ___U3CxrFaceSubsystemU3Ek__BackingField_1; }
	inline XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 ** get_address_of_U3CxrFaceSubsystemU3Ek__BackingField_1() { return &___U3CxrFaceSubsystemU3Ek__BackingField_1; }
	inline void set_U3CxrFaceSubsystemU3Ek__BackingField_1(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * value)
	{
		___U3CxrFaceSubsystemU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CxrFaceSubsystemU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs
struct FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshaled_pinvoke
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs
struct FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshaled_com
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;
};
#endif // FACEREMOVEDEVENTARGS_T2D9E4A417A63098195E12566E34160103841D651_H
#ifndef FACEUPDATEDEVENTARGS_T1EB3DBB5A762065163012A9BACA88A75C7010881_H
#define FACEUPDATEDEVENTARGS_T1EB3DBB5A762065163012A9BACA88A75C7010881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs
struct  FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 
{
public:
	// UnityEngine.XR.FaceSubsystem.XRFace UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::<xrFace>k__BackingField
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::<xrFaceSubsystem>k__BackingField
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CxrFaceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881, ___U3CxrFaceU3Ek__BackingField_0)); }
	inline XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  get_U3CxrFaceU3Ek__BackingField_0() const { return ___U3CxrFaceU3Ek__BackingField_0; }
	inline XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * get_address_of_U3CxrFaceU3Ek__BackingField_0() { return &___U3CxrFaceU3Ek__BackingField_0; }
	inline void set_U3CxrFaceU3Ek__BackingField_0(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  value)
	{
		___U3CxrFaceU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CxrFaceSubsystemU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881, ___U3CxrFaceSubsystemU3Ek__BackingField_1)); }
	inline XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * get_U3CxrFaceSubsystemU3Ek__BackingField_1() const { return ___U3CxrFaceSubsystemU3Ek__BackingField_1; }
	inline XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 ** get_address_of_U3CxrFaceSubsystemU3Ek__BackingField_1() { return &___U3CxrFaceSubsystemU3Ek__BackingField_1; }
	inline void set_U3CxrFaceSubsystemU3Ek__BackingField_1(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * value)
	{
		___U3CxrFaceSubsystemU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CxrFaceSubsystemU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs
struct FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshaled_pinvoke
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs
struct FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshaled_com
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;
};
#endif // FACEUPDATEDEVENTARGS_T1EB3DBB5A762065163012A9BACA88A75C7010881_H
#ifndef BSONWRITER_T618B49FFFCFEE187A002908C374F5DFE5979DBCB_H
#define BSONWRITER_T618B49FFFCFEE187A002908C374F5DFE5979DBCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonWriter
struct  BsonWriter_t618B49FFFCFEE187A002908C374F5DFE5979DBCB  : public JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091
{
public:
	// Newtonsoft.Json.Bson.BsonBinaryWriter Newtonsoft.Json.Bson.BsonWriter::_writer
	BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2 * ____writer_13;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonWriter::_root
	BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 * ____root_14;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonWriter::_parent
	BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 * ____parent_15;
	// System.String Newtonsoft.Json.Bson.BsonWriter::_propertyName
	String_t* ____propertyName_16;

public:
	inline static int32_t get_offset_of__writer_13() { return static_cast<int32_t>(offsetof(BsonWriter_t618B49FFFCFEE187A002908C374F5DFE5979DBCB, ____writer_13)); }
	inline BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2 * get__writer_13() const { return ____writer_13; }
	inline BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2 ** get_address_of__writer_13() { return &____writer_13; }
	inline void set__writer_13(BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2 * value)
	{
		____writer_13 = value;
		Il2CppCodeGenWriteBarrier((&____writer_13), value);
	}

	inline static int32_t get_offset_of__root_14() { return static_cast<int32_t>(offsetof(BsonWriter_t618B49FFFCFEE187A002908C374F5DFE5979DBCB, ____root_14)); }
	inline BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 * get__root_14() const { return ____root_14; }
	inline BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 ** get_address_of__root_14() { return &____root_14; }
	inline void set__root_14(BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 * value)
	{
		____root_14 = value;
		Il2CppCodeGenWriteBarrier((&____root_14), value);
	}

	inline static int32_t get_offset_of__parent_15() { return static_cast<int32_t>(offsetof(BsonWriter_t618B49FFFCFEE187A002908C374F5DFE5979DBCB, ____parent_15)); }
	inline BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 * get__parent_15() const { return ____parent_15; }
	inline BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 ** get_address_of__parent_15() { return &____parent_15; }
	inline void set__parent_15(BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973 * value)
	{
		____parent_15 = value;
		Il2CppCodeGenWriteBarrier((&____parent_15), value);
	}

	inline static int32_t get_offset_of__propertyName_16() { return static_cast<int32_t>(offsetof(BsonWriter_t618B49FFFCFEE187A002908C374F5DFE5979DBCB, ____propertyName_16)); }
	inline String_t* get__propertyName_16() const { return ____propertyName_16; }
	inline String_t** get_address_of__propertyName_16() { return &____propertyName_16; }
	inline void set__propertyName_16(String_t* value)
	{
		____propertyName_16 = value;
		Il2CppCodeGenWriteBarrier((&____propertyName_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONWRITER_T618B49FFFCFEE187A002908C374F5DFE5979DBCB_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef BASEPOSEPROVIDER_T951B9DB48DBBA7336FFE70B77529EEF42742B50B_H
#define BASEPOSEPROVIDER_T951B9DB48DBBA7336FFE70B77529EEF42742B50B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.Interaction.BasePoseProvider
struct  BasePoseProvider_t951B9DB48DBBA7336FFE70B77529EEF42742B50B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEPOSEPROVIDER_T951B9DB48DBBA7336FFE70B77529EEF42742B50B_H
#ifndef TRACKEDPOSEDRIVER_T7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A_H
#define TRACKEDPOSEDRIVER_T7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpatialTracking.TrackedPoseDriver
struct  TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.SpatialTracking.TrackedPoseDriver/DeviceType UnityEngine.SpatialTracking.TrackedPoseDriver::m_Device
	int32_t ___m_Device_4;
	// UnityEngine.SpatialTracking.TrackedPoseDriver/TrackedPose UnityEngine.SpatialTracking.TrackedPoseDriver::m_PoseSource
	int32_t ___m_PoseSource_5;
	// UnityEngine.Experimental.XR.Interaction.BasePoseProvider UnityEngine.SpatialTracking.TrackedPoseDriver::m_PoseProviderComponent
	BasePoseProvider_t951B9DB48DBBA7336FFE70B77529EEF42742B50B * ___m_PoseProviderComponent_6;
	// UnityEngine.SpatialTracking.TrackedPoseDriver/TrackingType UnityEngine.SpatialTracking.TrackedPoseDriver::m_TrackingType
	int32_t ___m_TrackingType_7;
	// UnityEngine.SpatialTracking.TrackedPoseDriver/UpdateType UnityEngine.SpatialTracking.TrackedPoseDriver::m_UpdateType
	int32_t ___m_UpdateType_8;
	// System.Boolean UnityEngine.SpatialTracking.TrackedPoseDriver::m_UseRelativeTransform
	bool ___m_UseRelativeTransform_9;
	// UnityEngine.Pose UnityEngine.SpatialTracking.TrackedPoseDriver::m_OriginPose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_OriginPose_10;

public:
	inline static int32_t get_offset_of_m_Device_4() { return static_cast<int32_t>(offsetof(TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A, ___m_Device_4)); }
	inline int32_t get_m_Device_4() const { return ___m_Device_4; }
	inline int32_t* get_address_of_m_Device_4() { return &___m_Device_4; }
	inline void set_m_Device_4(int32_t value)
	{
		___m_Device_4 = value;
	}

	inline static int32_t get_offset_of_m_PoseSource_5() { return static_cast<int32_t>(offsetof(TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A, ___m_PoseSource_5)); }
	inline int32_t get_m_PoseSource_5() const { return ___m_PoseSource_5; }
	inline int32_t* get_address_of_m_PoseSource_5() { return &___m_PoseSource_5; }
	inline void set_m_PoseSource_5(int32_t value)
	{
		___m_PoseSource_5 = value;
	}

	inline static int32_t get_offset_of_m_PoseProviderComponent_6() { return static_cast<int32_t>(offsetof(TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A, ___m_PoseProviderComponent_6)); }
	inline BasePoseProvider_t951B9DB48DBBA7336FFE70B77529EEF42742B50B * get_m_PoseProviderComponent_6() const { return ___m_PoseProviderComponent_6; }
	inline BasePoseProvider_t951B9DB48DBBA7336FFE70B77529EEF42742B50B ** get_address_of_m_PoseProviderComponent_6() { return &___m_PoseProviderComponent_6; }
	inline void set_m_PoseProviderComponent_6(BasePoseProvider_t951B9DB48DBBA7336FFE70B77529EEF42742B50B * value)
	{
		___m_PoseProviderComponent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PoseProviderComponent_6), value);
	}

	inline static int32_t get_offset_of_m_TrackingType_7() { return static_cast<int32_t>(offsetof(TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A, ___m_TrackingType_7)); }
	inline int32_t get_m_TrackingType_7() const { return ___m_TrackingType_7; }
	inline int32_t* get_address_of_m_TrackingType_7() { return &___m_TrackingType_7; }
	inline void set_m_TrackingType_7(int32_t value)
	{
		___m_TrackingType_7 = value;
	}

	inline static int32_t get_offset_of_m_UpdateType_8() { return static_cast<int32_t>(offsetof(TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A, ___m_UpdateType_8)); }
	inline int32_t get_m_UpdateType_8() const { return ___m_UpdateType_8; }
	inline int32_t* get_address_of_m_UpdateType_8() { return &___m_UpdateType_8; }
	inline void set_m_UpdateType_8(int32_t value)
	{
		___m_UpdateType_8 = value;
	}

	inline static int32_t get_offset_of_m_UseRelativeTransform_9() { return static_cast<int32_t>(offsetof(TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A, ___m_UseRelativeTransform_9)); }
	inline bool get_m_UseRelativeTransform_9() const { return ___m_UseRelativeTransform_9; }
	inline bool* get_address_of_m_UseRelativeTransform_9() { return &___m_UseRelativeTransform_9; }
	inline void set_m_UseRelativeTransform_9(bool value)
	{
		___m_UseRelativeTransform_9 = value;
	}

	inline static int32_t get_offset_of_m_OriginPose_10() { return static_cast<int32_t>(offsetof(TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A, ___m_OriginPose_10)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_OriginPose_10() const { return ___m_OriginPose_10; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_OriginPose_10() { return &___m_OriginPose_10; }
	inline void set_m_OriginPose_10(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_OriginPose_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKEDPOSEDRIVER_T7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A_H
#ifndef BASEARMMODEL_T3D5FC4A9BC5D3133D9D8190B32F9E215F3CDCDA7_H
#define BASEARMMODEL_T3D5FC4A9BC5D3133D9D8190B32F9E215F3CDCDA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.Interaction.BaseArmModel
struct  BaseArmModel_t3D5FC4A9BC5D3133D9D8190B32F9E215F3CDCDA7  : public BasePoseProvider_t951B9DB48DBBA7336FFE70B77529EEF42742B50B
{
public:
	// UnityEngine.SpatialTracking.TrackedPoseDriver/TrackedPose UnityEngine.Experimental.XR.Interaction.BaseArmModel::m_HeadPoseSource
	int32_t ___m_HeadPoseSource_4;
	// UnityEngine.SpatialTracking.TrackedPoseDriver/TrackedPose UnityEngine.Experimental.XR.Interaction.BaseArmModel::m_PoseSource
	int32_t ___m_PoseSource_5;

public:
	inline static int32_t get_offset_of_m_HeadPoseSource_4() { return static_cast<int32_t>(offsetof(BaseArmModel_t3D5FC4A9BC5D3133D9D8190B32F9E215F3CDCDA7, ___m_HeadPoseSource_4)); }
	inline int32_t get_m_HeadPoseSource_4() const { return ___m_HeadPoseSource_4; }
	inline int32_t* get_address_of_m_HeadPoseSource_4() { return &___m_HeadPoseSource_4; }
	inline void set_m_HeadPoseSource_4(int32_t value)
	{
		___m_HeadPoseSource_4 = value;
	}

	inline static int32_t get_offset_of_m_PoseSource_5() { return static_cast<int32_t>(offsetof(BaseArmModel_t3D5FC4A9BC5D3133D9D8190B32F9E215F3CDCDA7, ___m_PoseSource_5)); }
	inline int32_t get_m_PoseSource_5() const { return ___m_PoseSource_5; }
	inline int32_t* get_address_of_m_PoseSource_5() { return &___m_PoseSource_5; }
	inline void set_m_PoseSource_5(int32_t value)
	{
		___m_PoseSource_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEARMMODEL_T3D5FC4A9BC5D3133D9D8190B32F9E215F3CDCDA7_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5200 = { sizeof (XmlDocumentTypeWrapper_t580823554B71FD5205C8D8177B0EB1B339BC0BDF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5200[1] = 
{
	XmlDocumentTypeWrapper_t580823554B71FD5205C8D8177B0EB1B339BC0BDF::get_offset_of__documentType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5201 = { sizeof (XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5201[3] = 
{
	XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8::get_offset_of__node_0(),
	XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8::get_offset_of__childNodes_1(),
	XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8::get_offset_of__attributes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5202 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5203 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5204 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5205 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5206 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5207 = { sizeof (XDeclarationWrapper_t19BEE7F4C333B0A8EEAC25BDA6614C131ADE698B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5207[1] = 
{
	XDeclarationWrapper_t19BEE7F4C333B0A8EEAC25BDA6614C131ADE698B::get_offset_of_U3CDeclarationU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5208 = { sizeof (XDocumentTypeWrapper_t594BC04853709973E3A7CEE68502A1D6256DF7AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5208[1] = 
{
	XDocumentTypeWrapper_t594BC04853709973E3A7CEE68502A1D6256DF7AD::get_offset_of__documentType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5209 = { sizeof (XDocumentWrapper_t715FF948E28684D8586F3AA249CC48E112921363), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5210 = { sizeof (XTextWrapper_t0C09B42E98EB742AF5813BDEB9230C3DD113CD90), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5211 = { sizeof (XCommentWrapper_tF6D511EEEC8190037957744ABC04D1C17B370A96), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5212 = { sizeof (XProcessingInstructionWrapper_t6951DEA9256CEE1E71C353D0BB1B20C34E85B4EA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5213 = { sizeof (XContainerWrapper_tF1D4A2616F408C9F225A2A2F92C75CBA72C347C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5213[1] = 
{
	XContainerWrapper_tF1D4A2616F408C9F225A2A2F92C75CBA72C347C0::get_offset_of__childNodes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5214 = { sizeof (XObjectWrapper_t725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A), -1, sizeof(XObjectWrapper_t725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5214[2] = 
{
	XObjectWrapper_t725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A_StaticFields::get_offset_of_EmptyChildNodes_0(),
	XObjectWrapper_t725ED5487EB70103E8B8E8A3C049F2EB2EE74E0A::get_offset_of__xmlObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5215 = { sizeof (XAttributeWrapper_t560E9EF1F2D11F507C449C41A682293650E5C0D5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5216 = { sizeof (XElementWrapper_t5BDE61B8CE601DD94A50452AFAD7E88A969EA2D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5216[1] = 
{
	XElementWrapper_t5BDE61B8CE601DD94A50452AFAD7E88A969EA2D7::get_offset_of__attributes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5217 = { sizeof (XmlNodeConverter_t12A0E794803AB63F65E00D245B7EE3C4C8CEB304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5217[3] = 
{
	XmlNodeConverter_t12A0E794803AB63F65E00D245B7EE3C4C8CEB304::get_offset_of_U3CDeserializeRootElementNameU3Ek__BackingField_0(),
	XmlNodeConverter_t12A0E794803AB63F65E00D245B7EE3C4C8CEB304::get_offset_of_U3CWriteArrayAttributeU3Ek__BackingField_1(),
	XmlNodeConverter_t12A0E794803AB63F65E00D245B7EE3C4C8CEB304::get_offset_of_U3COmitRootObjectU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5218 = { sizeof (BsonBinaryType_t1FA8FE0696F46BBDF161A97E2AB1D1AF726B9858)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5218[8] = 
{
	BsonBinaryType_t1FA8FE0696F46BBDF161A97E2AB1D1AF726B9858::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5219 = { sizeof (BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2), -1, sizeof(BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5219[4] = 
{
	BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2_StaticFields::get_offset_of_Encoding_0(),
	BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2::get_offset_of__writer_1(),
	BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2::get_offset_of__largeByteBuffer_2(),
	BsonBinaryWriter_t39DD2DD462DE9FF693FFEF86A404347C8B060EA2::get_offset_of_U3CDateTimeKindHandlingU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5220 = { sizeof (BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5220[2] = 
{
	BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973::get_offset_of_U3CParentU3Ek__BackingField_0(),
	BsonToken_t3703970749E7450A6A4AA0DBF8DFDE514F518973::get_offset_of_U3CCalculatedSizeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5221 = { sizeof (BsonObject_tCC61A23F42600B1B6CA194FF0E549E688AAA45BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5221[1] = 
{
	BsonObject_tCC61A23F42600B1B6CA194FF0E549E688AAA45BB::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5222 = { sizeof (BsonArray_t409BB62F0CCDED5660D1BABDA0BB2085FB09C915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5222[1] = 
{
	BsonArray_t409BB62F0CCDED5660D1BABDA0BB2085FB09C915::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5223 = { sizeof (BsonValue_t1CADCCC6BD9CB06C512942B38623671052A09262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5223[2] = 
{
	BsonValue_t1CADCCC6BD9CB06C512942B38623671052A09262::get_offset_of__value_2(),
	BsonValue_t1CADCCC6BD9CB06C512942B38623671052A09262::get_offset_of__type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5224 = { sizeof (BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5224[2] = 
{
	BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46::get_offset_of_U3CByteCountU3Ek__BackingField_4(),
	BsonString_t7BA8171A0F1E08A1320F89FD6A4AAC201E402D46::get_offset_of_U3CIncludeLengthU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5225 = { sizeof (BsonBinary_tE52CAC83857D4FC5A09DBC2954EBCCF54EAAA1AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5225[1] = 
{
	BsonBinary_tE52CAC83857D4FC5A09DBC2954EBCCF54EAAA1AB::get_offset_of_U3CBinaryTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5226 = { sizeof (BsonRegex_t4366470059BFDDDD190CAFC4E538DFB38D88468E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5226[2] = 
{
	BsonRegex_t4366470059BFDDDD190CAFC4E538DFB38D88468E::get_offset_of_U3CPatternU3Ek__BackingField_2(),
	BsonRegex_t4366470059BFDDDD190CAFC4E538DFB38D88468E::get_offset_of_U3COptionsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5227 = { sizeof (BsonProperty_t4D924C12981551812A0C17ACA98F3D5C0C3C9ECC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5227[2] = 
{
	BsonProperty_t4D924C12981551812A0C17ACA98F3D5C0C3C9ECC::get_offset_of_U3CNameU3Ek__BackingField_0(),
	BsonProperty_t4D924C12981551812A0C17ACA98F3D5C0C3C9ECC::get_offset_of_U3CValueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5228 = { sizeof (BsonType_t7BD027216E37FF11157FAA220E1E48019CF823D1)+ sizeof (RuntimeObject), sizeof(int8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5228[21] = 
{
	BsonType_t7BD027216E37FF11157FAA220E1E48019CF823D1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5229 = { sizeof (BsonWriter_t618B49FFFCFEE187A002908C374F5DFE5979DBCB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5229[4] = 
{
	BsonWriter_t618B49FFFCFEE187A002908C374F5DFE5979DBCB::get_offset_of__writer_13(),
	BsonWriter_t618B49FFFCFEE187A002908C374F5DFE5979DBCB::get_offset_of__root_14(),
	BsonWriter_t618B49FFFCFEE187A002908C374F5DFE5979DBCB::get_offset_of__parent_15(),
	BsonWriter_t618B49FFFCFEE187A002908C374F5DFE5979DBCB::get_offset_of__propertyName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5230 = { sizeof (BsonObjectId_tACBE83AF96B5BCA4A847AD22C826939C821DD2D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5230[1] = 
{
	BsonObjectId_tACBE83AF96B5BCA4A847AD22C826939C821DD2D7::get_offset_of_U3CValueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5231 = { sizeof (U3CPrivateImplementationDetailsU3E_tBBC8E11E0053333620B8C574BE27D9CD62A99591), -1, sizeof(U3CPrivateImplementationDetailsU3E_tBBC8E11E0053333620B8C574BE27D9CD62A99591_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5231[5] = 
{
	U3CPrivateImplementationDetailsU3E_tBBC8E11E0053333620B8C574BE27D9CD62A99591_StaticFields::get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0(),
	U3CPrivateImplementationDetailsU3E_tBBC8E11E0053333620B8C574BE27D9CD62A99591_StaticFields::get_offset_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1(),
	U3CPrivateImplementationDetailsU3E_tBBC8E11E0053333620B8C574BE27D9CD62A99591_StaticFields::get_offset_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2(),
	U3CPrivateImplementationDetailsU3E_tBBC8E11E0053333620B8C574BE27D9CD62A99591_StaticFields::get_offset_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3(),
	U3CPrivateImplementationDetailsU3E_tBBC8E11E0053333620B8C574BE27D9CD62A99591_StaticFields::get_offset_of_E92B39D8233061927D9ACDE54665E68E7535635A_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5232 = { sizeof (__StaticArrayInitTypeSizeU3D10_t2210C3858CD7BBD086B8C29DD7AE0B07CF930F6E)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D10_t2210C3858CD7BBD086B8C29DD7AE0B07CF930F6E ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5233 = { sizeof (__StaticArrayInitTypeSizeU3D12_t6A42B5D9C1B6EC123AB21D43DAB4814988F7FE5A)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t6A42B5D9C1B6EC123AB21D43DAB4814988F7FE5A ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5234 = { sizeof (__StaticArrayInitTypeSizeU3D28_tD95E73C8180D450CB90FBFA3367F31DECB46B9A8)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D28_tD95E73C8180D450CB90FBFA3367F31DECB46B9A8 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5235 = { sizeof (__StaticArrayInitTypeSizeU3D52_tA3556BE2FDC9141F7C4FB86611890DADE17CDD63)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D52_tA3556BE2FDC9141F7C4FB86611890DADE17CDD63 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5236 = { sizeof (U3CModuleU3E_t9235715C8404A6E9A7653F6E7FFDB5E36D9C2D95), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5237 = { sizeof (AsyncCameraImageConversionStatus_tF869FB2C28DB2729AAC068D746FBDF9076725ECA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5237[6] = 
{
	AsyncCameraImageConversionStatus_tF869FB2C28DB2729AAC068D746FBDF9076725ECA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5238 = { sizeof (CameraConfiguration_tF2E20F6B31C6014A39C87E6C430247D1B0F3BDD7)+ sizeof (RuntimeObject), sizeof(CameraConfiguration_tF2E20F6B31C6014A39C87E6C430247D1B0F3BDD7 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5238[2] = 
{
	CameraConfiguration_tF2E20F6B31C6014A39C87E6C430247D1B0F3BDD7::get_offset_of_m_Resolution_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraConfiguration_tF2E20F6B31C6014A39C87E6C430247D1B0F3BDD7::get_offset_of_m_Framerate_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5239 = { sizeof (CameraConfigurationCollection_t32228119EE1DD23BE336BD6F1C49B339B10DB618)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5239[1] = 
{
	CameraConfigurationCollection_t32228119EE1DD23BE336BD6F1C49B339B10DB618::get_offset_of_m_CameraConfigApi_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5240 = { sizeof (ConfigEnumerator_tF8562092447F19D8DC60A0CEC6355037826B652F)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5240[3] = 
{
	ConfigEnumerator_tF8562092447F19D8DC60A0CEC6355037826B652F::get_offset_of_m_Index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConfigEnumerator_tF8562092447F19D8DC60A0CEC6355037826B652F::get_offset_of_m_Count_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConfigEnumerator_tF8562092447F19D8DC60A0CEC6355037826B652F::get_offset_of_m_CameraConfigApi_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5241 = { sizeof (CameraFocusMode_t198966DE20F938A7890CE5EBFB00CCA53EAF4ED4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5241[3] = 
{
	CameraFocusMode_t198966DE20F938A7890CE5EBFB00CCA53EAF4ED4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5242 = { sizeof (CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E)+ sizeof (RuntimeObject), -1, sizeof(CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5242[7] = 
{
	CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E::get_offset_of_U3CdimensionsU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E::get_offset_of_U3CplaneCountU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E::get_offset_of_U3CformatU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E::get_offset_of_U3CtimestampU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E::get_offset_of_m_NativeHandle_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E::get_offset_of_m_CameraImageApi_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E_StaticFields::get_offset_of_s_OnAsyncConversionComplete_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5243 = { sizeof (CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1)+ sizeof (RuntimeObject), sizeof(CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5243[4] = 
{
	CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1::get_offset_of_m_InputRect_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1::get_offset_of_m_OutputDimensions_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1::get_offset_of_m_Format_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1::get_offset_of_m_Transformation_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5244 = { sizeof (CameraImageFormat_t3B37B8C0CD1FDB32590DE83ABB369EEB65307EBD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5244[4] = 
{
	CameraImageFormat_t3B37B8C0CD1FDB32590DE83ABB369EEB65307EBD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5245 = { sizeof (CameraImageTransformation_t9244C88533E9586E4237E49856EE71A34B60E48D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5245[4] = 
{
	CameraImageTransformation_t9244C88533E9586E4237E49856EE71A34B60E48D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5246 = { sizeof (DefaultCameraConfigApi_t125599F6D1891BEF834E1B20C71C503AB27F4091), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5247 = { sizeof (DefaultCameraImageApi_t32807EEA82DF3B067158E1DCEA117A577B5E3BCB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5248 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5249 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5250 = { sizeof (PlaneDetectionFlags_t973AF579C70632C3A6345A61C688B125C5864215)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5250[4] = 
{
	PlaneDetectionFlags_t973AF579C70632C3A6345A61C688B125C5864215::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5251 = { sizeof (PlaneDetectionFlagsMaskAttribute_t891FFF72B896019B6EF9B5BC66B66B7482D85E36), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5252 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5252[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5253 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5254 = { sizeof (RegistrationHelper_t0925D2AC89FBDCEB2FA94AEDADCBA81670F5B531), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5255 = { sizeof (SessionAvailability_t66AE38CEACBF09105F9564006DE37A94C9DB370F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5255[4] = 
{
	SessionAvailability_t66AE38CEACBF09105F9564006DE37A94C9DB370F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5256 = { sizeof (SessionAvailabilityExtensions_tE9EC580D14C93F2A5732599A5A7F0435EC737929), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5257 = { sizeof (SessionInstallationStatus_tB8FC754E8CCA38CA2F32E61B354743D33DA3939B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5257[7] = 
{
	SessionInstallationStatus_tB8FC754E8CCA38CA2F32E61B354743D33DA3939B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5258 = { sizeof (XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D), -1, sizeof(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5258[14] = 
{
	XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields::get_offset_of_s_IsPermissionGrantedDelegate_0(),
	XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields::get_offset_of_s_TryGetColorCorrectionDelegate_1(),
	XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields::get_offset_of_s_GetNativePtrDelegate_2(),
	XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields::get_offset_of_s_CameraImageApi_3(),
	XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields::get_offset_of_s_DefaultCameraImageApi_4(),
	XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields::get_offset_of_s_CameraConfigApi_5(),
	XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields::get_offset_of_s_DefaultCameraConfigApi_6(),
	XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields::get_offset_of_s_TrySetFocusModeDelegate_7(),
	XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields::get_offset_of_s_IsPermissionGrantedDelegates_8(),
	XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields::get_offset_of_s_TryGetColorCorrectionDelegates_9(),
	XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields::get_offset_of_s_GetNativePtrDelegates_10(),
	XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields::get_offset_of_s_CameraImageApis_11(),
	XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields::get_offset_of_s_CameraConfigApis_12(),
	XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_StaticFields::get_offset_of_s_TrySetFocusModeDelegates_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5259 = { sizeof (TryGetColorCorrectionDelegate_t30B930C66D1DCEC1490F1A6624404BA665652BD8), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5260 = { sizeof (OnImageRequestCompleteDelegate_tC602138547507C797A3BE8CC2D7022659FA457B2), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5261 = { sizeof (XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE), -1, sizeof(XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5261[6] = 
{
	XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE_StaticFields::get_offset_of_s_GetNativePtrDelegate_0(),
	XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE_StaticFields::get_offset_of_s_GetTrackingStateDelegate_1(),
	XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE_StaticFields::get_offset_of_s_TrySetPlaneDetectionFlagsDelegate_2(),
	XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE_StaticFields::get_offset_of_s_GetNativePtrDelegates_3(),
	XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE_StaticFields::get_offset_of_s_GetTrackingStateDelegates_4(),
	XRPlaneExtensions_tE5907A4D3FC04AB6A65D1F86404095C31362D4BE_StaticFields::get_offset_of_s_TrySetPlaneDetectionFlagsDelegates_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5262 = { sizeof (XRReferencePointExtensions_tDB4598C0A44D28208D387FF166DD81A02D92D74D), -1, sizeof(XRReferencePointExtensions_tDB4598C0A44D28208D387FF166DD81A02D92D74D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5262[4] = 
{
	XRReferencePointExtensions_tDB4598C0A44D28208D387FF166DD81A02D92D74D_StaticFields::get_offset_of_s_AttachReferencePointDelegate_0(),
	XRReferencePointExtensions_tDB4598C0A44D28208D387FF166DD81A02D92D74D_StaticFields::get_offset_of_s_GetNativePtrDelegate_1(),
	XRReferencePointExtensions_tDB4598C0A44D28208D387FF166DD81A02D92D74D_StaticFields::get_offset_of_s_AttachReferencePointDelegates_2(),
	XRReferencePointExtensions_tDB4598C0A44D28208D387FF166DD81A02D92D74D_StaticFields::get_offset_of_s_GetNativePtrDelegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5263 = { sizeof (AttachReferencePointDelegate_t8E14528144FB8B3DE6B44FCD01B3B4C2090236DF), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5264 = { sizeof (XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49), -1, sizeof(XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5264[6] = 
{
	XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49_StaticFields::get_offset_of_s_InstallAsyncDelegate_0(),
	XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49_StaticFields::get_offset_of_s_GetAvailabilityAsyncDelegate_1(),
	XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49_StaticFields::get_offset_of_s_GetNativePtrDelegate_2(),
	XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49_StaticFields::get_offset_of_s_InstallAsyncDelegates_3(),
	XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49_StaticFields::get_offset_of_s_GetAvailabilityAsyncDelegates_4(),
	XRSessionExtensions_t90CFFFAE273DEBF123D02B08B529F491CFA1DF49_StaticFields::get_offset_of_s_GetNativePtrDelegates_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5265 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5266 = { sizeof (U3CModuleU3E_tD7C26C6BCAC76F130F59027F1362DDBCC127637D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5267 = { sizeof (FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5267[2] = 
{
	FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C::get_offset_of_U3CxrFaceU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C::get_offset_of_U3CxrFaceSubsystemU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5268 = { sizeof (FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5268[2] = 
{
	FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651::get_offset_of_U3CxrFaceU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651::get_offset_of_U3CxrFaceSubsystemU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5269 = { sizeof (FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5269[2] = 
{
	FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881::get_offset_of_U3CxrFaceU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881::get_offset_of_U3CxrFaceSubsystemU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5270 = { sizeof (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246)+ sizeof (RuntimeObject), sizeof(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5270[4] = 
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246::get_offset_of_m_TrackableId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246::get_offset_of_m_Pose_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246::get_offset_of_m_WasUpdated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246::get_offset_of_m_IsTracked_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5271 = { sizeof (XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86), -1, sizeof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5271[6] = 
{
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields::get_offset_of_s_FacesAddedThisFrame_1(),
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields::get_offset_of_s_FacesUpdatedThisFrame_2(),
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields::get_offset_of_s_FacesRemovedThisFrame_3(),
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86::get_offset_of_faceAdded_4(),
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86::get_offset_of_faceUpdated_5(),
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86::get_offset_of_faceRemoved_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5272 = { sizeof (XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5272[3] = 
{
	XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19::get_offset_of_U3CsupportsFacePoseU3Ek__BackingField_2(),
	XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19::get_offset_of_U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3(),
	XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19::get_offset_of_U3CsupportsFaceMeshUVsU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5273 = { sizeof (FaceSubsystemCapabilities_tA2A2AD15E172EF1B91CE85994931F8C11330BB2C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5273[5] = 
{
	FaceSubsystemCapabilities_tA2A2AD15E172EF1B91CE85994931F8C11330BB2C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5274 = { sizeof (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5274[3] = 
{
	FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040::get_offset_of_implementationType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040::get_offset_of_U3CcapabilitiesU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5275 = { sizeof (U3CModuleU3E_t61085D0352A59B7105C361DB16CFAD5E8986AE2E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5276 = { sizeof (BaseArmModel_t3D5FC4A9BC5D3133D9D8190B32F9E215F3CDCDA7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5276[2] = 
{
	BaseArmModel_t3D5FC4A9BC5D3133D9D8190B32F9E215F3CDCDA7::get_offset_of_m_HeadPoseSource_4(),
	BaseArmModel_t3D5FC4A9BC5D3133D9D8190B32F9E215F3CDCDA7::get_offset_of_m_PoseSource_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5277 = { sizeof (BasePoseProvider_t951B9DB48DBBA7336FFE70B77529EEF42742B50B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5278 = { sizeof (TrackedPoseDriverDataDescription_tEA05A7320AED44B790D678CF1B65B48138DA1ECA), -1, sizeof(TrackedPoseDriverDataDescription_tEA05A7320AED44B790D678CF1B65B48138DA1ECA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5278[1] = 
{
	TrackedPoseDriverDataDescription_tEA05A7320AED44B790D678CF1B65B48138DA1ECA_StaticFields::get_offset_of_DeviceData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5279 = { sizeof (PoseData_tF79A33767571168AAB333A85A6B6F0F9A8825DFE)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5279[2] = 
{
	PoseData_tF79A33767571168AAB333A85A6B6F0F9A8825DFE::get_offset_of_PoseNames_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoseData_tF79A33767571168AAB333A85A6B6F0F9A8825DFE::get_offset_of_Poses_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5280 = { sizeof (PoseDataSource_t922E6B2F7058F8A78172B8E29AEB0BBD748B4657), -1, sizeof(PoseDataSource_t922E6B2F7058F8A78172B8E29AEB0BBD748B4657_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5280[1] = 
{
	PoseDataSource_t922E6B2F7058F8A78172B8E29AEB0BBD748B4657_StaticFields::get_offset_of_nodeStates_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5281 = { sizeof (TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5281[7] = 
{
	TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A::get_offset_of_m_Device_4(),
	TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A::get_offset_of_m_PoseSource_5(),
	TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A::get_offset_of_m_PoseProviderComponent_6(),
	TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A::get_offset_of_m_TrackingType_7(),
	TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A::get_offset_of_m_UpdateType_8(),
	TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A::get_offset_of_m_UseRelativeTransform_9(),
	TrackedPoseDriver_t7B9E8C9FDD60EB5023A757F4ED70AC92AA3C8C0A::get_offset_of_m_OriginPose_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5282 = { sizeof (DeviceType_tC20CDCE75CB9BCB7EEF098A83B16071A0C86D198)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5282[4] = 
{
	DeviceType_tC20CDCE75CB9BCB7EEF098A83B16071A0C86D198::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5283 = { sizeof (TrackedPose_tBECB0ED5440415EE75C061C0588F16CF0D8E5DFA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5283[12] = 
{
	TrackedPose_tBECB0ED5440415EE75C061C0588F16CF0D8E5DFA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5284 = { sizeof (TrackingType_t54A99FD46DD997F9BDD94EB51082AEA72640B28A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5284[4] = 
{
	TrackingType_t54A99FD46DD997F9BDD94EB51082AEA72640B28A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5285 = { sizeof (UpdateType_t2E40BE3577C3CE3CB868167C75BA759A2B76BB10)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5285[4] = 
{
	UpdateType_t2E40BE3577C3CE3CB868167C75BA759A2B76BB10::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5286 = { sizeof (U3CModuleU3E_t76DD45B11E728799BA16B6E93B81827DD86E5AEE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5287 = { sizeof (EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5287[3] = 
{
	EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5288 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5289 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5290 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5291 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5292 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5293 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5294 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5295 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5296 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5297 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5298 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5299 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
