﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_tF2C68FFE572BEBC9BD9FFBA4B794164E3A87CBED;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE;
// System.Action`3<System.Int32,System.Int32,System.Int32>
struct Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Attribute
struct Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IDisposable
struct IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodBase
struct MethodBase_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TheNextFlow.UnityPlugins.AlertDialogProxy
struct AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241;
// TheNextFlow.UnityPlugins.AndroidNativePopups
struct AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368;
// TheNextFlow.UnityPlugins.DatePickerDialogProxy
struct DatePickerDialogProxy_t3DC13C8B3EFB9AB6890BF88F75AD27564AEB0F0E;
// TheNextFlow.UnityPlugins.IosNativePopups/CompletionHandler
struct CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE;
// TheNextFlow.UnityPlugins.IosNativePopups/InternalHandler
struct InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5;
// TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey0
struct U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18;
// TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey1
struct U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72;
// TheNextFlow.UnityPlugins.TimePickerDialogProxy
struct TimePickerDialogProxy_tEA3E774F2ACE744CCC2DBBF4A78BD2049B76E266;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;

extern RuntimeClass* AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_il2cpp_TypeInfo_var;
extern RuntimeClass* CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var;
extern RuntimeClass* CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var;
extern RuntimeClass* Guid_t_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern RuntimeClass* InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18_il2cpp_TypeInfo_var;
extern RuntimeClass* U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral42421480C15A2650AE377F9F228CB3729E8504FD;
extern String_t* _stringLiteral561DAA3E31894E0B8E0314512463120E85FBCDF9;
extern String_t* _stringLiteral73FCBF0DF8FE0B17A4F7C855909540A92F4C79E1;
extern String_t* _stringLiteralB0494FC133E51BA549EC26B16950914A2077FCE2;
extern String_t* _stringLiteralE62471C7B901EEA0B4178C8B96E00C73B67E44C6;
extern const RuntimeMethod* Action_2_Invoke_mCEA7B556FB0AEA5B7E000DC9073DC99CD834DE9C_RuntimeMethod_var;
extern const RuntimeMethod* Action_3_Invoke_m0011BAFE193EBA2976AA455819357DC75716EB61_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisAlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241_m764ACB117AE67296EC7A0CF67BC27E733DDD0123_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisAlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241_mB1C0A1FE3881A26839A8C87366B9C70252C88E22_RuntimeMethod_var;
extern const RuntimeMethod* IosNativePopups_ClickedButtonAtIndexCallback_m9394831AC9C6A86B31A6A0EE1AE9E1E1F1A1C183_RuntimeMethod_var;
extern const RuntimeMethod* U3COpenAlertDialogU3Ec__AnonStorey0_U3CU3Em__0_m53B608EAE978F2297C9D5ECF5B950008986DFB53_RuntimeMethod_var;
extern const RuntimeMethod* U3COpenAlertDialogU3Ec__AnonStorey1_U3CU3Em__1_mE376A1D7C2A8F3458666785EC28BE96D2CBA2D4B_RuntimeMethod_var;
extern const uint32_t AlertDialogProxy_OnCancel_m2D06305EEF95D9246598C65F9805346C2A055A80_MetadataUsageId;
extern const uint32_t AlertDialogProxy_OnNeutral_m9A2184158FE7CC2273B2E052DB51F6E8D0DD749F_MetadataUsageId;
extern const uint32_t AlertDialogProxy_OnOk_m719961E3F235E538463D00CED8FDDB44DD290FEF_MetadataUsageId;
extern const uint32_t AndroidNativePopups_CallStatic_m7CBCA98EF2737BA5C8318EFDC76DC70657F36B02_MetadataUsageId;
extern const uint32_t AndroidNativePopups_CreateAndroidJavaObject_mAC69C31D30F3179574DB4F9BD54B27F1C98E8BAC_MetadataUsageId;
extern const uint32_t AndroidNativePopups_Dispose_m52FDFEEB79D1ABF52350C710729C3529CE938823_MetadataUsageId;
extern const uint32_t AndroidNativePopups_OpenAlertDialog_m7F3F2553A4B0653775F714E60DA0645B48D5999B_MetadataUsageId;
extern const uint32_t AndroidNativePopups_OpenAlertDialog_mE4B2462D28BB5386AEB46A77A967F0FDEC55AE21_MetadataUsageId;
extern const uint32_t AndroidNativePopups__cctor_m32FC8C1BEFB6548A9AF6642E091C78DEC1887941_MetadataUsageId;
extern const uint32_t CompletionHandler_BeginInvoke_mB81B368F056C2144C97078ABCEF111F8E44C82B4_MetadataUsageId;
extern const uint32_t DatePickerDialogProxy_OnDateSet_mA2E9A372EB60CEDDDC9F9C45724D511F0182899A_MetadataUsageId;
extern const uint32_t InternalHandler_BeginInvoke_mE9C6ECA9F22A82EAB96A9A5428A8ECEFF0D2074F_MetadataUsageId;
extern const uint32_t IosNativePopups_ClickedButtonAtIndexCallback_m9394831AC9C6A86B31A6A0EE1AE9E1E1F1A1C183_MetadataUsageId;
extern const uint32_t IosNativePopups_OpenAlertDialog_m07E4B6D10F794A30E1F8A0B41CADFBA49D85FE13_MetadataUsageId;
extern const uint32_t MobileNativePopups_OpenAlertDialog_m7F5DBA7DF33B9CFA84BC49D6E9C171D3485EE8D9_MetadataUsageId;
extern const uint32_t MobileNativePopups_OpenAlertDialog_m9DA148B35CB9CEB0AAC1017CF6C1006FDF7811D6_MetadataUsageId;
extern const uint32_t TimePickerDialogProxy_OnTimeSet_m23F659ABBCD69358763F5E20033A6FC6531ED6BC_MetadataUsageId;

struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B;
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;


#ifndef U3CMODULEU3E_TF3A658440E2EA6B9E28737BEB10616BB13D75E94_H
#define U3CMODULEU3E_TF3A658440E2EA6B9E28737BEB10616BB13D75E94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tF3A658440E2EA6B9E28737BEB10616BB13D75E94 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TF3A658440E2EA6B9E28737BEB10616BB13D75E94_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ANDROIDNATIVEPOPUPS_TFCDF1E0DDBEC0C449E8C74993CE36894D3862368_H
#define ANDROIDNATIVEPOPUPS_TFCDF1E0DDBEC0C449E8C74993CE36894D3862368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.AndroidNativePopups
struct  AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368  : public RuntimeObject
{
public:

public:
};

struct AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_StaticFields
{
public:
	// System.IDisposable TheNextFlow.UnityPlugins.AndroidNativePopups::androidUtils
	RuntimeObject* ___androidUtils_0;

public:
	inline static int32_t get_offset_of_androidUtils_0() { return static_cast<int32_t>(offsetof(AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_StaticFields, ___androidUtils_0)); }
	inline RuntimeObject* get_androidUtils_0() const { return ___androidUtils_0; }
	inline RuntimeObject** get_address_of_androidUtils_0() { return &___androidUtils_0; }
	inline void set_androidUtils_0(RuntimeObject* value)
	{
		___androidUtils_0 = value;
		Il2CppCodeGenWriteBarrier((&___androidUtils_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDNATIVEPOPUPS_TFCDF1E0DDBEC0C449E8C74993CE36894D3862368_H
#ifndef IOSNATIVEPOPUPS_T6C8DB9578139592203A95BAB1632A0891D2AAA4B_H
#define IOSNATIVEPOPUPS_T6C8DB9578139592203A95BAB1632A0891D2AAA4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.IosNativePopups
struct  IosNativePopups_t6C8DB9578139592203A95BAB1632A0891D2AAA4B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNATIVEPOPUPS_T6C8DB9578139592203A95BAB1632A0891D2AAA4B_H
#ifndef MOBILENATIVEPOPUPS_T4171E4F9ED145F876E9953246D168B2CBA6F00D0_H
#define MOBILENATIVEPOPUPS_T4171E4F9ED145F876E9953246D168B2CBA6F00D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.MobileNativePopups
struct  MobileNativePopups_t4171E4F9ED145F876E9953246D168B2CBA6F00D0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILENATIVEPOPUPS_T4171E4F9ED145F876E9953246D168B2CBA6F00D0_H
#ifndef U3COPENALERTDIALOGU3EC__ANONSTOREY0_T7AA3F28020ACCEAB250731370FC4D05F470B4B18_H
#define U3COPENALERTDIALOGU3EC__ANONSTOREY0_T7AA3F28020ACCEAB250731370FC4D05F470B4B18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey0
struct  U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18  : public RuntimeObject
{
public:
	// System.Action TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey0::onCancel
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onCancel_0;

public:
	inline static int32_t get_offset_of_onCancel_0() { return static_cast<int32_t>(offsetof(U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18, ___onCancel_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onCancel_0() const { return ___onCancel_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onCancel_0() { return &___onCancel_0; }
	inline void set_onCancel_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onCancel_0 = value;
		Il2CppCodeGenWriteBarrier((&___onCancel_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3COPENALERTDIALOGU3EC__ANONSTOREY0_T7AA3F28020ACCEAB250731370FC4D05F470B4B18_H
#ifndef U3COPENALERTDIALOGU3EC__ANONSTOREY1_T464A898B52C251237E9C1DA5250B421534114C72_H
#define U3COPENALERTDIALOGU3EC__ANONSTOREY1_T464A898B52C251237E9C1DA5250B421534114C72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey1
struct  U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72  : public RuntimeObject
{
public:
	// System.Action TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey1::onCancel
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onCancel_0;
	// System.Action TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey1::onOk
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onOk_1;

public:
	inline static int32_t get_offset_of_onCancel_0() { return static_cast<int32_t>(offsetof(U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72, ___onCancel_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onCancel_0() const { return ___onCancel_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onCancel_0() { return &___onCancel_0; }
	inline void set_onCancel_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onCancel_0 = value;
		Il2CppCodeGenWriteBarrier((&___onCancel_0), value);
	}

	inline static int32_t get_offset_of_onOk_1() { return static_cast<int32_t>(offsetof(U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72, ___onOk_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onOk_1() const { return ___onOk_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onOk_1() { return &___onOk_1; }
	inline void set_onOk_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onOk_1 = value;
		Il2CppCodeGenWriteBarrier((&___onOk_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3COPENALERTDIALOGU3EC__ANONSTOREY1_T464A898B52C251237E9C1DA5250B421534114C72_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_TF2C68FFE572BEBC9BD9FFBA4B794164E3A87CBED_H
#define MONOPINVOKECALLBACKATTRIBUTE_TF2C68FFE572BEBC9BD9FFBA4B794164E3A87CBED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_tF2C68FFE572BEBC9BD9FFBA4B794164E3A87CBED  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_TF2C68FFE572BEBC9BD9FFBA4B794164E3A87CBED_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef CHAR_TBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_H
#define CHAR_TBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((&___categoryForLatin1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_TBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef METHODBASE_T_H
#define METHODBASE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T_H
#ifndef GCHANDLE_T39FAEE3EA592432C93B574A31DD83B87F1847DE3_H
#define GCHANDLE_T39FAEE3EA592432C93B574A31DD83B87F1847DE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T39FAEE3EA592432C93B574A31DD83B87F1847DE3_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#define RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RUNTIMEPLATFORM_TD5F5737C1BBBCBB115EB104DF2B7876387E80132_H
#define RUNTIMEPLATFORM_TD5F5737C1BBBCBB115EB104DF2B7876387E80132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_TD5F5737C1BBBCBB115EB104DF2B7876387E80132_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#define GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifndef ACTION_T591D2A86165F896B4B800BB5C25CE18672A55579_H
#define ACTION_T591D2A86165F896B4B800BB5C25CE18672A55579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action
struct  Action_t591D2A86165F896B4B800BB5C25CE18672A55579  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T591D2A86165F896B4B800BB5C25CE18672A55579_H
#ifndef ACTION_2_T7F48DB8D71AB14B1331B4BB8EE28580F28191ACE_H
#define ACTION_2_T7F48DB8D71AB14B1331B4BB8EE28580F28191ACE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`2<System.Int32,System.Int32>
struct  Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_2_T7F48DB8D71AB14B1331B4BB8EE28580F28191ACE_H
#ifndef ACTION_3_TD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB_H
#define ACTION_3_TD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`3<System.Int32,System.Int32,System.Int32>
struct  Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_3_TD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB_H
#ifndef ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#define ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#ifndef COMPLETIONHANDLER_T57827C46E0121AC0285B25FF50B80A23FFE2A7CE_H
#define COMPLETIONHANDLER_T57827C46E0121AC0285B25FF50B80A23FFE2A7CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.IosNativePopups/CompletionHandler
struct  CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPLETIONHANDLER_T57827C46E0121AC0285B25FF50B80A23FFE2A7CE_H
#ifndef INTERNALHANDLER_T99221D275AB4B7194ED4F74F9193CA944BD83FD5_H
#define INTERNALHANDLER_T99221D275AB4B7194ED4F74F9193CA944BD83FD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.IosNativePopups/InternalHandler
struct  InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALHANDLER_T99221D275AB4B7194ED4F74F9193CA944BD83FD5_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ALERTDIALOGPROXY_T55B5EC27E25A527CCB4EE248A488E8B702897241_H
#define ALERTDIALOGPROXY_T55B5EC27E25A527CCB4EE248A488E8B702897241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.AlertDialogProxy
struct  AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action TheNextFlow.UnityPlugins.AlertDialogProxy::onOk
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onOk_4;
	// System.Action TheNextFlow.UnityPlugins.AlertDialogProxy::onCancel
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onCancel_5;
	// System.Action TheNextFlow.UnityPlugins.AlertDialogProxy::onNeutral
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onNeutral_6;

public:
	inline static int32_t get_offset_of_onOk_4() { return static_cast<int32_t>(offsetof(AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241, ___onOk_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onOk_4() const { return ___onOk_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onOk_4() { return &___onOk_4; }
	inline void set_onOk_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onOk_4 = value;
		Il2CppCodeGenWriteBarrier((&___onOk_4), value);
	}

	inline static int32_t get_offset_of_onCancel_5() { return static_cast<int32_t>(offsetof(AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241, ___onCancel_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onCancel_5() const { return ___onCancel_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onCancel_5() { return &___onCancel_5; }
	inline void set_onCancel_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onCancel_5 = value;
		Il2CppCodeGenWriteBarrier((&___onCancel_5), value);
	}

	inline static int32_t get_offset_of_onNeutral_6() { return static_cast<int32_t>(offsetof(AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241, ___onNeutral_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onNeutral_6() const { return ___onNeutral_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onNeutral_6() { return &___onNeutral_6; }
	inline void set_onNeutral_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onNeutral_6 = value;
		Il2CppCodeGenWriteBarrier((&___onNeutral_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTDIALOGPROXY_T55B5EC27E25A527CCB4EE248A488E8B702897241_H
#ifndef DATEPICKERDIALOGPROXY_T3DC13C8B3EFB9AB6890BF88F75AD27564AEB0F0E_H
#define DATEPICKERDIALOGPROXY_T3DC13C8B3EFB9AB6890BF88F75AD27564AEB0F0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.DatePickerDialogProxy
struct  DatePickerDialogProxy_t3DC13C8B3EFB9AB6890BF88F75AD27564AEB0F0E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action`3<System.Int32,System.Int32,System.Int32> TheNextFlow.UnityPlugins.DatePickerDialogProxy::onDateSet
	Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB * ___onDateSet_4;

public:
	inline static int32_t get_offset_of_onDateSet_4() { return static_cast<int32_t>(offsetof(DatePickerDialogProxy_t3DC13C8B3EFB9AB6890BF88F75AD27564AEB0F0E, ___onDateSet_4)); }
	inline Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB * get_onDateSet_4() const { return ___onDateSet_4; }
	inline Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB ** get_address_of_onDateSet_4() { return &___onDateSet_4; }
	inline void set_onDateSet_4(Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB * value)
	{
		___onDateSet_4 = value;
		Il2CppCodeGenWriteBarrier((&___onDateSet_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEPICKERDIALOGPROXY_T3DC13C8B3EFB9AB6890BF88F75AD27564AEB0F0E_H
#ifndef TIMEPICKERDIALOGPROXY_TEA3E774F2ACE744CCC2DBBF4A78BD2049B76E266_H
#define TIMEPICKERDIALOGPROXY_TEA3E774F2ACE744CCC2DBBF4A78BD2049B76E266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.TimePickerDialogProxy
struct  TimePickerDialogProxy_tEA3E774F2ACE744CCC2DBBF4A78BD2049B76E266  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action`2<System.Int32,System.Int32> TheNextFlow.UnityPlugins.TimePickerDialogProxy::onTimeSet
	Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * ___onTimeSet_4;

public:
	inline static int32_t get_offset_of_onTimeSet_4() { return static_cast<int32_t>(offsetof(TimePickerDialogProxy_tEA3E774F2ACE744CCC2DBBF4A78BD2049B76E266, ___onTimeSet_4)); }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * get_onTimeSet_4() const { return ___onTimeSet_4; }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE ** get_address_of_onTimeSet_4() { return &___onTimeSet_4; }
	inline void set_onTimeSet_4(Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * value)
	{
		___onTimeSet_4 = value;
		Il2CppCodeGenWriteBarrier((&___onTimeSet_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEPICKERDIALOGPROXY_TEA3E774F2ACE744CCC2DBBF4A78BD2049B76E266_H
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) MethodInfo_t * m_Items[1];

public:
	inline MethodInfo_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MethodInfo_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline MethodInfo_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MethodInfo_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_mCB8164FB05F8DCF99E098ADC5E13E96FEF6FC4E9_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void System.Action`3<System.Int32,System.Int32,System.Int32>::Invoke(!0,!1,!2)
extern "C" IL2CPP_METHOD_ATTR void Action_3_Invoke_m0011BAFE193EBA2976AA455819357DC75716EB61_gshared (Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void System.Action`2<System.Int32,System.Int32>::Invoke(!0,!1)
extern "C" IL2CPP_METHOD_ATTR void Action_2_Invoke_mCEA7B556FB0AEA5B7E000DC9073DC99CD834DE9C_gshared (Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * __this, int32_t p0, int32_t p1, const RuntimeMethod* method);

// System.Void System.Attribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0 (Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void System.Action::Invoke()
extern "C" IL2CPP_METHOD_ATTR void Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.IDisposable TheNextFlow.UnityPlugins.AndroidNativePopups::CreateAndroidJavaObject(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* AndroidNativePopups_CreateAndroidJavaObject_mAC69C31D30F3179574DB4F9BD54B27F1C98E8BAC (String_t* ___className0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// System.Type System.Type::GetType(System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR Type_t * Type_GetType_m8A8A6481B24551476F2AF999A970AD705BA68C7A (String_t* p0, bool p1, const RuntimeMethod* method);
// System.Object System.Activator::CreateInstance(System.Type,System.Object[])
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Activator_CreateInstance_mEE50708E1E8AAD4E5021A2FFDB992DDF65727E17 (Type_t * p0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* p1, const RuntimeMethod* method);
// System.Type System.Object::GetType()
extern "C" IL2CPP_METHOD_ATTR Type_t * Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Reflection.MethodInfo[] System.Type::GetMethods()
extern "C" IL2CPP_METHOD_ATTR MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* Type_GetMethods_m50864CCA29AC38E53711C885031DB3793D4C8C60 (Type_t * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[])
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * MethodBase_Invoke_m471794D56262D9DB5B5A324883030AB16BD39674 (MethodBase_t * __this, RuntimeObject * p0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* p1, const RuntimeMethod* method);
// System.Guid System.Guid::NewGuid()
extern "C" IL2CPP_METHOD_ATTR Guid_t  Guid_NewGuid_m541CAC23EBB140DFD3AB5B313315647E95FADB29 (const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<TheNextFlow.UnityPlugins.AlertDialogProxy>()
inline AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241 * GameObject_AddComponent_TisAlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241_m764ACB117AE67296EC7A0CF67BC27E733DDD0123 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mCB8164FB05F8DCF99E098ADC5E13E96FEF6FC4E9_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<TheNextFlow.UnityPlugins.AlertDialogProxy>()
inline AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241 * GameObject_GetComponent_TisAlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241_mB1C0A1FE3881A26839A8C87366B9C70252C88E22 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared)(__this, method);
}
// System.Void TheNextFlow.UnityPlugins.AndroidNativePopups::CallStatic(System.Object,System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR void AndroidNativePopups_CallStatic_m7CBCA98EF2737BA5C8318EFDC76DC70657F36B02 (RuntimeObject * ___classInstance0, String_t* ___methodName1, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args2, const RuntimeMethod* method);
// System.String[] System.String::Split(System.Char[])
extern "C" IL2CPP_METHOD_ATTR StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* String_Split_m13262358217AD2C119FD1B9733C3C0289D608512 (String_t* __this, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* p0, const RuntimeMethod* method);
// System.Int32 System.Int32::Parse(System.String)
extern "C" IL2CPP_METHOD_ATTR int32_t Int32_Parse_m5807B6243415790250FC25168F767C08FC16FDEA (String_t* p0, const RuntimeMethod* method);
// System.Void System.Action`3<System.Int32,System.Int32,System.Int32>::Invoke(!0,!1,!2)
inline void Action_3_Invoke_m0011BAFE193EBA2976AA455819357DC75716EB61 (Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	((  void (*) (Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB *, int32_t, int32_t, int32_t, const RuntimeMethod*))Action_3_Invoke_m0011BAFE193EBA2976AA455819357DC75716EB61_gshared)(__this, p0, p1, p2, method);
}
// System.Void TheNextFlow.UnityPlugins.IosNativePopups::ClickedButtonAtIndexCallback(System.IntPtr,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IosNativePopups_ClickedButtonAtIndexCallback_m9394831AC9C6A86B31A6A0EE1AE9E1E1F1A1C183 (intptr_t ___completionPtr0, int32_t ___result1, const RuntimeMethod* method);
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::Alloc(System.Object)
extern "C" IL2CPP_METHOD_ATTR GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  GCHandle_Alloc_m5BF9DC23B533B904BFEA61136B92916683B46B0F (RuntimeObject * p0, const RuntimeMethod* method);
// System.IntPtr System.Runtime.InteropServices.GCHandle::op_Explicit(System.Runtime.InteropServices.GCHandle)
extern "C" IL2CPP_METHOD_ATTR intptr_t GCHandle_op_Explicit_mDDDE375E679609F240EF76F20E982C5B73A7D6BA (GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  p0, const RuntimeMethod* method);
// System.Void TheNextFlow.UnityPlugins.IosNativePopups/InternalHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void InternalHandler__ctor_m56221A94694F3C429A61B5890F130357A83DC5CE (InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void TheNextFlow.UnityPlugins.IosNativePopups::_showAlertView(System.String,System.String,System.String,System.Int32,System.String[],TheNextFlow.UnityPlugins.IosNativePopups/InternalHandler,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void IosNativePopups__showAlertView_m20C0C6D7CF72B2B70ADBE58AF5F064CAA58A0362 (String_t* ___title0, String_t* ___message1, String_t* ___cancelButtonTitle2, int32_t ___otherButtonCount3, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___otherButtonTitles4, InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5 * ___handler5, intptr_t ___completion6, const RuntimeMethod* method);
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR bool IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61 (intptr_t p0, intptr_t p1, const RuntimeMethod* method);
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::op_Explicit(System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  GCHandle_op_Explicit_m12AF9B7126A0899E8011F1CCDCB877AAA56A0C8C (intptr_t p0, const RuntimeMethod* method);
// System.Object System.Runtime.InteropServices.GCHandle::get_Target()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GCHandle_get_Target_mDBDEA6883245CF1EF963D9FA945569B2D59DCCF8 (GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 * __this, const RuntimeMethod* method);
// System.Void TheNextFlow.UnityPlugins.IosNativePopups/CompletionHandler::Invoke(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void CompletionHandler_Invoke_mE00BA622997117342C0345B8F13C91F89D1489CD (CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE * __this, int32_t ___result0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.GCHandle::Free()
extern "C" IL2CPP_METHOD_ATTR void GCHandle_Free_m392ECC9B1058E35A0FD5CF21A65F212873FC26F0 (GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 * __this, const RuntimeMethod* method);
// System.Void TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3COpenAlertDialogU3Ec__AnonStorey0__ctor_m827556540237A1E684D770026607F7C47AE277DD (U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18 * __this, const RuntimeMethod* method);
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C" IL2CPP_METHOD_ATTR int32_t Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672 (const RuntimeMethod* method);
// System.Void TheNextFlow.UnityPlugins.IosNativePopups/CompletionHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void CompletionHandler__ctor_m26FC7A14A5736A9A1EE5C9BA537B3D14D9570DA9 (CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void TheNextFlow.UnityPlugins.IosNativePopups::OpenAlertDialog(System.String,System.String,System.String,System.String[],TheNextFlow.UnityPlugins.IosNativePopups/CompletionHandler)
extern "C" IL2CPP_METHOD_ATTR void IosNativePopups_OpenAlertDialog_m07E4B6D10F794A30E1F8A0B41CADFBA49D85FE13 (String_t* ___title0, String_t* ___message1, String_t* ___cancelButtonTitle2, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___otherButtonTitles3, CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE * ___completion4, const RuntimeMethod* method);
// System.Void TheNextFlow.UnityPlugins.AndroidNativePopups::OpenAlertDialog(System.String,System.String,System.String,System.Action)
extern "C" IL2CPP_METHOD_ATTR void AndroidNativePopups_OpenAlertDialog_mE4B2462D28BB5386AEB46A77A967F0FDEC55AE21 (String_t* ___title0, String_t* ___message1, String_t* ___cancel2, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onCancel3, const RuntimeMethod* method);
// System.Void TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey1::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3COpenAlertDialogU3Ec__AnonStorey1__ctor_m9ACB644571CD27824BDD93D01306CBAEFF8C2A0D (U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72 * __this, const RuntimeMethod* method);
// System.Void TheNextFlow.UnityPlugins.AndroidNativePopups::OpenAlertDialog(System.String,System.String,System.String,System.String,System.Action,System.Action)
extern "C" IL2CPP_METHOD_ATTR void AndroidNativePopups_OpenAlertDialog_m7F3F2553A4B0653775F714E60DA0645B48D5999B (String_t* ___title0, String_t* ___message1, String_t* ___ok2, String_t* ___cancel3, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onOk4, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onCancel5, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.Action`2<System.Int32,System.Int32>::Invoke(!0,!1)
inline void Action_2_Invoke_mCEA7B556FB0AEA5B7E000DC9073DC99CD834DE9C (Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * __this, int32_t p0, int32_t p1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE *, int32_t, int32_t, const RuntimeMethod*))Action_2_Invoke_mCEA7B556FB0AEA5B7E000DC9073DC99CD834DE9C_gshared)(__this, p0, p1, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C" IL2CPP_METHOD_ATTR void MonoPInvokeCallbackAttribute__ctor_mE7FE16E4C73A183DD18E5E4AB8B1211CEC66516D (MonoPInvokeCallbackAttribute_tF2C68FFE572BEBC9BD9FFBA4B794164E3A87CBED * __this, Type_t * ___type0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TheNextFlow.UnityPlugins.AlertDialogProxy::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AlertDialogProxy__ctor_m06C1C246C8CECCDDE811DA5681EB73DD431A88F2 (AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TheNextFlow.UnityPlugins.AlertDialogProxy::OnOk()
extern "C" IL2CPP_METHOD_ATTR void AlertDialogProxy_OnOk_m719961E3F235E538463D00CED8FDDB44DD290FEF (AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AlertDialogProxy_OnOk_m719961E3F235E538463D00CED8FDDB44DD290FEF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_0 = __this->get_onOk_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_1 = __this->get_onOk_4();
		NullCheck(L_1);
		Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TheNextFlow.UnityPlugins.AlertDialogProxy::OnCancel()
extern "C" IL2CPP_METHOD_ATTR void AlertDialogProxy_OnCancel_m2D06305EEF95D9246598C65F9805346C2A055A80 (AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AlertDialogProxy_OnCancel_m2D06305EEF95D9246598C65F9805346C2A055A80_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_0 = __this->get_onCancel_5();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_1 = __this->get_onCancel_5();
		NullCheck(L_1);
		Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TheNextFlow.UnityPlugins.AlertDialogProxy::OnNeutral()
extern "C" IL2CPP_METHOD_ATTR void AlertDialogProxy_OnNeutral_m9A2184158FE7CC2273B2E052DB51F6E8D0DD749F (AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AlertDialogProxy_OnNeutral_m9A2184158FE7CC2273B2E052DB51F6E8D0DD749F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_0 = __this->get_onNeutral_6();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_1 = __this->get_onNeutral_6();
		NullCheck(L_1);
		Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TheNextFlow.UnityPlugins.AndroidNativePopups::.cctor()
extern "C" IL2CPP_METHOD_ATTR void AndroidNativePopups__cctor_m32FC8C1BEFB6548A9AF6642E091C78DEC1887941 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidNativePopups__cctor_m32FC8C1BEFB6548A9AF6642E091C78DEC1887941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)0);
		RuntimeObject* L_1 = AndroidNativePopups_CreateAndroidJavaObject_mAC69C31D30F3179574DB4F9BD54B27F1C98E8BAC(_stringLiteral73FCBF0DF8FE0B17A4F7C855909540A92F4C79E1, L_0, /*hidden argument*/NULL);
		((AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_StaticFields*)il2cpp_codegen_static_fields_for(AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_il2cpp_TypeInfo_var))->set_androidUtils_0(L_1);
		return;
	}
}
// System.IDisposable TheNextFlow.UnityPlugins.AndroidNativePopups::CreateAndroidJavaObject(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* AndroidNativePopups_CreateAndroidJavaObject_mAC69C31D30F3179574DB4F9BD54B27F1C98E8BAC (String_t* ___className0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidNativePopups_CreateAndroidJavaObject_mAC69C31D30F3179574DB4F9BD54B27F1C98E8BAC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m8A8A6481B24551476F2AF999A970AD705BA68C7A, _stringLiteral561DAA3E31894E0B8E0314512463120E85FBCDF9, (bool)1, "MobileNativePopups, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		V_0 = L_0;
		Type_t * L_1 = V_0;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = L_2;
		String_t* L_4 = ___className0;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_3;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_6 = ___args1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_6);
		RuntimeObject * L_7 = Activator_CreateInstance_mEE50708E1E8AAD4E5021A2FFDB992DDF65727E17(L_1, L_5, /*hidden argument*/NULL);
		return ((RuntimeObject*)Castclass((RuntimeObject*)L_7, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var));
	}
}
// System.Void TheNextFlow.UnityPlugins.AndroidNativePopups::CallStatic(System.Object,System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR void AndroidNativePopups_CallStatic_m7CBCA98EF2737BA5C8318EFDC76DC70657F36B02 (RuntimeObject * ___classInstance0, String_t* ___methodName1, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidNativePopups_CallStatic_m7CBCA98EF2737BA5C8318EFDC76DC70657F36B02_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* V_2 = NULL;
	int32_t V_3 = 0;
	MethodInfo_t * V_4 = NULL;
	{
		RuntimeObject * L_0 = ___classInstance0;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = (MethodInfo_t *)NULL;
		Type_t * L_2 = V_0;
		NullCheck(L_2);
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_3 = Type_GetMethods_m50864CCA29AC38E53711C885031DB3793D4C8C60(L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		V_3 = 0;
		goto IL_004a;
	}

IL_0017:
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		MethodInfo_t * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_4 = L_7;
		MethodInfo_t * L_8 = V_4;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_8);
		bool L_10 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_9, _stringLiteral42421480C15A2650AE377F9F228CB3729E8504FD, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0046;
		}
	}
	{
		MethodInfo_t * L_11 = V_4;
		NullCheck(L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(27 /* System.Boolean System.Reflection.MethodBase::get_IsGenericMethod() */, L_11);
		if (L_12)
		{
			goto IL_0046;
		}
	}
	{
		MethodInfo_t * L_13 = V_4;
		V_1 = L_13;
		goto IL_0053;
	}

IL_0046:
	{
		int32_t L_14 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_004a:
	{
		int32_t L_15 = V_3;
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_16 = V_2;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_16)->max_length)))))))
		{
			goto IL_0017;
		}
	}

IL_0053:
	{
		MethodInfo_t * L_17 = V_1;
		if (!L_17)
		{
			goto IL_006f;
		}
	}
	{
		MethodInfo_t * L_18 = V_1;
		RuntimeObject * L_19 = ___classInstance0;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_20 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_21 = L_20;
		String_t* L_22 = ___methodName1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_22);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_23 = L_21;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_24 = ___args2;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_24);
		NullCheck(L_18);
		MethodBase_Invoke_m471794D56262D9DB5B5A324883030AB16BD39674(L_18, L_19, L_23, /*hidden argument*/NULL);
	}

IL_006f:
	{
		return;
	}
}
// System.Void TheNextFlow.UnityPlugins.AndroidNativePopups::OpenAlertDialog(System.String,System.String,System.String,System.Action)
extern "C" IL2CPP_METHOD_ATTR void AndroidNativePopups_OpenAlertDialog_mE4B2462D28BB5386AEB46A77A967F0FDEC55AE21 (String_t* ___title0, String_t* ___message1, String_t* ___cancel2, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onCancel3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidNativePopups_OpenAlertDialog_mE4B2462D28BB5386AEB46A77A967F0FDEC55AE21_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_0 = Guid_NewGuid_m541CAC23EBB140DFD3AB5B313315647E95FADB29(/*hidden argument*/NULL);
		Guid_t  L_1 = L_0;
		RuntimeObject * L_2 = Box(Guid_t_il2cpp_TypeInfo_var, &L_1);
		String_t* L_3 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteralB0494FC133E51BA549EC26B16950914A2077FCE2, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = V_1;
		NullCheck(L_6);
		GameObject_AddComponent_TisAlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241_m764ACB117AE67296EC7A0CF67BC27E733DDD0123(L_6, /*hidden argument*/GameObject_AddComponent_TisAlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241_m764ACB117AE67296EC7A0CF67BC27E733DDD0123_RuntimeMethod_var);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = V_1;
		NullCheck(L_7);
		AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241 * L_8 = GameObject_GetComponent_TisAlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241_mB1C0A1FE3881A26839A8C87366B9C70252C88E22(L_7, /*hidden argument*/GameObject_GetComponent_TisAlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241_mB1C0A1FE3881A26839A8C87366B9C70252C88E22_RuntimeMethod_var);
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_9 = ___onCancel3;
		NullCheck(L_8);
		L_8->set_onCancel_5(L_9);
		IL2CPP_RUNTIME_CLASS_INIT(AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_il2cpp_TypeInfo_var);
		RuntimeObject* L_10 = ((AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_StaticFields*)il2cpp_codegen_static_fields_for(AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_il2cpp_TypeInfo_var))->get_androidUtils_0();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_12 = L_11;
		String_t* L_13 = ___title0;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_13);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_14 = L_12;
		String_t* L_15 = ___message1;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_15);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_16 = L_14;
		String_t* L_17 = ___cancel2;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_17);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_18 = L_16;
		String_t* L_19 = V_0;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_19);
		AndroidNativePopups_CallStatic_m7CBCA98EF2737BA5C8318EFDC76DC70657F36B02(L_10, _stringLiteralE62471C7B901EEA0B4178C8B96E00C73B67E44C6, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TheNextFlow.UnityPlugins.AndroidNativePopups::OpenAlertDialog(System.String,System.String,System.String,System.String,System.Action,System.Action)
extern "C" IL2CPP_METHOD_ATTR void AndroidNativePopups_OpenAlertDialog_m7F3F2553A4B0653775F714E60DA0645B48D5999B (String_t* ___title0, String_t* ___message1, String_t* ___ok2, String_t* ___cancel3, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onOk4, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onCancel5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidNativePopups_OpenAlertDialog_m7F3F2553A4B0653775F714E60DA0645B48D5999B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_0 = Guid_NewGuid_m541CAC23EBB140DFD3AB5B313315647E95FADB29(/*hidden argument*/NULL);
		Guid_t  L_1 = L_0;
		RuntimeObject * L_2 = Box(Guid_t_il2cpp_TypeInfo_var, &L_1);
		String_t* L_3 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteralB0494FC133E51BA549EC26B16950914A2077FCE2, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = V_1;
		NullCheck(L_6);
		GameObject_AddComponent_TisAlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241_m764ACB117AE67296EC7A0CF67BC27E733DDD0123(L_6, /*hidden argument*/GameObject_AddComponent_TisAlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241_m764ACB117AE67296EC7A0CF67BC27E733DDD0123_RuntimeMethod_var);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = V_1;
		NullCheck(L_7);
		AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241 * L_8 = GameObject_GetComponent_TisAlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241_mB1C0A1FE3881A26839A8C87366B9C70252C88E22(L_7, /*hidden argument*/GameObject_GetComponent_TisAlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241_mB1C0A1FE3881A26839A8C87366B9C70252C88E22_RuntimeMethod_var);
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_9 = ___onOk4;
		NullCheck(L_8);
		L_8->set_onOk_4(L_9);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = V_1;
		NullCheck(L_10);
		AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241 * L_11 = GameObject_GetComponent_TisAlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241_mB1C0A1FE3881A26839A8C87366B9C70252C88E22(L_10, /*hidden argument*/GameObject_GetComponent_TisAlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241_mB1C0A1FE3881A26839A8C87366B9C70252C88E22_RuntimeMethod_var);
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_12 = ___onCancel5;
		NullCheck(L_11);
		L_11->set_onCancel_5(L_12);
		IL2CPP_RUNTIME_CLASS_INIT(AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_il2cpp_TypeInfo_var);
		RuntimeObject* L_13 = ((AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_StaticFields*)il2cpp_codegen_static_fields_for(AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_il2cpp_TypeInfo_var))->get_androidUtils_0();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_14 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_15 = L_14;
		String_t* L_16 = ___title0;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_16);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_17 = L_15;
		String_t* L_18 = ___message1;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_18);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_19 = L_17;
		String_t* L_20 = ___ok2;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_20);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_21 = L_19;
		String_t* L_22 = ___cancel3;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_22);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_23 = L_21;
		String_t* L_24 = V_0;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_24);
		AndroidNativePopups_CallStatic_m7CBCA98EF2737BA5C8318EFDC76DC70657F36B02(L_13, _stringLiteralE62471C7B901EEA0B4178C8B96E00C73B67E44C6, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TheNextFlow.UnityPlugins.AndroidNativePopups::Dispose()
extern "C" IL2CPP_METHOD_ATTR void AndroidNativePopups_Dispose_m52FDFEEB79D1ABF52350C710729C3529CE938823 (AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidNativePopups_Dispose_m52FDFEEB79D1ABF52350C710729C3529CE938823_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_StaticFields*)il2cpp_codegen_static_fields_for(AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_il2cpp_TypeInfo_var))->get_androidUtils_0();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TheNextFlow.UnityPlugins.DatePickerDialogProxy::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DatePickerDialogProxy__ctor_mF7B23AF72555E1A98A03484E3E1A416E2D657197 (DatePickerDialogProxy_t3DC13C8B3EFB9AB6890BF88F75AD27564AEB0F0E * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TheNextFlow.UnityPlugins.DatePickerDialogProxy::OnDateSet(System.String)
extern "C" IL2CPP_METHOD_ATTR void DatePickerDialogProxy_OnDateSet_mA2E9A372EB60CEDDDC9F9C45724D511F0182899A (DatePickerDialogProxy_t3DC13C8B3EFB9AB6890BF88F75AD27564AEB0F0E * __this, String_t* ___date0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DatePickerDialogProxy_OnDateSet_mA2E9A372EB60CEDDDC9F9C45724D511F0182899A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* V_0 = NULL;
	{
		String_t* L_0 = ___date0;
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_1 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_2 = L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)47));
		NullCheck(L_0);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = String_Split_m13262358217AD2C119FD1B9733C3C0289D608512(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB * L_4 = __this->get_onDateSet_4();
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB * L_5 = __this->get_onDateSet_4();
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = 0;
		String_t* L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		int32_t L_9 = Int32_Parse_m5807B6243415790250FC25168F767C08FC16FDEA(L_8, /*hidden argument*/NULL);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = 1;
		String_t* L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		int32_t L_13 = Int32_Parse_m5807B6243415790250FC25168F767C08FC16FDEA(L_12, /*hidden argument*/NULL);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_14 = V_0;
		NullCheck(L_14);
		int32_t L_15 = 2;
		String_t* L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		int32_t L_17 = Int32_Parse_m5807B6243415790250FC25168F767C08FC16FDEA(L_16, /*hidden argument*/NULL);
		NullCheck(L_5);
		Action_3_Invoke_m0011BAFE193EBA2976AA455819357DC75716EB61(L_5, L_9, L_13, L_17, /*hidden argument*/Action_3_Invoke_m0011BAFE193EBA2976AA455819357DC75716EB61_RuntimeMethod_var);
	}

IL_0040:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_18 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_18, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosNativePopups_ClickedButtonAtIndexCallback_m9394831AC9C6A86B31A6A0EE1AE9E1E1F1A1C183(intptr_t ___completionPtr0, int32_t ___result1)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Managed method invocation
	IosNativePopups_ClickedButtonAtIndexCallback_m9394831AC9C6A86B31A6A0EE1AE9E1E1F1A1C183(___completionPtr0, ___result1, NULL);

}
extern "C" void DEFAULT_CALL _showAlertView(char*, char*, char*, int32_t, char**, Il2CppMethodPointer, intptr_t);
// System.Void TheNextFlow.UnityPlugins.IosNativePopups::_showAlertView(System.String,System.String,System.String,System.Int32,System.String[],TheNextFlow.UnityPlugins.IosNativePopups/InternalHandler,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void IosNativePopups__showAlertView_m20C0C6D7CF72B2B70ADBE58AF5F064CAA58A0362 (String_t* ___title0, String_t* ___message1, String_t* ___cancelButtonTitle2, int32_t ___otherButtonCount3, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___otherButtonTitles4, InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5 * ___handler5, intptr_t ___completion6, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*, int32_t, char**, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___title0' to native representation
	char* ____title0_marshaled = NULL;
	____title0_marshaled = il2cpp_codegen_marshal_string(___title0);

	// Marshaling of parameter '___message1' to native representation
	char* ____message1_marshaled = NULL;
	____message1_marshaled = il2cpp_codegen_marshal_string(___message1);

	// Marshaling of parameter '___cancelButtonTitle2' to native representation
	char* ____cancelButtonTitle2_marshaled = NULL;
	____cancelButtonTitle2_marshaled = il2cpp_codegen_marshal_string(___cancelButtonTitle2);

	// Marshaling of parameter '___otherButtonTitles4' to native representation
	char** ____otherButtonTitles4_marshaled = NULL;
	if (___otherButtonTitles4 != NULL)
	{
		il2cpp_array_size_t ____otherButtonTitles4_Length = (___otherButtonTitles4)->max_length;
		____otherButtonTitles4_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____otherButtonTitles4_Length + 1);
		(____otherButtonTitles4_marshaled)[____otherButtonTitles4_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____otherButtonTitles4_Length); i++)
		{
			(____otherButtonTitles4_marshaled)[i] = il2cpp_codegen_marshal_string((___otherButtonTitles4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____otherButtonTitles4_marshaled = NULL;
	}

	// Marshaling of parameter '___handler5' to native representation
	Il2CppMethodPointer ____handler5_marshaled = NULL;
	____handler5_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___handler5));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_showAlertView)(____title0_marshaled, ____message1_marshaled, ____cancelButtonTitle2_marshaled, ___otherButtonCount3, ____otherButtonTitles4_marshaled, ____handler5_marshaled, ___completion6);

	// Marshaling cleanup of parameter '___title0' native representation
	il2cpp_codegen_marshal_free(____title0_marshaled);
	____title0_marshaled = NULL;

	// Marshaling cleanup of parameter '___message1' native representation
	il2cpp_codegen_marshal_free(____message1_marshaled);
	____message1_marshaled = NULL;

	// Marshaling cleanup of parameter '___cancelButtonTitle2' native representation
	il2cpp_codegen_marshal_free(____cancelButtonTitle2_marshaled);
	____cancelButtonTitle2_marshaled = NULL;

	// Marshaling cleanup of parameter '___otherButtonTitles4' native representation
	if (____otherButtonTitles4_marshaled != NULL)
	{
		const il2cpp_array_size_t ____otherButtonTitles4_marshaled_CleanupLoopCount = (___otherButtonTitles4 != NULL) ? (___otherButtonTitles4)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____otherButtonTitles4_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____otherButtonTitles4_marshaled)[i]);
			(____otherButtonTitles4_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____otherButtonTitles4_marshaled);
		____otherButtonTitles4_marshaled = NULL;
	}

}
// System.Void TheNextFlow.UnityPlugins.IosNativePopups::OpenAlertDialog(System.String,System.String,System.String,System.String[],TheNextFlow.UnityPlugins.IosNativePopups/CompletionHandler)
extern "C" IL2CPP_METHOD_ATTR void IosNativePopups_OpenAlertDialog_m07E4B6D10F794A30E1F8A0B41CADFBA49D85FE13 (String_t* ___title0, String_t* ___message1, String_t* ___cancelButtonTitle2, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___otherButtonTitles3, CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE * ___completion4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IosNativePopups_OpenAlertDialog_m07E4B6D10F794A30E1F8A0B41CADFBA49D85FE13_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	intptr_t G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	String_t* G_B5_0 = NULL;
	String_t* G_B5_1 = NULL;
	String_t* G_B5_2 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B4_1 = NULL;
	String_t* G_B4_2 = NULL;
	int32_t G_B6_0 = 0;
	String_t* G_B6_1 = NULL;
	String_t* G_B6_2 = NULL;
	String_t* G_B6_3 = NULL;
	{
		CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE * L_0 = ___completion4;
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE * L_1 = ___completion4;
		GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  L_2 = GCHandle_Alloc_m5BF9DC23B533B904BFEA61136B92916683B46B0F(L_1, /*hidden argument*/NULL);
		intptr_t L_3 = GCHandle_op_Explicit_mDDDE375E679609F240EF76F20E982C5B73A7D6BA(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_001d;
	}

IL_0018:
	{
		G_B3_0 = (0);
	}

IL_001d:
	{
		V_0 = (intptr_t)G_B3_0;
		String_t* L_4 = ___title0;
		String_t* L_5 = ___message1;
		String_t* L_6 = ___cancelButtonTitle2;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_7 = ___otherButtonTitles3;
		G_B4_0 = L_6;
		G_B4_1 = L_5;
		G_B4_2 = L_4;
		if (!L_7)
		{
			G_B5_0 = L_6;
			G_B5_1 = L_5;
			G_B5_2 = L_4;
			goto IL_002f;
		}
	}
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_8 = ___otherButtonTitles3;
		NullCheck(L_8);
		G_B6_0 = (((int32_t)((int32_t)(((RuntimeArray *)L_8)->max_length))));
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0030;
	}

IL_002f:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0030:
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_9 = ___otherButtonTitles3;
		InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5 * L_10 = (InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5 *)il2cpp_codegen_object_new(InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5_il2cpp_TypeInfo_var);
		InternalHandler__ctor_m56221A94694F3C429A61B5890F130357A83DC5CE(L_10, NULL, (intptr_t)((intptr_t)IosNativePopups_ClickedButtonAtIndexCallback_m9394831AC9C6A86B31A6A0EE1AE9E1E1F1A1C183_RuntimeMethod_var), /*hidden argument*/NULL);
		intptr_t L_11 = V_0;
		IosNativePopups__showAlertView_m20C0C6D7CF72B2B70ADBE58AF5F064CAA58A0362(G_B6_3, G_B6_2, G_B6_1, G_B6_0, L_9, L_10, (intptr_t)L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TheNextFlow.UnityPlugins.IosNativePopups::ClickedButtonAtIndexCallback(System.IntPtr,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IosNativePopups_ClickedButtonAtIndexCallback_m9394831AC9C6A86B31A6A0EE1AE9E1E1F1A1C183 (intptr_t ___completionPtr0, int32_t ___result1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IosNativePopups_ClickedButtonAtIndexCallback_m9394831AC9C6A86B31A6A0EE1AE9E1E1F1A1C183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  V_0;
	memset(&V_0, 0, sizeof(V_0));
	CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE * V_1 = NULL;
	{
		intptr_t L_0 = ___completionPtr0;
		bool L_1 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		intptr_t L_2 = ___completionPtr0;
		GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  L_3 = GCHandle_op_Explicit_m12AF9B7126A0899E8011F1CCDCB877AAA56A0C8C((intptr_t)L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		RuntimeObject * L_4 = GCHandle_get_Target_mDBDEA6883245CF1EF963D9FA945569B2D59DCCF8((GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 *)(&V_0), /*hidden argument*/NULL);
		V_1 = ((CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE *)IsInstSealed((RuntimeObject*)L_4, CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE_il2cpp_TypeInfo_var));
		CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE * L_5 = V_1;
		int32_t L_6 = ___result1;
		NullCheck(L_5);
		CompletionHandler_Invoke_mE00BA622997117342C0345B8F13C91F89D1489CD(L_5, L_6, /*hidden argument*/NULL);
		GCHandle_Free_m392ECC9B1058E35A0FD5CF21A65F212873FC26F0((GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 *)(&V_0), /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE (CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE * __this, int32_t ___result0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___result0);

}
// System.Void TheNextFlow.UnityPlugins.IosNativePopups/CompletionHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void CompletionHandler__ctor_m26FC7A14A5736A9A1EE5C9BA537B3D14D9570DA9 (CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TheNextFlow.UnityPlugins.IosNativePopups/CompletionHandler::Invoke(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void CompletionHandler_Invoke_mE00BA622997117342C0345B8F13C91F89D1489CD (CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE * __this, int32_t ___result0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 1)
				{
					// open
					typedef void (*FunctionPointerType) (int32_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___result0, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, int32_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___result0, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (int32_t, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___result0, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< int32_t >::Invoke(targetMethod, targetThis, ___result0);
							else
								GenericVirtActionInvoker1< int32_t >::Invoke(targetMethod, targetThis, ___result0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___result0);
							else
								VirtActionInvoker1< int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___result0);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, int32_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___result0, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___result0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___result0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (int32_t, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___result0, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< int32_t >::Invoke(targetMethod, targetThis, ___result0);
						else
							GenericVirtActionInvoker1< int32_t >::Invoke(targetMethod, targetThis, ___result0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___result0);
						else
							VirtActionInvoker1< int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___result0);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___result0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult TheNextFlow.UnityPlugins.IosNativePopups/CompletionHandler::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* CompletionHandler_BeginInvoke_mB81B368F056C2144C97078ABCEF111F8E44C82B4 (CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE * __this, int32_t ___result0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CompletionHandler_BeginInvoke_mB81B368F056C2144C97078ABCEF111F8E44C82B4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &___result0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void TheNextFlow.UnityPlugins.IosNativePopups/CompletionHandler::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void CompletionHandler_EndInvoke_mC5FFA1D928D4B3A65F1EE80AC8E5435310E56E90 (CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5 (InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5 * __this, intptr_t ___delegatePtr0, int32_t ___result1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(intptr_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___delegatePtr0, ___result1);

}
// System.Void TheNextFlow.UnityPlugins.IosNativePopups/InternalHandler::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void InternalHandler__ctor_m56221A94694F3C429A61B5890F130357A83DC5CE (InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TheNextFlow.UnityPlugins.IosNativePopups/InternalHandler::Invoke(System.IntPtr,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void InternalHandler_Invoke_mAC1D88EC98DEA806225DCF8CC83481C2F91BA0DE (InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5 * __this, intptr_t ___delegatePtr0, int32_t ___result1, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 2)
				{
					// open
					typedef void (*FunctionPointerType) (intptr_t, int32_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___delegatePtr0, ___result1, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, intptr_t, int32_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___delegatePtr0, ___result1, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (intptr_t, int32_t, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___delegatePtr0, ___result1, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker2< intptr_t, int32_t >::Invoke(targetMethod, targetThis, ___delegatePtr0, ___result1);
							else
								GenericVirtActionInvoker2< intptr_t, int32_t >::Invoke(targetMethod, targetThis, ___delegatePtr0, ___result1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker2< intptr_t, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___delegatePtr0, ___result1);
							else
								VirtActionInvoker2< intptr_t, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___delegatePtr0, ___result1);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, intptr_t, int32_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___delegatePtr0, ___result1, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (intptr_t, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___delegatePtr0, ___result1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, intptr_t, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___delegatePtr0, ___result1, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (intptr_t, int32_t, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___delegatePtr0, ___result1, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker2< intptr_t, int32_t >::Invoke(targetMethod, targetThis, ___delegatePtr0, ___result1);
						else
							GenericVirtActionInvoker2< intptr_t, int32_t >::Invoke(targetMethod, targetThis, ___delegatePtr0, ___result1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker2< intptr_t, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___delegatePtr0, ___result1);
						else
							VirtActionInvoker2< intptr_t, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___delegatePtr0, ___result1);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, intptr_t, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___delegatePtr0, ___result1, targetMethod);
			}
		}
	}
}
// System.IAsyncResult TheNextFlow.UnityPlugins.IosNativePopups/InternalHandler::BeginInvoke(System.IntPtr,System.Int32,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* InternalHandler_BeginInvoke_mE9C6ECA9F22A82EAB96A9A5428A8ECEFF0D2074F (InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5 * __this, intptr_t ___delegatePtr0, int32_t ___result1, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalHandler_BeginInvoke_mE9C6ECA9F22A82EAB96A9A5428A8ECEFF0D2074F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___delegatePtr0);
	__d_args[1] = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &___result1);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void TheNextFlow.UnityPlugins.IosNativePopups/InternalHandler::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void InternalHandler_EndInvoke_m5C5EC8163B31009EC8507072DE3C00321DD99401 (InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TheNextFlow.UnityPlugins.MobileNativePopups::OpenAlertDialog(System.String,System.String,System.String,System.Action)
extern "C" IL2CPP_METHOD_ATTR void MobileNativePopups_OpenAlertDialog_m7F5DBA7DF33B9CFA84BC49D6E9C171D3485EE8D9 (String_t* ___title0, String_t* ___message1, String_t* ___cancel2, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onCancel3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MobileNativePopups_OpenAlertDialog_m7F5DBA7DF33B9CFA84BC49D6E9C171D3485EE8D9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18 * V_0 = NULL;
	{
		U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18 * L_0 = (U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18 *)il2cpp_codegen_object_new(U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18_il2cpp_TypeInfo_var);
		U3COpenAlertDialogU3Ec__AnonStorey0__ctor_m827556540237A1E684D770026607F7C47AE277DD(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18 * L_1 = V_0;
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_2 = ___onCancel3;
		NullCheck(L_1);
		L_1->set_onCancel_0(L_2);
		int32_t L_3 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_4 = ___title0;
		String_t* L_5 = ___message1;
		String_t* L_6 = ___cancel2;
		U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18 * L_7 = V_0;
		CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE * L_8 = (CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE *)il2cpp_codegen_object_new(CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE_il2cpp_TypeInfo_var);
		CompletionHandler__ctor_m26FC7A14A5736A9A1EE5C9BA537B3D14D9570DA9(L_8, L_7, (intptr_t)((intptr_t)U3COpenAlertDialogU3Ec__AnonStorey0_U3CU3Em__0_m53B608EAE978F2297C9D5ECF5B950008986DFB53_RuntimeMethod_var), /*hidden argument*/NULL);
		IosNativePopups_OpenAlertDialog_m07E4B6D10F794A30E1F8A0B41CADFBA49D85FE13(L_4, L_5, L_6, (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)NULL, L_8, /*hidden argument*/NULL);
		goto IL_004c;
	}

IL_0032:
	{
		int32_t L_9 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_004c;
		}
	}
	{
		String_t* L_10 = ___title0;
		String_t* L_11 = ___message1;
		String_t* L_12 = ___cancel2;
		U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18 * L_13 = V_0;
		NullCheck(L_13);
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_14 = L_13->get_onCancel_0();
		IL2CPP_RUNTIME_CLASS_INIT(AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_il2cpp_TypeInfo_var);
		AndroidNativePopups_OpenAlertDialog_mE4B2462D28BB5386AEB46A77A967F0FDEC55AE21(L_10, L_11, L_12, L_14, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
// System.Void TheNextFlow.UnityPlugins.MobileNativePopups::OpenAlertDialog(System.String,System.String,System.String,System.String,System.Action,System.Action)
extern "C" IL2CPP_METHOD_ATTR void MobileNativePopups_OpenAlertDialog_m9DA148B35CB9CEB0AAC1017CF6C1006FDF7811D6 (String_t* ___title0, String_t* ___message1, String_t* ___ok2, String_t* ___cancel3, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onOk4, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onCancel5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MobileNativePopups_OpenAlertDialog_m9DA148B35CB9CEB0AAC1017CF6C1006FDF7811D6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72 * V_0 = NULL;
	{
		U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72 * L_0 = (U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72 *)il2cpp_codegen_object_new(U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72_il2cpp_TypeInfo_var);
		U3COpenAlertDialogU3Ec__AnonStorey1__ctor_m9ACB644571CD27824BDD93D01306CBAEFF8C2A0D(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72 * L_1 = V_0;
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_2 = ___onCancel5;
		NullCheck(L_1);
		L_1->set_onCancel_0(L_2);
		U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72 * L_3 = V_0;
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_4 = ___onOk4;
		NullCheck(L_3);
		L_3->set_onOk_1(L_4);
		int32_t L_5 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)8))))
		{
			goto IL_0044;
		}
	}
	{
		String_t* L_6 = ___title0;
		String_t* L_7 = ___message1;
		String_t* L_8 = ___cancel3;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_9 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)1);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_10 = L_9;
		String_t* L_11 = ___ok2;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_11);
		U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72 * L_12 = V_0;
		CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE * L_13 = (CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE *)il2cpp_codegen_object_new(CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE_il2cpp_TypeInfo_var);
		CompletionHandler__ctor_m26FC7A14A5736A9A1EE5C9BA537B3D14D9570DA9(L_13, L_12, (intptr_t)((intptr_t)U3COpenAlertDialogU3Ec__AnonStorey1_U3CU3Em__1_mE376A1D7C2A8F3458666785EC28BE96D2CBA2D4B_RuntimeMethod_var), /*hidden argument*/NULL);
		IosNativePopups_OpenAlertDialog_m07E4B6D10F794A30E1F8A0B41CADFBA49D85FE13(L_6, L_7, L_8, L_10, L_13, /*hidden argument*/NULL);
		goto IL_0065;
	}

IL_0044:
	{
		int32_t L_14 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_15 = ___title0;
		String_t* L_16 = ___message1;
		String_t* L_17 = ___ok2;
		String_t* L_18 = ___cancel3;
		U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72 * L_19 = V_0;
		NullCheck(L_19);
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_20 = L_19->get_onOk_1();
		U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72 * L_21 = V_0;
		NullCheck(L_21);
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_22 = L_21->get_onCancel_0();
		IL2CPP_RUNTIME_CLASS_INIT(AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_il2cpp_TypeInfo_var);
		AndroidNativePopups_OpenAlertDialog_m7F3F2553A4B0653775F714E60DA0645B48D5999B(L_15, L_16, L_17, L_18, L_20, L_22, /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3COpenAlertDialogU3Ec__AnonStorey0__ctor_m827556540237A1E684D770026607F7C47AE277DD (U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey0::<>m__0(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3COpenAlertDialogU3Ec__AnonStorey0_U3CU3Em__0_m53B608EAE978F2297C9D5ECF5B950008986DFB53 (U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18 * __this, int32_t ___buttonIndex0, const RuntimeMethod* method)
{
	{
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_0 = __this->get_onCancel_0();
		NullCheck(L_0);
		Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD(L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey1::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3COpenAlertDialogU3Ec__AnonStorey1__ctor_m9ACB644571CD27824BDD93D01306CBAEFF8C2A0D (U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey1::<>m__1(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3COpenAlertDialogU3Ec__AnonStorey1_U3CU3Em__1_mE376A1D7C2A8F3458666785EC28BE96D2CBA2D4B (U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72 * __this, int32_t ___buttonIndex0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___buttonIndex0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_1 = __this->get_onCancel_0();
		NullCheck(L_1);
		Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD(L_1, /*hidden argument*/NULL);
		goto IL_0021;
	}

IL_0016:
	{
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_2 = __this->get_onOk_1();
		NullCheck(L_2);
		Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD(L_2, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TheNextFlow.UnityPlugins.TimePickerDialogProxy::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TimePickerDialogProxy__ctor_m9D1BB1AF2B72DC28FE990C040E035C012F1FE351 (TimePickerDialogProxy_tEA3E774F2ACE744CCC2DBBF4A78BD2049B76E266 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TheNextFlow.UnityPlugins.TimePickerDialogProxy::OnTimeSet(System.String)
extern "C" IL2CPP_METHOD_ATTR void TimePickerDialogProxy_OnTimeSet_m23F659ABBCD69358763F5E20033A6FC6531ED6BC (TimePickerDialogProxy_tEA3E774F2ACE744CCC2DBBF4A78BD2049B76E266 * __this, String_t* ___time0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimePickerDialogProxy_OnTimeSet_m23F659ABBCD69358763F5E20033A6FC6531ED6BC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* V_0 = NULL;
	{
		String_t* L_0 = ___time0;
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_1 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_2 = L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)58));
		NullCheck(L_0);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = String_Split_m13262358217AD2C119FD1B9733C3C0289D608512(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * L_4 = __this->get_onTimeSet_4();
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * L_5 = __this->get_onTimeSet_4();
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = 0;
		String_t* L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		int32_t L_9 = Int32_Parse_m5807B6243415790250FC25168F767C08FC16FDEA(L_8, /*hidden argument*/NULL);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = 1;
		String_t* L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		int32_t L_13 = Int32_Parse_m5807B6243415790250FC25168F767C08FC16FDEA(L_12, /*hidden argument*/NULL);
		NullCheck(L_5);
		Action_2_Invoke_mCEA7B556FB0AEA5B7E000DC9073DC99CD834DE9C(L_5, L_9, L_13, /*hidden argument*/Action_2_Invoke_mCEA7B556FB0AEA5B7E000DC9073DC99CD834DE9C_RuntimeMethod_var);
	}

IL_0038:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_14, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
