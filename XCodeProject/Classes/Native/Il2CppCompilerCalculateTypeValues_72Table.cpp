﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// LoadingIndicator
struct LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB;
// MPAR.Common.Scripting.Script
struct Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC;
// PersistentSettings
struct PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Zenject.DiContainer>
struct Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<System.Type,Zenject.ZenjectTypeInfo>
struct Dictionary_2_t2ABD44961254C2FE29A751ED91E310B477F82A4D;
// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ISignalHandler>>
struct Dictionary_2_tFDB1B16A194376B196608E774F54FF44416F59EA;
// System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>
struct IEnumerator_1_tC65D35659A0144DCE2D08E61BD96C952A80AE3EC;
// System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>
struct IEnumerator_1_t537D797E2644AF9046B1E4CAAC405D8CE9D01534;
// System.Collections.Generic.IEnumerator`1<UnityEngine.SceneManagement.Scene>
struct IEnumerator_1_tD380563E1B2946325932A78A8B0091DD21D78B8D;
// System.Collections.Generic.List`1<System.Reflection.FieldInfo>
struct List_1_t0F52CF0559ED3BE13F054035F8208B5FDEB9D9DA;
// System.Collections.Generic.List`1<System.Type>
struct List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0;
// System.Collections.Generic.List`1<Zenject.IMemoryPool>
struct List_1_t90691D342192BDA916E86770DC2901E8F3E6A1D5;
// System.Collections.Generic.List`1<Zenject.InjectableInfo>
struct List_1_t1F70275437A47549627934C5EFBB8EE5054BB282;
// System.Collections.Generic.List`1<Zenject.PostInjectableInfo>
struct List_1_tCF9CD8D2722EFB6831B5E7C28799BFAFEB16A8F1;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`2<System.Collections.Generic.List`1<Zenject.ISignalHandler>,System.Collections.Generic.IEnumerable`1<Zenject.ISignalHandler>>
struct Func_2_t3CBA2F32C4AD9429373B2F3156293476A385EFD7;
// System.Func`2<System.Linq.IGrouping`2<System.String,System.String>,System.String>
struct Func_2_t6CF8B1F7ED40EAD88BCBE2322E955846366EF98B;
// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean>
struct Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D;
// System.Func`2<System.Reflection.FieldInfo,System.String>
struct Func_2_tB05273985A55D6B76307D55CDF577A8EE8D2E3E2;
// System.Func`2<System.Reflection.MethodInfo,System.Boolean>
struct Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438;
// System.Func`2<System.Reflection.ParameterInfo,Zenject.InjectableInfo>
struct Func_2_tE339DB5B0571B8CC43DA3C09F35F85011FBE664D;
// System.Func`2<System.Reflection.PropertyInfo,System.Boolean>
struct Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C;
// System.Func`2<System.String,System.String[]>
struct Func_2_t221131D6B57E18A8A6CF24B7A5F136E1307A80E9;
// System.Func`2<System.String[],System.String>
struct Func_2_t0D79B303B01470E9550E1FAB1948BC73A7572D53;
// System.Func`2<System.Type,Zenject.TypeValuePair>
struct Func_2_t1C7ECA84A4631405AD4807A4F38B93978D571BE7;
// System.Func`2<UnityEngine.GameObject,System.Boolean>
struct Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273;
// System.Func`2<UnityEngine.GameObject,System.Collections.Generic.IEnumerable`1<Zenject.SceneContext>>
struct Func_2_tC515A1EBB1A79A89392E42BD4A197686B1557DFF;
// System.Func`2<Zenject.PostInjectableInfo,System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo>>
struct Func_2_tA04F5232132EDB205022805E6F0BA24974CC4C2B;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tF94AD07E062BC08ECD019A21E7A7B861654905F7;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.InputField
struct InputField_t533609195B110760BCFF00B746C87D81969CB005;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// Zenject.BindFinalizerWrapper
struct BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6;
// Zenject.BindInfo
struct BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1;
// Zenject.BindingId
struct BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1;
// Zenject.DiContainer
struct DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9;
// Zenject.InjectableInfo
struct InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4;
// Zenject.ProjectKernel
struct ProjectKernel_t2E658E71A90EDA4D5E816495D605B0D0BB3E80AC;
// Zenject.SceneContext
struct SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F;
// Zenject.SignalManager
struct SignalManager_tF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E;
// Zenject.SignalSettings
struct SignalSettings_t8968654EED36CD78A8475AA11C018307AD655E9F;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EF__ANONYMOUSTYPE1_T7BEBF5F43ACE43421537B4C061E9217BAE29151F_H
#define U3CU3EF__ANONYMOUSTYPE1_T7BEBF5F43ACE43421537B4C061E9217BAE29151F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <>f__AnonymousType1
struct  U3CU3Ef__AnonymousType1_t7BEBF5F43ACE43421537B4C061E9217BAE29151F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EF__ANONYMOUSTYPE1_T7BEBF5F43ACE43421537B4C061E9217BAE29151F_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1462DEA80E3C0F6D30E594F33C4F5283DF75F28F_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1462DEA80E3C0F6D30E594F33C4F5283DF75F28F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1462DEA80E3C0F6D30E594F33C4F5283DF75F28F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1462DEA80E3C0F6D30E594F33C4F5283DF75F28F_H
#ifndef IPMANAGER_T152C9A4281FBE031475645C78F9FA88CA6BF6765_H
#define IPMANAGER_T152C9A4281FBE031475645C78F9FA88CA6BF6765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IPManager
struct  IPManager_t152C9A4281FBE031475645C78F9FA88CA6BF6765  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPMANAGER_T152C9A4281FBE031475645C78F9FA88CA6BF6765_H
#ifndef U3CHIDECOROUTINEU3ED__15_TAB671F3B963EF2DA43B161FF19BB55DB76177E48_H
#define U3CHIDECOROUTINEU3ED__15_TAB671F3B963EF2DA43B161FF19BB55DB76177E48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadingIndicator/<HideCoroutine>d__15
struct  U3CHideCoroutineU3Ed__15_tAB671F3B963EF2DA43B161FF19BB55DB76177E48  : public RuntimeObject
{
public:
	// System.Int32 LoadingIndicator/<HideCoroutine>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadingIndicator/<HideCoroutine>d__15::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoadingIndicator LoadingIndicator/<HideCoroutine>d__15::<>4__this
	LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CHideCoroutineU3Ed__15_tAB671F3B963EF2DA43B161FF19BB55DB76177E48, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CHideCoroutineU3Ed__15_tAB671F3B963EF2DA43B161FF19BB55DB76177E48, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CHideCoroutineU3Ed__15_tAB671F3B963EF2DA43B161FF19BB55DB76177E48, ___U3CU3E4__this_2)); }
	inline LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIDECOROUTINEU3ED__15_TAB671F3B963EF2DA43B161FF19BB55DB76177E48_H
#ifndef U3CSHOWCOROUTINEU3ED__14_T729AE09528028F2932139F148F8E55265C8B4362_H
#define U3CSHOWCOROUTINEU3ED__14_T729AE09528028F2932139F148F8E55265C8B4362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadingIndicator/<ShowCoroutine>d__14
struct  U3CShowCoroutineU3Ed__14_t729AE09528028F2932139F148F8E55265C8B4362  : public RuntimeObject
{
public:
	// System.Int32 LoadingIndicator/<ShowCoroutine>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadingIndicator/<ShowCoroutine>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoadingIndicator LoadingIndicator/<ShowCoroutine>d__14::<>4__this
	LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB * ___U3CU3E4__this_2;
	// System.Single LoadingIndicator/<ShowCoroutine>d__14::<blurLevel>5__2
	float ___U3CblurLevelU3E5__2_3;
	// System.Single LoadingIndicator/<ShowCoroutine>d__14::<startTime>5__3
	float ___U3CstartTimeU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowCoroutineU3Ed__14_t729AE09528028F2932139F148F8E55265C8B4362, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CShowCoroutineU3Ed__14_t729AE09528028F2932139F148F8E55265C8B4362, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CShowCoroutineU3Ed__14_t729AE09528028F2932139F148F8E55265C8B4362, ___U3CU3E4__this_2)); }
	inline LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CblurLevelU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CShowCoroutineU3Ed__14_t729AE09528028F2932139F148F8E55265C8B4362, ___U3CblurLevelU3E5__2_3)); }
	inline float get_U3CblurLevelU3E5__2_3() const { return ___U3CblurLevelU3E5__2_3; }
	inline float* get_address_of_U3CblurLevelU3E5__2_3() { return &___U3CblurLevelU3E5__2_3; }
	inline void set_U3CblurLevelU3E5__2_3(float value)
	{
		___U3CblurLevelU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CShowCoroutineU3Ed__14_t729AE09528028F2932139F148F8E55265C8B4362, ___U3CstartTimeU3E5__3_4)); }
	inline float get_U3CstartTimeU3E5__3_4() const { return ___U3CstartTimeU3E5__3_4; }
	inline float* get_address_of_U3CstartTimeU3E5__3_4() { return &___U3CstartTimeU3E5__3_4; }
	inline void set_U3CstartTimeU3E5__3_4(float value)
	{
		___U3CstartTimeU3E5__3_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWCOROUTINEU3ED__14_T729AE09528028F2932139F148F8E55265C8B4362_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef URIHELPER_TB7DB0E6AFBD8133A6AC20D4ED04BFAC35DCC1098_H
#define URIHELPER_TB7DB0E6AFBD8133A6AC20D4ED04BFAC35DCC1098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UriHelper
struct  UriHelper_tB7DB0E6AFBD8133A6AC20D4ED04BFAC35DCC1098  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIHELPER_TB7DB0E6AFBD8133A6AC20D4ED04BFAC35DCC1098_H
#ifndef U3CU3EC_T4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_H
#define U3CU3EC_T4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UriHelper/<>c
struct  U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_StaticFields
{
public:
	// UriHelper/<>c UriHelper/<>c::<>9
	U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.String[]> UriHelper/<>c::<>9__0_0
	Func_2_t221131D6B57E18A8A6CF24B7A5F136E1307A80E9 * ___U3CU3E9__0_0_1;
	// System.Func`2<System.String[],System.String> UriHelper/<>c::<>9__0_1
	Func_2_t0D79B303B01470E9550E1FAB1948BC73A7572D53 * ___U3CU3E9__0_1_2;
	// System.Func`2<System.String[],System.String> UriHelper/<>c::<>9__0_2
	Func_2_t0D79B303B01470E9550E1FAB1948BC73A7572D53 * ___U3CU3E9__0_2_3;
	// System.Func`2<System.Linq.IGrouping`2<System.String,System.String>,System.String> UriHelper/<>c::<>9__0_3
	Func_2_t6CF8B1F7ED40EAD88BCBE2322E955846366EF98B * ___U3CU3E9__0_3_4;
	// System.Func`2<System.Linq.IGrouping`2<System.String,System.String>,System.String> UriHelper/<>c::<>9__0_4
	Func_2_t6CF8B1F7ED40EAD88BCBE2322E955846366EF98B * ___U3CU3E9__0_4_5;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Func_2_t221131D6B57E18A8A6CF24B7A5F136E1307A80E9 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Func_2_t221131D6B57E18A8A6CF24B7A5F136E1307A80E9 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Func_2_t221131D6B57E18A8A6CF24B7A5F136E1307A80E9 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_StaticFields, ___U3CU3E9__0_1_2)); }
	inline Func_2_t0D79B303B01470E9550E1FAB1948BC73A7572D53 * get_U3CU3E9__0_1_2() const { return ___U3CU3E9__0_1_2; }
	inline Func_2_t0D79B303B01470E9550E1FAB1948BC73A7572D53 ** get_address_of_U3CU3E9__0_1_2() { return &___U3CU3E9__0_1_2; }
	inline void set_U3CU3E9__0_1_2(Func_2_t0D79B303B01470E9550E1FAB1948BC73A7572D53 * value)
	{
		___U3CU3E9__0_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_StaticFields, ___U3CU3E9__0_2_3)); }
	inline Func_2_t0D79B303B01470E9550E1FAB1948BC73A7572D53 * get_U3CU3E9__0_2_3() const { return ___U3CU3E9__0_2_3; }
	inline Func_2_t0D79B303B01470E9550E1FAB1948BC73A7572D53 ** get_address_of_U3CU3E9__0_2_3() { return &___U3CU3E9__0_2_3; }
	inline void set_U3CU3E9__0_2_3(Func_2_t0D79B303B01470E9550E1FAB1948BC73A7572D53 * value)
	{
		___U3CU3E9__0_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_StaticFields, ___U3CU3E9__0_3_4)); }
	inline Func_2_t6CF8B1F7ED40EAD88BCBE2322E955846366EF98B * get_U3CU3E9__0_3_4() const { return ___U3CU3E9__0_3_4; }
	inline Func_2_t6CF8B1F7ED40EAD88BCBE2322E955846366EF98B ** get_address_of_U3CU3E9__0_3_4() { return &___U3CU3E9__0_3_4; }
	inline void set_U3CU3E9__0_3_4(Func_2_t6CF8B1F7ED40EAD88BCBE2322E955846366EF98B * value)
	{
		___U3CU3E9__0_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_3_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_4_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_StaticFields, ___U3CU3E9__0_4_5)); }
	inline Func_2_t6CF8B1F7ED40EAD88BCBE2322E955846366EF98B * get_U3CU3E9__0_4_5() const { return ___U3CU3E9__0_4_5; }
	inline Func_2_t6CF8B1F7ED40EAD88BCBE2322E955846366EF98B ** get_address_of_U3CU3E9__0_4_5() { return &___U3CU3E9__0_4_5; }
	inline void set_U3CU3E9__0_4_5(Func_2_t6CF8B1F7ED40EAD88BCBE2322E955846366EF98B * value)
	{
		___U3CU3E9__0_4_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_4_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_H
#ifndef INSTALLERBASE_TAD3E3395B906666822EB47EAC578547EEB7DC56F_H
#define INSTALLERBASE_TAD3E3395B906666822EB47EAC578547EEB7DC56F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstallerBase
struct  InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.InstallerBase::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_0;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F, ____container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_0() const { return ____container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLERBASE_TAD3E3395B906666822EB47EAC578547EEB7DC56F_H
#ifndef ZENUTILINTERNAL_TDCB564D327DDB97F765E7D8466D507E68BD800EE_H
#define ZENUTILINTERNAL_TDCB564D327DDB97F765E7D8466D507E68BD800EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Internal.ZenUtilInternal
struct  ZenUtilInternal_tDCB564D327DDB97F765E7D8466D507E68BD800EE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZENUTILINTERNAL_TDCB564D327DDB97F765E7D8466D507E68BD800EE_H
#ifndef U3CU3EC_TEE1674805D45C1CC74A6FAD2B2A74BCF02363D03_H
#define U3CU3EC_TEE1674805D45C1CC74A6FAD2B2A74BCF02363D03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Internal.ZenUtilInternal/<>c
struct  U3CU3Ec_tEE1674805D45C1CC74A6FAD2B2A74BCF02363D03  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tEE1674805D45C1CC74A6FAD2B2A74BCF02363D03_StaticFields
{
public:
	// Zenject.Internal.ZenUtilInternal/<>c Zenject.Internal.ZenUtilInternal/<>c::<>9
	U3CU3Ec_tEE1674805D45C1CC74A6FAD2B2A74BCF02363D03 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.GameObject,System.Collections.Generic.IEnumerable`1<Zenject.SceneContext>> Zenject.Internal.ZenUtilInternal/<>c::<>9__3_0
	Func_2_tC515A1EBB1A79A89392E42BD4A197686B1557DFF * ___U3CU3E9__3_0_1;
	// System.Func`2<UnityEngine.GameObject,System.Boolean> Zenject.Internal.ZenUtilInternal/<>c::<>9__7_0
	Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 * ___U3CU3E9__7_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEE1674805D45C1CC74A6FAD2B2A74BCF02363D03_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tEE1674805D45C1CC74A6FAD2B2A74BCF02363D03 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tEE1674805D45C1CC74A6FAD2B2A74BCF02363D03 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tEE1674805D45C1CC74A6FAD2B2A74BCF02363D03 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEE1674805D45C1CC74A6FAD2B2A74BCF02363D03_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_2_tC515A1EBB1A79A89392E42BD4A197686B1557DFF * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_2_tC515A1EBB1A79A89392E42BD4A197686B1557DFF ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_2_tC515A1EBB1A79A89392E42BD4A197686B1557DFF * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEE1674805D45C1CC74A6FAD2B2A74BCF02363D03_StaticFields, ___U3CU3E9__7_0_2)); }
	inline Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 * get_U3CU3E9__7_0_2() const { return ___U3CU3E9__7_0_2; }
	inline Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 ** get_address_of_U3CU3E9__7_0_2() { return &___U3CU3E9__7_0_2; }
	inline void set_U3CU3E9__7_0_2(Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 * value)
	{
		___U3CU3E9__7_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TEE1674805D45C1CC74A6FAD2B2A74BCF02363D03_H
#ifndef U3CGETALLSCENECONTEXTSU3ED__3_T456733539FADBC242179F1A9000716AE4B453721_H
#define U3CGETALLSCENECONTEXTSU3ED__3_T456733539FADBC242179F1A9000716AE4B453721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Internal.ZenUtilInternal/<GetAllSceneContexts>d__3
struct  U3CGetAllSceneContextsU3Ed__3_t456733539FADBC242179F1A9000716AE4B453721  : public RuntimeObject
{
public:
	// System.Int32 Zenject.Internal.ZenUtilInternal/<GetAllSceneContexts>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Zenject.SceneContext Zenject.Internal.ZenUtilInternal/<GetAllSceneContexts>d__3::<>2__current
	SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F * ___U3CU3E2__current_1;
	// System.Int32 Zenject.Internal.ZenUtilInternal/<GetAllSceneContexts>d__3::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.SceneManagement.Scene> Zenject.Internal.ZenUtilInternal/<GetAllSceneContexts>d__3::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllSceneContextsU3Ed__3_t456733539FADBC242179F1A9000716AE4B453721, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllSceneContextsU3Ed__3_t456733539FADBC242179F1A9000716AE4B453721, ___U3CU3E2__current_1)); }
	inline SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllSceneContextsU3Ed__3_t456733539FADBC242179F1A9000716AE4B453721, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CGetAllSceneContextsU3Ed__3_t456733539FADBC242179F1A9000716AE4B453721, ___U3CU3E7__wrap1_3)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLSCENECONTEXTSU3ED__3_T456733539FADBC242179F1A9000716AE4B453721_H
#ifndef NONLAZYBINDER_TD353CE02C951C95FD045F6C21516882D489A0B8E_H
#define NONLAZYBINDER_TD353CE02C951C95FD045F6C21516882D489A0B8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NonLazyBinder
struct  NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.NonLazyBinder::<BindInfo>k__BackingField
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * ___U3CBindInfoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBindInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E, ___U3CBindInfoU3Ek__BackingField_0)); }
	inline BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * get_U3CBindInfoU3Ek__BackingField_0() const { return ___U3CBindInfoU3Ek__BackingField_0; }
	inline BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 ** get_address_of_U3CBindInfoU3Ek__BackingField_0() { return &___U3CBindInfoU3Ek__BackingField_0; }
	inline void set_U3CBindInfoU3Ek__BackingField_0(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * value)
	{
		___U3CBindInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindInfoU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONLAZYBINDER_TD353CE02C951C95FD045F6C21516882D489A0B8E_H
#ifndef POOLCLEANUPCHECKER_TB374AB024B9E70FDC66B091742099CCBD1FFC664_H
#define POOLCLEANUPCHECKER_TB374AB024B9E70FDC66B091742099CCBD1FFC664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PoolCleanupChecker
struct  PoolCleanupChecker_tB374AB024B9E70FDC66B091742099CCBD1FFC664  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.IMemoryPool> Zenject.PoolCleanupChecker::_poolFactories
	List_1_t90691D342192BDA916E86770DC2901E8F3E6A1D5 * ____poolFactories_0;

public:
	inline static int32_t get_offset_of__poolFactories_0() { return static_cast<int32_t>(offsetof(PoolCleanupChecker_tB374AB024B9E70FDC66B091742099CCBD1FFC664, ____poolFactories_0)); }
	inline List_1_t90691D342192BDA916E86770DC2901E8F3E6A1D5 * get__poolFactories_0() const { return ____poolFactories_0; }
	inline List_1_t90691D342192BDA916E86770DC2901E8F3E6A1D5 ** get_address_of__poolFactories_0() { return &____poolFactories_0; }
	inline void set__poolFactories_0(List_1_t90691D342192BDA916E86770DC2901E8F3E6A1D5 * value)
	{
		____poolFactories_0 = value;
		Il2CppCodeGenWriteBarrier((&____poolFactories_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLCLEANUPCHECKER_TB374AB024B9E70FDC66B091742099CCBD1FFC664_H
#ifndef POSTINJECTABLEINFO_TEF74AB1901215366F3CA2D03A895CEA79DD0CEBC_H
#define POSTINJECTABLEINFO_TEF74AB1901215366F3CA2D03A895CEA79DD0CEBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PostInjectableInfo
struct  PostInjectableInfo_tEF74AB1901215366F3CA2D03A895CEA79DD0CEBC  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Zenject.PostInjectableInfo::_methodInfo
	MethodInfo_t * ____methodInfo_0;
	// System.Collections.Generic.List`1<Zenject.InjectableInfo> Zenject.PostInjectableInfo::_injectableInfo
	List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 * ____injectableInfo_1;

public:
	inline static int32_t get_offset_of__methodInfo_0() { return static_cast<int32_t>(offsetof(PostInjectableInfo_tEF74AB1901215366F3CA2D03A895CEA79DD0CEBC, ____methodInfo_0)); }
	inline MethodInfo_t * get__methodInfo_0() const { return ____methodInfo_0; }
	inline MethodInfo_t ** get_address_of__methodInfo_0() { return &____methodInfo_0; }
	inline void set__methodInfo_0(MethodInfo_t * value)
	{
		____methodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____methodInfo_0), value);
	}

	inline static int32_t get_offset_of__injectableInfo_1() { return static_cast<int32_t>(offsetof(PostInjectableInfo_tEF74AB1901215366F3CA2D03A895CEA79DD0CEBC, ____injectableInfo_1)); }
	inline List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 * get__injectableInfo_1() const { return ____injectableInfo_1; }
	inline List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 ** get_address_of__injectableInfo_1() { return &____injectableInfo_1; }
	inline void set__injectableInfo_1(List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 * value)
	{
		____injectableInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&____injectableInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTINJECTABLEINFO_TEF74AB1901215366F3CA2D03A895CEA79DD0CEBC_H
#ifndef PROFILEBLOCK_T84DAB6CA71E6C8039E016DBAF39DB5AFCB08629F_H
#define PROFILEBLOCK_T84DAB6CA71E6C8039E016DBAF39DB5AFCB08629F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProfileBlock
struct  ProfileBlock_t84DAB6CA71E6C8039E016DBAF39DB5AFCB08629F  : public RuntimeObject
{
public:

public:
};

struct ProfileBlock_t84DAB6CA71E6C8039E016DBAF39DB5AFCB08629F_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex Zenject.ProfileBlock::<ProfilePattern>k__BackingField
	Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * ___U3CProfilePatternU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CProfilePatternU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProfileBlock_t84DAB6CA71E6C8039E016DBAF39DB5AFCB08629F_StaticFields, ___U3CProfilePatternU3Ek__BackingField_0)); }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * get_U3CProfilePatternU3Ek__BackingField_0() const { return ___U3CProfilePatternU3Ek__BackingField_0; }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF ** get_address_of_U3CProfilePatternU3Ek__BackingField_0() { return &___U3CProfilePatternU3Ek__BackingField_0; }
	inline void set_U3CProfilePatternU3Ek__BackingField_0(Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * value)
	{
		___U3CProfilePatternU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProfilePatternU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEBLOCK_T84DAB6CA71E6C8039E016DBAF39DB5AFCB08629F_H
#ifndef SIGNALBASE_TB098ECA8DBCCEC7D38BF290AB6E2DFE2D037D93B_H
#define SIGNALBASE_TB098ECA8DBCCEC7D38BF290AB6E2DFE2D037D93B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalBase
struct  SignalBase_tB098ECA8DBCCEC7D38BF290AB6E2DFE2D037D93B  : public RuntimeObject
{
public:
	// Zenject.SignalManager Zenject.SignalBase::_manager
	SignalManager_tF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E * ____manager_0;
	// Zenject.BindingId Zenject.SignalBase::<SignalId>k__BackingField
	BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * ___U3CSignalIdU3Ek__BackingField_1;
	// Zenject.SignalSettings Zenject.SignalBase::<Settings>k__BackingField
	SignalSettings_t8968654EED36CD78A8475AA11C018307AD655E9F * ___U3CSettingsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__manager_0() { return static_cast<int32_t>(offsetof(SignalBase_tB098ECA8DBCCEC7D38BF290AB6E2DFE2D037D93B, ____manager_0)); }
	inline SignalManager_tF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E * get__manager_0() const { return ____manager_0; }
	inline SignalManager_tF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E ** get_address_of__manager_0() { return &____manager_0; }
	inline void set__manager_0(SignalManager_tF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E * value)
	{
		____manager_0 = value;
		Il2CppCodeGenWriteBarrier((&____manager_0), value);
	}

	inline static int32_t get_offset_of_U3CSignalIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SignalBase_tB098ECA8DBCCEC7D38BF290AB6E2DFE2D037D93B, ___U3CSignalIdU3Ek__BackingField_1)); }
	inline BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * get_U3CSignalIdU3Ek__BackingField_1() const { return ___U3CSignalIdU3Ek__BackingField_1; }
	inline BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 ** get_address_of_U3CSignalIdU3Ek__BackingField_1() { return &___U3CSignalIdU3Ek__BackingField_1; }
	inline void set_U3CSignalIdU3Ek__BackingField_1(BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * value)
	{
		___U3CSignalIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSignalIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSettingsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SignalBase_tB098ECA8DBCCEC7D38BF290AB6E2DFE2D037D93B, ___U3CSettingsU3Ek__BackingField_2)); }
	inline SignalSettings_t8968654EED36CD78A8475AA11C018307AD655E9F * get_U3CSettingsU3Ek__BackingField_2() const { return ___U3CSettingsU3Ek__BackingField_2; }
	inline SignalSettings_t8968654EED36CD78A8475AA11C018307AD655E9F ** get_address_of_U3CSettingsU3Ek__BackingField_2() { return &___U3CSettingsU3Ek__BackingField_2; }
	inline void set_U3CSettingsU3Ek__BackingField_2(SignalSettings_t8968654EED36CD78A8475AA11C018307AD655E9F * value)
	{
		___U3CSettingsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSettingsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALBASE_TB098ECA8DBCCEC7D38BF290AB6E2DFE2D037D93B_H
#ifndef SIGNALEXTENSIONS_T7FE75A1B1F545070DE7264715F2617C76FD982CE_H
#define SIGNALEXTENSIONS_T7FE75A1B1F545070DE7264715F2617C76FD982CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalExtensions
struct  SignalExtensions_t7FE75A1B1F545070DE7264715F2617C76FD982CE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALEXTENSIONS_T7FE75A1B1F545070DE7264715F2617C76FD982CE_H
#ifndef SIGNALHANDLERBASE_T8488438F18A6B1865F24F60653E2557F7B423C48_H
#define SIGNALHANDLERBASE_T8488438F18A6B1865F24F60653E2557F7B423C48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBase
struct  SignalHandlerBase_t8488438F18A6B1865F24F60653E2557F7B423C48  : public RuntimeObject
{
public:
	// Zenject.SignalManager Zenject.SignalHandlerBase::_manager
	SignalManager_tF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E * ____manager_0;
	// Zenject.BindingId Zenject.SignalHandlerBase::_signalId
	BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * ____signalId_1;

public:
	inline static int32_t get_offset_of__manager_0() { return static_cast<int32_t>(offsetof(SignalHandlerBase_t8488438F18A6B1865F24F60653E2557F7B423C48, ____manager_0)); }
	inline SignalManager_tF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E * get__manager_0() const { return ____manager_0; }
	inline SignalManager_tF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E ** get_address_of__manager_0() { return &____manager_0; }
	inline void set__manager_0(SignalManager_tF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E * value)
	{
		____manager_0 = value;
		Il2CppCodeGenWriteBarrier((&____manager_0), value);
	}

	inline static int32_t get_offset_of__signalId_1() { return static_cast<int32_t>(offsetof(SignalHandlerBase_t8488438F18A6B1865F24F60653E2557F7B423C48, ____signalId_1)); }
	inline BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * get__signalId_1() const { return ____signalId_1; }
	inline BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 ** get_address_of__signalId_1() { return &____signalId_1; }
	inline void set__signalId_1(BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * value)
	{
		____signalId_1 = value;
		Il2CppCodeGenWriteBarrier((&____signalId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBASE_T8488438F18A6B1865F24F60653E2557F7B423C48_H
#ifndef SIGNALHANDLERBINDER_TEA2ED51A4DC1839C4FCF194C1AFE7308B4DFD018_H
#define SIGNALHANDLERBINDER_TEA2ED51A4DC1839C4FCF194C1AFE7308B4DFD018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBinder
struct  SignalHandlerBinder_tEA2ED51A4DC1839C4FCF194C1AFE7308B4DFD018  : public RuntimeObject
{
public:
	// Zenject.BindFinalizerWrapper Zenject.SignalHandlerBinder::_finalizerWrapper
	BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 * ____finalizerWrapper_0;
	// System.Type Zenject.SignalHandlerBinder::_signalType
	Type_t * ____signalType_1;
	// Zenject.DiContainer Zenject.SignalHandlerBinder::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_2;
	// System.Object Zenject.SignalHandlerBinder::<Identifier>k__BackingField
	RuntimeObject * ___U3CIdentifierU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__finalizerWrapper_0() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_tEA2ED51A4DC1839C4FCF194C1AFE7308B4DFD018, ____finalizerWrapper_0)); }
	inline BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 * get__finalizerWrapper_0() const { return ____finalizerWrapper_0; }
	inline BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 ** get_address_of__finalizerWrapper_0() { return &____finalizerWrapper_0; }
	inline void set__finalizerWrapper_0(BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 * value)
	{
		____finalizerWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&____finalizerWrapper_0), value);
	}

	inline static int32_t get_offset_of__signalType_1() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_tEA2ED51A4DC1839C4FCF194C1AFE7308B4DFD018, ____signalType_1)); }
	inline Type_t * get__signalType_1() const { return ____signalType_1; }
	inline Type_t ** get_address_of__signalType_1() { return &____signalType_1; }
	inline void set__signalType_1(Type_t * value)
	{
		____signalType_1 = value;
		Il2CppCodeGenWriteBarrier((&____signalType_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_tEA2ED51A4DC1839C4FCF194C1AFE7308B4DFD018, ____container_2)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_2() const { return ____container_2; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}

	inline static int32_t get_offset_of_U3CIdentifierU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_tEA2ED51A4DC1839C4FCF194C1AFE7308B4DFD018, ___U3CIdentifierU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CIdentifierU3Ek__BackingField_3() const { return ___U3CIdentifierU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CIdentifierU3Ek__BackingField_3() { return &___U3CIdentifierU3Ek__BackingField_3; }
	inline void set_U3CIdentifierU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CIdentifierU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdentifierU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBINDER_TEA2ED51A4DC1839C4FCF194C1AFE7308B4DFD018_H
#ifndef SIGNALMANAGER_TF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E_H
#define SIGNALMANAGER_TF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalManager
struct  SignalManager_tF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ISignalHandler>> Zenject.SignalManager::_signalHandlers
	Dictionary_2_tFDB1B16A194376B196608E774F54FF44416F59EA * ____signalHandlers_0;

public:
	inline static int32_t get_offset_of__signalHandlers_0() { return static_cast<int32_t>(offsetof(SignalManager_tF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E, ____signalHandlers_0)); }
	inline Dictionary_2_tFDB1B16A194376B196608E774F54FF44416F59EA * get__signalHandlers_0() const { return ____signalHandlers_0; }
	inline Dictionary_2_tFDB1B16A194376B196608E774F54FF44416F59EA ** get_address_of__signalHandlers_0() { return &____signalHandlers_0; }
	inline void set__signalHandlers_0(Dictionary_2_tFDB1B16A194376B196608E774F54FF44416F59EA * value)
	{
		____signalHandlers_0 = value;
		Il2CppCodeGenWriteBarrier((&____signalHandlers_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALMANAGER_TF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E_H
#ifndef U3CU3EC_T806CEA5E14B58FCDEE373B21AE833BC725C5B545_H
#define U3CU3EC_T806CEA5E14B58FCDEE373B21AE833BC725C5B545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalManager/<>c
struct  U3CU3Ec_t806CEA5E14B58FCDEE373B21AE833BC725C5B545  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t806CEA5E14B58FCDEE373B21AE833BC725C5B545_StaticFields
{
public:
	// Zenject.SignalManager/<>c Zenject.SignalManager/<>c::<>9
	U3CU3Ec_t806CEA5E14B58FCDEE373B21AE833BC725C5B545 * ___U3CU3E9_0;
	// System.Func`2<System.Collections.Generic.List`1<Zenject.ISignalHandler>,System.Collections.Generic.IEnumerable`1<Zenject.ISignalHandler>> Zenject.SignalManager/<>c::<>9__6_0
	Func_2_t3CBA2F32C4AD9429373B2F3156293476A385EFD7 * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t806CEA5E14B58FCDEE373B21AE833BC725C5B545_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t806CEA5E14B58FCDEE373B21AE833BC725C5B545 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t806CEA5E14B58FCDEE373B21AE833BC725C5B545 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t806CEA5E14B58FCDEE373B21AE833BC725C5B545 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t806CEA5E14B58FCDEE373B21AE833BC725C5B545_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Func_2_t3CBA2F32C4AD9429373B2F3156293476A385EFD7 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Func_2_t3CBA2F32C4AD9429373B2F3156293476A385EFD7 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Func_2_t3CBA2F32C4AD9429373B2F3156293476A385EFD7 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T806CEA5E14B58FCDEE373B21AE833BC725C5B545_H
#ifndef SIGNALSETTINGS_T8968654EED36CD78A8475AA11C018307AD655E9F_H
#define SIGNALSETTINGS_T8968654EED36CD78A8475AA11C018307AD655E9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalSettings
struct  SignalSettings_t8968654EED36CD78A8475AA11C018307AD655E9F  : public RuntimeObject
{
public:
	// System.Boolean Zenject.SignalSettings::RequiresHandler
	bool ___RequiresHandler_0;

public:
	inline static int32_t get_offset_of_RequiresHandler_0() { return static_cast<int32_t>(offsetof(SignalSettings_t8968654EED36CD78A8475AA11C018307AD655E9F, ___RequiresHandler_0)); }
	inline bool get_RequiresHandler_0() const { return ___RequiresHandler_0; }
	inline bool* get_address_of_RequiresHandler_0() { return &___RequiresHandler_0; }
	inline void set_RequiresHandler_0(bool value)
	{
		___RequiresHandler_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALSETTINGS_T8968654EED36CD78A8475AA11C018307AD655E9F_H
#ifndef TYPEANALYZER_TB7AFD0F4DC5654ACE781C43487CF20A9C24CC53C_H
#define TYPEANALYZER_TB7AFD0F4DC5654ACE781C43487CF20A9C24CC53C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer
struct  TypeAnalyzer_tB7AFD0F4DC5654ACE781C43487CF20A9C24CC53C  : public RuntimeObject
{
public:

public:
};

struct TypeAnalyzer_tB7AFD0F4DC5654ACE781C43487CF20A9C24CC53C_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Zenject.ZenjectTypeInfo> Zenject.TypeAnalyzer::_typeInfo
	Dictionary_2_t2ABD44961254C2FE29A751ED91E310B477F82A4D * ____typeInfo_0;

public:
	inline static int32_t get_offset_of__typeInfo_0() { return static_cast<int32_t>(offsetof(TypeAnalyzer_tB7AFD0F4DC5654ACE781C43487CF20A9C24CC53C_StaticFields, ____typeInfo_0)); }
	inline Dictionary_2_t2ABD44961254C2FE29A751ED91E310B477F82A4D * get__typeInfo_0() const { return ____typeInfo_0; }
	inline Dictionary_2_t2ABD44961254C2FE29A751ED91E310B477F82A4D ** get_address_of__typeInfo_0() { return &____typeInfo_0; }
	inline void set__typeInfo_0(Dictionary_2_t2ABD44961254C2FE29A751ED91E310B477F82A4D * value)
	{
		____typeInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____typeInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEANALYZER_TB7AFD0F4DC5654ACE781C43487CF20A9C24CC53C_H
#ifndef U3CU3EC_T7FFB988753203548A29B999197FB5969E6D0D722_H
#define U3CU3EC_T7FFB988753203548A29B999197FB5969E6D0D722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer/<>c
struct  U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields
{
public:
	// Zenject.TypeAnalyzer/<>c Zenject.TypeAnalyzer/<>c::<>9
	U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Zenject.TypeAnalyzer/<>c::<>9__6_0
	Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * ___U3CU3E9__6_0_1;
	// System.Func`2<System.Reflection.PropertyInfo,System.Boolean> Zenject.TypeAnalyzer/<>c::<>9__7_0
	Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C * ___U3CU3E9__7_0_2;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> Zenject.TypeAnalyzer/<>c::<>9__8_0
	Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * ___U3CU3E9__8_0_3;
	// System.Func`2<System.Reflection.FieldInfo,System.String> Zenject.TypeAnalyzer/<>c::<>9__10_1
	Func_2_tB05273985A55D6B76307D55CDF577A8EE8D2E3E2 * ___U3CU3E9__10_1_4;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean> Zenject.TypeAnalyzer/<>c::<>9__12_0
	Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * ___U3CU3E9__12_0_5;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean> Zenject.TypeAnalyzer/<>c::<>9__12_1
	Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * ___U3CU3E9__12_1_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields, ___U3CU3E9__7_0_2)); }
	inline Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C * get_U3CU3E9__7_0_2() const { return ___U3CU3E9__7_0_2; }
	inline Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C ** get_address_of_U3CU3E9__7_0_2() { return &___U3CU3E9__7_0_2; }
	inline void set_U3CU3E9__7_0_2(Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C * value)
	{
		___U3CU3E9__7_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields, ___U3CU3E9__8_0_3)); }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * get_U3CU3E9__8_0_3() const { return ___U3CU3E9__8_0_3; }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D ** get_address_of_U3CU3E9__8_0_3() { return &___U3CU3E9__8_0_3; }
	inline void set_U3CU3E9__8_0_3(Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * value)
	{
		___U3CU3E9__8_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields, ___U3CU3E9__10_1_4)); }
	inline Func_2_tB05273985A55D6B76307D55CDF577A8EE8D2E3E2 * get_U3CU3E9__10_1_4() const { return ___U3CU3E9__10_1_4; }
	inline Func_2_tB05273985A55D6B76307D55CDF577A8EE8D2E3E2 ** get_address_of_U3CU3E9__10_1_4() { return &___U3CU3E9__10_1_4; }
	inline void set_U3CU3E9__10_1_4(Func_2_tB05273985A55D6B76307D55CDF577A8EE8D2E3E2 * value)
	{
		___U3CU3E9__10_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__10_1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields, ___U3CU3E9__12_0_5)); }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * get_U3CU3E9__12_0_5() const { return ___U3CU3E9__12_0_5; }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 ** get_address_of_U3CU3E9__12_0_5() { return &___U3CU3E9__12_0_5; }
	inline void set_U3CU3E9__12_0_5(Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * value)
	{
		___U3CU3E9__12_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_1_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields, ___U3CU3E9__12_1_6)); }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * get_U3CU3E9__12_1_6() const { return ___U3CU3E9__12_1_6; }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 ** get_address_of_U3CU3E9__12_1_6() { return &___U3CU3E9__12_1_6; }
	inline void set_U3CU3E9__12_1_6(Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * value)
	{
		___U3CU3E9__12_1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T7FFB988753203548A29B999197FB5969E6D0D722_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_T07DC0D4538DA5CE4F9C4E223D7F8C9F4EFF82321_H
#define U3CU3EC__DISPLAYCLASS10_0_T07DC0D4538DA5CE4F9C4E223D7F8C9F4EFF82321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer/<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t07DC0D4538DA5CE4F9C4E223D7F8C9F4EFF82321  : public RuntimeObject
{
public:
	// System.String Zenject.TypeAnalyzer/<>c__DisplayClass10_0::propertyName
	String_t* ___propertyName_0;
	// System.Collections.Generic.List`1<System.Reflection.FieldInfo> Zenject.TypeAnalyzer/<>c__DisplayClass10_0::writeableFields
	List_1_t0F52CF0559ED3BE13F054035F8208B5FDEB9D9DA * ___writeableFields_1;

public:
	inline static int32_t get_offset_of_propertyName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t07DC0D4538DA5CE4F9C4E223D7F8C9F4EFF82321, ___propertyName_0)); }
	inline String_t* get_propertyName_0() const { return ___propertyName_0; }
	inline String_t** get_address_of_propertyName_0() { return &___propertyName_0; }
	inline void set_propertyName_0(String_t* value)
	{
		___propertyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___propertyName_0), value);
	}

	inline static int32_t get_offset_of_writeableFields_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t07DC0D4538DA5CE4F9C4E223D7F8C9F4EFF82321, ___writeableFields_1)); }
	inline List_1_t0F52CF0559ED3BE13F054035F8208B5FDEB9D9DA * get_writeableFields_1() const { return ___writeableFields_1; }
	inline List_1_t0F52CF0559ED3BE13F054035F8208B5FDEB9D9DA ** get_address_of_writeableFields_1() { return &___writeableFields_1; }
	inline void set_writeableFields_1(List_1_t0F52CF0559ED3BE13F054035F8208B5FDEB9D9DA * value)
	{
		___writeableFields_1 = value;
		Il2CppCodeGenWriteBarrier((&___writeableFields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_T07DC0D4538DA5CE4F9C4E223D7F8C9F4EFF82321_H
#ifndef U3CU3EC__DISPLAYCLASS10_1_T8C31ABD7C912DD5FB2FEBB29F63632DABF71A623_H
#define U3CU3EC__DISPLAYCLASS10_1_T8C31ABD7C912DD5FB2FEBB29F63632DABF71A623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer/<>c__DisplayClass10_1
struct  U3CU3Ec__DisplayClass10_1_t8C31ABD7C912DD5FB2FEBB29F63632DABF71A623  : public RuntimeObject
{
public:
	// System.Object Zenject.TypeAnalyzer/<>c__DisplayClass10_1::injectable
	RuntimeObject * ___injectable_0;
	// System.Object Zenject.TypeAnalyzer/<>c__DisplayClass10_1::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_injectable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_1_t8C31ABD7C912DD5FB2FEBB29F63632DABF71A623, ___injectable_0)); }
	inline RuntimeObject * get_injectable_0() const { return ___injectable_0; }
	inline RuntimeObject ** get_address_of_injectable_0() { return &___injectable_0; }
	inline void set_injectable_0(RuntimeObject * value)
	{
		___injectable_0 = value;
		Il2CppCodeGenWriteBarrier((&___injectable_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_1_t8C31ABD7C912DD5FB2FEBB29F63632DABF71A623, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_1_T8C31ABD7C912DD5FB2FEBB29F63632DABF71A623_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T9035D85BB63F234BC043F613C74CEA738B56F444_H
#define U3CU3EC__DISPLAYCLASS11_0_T9035D85BB63F234BC043F613C74CEA738B56F444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer/<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t9035D85BB63F234BC043F613C74CEA738B56F444  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo Zenject.TypeAnalyzer/<>c__DisplayClass11_0::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t9035D85BB63F234BC043F613C74CEA738B56F444, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T9035D85BB63F234BC043F613C74CEA738B56F444_H
#ifndef U3CU3EC__DISPLAYCLASS11_1_TFF9724DE534BEF23F7F0C5A367776B65E8EAEF6C_H
#define U3CU3EC__DISPLAYCLASS11_1_TFF9724DE534BEF23F7F0C5A367776B65E8EAEF6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer/<>c__DisplayClass11_1
struct  U3CU3Ec__DisplayClass11_1_tFF9724DE534BEF23F7F0C5A367776B65E8EAEF6C  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo Zenject.TypeAnalyzer/<>c__DisplayClass11_1::propInfo
	PropertyInfo_t * ___propInfo_0;

public:
	inline static int32_t get_offset_of_propInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_1_tFF9724DE534BEF23F7F0C5A367776B65E8EAEF6C, ___propInfo_0)); }
	inline PropertyInfo_t * get_propInfo_0() const { return ___propInfo_0; }
	inline PropertyInfo_t ** get_address_of_propInfo_0() { return &___propInfo_0; }
	inline void set_propInfo_0(PropertyInfo_t * value)
	{
		___propInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___propInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_1_TFF9724DE534BEF23F7F0C5A367776B65E8EAEF6C_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T0FF8638353C928286144EDDB3D04D241505EAC9D_H
#define U3CU3EC__DISPLAYCLASS4_0_T0FF8638353C928286144EDDB3D04D241505EAC9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t0FF8638353C928286144EDDB3D04D241505EAC9D  : public RuntimeObject
{
public:
	// System.Type Zenject.TypeAnalyzer/<>c__DisplayClass4_0::parentType
	Type_t * ___parentType_0;

public:
	inline static int32_t get_offset_of_parentType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t0FF8638353C928286144EDDB3D04D241505EAC9D, ___parentType_0)); }
	inline Type_t * get_parentType_0() const { return ___parentType_0; }
	inline Type_t ** get_address_of_parentType_0() { return &___parentType_0; }
	inline void set_parentType_0(Type_t * value)
	{
		___parentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___parentType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T0FF8638353C928286144EDDB3D04D241505EAC9D_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T2AE3616A9A9207F9862BE92BDAB0A6E163BFC733_H
#define U3CU3EC__DISPLAYCLASS6_0_T2AE3616A9A9207F9862BE92BDAB0A6E163BFC733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t2AE3616A9A9207F9862BE92BDAB0A6E163BFC733  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Type> Zenject.TypeAnalyzer/<>c__DisplayClass6_0::heirarchyList
	List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * ___heirarchyList_0;
	// System.Type Zenject.TypeAnalyzer/<>c__DisplayClass6_0::type
	Type_t * ___type_1;
	// System.Func`2<System.Reflection.ParameterInfo,Zenject.InjectableInfo> Zenject.TypeAnalyzer/<>c__DisplayClass6_0::<>9__2
	Func_2_tE339DB5B0571B8CC43DA3C09F35F85011FBE664D * ___U3CU3E9__2_2;

public:
	inline static int32_t get_offset_of_heirarchyList_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t2AE3616A9A9207F9862BE92BDAB0A6E163BFC733, ___heirarchyList_0)); }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * get_heirarchyList_0() const { return ___heirarchyList_0; }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 ** get_address_of_heirarchyList_0() { return &___heirarchyList_0; }
	inline void set_heirarchyList_0(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * value)
	{
		___heirarchyList_0 = value;
		Il2CppCodeGenWriteBarrier((&___heirarchyList_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t2AE3616A9A9207F9862BE92BDAB0A6E163BFC733, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t2AE3616A9A9207F9862BE92BDAB0A6E163BFC733, ___U3CU3E9__2_2)); }
	inline Func_2_tE339DB5B0571B8CC43DA3C09F35F85011FBE664D * get_U3CU3E9__2_2() const { return ___U3CU3E9__2_2; }
	inline Func_2_tE339DB5B0571B8CC43DA3C09F35F85011FBE664D ** get_address_of_U3CU3E9__2_2() { return &___U3CU3E9__2_2; }
	inline void set_U3CU3E9__2_2(Func_2_tE339DB5B0571B8CC43DA3C09F35F85011FBE664D * value)
	{
		___U3CU3E9__2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T2AE3616A9A9207F9862BE92BDAB0A6E163BFC733_H
#ifndef U3CGETFIELDINJECTABLESU3ED__8_TC012AA2E4291280EC44964CCC63BAB12483FB205_H
#define U3CGETFIELDINJECTABLESU3ED__8_TC012AA2E4291280EC44964CCC63BAB12483FB205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer/<GetFieldInjectables>d__8
struct  U3CGetFieldInjectablesU3Ed__8_tC012AA2E4291280EC44964CCC63BAB12483FB205  : public RuntimeObject
{
public:
	// System.Int32 Zenject.TypeAnalyzer/<GetFieldInjectables>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Zenject.InjectableInfo Zenject.TypeAnalyzer/<GetFieldInjectables>d__8::<>2__current
	InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4 * ___U3CU3E2__current_1;
	// System.Int32 Zenject.TypeAnalyzer/<GetFieldInjectables>d__8::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Zenject.TypeAnalyzer/<GetFieldInjectables>d__8::type
	Type_t * ___type_3;
	// System.Type Zenject.TypeAnalyzer/<GetFieldInjectables>d__8::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo> Zenject.TypeAnalyzer/<GetFieldInjectables>d__8::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetFieldInjectablesU3Ed__8_tC012AA2E4291280EC44964CCC63BAB12483FB205, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetFieldInjectablesU3Ed__8_tC012AA2E4291280EC44964CCC63BAB12483FB205, ___U3CU3E2__current_1)); }
	inline InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetFieldInjectablesU3Ed__8_tC012AA2E4291280EC44964CCC63BAB12483FB205, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetFieldInjectablesU3Ed__8_tC012AA2E4291280EC44964CCC63BAB12483FB205, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetFieldInjectablesU3Ed__8_tC012AA2E4291280EC44964CCC63BAB12483FB205, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetFieldInjectablesU3Ed__8_tC012AA2E4291280EC44964CCC63BAB12483FB205, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETFIELDINJECTABLESU3ED__8_TC012AA2E4291280EC44964CCC63BAB12483FB205_H
#ifndef U3CGETPROPERTYINJECTABLESU3ED__7_TA371626F15F0D0108FF26FB6F4EF8139B5BC657D_H
#define U3CGETPROPERTYINJECTABLESU3ED__7_TA371626F15F0D0108FF26FB6F4EF8139B5BC657D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer/<GetPropertyInjectables>d__7
struct  U3CGetPropertyInjectablesU3Ed__7_tA371626F15F0D0108FF26FB6F4EF8139B5BC657D  : public RuntimeObject
{
public:
	// System.Int32 Zenject.TypeAnalyzer/<GetPropertyInjectables>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Zenject.InjectableInfo Zenject.TypeAnalyzer/<GetPropertyInjectables>d__7::<>2__current
	InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4 * ___U3CU3E2__current_1;
	// System.Int32 Zenject.TypeAnalyzer/<GetPropertyInjectables>d__7::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Zenject.TypeAnalyzer/<GetPropertyInjectables>d__7::type
	Type_t * ___type_3;
	// System.Type Zenject.TypeAnalyzer/<GetPropertyInjectables>d__7::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo> Zenject.TypeAnalyzer/<GetPropertyInjectables>d__7::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ed__7_tA371626F15F0D0108FF26FB6F4EF8139B5BC657D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ed__7_tA371626F15F0D0108FF26FB6F4EF8139B5BC657D, ___U3CU3E2__current_1)); }
	inline InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ed__7_tA371626F15F0D0108FF26FB6F4EF8139B5BC657D, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ed__7_tA371626F15F0D0108FF26FB6F4EF8139B5BC657D, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ed__7_tA371626F15F0D0108FF26FB6F4EF8139B5BC657D, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ed__7_tA371626F15F0D0108FF26FB6F4EF8139B5BC657D, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPROPERTYINJECTABLESU3ED__7_TA371626F15F0D0108FF26FB6F4EF8139B5BC657D_H
#ifndef VALIDATIONMARKER_T6FB7733E5ABF3FED47A9D0F3D7881E76376A622C_H
#define VALIDATIONMARKER_T6FB7733E5ABF3FED47A9D0F3D7881E76376A622C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ValidationMarker
struct  ValidationMarker_t6FB7733E5ABF3FED47A9D0F3D7881E76376A622C  : public RuntimeObject
{
public:
	// System.Type Zenject.ValidationMarker::<MarkedType>k__BackingField
	Type_t * ___U3CMarkedTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CMarkedTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ValidationMarker_t6FB7733E5ABF3FED47A9D0F3D7881E76376A622C, ___U3CMarkedTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CMarkedTypeU3Ek__BackingField_0() const { return ___U3CMarkedTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CMarkedTypeU3Ek__BackingField_0() { return &___U3CMarkedTypeU3Ek__BackingField_0; }
	inline void set_U3CMarkedTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CMarkedTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMarkedTypeU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONMARKER_T6FB7733E5ABF3FED47A9D0F3D7881E76376A622C_H
#ifndef VALIDATIONUTIL_T4DC76246AB32FBE6FA3A05DF467E405CD671E2D4_H
#define VALIDATIONUTIL_T4DC76246AB32FBE6FA3A05DF467E405CD671E2D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ValidationUtil
struct  ValidationUtil_t4DC76246AB32FBE6FA3A05DF467E405CD671E2D4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONUTIL_T4DC76246AB32FBE6FA3A05DF467E405CD671E2D4_H
#ifndef U3CU3EC_T44386611299F7A5044D2E232230F8D62EE14300F_H
#define U3CU3EC_T44386611299F7A5044D2E232230F8D62EE14300F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ValidationUtil/<>c
struct  U3CU3Ec_t44386611299F7A5044D2E232230F8D62EE14300F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t44386611299F7A5044D2E232230F8D62EE14300F_StaticFields
{
public:
	// Zenject.ValidationUtil/<>c Zenject.ValidationUtil/<>c::<>9
	U3CU3Ec_t44386611299F7A5044D2E232230F8D62EE14300F * ___U3CU3E9_0;
	// System.Func`2<System.Type,Zenject.TypeValuePair> Zenject.ValidationUtil/<>c::<>9__0_0
	Func_2_t1C7ECA84A4631405AD4807A4F38B93978D571BE7 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t44386611299F7A5044D2E232230F8D62EE14300F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t44386611299F7A5044D2E232230F8D62EE14300F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t44386611299F7A5044D2E232230F8D62EE14300F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t44386611299F7A5044D2E232230F8D62EE14300F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t44386611299F7A5044D2E232230F8D62EE14300F_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Func_2_t1C7ECA84A4631405AD4807A4F38B93978D571BE7 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Func_2_t1C7ECA84A4631405AD4807A4F38B93978D571BE7 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Func_2_t1C7ECA84A4631405AD4807A4F38B93978D571BE7 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T44386611299F7A5044D2E232230F8D62EE14300F_H
#ifndef ZENJECTSCENELOADER_TC1B08B820353C3517187298745318FB9BAD3062D_H
#define ZENJECTSCENELOADER_TC1B08B820353C3517187298745318FB9BAD3062D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectSceneLoader
struct  ZenjectSceneLoader_tC1B08B820353C3517187298745318FB9BAD3062D  : public RuntimeObject
{
public:
	// Zenject.ProjectKernel Zenject.ZenjectSceneLoader::_projectKernel
	ProjectKernel_t2E658E71A90EDA4D5E816495D605B0D0BB3E80AC * ____projectKernel_0;
	// Zenject.DiContainer Zenject.ZenjectSceneLoader::_sceneContainer
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____sceneContainer_1;

public:
	inline static int32_t get_offset_of__projectKernel_0() { return static_cast<int32_t>(offsetof(ZenjectSceneLoader_tC1B08B820353C3517187298745318FB9BAD3062D, ____projectKernel_0)); }
	inline ProjectKernel_t2E658E71A90EDA4D5E816495D605B0D0BB3E80AC * get__projectKernel_0() const { return ____projectKernel_0; }
	inline ProjectKernel_t2E658E71A90EDA4D5E816495D605B0D0BB3E80AC ** get_address_of__projectKernel_0() { return &____projectKernel_0; }
	inline void set__projectKernel_0(ProjectKernel_t2E658E71A90EDA4D5E816495D605B0D0BB3E80AC * value)
	{
		____projectKernel_0 = value;
		Il2CppCodeGenWriteBarrier((&____projectKernel_0), value);
	}

	inline static int32_t get_offset_of__sceneContainer_1() { return static_cast<int32_t>(offsetof(ZenjectSceneLoader_tC1B08B820353C3517187298745318FB9BAD3062D, ____sceneContainer_1)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__sceneContainer_1() const { return ____sceneContainer_1; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__sceneContainer_1() { return &____sceneContainer_1; }
	inline void set__sceneContainer_1(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____sceneContainer_1 = value;
		Il2CppCodeGenWriteBarrier((&____sceneContainer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZENJECTSCENELOADER_TC1B08B820353C3517187298745318FB9BAD3062D_H
#ifndef ZENJECTTYPEINFO_TAED38CE1BA075C821D761EF4C05FFC687D6C95D7_H
#define ZENJECTTYPEINFO_TAED38CE1BA075C821D761EF4C05FFC687D6C95D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectTypeInfo
struct  ZenjectTypeInfo_tAED38CE1BA075C821D761EF4C05FFC687D6C95D7  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.PostInjectableInfo> Zenject.ZenjectTypeInfo::_postInjectMethods
	List_1_tCF9CD8D2722EFB6831B5E7C28799BFAFEB16A8F1 * ____postInjectMethods_0;
	// System.Collections.Generic.List`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::_constructorInjectables
	List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 * ____constructorInjectables_1;
	// System.Collections.Generic.List`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::_fieldInjectables
	List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 * ____fieldInjectables_2;
	// System.Collections.Generic.List`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::_propertyInjectables
	List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 * ____propertyInjectables_3;
	// System.Reflection.ConstructorInfo Zenject.ZenjectTypeInfo::_injectConstructor
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ____injectConstructor_4;
	// System.Type Zenject.ZenjectTypeInfo::_typeAnalyzed
	Type_t * ____typeAnalyzed_5;

public:
	inline static int32_t get_offset_of__postInjectMethods_0() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_tAED38CE1BA075C821D761EF4C05FFC687D6C95D7, ____postInjectMethods_0)); }
	inline List_1_tCF9CD8D2722EFB6831B5E7C28799BFAFEB16A8F1 * get__postInjectMethods_0() const { return ____postInjectMethods_0; }
	inline List_1_tCF9CD8D2722EFB6831B5E7C28799BFAFEB16A8F1 ** get_address_of__postInjectMethods_0() { return &____postInjectMethods_0; }
	inline void set__postInjectMethods_0(List_1_tCF9CD8D2722EFB6831B5E7C28799BFAFEB16A8F1 * value)
	{
		____postInjectMethods_0 = value;
		Il2CppCodeGenWriteBarrier((&____postInjectMethods_0), value);
	}

	inline static int32_t get_offset_of__constructorInjectables_1() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_tAED38CE1BA075C821D761EF4C05FFC687D6C95D7, ____constructorInjectables_1)); }
	inline List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 * get__constructorInjectables_1() const { return ____constructorInjectables_1; }
	inline List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 ** get_address_of__constructorInjectables_1() { return &____constructorInjectables_1; }
	inline void set__constructorInjectables_1(List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 * value)
	{
		____constructorInjectables_1 = value;
		Il2CppCodeGenWriteBarrier((&____constructorInjectables_1), value);
	}

	inline static int32_t get_offset_of__fieldInjectables_2() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_tAED38CE1BA075C821D761EF4C05FFC687D6C95D7, ____fieldInjectables_2)); }
	inline List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 * get__fieldInjectables_2() const { return ____fieldInjectables_2; }
	inline List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 ** get_address_of__fieldInjectables_2() { return &____fieldInjectables_2; }
	inline void set__fieldInjectables_2(List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 * value)
	{
		____fieldInjectables_2 = value;
		Il2CppCodeGenWriteBarrier((&____fieldInjectables_2), value);
	}

	inline static int32_t get_offset_of__propertyInjectables_3() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_tAED38CE1BA075C821D761EF4C05FFC687D6C95D7, ____propertyInjectables_3)); }
	inline List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 * get__propertyInjectables_3() const { return ____propertyInjectables_3; }
	inline List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 ** get_address_of__propertyInjectables_3() { return &____propertyInjectables_3; }
	inline void set__propertyInjectables_3(List_1_t1F70275437A47549627934C5EFBB8EE5054BB282 * value)
	{
		____propertyInjectables_3 = value;
		Il2CppCodeGenWriteBarrier((&____propertyInjectables_3), value);
	}

	inline static int32_t get_offset_of__injectConstructor_4() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_tAED38CE1BA075C821D761EF4C05FFC687D6C95D7, ____injectConstructor_4)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get__injectConstructor_4() const { return ____injectConstructor_4; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of__injectConstructor_4() { return &____injectConstructor_4; }
	inline void set__injectConstructor_4(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		____injectConstructor_4 = value;
		Il2CppCodeGenWriteBarrier((&____injectConstructor_4), value);
	}

	inline static int32_t get_offset_of__typeAnalyzed_5() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_tAED38CE1BA075C821D761EF4C05FFC687D6C95D7, ____typeAnalyzed_5)); }
	inline Type_t * get__typeAnalyzed_5() const { return ____typeAnalyzed_5; }
	inline Type_t ** get_address_of__typeAnalyzed_5() { return &____typeAnalyzed_5; }
	inline void set__typeAnalyzed_5(Type_t * value)
	{
		____typeAnalyzed_5 = value;
		Il2CppCodeGenWriteBarrier((&____typeAnalyzed_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZENJECTTYPEINFO_TAED38CE1BA075C821D761EF4C05FFC687D6C95D7_H
#ifndef U3CU3EC_T09EAB8D4CF740522DE76DF2B1DA86E15BAFD829B_H
#define U3CU3EC_T09EAB8D4CF740522DE76DF2B1DA86E15BAFD829B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectTypeInfo/<>c
struct  U3CU3Ec_t09EAB8D4CF740522DE76DF2B1DA86E15BAFD829B  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t09EAB8D4CF740522DE76DF2B1DA86E15BAFD829B_StaticFields
{
public:
	// Zenject.ZenjectTypeInfo/<>c Zenject.ZenjectTypeInfo/<>c::<>9
	U3CU3Ec_t09EAB8D4CF740522DE76DF2B1DA86E15BAFD829B * ___U3CU3E9_0;
	// System.Func`2<Zenject.PostInjectableInfo,System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo>> Zenject.ZenjectTypeInfo/<>c::<>9__12_0
	Func_2_tA04F5232132EDB205022805E6F0BA24974CC4C2B * ___U3CU3E9__12_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t09EAB8D4CF740522DE76DF2B1DA86E15BAFD829B_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t09EAB8D4CF740522DE76DF2B1DA86E15BAFD829B * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t09EAB8D4CF740522DE76DF2B1DA86E15BAFD829B ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t09EAB8D4CF740522DE76DF2B1DA86E15BAFD829B * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t09EAB8D4CF740522DE76DF2B1DA86E15BAFD829B_StaticFields, ___U3CU3E9__12_0_1)); }
	inline Func_2_tA04F5232132EDB205022805E6F0BA24974CC4C2B * get_U3CU3E9__12_0_1() const { return ___U3CU3E9__12_0_1; }
	inline Func_2_tA04F5232132EDB205022805E6F0BA24974CC4C2B ** get_address_of_U3CU3E9__12_0_1() { return &___U3CU3E9__12_0_1; }
	inline void set_U3CU3E9__12_0_1(Func_2_tA04F5232132EDB205022805E6F0BA24974CC4C2B * value)
	{
		___U3CU3E9__12_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T09EAB8D4CF740522DE76DF2B1DA86E15BAFD829B_H
#ifndef PRESERVEATTRIBUTE_TE222384D2C1A3A56A61976D23DC21C7DD6B39A57_H
#define PRESERVEATTRIBUTE_TE222384D2C1A3A56A61976D23DC21C7DD6B39A57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.PreserveAttribute
struct  PreserveAttribute_tE222384D2C1A3A56A61976D23DC21C7DD6B39A57  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEATTRIBUTE_TE222384D2C1A3A56A61976D23DC21C7DD6B39A57_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef SCENE_T942E023788C2BC9FBB7EC8356B4FB0088B2CFED2_H
#define SCENE_T942E023788C2BC9FBB7EC8356B4FB0088B2CFED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T942E023788C2BC9FBB7EC8356B4FB0088B2CFED2_H
#ifndef COPYNONLAZYBINDER_T4C212B7E1BE267539D091F759AA6A0A9A4029A67_H
#define COPYNONLAZYBINDER_T4C212B7E1BE267539D091F759AA6A0A9A4029A67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.CopyNonLazyBinder
struct  CopyNonLazyBinder_t4C212B7E1BE267539D091F759AA6A0A9A4029A67  : public NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYNONLAZYBINDER_T4C212B7E1BE267539D091F759AA6A0A9A4029A67_H
#ifndef INSTALLER_1_T7B0D6B74E152A6A77ECC9A94F10159F721157919_H
#define INSTALLER_1_T7B0D6B74E152A6A77ECC9A94F10159F721157919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Installer`1<Zenject.ActionInstaller>
struct  Installer_1_t7B0D6B74E152A6A77ECC9A94F10159F721157919  : public InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLER_1_T7B0D6B74E152A6A77ECC9A94F10159F721157919_H
#ifndef INSTALLER_2_TC1E4F2BF52E8FBBCE95ED726A3D55E3203EB33CD_H
#define INSTALLER_2_TC1E4F2BF52E8FBBCE95ED726A3D55E3203EB33CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Installer`2<System.Collections.Generic.List`1<System.Type>,Zenject.ExecutionOrderInstaller>
struct  Installer_2_tC1E4F2BF52E8FBBCE95ED726A3D55E3203EB33CD  : public InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLER_2_TC1E4F2BF52E8FBBCE95ED726A3D55E3203EB33CD_H
#ifndef SIGNALHANDLERBINDERWITHID_T030814397863E330449B8C95C8BAF2681AB7DE9E_H
#define SIGNALHANDLERBINDERWITHID_T030814397863E330449B8C95C8BAF2681AB7DE9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBinderWithId
struct  SignalHandlerBinderWithId_t030814397863E330449B8C95C8BAF2681AB7DE9E  : public SignalHandlerBinder_tEA2ED51A4DC1839C4FCF194C1AFE7308B4DFD018
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBINDERWITHID_T030814397863E330449B8C95C8BAF2681AB7DE9E_H
#ifndef STATICMETHODSIGNALHANDLER_T2FF481FFB7BE3B78D4AF64E833E34D87236AB20D_H
#define STATICMETHODSIGNALHANDLER_T2FF481FFB7BE3B78D4AF64E833E34D87236AB20D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.StaticMethodSignalHandler
struct  StaticMethodSignalHandler_t2FF481FFB7BE3B78D4AF64E833E34D87236AB20D  : public SignalHandlerBase_t8488438F18A6B1865F24F60653E2557F7B423C48
{
public:
	// System.Action Zenject.StaticMethodSignalHandler::_method
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____method_2;

public:
	inline static int32_t get_offset_of__method_2() { return static_cast<int32_t>(offsetof(StaticMethodSignalHandler_t2FF481FFB7BE3B78D4AF64E833E34D87236AB20D, ____method_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__method_2() const { return ____method_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__method_2() { return &____method_2; }
	inline void set__method_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____method_2 = value;
		Il2CppCodeGenWriteBarrier((&____method_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICMETHODSIGNALHANDLER_T2FF481FFB7BE3B78D4AF64E833E34D87236AB20D_H
#ifndef ZENJECTALLOWDURINGVALIDATIONATTRIBUTE_TD31B7B10429387F97A4AA96518DD6940472D0A5F_H
#define ZENJECTALLOWDURINGVALIDATIONATTRIBUTE_TD31B7B10429387F97A4AA96518DD6940472D0A5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectAllowDuringValidationAttribute
struct  ZenjectAllowDuringValidationAttribute_tD31B7B10429387F97A4AA96518DD6940472D0A5F  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZENJECTALLOWDURINGVALIDATIONATTRIBUTE_TD31B7B10429387F97A4AA96518DD6940472D0A5F_H
#ifndef ZENJECTEXCEPTION_TF4348A9294229C0016AA92037BF88C84E71B3F79_H
#define ZENJECTEXCEPTION_TF4348A9294229C0016AA92037BF88C84E71B3F79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectException
struct  ZenjectException_tF4348A9294229C0016AA92037BF88C84E71B3F79  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZENJECTEXCEPTION_TF4348A9294229C0016AA92037BF88C84E71B3F79_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef ACTIONINSTALLER_T208DD56E3EF89CE3D74246CBDC1E3F9C002086D3_H
#define ACTIONINSTALLER_T208DD56E3EF89CE3D74246CBDC1E3F9C002086D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ActionInstaller
struct  ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3  : public Installer_1_t7B0D6B74E152A6A77ECC9A94F10159F721157919
{
public:
	// System.Action`1<Zenject.DiContainer> Zenject.ActionInstaller::_installMethod
	Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * ____installMethod_1;

public:
	inline static int32_t get_offset_of__installMethod_1() { return static_cast<int32_t>(offsetof(ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3, ____installMethod_1)); }
	inline Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * get__installMethod_1() const { return ____installMethod_1; }
	inline Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A ** get_address_of__installMethod_1() { return &____installMethod_1; }
	inline void set__installMethod_1(Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * value)
	{
		____installMethod_1 = value;
		Il2CppCodeGenWriteBarrier((&____installMethod_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONINSTALLER_T208DD56E3EF89CE3D74246CBDC1E3F9C002086D3_H
#ifndef CONDITIONCOPYNONLAZYBINDER_TA82CD5BABE0526375567A1A0064DAE8035AA37C7_H
#define CONDITIONCOPYNONLAZYBINDER_TA82CD5BABE0526375567A1A0064DAE8035AA37C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConditionCopyNonLazyBinder
struct  ConditionCopyNonLazyBinder_tA82CD5BABE0526375567A1A0064DAE8035AA37C7  : public CopyNonLazyBinder_t4C212B7E1BE267539D091F759AA6A0A9A4029A67
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONCOPYNONLAZYBINDER_TA82CD5BABE0526375567A1A0064DAE8035AA37C7_H
#ifndef EXECUTIONORDERINSTALLER_T2C906E26613B24B3771916A03DC1759988A9A332_H
#define EXECUTIONORDERINSTALLER_T2C906E26613B24B3771916A03DC1759988A9A332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ExecutionOrderInstaller
struct  ExecutionOrderInstaller_t2C906E26613B24B3771916A03DC1759988A9A332  : public Installer_2_tC1E4F2BF52E8FBBCE95ED726A3D55E3203EB33CD
{
public:
	// System.Collections.Generic.List`1<System.Type> Zenject.ExecutionOrderInstaller::_typeOrder
	List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * ____typeOrder_1;

public:
	inline static int32_t get_offset_of__typeOrder_1() { return static_cast<int32_t>(offsetof(ExecutionOrderInstaller_t2C906E26613B24B3771916A03DC1759988A9A332, ____typeOrder_1)); }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * get__typeOrder_1() const { return ____typeOrder_1; }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 ** get_address_of__typeOrder_1() { return &____typeOrder_1; }
	inline void set__typeOrder_1(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * value)
	{
		____typeOrder_1 = value;
		Il2CppCodeGenWriteBarrier((&____typeOrder_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECUTIONORDERINSTALLER_T2C906E26613B24B3771916A03DC1759988A9A332_H
#ifndef INJECTSOURCES_T05F31227C87E0653D0F785E74E838313F2158963_H
#define INJECTSOURCES_T05F31227C87E0653D0F785E74E838313F2158963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectSources
struct  InjectSources_t05F31227C87E0653D0F785E74E838313F2158963 
{
public:
	// System.Int32 Zenject.InjectSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InjectSources_t05F31227C87E0653D0F785E74E838313F2158963, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTSOURCES_T05F31227C87E0653D0F785E74E838313F2158963_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T46D0A971F49129FC26FF8136DA57C7B85197ADEA_H
#define U3CU3EC__DISPLAYCLASS7_0_T46D0A971F49129FC26FF8136DA57C7B85197ADEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Internal.ZenUtilInternal/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t46D0A971F49129FC26FF8136DA57C7B85197ADEA  : public RuntimeObject
{
public:
	// UnityEngine.SceneManagement.Scene Zenject.Internal.ZenUtilInternal/<>c__DisplayClass7_0::scene
	Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  ___scene_0;

public:
	inline static int32_t get_offset_of_scene_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t46D0A971F49129FC26FF8136DA57C7B85197ADEA, ___scene_0)); }
	inline Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  get_scene_0() const { return ___scene_0; }
	inline Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2 * get_address_of_scene_0() { return &___scene_0; }
	inline void set_scene_0(Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  value)
	{
		___scene_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T46D0A971F49129FC26FF8136DA57C7B85197ADEA_H
#ifndef LOADSCENERELATIONSHIP_T93F81BF78DC9003120628D3DD26E4586B1BDDA0B_H
#define LOADSCENERELATIONSHIP_T93F81BF78DC9003120628D3DD26E4586B1BDDA0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.LoadSceneRelationship
struct  LoadSceneRelationship_t93F81BF78DC9003120628D3DD26E4586B1BDDA0B 
{
public:
	// System.Int32 Zenject.LoadSceneRelationship::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadSceneRelationship_t93F81BF78DC9003120628D3DD26E4586B1BDDA0B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENERELATIONSHIP_T93F81BF78DC9003120628D3DD26E4586B1BDDA0B_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef INJECTATTRIBUTEBASE_T9AE5EBBE04FDB06253E8168BBAC19336B1D545B6_H
#define INJECTATTRIBUTEBASE_T9AE5EBBE04FDB06253E8168BBAC19336B1D545B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectAttributeBase
struct  InjectAttributeBase_t9AE5EBBE04FDB06253E8168BBAC19336B1D545B6  : public PreserveAttribute_tE222384D2C1A3A56A61976D23DC21C7DD6B39A57
{
public:
	// System.Boolean Zenject.InjectAttributeBase::<Optional>k__BackingField
	bool ___U3COptionalU3Ek__BackingField_0;
	// System.Object Zenject.InjectAttributeBase::<Id>k__BackingField
	RuntimeObject * ___U3CIdU3Ek__BackingField_1;
	// Zenject.InjectSources Zenject.InjectAttributeBase::<Source>k__BackingField
	int32_t ___U3CSourceU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3COptionalU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InjectAttributeBase_t9AE5EBBE04FDB06253E8168BBAC19336B1D545B6, ___U3COptionalU3Ek__BackingField_0)); }
	inline bool get_U3COptionalU3Ek__BackingField_0() const { return ___U3COptionalU3Ek__BackingField_0; }
	inline bool* get_address_of_U3COptionalU3Ek__BackingField_0() { return &___U3COptionalU3Ek__BackingField_0; }
	inline void set_U3COptionalU3Ek__BackingField_0(bool value)
	{
		___U3COptionalU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InjectAttributeBase_t9AE5EBBE04FDB06253E8168BBAC19336B1D545B6, ___U3CIdU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CIdU3Ek__BackingField_1() const { return ___U3CIdU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CIdU3Ek__BackingField_1() { return &___U3CIdU3Ek__BackingField_1; }
	inline void set_U3CIdU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSourceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InjectAttributeBase_t9AE5EBBE04FDB06253E8168BBAC19336B1D545B6, ___U3CSourceU3Ek__BackingField_2)); }
	inline int32_t get_U3CSourceU3Ek__BackingField_2() const { return ___U3CSourceU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CSourceU3Ek__BackingField_2() { return &___U3CSourceU3Ek__BackingField_2; }
	inline void set_U3CSourceU3Ek__BackingField_2(int32_t value)
	{
		___U3CSourceU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTATTRIBUTEBASE_T9AE5EBBE04FDB06253E8168BBAC19336B1D545B6_H
#ifndef SIGNALBINDER_T2F36226E6B050D656BB4B40F7574E30D1F2D735A_H
#define SIGNALBINDER_T2F36226E6B050D656BB4B40F7574E30D1F2D735A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalBinder
struct  SignalBinder_t2F36226E6B050D656BB4B40F7574E30D1F2D735A  : public ConditionCopyNonLazyBinder_tA82CD5BABE0526375567A1A0064DAE8035AA37C7
{
public:
	// Zenject.SignalSettings Zenject.SignalBinder::_signalSettings
	SignalSettings_t8968654EED36CD78A8475AA11C018307AD655E9F * ____signalSettings_1;

public:
	inline static int32_t get_offset_of__signalSettings_1() { return static_cast<int32_t>(offsetof(SignalBinder_t2F36226E6B050D656BB4B40F7574E30D1F2D735A, ____signalSettings_1)); }
	inline SignalSettings_t8968654EED36CD78A8475AA11C018307AD655E9F * get__signalSettings_1() const { return ____signalSettings_1; }
	inline SignalSettings_t8968654EED36CD78A8475AA11C018307AD655E9F ** get_address_of__signalSettings_1() { return &____signalSettings_1; }
	inline void set__signalSettings_1(SignalSettings_t8968654EED36CD78A8475AA11C018307AD655E9F * value)
	{
		____signalSettings_1 = value;
		Il2CppCodeGenWriteBarrier((&____signalSettings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALBINDER_T2F36226E6B050D656BB4B40F7574E30D1F2D735A_H
#ifndef PLATFORMSETUP_T314B48FE4C6ED5B5F237D7FB984325512C04D941_H
#define PLATFORMSETUP_T314B48FE4C6ED5B5F237D7FB984325512C04D941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformSetup
struct  PlatformSetup_t314B48FE4C6ED5B5F237D7FB984325512C04D941  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// UnityEngine.GameObject PlatformSetup::StartMenu
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___StartMenu_4;
	// UnityEngine.GameObject PlatformSetup::GameMenu
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___GameMenu_5;

public:
	inline static int32_t get_offset_of_StartMenu_4() { return static_cast<int32_t>(offsetof(PlatformSetup_t314B48FE4C6ED5B5F237D7FB984325512C04D941, ___StartMenu_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_StartMenu_4() const { return ___StartMenu_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_StartMenu_4() { return &___StartMenu_4; }
	inline void set_StartMenu_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___StartMenu_4 = value;
		Il2CppCodeGenWriteBarrier((&___StartMenu_4), value);
	}

	inline static int32_t get_offset_of_GameMenu_5() { return static_cast<int32_t>(offsetof(PlatformSetup_t314B48FE4C6ED5B5F237D7FB984325512C04D941, ___GameMenu_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_GameMenu_5() const { return ___GameMenu_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_GameMenu_5() { return &___GameMenu_5; }
	inline void set_GameMenu_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___GameMenu_5 = value;
		Il2CppCodeGenWriteBarrier((&___GameMenu_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMSETUP_T314B48FE4C6ED5B5F237D7FB984325512C04D941_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef INJECTATTRIBUTE_TD11697BE64A422DBA087366E7D6DBDDEF8A3D575_H
#define INJECTATTRIBUTE_TD11697BE64A422DBA087366E7D6DBDDEF8A3D575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectAttribute
struct  InjectAttribute_tD11697BE64A422DBA087366E7D6DBDDEF8A3D575  : public InjectAttributeBase_t9AE5EBBE04FDB06253E8168BBAC19336B1D545B6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTATTRIBUTE_TD11697BE64A422DBA087366E7D6DBDDEF8A3D575_H
#ifndef INJECTLOCALATTRIBUTE_TBAB193047C0DD86A3222C49E6C428EC2ABD23D0B_H
#define INJECTLOCALATTRIBUTE_TBAB193047C0DD86A3222C49E6C428EC2ABD23D0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectLocalAttribute
struct  InjectLocalAttribute_tBAB193047C0DD86A3222C49E6C428EC2ABD23D0B  : public InjectAttributeBase_t9AE5EBBE04FDB06253E8168BBAC19336B1D545B6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTLOCALATTRIBUTE_TBAB193047C0DD86A3222C49E6C428EC2ABD23D0B_H
#ifndef INJECTOPTIONALATTRIBUTE_T6EA59B0AE4782A587F5331A877F30A6E264A9F5E_H
#define INJECTOPTIONALATTRIBUTE_T6EA59B0AE4782A587F5331A877F30A6E264A9F5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectOptionalAttribute
struct  InjectOptionalAttribute_t6EA59B0AE4782A587F5331A877F30A6E264A9F5E  : public InjectAttributeBase_t9AE5EBBE04FDB06253E8168BBAC19336B1D545B6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTOPTIONALATTRIBUTE_T6EA59B0AE4782A587F5331A877F30A6E264A9F5E_H
#ifndef SIGNALBINDERWITHID_TC4D95917D196EF9604E843813A2EEB7101F70A2C_H
#define SIGNALBINDERWITHID_TC4D95917D196EF9604E843813A2EEB7101F70A2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalBinderWithId
struct  SignalBinderWithId_tC4D95917D196EF9604E843813A2EEB7101F70A2C  : public SignalBinder_t2F36226E6B050D656BB4B40F7574E30D1F2D735A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALBINDERWITHID_TC4D95917D196EF9604E843813A2EEB7101F70A2C_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef APIKEYLOADER_T53B4E51AD81E5342BDB911863D5A45C801A63BBD_H
#define APIKEYLOADER_T53B4E51AD81E5342BDB911863D5A45C801A63BBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIKeyLoader
struct  APIKeyLoader_t53B4E51AD81E5342BDB911863D5A45C801A63BBD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.InputField APIKeyLoader::inputField
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___inputField_4;
	// System.String APIKeyLoader::apiKey
	String_t* ___apiKey_5;

public:
	inline static int32_t get_offset_of_inputField_4() { return static_cast<int32_t>(offsetof(APIKeyLoader_t53B4E51AD81E5342BDB911863D5A45C801A63BBD, ___inputField_4)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_inputField_4() const { return ___inputField_4; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_inputField_4() { return &___inputField_4; }
	inline void set_inputField_4(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___inputField_4 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_4), value);
	}

	inline static int32_t get_offset_of_apiKey_5() { return static_cast<int32_t>(offsetof(APIKeyLoader_t53B4E51AD81E5342BDB911863D5A45C801A63BBD, ___apiKey_5)); }
	inline String_t* get_apiKey_5() const { return ___apiKey_5; }
	inline String_t** get_address_of_apiKey_5() { return &___apiKey_5; }
	inline void set_apiKey_5(String_t* value)
	{
		___apiKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___apiKey_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIKEYLOADER_T53B4E51AD81E5342BDB911863D5A45C801A63BBD_H
#ifndef APIKEYINPUT_T4C9C8B4657469B23E91FA9DCA9A803DF8BDC52CE_H
#define APIKEYINPUT_T4C9C8B4657469B23E91FA9DCA9A803DF8BDC52CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiKeyInput
struct  ApiKeyInput_t4C9C8B4657469B23E91FA9DCA9A803DF8BDC52CE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.InputField ApiKeyInput::input
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___input_4;

public:
	inline static int32_t get_offset_of_input_4() { return static_cast<int32_t>(offsetof(ApiKeyInput_t4C9C8B4657469B23E91FA9DCA9A803DF8BDC52CE, ___input_4)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_input_4() const { return ___input_4; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_input_4() { return &___input_4; }
	inline void set_input_4(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___input_4 = value;
		Il2CppCodeGenWriteBarrier((&___input_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIKEYINPUT_T4C9C8B4657469B23E91FA9DCA9A803DF8BDC52CE_H
#ifndef SINGLETON_1_T561B5EFC5E641148E63C0370D10E00BADD54A3AD_H
#define SINGLETON_1_T561B5EFC5E641148E63C0370D10E00BADD54A3AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<PersistentSettings>
struct  Singleton_1_t561B5EFC5E641148E63C0370D10E00BADD54A3AD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t561B5EFC5E641148E63C0370D10E00BADD54A3AD_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_t561B5EFC5E641148E63C0370D10E00BADD54A3AD_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T561B5EFC5E641148E63C0370D10E00BADD54A3AD_H
#ifndef DESKTOPCURSOR_T318E148B8A08EFF28B82007A4B40E19DA47ABF66_H
#define DESKTOPCURSOR_T318E148B8A08EFF28B82007A4B40E19DA47ABF66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DesktopCursor
struct  DesktopCursor_t318E148B8A08EFF28B82007A4B40E19DA47ABF66  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single DesktopCursor::startDistance
	float ___startDistance_4;
	// UnityEngine.Quaternion DesktopCursor::startRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___startRotation_5;

public:
	inline static int32_t get_offset_of_startDistance_4() { return static_cast<int32_t>(offsetof(DesktopCursor_t318E148B8A08EFF28B82007A4B40E19DA47ABF66, ___startDistance_4)); }
	inline float get_startDistance_4() const { return ___startDistance_4; }
	inline float* get_address_of_startDistance_4() { return &___startDistance_4; }
	inline void set_startDistance_4(float value)
	{
		___startDistance_4 = value;
	}

	inline static int32_t get_offset_of_startRotation_5() { return static_cast<int32_t>(offsetof(DesktopCursor_t318E148B8A08EFF28B82007A4B40E19DA47ABF66, ___startRotation_5)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_startRotation_5() const { return ___startRotation_5; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_startRotation_5() { return &___startRotation_5; }
	inline void set_startRotation_5(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___startRotation_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESKTOPCURSOR_T318E148B8A08EFF28B82007A4B40E19DA47ABF66_H
#ifndef IMAGESIZETOPARENT_T29A2F6AEE68C4DB339A52AF8438AA9238E16D3B5_H
#define IMAGESIZETOPARENT_T29A2F6AEE68C4DB339A52AF8438AA9238E16D3B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageSizeToParent
struct  ImageSizeToParent_t29A2F6AEE68C4DB339A52AF8438AA9238E16D3B5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 ImageSizeToParent::screenWidth
	int32_t ___screenWidth_4;
	// System.Int32 ImageSizeToParent::screenHeight
	int32_t ___screenHeight_5;
	// UnityEngine.UI.RawImage ImageSizeToParent::image
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___image_6;

public:
	inline static int32_t get_offset_of_screenWidth_4() { return static_cast<int32_t>(offsetof(ImageSizeToParent_t29A2F6AEE68C4DB339A52AF8438AA9238E16D3B5, ___screenWidth_4)); }
	inline int32_t get_screenWidth_4() const { return ___screenWidth_4; }
	inline int32_t* get_address_of_screenWidth_4() { return &___screenWidth_4; }
	inline void set_screenWidth_4(int32_t value)
	{
		___screenWidth_4 = value;
	}

	inline static int32_t get_offset_of_screenHeight_5() { return static_cast<int32_t>(offsetof(ImageSizeToParent_t29A2F6AEE68C4DB339A52AF8438AA9238E16D3B5, ___screenHeight_5)); }
	inline int32_t get_screenHeight_5() const { return ___screenHeight_5; }
	inline int32_t* get_address_of_screenHeight_5() { return &___screenHeight_5; }
	inline void set_screenHeight_5(int32_t value)
	{
		___screenHeight_5 = value;
	}

	inline static int32_t get_offset_of_image_6() { return static_cast<int32_t>(offsetof(ImageSizeToParent_t29A2F6AEE68C4DB339A52AF8438AA9238E16D3B5, ___image_6)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_image_6() const { return ___image_6; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_image_6() { return &___image_6; }
	inline void set_image_6(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___image_6 = value;
		Il2CppCodeGenWriteBarrier((&___image_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGESIZETOPARENT_T29A2F6AEE68C4DB339A52AF8438AA9238E16D3B5_H
#ifndef ISMOUSEOVER_T8E136C664156CA3C198053A1DA0DB8BF23C2D5EA_H
#define ISMOUSEOVER_T8E136C664156CA3C198053A1DA0DB8BF23C2D5EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IsMouseOver
struct  IsMouseOver_t8E136C664156CA3C198053A1DA0DB8BF23C2D5EA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean IsMouseOver::isOver
	bool ___isOver_4;

public:
	inline static int32_t get_offset_of_isOver_4() { return static_cast<int32_t>(offsetof(IsMouseOver_t8E136C664156CA3C198053A1DA0DB8BF23C2D5EA, ___isOver_4)); }
	inline bool get_isOver_4() const { return ___isOver_4; }
	inline bool* get_address_of_isOver_4() { return &___isOver_4; }
	inline void set_isOver_4(bool value)
	{
		___isOver_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISMOUSEOVER_T8E136C664156CA3C198053A1DA0DB8BF23C2D5EA_H
#ifndef LOADINGINDICATOR_TBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB_H
#define LOADINGINDICATOR_TBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadingIndicator
struct  LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single LoadingIndicator::maxBlurLevel
	float ___maxBlurLevel_4;
	// System.Single LoadingIndicator::currentBlurLevel
	float ___currentBlurLevel_5;
	// System.Single LoadingIndicator::blurTransitionTime
	float ___blurTransitionTime_6;
	// UnityEngine.UI.Image LoadingIndicator::indicator
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___indicator_7;
	// UnityEngine.Sprite[] LoadingIndicator::indicatorTextures
	SpriteU5BU5D_tF94AD07E062BC08ECD019A21E7A7B861654905F7* ___indicatorTextures_8;
	// System.Single LoadingIndicator::secondsPerFrame
	float ___secondsPerFrame_9;
	// System.Single LoadingIndicator::lastFrameChange
	float ___lastFrameChange_10;
	// System.Int32 LoadingIndicator::textureIndex
	int32_t ___textureIndex_11;
	// System.Boolean LoadingIndicator::showing
	bool ___showing_12;
	// UnityEngine.GameObject[] LoadingIndicator::objectsToDisableWhileLoading
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___objectsToDisableWhileLoading_13;

public:
	inline static int32_t get_offset_of_maxBlurLevel_4() { return static_cast<int32_t>(offsetof(LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB, ___maxBlurLevel_4)); }
	inline float get_maxBlurLevel_4() const { return ___maxBlurLevel_4; }
	inline float* get_address_of_maxBlurLevel_4() { return &___maxBlurLevel_4; }
	inline void set_maxBlurLevel_4(float value)
	{
		___maxBlurLevel_4 = value;
	}

	inline static int32_t get_offset_of_currentBlurLevel_5() { return static_cast<int32_t>(offsetof(LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB, ___currentBlurLevel_5)); }
	inline float get_currentBlurLevel_5() const { return ___currentBlurLevel_5; }
	inline float* get_address_of_currentBlurLevel_5() { return &___currentBlurLevel_5; }
	inline void set_currentBlurLevel_5(float value)
	{
		___currentBlurLevel_5 = value;
	}

	inline static int32_t get_offset_of_blurTransitionTime_6() { return static_cast<int32_t>(offsetof(LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB, ___blurTransitionTime_6)); }
	inline float get_blurTransitionTime_6() const { return ___blurTransitionTime_6; }
	inline float* get_address_of_blurTransitionTime_6() { return &___blurTransitionTime_6; }
	inline void set_blurTransitionTime_6(float value)
	{
		___blurTransitionTime_6 = value;
	}

	inline static int32_t get_offset_of_indicator_7() { return static_cast<int32_t>(offsetof(LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB, ___indicator_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_indicator_7() const { return ___indicator_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_indicator_7() { return &___indicator_7; }
	inline void set_indicator_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___indicator_7 = value;
		Il2CppCodeGenWriteBarrier((&___indicator_7), value);
	}

	inline static int32_t get_offset_of_indicatorTextures_8() { return static_cast<int32_t>(offsetof(LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB, ___indicatorTextures_8)); }
	inline SpriteU5BU5D_tF94AD07E062BC08ECD019A21E7A7B861654905F7* get_indicatorTextures_8() const { return ___indicatorTextures_8; }
	inline SpriteU5BU5D_tF94AD07E062BC08ECD019A21E7A7B861654905F7** get_address_of_indicatorTextures_8() { return &___indicatorTextures_8; }
	inline void set_indicatorTextures_8(SpriteU5BU5D_tF94AD07E062BC08ECD019A21E7A7B861654905F7* value)
	{
		___indicatorTextures_8 = value;
		Il2CppCodeGenWriteBarrier((&___indicatorTextures_8), value);
	}

	inline static int32_t get_offset_of_secondsPerFrame_9() { return static_cast<int32_t>(offsetof(LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB, ___secondsPerFrame_9)); }
	inline float get_secondsPerFrame_9() const { return ___secondsPerFrame_9; }
	inline float* get_address_of_secondsPerFrame_9() { return &___secondsPerFrame_9; }
	inline void set_secondsPerFrame_9(float value)
	{
		___secondsPerFrame_9 = value;
	}

	inline static int32_t get_offset_of_lastFrameChange_10() { return static_cast<int32_t>(offsetof(LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB, ___lastFrameChange_10)); }
	inline float get_lastFrameChange_10() const { return ___lastFrameChange_10; }
	inline float* get_address_of_lastFrameChange_10() { return &___lastFrameChange_10; }
	inline void set_lastFrameChange_10(float value)
	{
		___lastFrameChange_10 = value;
	}

	inline static int32_t get_offset_of_textureIndex_11() { return static_cast<int32_t>(offsetof(LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB, ___textureIndex_11)); }
	inline int32_t get_textureIndex_11() const { return ___textureIndex_11; }
	inline int32_t* get_address_of_textureIndex_11() { return &___textureIndex_11; }
	inline void set_textureIndex_11(int32_t value)
	{
		___textureIndex_11 = value;
	}

	inline static int32_t get_offset_of_showing_12() { return static_cast<int32_t>(offsetof(LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB, ___showing_12)); }
	inline bool get_showing_12() const { return ___showing_12; }
	inline bool* get_address_of_showing_12() { return &___showing_12; }
	inline void set_showing_12(bool value)
	{
		___showing_12 = value;
	}

	inline static int32_t get_offset_of_objectsToDisableWhileLoading_13() { return static_cast<int32_t>(offsetof(LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB, ___objectsToDisableWhileLoading_13)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_objectsToDisableWhileLoading_13() const { return ___objectsToDisableWhileLoading_13; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_objectsToDisableWhileLoading_13() { return &___objectsToDisableWhileLoading_13; }
	inline void set_objectsToDisableWhileLoading_13(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___objectsToDisableWhileLoading_13 = value;
		Il2CppCodeGenWriteBarrier((&___objectsToDisableWhileLoading_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADINGINDICATOR_TBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB_H
#ifndef SOUNDEFFECTPLAYER_TE1463ABA00A81FD6189B2D6EF1556AF4B7164765_H
#define SOUNDEFFECTPLAYER_TE1463ABA00A81FD6189B2D6EF1556AF4B7164765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundEffectPlayer
struct  SoundEffectPlayer_tE1463ABA00A81FD6189B2D6EF1556AF4B7164765  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioClip SoundEffectPlayer::ScanConfirmed
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ScanConfirmed_4;
	// UnityEngine.AudioSource SoundEffectPlayer::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_5;

public:
	inline static int32_t get_offset_of_ScanConfirmed_4() { return static_cast<int32_t>(offsetof(SoundEffectPlayer_tE1463ABA00A81FD6189B2D6EF1556AF4B7164765, ___ScanConfirmed_4)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ScanConfirmed_4() const { return ___ScanConfirmed_4; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ScanConfirmed_4() { return &___ScanConfirmed_4; }
	inline void set_ScanConfirmed_4(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ScanConfirmed_4 = value;
		Il2CppCodeGenWriteBarrier((&___ScanConfirmed_4), value);
	}

	inline static int32_t get_offset_of_audioSource_5() { return static_cast<int32_t>(offsetof(SoundEffectPlayer_tE1463ABA00A81FD6189B2D6EF1556AF4B7164765, ___audioSource_5)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_5() const { return ___audioSource_5; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_5() { return &___audioSource_5; }
	inline void set_audioSource_5(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_5 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_5), value);
	}
};

struct SoundEffectPlayer_tE1463ABA00A81FD6189B2D6EF1556AF4B7164765_StaticFields
{
public:
	// SoundEffectPlayer SoundEffectPlayer::instance
	SoundEffectPlayer_tE1463ABA00A81FD6189B2D6EF1556AF4B7164765 * ___instance_6;

public:
	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(SoundEffectPlayer_tE1463ABA00A81FD6189B2D6EF1556AF4B7164765_StaticFields, ___instance_6)); }
	inline SoundEffectPlayer_tE1463ABA00A81FD6189B2D6EF1556AF4B7164765 * get_instance_6() const { return ___instance_6; }
	inline SoundEffectPlayer_tE1463ABA00A81FD6189B2D6EF1556AF4B7164765 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(SoundEffectPlayer_tE1463ABA00A81FD6189B2D6EF1556AF4B7164765 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___instance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDEFFECTPLAYER_TE1463ABA00A81FD6189B2D6EF1556AF4B7164765_H
#ifndef PERSISTENTSETTINGS_T2DE52C036DBB553D990D48539ED2D394EA3F5A26_H
#define PERSISTENTSETTINGS_T2DE52C036DBB553D990D48539ED2D394EA3F5A26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PersistentSettings
struct  PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26  : public Singleton_1_t561B5EFC5E641148E63C0370D10E00BADD54A3AD
{
public:
	// MPAR.Common.Scripting.Script PersistentSettings::<CurrentScript>k__BackingField
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___U3CCurrentScriptU3Ek__BackingField_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PersistentSettings::<Settings>k__BackingField
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___U3CSettingsU3Ek__BackingField_6;
	// System.String PersistentSettings::<DebuggingUserId>k__BackingField
	String_t* ___U3CDebuggingUserIdU3Ek__BackingField_7;
	// System.String PersistentSettings::<DebuggingMixId>k__BackingField
	String_t* ___U3CDebuggingMixIdU3Ek__BackingField_8;
	// System.String PersistentSettings::<GameSceneName>k__BackingField
	String_t* ___U3CGameSceneNameU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CCurrentScriptU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26, ___U3CCurrentScriptU3Ek__BackingField_5)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_U3CCurrentScriptU3Ek__BackingField_5() const { return ___U3CCurrentScriptU3Ek__BackingField_5; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_U3CCurrentScriptU3Ek__BackingField_5() { return &___U3CCurrentScriptU3Ek__BackingField_5; }
	inline void set_U3CCurrentScriptU3Ek__BackingField_5(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___U3CCurrentScriptU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentScriptU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CSettingsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26, ___U3CSettingsU3Ek__BackingField_6)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_U3CSettingsU3Ek__BackingField_6() const { return ___U3CSettingsU3Ek__BackingField_6; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_U3CSettingsU3Ek__BackingField_6() { return &___U3CSettingsU3Ek__BackingField_6; }
	inline void set_U3CSettingsU3Ek__BackingField_6(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___U3CSettingsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSettingsU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CDebuggingUserIdU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26, ___U3CDebuggingUserIdU3Ek__BackingField_7)); }
	inline String_t* get_U3CDebuggingUserIdU3Ek__BackingField_7() const { return ___U3CDebuggingUserIdU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CDebuggingUserIdU3Ek__BackingField_7() { return &___U3CDebuggingUserIdU3Ek__BackingField_7; }
	inline void set_U3CDebuggingUserIdU3Ek__BackingField_7(String_t* value)
	{
		___U3CDebuggingUserIdU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDebuggingUserIdU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CDebuggingMixIdU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26, ___U3CDebuggingMixIdU3Ek__BackingField_8)); }
	inline String_t* get_U3CDebuggingMixIdU3Ek__BackingField_8() const { return ___U3CDebuggingMixIdU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CDebuggingMixIdU3Ek__BackingField_8() { return &___U3CDebuggingMixIdU3Ek__BackingField_8; }
	inline void set_U3CDebuggingMixIdU3Ek__BackingField_8(String_t* value)
	{
		___U3CDebuggingMixIdU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDebuggingMixIdU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CGameSceneNameU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26, ___U3CGameSceneNameU3Ek__BackingField_9)); }
	inline String_t* get_U3CGameSceneNameU3Ek__BackingField_9() const { return ___U3CGameSceneNameU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CGameSceneNameU3Ek__BackingField_9() { return &___U3CGameSceneNameU3Ek__BackingField_9; }
	inline void set_U3CGameSceneNameU3Ek__BackingField_9(String_t* value)
	{
		___U3CGameSceneNameU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameSceneNameU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTSETTINGS_T2DE52C036DBB553D990D48539ED2D394EA3F5A26_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7200 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7200[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7201 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7202 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7202[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7203 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7204 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7204[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7205 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7206 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7206[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7207 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7208 = { sizeof (SignalSettings_t8968654EED36CD78A8475AA11C018307AD655E9F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7208[1] = 
{
	SignalSettings_t8968654EED36CD78A8475AA11C018307AD655E9F::get_offset_of_RequiresHandler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7209 = { sizeof (SignalBase_tB098ECA8DBCCEC7D38BF290AB6E2DFE2D037D93B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7209[3] = 
{
	SignalBase_tB098ECA8DBCCEC7D38BF290AB6E2DFE2D037D93B::get_offset_of__manager_0(),
	SignalBase_tB098ECA8DBCCEC7D38BF290AB6E2DFE2D037D93B::get_offset_of_U3CSignalIdU3Ek__BackingField_1(),
	SignalBase_tB098ECA8DBCCEC7D38BF290AB6E2DFE2D037D93B::get_offset_of_U3CSettingsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7210 = { sizeof (SignalBinder_t2F36226E6B050D656BB4B40F7574E30D1F2D735A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7210[1] = 
{
	SignalBinder_t2F36226E6B050D656BB4B40F7574E30D1F2D735A::get_offset_of__signalSettings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7211 = { sizeof (SignalBinderWithId_tC4D95917D196EF9604E843813A2EEB7101F70A2C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7212 = { sizeof (SignalExtensions_t7FE75A1B1F545070DE7264715F2617C76FD982CE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7213 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7213[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7214 = { sizeof (SignalHandlerBase_t8488438F18A6B1865F24F60653E2557F7B423C48), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7214[2] = 
{
	SignalHandlerBase_t8488438F18A6B1865F24F60653E2557F7B423C48::get_offset_of__manager_0(),
	SignalHandlerBase_t8488438F18A6B1865F24F60653E2557F7B423C48::get_offset_of__signalId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7215 = { sizeof (SignalHandlerBinder_tEA2ED51A4DC1839C4FCF194C1AFE7308B4DFD018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7215[4] = 
{
	SignalHandlerBinder_tEA2ED51A4DC1839C4FCF194C1AFE7308B4DFD018::get_offset_of__finalizerWrapper_0(),
	SignalHandlerBinder_tEA2ED51A4DC1839C4FCF194C1AFE7308B4DFD018::get_offset_of__signalType_1(),
	SignalHandlerBinder_tEA2ED51A4DC1839C4FCF194C1AFE7308B4DFD018::get_offset_of__container_2(),
	SignalHandlerBinder_tEA2ED51A4DC1839C4FCF194C1AFE7308B4DFD018::get_offset_of_U3CIdentifierU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7216 = { sizeof (SignalHandlerBinderWithId_t030814397863E330449B8C95C8BAF2681AB7DE9E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7217 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7217[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7218 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7219 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7219[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7220 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7221 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7221[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7222 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7223 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7223[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7224 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7225 = { sizeof (SignalManager_tF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7225[1] = 
{
	SignalManager_tF76D2EC7420947CFD3D5AC36A7FE6D5E9709FD7E::get_offset_of__signalHandlers_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7226 = { sizeof (U3CU3Ec_t806CEA5E14B58FCDEE373B21AE833BC725C5B545), -1, sizeof(U3CU3Ec_t806CEA5E14B58FCDEE373B21AE833BC725C5B545_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7226[2] = 
{
	U3CU3Ec_t806CEA5E14B58FCDEE373B21AE833BC725C5B545_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t806CEA5E14B58FCDEE373B21AE833BC725C5B545_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7227 = { sizeof (StaticMethodSignalHandler_t2FF481FFB7BE3B78D4AF64E833E34D87236AB20D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7227[1] = 
{
	StaticMethodSignalHandler_t2FF481FFB7BE3B78D4AF64E833E34D87236AB20D::get_offset_of__method_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7228 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7228[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7229 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7229[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7230 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7230[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7231 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7231[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7232 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7232[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7233 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7233[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7234 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7234[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7235 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7235[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7236 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7236[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7237 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7238 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7239 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7240 = { sizeof (InjectAttribute_tD11697BE64A422DBA087366E7D6DBDDEF8A3D575), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7241 = { sizeof (InjectAttributeBase_t9AE5EBBE04FDB06253E8168BBAC19336B1D545B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7241[3] = 
{
	InjectAttributeBase_t9AE5EBBE04FDB06253E8168BBAC19336B1D545B6::get_offset_of_U3COptionalU3Ek__BackingField_0(),
	InjectAttributeBase_t9AE5EBBE04FDB06253E8168BBAC19336B1D545B6::get_offset_of_U3CIdU3Ek__BackingField_1(),
	InjectAttributeBase_t9AE5EBBE04FDB06253E8168BBAC19336B1D545B6::get_offset_of_U3CSourceU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7242 = { sizeof (InjectLocalAttribute_tBAB193047C0DD86A3222C49E6C428EC2ABD23D0B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7243 = { sizeof (InjectOptionalAttribute_t6EA59B0AE4782A587F5331A877F30A6E264A9F5E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7244 = { sizeof (InjectSources_t05F31227C87E0653D0F785E74E838313F2158963)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7244[5] = 
{
	InjectSources_t05F31227C87E0653D0F785E74E838313F2158963::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7245 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7246 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7247 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7248 = { sizeof (ZenjectAllowDuringValidationAttribute_tD31B7B10429387F97A4AA96518DD6940472D0A5F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7249 = { sizeof (ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7249[1] = 
{
	ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3::get_offset_of__installMethod_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7250 = { sizeof (ExecutionOrderInstaller_t2C906E26613B24B3771916A03DC1759988A9A332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7250[1] = 
{
	ExecutionOrderInstaller_t2C906E26613B24B3771916A03DC1759988A9A332::get_offset_of__typeOrder_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7251 = { sizeof (PoolCleanupChecker_tB374AB024B9E70FDC66B091742099CCBD1FFC664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7251[1] = 
{
	PoolCleanupChecker_tB374AB024B9E70FDC66B091742099CCBD1FFC664::get_offset_of__poolFactories_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7252 = { sizeof (ProfileBlock_t84DAB6CA71E6C8039E016DBAF39DB5AFCB08629F), -1, sizeof(ProfileBlock_t84DAB6CA71E6C8039E016DBAF39DB5AFCB08629F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7252[1] = 
{
	ProfileBlock_t84DAB6CA71E6C8039E016DBAF39DB5AFCB08629F_StaticFields::get_offset_of_U3CProfilePatternU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7253 = { sizeof (TypeAnalyzer_tB7AFD0F4DC5654ACE781C43487CF20A9C24CC53C), -1, sizeof(TypeAnalyzer_tB7AFD0F4DC5654ACE781C43487CF20A9C24CC53C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7253[1] = 
{
	TypeAnalyzer_tB7AFD0F4DC5654ACE781C43487CF20A9C24CC53C_StaticFields::get_offset_of__typeInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7254 = { sizeof (U3CU3Ec__DisplayClass4_0_t0FF8638353C928286144EDDB3D04D241505EAC9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7254[1] = 
{
	U3CU3Ec__DisplayClass4_0_t0FF8638353C928286144EDDB3D04D241505EAC9D::get_offset_of_parentType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7255 = { sizeof (U3CU3Ec__DisplayClass6_0_t2AE3616A9A9207F9862BE92BDAB0A6E163BFC733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7255[3] = 
{
	U3CU3Ec__DisplayClass6_0_t2AE3616A9A9207F9862BE92BDAB0A6E163BFC733::get_offset_of_heirarchyList_0(),
	U3CU3Ec__DisplayClass6_0_t2AE3616A9A9207F9862BE92BDAB0A6E163BFC733::get_offset_of_type_1(),
	U3CU3Ec__DisplayClass6_0_t2AE3616A9A9207F9862BE92BDAB0A6E163BFC733::get_offset_of_U3CU3E9__2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7256 = { sizeof (U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722), -1, sizeof(U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7256[7] = 
{
	U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
	U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields::get_offset_of_U3CU3E9__7_0_2(),
	U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields::get_offset_of_U3CU3E9__8_0_3(),
	U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields::get_offset_of_U3CU3E9__10_1_4(),
	U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields::get_offset_of_U3CU3E9__12_0_5(),
	U3CU3Ec_t7FFB988753203548A29B999197FB5969E6D0D722_StaticFields::get_offset_of_U3CU3E9__12_1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7257 = { sizeof (U3CGetPropertyInjectablesU3Ed__7_tA371626F15F0D0108FF26FB6F4EF8139B5BC657D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7257[6] = 
{
	U3CGetPropertyInjectablesU3Ed__7_tA371626F15F0D0108FF26FB6F4EF8139B5BC657D::get_offset_of_U3CU3E1__state_0(),
	U3CGetPropertyInjectablesU3Ed__7_tA371626F15F0D0108FF26FB6F4EF8139B5BC657D::get_offset_of_U3CU3E2__current_1(),
	U3CGetPropertyInjectablesU3Ed__7_tA371626F15F0D0108FF26FB6F4EF8139B5BC657D::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetPropertyInjectablesU3Ed__7_tA371626F15F0D0108FF26FB6F4EF8139B5BC657D::get_offset_of_type_3(),
	U3CGetPropertyInjectablesU3Ed__7_tA371626F15F0D0108FF26FB6F4EF8139B5BC657D::get_offset_of_U3CU3E3__type_4(),
	U3CGetPropertyInjectablesU3Ed__7_tA371626F15F0D0108FF26FB6F4EF8139B5BC657D::get_offset_of_U3CU3E7__wrap1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7258 = { sizeof (U3CGetFieldInjectablesU3Ed__8_tC012AA2E4291280EC44964CCC63BAB12483FB205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7258[6] = 
{
	U3CGetFieldInjectablesU3Ed__8_tC012AA2E4291280EC44964CCC63BAB12483FB205::get_offset_of_U3CU3E1__state_0(),
	U3CGetFieldInjectablesU3Ed__8_tC012AA2E4291280EC44964CCC63BAB12483FB205::get_offset_of_U3CU3E2__current_1(),
	U3CGetFieldInjectablesU3Ed__8_tC012AA2E4291280EC44964CCC63BAB12483FB205::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetFieldInjectablesU3Ed__8_tC012AA2E4291280EC44964CCC63BAB12483FB205::get_offset_of_type_3(),
	U3CGetFieldInjectablesU3Ed__8_tC012AA2E4291280EC44964CCC63BAB12483FB205::get_offset_of_U3CU3E3__type_4(),
	U3CGetFieldInjectablesU3Ed__8_tC012AA2E4291280EC44964CCC63BAB12483FB205::get_offset_of_U3CU3E7__wrap1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7259 = { sizeof (U3CU3Ec__DisplayClass10_0_t07DC0D4538DA5CE4F9C4E223D7F8C9F4EFF82321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7259[2] = 
{
	U3CU3Ec__DisplayClass10_0_t07DC0D4538DA5CE4F9C4E223D7F8C9F4EFF82321::get_offset_of_propertyName_0(),
	U3CU3Ec__DisplayClass10_0_t07DC0D4538DA5CE4F9C4E223D7F8C9F4EFF82321::get_offset_of_writeableFields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7260 = { sizeof (U3CU3Ec__DisplayClass10_1_t8C31ABD7C912DD5FB2FEBB29F63632DABF71A623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7260[2] = 
{
	U3CU3Ec__DisplayClass10_1_t8C31ABD7C912DD5FB2FEBB29F63632DABF71A623::get_offset_of_injectable_0(),
	U3CU3Ec__DisplayClass10_1_t8C31ABD7C912DD5FB2FEBB29F63632DABF71A623::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7261 = { sizeof (U3CU3Ec__DisplayClass11_0_t9035D85BB63F234BC043F613C74CEA738B56F444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7261[1] = 
{
	U3CU3Ec__DisplayClass11_0_t9035D85BB63F234BC043F613C74CEA738B56F444::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7262 = { sizeof (U3CU3Ec__DisplayClass11_1_tFF9724DE534BEF23F7F0C5A367776B65E8EAEF6C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7262[1] = 
{
	U3CU3Ec__DisplayClass11_1_tFF9724DE534BEF23F7F0C5A367776B65E8EAEF6C::get_offset_of_propInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7263 = { sizeof (ValidationUtil_t4DC76246AB32FBE6FA3A05DF467E405CD671E2D4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7264 = { sizeof (U3CU3Ec_t44386611299F7A5044D2E232230F8D62EE14300F), -1, sizeof(U3CU3Ec_t44386611299F7A5044D2E232230F8D62EE14300F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7264[2] = 
{
	U3CU3Ec_t44386611299F7A5044D2E232230F8D62EE14300F_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t44386611299F7A5044D2E232230F8D62EE14300F_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7265 = { sizeof (ZenjectException_tF4348A9294229C0016AA92037BF88C84E71B3F79), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7266 = { sizeof (LoadSceneRelationship_t93F81BF78DC9003120628D3DD26E4586B1BDDA0B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7266[4] = 
{
	LoadSceneRelationship_t93F81BF78DC9003120628D3DD26E4586B1BDDA0B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7267 = { sizeof (ZenjectSceneLoader_tC1B08B820353C3517187298745318FB9BAD3062D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7267[2] = 
{
	ZenjectSceneLoader_tC1B08B820353C3517187298745318FB9BAD3062D::get_offset_of__projectKernel_0(),
	ZenjectSceneLoader_tC1B08B820353C3517187298745318FB9BAD3062D::get_offset_of__sceneContainer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7268 = { sizeof (PostInjectableInfo_tEF74AB1901215366F3CA2D03A895CEA79DD0CEBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7268[2] = 
{
	PostInjectableInfo_tEF74AB1901215366F3CA2D03A895CEA79DD0CEBC::get_offset_of__methodInfo_0(),
	PostInjectableInfo_tEF74AB1901215366F3CA2D03A895CEA79DD0CEBC::get_offset_of__injectableInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7269 = { sizeof (ZenjectTypeInfo_tAED38CE1BA075C821D761EF4C05FFC687D6C95D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7269[6] = 
{
	ZenjectTypeInfo_tAED38CE1BA075C821D761EF4C05FFC687D6C95D7::get_offset_of__postInjectMethods_0(),
	ZenjectTypeInfo_tAED38CE1BA075C821D761EF4C05FFC687D6C95D7::get_offset_of__constructorInjectables_1(),
	ZenjectTypeInfo_tAED38CE1BA075C821D761EF4C05FFC687D6C95D7::get_offset_of__fieldInjectables_2(),
	ZenjectTypeInfo_tAED38CE1BA075C821D761EF4C05FFC687D6C95D7::get_offset_of__propertyInjectables_3(),
	ZenjectTypeInfo_tAED38CE1BA075C821D761EF4C05FFC687D6C95D7::get_offset_of__injectConstructor_4(),
	ZenjectTypeInfo_tAED38CE1BA075C821D761EF4C05FFC687D6C95D7::get_offset_of__typeAnalyzed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7270 = { sizeof (U3CU3Ec_t09EAB8D4CF740522DE76DF2B1DA86E15BAFD829B), -1, sizeof(U3CU3Ec_t09EAB8D4CF740522DE76DF2B1DA86E15BAFD829B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7270[2] = 
{
	U3CU3Ec_t09EAB8D4CF740522DE76DF2B1DA86E15BAFD829B_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t09EAB8D4CF740522DE76DF2B1DA86E15BAFD829B_StaticFields::get_offset_of_U3CU3E9__12_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7271 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7272 = { sizeof (ValidationMarker_t6FB7733E5ABF3FED47A9D0F3D7881E76376A622C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7272[1] = 
{
	ValidationMarker_t6FB7733E5ABF3FED47A9D0F3D7881E76376A622C::get_offset_of_U3CMarkedTypeU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7273 = { sizeof (ZenUtilInternal_tDCB564D327DDB97F765E7D8466D507E68BD800EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7274 = { sizeof (U3CU3Ec_tEE1674805D45C1CC74A6FAD2B2A74BCF02363D03), -1, sizeof(U3CU3Ec_tEE1674805D45C1CC74A6FAD2B2A74BCF02363D03_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7274[3] = 
{
	U3CU3Ec_tEE1674805D45C1CC74A6FAD2B2A74BCF02363D03_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tEE1674805D45C1CC74A6FAD2B2A74BCF02363D03_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
	U3CU3Ec_tEE1674805D45C1CC74A6FAD2B2A74BCF02363D03_StaticFields::get_offset_of_U3CU3E9__7_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7275 = { sizeof (U3CGetAllSceneContextsU3Ed__3_t456733539FADBC242179F1A9000716AE4B453721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7275[4] = 
{
	U3CGetAllSceneContextsU3Ed__3_t456733539FADBC242179F1A9000716AE4B453721::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllSceneContextsU3Ed__3_t456733539FADBC242179F1A9000716AE4B453721::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllSceneContextsU3Ed__3_t456733539FADBC242179F1A9000716AE4B453721::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAllSceneContextsU3Ed__3_t456733539FADBC242179F1A9000716AE4B453721::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7276 = { sizeof (U3CU3Ec__DisplayClass7_0_t46D0A971F49129FC26FF8136DA57C7B85197ADEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7276[1] = 
{
	U3CU3Ec__DisplayClass7_0_t46D0A971F49129FC26FF8136DA57C7B85197ADEA::get_offset_of_scene_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7277 = { sizeof (U3CPrivateImplementationDetailsU3E_t1462DEA80E3C0F6D30E594F33C4F5283DF75F28F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7278 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7279 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7279[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7280 = { sizeof (U3CU3Ef__AnonymousType1_t7BEBF5F43ACE43421537B4C061E9217BAE29151F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7281 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7281[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7282 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7282[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7283 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7283[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7284 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7284[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7285 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7285[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7286 = { sizeof (SoundEffectPlayer_tE1463ABA00A81FD6189B2D6EF1556AF4B7164765), -1, sizeof(SoundEffectPlayer_tE1463ABA00A81FD6189B2D6EF1556AF4B7164765_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7286[3] = 
{
	SoundEffectPlayer_tE1463ABA00A81FD6189B2D6EF1556AF4B7164765::get_offset_of_ScanConfirmed_4(),
	SoundEffectPlayer_tE1463ABA00A81FD6189B2D6EF1556AF4B7164765::get_offset_of_audioSource_5(),
	SoundEffectPlayer_tE1463ABA00A81FD6189B2D6EF1556AF4B7164765_StaticFields::get_offset_of_instance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7287 = { sizeof (IPManager_t152C9A4281FBE031475645C78F9FA88CA6BF6765), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7288 = { sizeof (ApiKeyInput_t4C9C8B4657469B23E91FA9DCA9A803DF8BDC52CE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7288[1] = 
{
	ApiKeyInput_t4C9C8B4657469B23E91FA9DCA9A803DF8BDC52CE::get_offset_of_input_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7289 = { sizeof (APIKeyLoader_t53B4E51AD81E5342BDB911863D5A45C801A63BBD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7289[2] = 
{
	APIKeyLoader_t53B4E51AD81E5342BDB911863D5A45C801A63BBD::get_offset_of_inputField_4(),
	APIKeyLoader_t53B4E51AD81E5342BDB911863D5A45C801A63BBD::get_offset_of_apiKey_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7290 = { sizeof (PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7290[5] = 
{
	PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26::get_offset_of_U3CCurrentScriptU3Ek__BackingField_5(),
	PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26::get_offset_of_U3CSettingsU3Ek__BackingField_6(),
	PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26::get_offset_of_U3CDebuggingUserIdU3Ek__BackingField_7(),
	PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26::get_offset_of_U3CDebuggingMixIdU3Ek__BackingField_8(),
	PersistentSettings_t2DE52C036DBB553D990D48539ED2D394EA3F5A26::get_offset_of_U3CGameSceneNameU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7291 = { sizeof (PlatformSetup_t314B48FE4C6ED5B5F237D7FB984325512C04D941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7291[2] = 
{
	PlatformSetup_t314B48FE4C6ED5B5F237D7FB984325512C04D941::get_offset_of_StartMenu_4(),
	PlatformSetup_t314B48FE4C6ED5B5F237D7FB984325512C04D941::get_offset_of_GameMenu_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7292 = { sizeof (LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7292[10] = 
{
	LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB::get_offset_of_maxBlurLevel_4(),
	LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB::get_offset_of_currentBlurLevel_5(),
	LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB::get_offset_of_blurTransitionTime_6(),
	LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB::get_offset_of_indicator_7(),
	LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB::get_offset_of_indicatorTextures_8(),
	LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB::get_offset_of_secondsPerFrame_9(),
	LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB::get_offset_of_lastFrameChange_10(),
	LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB::get_offset_of_textureIndex_11(),
	LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB::get_offset_of_showing_12(),
	LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB::get_offset_of_objectsToDisableWhileLoading_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7293 = { sizeof (U3CShowCoroutineU3Ed__14_t729AE09528028F2932139F148F8E55265C8B4362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7293[5] = 
{
	U3CShowCoroutineU3Ed__14_t729AE09528028F2932139F148F8E55265C8B4362::get_offset_of_U3CU3E1__state_0(),
	U3CShowCoroutineU3Ed__14_t729AE09528028F2932139F148F8E55265C8B4362::get_offset_of_U3CU3E2__current_1(),
	U3CShowCoroutineU3Ed__14_t729AE09528028F2932139F148F8E55265C8B4362::get_offset_of_U3CU3E4__this_2(),
	U3CShowCoroutineU3Ed__14_t729AE09528028F2932139F148F8E55265C8B4362::get_offset_of_U3CblurLevelU3E5__2_3(),
	U3CShowCoroutineU3Ed__14_t729AE09528028F2932139F148F8E55265C8B4362::get_offset_of_U3CstartTimeU3E5__3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7294 = { sizeof (U3CHideCoroutineU3Ed__15_tAB671F3B963EF2DA43B161FF19BB55DB76177E48), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7294[3] = 
{
	U3CHideCoroutineU3Ed__15_tAB671F3B963EF2DA43B161FF19BB55DB76177E48::get_offset_of_U3CU3E1__state_0(),
	U3CHideCoroutineU3Ed__15_tAB671F3B963EF2DA43B161FF19BB55DB76177E48::get_offset_of_U3CU3E2__current_1(),
	U3CHideCoroutineU3Ed__15_tAB671F3B963EF2DA43B161FF19BB55DB76177E48::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7295 = { sizeof (ImageSizeToParent_t29A2F6AEE68C4DB339A52AF8438AA9238E16D3B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7295[3] = 
{
	ImageSizeToParent_t29A2F6AEE68C4DB339A52AF8438AA9238E16D3B5::get_offset_of_screenWidth_4(),
	ImageSizeToParent_t29A2F6AEE68C4DB339A52AF8438AA9238E16D3B5::get_offset_of_screenHeight_5(),
	ImageSizeToParent_t29A2F6AEE68C4DB339A52AF8438AA9238E16D3B5::get_offset_of_image_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7296 = { sizeof (IsMouseOver_t8E136C664156CA3C198053A1DA0DB8BF23C2D5EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7296[1] = 
{
	IsMouseOver_t8E136C664156CA3C198053A1DA0DB8BF23C2D5EA::get_offset_of_isOver_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7297 = { sizeof (UriHelper_tB7DB0E6AFBD8133A6AC20D4ED04BFAC35DCC1098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7298 = { sizeof (U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239), -1, sizeof(U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7298[6] = 
{
	U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
	U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_StaticFields::get_offset_of_U3CU3E9__0_1_2(),
	U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_StaticFields::get_offset_of_U3CU3E9__0_2_3(),
	U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_StaticFields::get_offset_of_U3CU3E9__0_3_4(),
	U3CU3Ec_t4C31C22F35C5523AC81D5E36A3EBFF10ECD99239_StaticFields::get_offset_of_U3CU3E9__0_4_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7299 = { sizeof (DesktopCursor_t318E148B8A08EFF28B82007A4B40E19DA47ABF66), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7299[2] = 
{
	DesktopCursor_t318E148B8A08EFF28B82007A4B40E19DA47ABF66::get_offset_of_startDistance_4(),
	DesktopCursor_t318E148B8A08EFF28B82007A4B40E19DA47ABF66::get_offset_of_startRotation_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
