﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ARWorldMapController
struct ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737;
// AssetBundles.AssetBundleLoadAssetOperation
struct AssetBundleLoadAssetOperation_t5A1401110A453E5BC738720B9A4E596235815463;
// Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem
struct ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488;
// Assets._Project.Scripts.Common.UI.ScriptTile
struct ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E;
// BackMenuSlider
struct BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9;
// BarcodeScanner.Parser.ZXingParser
struct ZXingParser_t7B9AC8160C3A1565FA96F8714D30B9C7E852DE26;
// BrowseScriptsMenu
struct BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD;
// BrowseScriptsMenu/<>c__DisplayClass17_0
struct U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598;
// DesktopLog
struct DesktopLog_t42A4E8BFEBF79EE1FC717B1EFDB6DE299EE9807F;
// LightEstimation
struct LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319;
// LoadAssets
struct LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50;
// LoadScenes
struct LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0;
// LoadTanks
struct LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B;
// LoadVariants
struct LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7;
// LoadingIndicator
struct LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB;
// Localizatron
struct Localizatron_tC7AF71F3CADDA057CB4068F6260C5B90A047962E;
// MPAR.Common.AssetLoader.IPlatformDependentAssetLoader
struct IPlatformDependentAssetLoader_t4191A469DA1A9ECC9F8F2F7E37DAF82A3B2DBB2E;
// MPAR.Common.Persistence.ISaveLoadManager
struct ISaveLoadManager_tFCCBBA0EEB7591B57A7AA74ED9EF18A0073086B5;
// MPAR.Common.Scripting.Script
struct Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC;
// MPAR.Common.UI.IPlatformDependentScriptMenu
struct IPlatformDependentScriptMenu_tBBB6BBD89701461F2C659808C67378235B3D1AA0;
// MPAR.Common.UI.PortableMenu.PortableMenuItem
struct PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867;
// MPAR.Common.UI.PortableMenu.PortableMenuItem[]
struct PortableMenuItemU5BU5D_t96F76D1F6EC232B8A030659418EF7D74A9C0AF44;
// MenuItemOptions
struct MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7;
// RecentScriptsMenu
struct RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088;
// RecentScriptsMenu/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_tD85AB27D7A3268945ED7995DA996DFC51CC2953F;
// ScanView
struct ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<MPAR.Common.Scripting.Script>
struct Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E;
// System.Action`1<System.Collections.Generic.List`1<MPAR.Common.Scripting.Script>>
struct Action_1_t50C72EBE10EB02C3DAEB93F043716129AD3C9E5B;
// System.Action`1<UnityThreading.ActionThread>
struct Action_1_tE3217F0296A761BE816CF81030F2F771090C1B77;
// System.Collections.Generic.Dictionary`2<System.String,System.Action>
struct Dictionary_2_t8B6634B584453750D056334B3141E31F88DD5F5C;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<UnityEngine.XR.ARKit.XRArkitBlendShapeLocation,System.Int32>
struct Dictionary_2_t056393A25D6B313BA194EBC2FD3230D220301357;
// System.Collections.Generic.List`1<Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem>
struct List_1_t5DFB316E3A16E508385B16D18E565D7775541A8C;
// System.Collections.Generic.List`1<Assets._Project.Scripts.Common.UI.ScriptTile>
struct List_1_t738D3CE973A389B13AA52C796419D7B95FB33EC2;
// System.Collections.Generic.List`1<MPAR.Common.Scripting.Script>
struct List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t2E429D48492C9F1ED16C7D74224A8AAB590A7B32;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<System.Threading.Tasks.Task>
struct List_1_tC62C1E1B0AD84992F0A374A5A4679609955E117E;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>
struct List_1_tCD714E0010765A4D65BD80974698E4FA9EFBC2B6;
// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit>
struct List_1_tEB129E1534E65DF322C5B5AAECF0CA8A562063F6;
// System.Collections.Generic.List`1<UnityEngine.XR.ARKit.XRFaceArkitBlendShapeCoefficient>
struct List_1_t797F7EE5A5B8DA5CE89E3AE41EE2BC966BDBCF67;
// System.Collections.Generic.List`1<UnityThreading.ThreadBase>
struct List_1_tC440ECADB0E49325F80F6D99AD7AD37A932F7219;
// System.EventHandler`1<MPAR.Common.UI.EventActionArgs>
struct EventHandler_1_t28058BB22451F0F583F3F63C15CBF591BCD18E50;
// System.Func`1<System.Collections.IEnumerator>
struct Func_1_tB87849BEE3BBD29FEB301AB876F67ADA1BFEC3AB;
// System.Func`2<Assets._Project.Scripts.Common.UI.ScriptTile,System.Boolean>
struct Func_2_tEEDE8C33795CA1866609DD038582736DFFFE338C;
// System.Func`2<UnityEngine.RectTransform,System.Boolean>
struct Func_2_tDC0ACDEB2BCC36F845B1DCEE9E818B13B4C16975;
// System.Func`2<UnityEngine.UI.Button,System.Boolean>
struct Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD;
// System.Func`2<UnityThreading.ThreadBase,System.Boolean>
struct Func_2_t22351DE6F3F46A96FDCA9ADDA9030B7EAC289A80;
// System.Func`5<UnityEngine.Texture2D,System.Byte[],System.Int32,System.Int32,System.Boolean>
struct Func_5_tF569143B5BBB2D7031D2DD25254C0EB863B22668;
// System.IO.BinaryReader
struct BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969;
// System.Predicate`1<MPAR.Common.Scripting.Script>
struct Predicate_1_tDD7825CF62B31E860538632BC9CFF86169581294;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.Tasks.Task
struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.Experimental.XR.XRSessionSubsystem
struct XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Light
struct Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_tF94B8797BE321CF3ED1C3E6426CE13AFC8F1D478;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Dropdown
struct Dropdown_tF6331401084B1213CAB10587A6EC81461501930F;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83;
// UnityEngine.UI.GridLayoutGroup
struct GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// UnityEngine.UI.ScrollRect
struct ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.XR.ARFoundation.ARFace
struct ARFace_t6743D3C0C57B5B559B335F7EB6523211A4EA579F;
// UnityEngine.XR.ARFoundation.ARPlane
struct ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E;
// UnityEngine.XR.ARFoundation.ARPlaneManager
struct ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A;
// UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer
struct ARPlaneMeshVisualizer_t1CAC012DACAA7D8D30B94247B8762D71D2624923;
// UnityEngine.XR.ARFoundation.ARSession
struct ARSession_tFD6F1BD76D4C003B8141D9B6255B904D8C5036AB;
// UnityEngine.XR.ARFoundation.ARSessionOrigin
struct ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF;
// UnityEngine.XR.ARKit.ARKitFaceSubsystem
struct ARKitFaceSubsystem_t24E01CF6C590EFAC9B51F7B1D341C646A69E9CD9;
// UnityThreading.Dispatcher
struct Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297;
// UnityThreading.TaskDistributor
struct TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D;
// VRCustomMenu
struct VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CLOADU3ED__28_T5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D_H
#define U3CLOADU3ED__28_T5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARWorldMapController/<Load>d__28
struct  U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D  : public RuntimeObject
{
public:
	// System.Int32 ARWorldMapController/<Load>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ARWorldMapController/<Load>d__28::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ARWorldMapController ARWorldMapController/<Load>d__28::<>4__this
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * ___U3CU3E4__this_2;
	// UnityEngine.Experimental.XR.XRSessionSubsystem ARWorldMapController/<Load>d__28::<sessionSubsystem>5__2
	XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F * ___U3CsessionSubsystemU3E5__2_3;
	// System.Int32 ARWorldMapController/<Load>d__28::<bytesPerFrame>5__3
	int32_t ___U3CbytesPerFrameU3E5__3_4;
	// System.Int64 ARWorldMapController/<Load>d__28::<bytesRemaining>5__4
	int64_t ___U3CbytesRemainingU3E5__4_5;
	// System.IO.BinaryReader ARWorldMapController/<Load>d__28::<binaryReader>5__5
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * ___U3CbinaryReaderU3E5__5_6;
	// System.Collections.Generic.List`1<System.Byte> ARWorldMapController/<Load>d__28::<allBytes>5__6
	List_1_t2E429D48492C9F1ED16C7D74224A8AAB590A7B32 * ___U3CallBytesU3E5__6_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CU3E4__this_2)); }
	inline ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CsessionSubsystemU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CsessionSubsystemU3E5__2_3)); }
	inline XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F * get_U3CsessionSubsystemU3E5__2_3() const { return ___U3CsessionSubsystemU3E5__2_3; }
	inline XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F ** get_address_of_U3CsessionSubsystemU3E5__2_3() { return &___U3CsessionSubsystemU3E5__2_3; }
	inline void set_U3CsessionSubsystemU3E5__2_3(XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F * value)
	{
		___U3CsessionSubsystemU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsessionSubsystemU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CbytesPerFrameU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CbytesPerFrameU3E5__3_4)); }
	inline int32_t get_U3CbytesPerFrameU3E5__3_4() const { return ___U3CbytesPerFrameU3E5__3_4; }
	inline int32_t* get_address_of_U3CbytesPerFrameU3E5__3_4() { return &___U3CbytesPerFrameU3E5__3_4; }
	inline void set_U3CbytesPerFrameU3E5__3_4(int32_t value)
	{
		___U3CbytesPerFrameU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CbytesRemainingU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CbytesRemainingU3E5__4_5)); }
	inline int64_t get_U3CbytesRemainingU3E5__4_5() const { return ___U3CbytesRemainingU3E5__4_5; }
	inline int64_t* get_address_of_U3CbytesRemainingU3E5__4_5() { return &___U3CbytesRemainingU3E5__4_5; }
	inline void set_U3CbytesRemainingU3E5__4_5(int64_t value)
	{
		___U3CbytesRemainingU3E5__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CbinaryReaderU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CbinaryReaderU3E5__5_6)); }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * get_U3CbinaryReaderU3E5__5_6() const { return ___U3CbinaryReaderU3E5__5_6; }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 ** get_address_of_U3CbinaryReaderU3E5__5_6() { return &___U3CbinaryReaderU3E5__5_6; }
	inline void set_U3CbinaryReaderU3E5__5_6(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * value)
	{
		___U3CbinaryReaderU3E5__5_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbinaryReaderU3E5__5_6), value);
	}

	inline static int32_t get_offset_of_U3CallBytesU3E5__6_7() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CallBytesU3E5__6_7)); }
	inline List_1_t2E429D48492C9F1ED16C7D74224A8AAB590A7B32 * get_U3CallBytesU3E5__6_7() const { return ___U3CallBytesU3E5__6_7; }
	inline List_1_t2E429D48492C9F1ED16C7D74224A8AAB590A7B32 ** get_address_of_U3CallBytesU3E5__6_7() { return &___U3CallBytesU3E5__6_7; }
	inline void set_U3CallBytesU3E5__6_7(List_1_t2E429D48492C9F1ED16C7D74224A8AAB590A7B32 * value)
	{
		___U3CallBytesU3E5__6_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallBytesU3E5__6_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3ED__28_T5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D_H
#ifndef U3CU3EC_T249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_H
#define U3CU3EC_T249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackMenuSlider/<>c
struct  U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields
{
public:
	// BackMenuSlider/<>c BackMenuSlider/<>c::<>9
	U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.UI.Button,System.Boolean> BackMenuSlider/<>c::<>9__37_1
	Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * ___U3CU3E9__37_1_1;
	// System.Func`2<UnityEngine.UI.Button,System.Boolean> BackMenuSlider/<>c::<>9__38_1
	Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * ___U3CU3E9__38_1_2;
	// System.Func`2<UnityEngine.UI.Button,System.Boolean> BackMenuSlider/<>c::<>9__39_1
	Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * ___U3CU3E9__39_1_3;
	// System.Func`2<UnityEngine.UI.Button,System.Boolean> BackMenuSlider/<>c::<>9__40_1
	Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * ___U3CU3E9__40_1_4;
	// System.Func`2<UnityEngine.UI.Button,System.Boolean> BackMenuSlider/<>c::<>9__41_3
	Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * ___U3CU3E9__41_3_5;
	// System.Func`2<UnityEngine.RectTransform,System.Boolean> BackMenuSlider/<>c::<>9__41_0
	Func_2_tDC0ACDEB2BCC36F845B1DCEE9E818B13B4C16975 * ___U3CU3E9__41_0_6;
	// System.Func`2<UnityEngine.UI.Button,System.Boolean> BackMenuSlider/<>c::<>9__41_1
	Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * ___U3CU3E9__41_1_7;
	// System.Func`2<UnityEngine.RectTransform,System.Boolean> BackMenuSlider/<>c::<>9__44_0
	Func_2_tDC0ACDEB2BCC36F845B1DCEE9E818B13B4C16975 * ___U3CU3E9__44_0_8;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields, ___U3CU3E9__37_1_1)); }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * get_U3CU3E9__37_1_1() const { return ___U3CU3E9__37_1_1; }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD ** get_address_of_U3CU3E9__37_1_1() { return &___U3CU3E9__37_1_1; }
	inline void set_U3CU3E9__37_1_1(Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * value)
	{
		___U3CU3E9__37_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__37_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__38_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields, ___U3CU3E9__38_1_2)); }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * get_U3CU3E9__38_1_2() const { return ___U3CU3E9__38_1_2; }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD ** get_address_of_U3CU3E9__38_1_2() { return &___U3CU3E9__38_1_2; }
	inline void set_U3CU3E9__38_1_2(Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * value)
	{
		___U3CU3E9__38_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__38_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__39_1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields, ___U3CU3E9__39_1_3)); }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * get_U3CU3E9__39_1_3() const { return ___U3CU3E9__39_1_3; }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD ** get_address_of_U3CU3E9__39_1_3() { return &___U3CU3E9__39_1_3; }
	inline void set_U3CU3E9__39_1_3(Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * value)
	{
		___U3CU3E9__39_1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__39_1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__40_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields, ___U3CU3E9__40_1_4)); }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * get_U3CU3E9__40_1_4() const { return ___U3CU3E9__40_1_4; }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD ** get_address_of_U3CU3E9__40_1_4() { return &___U3CU3E9__40_1_4; }
	inline void set_U3CU3E9__40_1_4(Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * value)
	{
		___U3CU3E9__40_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__40_1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_3_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields, ___U3CU3E9__41_3_5)); }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * get_U3CU3E9__41_3_5() const { return ___U3CU3E9__41_3_5; }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD ** get_address_of_U3CU3E9__41_3_5() { return &___U3CU3E9__41_3_5; }
	inline void set_U3CU3E9__41_3_5(Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * value)
	{
		___U3CU3E9__41_3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_3_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields, ___U3CU3E9__41_0_6)); }
	inline Func_2_tDC0ACDEB2BCC36F845B1DCEE9E818B13B4C16975 * get_U3CU3E9__41_0_6() const { return ___U3CU3E9__41_0_6; }
	inline Func_2_tDC0ACDEB2BCC36F845B1DCEE9E818B13B4C16975 ** get_address_of_U3CU3E9__41_0_6() { return &___U3CU3E9__41_0_6; }
	inline void set_U3CU3E9__41_0_6(Func_2_tDC0ACDEB2BCC36F845B1DCEE9E818B13B4C16975 * value)
	{
		___U3CU3E9__41_0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_1_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields, ___U3CU3E9__41_1_7)); }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * get_U3CU3E9__41_1_7() const { return ___U3CU3E9__41_1_7; }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD ** get_address_of_U3CU3E9__41_1_7() { return &___U3CU3E9__41_1_7; }
	inline void set_U3CU3E9__41_1_7(Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * value)
	{
		___U3CU3E9__41_1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__44_0_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields, ___U3CU3E9__44_0_8)); }
	inline Func_2_tDC0ACDEB2BCC36F845B1DCEE9E818B13B4C16975 * get_U3CU3E9__44_0_8() const { return ___U3CU3E9__44_0_8; }
	inline Func_2_tDC0ACDEB2BCC36F845B1DCEE9E818B13B4C16975 ** get_address_of_U3CU3E9__44_0_8() { return &___U3CU3E9__44_0_8; }
	inline void set_U3CU3E9__44_0_8(Func_2_tDC0ACDEB2BCC36F845B1DCEE9E818B13B4C16975 * value)
	{
		___U3CU3E9__44_0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__44_0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_H
#ifndef U3CU3EC__DISPLAYCLASS33_0_TFA1B99BDA082C3BD150F1E190E8931338ABB8EF9_H
#define U3CU3EC__DISPLAYCLASS33_0_TFA1B99BDA082C3BD150F1E190E8931338ABB8EF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackMenuSlider/<>c__DisplayClass33_0
struct  U3CU3Ec__DisplayClass33_0_tFA1B99BDA082C3BD150F1E190E8931338ABB8EF9  : public RuntimeObject
{
public:
	// System.String BackMenuSlider/<>c__DisplayClass33_0::key
	String_t* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_tFA1B99BDA082C3BD150F1E190E8931338ABB8EF9, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_0_TFA1B99BDA082C3BD150F1E190E8931338ABB8EF9_H
#ifndef U3CU3EC__DISPLAYCLASS34_0_T718E570EE15155B9937389A67A3E3BCB293C5CBA_H
#define U3CU3EC__DISPLAYCLASS34_0_T718E570EE15155B9937389A67A3E3BCB293C5CBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackMenuSlider/<>c__DisplayClass34_0
struct  U3CU3Ec__DisplayClass34_0_t718E570EE15155B9937389A67A3E3BCB293C5CBA  : public RuntimeObject
{
public:
	// Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem BackMenuSlider/<>c__DisplayClass34_0::item
	ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * ___item_0;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t718E570EE15155B9937389A67A3E3BCB293C5CBA, ___item_0)); }
	inline ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * get_item_0() const { return ___item_0; }
	inline ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((&___item_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS34_0_T718E570EE15155B9937389A67A3E3BCB293C5CBA_H
#ifndef U3CU3EC__DISPLAYCLASS37_0_T74AE714D46F0F97F0A01D23DFB12DFDEE2DF1BE7_H
#define U3CU3EC__DISPLAYCLASS37_0_T74AE714D46F0F97F0A01D23DFB12DFDEE2DF1BE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackMenuSlider/<>c__DisplayClass37_0
struct  U3CU3Ec__DisplayClass37_0_t74AE714D46F0F97F0A01D23DFB12DFDEE2DF1BE7  : public RuntimeObject
{
public:
	// System.String BackMenuSlider/<>c__DisplayClass37_0::key
	String_t* ___key_0;
	// UnityEngine.UI.Button BackMenuSlider/<>c__DisplayClass37_0::button
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___button_1;
	// System.Func`2<UnityEngine.UI.Button,System.Boolean> BackMenuSlider/<>c__DisplayClass37_0::<>9__3
	Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * ___U3CU3E9__3_2;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t74AE714D46F0F97F0A01D23DFB12DFDEE2DF1BE7, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_button_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t74AE714D46F0F97F0A01D23DFB12DFDEE2DF1BE7, ___button_1)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_button_1() const { return ___button_1; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_button_1() { return &___button_1; }
	inline void set_button_1(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___button_1 = value;
		Il2CppCodeGenWriteBarrier((&___button_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t74AE714D46F0F97F0A01D23DFB12DFDEE2DF1BE7, ___U3CU3E9__3_2)); }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * get_U3CU3E9__3_2() const { return ___U3CU3E9__3_2; }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD ** get_address_of_U3CU3E9__3_2() { return &___U3CU3E9__3_2; }
	inline void set_U3CU3E9__3_2(Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * value)
	{
		___U3CU3E9__3_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS37_0_T74AE714D46F0F97F0A01D23DFB12DFDEE2DF1BE7_H
#ifndef U3CU3EC__DISPLAYCLASS38_0_T68897C520D027BF355A20D69379A4DB049077100_H
#define U3CU3EC__DISPLAYCLASS38_0_T68897C520D027BF355A20D69379A4DB049077100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackMenuSlider/<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_t68897C520D027BF355A20D69379A4DB049077100  : public RuntimeObject
{
public:
	// System.String BackMenuSlider/<>c__DisplayClass38_0::key
	String_t* ___key_0;
	// System.Func`2<UnityEngine.UI.Button,System.Boolean> BackMenuSlider/<>c__DisplayClass38_0::<>9__2
	Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * ___U3CU3E9__2_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t68897C520D027BF355A20D69379A4DB049077100, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t68897C520D027BF355A20D69379A4DB049077100, ___U3CU3E9__2_1)); }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * get_U3CU3E9__2_1() const { return ___U3CU3E9__2_1; }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD ** get_address_of_U3CU3E9__2_1() { return &___U3CU3E9__2_1; }
	inline void set_U3CU3E9__2_1(Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * value)
	{
		___U3CU3E9__2_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_0_T68897C520D027BF355A20D69379A4DB049077100_H
#ifndef U3CU3EC__DISPLAYCLASS39_0_T8068FA9014A2C118159256A57442CC56463FCDF6_H
#define U3CU3EC__DISPLAYCLASS39_0_T8068FA9014A2C118159256A57442CC56463FCDF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackMenuSlider/<>c__DisplayClass39_0
struct  U3CU3Ec__DisplayClass39_0_t8068FA9014A2C118159256A57442CC56463FCDF6  : public RuntimeObject
{
public:
	// System.String BackMenuSlider/<>c__DisplayClass39_0::key
	String_t* ___key_0;
	// System.Func`2<UnityEngine.UI.Button,System.Boolean> BackMenuSlider/<>c__DisplayClass39_0::<>9__2
	Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * ___U3CU3E9__2_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t8068FA9014A2C118159256A57442CC56463FCDF6, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t8068FA9014A2C118159256A57442CC56463FCDF6, ___U3CU3E9__2_1)); }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * get_U3CU3E9__2_1() const { return ___U3CU3E9__2_1; }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD ** get_address_of_U3CU3E9__2_1() { return &___U3CU3E9__2_1; }
	inline void set_U3CU3E9__2_1(Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * value)
	{
		___U3CU3E9__2_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS39_0_T8068FA9014A2C118159256A57442CC56463FCDF6_H
#ifndef U3CU3EC__DISPLAYCLASS40_0_T7737C4E49EBC29B40C9F85C5712EE20D3133F5AD_H
#define U3CU3EC__DISPLAYCLASS40_0_T7737C4E49EBC29B40C9F85C5712EE20D3133F5AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackMenuSlider/<>c__DisplayClass40_0
struct  U3CU3Ec__DisplayClass40_0_t7737C4E49EBC29B40C9F85C5712EE20D3133F5AD  : public RuntimeObject
{
public:
	// System.String BackMenuSlider/<>c__DisplayClass40_0::key
	String_t* ___key_0;
	// System.Func`2<UnityEngine.UI.Button,System.Boolean> BackMenuSlider/<>c__DisplayClass40_0::<>9__2
	Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * ___U3CU3E9__2_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t7737C4E49EBC29B40C9F85C5712EE20D3133F5AD, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t7737C4E49EBC29B40C9F85C5712EE20D3133F5AD, ___U3CU3E9__2_1)); }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * get_U3CU3E9__2_1() const { return ___U3CU3E9__2_1; }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD ** get_address_of_U3CU3E9__2_1() { return &___U3CU3E9__2_1; }
	inline void set_U3CU3E9__2_1(Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * value)
	{
		___U3CU3E9__2_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS40_0_T7737C4E49EBC29B40C9F85C5712EE20D3133F5AD_H
#ifndef U3CU3EC__DISPLAYCLASS41_0_T04425736791E5133862CFB5808B3E9FBA7739423_H
#define U3CU3EC__DISPLAYCLASS41_0_T04425736791E5133862CFB5808B3E9FBA7739423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackMenuSlider/<>c__DisplayClass41_0
struct  U3CU3Ec__DisplayClass41_0_t04425736791E5133862CFB5808B3E9FBA7739423  : public RuntimeObject
{
public:
	// UnityEngine.UI.Button BackMenuSlider/<>c__DisplayClass41_0::buttonToSet
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___buttonToSet_0;

public:
	inline static int32_t get_offset_of_buttonToSet_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_0_t04425736791E5133862CFB5808B3E9FBA7739423, ___buttonToSet_0)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_buttonToSet_0() const { return ___buttonToSet_0; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_buttonToSet_0() { return &___buttonToSet_0; }
	inline void set_buttonToSet_0(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___buttonToSet_0 = value;
		Il2CppCodeGenWriteBarrier((&___buttonToSet_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS41_0_T04425736791E5133862CFB5808B3E9FBA7739423_H
#ifndef U3CU3EC__DISPLAYCLASS43_0_T6A13AB9296D5EB88EC6CDDC8EC4BB77AA6F79C5F_H
#define U3CU3EC__DISPLAYCLASS43_0_T6A13AB9296D5EB88EC6CDDC8EC4BB77AA6F79C5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackMenuSlider/<>c__DisplayClass43_0
struct  U3CU3Ec__DisplayClass43_0_t6A13AB9296D5EB88EC6CDDC8EC4BB77AA6F79C5F  : public RuntimeObject
{
public:
	// Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem BackMenuSlider/<>c__DisplayClass43_0::item
	ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * ___item_0;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_0_t6A13AB9296D5EB88EC6CDDC8EC4BB77AA6F79C5F, ___item_0)); }
	inline ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * get_item_0() const { return ___item_0; }
	inline ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((&___item_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS43_0_T6A13AB9296D5EB88EC6CDDC8EC4BB77AA6F79C5F_H
#ifndef U3CU3EC__DISPLAYCLASS43_1_T3C9556A3E583A6C6C63E1F8F59A53EDED1F4EC8D_H
#define U3CU3EC__DISPLAYCLASS43_1_T3C9556A3E583A6C6C63E1F8F59A53EDED1F4EC8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackMenuSlider/<>c__DisplayClass43_1
struct  U3CU3Ec__DisplayClass43_1_t3C9556A3E583A6C6C63E1F8F59A53EDED1F4EC8D  : public RuntimeObject
{
public:
	// Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem BackMenuSlider/<>c__DisplayClass43_1::item
	ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * ___item_0;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_1_t3C9556A3E583A6C6C63E1F8F59A53EDED1F4EC8D, ___item_0)); }
	inline ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * get_item_0() const { return ___item_0; }
	inline ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((&___item_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS43_1_T3C9556A3E583A6C6C63E1F8F59A53EDED1F4EC8D_H
#ifndef U3CANIMATEU3ED__48_T7B0CB3089BE454E34C9A4DB414B4DC1A2C41B9A2_H
#define U3CANIMATEU3ED__48_T7B0CB3089BE454E34C9A4DB414B4DC1A2C41B9A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackMenuSlider/<Animate>d__48
struct  U3CAnimateU3Ed__48_t7B0CB3089BE454E34C9A4DB414B4DC1A2C41B9A2  : public RuntimeObject
{
public:
	// System.Int32 BackMenuSlider/<Animate>d__48::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BackMenuSlider/<Animate>d__48::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BackMenuSlider BackMenuSlider/<Animate>d__48::<>4__this
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9 * ___U3CU3E4__this_2;
	// System.Single BackMenuSlider/<Animate>d__48::<startTime>5__2
	float ___U3CstartTimeU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimateU3Ed__48_t7B0CB3089BE454E34C9A4DB414B4DC1A2C41B9A2, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimateU3Ed__48_t7B0CB3089BE454E34C9A4DB414B4DC1A2C41B9A2, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAnimateU3Ed__48_t7B0CB3089BE454E34C9A4DB414B4DC1A2C41B9A2, ___U3CU3E4__this_2)); }
	inline BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CAnimateU3Ed__48_t7B0CB3089BE454E34C9A4DB414B4DC1A2C41B9A2, ___U3CstartTimeU3E5__2_3)); }
	inline float get_U3CstartTimeU3E5__2_3() const { return ___U3CstartTimeU3E5__2_3; }
	inline float* get_address_of_U3CstartTimeU3E5__2_3() { return &___U3CstartTimeU3E5__2_3; }
	inline void set_U3CstartTimeU3E5__2_3(float value)
	{
		___U3CstartTimeU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEU3ED__48_T7B0CB3089BE454E34C9A4DB414B4DC1A2C41B9A2_H
#ifndef U3CU3EC_T17EA2A72F88EC2DDA1A4E8E6A813A11662761B9E_H
#define U3CU3EC_T17EA2A72F88EC2DDA1A4E8E6A813A11662761B9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackToMenuPanel/<>c
struct  U3CU3Ec_t17EA2A72F88EC2DDA1A4E8E6A813A11662761B9E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t17EA2A72F88EC2DDA1A4E8E6A813A11662761B9E_StaticFields
{
public:
	// BackToMenuPanel/<>c BackToMenuPanel/<>c::<>9
	U3CU3Ec_t17EA2A72F88EC2DDA1A4E8E6A813A11662761B9E * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.UI.Button,System.Boolean> BackToMenuPanel/<>c::<>9__1_0
	Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t17EA2A72F88EC2DDA1A4E8E6A813A11662761B9E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t17EA2A72F88EC2DDA1A4E8E6A813A11662761B9E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t17EA2A72F88EC2DDA1A4E8E6A813A11662761B9E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t17EA2A72F88EC2DDA1A4E8E6A813A11662761B9E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t17EA2A72F88EC2DDA1A4E8E6A813A11662761B9E_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_t0CECCCDEBB40A731FC016B66B1C857D00D76EECD * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T17EA2A72F88EC2DDA1A4E8E6A813A11662761B9E_H
#ifndef U3CU3EC_T098AE564161BD7FBAC97DEACF32DED3699C91E0B_H
#define U3CU3EC_T098AE564161BD7FBAC97DEACF32DED3699C91E0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BrowseScriptsMenu/<>c
struct  U3CU3Ec_t098AE564161BD7FBAC97DEACF32DED3699C91E0B  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t098AE564161BD7FBAC97DEACF32DED3699C91E0B_StaticFields
{
public:
	// BrowseScriptsMenu/<>c BrowseScriptsMenu/<>c::<>9
	U3CU3Ec_t098AE564161BD7FBAC97DEACF32DED3699C91E0B * ___U3CU3E9_0;
	// System.Predicate`1<MPAR.Common.Scripting.Script> BrowseScriptsMenu/<>c::<>9__18_0
	Predicate_1_tDD7825CF62B31E860538632BC9CFF86169581294 * ___U3CU3E9__18_0_1;
	// System.Func`2<Assets._Project.Scripts.Common.UI.ScriptTile,System.Boolean> BrowseScriptsMenu/<>c::<>9__21_1
	Func_2_tEEDE8C33795CA1866609DD038582736DFFFE338C * ___U3CU3E9__21_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t098AE564161BD7FBAC97DEACF32DED3699C91E0B_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t098AE564161BD7FBAC97DEACF32DED3699C91E0B * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t098AE564161BD7FBAC97DEACF32DED3699C91E0B ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t098AE564161BD7FBAC97DEACF32DED3699C91E0B * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t098AE564161BD7FBAC97DEACF32DED3699C91E0B_StaticFields, ___U3CU3E9__18_0_1)); }
	inline Predicate_1_tDD7825CF62B31E860538632BC9CFF86169581294 * get_U3CU3E9__18_0_1() const { return ___U3CU3E9__18_0_1; }
	inline Predicate_1_tDD7825CF62B31E860538632BC9CFF86169581294 ** get_address_of_U3CU3E9__18_0_1() { return &___U3CU3E9__18_0_1; }
	inline void set_U3CU3E9__18_0_1(Predicate_1_tDD7825CF62B31E860538632BC9CFF86169581294 * value)
	{
		___U3CU3E9__18_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__21_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t098AE564161BD7FBAC97DEACF32DED3699C91E0B_StaticFields, ___U3CU3E9__21_1_2)); }
	inline Func_2_tEEDE8C33795CA1866609DD038582736DFFFE338C * get_U3CU3E9__21_1_2() const { return ___U3CU3E9__21_1_2; }
	inline Func_2_tEEDE8C33795CA1866609DD038582736DFFFE338C ** get_address_of_U3CU3E9__21_1_2() { return &___U3CU3E9__21_1_2; }
	inline void set_U3CU3E9__21_1_2(Func_2_tEEDE8C33795CA1866609DD038582736DFFFE338C * value)
	{
		___U3CU3E9__21_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__21_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T098AE564161BD7FBAC97DEACF32DED3699C91E0B_H
#ifndef U3CU3EC__DISPLAYCLASS17_0_TFAE55B0BA31450782DFF4F15F90C829778656598_H
#define U3CU3EC__DISPLAYCLASS17_0_TFAE55B0BA31450782DFF4F15F90C829778656598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BrowseScriptsMenu/<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598  : public RuntimeObject
{
public:
	// BrowseScriptsMenu BrowseScriptsMenu/<>c__DisplayClass17_0::<>4__this
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD * ___U3CU3E4__this_0;
	// System.Int32 BrowseScriptsMenu/<>c__DisplayClass17_0::skip
	int32_t ___skip_1;
	// System.Boolean BrowseScriptsMenu/<>c__DisplayClass17_0::breakEarly
	bool ___breakEarly_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598, ___U3CU3E4__this_0)); }
	inline BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_skip_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598, ___skip_1)); }
	inline int32_t get_skip_1() const { return ___skip_1; }
	inline int32_t* get_address_of_skip_1() { return &___skip_1; }
	inline void set_skip_1(int32_t value)
	{
		___skip_1 = value;
	}

	inline static int32_t get_offset_of_breakEarly_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598, ___breakEarly_2)); }
	inline bool get_breakEarly_2() const { return ___breakEarly_2; }
	inline bool* get_address_of_breakEarly_2() { return &___breakEarly_2; }
	inline void set_breakEarly_2(bool value)
	{
		___breakEarly_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_0_TFAE55B0BA31450782DFF4F15F90C829778656598_H
#ifndef U3CU3EC__DISPLAYCLASS17_1_TCCA3C8755235DAA64D3BE1F4F900464680FE475F_H
#define U3CU3EC__DISPLAYCLASS17_1_TCCA3C8755235DAA64D3BE1F4F900464680FE475F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BrowseScriptsMenu/<>c__DisplayClass17_1
struct  U3CU3Ec__DisplayClass17_1_tCCA3C8755235DAA64D3BE1F4F900464680FE475F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Threading.Tasks.Task> BrowseScriptsMenu/<>c__DisplayClass17_1::waitForThumbnailTasks
	List_1_tC62C1E1B0AD84992F0A374A5A4679609955E117E * ___waitForThumbnailTasks_0;
	// BrowseScriptsMenu/<>c__DisplayClass17_0 BrowseScriptsMenu/<>c__DisplayClass17_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_waitForThumbnailTasks_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_1_tCCA3C8755235DAA64D3BE1F4F900464680FE475F, ___waitForThumbnailTasks_0)); }
	inline List_1_tC62C1E1B0AD84992F0A374A5A4679609955E117E * get_waitForThumbnailTasks_0() const { return ___waitForThumbnailTasks_0; }
	inline List_1_tC62C1E1B0AD84992F0A374A5A4679609955E117E ** get_address_of_waitForThumbnailTasks_0() { return &___waitForThumbnailTasks_0; }
	inline void set_waitForThumbnailTasks_0(List_1_tC62C1E1B0AD84992F0A374A5A4679609955E117E * value)
	{
		___waitForThumbnailTasks_0 = value;
		Il2CppCodeGenWriteBarrier((&___waitForThumbnailTasks_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_1_tCCA3C8755235DAA64D3BE1F4F900464680FE475F, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_1_TCCA3C8755235DAA64D3BE1F4F900464680FE475F_H
#ifndef U3CU3EC__DISPLAYCLASS17_2_T45915D3DE18E0CBC3B8BCB3A59081D559F393FC6_H
#define U3CU3EC__DISPLAYCLASS17_2_T45915D3DE18E0CBC3B8BCB3A59081D559F393FC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BrowseScriptsMenu/<>c__DisplayClass17_2
struct  U3CU3Ec__DisplayClass17_2_t45915D3DE18E0CBC3B8BCB3A59081D559F393FC6  : public RuntimeObject
{
public:
	// MPAR.Common.Scripting.Script BrowseScriptsMenu/<>c__DisplayClass17_2::script
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___script_0;

public:
	inline static int32_t get_offset_of_script_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_2_t45915D3DE18E0CBC3B8BCB3A59081D559F393FC6, ___script_0)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_script_0() const { return ___script_0; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_script_0() { return &___script_0; }
	inline void set_script_0(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___script_0 = value;
		Il2CppCodeGenWriteBarrier((&___script_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_2_T45915D3DE18E0CBC3B8BCB3A59081D559F393FC6_H
#ifndef U3CU3EC__DISPLAYCLASS17_3_T6CC15054C276022A254C677732BF5C8C5EA3EEF1_H
#define U3CU3EC__DISPLAYCLASS17_3_T6CC15054C276022A254C677732BF5C8C5EA3EEF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BrowseScriptsMenu/<>c__DisplayClass17_3
struct  U3CU3Ec__DisplayClass17_3_t6CC15054C276022A254C677732BF5C8C5EA3EEF1  : public RuntimeObject
{
public:
	// MPAR.Common.Scripting.Script BrowseScriptsMenu/<>c__DisplayClass17_3::script
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___script_0;

public:
	inline static int32_t get_offset_of_script_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_3_t6CC15054C276022A254C677732BF5C8C5EA3EEF1, ___script_0)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_script_0() const { return ___script_0; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_script_0() { return &___script_0; }
	inline void set_script_0(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___script_0 = value;
		Il2CppCodeGenWriteBarrier((&___script_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_3_T6CC15054C276022A254C677732BF5C8C5EA3EEF1_H
#ifndef U3CU3EC__DISPLAYCLASS17_4_TF0A30A825F69C90957707E7215AC751AC6791100_H
#define U3CU3EC__DISPLAYCLASS17_4_TF0A30A825F69C90957707E7215AC751AC6791100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BrowseScriptsMenu/<>c__DisplayClass17_4
struct  U3CU3Ec__DisplayClass17_4_tF0A30A825F69C90957707E7215AC751AC6791100  : public RuntimeObject
{
public:
	// MPAR.Common.Scripting.Script BrowseScriptsMenu/<>c__DisplayClass17_4::scr
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___scr_0;

public:
	inline static int32_t get_offset_of_scr_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_4_tF0A30A825F69C90957707E7215AC751AC6791100, ___scr_0)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_scr_0() const { return ___scr_0; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_scr_0() { return &___scr_0; }
	inline void set_scr_0(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___scr_0 = value;
		Il2CppCodeGenWriteBarrier((&___scr_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_4_TF0A30A825F69C90957707E7215AC751AC6791100_H
#ifndef U3CU3EC__DISPLAYCLASS19_0_T28AA9715A035BA5DBE5DC8319B4C02B730C733D7_H
#define U3CU3EC__DISPLAYCLASS19_0_T28AA9715A035BA5DBE5DC8319B4C02B730C733D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BrowseScriptsMenu/<>c__DisplayClass19_0
struct  U3CU3Ec__DisplayClass19_0_t28AA9715A035BA5DBE5DC8319B4C02B730C733D7  : public RuntimeObject
{
public:
	// MPAR.Common.Scripting.Script BrowseScriptsMenu/<>c__DisplayClass19_0::script
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___script_0;

public:
	inline static int32_t get_offset_of_script_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t28AA9715A035BA5DBE5DC8319B4C02B730C733D7, ___script_0)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_script_0() const { return ___script_0; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_script_0() { return &___script_0; }
	inline void set_script_0(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___script_0 = value;
		Il2CppCodeGenWriteBarrier((&___script_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS19_0_T28AA9715A035BA5DBE5DC8319B4C02B730C733D7_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_T109044F89A93A26ACF496600B3533C7FB8620BF0_H
#define U3CU3EC__DISPLAYCLASS21_0_T109044F89A93A26ACF496600B3533C7FB8620BF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BrowseScriptsMenu/<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_t109044F89A93A26ACF496600B3533C7FB8620BF0  : public RuntimeObject
{
public:
	// System.Single BrowseScriptsMenu/<>c__DisplayClass21_0::firstObjectPosition
	float ___firstObjectPosition_0;

public:
	inline static int32_t get_offset_of_firstObjectPosition_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t109044F89A93A26ACF496600B3533C7FB8620BF0, ___firstObjectPosition_0)); }
	inline float get_firstObjectPosition_0() const { return ___firstObjectPosition_0; }
	inline float* get_address_of_firstObjectPosition_0() { return &___firstObjectPosition_0; }
	inline void set_firstObjectPosition_0(float value)
	{
		___firstObjectPosition_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_T109044F89A93A26ACF496600B3533C7FB8620BF0_H
#ifndef U3CLOADSCRIPTSU3ED__17_T2373122EDF3CADBD2A658248B39F907F0D4D39AB_H
#define U3CLOADSCRIPTSU3ED__17_T2373122EDF3CADBD2A658248B39F907F0D4D39AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BrowseScriptsMenu/<LoadScripts>d__17
struct  U3CLoadScriptsU3Ed__17_t2373122EDF3CADBD2A658248B39F907F0D4D39AB  : public RuntimeObject
{
public:
	// System.Int32 BrowseScriptsMenu/<LoadScripts>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BrowseScriptsMenu/<LoadScripts>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BrowseScriptsMenu BrowseScriptsMenu/<LoadScripts>d__17::<>4__this
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD * ___U3CU3E4__this_2;
	// BrowseScriptsMenu/<>c__DisplayClass17_0 BrowseScriptsMenu/<LoadScripts>d__17::<>8__1
	U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598 * ___U3CU3E8__1_3;
	// System.Int32 BrowseScriptsMenu/<LoadScripts>d__17::numScriptsToLoad
	int32_t ___numScriptsToLoad_4;
	// System.Boolean BrowseScriptsMenu/<LoadScripts>d__17::scrollToTopAfter
	bool ___scrollToTopAfter_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadScriptsU3Ed__17_t2373122EDF3CADBD2A658248B39F907F0D4D39AB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadScriptsU3Ed__17_t2373122EDF3CADBD2A658248B39F907F0D4D39AB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadScriptsU3Ed__17_t2373122EDF3CADBD2A658248B39F907F0D4D39AB, ___U3CU3E4__this_2)); }
	inline BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_3() { return static_cast<int32_t>(offsetof(U3CLoadScriptsU3Ed__17_t2373122EDF3CADBD2A658248B39F907F0D4D39AB, ___U3CU3E8__1_3)); }
	inline U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598 * get_U3CU3E8__1_3() const { return ___U3CU3E8__1_3; }
	inline U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598 ** get_address_of_U3CU3E8__1_3() { return &___U3CU3E8__1_3; }
	inline void set_U3CU3E8__1_3(U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598 * value)
	{
		___U3CU3E8__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_3), value);
	}

	inline static int32_t get_offset_of_numScriptsToLoad_4() { return static_cast<int32_t>(offsetof(U3CLoadScriptsU3Ed__17_t2373122EDF3CADBD2A658248B39F907F0D4D39AB, ___numScriptsToLoad_4)); }
	inline int32_t get_numScriptsToLoad_4() const { return ___numScriptsToLoad_4; }
	inline int32_t* get_address_of_numScriptsToLoad_4() { return &___numScriptsToLoad_4; }
	inline void set_numScriptsToLoad_4(int32_t value)
	{
		___numScriptsToLoad_4 = value;
	}

	inline static int32_t get_offset_of_scrollToTopAfter_5() { return static_cast<int32_t>(offsetof(U3CLoadScriptsU3Ed__17_t2373122EDF3CADBD2A658248B39F907F0D4D39AB, ___scrollToTopAfter_5)); }
	inline bool get_scrollToTopAfter_5() const { return ___scrollToTopAfter_5; }
	inline bool* get_address_of_scrollToTopAfter_5() { return &___scrollToTopAfter_5; }
	inline void set_scrollToTopAfter_5(bool value)
	{
		___scrollToTopAfter_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCRIPTSU3ED__17_T2373122EDF3CADBD2A658248B39F907F0D4D39AB_H
#ifndef U3CSTARTU3ED__14_T8B255A94D0499030A00EFAAB80F62CC0D3761C8B_H
#define U3CSTARTU3ED__14_T8B255A94D0499030A00EFAAB80F62CC0D3761C8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BrowseScriptsMenu/<Start>d__14
struct  U3CStartU3Ed__14_t8B255A94D0499030A00EFAAB80F62CC0D3761C8B  : public RuntimeObject
{
public:
	// System.Int32 BrowseScriptsMenu/<Start>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BrowseScriptsMenu/<Start>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BrowseScriptsMenu BrowseScriptsMenu/<Start>d__14::<>4__this
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__14_t8B255A94D0499030A00EFAAB80F62CC0D3761C8B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__14_t8B255A94D0499030A00EFAAB80F62CC0D3761C8B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__14_t8B255A94D0499030A00EFAAB80F62CC0D3761C8B, ___U3CU3E4__this_2)); }
	inline BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__14_T8B255A94D0499030A00EFAAB80F62CC0D3761C8B_H
#ifndef U3CSHAREPHOTOU3ED__2_T24631812439B83CC9EA8BD2F6A6676D7244A36F6_H
#define U3CSHAREPHOTOU3ED__2_T24631812439B83CC9EA8BD2F6A6676D7244A36F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraButton/<SharePhoto>d__2
struct  U3CSharePhotoU3Ed__2_t24631812439B83CC9EA8BD2F6A6676D7244A36F6  : public RuntimeObject
{
public:
	// System.Int32 CameraButton/<SharePhoto>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CameraButton/<SharePhoto>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSharePhotoU3Ed__2_t24631812439B83CC9EA8BD2F6A6676D7244A36F6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSharePhotoU3Ed__2_t24631812439B83CC9EA8BD2F6A6676D7244A36F6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHAREPHOTOU3ED__2_T24631812439B83CC9EA8BD2F6A6676D7244A36F6_H
#ifndef IENUMERABLEEXTENSION_TB0E9A4F82BB6FDA85587C01ACE217018F2FAC253_H
#define IENUMERABLEEXTENSION_TB0E9A4F82BB6FDA85587C01ACE217018F2FAC253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IEnumerableExtension
struct  IEnumerableExtension_tB0E9A4F82BB6FDA85587C01ACE217018F2FAC253  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IENUMERABLEEXTENSION_TB0E9A4F82BB6FDA85587C01ACE217018F2FAC253_H
#ifndef U3CINITIALIZEU3ED__4_T31F872E342422C3745F7768E9630CF38B7236CC4_H
#define U3CINITIALIZEU3ED__4_T31F872E342422C3745F7768E9630CF38B7236CC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadAssets/<Initialize>d__4
struct  U3CInitializeU3Ed__4_t31F872E342422C3745F7768E9630CF38B7236CC4  : public RuntimeObject
{
public:
	// System.Int32 LoadAssets/<Initialize>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadAssets/<Initialize>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoadAssets LoadAssets/<Initialize>d__4::<>4__this
	LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__4_t31F872E342422C3745F7768E9630CF38B7236CC4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__4_t31F872E342422C3745F7768E9630CF38B7236CC4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__4_t31F872E342422C3745F7768E9630CF38B7236CC4, ___U3CU3E4__this_2)); }
	inline LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZEU3ED__4_T31F872E342422C3745F7768E9630CF38B7236CC4_H
#ifndef U3CINSTANTIATEGAMEOBJECTASYNCU3ED__5_TD561F032451E154661DA400E17C227BD1859CAEA_H
#define U3CINSTANTIATEGAMEOBJECTASYNCU3ED__5_TD561F032451E154661DA400E17C227BD1859CAEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadAssets/<InstantiateGameObjectAsync>d__5
struct  U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA  : public RuntimeObject
{
public:
	// System.Int32 LoadAssets/<InstantiateGameObjectAsync>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadAssets/<InstantiateGameObjectAsync>d__5::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String LoadAssets/<InstantiateGameObjectAsync>d__5::assetBundleName
	String_t* ___assetBundleName_2;
	// System.String LoadAssets/<InstantiateGameObjectAsync>d__5::assetName
	String_t* ___assetName_3;
	// LoadAssets LoadAssets/<InstantiateGameObjectAsync>d__5::<>4__this
	LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50 * ___U3CU3E4__this_4;
	// System.Single LoadAssets/<InstantiateGameObjectAsync>d__5::<startTime>5__2
	float ___U3CstartTimeU3E5__2_5;
	// AssetBundles.AssetBundleLoadAssetOperation LoadAssets/<InstantiateGameObjectAsync>d__5::<request>5__3
	AssetBundleLoadAssetOperation_t5A1401110A453E5BC738720B9A4E596235815463 * ___U3CrequestU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_assetBundleName_2() { return static_cast<int32_t>(offsetof(U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA, ___assetBundleName_2)); }
	inline String_t* get_assetBundleName_2() const { return ___assetBundleName_2; }
	inline String_t** get_address_of_assetBundleName_2() { return &___assetBundleName_2; }
	inline void set_assetBundleName_2(String_t* value)
	{
		___assetBundleName_2 = value;
		Il2CppCodeGenWriteBarrier((&___assetBundleName_2), value);
	}

	inline static int32_t get_offset_of_assetName_3() { return static_cast<int32_t>(offsetof(U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA, ___assetName_3)); }
	inline String_t* get_assetName_3() const { return ___assetName_3; }
	inline String_t** get_address_of_assetName_3() { return &___assetName_3; }
	inline void set_assetName_3(String_t* value)
	{
		___assetName_3 = value;
		Il2CppCodeGenWriteBarrier((&___assetName_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA, ___U3CU3E4__this_4)); }
	inline LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA, ___U3CstartTimeU3E5__2_5)); }
	inline float get_U3CstartTimeU3E5__2_5() const { return ___U3CstartTimeU3E5__2_5; }
	inline float* get_address_of_U3CstartTimeU3E5__2_5() { return &___U3CstartTimeU3E5__2_5; }
	inline void set_U3CstartTimeU3E5__2_5(float value)
	{
		___U3CstartTimeU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA, ___U3CrequestU3E5__3_6)); }
	inline AssetBundleLoadAssetOperation_t5A1401110A453E5BC738720B9A4E596235815463 * get_U3CrequestU3E5__3_6() const { return ___U3CrequestU3E5__3_6; }
	inline AssetBundleLoadAssetOperation_t5A1401110A453E5BC738720B9A4E596235815463 ** get_address_of_U3CrequestU3E5__3_6() { return &___U3CrequestU3E5__3_6; }
	inline void set_U3CrequestU3E5__3_6(AssetBundleLoadAssetOperation_t5A1401110A453E5BC738720B9A4E596235815463 * value)
	{
		___U3CrequestU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E5__3_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINSTANTIATEGAMEOBJECTASYNCU3ED__5_TD561F032451E154661DA400E17C227BD1859CAEA_H
#ifndef U3CSTARTU3ED__3_T41684ADC3C5CE830B47B959B085FFCB39DED114B_H
#define U3CSTARTU3ED__3_T41684ADC3C5CE830B47B959B085FFCB39DED114B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadAssets/<Start>d__3
struct  U3CStartU3Ed__3_t41684ADC3C5CE830B47B959B085FFCB39DED114B  : public RuntimeObject
{
public:
	// System.Int32 LoadAssets/<Start>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadAssets/<Start>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoadAssets LoadAssets/<Start>d__3::<>4__this
	LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__3_t41684ADC3C5CE830B47B959B085FFCB39DED114B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__3_t41684ADC3C5CE830B47B959B085FFCB39DED114B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__3_t41684ADC3C5CE830B47B959B085FFCB39DED114B, ___U3CU3E4__this_2)); }
	inline LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__3_T41684ADC3C5CE830B47B959B085FFCB39DED114B_H
#ifndef U3CINITIALIZEU3ED__3_T3651A4182A021EAA3049CFA47EC561606E05CECE_H
#define U3CINITIALIZEU3ED__3_T3651A4182A021EAA3049CFA47EC561606E05CECE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadScenes/<Initialize>d__3
struct  U3CInitializeU3Ed__3_t3651A4182A021EAA3049CFA47EC561606E05CECE  : public RuntimeObject
{
public:
	// System.Int32 LoadScenes/<Initialize>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadScenes/<Initialize>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoadScenes LoadScenes/<Initialize>d__3::<>4__this
	LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t3651A4182A021EAA3049CFA47EC561606E05CECE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t3651A4182A021EAA3049CFA47EC561606E05CECE, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t3651A4182A021EAA3049CFA47EC561606E05CECE, ___U3CU3E4__this_2)); }
	inline LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZEU3ED__3_T3651A4182A021EAA3049CFA47EC561606E05CECE_H
#ifndef U3CINITIALIZELEVELASYNCU3ED__4_T3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E_H
#define U3CINITIALIZELEVELASYNCU3ED__4_T3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadScenes/<InitializeLevelAsync>d__4
struct  U3CInitializeLevelAsyncU3Ed__4_t3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E  : public RuntimeObject
{
public:
	// System.Int32 LoadScenes/<InitializeLevelAsync>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadScenes/<InitializeLevelAsync>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoadScenes LoadScenes/<InitializeLevelAsync>d__4::<>4__this
	LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0 * ___U3CU3E4__this_2;
	// System.String LoadScenes/<InitializeLevelAsync>d__4::levelName
	String_t* ___levelName_3;
	// System.Boolean LoadScenes/<InitializeLevelAsync>d__4::isAdditive
	bool ___isAdditive_4;
	// System.Single LoadScenes/<InitializeLevelAsync>d__4::<startTime>5__2
	float ___U3CstartTimeU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__4_t3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__4_t3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__4_t3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E, ___U3CU3E4__this_2)); }
	inline LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_levelName_3() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__4_t3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E, ___levelName_3)); }
	inline String_t* get_levelName_3() const { return ___levelName_3; }
	inline String_t** get_address_of_levelName_3() { return &___levelName_3; }
	inline void set_levelName_3(String_t* value)
	{
		___levelName_3 = value;
		Il2CppCodeGenWriteBarrier((&___levelName_3), value);
	}

	inline static int32_t get_offset_of_isAdditive_4() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__4_t3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E, ___isAdditive_4)); }
	inline bool get_isAdditive_4() const { return ___isAdditive_4; }
	inline bool* get_address_of_isAdditive_4() { return &___isAdditive_4; }
	inline void set_isAdditive_4(bool value)
	{
		___isAdditive_4 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__4_t3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E, ___U3CstartTimeU3E5__2_5)); }
	inline float get_U3CstartTimeU3E5__2_5() const { return ___U3CstartTimeU3E5__2_5; }
	inline float* get_address_of_U3CstartTimeU3E5__2_5() { return &___U3CstartTimeU3E5__2_5; }
	inline void set_U3CstartTimeU3E5__2_5(float value)
	{
		___U3CstartTimeU3E5__2_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZELEVELASYNCU3ED__4_T3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E_H
#ifndef U3CSTARTU3ED__2_TEDE4D4A44571214E27D0B44FCEF4761D389CE597_H
#define U3CSTARTU3ED__2_TEDE4D4A44571214E27D0B44FCEF4761D389CE597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadScenes/<Start>d__2
struct  U3CStartU3Ed__2_tEDE4D4A44571214E27D0B44FCEF4761D389CE597  : public RuntimeObject
{
public:
	// System.Int32 LoadScenes/<Start>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadScenes/<Start>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoadScenes LoadScenes/<Start>d__2::<>4__this
	LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__2_tEDE4D4A44571214E27D0B44FCEF4761D389CE597, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__2_tEDE4D4A44571214E27D0B44FCEF4761D389CE597, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__2_tEDE4D4A44571214E27D0B44FCEF4761D389CE597, ___U3CU3E4__this_2)); }
	inline LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__2_TEDE4D4A44571214E27D0B44FCEF4761D389CE597_H
#ifndef U3CBEGINEXAMPLEU3ED__17_T97D6A873FBC50C13120EA144FE4465BAA4493CA5_H
#define U3CBEGINEXAMPLEU3ED__17_T97D6A873FBC50C13120EA144FE4465BAA4493CA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadTanks/<BeginExample>d__17
struct  U3CBeginExampleU3Ed__17_t97D6A873FBC50C13120EA144FE4465BAA4493CA5  : public RuntimeObject
{
public:
	// System.Int32 LoadTanks/<BeginExample>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadTanks/<BeginExample>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoadTanks LoadTanks/<BeginExample>d__17::<>4__this
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CBeginExampleU3Ed__17_t97D6A873FBC50C13120EA144FE4465BAA4493CA5, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CBeginExampleU3Ed__17_t97D6A873FBC50C13120EA144FE4465BAA4493CA5, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CBeginExampleU3Ed__17_t97D6A873FBC50C13120EA144FE4465BAA4493CA5, ___U3CU3E4__this_2)); }
	inline LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBEGINEXAMPLEU3ED__17_T97D6A873FBC50C13120EA144FE4465BAA4493CA5_H
#ifndef U3CINITIALIZEU3ED__18_T814E51B4BB3128CADC5D61333A49108A43FEF8B4_H
#define U3CINITIALIZEU3ED__18_T814E51B4BB3128CADC5D61333A49108A43FEF8B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadTanks/<Initialize>d__18
struct  U3CInitializeU3Ed__18_t814E51B4BB3128CADC5D61333A49108A43FEF8B4  : public RuntimeObject
{
public:
	// System.Int32 LoadTanks/<Initialize>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadTanks/<Initialize>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoadTanks LoadTanks/<Initialize>d__18::<>4__this
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__18_t814E51B4BB3128CADC5D61333A49108A43FEF8B4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__18_t814E51B4BB3128CADC5D61333A49108A43FEF8B4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__18_t814E51B4BB3128CADC5D61333A49108A43FEF8B4, ___U3CU3E4__this_2)); }
	inline LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZEU3ED__18_T814E51B4BB3128CADC5D61333A49108A43FEF8B4_H
#ifndef U3CINITIALIZELEVELASYNCU3ED__19_TDD83F9C503DBF78435F94DBE9DFA1E8F5B675069_H
#define U3CINITIALIZELEVELASYNCU3ED__19_TDD83F9C503DBF78435F94DBE9DFA1E8F5B675069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadTanks/<InitializeLevelAsync>d__19
struct  U3CInitializeLevelAsyncU3Ed__19_tDD83F9C503DBF78435F94DBE9DFA1E8F5B675069  : public RuntimeObject
{
public:
	// System.Int32 LoadTanks/<InitializeLevelAsync>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadTanks/<InitializeLevelAsync>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoadTanks LoadTanks/<InitializeLevelAsync>d__19::<>4__this
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B * ___U3CU3E4__this_2;
	// System.String LoadTanks/<InitializeLevelAsync>d__19::levelName
	String_t* ___levelName_3;
	// System.Boolean LoadTanks/<InitializeLevelAsync>d__19::isAdditive
	bool ___isAdditive_4;
	// System.Single LoadTanks/<InitializeLevelAsync>d__19::<startTime>5__2
	float ___U3CstartTimeU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__19_tDD83F9C503DBF78435F94DBE9DFA1E8F5B675069, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__19_tDD83F9C503DBF78435F94DBE9DFA1E8F5B675069, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__19_tDD83F9C503DBF78435F94DBE9DFA1E8F5B675069, ___U3CU3E4__this_2)); }
	inline LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_levelName_3() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__19_tDD83F9C503DBF78435F94DBE9DFA1E8F5B675069, ___levelName_3)); }
	inline String_t* get_levelName_3() const { return ___levelName_3; }
	inline String_t** get_address_of_levelName_3() { return &___levelName_3; }
	inline void set_levelName_3(String_t* value)
	{
		___levelName_3 = value;
		Il2CppCodeGenWriteBarrier((&___levelName_3), value);
	}

	inline static int32_t get_offset_of_isAdditive_4() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__19_tDD83F9C503DBF78435F94DBE9DFA1E8F5B675069, ___isAdditive_4)); }
	inline bool get_isAdditive_4() const { return ___isAdditive_4; }
	inline bool* get_address_of_isAdditive_4() { return &___isAdditive_4; }
	inline void set_isAdditive_4(bool value)
	{
		___isAdditive_4 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__19_tDD83F9C503DBF78435F94DBE9DFA1E8F5B675069, ___U3CstartTimeU3E5__2_5)); }
	inline float get_U3CstartTimeU3E5__2_5() const { return ___U3CstartTimeU3E5__2_5; }
	inline float* get_address_of_U3CstartTimeU3E5__2_5() { return &___U3CstartTimeU3E5__2_5; }
	inline void set_U3CstartTimeU3E5__2_5(float value)
	{
		___U3CstartTimeU3E5__2_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZELEVELASYNCU3ED__19_TDD83F9C503DBF78435F94DBE9DFA1E8F5B675069_H
#ifndef U3CINSTANTIATEGAMEOBJECTASYNCU3ED__20_T6256914044263C7A2C7E5E1C4AFA4D6530DDFD15_H
#define U3CINSTANTIATEGAMEOBJECTASYNCU3ED__20_T6256914044263C7A2C7E5E1C4AFA4D6530DDFD15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadTanks/<InstantiateGameObjectAsync>d__20
struct  U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15  : public RuntimeObject
{
public:
	// System.Int32 LoadTanks/<InstantiateGameObjectAsync>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadTanks/<InstantiateGameObjectAsync>d__20::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String LoadTanks/<InstantiateGameObjectAsync>d__20::assetBundleName
	String_t* ___assetBundleName_2;
	// System.String LoadTanks/<InstantiateGameObjectAsync>d__20::assetName
	String_t* ___assetName_3;
	// LoadTanks LoadTanks/<InstantiateGameObjectAsync>d__20::<>4__this
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B * ___U3CU3E4__this_4;
	// System.Single LoadTanks/<InstantiateGameObjectAsync>d__20::<startTime>5__2
	float ___U3CstartTimeU3E5__2_5;
	// AssetBundles.AssetBundleLoadAssetOperation LoadTanks/<InstantiateGameObjectAsync>d__20::<request>5__3
	AssetBundleLoadAssetOperation_t5A1401110A453E5BC738720B9A4E596235815463 * ___U3CrequestU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_assetBundleName_2() { return static_cast<int32_t>(offsetof(U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15, ___assetBundleName_2)); }
	inline String_t* get_assetBundleName_2() const { return ___assetBundleName_2; }
	inline String_t** get_address_of_assetBundleName_2() { return &___assetBundleName_2; }
	inline void set_assetBundleName_2(String_t* value)
	{
		___assetBundleName_2 = value;
		Il2CppCodeGenWriteBarrier((&___assetBundleName_2), value);
	}

	inline static int32_t get_offset_of_assetName_3() { return static_cast<int32_t>(offsetof(U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15, ___assetName_3)); }
	inline String_t* get_assetName_3() const { return ___assetName_3; }
	inline String_t** get_address_of_assetName_3() { return &___assetName_3; }
	inline void set_assetName_3(String_t* value)
	{
		___assetName_3 = value;
		Il2CppCodeGenWriteBarrier((&___assetName_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15, ___U3CU3E4__this_4)); }
	inline LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15, ___U3CstartTimeU3E5__2_5)); }
	inline float get_U3CstartTimeU3E5__2_5() const { return ___U3CstartTimeU3E5__2_5; }
	inline float* get_address_of_U3CstartTimeU3E5__2_5() { return &___U3CstartTimeU3E5__2_5; }
	inline void set_U3CstartTimeU3E5__2_5(float value)
	{
		___U3CstartTimeU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15, ___U3CrequestU3E5__3_6)); }
	inline AssetBundleLoadAssetOperation_t5A1401110A453E5BC738720B9A4E596235815463 * get_U3CrequestU3E5__3_6() const { return ___U3CrequestU3E5__3_6; }
	inline AssetBundleLoadAssetOperation_t5A1401110A453E5BC738720B9A4E596235815463 ** get_address_of_U3CrequestU3E5__3_6() { return &___U3CrequestU3E5__3_6; }
	inline void set_U3CrequestU3E5__3_6(AssetBundleLoadAssetOperation_t5A1401110A453E5BC738720B9A4E596235815463 * value)
	{
		___U3CrequestU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E5__3_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINSTANTIATEGAMEOBJECTASYNCU3ED__20_T6256914044263C7A2C7E5E1C4AFA4D6530DDFD15_H
#ifndef U3CBEGINEXAMPLEU3ED__6_TE817DA7D7BD942FEA3AA7C51343CEBD411308891_H
#define U3CBEGINEXAMPLEU3ED__6_TE817DA7D7BD942FEA3AA7C51343CEBD411308891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadVariants/<BeginExample>d__6
struct  U3CBeginExampleU3Ed__6_tE817DA7D7BD942FEA3AA7C51343CEBD411308891  : public RuntimeObject
{
public:
	// System.Int32 LoadVariants/<BeginExample>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadVariants/<BeginExample>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoadVariants LoadVariants/<BeginExample>d__6::<>4__this
	LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CBeginExampleU3Ed__6_tE817DA7D7BD942FEA3AA7C51343CEBD411308891, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CBeginExampleU3Ed__6_tE817DA7D7BD942FEA3AA7C51343CEBD411308891, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CBeginExampleU3Ed__6_tE817DA7D7BD942FEA3AA7C51343CEBD411308891, ___U3CU3E4__this_2)); }
	inline LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBEGINEXAMPLEU3ED__6_TE817DA7D7BD942FEA3AA7C51343CEBD411308891_H
#ifndef U3CINITIALIZEU3ED__7_T269FB3A3B9BCEE101A36FD8E66714A3C3F996ADB_H
#define U3CINITIALIZEU3ED__7_T269FB3A3B9BCEE101A36FD8E66714A3C3F996ADB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadVariants/<Initialize>d__7
struct  U3CInitializeU3Ed__7_t269FB3A3B9BCEE101A36FD8E66714A3C3F996ADB  : public RuntimeObject
{
public:
	// System.Int32 LoadVariants/<Initialize>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadVariants/<Initialize>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoadVariants LoadVariants/<Initialize>d__7::<>4__this
	LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__7_t269FB3A3B9BCEE101A36FD8E66714A3C3F996ADB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__7_t269FB3A3B9BCEE101A36FD8E66714A3C3F996ADB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__7_t269FB3A3B9BCEE101A36FD8E66714A3C3F996ADB, ___U3CU3E4__this_2)); }
	inline LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZEU3ED__7_T269FB3A3B9BCEE101A36FD8E66714A3C3F996ADB_H
#ifndef U3CINITIALIZELEVELASYNCU3ED__8_T2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6_H
#define U3CINITIALIZELEVELASYNCU3ED__8_T2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadVariants/<InitializeLevelAsync>d__8
struct  U3CInitializeLevelAsyncU3Ed__8_t2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6  : public RuntimeObject
{
public:
	// System.Int32 LoadVariants/<InitializeLevelAsync>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadVariants/<InitializeLevelAsync>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String LoadVariants/<InitializeLevelAsync>d__8::levelName
	String_t* ___levelName_2;
	// System.Boolean LoadVariants/<InitializeLevelAsync>d__8::isAdditive
	bool ___isAdditive_3;
	// LoadVariants LoadVariants/<InitializeLevelAsync>d__8::<>4__this
	LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7 * ___U3CU3E4__this_4;
	// System.Single LoadVariants/<InitializeLevelAsync>d__8::<startTime>5__2
	float ___U3CstartTimeU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__8_t2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__8_t2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_levelName_2() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__8_t2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6, ___levelName_2)); }
	inline String_t* get_levelName_2() const { return ___levelName_2; }
	inline String_t** get_address_of_levelName_2() { return &___levelName_2; }
	inline void set_levelName_2(String_t* value)
	{
		___levelName_2 = value;
		Il2CppCodeGenWriteBarrier((&___levelName_2), value);
	}

	inline static int32_t get_offset_of_isAdditive_3() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__8_t2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6, ___isAdditive_3)); }
	inline bool get_isAdditive_3() const { return ___isAdditive_3; }
	inline bool* get_address_of_isAdditive_3() { return &___isAdditive_3; }
	inline void set_isAdditive_3(bool value)
	{
		___isAdditive_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__8_t2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6, ___U3CU3E4__this_4)); }
	inline LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CInitializeLevelAsyncU3Ed__8_t2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6, ___U3CstartTimeU3E5__2_5)); }
	inline float get_U3CstartTimeU3E5__2_5() const { return ___U3CstartTimeU3E5__2_5; }
	inline float* get_address_of_U3CstartTimeU3E5__2_5() { return &___U3CstartTimeU3E5__2_5; }
	inline void set_U3CstartTimeU3E5__2_5(float value)
	{
		___U3CstartTimeU3E5__2_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZELEVELASYNCU3ED__8_T2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6_H
#ifndef U3CU3EC_T26E20EF84C663EA1EBEB62D1E58357CB34FE1904_H
#define U3CU3EC_T26E20EF84C663EA1EBEB62D1E58357CB34FE1904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuItemOptions/<>c
struct  U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904_StaticFields
{
public:
	// MenuItemOptions/<>c MenuItemOptions/<>c::<>9
	U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904 * ___U3CU3E9_0;
	// System.Action MenuItemOptions/<>c::<>9__30_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__30_0_1;
	// System.Action MenuItemOptions/<>c::<>9__31_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__31_0_2;
	// System.Action MenuItemOptions/<>c::<>9__33_2
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__33_2_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__30_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904_StaticFields, ___U3CU3E9__30_0_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__30_0_1() const { return ___U3CU3E9__30_0_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__30_0_1() { return &___U3CU3E9__30_0_1; }
	inline void set_U3CU3E9__30_0_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__30_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__30_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904_StaticFields, ___U3CU3E9__31_0_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__31_0_2() const { return ___U3CU3E9__31_0_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__31_0_2() { return &___U3CU3E9__31_0_2; }
	inline void set_U3CU3E9__31_0_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__31_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__33_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904_StaticFields, ___U3CU3E9__33_2_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__33_2_3() const { return ___U3CU3E9__33_2_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__33_2_3() { return &___U3CU3E9__33_2_3; }
	inline void set_U3CU3E9__33_2_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__33_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__33_2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T26E20EF84C663EA1EBEB62D1E58357CB34FE1904_H
#ifndef U3CU3EC__DISPLAYCLASS33_0_T16488CE66582A182664EE32A23C91A46C892C003_H
#define U3CU3EC__DISPLAYCLASS33_0_T16488CE66582A182664EE32A23C91A46C892C003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuItemOptions/<>c__DisplayClass33_0
struct  U3CU3Ec__DisplayClass33_0_t16488CE66582A182664EE32A23C91A46C892C003  : public RuntimeObject
{
public:
	// MenuItemOptions MenuItemOptions/<>c__DisplayClass33_0::<>4__this
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7 * ___U3CU3E4__this_0;
	// MPAR.Common.Scripting.Script MenuItemOptions/<>c__DisplayClass33_0::script
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___script_1;
	// System.Action`1<MPAR.Common.Scripting.Script> MenuItemOptions/<>c__DisplayClass33_0::<>9__3
	Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * ___U3CU3E9__3_2;
	// System.Action MenuItemOptions/<>c__DisplayClass33_0::<>9__1
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t16488CE66582A182664EE32A23C91A46C892C003, ___U3CU3E4__this_0)); }
	inline MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_script_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t16488CE66582A182664EE32A23C91A46C892C003, ___script_1)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_script_1() const { return ___script_1; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_script_1() { return &___script_1; }
	inline void set_script_1(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___script_1 = value;
		Il2CppCodeGenWriteBarrier((&___script_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t16488CE66582A182664EE32A23C91A46C892C003, ___U3CU3E9__3_2)); }
	inline Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * get_U3CU3E9__3_2() const { return ___U3CU3E9__3_2; }
	inline Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E ** get_address_of_U3CU3E9__3_2() { return &___U3CU3E9__3_2; }
	inline void set_U3CU3E9__3_2(Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * value)
	{
		___U3CU3E9__3_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t16488CE66582A182664EE32A23C91A46C892C003, ___U3CU3E9__1_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__1_3() const { return ___U3CU3E9__1_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__1_3() { return &___U3CU3E9__1_3; }
	inline void set_U3CU3E9__1_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_0_T16488CE66582A182664EE32A23C91A46C892C003_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_TD85AB27D7A3268945ED7995DA996DFC51CC2953F_H
#define U3CU3EC__DISPLAYCLASS8_0_TD85AB27D7A3268945ED7995DA996DFC51CC2953F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecentScriptsMenu/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_tD85AB27D7A3268945ED7995DA996DFC51CC2953F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<MPAR.Common.Scripting.Script> RecentScriptsMenu/<>c__DisplayClass8_0::scripts
	List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * ___scripts_0;
	// RecentScriptsMenu RecentScriptsMenu/<>c__DisplayClass8_0::<>4__this
	RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088 * ___U3CU3E4__this_1;
	// System.Action`1<System.Collections.Generic.List`1<MPAR.Common.Scripting.Script>> RecentScriptsMenu/<>c__DisplayClass8_0::setScriptsVar
	Action_1_t50C72EBE10EB02C3DAEB93F043716129AD3C9E5B * ___setScriptsVar_2;

public:
	inline static int32_t get_offset_of_scripts_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tD85AB27D7A3268945ED7995DA996DFC51CC2953F, ___scripts_0)); }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * get_scripts_0() const { return ___scripts_0; }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 ** get_address_of_scripts_0() { return &___scripts_0; }
	inline void set_scripts_0(List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * value)
	{
		___scripts_0 = value;
		Il2CppCodeGenWriteBarrier((&___scripts_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tD85AB27D7A3268945ED7995DA996DFC51CC2953F, ___U3CU3E4__this_1)); }
	inline RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}

	inline static int32_t get_offset_of_setScriptsVar_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tD85AB27D7A3268945ED7995DA996DFC51CC2953F, ___setScriptsVar_2)); }
	inline Action_1_t50C72EBE10EB02C3DAEB93F043716129AD3C9E5B * get_setScriptsVar_2() const { return ___setScriptsVar_2; }
	inline Action_1_t50C72EBE10EB02C3DAEB93F043716129AD3C9E5B ** get_address_of_setScriptsVar_2() { return &___setScriptsVar_2; }
	inline void set_setScriptsVar_2(Action_1_t50C72EBE10EB02C3DAEB93F043716129AD3C9E5B * value)
	{
		___setScriptsVar_2 = value;
		Il2CppCodeGenWriteBarrier((&___setScriptsVar_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_TD85AB27D7A3268945ED7995DA996DFC51CC2953F_H
#ifndef U3CREFRESHCOROUTINEU3ED__8_T47623F75E0714ADB7A3398C383C8825AEF697CC3_H
#define U3CREFRESHCOROUTINEU3ED__8_T47623F75E0714ADB7A3398C383C8825AEF697CC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecentScriptsMenu/<RefreshCoroutine>d__8
struct  U3CRefreshCoroutineU3Ed__8_t47623F75E0714ADB7A3398C383C8825AEF697CC3  : public RuntimeObject
{
public:
	// System.Int32 RecentScriptsMenu/<RefreshCoroutine>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RecentScriptsMenu/<RefreshCoroutine>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RecentScriptsMenu RecentScriptsMenu/<RefreshCoroutine>d__8::<>4__this
	RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088 * ___U3CU3E4__this_2;
	// RecentScriptsMenu/<>c__DisplayClass8_0 RecentScriptsMenu/<RefreshCoroutine>d__8::<>8__1
	U3CU3Ec__DisplayClass8_0_tD85AB27D7A3268945ED7995DA996DFC51CC2953F * ___U3CU3E8__1_3;
	// System.Action RecentScriptsMenu/<RefreshCoroutine>d__8::onFinishRefresh
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onFinishRefresh_4;
	// System.Threading.Tasks.Task RecentScriptsMenu/<RefreshCoroutine>d__8::<task>5__2
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___U3CtaskU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRefreshCoroutineU3Ed__8_t47623F75E0714ADB7A3398C383C8825AEF697CC3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRefreshCoroutineU3Ed__8_t47623F75E0714ADB7A3398C383C8825AEF697CC3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CRefreshCoroutineU3Ed__8_t47623F75E0714ADB7A3398C383C8825AEF697CC3, ___U3CU3E4__this_2)); }
	inline RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_3() { return static_cast<int32_t>(offsetof(U3CRefreshCoroutineU3Ed__8_t47623F75E0714ADB7A3398C383C8825AEF697CC3, ___U3CU3E8__1_3)); }
	inline U3CU3Ec__DisplayClass8_0_tD85AB27D7A3268945ED7995DA996DFC51CC2953F * get_U3CU3E8__1_3() const { return ___U3CU3E8__1_3; }
	inline U3CU3Ec__DisplayClass8_0_tD85AB27D7A3268945ED7995DA996DFC51CC2953F ** get_address_of_U3CU3E8__1_3() { return &___U3CU3E8__1_3; }
	inline void set_U3CU3E8__1_3(U3CU3Ec__DisplayClass8_0_tD85AB27D7A3268945ED7995DA996DFC51CC2953F * value)
	{
		___U3CU3E8__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_3), value);
	}

	inline static int32_t get_offset_of_onFinishRefresh_4() { return static_cast<int32_t>(offsetof(U3CRefreshCoroutineU3Ed__8_t47623F75E0714ADB7A3398C383C8825AEF697CC3, ___onFinishRefresh_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onFinishRefresh_4() const { return ___onFinishRefresh_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onFinishRefresh_4() { return &___onFinishRefresh_4; }
	inline void set_onFinishRefresh_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onFinishRefresh_4 = value;
		Il2CppCodeGenWriteBarrier((&___onFinishRefresh_4), value);
	}

	inline static int32_t get_offset_of_U3CtaskU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CRefreshCoroutineU3Ed__8_t47623F75E0714ADB7A3398C383C8825AEF697CC3, ___U3CtaskU3E5__2_5)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_U3CtaskU3E5__2_5() const { return ___U3CtaskU3E5__2_5; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_U3CtaskU3E5__2_5() { return &___U3CtaskU3E5__2_5; }
	inline void set_U3CtaskU3E5__2_5(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___U3CtaskU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtaskU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREFRESHCOROUTINEU3ED__8_T47623F75E0714ADB7A3398C383C8825AEF697CC3_H
#ifndef U3CSTARTU3ED__5_TAAA4B9D5168B9BB29E32A1E92D090D042355BA0D_H
#define U3CSTARTU3ED__5_TAAA4B9D5168B9BB29E32A1E92D090D042355BA0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecentScriptsMenu/<Start>d__5
struct  U3CStartU3Ed__5_tAAA4B9D5168B9BB29E32A1E92D090D042355BA0D  : public RuntimeObject
{
public:
	// System.Int32 RecentScriptsMenu/<Start>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RecentScriptsMenu/<Start>d__5::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RecentScriptsMenu RecentScriptsMenu/<Start>d__5::<>4__this
	RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__5_tAAA4B9D5168B9BB29E32A1E92D090D042355BA0D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__5_tAAA4B9D5168B9BB29E32A1E92D090D042355BA0D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__5_tAAA4B9D5168B9BB29E32A1E92D090D042355BA0D, ___U3CU3E4__this_2)); }
	inline RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__5_TAAA4B9D5168B9BB29E32A1E92D090D042355BA0D_H
#ifndef U3CU3EC__DISPLAYCLASS40_0_T0C0F190947FB9FA030087A000A38F81CFCE8D6E8_H
#define U3CU3EC__DISPLAYCLASS40_0_T0C0F190947FB9FA030087A000A38F81CFCE8D6E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScanView/<>c__DisplayClass40_0
struct  U3CU3Ec__DisplayClass40_0_t0C0F190947FB9FA030087A000A38F81CFCE8D6E8  : public RuntimeObject
{
public:
	// System.Func`5<UnityEngine.Texture2D,System.Byte[],System.Int32,System.Int32,System.Boolean> ScanView/<>c__DisplayClass40_0::callback
	Func_5_tF569143B5BBB2D7031D2DD25254C0EB863B22668 * ___callback_0;
	// ScanView ScanView/<>c__DisplayClass40_0::<>4__this
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t0C0F190947FB9FA030087A000A38F81CFCE8D6E8, ___callback_0)); }
	inline Func_5_tF569143B5BBB2D7031D2DD25254C0EB863B22668 * get_callback_0() const { return ___callback_0; }
	inline Func_5_tF569143B5BBB2D7031D2DD25254C0EB863B22668 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Func_5_tF569143B5BBB2D7031D2DD25254C0EB863B22668 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t0C0F190947FB9FA030087A000A38F81CFCE8D6E8, ___U3CU3E4__this_1)); }
	inline ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS40_0_T0C0F190947FB9FA030087A000A38F81CFCE8D6E8_H
#ifndef U3CU3EC__DISPLAYCLASS45_0_T780CC3EBA6C391DFA98275CD75A588EE83D66D72_H
#define U3CU3EC__DISPLAYCLASS45_0_T780CC3EBA6C391DFA98275CD75A588EE83D66D72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScanView/<>c__DisplayClass45_0
struct  U3CU3Ec__DisplayClass45_0_t780CC3EBA6C391DFA98275CD75A588EE83D66D72  : public RuntimeObject
{
public:
	// System.Action`1<MPAR.Common.Scripting.Script> ScanView/<>c__DisplayClass45_0::callback
	Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass45_0_t780CC3EBA6C391DFA98275CD75A588EE83D66D72, ___callback_0)); }
	inline Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * get_callback_0() const { return ___callback_0; }
	inline Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS45_0_T780CC3EBA6C391DFA98275CD75A588EE83D66D72_H
#ifndef U3CLOADSCRIPTU3ED__45_TEB913D23620B8FC3697FE3E5D6456F77243749BD_H
#define U3CLOADSCRIPTU3ED__45_TEB913D23620B8FC3697FE3E5D6456F77243749BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScanView/<LoadScript>d__45
struct  U3CLoadScriptU3Ed__45_tEB913D23620B8FC3697FE3E5D6456F77243749BD  : public RuntimeObject
{
public:
	// System.Int32 ScanView/<LoadScript>d__45::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ScanView/<LoadScript>d__45::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Action`1<MPAR.Common.Scripting.Script> ScanView/<LoadScript>d__45::callback
	Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * ___callback_2;
	// System.String ScanView/<LoadScript>d__45::scriptUrl
	String_t* ___scriptUrl_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadScriptU3Ed__45_tEB913D23620B8FC3697FE3E5D6456F77243749BD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadScriptU3Ed__45_tEB913D23620B8FC3697FE3E5D6456F77243749BD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CLoadScriptU3Ed__45_tEB913D23620B8FC3697FE3E5D6456F77243749BD, ___callback_2)); }
	inline Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * get_callback_2() const { return ___callback_2; }
	inline Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_scriptUrl_3() { return static_cast<int32_t>(offsetof(U3CLoadScriptU3Ed__45_tEB913D23620B8FC3697FE3E5D6456F77243749BD, ___scriptUrl_3)); }
	inline String_t* get_scriptUrl_3() const { return ___scriptUrl_3; }
	inline String_t** get_address_of_scriptUrl_3() { return &___scriptUrl_3; }
	inline void set_scriptUrl_3(String_t* value)
	{
		___scriptUrl_3 = value;
		Il2CppCodeGenWriteBarrier((&___scriptUrl_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCRIPTU3ED__45_TEB913D23620B8FC3697FE3E5D6456F77243749BD_H
#ifndef U3CSCANSCREENIOSCOROUTINEU3ED__38_T49626750EA6146896E18FA1B9742B190F8C1129D_H
#define U3CSCANSCREENIOSCOROUTINEU3ED__38_T49626750EA6146896E18FA1B9742B190F8C1129D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScanView/<ScanScreenIOSCoroutine>d__38
struct  U3CScanScreenIOSCoroutineU3Ed__38_t49626750EA6146896E18FA1B9742B190F8C1129D  : public RuntimeObject
{
public:
	// System.Int32 ScanView/<ScanScreenIOSCoroutine>d__38::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ScanView/<ScanScreenIOSCoroutine>d__38::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ScanView ScanView/<ScanScreenIOSCoroutine>d__38::<>4__this
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB * ___U3CU3E4__this_2;
	// System.Func`5<UnityEngine.Texture2D,System.Byte[],System.Int32,System.Int32,System.Boolean> ScanView/<ScanScreenIOSCoroutine>d__38::callback
	Func_5_tF569143B5BBB2D7031D2DD25254C0EB863B22668 * ___callback_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScanScreenIOSCoroutineU3Ed__38_t49626750EA6146896E18FA1B9742B190F8C1129D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScanScreenIOSCoroutineU3Ed__38_t49626750EA6146896E18FA1B9742B190F8C1129D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CScanScreenIOSCoroutineU3Ed__38_t49626750EA6146896E18FA1B9742B190F8C1129D, ___U3CU3E4__this_2)); }
	inline ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CScanScreenIOSCoroutineU3Ed__38_t49626750EA6146896E18FA1B9742B190F8C1129D, ___callback_3)); }
	inline Func_5_tF569143B5BBB2D7031D2DD25254C0EB863B22668 * get_callback_3() const { return ___callback_3; }
	inline Func_5_tF569143B5BBB2D7031D2DD25254C0EB863B22668 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(Func_5_tF569143B5BBB2D7031D2DD25254C0EB863B22668 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCANSCREENIOSCOROUTINEU3ED__38_T49626750EA6146896E18FA1B9742B190F8C1129D_H
#ifndef U3CSTARTSCANNINGCOROUTINEU3ED__47_T6A0D9196332D613CA17FC4DC8724DAC1793611CA_H
#define U3CSTARTSCANNINGCOROUTINEU3ED__47_T6A0D9196332D613CA17FC4DC8724DAC1793611CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScanView/<StartScanningCoroutine>d__47
struct  U3CStartScanningCoroutineU3Ed__47_t6A0D9196332D613CA17FC4DC8724DAC1793611CA  : public RuntimeObject
{
public:
	// System.Int32 ScanView/<StartScanningCoroutine>d__47::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ScanView/<StartScanningCoroutine>d__47::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ScanView ScanView/<StartScanningCoroutine>d__47::<>4__this
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartScanningCoroutineU3Ed__47_t6A0D9196332D613CA17FC4DC8724DAC1793611CA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartScanningCoroutineU3Ed__47_t6A0D9196332D613CA17FC4DC8724DAC1793611CA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartScanningCoroutineU3Ed__47_t6A0D9196332D613CA17FC4DC8724DAC1793611CA, ___U3CU3E4__this_2)); }
	inline ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTSCANNINGCOROUTINEU3ED__47_T6A0D9196332D613CA17FC4DC8724DAC1793611CA_H
#ifndef U3CU3EC_T0C79CC841305608AA3ACB2E65E14830DC73474BF_H
#define U3CU3EC_T0C79CC841305608AA3ACB2E65E14830DC73474BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShareButton/<>c
struct  U3CU3Ec_t0C79CC841305608AA3ACB2E65E14830DC73474BF  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t0C79CC841305608AA3ACB2E65E14830DC73474BF_StaticFields
{
public:
	// ShareButton/<>c ShareButton/<>c::<>9
	U3CU3Ec_t0C79CC841305608AA3ACB2E65E14830DC73474BF * ___U3CU3E9_0;
	// UnityEngine.Events.UnityAction ShareButton/<>c::<>9__0_0
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0C79CC841305608AA3ACB2E65E14830DC73474BF_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t0C79CC841305608AA3ACB2E65E14830DC73474BF * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t0C79CC841305608AA3ACB2E65E14830DC73474BF ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t0C79CC841305608AA3ACB2E65E14830DC73474BF * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0C79CC841305608AA3ACB2E65E14830DC73474BF_StaticFields, ___U3CU3E9__0_0_1)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T0C79CC841305608AA3ACB2E65E14830DC73474BF_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef URIHELPER_TAA55C759A587A6BAA6C56752BE38AB5CF36C9F93_H
#define URIHELPER_TAA55C759A587A6BAA6C56752BE38AB5CF36C9F93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// URIHelper
struct  URIHelper_tAA55C759A587A6BAA6C56752BE38AB5CF36C9F93  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIHELPER_TAA55C759A587A6BAA6C56752BE38AB5CF36C9F93_H
#ifndef U3CU3EC_T8D858DEFEB647A015B589DADBBBA22F95E35E4D6_H
#define U3CU3EC_T8D858DEFEB647A015B589DADBBBA22F95E35E4D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreadHelper/<>c
struct  U3CU3Ec_t8D858DEFEB647A015B589DADBBBA22F95E35E4D6  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t8D858DEFEB647A015B589DADBBBA22F95E35E4D6_StaticFields
{
public:
	// UnityThreadHelper/<>c UnityThreadHelper/<>c::<>9
	U3CU3Ec_t8D858DEFEB647A015B589DADBBBA22F95E35E4D6 * ___U3CU3E9_0;
	// System.Func`2<UnityThreading.ThreadBase,System.Boolean> UnityThreadHelper/<>c::<>9__27_0
	Func_2_t22351DE6F3F46A96FDCA9ADDA9030B7EAC289A80 * ___U3CU3E9__27_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D858DEFEB647A015B589DADBBBA22F95E35E4D6_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t8D858DEFEB647A015B589DADBBBA22F95E35E4D6 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t8D858DEFEB647A015B589DADBBBA22F95E35E4D6 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t8D858DEFEB647A015B589DADBBBA22F95E35E4D6 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D858DEFEB647A015B589DADBBBA22F95E35E4D6_StaticFields, ___U3CU3E9__27_0_1)); }
	inline Func_2_t22351DE6F3F46A96FDCA9ADDA9030B7EAC289A80 * get_U3CU3E9__27_0_1() const { return ___U3CU3E9__27_0_1; }
	inline Func_2_t22351DE6F3F46A96FDCA9ADDA9030B7EAC289A80 ** get_address_of_U3CU3E9__27_0_1() { return &___U3CU3E9__27_0_1; }
	inline void set_U3CU3E9__27_0_1(Func_2_t22351DE6F3F46A96FDCA9ADDA9030B7EAC289A80 * value)
	{
		___U3CU3E9__27_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T8D858DEFEB647A015B589DADBBBA22F95E35E4D6_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_T442FC0F2F333649450EED970052BE434EC648078_H
#define U3CU3EC__DISPLAYCLASS16_0_T442FC0F2F333649450EED970052BE434EC648078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreadHelper/<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t442FC0F2F333649450EED970052BE434EC648078  : public RuntimeObject
{
public:
	// System.Action`1<UnityThreading.ActionThread> UnityThreadHelper/<>c__DisplayClass16_0::action
	Action_1_tE3217F0296A761BE816CF81030F2F771090C1B77 * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t442FC0F2F333649450EED970052BE434EC648078, ___action_0)); }
	inline Action_1_tE3217F0296A761BE816CF81030F2F771090C1B77 * get_action_0() const { return ___action_0; }
	inline Action_1_tE3217F0296A761BE816CF81030F2F771090C1B77 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_tE3217F0296A761BE816CF81030F2F771090C1B77 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_T442FC0F2F333649450EED970052BE434EC648078_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_TF5AD97BDC607B9D93581B31AD3FB98092FDFEB1F_H
#define U3CU3EC__DISPLAYCLASS18_0_TF5AD97BDC607B9D93581B31AD3FB98092FDFEB1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreadHelper/<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_tF5AD97BDC607B9D93581B31AD3FB98092FDFEB1F  : public RuntimeObject
{
public:
	// System.Action UnityThreadHelper/<>c__DisplayClass18_0::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_tF5AD97BDC607B9D93581B31AD3FB98092FDFEB1F, ___action_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_0() const { return ___action_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_TF5AD97BDC607B9D93581B31AD3FB98092FDFEB1F_H
#ifndef U3CU3EC__DISPLAYCLASS19_0_T1FB565FD9DD5CF5E6183CAFC3874FCFD6E622E98_H
#define U3CU3EC__DISPLAYCLASS19_0_T1FB565FD9DD5CF5E6183CAFC3874FCFD6E622E98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreadHelper/<>c__DisplayClass19_0
struct  U3CU3Ec__DisplayClass19_0_t1FB565FD9DD5CF5E6183CAFC3874FCFD6E622E98  : public RuntimeObject
{
public:
	// System.Action UnityThreadHelper/<>c__DisplayClass19_0::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t1FB565FD9DD5CF5E6183CAFC3874FCFD6E622E98, ___action_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_0() const { return ___action_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS19_0_T1FB565FD9DD5CF5E6183CAFC3874FCFD6E622E98_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T16969379ECEBA207E671428B46514F17083B416F_H
#define U3CU3EC__DISPLAYCLASS22_0_T16969379ECEBA207E671428B46514F17083B416F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreadHelper/<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t16969379ECEBA207E671428B46514F17083B416F  : public RuntimeObject
{
public:
	// System.Func`1<System.Collections.IEnumerator> UnityThreadHelper/<>c__DisplayClass22_0::action
	Func_1_tB87849BEE3BBD29FEB301AB876F67ADA1BFEC3AB * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t16969379ECEBA207E671428B46514F17083B416F, ___action_0)); }
	inline Func_1_tB87849BEE3BBD29FEB301AB876F67ADA1BFEC3AB * get_action_0() const { return ___action_0; }
	inline Func_1_tB87849BEE3BBD29FEB301AB876F67ADA1BFEC3AB ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Func_1_tB87849BEE3BBD29FEB301AB876F67ADA1BFEC3AB * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T16969379ECEBA207E671428B46514F17083B416F_H
#ifndef U3CU3EC__DISPLAYCLASS23_0_T8BB6B3093B707DC5D4CC61B863DD591A70200B8B_H
#define U3CU3EC__DISPLAYCLASS23_0_T8BB6B3093B707DC5D4CC61B863DD591A70200B8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreadHelper/<>c__DisplayClass23_0
struct  U3CU3Ec__DisplayClass23_0_t8BB6B3093B707DC5D4CC61B863DD591A70200B8B  : public RuntimeObject
{
public:
	// System.Func`1<System.Collections.IEnumerator> UnityThreadHelper/<>c__DisplayClass23_0::action
	Func_1_tB87849BEE3BBD29FEB301AB876F67ADA1BFEC3AB * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_0_t8BB6B3093B707DC5D4CC61B863DD591A70200B8B, ___action_0)); }
	inline Func_1_tB87849BEE3BBD29FEB301AB876F67ADA1BFEC3AB * get_action_0() const { return ___action_0; }
	inline Func_1_tB87849BEE3BBD29FEB301AB876F67ADA1BFEC3AB ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Func_1_tB87849BEE3BBD29FEB301AB876F67ADA1BFEC3AB * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS23_0_T8BB6B3093B707DC5D4CC61B863DD591A70200B8B_H
#ifndef U3CU3EC_T96CBFEAF5BE0F3C2457C550F97A6B99D0E3496B7_H
#define U3CU3EC_T96CBFEAF5BE0F3C2457C550F97A6B99D0E3496B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRCustomMenu/<>c
struct  U3CU3Ec_t96CBFEAF5BE0F3C2457C550F97A6B99D0E3496B7  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t96CBFEAF5BE0F3C2457C550F97A6B99D0E3496B7_StaticFields
{
public:
	// VRCustomMenu/<>c VRCustomMenu/<>c::<>9
	U3CU3Ec_t96CBFEAF5BE0F3C2457C550F97A6B99D0E3496B7 * ___U3CU3E9_0;
	// System.EventHandler`1<MPAR.Common.UI.EventActionArgs> VRCustomMenu/<>c::<>9__6_0
	EventHandler_1_t28058BB22451F0F583F3F63C15CBF591BCD18E50 * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t96CBFEAF5BE0F3C2457C550F97A6B99D0E3496B7_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t96CBFEAF5BE0F3C2457C550F97A6B99D0E3496B7 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t96CBFEAF5BE0F3C2457C550F97A6B99D0E3496B7 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t96CBFEAF5BE0F3C2457C550F97A6B99D0E3496B7 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t96CBFEAF5BE0F3C2457C550F97A6B99D0E3496B7_StaticFields, ___U3CU3E9__6_0_1)); }
	inline EventHandler_1_t28058BB22451F0F583F3F63C15CBF591BCD18E50 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline EventHandler_1_t28058BB22451F0F583F3F63C15CBF591BCD18E50 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(EventHandler_1_t28058BB22451F0F583F3F63C15CBF591BCD18E50 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T96CBFEAF5BE0F3C2457C550F97A6B99D0E3496B7_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_T87FD7AED8450BA21DEB419FF79378F4C53666845_H
#define U3CU3EC__DISPLAYCLASS10_0_T87FD7AED8450BA21DEB419FF79378F4C53666845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRCustomMenu/<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t87FD7AED8450BA21DEB419FF79378F4C53666845  : public RuntimeObject
{
public:
	// System.String VRCustomMenu/<>c__DisplayClass10_0::key
	String_t* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t87FD7AED8450BA21DEB419FF79378F4C53666845, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_T87FD7AED8450BA21DEB419FF79378F4C53666845_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T5962AB1C9ADAAEDE5A9A77AC8A010BE00368D270_H
#define U3CU3EC__DISPLAYCLASS11_0_T5962AB1C9ADAAEDE5A9A77AC8A010BE00368D270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRCustomMenu/<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t5962AB1C9ADAAEDE5A9A77AC8A010BE00368D270  : public RuntimeObject
{
public:
	// Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem VRCustomMenu/<>c__DisplayClass11_0::item
	ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * ___item_0;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t5962AB1C9ADAAEDE5A9A77AC8A010BE00368D270, ___item_0)); }
	inline ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * get_item_0() const { return ___item_0; }
	inline ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((&___item_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T5962AB1C9ADAAEDE5A9A77AC8A010BE00368D270_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T0C60D59EFE49F8EE54269EBE2C2975BB62E10BEE_H
#define U3CU3EC__DISPLAYCLASS13_0_T0C60D59EFE49F8EE54269EBE2C2975BB62E10BEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRCustomMenu/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t0C60D59EFE49F8EE54269EBE2C2975BB62E10BEE  : public RuntimeObject
{
public:
	// System.String VRCustomMenu/<>c__DisplayClass13_0::key
	String_t* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t0C60D59EFE49F8EE54269EBE2C2975BB62E10BEE, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T0C60D59EFE49F8EE54269EBE2C2975BB62E10BEE_H
#ifndef U3CU3EC__DISPLAYCLASS14_0_TBA2A10F3D4DEB22AEE65A7E3F152A71D9B0880FB_H
#define U3CU3EC__DISPLAYCLASS14_0_TBA2A10F3D4DEB22AEE65A7E3F152A71D9B0880FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRCustomMenu/<>c__DisplayClass14_0
struct  U3CU3Ec__DisplayClass14_0_tBA2A10F3D4DEB22AEE65A7E3F152A71D9B0880FB  : public RuntimeObject
{
public:
	// System.String VRCustomMenu/<>c__DisplayClass14_0::key
	String_t* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_tBA2A10F3D4DEB22AEE65A7E3F152A71D9B0880FB, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS14_0_TBA2A10F3D4DEB22AEE65A7E3F152A71D9B0880FB_H
#ifndef U3CU3EC__DISPLAYCLASS15_0_TB08AB79105F26EADA045302693AE1A7FCC4393D4_H
#define U3CU3EC__DISPLAYCLASS15_0_TB08AB79105F26EADA045302693AE1A7FCC4393D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRCustomMenu/<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_tB08AB79105F26EADA045302693AE1A7FCC4393D4  : public RuntimeObject
{
public:
	// System.String VRCustomMenu/<>c__DisplayClass15_0::key
	String_t* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_tB08AB79105F26EADA045302693AE1A7FCC4393D4, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_0_TB08AB79105F26EADA045302693AE1A7FCC4393D4_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_TE82723E69FB61028CED69E38EE4D3EC531C679FE_H
#define U3CU3EC__DISPLAYCLASS9_0_TE82723E69FB61028CED69E38EE4D3EC531C679FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRCustomMenu/<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_tE82723E69FB61028CED69E38EE4D3EC531C679FE  : public RuntimeObject
{
public:
	// Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem VRCustomMenu/<>c__DisplayClass9_0::menuItem
	ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * ___menuItem_0;
	// MPAR.Common.UI.PortableMenu.PortableMenuItem VRCustomMenu/<>c__DisplayClass9_0::menuItemObject
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * ___menuItemObject_1;
	// VRCustomMenu VRCustomMenu/<>c__DisplayClass9_0::<>4__this
	VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_menuItem_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_tE82723E69FB61028CED69E38EE4D3EC531C679FE, ___menuItem_0)); }
	inline ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * get_menuItem_0() const { return ___menuItem_0; }
	inline ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 ** get_address_of_menuItem_0() { return &___menuItem_0; }
	inline void set_menuItem_0(ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * value)
	{
		___menuItem_0 = value;
		Il2CppCodeGenWriteBarrier((&___menuItem_0), value);
	}

	inline static int32_t get_offset_of_menuItemObject_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_tE82723E69FB61028CED69E38EE4D3EC531C679FE, ___menuItemObject_1)); }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * get_menuItemObject_1() const { return ___menuItemObject_1; }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 ** get_address_of_menuItemObject_1() { return &___menuItemObject_1; }
	inline void set_menuItemObject_1(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * value)
	{
		___menuItemObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___menuItemObject_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_tE82723E69FB61028CED69E38EE4D3EC531C679FE, ___U3CU3E4__this_2)); }
	inline VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_TE82723E69FB61028CED69E38EE4D3EC531C679FE_H
#ifndef ENUMERATOR_T397C2C22F4171233B76924B92EE05EB4B7F90F2B_H
#define ENUMERATOR_T397C2C22F4171233B76924B92EE05EB4B7F90F2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<MPAR.Common.Scripting.Script>
struct  Enumerator_t397C2C22F4171233B76924B92EE05EB4B7F90F2B 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t397C2C22F4171233B76924B92EE05EB4B7F90F2B, ___list_0)); }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * get_list_0() const { return ___list_0; }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t397C2C22F4171233B76924B92EE05EB4B7F90F2B, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t397C2C22F4171233B76924B92EE05EB4B7F90F2B, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t397C2C22F4171233B76924B92EE05EB4B7F90F2B, ___current_3)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_current_3() const { return ___current_3; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T397C2C22F4171233B76924B92EE05EB4B7F90F2B_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_TE4EDC8D5ED2772A911F67696644E6C77FA716DC0_H
#define NULLABLE_1_TE4EDC8D5ED2772A911F67696644E6C77FA716DC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TE4EDC8D5ED2772A911F67696644E6C77FA716DC0_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef ARWORLDMAPREQUEST_T4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167_H
#define ARWORLDMAPREQUEST_T4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARWorldMapRequest
struct  ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMapRequest::m_RequestId
	int32_t ___m_RequestId_0;

public:
	inline static int32_t get_offset_of_m_RequestId_0() { return static_cast<int32_t>(offsetof(ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167, ___m_RequestId_0)); }
	inline int32_t get_m_RequestId_0() const { return ___m_RequestId_0; }
	inline int32_t* get_address_of_m_RequestId_0() { return &___m_RequestId_0; }
	inline void set_m_RequestId_0(int32_t value)
	{
		___m_RequestId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARWORLDMAPREQUEST_T4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167_H
#ifndef U3CSAVEU3ED__27_TAAAF45083EB844EA39B7CB4CF64187F93C5A676B_H
#define U3CSAVEU3ED__27_TAAAF45083EB844EA39B7CB4CF64187F93C5A676B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARWorldMapController/<Save>d__27
struct  U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B  : public RuntimeObject
{
public:
	// System.Int32 ARWorldMapController/<Save>d__27::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ARWorldMapController/<Save>d__27::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ARWorldMapController ARWorldMapController/<Save>d__27::<>4__this
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * ___U3CU3E4__this_2;
	// UnityEngine.XR.ARKit.ARWorldMapRequest ARWorldMapController/<Save>d__27::<request>5__2
	ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167  ___U3CrequestU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B, ___U3CU3E4__this_2)); }
	inline ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B, ___U3CrequestU3E5__2_3)); }
	inline ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167  get_U3CrequestU3E5__2_3() const { return ___U3CrequestU3E5__2_3; }
	inline ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167 * get_address_of_U3CrequestU3E5__2_3() { return &___U3CrequestU3E5__2_3; }
	inline void set_U3CrequestU3E5__2_3(ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167  value)
	{
		___U3CrequestU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVEU3ED__27_TAAAF45083EB844EA39B7CB4CF64187F93C5A676B_H
#ifndef U3CU3CLOADSCRIPTSU3EG__CALLBACKU7C0U3ED_TB240D73D1C3DB0AABC37B0EC560DC2C508EF8182_H
#define U3CU3CLOADSCRIPTSU3EG__CALLBACKU7C0U3ED_TB240D73D1C3DB0AABC37B0EC560DC2C508EF8182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BrowseScriptsMenu/<>c__DisplayClass17_0/<<LoadScripts>g__callback|0>d
struct  U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182  : public RuntimeObject
{
public:
	// System.Int32 BrowseScriptsMenu/<>c__DisplayClass17_0/<<LoadScripts>g__callback|0>d::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BrowseScriptsMenu/<>c__DisplayClass17_0/<<LoadScripts>g__callback|0>d::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BrowseScriptsMenu/<>c__DisplayClass17_0 BrowseScriptsMenu/<>c__DisplayClass17_0/<<LoadScripts>g__callback|0>d::<>4__this
	U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598 * ___U3CU3E4__this_2;
	// System.String BrowseScriptsMenu/<>c__DisplayClass17_0/<<LoadScripts>g__callback|0>d::response
	String_t* ___response_3;
	// System.Collections.Generic.List`1<MPAR.Common.Scripting.Script> BrowseScriptsMenu/<>c__DisplayClass17_0/<<LoadScripts>g__callback|0>d::<downloadedScripts>5__2
	List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * ___U3CdownloadedScriptsU3E5__2_4;
	// System.Threading.Tasks.Task BrowseScriptsMenu/<>c__DisplayClass17_0/<<LoadScripts>g__callback|0>d::<whenAllTasksComplete>5__3
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___U3CwhenAllTasksCompleteU3E5__3_5;
	// System.Collections.Generic.List`1/Enumerator<MPAR.Common.Scripting.Script> BrowseScriptsMenu/<>c__DisplayClass17_0/<<LoadScripts>g__callback|0>d::<>7__wrap3
	Enumerator_t397C2C22F4171233B76924B92EE05EB4B7F90F2B  ___U3CU3E7__wrap3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182, ___U3CU3E4__this_2)); }
	inline U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_response_3() { return static_cast<int32_t>(offsetof(U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182, ___response_3)); }
	inline String_t* get_response_3() const { return ___response_3; }
	inline String_t** get_address_of_response_3() { return &___response_3; }
	inline void set_response_3(String_t* value)
	{
		___response_3 = value;
		Il2CppCodeGenWriteBarrier((&___response_3), value);
	}

	inline static int32_t get_offset_of_U3CdownloadedScriptsU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182, ___U3CdownloadedScriptsU3E5__2_4)); }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * get_U3CdownloadedScriptsU3E5__2_4() const { return ___U3CdownloadedScriptsU3E5__2_4; }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 ** get_address_of_U3CdownloadedScriptsU3E5__2_4() { return &___U3CdownloadedScriptsU3E5__2_4; }
	inline void set_U3CdownloadedScriptsU3E5__2_4(List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * value)
	{
		___U3CdownloadedScriptsU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdownloadedScriptsU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CwhenAllTasksCompleteU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182, ___U3CwhenAllTasksCompleteU3E5__3_5)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_U3CwhenAllTasksCompleteU3E5__3_5() const { return ___U3CwhenAllTasksCompleteU3E5__3_5; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_U3CwhenAllTasksCompleteU3E5__3_5() { return &___U3CwhenAllTasksCompleteU3E5__3_5; }
	inline void set_U3CwhenAllTasksCompleteU3E5__3_5(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___U3CwhenAllTasksCompleteU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwhenAllTasksCompleteU3E5__3_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap3_6() { return static_cast<int32_t>(offsetof(U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182, ___U3CU3E7__wrap3_6)); }
	inline Enumerator_t397C2C22F4171233B76924B92EE05EB4B7F90F2B  get_U3CU3E7__wrap3_6() const { return ___U3CU3E7__wrap3_6; }
	inline Enumerator_t397C2C22F4171233B76924B92EE05EB4B7F90F2B * get_address_of_U3CU3E7__wrap3_6() { return &___U3CU3E7__wrap3_6; }
	inline void set_U3CU3E7__wrap3_6(Enumerator_t397C2C22F4171233B76924B92EE05EB4B7F90F2B  value)
	{
		___U3CU3E7__wrap3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3CLOADSCRIPTSU3EG__CALLBACKU7C0U3ED_TB240D73D1C3DB0AABC37B0EC560DC2C508EF8182_H
#ifndef NULLABLE_1_TAF01623AB359AB6D460A6F432BF98EA08C7F9C60_H
#define NULLABLE_1_TAF01623AB359AB6D460A6F432BF98EA08C7F9C60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Color>
struct  Nullable_1_tAF01623AB359AB6D460A6F432BF98EA08C7F9C60 
{
public:
	// T System.Nullable`1::value
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tAF01623AB359AB6D460A6F432BF98EA08C7F9C60, ___value_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_value_0() const { return ___value_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tAF01623AB359AB6D460A6F432BF98EA08C7F9C60, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TAF01623AB359AB6D460A6F432BF98EA08C7F9C60_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ARFACEARKITBLENDSHAPEVISUALIZER_TC8E333D7AED58A917AE539D7E3987050F77EC048_H
#define ARFACEARKITBLENDSHAPEVISUALIZER_TC8E333D7AED58A917AE539D7E3987050F77EC048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARFaceArkitBlendShapeVisualizer
struct  ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single ARFaceArkitBlendShapeVisualizer::m_CoefficientScale
	float ___m_CoefficientScale_4;
	// UnityEngine.SkinnedMeshRenderer ARFaceArkitBlendShapeVisualizer::m_SkinnedMeshRenderer
	SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * ___m_SkinnedMeshRenderer_5;
	// UnityEngine.XR.ARKit.ARKitFaceSubsystem ARFaceArkitBlendShapeVisualizer::m_ArkitFaceSubsystem
	ARKitFaceSubsystem_t24E01CF6C590EFAC9B51F7B1D341C646A69E9CD9 * ___m_ArkitFaceSubsystem_7;
	// System.Collections.Generic.Dictionary`2<UnityEngine.XR.ARKit.XRArkitBlendShapeLocation,System.Int32> ARFaceArkitBlendShapeVisualizer::m_FaceArkitBlendShapeIndexMap
	Dictionary_2_t056393A25D6B313BA194EBC2FD3230D220301357 * ___m_FaceArkitBlendShapeIndexMap_8;
	// UnityEngine.XR.ARFoundation.ARFace ARFaceArkitBlendShapeVisualizer::m_Face
	ARFace_t6743D3C0C57B5B559B335F7EB6523211A4EA579F * ___m_Face_9;

public:
	inline static int32_t get_offset_of_m_CoefficientScale_4() { return static_cast<int32_t>(offsetof(ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048, ___m_CoefficientScale_4)); }
	inline float get_m_CoefficientScale_4() const { return ___m_CoefficientScale_4; }
	inline float* get_address_of_m_CoefficientScale_4() { return &___m_CoefficientScale_4; }
	inline void set_m_CoefficientScale_4(float value)
	{
		___m_CoefficientScale_4 = value;
	}

	inline static int32_t get_offset_of_m_SkinnedMeshRenderer_5() { return static_cast<int32_t>(offsetof(ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048, ___m_SkinnedMeshRenderer_5)); }
	inline SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * get_m_SkinnedMeshRenderer_5() const { return ___m_SkinnedMeshRenderer_5; }
	inline SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 ** get_address_of_m_SkinnedMeshRenderer_5() { return &___m_SkinnedMeshRenderer_5; }
	inline void set_m_SkinnedMeshRenderer_5(SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * value)
	{
		___m_SkinnedMeshRenderer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_SkinnedMeshRenderer_5), value);
	}

	inline static int32_t get_offset_of_m_ArkitFaceSubsystem_7() { return static_cast<int32_t>(offsetof(ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048, ___m_ArkitFaceSubsystem_7)); }
	inline ARKitFaceSubsystem_t24E01CF6C590EFAC9B51F7B1D341C646A69E9CD9 * get_m_ArkitFaceSubsystem_7() const { return ___m_ArkitFaceSubsystem_7; }
	inline ARKitFaceSubsystem_t24E01CF6C590EFAC9B51F7B1D341C646A69E9CD9 ** get_address_of_m_ArkitFaceSubsystem_7() { return &___m_ArkitFaceSubsystem_7; }
	inline void set_m_ArkitFaceSubsystem_7(ARKitFaceSubsystem_t24E01CF6C590EFAC9B51F7B1D341C646A69E9CD9 * value)
	{
		___m_ArkitFaceSubsystem_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ArkitFaceSubsystem_7), value);
	}

	inline static int32_t get_offset_of_m_FaceArkitBlendShapeIndexMap_8() { return static_cast<int32_t>(offsetof(ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048, ___m_FaceArkitBlendShapeIndexMap_8)); }
	inline Dictionary_2_t056393A25D6B313BA194EBC2FD3230D220301357 * get_m_FaceArkitBlendShapeIndexMap_8() const { return ___m_FaceArkitBlendShapeIndexMap_8; }
	inline Dictionary_2_t056393A25D6B313BA194EBC2FD3230D220301357 ** get_address_of_m_FaceArkitBlendShapeIndexMap_8() { return &___m_FaceArkitBlendShapeIndexMap_8; }
	inline void set_m_FaceArkitBlendShapeIndexMap_8(Dictionary_2_t056393A25D6B313BA194EBC2FD3230D220301357 * value)
	{
		___m_FaceArkitBlendShapeIndexMap_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_FaceArkitBlendShapeIndexMap_8), value);
	}

	inline static int32_t get_offset_of_m_Face_9() { return static_cast<int32_t>(offsetof(ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048, ___m_Face_9)); }
	inline ARFace_t6743D3C0C57B5B559B335F7EB6523211A4EA579F * get_m_Face_9() const { return ___m_Face_9; }
	inline ARFace_t6743D3C0C57B5B559B335F7EB6523211A4EA579F ** get_address_of_m_Face_9() { return &___m_Face_9; }
	inline void set_m_Face_9(ARFace_t6743D3C0C57B5B559B335F7EB6523211A4EA579F * value)
	{
		___m_Face_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Face_9), value);
	}
};

struct ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARKit.XRFaceArkitBlendShapeCoefficient> ARFaceArkitBlendShapeVisualizer::s_FaceArkitBlendShapeCoefficients
	List_1_t797F7EE5A5B8DA5CE89E3AE41EE2BC966BDBCF67 * ___s_FaceArkitBlendShapeCoefficients_6;

public:
	inline static int32_t get_offset_of_s_FaceArkitBlendShapeCoefficients_6() { return static_cast<int32_t>(offsetof(ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048_StaticFields, ___s_FaceArkitBlendShapeCoefficients_6)); }
	inline List_1_t797F7EE5A5B8DA5CE89E3AE41EE2BC966BDBCF67 * get_s_FaceArkitBlendShapeCoefficients_6() const { return ___s_FaceArkitBlendShapeCoefficients_6; }
	inline List_1_t797F7EE5A5B8DA5CE89E3AE41EE2BC966BDBCF67 ** get_address_of_s_FaceArkitBlendShapeCoefficients_6() { return &___s_FaceArkitBlendShapeCoefficients_6; }
	inline void set_s_FaceArkitBlendShapeCoefficients_6(List_1_t797F7EE5A5B8DA5CE89E3AE41EE2BC966BDBCF67 * value)
	{
		___s_FaceArkitBlendShapeCoefficients_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_FaceArkitBlendShapeCoefficients_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFACEARKITBLENDSHAPEVISUALIZER_TC8E333D7AED58A917AE539D7E3987050F77EC048_H
#ifndef ARFEATHEREDPLANEMESHVISUALIZER_TE778AE37070D3D5969CFD0FEEE8826992D092A67_H
#define ARFEATHEREDPLANEMESHVISUALIZER_TE778AE37070D3D5969CFD0FEEE8826992D092A67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARFeatheredPlaneMeshVisualizer
struct  ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single ARFeatheredPlaneMeshVisualizer::m_FeatheringWidth
	float ___m_FeatheringWidth_4;
	// UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer ARFeatheredPlaneMeshVisualizer::m_PlaneMeshVisualizer
	ARPlaneMeshVisualizer_t1CAC012DACAA7D8D30B94247B8762D71D2624923 * ___m_PlaneMeshVisualizer_7;
	// UnityEngine.Material ARFeatheredPlaneMeshVisualizer::m_FeatheredPlaneMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_FeatheredPlaneMaterial_8;

public:
	inline static int32_t get_offset_of_m_FeatheringWidth_4() { return static_cast<int32_t>(offsetof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67, ___m_FeatheringWidth_4)); }
	inline float get_m_FeatheringWidth_4() const { return ___m_FeatheringWidth_4; }
	inline float* get_address_of_m_FeatheringWidth_4() { return &___m_FeatheringWidth_4; }
	inline void set_m_FeatheringWidth_4(float value)
	{
		___m_FeatheringWidth_4 = value;
	}

	inline static int32_t get_offset_of_m_PlaneMeshVisualizer_7() { return static_cast<int32_t>(offsetof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67, ___m_PlaneMeshVisualizer_7)); }
	inline ARPlaneMeshVisualizer_t1CAC012DACAA7D8D30B94247B8762D71D2624923 * get_m_PlaneMeshVisualizer_7() const { return ___m_PlaneMeshVisualizer_7; }
	inline ARPlaneMeshVisualizer_t1CAC012DACAA7D8D30B94247B8762D71D2624923 ** get_address_of_m_PlaneMeshVisualizer_7() { return &___m_PlaneMeshVisualizer_7; }
	inline void set_m_PlaneMeshVisualizer_7(ARPlaneMeshVisualizer_t1CAC012DACAA7D8D30B94247B8762D71D2624923 * value)
	{
		___m_PlaneMeshVisualizer_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlaneMeshVisualizer_7), value);
	}

	inline static int32_t get_offset_of_m_FeatheredPlaneMaterial_8() { return static_cast<int32_t>(offsetof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67, ___m_FeatheredPlaneMaterial_8)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_FeatheredPlaneMaterial_8() const { return ___m_FeatheredPlaneMaterial_8; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_FeatheredPlaneMaterial_8() { return &___m_FeatheredPlaneMaterial_8; }
	inline void set_m_FeatheredPlaneMaterial_8(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_FeatheredPlaneMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_FeatheredPlaneMaterial_8), value);
	}
};

struct ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> ARFeatheredPlaneMeshVisualizer::s_FeatheringUVs
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___s_FeatheringUVs_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> ARFeatheredPlaneMeshVisualizer::s_Vertices
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___s_Vertices_6;

public:
	inline static int32_t get_offset_of_s_FeatheringUVs_5() { return static_cast<int32_t>(offsetof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields, ___s_FeatheringUVs_5)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_s_FeatheringUVs_5() const { return ___s_FeatheringUVs_5; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_s_FeatheringUVs_5() { return &___s_FeatheringUVs_5; }
	inline void set_s_FeatheringUVs_5(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___s_FeatheringUVs_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_FeatheringUVs_5), value);
	}

	inline static int32_t get_offset_of_s_Vertices_6() { return static_cast<int32_t>(offsetof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields, ___s_Vertices_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_s_Vertices_6() const { return ___s_Vertices_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_s_Vertices_6() { return &___s_Vertices_6; }
	inline void set_s_Vertices_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___s_Vertices_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Vertices_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFEATHEREDPLANEMESHVISUALIZER_TE778AE37070D3D5969CFD0FEEE8826992D092A67_H
#ifndef ARWORLDMAPCONTROLLER_TE07FBF1307D659E7AB45F66CFBA14CAC07C98737_H
#define ARWORLDMAPCONTROLLER_TE07FBF1307D659E7AB45F66CFBA14CAC07C98737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARWorldMapController
struct  ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.XR.ARFoundation.ARSession ARWorldMapController::m_ARSession
	ARSession_tFD6F1BD76D4C003B8141D9B6255B904D8C5036AB * ___m_ARSession_4;
	// UnityEngine.UI.Text ARWorldMapController::m_ErrorText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_ErrorText_5;
	// UnityEngine.UI.Text ARWorldMapController::m_LogText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_LogText_6;
	// UnityEngine.UI.Text ARWorldMapController::m_MappingStatusText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_MappingStatusText_7;
	// UnityEngine.UI.Button ARWorldMapController::m_SaveButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_SaveButton_8;
	// UnityEngine.UI.Button ARWorldMapController::m_LoadButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_LoadButton_9;
	// System.Collections.Generic.List`1<System.String> ARWorldMapController::m_LogMessages
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___m_LogMessages_10;

public:
	inline static int32_t get_offset_of_m_ARSession_4() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_ARSession_4)); }
	inline ARSession_tFD6F1BD76D4C003B8141D9B6255B904D8C5036AB * get_m_ARSession_4() const { return ___m_ARSession_4; }
	inline ARSession_tFD6F1BD76D4C003B8141D9B6255B904D8C5036AB ** get_address_of_m_ARSession_4() { return &___m_ARSession_4; }
	inline void set_m_ARSession_4(ARSession_tFD6F1BD76D4C003B8141D9B6255B904D8C5036AB * value)
	{
		___m_ARSession_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ARSession_4), value);
	}

	inline static int32_t get_offset_of_m_ErrorText_5() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_ErrorText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_ErrorText_5() const { return ___m_ErrorText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_ErrorText_5() { return &___m_ErrorText_5; }
	inline void set_m_ErrorText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_ErrorText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ErrorText_5), value);
	}

	inline static int32_t get_offset_of_m_LogText_6() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_LogText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_LogText_6() const { return ___m_LogText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_LogText_6() { return &___m_LogText_6; }
	inline void set_m_LogText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_LogText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_LogText_6), value);
	}

	inline static int32_t get_offset_of_m_MappingStatusText_7() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_MappingStatusText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_MappingStatusText_7() const { return ___m_MappingStatusText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_MappingStatusText_7() { return &___m_MappingStatusText_7; }
	inline void set_m_MappingStatusText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_MappingStatusText_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_MappingStatusText_7), value);
	}

	inline static int32_t get_offset_of_m_SaveButton_8() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_SaveButton_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_SaveButton_8() const { return ___m_SaveButton_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_SaveButton_8() { return &___m_SaveButton_8; }
	inline void set_m_SaveButton_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_SaveButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SaveButton_8), value);
	}

	inline static int32_t get_offset_of_m_LoadButton_9() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_LoadButton_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_LoadButton_9() const { return ___m_LoadButton_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_LoadButton_9() { return &___m_LoadButton_9; }
	inline void set_m_LoadButton_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_LoadButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_LoadButton_9), value);
	}

	inline static int32_t get_offset_of_m_LogMessages_10() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_LogMessages_10)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_m_LogMessages_10() const { return ___m_LogMessages_10; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_m_LogMessages_10() { return &___m_LogMessages_10; }
	inline void set_m_LogMessages_10(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___m_LogMessages_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_LogMessages_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARWORLDMAPCONTROLLER_TE07FBF1307D659E7AB45F66CFBA14CAC07C98737_H
#ifndef BACKMENUSLIDER_T9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9_H
#define BACKMENUSLIDER_T9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackMenuSlider
struct  BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject BackMenuSlider::MainPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___MainPanel_4;
	// UnityEngine.GameObject BackMenuSlider::SliderPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___SliderPanel_5;
	// UnityEngine.GameObject BackMenuSlider::ButtonsPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ButtonsPanel_6;
	// UnityEngine.UI.Image BackMenuSlider::arrowImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___arrowImage_7;
	// UnityEngine.RectTransform BackMenuSlider::BottomMargin
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___BottomMargin_8;
	// System.Single BackMenuSlider::mainPanelHeight
	float ___mainPanelHeight_9;
	// UnityEngine.Sprite BackMenuSlider::arrowSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___arrowSprite_10;
	// UnityEngine.Sprite BackMenuSlider::expandButtonSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___expandButtonSprite_11;
	// System.Single BackMenuSlider::closedPositionPercentage
	float ___closedPositionPercentage_12;
	// System.Single BackMenuSlider::openPositionPercentage
	float ___openPositionPercentage_13;
	// UnityEngine.Vector2 BackMenuSlider::openPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___openPosition_14;
	// UnityEngine.Vector2 BackMenuSlider::closedPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___closedPosition_15;
	// System.Single BackMenuSlider::animatingDelta
	float ___animatingDelta_16;
	// System.Int32 BackMenuSlider::lastWidth
	int32_t ___lastWidth_17;
	// System.Int32 BackMenuSlider::lastHeight
	int32_t ___lastHeight_18;
	// System.Boolean BackMenuSlider::menuOpen
	bool ___menuOpen_19;
	// System.Single BackMenuSlider::secondsToAnimate
	float ___secondsToAnimate_20;
	// System.Boolean BackMenuSlider::touchDown
	bool ___touchDown_21;
	// UnityEngine.Vector2 BackMenuSlider::touchDownPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___touchDownPosition_22;
	// UnityEngine.Vector2 BackMenuSlider::lastTouchPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___lastTouchPosition_23;
	// UnityEngine.Vector2 BackMenuSlider::lastTouchPositionRaw
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___lastTouchPositionRaw_24;
	// UnityEngine.RectTransform[] BackMenuSlider::CustomizablePanels
	RectTransformU5BU5D_tF94B8797BE321CF3ED1C3E6426CE13AFC8F1D478* ___CustomizablePanels_25;
	// UnityEngine.UI.Button BackMenuSlider::lastCreatedButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___lastCreatedButton_26;
	// System.Collections.Generic.List`1<Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem> BackMenuSlider::menuItems
	List_1_t5DFB316E3A16E508385B16D18E565D7775541A8C * ___menuItems_27;
	// System.Int32 BackMenuSlider::skip
	int32_t ___skip_28;
	// System.Int32 BackMenuSlider::take
	int32_t ___take_29;
	// Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem BackMenuSlider::previousButton
	ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * ___previousButton_30;
	// Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem BackMenuSlider::nextButton
	ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * ___nextButton_31;
	// UnityEngine.UI.GraphicRaycaster BackMenuSlider::graphicRaycaster
	GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * ___graphicRaycaster_32;
	// UnityEngine.EventSystems.EventSystem BackMenuSlider::eventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___eventSystem_33;
	// UnityEngine.Material BackMenuSlider::closedIconMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___closedIconMaterial_34;
	// UnityEngine.Material BackMenuSlider::openIconMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___openIconMaterial_35;

public:
	inline static int32_t get_offset_of_MainPanel_4() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___MainPanel_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_MainPanel_4() const { return ___MainPanel_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_MainPanel_4() { return &___MainPanel_4; }
	inline void set_MainPanel_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___MainPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___MainPanel_4), value);
	}

	inline static int32_t get_offset_of_SliderPanel_5() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___SliderPanel_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_SliderPanel_5() const { return ___SliderPanel_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_SliderPanel_5() { return &___SliderPanel_5; }
	inline void set_SliderPanel_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___SliderPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___SliderPanel_5), value);
	}

	inline static int32_t get_offset_of_ButtonsPanel_6() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___ButtonsPanel_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ButtonsPanel_6() const { return ___ButtonsPanel_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ButtonsPanel_6() { return &___ButtonsPanel_6; }
	inline void set_ButtonsPanel_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ButtonsPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonsPanel_6), value);
	}

	inline static int32_t get_offset_of_arrowImage_7() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___arrowImage_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_arrowImage_7() const { return ___arrowImage_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_arrowImage_7() { return &___arrowImage_7; }
	inline void set_arrowImage_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___arrowImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___arrowImage_7), value);
	}

	inline static int32_t get_offset_of_BottomMargin_8() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___BottomMargin_8)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_BottomMargin_8() const { return ___BottomMargin_8; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_BottomMargin_8() { return &___BottomMargin_8; }
	inline void set_BottomMargin_8(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___BottomMargin_8 = value;
		Il2CppCodeGenWriteBarrier((&___BottomMargin_8), value);
	}

	inline static int32_t get_offset_of_mainPanelHeight_9() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___mainPanelHeight_9)); }
	inline float get_mainPanelHeight_9() const { return ___mainPanelHeight_9; }
	inline float* get_address_of_mainPanelHeight_9() { return &___mainPanelHeight_9; }
	inline void set_mainPanelHeight_9(float value)
	{
		___mainPanelHeight_9 = value;
	}

	inline static int32_t get_offset_of_arrowSprite_10() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___arrowSprite_10)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_arrowSprite_10() const { return ___arrowSprite_10; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_arrowSprite_10() { return &___arrowSprite_10; }
	inline void set_arrowSprite_10(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___arrowSprite_10 = value;
		Il2CppCodeGenWriteBarrier((&___arrowSprite_10), value);
	}

	inline static int32_t get_offset_of_expandButtonSprite_11() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___expandButtonSprite_11)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_expandButtonSprite_11() const { return ___expandButtonSprite_11; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_expandButtonSprite_11() { return &___expandButtonSprite_11; }
	inline void set_expandButtonSprite_11(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___expandButtonSprite_11 = value;
		Il2CppCodeGenWriteBarrier((&___expandButtonSprite_11), value);
	}

	inline static int32_t get_offset_of_closedPositionPercentage_12() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___closedPositionPercentage_12)); }
	inline float get_closedPositionPercentage_12() const { return ___closedPositionPercentage_12; }
	inline float* get_address_of_closedPositionPercentage_12() { return &___closedPositionPercentage_12; }
	inline void set_closedPositionPercentage_12(float value)
	{
		___closedPositionPercentage_12 = value;
	}

	inline static int32_t get_offset_of_openPositionPercentage_13() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___openPositionPercentage_13)); }
	inline float get_openPositionPercentage_13() const { return ___openPositionPercentage_13; }
	inline float* get_address_of_openPositionPercentage_13() { return &___openPositionPercentage_13; }
	inline void set_openPositionPercentage_13(float value)
	{
		___openPositionPercentage_13 = value;
	}

	inline static int32_t get_offset_of_openPosition_14() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___openPosition_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_openPosition_14() const { return ___openPosition_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_openPosition_14() { return &___openPosition_14; }
	inline void set_openPosition_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___openPosition_14 = value;
	}

	inline static int32_t get_offset_of_closedPosition_15() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___closedPosition_15)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_closedPosition_15() const { return ___closedPosition_15; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_closedPosition_15() { return &___closedPosition_15; }
	inline void set_closedPosition_15(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___closedPosition_15 = value;
	}

	inline static int32_t get_offset_of_animatingDelta_16() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___animatingDelta_16)); }
	inline float get_animatingDelta_16() const { return ___animatingDelta_16; }
	inline float* get_address_of_animatingDelta_16() { return &___animatingDelta_16; }
	inline void set_animatingDelta_16(float value)
	{
		___animatingDelta_16 = value;
	}

	inline static int32_t get_offset_of_lastWidth_17() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___lastWidth_17)); }
	inline int32_t get_lastWidth_17() const { return ___lastWidth_17; }
	inline int32_t* get_address_of_lastWidth_17() { return &___lastWidth_17; }
	inline void set_lastWidth_17(int32_t value)
	{
		___lastWidth_17 = value;
	}

	inline static int32_t get_offset_of_lastHeight_18() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___lastHeight_18)); }
	inline int32_t get_lastHeight_18() const { return ___lastHeight_18; }
	inline int32_t* get_address_of_lastHeight_18() { return &___lastHeight_18; }
	inline void set_lastHeight_18(int32_t value)
	{
		___lastHeight_18 = value;
	}

	inline static int32_t get_offset_of_menuOpen_19() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___menuOpen_19)); }
	inline bool get_menuOpen_19() const { return ___menuOpen_19; }
	inline bool* get_address_of_menuOpen_19() { return &___menuOpen_19; }
	inline void set_menuOpen_19(bool value)
	{
		___menuOpen_19 = value;
	}

	inline static int32_t get_offset_of_secondsToAnimate_20() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___secondsToAnimate_20)); }
	inline float get_secondsToAnimate_20() const { return ___secondsToAnimate_20; }
	inline float* get_address_of_secondsToAnimate_20() { return &___secondsToAnimate_20; }
	inline void set_secondsToAnimate_20(float value)
	{
		___secondsToAnimate_20 = value;
	}

	inline static int32_t get_offset_of_touchDown_21() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___touchDown_21)); }
	inline bool get_touchDown_21() const { return ___touchDown_21; }
	inline bool* get_address_of_touchDown_21() { return &___touchDown_21; }
	inline void set_touchDown_21(bool value)
	{
		___touchDown_21 = value;
	}

	inline static int32_t get_offset_of_touchDownPosition_22() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___touchDownPosition_22)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_touchDownPosition_22() const { return ___touchDownPosition_22; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_touchDownPosition_22() { return &___touchDownPosition_22; }
	inline void set_touchDownPosition_22(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___touchDownPosition_22 = value;
	}

	inline static int32_t get_offset_of_lastTouchPosition_23() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___lastTouchPosition_23)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_lastTouchPosition_23() const { return ___lastTouchPosition_23; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_lastTouchPosition_23() { return &___lastTouchPosition_23; }
	inline void set_lastTouchPosition_23(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___lastTouchPosition_23 = value;
	}

	inline static int32_t get_offset_of_lastTouchPositionRaw_24() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___lastTouchPositionRaw_24)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_lastTouchPositionRaw_24() const { return ___lastTouchPositionRaw_24; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_lastTouchPositionRaw_24() { return &___lastTouchPositionRaw_24; }
	inline void set_lastTouchPositionRaw_24(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___lastTouchPositionRaw_24 = value;
	}

	inline static int32_t get_offset_of_CustomizablePanels_25() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___CustomizablePanels_25)); }
	inline RectTransformU5BU5D_tF94B8797BE321CF3ED1C3E6426CE13AFC8F1D478* get_CustomizablePanels_25() const { return ___CustomizablePanels_25; }
	inline RectTransformU5BU5D_tF94B8797BE321CF3ED1C3E6426CE13AFC8F1D478** get_address_of_CustomizablePanels_25() { return &___CustomizablePanels_25; }
	inline void set_CustomizablePanels_25(RectTransformU5BU5D_tF94B8797BE321CF3ED1C3E6426CE13AFC8F1D478* value)
	{
		___CustomizablePanels_25 = value;
		Il2CppCodeGenWriteBarrier((&___CustomizablePanels_25), value);
	}

	inline static int32_t get_offset_of_lastCreatedButton_26() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___lastCreatedButton_26)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_lastCreatedButton_26() const { return ___lastCreatedButton_26; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_lastCreatedButton_26() { return &___lastCreatedButton_26; }
	inline void set_lastCreatedButton_26(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___lastCreatedButton_26 = value;
		Il2CppCodeGenWriteBarrier((&___lastCreatedButton_26), value);
	}

	inline static int32_t get_offset_of_menuItems_27() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___menuItems_27)); }
	inline List_1_t5DFB316E3A16E508385B16D18E565D7775541A8C * get_menuItems_27() const { return ___menuItems_27; }
	inline List_1_t5DFB316E3A16E508385B16D18E565D7775541A8C ** get_address_of_menuItems_27() { return &___menuItems_27; }
	inline void set_menuItems_27(List_1_t5DFB316E3A16E508385B16D18E565D7775541A8C * value)
	{
		___menuItems_27 = value;
		Il2CppCodeGenWriteBarrier((&___menuItems_27), value);
	}

	inline static int32_t get_offset_of_skip_28() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___skip_28)); }
	inline int32_t get_skip_28() const { return ___skip_28; }
	inline int32_t* get_address_of_skip_28() { return &___skip_28; }
	inline void set_skip_28(int32_t value)
	{
		___skip_28 = value;
	}

	inline static int32_t get_offset_of_take_29() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___take_29)); }
	inline int32_t get_take_29() const { return ___take_29; }
	inline int32_t* get_address_of_take_29() { return &___take_29; }
	inline void set_take_29(int32_t value)
	{
		___take_29 = value;
	}

	inline static int32_t get_offset_of_previousButton_30() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___previousButton_30)); }
	inline ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * get_previousButton_30() const { return ___previousButton_30; }
	inline ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 ** get_address_of_previousButton_30() { return &___previousButton_30; }
	inline void set_previousButton_30(ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * value)
	{
		___previousButton_30 = value;
		Il2CppCodeGenWriteBarrier((&___previousButton_30), value);
	}

	inline static int32_t get_offset_of_nextButton_31() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___nextButton_31)); }
	inline ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * get_nextButton_31() const { return ___nextButton_31; }
	inline ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 ** get_address_of_nextButton_31() { return &___nextButton_31; }
	inline void set_nextButton_31(ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488 * value)
	{
		___nextButton_31 = value;
		Il2CppCodeGenWriteBarrier((&___nextButton_31), value);
	}

	inline static int32_t get_offset_of_graphicRaycaster_32() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___graphicRaycaster_32)); }
	inline GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * get_graphicRaycaster_32() const { return ___graphicRaycaster_32; }
	inline GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 ** get_address_of_graphicRaycaster_32() { return &___graphicRaycaster_32; }
	inline void set_graphicRaycaster_32(GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * value)
	{
		___graphicRaycaster_32 = value;
		Il2CppCodeGenWriteBarrier((&___graphicRaycaster_32), value);
	}

	inline static int32_t get_offset_of_eventSystem_33() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___eventSystem_33)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_eventSystem_33() const { return ___eventSystem_33; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_eventSystem_33() { return &___eventSystem_33; }
	inline void set_eventSystem_33(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___eventSystem_33 = value;
		Il2CppCodeGenWriteBarrier((&___eventSystem_33), value);
	}

	inline static int32_t get_offset_of_closedIconMaterial_34() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___closedIconMaterial_34)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_closedIconMaterial_34() const { return ___closedIconMaterial_34; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_closedIconMaterial_34() { return &___closedIconMaterial_34; }
	inline void set_closedIconMaterial_34(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___closedIconMaterial_34 = value;
		Il2CppCodeGenWriteBarrier((&___closedIconMaterial_34), value);
	}

	inline static int32_t get_offset_of_openIconMaterial_35() { return static_cast<int32_t>(offsetof(BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9, ___openIconMaterial_35)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_openIconMaterial_35() const { return ___openIconMaterial_35; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_openIconMaterial_35() { return &___openIconMaterial_35; }
	inline void set_openIconMaterial_35(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___openIconMaterial_35 = value;
		Il2CppCodeGenWriteBarrier((&___openIconMaterial_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKMENUSLIDER_T9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9_H
#ifndef BACKTOMENUPANEL_TD676642900C6B32D11D1F8BD17C9691AF652E1AE_H
#define BACKTOMENUPANEL_TD676642900C6B32D11D1F8BD17C9691AF652E1AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackToMenuPanel
struct  BackToMenuPanel_tD676642900C6B32D11D1F8BD17C9691AF652E1AE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button BackToMenuPanel::menuButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___menuButton_4;

public:
	inline static int32_t get_offset_of_menuButton_4() { return static_cast<int32_t>(offsetof(BackToMenuPanel_tD676642900C6B32D11D1F8BD17C9691AF652E1AE, ___menuButton_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_menuButton_4() const { return ___menuButton_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_menuButton_4() { return &___menuButton_4; }
	inline void set_menuButton_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___menuButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___menuButton_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKTOMENUPANEL_TD676642900C6B32D11D1F8BD17C9691AF652E1AE_H
#ifndef CAMERABUTTON_T38DB2AA361E7A3116524B70103A3A23CFF820C0A_H
#define CAMERABUTTON_T38DB2AA361E7A3116524B70103A3A23CFF820C0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraButton
struct  CameraButton_t38DB2AA361E7A3116524B70103A3A23CFF820C0A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button CameraButton::button
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___button_4;

public:
	inline static int32_t get_offset_of_button_4() { return static_cast<int32_t>(offsetof(CameraButton_t38DB2AA361E7A3116524B70103A3A23CFF820C0A, ___button_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_button_4() const { return ___button_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_button_4() { return &___button_4; }
	inline void set_button_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___button_4 = value;
		Il2CppCodeGenWriteBarrier((&___button_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERABUTTON_T38DB2AA361E7A3116524B70103A3A23CFF820C0A_H
#ifndef CAMERACONFIGCONTROLLER_T7D9D87FE02FA864F391F3BB2B9E9C7923F85279B_H
#define CAMERACONFIGCONTROLLER_T7D9D87FE02FA864F391F3BB2B9E9C7923F85279B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraConfigController
struct  CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<System.String> CameraConfigController::m_ConfigurationNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___m_ConfigurationNames_4;
	// UnityEngine.UI.Dropdown CameraConfigController::m_Dropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___m_Dropdown_5;

public:
	inline static int32_t get_offset_of_m_ConfigurationNames_4() { return static_cast<int32_t>(offsetof(CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B, ___m_ConfigurationNames_4)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_m_ConfigurationNames_4() const { return ___m_ConfigurationNames_4; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_m_ConfigurationNames_4() { return &___m_ConfigurationNames_4; }
	inline void set_m_ConfigurationNames_4(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___m_ConfigurationNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConfigurationNames_4), value);
	}

	inline static int32_t get_offset_of_m_Dropdown_5() { return static_cast<int32_t>(offsetof(CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B, ___m_Dropdown_5)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_m_Dropdown_5() const { return ___m_Dropdown_5; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_m_Dropdown_5() { return &___m_Dropdown_5; }
	inline void set_m_Dropdown_5(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___m_Dropdown_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dropdown_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONFIGCONTROLLER_T7D9D87FE02FA864F391F3BB2B9E9C7923F85279B_H
#ifndef SINGLETON_1_T2A99EC44A1B00CD7E55DF146826BC9DCF38E9D0E_H
#define SINGLETON_1_T2A99EC44A1B00CD7E55DF146826BC9DCF38E9D0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<DesktopLog>
struct  Singleton_1_t2A99EC44A1B00CD7E55DF146826BC9DCF38E9D0E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t2A99EC44A1B00CD7E55DF146826BC9DCF38E9D0E_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	DesktopLog_t42A4E8BFEBF79EE1FC717B1EFDB6DE299EE9807F * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_t2A99EC44A1B00CD7E55DF146826BC9DCF38E9D0E_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline DesktopLog_t42A4E8BFEBF79EE1FC717B1EFDB6DE299EE9807F * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline DesktopLog_t42A4E8BFEBF79EE1FC717B1EFDB6DE299EE9807F ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(DesktopLog_t42A4E8BFEBF79EE1FC717B1EFDB6DE299EE9807F * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2A99EC44A1B00CD7E55DF146826BC9DCF38E9D0E_H
#ifndef DISABLEVERTICALPLANES_T40082019580280FBDED0DF62CC5DA4E1A8E32D19_H
#define DISABLEVERTICALPLANES_T40082019580280FBDED0DF62CC5DA4E1A8E32D19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisableVerticalPlanes
struct  DisableVerticalPlanes_t40082019580280FBDED0DF62CC5DA4E1A8E32D19  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text DisableVerticalPlanes::m_LogText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_LogText_4;

public:
	inline static int32_t get_offset_of_m_LogText_4() { return static_cast<int32_t>(offsetof(DisableVerticalPlanes_t40082019580280FBDED0DF62CC5DA4E1A8E32D19, ___m_LogText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_LogText_4() const { return ___m_LogText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_LogText_4() { return &___m_LogText_4; }
	inline void set_m_LogText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_LogText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_LogText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEVERTICALPLANES_T40082019580280FBDED0DF62CC5DA4E1A8E32D19_H
#ifndef EXAMPLE_TB7AEFF417A3F2A1E30F8931285218D37BC4954CF_H
#define EXAMPLE_TB7AEFF417A3F2A1E30F8931285218D37BC4954CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Example
struct  Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Example::_screenMiddleX
	float ____screenMiddleX_4;
	// System.Single Example::_screenMiddleY
	float ____screenMiddleY_5;

public:
	inline static int32_t get_offset_of__screenMiddleX_4() { return static_cast<int32_t>(offsetof(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF, ____screenMiddleX_4)); }
	inline float get__screenMiddleX_4() const { return ____screenMiddleX_4; }
	inline float* get_address_of__screenMiddleX_4() { return &____screenMiddleX_4; }
	inline void set__screenMiddleX_4(float value)
	{
		____screenMiddleX_4 = value;
	}

	inline static int32_t get_offset_of__screenMiddleY_5() { return static_cast<int32_t>(offsetof(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF, ____screenMiddleY_5)); }
	inline float get__screenMiddleY_5() const { return ____screenMiddleY_5; }
	inline float* get_address_of__screenMiddleY_5() { return &____screenMiddleY_5; }
	inline void set__screenMiddleY_5(float value)
	{
		____screenMiddleY_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE_TB7AEFF417A3F2A1E30F8931285218D37BC4954CF_H
#ifndef FADEPLANEONBOUNDARYCHANGE_T88629D7DC409498EABE1D1CCE054157CBE47CD7A_H
#define FADEPLANEONBOUNDARYCHANGE_T88629D7DC409498EABE1D1CCE054157CBE47CD7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadePlaneOnBoundaryChange
struct  FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Animator FadePlaneOnBoundaryChange::m_Animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_Animator_7;
	// UnityEngine.XR.ARFoundation.ARPlane FadePlaneOnBoundaryChange::m_Plane
	ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * ___m_Plane_8;
	// System.Single FadePlaneOnBoundaryChange::m_ShowTime
	float ___m_ShowTime_9;
	// System.Boolean FadePlaneOnBoundaryChange::m_UpdatingPlane
	bool ___m_UpdatingPlane_10;

public:
	inline static int32_t get_offset_of_m_Animator_7() { return static_cast<int32_t>(offsetof(FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A, ___m_Animator_7)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_Animator_7() const { return ___m_Animator_7; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_Animator_7() { return &___m_Animator_7; }
	inline void set_m_Animator_7(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_Animator_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Animator_7), value);
	}

	inline static int32_t get_offset_of_m_Plane_8() { return static_cast<int32_t>(offsetof(FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A, ___m_Plane_8)); }
	inline ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * get_m_Plane_8() const { return ___m_Plane_8; }
	inline ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E ** get_address_of_m_Plane_8() { return &___m_Plane_8; }
	inline void set_m_Plane_8(ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * value)
	{
		___m_Plane_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Plane_8), value);
	}

	inline static int32_t get_offset_of_m_ShowTime_9() { return static_cast<int32_t>(offsetof(FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A, ___m_ShowTime_9)); }
	inline float get_m_ShowTime_9() const { return ___m_ShowTime_9; }
	inline float* get_address_of_m_ShowTime_9() { return &___m_ShowTime_9; }
	inline void set_m_ShowTime_9(float value)
	{
		___m_ShowTime_9 = value;
	}

	inline static int32_t get_offset_of_m_UpdatingPlane_10() { return static_cast<int32_t>(offsetof(FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A, ___m_UpdatingPlane_10)); }
	inline bool get_m_UpdatingPlane_10() const { return ___m_UpdatingPlane_10; }
	inline bool* get_address_of_m_UpdatingPlane_10() { return &___m_UpdatingPlane_10; }
	inline void set_m_UpdatingPlane_10(bool value)
	{
		___m_UpdatingPlane_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEPLANEONBOUNDARYCHANGE_T88629D7DC409498EABE1D1CCE054157CBE47CD7A_H
#ifndef LIGHTESTIMATION_TAC157B297F822653381F8F35A6DE3600920A4319_H
#define LIGHTESTIMATION_TAC157B297F822653381F8F35A6DE3600920A4319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightEstimation
struct  LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Nullable`1<System.Single> LightEstimation::<brightness>k__BackingField
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___U3CbrightnessU3Ek__BackingField_4;
	// System.Nullable`1<System.Single> LightEstimation::<colorTemperature>k__BackingField
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___U3CcolorTemperatureU3Ek__BackingField_5;
	// System.Nullable`1<UnityEngine.Color> LightEstimation::<colorCorrection>k__BackingField
	Nullable_1_tAF01623AB359AB6D460A6F432BF98EA08C7F9C60  ___U3CcolorCorrectionU3Ek__BackingField_6;
	// UnityEngine.Light LightEstimation::m_Light
	Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * ___m_Light_7;

public:
	inline static int32_t get_offset_of_U3CbrightnessU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319, ___U3CbrightnessU3Ek__BackingField_4)); }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  get_U3CbrightnessU3Ek__BackingField_4() const { return ___U3CbrightnessU3Ek__BackingField_4; }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * get_address_of_U3CbrightnessU3Ek__BackingField_4() { return &___U3CbrightnessU3Ek__BackingField_4; }
	inline void set_U3CbrightnessU3Ek__BackingField_4(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  value)
	{
		___U3CbrightnessU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CcolorTemperatureU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319, ___U3CcolorTemperatureU3Ek__BackingField_5)); }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  get_U3CcolorTemperatureU3Ek__BackingField_5() const { return ___U3CcolorTemperatureU3Ek__BackingField_5; }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * get_address_of_U3CcolorTemperatureU3Ek__BackingField_5() { return &___U3CcolorTemperatureU3Ek__BackingField_5; }
	inline void set_U3CcolorTemperatureU3Ek__BackingField_5(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  value)
	{
		___U3CcolorTemperatureU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CcolorCorrectionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319, ___U3CcolorCorrectionU3Ek__BackingField_6)); }
	inline Nullable_1_tAF01623AB359AB6D460A6F432BF98EA08C7F9C60  get_U3CcolorCorrectionU3Ek__BackingField_6() const { return ___U3CcolorCorrectionU3Ek__BackingField_6; }
	inline Nullable_1_tAF01623AB359AB6D460A6F432BF98EA08C7F9C60 * get_address_of_U3CcolorCorrectionU3Ek__BackingField_6() { return &___U3CcolorCorrectionU3Ek__BackingField_6; }
	inline void set_U3CcolorCorrectionU3Ek__BackingField_6(Nullable_1_tAF01623AB359AB6D460A6F432BF98EA08C7F9C60  value)
	{
		___U3CcolorCorrectionU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_m_Light_7() { return static_cast<int32_t>(offsetof(LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319, ___m_Light_7)); }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * get_m_Light_7() const { return ___m_Light_7; }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C ** get_address_of_m_Light_7() { return &___m_Light_7; }
	inline void set_m_Light_7(Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * value)
	{
		___m_Light_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Light_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTESTIMATION_TAC157B297F822653381F8F35A6DE3600920A4319_H
#ifndef LIGHTESTIMATIONUI_TABE99F2D6E580AB4AD14174CE323C527882D6DA4_H
#define LIGHTESTIMATIONUI_TABE99F2D6E580AB4AD14174CE323C527882D6DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightEstimationUI
struct  LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text LightEstimationUI::m_BrightnessText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_BrightnessText_4;
	// UnityEngine.UI.Text LightEstimationUI::m_ColorTemperatureText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_ColorTemperatureText_5;
	// UnityEngine.UI.Text LightEstimationUI::m_ColorCorrectionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_ColorCorrectionText_6;
	// LightEstimation LightEstimationUI::m_LightEstimation
	LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319 * ___m_LightEstimation_8;

public:
	inline static int32_t get_offset_of_m_BrightnessText_4() { return static_cast<int32_t>(offsetof(LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4, ___m_BrightnessText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_BrightnessText_4() const { return ___m_BrightnessText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_BrightnessText_4() { return &___m_BrightnessText_4; }
	inline void set_m_BrightnessText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_BrightnessText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_BrightnessText_4), value);
	}

	inline static int32_t get_offset_of_m_ColorTemperatureText_5() { return static_cast<int32_t>(offsetof(LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4, ___m_ColorTemperatureText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_ColorTemperatureText_5() const { return ___m_ColorTemperatureText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_ColorTemperatureText_5() { return &___m_ColorTemperatureText_5; }
	inline void set_m_ColorTemperatureText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_ColorTemperatureText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTemperatureText_5), value);
	}

	inline static int32_t get_offset_of_m_ColorCorrectionText_6() { return static_cast<int32_t>(offsetof(LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4, ___m_ColorCorrectionText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_ColorCorrectionText_6() const { return ___m_ColorCorrectionText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_ColorCorrectionText_6() { return &___m_ColorCorrectionText_6; }
	inline void set_m_ColorCorrectionText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_ColorCorrectionText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorCorrectionText_6), value);
	}

	inline static int32_t get_offset_of_m_LightEstimation_8() { return static_cast<int32_t>(offsetof(LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4, ___m_LightEstimation_8)); }
	inline LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319 * get_m_LightEstimation_8() const { return ___m_LightEstimation_8; }
	inline LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319 ** get_address_of_m_LightEstimation_8() { return &___m_LightEstimation_8; }
	inline void set_m_LightEstimation_8(LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319 * value)
	{
		___m_LightEstimation_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_LightEstimation_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTESTIMATIONUI_TABE99F2D6E580AB4AD14174CE323C527882D6DA4_H
#ifndef LOADASSETS_T041E16E022E10FE751B14E752B3E86D51C4ECC50_H
#define LOADASSETS_T041E16E022E10FE751B14E752B3E86D51C4ECC50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadAssets
struct  LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String LoadAssets::assetBundleName
	String_t* ___assetBundleName_5;
	// System.String LoadAssets::assetName
	String_t* ___assetName_6;

public:
	inline static int32_t get_offset_of_assetBundleName_5() { return static_cast<int32_t>(offsetof(LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50, ___assetBundleName_5)); }
	inline String_t* get_assetBundleName_5() const { return ___assetBundleName_5; }
	inline String_t** get_address_of_assetBundleName_5() { return &___assetBundleName_5; }
	inline void set_assetBundleName_5(String_t* value)
	{
		___assetBundleName_5 = value;
		Il2CppCodeGenWriteBarrier((&___assetBundleName_5), value);
	}

	inline static int32_t get_offset_of_assetName_6() { return static_cast<int32_t>(offsetof(LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50, ___assetName_6)); }
	inline String_t* get_assetName_6() const { return ___assetName_6; }
	inline String_t** get_address_of_assetName_6() { return &___assetName_6; }
	inline void set_assetName_6(String_t* value)
	{
		___assetName_6 = value;
		Il2CppCodeGenWriteBarrier((&___assetName_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADASSETS_T041E16E022E10FE751B14E752B3E86D51C4ECC50_H
#ifndef LOADSCENES_T6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0_H
#define LOADSCENES_T6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadScenes
struct  LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String LoadScenes::sceneAssetBundle
	String_t* ___sceneAssetBundle_4;
	// System.String LoadScenes::sceneName
	String_t* ___sceneName_5;

public:
	inline static int32_t get_offset_of_sceneAssetBundle_4() { return static_cast<int32_t>(offsetof(LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0, ___sceneAssetBundle_4)); }
	inline String_t* get_sceneAssetBundle_4() const { return ___sceneAssetBundle_4; }
	inline String_t** get_address_of_sceneAssetBundle_4() { return &___sceneAssetBundle_4; }
	inline void set_sceneAssetBundle_4(String_t* value)
	{
		___sceneAssetBundle_4 = value;
		Il2CppCodeGenWriteBarrier((&___sceneAssetBundle_4), value);
	}

	inline static int32_t get_offset_of_sceneName_5() { return static_cast<int32_t>(offsetof(LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0, ___sceneName_5)); }
	inline String_t* get_sceneName_5() const { return ___sceneName_5; }
	inline String_t** get_address_of_sceneName_5() { return &___sceneName_5; }
	inline void set_sceneName_5(String_t* value)
	{
		___sceneName_5 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENES_T6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0_H
#ifndef LOADTANKS_T879582AEE457760FDE6E59833ECB2453EEEA3D0B_H
#define LOADTANKS_T879582AEE457760FDE6E59833ECB2453EEEA3D0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadTanks
struct  LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String LoadTanks::sceneAssetBundle
	String_t* ___sceneAssetBundle_4;
	// System.String LoadTanks::sceneName
	String_t* ___sceneName_5;
	// System.String LoadTanks::textAssetBundle
	String_t* ___textAssetBundle_6;
	// System.String LoadTanks::textAssetName
	String_t* ___textAssetName_7;
	// System.String[] LoadTanks::activeVariants
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___activeVariants_8;
	// System.Boolean LoadTanks::bundlesLoaded
	bool ___bundlesLoaded_9;
	// System.Boolean LoadTanks::sd
	bool ___sd_10;
	// System.Boolean LoadTanks::hd
	bool ___hd_11;
	// System.Boolean LoadTanks::normal
	bool ___normal_12;
	// System.Boolean LoadTanks::desert
	bool ___desert_13;
	// System.Boolean LoadTanks::english
	bool ___english_14;
	// System.Boolean LoadTanks::danish
	bool ___danish_15;
	// System.String LoadTanks::tankAlbedoStyle
	String_t* ___tankAlbedoStyle_16;
	// System.String LoadTanks::tankAlbedoResolution
	String_t* ___tankAlbedoResolution_17;
	// System.String LoadTanks::language
	String_t* ___language_18;

public:
	inline static int32_t get_offset_of_sceneAssetBundle_4() { return static_cast<int32_t>(offsetof(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B, ___sceneAssetBundle_4)); }
	inline String_t* get_sceneAssetBundle_4() const { return ___sceneAssetBundle_4; }
	inline String_t** get_address_of_sceneAssetBundle_4() { return &___sceneAssetBundle_4; }
	inline void set_sceneAssetBundle_4(String_t* value)
	{
		___sceneAssetBundle_4 = value;
		Il2CppCodeGenWriteBarrier((&___sceneAssetBundle_4), value);
	}

	inline static int32_t get_offset_of_sceneName_5() { return static_cast<int32_t>(offsetof(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B, ___sceneName_5)); }
	inline String_t* get_sceneName_5() const { return ___sceneName_5; }
	inline String_t** get_address_of_sceneName_5() { return &___sceneName_5; }
	inline void set_sceneName_5(String_t* value)
	{
		___sceneName_5 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_5), value);
	}

	inline static int32_t get_offset_of_textAssetBundle_6() { return static_cast<int32_t>(offsetof(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B, ___textAssetBundle_6)); }
	inline String_t* get_textAssetBundle_6() const { return ___textAssetBundle_6; }
	inline String_t** get_address_of_textAssetBundle_6() { return &___textAssetBundle_6; }
	inline void set_textAssetBundle_6(String_t* value)
	{
		___textAssetBundle_6 = value;
		Il2CppCodeGenWriteBarrier((&___textAssetBundle_6), value);
	}

	inline static int32_t get_offset_of_textAssetName_7() { return static_cast<int32_t>(offsetof(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B, ___textAssetName_7)); }
	inline String_t* get_textAssetName_7() const { return ___textAssetName_7; }
	inline String_t** get_address_of_textAssetName_7() { return &___textAssetName_7; }
	inline void set_textAssetName_7(String_t* value)
	{
		___textAssetName_7 = value;
		Il2CppCodeGenWriteBarrier((&___textAssetName_7), value);
	}

	inline static int32_t get_offset_of_activeVariants_8() { return static_cast<int32_t>(offsetof(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B, ___activeVariants_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_activeVariants_8() const { return ___activeVariants_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_activeVariants_8() { return &___activeVariants_8; }
	inline void set_activeVariants_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___activeVariants_8 = value;
		Il2CppCodeGenWriteBarrier((&___activeVariants_8), value);
	}

	inline static int32_t get_offset_of_bundlesLoaded_9() { return static_cast<int32_t>(offsetof(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B, ___bundlesLoaded_9)); }
	inline bool get_bundlesLoaded_9() const { return ___bundlesLoaded_9; }
	inline bool* get_address_of_bundlesLoaded_9() { return &___bundlesLoaded_9; }
	inline void set_bundlesLoaded_9(bool value)
	{
		___bundlesLoaded_9 = value;
	}

	inline static int32_t get_offset_of_sd_10() { return static_cast<int32_t>(offsetof(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B, ___sd_10)); }
	inline bool get_sd_10() const { return ___sd_10; }
	inline bool* get_address_of_sd_10() { return &___sd_10; }
	inline void set_sd_10(bool value)
	{
		___sd_10 = value;
	}

	inline static int32_t get_offset_of_hd_11() { return static_cast<int32_t>(offsetof(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B, ___hd_11)); }
	inline bool get_hd_11() const { return ___hd_11; }
	inline bool* get_address_of_hd_11() { return &___hd_11; }
	inline void set_hd_11(bool value)
	{
		___hd_11 = value;
	}

	inline static int32_t get_offset_of_normal_12() { return static_cast<int32_t>(offsetof(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B, ___normal_12)); }
	inline bool get_normal_12() const { return ___normal_12; }
	inline bool* get_address_of_normal_12() { return &___normal_12; }
	inline void set_normal_12(bool value)
	{
		___normal_12 = value;
	}

	inline static int32_t get_offset_of_desert_13() { return static_cast<int32_t>(offsetof(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B, ___desert_13)); }
	inline bool get_desert_13() const { return ___desert_13; }
	inline bool* get_address_of_desert_13() { return &___desert_13; }
	inline void set_desert_13(bool value)
	{
		___desert_13 = value;
	}

	inline static int32_t get_offset_of_english_14() { return static_cast<int32_t>(offsetof(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B, ___english_14)); }
	inline bool get_english_14() const { return ___english_14; }
	inline bool* get_address_of_english_14() { return &___english_14; }
	inline void set_english_14(bool value)
	{
		___english_14 = value;
	}

	inline static int32_t get_offset_of_danish_15() { return static_cast<int32_t>(offsetof(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B, ___danish_15)); }
	inline bool get_danish_15() const { return ___danish_15; }
	inline bool* get_address_of_danish_15() { return &___danish_15; }
	inline void set_danish_15(bool value)
	{
		___danish_15 = value;
	}

	inline static int32_t get_offset_of_tankAlbedoStyle_16() { return static_cast<int32_t>(offsetof(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B, ___tankAlbedoStyle_16)); }
	inline String_t* get_tankAlbedoStyle_16() const { return ___tankAlbedoStyle_16; }
	inline String_t** get_address_of_tankAlbedoStyle_16() { return &___tankAlbedoStyle_16; }
	inline void set_tankAlbedoStyle_16(String_t* value)
	{
		___tankAlbedoStyle_16 = value;
		Il2CppCodeGenWriteBarrier((&___tankAlbedoStyle_16), value);
	}

	inline static int32_t get_offset_of_tankAlbedoResolution_17() { return static_cast<int32_t>(offsetof(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B, ___tankAlbedoResolution_17)); }
	inline String_t* get_tankAlbedoResolution_17() const { return ___tankAlbedoResolution_17; }
	inline String_t** get_address_of_tankAlbedoResolution_17() { return &___tankAlbedoResolution_17; }
	inline void set_tankAlbedoResolution_17(String_t* value)
	{
		___tankAlbedoResolution_17 = value;
		Il2CppCodeGenWriteBarrier((&___tankAlbedoResolution_17), value);
	}

	inline static int32_t get_offset_of_language_18() { return static_cast<int32_t>(offsetof(LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B, ___language_18)); }
	inline String_t* get_language_18() const { return ___language_18; }
	inline String_t** get_address_of_language_18() { return &___language_18; }
	inline void set_language_18(String_t* value)
	{
		___language_18 = value;
		Il2CppCodeGenWriteBarrier((&___language_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADTANKS_T879582AEE457760FDE6E59833ECB2453EEEA3D0B_H
#ifndef LOADVARIANTS_T9063903916BA659A2ED9EE14431E9C99A7D557C7_H
#define LOADVARIANTS_T9063903916BA659A2ED9EE14431E9C99A7D557C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadVariants
struct  LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] LoadVariants::activeVariants
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___activeVariants_6;
	// System.Boolean LoadVariants::bundlesLoaded
	bool ___bundlesLoaded_7;

public:
	inline static int32_t get_offset_of_activeVariants_6() { return static_cast<int32_t>(offsetof(LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7, ___activeVariants_6)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_activeVariants_6() const { return ___activeVariants_6; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_activeVariants_6() { return &___activeVariants_6; }
	inline void set_activeVariants_6(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___activeVariants_6 = value;
		Il2CppCodeGenWriteBarrier((&___activeVariants_6), value);
	}

	inline static int32_t get_offset_of_bundlesLoaded_7() { return static_cast<int32_t>(offsetof(LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7, ___bundlesLoaded_7)); }
	inline bool get_bundlesLoaded_7() const { return ___bundlesLoaded_7; }
	inline bool* get_address_of_bundlesLoaded_7() { return &___bundlesLoaded_7; }
	inline void set_bundlesLoaded_7(bool value)
	{
		___bundlesLoaded_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADVARIANTS_T9063903916BA659A2ED9EE14431E9C99A7D557C7_H
#ifndef MENUITEMOPTIONS_TEB6F74CE11B07EBBA645666DA29F91ADE69080F7_H
#define MENUITEMOPTIONS_TEB6F74CE11B07EBBA645666DA29F91ADE69080F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuItemOptions
struct  MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button MenuItemOptions::DeleteButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___DeleteButton_4;
	// UnityEngine.GameObject MenuItemOptions::BackBottomButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___BackBottomButton_5;
	// UnityEngine.GameObject MenuItemOptions::DetailBottomButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DetailBottomButton_6;
	// UnityEngine.UI.RawImage MenuItemOptions::QRCodeImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___QRCodeImage_7;
	// UnityEngine.UI.RawImage MenuItemOptions::ThumbnailImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___ThumbnailImage_8;
	// UnityEngine.UI.Text MenuItemOptions::NameText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___NameText_9;
	// UnityEngine.UI.Text MenuItemOptions::AuthorText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___AuthorText_10;
	// UnityEngine.UI.Text MenuItemOptions::VersionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___VersionText_11;
	// UnityEngine.UI.Text MenuItemOptions::DescriptionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___DescriptionText_12;
	// UnityEngine.GameObject MenuItemOptions::MainMenuObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___MainMenuObject_13;
	// UnityEngine.Color MenuItemOptions::normalPanelColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___normalPanelColor_14;
	// UnityEngine.Color MenuItemOptions::confirmDeletePanelColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___confirmDeletePanelColor_15;
	// System.Action MenuItemOptions::onBackButtonPressed
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onBackButtonPressed_16;
	// MPAR.Common.Scripting.Script MenuItemOptions::<CurrentScript>k__BackingField
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___U3CCurrentScriptU3Ek__BackingField_17;
	// MPAR.Common.Persistence.ISaveLoadManager MenuItemOptions::<SaveLoadManager>k__BackingField
	RuntimeObject* ___U3CSaveLoadManagerU3Ek__BackingField_18;
	// MPAR.Common.UI.IPlatformDependentScriptMenu MenuItemOptions::<ScriptMenu>k__BackingField
	RuntimeObject* ___U3CScriptMenuU3Ek__BackingField_19;
	// UnityEngine.Vector2 MenuItemOptions::resolution
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___resolution_20;

public:
	inline static int32_t get_offset_of_DeleteButton_4() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___DeleteButton_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_DeleteButton_4() const { return ___DeleteButton_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_DeleteButton_4() { return &___DeleteButton_4; }
	inline void set_DeleteButton_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___DeleteButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___DeleteButton_4), value);
	}

	inline static int32_t get_offset_of_BackBottomButton_5() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___BackBottomButton_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_BackBottomButton_5() const { return ___BackBottomButton_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_BackBottomButton_5() { return &___BackBottomButton_5; }
	inline void set_BackBottomButton_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___BackBottomButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___BackBottomButton_5), value);
	}

	inline static int32_t get_offset_of_DetailBottomButton_6() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___DetailBottomButton_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DetailBottomButton_6() const { return ___DetailBottomButton_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DetailBottomButton_6() { return &___DetailBottomButton_6; }
	inline void set_DetailBottomButton_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DetailBottomButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___DetailBottomButton_6), value);
	}

	inline static int32_t get_offset_of_QRCodeImage_7() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___QRCodeImage_7)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_QRCodeImage_7() const { return ___QRCodeImage_7; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_QRCodeImage_7() { return &___QRCodeImage_7; }
	inline void set_QRCodeImage_7(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___QRCodeImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___QRCodeImage_7), value);
	}

	inline static int32_t get_offset_of_ThumbnailImage_8() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___ThumbnailImage_8)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_ThumbnailImage_8() const { return ___ThumbnailImage_8; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_ThumbnailImage_8() { return &___ThumbnailImage_8; }
	inline void set_ThumbnailImage_8(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___ThumbnailImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___ThumbnailImage_8), value);
	}

	inline static int32_t get_offset_of_NameText_9() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___NameText_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_NameText_9() const { return ___NameText_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_NameText_9() { return &___NameText_9; }
	inline void set_NameText_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___NameText_9 = value;
		Il2CppCodeGenWriteBarrier((&___NameText_9), value);
	}

	inline static int32_t get_offset_of_AuthorText_10() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___AuthorText_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_AuthorText_10() const { return ___AuthorText_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_AuthorText_10() { return &___AuthorText_10; }
	inline void set_AuthorText_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___AuthorText_10 = value;
		Il2CppCodeGenWriteBarrier((&___AuthorText_10), value);
	}

	inline static int32_t get_offset_of_VersionText_11() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___VersionText_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_VersionText_11() const { return ___VersionText_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_VersionText_11() { return &___VersionText_11; }
	inline void set_VersionText_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___VersionText_11 = value;
		Il2CppCodeGenWriteBarrier((&___VersionText_11), value);
	}

	inline static int32_t get_offset_of_DescriptionText_12() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___DescriptionText_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_DescriptionText_12() const { return ___DescriptionText_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_DescriptionText_12() { return &___DescriptionText_12; }
	inline void set_DescriptionText_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___DescriptionText_12 = value;
		Il2CppCodeGenWriteBarrier((&___DescriptionText_12), value);
	}

	inline static int32_t get_offset_of_MainMenuObject_13() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___MainMenuObject_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_MainMenuObject_13() const { return ___MainMenuObject_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_MainMenuObject_13() { return &___MainMenuObject_13; }
	inline void set_MainMenuObject_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___MainMenuObject_13 = value;
		Il2CppCodeGenWriteBarrier((&___MainMenuObject_13), value);
	}

	inline static int32_t get_offset_of_normalPanelColor_14() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___normalPanelColor_14)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_normalPanelColor_14() const { return ___normalPanelColor_14; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_normalPanelColor_14() { return &___normalPanelColor_14; }
	inline void set_normalPanelColor_14(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___normalPanelColor_14 = value;
	}

	inline static int32_t get_offset_of_confirmDeletePanelColor_15() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___confirmDeletePanelColor_15)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_confirmDeletePanelColor_15() const { return ___confirmDeletePanelColor_15; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_confirmDeletePanelColor_15() { return &___confirmDeletePanelColor_15; }
	inline void set_confirmDeletePanelColor_15(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___confirmDeletePanelColor_15 = value;
	}

	inline static int32_t get_offset_of_onBackButtonPressed_16() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___onBackButtonPressed_16)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onBackButtonPressed_16() const { return ___onBackButtonPressed_16; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onBackButtonPressed_16() { return &___onBackButtonPressed_16; }
	inline void set_onBackButtonPressed_16(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onBackButtonPressed_16 = value;
		Il2CppCodeGenWriteBarrier((&___onBackButtonPressed_16), value);
	}

	inline static int32_t get_offset_of_U3CCurrentScriptU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___U3CCurrentScriptU3Ek__BackingField_17)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_U3CCurrentScriptU3Ek__BackingField_17() const { return ___U3CCurrentScriptU3Ek__BackingField_17; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_U3CCurrentScriptU3Ek__BackingField_17() { return &___U3CCurrentScriptU3Ek__BackingField_17; }
	inline void set_U3CCurrentScriptU3Ek__BackingField_17(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___U3CCurrentScriptU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentScriptU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CSaveLoadManagerU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___U3CSaveLoadManagerU3Ek__BackingField_18)); }
	inline RuntimeObject* get_U3CSaveLoadManagerU3Ek__BackingField_18() const { return ___U3CSaveLoadManagerU3Ek__BackingField_18; }
	inline RuntimeObject** get_address_of_U3CSaveLoadManagerU3Ek__BackingField_18() { return &___U3CSaveLoadManagerU3Ek__BackingField_18; }
	inline void set_U3CSaveLoadManagerU3Ek__BackingField_18(RuntimeObject* value)
	{
		___U3CSaveLoadManagerU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSaveLoadManagerU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CScriptMenuU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___U3CScriptMenuU3Ek__BackingField_19)); }
	inline RuntimeObject* get_U3CScriptMenuU3Ek__BackingField_19() const { return ___U3CScriptMenuU3Ek__BackingField_19; }
	inline RuntimeObject** get_address_of_U3CScriptMenuU3Ek__BackingField_19() { return &___U3CScriptMenuU3Ek__BackingField_19; }
	inline void set_U3CScriptMenuU3Ek__BackingField_19(RuntimeObject* value)
	{
		___U3CScriptMenuU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScriptMenuU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_resolution_20() { return static_cast<int32_t>(offsetof(MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7, ___resolution_20)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_resolution_20() const { return ___resolution_20; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_resolution_20() { return &___resolution_20; }
	inline void set_resolution_20(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___resolution_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUITEMOPTIONS_TEB6F74CE11B07EBBA645666DA29F91ADE69080F7_H
#ifndef MOBILESCANMENU_T94E40E5F244F7A8B5AEA99C337E011B2089C9AC7_H
#define MOBILESCANMENU_T94E40E5F244F7A8B5AEA99C337E011B2089C9AC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobileScanMenu
struct  MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject MobileScanMenu::BackToMenuButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___BackToMenuButton_4;
	// UnityEngine.UI.Button MobileScanMenu::ScanButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___ScanButton_5;
	// UnityEngine.UI.Button MobileScanMenu::MenuButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___MenuButton_6;
	// System.Int32 MobileScanMenu::horizontalPadding
	int32_t ___horizontalPadding_7;
	// System.Int32 MobileScanMenu::verticalPadding
	int32_t ___verticalPadding_8;
	// System.Single MobileScanMenu::spacing
	float ___spacing_9;
	// System.Int32 MobileScanMenu::lastWidth
	int32_t ___lastWidth_10;
	// System.Int32 MobileScanMenu::lastHeight
	int32_t ___lastHeight_11;

public:
	inline static int32_t get_offset_of_BackToMenuButton_4() { return static_cast<int32_t>(offsetof(MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7, ___BackToMenuButton_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_BackToMenuButton_4() const { return ___BackToMenuButton_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_BackToMenuButton_4() { return &___BackToMenuButton_4; }
	inline void set_BackToMenuButton_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___BackToMenuButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___BackToMenuButton_4), value);
	}

	inline static int32_t get_offset_of_ScanButton_5() { return static_cast<int32_t>(offsetof(MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7, ___ScanButton_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_ScanButton_5() const { return ___ScanButton_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_ScanButton_5() { return &___ScanButton_5; }
	inline void set_ScanButton_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___ScanButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___ScanButton_5), value);
	}

	inline static int32_t get_offset_of_MenuButton_6() { return static_cast<int32_t>(offsetof(MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7, ___MenuButton_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_MenuButton_6() const { return ___MenuButton_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_MenuButton_6() { return &___MenuButton_6; }
	inline void set_MenuButton_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___MenuButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___MenuButton_6), value);
	}

	inline static int32_t get_offset_of_horizontalPadding_7() { return static_cast<int32_t>(offsetof(MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7, ___horizontalPadding_7)); }
	inline int32_t get_horizontalPadding_7() const { return ___horizontalPadding_7; }
	inline int32_t* get_address_of_horizontalPadding_7() { return &___horizontalPadding_7; }
	inline void set_horizontalPadding_7(int32_t value)
	{
		___horizontalPadding_7 = value;
	}

	inline static int32_t get_offset_of_verticalPadding_8() { return static_cast<int32_t>(offsetof(MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7, ___verticalPadding_8)); }
	inline int32_t get_verticalPadding_8() const { return ___verticalPadding_8; }
	inline int32_t* get_address_of_verticalPadding_8() { return &___verticalPadding_8; }
	inline void set_verticalPadding_8(int32_t value)
	{
		___verticalPadding_8 = value;
	}

	inline static int32_t get_offset_of_spacing_9() { return static_cast<int32_t>(offsetof(MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7, ___spacing_9)); }
	inline float get_spacing_9() const { return ___spacing_9; }
	inline float* get_address_of_spacing_9() { return &___spacing_9; }
	inline void set_spacing_9(float value)
	{
		___spacing_9 = value;
	}

	inline static int32_t get_offset_of_lastWidth_10() { return static_cast<int32_t>(offsetof(MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7, ___lastWidth_10)); }
	inline int32_t get_lastWidth_10() const { return ___lastWidth_10; }
	inline int32_t* get_address_of_lastWidth_10() { return &___lastWidth_10; }
	inline void set_lastWidth_10(int32_t value)
	{
		___lastWidth_10 = value;
	}

	inline static int32_t get_offset_of_lastHeight_11() { return static_cast<int32_t>(offsetof(MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7, ___lastHeight_11)); }
	inline int32_t get_lastHeight_11() const { return ___lastHeight_11; }
	inline int32_t* get_address_of_lastHeight_11() { return &___lastHeight_11; }
	inline void set_lastHeight_11(int32_t value)
	{
		___lastHeight_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILESCANMENU_T94E40E5F244F7A8B5AEA99C337E011B2089C9AC7_H
#ifndef OPTIONSBACKBUTTON_T8AE364F7F02A38DCE89E2228971F47F55F8CE6E9_H
#define OPTIONSBACKBUTTON_T8AE364F7F02A38DCE89E2228971F47F55F8CE6E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OptionsBackButton
struct  OptionsBackButton_t8AE364F7F02A38DCE89E2228971F47F55F8CE6E9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONSBACKBUTTON_T8AE364F7F02A38DCE89E2228971F47F55F8CE6E9_H
#ifndef SINGLETON_1_T700CAB15E305EB8EEE879E9CBBF8AB45B71663F0_H
#define SINGLETON_1_T700CAB15E305EB8EEE879E9CBBF8AB45B71663F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Patch.Singleton`1<Localizatron>
struct  Singleton_1_t700CAB15E305EB8EEE879E9CBBF8AB45B71663F0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t700CAB15E305EB8EEE879E9CBBF8AB45B71663F0_StaticFields
{
public:
	// T Patch.Singleton`1::_instance
	Localizatron_tC7AF71F3CADDA057CB4068F6260C5B90A047962E * ____instance_4;
	// System.Object Patch.Singleton`1::_lock
	RuntimeObject * ____lock_5;
	// System.Boolean Patch.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_6;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_t700CAB15E305EB8EEE879E9CBBF8AB45B71663F0_StaticFields, ____instance_4)); }
	inline Localizatron_tC7AF71F3CADDA057CB4068F6260C5B90A047962E * get__instance_4() const { return ____instance_4; }
	inline Localizatron_tC7AF71F3CADDA057CB4068F6260C5B90A047962E ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(Localizatron_tC7AF71F3CADDA057CB4068F6260C5B90A047962E * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((&____instance_4), value);
	}

	inline static int32_t get_offset_of__lock_5() { return static_cast<int32_t>(offsetof(Singleton_1_t700CAB15E305EB8EEE879E9CBBF8AB45B71663F0_StaticFields, ____lock_5)); }
	inline RuntimeObject * get__lock_5() const { return ____lock_5; }
	inline RuntimeObject ** get_address_of__lock_5() { return &____lock_5; }
	inline void set__lock_5(RuntimeObject * value)
	{
		____lock_5 = value;
		Il2CppCodeGenWriteBarrier((&____lock_5), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_6() { return static_cast<int32_t>(offsetof(Singleton_1_t700CAB15E305EB8EEE879E9CBBF8AB45B71663F0_StaticFields, ___applicationIsQuitting_6)); }
	inline bool get_applicationIsQuitting_6() const { return ___applicationIsQuitting_6; }
	inline bool* get_address_of_applicationIsQuitting_6() { return &___applicationIsQuitting_6; }
	inline void set_applicationIsQuitting_6(bool value)
	{
		___applicationIsQuitting_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T700CAB15E305EB8EEE879E9CBBF8AB45B71663F0_H
#ifndef PLACEMULTIPLEOBJECTSONPLANE_T776325CC089E260D234CC3B495856446CEB362CC_H
#define PLACEMULTIPLEOBJECTSONPLANE_T776325CC089E260D234CC3B495856446CEB362CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaceMultipleObjectsOnPlane
struct  PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PlaceMultipleObjectsOnPlane::m_PlacedPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_PlacedPrefab_4;
	// UnityEngine.GameObject PlaceMultipleObjectsOnPlane::<spawnedObject>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CspawnedObjectU3Ek__BackingField_5;
	// UnityEngine.XR.ARFoundation.ARSessionOrigin PlaceMultipleObjectsOnPlane::m_SessionOrigin
	ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * ___m_SessionOrigin_7;

public:
	inline static int32_t get_offset_of_m_PlacedPrefab_4() { return static_cast<int32_t>(offsetof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC, ___m_PlacedPrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_PlacedPrefab_4() const { return ___m_PlacedPrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_PlacedPrefab_4() { return &___m_PlacedPrefab_4; }
	inline void set_m_PlacedPrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_PlacedPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlacedPrefab_4), value);
	}

	inline static int32_t get_offset_of_U3CspawnedObjectU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC, ___U3CspawnedObjectU3Ek__BackingField_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CspawnedObjectU3Ek__BackingField_5() const { return ___U3CspawnedObjectU3Ek__BackingField_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CspawnedObjectU3Ek__BackingField_5() { return &___U3CspawnedObjectU3Ek__BackingField_5; }
	inline void set_U3CspawnedObjectU3Ek__BackingField_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CspawnedObjectU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CspawnedObjectU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_m_SessionOrigin_7() { return static_cast<int32_t>(offsetof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC, ___m_SessionOrigin_7)); }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * get_m_SessionOrigin_7() const { return ___m_SessionOrigin_7; }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF ** get_address_of_m_SessionOrigin_7() { return &___m_SessionOrigin_7; }
	inline void set_m_SessionOrigin_7(ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * value)
	{
		___m_SessionOrigin_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SessionOrigin_7), value);
	}
};

struct PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields
{
public:
	// System.Action PlaceMultipleObjectsOnPlane::onPlacedObject
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onPlacedObject_6;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit> PlaceMultipleObjectsOnPlane::s_Hits
	List_1_tEB129E1534E65DF322C5B5AAECF0CA8A562063F6 * ___s_Hits_8;

public:
	inline static int32_t get_offset_of_onPlacedObject_6() { return static_cast<int32_t>(offsetof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields, ___onPlacedObject_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onPlacedObject_6() const { return ___onPlacedObject_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onPlacedObject_6() { return &___onPlacedObject_6; }
	inline void set_onPlacedObject_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onPlacedObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPlacedObject_6), value);
	}

	inline static int32_t get_offset_of_s_Hits_8() { return static_cast<int32_t>(offsetof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields, ___s_Hits_8)); }
	inline List_1_tEB129E1534E65DF322C5B5AAECF0CA8A562063F6 * get_s_Hits_8() const { return ___s_Hits_8; }
	inline List_1_tEB129E1534E65DF322C5B5AAECF0CA8A562063F6 ** get_address_of_s_Hits_8() { return &___s_Hits_8; }
	inline void set_s_Hits_8(List_1_tEB129E1534E65DF322C5B5AAECF0CA8A562063F6 * value)
	{
		___s_Hits_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_Hits_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEMULTIPLEOBJECTSONPLANE_T776325CC089E260D234CC3B495856446CEB362CC_H
#ifndef PLACEONPLANE_TC5323EAA9F7603ADAB59DB462174996553CAF0C2_H
#define PLACEONPLANE_TC5323EAA9F7603ADAB59DB462174996553CAF0C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaceOnPlane
struct  PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PlaceOnPlane::m_PlacedPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_PlacedPrefab_4;
	// UnityEngine.GameObject PlaceOnPlane::<spawnedObject>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CspawnedObjectU3Ek__BackingField_5;
	// UnityEngine.XR.ARFoundation.ARSessionOrigin PlaceOnPlane::m_SessionOrigin
	ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * ___m_SessionOrigin_7;

public:
	inline static int32_t get_offset_of_m_PlacedPrefab_4() { return static_cast<int32_t>(offsetof(PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2, ___m_PlacedPrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_PlacedPrefab_4() const { return ___m_PlacedPrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_PlacedPrefab_4() { return &___m_PlacedPrefab_4; }
	inline void set_m_PlacedPrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_PlacedPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlacedPrefab_4), value);
	}

	inline static int32_t get_offset_of_U3CspawnedObjectU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2, ___U3CspawnedObjectU3Ek__BackingField_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CspawnedObjectU3Ek__BackingField_5() const { return ___U3CspawnedObjectU3Ek__BackingField_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CspawnedObjectU3Ek__BackingField_5() { return &___U3CspawnedObjectU3Ek__BackingField_5; }
	inline void set_U3CspawnedObjectU3Ek__BackingField_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CspawnedObjectU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CspawnedObjectU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_m_SessionOrigin_7() { return static_cast<int32_t>(offsetof(PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2, ___m_SessionOrigin_7)); }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * get_m_SessionOrigin_7() const { return ___m_SessionOrigin_7; }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF ** get_address_of_m_SessionOrigin_7() { return &___m_SessionOrigin_7; }
	inline void set_m_SessionOrigin_7(ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * value)
	{
		___m_SessionOrigin_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SessionOrigin_7), value);
	}
};

struct PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit> PlaceOnPlane::s_Hits
	List_1_tEB129E1534E65DF322C5B5AAECF0CA8A562063F6 * ___s_Hits_6;

public:
	inline static int32_t get_offset_of_s_Hits_6() { return static_cast<int32_t>(offsetof(PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2_StaticFields, ___s_Hits_6)); }
	inline List_1_tEB129E1534E65DF322C5B5AAECF0CA8A562063F6 * get_s_Hits_6() const { return ___s_Hits_6; }
	inline List_1_tEB129E1534E65DF322C5B5AAECF0CA8A562063F6 ** get_address_of_s_Hits_6() { return &___s_Hits_6; }
	inline void set_s_Hits_6(List_1_tEB129E1534E65DF322C5B5AAECF0CA8A562063F6 * value)
	{
		___s_Hits_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Hits_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEONPLANE_TC5323EAA9F7603ADAB59DB462174996553CAF0C2_H
#ifndef PLANEDETECTIONCONTROLLER_TF91A38E8CF0739882A647588DB89B4E0B91482DD_H
#define PLANEDETECTIONCONTROLLER_TF91A38E8CF0739882A647588DB89B4E0B91482DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneDetectionController
struct  PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text PlaneDetectionController::m_TogglePlaneDetectionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_TogglePlaneDetectionText_4;
	// UnityEngine.XR.ARFoundation.ARPlaneManager PlaneDetectionController::m_ARPlaneManager
	ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * ___m_ARPlaneManager_5;

public:
	inline static int32_t get_offset_of_m_TogglePlaneDetectionText_4() { return static_cast<int32_t>(offsetof(PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD, ___m_TogglePlaneDetectionText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_TogglePlaneDetectionText_4() const { return ___m_TogglePlaneDetectionText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_TogglePlaneDetectionText_4() { return &___m_TogglePlaneDetectionText_4; }
	inline void set_m_TogglePlaneDetectionText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_TogglePlaneDetectionText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TogglePlaneDetectionText_4), value);
	}

	inline static int32_t get_offset_of_m_ARPlaneManager_5() { return static_cast<int32_t>(offsetof(PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD, ___m_ARPlaneManager_5)); }
	inline ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * get_m_ARPlaneManager_5() const { return ___m_ARPlaneManager_5; }
	inline ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A ** get_address_of_m_ARPlaneManager_5() { return &___m_ARPlaneManager_5; }
	inline void set_m_ARPlaneManager_5(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * value)
	{
		___m_ARPlaneManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ARPlaneManager_5), value);
	}
};

struct PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane> PlaneDetectionController::s_Planes
	List_1_tCD714E0010765A4D65BD80974698E4FA9EFBC2B6 * ___s_Planes_6;

public:
	inline static int32_t get_offset_of_s_Planes_6() { return static_cast<int32_t>(offsetof(PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD_StaticFields, ___s_Planes_6)); }
	inline List_1_tCD714E0010765A4D65BD80974698E4FA9EFBC2B6 * get_s_Planes_6() const { return ___s_Planes_6; }
	inline List_1_tCD714E0010765A4D65BD80974698E4FA9EFBC2B6 ** get_address_of_s_Planes_6() { return &___s_Planes_6; }
	inline void set_s_Planes_6(List_1_tCD714E0010765A4D65BD80974698E4FA9EFBC2B6 * value)
	{
		___s_Planes_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Planes_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANEDETECTIONCONTROLLER_TF91A38E8CF0739882A647588DB89B4E0B91482DD_H
#ifndef SCANVIEW_TCB5272968F947E75D63CF3AC05499234FF5769AB_H
#define SCANVIEW_TCB5272968F947E75D63CF3AC05499234FF5769AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScanView
struct  ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean ScanView::<Running>k__BackingField
	bool ___U3CRunningU3Ek__BackingField_4;
	// System.Single ScanView::<LastInvoke>k__BackingField
	float ___U3CLastInvokeU3Ek__BackingField_5;
	// System.Single ScanView::<InvokeInterval>k__BackingField
	float ___U3CInvokeIntervalU3Ek__BackingField_6;
	// BarcodeScanner.Parser.ZXingParser ScanView::<Parser>k__BackingField
	ZXingParser_t7B9AC8160C3A1565FA96F8714D30B9C7E852DE26 * ___U3CParserU3Ek__BackingField_7;
	// System.String ScanView::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_8;
	// System.Boolean ScanView::InProgress
	bool ___InProgress_9;
	// UnityEngine.Vector2 ScanView::minScanAnchor
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___minScanAnchor_10;
	// UnityEngine.Vector2 ScanView::maxScanAnchor
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___maxScanAnchor_11;
	// UnityEngine.Vector2 ScanView::imageSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___imageSize_12;
	// UnityEngine.Vector2 ScanView::resolution
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___resolution_13;
	// UnityEngine.Texture2D ScanView::tex
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___tex_14;
	// UnityEngine.Texture2D ScanView::Image
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Image_15;
	// UnityEngine.Texture2D ScanView::m_Texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_Texture_16;
	// LoadingIndicator ScanView::LoadingIndicator
	LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB * ___LoadingIndicator_17;
	// System.Boolean ScanView::startedScanning
	bool ___startedScanning_18;

public:
	inline static int32_t get_offset_of_U3CRunningU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB, ___U3CRunningU3Ek__BackingField_4)); }
	inline bool get_U3CRunningU3Ek__BackingField_4() const { return ___U3CRunningU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CRunningU3Ek__BackingField_4() { return &___U3CRunningU3Ek__BackingField_4; }
	inline void set_U3CRunningU3Ek__BackingField_4(bool value)
	{
		___U3CRunningU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CLastInvokeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB, ___U3CLastInvokeU3Ek__BackingField_5)); }
	inline float get_U3CLastInvokeU3Ek__BackingField_5() const { return ___U3CLastInvokeU3Ek__BackingField_5; }
	inline float* get_address_of_U3CLastInvokeU3Ek__BackingField_5() { return &___U3CLastInvokeU3Ek__BackingField_5; }
	inline void set_U3CLastInvokeU3Ek__BackingField_5(float value)
	{
		___U3CLastInvokeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CInvokeIntervalU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB, ___U3CInvokeIntervalU3Ek__BackingField_6)); }
	inline float get_U3CInvokeIntervalU3Ek__BackingField_6() const { return ___U3CInvokeIntervalU3Ek__BackingField_6; }
	inline float* get_address_of_U3CInvokeIntervalU3Ek__BackingField_6() { return &___U3CInvokeIntervalU3Ek__BackingField_6; }
	inline void set_U3CInvokeIntervalU3Ek__BackingField_6(float value)
	{
		___U3CInvokeIntervalU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CParserU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB, ___U3CParserU3Ek__BackingField_7)); }
	inline ZXingParser_t7B9AC8160C3A1565FA96F8714D30B9C7E852DE26 * get_U3CParserU3Ek__BackingField_7() const { return ___U3CParserU3Ek__BackingField_7; }
	inline ZXingParser_t7B9AC8160C3A1565FA96F8714D30B9C7E852DE26 ** get_address_of_U3CParserU3Ek__BackingField_7() { return &___U3CParserU3Ek__BackingField_7; }
	inline void set_U3CParserU3Ek__BackingField_7(ZXingParser_t7B9AC8160C3A1565FA96F8714D30B9C7E852DE26 * value)
	{
		___U3CParserU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParserU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB, ___U3CValueU3Ek__BackingField_8)); }
	inline String_t* get_U3CValueU3Ek__BackingField_8() const { return ___U3CValueU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_8() { return &___U3CValueU3Ek__BackingField_8; }
	inline void set_U3CValueU3Ek__BackingField_8(String_t* value)
	{
		___U3CValueU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_InProgress_9() { return static_cast<int32_t>(offsetof(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB, ___InProgress_9)); }
	inline bool get_InProgress_9() const { return ___InProgress_9; }
	inline bool* get_address_of_InProgress_9() { return &___InProgress_9; }
	inline void set_InProgress_9(bool value)
	{
		___InProgress_9 = value;
	}

	inline static int32_t get_offset_of_minScanAnchor_10() { return static_cast<int32_t>(offsetof(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB, ___minScanAnchor_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_minScanAnchor_10() const { return ___minScanAnchor_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_minScanAnchor_10() { return &___minScanAnchor_10; }
	inline void set_minScanAnchor_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___minScanAnchor_10 = value;
	}

	inline static int32_t get_offset_of_maxScanAnchor_11() { return static_cast<int32_t>(offsetof(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB, ___maxScanAnchor_11)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_maxScanAnchor_11() const { return ___maxScanAnchor_11; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_maxScanAnchor_11() { return &___maxScanAnchor_11; }
	inline void set_maxScanAnchor_11(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___maxScanAnchor_11 = value;
	}

	inline static int32_t get_offset_of_imageSize_12() { return static_cast<int32_t>(offsetof(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB, ___imageSize_12)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_imageSize_12() const { return ___imageSize_12; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_imageSize_12() { return &___imageSize_12; }
	inline void set_imageSize_12(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___imageSize_12 = value;
	}

	inline static int32_t get_offset_of_resolution_13() { return static_cast<int32_t>(offsetof(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB, ___resolution_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_resolution_13() const { return ___resolution_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_resolution_13() { return &___resolution_13; }
	inline void set_resolution_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___resolution_13 = value;
	}

	inline static int32_t get_offset_of_tex_14() { return static_cast<int32_t>(offsetof(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB, ___tex_14)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_tex_14() const { return ___tex_14; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_tex_14() { return &___tex_14; }
	inline void set_tex_14(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___tex_14 = value;
		Il2CppCodeGenWriteBarrier((&___tex_14), value);
	}

	inline static int32_t get_offset_of_Image_15() { return static_cast<int32_t>(offsetof(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB, ___Image_15)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Image_15() const { return ___Image_15; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Image_15() { return &___Image_15; }
	inline void set_Image_15(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Image_15 = value;
		Il2CppCodeGenWriteBarrier((&___Image_15), value);
	}

	inline static int32_t get_offset_of_m_Texture_16() { return static_cast<int32_t>(offsetof(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB, ___m_Texture_16)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_m_Texture_16() const { return ___m_Texture_16; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_m_Texture_16() { return &___m_Texture_16; }
	inline void set_m_Texture_16(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___m_Texture_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_16), value);
	}

	inline static int32_t get_offset_of_LoadingIndicator_17() { return static_cast<int32_t>(offsetof(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB, ___LoadingIndicator_17)); }
	inline LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB * get_LoadingIndicator_17() const { return ___LoadingIndicator_17; }
	inline LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB ** get_address_of_LoadingIndicator_17() { return &___LoadingIndicator_17; }
	inline void set_LoadingIndicator_17(LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB * value)
	{
		___LoadingIndicator_17 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingIndicator_17), value);
	}

	inline static int32_t get_offset_of_startedScanning_18() { return static_cast<int32_t>(offsetof(ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB, ___startedScanning_18)); }
	inline bool get_startedScanning_18() const { return ___startedScanning_18; }
	inline bool* get_address_of_startedScanning_18() { return &___startedScanning_18; }
	inline void set_startedScanning_18(bool value)
	{
		___startedScanning_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANVIEW_TCB5272968F947E75D63CF3AC05499234FF5769AB_H
#ifndef SETTARGETFRAMERATE_TCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0_H
#define SETTARGETFRAMERATE_TCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetTargetFramerate
struct  SetTargetFramerate_tCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 SetTargetFramerate::m_TargetFrameRate
	int32_t ___m_TargetFrameRate_4;

public:
	inline static int32_t get_offset_of_m_TargetFrameRate_4() { return static_cast<int32_t>(offsetof(SetTargetFramerate_tCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0, ___m_TargetFrameRate_4)); }
	inline int32_t get_m_TargetFrameRate_4() const { return ___m_TargetFrameRate_4; }
	inline int32_t* get_address_of_m_TargetFrameRate_4() { return &___m_TargetFrameRate_4; }
	inline void set_m_TargetFrameRate_4(int32_t value)
	{
		___m_TargetFrameRate_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTARGETFRAMERATE_TCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0_H
#ifndef SHAREBUTTON_TA3C0A87B1AC8256D48333110C2C9D01979DC974B_H
#define SHAREBUTTON_TA3C0A87B1AC8256D48333110C2C9D01979DC974B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShareButton
struct  ShareButton_tA3C0A87B1AC8256D48333110C2C9D01979DC974B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREBUTTON_TA3C0A87B1AC8256D48333110C2C9D01979DC974B_H
#ifndef SWIPEMENUSIZE_T6BE6690EB91690BA2CA28A6B7D3AAE4F474E93F1_H
#define SWIPEMENUSIZE_T6BE6690EB91690BA2CA28A6B7D3AAE4F474E93F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SwipeMenuSize
struct  SwipeMenuSize_t6BE6690EB91690BA2CA28A6B7D3AAE4F474E93F1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 SwipeMenuSize::width
	int32_t ___width_4;
	// System.Int32 SwipeMenuSize::height
	int32_t ___height_5;
	// UnityEngine.UI.GridLayoutGroup SwipeMenuSize::buttonPanel
	GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * ___buttonPanel_6;

public:
	inline static int32_t get_offset_of_width_4() { return static_cast<int32_t>(offsetof(SwipeMenuSize_t6BE6690EB91690BA2CA28A6B7D3AAE4F474E93F1, ___width_4)); }
	inline int32_t get_width_4() const { return ___width_4; }
	inline int32_t* get_address_of_width_4() { return &___width_4; }
	inline void set_width_4(int32_t value)
	{
		___width_4 = value;
	}

	inline static int32_t get_offset_of_height_5() { return static_cast<int32_t>(offsetof(SwipeMenuSize_t6BE6690EB91690BA2CA28A6B7D3AAE4F474E93F1, ___height_5)); }
	inline int32_t get_height_5() const { return ___height_5; }
	inline int32_t* get_address_of_height_5() { return &___height_5; }
	inline void set_height_5(int32_t value)
	{
		___height_5 = value;
	}

	inline static int32_t get_offset_of_buttonPanel_6() { return static_cast<int32_t>(offsetof(SwipeMenuSize_t6BE6690EB91690BA2CA28A6B7D3AAE4F474E93F1, ___buttonPanel_6)); }
	inline GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * get_buttonPanel_6() const { return ___buttonPanel_6; }
	inline GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 ** get_address_of_buttonPanel_6() { return &___buttonPanel_6; }
	inline void set_buttonPanel_6(GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * value)
	{
		___buttonPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___buttonPanel_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEMENUSIZE_T6BE6690EB91690BA2CA28A6B7D3AAE4F474E93F1_H
#ifndef TESTCAMERAIMAGE_TB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6_H
#define TESTCAMERAIMAGE_TB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestCameraImage
struct  TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.RawImage TestCameraImage::m_RawImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___m_RawImage_4;
	// UnityEngine.UI.Text TestCameraImage::m_ImageInfo
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_ImageInfo_5;
	// UnityEngine.Texture2D TestCameraImage::m_Texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_Texture_6;

public:
	inline static int32_t get_offset_of_m_RawImage_4() { return static_cast<int32_t>(offsetof(TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6, ___m_RawImage_4)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_m_RawImage_4() const { return ___m_RawImage_4; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_m_RawImage_4() { return &___m_RawImage_4; }
	inline void set_m_RawImage_4(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___m_RawImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RawImage_4), value);
	}

	inline static int32_t get_offset_of_m_ImageInfo_5() { return static_cast<int32_t>(offsetof(TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6, ___m_ImageInfo_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_ImageInfo_5() const { return ___m_ImageInfo_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_ImageInfo_5() { return &___m_ImageInfo_5; }
	inline void set_m_ImageInfo_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_ImageInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ImageInfo_5), value);
	}

	inline static int32_t get_offset_of_m_Texture_6() { return static_cast<int32_t>(offsetof(TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6, ___m_Texture_6)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_m_Texture_6() const { return ___m_Texture_6; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_m_Texture_6() { return &___m_Texture_6; }
	inline void set_m_Texture_6(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___m_Texture_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTCAMERAIMAGE_TB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6_H
#ifndef TILEDSCRIPTMENUDISPLAY_T7234091EF72E6C896E3D9EF92BB91E3597AFCFCE_H
#define TILEDSCRIPTMENUDISPLAY_T7234091EF72E6C896E3D9EF92BB91E3597AFCFCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TiledScriptMenuDisplay
struct  TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RectTransform TiledScriptMenuDisplay::contentParent
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___contentParent_4;
	// Assets._Project.Scripts.Common.UI.ScriptTile TiledScriptMenuDisplay::menuItemPrefab
	ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E * ___menuItemPrefab_5;
	// UnityEngine.Vector2 TiledScriptMenuDisplay::resolution
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___resolution_6;
	// System.Int32 TiledScriptMenuDisplay::numberOfColumns
	int32_t ___numberOfColumns_7;
	// System.Single TiledScriptMenuDisplay::paddingX
	float ___paddingX_8;
	// System.Collections.Generic.List`1<MPAR.Common.Scripting.Script> TiledScriptMenuDisplay::scripts
	List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * ___scripts_9;
	// System.Collections.Generic.List`1<Assets._Project.Scripts.Common.UI.ScriptTile> TiledScriptMenuDisplay::menuItemObjects
	List_1_t738D3CE973A389B13AA52C796419D7B95FB33EC2 * ___menuItemObjects_10;
	// System.Boolean TiledScriptMenuDisplay::usingLocalScripts
	bool ___usingLocalScripts_11;
	// UnityEngine.RectTransform TiledScriptMenuDisplay::refreshIcon
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___refreshIcon_12;
	// System.Single TiledScriptMenuDisplay::normalizedPositionY
	float ___normalizedPositionY_13;
	// System.Single TiledScriptMenuDisplay::sizeY
	float ___sizeY_14;
	// System.Boolean TiledScriptMenuDisplay::refreshing
	bool ___refreshing_15;
	// System.Boolean TiledScriptMenuDisplay::waitingToStopRefreshing
	bool ___waitingToStopRefreshing_16;
	// System.Single TiledScriptMenuDisplay::waitingToStopRefreshingStartTime
	float ___waitingToStopRefreshingStartTime_17;
	// System.Boolean TiledScriptMenuDisplay::releasedTouchSinceRefresh
	bool ___releasedTouchSinceRefresh_18;
	// System.Single TiledScriptMenuDisplay::normalizedPositionYSpinThreshold
	float ___normalizedPositionYSpinThreshold_19;
	// System.Single TiledScriptMenuDisplay::normalizedPositionYRefreshThreshold
	float ___normalizedPositionYRefreshThreshold_20;
	// System.Single TiledScriptMenuDisplay::refreshRotationSpeed
	float ___refreshRotationSpeed_21;
	// UnityEngine.UI.ScrollRect TiledScriptMenuDisplay::scrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___scrollRect_22;

public:
	inline static int32_t get_offset_of_contentParent_4() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___contentParent_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_contentParent_4() const { return ___contentParent_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_contentParent_4() { return &___contentParent_4; }
	inline void set_contentParent_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___contentParent_4 = value;
		Il2CppCodeGenWriteBarrier((&___contentParent_4), value);
	}

	inline static int32_t get_offset_of_menuItemPrefab_5() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___menuItemPrefab_5)); }
	inline ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E * get_menuItemPrefab_5() const { return ___menuItemPrefab_5; }
	inline ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E ** get_address_of_menuItemPrefab_5() { return &___menuItemPrefab_5; }
	inline void set_menuItemPrefab_5(ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E * value)
	{
		___menuItemPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___menuItemPrefab_5), value);
	}

	inline static int32_t get_offset_of_resolution_6() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___resolution_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_resolution_6() const { return ___resolution_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_resolution_6() { return &___resolution_6; }
	inline void set_resolution_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___resolution_6 = value;
	}

	inline static int32_t get_offset_of_numberOfColumns_7() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___numberOfColumns_7)); }
	inline int32_t get_numberOfColumns_7() const { return ___numberOfColumns_7; }
	inline int32_t* get_address_of_numberOfColumns_7() { return &___numberOfColumns_7; }
	inline void set_numberOfColumns_7(int32_t value)
	{
		___numberOfColumns_7 = value;
	}

	inline static int32_t get_offset_of_paddingX_8() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___paddingX_8)); }
	inline float get_paddingX_8() const { return ___paddingX_8; }
	inline float* get_address_of_paddingX_8() { return &___paddingX_8; }
	inline void set_paddingX_8(float value)
	{
		___paddingX_8 = value;
	}

	inline static int32_t get_offset_of_scripts_9() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___scripts_9)); }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * get_scripts_9() const { return ___scripts_9; }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 ** get_address_of_scripts_9() { return &___scripts_9; }
	inline void set_scripts_9(List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * value)
	{
		___scripts_9 = value;
		Il2CppCodeGenWriteBarrier((&___scripts_9), value);
	}

	inline static int32_t get_offset_of_menuItemObjects_10() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___menuItemObjects_10)); }
	inline List_1_t738D3CE973A389B13AA52C796419D7B95FB33EC2 * get_menuItemObjects_10() const { return ___menuItemObjects_10; }
	inline List_1_t738D3CE973A389B13AA52C796419D7B95FB33EC2 ** get_address_of_menuItemObjects_10() { return &___menuItemObjects_10; }
	inline void set_menuItemObjects_10(List_1_t738D3CE973A389B13AA52C796419D7B95FB33EC2 * value)
	{
		___menuItemObjects_10 = value;
		Il2CppCodeGenWriteBarrier((&___menuItemObjects_10), value);
	}

	inline static int32_t get_offset_of_usingLocalScripts_11() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___usingLocalScripts_11)); }
	inline bool get_usingLocalScripts_11() const { return ___usingLocalScripts_11; }
	inline bool* get_address_of_usingLocalScripts_11() { return &___usingLocalScripts_11; }
	inline void set_usingLocalScripts_11(bool value)
	{
		___usingLocalScripts_11 = value;
	}

	inline static int32_t get_offset_of_refreshIcon_12() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___refreshIcon_12)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_refreshIcon_12() const { return ___refreshIcon_12; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_refreshIcon_12() { return &___refreshIcon_12; }
	inline void set_refreshIcon_12(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___refreshIcon_12 = value;
		Il2CppCodeGenWriteBarrier((&___refreshIcon_12), value);
	}

	inline static int32_t get_offset_of_normalizedPositionY_13() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___normalizedPositionY_13)); }
	inline float get_normalizedPositionY_13() const { return ___normalizedPositionY_13; }
	inline float* get_address_of_normalizedPositionY_13() { return &___normalizedPositionY_13; }
	inline void set_normalizedPositionY_13(float value)
	{
		___normalizedPositionY_13 = value;
	}

	inline static int32_t get_offset_of_sizeY_14() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___sizeY_14)); }
	inline float get_sizeY_14() const { return ___sizeY_14; }
	inline float* get_address_of_sizeY_14() { return &___sizeY_14; }
	inline void set_sizeY_14(float value)
	{
		___sizeY_14 = value;
	}

	inline static int32_t get_offset_of_refreshing_15() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___refreshing_15)); }
	inline bool get_refreshing_15() const { return ___refreshing_15; }
	inline bool* get_address_of_refreshing_15() { return &___refreshing_15; }
	inline void set_refreshing_15(bool value)
	{
		___refreshing_15 = value;
	}

	inline static int32_t get_offset_of_waitingToStopRefreshing_16() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___waitingToStopRefreshing_16)); }
	inline bool get_waitingToStopRefreshing_16() const { return ___waitingToStopRefreshing_16; }
	inline bool* get_address_of_waitingToStopRefreshing_16() { return &___waitingToStopRefreshing_16; }
	inline void set_waitingToStopRefreshing_16(bool value)
	{
		___waitingToStopRefreshing_16 = value;
	}

	inline static int32_t get_offset_of_waitingToStopRefreshingStartTime_17() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___waitingToStopRefreshingStartTime_17)); }
	inline float get_waitingToStopRefreshingStartTime_17() const { return ___waitingToStopRefreshingStartTime_17; }
	inline float* get_address_of_waitingToStopRefreshingStartTime_17() { return &___waitingToStopRefreshingStartTime_17; }
	inline void set_waitingToStopRefreshingStartTime_17(float value)
	{
		___waitingToStopRefreshingStartTime_17 = value;
	}

	inline static int32_t get_offset_of_releasedTouchSinceRefresh_18() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___releasedTouchSinceRefresh_18)); }
	inline bool get_releasedTouchSinceRefresh_18() const { return ___releasedTouchSinceRefresh_18; }
	inline bool* get_address_of_releasedTouchSinceRefresh_18() { return &___releasedTouchSinceRefresh_18; }
	inline void set_releasedTouchSinceRefresh_18(bool value)
	{
		___releasedTouchSinceRefresh_18 = value;
	}

	inline static int32_t get_offset_of_normalizedPositionYSpinThreshold_19() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___normalizedPositionYSpinThreshold_19)); }
	inline float get_normalizedPositionYSpinThreshold_19() const { return ___normalizedPositionYSpinThreshold_19; }
	inline float* get_address_of_normalizedPositionYSpinThreshold_19() { return &___normalizedPositionYSpinThreshold_19; }
	inline void set_normalizedPositionYSpinThreshold_19(float value)
	{
		___normalizedPositionYSpinThreshold_19 = value;
	}

	inline static int32_t get_offset_of_normalizedPositionYRefreshThreshold_20() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___normalizedPositionYRefreshThreshold_20)); }
	inline float get_normalizedPositionYRefreshThreshold_20() const { return ___normalizedPositionYRefreshThreshold_20; }
	inline float* get_address_of_normalizedPositionYRefreshThreshold_20() { return &___normalizedPositionYRefreshThreshold_20; }
	inline void set_normalizedPositionYRefreshThreshold_20(float value)
	{
		___normalizedPositionYRefreshThreshold_20 = value;
	}

	inline static int32_t get_offset_of_refreshRotationSpeed_21() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___refreshRotationSpeed_21)); }
	inline float get_refreshRotationSpeed_21() const { return ___refreshRotationSpeed_21; }
	inline float* get_address_of_refreshRotationSpeed_21() { return &___refreshRotationSpeed_21; }
	inline void set_refreshRotationSpeed_21(float value)
	{
		___refreshRotationSpeed_21 = value;
	}

	inline static int32_t get_offset_of_scrollRect_22() { return static_cast<int32_t>(offsetof(TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE, ___scrollRect_22)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_scrollRect_22() const { return ___scrollRect_22; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_scrollRect_22() { return &___scrollRect_22; }
	inline void set_scrollRect_22(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___scrollRect_22 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILEDSCRIPTMENUDISPLAY_T7234091EF72E6C896E3D9EF92BB91E3597AFCFCE_H
#ifndef UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#define UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager
struct  UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.XR.ARFoundation.ARPlaneManager UIManager::m_PlaneManager
	ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * ___m_PlaneManager_6;
	// UnityEngine.Animator UIManager::m_MoveDeviceAnimation
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_MoveDeviceAnimation_7;
	// UnityEngine.Animator UIManager::m_TapToPlaceAnimation
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_TapToPlaceAnimation_8;
	// System.Boolean UIManager::m_ShowingTapToPlace
	bool ___m_ShowingTapToPlace_10;
	// System.Boolean UIManager::m_ShowingMoveDevice
	bool ___m_ShowingMoveDevice_11;

public:
	inline static int32_t get_offset_of_m_PlaneManager_6() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_PlaneManager_6)); }
	inline ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * get_m_PlaneManager_6() const { return ___m_PlaneManager_6; }
	inline ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A ** get_address_of_m_PlaneManager_6() { return &___m_PlaneManager_6; }
	inline void set_m_PlaneManager_6(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * value)
	{
		___m_PlaneManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlaneManager_6), value);
	}

	inline static int32_t get_offset_of_m_MoveDeviceAnimation_7() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_MoveDeviceAnimation_7)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_MoveDeviceAnimation_7() const { return ___m_MoveDeviceAnimation_7; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_MoveDeviceAnimation_7() { return &___m_MoveDeviceAnimation_7; }
	inline void set_m_MoveDeviceAnimation_7(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_MoveDeviceAnimation_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_MoveDeviceAnimation_7), value);
	}

	inline static int32_t get_offset_of_m_TapToPlaceAnimation_8() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_TapToPlaceAnimation_8)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_TapToPlaceAnimation_8() const { return ___m_TapToPlaceAnimation_8; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_TapToPlaceAnimation_8() { return &___m_TapToPlaceAnimation_8; }
	inline void set_m_TapToPlaceAnimation_8(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_TapToPlaceAnimation_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_TapToPlaceAnimation_8), value);
	}

	inline static int32_t get_offset_of_m_ShowingTapToPlace_10() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_ShowingTapToPlace_10)); }
	inline bool get_m_ShowingTapToPlace_10() const { return ___m_ShowingTapToPlace_10; }
	inline bool* get_address_of_m_ShowingTapToPlace_10() { return &___m_ShowingTapToPlace_10; }
	inline void set_m_ShowingTapToPlace_10(bool value)
	{
		___m_ShowingTapToPlace_10 = value;
	}

	inline static int32_t get_offset_of_m_ShowingMoveDevice_11() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_ShowingMoveDevice_11)); }
	inline bool get_m_ShowingMoveDevice_11() const { return ___m_ShowingMoveDevice_11; }
	inline bool* get_address_of_m_ShowingMoveDevice_11() { return &___m_ShowingMoveDevice_11; }
	inline void set_m_ShowingMoveDevice_11(bool value)
	{
		___m_ShowingMoveDevice_11 = value;
	}
};

struct UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane> UIManager::s_Planes
	List_1_tCD714E0010765A4D65BD80974698E4FA9EFBC2B6 * ___s_Planes_9;

public:
	inline static int32_t get_offset_of_s_Planes_9() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields, ___s_Planes_9)); }
	inline List_1_tCD714E0010765A4D65BD80974698E4FA9EFBC2B6 * get_s_Planes_9() const { return ___s_Planes_9; }
	inline List_1_tCD714E0010765A4D65BD80974698E4FA9EFBC2B6 ** get_address_of_s_Planes_9() { return &___s_Planes_9; }
	inline void set_s_Planes_9(List_1_tCD714E0010765A4D65BD80974698E4FA9EFBC2B6 * value)
	{
		___s_Planes_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_Planes_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#ifndef UNITYTHREADHELPER_TD1E5142ACFA8151C129F12A85A94A314FD16C233_H
#define UNITYTHREADHELPER_TD1E5142ACFA8151C129F12A85A94A314FD16C233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreadHelper
struct  UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityThreading.Dispatcher UnityThreadHelper::dispatcher
	Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 * ___dispatcher_6;
	// UnityThreading.TaskDistributor UnityThreadHelper::taskDistributor
	TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D * ___taskDistributor_7;
	// System.Collections.Generic.List`1<UnityThreading.ThreadBase> UnityThreadHelper::registeredThreads
	List_1_tC440ECADB0E49325F80F6D99AD7AD37A932F7219 * ___registeredThreads_8;

public:
	inline static int32_t get_offset_of_dispatcher_6() { return static_cast<int32_t>(offsetof(UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233, ___dispatcher_6)); }
	inline Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 * get_dispatcher_6() const { return ___dispatcher_6; }
	inline Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 ** get_address_of_dispatcher_6() { return &___dispatcher_6; }
	inline void set_dispatcher_6(Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 * value)
	{
		___dispatcher_6 = value;
		Il2CppCodeGenWriteBarrier((&___dispatcher_6), value);
	}

	inline static int32_t get_offset_of_taskDistributor_7() { return static_cast<int32_t>(offsetof(UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233, ___taskDistributor_7)); }
	inline TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D * get_taskDistributor_7() const { return ___taskDistributor_7; }
	inline TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D ** get_address_of_taskDistributor_7() { return &___taskDistributor_7; }
	inline void set_taskDistributor_7(TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D * value)
	{
		___taskDistributor_7 = value;
		Il2CppCodeGenWriteBarrier((&___taskDistributor_7), value);
	}

	inline static int32_t get_offset_of_registeredThreads_8() { return static_cast<int32_t>(offsetof(UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233, ___registeredThreads_8)); }
	inline List_1_tC440ECADB0E49325F80F6D99AD7AD37A932F7219 * get_registeredThreads_8() const { return ___registeredThreads_8; }
	inline List_1_tC440ECADB0E49325F80F6D99AD7AD37A932F7219 ** get_address_of_registeredThreads_8() { return &___registeredThreads_8; }
	inline void set_registeredThreads_8(List_1_tC440ECADB0E49325F80F6D99AD7AD37A932F7219 * value)
	{
		___registeredThreads_8 = value;
		Il2CppCodeGenWriteBarrier((&___registeredThreads_8), value);
	}
};

struct UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233_StaticFields
{
public:
	// UnityThreadHelper UnityThreadHelper::instance
	UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233 * ___instance_4;
	// System.Object UnityThreadHelper::syncRoot
	RuntimeObject * ___syncRoot_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233_StaticFields, ___instance_4)); }
	inline UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233 * get_instance_4() const { return ___instance_4; }
	inline UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_syncRoot_5() { return static_cast<int32_t>(offsetof(UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233_StaticFields, ___syncRoot_5)); }
	inline RuntimeObject * get_syncRoot_5() const { return ___syncRoot_5; }
	inline RuntimeObject ** get_address_of_syncRoot_5() { return &___syncRoot_5; }
	inline void set_syncRoot_5(RuntimeObject * value)
	{
		___syncRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTHREADHELPER_TD1E5142ACFA8151C129F12A85A94A314FD16C233_H
#ifndef VRCUSTOMMENU_T78F51F189CB1B5A28B805AADB961D74D474B4B6B_H
#define VRCUSTOMMENU_T78F51F189CB1B5A28B805AADB961D74D474B4B6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRCustomMenu
struct  VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject VRCustomMenu::VRMenu
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___VRMenu_4;
	// MPAR.Common.UI.PortableMenu.PortableMenuItem[] VRCustomMenu::menuItemObjects
	PortableMenuItemU5BU5D_t96F76D1F6EC232B8A030659418EF7D74A9C0AF44* ___menuItemObjects_5;
	// System.Collections.Generic.List`1<Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem> VRCustomMenu::menuItems
	List_1_t5DFB316E3A16E508385B16D18E565D7775541A8C * ___menuItems_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action> VRCustomMenu::menuItemCallbacks
	Dictionary_2_t8B6634B584453750D056334B3141E31F88DD5F5C * ___menuItemCallbacks_7;
	// System.Int32 VRCustomMenu::skip
	int32_t ___skip_8;
	// System.Int32 VRCustomMenu::take
	int32_t ___take_9;

public:
	inline static int32_t get_offset_of_VRMenu_4() { return static_cast<int32_t>(offsetof(VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B, ___VRMenu_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_VRMenu_4() const { return ___VRMenu_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_VRMenu_4() { return &___VRMenu_4; }
	inline void set_VRMenu_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___VRMenu_4 = value;
		Il2CppCodeGenWriteBarrier((&___VRMenu_4), value);
	}

	inline static int32_t get_offset_of_menuItemObjects_5() { return static_cast<int32_t>(offsetof(VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B, ___menuItemObjects_5)); }
	inline PortableMenuItemU5BU5D_t96F76D1F6EC232B8A030659418EF7D74A9C0AF44* get_menuItemObjects_5() const { return ___menuItemObjects_5; }
	inline PortableMenuItemU5BU5D_t96F76D1F6EC232B8A030659418EF7D74A9C0AF44** get_address_of_menuItemObjects_5() { return &___menuItemObjects_5; }
	inline void set_menuItemObjects_5(PortableMenuItemU5BU5D_t96F76D1F6EC232B8A030659418EF7D74A9C0AF44* value)
	{
		___menuItemObjects_5 = value;
		Il2CppCodeGenWriteBarrier((&___menuItemObjects_5), value);
	}

	inline static int32_t get_offset_of_menuItems_6() { return static_cast<int32_t>(offsetof(VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B, ___menuItems_6)); }
	inline List_1_t5DFB316E3A16E508385B16D18E565D7775541A8C * get_menuItems_6() const { return ___menuItems_6; }
	inline List_1_t5DFB316E3A16E508385B16D18E565D7775541A8C ** get_address_of_menuItems_6() { return &___menuItems_6; }
	inline void set_menuItems_6(List_1_t5DFB316E3A16E508385B16D18E565D7775541A8C * value)
	{
		___menuItems_6 = value;
		Il2CppCodeGenWriteBarrier((&___menuItems_6), value);
	}

	inline static int32_t get_offset_of_menuItemCallbacks_7() { return static_cast<int32_t>(offsetof(VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B, ___menuItemCallbacks_7)); }
	inline Dictionary_2_t8B6634B584453750D056334B3141E31F88DD5F5C * get_menuItemCallbacks_7() const { return ___menuItemCallbacks_7; }
	inline Dictionary_2_t8B6634B584453750D056334B3141E31F88DD5F5C ** get_address_of_menuItemCallbacks_7() { return &___menuItemCallbacks_7; }
	inline void set_menuItemCallbacks_7(Dictionary_2_t8B6634B584453750D056334B3141E31F88DD5F5C * value)
	{
		___menuItemCallbacks_7 = value;
		Il2CppCodeGenWriteBarrier((&___menuItemCallbacks_7), value);
	}

	inline static int32_t get_offset_of_skip_8() { return static_cast<int32_t>(offsetof(VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B, ___skip_8)); }
	inline int32_t get_skip_8() const { return ___skip_8; }
	inline int32_t* get_address_of_skip_8() { return &___skip_8; }
	inline void set_skip_8(int32_t value)
	{
		___skip_8 = value;
	}

	inline static int32_t get_offset_of_take_9() { return static_cast<int32_t>(offsetof(VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B, ___take_9)); }
	inline int32_t get_take_9() const { return ___take_9; }
	inline int32_t* get_address_of_take_9() { return &___take_9; }
	inline void set_take_9(int32_t value)
	{
		___take_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRCUSTOMMENU_T78F51F189CB1B5A28B805AADB961D74D474B4B6B_H
#ifndef BROWSESCRIPTSMENU_TCE0C74477CAC423E70D85E4E6D886D2E340071AD_H
#define BROWSESCRIPTSMENU_TCE0C74477CAC423E70D85E4E6D886D2E340071AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BrowseScriptsMenu
struct  BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD  : public TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE
{
public:
	// MPAR.Common.AssetLoader.IPlatformDependentAssetLoader BrowseScriptsMenu::assets
	RuntimeObject* ___assets_23;
	// System.Int32 BrowseScriptsMenu::totalScriptsInGallery
	int32_t ___totalScriptsInGallery_24;
	// System.Int32 BrowseScriptsMenu::scriptsToLoadAtOnce
	int32_t ___scriptsToLoadAtOnce_25;
	// System.Int32 BrowseScriptsMenu::scriptsLoaded
	int32_t ___scriptsLoaded_26;
	// System.Boolean BrowseScriptsMenu::loading
	bool ___loading_27;
	// System.Collections.Generic.List`1<MPAR.Common.Scripting.Script> BrowseScriptsMenu::allScripts
	List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * ___allScripts_28;
	// System.Single BrowseScriptsMenu::lastUpdateTime
	float ___lastUpdateTime_29;
	// System.Single BrowseScriptsMenu::minUpdateInterval
	float ___minUpdateInterval_30;
	// System.Single BrowseScriptsMenu::normalUpdateInterval
	float ___normalUpdateInterval_31;
	// System.Single BrowseScriptsMenu::previousVerticalNormalizedPosition
	float ___previousVerticalNormalizedPosition_32;
	// System.Single BrowseScriptsMenu::previousHeight
	float ___previousHeight_33;
	// LoadingIndicator BrowseScriptsMenu::LoadingIndicator
	LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB * ___LoadingIndicator_34;
	// System.Boolean BrowseScriptsMenu::showingLoadingIndicator
	bool ___showingLoadingIndicator_35;
	// System.Boolean BrowseScriptsMenu::doingFullRefresh
	bool ___doingFullRefresh_36;

public:
	inline static int32_t get_offset_of_assets_23() { return static_cast<int32_t>(offsetof(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD, ___assets_23)); }
	inline RuntimeObject* get_assets_23() const { return ___assets_23; }
	inline RuntimeObject** get_address_of_assets_23() { return &___assets_23; }
	inline void set_assets_23(RuntimeObject* value)
	{
		___assets_23 = value;
		Il2CppCodeGenWriteBarrier((&___assets_23), value);
	}

	inline static int32_t get_offset_of_totalScriptsInGallery_24() { return static_cast<int32_t>(offsetof(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD, ___totalScriptsInGallery_24)); }
	inline int32_t get_totalScriptsInGallery_24() const { return ___totalScriptsInGallery_24; }
	inline int32_t* get_address_of_totalScriptsInGallery_24() { return &___totalScriptsInGallery_24; }
	inline void set_totalScriptsInGallery_24(int32_t value)
	{
		___totalScriptsInGallery_24 = value;
	}

	inline static int32_t get_offset_of_scriptsToLoadAtOnce_25() { return static_cast<int32_t>(offsetof(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD, ___scriptsToLoadAtOnce_25)); }
	inline int32_t get_scriptsToLoadAtOnce_25() const { return ___scriptsToLoadAtOnce_25; }
	inline int32_t* get_address_of_scriptsToLoadAtOnce_25() { return &___scriptsToLoadAtOnce_25; }
	inline void set_scriptsToLoadAtOnce_25(int32_t value)
	{
		___scriptsToLoadAtOnce_25 = value;
	}

	inline static int32_t get_offset_of_scriptsLoaded_26() { return static_cast<int32_t>(offsetof(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD, ___scriptsLoaded_26)); }
	inline int32_t get_scriptsLoaded_26() const { return ___scriptsLoaded_26; }
	inline int32_t* get_address_of_scriptsLoaded_26() { return &___scriptsLoaded_26; }
	inline void set_scriptsLoaded_26(int32_t value)
	{
		___scriptsLoaded_26 = value;
	}

	inline static int32_t get_offset_of_loading_27() { return static_cast<int32_t>(offsetof(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD, ___loading_27)); }
	inline bool get_loading_27() const { return ___loading_27; }
	inline bool* get_address_of_loading_27() { return &___loading_27; }
	inline void set_loading_27(bool value)
	{
		___loading_27 = value;
	}

	inline static int32_t get_offset_of_allScripts_28() { return static_cast<int32_t>(offsetof(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD, ___allScripts_28)); }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * get_allScripts_28() const { return ___allScripts_28; }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 ** get_address_of_allScripts_28() { return &___allScripts_28; }
	inline void set_allScripts_28(List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * value)
	{
		___allScripts_28 = value;
		Il2CppCodeGenWriteBarrier((&___allScripts_28), value);
	}

	inline static int32_t get_offset_of_lastUpdateTime_29() { return static_cast<int32_t>(offsetof(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD, ___lastUpdateTime_29)); }
	inline float get_lastUpdateTime_29() const { return ___lastUpdateTime_29; }
	inline float* get_address_of_lastUpdateTime_29() { return &___lastUpdateTime_29; }
	inline void set_lastUpdateTime_29(float value)
	{
		___lastUpdateTime_29 = value;
	}

	inline static int32_t get_offset_of_minUpdateInterval_30() { return static_cast<int32_t>(offsetof(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD, ___minUpdateInterval_30)); }
	inline float get_minUpdateInterval_30() const { return ___minUpdateInterval_30; }
	inline float* get_address_of_minUpdateInterval_30() { return &___minUpdateInterval_30; }
	inline void set_minUpdateInterval_30(float value)
	{
		___minUpdateInterval_30 = value;
	}

	inline static int32_t get_offset_of_normalUpdateInterval_31() { return static_cast<int32_t>(offsetof(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD, ___normalUpdateInterval_31)); }
	inline float get_normalUpdateInterval_31() const { return ___normalUpdateInterval_31; }
	inline float* get_address_of_normalUpdateInterval_31() { return &___normalUpdateInterval_31; }
	inline void set_normalUpdateInterval_31(float value)
	{
		___normalUpdateInterval_31 = value;
	}

	inline static int32_t get_offset_of_previousVerticalNormalizedPosition_32() { return static_cast<int32_t>(offsetof(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD, ___previousVerticalNormalizedPosition_32)); }
	inline float get_previousVerticalNormalizedPosition_32() const { return ___previousVerticalNormalizedPosition_32; }
	inline float* get_address_of_previousVerticalNormalizedPosition_32() { return &___previousVerticalNormalizedPosition_32; }
	inline void set_previousVerticalNormalizedPosition_32(float value)
	{
		___previousVerticalNormalizedPosition_32 = value;
	}

	inline static int32_t get_offset_of_previousHeight_33() { return static_cast<int32_t>(offsetof(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD, ___previousHeight_33)); }
	inline float get_previousHeight_33() const { return ___previousHeight_33; }
	inline float* get_address_of_previousHeight_33() { return &___previousHeight_33; }
	inline void set_previousHeight_33(float value)
	{
		___previousHeight_33 = value;
	}

	inline static int32_t get_offset_of_LoadingIndicator_34() { return static_cast<int32_t>(offsetof(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD, ___LoadingIndicator_34)); }
	inline LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB * get_LoadingIndicator_34() const { return ___LoadingIndicator_34; }
	inline LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB ** get_address_of_LoadingIndicator_34() { return &___LoadingIndicator_34; }
	inline void set_LoadingIndicator_34(LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB * value)
	{
		___LoadingIndicator_34 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingIndicator_34), value);
	}

	inline static int32_t get_offset_of_showingLoadingIndicator_35() { return static_cast<int32_t>(offsetof(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD, ___showingLoadingIndicator_35)); }
	inline bool get_showingLoadingIndicator_35() const { return ___showingLoadingIndicator_35; }
	inline bool* get_address_of_showingLoadingIndicator_35() { return &___showingLoadingIndicator_35; }
	inline void set_showingLoadingIndicator_35(bool value)
	{
		___showingLoadingIndicator_35 = value;
	}

	inline static int32_t get_offset_of_doingFullRefresh_36() { return static_cast<int32_t>(offsetof(BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD, ___doingFullRefresh_36)); }
	inline bool get_doingFullRefresh_36() const { return ___doingFullRefresh_36; }
	inline bool* get_address_of_doingFullRefresh_36() { return &___doingFullRefresh_36; }
	inline void set_doingFullRefresh_36(bool value)
	{
		___doingFullRefresh_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BROWSESCRIPTSMENU_TCE0C74477CAC423E70D85E4E6D886D2E340071AD_H
#ifndef DESKTOPLOG_T42A4E8BFEBF79EE1FC717B1EFDB6DE299EE9807F_H
#define DESKTOPLOG_T42A4E8BFEBF79EE1FC717B1EFDB6DE299EE9807F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DesktopLog
struct  DesktopLog_t42A4E8BFEBF79EE1FC717B1EFDB6DE299EE9807F  : public Singleton_1_t2A99EC44A1B00CD7E55DF146826BC9DCF38E9D0E
{
public:
	// UnityEngine.UI.ScrollRect DesktopLog::log
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___log_5;
	// UnityEngine.UI.Text DesktopLog::text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___text_6;

public:
	inline static int32_t get_offset_of_log_5() { return static_cast<int32_t>(offsetof(DesktopLog_t42A4E8BFEBF79EE1FC717B1EFDB6DE299EE9807F, ___log_5)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_log_5() const { return ___log_5; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_log_5() { return &___log_5; }
	inline void set_log_5(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___log_5 = value;
		Il2CppCodeGenWriteBarrier((&___log_5), value);
	}

	inline static int32_t get_offset_of_text_6() { return static_cast<int32_t>(offsetof(DesktopLog_t42A4E8BFEBF79EE1FC717B1EFDB6DE299EE9807F, ___text_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_text_6() const { return ___text_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_text_6() { return &___text_6; }
	inline void set_text_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___text_6 = value;
		Il2CppCodeGenWriteBarrier((&___text_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESKTOPLOG_T42A4E8BFEBF79EE1FC717B1EFDB6DE299EE9807F_H
#ifndef LOCALIZATRON_TC7AF71F3CADDA057CB4068F6260C5B90A047962E_H
#define LOCALIZATRON_TC7AF71F3CADDA057CB4068F6260C5B90A047962E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Localizatron
struct  Localizatron_tC7AF71F3CADDA057CB4068F6260C5B90A047962E  : public Singleton_1_t700CAB15E305EB8EEE879E9CBBF8AB45B71663F0
{
public:
	// System.String Localizatron::_languagePath
	String_t* ____languagePath_7;
	// System.String Localizatron::_currentLanguage
	String_t* ____currentLanguage_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Localizatron::languageTable
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___languageTable_9;

public:
	inline static int32_t get_offset_of__languagePath_7() { return static_cast<int32_t>(offsetof(Localizatron_tC7AF71F3CADDA057CB4068F6260C5B90A047962E, ____languagePath_7)); }
	inline String_t* get__languagePath_7() const { return ____languagePath_7; }
	inline String_t** get_address_of__languagePath_7() { return &____languagePath_7; }
	inline void set__languagePath_7(String_t* value)
	{
		____languagePath_7 = value;
		Il2CppCodeGenWriteBarrier((&____languagePath_7), value);
	}

	inline static int32_t get_offset_of__currentLanguage_8() { return static_cast<int32_t>(offsetof(Localizatron_tC7AF71F3CADDA057CB4068F6260C5B90A047962E, ____currentLanguage_8)); }
	inline String_t* get__currentLanguage_8() const { return ____currentLanguage_8; }
	inline String_t** get_address_of__currentLanguage_8() { return &____currentLanguage_8; }
	inline void set__currentLanguage_8(String_t* value)
	{
		____currentLanguage_8 = value;
		Il2CppCodeGenWriteBarrier((&____currentLanguage_8), value);
	}

	inline static int32_t get_offset_of_languageTable_9() { return static_cast<int32_t>(offsetof(Localizatron_tC7AF71F3CADDA057CB4068F6260C5B90A047962E, ___languageTable_9)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_languageTable_9() const { return ___languageTable_9; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_languageTable_9() { return &___languageTable_9; }
	inline void set_languageTable_9(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___languageTable_9 = value;
		Il2CppCodeGenWriteBarrier((&___languageTable_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZATRON_TC7AF71F3CADDA057CB4068F6260C5B90A047962E_H
#ifndef RECENTSCRIPTSMENU_T8F13B305A1E4FDA2A89487715B6BE127862A0088_H
#define RECENTSCRIPTSMENU_T8F13B305A1E4FDA2A89487715B6BE127862A0088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecentScriptsMenu
struct  RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088  : public TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE
{
public:
	// MPAR.Common.Persistence.ISaveLoadManager RecentScriptsMenu::<SaveLoadManager>k__BackingField
	RuntimeObject* ___U3CSaveLoadManagerU3Ek__BackingField_23;
	// LoadingIndicator RecentScriptsMenu::LoadingIndicator
	LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB * ___LoadingIndicator_24;

public:
	inline static int32_t get_offset_of_U3CSaveLoadManagerU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088, ___U3CSaveLoadManagerU3Ek__BackingField_23)); }
	inline RuntimeObject* get_U3CSaveLoadManagerU3Ek__BackingField_23() const { return ___U3CSaveLoadManagerU3Ek__BackingField_23; }
	inline RuntimeObject** get_address_of_U3CSaveLoadManagerU3Ek__BackingField_23() { return &___U3CSaveLoadManagerU3Ek__BackingField_23; }
	inline void set_U3CSaveLoadManagerU3Ek__BackingField_23(RuntimeObject* value)
	{
		___U3CSaveLoadManagerU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSaveLoadManagerU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_LoadingIndicator_24() { return static_cast<int32_t>(offsetof(RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088, ___LoadingIndicator_24)); }
	inline LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB * get_LoadingIndicator_24() const { return ___LoadingIndicator_24; }
	inline LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB ** get_address_of_LoadingIndicator_24() { return &___LoadingIndicator_24; }
	inline void set_LoadingIndicator_24(LoadingIndicator_tBAA6B31EF6D3DDA7E414C9BF1B276E89E40208EB * value)
	{
		___LoadingIndicator_24 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingIndicator_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECENTSCRIPTSMENU_T8F13B305A1E4FDA2A89487715B6BE127862A0088_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7300 = { sizeof (DesktopLog_t42A4E8BFEBF79EE1FC717B1EFDB6DE299EE9807F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7300[2] = 
{
	DesktopLog_t42A4E8BFEBF79EE1FC717B1EFDB6DE299EE9807F::get_offset_of_log_5(),
	DesktopLog_t42A4E8BFEBF79EE1FC717B1EFDB6DE299EE9807F::get_offset_of_text_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7301 = { sizeof (BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7301[32] = 
{
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_MainPanel_4(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_SliderPanel_5(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_ButtonsPanel_6(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_arrowImage_7(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_BottomMargin_8(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_mainPanelHeight_9(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_arrowSprite_10(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_expandButtonSprite_11(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_closedPositionPercentage_12(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_openPositionPercentage_13(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_openPosition_14(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_closedPosition_15(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_animatingDelta_16(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_lastWidth_17(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_lastHeight_18(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_menuOpen_19(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_secondsToAnimate_20(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_touchDown_21(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_touchDownPosition_22(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_lastTouchPosition_23(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_lastTouchPositionRaw_24(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_CustomizablePanels_25(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_lastCreatedButton_26(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_menuItems_27(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_skip_28(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_take_29(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_previousButton_30(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_nextButton_31(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_graphicRaycaster_32(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_eventSystem_33(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_closedIconMaterial_34(),
	BackMenuSlider_t9211479ACAC197AFF4D9EF71FFEFE4A88E8451B9::get_offset_of_openIconMaterial_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7302 = { sizeof (U3CU3Ec__DisplayClass33_0_tFA1B99BDA082C3BD150F1E190E8931338ABB8EF9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7302[1] = 
{
	U3CU3Ec__DisplayClass33_0_tFA1B99BDA082C3BD150F1E190E8931338ABB8EF9::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7303 = { sizeof (U3CU3Ec__DisplayClass34_0_t718E570EE15155B9937389A67A3E3BCB293C5CBA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7303[1] = 
{
	U3CU3Ec__DisplayClass34_0_t718E570EE15155B9937389A67A3E3BCB293C5CBA::get_offset_of_item_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7304 = { sizeof (U3CU3Ec__DisplayClass37_0_t74AE714D46F0F97F0A01D23DFB12DFDEE2DF1BE7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7304[3] = 
{
	U3CU3Ec__DisplayClass37_0_t74AE714D46F0F97F0A01D23DFB12DFDEE2DF1BE7::get_offset_of_key_0(),
	U3CU3Ec__DisplayClass37_0_t74AE714D46F0F97F0A01D23DFB12DFDEE2DF1BE7::get_offset_of_button_1(),
	U3CU3Ec__DisplayClass37_0_t74AE714D46F0F97F0A01D23DFB12DFDEE2DF1BE7::get_offset_of_U3CU3E9__3_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7305 = { sizeof (U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A), -1, sizeof(U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7305[9] = 
{
	U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields::get_offset_of_U3CU3E9__37_1_1(),
	U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields::get_offset_of_U3CU3E9__38_1_2(),
	U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields::get_offset_of_U3CU3E9__39_1_3(),
	U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields::get_offset_of_U3CU3E9__40_1_4(),
	U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields::get_offset_of_U3CU3E9__41_3_5(),
	U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields::get_offset_of_U3CU3E9__41_0_6(),
	U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields::get_offset_of_U3CU3E9__41_1_7(),
	U3CU3Ec_t249DF3CC69CDA7B96A3E679F3DD20AEB40A5CC5A_StaticFields::get_offset_of_U3CU3E9__44_0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7306 = { sizeof (U3CU3Ec__DisplayClass38_0_t68897C520D027BF355A20D69379A4DB049077100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7306[2] = 
{
	U3CU3Ec__DisplayClass38_0_t68897C520D027BF355A20D69379A4DB049077100::get_offset_of_key_0(),
	U3CU3Ec__DisplayClass38_0_t68897C520D027BF355A20D69379A4DB049077100::get_offset_of_U3CU3E9__2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7307 = { sizeof (U3CU3Ec__DisplayClass39_0_t8068FA9014A2C118159256A57442CC56463FCDF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7307[2] = 
{
	U3CU3Ec__DisplayClass39_0_t8068FA9014A2C118159256A57442CC56463FCDF6::get_offset_of_key_0(),
	U3CU3Ec__DisplayClass39_0_t8068FA9014A2C118159256A57442CC56463FCDF6::get_offset_of_U3CU3E9__2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7308 = { sizeof (U3CU3Ec__DisplayClass40_0_t7737C4E49EBC29B40C9F85C5712EE20D3133F5AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7308[2] = 
{
	U3CU3Ec__DisplayClass40_0_t7737C4E49EBC29B40C9F85C5712EE20D3133F5AD::get_offset_of_key_0(),
	U3CU3Ec__DisplayClass40_0_t7737C4E49EBC29B40C9F85C5712EE20D3133F5AD::get_offset_of_U3CU3E9__2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7309 = { sizeof (U3CU3Ec__DisplayClass41_0_t04425736791E5133862CFB5808B3E9FBA7739423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7309[1] = 
{
	U3CU3Ec__DisplayClass41_0_t04425736791E5133862CFB5808B3E9FBA7739423::get_offset_of_buttonToSet_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7310 = { sizeof (U3CU3Ec__DisplayClass43_0_t6A13AB9296D5EB88EC6CDDC8EC4BB77AA6F79C5F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7310[1] = 
{
	U3CU3Ec__DisplayClass43_0_t6A13AB9296D5EB88EC6CDDC8EC4BB77AA6F79C5F::get_offset_of_item_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7311 = { sizeof (U3CU3Ec__DisplayClass43_1_t3C9556A3E583A6C6C63E1F8F59A53EDED1F4EC8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7311[1] = 
{
	U3CU3Ec__DisplayClass43_1_t3C9556A3E583A6C6C63E1F8F59A53EDED1F4EC8D::get_offset_of_item_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7312 = { sizeof (U3CAnimateU3Ed__48_t7B0CB3089BE454E34C9A4DB414B4DC1A2C41B9A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7312[4] = 
{
	U3CAnimateU3Ed__48_t7B0CB3089BE454E34C9A4DB414B4DC1A2C41B9A2::get_offset_of_U3CU3E1__state_0(),
	U3CAnimateU3Ed__48_t7B0CB3089BE454E34C9A4DB414B4DC1A2C41B9A2::get_offset_of_U3CU3E2__current_1(),
	U3CAnimateU3Ed__48_t7B0CB3089BE454E34C9A4DB414B4DC1A2C41B9A2::get_offset_of_U3CU3E4__this_2(),
	U3CAnimateU3Ed__48_t7B0CB3089BE454E34C9A4DB414B4DC1A2C41B9A2::get_offset_of_U3CstartTimeU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7313 = { sizeof (BackToMenuPanel_tD676642900C6B32D11D1F8BD17C9691AF652E1AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7313[1] = 
{
	BackToMenuPanel_tD676642900C6B32D11D1F8BD17C9691AF652E1AE::get_offset_of_menuButton_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7314 = { sizeof (U3CU3Ec_t17EA2A72F88EC2DDA1A4E8E6A813A11662761B9E), -1, sizeof(U3CU3Ec_t17EA2A72F88EC2DDA1A4E8E6A813A11662761B9E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7314[2] = 
{
	U3CU3Ec_t17EA2A72F88EC2DDA1A4E8E6A813A11662761B9E_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t17EA2A72F88EC2DDA1A4E8E6A813A11662761B9E_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7315 = { sizeof (BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7315[14] = 
{
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD::get_offset_of_assets_23(),
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD::get_offset_of_totalScriptsInGallery_24(),
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD::get_offset_of_scriptsToLoadAtOnce_25(),
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD::get_offset_of_scriptsLoaded_26(),
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD::get_offset_of_loading_27(),
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD::get_offset_of_allScripts_28(),
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD::get_offset_of_lastUpdateTime_29(),
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD::get_offset_of_minUpdateInterval_30(),
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD::get_offset_of_normalUpdateInterval_31(),
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD::get_offset_of_previousVerticalNormalizedPosition_32(),
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD::get_offset_of_previousHeight_33(),
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD::get_offset_of_LoadingIndicator_34(),
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD::get_offset_of_showingLoadingIndicator_35(),
	BrowseScriptsMenu_tCE0C74477CAC423E70D85E4E6D886D2E340071AD::get_offset_of_doingFullRefresh_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7316 = { sizeof (U3CStartU3Ed__14_t8B255A94D0499030A00EFAAB80F62CC0D3761C8B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7316[3] = 
{
	U3CStartU3Ed__14_t8B255A94D0499030A00EFAAB80F62CC0D3761C8B::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__14_t8B255A94D0499030A00EFAAB80F62CC0D3761C8B::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__14_t8B255A94D0499030A00EFAAB80F62CC0D3761C8B::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7317 = { sizeof (U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7317[3] = 
{
	U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598::get_offset_of_skip_1(),
	U3CU3Ec__DisplayClass17_0_tFAE55B0BA31450782DFF4F15F90C829778656598::get_offset_of_breakEarly_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7318 = { sizeof (U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7318[7] = 
{
	U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182::get_offset_of_U3CU3E1__state_0(),
	U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182::get_offset_of_U3CU3E2__current_1(),
	U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182::get_offset_of_U3CU3E4__this_2(),
	U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182::get_offset_of_response_3(),
	U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182::get_offset_of_U3CdownloadedScriptsU3E5__2_4(),
	U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182::get_offset_of_U3CwhenAllTasksCompleteU3E5__3_5(),
	U3CU3CLoadScriptsU3Eg__callbackU7C0U3Ed_tB240D73D1C3DB0AABC37B0EC560DC2C508EF8182::get_offset_of_U3CU3E7__wrap3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7319 = { sizeof (U3CU3Ec__DisplayClass17_1_tCCA3C8755235DAA64D3BE1F4F900464680FE475F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7319[2] = 
{
	U3CU3Ec__DisplayClass17_1_tCCA3C8755235DAA64D3BE1F4F900464680FE475F::get_offset_of_waitForThumbnailTasks_0(),
	U3CU3Ec__DisplayClass17_1_tCCA3C8755235DAA64D3BE1F4F900464680FE475F::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7320 = { sizeof (U3CU3Ec__DisplayClass17_2_t45915D3DE18E0CBC3B8BCB3A59081D559F393FC6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7320[1] = 
{
	U3CU3Ec__DisplayClass17_2_t45915D3DE18E0CBC3B8BCB3A59081D559F393FC6::get_offset_of_script_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7321 = { sizeof (U3CU3Ec__DisplayClass17_3_t6CC15054C276022A254C677732BF5C8C5EA3EEF1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7321[1] = 
{
	U3CU3Ec__DisplayClass17_3_t6CC15054C276022A254C677732BF5C8C5EA3EEF1::get_offset_of_script_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7322 = { sizeof (U3CU3Ec__DisplayClass17_4_tF0A30A825F69C90957707E7215AC751AC6791100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7322[1] = 
{
	U3CU3Ec__DisplayClass17_4_tF0A30A825F69C90957707E7215AC751AC6791100::get_offset_of_scr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7323 = { sizeof (U3CLoadScriptsU3Ed__17_t2373122EDF3CADBD2A658248B39F907F0D4D39AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7323[6] = 
{
	U3CLoadScriptsU3Ed__17_t2373122EDF3CADBD2A658248B39F907F0D4D39AB::get_offset_of_U3CU3E1__state_0(),
	U3CLoadScriptsU3Ed__17_t2373122EDF3CADBD2A658248B39F907F0D4D39AB::get_offset_of_U3CU3E2__current_1(),
	U3CLoadScriptsU3Ed__17_t2373122EDF3CADBD2A658248B39F907F0D4D39AB::get_offset_of_U3CU3E4__this_2(),
	U3CLoadScriptsU3Ed__17_t2373122EDF3CADBD2A658248B39F907F0D4D39AB::get_offset_of_U3CU3E8__1_3(),
	U3CLoadScriptsU3Ed__17_t2373122EDF3CADBD2A658248B39F907F0D4D39AB::get_offset_of_numScriptsToLoad_4(),
	U3CLoadScriptsU3Ed__17_t2373122EDF3CADBD2A658248B39F907F0D4D39AB::get_offset_of_scrollToTopAfter_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7324 = { sizeof (U3CU3Ec_t098AE564161BD7FBAC97DEACF32DED3699C91E0B), -1, sizeof(U3CU3Ec_t098AE564161BD7FBAC97DEACF32DED3699C91E0B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7324[3] = 
{
	U3CU3Ec_t098AE564161BD7FBAC97DEACF32DED3699C91E0B_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t098AE564161BD7FBAC97DEACF32DED3699C91E0B_StaticFields::get_offset_of_U3CU3E9__18_0_1(),
	U3CU3Ec_t098AE564161BD7FBAC97DEACF32DED3699C91E0B_StaticFields::get_offset_of_U3CU3E9__21_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7325 = { sizeof (U3CU3Ec__DisplayClass19_0_t28AA9715A035BA5DBE5DC8319B4C02B730C733D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7325[1] = 
{
	U3CU3Ec__DisplayClass19_0_t28AA9715A035BA5DBE5DC8319B4C02B730C733D7::get_offset_of_script_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7326 = { sizeof (U3CU3Ec__DisplayClass21_0_t109044F89A93A26ACF496600B3533C7FB8620BF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7326[1] = 
{
	U3CU3Ec__DisplayClass21_0_t109044F89A93A26ACF496600B3533C7FB8620BF0::get_offset_of_firstObjectPosition_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7327 = { sizeof (CameraButton_t38DB2AA361E7A3116524B70103A3A23CFF820C0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7327[1] = 
{
	CameraButton_t38DB2AA361E7A3116524B70103A3A23CFF820C0A::get_offset_of_button_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7328 = { sizeof (U3CSharePhotoU3Ed__2_t24631812439B83CC9EA8BD2F6A6676D7244A36F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7328[2] = 
{
	U3CSharePhotoU3Ed__2_t24631812439B83CC9EA8BD2F6A6676D7244A36F6::get_offset_of_U3CU3E1__state_0(),
	U3CSharePhotoU3Ed__2_t24631812439B83CC9EA8BD2F6A6676D7244A36F6::get_offset_of_U3CU3E2__current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7329 = { sizeof (MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7329[17] = 
{
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_DeleteButton_4(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_BackBottomButton_5(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_DetailBottomButton_6(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_QRCodeImage_7(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_ThumbnailImage_8(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_NameText_9(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_AuthorText_10(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_VersionText_11(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_DescriptionText_12(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_MainMenuObject_13(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_normalPanelColor_14(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_confirmDeletePanelColor_15(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_onBackButtonPressed_16(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_U3CCurrentScriptU3Ek__BackingField_17(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_U3CSaveLoadManagerU3Ek__BackingField_18(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_U3CScriptMenuU3Ek__BackingField_19(),
	MenuItemOptions_tEB6F74CE11B07EBBA645666DA29F91ADE69080F7::get_offset_of_resolution_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7330 = { sizeof (U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904), -1, sizeof(U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7330[4] = 
{
	U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904_StaticFields::get_offset_of_U3CU3E9__30_0_1(),
	U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904_StaticFields::get_offset_of_U3CU3E9__31_0_2(),
	U3CU3Ec_t26E20EF84C663EA1EBEB62D1E58357CB34FE1904_StaticFields::get_offset_of_U3CU3E9__33_2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7331 = { sizeof (U3CU3Ec__DisplayClass33_0_t16488CE66582A182664EE32A23C91A46C892C003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7331[4] = 
{
	U3CU3Ec__DisplayClass33_0_t16488CE66582A182664EE32A23C91A46C892C003::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass33_0_t16488CE66582A182664EE32A23C91A46C892C003::get_offset_of_script_1(),
	U3CU3Ec__DisplayClass33_0_t16488CE66582A182664EE32A23C91A46C892C003::get_offset_of_U3CU3E9__3_2(),
	U3CU3Ec__DisplayClass33_0_t16488CE66582A182664EE32A23C91A46C892C003::get_offset_of_U3CU3E9__1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7332 = { sizeof (MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7332[8] = 
{
	MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7::get_offset_of_BackToMenuButton_4(),
	MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7::get_offset_of_ScanButton_5(),
	MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7::get_offset_of_MenuButton_6(),
	MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7::get_offset_of_horizontalPadding_7(),
	MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7::get_offset_of_verticalPadding_8(),
	MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7::get_offset_of_spacing_9(),
	MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7::get_offset_of_lastWidth_10(),
	MobileScanMenu_t94E40E5F244F7A8B5AEA99C337E011B2089C9AC7::get_offset_of_lastHeight_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7333 = { sizeof (OptionsBackButton_t8AE364F7F02A38DCE89E2228971F47F55F8CE6E9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7334 = { sizeof (RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7334[2] = 
{
	RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088::get_offset_of_U3CSaveLoadManagerU3Ek__BackingField_23(),
	RecentScriptsMenu_t8F13B305A1E4FDA2A89487715B6BE127862A0088::get_offset_of_LoadingIndicator_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7335 = { sizeof (U3CStartU3Ed__5_tAAA4B9D5168B9BB29E32A1E92D090D042355BA0D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7335[3] = 
{
	U3CStartU3Ed__5_tAAA4B9D5168B9BB29E32A1E92D090D042355BA0D::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__5_tAAA4B9D5168B9BB29E32A1E92D090D042355BA0D::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__5_tAAA4B9D5168B9BB29E32A1E92D090D042355BA0D::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7336 = { sizeof (U3CU3Ec__DisplayClass8_0_tD85AB27D7A3268945ED7995DA996DFC51CC2953F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7336[3] = 
{
	U3CU3Ec__DisplayClass8_0_tD85AB27D7A3268945ED7995DA996DFC51CC2953F::get_offset_of_scripts_0(),
	U3CU3Ec__DisplayClass8_0_tD85AB27D7A3268945ED7995DA996DFC51CC2953F::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass8_0_tD85AB27D7A3268945ED7995DA996DFC51CC2953F::get_offset_of_setScriptsVar_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7337 = { sizeof (U3CRefreshCoroutineU3Ed__8_t47623F75E0714ADB7A3398C383C8825AEF697CC3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7337[6] = 
{
	U3CRefreshCoroutineU3Ed__8_t47623F75E0714ADB7A3398C383C8825AEF697CC3::get_offset_of_U3CU3E1__state_0(),
	U3CRefreshCoroutineU3Ed__8_t47623F75E0714ADB7A3398C383C8825AEF697CC3::get_offset_of_U3CU3E2__current_1(),
	U3CRefreshCoroutineU3Ed__8_t47623F75E0714ADB7A3398C383C8825AEF697CC3::get_offset_of_U3CU3E4__this_2(),
	U3CRefreshCoroutineU3Ed__8_t47623F75E0714ADB7A3398C383C8825AEF697CC3::get_offset_of_U3CU3E8__1_3(),
	U3CRefreshCoroutineU3Ed__8_t47623F75E0714ADB7A3398C383C8825AEF697CC3::get_offset_of_onFinishRefresh_4(),
	U3CRefreshCoroutineU3Ed__8_t47623F75E0714ADB7A3398C383C8825AEF697CC3::get_offset_of_U3CtaskU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7338 = { sizeof (ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7338[15] = 
{
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB::get_offset_of_U3CRunningU3Ek__BackingField_4(),
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB::get_offset_of_U3CLastInvokeU3Ek__BackingField_5(),
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB::get_offset_of_U3CInvokeIntervalU3Ek__BackingField_6(),
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB::get_offset_of_U3CParserU3Ek__BackingField_7(),
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB::get_offset_of_U3CValueU3Ek__BackingField_8(),
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB::get_offset_of_InProgress_9(),
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB::get_offset_of_minScanAnchor_10(),
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB::get_offset_of_maxScanAnchor_11(),
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB::get_offset_of_imageSize_12(),
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB::get_offset_of_resolution_13(),
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB::get_offset_of_tex_14(),
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB::get_offset_of_Image_15(),
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB::get_offset_of_m_Texture_16(),
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB::get_offset_of_LoadingIndicator_17(),
	ScanView_tCB5272968F947E75D63CF3AC05499234FF5769AB::get_offset_of_startedScanning_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7339 = { sizeof (U3CScanScreenIOSCoroutineU3Ed__38_t49626750EA6146896E18FA1B9742B190F8C1129D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7339[4] = 
{
	U3CScanScreenIOSCoroutineU3Ed__38_t49626750EA6146896E18FA1B9742B190F8C1129D::get_offset_of_U3CU3E1__state_0(),
	U3CScanScreenIOSCoroutineU3Ed__38_t49626750EA6146896E18FA1B9742B190F8C1129D::get_offset_of_U3CU3E2__current_1(),
	U3CScanScreenIOSCoroutineU3Ed__38_t49626750EA6146896E18FA1B9742B190F8C1129D::get_offset_of_U3CU3E4__this_2(),
	U3CScanScreenIOSCoroutineU3Ed__38_t49626750EA6146896E18FA1B9742B190F8C1129D::get_offset_of_callback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7340 = { sizeof (U3CU3Ec__DisplayClass40_0_t0C0F190947FB9FA030087A000A38F81CFCE8D6E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7340[2] = 
{
	U3CU3Ec__DisplayClass40_0_t0C0F190947FB9FA030087A000A38F81CFCE8D6E8::get_offset_of_callback_0(),
	U3CU3Ec__DisplayClass40_0_t0C0F190947FB9FA030087A000A38F81CFCE8D6E8::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7341 = { sizeof (U3CU3Ec__DisplayClass45_0_t780CC3EBA6C391DFA98275CD75A588EE83D66D72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7341[1] = 
{
	U3CU3Ec__DisplayClass45_0_t780CC3EBA6C391DFA98275CD75A588EE83D66D72::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7342 = { sizeof (U3CLoadScriptU3Ed__45_tEB913D23620B8FC3697FE3E5D6456F77243749BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7342[4] = 
{
	U3CLoadScriptU3Ed__45_tEB913D23620B8FC3697FE3E5D6456F77243749BD::get_offset_of_U3CU3E1__state_0(),
	U3CLoadScriptU3Ed__45_tEB913D23620B8FC3697FE3E5D6456F77243749BD::get_offset_of_U3CU3E2__current_1(),
	U3CLoadScriptU3Ed__45_tEB913D23620B8FC3697FE3E5D6456F77243749BD::get_offset_of_callback_2(),
	U3CLoadScriptU3Ed__45_tEB913D23620B8FC3697FE3E5D6456F77243749BD::get_offset_of_scriptUrl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7343 = { sizeof (U3CStartScanningCoroutineU3Ed__47_t6A0D9196332D613CA17FC4DC8724DAC1793611CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7343[3] = 
{
	U3CStartScanningCoroutineU3Ed__47_t6A0D9196332D613CA17FC4DC8724DAC1793611CA::get_offset_of_U3CU3E1__state_0(),
	U3CStartScanningCoroutineU3Ed__47_t6A0D9196332D613CA17FC4DC8724DAC1793611CA::get_offset_of_U3CU3E2__current_1(),
	U3CStartScanningCoroutineU3Ed__47_t6A0D9196332D613CA17FC4DC8724DAC1793611CA::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7344 = { sizeof (ShareButton_tA3C0A87B1AC8256D48333110C2C9D01979DC974B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7345 = { sizeof (U3CU3Ec_t0C79CC841305608AA3ACB2E65E14830DC73474BF), -1, sizeof(U3CU3Ec_t0C79CC841305608AA3ACB2E65E14830DC73474BF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7345[2] = 
{
	U3CU3Ec_t0C79CC841305608AA3ACB2E65E14830DC73474BF_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t0C79CC841305608AA3ACB2E65E14830DC73474BF_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7346 = { sizeof (SwipeMenuSize_t6BE6690EB91690BA2CA28A6B7D3AAE4F474E93F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7346[3] = 
{
	SwipeMenuSize_t6BE6690EB91690BA2CA28A6B7D3AAE4F474E93F1::get_offset_of_width_4(),
	SwipeMenuSize_t6BE6690EB91690BA2CA28A6B7D3AAE4F474E93F1::get_offset_of_height_5(),
	SwipeMenuSize_t6BE6690EB91690BA2CA28A6B7D3AAE4F474E93F1::get_offset_of_buttonPanel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7347 = { sizeof (TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7347[19] = 
{
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_contentParent_4(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_menuItemPrefab_5(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_resolution_6(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_numberOfColumns_7(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_paddingX_8(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_scripts_9(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_menuItemObjects_10(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_usingLocalScripts_11(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_refreshIcon_12(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_normalizedPositionY_13(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_sizeY_14(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_refreshing_15(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_waitingToStopRefreshing_16(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_waitingToStopRefreshingStartTime_17(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_releasedTouchSinceRefresh_18(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_normalizedPositionYSpinThreshold_19(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_normalizedPositionYRefreshThreshold_20(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_refreshRotationSpeed_21(),
	TiledScriptMenuDisplay_t7234091EF72E6C896E3D9EF92BB91E3597AFCFCE::get_offset_of_scrollRect_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7348 = { sizeof (VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7348[6] = 
{
	VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B::get_offset_of_VRMenu_4(),
	VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B::get_offset_of_menuItemObjects_5(),
	VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B::get_offset_of_menuItems_6(),
	VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B::get_offset_of_menuItemCallbacks_7(),
	VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B::get_offset_of_skip_8(),
	VRCustomMenu_t78F51F189CB1B5A28B805AADB961D74D474B4B6B::get_offset_of_take_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7349 = { sizeof (U3CU3Ec_t96CBFEAF5BE0F3C2457C550F97A6B99D0E3496B7), -1, sizeof(U3CU3Ec_t96CBFEAF5BE0F3C2457C550F97A6B99D0E3496B7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7349[2] = 
{
	U3CU3Ec_t96CBFEAF5BE0F3C2457C550F97A6B99D0E3496B7_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t96CBFEAF5BE0F3C2457C550F97A6B99D0E3496B7_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7350 = { sizeof (U3CU3Ec__DisplayClass9_0_tE82723E69FB61028CED69E38EE4D3EC531C679FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7350[3] = 
{
	U3CU3Ec__DisplayClass9_0_tE82723E69FB61028CED69E38EE4D3EC531C679FE::get_offset_of_menuItem_0(),
	U3CU3Ec__DisplayClass9_0_tE82723E69FB61028CED69E38EE4D3EC531C679FE::get_offset_of_menuItemObject_1(),
	U3CU3Ec__DisplayClass9_0_tE82723E69FB61028CED69E38EE4D3EC531C679FE::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7351 = { sizeof (U3CU3Ec__DisplayClass10_0_t87FD7AED8450BA21DEB419FF79378F4C53666845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7351[1] = 
{
	U3CU3Ec__DisplayClass10_0_t87FD7AED8450BA21DEB419FF79378F4C53666845::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7352 = { sizeof (U3CU3Ec__DisplayClass11_0_t5962AB1C9ADAAEDE5A9A77AC8A010BE00368D270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7352[1] = 
{
	U3CU3Ec__DisplayClass11_0_t5962AB1C9ADAAEDE5A9A77AC8A010BE00368D270::get_offset_of_item_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7353 = { sizeof (U3CU3Ec__DisplayClass13_0_t0C60D59EFE49F8EE54269EBE2C2975BB62E10BEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7353[1] = 
{
	U3CU3Ec__DisplayClass13_0_t0C60D59EFE49F8EE54269EBE2C2975BB62E10BEE::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7354 = { sizeof (U3CU3Ec__DisplayClass14_0_tBA2A10F3D4DEB22AEE65A7E3F152A71D9B0880FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7354[1] = 
{
	U3CU3Ec__DisplayClass14_0_tBA2A10F3D4DEB22AEE65A7E3F152A71D9B0880FB::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7355 = { sizeof (U3CU3Ec__DisplayClass15_0_tB08AB79105F26EADA045302693AE1A7FCC4393D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7355[1] = 
{
	U3CU3Ec__DisplayClass15_0_tB08AB79105F26EADA045302693AE1A7FCC4393D4::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7356 = { sizeof (ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048), -1, sizeof(ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7356[6] = 
{
	ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048::get_offset_of_m_CoefficientScale_4(),
	ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048::get_offset_of_m_SkinnedMeshRenderer_5(),
	ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048_StaticFields::get_offset_of_s_FaceArkitBlendShapeCoefficients_6(),
	ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048::get_offset_of_m_ArkitFaceSubsystem_7(),
	ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048::get_offset_of_m_FaceArkitBlendShapeIndexMap_8(),
	ARFaceArkitBlendShapeVisualizer_tC8E333D7AED58A917AE539D7E3987050F77EC048::get_offset_of_m_Face_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7357 = { sizeof (ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67), -1, sizeof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7357[5] = 
{
	ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67::get_offset_of_m_FeatheringWidth_4(),
	ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields::get_offset_of_s_FeatheringUVs_5(),
	ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields::get_offset_of_s_Vertices_6(),
	ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67::get_offset_of_m_PlaneMeshVisualizer_7(),
	ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67::get_offset_of_m_FeatheredPlaneMaterial_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7358 = { sizeof (ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7358[7] = 
{
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_ARSession_4(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_ErrorText_5(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_LogText_6(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_MappingStatusText_7(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_SaveButton_8(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_LoadButton_9(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_LogMessages_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7359 = { sizeof (U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7359[4] = 
{
	U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B::get_offset_of_U3CU3E1__state_0(),
	U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B::get_offset_of_U3CU3E2__current_1(),
	U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B::get_offset_of_U3CU3E4__this_2(),
	U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B::get_offset_of_U3CrequestU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7360 = { sizeof (U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7360[8] = 
{
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CU3E1__state_0(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CU3E2__current_1(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CU3E4__this_2(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CsessionSubsystemU3E5__2_3(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CbytesPerFrameU3E5__3_4(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CbytesRemainingU3E5__4_5(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CbinaryReaderU3E5__5_6(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CallBytesU3E5__6_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7361 = { sizeof (CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7361[2] = 
{
	CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B::get_offset_of_m_ConfigurationNames_4(),
	CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B::get_offset_of_m_Dropdown_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7362 = { sizeof (DisableVerticalPlanes_t40082019580280FBDED0DF62CC5DA4E1A8E32D19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7362[1] = 
{
	DisableVerticalPlanes_t40082019580280FBDED0DF62CC5DA4E1A8E32D19::get_offset_of_m_LogText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7363 = { sizeof (LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7363[4] = 
{
	LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319::get_offset_of_U3CbrightnessU3Ek__BackingField_4(),
	LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319::get_offset_of_U3CcolorTemperatureU3Ek__BackingField_5(),
	LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319::get_offset_of_U3CcolorCorrectionU3Ek__BackingField_6(),
	LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319::get_offset_of_m_Light_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7364 = { sizeof (LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7364[5] = 
{
	LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4::get_offset_of_m_BrightnessText_4(),
	LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4::get_offset_of_m_ColorTemperatureText_5(),
	LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4::get_offset_of_m_ColorCorrectionText_6(),
	0,
	LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4::get_offset_of_m_LightEstimation_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7365 = { sizeof (PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC), -1, sizeof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7365[5] = 
{
	PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC::get_offset_of_m_PlacedPrefab_4(),
	PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC::get_offset_of_U3CspawnedObjectU3Ek__BackingField_5(),
	PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields::get_offset_of_onPlacedObject_6(),
	PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC::get_offset_of_m_SessionOrigin_7(),
	PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields::get_offset_of_s_Hits_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7366 = { sizeof (PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2), -1, sizeof(PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7366[4] = 
{
	PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2::get_offset_of_m_PlacedPrefab_4(),
	PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2::get_offset_of_U3CspawnedObjectU3Ek__BackingField_5(),
	PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2_StaticFields::get_offset_of_s_Hits_6(),
	PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2::get_offset_of_m_SessionOrigin_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7367 = { sizeof (PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD), -1, sizeof(PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7367[3] = 
{
	PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD::get_offset_of_m_TogglePlaneDetectionText_4(),
	PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD::get_offset_of_m_ARPlaneManager_5(),
	PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD_StaticFields::get_offset_of_s_Planes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7368 = { sizeof (SetTargetFramerate_tCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7368[1] = 
{
	SetTargetFramerate_tCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0::get_offset_of_m_TargetFrameRate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7369 = { sizeof (TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7369[3] = 
{
	TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6::get_offset_of_m_RawImage_4(),
	TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6::get_offset_of_m_ImageInfo_5(),
	TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6::get_offset_of_m_Texture_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7370 = { sizeof (FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7370[7] = 
{
	0,
	0,
	0,
	FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A::get_offset_of_m_Animator_7(),
	FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A::get_offset_of_m_Plane_8(),
	FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A::get_offset_of_m_ShowTime_9(),
	FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A::get_offset_of_m_UpdatingPlane_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7371 = { sizeof (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C), -1, sizeof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7371[8] = 
{
	0,
	0,
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_PlaneManager_6(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_MoveDeviceAnimation_7(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_TapToPlaceAnimation_8(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields::get_offset_of_s_Planes_9(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_ShowingTapToPlace_10(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_ShowingMoveDevice_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7372 = { sizeof (LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7372[3] = 
{
	0,
	LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50::get_offset_of_assetBundleName_5(),
	LoadAssets_t041E16E022E10FE751B14E752B3E86D51C4ECC50::get_offset_of_assetName_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7373 = { sizeof (U3CStartU3Ed__3_t41684ADC3C5CE830B47B959B085FFCB39DED114B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7373[3] = 
{
	U3CStartU3Ed__3_t41684ADC3C5CE830B47B959B085FFCB39DED114B::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__3_t41684ADC3C5CE830B47B959B085FFCB39DED114B::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__3_t41684ADC3C5CE830B47B959B085FFCB39DED114B::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7374 = { sizeof (U3CInitializeU3Ed__4_t31F872E342422C3745F7768E9630CF38B7236CC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7374[3] = 
{
	U3CInitializeU3Ed__4_t31F872E342422C3745F7768E9630CF38B7236CC4::get_offset_of_U3CU3E1__state_0(),
	U3CInitializeU3Ed__4_t31F872E342422C3745F7768E9630CF38B7236CC4::get_offset_of_U3CU3E2__current_1(),
	U3CInitializeU3Ed__4_t31F872E342422C3745F7768E9630CF38B7236CC4::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7375 = { sizeof (U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7375[7] = 
{
	U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA::get_offset_of_U3CU3E1__state_0(),
	U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA::get_offset_of_U3CU3E2__current_1(),
	U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA::get_offset_of_assetBundleName_2(),
	U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA::get_offset_of_assetName_3(),
	U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA::get_offset_of_U3CU3E4__this_4(),
	U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA::get_offset_of_U3CstartTimeU3E5__2_5(),
	U3CInstantiateGameObjectAsyncU3Ed__5_tD561F032451E154661DA400E17C227BD1859CAEA::get_offset_of_U3CrequestU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7376 = { sizeof (LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7376[2] = 
{
	LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0::get_offset_of_sceneAssetBundle_4(),
	LoadScenes_t6FBEA3570D5A97832D20E8B489C5BE85FFF3BFF0::get_offset_of_sceneName_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7377 = { sizeof (U3CStartU3Ed__2_tEDE4D4A44571214E27D0B44FCEF4761D389CE597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7377[3] = 
{
	U3CStartU3Ed__2_tEDE4D4A44571214E27D0B44FCEF4761D389CE597::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__2_tEDE4D4A44571214E27D0B44FCEF4761D389CE597::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__2_tEDE4D4A44571214E27D0B44FCEF4761D389CE597::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7378 = { sizeof (U3CInitializeU3Ed__3_t3651A4182A021EAA3049CFA47EC561606E05CECE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7378[3] = 
{
	U3CInitializeU3Ed__3_t3651A4182A021EAA3049CFA47EC561606E05CECE::get_offset_of_U3CU3E1__state_0(),
	U3CInitializeU3Ed__3_t3651A4182A021EAA3049CFA47EC561606E05CECE::get_offset_of_U3CU3E2__current_1(),
	U3CInitializeU3Ed__3_t3651A4182A021EAA3049CFA47EC561606E05CECE::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7379 = { sizeof (U3CInitializeLevelAsyncU3Ed__4_t3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7379[6] = 
{
	U3CInitializeLevelAsyncU3Ed__4_t3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E::get_offset_of_U3CU3E1__state_0(),
	U3CInitializeLevelAsyncU3Ed__4_t3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E::get_offset_of_U3CU3E2__current_1(),
	U3CInitializeLevelAsyncU3Ed__4_t3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E::get_offset_of_U3CU3E4__this_2(),
	U3CInitializeLevelAsyncU3Ed__4_t3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E::get_offset_of_levelName_3(),
	U3CInitializeLevelAsyncU3Ed__4_t3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E::get_offset_of_isAdditive_4(),
	U3CInitializeLevelAsyncU3Ed__4_t3FF9DFCA5578F62FB362763D1E35ABB1CB1A1F7E::get_offset_of_U3CstartTimeU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7380 = { sizeof (LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7380[15] = 
{
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B::get_offset_of_sceneAssetBundle_4(),
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B::get_offset_of_sceneName_5(),
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B::get_offset_of_textAssetBundle_6(),
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B::get_offset_of_textAssetName_7(),
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B::get_offset_of_activeVariants_8(),
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B::get_offset_of_bundlesLoaded_9(),
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B::get_offset_of_sd_10(),
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B::get_offset_of_hd_11(),
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B::get_offset_of_normal_12(),
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B::get_offset_of_desert_13(),
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B::get_offset_of_english_14(),
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B::get_offset_of_danish_15(),
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B::get_offset_of_tankAlbedoStyle_16(),
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B::get_offset_of_tankAlbedoResolution_17(),
	LoadTanks_t879582AEE457760FDE6E59833ECB2453EEEA3D0B::get_offset_of_language_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7381 = { sizeof (U3CBeginExampleU3Ed__17_t97D6A873FBC50C13120EA144FE4465BAA4493CA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7381[3] = 
{
	U3CBeginExampleU3Ed__17_t97D6A873FBC50C13120EA144FE4465BAA4493CA5::get_offset_of_U3CU3E1__state_0(),
	U3CBeginExampleU3Ed__17_t97D6A873FBC50C13120EA144FE4465BAA4493CA5::get_offset_of_U3CU3E2__current_1(),
	U3CBeginExampleU3Ed__17_t97D6A873FBC50C13120EA144FE4465BAA4493CA5::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7382 = { sizeof (U3CInitializeU3Ed__18_t814E51B4BB3128CADC5D61333A49108A43FEF8B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7382[3] = 
{
	U3CInitializeU3Ed__18_t814E51B4BB3128CADC5D61333A49108A43FEF8B4::get_offset_of_U3CU3E1__state_0(),
	U3CInitializeU3Ed__18_t814E51B4BB3128CADC5D61333A49108A43FEF8B4::get_offset_of_U3CU3E2__current_1(),
	U3CInitializeU3Ed__18_t814E51B4BB3128CADC5D61333A49108A43FEF8B4::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7383 = { sizeof (U3CInitializeLevelAsyncU3Ed__19_tDD83F9C503DBF78435F94DBE9DFA1E8F5B675069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7383[6] = 
{
	U3CInitializeLevelAsyncU3Ed__19_tDD83F9C503DBF78435F94DBE9DFA1E8F5B675069::get_offset_of_U3CU3E1__state_0(),
	U3CInitializeLevelAsyncU3Ed__19_tDD83F9C503DBF78435F94DBE9DFA1E8F5B675069::get_offset_of_U3CU3E2__current_1(),
	U3CInitializeLevelAsyncU3Ed__19_tDD83F9C503DBF78435F94DBE9DFA1E8F5B675069::get_offset_of_U3CU3E4__this_2(),
	U3CInitializeLevelAsyncU3Ed__19_tDD83F9C503DBF78435F94DBE9DFA1E8F5B675069::get_offset_of_levelName_3(),
	U3CInitializeLevelAsyncU3Ed__19_tDD83F9C503DBF78435F94DBE9DFA1E8F5B675069::get_offset_of_isAdditive_4(),
	U3CInitializeLevelAsyncU3Ed__19_tDD83F9C503DBF78435F94DBE9DFA1E8F5B675069::get_offset_of_U3CstartTimeU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7384 = { sizeof (U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7384[7] = 
{
	U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15::get_offset_of_U3CU3E1__state_0(),
	U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15::get_offset_of_U3CU3E2__current_1(),
	U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15::get_offset_of_assetBundleName_2(),
	U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15::get_offset_of_assetName_3(),
	U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15::get_offset_of_U3CU3E4__this_4(),
	U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15::get_offset_of_U3CstartTimeU3E5__2_5(),
	U3CInstantiateGameObjectAsyncU3Ed__20_t6256914044263C7A2C7E5E1C4AFA4D6530DDFD15::get_offset_of_U3CrequestU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7385 = { sizeof (LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7385[4] = 
{
	0,
	0,
	LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7::get_offset_of_activeVariants_6(),
	LoadVariants_t9063903916BA659A2ED9EE14431E9C99A7D557C7::get_offset_of_bundlesLoaded_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7386 = { sizeof (U3CBeginExampleU3Ed__6_tE817DA7D7BD942FEA3AA7C51343CEBD411308891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7386[3] = 
{
	U3CBeginExampleU3Ed__6_tE817DA7D7BD942FEA3AA7C51343CEBD411308891::get_offset_of_U3CU3E1__state_0(),
	U3CBeginExampleU3Ed__6_tE817DA7D7BD942FEA3AA7C51343CEBD411308891::get_offset_of_U3CU3E2__current_1(),
	U3CBeginExampleU3Ed__6_tE817DA7D7BD942FEA3AA7C51343CEBD411308891::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7387 = { sizeof (U3CInitializeU3Ed__7_t269FB3A3B9BCEE101A36FD8E66714A3C3F996ADB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7387[3] = 
{
	U3CInitializeU3Ed__7_t269FB3A3B9BCEE101A36FD8E66714A3C3F996ADB::get_offset_of_U3CU3E1__state_0(),
	U3CInitializeU3Ed__7_t269FB3A3B9BCEE101A36FD8E66714A3C3F996ADB::get_offset_of_U3CU3E2__current_1(),
	U3CInitializeU3Ed__7_t269FB3A3B9BCEE101A36FD8E66714A3C3F996ADB::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7388 = { sizeof (U3CInitializeLevelAsyncU3Ed__8_t2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7388[6] = 
{
	U3CInitializeLevelAsyncU3Ed__8_t2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6::get_offset_of_U3CU3E1__state_0(),
	U3CInitializeLevelAsyncU3Ed__8_t2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6::get_offset_of_U3CU3E2__current_1(),
	U3CInitializeLevelAsyncU3Ed__8_t2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6::get_offset_of_levelName_2(),
	U3CInitializeLevelAsyncU3Ed__8_t2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6::get_offset_of_isAdditive_3(),
	U3CInitializeLevelAsyncU3Ed__8_t2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6::get_offset_of_U3CU3E4__this_4(),
	U3CInitializeLevelAsyncU3Ed__8_t2B2EB75C7CC4F3C34660217A2C7F137BCC9187C6::get_offset_of_U3CstartTimeU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7389 = { sizeof (URIHelper_tAA55C759A587A6BAA6C56752BE38AB5CF36C9F93), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7389[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7390 = { sizeof (Localizatron_tC7AF71F3CADDA057CB4068F6260C5B90A047962E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7390[3] = 
{
	Localizatron_tC7AF71F3CADDA057CB4068F6260C5B90A047962E::get_offset_of__languagePath_7(),
	Localizatron_tC7AF71F3CADDA057CB4068F6260C5B90A047962E::get_offset_of__currentLanguage_8(),
	Localizatron_tC7AF71F3CADDA057CB4068F6260C5B90A047962E::get_offset_of_languageTable_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7391 = { sizeof (Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7391[2] = 
{
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF::get_offset_of__screenMiddleX_4(),
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF::get_offset_of__screenMiddleY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7392 = { sizeof (IEnumerableExtension_tB0E9A4F82BB6FDA85587C01ACE217018F2FAC253), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7393 = { sizeof (UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233), -1, sizeof(UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7393[5] = 
{
	UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233_StaticFields::get_offset_of_instance_4(),
	UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233_StaticFields::get_offset_of_syncRoot_5(),
	UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233::get_offset_of_dispatcher_6(),
	UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233::get_offset_of_taskDistributor_7(),
	UnityThreadHelper_tD1E5142ACFA8151C129F12A85A94A314FD16C233::get_offset_of_registeredThreads_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7394 = { sizeof (U3CU3Ec__DisplayClass16_0_t442FC0F2F333649450EED970052BE434EC648078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7394[1] = 
{
	U3CU3Ec__DisplayClass16_0_t442FC0F2F333649450EED970052BE434EC648078::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7395 = { sizeof (U3CU3Ec__DisplayClass18_0_tF5AD97BDC607B9D93581B31AD3FB98092FDFEB1F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7395[1] = 
{
	U3CU3Ec__DisplayClass18_0_tF5AD97BDC607B9D93581B31AD3FB98092FDFEB1F::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7396 = { sizeof (U3CU3Ec__DisplayClass19_0_t1FB565FD9DD5CF5E6183CAFC3874FCFD6E622E98), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7396[1] = 
{
	U3CU3Ec__DisplayClass19_0_t1FB565FD9DD5CF5E6183CAFC3874FCFD6E622E98::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7397 = { sizeof (U3CU3Ec__DisplayClass22_0_t16969379ECEBA207E671428B46514F17083B416F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7397[1] = 
{
	U3CU3Ec__DisplayClass22_0_t16969379ECEBA207E671428B46514F17083B416F::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7398 = { sizeof (U3CU3Ec__DisplayClass23_0_t8BB6B3093B707DC5D4CC61B863DD591A70200B8B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7398[1] = 
{
	U3CU3Ec__DisplayClass23_0_t8BB6B3093B707DC5D4CC61B863DD591A70200B8B::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7399 = { sizeof (U3CU3Ec_t8D858DEFEB647A015B589DADBBBA22F95E35E4D6), -1, sizeof(U3CU3Ec_t8D858DEFEB647A015B589DADBBBA22F95E35E4D6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7399[2] = 
{
	U3CU3Ec_t8D858DEFEB647A015B589DADBBBA22F95E35E4D6_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t8D858DEFEB647A015B589DADBBBA22F95E35E4D6_StaticFields::get_offset_of_U3CU3E9__27_0_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
