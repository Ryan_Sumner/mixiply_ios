﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>
struct Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B;
// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>
struct Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466;
// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>
struct Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4;
// System.ArgumentNullException
struct ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>
struct List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A;
// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>
struct List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483;
// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>
struct List_1_tE2C2476479F1A844897C46751D09491B2534EC87;
// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.XRFace>
struct List_1_t9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Experimental.ISubsystemDescriptor
struct ISubsystemDescriptor_tDF5EB3ED639A15690D2CB9993789BB21F24D3934;
// UnityEngine.Experimental.SubsystemDescriptor
struct SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4;
// UnityEngine.Experimental.SubsystemDescriptor`1<System.Object>
struct SubsystemDescriptor_1_t1C1E51FDC44D89022B4C81C15093AD21964A93AE;
// UnityEngine.Experimental.SubsystemDescriptor`1<UnityEngine.XR.FaceSubsystem.XRFaceSubsystem>
struct SubsystemDescriptor_1_t8BC7D01A6BF03D22D133B8FB58E5B2D85675FDD7;
// UnityEngine.Experimental.Subsystem`1<System.Object>
struct Subsystem_1_tC290A9E5F38C778DC33C2DF9B1D10609BC0DC7CA;
// UnityEngine.Experimental.Subsystem`1<UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor>
struct Subsystem_1_t7A31187B2E3DFF03501265C781C9A8547A073F0C;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs[]
struct FaceAddedEventArgsU5BU5D_tB9F39D27E726180A666B7E9B42BFA2DB0E0DCE5F;
// UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs[]
struct FaceRemovedEventArgsU5BU5D_t315CE7244E820D123F6C8065735C18CFE3DA4837;
// UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs[]
struct FaceUpdatedEventArgsU5BU5D_tBCC31A8F2BA3CDE64D72143F74A3A25056625D94;
// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem
struct XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86;
// UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor
struct XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19;
// UnityEngine.XR.FaceSubsystem.XRFace[]
struct XRFaceU5BU5D_t4C663FE7C32161D7F376CB8DCE5B1A840A8E672F;

extern RuntimeClass* Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var;
extern RuntimeClass* FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_il2cpp_TypeInfo_var;
extern RuntimeClass* FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_il2cpp_TypeInfo_var;
extern RuntimeClass* FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_il2cpp_TypeInfo_var;
extern RuntimeClass* FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_tE2C2476479F1A844897C46751D09491B2534EC87_il2cpp_TypeInfo_var;
extern RuntimeClass* SubsystemRegistration_t0A22FECC46483ABBFFC039449407F73FF11F5A1A_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19_il2cpp_TypeInfo_var;
extern RuntimeClass* XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_il2cpp_TypeInfo_var;
extern RuntimeClass* XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral37A0B5FF3D6DDD7EB1501626195A8DC712AD0A51;
extern String_t* _stringLiteral3DE5C8935E5C0504750A6A49DD9533047C38EC8C;
extern String_t* _stringLiteral84DBAEF25399215C4823814053CC133F7AD11C90;
extern String_t* _stringLiteralBE956C6BBE22C9317B08530A0E1E1E9ABED5815A;
extern const RuntimeMethod* Action_1_Invoke_mB27FFCE02ADEA076FA5F2DB7FDDD2840F843702A_RuntimeMethod_var;
extern const RuntimeMethod* Action_1_Invoke_mB4FE57B363F0B93B1DA4E0E3B8AA425AB7B5C9A2_RuntimeMethod_var;
extern const RuntimeMethod* Action_1_Invoke_mB5C96A623A8DD9F97E66150F57C9AFDE3EDB85C4_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3155A03CAD8A96B0EF1E02E4FE8AF5DA522CDCC2_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m4FD30D793AA03BA4EF0D7D2CF85A1141EE9905C8_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_mFECED8B0D7B6FF3521473829AFD852810EED5A32_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m454F2B7C556DBB8E508589BBBDE61EB6178CFCAE_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m67B00ADB5FD5EADF1EAE0E911B37CA27B01D267B_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m7A3BAA9E235E50489080103011B7655AB5071614_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m17C74A966C414AF87B4926F71D5C2D738276CD1B_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m9BEB9ACA83DE929A3EC5EFAF5010BCACDC05887F_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_mA2A1CDFBB945052FF9593C69C6DFBE039C26A456_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m8933F527B5707C1B7AC57EA271F051C9D31DB390_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mB1E98B155D266CFCD2630F9CC1B9F09DB37AF636_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mE4B2ECBE4618987FBA1B0BAC4DB8399E9DCDF343_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m06BA343FB4E149EB045D8D2603E1AD239E1E4477_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m07FCFBF2DE6606E960452AEE8427E1F66B861A64_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m471E9C96E037C142725FB47332197B948E5D38AE_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m5F612B61829DD1074F050617F5ECB93D7A81EB6B_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m8E7A9E8CF891528845C4B071CB8166DDB1A384B8_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_mE389EDEE3E1D94DE8DB369C6D69741DE655FBA18_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_mE5C402CDF41FDFF018276341D49C6864C9950827_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m1ED4F0E86DCB172B575B9FB56A3A3C4B4CC04BA8_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m4C931B506F93C24E8E7B6ADA0233745BC526A3DB_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m844D7301792413D133453D6B32089B45527FA4D9_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_mA0B1C89A6269A80CC746ED26EC65BD390FD75665_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_mD27DD9E2C6D2DD48607B83DF444B3CF65AE90A19_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_mF51861616D3E151AAAA319FF158E26510A0D359C_RuntimeMethod_var;
extern const RuntimeMethod* SubsystemDescriptor_1__ctor_m0B1EAA83AAE682789A90078505E8DEE4048F8C1E_RuntimeMethod_var;
extern const RuntimeMethod* Subsystem_1__ctor_m1CF0FE2230E65CDBD346CF4F52035DD2BC9D2822_RuntimeMethod_var;
extern const RuntimeMethod* XRFaceSubsystem_TryGetAllFaces_mAEF395610A1CA3CA934A05F442DE066B5E9C6D0B_RuntimeMethod_var;
extern const RuntimeMethod* XRFaceSubsystem_TryGetFaceMeshIndices_m13B824E553BA6411FB0211C707595706E7B77EA1_RuntimeMethod_var;
extern const RuntimeMethod* XRFaceSubsystem_TryGetFaceMeshUVs_m3815AB707969C1199BEE286F57B540DFC3C059FE_RuntimeMethod_var;
extern const RuntimeMethod* XRFaceSubsystem_TryGetFaceMeshVertices_m0AE585B6EE72BB59AD0CD4816787A990CCF4712E_RuntimeMethod_var;
extern const uint32_t FaceAddedEventArgs_Equals_m3D86CD998A55813D718D2482307DBF45B4DEF909_MetadataUsageId;
extern const uint32_t FaceRemovedEventArgs_Equals_m66078B92345895ABCB42AA01078CAB075E218075_MetadataUsageId;
extern const uint32_t FaceSubsystemParams_Equals_m78CA1466752559D70811FFF4873496CFB7B4FA59_MetadataUsageId;
extern const uint32_t FaceSubsystemParams_Equals_mF5B83F9C750E61CED7610753A84927E59AC9B4DF_MetadataUsageId;
extern const uint32_t FaceUpdatedEventArgs_Equals_m07A86F9854A5DA3DF39FFA2A8B89128F7475D80D_MetadataUsageId;
extern const uint32_t XRFaceSubsystemDescriptor_Create_m11BC388D605EE249E0249F9D4CCC85FA9B9435C4_MetadataUsageId;
extern const uint32_t XRFaceSubsystemDescriptor__ctor_m64CAD1F471D073C278B7BB46ECB6A8A86CC35BE0_MetadataUsageId;
extern const uint32_t XRFaceSubsystem_InvokeFaceAddedCallback_m4FABBC7D82C155B417631DD0BAAB3F34B6AA4168_MetadataUsageId;
extern const uint32_t XRFaceSubsystem_InvokeFaceRemovedCallback_mF19536F930D1F55268D8EFFAC0A44615B5ED1406_MetadataUsageId;
extern const uint32_t XRFaceSubsystem_InvokeFaceUpdatedCallback_m7D8F0ADC24BA2D681B7A9F01AF7E10953E46F1E7_MetadataUsageId;
extern const uint32_t XRFaceSubsystem_OnBeginFrame_m42D691401EE4056D7BE5142BFAEFA492CD02228A_MetadataUsageId;
extern const uint32_t XRFaceSubsystem_TryGetAllFaces_mAEF395610A1CA3CA934A05F442DE066B5E9C6D0B_MetadataUsageId;
extern const uint32_t XRFaceSubsystem_TryGetFaceMeshIndices_m13B824E553BA6411FB0211C707595706E7B77EA1_MetadataUsageId;
extern const uint32_t XRFaceSubsystem_TryGetFaceMeshUVs_m3815AB707969C1199BEE286F57B540DFC3C059FE_MetadataUsageId;
extern const uint32_t XRFaceSubsystem_TryGetFaceMeshVertices_m0AE585B6EE72BB59AD0CD4816787A990CCF4712E_MetadataUsageId;
extern const uint32_t XRFaceSubsystem__ctor_mE2CFD752717175ECF40CD6A8A267ED8768352C8F_MetadataUsageId;
extern const uint32_t XRFaceSubsystem_add_faceAdded_m9063078B8F717927ADEB63885B231F862E269F87_MetadataUsageId;
extern const uint32_t XRFaceSubsystem_add_faceRemoved_m56B1750EA0FE784A945908FFA89D2E9650B12C43_MetadataUsageId;
extern const uint32_t XRFaceSubsystem_add_faceUpdated_m7A38CEF5EAFD6BADECE5AE02B3DC1775A261FA96_MetadataUsageId;
extern const uint32_t XRFaceSubsystem_remove_faceAdded_m51A63CB063397B2B0AA0340267FAB2A799325BC1_MetadataUsageId;
extern const uint32_t XRFaceSubsystem_remove_faceRemoved_mC3F6C6B0A2585F72E2E5649B9D21ADC493AF2543_MetadataUsageId;
extern const uint32_t XRFaceSubsystem_remove_faceUpdated_m21DFD1FAD1E2F0CE775C2C1EC66A5C8070EBE637_MetadataUsageId;
extern const uint32_t XRFace_Equals_m97884868087251BBF598B60530D90989ED5494AB_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_TD7C26C6BCAC76F130F59027F1362DDBCC127637D_H
#define U3CMODULEU3E_TD7C26C6BCAC76F130F59027F1362DDBCC127637D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tD7C26C6BCAC76F130F59027F1362DDBCC127637D 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TD7C26C6BCAC76F130F59027F1362DDBCC127637D_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_TE1526161A558A17A39A8B69D8EEF3801393B6226_H
#define LIST_1_TE1526161A558A17A39A8B69D8EEF3801393B6226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____items_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TE1526161A558A17A39A8B69D8EEF3801393B6226_H
#ifndef LIST_1_T0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_H
#define LIST_1_T0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct  List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____items_1)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_StaticFields, ____emptyArray_5)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_H
#ifndef LIST_1_TFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_H
#define LIST_1_TFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____items_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_StaticFields, ____emptyArray_5)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_H
#ifndef LIST_1_TAF498548EF78A979CA19AF2EEE32849D1F3B737A_H
#define LIST_1_TAF498548EF78A979CA19AF2EEE32849D1F3B737A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>
struct  List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	FaceAddedEventArgsU5BU5D_tB9F39D27E726180A666B7E9B42BFA2DB0E0DCE5F* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A, ____items_1)); }
	inline FaceAddedEventArgsU5BU5D_tB9F39D27E726180A666B7E9B42BFA2DB0E0DCE5F* get__items_1() const { return ____items_1; }
	inline FaceAddedEventArgsU5BU5D_tB9F39D27E726180A666B7E9B42BFA2DB0E0DCE5F** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(FaceAddedEventArgsU5BU5D_tB9F39D27E726180A666B7E9B42BFA2DB0E0DCE5F* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	FaceAddedEventArgsU5BU5D_tB9F39D27E726180A666B7E9B42BFA2DB0E0DCE5F* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A_StaticFields, ____emptyArray_5)); }
	inline FaceAddedEventArgsU5BU5D_tB9F39D27E726180A666B7E9B42BFA2DB0E0DCE5F* get__emptyArray_5() const { return ____emptyArray_5; }
	inline FaceAddedEventArgsU5BU5D_tB9F39D27E726180A666B7E9B42BFA2DB0E0DCE5F** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(FaceAddedEventArgsU5BU5D_tB9F39D27E726180A666B7E9B42BFA2DB0E0DCE5F* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TAF498548EF78A979CA19AF2EEE32849D1F3B737A_H
#ifndef LIST_1_T00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483_H
#define LIST_1_T00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>
struct  List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	FaceRemovedEventArgsU5BU5D_t315CE7244E820D123F6C8065735C18CFE3DA4837* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483, ____items_1)); }
	inline FaceRemovedEventArgsU5BU5D_t315CE7244E820D123F6C8065735C18CFE3DA4837* get__items_1() const { return ____items_1; }
	inline FaceRemovedEventArgsU5BU5D_t315CE7244E820D123F6C8065735C18CFE3DA4837** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(FaceRemovedEventArgsU5BU5D_t315CE7244E820D123F6C8065735C18CFE3DA4837* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	FaceRemovedEventArgsU5BU5D_t315CE7244E820D123F6C8065735C18CFE3DA4837* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483_StaticFields, ____emptyArray_5)); }
	inline FaceRemovedEventArgsU5BU5D_t315CE7244E820D123F6C8065735C18CFE3DA4837* get__emptyArray_5() const { return ____emptyArray_5; }
	inline FaceRemovedEventArgsU5BU5D_t315CE7244E820D123F6C8065735C18CFE3DA4837** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(FaceRemovedEventArgsU5BU5D_t315CE7244E820D123F6C8065735C18CFE3DA4837* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483_H
#ifndef LIST_1_TE2C2476479F1A844897C46751D09491B2534EC87_H
#define LIST_1_TE2C2476479F1A844897C46751D09491B2534EC87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>
struct  List_1_tE2C2476479F1A844897C46751D09491B2534EC87  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	FaceUpdatedEventArgsU5BU5D_tBCC31A8F2BA3CDE64D72143F74A3A25056625D94* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE2C2476479F1A844897C46751D09491B2534EC87, ____items_1)); }
	inline FaceUpdatedEventArgsU5BU5D_tBCC31A8F2BA3CDE64D72143F74A3A25056625D94* get__items_1() const { return ____items_1; }
	inline FaceUpdatedEventArgsU5BU5D_tBCC31A8F2BA3CDE64D72143F74A3A25056625D94** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(FaceUpdatedEventArgsU5BU5D_tBCC31A8F2BA3CDE64D72143F74A3A25056625D94* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE2C2476479F1A844897C46751D09491B2534EC87, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE2C2476479F1A844897C46751D09491B2534EC87, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE2C2476479F1A844897C46751D09491B2534EC87, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tE2C2476479F1A844897C46751D09491B2534EC87_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	FaceUpdatedEventArgsU5BU5D_tBCC31A8F2BA3CDE64D72143F74A3A25056625D94* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE2C2476479F1A844897C46751D09491B2534EC87_StaticFields, ____emptyArray_5)); }
	inline FaceUpdatedEventArgsU5BU5D_tBCC31A8F2BA3CDE64D72143F74A3A25056625D94* get__emptyArray_5() const { return ____emptyArray_5; }
	inline FaceUpdatedEventArgsU5BU5D_tBCC31A8F2BA3CDE64D72143F74A3A25056625D94** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(FaceUpdatedEventArgsU5BU5D_tBCC31A8F2BA3CDE64D72143F74A3A25056625D94* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TE2C2476479F1A844897C46751D09491B2534EC87_H
#ifndef LIST_1_T9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30_H
#define LIST_1_T9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.XRFace>
struct  List_1_t9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	XRFaceU5BU5D_t4C663FE7C32161D7F376CB8DCE5B1A840A8E672F* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30, ____items_1)); }
	inline XRFaceU5BU5D_t4C663FE7C32161D7F376CB8DCE5B1A840A8E672F* get__items_1() const { return ____items_1; }
	inline XRFaceU5BU5D_t4C663FE7C32161D7F376CB8DCE5B1A840A8E672F** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(XRFaceU5BU5D_t4C663FE7C32161D7F376CB8DCE5B1A840A8E672F* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	XRFaceU5BU5D_t4C663FE7C32161D7F376CB8DCE5B1A840A8E672F* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30_StaticFields, ____emptyArray_5)); }
	inline XRFaceU5BU5D_t4C663FE7C32161D7F376CB8DCE5B1A840A8E672F* get__emptyArray_5() const { return ____emptyArray_5; }
	inline XRFaceU5BU5D_t4C663FE7C32161D7F376CB8DCE5B1A840A8E672F** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(XRFaceU5BU5D_t4C663FE7C32161D7F376CB8DCE5B1A840A8E672F* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef SUBSYSTEM_T9D40133A9FEB984E2C3695B077DA4DDFC91CA181_H
#define SUBSYSTEM_T9D40133A9FEB984E2C3695B077DA4DDFC91CA181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Subsystem
struct  Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181  : public RuntimeObject
{
public:
	// UnityEngine.Experimental.ISubsystemDescriptor UnityEngine.Experimental.Subsystem::m_subsystemDescriptor
	RuntimeObject* ___m_subsystemDescriptor_0;

public:
	inline static int32_t get_offset_of_m_subsystemDescriptor_0() { return static_cast<int32_t>(offsetof(Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181, ___m_subsystemDescriptor_0)); }
	inline RuntimeObject* get_m_subsystemDescriptor_0() const { return ___m_subsystemDescriptor_0; }
	inline RuntimeObject** get_address_of_m_subsystemDescriptor_0() { return &___m_subsystemDescriptor_0; }
	inline void set_m_subsystemDescriptor_0(RuntimeObject* value)
	{
		___m_subsystemDescriptor_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_subsystemDescriptor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEM_T9D40133A9FEB984E2C3695B077DA4DDFC91CA181_H
#ifndef SUBSYSTEMDESCRIPTOR_T39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4_H
#define SUBSYSTEMDESCRIPTOR_T39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.SubsystemDescriptor
struct  SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4  : public RuntimeObject
{
public:
	// System.String UnityEngine.Experimental.SubsystemDescriptor::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// System.Type UnityEngine.Experimental.SubsystemDescriptor::<subsystemImplementationType>k__BackingField
	Type_t * ___U3CsubsystemImplementationTypeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsubsystemImplementationTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4, ___U3CsubsystemImplementationTypeU3Ek__BackingField_1)); }
	inline Type_t * get_U3CsubsystemImplementationTypeU3Ek__BackingField_1() const { return ___U3CsubsystemImplementationTypeU3Ek__BackingField_1; }
	inline Type_t ** get_address_of_U3CsubsystemImplementationTypeU3Ek__BackingField_1() { return &___U3CsubsystemImplementationTypeU3Ek__BackingField_1; }
	inline void set_U3CsubsystemImplementationTypeU3Ek__BackingField_1(Type_t * value)
	{
		___U3CsubsystemImplementationTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsubsystemImplementationTypeU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEMDESCRIPTOR_T39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef SUBSYSTEMDESCRIPTOR_1_T8BC7D01A6BF03D22D133B8FB58E5B2D85675FDD7_H
#define SUBSYSTEMDESCRIPTOR_1_T8BC7D01A6BF03D22D133B8FB58E5B2D85675FDD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.SubsystemDescriptor`1<UnityEngine.XR.FaceSubsystem.XRFaceSubsystem>
struct  SubsystemDescriptor_1_t8BC7D01A6BF03D22D133B8FB58E5B2D85675FDD7  : public SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEMDESCRIPTOR_1_T8BC7D01A6BF03D22D133B8FB58E5B2D85675FDD7_H
#ifndef SUBSYSTEM_1_T7A31187B2E3DFF03501265C781C9A8547A073F0C_H
#define SUBSYSTEM_1_T7A31187B2E3DFF03501265C781C9A8547A073F0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Subsystem`1<UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor>
struct  Subsystem_1_t7A31187B2E3DFF03501265C781C9A8547A073F0C  : public Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEM_1_T7A31187B2E3DFF03501265C781C9A8547A073F0C_H
#ifndef TRACKABLEID_TA539F57E82A04D410FE11E10ACC830CF7CD71F7B_H
#define TRACKABLEID_TA539F57E82A04D410FE11E10ACC830CF7CD71F7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.TrackableId
struct  TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B 
{
public:
	// System.UInt64 UnityEngine.Experimental.XR.TrackableId::m_SubId1
	uint64_t ___m_SubId1_1;
	// System.UInt64 UnityEngine.Experimental.XR.TrackableId::m_SubId2
	uint64_t ___m_SubId2_2;

public:
	inline static int32_t get_offset_of_m_SubId1_1() { return static_cast<int32_t>(offsetof(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B, ___m_SubId1_1)); }
	inline uint64_t get_m_SubId1_1() const { return ___m_SubId1_1; }
	inline uint64_t* get_address_of_m_SubId1_1() { return &___m_SubId1_1; }
	inline void set_m_SubId1_1(uint64_t value)
	{
		___m_SubId1_1 = value;
	}

	inline static int32_t get_offset_of_m_SubId2_2() { return static_cast<int32_t>(offsetof(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B, ___m_SubId2_2)); }
	inline uint64_t get_m_SubId2_2() const { return ___m_SubId2_2; }
	inline uint64_t* get_address_of_m_SubId2_2() { return &___m_SubId2_2; }
	inline void set_m_SubId2_2(uint64_t value)
	{
		___m_SubId2_2 = value;
	}
};

struct TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B_StaticFields
{
public:
	// UnityEngine.Experimental.XR.TrackableId UnityEngine.Experimental.XR.TrackableId::s_InvalidId
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___s_InvalidId_0;

public:
	inline static int32_t get_offset_of_s_InvalidId_0() { return static_cast<int32_t>(offsetof(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B_StaticFields, ___s_InvalidId_0)); }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  get_s_InvalidId_0() const { return ___s_InvalidId_0; }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B * get_address_of_s_InvalidId_0() { return &___s_InvalidId_0; }
	inline void set_s_InvalidId_0(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  value)
	{
		___s_InvalidId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEID_TA539F57E82A04D410FE11E10ACC830CF7CD71F7B_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef ARGUMENTEXCEPTION_TEDCD16F20A09ECE461C3DA766C16EDA8864057D1_H
#define ARGUMENTEXCEPTION_TEDCD16F20A09ECE461C3DA766C16EDA8864057D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_paramName_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_TEDCD16F20A09ECE461C3DA766C16EDA8864057D1_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#define RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifndef POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#define POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Pose
struct  Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 
{
public:
	// UnityEngine.Vector3 UnityEngine.Pose::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Quaternion UnityEngine.Pose::rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___rotation_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rotation_1() const { return ___rotation_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rotation_1 = value;
	}
};

struct Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields
{
public:
	// UnityEngine.Pose UnityEngine.Pose::k_Identity
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___k_Identity_2;

public:
	inline static int32_t get_offset_of_k_Identity_2() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields, ___k_Identity_2)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_k_Identity_2() const { return ___k_Identity_2; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_k_Identity_2() { return &___k_Identity_2; }
	inline void set_k_Identity_2(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___k_Identity_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#ifndef FACESUBSYSTEMCAPABILITIES_TA2A2AD15E172EF1B91CE85994931F8C11330BB2C_H
#define FACESUBSYSTEMCAPABILITIES_TA2A2AD15E172EF1B91CE85994931F8C11330BB2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemCapabilities
struct  FaceSubsystemCapabilities_tA2A2AD15E172EF1B91CE85994931F8C11330BB2C 
{
public:
	// System.Int32 UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemCapabilities::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FaceSubsystemCapabilities_tA2A2AD15E172EF1B91CE85994931F8C11330BB2C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACESUBSYSTEMCAPABILITIES_TA2A2AD15E172EF1B91CE85994931F8C11330BB2C_H
#ifndef XRFACESUBSYSTEM_TE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_H
#define XRFACESUBSYSTEM_TE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem
struct  XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86  : public Subsystem_1_t7A31187B2E3DFF03501265C781C9A8547A073F0C
{
public:
	// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::faceAdded
	Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * ___faceAdded_4;
	// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::faceUpdated
	Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * ___faceUpdated_5;
	// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::faceRemoved
	Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * ___faceRemoved_6;

public:
	inline static int32_t get_offset_of_faceAdded_4() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86, ___faceAdded_4)); }
	inline Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * get_faceAdded_4() const { return ___faceAdded_4; }
	inline Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B ** get_address_of_faceAdded_4() { return &___faceAdded_4; }
	inline void set_faceAdded_4(Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * value)
	{
		___faceAdded_4 = value;
		Il2CppCodeGenWriteBarrier((&___faceAdded_4), value);
	}

	inline static int32_t get_offset_of_faceUpdated_5() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86, ___faceUpdated_5)); }
	inline Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * get_faceUpdated_5() const { return ___faceUpdated_5; }
	inline Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 ** get_address_of_faceUpdated_5() { return &___faceUpdated_5; }
	inline void set_faceUpdated_5(Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * value)
	{
		___faceUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___faceUpdated_5), value);
	}

	inline static int32_t get_offset_of_faceRemoved_6() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86, ___faceRemoved_6)); }
	inline Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * get_faceRemoved_6() const { return ___faceRemoved_6; }
	inline Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 ** get_address_of_faceRemoved_6() { return &___faceRemoved_6; }
	inline void set_faceRemoved_6(Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * value)
	{
		___faceRemoved_6 = value;
		Il2CppCodeGenWriteBarrier((&___faceRemoved_6), value);
	}
};

struct XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::s_FacesAddedThisFrame
	List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * ___s_FacesAddedThisFrame_1;
	// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::s_FacesUpdatedThisFrame
	List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * ___s_FacesUpdatedThisFrame_2;
	// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::s_FacesRemovedThisFrame
	List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * ___s_FacesRemovedThisFrame_3;

public:
	inline static int32_t get_offset_of_s_FacesAddedThisFrame_1() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields, ___s_FacesAddedThisFrame_1)); }
	inline List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * get_s_FacesAddedThisFrame_1() const { return ___s_FacesAddedThisFrame_1; }
	inline List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A ** get_address_of_s_FacesAddedThisFrame_1() { return &___s_FacesAddedThisFrame_1; }
	inline void set_s_FacesAddedThisFrame_1(List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * value)
	{
		___s_FacesAddedThisFrame_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_FacesAddedThisFrame_1), value);
	}

	inline static int32_t get_offset_of_s_FacesUpdatedThisFrame_2() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields, ___s_FacesUpdatedThisFrame_2)); }
	inline List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * get_s_FacesUpdatedThisFrame_2() const { return ___s_FacesUpdatedThisFrame_2; }
	inline List_1_tE2C2476479F1A844897C46751D09491B2534EC87 ** get_address_of_s_FacesUpdatedThisFrame_2() { return &___s_FacesUpdatedThisFrame_2; }
	inline void set_s_FacesUpdatedThisFrame_2(List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * value)
	{
		___s_FacesUpdatedThisFrame_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_FacesUpdatedThisFrame_2), value);
	}

	inline static int32_t get_offset_of_s_FacesRemovedThisFrame_3() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields, ___s_FacesRemovedThisFrame_3)); }
	inline List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * get_s_FacesRemovedThisFrame_3() const { return ___s_FacesRemovedThisFrame_3; }
	inline List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 ** get_address_of_s_FacesRemovedThisFrame_3() { return &___s_FacesRemovedThisFrame_3; }
	inline void set_s_FacesRemovedThisFrame_3(List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * value)
	{
		___s_FacesRemovedThisFrame_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_FacesRemovedThisFrame_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRFACESUBSYSTEM_TE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_H
#ifndef XRFACESUBSYSTEMDESCRIPTOR_T6952E974969B7D18CA2047C47FC5EB740507FA19_H
#define XRFACESUBSYSTEMDESCRIPTOR_T6952E974969B7D18CA2047C47FC5EB740507FA19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor
struct  XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19  : public SubsystemDescriptor_1_t8BC7D01A6BF03D22D133B8FB58E5B2D85675FDD7
{
public:
	// System.Boolean UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor::<supportsFacePose>k__BackingField
	bool ___U3CsupportsFacePoseU3Ek__BackingField_2;
	// System.Boolean UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor::<supportsFaceMeshVerticesAndIndices>k__BackingField
	bool ___U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3;
	// System.Boolean UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor::<supportsFaceMeshUVs>k__BackingField
	bool ___U3CsupportsFaceMeshUVsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CsupportsFacePoseU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19, ___U3CsupportsFacePoseU3Ek__BackingField_2)); }
	inline bool get_U3CsupportsFacePoseU3Ek__BackingField_2() const { return ___U3CsupportsFacePoseU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CsupportsFacePoseU3Ek__BackingField_2() { return &___U3CsupportsFacePoseU3Ek__BackingField_2; }
	inline void set_U3CsupportsFacePoseU3Ek__BackingField_2(bool value)
	{
		___U3CsupportsFacePoseU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19, ___U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3)); }
	inline bool get_U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3() const { return ___U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3() { return &___U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3; }
	inline void set_U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3(bool value)
	{
		___U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CsupportsFaceMeshUVsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19, ___U3CsupportsFaceMeshUVsU3Ek__BackingField_4)); }
	inline bool get_U3CsupportsFaceMeshUVsU3Ek__BackingField_4() const { return ___U3CsupportsFaceMeshUVsU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CsupportsFaceMeshUVsU3Ek__BackingField_4() { return &___U3CsupportsFaceMeshUVsU3Ek__BackingField_4; }
	inline void set_U3CsupportsFaceMeshUVsU3Ek__BackingField_4(bool value)
	{
		___U3CsupportsFaceMeshUVsU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRFACESUBSYSTEMDESCRIPTOR_T6952E974969B7D18CA2047C47FC5EB740507FA19_H
#ifndef ARGUMENTNULLEXCEPTION_T581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_H
#define ARGUMENTNULLEXCEPTION_T581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef FACESUBSYSTEMPARAMS_TA2026F8E1817583A4FCE47A581DD5C29F3015040_H
#define FACESUBSYSTEMPARAMS_TA2026F8E1817583A4FCE47A581DD5C29F3015040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams
struct  FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 
{
public:
	// System.String UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::id
	String_t* ___id_0;
	// System.Type UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::implementationType
	Type_t * ___implementationType_1;
	// UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemCapabilities UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::<capabilities>k__BackingField
	int32_t ___U3CcapabilitiesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_implementationType_1() { return static_cast<int32_t>(offsetof(FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040, ___implementationType_1)); }
	inline Type_t * get_implementationType_1() const { return ___implementationType_1; }
	inline Type_t ** get_address_of_implementationType_1() { return &___implementationType_1; }
	inline void set_implementationType_1(Type_t * value)
	{
		___implementationType_1 = value;
		Il2CppCodeGenWriteBarrier((&___implementationType_1), value);
	}

	inline static int32_t get_offset_of_U3CcapabilitiesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040, ___U3CcapabilitiesU3Ek__BackingField_2)); }
	inline int32_t get_U3CcapabilitiesU3Ek__BackingField_2() const { return ___U3CcapabilitiesU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CcapabilitiesU3Ek__BackingField_2() { return &___U3CcapabilitiesU3Ek__BackingField_2; }
	inline void set_U3CcapabilitiesU3Ek__BackingField_2(int32_t value)
	{
		___U3CcapabilitiesU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams
struct FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshaled_pinvoke
{
	char* ___id_0;
	Type_t * ___implementationType_1;
	int32_t ___U3CcapabilitiesU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams
struct FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshaled_com
{
	Il2CppChar* ___id_0;
	Type_t * ___implementationType_1;
	int32_t ___U3CcapabilitiesU3Ek__BackingField_2;
};
#endif // FACESUBSYSTEMPARAMS_TA2026F8E1817583A4FCE47A581DD5C29F3015040_H
#ifndef XRFACE_TD2DF3125FE693EE9DABE72A78349CB7F07A51246_H
#define XRFACE_TD2DF3125FE693EE9DABE72A78349CB7F07A51246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.XRFace
struct  XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 
{
public:
	// UnityEngine.Experimental.XR.TrackableId UnityEngine.XR.FaceSubsystem.XRFace::m_TrackableId
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___m_TrackableId_0;
	// UnityEngine.Pose UnityEngine.XR.FaceSubsystem.XRFace::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_1;
	// System.Int32 UnityEngine.XR.FaceSubsystem.XRFace::m_WasUpdated
	int32_t ___m_WasUpdated_2;
	// System.Int32 UnityEngine.XR.FaceSubsystem.XRFace::m_IsTracked
	int32_t ___m_IsTracked_3;

public:
	inline static int32_t get_offset_of_m_TrackableId_0() { return static_cast<int32_t>(offsetof(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246, ___m_TrackableId_0)); }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  get_m_TrackableId_0() const { return ___m_TrackableId_0; }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B * get_address_of_m_TrackableId_0() { return &___m_TrackableId_0; }
	inline void set_m_TrackableId_0(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  value)
	{
		___m_TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_m_Pose_1() { return static_cast<int32_t>(offsetof(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246, ___m_Pose_1)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_1() const { return ___m_Pose_1; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_1() { return &___m_Pose_1; }
	inline void set_m_Pose_1(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_1 = value;
	}

	inline static int32_t get_offset_of_m_WasUpdated_2() { return static_cast<int32_t>(offsetof(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246, ___m_WasUpdated_2)); }
	inline int32_t get_m_WasUpdated_2() const { return ___m_WasUpdated_2; }
	inline int32_t* get_address_of_m_WasUpdated_2() { return &___m_WasUpdated_2; }
	inline void set_m_WasUpdated_2(int32_t value)
	{
		___m_WasUpdated_2 = value;
	}

	inline static int32_t get_offset_of_m_IsTracked_3() { return static_cast<int32_t>(offsetof(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246, ___m_IsTracked_3)); }
	inline int32_t get_m_IsTracked_3() const { return ___m_IsTracked_3; }
	inline int32_t* get_address_of_m_IsTracked_3() { return &___m_IsTracked_3; }
	inline void set_m_IsTracked_3(int32_t value)
	{
		___m_IsTracked_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRFACE_TD2DF3125FE693EE9DABE72A78349CB7F07A51246_H
#ifndef FACEADDEDEVENTARGS_T9D33653893D8E72028F38DD5820FE6E488491B0C_H
#define FACEADDEDEVENTARGS_T9D33653893D8E72028F38DD5820FE6E488491B0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs
struct  FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C 
{
public:
	// UnityEngine.XR.FaceSubsystem.XRFace UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::<xrFace>k__BackingField
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::<xrFaceSubsystem>k__BackingField
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CxrFaceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C, ___U3CxrFaceU3Ek__BackingField_0)); }
	inline XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  get_U3CxrFaceU3Ek__BackingField_0() const { return ___U3CxrFaceU3Ek__BackingField_0; }
	inline XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * get_address_of_U3CxrFaceU3Ek__BackingField_0() { return &___U3CxrFaceU3Ek__BackingField_0; }
	inline void set_U3CxrFaceU3Ek__BackingField_0(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  value)
	{
		___U3CxrFaceU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CxrFaceSubsystemU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C, ___U3CxrFaceSubsystemU3Ek__BackingField_1)); }
	inline XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * get_U3CxrFaceSubsystemU3Ek__BackingField_1() const { return ___U3CxrFaceSubsystemU3Ek__BackingField_1; }
	inline XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 ** get_address_of_U3CxrFaceSubsystemU3Ek__BackingField_1() { return &___U3CxrFaceSubsystemU3Ek__BackingField_1; }
	inline void set_U3CxrFaceSubsystemU3Ek__BackingField_1(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * value)
	{
		___U3CxrFaceSubsystemU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CxrFaceSubsystemU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs
struct FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshaled_pinvoke
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs
struct FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshaled_com
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;
};
#endif // FACEADDEDEVENTARGS_T9D33653893D8E72028F38DD5820FE6E488491B0C_H
#ifndef FACEREMOVEDEVENTARGS_T2D9E4A417A63098195E12566E34160103841D651_H
#define FACEREMOVEDEVENTARGS_T2D9E4A417A63098195E12566E34160103841D651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs
struct  FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 
{
public:
	// UnityEngine.XR.FaceSubsystem.XRFace UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::<xrFace>k__BackingField
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::<xrFaceSubsystem>k__BackingField
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CxrFaceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651, ___U3CxrFaceU3Ek__BackingField_0)); }
	inline XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  get_U3CxrFaceU3Ek__BackingField_0() const { return ___U3CxrFaceU3Ek__BackingField_0; }
	inline XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * get_address_of_U3CxrFaceU3Ek__BackingField_0() { return &___U3CxrFaceU3Ek__BackingField_0; }
	inline void set_U3CxrFaceU3Ek__BackingField_0(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  value)
	{
		___U3CxrFaceU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CxrFaceSubsystemU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651, ___U3CxrFaceSubsystemU3Ek__BackingField_1)); }
	inline XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * get_U3CxrFaceSubsystemU3Ek__BackingField_1() const { return ___U3CxrFaceSubsystemU3Ek__BackingField_1; }
	inline XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 ** get_address_of_U3CxrFaceSubsystemU3Ek__BackingField_1() { return &___U3CxrFaceSubsystemU3Ek__BackingField_1; }
	inline void set_U3CxrFaceSubsystemU3Ek__BackingField_1(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * value)
	{
		___U3CxrFaceSubsystemU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CxrFaceSubsystemU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs
struct FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshaled_pinvoke
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs
struct FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshaled_com
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;
};
#endif // FACEREMOVEDEVENTARGS_T2D9E4A417A63098195E12566E34160103841D651_H
#ifndef FACEUPDATEDEVENTARGS_T1EB3DBB5A762065163012A9BACA88A75C7010881_H
#define FACEUPDATEDEVENTARGS_T1EB3DBB5A762065163012A9BACA88A75C7010881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs
struct  FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 
{
public:
	// UnityEngine.XR.FaceSubsystem.XRFace UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::<xrFace>k__BackingField
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::<xrFaceSubsystem>k__BackingField
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CxrFaceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881, ___U3CxrFaceU3Ek__BackingField_0)); }
	inline XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  get_U3CxrFaceU3Ek__BackingField_0() const { return ___U3CxrFaceU3Ek__BackingField_0; }
	inline XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * get_address_of_U3CxrFaceU3Ek__BackingField_0() { return &___U3CxrFaceU3Ek__BackingField_0; }
	inline void set_U3CxrFaceU3Ek__BackingField_0(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  value)
	{
		___U3CxrFaceU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CxrFaceSubsystemU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881, ___U3CxrFaceSubsystemU3Ek__BackingField_1)); }
	inline XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * get_U3CxrFaceSubsystemU3Ek__BackingField_1() const { return ___U3CxrFaceSubsystemU3Ek__BackingField_1; }
	inline XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 ** get_address_of_U3CxrFaceSubsystemU3Ek__BackingField_1() { return &___U3CxrFaceSubsystemU3Ek__BackingField_1; }
	inline void set_U3CxrFaceSubsystemU3Ek__BackingField_1(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * value)
	{
		___U3CxrFaceSubsystemU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CxrFaceSubsystemU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs
struct FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshaled_pinvoke
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs
struct FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshaled_com
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___U3CxrFaceU3Ek__BackingField_0;
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___U3CxrFaceSubsystemU3Ek__BackingField_1;
};
#endif // FACEUPDATEDEVENTARGS_T1EB3DBB5A762065163012A9BACA88A75C7010881_H
#ifndef ACTION_1_TC60D9BF31F8190A675CBE7DE6C644BBC432EA87B_H
#define ACTION_1_TC60D9BF31F8190A675CBE7DE6C644BBC432EA87B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>
struct  Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_TC60D9BF31F8190A675CBE7DE6C644BBC432EA87B_H
#ifndef ACTION_1_T9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466_H
#define ACTION_1_T9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>
struct  Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466_H
#ifndef ACTION_1_TFC89EA1D6FACA3D17CE797444240A0D4CA9127D4_H
#define ACTION_1_TFC89EA1D6FACA3D17CE797444240A0D4CA9127D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>
struct  Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_TFC89EA1D6FACA3D17CE797444240A0D4CA9127D4_H
#ifndef ENUMERATOR_T3561B819862327744BA5B81A2CE1C15CF60A3175_H
#define ENUMERATOR_T3561B819862327744BA5B81A2CE1C15CF60A3175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>
struct  Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175, ___list_0)); }
	inline List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * get_list_0() const { return ___list_0; }
	inline List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175, ___current_3)); }
	inline FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  get_current_3() const { return ___current_3; }
	inline FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3561B819862327744BA5B81A2CE1C15CF60A3175_H
#ifndef ENUMERATOR_TB20AD31027BC3712B788271F96C8DB3F264A1DF2_H
#define ENUMERATOR_TB20AD31027BC3712B788271F96C8DB3F264A1DF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>
struct  Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2, ___list_0)); }
	inline List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * get_list_0() const { return ___list_0; }
	inline List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2, ___current_3)); }
	inline FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  get_current_3() const { return ___current_3; }
	inline FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_TB20AD31027BC3712B788271F96C8DB3F264A1DF2_H
#ifndef ENUMERATOR_T85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52_H
#define ENUMERATOR_T85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>
struct  Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52, ___list_0)); }
	inline List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * get_list_0() const { return ___list_0; }
	inline List_1_tE2C2476479F1A844897C46751D09491B2534EC87 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52, ___current_3)); }
	inline FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  get_current_3() const { return ___current_3; }
	inline FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52_H


// System.Void UnityEngine.Experimental.Subsystem`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Subsystem_1__ctor_m481C47F32AD969E675437AF66262DAADD2322E51_gshared (Subsystem_1_tC290A9E5F38C778DC33C2DF9B1D10609BC0DC7CA * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mF51861616D3E151AAAA319FF158E26510A0D359C_gshared (List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mA0B1C89A6269A80CC746ED26EC65BD390FD75665_gshared (List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mD27DD9E2C6D2DD48607B83DF444B3CF65AE90A19_gshared (List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.XRFace>::Clear()
extern "C" IL2CPP_METHOD_ATTR void List_1_Clear_mE5C402CDF41FDFF018276341D49C6864C9950827_gshared (List_1_t9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
extern "C" IL2CPP_METHOD_ATTR void List_1_Clear_m8E7A9E8CF891528845C4B071CB8166DDB1A384B8_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear()
extern "C" IL2CPP_METHOD_ATTR void List_1_Clear_m471E9C96E037C142725FB47332197B948E5D38AE_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C" IL2CPP_METHOD_ATTR void List_1_Clear_m06BA343FB4E149EB045D8D2603E1AD239E1E4477_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_mB1E98B155D266CFCD2630F9CC1B9F09DB37AF636_gshared (List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * __this, FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m8933F527B5707C1B7AC57EA271F051C9D31DB390_gshared (List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * __this, FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_mE4B2ECBE4618987FBA1B0BAC4DB8399E9DCDF343_gshared (List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * __this, FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175  List_1_GetEnumerator_m844D7301792413D133453D6B32089B45527FA4D9_gshared (List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::get_Current()
extern "C" IL2CPP_METHOD_ATTR FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  Enumerator_get_Current_m17C74A966C414AF87B4926F71D5C2D738276CD1B_gshared (Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::Invoke(!0)
extern "C" IL2CPP_METHOD_ATTR void Action_1_Invoke_mB27FFCE02ADEA076FA5F2DB7FDDD2840F843702A_gshared (Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * __this, FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m454F2B7C556DBB8E508589BBBDE61EB6178CFCAE_gshared (Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m4FD30D793AA03BA4EF0D7D2CF85A1141EE9905C8_gshared (Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52  List_1_GetEnumerator_m1ED4F0E86DCB172B575B9FB56A3A3C4B4CC04BA8_gshared (List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::get_Current()
extern "C" IL2CPP_METHOD_ATTR FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  Enumerator_get_Current_mA2A1CDFBB945052FF9593C69C6DFBE039C26A456_gshared (Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::Invoke(!0)
extern "C" IL2CPP_METHOD_ATTR void Action_1_Invoke_mB4FE57B363F0B93B1DA4E0E3B8AA425AB7B5C9A2_gshared (Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * __this, FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m67B00ADB5FD5EADF1EAE0E911B37CA27B01D267B_gshared (Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_mFECED8B0D7B6FF3521473829AFD852810EED5A32_gshared (Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2  List_1_GetEnumerator_m4C931B506F93C24E8E7B6ADA0233745BC526A3DB_gshared (List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::get_Current()
extern "C" IL2CPP_METHOD_ATTR FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  Enumerator_get_Current_m9BEB9ACA83DE929A3EC5EFAF5010BCACDC05887F_gshared (Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::Invoke(!0)
extern "C" IL2CPP_METHOD_ATTR void Action_1_Invoke_mB5C96A623A8DD9F97E66150F57C9AFDE3EDB85C4_gshared (Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * __this, FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m7A3BAA9E235E50489080103011B7655AB5071614_gshared (Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m3155A03CAD8A96B0EF1E02E4FE8AF5DA522CDCC2_gshared (Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::Clear()
extern "C" IL2CPP_METHOD_ATTR void List_1_Clear_m5F612B61829DD1074F050617F5ECB93D7A81EB6B_gshared (List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::Clear()
extern "C" IL2CPP_METHOD_ATTR void List_1_Clear_mE389EDEE3E1D94DE8DB369C6D69741DE655FBA18_gshared (List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::Clear()
extern "C" IL2CPP_METHOD_ATTR void List_1_Clear_m07FCFBF2DE6606E960452AEE8427E1F66B861A64_gshared (List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Experimental.SubsystemDescriptor`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SubsystemDescriptor_1__ctor_mA1D36ACE2B90427C315C28AFD73B47C3E49CAA31_gshared (SubsystemDescriptor_1_t1C1E51FDC44D89022B4C81C15093AD21964A93AE * __this, const RuntimeMethod* method);

// UnityEngine.XR.FaceSubsystem.XRFace UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::get_xrFace()
extern "C" IL2CPP_METHOD_ATTR XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  FaceAddedEventArgs_get_xrFace_m09AA6413D489900F39876D70D55D88CBA1694AD5 (FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::set_xrFace(UnityEngine.XR.FaceSubsystem.XRFace)
extern "C" IL2CPP_METHOD_ATTR void FaceAddedEventArgs_set_xrFace_m06387391A12AFF9158D5F025B054D8393B517A53 (FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * __this, XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___value0, const RuntimeMethod* method);
// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::get_xrFaceSubsystem()
extern "C" IL2CPP_METHOD_ATTR XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * FaceAddedEventArgs_get_xrFaceSubsystem_m029BA347E281B46D02A402C948985BE772772B7C (FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::set_xrFaceSubsystem(UnityEngine.XR.FaceSubsystem.XRFaceSubsystem)
extern "C" IL2CPP_METHOD_ATTR void FaceAddedEventArgs_set_xrFaceSubsystem_mB59DB99C26BF29EBEDD53C2F13912B5269272F5B (FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * __this, XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.FaceSubsystem.XRFace::Equals(UnityEngine.XR.FaceSubsystem.XRFace)
extern "C" IL2CPP_METHOD_ATTR bool XRFace_Equals_mF790D0578A3004BB23703684FE36115A30FFA654 (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * __this, XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___other0, const RuntimeMethod* method);
// System.Boolean System.Object::Equals(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_Equals_mD98CD6D19C28ADC48B8468F78F94D38E203D0521 (RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::Equals(UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs)
extern "C" IL2CPP_METHOD_ATTR bool FaceAddedEventArgs_Equals_m5AE88FFD662267EAB5E8EAD76AAA68073AAC1B58 (FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * __this, FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  ___other0, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool FaceAddedEventArgs_Equals_m3D86CD998A55813D718D2482307DBF45B4DEF909 (FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Int32 UnityEngine.XR.FaceSubsystem.XRFace::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t XRFace_GetHashCode_mA73B05DA6D09D503A426C95A687D07CA823CBCC7 (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t FaceAddedEventArgs_GetHashCode_mA5D4C6B44DDB06F050D1E87098E17ADB51CE25D6 (FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * __this, const RuntimeMethod* method);
// UnityEngine.XR.FaceSubsystem.XRFace UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::get_xrFace()
extern "C" IL2CPP_METHOD_ATTR XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  FaceRemovedEventArgs_get_xrFace_m4595F580303FDEA15A6F60DA9E97E0BE865010F2 (FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::set_xrFace(UnityEngine.XR.FaceSubsystem.XRFace)
extern "C" IL2CPP_METHOD_ATTR void FaceRemovedEventArgs_set_xrFace_mD8D3D1A092F87ED3A84579CEA34CCB869C23D8CE (FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * __this, XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___value0, const RuntimeMethod* method);
// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::get_xrFaceSubsystem()
extern "C" IL2CPP_METHOD_ATTR XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * FaceRemovedEventArgs_get_xrFaceSubsystem_mA5CD378FB6469B0CEAD45AA13F662357A66C0992 (FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::set_xrFaceSubsystem(UnityEngine.XR.FaceSubsystem.XRFaceSubsystem)
extern "C" IL2CPP_METHOD_ATTR void FaceRemovedEventArgs_set_xrFaceSubsystem_m0EE856E16539263DC748734E2C0A8E868C5BAEDC (FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * __this, XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::Equals(UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs)
extern "C" IL2CPP_METHOD_ATTR bool FaceRemovedEventArgs_Equals_m3B57315AEE8A5F24145ACED480571F8E70EE9135 (FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * __this, FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  ___other0, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool FaceRemovedEventArgs_Equals_m66078B92345895ABCB42AA01078CAB075E218075 (FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Int32 UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t FaceRemovedEventArgs_GetHashCode_mA53A888A8BF0A9996893C8BE7C454D46374CF43B (FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * __this, const RuntimeMethod* method);
// UnityEngine.XR.FaceSubsystem.XRFace UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::get_xrFace()
extern "C" IL2CPP_METHOD_ATTR XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  FaceUpdatedEventArgs_get_xrFace_m30FD6A70CC39F75122484CC0CC994775EA38F335 (FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::set_xrFace(UnityEngine.XR.FaceSubsystem.XRFace)
extern "C" IL2CPP_METHOD_ATTR void FaceUpdatedEventArgs_set_xrFace_m80640B5719F9F606EE6294E8147778FE4548E619 (FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * __this, XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___value0, const RuntimeMethod* method);
// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::get_xrFaceSubsystem()
extern "C" IL2CPP_METHOD_ATTR XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * FaceUpdatedEventArgs_get_xrFaceSubsystem_m398584DF2801F81F073C14859B52651FF5A0641B (FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::set_xrFaceSubsystem(UnityEngine.XR.FaceSubsystem.XRFaceSubsystem)
extern "C" IL2CPP_METHOD_ATTR void FaceUpdatedEventArgs_set_xrFaceSubsystem_m9B360E62675AC6D1A16511FDA94C05FBF59CE710 (FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * __this, XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::Equals(UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs)
extern "C" IL2CPP_METHOD_ATTR bool FaceUpdatedEventArgs_Equals_mE1BC234F7CD99FCB3ADBD3BAE6ABD0167A7AAC64 (FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * __this, FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  ___other0, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool FaceUpdatedEventArgs_Equals_m07A86F9854A5DA3DF39FFA2A8B89128F7475D80D (FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Int32 UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t FaceUpdatedEventArgs_GetHashCode_mB0178C36B30D0FE0544A550E21291D3F8E990768 (FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * __this, const RuntimeMethod* method);
// UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemCapabilities UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::get_capabilities()
extern "C" IL2CPP_METHOD_ATTR int32_t FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383 (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::get_supportsFacePose()
extern "C" IL2CPP_METHOD_ATTR bool FaceSubsystemParams_get_supportsFacePose_mBA2C2F12B13C70EC4A28EAEA117B2F37E544D484 (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::set_capabilities(UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemCapabilities)
extern "C" IL2CPP_METHOD_ATTR void FaceSubsystemParams_set_capabilities_m97A763938B473E82F5D6B0CF04835F19F76E7852 (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::set_supportsFacePose(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FaceSubsystemParams_set_supportsFacePose_m9EFB37DEDF1C8F11C80FD3D08DF001003D3DE4A0 (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::get_supportsFaceMeshVerticesAndIndices()
extern "C" IL2CPP_METHOD_ATTR bool FaceSubsystemParams_get_supportsFaceMeshVerticesAndIndices_m72F0281E748C0F601FA1069EE360125C7756452B (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::set_supportsFaceMeshVerticesAndIndices(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FaceSubsystemParams_set_supportsFaceMeshVerticesAndIndices_m6B5F82152F894432767871C6263B60AF174878F5 (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::get_supportsFaceMeshUVs()
extern "C" IL2CPP_METHOD_ATTR bool FaceSubsystemParams_get_supportsFaceMeshUVs_m1B8096A3E173E864538204DD495F5B58278847DE (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::set_supportsFaceMeshUVs(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FaceSubsystemParams_set_supportsFaceMeshUVs_m0AB5CCED2C9C601389A1C68DADCDF0DCAD4A02BA (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean System.String::Equals(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_Equals_m9C4D78DFA0979504FE31429B64A4C26DF48020D1 (String_t* __this, String_t* p0, const RuntimeMethod* method);
// System.Boolean System.Type::op_Equality(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR bool Type_op_Equality_m7040622C9E1037EFC73E1F0EDB1DD241282BE3D8 (Type_t * p0, Type_t * p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::Equals(UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams)
extern "C" IL2CPP_METHOD_ATTR bool FaceSubsystemParams_Equals_mF5B83F9C750E61CED7610753A84927E59AC9B4DF (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040  ___other0, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool FaceSubsystemParams_Equals_m78CA1466752559D70811FFF4873496CFB7B4FA59 (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Int32 System.Int32::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t Int32_GetHashCode_m245C424ECE351E5FE3277A88EEB02132DAB8C25A (int32_t* __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t FaceSubsystemParams_GetHashCode_mF1A4912B21F5E3855DFB48B6367803E3936F7B69 (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, const RuntimeMethod* method);
// UnityEngine.Experimental.XR.TrackableId UnityEngine.XR.FaceSubsystem.XRFace::get_trackableId()
extern "C" IL2CPP_METHOD_ATTR TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  XRFace_get_trackableId_mA9C77882D3F53863FAAFD46B8B0061BA26035314 (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * __this, const RuntimeMethod* method);
// UnityEngine.Pose UnityEngine.XR.FaceSubsystem.XRFace::get_pose()
extern "C" IL2CPP_METHOD_ATTR Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  XRFace_get_pose_mB994DFF96F2BEBF1D9696A60BEE72F94F51C859B (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.FaceSubsystem.XRFace::get_wasUpdated()
extern "C" IL2CPP_METHOD_ATTR bool XRFace_get_wasUpdated_mD57787D097DCDA6031D9859E87842B0E484EBF1C (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.FaceSubsystem.XRFace::get_isTracked()
extern "C" IL2CPP_METHOD_ATTR bool XRFace_get_isTracked_m8C63C8FEDC4EF3D6093BCB33FEEF3B27E6425ACC (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.FaceSubsystem.XRFace::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool XRFace_Equals_m97884868087251BBF598B60530D90989ED5494AB (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Experimental.XR.TrackableId::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t TrackableId_GetHashCode_mCADD3CC9841B05FEE526C31572F130C063C0D39B (TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Pose::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t Pose_GetHashCode_m17AC0D28F5BD43DE0CCFA4CC1A870C525E0D6066 (Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * __this, const RuntimeMethod* method);
// System.Int32 System.Boolean::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t Boolean_GetHashCode_m92C426D44100ED098FEECC96A743C3CB92DFF737 (bool* __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Experimental.XR.TrackableId::Equals(UnityEngine.Experimental.XR.TrackableId)
extern "C" IL2CPP_METHOD_ATTR bool TrackableId_Equals_mCF5272276A4DC394A0590A0A417B2578E4C76E4D (TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B * __this, TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Pose::Equals(UnityEngine.Pose)
extern "C" IL2CPP_METHOD_ATTR bool Pose_Equals_m867264C8DF91FF8DC3AD957EF1625902CDEBAEDD (Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * __this, Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  p0, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C" IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1 (Delegate_t * p0, Delegate_t * p1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C" IL2CPP_METHOD_ATTR Delegate_t * Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D (Delegate_t * p0, Delegate_t * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Experimental.Subsystem`1<UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor>::.ctor()
inline void Subsystem_1__ctor_m1CF0FE2230E65CDBD346CF4F52035DD2BC9D2822 (Subsystem_1_t7A31187B2E3DFF03501265C781C9A8547A073F0C * __this, const RuntimeMethod* method)
{
	((  void (*) (Subsystem_1_t7A31187B2E3DFF03501265C781C9A8547A073F0C *, const RuntimeMethod*))Subsystem_1__ctor_m481C47F32AD969E675437AF66262DAADD2322E51_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::.ctor()
inline void List_1__ctor_mF51861616D3E151AAAA319FF158E26510A0D359C (List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A *, const RuntimeMethod*))List_1__ctor_mF51861616D3E151AAAA319FF158E26510A0D359C_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::.ctor()
inline void List_1__ctor_mA0B1C89A6269A80CC746ED26EC65BD390FD75665 (List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE2C2476479F1A844897C46751D09491B2534EC87 *, const RuntimeMethod*))List_1__ctor_mA0B1C89A6269A80CC746ED26EC65BD390FD75665_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::.ctor()
inline void List_1__ctor_mD27DD9E2C6D2DD48607B83DF444B3CF65AE90A19 (List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 *, const RuntimeMethod*))List_1__ctor_mD27DD9E2C6D2DD48607B83DF444B3CF65AE90A19_gshared)(__this, method);
}
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * __this, String_t* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.XRFace>::Clear()
inline void List_1_Clear_mE5C402CDF41FDFF018276341D49C6864C9950827 (List_1_t9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30 *, const RuntimeMethod*))List_1_Clear_mE5C402CDF41FDFF018276341D49C6864C9950827_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
inline void List_1_Clear_m8E7A9E8CF891528845C4B071CB8166DDB1A384B8 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, const RuntimeMethod*))List_1_Clear_m8E7A9E8CF891528845C4B071CB8166DDB1A384B8_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear()
inline void List_1_Clear_m471E9C96E037C142725FB47332197B948E5D38AE (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, const RuntimeMethod*))List_1_Clear_m471E9C96E037C142725FB47332197B948E5D38AE_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
inline void List_1_Clear_m06BA343FB4E149EB045D8D2603E1AD239E1E4477 (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, const RuntimeMethod*))List_1_Clear_m06BA343FB4E149EB045D8D2603E1AD239E1E4477_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::Add(!0)
inline void List_1_Add_mB1E98B155D266CFCD2630F9CC1B9F09DB37AF636 (List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * __this, FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A *, FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C , const RuntimeMethod*))List_1_Add_mB1E98B155D266CFCD2630F9CC1B9F09DB37AF636_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::Add(!0)
inline void List_1_Add_m8933F527B5707C1B7AC57EA271F051C9D31DB390 (List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * __this, FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE2C2476479F1A844897C46751D09491B2534EC87 *, FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 , const RuntimeMethod*))List_1_Add_m8933F527B5707C1B7AC57EA271F051C9D31DB390_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::Add(!0)
inline void List_1_Add_mE4B2ECBE4618987FBA1B0BAC4DB8399E9DCDF343 (List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * __this, FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 *, FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 , const RuntimeMethod*))List_1_Add_mE4B2ECBE4618987FBA1B0BAC4DB8399E9DCDF343_gshared)(__this, p0, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::GetEnumerator()
inline Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175  List_1_GetEnumerator_m844D7301792413D133453D6B32089B45527FA4D9 (List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175  (*) (List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A *, const RuntimeMethod*))List_1_GetEnumerator_m844D7301792413D133453D6B32089B45527FA4D9_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::get_Current()
inline FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  Enumerator_get_Current_m17C74A966C414AF87B4926F71D5C2D738276CD1B (Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175 * __this, const RuntimeMethod* method)
{
	return ((  FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  (*) (Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175 *, const RuntimeMethod*))Enumerator_get_Current_m17C74A966C414AF87B4926F71D5C2D738276CD1B_gshared)(__this, method);
}
// System.Void System.Action`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::Invoke(!0)
inline void Action_1_Invoke_mB27FFCE02ADEA076FA5F2DB7FDDD2840F843702A (Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * __this, FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  p0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B *, FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C , const RuntimeMethod*))Action_1_Invoke_mB27FFCE02ADEA076FA5F2DB7FDDD2840F843702A_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::MoveNext()
inline bool Enumerator_MoveNext_m454F2B7C556DBB8E508589BBBDE61EB6178CFCAE (Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175 *, const RuntimeMethod*))Enumerator_MoveNext_m454F2B7C556DBB8E508589BBBDE61EB6178CFCAE_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::Dispose()
inline void Enumerator_Dispose_m4FD30D793AA03BA4EF0D7D2CF85A1141EE9905C8 (Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175 *, const RuntimeMethod*))Enumerator_Dispose_m4FD30D793AA03BA4EF0D7D2CF85A1141EE9905C8_gshared)(__this, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::GetEnumerator()
inline Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52  List_1_GetEnumerator_m1ED4F0E86DCB172B575B9FB56A3A3C4B4CC04BA8 (List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52  (*) (List_1_tE2C2476479F1A844897C46751D09491B2534EC87 *, const RuntimeMethod*))List_1_GetEnumerator_m1ED4F0E86DCB172B575B9FB56A3A3C4B4CC04BA8_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::get_Current()
inline FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  Enumerator_get_Current_mA2A1CDFBB945052FF9593C69C6DFBE039C26A456 (Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52 * __this, const RuntimeMethod* method)
{
	return ((  FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  (*) (Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52 *, const RuntimeMethod*))Enumerator_get_Current_mA2A1CDFBB945052FF9593C69C6DFBE039C26A456_gshared)(__this, method);
}
// System.Void System.Action`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::Invoke(!0)
inline void Action_1_Invoke_mB4FE57B363F0B93B1DA4E0E3B8AA425AB7B5C9A2 (Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * __this, FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  p0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 *, FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 , const RuntimeMethod*))Action_1_Invoke_mB4FE57B363F0B93B1DA4E0E3B8AA425AB7B5C9A2_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::MoveNext()
inline bool Enumerator_MoveNext_m67B00ADB5FD5EADF1EAE0E911B37CA27B01D267B (Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52 *, const RuntimeMethod*))Enumerator_MoveNext_m67B00ADB5FD5EADF1EAE0E911B37CA27B01D267B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::Dispose()
inline void Enumerator_Dispose_mFECED8B0D7B6FF3521473829AFD852810EED5A32 (Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52 *, const RuntimeMethod*))Enumerator_Dispose_mFECED8B0D7B6FF3521473829AFD852810EED5A32_gshared)(__this, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::GetEnumerator()
inline Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2  List_1_GetEnumerator_m4C931B506F93C24E8E7B6ADA0233745BC526A3DB (List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2  (*) (List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 *, const RuntimeMethod*))List_1_GetEnumerator_m4C931B506F93C24E8E7B6ADA0233745BC526A3DB_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::get_Current()
inline FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  Enumerator_get_Current_m9BEB9ACA83DE929A3EC5EFAF5010BCACDC05887F (Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2 * __this, const RuntimeMethod* method)
{
	return ((  FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  (*) (Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2 *, const RuntimeMethod*))Enumerator_get_Current_m9BEB9ACA83DE929A3EC5EFAF5010BCACDC05887F_gshared)(__this, method);
}
// System.Void System.Action`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::Invoke(!0)
inline void Action_1_Invoke_mB5C96A623A8DD9F97E66150F57C9AFDE3EDB85C4 (Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * __this, FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  p0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 *, FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 , const RuntimeMethod*))Action_1_Invoke_mB5C96A623A8DD9F97E66150F57C9AFDE3EDB85C4_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::MoveNext()
inline bool Enumerator_MoveNext_m7A3BAA9E235E50489080103011B7655AB5071614 (Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2 *, const RuntimeMethod*))Enumerator_MoveNext_m7A3BAA9E235E50489080103011B7655AB5071614_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::Dispose()
inline void Enumerator_Dispose_m3155A03CAD8A96B0EF1E02E4FE8AF5DA522CDCC2 (Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2 *, const RuntimeMethod*))Enumerator_Dispose_m3155A03CAD8A96B0EF1E02E4FE8AF5DA522CDCC2_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>::Clear()
inline void List_1_Clear_m5F612B61829DD1074F050617F5ECB93D7A81EB6B (List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A *, const RuntimeMethod*))List_1_Clear_m5F612B61829DD1074F050617F5ECB93D7A81EB6B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>::Clear()
inline void List_1_Clear_mE389EDEE3E1D94DE8DB369C6D69741DE655FBA18 (List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE2C2476479F1A844897C46751D09491B2534EC87 *, const RuntimeMethod*))List_1_Clear_mE389EDEE3E1D94DE8DB369C6D69741DE655FBA18_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>::Clear()
inline void List_1_Clear_m07FCFBF2DE6606E960452AEE8427E1F66B861A64 (List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 *, const RuntimeMethod*))List_1_Clear_m07FCFBF2DE6606E960452AEE8427E1F66B861A64_gshared)(__this, method);
}
// System.Void UnityEngine.Experimental.SubsystemDescriptor`1<UnityEngine.XR.FaceSubsystem.XRFaceSubsystem>::.ctor()
inline void SubsystemDescriptor_1__ctor_m0B1EAA83AAE682789A90078505E8DEE4048F8C1E (SubsystemDescriptor_1_t8BC7D01A6BF03D22D133B8FB58E5B2D85675FDD7 * __this, const RuntimeMethod* method)
{
	((  void (*) (SubsystemDescriptor_1_t8BC7D01A6BF03D22D133B8FB58E5B2D85675FDD7 *, const RuntimeMethod*))SubsystemDescriptor_1__ctor_mA1D36ACE2B90427C315C28AFD73B47C3E49CAA31_gshared)(__this, method);
}
// System.Void UnityEngine.Experimental.SubsystemDescriptor::set_id(System.String)
extern "C" IL2CPP_METHOD_ATTR void SubsystemDescriptor_set_id_mB3D1A7164F096650AE5249037EEA91AB92E21AB4 (SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Experimental.SubsystemDescriptor::set_subsystemImplementationType(System.Type)
extern "C" IL2CPP_METHOD_ATTR void SubsystemDescriptor_set_subsystemImplementationType_m17E1DD79B83EC74F0171B4D0B3F0478FBC91F202 (SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4 * __this, Type_t * p0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor::.ctor(UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams)
extern "C" IL2CPP_METHOD_ATTR void XRFaceSubsystemDescriptor__ctor_m64CAD1F471D073C278B7BB46ECB6A8A86CC35BE0 (XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19 * __this, FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040  ___descriptorParams0, const RuntimeMethod* method);
// System.Boolean UnityEngine.SubsystemRegistration::CreateDescriptor(UnityEngine.Experimental.SubsystemDescriptor)
extern "C" IL2CPP_METHOD_ATTR bool SubsystemRegistration_CreateDescriptor_m6BADF7D1D4E34D466EDCDDC0E533A808EABB4BE0 (SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4 * p0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs
extern "C" void FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshal_pinvoke(const FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C& unmarshaled, FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshaled_pinvoke& marshaled)
{
	Exception_t* ___U3CxrFaceSubsystemU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<xrFaceSubsystem>k__BackingField' of type 'FaceAddedEventArgs': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CxrFaceSubsystemU3Ek__BackingField_1Exception, NULL, NULL);
}
extern "C" void FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshal_pinvoke_back(const FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshaled_pinvoke& marshaled, FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C& unmarshaled)
{
	Exception_t* ___U3CxrFaceSubsystemU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<xrFaceSubsystem>k__BackingField' of type 'FaceAddedEventArgs': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CxrFaceSubsystemU3Ek__BackingField_1Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs
extern "C" void FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshal_pinvoke_cleanup(FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs
extern "C" void FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshal_com(const FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C& unmarshaled, FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshaled_com& marshaled)
{
	Exception_t* ___U3CxrFaceSubsystemU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<xrFaceSubsystem>k__BackingField' of type 'FaceAddedEventArgs': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CxrFaceSubsystemU3Ek__BackingField_1Exception, NULL, NULL);
}
extern "C" void FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshal_com_back(const FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshaled_com& marshaled, FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C& unmarshaled)
{
	Exception_t* ___U3CxrFaceSubsystemU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<xrFaceSubsystem>k__BackingField' of type 'FaceAddedEventArgs': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CxrFaceSubsystemU3Ek__BackingField_1Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs
extern "C" void FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshal_com_cleanup(FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_marshaled_com& marshaled)
{
}
// UnityEngine.XR.FaceSubsystem.XRFace UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::get_xrFace()
extern "C" IL2CPP_METHOD_ATTR XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  FaceAddedEventArgs_get_xrFace_m09AA6413D489900F39876D70D55D88CBA1694AD5 (FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * __this, const RuntimeMethod* method)
{
	{
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_0 = __this->get_U3CxrFaceU3Ek__BackingField_0();
		return L_0;
	}
}
extern "C"  XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  FaceAddedEventArgs_get_xrFace_m09AA6413D489900F39876D70D55D88CBA1694AD5_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * _thisAdjusted = reinterpret_cast<FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *>(__this + 1);
	return FaceAddedEventArgs_get_xrFace_m09AA6413D489900F39876D70D55D88CBA1694AD5(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::set_xrFace(UnityEngine.XR.FaceSubsystem.XRFace)
extern "C" IL2CPP_METHOD_ATTR void FaceAddedEventArgs_set_xrFace_m06387391A12AFF9158D5F025B054D8393B517A53 (FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * __this, XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___value0, const RuntimeMethod* method)
{
	{
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_0 = ___value0;
		__this->set_U3CxrFaceU3Ek__BackingField_0(L_0);
		return;
	}
}
extern "C"  void FaceAddedEventArgs_set_xrFace_m06387391A12AFF9158D5F025B054D8393B517A53_AdjustorThunk (RuntimeObject * __this, XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___value0, const RuntimeMethod* method)
{
	FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * _thisAdjusted = reinterpret_cast<FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *>(__this + 1);
	FaceAddedEventArgs_set_xrFace_m06387391A12AFF9158D5F025B054D8393B517A53(_thisAdjusted, ___value0, method);
}
// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::get_xrFaceSubsystem()
extern "C" IL2CPP_METHOD_ATTR XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * FaceAddedEventArgs_get_xrFaceSubsystem_m029BA347E281B46D02A402C948985BE772772B7C (FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * __this, const RuntimeMethod* method)
{
	{
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_0 = __this->get_U3CxrFaceSubsystemU3Ek__BackingField_1();
		return L_0;
	}
}
extern "C"  XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * FaceAddedEventArgs_get_xrFaceSubsystem_m029BA347E281B46D02A402C948985BE772772B7C_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * _thisAdjusted = reinterpret_cast<FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *>(__this + 1);
	return FaceAddedEventArgs_get_xrFaceSubsystem_m029BA347E281B46D02A402C948985BE772772B7C(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::set_xrFaceSubsystem(UnityEngine.XR.FaceSubsystem.XRFaceSubsystem)
extern "C" IL2CPP_METHOD_ATTR void FaceAddedEventArgs_set_xrFaceSubsystem_mB59DB99C26BF29EBEDD53C2F13912B5269272F5B (FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * __this, XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___value0, const RuntimeMethod* method)
{
	{
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_0 = ___value0;
		__this->set_U3CxrFaceSubsystemU3Ek__BackingField_1(L_0);
		return;
	}
}
extern "C"  void FaceAddedEventArgs_set_xrFaceSubsystem_mB59DB99C26BF29EBEDD53C2F13912B5269272F5B_AdjustorThunk (RuntimeObject * __this, XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___value0, const RuntimeMethod* method)
{
	FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * _thisAdjusted = reinterpret_cast<FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *>(__this + 1);
	FaceAddedEventArgs_set_xrFaceSubsystem_mB59DB99C26BF29EBEDD53C2F13912B5269272F5B(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::Equals(UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs)
extern "C" IL2CPP_METHOD_ATTR bool FaceAddedEventArgs_Equals_m5AE88FFD662267EAB5E8EAD76AAA68073AAC1B58 (FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * __this, FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  ___other0, const RuntimeMethod* method)
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_0 = FaceAddedEventArgs_get_xrFace_m09AA6413D489900F39876D70D55D88CBA1694AD5((FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *)__this, /*hidden argument*/NULL);
		V_0 = L_0;
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_1 = FaceAddedEventArgs_get_xrFace_m09AA6413D489900F39876D70D55D88CBA1694AD5((FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *)(&___other0), /*hidden argument*/NULL);
		bool L_2 = XRFace_Equals_mF790D0578A3004BB23703684FE36115A30FFA654((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)(&V_0), L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_3 = FaceAddedEventArgs_get_xrFaceSubsystem_m029BA347E281B46D02A402C948985BE772772B7C((FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *)__this, /*hidden argument*/NULL);
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_4 = FaceAddedEventArgs_get_xrFaceSubsystem_m029BA347E281B46D02A402C948985BE772772B7C((FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *)(&___other0), /*hidden argument*/NULL);
		bool L_5 = Object_Equals_mD98CD6D19C28ADC48B8468F78F94D38E203D0521(L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_002a:
	{
		return (bool)0;
	}
}
extern "C"  bool FaceAddedEventArgs_Equals_m5AE88FFD662267EAB5E8EAD76AAA68073AAC1B58_AdjustorThunk (RuntimeObject * __this, FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  ___other0, const RuntimeMethod* method)
{
	FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * _thisAdjusted = reinterpret_cast<FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *>(__this + 1);
	return FaceAddedEventArgs_Equals_m5AE88FFD662267EAB5E8EAD76AAA68073AAC1B58(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool FaceAddedEventArgs_Equals_m3D86CD998A55813D718D2482307DBF45B4DEF909 (FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FaceAddedEventArgs_Equals_m3D86CD998A55813D718D2482307DBF45B4DEF909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}

IL_0005:
	{
		RuntimeObject * L_1 = ___obj0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_1, FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_il2cpp_TypeInfo_var)))
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject * L_2 = ___obj0;
		bool L_3 = FaceAddedEventArgs_Equals_m5AE88FFD662267EAB5E8EAD76AAA68073AAC1B58((FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *)__this, ((*(FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *)((FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *)UnBox(L_2, FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_3;
	}

IL_001a:
	{
		return (bool)0;
	}
}
extern "C"  bool FaceAddedEventArgs_Equals_m3D86CD998A55813D718D2482307DBF45B4DEF909_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * _thisAdjusted = reinterpret_cast<FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *>(__this + 1);
	return FaceAddedEventArgs_Equals_m3D86CD998A55813D718D2482307DBF45B4DEF909(_thisAdjusted, ___obj0, method);
}
// System.Int32 UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t FaceAddedEventArgs_GetHashCode_mA5D4C6B44DDB06F050D1E87098E17ADB51CE25D6 (FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * __this, const RuntimeMethod* method)
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	{
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_0 = FaceAddedEventArgs_get_xrFace_m09AA6413D489900F39876D70D55D88CBA1694AD5((FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *)__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = XRFace_GetHashCode_mA73B05DA6D09D503A426C95A687D07CA823CBCC7((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)(&V_0), /*hidden argument*/NULL);
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_2 = FaceAddedEventArgs_get_xrFaceSubsystem_m029BA347E281B46D02A402C948985BE772772B7C((FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *)__this, /*hidden argument*/NULL);
		G_B1_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)((int32_t)486187739)));
		if (L_2)
		{
			G_B2_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)((int32_t)486187739)));
			goto IL_0025;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_0030;
	}

IL_0025:
	{
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_3 = FaceAddedEventArgs_get_xrFaceSubsystem_m029BA347E281B46D02A402C948985BE772772B7C((FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *)__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_3);
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_0030:
	{
		return ((int32_t)il2cpp_codegen_add((int32_t)G_B3_1, (int32_t)G_B3_0));
	}
}
extern "C"  int32_t FaceAddedEventArgs_GetHashCode_mA5D4C6B44DDB06F050D1E87098E17ADB51CE25D6_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C * _thisAdjusted = reinterpret_cast<FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *>(__this + 1);
	return FaceAddedEventArgs_GetHashCode_mA5D4C6B44DDB06F050D1E87098E17ADB51CE25D6(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs
extern "C" void FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshal_pinvoke(const FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651& unmarshaled, FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshaled_pinvoke& marshaled)
{
	Exception_t* ___U3CxrFaceSubsystemU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<xrFaceSubsystem>k__BackingField' of type 'FaceRemovedEventArgs': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CxrFaceSubsystemU3Ek__BackingField_1Exception, NULL, NULL);
}
extern "C" void FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshal_pinvoke_back(const FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshaled_pinvoke& marshaled, FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651& unmarshaled)
{
	Exception_t* ___U3CxrFaceSubsystemU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<xrFaceSubsystem>k__BackingField' of type 'FaceRemovedEventArgs': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CxrFaceSubsystemU3Ek__BackingField_1Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs
extern "C" void FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshal_pinvoke_cleanup(FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs
extern "C" void FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshal_com(const FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651& unmarshaled, FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshaled_com& marshaled)
{
	Exception_t* ___U3CxrFaceSubsystemU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<xrFaceSubsystem>k__BackingField' of type 'FaceRemovedEventArgs': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CxrFaceSubsystemU3Ek__BackingField_1Exception, NULL, NULL);
}
extern "C" void FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshal_com_back(const FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshaled_com& marshaled, FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651& unmarshaled)
{
	Exception_t* ___U3CxrFaceSubsystemU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<xrFaceSubsystem>k__BackingField' of type 'FaceRemovedEventArgs': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CxrFaceSubsystemU3Ek__BackingField_1Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs
extern "C" void FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshal_com_cleanup(FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_marshaled_com& marshaled)
{
}
// UnityEngine.XR.FaceSubsystem.XRFace UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::get_xrFace()
extern "C" IL2CPP_METHOD_ATTR XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  FaceRemovedEventArgs_get_xrFace_m4595F580303FDEA15A6F60DA9E97E0BE865010F2 (FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * __this, const RuntimeMethod* method)
{
	{
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_0 = __this->get_U3CxrFaceU3Ek__BackingField_0();
		return L_0;
	}
}
extern "C"  XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  FaceRemovedEventArgs_get_xrFace_m4595F580303FDEA15A6F60DA9E97E0BE865010F2_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * _thisAdjusted = reinterpret_cast<FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *>(__this + 1);
	return FaceRemovedEventArgs_get_xrFace_m4595F580303FDEA15A6F60DA9E97E0BE865010F2(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::set_xrFace(UnityEngine.XR.FaceSubsystem.XRFace)
extern "C" IL2CPP_METHOD_ATTR void FaceRemovedEventArgs_set_xrFace_mD8D3D1A092F87ED3A84579CEA34CCB869C23D8CE (FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * __this, XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___value0, const RuntimeMethod* method)
{
	{
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_0 = ___value0;
		__this->set_U3CxrFaceU3Ek__BackingField_0(L_0);
		return;
	}
}
extern "C"  void FaceRemovedEventArgs_set_xrFace_mD8D3D1A092F87ED3A84579CEA34CCB869C23D8CE_AdjustorThunk (RuntimeObject * __this, XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___value0, const RuntimeMethod* method)
{
	FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * _thisAdjusted = reinterpret_cast<FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *>(__this + 1);
	FaceRemovedEventArgs_set_xrFace_mD8D3D1A092F87ED3A84579CEA34CCB869C23D8CE(_thisAdjusted, ___value0, method);
}
// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::get_xrFaceSubsystem()
extern "C" IL2CPP_METHOD_ATTR XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * FaceRemovedEventArgs_get_xrFaceSubsystem_mA5CD378FB6469B0CEAD45AA13F662357A66C0992 (FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * __this, const RuntimeMethod* method)
{
	{
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_0 = __this->get_U3CxrFaceSubsystemU3Ek__BackingField_1();
		return L_0;
	}
}
extern "C"  XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * FaceRemovedEventArgs_get_xrFaceSubsystem_mA5CD378FB6469B0CEAD45AA13F662357A66C0992_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * _thisAdjusted = reinterpret_cast<FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *>(__this + 1);
	return FaceRemovedEventArgs_get_xrFaceSubsystem_mA5CD378FB6469B0CEAD45AA13F662357A66C0992(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::set_xrFaceSubsystem(UnityEngine.XR.FaceSubsystem.XRFaceSubsystem)
extern "C" IL2CPP_METHOD_ATTR void FaceRemovedEventArgs_set_xrFaceSubsystem_m0EE856E16539263DC748734E2C0A8E868C5BAEDC (FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * __this, XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___value0, const RuntimeMethod* method)
{
	{
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_0 = ___value0;
		__this->set_U3CxrFaceSubsystemU3Ek__BackingField_1(L_0);
		return;
	}
}
extern "C"  void FaceRemovedEventArgs_set_xrFaceSubsystem_m0EE856E16539263DC748734E2C0A8E868C5BAEDC_AdjustorThunk (RuntimeObject * __this, XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___value0, const RuntimeMethod* method)
{
	FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * _thisAdjusted = reinterpret_cast<FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *>(__this + 1);
	FaceRemovedEventArgs_set_xrFaceSubsystem_m0EE856E16539263DC748734E2C0A8E868C5BAEDC(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::Equals(UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs)
extern "C" IL2CPP_METHOD_ATTR bool FaceRemovedEventArgs_Equals_m3B57315AEE8A5F24145ACED480571F8E70EE9135 (FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * __this, FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  ___other0, const RuntimeMethod* method)
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_0 = FaceRemovedEventArgs_get_xrFace_m4595F580303FDEA15A6F60DA9E97E0BE865010F2((FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *)__this, /*hidden argument*/NULL);
		V_0 = L_0;
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_1 = FaceRemovedEventArgs_get_xrFace_m4595F580303FDEA15A6F60DA9E97E0BE865010F2((FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *)(&___other0), /*hidden argument*/NULL);
		bool L_2 = XRFace_Equals_mF790D0578A3004BB23703684FE36115A30FFA654((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)(&V_0), L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_3 = FaceRemovedEventArgs_get_xrFaceSubsystem_mA5CD378FB6469B0CEAD45AA13F662357A66C0992((FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *)__this, /*hidden argument*/NULL);
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_4 = FaceRemovedEventArgs_get_xrFaceSubsystem_mA5CD378FB6469B0CEAD45AA13F662357A66C0992((FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *)(&___other0), /*hidden argument*/NULL);
		bool L_5 = Object_Equals_mD98CD6D19C28ADC48B8468F78F94D38E203D0521(L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_002a:
	{
		return (bool)0;
	}
}
extern "C"  bool FaceRemovedEventArgs_Equals_m3B57315AEE8A5F24145ACED480571F8E70EE9135_AdjustorThunk (RuntimeObject * __this, FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  ___other0, const RuntimeMethod* method)
{
	FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * _thisAdjusted = reinterpret_cast<FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *>(__this + 1);
	return FaceRemovedEventArgs_Equals_m3B57315AEE8A5F24145ACED480571F8E70EE9135(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool FaceRemovedEventArgs_Equals_m66078B92345895ABCB42AA01078CAB075E218075 (FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FaceRemovedEventArgs_Equals_m66078B92345895ABCB42AA01078CAB075E218075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}

IL_0005:
	{
		RuntimeObject * L_1 = ___obj0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_1, FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_il2cpp_TypeInfo_var)))
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject * L_2 = ___obj0;
		bool L_3 = FaceRemovedEventArgs_Equals_m3B57315AEE8A5F24145ACED480571F8E70EE9135((FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *)__this, ((*(FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *)((FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *)UnBox(L_2, FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_3;
	}

IL_001a:
	{
		return (bool)0;
	}
}
extern "C"  bool FaceRemovedEventArgs_Equals_m66078B92345895ABCB42AA01078CAB075E218075_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * _thisAdjusted = reinterpret_cast<FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *>(__this + 1);
	return FaceRemovedEventArgs_Equals_m66078B92345895ABCB42AA01078CAB075E218075(_thisAdjusted, ___obj0, method);
}
// System.Int32 UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t FaceRemovedEventArgs_GetHashCode_mA53A888A8BF0A9996893C8BE7C454D46374CF43B (FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * __this, const RuntimeMethod* method)
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	{
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_0 = FaceRemovedEventArgs_get_xrFace_m4595F580303FDEA15A6F60DA9E97E0BE865010F2((FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *)__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = XRFace_GetHashCode_mA73B05DA6D09D503A426C95A687D07CA823CBCC7((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)(&V_0), /*hidden argument*/NULL);
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_2 = FaceRemovedEventArgs_get_xrFaceSubsystem_mA5CD378FB6469B0CEAD45AA13F662357A66C0992((FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *)__this, /*hidden argument*/NULL);
		G_B1_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)((int32_t)486187739)));
		if (L_2)
		{
			G_B2_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)((int32_t)486187739)));
			goto IL_0025;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_0030;
	}

IL_0025:
	{
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_3 = FaceRemovedEventArgs_get_xrFaceSubsystem_mA5CD378FB6469B0CEAD45AA13F662357A66C0992((FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *)__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_3);
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_0030:
	{
		return ((int32_t)il2cpp_codegen_add((int32_t)G_B3_1, (int32_t)G_B3_0));
	}
}
extern "C"  int32_t FaceRemovedEventArgs_GetHashCode_mA53A888A8BF0A9996893C8BE7C454D46374CF43B_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 * _thisAdjusted = reinterpret_cast<FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *>(__this + 1);
	return FaceRemovedEventArgs_GetHashCode_mA53A888A8BF0A9996893C8BE7C454D46374CF43B(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs
extern "C" void FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshal_pinvoke(const FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881& unmarshaled, FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshaled_pinvoke& marshaled)
{
	Exception_t* ___U3CxrFaceSubsystemU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<xrFaceSubsystem>k__BackingField' of type 'FaceUpdatedEventArgs': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CxrFaceSubsystemU3Ek__BackingField_1Exception, NULL, NULL);
}
extern "C" void FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshal_pinvoke_back(const FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshaled_pinvoke& marshaled, FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881& unmarshaled)
{
	Exception_t* ___U3CxrFaceSubsystemU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<xrFaceSubsystem>k__BackingField' of type 'FaceUpdatedEventArgs': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CxrFaceSubsystemU3Ek__BackingField_1Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs
extern "C" void FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshal_pinvoke_cleanup(FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs
extern "C" void FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshal_com(const FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881& unmarshaled, FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshaled_com& marshaled)
{
	Exception_t* ___U3CxrFaceSubsystemU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<xrFaceSubsystem>k__BackingField' of type 'FaceUpdatedEventArgs': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CxrFaceSubsystemU3Ek__BackingField_1Exception, NULL, NULL);
}
extern "C" void FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshal_com_back(const FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshaled_com& marshaled, FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881& unmarshaled)
{
	Exception_t* ___U3CxrFaceSubsystemU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<xrFaceSubsystem>k__BackingField' of type 'FaceUpdatedEventArgs': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CxrFaceSubsystemU3Ek__BackingField_1Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs
extern "C" void FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshal_com_cleanup(FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_marshaled_com& marshaled)
{
}
// UnityEngine.XR.FaceSubsystem.XRFace UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::get_xrFace()
extern "C" IL2CPP_METHOD_ATTR XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  FaceUpdatedEventArgs_get_xrFace_m30FD6A70CC39F75122484CC0CC994775EA38F335 (FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * __this, const RuntimeMethod* method)
{
	{
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_0 = __this->get_U3CxrFaceU3Ek__BackingField_0();
		return L_0;
	}
}
extern "C"  XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  FaceUpdatedEventArgs_get_xrFace_m30FD6A70CC39F75122484CC0CC994775EA38F335_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * _thisAdjusted = reinterpret_cast<FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *>(__this + 1);
	return FaceUpdatedEventArgs_get_xrFace_m30FD6A70CC39F75122484CC0CC994775EA38F335(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::set_xrFace(UnityEngine.XR.FaceSubsystem.XRFace)
extern "C" IL2CPP_METHOD_ATTR void FaceUpdatedEventArgs_set_xrFace_m80640B5719F9F606EE6294E8147778FE4548E619 (FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * __this, XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___value0, const RuntimeMethod* method)
{
	{
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_0 = ___value0;
		__this->set_U3CxrFaceU3Ek__BackingField_0(L_0);
		return;
	}
}
extern "C"  void FaceUpdatedEventArgs_set_xrFace_m80640B5719F9F606EE6294E8147778FE4548E619_AdjustorThunk (RuntimeObject * __this, XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___value0, const RuntimeMethod* method)
{
	FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * _thisAdjusted = reinterpret_cast<FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *>(__this + 1);
	FaceUpdatedEventArgs_set_xrFace_m80640B5719F9F606EE6294E8147778FE4548E619(_thisAdjusted, ___value0, method);
}
// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::get_xrFaceSubsystem()
extern "C" IL2CPP_METHOD_ATTR XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * FaceUpdatedEventArgs_get_xrFaceSubsystem_m398584DF2801F81F073C14859B52651FF5A0641B (FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * __this, const RuntimeMethod* method)
{
	{
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_0 = __this->get_U3CxrFaceSubsystemU3Ek__BackingField_1();
		return L_0;
	}
}
extern "C"  XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * FaceUpdatedEventArgs_get_xrFaceSubsystem_m398584DF2801F81F073C14859B52651FF5A0641B_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * _thisAdjusted = reinterpret_cast<FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *>(__this + 1);
	return FaceUpdatedEventArgs_get_xrFaceSubsystem_m398584DF2801F81F073C14859B52651FF5A0641B(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::set_xrFaceSubsystem(UnityEngine.XR.FaceSubsystem.XRFaceSubsystem)
extern "C" IL2CPP_METHOD_ATTR void FaceUpdatedEventArgs_set_xrFaceSubsystem_m9B360E62675AC6D1A16511FDA94C05FBF59CE710 (FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * __this, XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___value0, const RuntimeMethod* method)
{
	{
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_0 = ___value0;
		__this->set_U3CxrFaceSubsystemU3Ek__BackingField_1(L_0);
		return;
	}
}
extern "C"  void FaceUpdatedEventArgs_set_xrFaceSubsystem_m9B360E62675AC6D1A16511FDA94C05FBF59CE710_AdjustorThunk (RuntimeObject * __this, XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___value0, const RuntimeMethod* method)
{
	FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * _thisAdjusted = reinterpret_cast<FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *>(__this + 1);
	FaceUpdatedEventArgs_set_xrFaceSubsystem_m9B360E62675AC6D1A16511FDA94C05FBF59CE710(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::Equals(UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs)
extern "C" IL2CPP_METHOD_ATTR bool FaceUpdatedEventArgs_Equals_mE1BC234F7CD99FCB3ADBD3BAE6ABD0167A7AAC64 (FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * __this, FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  ___other0, const RuntimeMethod* method)
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_0 = FaceUpdatedEventArgs_get_xrFace_m30FD6A70CC39F75122484CC0CC994775EA38F335((FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *)__this, /*hidden argument*/NULL);
		V_0 = L_0;
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_1 = FaceUpdatedEventArgs_get_xrFace_m30FD6A70CC39F75122484CC0CC994775EA38F335((FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *)(&___other0), /*hidden argument*/NULL);
		bool L_2 = XRFace_Equals_mF790D0578A3004BB23703684FE36115A30FFA654((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)(&V_0), L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_3 = FaceUpdatedEventArgs_get_xrFaceSubsystem_m398584DF2801F81F073C14859B52651FF5A0641B((FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *)__this, /*hidden argument*/NULL);
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_4 = FaceUpdatedEventArgs_get_xrFaceSubsystem_m398584DF2801F81F073C14859B52651FF5A0641B((FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *)(&___other0), /*hidden argument*/NULL);
		bool L_5 = Object_Equals_mD98CD6D19C28ADC48B8468F78F94D38E203D0521(L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_002a:
	{
		return (bool)0;
	}
}
extern "C"  bool FaceUpdatedEventArgs_Equals_mE1BC234F7CD99FCB3ADBD3BAE6ABD0167A7AAC64_AdjustorThunk (RuntimeObject * __this, FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  ___other0, const RuntimeMethod* method)
{
	FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * _thisAdjusted = reinterpret_cast<FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *>(__this + 1);
	return FaceUpdatedEventArgs_Equals_mE1BC234F7CD99FCB3ADBD3BAE6ABD0167A7AAC64(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool FaceUpdatedEventArgs_Equals_m07A86F9854A5DA3DF39FFA2A8B89128F7475D80D (FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FaceUpdatedEventArgs_Equals_m07A86F9854A5DA3DF39FFA2A8B89128F7475D80D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}

IL_0005:
	{
		RuntimeObject * L_1 = ___obj0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_1, FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_il2cpp_TypeInfo_var)))
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject * L_2 = ___obj0;
		bool L_3 = FaceUpdatedEventArgs_Equals_mE1BC234F7CD99FCB3ADBD3BAE6ABD0167A7AAC64((FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *)__this, ((*(FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *)((FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *)UnBox(L_2, FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_3;
	}

IL_001a:
	{
		return (bool)0;
	}
}
extern "C"  bool FaceUpdatedEventArgs_Equals_m07A86F9854A5DA3DF39FFA2A8B89128F7475D80D_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * _thisAdjusted = reinterpret_cast<FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *>(__this + 1);
	return FaceUpdatedEventArgs_Equals_m07A86F9854A5DA3DF39FFA2A8B89128F7475D80D(_thisAdjusted, ___obj0, method);
}
// System.Int32 UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t FaceUpdatedEventArgs_GetHashCode_mB0178C36B30D0FE0544A550E21291D3F8E990768 (FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * __this, const RuntimeMethod* method)
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	{
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_0 = FaceUpdatedEventArgs_get_xrFace_m30FD6A70CC39F75122484CC0CC994775EA38F335((FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *)__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = XRFace_GetHashCode_mA73B05DA6D09D503A426C95A687D07CA823CBCC7((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)(&V_0), /*hidden argument*/NULL);
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_2 = FaceUpdatedEventArgs_get_xrFaceSubsystem_m398584DF2801F81F073C14859B52651FF5A0641B((FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *)__this, /*hidden argument*/NULL);
		G_B1_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)((int32_t)486187739)));
		if (L_2)
		{
			G_B2_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)((int32_t)486187739)));
			goto IL_0025;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_0030;
	}

IL_0025:
	{
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_3 = FaceUpdatedEventArgs_get_xrFaceSubsystem_m398584DF2801F81F073C14859B52651FF5A0641B((FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *)__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_3);
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_0030:
	{
		return ((int32_t)il2cpp_codegen_add((int32_t)G_B3_1, (int32_t)G_B3_0));
	}
}
extern "C"  int32_t FaceUpdatedEventArgs_GetHashCode_mB0178C36B30D0FE0544A550E21291D3F8E990768_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 * _thisAdjusted = reinterpret_cast<FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *>(__this + 1);
	return FaceUpdatedEventArgs_GetHashCode_mB0178C36B30D0FE0544A550E21291D3F8E990768(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams
extern "C" void FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshal_pinvoke(const FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040& unmarshaled, FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshaled_pinvoke& marshaled)
{
	Exception_t* ___implementationType_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'implementationType' of type 'FaceSubsystemParams': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___implementationType_1Exception, NULL, NULL);
}
extern "C" void FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshal_pinvoke_back(const FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshaled_pinvoke& marshaled, FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040& unmarshaled)
{
	Exception_t* ___implementationType_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'implementationType' of type 'FaceSubsystemParams': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___implementationType_1Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams
extern "C" void FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshal_pinvoke_cleanup(FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams
extern "C" void FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshal_com(const FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040& unmarshaled, FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshaled_com& marshaled)
{
	Exception_t* ___implementationType_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'implementationType' of type 'FaceSubsystemParams': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___implementationType_1Exception, NULL, NULL);
}
extern "C" void FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshal_com_back(const FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshaled_com& marshaled, FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040& unmarshaled)
{
	Exception_t* ___implementationType_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'implementationType' of type 'FaceSubsystemParams': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___implementationType_1Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams
extern "C" void FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshal_com_cleanup(FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::get_supportsFacePose()
extern "C" IL2CPP_METHOD_ATTR bool FaceSubsystemParams_get_supportsFacePose_mBA2C2F12B13C70EC4A28EAEA117B2F37E544D484 (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, /*hidden argument*/NULL);
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)1))) <= ((uint32_t)0)))? 1 : 0);
	}
}
extern "C"  bool FaceSubsystemParams_get_supportsFacePose_mBA2C2F12B13C70EC4A28EAEA117B2F37E544D484_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * _thisAdjusted = reinterpret_cast<FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *>(__this + 1);
	return FaceSubsystemParams_get_supportsFacePose_mBA2C2F12B13C70EC4A28EAEA117B2F37E544D484(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::set_supportsFacePose(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FaceSubsystemParams_set_supportsFacePose_m9EFB37DEDF1C8F11C80FD3D08DF001003D3DE4A0 (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, /*hidden argument*/NULL);
		FaceSubsystemParams_set_capabilities_m97A763938B473E82F5D6B0CF04835F19F76E7852((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, ((int32_t)((int32_t)L_1|(int32_t)1)), /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		int32_t L_2 = FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, /*hidden argument*/NULL);
		FaceSubsystemParams_set_capabilities_m97A763938B473E82F5D6B0CF04835F19F76E7852((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, ((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2))), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void FaceSubsystemParams_set_supportsFacePose_m9EFB37DEDF1C8F11C80FD3D08DF001003D3DE4A0_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * _thisAdjusted = reinterpret_cast<FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *>(__this + 1);
	FaceSubsystemParams_set_supportsFacePose_m9EFB37DEDF1C8F11C80FD3D08DF001003D3DE4A0(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::get_supportsFaceMeshVerticesAndIndices()
extern "C" IL2CPP_METHOD_ATTR bool FaceSubsystemParams_get_supportsFaceMeshVerticesAndIndices_m72F0281E748C0F601FA1069EE360125C7756452B (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, /*hidden argument*/NULL);
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)2))) <= ((uint32_t)0)))? 1 : 0);
	}
}
extern "C"  bool FaceSubsystemParams_get_supportsFaceMeshVerticesAndIndices_m72F0281E748C0F601FA1069EE360125C7756452B_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * _thisAdjusted = reinterpret_cast<FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *>(__this + 1);
	return FaceSubsystemParams_get_supportsFaceMeshVerticesAndIndices_m72F0281E748C0F601FA1069EE360125C7756452B(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::set_supportsFaceMeshVerticesAndIndices(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FaceSubsystemParams_set_supportsFaceMeshVerticesAndIndices_m6B5F82152F894432767871C6263B60AF174878F5 (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, /*hidden argument*/NULL);
		FaceSubsystemParams_set_capabilities_m97A763938B473E82F5D6B0CF04835F19F76E7852((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, ((int32_t)((int32_t)L_1|(int32_t)2)), /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		int32_t L_2 = FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, /*hidden argument*/NULL);
		FaceSubsystemParams_set_capabilities_m97A763938B473E82F5D6B0CF04835F19F76E7852((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, ((int32_t)((int32_t)L_2&(int32_t)((int32_t)-3))), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void FaceSubsystemParams_set_supportsFaceMeshVerticesAndIndices_m6B5F82152F894432767871C6263B60AF174878F5_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * _thisAdjusted = reinterpret_cast<FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *>(__this + 1);
	FaceSubsystemParams_set_supportsFaceMeshVerticesAndIndices_m6B5F82152F894432767871C6263B60AF174878F5(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::get_supportsFaceMeshUVs()
extern "C" IL2CPP_METHOD_ATTR bool FaceSubsystemParams_get_supportsFaceMeshUVs_m1B8096A3E173E864538204DD495F5B58278847DE (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, /*hidden argument*/NULL);
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)4))) <= ((uint32_t)0)))? 1 : 0);
	}
}
extern "C"  bool FaceSubsystemParams_get_supportsFaceMeshUVs_m1B8096A3E173E864538204DD495F5B58278847DE_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * _thisAdjusted = reinterpret_cast<FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *>(__this + 1);
	return FaceSubsystemParams_get_supportsFaceMeshUVs_m1B8096A3E173E864538204DD495F5B58278847DE(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::set_supportsFaceMeshUVs(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FaceSubsystemParams_set_supportsFaceMeshUVs_m0AB5CCED2C9C601389A1C68DADCDF0DCAD4A02BA (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, /*hidden argument*/NULL);
		FaceSubsystemParams_set_capabilities_m97A763938B473E82F5D6B0CF04835F19F76E7852((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, ((int32_t)((int32_t)L_1|(int32_t)4)), /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		int32_t L_2 = FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, /*hidden argument*/NULL);
		FaceSubsystemParams_set_capabilities_m97A763938B473E82F5D6B0CF04835F19F76E7852((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, ((int32_t)((int32_t)L_2&(int32_t)((int32_t)-5))), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void FaceSubsystemParams_set_supportsFaceMeshUVs_m0AB5CCED2C9C601389A1C68DADCDF0DCAD4A02BA_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * _thisAdjusted = reinterpret_cast<FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *>(__this + 1);
	FaceSubsystemParams_set_supportsFaceMeshUVs_m0AB5CCED2C9C601389A1C68DADCDF0DCAD4A02BA(_thisAdjusted, ___value0, method);
}
// UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemCapabilities UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::get_capabilities()
extern "C" IL2CPP_METHOD_ATTR int32_t FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383 (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CcapabilitiesU3Ek__BackingField_2();
		return L_0;
	}
}
extern "C"  int32_t FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * _thisAdjusted = reinterpret_cast<FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *>(__this + 1);
	return FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::set_capabilities(UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemCapabilities)
extern "C" IL2CPP_METHOD_ATTR void FaceSubsystemParams_set_capabilities_m97A763938B473E82F5D6B0CF04835F19F76E7852 (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CcapabilitiesU3Ek__BackingField_2(L_0);
		return;
	}
}
extern "C"  void FaceSubsystemParams_set_capabilities_m97A763938B473E82F5D6B0CF04835F19F76E7852_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * _thisAdjusted = reinterpret_cast<FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *>(__this + 1);
	FaceSubsystemParams_set_capabilities_m97A763938B473E82F5D6B0CF04835F19F76E7852(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::Equals(UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams)
extern "C" IL2CPP_METHOD_ATTR bool FaceSubsystemParams_Equals_mF5B83F9C750E61CED7610753A84927E59AC9B4DF (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040  ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FaceSubsystemParams_Equals_mF5B83F9C750E61CED7610753A84927E59AC9B4DF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, /*hidden argument*/NULL);
		int32_t L_1 = FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)(&___other0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_2 = __this->get_id_0();
		FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040  L_3 = ___other0;
		String_t* L_4 = L_3.get_id_0();
		NullCheck(L_2);
		bool L_5 = String_Equals_m9C4D78DFA0979504FE31429B64A4C26DF48020D1(L_2, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		Type_t * L_6 = __this->get_implementationType_1();
		FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040  L_7 = ___other0;
		Type_t * L_8 = L_7.get_implementationType_1();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		bool L_9 = Type_op_Equality_m7040622C9E1037EFC73E1F0EDB1DD241282BE3D8(L_6, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0034:
	{
		return (bool)0;
	}
}
extern "C"  bool FaceSubsystemParams_Equals_mF5B83F9C750E61CED7610753A84927E59AC9B4DF_AdjustorThunk (RuntimeObject * __this, FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040  ___other0, const RuntimeMethod* method)
{
	FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * _thisAdjusted = reinterpret_cast<FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *>(__this + 1);
	return FaceSubsystemParams_Equals_mF5B83F9C750E61CED7610753A84927E59AC9B4DF(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool FaceSubsystemParams_Equals_m78CA1466752559D70811FFF4873496CFB7B4FA59 (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FaceSubsystemParams_Equals_m78CA1466752559D70811FFF4873496CFB7B4FA59_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___obj0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_il2cpp_TypeInfo_var)))
		{
			goto IL_000a;
		}
	}
	{
		return (bool)0;
	}

IL_000a:
	{
		RuntimeObject * L_1 = ___obj0;
		bool L_2 = FaceSubsystemParams_Equals_mF5B83F9C750E61CED7610753A84927E59AC9B4DF((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, ((*(FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)UnBox(L_1, FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  bool FaceSubsystemParams_Equals_m78CA1466752559D70811FFF4873496CFB7B4FA59_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * _thisAdjusted = reinterpret_cast<FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *>(__this + 1);
	return FaceSubsystemParams_Equals_m78CA1466752559D70811FFF4873496CFB7B4FA59(_thisAdjusted, ___obj0, method);
}
// System.Int32 UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t FaceSubsystemParams_GetHashCode_mF1A4912B21F5E3855DFB48B6367803E3936F7B69 (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = __this->get_id_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		Type_t * L_2 = __this->get_implementationType_1();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_2);
		int32_t L_4 = FaceSubsystemParams_get_capabilities_m6EE9AF5E84E06F3FEA9013DCFD086A217C1B3383((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)__this, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = Int32_GetHashCode_m245C424ECE351E5FE3277A88EEB02132DAB8C25A((int32_t*)(&V_0), /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)((int32_t)486187739))), (int32_t)L_3)), (int32_t)((int32_t)486187739))), (int32_t)L_5));
	}
}
extern "C"  int32_t FaceSubsystemParams_GetHashCode_mF1A4912B21F5E3855DFB48B6367803E3936F7B69_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 * _thisAdjusted = reinterpret_cast<FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *>(__this + 1);
	return FaceSubsystemParams_GetHashCode_mF1A4912B21F5E3855DFB48B6367803E3936F7B69(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Experimental.XR.TrackableId UnityEngine.XR.FaceSubsystem.XRFace::get_trackableId()
extern "C" IL2CPP_METHOD_ATTR TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  XRFace_get_trackableId_mA9C77882D3F53863FAAFD46B8B0061BA26035314 (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * __this, const RuntimeMethod* method)
{
	{
		TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  L_0 = __this->get_m_TrackableId_0();
		return L_0;
	}
}
extern "C"  TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  XRFace_get_trackableId_mA9C77882D3F53863FAAFD46B8B0061BA26035314_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * _thisAdjusted = reinterpret_cast<XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *>(__this + 1);
	return XRFace_get_trackableId_mA9C77882D3F53863FAAFD46B8B0061BA26035314(_thisAdjusted, method);
}
// UnityEngine.Pose UnityEngine.XR.FaceSubsystem.XRFace::get_pose()
extern "C" IL2CPP_METHOD_ATTR Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  XRFace_get_pose_mB994DFF96F2BEBF1D9696A60BEE72F94F51C859B (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * __this, const RuntimeMethod* method)
{
	{
		Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  L_0 = __this->get_m_Pose_1();
		return L_0;
	}
}
extern "C"  Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  XRFace_get_pose_mB994DFF96F2BEBF1D9696A60BEE72F94F51C859B_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * _thisAdjusted = reinterpret_cast<XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *>(__this + 1);
	return XRFace_get_pose_mB994DFF96F2BEBF1D9696A60BEE72F94F51C859B(_thisAdjusted, method);
}
// System.Boolean UnityEngine.XR.FaceSubsystem.XRFace::get_wasUpdated()
extern "C" IL2CPP_METHOD_ATTR bool XRFace_get_wasUpdated_mD57787D097DCDA6031D9859E87842B0E484EBF1C (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_WasUpdated_2();
		return (bool)((!(((uint32_t)L_0) <= ((uint32_t)0)))? 1 : 0);
	}
}
extern "C"  bool XRFace_get_wasUpdated_mD57787D097DCDA6031D9859E87842B0E484EBF1C_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * _thisAdjusted = reinterpret_cast<XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *>(__this + 1);
	return XRFace_get_wasUpdated_mD57787D097DCDA6031D9859E87842B0E484EBF1C(_thisAdjusted, method);
}
// System.Boolean UnityEngine.XR.FaceSubsystem.XRFace::get_isTracked()
extern "C" IL2CPP_METHOD_ATTR bool XRFace_get_isTracked_m8C63C8FEDC4EF3D6093BCB33FEEF3B27E6425ACC (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_IsTracked_3();
		return (bool)((!(((uint32_t)L_0) <= ((uint32_t)0)))? 1 : 0);
	}
}
extern "C"  bool XRFace_get_isTracked_m8C63C8FEDC4EF3D6093BCB33FEEF3B27E6425ACC_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * _thisAdjusted = reinterpret_cast<XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *>(__this + 1);
	return XRFace_get_isTracked_m8C63C8FEDC4EF3D6093BCB33FEEF3B27E6425ACC(_thisAdjusted, method);
}
// System.Boolean UnityEngine.XR.FaceSubsystem.XRFace::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool XRFace_Equals_m97884868087251BBF598B60530D90989ED5494AB (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFace_Equals_m97884868087251BBF598B60530D90989ED5494AB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}

IL_0005:
	{
		RuntimeObject * L_1 = ___obj0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_1, XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246_il2cpp_TypeInfo_var)))
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject * L_2 = ___obj0;
		bool L_3 = XRFace_Equals_mF790D0578A3004BB23703684FE36115A30FFA654((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)__this, ((*(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)UnBox(L_2, XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_3;
	}

IL_001a:
	{
		return (bool)0;
	}
}
extern "C"  bool XRFace_Equals_m97884868087251BBF598B60530D90989ED5494AB_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * _thisAdjusted = reinterpret_cast<XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *>(__this + 1);
	return XRFace_Equals_m97884868087251BBF598B60530D90989ED5494AB(_thisAdjusted, ___obj0, method);
}
// System.Int32 UnityEngine.XR.FaceSubsystem.XRFace::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t XRFace_GetHashCode_mA73B05DA6D09D503A426C95A687D07CA823CBCC7 (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * __this, const RuntimeMethod* method)
{
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	{
		TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  L_0 = XRFace_get_trackableId_mA9C77882D3F53863FAAFD46B8B0061BA26035314((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = TrackableId_GetHashCode_mCADD3CC9841B05FEE526C31572F130C063C0D39B((TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B *)(&V_0), /*hidden argument*/NULL);
		Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  L_2 = XRFace_get_pose_mB994DFF96F2BEBF1D9696A60BEE72F94F51C859B((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Pose_GetHashCode_m17AC0D28F5BD43DE0CCFA4CC1A870C525E0D6066((Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 *)(&V_1), /*hidden argument*/NULL);
		bool L_4 = XRFace_get_wasUpdated_mD57787D097DCDA6031D9859E87842B0E484EBF1C((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Boolean_GetHashCode_m92C426D44100ED098FEECC96A743C3CB92DFF737((bool*)(&V_2), /*hidden argument*/NULL);
		bool L_6 = XRFace_get_isTracked_m8C63C8FEDC4EF3D6093BCB33FEEF3B27E6425ACC((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)__this, /*hidden argument*/NULL);
		V_2 = L_6;
		int32_t L_7 = Boolean_GetHashCode_m92C426D44100ED098FEECC96A743C3CB92DFF737((bool*)(&V_2), /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)((int32_t)486187739))), (int32_t)L_3)), (int32_t)((int32_t)486187739))), (int32_t)L_5)), (int32_t)((int32_t)486187739))), (int32_t)L_7));
	}
}
extern "C"  int32_t XRFace_GetHashCode_mA73B05DA6D09D503A426C95A687D07CA823CBCC7_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * _thisAdjusted = reinterpret_cast<XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *>(__this + 1);
	return XRFace_GetHashCode_mA73B05DA6D09D503A426C95A687D07CA823CBCC7(_thisAdjusted, method);
}
// System.Boolean UnityEngine.XR.FaceSubsystem.XRFace::Equals(UnityEngine.XR.FaceSubsystem.XRFace)
extern "C" IL2CPP_METHOD_ATTR bool XRFace_Equals_mF790D0578A3004BB23703684FE36115A30FFA654 (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * __this, XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___other0, const RuntimeMethod* method)
{
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  L_0 = XRFace_get_trackableId_mA9C77882D3F53863FAAFD46B8B0061BA26035314((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)__this, /*hidden argument*/NULL);
		V_0 = L_0;
		TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  L_1 = XRFace_get_trackableId_mA9C77882D3F53863FAAFD46B8B0061BA26035314((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)(&___other0), /*hidden argument*/NULL);
		bool L_2 = TrackableId_Equals_mCF5272276A4DC394A0590A0A417B2578E4C76E4D((TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B *)(&V_0), L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004d;
		}
	}
	{
		Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  L_3 = XRFace_get_pose_mB994DFF96F2BEBF1D9696A60BEE72F94F51C859B((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)__this, /*hidden argument*/NULL);
		V_1 = L_3;
		Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  L_4 = XRFace_get_pose_mB994DFF96F2BEBF1D9696A60BEE72F94F51C859B((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)(&___other0), /*hidden argument*/NULL);
		bool L_5 = Pose_Equals_m867264C8DF91FF8DC3AD957EF1625902CDEBAEDD((Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 *)(&V_1), L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004d;
		}
	}
	{
		bool L_6 = XRFace_get_wasUpdated_mD57787D097DCDA6031D9859E87842B0E484EBF1C((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)__this, /*hidden argument*/NULL);
		bool L_7 = XRFace_get_wasUpdated_mD57787D097DCDA6031D9859E87842B0E484EBF1C((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)(&___other0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_004d;
		}
	}
	{
		bool L_8 = XRFace_get_isTracked_m8C63C8FEDC4EF3D6093BCB33FEEF3B27E6425ACC((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)__this, /*hidden argument*/NULL);
		bool L_9 = XRFace_get_isTracked_m8C63C8FEDC4EF3D6093BCB33FEEF3B27E6425ACC((XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *)(&___other0), /*hidden argument*/NULL);
		return (bool)((((int32_t)L_8) == ((int32_t)L_9))? 1 : 0);
	}

IL_004d:
	{
		return (bool)0;
	}
}
extern "C"  bool XRFace_Equals_mF790D0578A3004BB23703684FE36115A30FFA654_AdjustorThunk (RuntimeObject * __this, XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___other0, const RuntimeMethod* method)
{
	XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 * _thisAdjusted = reinterpret_cast<XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 *>(__this + 1);
	return XRFace_Equals_mF790D0578A3004BB23703684FE36115A30FFA654(_thisAdjusted, ___other0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::add_faceAdded(System.Action`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>)
extern "C" IL2CPP_METHOD_ATTR void XRFaceSubsystem_add_faceAdded_m9063078B8F717927ADEB63885B231F862E269F87 (XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * __this, Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystem_add_faceAdded_m9063078B8F717927ADEB63885B231F862E269F87_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * V_0 = NULL;
	Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * V_1 = NULL;
	Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * V_2 = NULL;
	{
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_0 = __this->get_faceAdded_4();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_1 = V_0;
		V_1 = L_1;
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_2 = V_1;
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B *)CastclassSealed((RuntimeObject*)L_4, Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B_il2cpp_TypeInfo_var));
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B ** L_5 = __this->get_address_of_faceAdded_4();
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_6 = V_2;
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_7 = V_1;
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_8 = InterlockedCompareExchangeImpl<Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B *>((Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B **)L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_9 = V_0;
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B *)L_9) == ((RuntimeObject*)(Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::remove_faceAdded(System.Action`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>)
extern "C" IL2CPP_METHOD_ATTR void XRFaceSubsystem_remove_faceAdded_m51A63CB063397B2B0AA0340267FAB2A799325BC1 (XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * __this, Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystem_remove_faceAdded_m51A63CB063397B2B0AA0340267FAB2A799325BC1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * V_0 = NULL;
	Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * V_1 = NULL;
	Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * V_2 = NULL;
	{
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_0 = __this->get_faceAdded_4();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_1 = V_0;
		V_1 = L_1;
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_2 = V_1;
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B *)CastclassSealed((RuntimeObject*)L_4, Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B_il2cpp_TypeInfo_var));
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B ** L_5 = __this->get_address_of_faceAdded_4();
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_6 = V_2;
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_7 = V_1;
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_8 = InterlockedCompareExchangeImpl<Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B *>((Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B **)L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_9 = V_0;
		Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B *)L_9) == ((RuntimeObject*)(Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::add_faceUpdated(System.Action`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>)
extern "C" IL2CPP_METHOD_ATTR void XRFaceSubsystem_add_faceUpdated_m7A38CEF5EAFD6BADECE5AE02B3DC1775A261FA96 (XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * __this, Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystem_add_faceUpdated_m7A38CEF5EAFD6BADECE5AE02B3DC1775A261FA96_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * V_0 = NULL;
	Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * V_1 = NULL;
	Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * V_2 = NULL;
	{
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_0 = __this->get_faceUpdated_5();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_1 = V_0;
		V_1 = L_1;
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_2 = V_1;
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 *)CastclassSealed((RuntimeObject*)L_4, Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4_il2cpp_TypeInfo_var));
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 ** L_5 = __this->get_address_of_faceUpdated_5();
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_6 = V_2;
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_7 = V_1;
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_8 = InterlockedCompareExchangeImpl<Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 *>((Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 **)L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_9 = V_0;
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 *)L_9) == ((RuntimeObject*)(Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::remove_faceUpdated(System.Action`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>)
extern "C" IL2CPP_METHOD_ATTR void XRFaceSubsystem_remove_faceUpdated_m21DFD1FAD1E2F0CE775C2C1EC66A5C8070EBE637 (XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * __this, Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystem_remove_faceUpdated_m21DFD1FAD1E2F0CE775C2C1EC66A5C8070EBE637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * V_0 = NULL;
	Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * V_1 = NULL;
	Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * V_2 = NULL;
	{
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_0 = __this->get_faceUpdated_5();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_1 = V_0;
		V_1 = L_1;
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_2 = V_1;
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 *)CastclassSealed((RuntimeObject*)L_4, Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4_il2cpp_TypeInfo_var));
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 ** L_5 = __this->get_address_of_faceUpdated_5();
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_6 = V_2;
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_7 = V_1;
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_8 = InterlockedCompareExchangeImpl<Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 *>((Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 **)L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_9 = V_0;
		Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 *)L_9) == ((RuntimeObject*)(Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::add_faceRemoved(System.Action`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>)
extern "C" IL2CPP_METHOD_ATTR void XRFaceSubsystem_add_faceRemoved_m56B1750EA0FE784A945908FFA89D2E9650B12C43 (XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * __this, Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystem_add_faceRemoved_m56B1750EA0FE784A945908FFA89D2E9650B12C43_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * V_0 = NULL;
	Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * V_1 = NULL;
	Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * V_2 = NULL;
	{
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_0 = __this->get_faceRemoved_6();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_2 = V_1;
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 *)CastclassSealed((RuntimeObject*)L_4, Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466_il2cpp_TypeInfo_var));
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 ** L_5 = __this->get_address_of_faceRemoved_6();
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_6 = V_2;
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_7 = V_1;
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_8 = InterlockedCompareExchangeImpl<Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 *>((Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 **)L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_9 = V_0;
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 *)L_9) == ((RuntimeObject*)(Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::remove_faceRemoved(System.Action`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>)
extern "C" IL2CPP_METHOD_ATTR void XRFaceSubsystem_remove_faceRemoved_mC3F6C6B0A2585F72E2E5649B9D21ADC493AF2543 (XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * __this, Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystem_remove_faceRemoved_mC3F6C6B0A2585F72E2E5649B9D21ADC493AF2543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * V_0 = NULL;
	Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * V_1 = NULL;
	Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * V_2 = NULL;
	{
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_0 = __this->get_faceRemoved_6();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_2 = V_1;
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 *)CastclassSealed((RuntimeObject*)L_4, Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466_il2cpp_TypeInfo_var));
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 ** L_5 = __this->get_address_of_faceRemoved_6();
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_6 = V_2;
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_7 = V_1;
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_8 = InterlockedCompareExchangeImpl<Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 *>((Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 **)L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_9 = V_0;
		Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 *)L_9) == ((RuntimeObject*)(Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::.ctor()
extern "C" IL2CPP_METHOD_ATTR void XRFaceSubsystem__ctor_mE2CFD752717175ECF40CD6A8A267ED8768352C8F (XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystem__ctor_mE2CFD752717175ECF40CD6A8A267ED8768352C8F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Subsystem_1__ctor_m1CF0FE2230E65CDBD346CF4F52035DD2BC9D2822(__this, /*hidden argument*/Subsystem_1__ctor_m1CF0FE2230E65CDBD346CF4F52035DD2BC9D2822_RuntimeMethod_var);
		List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * L_0 = (List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A *)il2cpp_codegen_object_new(List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A_il2cpp_TypeInfo_var);
		List_1__ctor_mF51861616D3E151AAAA319FF158E26510A0D359C(L_0, /*hidden argument*/List_1__ctor_mF51861616D3E151AAAA319FF158E26510A0D359C_RuntimeMethod_var);
		((XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields*)il2cpp_codegen_static_fields_for(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_il2cpp_TypeInfo_var))->set_s_FacesAddedThisFrame_1(L_0);
		List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * L_1 = (List_1_tE2C2476479F1A844897C46751D09491B2534EC87 *)il2cpp_codegen_object_new(List_1_tE2C2476479F1A844897C46751D09491B2534EC87_il2cpp_TypeInfo_var);
		List_1__ctor_mA0B1C89A6269A80CC746ED26EC65BD390FD75665(L_1, /*hidden argument*/List_1__ctor_mA0B1C89A6269A80CC746ED26EC65BD390FD75665_RuntimeMethod_var);
		((XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields*)il2cpp_codegen_static_fields_for(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_il2cpp_TypeInfo_var))->set_s_FacesUpdatedThisFrame_2(L_1);
		List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * L_2 = (List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 *)il2cpp_codegen_object_new(List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483_il2cpp_TypeInfo_var);
		List_1__ctor_mD27DD9E2C6D2DD48607B83DF444B3CF65AE90A19(L_2, /*hidden argument*/List_1__ctor_mD27DD9E2C6D2DD48607B83DF444B3CF65AE90A19_RuntimeMethod_var);
		((XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields*)il2cpp_codegen_static_fields_for(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_il2cpp_TypeInfo_var))->set_s_FacesRemovedThisFrame_3(L_2);
		return;
	}
}
// System.Boolean UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::TryGetAllFaces(System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.XRFace>)
extern "C" IL2CPP_METHOD_ATTR bool XRFaceSubsystem_TryGetAllFaces_mAEF395610A1CA3CA934A05F442DE066B5E9C6D0B (XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * __this, List_1_t9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30 * ___facesOut0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystem_TryGetAllFaces_mAEF395610A1CA3CA934A05F442DE066B5E9C6D0B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30 * L_0 = ___facesOut0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, _stringLiteral37A0B5FF3D6DDD7EB1501626195A8DC712AD0A51, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, XRFaceSubsystem_TryGetAllFaces_mAEF395610A1CA3CA934A05F442DE066B5E9C6D0B_RuntimeMethod_var);
	}

IL_000e:
	{
		List_1_t9CD6DC3D8EF96DD882967736B3E6F8D44D44BC30 * L_2 = ___facesOut0;
		NullCheck(L_2);
		List_1_Clear_mE5C402CDF41FDFF018276341D49C6864C9950827(L_2, /*hidden argument*/List_1_Clear_mE5C402CDF41FDFF018276341D49C6864C9950827_RuntimeMethod_var);
		return (bool)0;
	}
}
// System.Boolean UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::TryRemoveFace(UnityEngine.Experimental.XR.TrackableId)
extern "C" IL2CPP_METHOD_ATTR bool XRFaceSubsystem_TryRemoveFace_m33BCAD1AAFAF96F315D834108C001E1625C77B65 (XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * __this, TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___faceId0, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::TryGetFaceMeshVertices(UnityEngine.Experimental.XR.TrackableId,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C" IL2CPP_METHOD_ATTR bool XRFaceSubsystem_TryGetFaceMeshVertices_m0AE585B6EE72BB59AD0CD4816787A990CCF4712E (XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * __this, TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___faceId0, List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___verticesOut1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystem_TryGetFaceMeshVertices_m0AE585B6EE72BB59AD0CD4816787A990CCF4712E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_0 = ___verticesOut1;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, _stringLiteral84DBAEF25399215C4823814053CC133F7AD11C90, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, XRFaceSubsystem_TryGetFaceMeshVertices_m0AE585B6EE72BB59AD0CD4816787A990CCF4712E_RuntimeMethod_var);
	}

IL_000e:
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_2 = ___verticesOut1;
		NullCheck(L_2);
		List_1_Clear_m8E7A9E8CF891528845C4B071CB8166DDB1A384B8(L_2, /*hidden argument*/List_1_Clear_m8E7A9E8CF891528845C4B071CB8166DDB1A384B8_RuntimeMethod_var);
		return (bool)0;
	}
}
// System.Boolean UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::TryGetFaceMeshUVs(UnityEngine.Experimental.XR.TrackableId,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern "C" IL2CPP_METHOD_ATTR bool XRFaceSubsystem_TryGetFaceMeshUVs_m3815AB707969C1199BEE286F57B540DFC3C059FE (XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * __this, TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___faceId0, List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___uvsOut1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystem_TryGetFaceMeshUVs_m3815AB707969C1199BEE286F57B540DFC3C059FE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_0 = ___uvsOut1;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, _stringLiteralBE956C6BBE22C9317B08530A0E1E1E9ABED5815A, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, XRFaceSubsystem_TryGetFaceMeshUVs_m3815AB707969C1199BEE286F57B540DFC3C059FE_RuntimeMethod_var);
	}

IL_000e:
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_2 = ___uvsOut1;
		NullCheck(L_2);
		List_1_Clear_m471E9C96E037C142725FB47332197B948E5D38AE(L_2, /*hidden argument*/List_1_Clear_m471E9C96E037C142725FB47332197B948E5D38AE_RuntimeMethod_var);
		return (bool)0;
	}
}
// System.Boolean UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::TryGetFaceMeshIndices(UnityEngine.Experimental.XR.TrackableId,System.Collections.Generic.List`1<System.Int32>)
extern "C" IL2CPP_METHOD_ATTR bool XRFaceSubsystem_TryGetFaceMeshIndices_m13B824E553BA6411FB0211C707595706E7B77EA1 (XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * __this, TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___faceId0, List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___indicesOut1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystem_TryGetFaceMeshIndices_m13B824E553BA6411FB0211C707595706E7B77EA1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_0 = ___indicesOut1;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, _stringLiteral3DE5C8935E5C0504750A6A49DD9533047C38EC8C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, XRFaceSubsystem_TryGetFaceMeshIndices_m13B824E553BA6411FB0211C707595706E7B77EA1_RuntimeMethod_var);
	}

IL_000e:
	{
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_2 = ___indicesOut1;
		NullCheck(L_2);
		List_1_Clear_m06BA343FB4E149EB045D8D2603E1AD239E1E4477(L_2, /*hidden argument*/List_1_Clear_m06BA343FB4E149EB045D8D2603E1AD239E1E4477_RuntimeMethod_var);
		return (bool)0;
	}
}
// System.Void UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::InvokeFaceAddedCallback(UnityEngine.XR.FaceSubsystem.XRFace,UnityEngine.XR.FaceSubsystem.XRFaceSubsystem)
extern "C" IL2CPP_METHOD_ATTR void XRFaceSubsystem_InvokeFaceAddedCallback_m4FABBC7D82C155B417631DD0BAAB3F34B6AA4168 (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___xrFace0, XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___faceSubsystem1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystem_InvokeFaceAddedCallback_m4FABBC7D82C155B417631DD0BAAB3F34B6AA4168_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  V_0;
	memset(&V_0, 0, sizeof(V_0));
	FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		il2cpp_codegen_initobj((&V_1), sizeof(FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C ));
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_0 = ___xrFace0;
		FaceAddedEventArgs_set_xrFace_m06387391A12AFF9158D5F025B054D8393B517A53((FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *)(&V_1), L_0, /*hidden argument*/NULL);
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_1 = ___faceSubsystem1;
		FaceAddedEventArgs_set_xrFaceSubsystem_mB59DB99C26BF29EBEDD53C2F13912B5269272F5B((FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *)(&V_1), L_1, /*hidden argument*/NULL);
		FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  L_2 = V_1;
		V_0 = L_2;
		List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * L_3 = ((XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields*)il2cpp_codegen_static_fields_for(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_il2cpp_TypeInfo_var))->get_s_FacesAddedThisFrame_1();
		FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  L_4 = V_0;
		NullCheck(L_3);
		List_1_Add_mB1E98B155D266CFCD2630F9CC1B9F09DB37AF636(L_3, L_4, /*hidden argument*/List_1_Add_mB1E98B155D266CFCD2630F9CC1B9F09DB37AF636_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::InvokeFaceUpdatedCallback(UnityEngine.XR.FaceSubsystem.XRFace,UnityEngine.XR.FaceSubsystem.XRFaceSubsystem)
extern "C" IL2CPP_METHOD_ATTR void XRFaceSubsystem_InvokeFaceUpdatedCallback_m7D8F0ADC24BA2D681B7A9F01AF7E10953E46F1E7 (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___xrFace0, XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___faceSubsystem1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystem_InvokeFaceUpdatedCallback_m7D8F0ADC24BA2D681B7A9F01AF7E10953E46F1E7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  V_0;
	memset(&V_0, 0, sizeof(V_0));
	FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		il2cpp_codegen_initobj((&V_1), sizeof(FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 ));
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_0 = ___xrFace0;
		FaceUpdatedEventArgs_set_xrFace_m80640B5719F9F606EE6294E8147778FE4548E619((FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *)(&V_1), L_0, /*hidden argument*/NULL);
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_1 = ___faceSubsystem1;
		FaceUpdatedEventArgs_set_xrFaceSubsystem_m9B360E62675AC6D1A16511FDA94C05FBF59CE710((FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *)(&V_1), L_1, /*hidden argument*/NULL);
		FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  L_2 = V_1;
		V_0 = L_2;
		List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * L_3 = ((XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields*)il2cpp_codegen_static_fields_for(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_il2cpp_TypeInfo_var))->get_s_FacesUpdatedThisFrame_2();
		FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  L_4 = V_0;
		NullCheck(L_3);
		List_1_Add_m8933F527B5707C1B7AC57EA271F051C9D31DB390(L_3, L_4, /*hidden argument*/List_1_Add_m8933F527B5707C1B7AC57EA271F051C9D31DB390_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::InvokeFaceRemovedCallback(UnityEngine.XR.FaceSubsystem.XRFace,UnityEngine.XR.FaceSubsystem.XRFaceSubsystem)
extern "C" IL2CPP_METHOD_ATTR void XRFaceSubsystem_InvokeFaceRemovedCallback_mF19536F930D1F55268D8EFFAC0A44615B5ED1406 (XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  ___xrFace0, XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___faceSubsystem1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystem_InvokeFaceRemovedCallback_mF19536F930D1F55268D8EFFAC0A44615B5ED1406_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  V_0;
	memset(&V_0, 0, sizeof(V_0));
	FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		il2cpp_codegen_initobj((&V_1), sizeof(FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 ));
		XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246  L_0 = ___xrFace0;
		FaceRemovedEventArgs_set_xrFace_mD8D3D1A092F87ED3A84579CEA34CCB869C23D8CE((FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *)(&V_1), L_0, /*hidden argument*/NULL);
		XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_1 = ___faceSubsystem1;
		FaceRemovedEventArgs_set_xrFaceSubsystem_m0EE856E16539263DC748734E2C0A8E868C5BAEDC((FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *)(&V_1), L_1, /*hidden argument*/NULL);
		FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  L_2 = V_1;
		V_0 = L_2;
		List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * L_3 = ((XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields*)il2cpp_codegen_static_fields_for(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_il2cpp_TypeInfo_var))->get_s_FacesRemovedThisFrame_3();
		FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  L_4 = V_0;
		NullCheck(L_3);
		List_1_Add_mE4B2ECBE4618987FBA1B0BAC4DB8399E9DCDF343(L_3, L_4, /*hidden argument*/List_1_Add_mE4B2ECBE4618987FBA1B0BAC4DB8399E9DCDF343_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::OnBeginFrame()
extern "C" IL2CPP_METHOD_ATTR void XRFaceSubsystem_OnBeginFrame_m42D691401EE4056D7BE5142BFAEFA492CD02228A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystem_OnBeginFrame_m42D691401EE4056D7BE5142BFAEFA492CD02228A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175  V_0;
	memset(&V_0, 0, sizeof(V_0));
	FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  V_1;
	memset(&V_1, 0, sizeof(V_1));
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * V_2 = NULL;
	Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52  V_3;
	memset(&V_3, 0, sizeof(V_3));
	FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  V_4;
	memset(&V_4, 0, sizeof(V_4));
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * V_5 = NULL;
	Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2  V_6;
	memset(&V_6, 0, sizeof(V_6));
	FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  V_7;
	memset(&V_7, 0, sizeof(V_7));
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * V_8 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * L_0 = ((XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields*)il2cpp_codegen_static_fields_for(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_il2cpp_TypeInfo_var))->get_s_FacesAddedThisFrame_1();
		NullCheck(L_0);
		Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175  L_1 = List_1_GetEnumerator_m844D7301792413D133453D6B32089B45527FA4D9(L_0, /*hidden argument*/List_1_GetEnumerator_m844D7301792413D133453D6B32089B45527FA4D9_RuntimeMethod_var);
		V_0 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0031;
		}

IL_000d:
		{
			FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  L_2 = Enumerator_get_Current_m17C74A966C414AF87B4926F71D5C2D738276CD1B((Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175 *)(&V_0), /*hidden argument*/Enumerator_get_Current_m17C74A966C414AF87B4926F71D5C2D738276CD1B_RuntimeMethod_var);
			V_1 = L_2;
			XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_3 = FaceAddedEventArgs_get_xrFaceSubsystem_m029BA347E281B46D02A402C948985BE772772B7C((FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C *)(&V_1), /*hidden argument*/NULL);
			V_2 = L_3;
			XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_4 = V_2;
			NullCheck(L_4);
			Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_5 = L_4->get_faceAdded_4();
			if (!L_5)
			{
				goto IL_0031;
			}
		}

IL_0025:
		{
			XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_6 = V_2;
			NullCheck(L_6);
			Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * L_7 = L_6->get_faceAdded_4();
			FaceAddedEventArgs_t9D33653893D8E72028F38DD5820FE6E488491B0C  L_8 = V_1;
			NullCheck(L_7);
			Action_1_Invoke_mB27FFCE02ADEA076FA5F2DB7FDDD2840F843702A(L_7, L_8, /*hidden argument*/Action_1_Invoke_mB27FFCE02ADEA076FA5F2DB7FDDD2840F843702A_RuntimeMethod_var);
		}

IL_0031:
		{
			bool L_9 = Enumerator_MoveNext_m454F2B7C556DBB8E508589BBBDE61EB6178CFCAE((Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m454F2B7C556DBB8E508589BBBDE61EB6178CFCAE_RuntimeMethod_var);
			if (L_9)
			{
				goto IL_000d;
			}
		}

IL_003a:
		{
			IL2CPP_LEAVE(0x4A, FINALLY_003c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_003c;
	}

FINALLY_003c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4FD30D793AA03BA4EF0D7D2CF85A1141EE9905C8((Enumerator_t3561B819862327744BA5B81A2CE1C15CF60A3175 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m4FD30D793AA03BA4EF0D7D2CF85A1141EE9905C8_RuntimeMethod_var);
		IL2CPP_END_FINALLY(60)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(60)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_004a:
	{
		List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * L_10 = ((XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields*)il2cpp_codegen_static_fields_for(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_il2cpp_TypeInfo_var))->get_s_FacesUpdatedThisFrame_2();
		NullCheck(L_10);
		Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52  L_11 = List_1_GetEnumerator_m1ED4F0E86DCB172B575B9FB56A3A3C4B4CC04BA8(L_10, /*hidden argument*/List_1_GetEnumerator_m1ED4F0E86DCB172B575B9FB56A3A3C4B4CC04BA8_RuntimeMethod_var);
		V_3 = L_11;
	}

IL_0055:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0080;
		}

IL_0057:
		{
			FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  L_12 = Enumerator_get_Current_mA2A1CDFBB945052FF9593C69C6DFBE039C26A456((Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52 *)(&V_3), /*hidden argument*/Enumerator_get_Current_mA2A1CDFBB945052FF9593C69C6DFBE039C26A456_RuntimeMethod_var);
			V_4 = L_12;
			XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_13 = FaceUpdatedEventArgs_get_xrFaceSubsystem_m398584DF2801F81F073C14859B52651FF5A0641B((FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881 *)(&V_4), /*hidden argument*/NULL);
			V_5 = L_13;
			XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_14 = V_5;
			NullCheck(L_14);
			Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_15 = L_14->get_faceUpdated_5();
			if (!L_15)
			{
				goto IL_0080;
			}
		}

IL_0072:
		{
			XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_16 = V_5;
			NullCheck(L_16);
			Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * L_17 = L_16->get_faceUpdated_5();
			FaceUpdatedEventArgs_t1EB3DBB5A762065163012A9BACA88A75C7010881  L_18 = V_4;
			NullCheck(L_17);
			Action_1_Invoke_mB4FE57B363F0B93B1DA4E0E3B8AA425AB7B5C9A2(L_17, L_18, /*hidden argument*/Action_1_Invoke_mB4FE57B363F0B93B1DA4E0E3B8AA425AB7B5C9A2_RuntimeMethod_var);
		}

IL_0080:
		{
			bool L_19 = Enumerator_MoveNext_m67B00ADB5FD5EADF1EAE0E911B37CA27B01D267B((Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52 *)(&V_3), /*hidden argument*/Enumerator_MoveNext_m67B00ADB5FD5EADF1EAE0E911B37CA27B01D267B_RuntimeMethod_var);
			if (L_19)
			{
				goto IL_0057;
			}
		}

IL_0089:
		{
			IL2CPP_LEAVE(0x99, FINALLY_008b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_008b;
	}

FINALLY_008b:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mFECED8B0D7B6FF3521473829AFD852810EED5A32((Enumerator_t85FFFFDA0D9D6A134ACBBF1B1379B0266830DF52 *)(&V_3), /*hidden argument*/Enumerator_Dispose_mFECED8B0D7B6FF3521473829AFD852810EED5A32_RuntimeMethod_var);
		IL2CPP_END_FINALLY(139)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(139)
	{
		IL2CPP_JUMP_TBL(0x99, IL_0099)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0099:
	{
		List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * L_20 = ((XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields*)il2cpp_codegen_static_fields_for(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_il2cpp_TypeInfo_var))->get_s_FacesRemovedThisFrame_3();
		NullCheck(L_20);
		Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2  L_21 = List_1_GetEnumerator_m4C931B506F93C24E8E7B6ADA0233745BC526A3DB(L_20, /*hidden argument*/List_1_GetEnumerator_m4C931B506F93C24E8E7B6ADA0233745BC526A3DB_RuntimeMethod_var);
		V_6 = L_21;
	}

IL_00a5:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00d0;
		}

IL_00a7:
		{
			FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  L_22 = Enumerator_get_Current_m9BEB9ACA83DE929A3EC5EFAF5010BCACDC05887F((Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2 *)(&V_6), /*hidden argument*/Enumerator_get_Current_m9BEB9ACA83DE929A3EC5EFAF5010BCACDC05887F_RuntimeMethod_var);
			V_7 = L_22;
			XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_23 = FaceRemovedEventArgs_get_xrFaceSubsystem_mA5CD378FB6469B0CEAD45AA13F662357A66C0992((FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651 *)(&V_7), /*hidden argument*/NULL);
			V_8 = L_23;
			XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_24 = V_8;
			NullCheck(L_24);
			Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_25 = L_24->get_faceRemoved_6();
			if (!L_25)
			{
				goto IL_00d0;
			}
		}

IL_00c2:
		{
			XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * L_26 = V_8;
			NullCheck(L_26);
			Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * L_27 = L_26->get_faceRemoved_6();
			FaceRemovedEventArgs_t2D9E4A417A63098195E12566E34160103841D651  L_28 = V_7;
			NullCheck(L_27);
			Action_1_Invoke_mB5C96A623A8DD9F97E66150F57C9AFDE3EDB85C4(L_27, L_28, /*hidden argument*/Action_1_Invoke_mB5C96A623A8DD9F97E66150F57C9AFDE3EDB85C4_RuntimeMethod_var);
		}

IL_00d0:
		{
			bool L_29 = Enumerator_MoveNext_m7A3BAA9E235E50489080103011B7655AB5071614((Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2 *)(&V_6), /*hidden argument*/Enumerator_MoveNext_m7A3BAA9E235E50489080103011B7655AB5071614_RuntimeMethod_var);
			if (L_29)
			{
				goto IL_00a7;
			}
		}

IL_00d9:
		{
			IL2CPP_LEAVE(0xE9, FINALLY_00db);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00db;
	}

FINALLY_00db:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3155A03CAD8A96B0EF1E02E4FE8AF5DA522CDCC2((Enumerator_tB20AD31027BC3712B788271F96C8DB3F264A1DF2 *)(&V_6), /*hidden argument*/Enumerator_Dispose_m3155A03CAD8A96B0EF1E02E4FE8AF5DA522CDCC2_RuntimeMethod_var);
		IL2CPP_END_FINALLY(219)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(219)
	{
		IL2CPP_JUMP_TBL(0xE9, IL_00e9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00e9:
	{
		List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * L_30 = ((XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields*)il2cpp_codegen_static_fields_for(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_il2cpp_TypeInfo_var))->get_s_FacesAddedThisFrame_1();
		NullCheck(L_30);
		List_1_Clear_m5F612B61829DD1074F050617F5ECB93D7A81EB6B(L_30, /*hidden argument*/List_1_Clear_m5F612B61829DD1074F050617F5ECB93D7A81EB6B_RuntimeMethod_var);
		List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * L_31 = ((XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields*)il2cpp_codegen_static_fields_for(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_il2cpp_TypeInfo_var))->get_s_FacesUpdatedThisFrame_2();
		NullCheck(L_31);
		List_1_Clear_mE389EDEE3E1D94DE8DB369C6D69741DE655FBA18(L_31, /*hidden argument*/List_1_Clear_mE389EDEE3E1D94DE8DB369C6D69741DE655FBA18_RuntimeMethod_var);
		List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * L_32 = ((XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields*)il2cpp_codegen_static_fields_for(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_il2cpp_TypeInfo_var))->get_s_FacesRemovedThisFrame_3();
		NullCheck(L_32);
		List_1_Clear_m07FCFBF2DE6606E960452AEE8427E1F66B861A64(L_32, /*hidden argument*/List_1_Clear_m07FCFBF2DE6606E960452AEE8427E1F66B861A64_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor::.ctor(UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams)
extern "C" IL2CPP_METHOD_ATTR void XRFaceSubsystemDescriptor__ctor_m64CAD1F471D073C278B7BB46ECB6A8A86CC35BE0 (XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19 * __this, FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040  ___descriptorParams0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystemDescriptor__ctor_m64CAD1F471D073C278B7BB46ECB6A8A86CC35BE0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubsystemDescriptor_1__ctor_m0B1EAA83AAE682789A90078505E8DEE4048F8C1E(__this, /*hidden argument*/SubsystemDescriptor_1__ctor_m0B1EAA83AAE682789A90078505E8DEE4048F8C1E_RuntimeMethod_var);
		FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040  L_0 = ___descriptorParams0;
		String_t* L_1 = L_0.get_id_0();
		SubsystemDescriptor_set_id_mB3D1A7164F096650AE5249037EEA91AB92E21AB4(__this, L_1, /*hidden argument*/NULL);
		FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040  L_2 = ___descriptorParams0;
		Type_t * L_3 = L_2.get_implementationType_1();
		SubsystemDescriptor_set_subsystemImplementationType_m17E1DD79B83EC74F0171B4D0B3F0478FBC91F202(__this, L_3, /*hidden argument*/NULL);
		bool L_4 = FaceSubsystemParams_get_supportsFacePose_mBA2C2F12B13C70EC4A28EAEA117B2F37E544D484((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)(&___descriptorParams0), /*hidden argument*/NULL);
		__this->set_U3CsupportsFacePoseU3Ek__BackingField_2(L_4);
		bool L_5 = FaceSubsystemParams_get_supportsFaceMeshVerticesAndIndices_m72F0281E748C0F601FA1069EE360125C7756452B((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)(&___descriptorParams0), /*hidden argument*/NULL);
		__this->set_U3CsupportsFaceMeshVerticesAndIndicesU3Ek__BackingField_3(L_5);
		bool L_6 = FaceSubsystemParams_get_supportsFaceMeshUVs_m1B8096A3E173E864538204DD495F5B58278847DE((FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040 *)(&___descriptorParams0), /*hidden argument*/NULL);
		__this->set_U3CsupportsFaceMeshUVsU3Ek__BackingField_4(L_6);
		return;
	}
}
// System.Void UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor::Create(UnityEngine.XR.FaceSubsystem.Providing.FaceSubsystemParams)
extern "C" IL2CPP_METHOD_ATTR void XRFaceSubsystemDescriptor_Create_m11BC388D605EE249E0249F9D4CCC85FA9B9435C4 (FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040  ___descriptorParams0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XRFaceSubsystemDescriptor_Create_m11BC388D605EE249E0249F9D4CCC85FA9B9435C4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FaceSubsystemParams_tA2026F8E1817583A4FCE47A581DD5C29F3015040  L_0 = ___descriptorParams0;
		XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19 * L_1 = (XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19 *)il2cpp_codegen_object_new(XRFaceSubsystemDescriptor_t6952E974969B7D18CA2047C47FC5EB740507FA19_il2cpp_TypeInfo_var);
		XRFaceSubsystemDescriptor__ctor_m64CAD1F471D073C278B7BB46ECB6A8A86CC35BE0(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SubsystemRegistration_t0A22FECC46483ABBFFC039449407F73FF11F5A1A_il2cpp_TypeInfo_var);
		SubsystemRegistration_CreateDescriptor_m6BADF7D1D4E34D466EDCDDC0E533A808EABB4BE0(L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
