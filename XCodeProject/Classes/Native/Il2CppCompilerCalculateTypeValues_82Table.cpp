﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// MPAR.Common.AssetLoader.IAssets.GalleryAssetScript
struct GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B;
// MPAR.Common.AssetLoader.VideoControls
struct VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE;
// MPAR.Common.AssetLoader.VideoOptions
struct VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662;
// MPAR.Common.Input.Utility.GestureManager
struct GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Func`2<UnityEngine.GameObject,System.Boolean>
struct Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t26EB10EEE89DD3EF913D52E8797FAB841F6F2AA3;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Transform[]
struct TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC_T918A47AC82D266C2DE7DF9FF63E64936A81C9667_H
#define U3CU3EC_T918A47AC82D266C2DE7DF9FF63E64936A81C9667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.IAssets.GalleryAssetScript/<>c
struct  U3CU3Ec_t918A47AC82D266C2DE7DF9FF63E64936A81C9667  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t918A47AC82D266C2DE7DF9FF63E64936A81C9667_StaticFields
{
public:
	// MPAR.Common.AssetLoader.IAssets.GalleryAssetScript/<>c MPAR.Common.AssetLoader.IAssets.GalleryAssetScript/<>c::<>9
	U3CU3Ec_t918A47AC82D266C2DE7DF9FF63E64936A81C9667 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.GameObject,System.Boolean> MPAR.Common.AssetLoader.IAssets.GalleryAssetScript/<>c::<>9__23_0
	Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 * ___U3CU3E9__23_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t918A47AC82D266C2DE7DF9FF63E64936A81C9667_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t918A47AC82D266C2DE7DF9FF63E64936A81C9667 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t918A47AC82D266C2DE7DF9FF63E64936A81C9667 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t918A47AC82D266C2DE7DF9FF63E64936A81C9667 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__23_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t918A47AC82D266C2DE7DF9FF63E64936A81C9667_StaticFields, ___U3CU3E9__23_0_1)); }
	inline Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 * get_U3CU3E9__23_0_1() const { return ___U3CU3E9__23_0_1; }
	inline Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 ** get_address_of_U3CU3E9__23_0_1() { return &___U3CU3E9__23_0_1; }
	inline void set_U3CU3E9__23_0_1(Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 * value)
	{
		___U3CU3E9__23_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__23_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T918A47AC82D266C2DE7DF9FF63E64936A81C9667_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T5D67ABB60CD9310E8EEE68BD813524D8DE403452_H
#define U3CU3EC__DISPLAYCLASS22_0_T5D67ABB60CD9310E8EEE68BD813524D8DE403452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.IAssets.GalleryAssetScript/<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t5D67ABB60CD9310E8EEE68BD813524D8DE403452  : public RuntimeObject
{
public:
	// UnityEngine.Quaternion[] MPAR.Common.AssetLoader.IAssets.GalleryAssetScript/<>c__DisplayClass22_0::rotations
	QuaternionU5BU5D_t26EB10EEE89DD3EF913D52E8797FAB841F6F2AA3* ___rotations_0;
	// MPAR.Common.AssetLoader.IAssets.GalleryAssetScript MPAR.Common.AssetLoader.IAssets.GalleryAssetScript/<>c__DisplayClass22_0::<>4__this
	GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_rotations_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t5D67ABB60CD9310E8EEE68BD813524D8DE403452, ___rotations_0)); }
	inline QuaternionU5BU5D_t26EB10EEE89DD3EF913D52E8797FAB841F6F2AA3* get_rotations_0() const { return ___rotations_0; }
	inline QuaternionU5BU5D_t26EB10EEE89DD3EF913D52E8797FAB841F6F2AA3** get_address_of_rotations_0() { return &___rotations_0; }
	inline void set_rotations_0(QuaternionU5BU5D_t26EB10EEE89DD3EF913D52E8797FAB841F6F2AA3* value)
	{
		___rotations_0 = value;
		Il2CppCodeGenWriteBarrier((&___rotations_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t5D67ABB60CD9310E8EEE68BD813524D8DE403452, ___U3CU3E4__this_1)); }
	inline GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T5D67ABB60CD9310E8EEE68BD813524D8DE403452_H
#ifndef U3CU3EC__DISPLAYCLASS24_0_T2960E9FE7D5062420874CDD71AA392B3BD115E0D_H
#define U3CU3EC__DISPLAYCLASS24_0_T2960E9FE7D5062420874CDD71AA392B3BD115E0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.IAssets.GalleryAssetScript/<>c__DisplayClass24_0
struct  U3CU3Ec__DisplayClass24_0_t2960E9FE7D5062420874CDD71AA392B3BD115E0D  : public RuntimeObject
{
public:
	// UnityEngine.GameObject MPAR.Common.AssetLoader.IAssets.GalleryAssetScript/<>c__DisplayClass24_0::hitObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___hitObject_0;

public:
	inline static int32_t get_offset_of_hitObject_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_t2960E9FE7D5062420874CDD71AA392B3BD115E0D, ___hitObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_hitObject_0() const { return ___hitObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_hitObject_0() { return &___hitObject_0; }
	inline void set_hitObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___hitObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___hitObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS24_0_T2960E9FE7D5062420874CDD71AA392B3BD115E0D_H
#ifndef U3CU3EC__DISPLAYCLASS33_0_T2F2EC482EA2D9A093DBF1D936950BD5DE10E1FCB_H
#define U3CU3EC__DISPLAYCLASS33_0_T2F2EC482EA2D9A093DBF1D936950BD5DE10E1FCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.VideoControls/<>c__DisplayClass33_0
struct  U3CU3Ec__DisplayClass33_0_t2F2EC482EA2D9A093DBF1D936950BD5DE10E1FCB  : public RuntimeObject
{
public:
	// MPAR.Common.AssetLoader.VideoControls MPAR.Common.AssetLoader.VideoControls/<>c__DisplayClass33_0::<>4__this
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE * ___U3CU3E4__this_0;
	// UnityEngine.Transform MPAR.Common.AssetLoader.VideoControls/<>c__DisplayClass33_0::pauseButton
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___pauseButton_1;
	// UnityEngine.Transform MPAR.Common.AssetLoader.VideoControls/<>c__DisplayClass33_0::playButton
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___playButton_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t2F2EC482EA2D9A093DBF1D936950BD5DE10E1FCB, ___U3CU3E4__this_0)); }
	inline VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_pauseButton_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t2F2EC482EA2D9A093DBF1D936950BD5DE10E1FCB, ___pauseButton_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_pauseButton_1() const { return ___pauseButton_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_pauseButton_1() { return &___pauseButton_1; }
	inline void set_pauseButton_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___pauseButton_1 = value;
		Il2CppCodeGenWriteBarrier((&___pauseButton_1), value);
	}

	inline static int32_t get_offset_of_playButton_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t2F2EC482EA2D9A093DBF1D936950BD5DE10E1FCB, ___playButton_2)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_playButton_2() const { return ___playButton_2; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_playButton_2() { return &___playButton_2; }
	inline void set_playButton_2(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___playButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___playButton_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_0_T2F2EC482EA2D9A093DBF1D936950BD5DE10E1FCB_H
#ifndef U3CLOADFIRSTFRAMEU3ED__16_TD8D4631475B1ABDF9129346CF738C44D0295AF21_H
#define U3CLOADFIRSTFRAMEU3ED__16_TD8D4631475B1ABDF9129346CF738C44D0295AF21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.VideoOptions/<LoadFirstFrame>d__16
struct  U3CLoadFirstFrameU3Ed__16_tD8D4631475B1ABDF9129346CF738C44D0295AF21  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.AssetLoader.VideoOptions/<LoadFirstFrame>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.AssetLoader.VideoOptions/<LoadFirstFrame>d__16::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.AssetLoader.VideoOptions MPAR.Common.AssetLoader.VideoOptions/<LoadFirstFrame>d__16::<>4__this
	VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadFirstFrameU3Ed__16_tD8D4631475B1ABDF9129346CF738C44D0295AF21, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadFirstFrameU3Ed__16_tD8D4631475B1ABDF9129346CF738C44D0295AF21, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadFirstFrameU3Ed__16_tD8D4631475B1ABDF9129346CF738C44D0295AF21, ___U3CU3E4__this_2)); }
	inline VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADFIRSTFRAMEU3ED__16_TD8D4631475B1ABDF9129346CF738C44D0295AF21_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef __STATICARRAYINITTYPESIZEU3D32_T94E9375C899479358C941227FA08AB6B16D04315_H
#define __STATICARRAYINITTYPESIZEU3D32_T94E9375C899479358C941227FA08AB6B16D04315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32
struct  __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315__padding[32];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D32_T94E9375C899479358C941227FA08AB6B16D04315_H
#ifndef __STATICARRAYINITTYPESIZEU3D6_T962A4BB0EB2726A97333CF6FA69780E6CF9B3690_H
#define __STATICARRAYINITTYPESIZEU3D6_T962A4BB0EB2726A97333CF6FA69780E6CF9B3690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6
struct  __StaticArrayInitTypeSizeU3D6_t962A4BB0EB2726A97333CF6FA69780E6CF9B3690 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D6_t962A4BB0EB2726A97333CF6FA69780E6CF9B3690__padding[6];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D6_T962A4BB0EB2726A97333CF6FA69780E6CF9B3690_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::1094EE94F079EDF296E553F323D5FAE1D4595CA2
	__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  ___1094EE94F079EDF296E553F323D5FAE1D4595CA2_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::F1AA51837B0CB2B8735A7F2F663CED5FC0947D72
	__StaticArrayInitTypeSizeU3D6_t962A4BB0EB2726A97333CF6FA69780E6CF9B3690  ___F1AA51837B0CB2B8735A7F2F663CED5FC0947D72_1;

public:
	inline static int32_t get_offset_of_U31094EE94F079EDF296E553F323D5FAE1D4595CA2_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___1094EE94F079EDF296E553F323D5FAE1D4595CA2_0)); }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  get_U31094EE94F079EDF296E553F323D5FAE1D4595CA2_0() const { return ___1094EE94F079EDF296E553F323D5FAE1D4595CA2_0; }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315 * get_address_of_U31094EE94F079EDF296E553F323D5FAE1D4595CA2_0() { return &___1094EE94F079EDF296E553F323D5FAE1D4595CA2_0; }
	inline void set_U31094EE94F079EDF296E553F323D5FAE1D4595CA2_0(__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  value)
	{
		___1094EE94F079EDF296E553F323D5FAE1D4595CA2_0 = value;
	}

	inline static int32_t get_offset_of_F1AA51837B0CB2B8735A7F2F663CED5FC0947D72_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___F1AA51837B0CB2B8735A7F2F663CED5FC0947D72_1)); }
	inline __StaticArrayInitTypeSizeU3D6_t962A4BB0EB2726A97333CF6FA69780E6CF9B3690  get_F1AA51837B0CB2B8735A7F2F663CED5FC0947D72_1() const { return ___F1AA51837B0CB2B8735A7F2F663CED5FC0947D72_1; }
	inline __StaticArrayInitTypeSizeU3D6_t962A4BB0EB2726A97333CF6FA69780E6CF9B3690 * get_address_of_F1AA51837B0CB2B8735A7F2F663CED5FC0947D72_1() { return &___F1AA51837B0CB2B8735A7F2F663CED5FC0947D72_1; }
	inline void set_F1AA51837B0CB2B8735A7F2F663CED5FC0947D72_1(__StaticArrayInitTypeSizeU3D6_t962A4BB0EB2726A97333CF6FA69780E6CF9B3690  value)
	{
		___F1AA51837B0CB2B8735A7F2F663CED5FC0947D72_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef VIDEOSOURCE_T32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A_H
#define VIDEOSOURCE_T32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoSource
struct  VideoSource_t32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A 
{
public:
	// System.Int32 UnityEngine.Video.VideoSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoSource_t32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOSOURCE_T32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef GALLERYASSETSCRIPT_T07AF2CEFA118045AF8E74999649112034986965B_H
#define GALLERYASSETSCRIPT_T07AF2CEFA118045AF8E74999649112034986965B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.IAssets.GalleryAssetScript
struct  GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject[] MPAR.Common.AssetLoader.IAssets.GalleryAssetScript::<ImageObjects>k__BackingField
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___U3CImageObjectsU3Ek__BackingField_4;
	// UnityEngine.Vector3 MPAR.Common.AssetLoader.IAssets.GalleryAssetScript::initialScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___initialScale_5;
	// UnityEngine.Transform[] MPAR.Common.AssetLoader.IAssets.GalleryAssetScript::<Positions>k__BackingField
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* ___U3CPositionsU3Ek__BackingField_6;
	// UnityEngine.Texture2D[] MPAR.Common.AssetLoader.IAssets.GalleryAssetScript::<Images>k__BackingField
	Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* ___U3CImagesU3Ek__BackingField_7;
	// System.Int32 MPAR.Common.AssetLoader.IAssets.GalleryAssetScript::currentImage
	int32_t ___currentImage_8;
	// UnityEngine.Vector3 MPAR.Common.AssetLoader.IAssets.GalleryAssetScript::originalScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___originalScale_9;
	// System.Boolean MPAR.Common.AssetLoader.IAssets.GalleryAssetScript::isManipulating
	bool ___isManipulating_10;
	// System.Boolean MPAR.Common.AssetLoader.IAssets.GalleryAssetScript::swiped
	bool ___swiped_11;
	// UnityEngine.Vector3 MPAR.Common.AssetLoader.IAssets.GalleryAssetScript::startHandPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startHandPosition_12;
	// UnityEngine.Vector3 MPAR.Common.AssetLoader.IAssets.GalleryAssetScript::moveVelocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___moveVelocity_13;
	// MPAR.Common.Input.Utility.GestureManager MPAR.Common.AssetLoader.IAssets.GalleryAssetScript::Gestures
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * ___Gestures_14;

public:
	inline static int32_t get_offset_of_U3CImageObjectsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B, ___U3CImageObjectsU3Ek__BackingField_4)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_U3CImageObjectsU3Ek__BackingField_4() const { return ___U3CImageObjectsU3Ek__BackingField_4; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_U3CImageObjectsU3Ek__BackingField_4() { return &___U3CImageObjectsU3Ek__BackingField_4; }
	inline void set_U3CImageObjectsU3Ek__BackingField_4(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___U3CImageObjectsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageObjectsU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_initialScale_5() { return static_cast<int32_t>(offsetof(GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B, ___initialScale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_initialScale_5() const { return ___initialScale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_initialScale_5() { return &___initialScale_5; }
	inline void set_initialScale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___initialScale_5 = value;
	}

	inline static int32_t get_offset_of_U3CPositionsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B, ___U3CPositionsU3Ek__BackingField_6)); }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* get_U3CPositionsU3Ek__BackingField_6() const { return ___U3CPositionsU3Ek__BackingField_6; }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804** get_address_of_U3CPositionsU3Ek__BackingField_6() { return &___U3CPositionsU3Ek__BackingField_6; }
	inline void set_U3CPositionsU3Ek__BackingField_6(TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* value)
	{
		___U3CPositionsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPositionsU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CImagesU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B, ___U3CImagesU3Ek__BackingField_7)); }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* get_U3CImagesU3Ek__BackingField_7() const { return ___U3CImagesU3Ek__BackingField_7; }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9** get_address_of_U3CImagesU3Ek__BackingField_7() { return &___U3CImagesU3Ek__BackingField_7; }
	inline void set_U3CImagesU3Ek__BackingField_7(Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* value)
	{
		___U3CImagesU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImagesU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_currentImage_8() { return static_cast<int32_t>(offsetof(GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B, ___currentImage_8)); }
	inline int32_t get_currentImage_8() const { return ___currentImage_8; }
	inline int32_t* get_address_of_currentImage_8() { return &___currentImage_8; }
	inline void set_currentImage_8(int32_t value)
	{
		___currentImage_8 = value;
	}

	inline static int32_t get_offset_of_originalScale_9() { return static_cast<int32_t>(offsetof(GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B, ___originalScale_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_originalScale_9() const { return ___originalScale_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_originalScale_9() { return &___originalScale_9; }
	inline void set_originalScale_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___originalScale_9 = value;
	}

	inline static int32_t get_offset_of_isManipulating_10() { return static_cast<int32_t>(offsetof(GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B, ___isManipulating_10)); }
	inline bool get_isManipulating_10() const { return ___isManipulating_10; }
	inline bool* get_address_of_isManipulating_10() { return &___isManipulating_10; }
	inline void set_isManipulating_10(bool value)
	{
		___isManipulating_10 = value;
	}

	inline static int32_t get_offset_of_swiped_11() { return static_cast<int32_t>(offsetof(GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B, ___swiped_11)); }
	inline bool get_swiped_11() const { return ___swiped_11; }
	inline bool* get_address_of_swiped_11() { return &___swiped_11; }
	inline void set_swiped_11(bool value)
	{
		___swiped_11 = value;
	}

	inline static int32_t get_offset_of_startHandPosition_12() { return static_cast<int32_t>(offsetof(GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B, ___startHandPosition_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startHandPosition_12() const { return ___startHandPosition_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startHandPosition_12() { return &___startHandPosition_12; }
	inline void set_startHandPosition_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startHandPosition_12 = value;
	}

	inline static int32_t get_offset_of_moveVelocity_13() { return static_cast<int32_t>(offsetof(GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B, ___moveVelocity_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_moveVelocity_13() const { return ___moveVelocity_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_moveVelocity_13() { return &___moveVelocity_13; }
	inline void set_moveVelocity_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___moveVelocity_13 = value;
	}

	inline static int32_t get_offset_of_Gestures_14() { return static_cast<int32_t>(offsetof(GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B, ___Gestures_14)); }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * get_Gestures_14() const { return ___Gestures_14; }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 ** get_address_of_Gestures_14() { return &___Gestures_14; }
	inline void set_Gestures_14(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * value)
	{
		___Gestures_14 = value;
		Il2CppCodeGenWriteBarrier((&___Gestures_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GALLERYASSETSCRIPT_T07AF2CEFA118045AF8E74999649112034986965B_H
#ifndef VIDEOOPTIONS_T3A1836F9B559300F8A3E125FAC35EFAEE9D35662_H
#define VIDEOOPTIONS_T3A1836F9B559300F8A3E125FAC35EFAEE9D35662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.VideoOptions
struct  VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Video.VideoPlayer MPAR.Common.AssetLoader.VideoOptions::videoPlayer
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * ___videoPlayer_4;
	// UnityEngine.Video.VideoSource MPAR.Common.AssetLoader.VideoOptions::videoSource
	int32_t ___videoSource_5;
	// UnityEngine.AudioSource MPAR.Common.AssetLoader.VideoOptions::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_6;
	// System.Boolean MPAR.Common.AssetLoader.VideoOptions::justPlayedOrPaused
	bool ___justPlayedOrPaused_7;
	// System.Boolean MPAR.Common.AssetLoader.VideoOptions::<IsPlaying>k__BackingField
	bool ___U3CIsPlayingU3Ek__BackingField_8;
	// System.Boolean MPAR.Common.AssetLoader.VideoOptions::<ReadyToPlay>k__BackingField
	bool ___U3CReadyToPlayU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_videoPlayer_4() { return static_cast<int32_t>(offsetof(VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662, ___videoPlayer_4)); }
	inline VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * get_videoPlayer_4() const { return ___videoPlayer_4; }
	inline VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 ** get_address_of_videoPlayer_4() { return &___videoPlayer_4; }
	inline void set_videoPlayer_4(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * value)
	{
		___videoPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayer_4), value);
	}

	inline static int32_t get_offset_of_videoSource_5() { return static_cast<int32_t>(offsetof(VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662, ___videoSource_5)); }
	inline int32_t get_videoSource_5() const { return ___videoSource_5; }
	inline int32_t* get_address_of_videoSource_5() { return &___videoSource_5; }
	inline void set_videoSource_5(int32_t value)
	{
		___videoSource_5 = value;
	}

	inline static int32_t get_offset_of_audioSource_6() { return static_cast<int32_t>(offsetof(VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662, ___audioSource_6)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_6() const { return ___audioSource_6; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_6() { return &___audioSource_6; }
	inline void set_audioSource_6(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_6 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_6), value);
	}

	inline static int32_t get_offset_of_justPlayedOrPaused_7() { return static_cast<int32_t>(offsetof(VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662, ___justPlayedOrPaused_7)); }
	inline bool get_justPlayedOrPaused_7() const { return ___justPlayedOrPaused_7; }
	inline bool* get_address_of_justPlayedOrPaused_7() { return &___justPlayedOrPaused_7; }
	inline void set_justPlayedOrPaused_7(bool value)
	{
		___justPlayedOrPaused_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsPlayingU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662, ___U3CIsPlayingU3Ek__BackingField_8)); }
	inline bool get_U3CIsPlayingU3Ek__BackingField_8() const { return ___U3CIsPlayingU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsPlayingU3Ek__BackingField_8() { return &___U3CIsPlayingU3Ek__BackingField_8; }
	inline void set_U3CIsPlayingU3Ek__BackingField_8(bool value)
	{
		___U3CIsPlayingU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CReadyToPlayU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662, ___U3CReadyToPlayU3Ek__BackingField_9)); }
	inline bool get_U3CReadyToPlayU3Ek__BackingField_9() const { return ___U3CReadyToPlayU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CReadyToPlayU3Ek__BackingField_9() { return &___U3CReadyToPlayU3Ek__BackingField_9; }
	inline void set_U3CReadyToPlayU3Ek__BackingField_9(bool value)
	{
		___U3CReadyToPlayU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOOPTIONS_T3A1836F9B559300F8A3E125FAC35EFAEE9D35662_H
#ifndef VIDEOPROGRESSDRAGGER_T5071237DD115D69BDE336BBDF806CABED58243D6_H
#define VIDEOPROGRESSDRAGGER_T5071237DD115D69BDE336BBDF806CABED58243D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.VideoProgressDragger
struct  VideoProgressDragger_t5071237DD115D69BDE336BBDF806CABED58243D6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MPAR.Common.AssetLoader.VideoControls MPAR.Common.AssetLoader.VideoProgressDragger::videoControl
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE * ___videoControl_4;
	// UnityEngine.Color MPAR.Common.AssetLoader.VideoProgressDragger::highlightColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___highlightColor_5;
	// UnityEngine.Color MPAR.Common.AssetLoader.VideoProgressDragger::originalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___originalColor_6;
	// System.Boolean MPAR.Common.AssetLoader.VideoProgressDragger::isManipulating
	bool ___isManipulating_7;
	// UnityEngine.Vector3 MPAR.Common.AssetLoader.VideoProgressDragger::startHandPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startHandPosition_8;
	// MPAR.Common.Input.Utility.GestureManager MPAR.Common.AssetLoader.VideoProgressDragger::Gestures
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * ___Gestures_9;

public:
	inline static int32_t get_offset_of_videoControl_4() { return static_cast<int32_t>(offsetof(VideoProgressDragger_t5071237DD115D69BDE336BBDF806CABED58243D6, ___videoControl_4)); }
	inline VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE * get_videoControl_4() const { return ___videoControl_4; }
	inline VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE ** get_address_of_videoControl_4() { return &___videoControl_4; }
	inline void set_videoControl_4(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE * value)
	{
		___videoControl_4 = value;
		Il2CppCodeGenWriteBarrier((&___videoControl_4), value);
	}

	inline static int32_t get_offset_of_highlightColor_5() { return static_cast<int32_t>(offsetof(VideoProgressDragger_t5071237DD115D69BDE336BBDF806CABED58243D6, ___highlightColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_highlightColor_5() const { return ___highlightColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_highlightColor_5() { return &___highlightColor_5; }
	inline void set_highlightColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___highlightColor_5 = value;
	}

	inline static int32_t get_offset_of_originalColor_6() { return static_cast<int32_t>(offsetof(VideoProgressDragger_t5071237DD115D69BDE336BBDF806CABED58243D6, ___originalColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_originalColor_6() const { return ___originalColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_originalColor_6() { return &___originalColor_6; }
	inline void set_originalColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___originalColor_6 = value;
	}

	inline static int32_t get_offset_of_isManipulating_7() { return static_cast<int32_t>(offsetof(VideoProgressDragger_t5071237DD115D69BDE336BBDF806CABED58243D6, ___isManipulating_7)); }
	inline bool get_isManipulating_7() const { return ___isManipulating_7; }
	inline bool* get_address_of_isManipulating_7() { return &___isManipulating_7; }
	inline void set_isManipulating_7(bool value)
	{
		___isManipulating_7 = value;
	}

	inline static int32_t get_offset_of_startHandPosition_8() { return static_cast<int32_t>(offsetof(VideoProgressDragger_t5071237DD115D69BDE336BBDF806CABED58243D6, ___startHandPosition_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startHandPosition_8() const { return ___startHandPosition_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startHandPosition_8() { return &___startHandPosition_8; }
	inline void set_startHandPosition_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startHandPosition_8 = value;
	}

	inline static int32_t get_offset_of_Gestures_9() { return static_cast<int32_t>(offsetof(VideoProgressDragger_t5071237DD115D69BDE336BBDF806CABED58243D6, ___Gestures_9)); }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * get_Gestures_9() const { return ___Gestures_9; }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 ** get_address_of_Gestures_9() { return &___Gestures_9; }
	inline void set_Gestures_9(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * value)
	{
		___Gestures_9 = value;
		Il2CppCodeGenWriteBarrier((&___Gestures_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOPROGRESSDRAGGER_T5071237DD115D69BDE336BBDF806CABED58243D6_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8200 = { sizeof (U3CU3Ec__DisplayClass33_0_t2F2EC482EA2D9A093DBF1D936950BD5DE10E1FCB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8200[3] = 
{
	U3CU3Ec__DisplayClass33_0_t2F2EC482EA2D9A093DBF1D936950BD5DE10E1FCB::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass33_0_t2F2EC482EA2D9A093DBF1D936950BD5DE10E1FCB::get_offset_of_pauseButton_1(),
	U3CU3Ec__DisplayClass33_0_t2F2EC482EA2D9A093DBF1D936950BD5DE10E1FCB::get_offset_of_playButton_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8201 = { sizeof (VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8201[6] = 
{
	VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662::get_offset_of_videoPlayer_4(),
	VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662::get_offset_of_videoSource_5(),
	VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662::get_offset_of_audioSource_6(),
	VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662::get_offset_of_justPlayedOrPaused_7(),
	VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662::get_offset_of_U3CIsPlayingU3Ek__BackingField_8(),
	VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662::get_offset_of_U3CReadyToPlayU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8202 = { sizeof (U3CLoadFirstFrameU3Ed__16_tD8D4631475B1ABDF9129346CF738C44D0295AF21), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8202[3] = 
{
	U3CLoadFirstFrameU3Ed__16_tD8D4631475B1ABDF9129346CF738C44D0295AF21::get_offset_of_U3CU3E1__state_0(),
	U3CLoadFirstFrameU3Ed__16_tD8D4631475B1ABDF9129346CF738C44D0295AF21::get_offset_of_U3CU3E2__current_1(),
	U3CLoadFirstFrameU3Ed__16_tD8D4631475B1ABDF9129346CF738C44D0295AF21::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8203 = { sizeof (VideoProgressDragger_t5071237DD115D69BDE336BBDF806CABED58243D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8203[6] = 
{
	VideoProgressDragger_t5071237DD115D69BDE336BBDF806CABED58243D6::get_offset_of_videoControl_4(),
	VideoProgressDragger_t5071237DD115D69BDE336BBDF806CABED58243D6::get_offset_of_highlightColor_5(),
	VideoProgressDragger_t5071237DD115D69BDE336BBDF806CABED58243D6::get_offset_of_originalColor_6(),
	VideoProgressDragger_t5071237DD115D69BDE336BBDF806CABED58243D6::get_offset_of_isManipulating_7(),
	VideoProgressDragger_t5071237DD115D69BDE336BBDF806CABED58243D6::get_offset_of_startHandPosition_8(),
	VideoProgressDragger_t5071237DD115D69BDE336BBDF806CABED58243D6::get_offset_of_Gestures_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8204 = { sizeof (GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8204[11] = 
{
	GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B::get_offset_of_U3CImageObjectsU3Ek__BackingField_4(),
	GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B::get_offset_of_initialScale_5(),
	GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B::get_offset_of_U3CPositionsU3Ek__BackingField_6(),
	GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B::get_offset_of_U3CImagesU3Ek__BackingField_7(),
	GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B::get_offset_of_currentImage_8(),
	GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B::get_offset_of_originalScale_9(),
	GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B::get_offset_of_isManipulating_10(),
	GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B::get_offset_of_swiped_11(),
	GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B::get_offset_of_startHandPosition_12(),
	GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B::get_offset_of_moveVelocity_13(),
	GalleryAssetScript_t07AF2CEFA118045AF8E74999649112034986965B::get_offset_of_Gestures_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8205 = { sizeof (U3CU3Ec__DisplayClass22_0_t5D67ABB60CD9310E8EEE68BD813524D8DE403452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8205[2] = 
{
	U3CU3Ec__DisplayClass22_0_t5D67ABB60CD9310E8EEE68BD813524D8DE403452::get_offset_of_rotations_0(),
	U3CU3Ec__DisplayClass22_0_t5D67ABB60CD9310E8EEE68BD813524D8DE403452::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8206 = { sizeof (U3CU3Ec_t918A47AC82D266C2DE7DF9FF63E64936A81C9667), -1, sizeof(U3CU3Ec_t918A47AC82D266C2DE7DF9FF63E64936A81C9667_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8206[2] = 
{
	U3CU3Ec_t918A47AC82D266C2DE7DF9FF63E64936A81C9667_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t918A47AC82D266C2DE7DF9FF63E64936A81C9667_StaticFields::get_offset_of_U3CU3E9__23_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8207 = { sizeof (U3CU3Ec__DisplayClass24_0_t2960E9FE7D5062420874CDD71AA392B3BD115E0D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8207[1] = 
{
	U3CU3Ec__DisplayClass24_0_t2960E9FE7D5062420874CDD71AA392B3BD115E0D::get_offset_of_hitObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8208 = { sizeof (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A), -1, sizeof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8208[2] = 
{
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U31094EE94F079EDF296E553F323D5FAE1D4595CA2_0(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_F1AA51837B0CB2B8735A7F2F663CED5FC0947D72_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8209 = { sizeof (__StaticArrayInitTypeSizeU3D6_t962A4BB0EB2726A97333CF6FA69780E6CF9B3690)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D6_t962A4BB0EB2726A97333CF6FA69780E6CF9B3690 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8210 = { sizeof (__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
