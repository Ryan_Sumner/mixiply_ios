﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.WeakReference>>
struct Dictionary_2_t25A7A14A5E5806EFC2762FF3B5AB06BAA4176D63;
// System.Collections.Generic.Dictionary`2<System.Linq.Expressions.ParameterExpression,System.Linq.Expressions.Interpreter.LocalVariable>
struct Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2;
// System.Collections.Generic.Dictionary`2<System.Type,System.Linq.Expressions.Compiler.DelegateHelpers/TypeInfo>
struct Dictionary_2_t640F20F362F56F6E2DB51C0E414689E968608D38;
// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
struct Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7;
// System.Collections.Generic.HashSet`1<System.Dynamic.BindingRestrictions>
struct HashSet_1_t70C479833447FE1D2D8841EC46FC5CB6F27117F9;
// System.Collections.Generic.Stack`1<System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>>
struct Stack_1_t60D719A2274AB28AA2E53FC3CCEE3CE8E41D8ECF;
// System.Collections.Generic.Stack`1<System.Dynamic.BindingRestrictions/TestBuilder/AndNode>
struct Stack_1_tD97CBF006DB330756FDF72BE1B5498D01364A831;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.String>
struct ReadOnlyCollection_1_tB9E469CEA1A95F21BDF5C8594323E208E5454BE0;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82;
// System.Dynamic.BindingRestrictions
struct BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3;
// System.Dynamic.DynamicMetaObject[]
struct DynamicMetaObjectU5BU5D_tB1A47565ACD3389A26C28E7244696F579A5B6049;
// System.Dynamic.ExpandoObject
struct ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88;
// System.Dynamic.ExpandoObject/ExpandoData
struct ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F;
// System.Dynamic.ExpandoObject/KeyCollection
struct KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230;
// System.Dynamic.Utils.CacheDict`2<System.Type,System.Func`2<System.Runtime.CompilerServices.CallSiteBinder,System.Runtime.CompilerServices.CallSite>>
struct CacheDict_2_t53E47CA277D18C28DB76A96F131A833431A67EEE;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Linq.Expressions.Compiler.DelegateHelpers/TypeInfo
struct TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447;
// System.Linq.Expressions.Expression
struct Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F;
// System.Linq.Expressions.Interpreter.Instruction
struct Instruction_t235F1D5246CE88164576679572E0E858988436C3;
// System.Linq.Expressions.Interpreter.InterpretedFrame
struct InterpretedFrame_t9A6E1D0F21363A3BF21977829F9C3D88E5CF0A90;
// System.Linq.Expressions.Interpreter.LightDelegateCreator
struct LightDelegateCreator_t8B2B6E125B98DB397DDF08153B3FD2E34C7FF132;
// System.Linq.Expressions.LabelTarget
struct LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.CompilerServices.CallSiteBinder
struct CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889;
// System.Runtime.CompilerServices.IRuntimeVariables
struct IRuntimeVariables_tDEAB5A363710C8201323E7CD24EDD7B4275D002D;
// System.Runtime.CompilerServices.IStrongBox[]
struct IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Type
struct Type_t;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef BINDINGRESTRICTIONS_T06EF96C7F15082002E16B09A0398D552E55727D3_H
#define BINDINGRESTRICTIONS_T06EF96C7F15082002E16B09A0398D552E55727D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions
struct  BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3  : public RuntimeObject
{
public:

public:
};

struct BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3_StaticFields
{
public:
	// System.Dynamic.BindingRestrictions System.Dynamic.BindingRestrictions::Empty
	BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3_StaticFields, ___Empty_0)); }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * get_Empty_0() const { return ___Empty_0; }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGRESTRICTIONS_T06EF96C7F15082002E16B09A0398D552E55727D3_H
#ifndef BINDINGRESTRICTIONSPROXY_TDEC6CC1C0B17D4B5055F3C2F6B10E1469EF452E1_H
#define BINDINGRESTRICTIONSPROXY_TDEC6CC1C0B17D4B5055F3C2F6B10E1469EF452E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions/BindingRestrictionsProxy
struct  BindingRestrictionsProxy_tDEC6CC1C0B17D4B5055F3C2F6B10E1469EF452E1  : public RuntimeObject
{
public:
	// System.Dynamic.BindingRestrictions System.Dynamic.BindingRestrictions/BindingRestrictionsProxy::_node
	BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * ____node_0;

public:
	inline static int32_t get_offset_of__node_0() { return static_cast<int32_t>(offsetof(BindingRestrictionsProxy_tDEC6CC1C0B17D4B5055F3C2F6B10E1469EF452E1, ____node_0)); }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * get__node_0() const { return ____node_0; }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 ** get_address_of__node_0() { return &____node_0; }
	inline void set__node_0(BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * value)
	{
		____node_0 = value;
		Il2CppCodeGenWriteBarrier((&____node_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGRESTRICTIONSPROXY_TDEC6CC1C0B17D4B5055F3C2F6B10E1469EF452E1_H
#ifndef TESTBUILDER_TC1A8F66697AB4E6E5506DE4ED0E07A94961551E8_H
#define TESTBUILDER_TC1A8F66697AB4E6E5506DE4ED0E07A94961551E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions/TestBuilder
struct  TestBuilder_tC1A8F66697AB4E6E5506DE4ED0E07A94961551E8  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<System.Dynamic.BindingRestrictions> System.Dynamic.BindingRestrictions/TestBuilder::_unique
	HashSet_1_t70C479833447FE1D2D8841EC46FC5CB6F27117F9 * ____unique_0;
	// System.Collections.Generic.Stack`1<System.Dynamic.BindingRestrictions/TestBuilder/AndNode> System.Dynamic.BindingRestrictions/TestBuilder::_tests
	Stack_1_tD97CBF006DB330756FDF72BE1B5498D01364A831 * ____tests_1;

public:
	inline static int32_t get_offset_of__unique_0() { return static_cast<int32_t>(offsetof(TestBuilder_tC1A8F66697AB4E6E5506DE4ED0E07A94961551E8, ____unique_0)); }
	inline HashSet_1_t70C479833447FE1D2D8841EC46FC5CB6F27117F9 * get__unique_0() const { return ____unique_0; }
	inline HashSet_1_t70C479833447FE1D2D8841EC46FC5CB6F27117F9 ** get_address_of__unique_0() { return &____unique_0; }
	inline void set__unique_0(HashSet_1_t70C479833447FE1D2D8841EC46FC5CB6F27117F9 * value)
	{
		____unique_0 = value;
		Il2CppCodeGenWriteBarrier((&____unique_0), value);
	}

	inline static int32_t get_offset_of__tests_1() { return static_cast<int32_t>(offsetof(TestBuilder_tC1A8F66697AB4E6E5506DE4ED0E07A94961551E8, ____tests_1)); }
	inline Stack_1_tD97CBF006DB330756FDF72BE1B5498D01364A831 * get__tests_1() const { return ____tests_1; }
	inline Stack_1_tD97CBF006DB330756FDF72BE1B5498D01364A831 ** get_address_of__tests_1() { return &____tests_1; }
	inline void set__tests_1(Stack_1_tD97CBF006DB330756FDF72BE1B5498D01364A831 * value)
	{
		____tests_1 = value;
		Il2CppCodeGenWriteBarrier((&____tests_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTBUILDER_TC1A8F66697AB4E6E5506DE4ED0E07A94961551E8_H
#ifndef CALLINFO_T8F6F2558ACDFC5BA2F5D53E25B18A11586479935_H
#define CALLINFO_T8F6F2558ACDFC5BA2F5D53E25B18A11586479935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.CallInfo
struct  CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935  : public RuntimeObject
{
public:
	// System.Int32 System.Dynamic.CallInfo::<ArgumentCount>k__BackingField
	int32_t ___U3CArgumentCountU3Ek__BackingField_0;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.String> System.Dynamic.CallInfo::<ArgumentNames>k__BackingField
	ReadOnlyCollection_1_tB9E469CEA1A95F21BDF5C8594323E208E5454BE0 * ___U3CArgumentNamesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CArgumentCountU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935, ___U3CArgumentCountU3Ek__BackingField_0)); }
	inline int32_t get_U3CArgumentCountU3Ek__BackingField_0() const { return ___U3CArgumentCountU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CArgumentCountU3Ek__BackingField_0() { return &___U3CArgumentCountU3Ek__BackingField_0; }
	inline void set_U3CArgumentCountU3Ek__BackingField_0(int32_t value)
	{
		___U3CArgumentCountU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CArgumentNamesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935, ___U3CArgumentNamesU3Ek__BackingField_1)); }
	inline ReadOnlyCollection_1_tB9E469CEA1A95F21BDF5C8594323E208E5454BE0 * get_U3CArgumentNamesU3Ek__BackingField_1() const { return ___U3CArgumentNamesU3Ek__BackingField_1; }
	inline ReadOnlyCollection_1_tB9E469CEA1A95F21BDF5C8594323E208E5454BE0 ** get_address_of_U3CArgumentNamesU3Ek__BackingField_1() { return &___U3CArgumentNamesU3Ek__BackingField_1; }
	inline void set_U3CArgumentNamesU3Ek__BackingField_1(ReadOnlyCollection_1_tB9E469CEA1A95F21BDF5C8594323E208E5454BE0 * value)
	{
		___U3CArgumentNamesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgumentNamesU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLINFO_T8F6F2558ACDFC5BA2F5D53E25B18A11586479935_H
#ifndef DYNAMICMETAOBJECT_TA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41_H
#define DYNAMICMETAOBJECT_TA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.DynamicMetaObject
struct  DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41  : public RuntimeObject
{
public:
	// System.Linq.Expressions.Expression System.Dynamic.DynamicMetaObject::<Expression>k__BackingField
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___U3CExpressionU3Ek__BackingField_1;
	// System.Dynamic.BindingRestrictions System.Dynamic.DynamicMetaObject::<Restrictions>k__BackingField
	BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * ___U3CRestrictionsU3Ek__BackingField_2;
	// System.Object System.Dynamic.DynamicMetaObject::<Value>k__BackingField
	RuntimeObject * ___U3CValueU3Ek__BackingField_3;
	// System.Boolean System.Dynamic.DynamicMetaObject::<HasValue>k__BackingField
	bool ___U3CHasValueU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CExpressionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41, ___U3CExpressionU3Ek__BackingField_1)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get_U3CExpressionU3Ek__BackingField_1() const { return ___U3CExpressionU3Ek__BackingField_1; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of_U3CExpressionU3Ek__BackingField_1() { return &___U3CExpressionU3Ek__BackingField_1; }
	inline void set_U3CExpressionU3Ek__BackingField_1(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		___U3CExpressionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExpressionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CRestrictionsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41, ___U3CRestrictionsU3Ek__BackingField_2)); }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * get_U3CRestrictionsU3Ek__BackingField_2() const { return ___U3CRestrictionsU3Ek__BackingField_2; }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 ** get_address_of_U3CRestrictionsU3Ek__BackingField_2() { return &___U3CRestrictionsU3Ek__BackingField_2; }
	inline void set_U3CRestrictionsU3Ek__BackingField_2(BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * value)
	{
		___U3CRestrictionsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRestrictionsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41, ___U3CValueU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CValueU3Ek__BackingField_3() const { return ___U3CValueU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CValueU3Ek__BackingField_3() { return &___U3CValueU3Ek__BackingField_3; }
	inline void set_U3CValueU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CValueU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CHasValueU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41, ___U3CHasValueU3Ek__BackingField_4)); }
	inline bool get_U3CHasValueU3Ek__BackingField_4() const { return ___U3CHasValueU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CHasValueU3Ek__BackingField_4() { return &___U3CHasValueU3Ek__BackingField_4; }
	inline void set_U3CHasValueU3Ek__BackingField_4(bool value)
	{
		___U3CHasValueU3Ek__BackingField_4 = value;
	}
};

struct DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41_StaticFields
{
public:
	// System.Dynamic.DynamicMetaObject[] System.Dynamic.DynamicMetaObject::EmptyMetaObjects
	DynamicMetaObjectU5BU5D_tB1A47565ACD3389A26C28E7244696F579A5B6049* ___EmptyMetaObjects_0;

public:
	inline static int32_t get_offset_of_EmptyMetaObjects_0() { return static_cast<int32_t>(offsetof(DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41_StaticFields, ___EmptyMetaObjects_0)); }
	inline DynamicMetaObjectU5BU5D_tB1A47565ACD3389A26C28E7244696F579A5B6049* get_EmptyMetaObjects_0() const { return ___EmptyMetaObjects_0; }
	inline DynamicMetaObjectU5BU5D_tB1A47565ACD3389A26C28E7244696F579A5B6049** get_address_of_EmptyMetaObjects_0() { return &___EmptyMetaObjects_0; }
	inline void set_EmptyMetaObjects_0(DynamicMetaObjectU5BU5D_tB1A47565ACD3389A26C28E7244696F579A5B6049* value)
	{
		___EmptyMetaObjects_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyMetaObjects_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICMETAOBJECT_TA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41_H
#ifndef EXPANDOCLASS_T8CBF5EECA6EF5D2C78610254F93D8FE8495B858D_H
#define EXPANDOCLASS_T8CBF5EECA6EF5D2C78610254F93D8FE8495B858D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoClass
struct  ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D  : public RuntimeObject
{
public:
	// System.String[] System.Dynamic.ExpandoClass::_keys
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____keys_0;
	// System.Int32 System.Dynamic.ExpandoClass::_hashCode
	int32_t ____hashCode_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.WeakReference>> System.Dynamic.ExpandoClass::_transitions
	Dictionary_2_t25A7A14A5E5806EFC2762FF3B5AB06BAA4176D63 * ____transitions_2;

public:
	inline static int32_t get_offset_of__keys_0() { return static_cast<int32_t>(offsetof(ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D, ____keys_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__keys_0() const { return ____keys_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__keys_0() { return &____keys_0; }
	inline void set__keys_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____keys_0 = value;
		Il2CppCodeGenWriteBarrier((&____keys_0), value);
	}

	inline static int32_t get_offset_of__hashCode_1() { return static_cast<int32_t>(offsetof(ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D, ____hashCode_1)); }
	inline int32_t get__hashCode_1() const { return ____hashCode_1; }
	inline int32_t* get_address_of__hashCode_1() { return &____hashCode_1; }
	inline void set__hashCode_1(int32_t value)
	{
		____hashCode_1 = value;
	}

	inline static int32_t get_offset_of__transitions_2() { return static_cast<int32_t>(offsetof(ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D, ____transitions_2)); }
	inline Dictionary_2_t25A7A14A5E5806EFC2762FF3B5AB06BAA4176D63 * get__transitions_2() const { return ____transitions_2; }
	inline Dictionary_2_t25A7A14A5E5806EFC2762FF3B5AB06BAA4176D63 ** get_address_of__transitions_2() { return &____transitions_2; }
	inline void set__transitions_2(Dictionary_2_t25A7A14A5E5806EFC2762FF3B5AB06BAA4176D63 * value)
	{
		____transitions_2 = value;
		Il2CppCodeGenWriteBarrier((&____transitions_2), value);
	}
};

struct ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D_StaticFields
{
public:
	// System.Dynamic.ExpandoClass System.Dynamic.ExpandoClass::Empty
	ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D * ___Empty_3;

public:
	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D_StaticFields, ___Empty_3)); }
	inline ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D * get_Empty_3() const { return ___Empty_3; }
	inline ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D ** get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D * value)
	{
		___Empty_3 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPANDOCLASS_T8CBF5EECA6EF5D2C78610254F93D8FE8495B858D_H
#ifndef EXPANDOOBJECT_T7A8A377D09D5D161A12CB600590714DD7EA96F88_H
#define EXPANDOOBJECT_T7A8A377D09D5D161A12CB600590714DD7EA96F88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject
struct  ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88  : public RuntimeObject
{
public:
	// System.Object System.Dynamic.ExpandoObject::LockObject
	RuntimeObject * ___LockObject_5;
	// System.Dynamic.ExpandoObject/ExpandoData System.Dynamic.ExpandoObject::_data
	ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * ____data_6;
	// System.Int32 System.Dynamic.ExpandoObject::_count
	int32_t ____count_7;
	// System.ComponentModel.PropertyChangedEventHandler System.Dynamic.ExpandoObject::_propertyChanged
	PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * ____propertyChanged_9;

public:
	inline static int32_t get_offset_of_LockObject_5() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88, ___LockObject_5)); }
	inline RuntimeObject * get_LockObject_5() const { return ___LockObject_5; }
	inline RuntimeObject ** get_address_of_LockObject_5() { return &___LockObject_5; }
	inline void set_LockObject_5(RuntimeObject * value)
	{
		___LockObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___LockObject_5), value);
	}

	inline static int32_t get_offset_of__data_6() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88, ____data_6)); }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * get__data_6() const { return ____data_6; }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F ** get_address_of__data_6() { return &____data_6; }
	inline void set__data_6(ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * value)
	{
		____data_6 = value;
		Il2CppCodeGenWriteBarrier((&____data_6), value);
	}

	inline static int32_t get_offset_of__count_7() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88, ____count_7)); }
	inline int32_t get__count_7() const { return ____count_7; }
	inline int32_t* get_address_of__count_7() { return &____count_7; }
	inline void set__count_7(int32_t value)
	{
		____count_7 = value;
	}

	inline static int32_t get_offset_of__propertyChanged_9() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88, ____propertyChanged_9)); }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * get__propertyChanged_9() const { return ____propertyChanged_9; }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 ** get_address_of__propertyChanged_9() { return &____propertyChanged_9; }
	inline void set__propertyChanged_9(PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * value)
	{
		____propertyChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&____propertyChanged_9), value);
	}
};

struct ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields
{
public:
	// System.Reflection.MethodInfo System.Dynamic.ExpandoObject::ExpandoTryGetValue
	MethodInfo_t * ___ExpandoTryGetValue_0;
	// System.Reflection.MethodInfo System.Dynamic.ExpandoObject::ExpandoTrySetValue
	MethodInfo_t * ___ExpandoTrySetValue_1;
	// System.Reflection.MethodInfo System.Dynamic.ExpandoObject::ExpandoTryDeleteValue
	MethodInfo_t * ___ExpandoTryDeleteValue_2;
	// System.Reflection.MethodInfo System.Dynamic.ExpandoObject::ExpandoPromoteClass
	MethodInfo_t * ___ExpandoPromoteClass_3;
	// System.Reflection.MethodInfo System.Dynamic.ExpandoObject::ExpandoCheckVersion
	MethodInfo_t * ___ExpandoCheckVersion_4;
	// System.Object System.Dynamic.ExpandoObject::Uninitialized
	RuntimeObject * ___Uninitialized_8;

public:
	inline static int32_t get_offset_of_ExpandoTryGetValue_0() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields, ___ExpandoTryGetValue_0)); }
	inline MethodInfo_t * get_ExpandoTryGetValue_0() const { return ___ExpandoTryGetValue_0; }
	inline MethodInfo_t ** get_address_of_ExpandoTryGetValue_0() { return &___ExpandoTryGetValue_0; }
	inline void set_ExpandoTryGetValue_0(MethodInfo_t * value)
	{
		___ExpandoTryGetValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___ExpandoTryGetValue_0), value);
	}

	inline static int32_t get_offset_of_ExpandoTrySetValue_1() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields, ___ExpandoTrySetValue_1)); }
	inline MethodInfo_t * get_ExpandoTrySetValue_1() const { return ___ExpandoTrySetValue_1; }
	inline MethodInfo_t ** get_address_of_ExpandoTrySetValue_1() { return &___ExpandoTrySetValue_1; }
	inline void set_ExpandoTrySetValue_1(MethodInfo_t * value)
	{
		___ExpandoTrySetValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___ExpandoTrySetValue_1), value);
	}

	inline static int32_t get_offset_of_ExpandoTryDeleteValue_2() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields, ___ExpandoTryDeleteValue_2)); }
	inline MethodInfo_t * get_ExpandoTryDeleteValue_2() const { return ___ExpandoTryDeleteValue_2; }
	inline MethodInfo_t ** get_address_of_ExpandoTryDeleteValue_2() { return &___ExpandoTryDeleteValue_2; }
	inline void set_ExpandoTryDeleteValue_2(MethodInfo_t * value)
	{
		___ExpandoTryDeleteValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___ExpandoTryDeleteValue_2), value);
	}

	inline static int32_t get_offset_of_ExpandoPromoteClass_3() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields, ___ExpandoPromoteClass_3)); }
	inline MethodInfo_t * get_ExpandoPromoteClass_3() const { return ___ExpandoPromoteClass_3; }
	inline MethodInfo_t ** get_address_of_ExpandoPromoteClass_3() { return &___ExpandoPromoteClass_3; }
	inline void set_ExpandoPromoteClass_3(MethodInfo_t * value)
	{
		___ExpandoPromoteClass_3 = value;
		Il2CppCodeGenWriteBarrier((&___ExpandoPromoteClass_3), value);
	}

	inline static int32_t get_offset_of_ExpandoCheckVersion_4() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields, ___ExpandoCheckVersion_4)); }
	inline MethodInfo_t * get_ExpandoCheckVersion_4() const { return ___ExpandoCheckVersion_4; }
	inline MethodInfo_t ** get_address_of_ExpandoCheckVersion_4() { return &___ExpandoCheckVersion_4; }
	inline void set_ExpandoCheckVersion_4(MethodInfo_t * value)
	{
		___ExpandoCheckVersion_4 = value;
		Il2CppCodeGenWriteBarrier((&___ExpandoCheckVersion_4), value);
	}

	inline static int32_t get_offset_of_Uninitialized_8() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields, ___Uninitialized_8)); }
	inline RuntimeObject * get_Uninitialized_8() const { return ___Uninitialized_8; }
	inline RuntimeObject ** get_address_of_Uninitialized_8() { return &___Uninitialized_8; }
	inline void set_Uninitialized_8(RuntimeObject * value)
	{
		___Uninitialized_8 = value;
		Il2CppCodeGenWriteBarrier((&___Uninitialized_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPANDOOBJECT_T7A8A377D09D5D161A12CB600590714DD7EA96F88_H
#ifndef KEYCOLLECTION_T9663A8B8CF8EE07D768F82F755A4294E26A0E230_H
#define KEYCOLLECTION_T9663A8B8CF8EE07D768F82F755A4294E26A0E230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject/KeyCollection
struct  KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230  : public RuntimeObject
{
public:
	// System.Dynamic.ExpandoObject System.Dynamic.ExpandoObject/KeyCollection::_expando
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 * ____expando_0;
	// System.Int32 System.Dynamic.ExpandoObject/KeyCollection::_expandoVersion
	int32_t ____expandoVersion_1;
	// System.Int32 System.Dynamic.ExpandoObject/KeyCollection::_expandoCount
	int32_t ____expandoCount_2;
	// System.Dynamic.ExpandoObject/ExpandoData System.Dynamic.ExpandoObject/KeyCollection::_expandoData
	ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * ____expandoData_3;

public:
	inline static int32_t get_offset_of__expando_0() { return static_cast<int32_t>(offsetof(KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230, ____expando_0)); }
	inline ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 * get__expando_0() const { return ____expando_0; }
	inline ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 ** get_address_of__expando_0() { return &____expando_0; }
	inline void set__expando_0(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 * value)
	{
		____expando_0 = value;
		Il2CppCodeGenWriteBarrier((&____expando_0), value);
	}

	inline static int32_t get_offset_of__expandoVersion_1() { return static_cast<int32_t>(offsetof(KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230, ____expandoVersion_1)); }
	inline int32_t get__expandoVersion_1() const { return ____expandoVersion_1; }
	inline int32_t* get_address_of__expandoVersion_1() { return &____expandoVersion_1; }
	inline void set__expandoVersion_1(int32_t value)
	{
		____expandoVersion_1 = value;
	}

	inline static int32_t get_offset_of__expandoCount_2() { return static_cast<int32_t>(offsetof(KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230, ____expandoCount_2)); }
	inline int32_t get__expandoCount_2() const { return ____expandoCount_2; }
	inline int32_t* get_address_of__expandoCount_2() { return &____expandoCount_2; }
	inline void set__expandoCount_2(int32_t value)
	{
		____expandoCount_2 = value;
	}

	inline static int32_t get_offset_of__expandoData_3() { return static_cast<int32_t>(offsetof(KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230, ____expandoData_3)); }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * get__expandoData_3() const { return ____expandoData_3; }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F ** get_address_of__expandoData_3() { return &____expandoData_3; }
	inline void set__expandoData_3(ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * value)
	{
		____expandoData_3 = value;
		Il2CppCodeGenWriteBarrier((&____expandoData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCOLLECTION_T9663A8B8CF8EE07D768F82F755A4294E26A0E230_H
#ifndef U3CGETENUMERATORU3ED__15_T334D5404BE0EF417A3D8EFFEF2C881F9F5474775_H
#define U3CGETENUMERATORU3ED__15_T334D5404BE0EF417A3D8EFFEF2C881F9F5474775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject/KeyCollection/<GetEnumerator>d__15
struct  U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775  : public RuntimeObject
{
public:
	// System.Int32 System.Dynamic.ExpandoObject/KeyCollection/<GetEnumerator>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.String System.Dynamic.ExpandoObject/KeyCollection/<GetEnumerator>d__15::<>2__current
	String_t* ___U3CU3E2__current_1;
	// System.Dynamic.ExpandoObject/KeyCollection System.Dynamic.ExpandoObject/KeyCollection/<GetEnumerator>d__15::<>4__this
	KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230 * ___U3CU3E4__this_2;
	// System.Int32 System.Dynamic.ExpandoObject/KeyCollection/<GetEnumerator>d__15::<i>5__1
	int32_t ___U3CiU3E5__1_3;
	// System.Int32 System.Dynamic.ExpandoObject/KeyCollection/<GetEnumerator>d__15::<n>5__2
	int32_t ___U3CnU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775, ___U3CU3E2__current_1)); }
	inline String_t* get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline String_t** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(String_t* value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775, ___U3CU3E4__this_2)); }
	inline KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775, ___U3CiU3E5__1_3)); }
	inline int32_t get_U3CiU3E5__1_3() const { return ___U3CiU3E5__1_3; }
	inline int32_t* get_address_of_U3CiU3E5__1_3() { return &___U3CiU3E5__1_3; }
	inline void set_U3CiU3E5__1_3(int32_t value)
	{
		___U3CiU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CnU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775, ___U3CnU3E5__2_4)); }
	inline int32_t get_U3CnU3E5__2_4() const { return ___U3CnU3E5__2_4; }
	inline int32_t* get_address_of_U3CnU3E5__2_4() { return &___U3CnU3E5__2_4; }
	inline void set_U3CnU3E5__2_4(int32_t value)
	{
		___U3CnU3E5__2_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__15_T334D5404BE0EF417A3D8EFFEF2C881F9F5474775_H
#ifndef KEYCOLLECTIONDEBUGVIEW_T5465EA831B384616D6EDC88E8E772350A7C1DC40_H
#define KEYCOLLECTIONDEBUGVIEW_T5465EA831B384616D6EDC88E8E772350A7C1DC40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject/KeyCollectionDebugView
struct  KeyCollectionDebugView_t5465EA831B384616D6EDC88E8E772350A7C1DC40  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCOLLECTIONDEBUGVIEW_T5465EA831B384616D6EDC88E8E772350A7C1DC40_H
#ifndef DELEGATEHELPERS_TF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633_H
#define DELEGATEHELPERS_TF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Compiler.DelegateHelpers
struct  DelegateHelpers_tF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633  : public RuntimeObject
{
public:

public:
};

struct DelegateHelpers_tF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633_StaticFields
{
public:
	// System.Linq.Expressions.Compiler.DelegateHelpers/TypeInfo System.Linq.Expressions.Compiler.DelegateHelpers::_DelegateCache
	TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447 * ____DelegateCache_0;

public:
	inline static int32_t get_offset_of__DelegateCache_0() { return static_cast<int32_t>(offsetof(DelegateHelpers_tF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633_StaticFields, ____DelegateCache_0)); }
	inline TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447 * get__DelegateCache_0() const { return ____DelegateCache_0; }
	inline TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447 ** get_address_of__DelegateCache_0() { return &____DelegateCache_0; }
	inline void set__DelegateCache_0(TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447 * value)
	{
		____DelegateCache_0 = value;
		Il2CppCodeGenWriteBarrier((&____DelegateCache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEHELPERS_TF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633_H
#ifndef TYPEINFO_TACB54B7AEC49F368356339669D418251DAB0B447_H
#define TYPEINFO_TACB54B7AEC49F368356339669D418251DAB0B447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Compiler.DelegateHelpers/TypeInfo
struct  TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447  : public RuntimeObject
{
public:
	// System.Type System.Linq.Expressions.Compiler.DelegateHelpers/TypeInfo::DelegateType
	Type_t * ___DelegateType_0;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Linq.Expressions.Compiler.DelegateHelpers/TypeInfo> System.Linq.Expressions.Compiler.DelegateHelpers/TypeInfo::TypeChain
	Dictionary_2_t640F20F362F56F6E2DB51C0E414689E968608D38 * ___TypeChain_1;

public:
	inline static int32_t get_offset_of_DelegateType_0() { return static_cast<int32_t>(offsetof(TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447, ___DelegateType_0)); }
	inline Type_t * get_DelegateType_0() const { return ___DelegateType_0; }
	inline Type_t ** get_address_of_DelegateType_0() { return &___DelegateType_0; }
	inline void set_DelegateType_0(Type_t * value)
	{
		___DelegateType_0 = value;
		Il2CppCodeGenWriteBarrier((&___DelegateType_0), value);
	}

	inline static int32_t get_offset_of_TypeChain_1() { return static_cast<int32_t>(offsetof(TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447, ___TypeChain_1)); }
	inline Dictionary_2_t640F20F362F56F6E2DB51C0E414689E968608D38 * get_TypeChain_1() const { return ___TypeChain_1; }
	inline Dictionary_2_t640F20F362F56F6E2DB51C0E414689E968608D38 ** get_address_of_TypeChain_1() { return &___TypeChain_1; }
	inline void set_TypeChain_1(Dictionary_2_t640F20F362F56F6E2DB51C0E414689E968608D38 * value)
	{
		___TypeChain_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeChain_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFO_TACB54B7AEC49F368356339669D418251DAB0B447_H
#ifndef EXPRESSIONVISITOR_T2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF_H
#define EXPRESSIONVISITOR_T2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ExpressionVisitor
struct  ExpressionVisitor_t2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONVISITOR_T2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF_H
#ifndef DELEGATEHELPERS_TC220AEB31E24035C65BDC2A87576B293B189EBF5_H
#define DELEGATEHELPERS_TC220AEB31E24035C65BDC2A87576B293B189EBF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.DelegateHelpers
struct  DelegateHelpers_tC220AEB31E24035C65BDC2A87576B293B189EBF5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEHELPERS_TC220AEB31E24035C65BDC2A87576B293B189EBF5_H
#ifndef U3CU3EC_TFB9335A435065C66EE55EDA65A70B1197E95FA40_H
#define U3CU3EC_TFB9335A435065C66EE55EDA65A70B1197E95FA40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.DelegateHelpers/<>c
struct  U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.DelegateHelpers/<>c System.Linq.Expressions.Interpreter.DelegateHelpers/<>c::<>9
	U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40 * ___U3CU3E9_0;
	// System.Func`2<System.Type,System.Boolean> System.Linq.Expressions.Interpreter.DelegateHelpers/<>c::<>9__1_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TFB9335A435065C66EE55EDA65A70B1197E95FA40_H
#ifndef EXCEPTIONHELPERS_T5E8A8C882B8B390FA25BB54BF282A31251678BB9_H
#define EXCEPTIONHELPERS_T5E8A8C882B8B390FA25BB54BF282A31251678BB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.ExceptionHelpers
struct  ExceptionHelpers_t5E8A8C882B8B390FA25BB54BF282A31251678BB9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONHELPERS_T5E8A8C882B8B390FA25BB54BF282A31251678BB9_H
#ifndef INSTRUCTION_T235F1D5246CE88164576679572E0E858988436C3_H
#define INSTRUCTION_T235F1D5246CE88164576679572E0E858988436C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.Instruction
struct  Instruction_t235F1D5246CE88164576679572E0E858988436C3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTRUCTION_T235F1D5246CE88164576679572E0E858988436C3_H
#ifndef RUNTIMEVARIABLES_T80F25BDAAAD3795F8687E62A259E14D255AC4E5F_H
#define RUNTIMEVARIABLES_T80F25BDAAAD3795F8687E62A259E14D255AC4E5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RuntimeVariables
struct  RuntimeVariables_t80F25BDAAAD3795F8687E62A259E14D255AC4E5F  : public RuntimeObject
{
public:
	// System.Runtime.CompilerServices.IStrongBox[] System.Linq.Expressions.Interpreter.RuntimeVariables::_boxes
	IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27* ____boxes_0;

public:
	inline static int32_t get_offset_of__boxes_0() { return static_cast<int32_t>(offsetof(RuntimeVariables_t80F25BDAAAD3795F8687E62A259E14D255AC4E5F, ____boxes_0)); }
	inline IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27* get__boxes_0() const { return ____boxes_0; }
	inline IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27** get_address_of__boxes_0() { return &____boxes_0; }
	inline void set__boxes_0(IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27* value)
	{
		____boxes_0 = value;
		Il2CppCodeGenWriteBarrier((&____boxes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEVARIABLES_T80F25BDAAAD3795F8687E62A259E14D255AC4E5F_H
#ifndef SCRIPTINGRUNTIMEHELPERS_T5EBE625EFE604DEF42AFBCA8F9EF3A808FF38DC7_H
#define SCRIPTINGRUNTIMEHELPERS_T5EBE625EFE604DEF42AFBCA8F9EF3A808FF38DC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.ScriptingRuntimeHelpers
struct  ScriptingRuntimeHelpers_t5EBE625EFE604DEF42AFBCA8F9EF3A808FF38DC7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINGRUNTIMEHELPERS_T5EBE625EFE604DEF42AFBCA8F9EF3A808FF38DC7_H
#ifndef CALLSITE_T8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E_H
#define CALLSITE_T8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.CallSite
struct  CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E  : public RuntimeObject
{
public:
	// System.Runtime.CompilerServices.CallSiteBinder System.Runtime.CompilerServices.CallSite::_binder
	CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889 * ____binder_1;
	// System.Boolean System.Runtime.CompilerServices.CallSite::_match
	bool ____match_2;

public:
	inline static int32_t get_offset_of__binder_1() { return static_cast<int32_t>(offsetof(CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E, ____binder_1)); }
	inline CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889 * get__binder_1() const { return ____binder_1; }
	inline CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889 ** get_address_of__binder_1() { return &____binder_1; }
	inline void set__binder_1(CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889 * value)
	{
		____binder_1 = value;
		Il2CppCodeGenWriteBarrier((&____binder_1), value);
	}

	inline static int32_t get_offset_of__match_2() { return static_cast<int32_t>(offsetof(CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E, ____match_2)); }
	inline bool get__match_2() const { return ____match_2; }
	inline bool* get_address_of__match_2() { return &____match_2; }
	inline void set__match_2(bool value)
	{
		____match_2 = value;
	}
};

struct CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E_StaticFields
{
public:
	// System.Dynamic.Utils.CacheDict`2<System.Type,System.Func`2<System.Runtime.CompilerServices.CallSiteBinder,System.Runtime.CompilerServices.CallSite>> modreq(System.Runtime.CompilerServices.IsVolatile) System.Runtime.CompilerServices.CallSite::s_siteCtors
	CacheDict_2_t53E47CA277D18C28DB76A96F131A833431A67EEE * ___s_siteCtors_0;

public:
	inline static int32_t get_offset_of_s_siteCtors_0() { return static_cast<int32_t>(offsetof(CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E_StaticFields, ___s_siteCtors_0)); }
	inline CacheDict_2_t53E47CA277D18C28DB76A96F131A833431A67EEE * get_s_siteCtors_0() const { return ___s_siteCtors_0; }
	inline CacheDict_2_t53E47CA277D18C28DB76A96F131A833431A67EEE ** get_address_of_s_siteCtors_0() { return &___s_siteCtors_0; }
	inline void set_s_siteCtors_0(CacheDict_2_t53E47CA277D18C28DB76A96F131A833431A67EEE * value)
	{
		___s_siteCtors_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_siteCtors_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLSITE_T8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E_H
#ifndef CALLSITEBINDER_T76CF754DF48E3820B386E9C71F8DA13FCDD1C889_H
#define CALLSITEBINDER_T76CF754DF48E3820B386E9C71F8DA13FCDD1C889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.CallSiteBinder
struct  CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Object> System.Runtime.CompilerServices.CallSiteBinder::Cache
	Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 * ___Cache_0;

public:
	inline static int32_t get_offset_of_Cache_0() { return static_cast<int32_t>(offsetof(CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889, ___Cache_0)); }
	inline Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 * get_Cache_0() const { return ___Cache_0; }
	inline Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 ** get_address_of_Cache_0() { return &___Cache_0; }
	inline void set_Cache_0(Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 * value)
	{
		___Cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___Cache_0), value);
	}
};

struct CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889_StaticFields
{
public:
	// System.Linq.Expressions.LabelTarget System.Runtime.CompilerServices.CallSiteBinder::<UpdateLabel>k__BackingField
	LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 * ___U3CUpdateLabelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CUpdateLabelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889_StaticFields, ___U3CUpdateLabelU3Ek__BackingField_1)); }
	inline LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 * get_U3CUpdateLabelU3Ek__BackingField_1() const { return ___U3CUpdateLabelU3Ek__BackingField_1; }
	inline LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 ** get_address_of_U3CUpdateLabelU3Ek__BackingField_1() { return &___U3CUpdateLabelU3Ek__BackingField_1; }
	inline void set_U3CUpdateLabelU3Ek__BackingField_1(LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 * value)
	{
		___U3CUpdateLabelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUpdateLabelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLSITEBINDER_T76CF754DF48E3820B386E9C71F8DA13FCDD1C889_H
#ifndef CALLSITEOPS_TD92C41D19E4798EFE9D07F0C3910A0914EB231B8_H
#define CALLSITEOPS_TD92C41D19E4798EFE9D07F0C3910A0914EB231B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.CallSiteOps
struct  CallSiteOps_tD92C41D19E4798EFE9D07F0C3910A0914EB231B8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLSITEOPS_TD92C41D19E4798EFE9D07F0C3910A0914EB231B8_H
#ifndef DEBUGINFOGENERATOR_TD7E0E52E58589C53D21CDCA37EB68E89E040ECDF_H
#define DEBUGINFOGENERATOR_TD7E0E52E58589C53D21CDCA37EB68E89E040ECDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.DebugInfoGenerator
struct  DebugInfoGenerator_tD7E0E52E58589C53D21CDCA37EB68E89E040ECDF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGINFOGENERATOR_TD7E0E52E58589C53D21CDCA37EB68E89E040ECDF_H
#ifndef RUNTIMEOPS_T4AB498C75C54378274A510D5CD7DC9152A23E777_H
#define RUNTIMEOPS_T4AB498C75C54378274A510D5CD7DC9152A23E777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.RuntimeOps
struct  RuntimeOps_t4AB498C75C54378274A510D5CD7DC9152A23E777  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOPS_T4AB498C75C54378274A510D5CD7DC9152A23E777_H
#ifndef MERGEDRUNTIMEVARIABLES_T8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD_H
#define MERGEDRUNTIMEVARIABLES_T8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.RuntimeOps/MergedRuntimeVariables
struct  MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD  : public RuntimeObject
{
public:
	// System.Runtime.CompilerServices.IRuntimeVariables System.Runtime.CompilerServices.RuntimeOps/MergedRuntimeVariables::_first
	RuntimeObject* ____first_0;
	// System.Runtime.CompilerServices.IRuntimeVariables System.Runtime.CompilerServices.RuntimeOps/MergedRuntimeVariables::_second
	RuntimeObject* ____second_1;
	// System.Int32[] System.Runtime.CompilerServices.RuntimeOps/MergedRuntimeVariables::_indexes
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____indexes_2;

public:
	inline static int32_t get_offset_of__first_0() { return static_cast<int32_t>(offsetof(MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD, ____first_0)); }
	inline RuntimeObject* get__first_0() const { return ____first_0; }
	inline RuntimeObject** get_address_of__first_0() { return &____first_0; }
	inline void set__first_0(RuntimeObject* value)
	{
		____first_0 = value;
		Il2CppCodeGenWriteBarrier((&____first_0), value);
	}

	inline static int32_t get_offset_of__second_1() { return static_cast<int32_t>(offsetof(MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD, ____second_1)); }
	inline RuntimeObject* get__second_1() const { return ____second_1; }
	inline RuntimeObject** get_address_of__second_1() { return &____second_1; }
	inline void set__second_1(RuntimeObject* value)
	{
		____second_1 = value;
		Il2CppCodeGenWriteBarrier((&____second_1), value);
	}

	inline static int32_t get_offset_of__indexes_2() { return static_cast<int32_t>(offsetof(MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD, ____indexes_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__indexes_2() const { return ____indexes_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__indexes_2() { return &____indexes_2; }
	inline void set__indexes_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____indexes_2 = value;
		Il2CppCodeGenWriteBarrier((&____indexes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MERGEDRUNTIMEVARIABLES_T8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD_H
#ifndef RUNTIMEVARIABLES_T08E07CA7021C6692E793278352675C63B744D61A_H
#define RUNTIMEVARIABLES_T08E07CA7021C6692E793278352675C63B744D61A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.RuntimeOps/RuntimeVariables
struct  RuntimeVariables_t08E07CA7021C6692E793278352675C63B744D61A  : public RuntimeObject
{
public:
	// System.Runtime.CompilerServices.IStrongBox[] System.Runtime.CompilerServices.RuntimeOps/RuntimeVariables::_boxes
	IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27* ____boxes_0;

public:
	inline static int32_t get_offset_of__boxes_0() { return static_cast<int32_t>(offsetof(RuntimeVariables_t08E07CA7021C6692E793278352675C63B744D61A, ____boxes_0)); }
	inline IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27* get__boxes_0() const { return ____boxes_0; }
	inline IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27** get_address_of__boxes_0() { return &____boxes_0; }
	inline void set__boxes_0(IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27* value)
	{
		____boxes_0 = value;
		Il2CppCodeGenWriteBarrier((&____boxes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEVARIABLES_T08E07CA7021C6692E793278352675C63B744D61A_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef CUSTOMRESTRICTION_T2CF2494AA202519C8EF7D3D8E16B0C5E446926C1_H
#define CUSTOMRESTRICTION_T2CF2494AA202519C8EF7D3D8E16B0C5E446926C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions/CustomRestriction
struct  CustomRestriction_t2CF2494AA202519C8EF7D3D8E16B0C5E446926C1  : public BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3
{
public:
	// System.Linq.Expressions.Expression System.Dynamic.BindingRestrictions/CustomRestriction::_expression
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____expression_1;

public:
	inline static int32_t get_offset_of__expression_1() { return static_cast<int32_t>(offsetof(CustomRestriction_t2CF2494AA202519C8EF7D3D8E16B0C5E446926C1, ____expression_1)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__expression_1() const { return ____expression_1; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__expression_1() { return &____expression_1; }
	inline void set__expression_1(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____expression_1 = value;
		Il2CppCodeGenWriteBarrier((&____expression_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMRESTRICTION_T2CF2494AA202519C8EF7D3D8E16B0C5E446926C1_H
#ifndef INSTANCERESTRICTION_T7EFC44E326B6ED49A28CA8A1E8009F18F54AF247_H
#define INSTANCERESTRICTION_T7EFC44E326B6ED49A28CA8A1E8009F18F54AF247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions/InstanceRestriction
struct  InstanceRestriction_t7EFC44E326B6ED49A28CA8A1E8009F18F54AF247  : public BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3
{
public:
	// System.Linq.Expressions.Expression System.Dynamic.BindingRestrictions/InstanceRestriction::_expression
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____expression_1;
	// System.Object System.Dynamic.BindingRestrictions/InstanceRestriction::_instance
	RuntimeObject * ____instance_2;

public:
	inline static int32_t get_offset_of__expression_1() { return static_cast<int32_t>(offsetof(InstanceRestriction_t7EFC44E326B6ED49A28CA8A1E8009F18F54AF247, ____expression_1)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__expression_1() const { return ____expression_1; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__expression_1() { return &____expression_1; }
	inline void set__expression_1(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____expression_1 = value;
		Il2CppCodeGenWriteBarrier((&____expression_1), value);
	}

	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(InstanceRestriction_t7EFC44E326B6ED49A28CA8A1E8009F18F54AF247, ____instance_2)); }
	inline RuntimeObject * get__instance_2() const { return ____instance_2; }
	inline RuntimeObject ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(RuntimeObject * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCERESTRICTION_T7EFC44E326B6ED49A28CA8A1E8009F18F54AF247_H
#ifndef MERGEDRESTRICTION_TAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F_H
#define MERGEDRESTRICTION_TAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions/MergedRestriction
struct  MergedRestriction_tAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F  : public BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3
{
public:
	// System.Dynamic.BindingRestrictions System.Dynamic.BindingRestrictions/MergedRestriction::Left
	BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * ___Left_1;
	// System.Dynamic.BindingRestrictions System.Dynamic.BindingRestrictions/MergedRestriction::Right
	BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * ___Right_2;

public:
	inline static int32_t get_offset_of_Left_1() { return static_cast<int32_t>(offsetof(MergedRestriction_tAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F, ___Left_1)); }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * get_Left_1() const { return ___Left_1; }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 ** get_address_of_Left_1() { return &___Left_1; }
	inline void set_Left_1(BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * value)
	{
		___Left_1 = value;
		Il2CppCodeGenWriteBarrier((&___Left_1), value);
	}

	inline static int32_t get_offset_of_Right_2() { return static_cast<int32_t>(offsetof(MergedRestriction_tAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F, ___Right_2)); }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * get_Right_2() const { return ___Right_2; }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 ** get_address_of_Right_2() { return &___Right_2; }
	inline void set_Right_2(BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * value)
	{
		___Right_2 = value;
		Il2CppCodeGenWriteBarrier((&___Right_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MERGEDRESTRICTION_TAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F_H
#ifndef ANDNODE_TCC48979AF5896DA4A881EE49E182099994188916_H
#define ANDNODE_TCC48979AF5896DA4A881EE49E182099994188916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions/TestBuilder/AndNode
struct  AndNode_tCC48979AF5896DA4A881EE49E182099994188916 
{
public:
	// System.Int32 System.Dynamic.BindingRestrictions/TestBuilder/AndNode::Depth
	int32_t ___Depth_0;
	// System.Linq.Expressions.Expression System.Dynamic.BindingRestrictions/TestBuilder/AndNode::Node
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___Node_1;

public:
	inline static int32_t get_offset_of_Depth_0() { return static_cast<int32_t>(offsetof(AndNode_tCC48979AF5896DA4A881EE49E182099994188916, ___Depth_0)); }
	inline int32_t get_Depth_0() const { return ___Depth_0; }
	inline int32_t* get_address_of_Depth_0() { return &___Depth_0; }
	inline void set_Depth_0(int32_t value)
	{
		___Depth_0 = value;
	}

	inline static int32_t get_offset_of_Node_1() { return static_cast<int32_t>(offsetof(AndNode_tCC48979AF5896DA4A881EE49E182099994188916, ___Node_1)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get_Node_1() const { return ___Node_1; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of_Node_1() { return &___Node_1; }
	inline void set_Node_1(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		___Node_1 = value;
		Il2CppCodeGenWriteBarrier((&___Node_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Dynamic.BindingRestrictions/TestBuilder/AndNode
struct AndNode_tCC48979AF5896DA4A881EE49E182099994188916_marshaled_pinvoke
{
	int32_t ___Depth_0;
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___Node_1;
};
// Native definition for COM marshalling of System.Dynamic.BindingRestrictions/TestBuilder/AndNode
struct AndNode_tCC48979AF5896DA4A881EE49E182099994188916_marshaled_com
{
	int32_t ___Depth_0;
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___Node_1;
};
#endif // ANDNODE_TCC48979AF5896DA4A881EE49E182099994188916_H
#ifndef TYPERESTRICTION_T636F9773C1635EB8CDC62B6D21B18DD743221E0D_H
#define TYPERESTRICTION_T636F9773C1635EB8CDC62B6D21B18DD743221E0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions/TypeRestriction
struct  TypeRestriction_t636F9773C1635EB8CDC62B6D21B18DD743221E0D  : public BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3
{
public:
	// System.Linq.Expressions.Expression System.Dynamic.BindingRestrictions/TypeRestriction::_expression
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____expression_1;
	// System.Type System.Dynamic.BindingRestrictions/TypeRestriction::_type
	Type_t * ____type_2;

public:
	inline static int32_t get_offset_of__expression_1() { return static_cast<int32_t>(offsetof(TypeRestriction_t636F9773C1635EB8CDC62B6D21B18DD743221E0D, ____expression_1)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__expression_1() const { return ____expression_1; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__expression_1() { return &____expression_1; }
	inline void set__expression_1(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____expression_1 = value;
		Il2CppCodeGenWriteBarrier((&____expression_1), value);
	}

	inline static int32_t get_offset_of__type_2() { return static_cast<int32_t>(offsetof(TypeRestriction_t636F9773C1635EB8CDC62B6D21B18DD743221E0D, ____type_2)); }
	inline Type_t * get__type_2() const { return ____type_2; }
	inline Type_t ** get_address_of__type_2() { return &____type_2; }
	inline void set__type_2(Type_t * value)
	{
		____type_2 = value;
		Il2CppCodeGenWriteBarrier((&____type_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPERESTRICTION_T636F9773C1635EB8CDC62B6D21B18DD743221E0D_H
#ifndef DYNAMICMETAOBJECTBINDER_TE2D934F8252D47EA3F731668D13EC80EF7FD06D4_H
#define DYNAMICMETAOBJECTBINDER_TE2D934F8252D47EA3F731668D13EC80EF7FD06D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.DynamicMetaObjectBinder
struct  DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4  : public CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICMETAOBJECTBINDER_TE2D934F8252D47EA3F731668D13EC80EF7FD06D4_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef CASTINSTRUCTION_T9D7DD2D7AFA96134CE216AE247702DADA774E6BC_H
#define CASTINSTRUCTION_T9D7DD2D7AFA96134CE216AE247702DADA774E6BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastInstruction
struct  CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Boolean
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Boolean_0;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Byte
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Byte_1;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Char
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Char_2;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_DateTime
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_DateTime_3;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Decimal
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Decimal_4;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Double
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Double_5;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Int16
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Int16_6;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Int32
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Int32_7;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Int64
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Int64_8;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_SByte
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_SByte_9;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Single
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Single_10;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_String
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_String_11;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_UInt16
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_UInt16_12;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_UInt32
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_UInt32_13;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_UInt64
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_UInt64_14;

public:
	inline static int32_t get_offset_of_s_Boolean_0() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Boolean_0)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Boolean_0() const { return ___s_Boolean_0; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Boolean_0() { return &___s_Boolean_0; }
	inline void set_s_Boolean_0(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Boolean_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Boolean_0), value);
	}

	inline static int32_t get_offset_of_s_Byte_1() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Byte_1)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Byte_1() const { return ___s_Byte_1; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Byte_1() { return &___s_Byte_1; }
	inline void set_s_Byte_1(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Byte_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Byte_1), value);
	}

	inline static int32_t get_offset_of_s_Char_2() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Char_2)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Char_2() const { return ___s_Char_2; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Char_2() { return &___s_Char_2; }
	inline void set_s_Char_2(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Char_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Char_2), value);
	}

	inline static int32_t get_offset_of_s_DateTime_3() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_DateTime_3)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_DateTime_3() const { return ___s_DateTime_3; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_DateTime_3() { return &___s_DateTime_3; }
	inline void set_s_DateTime_3(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_DateTime_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_DateTime_3), value);
	}

	inline static int32_t get_offset_of_s_Decimal_4() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Decimal_4)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Decimal_4() const { return ___s_Decimal_4; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Decimal_4() { return &___s_Decimal_4; }
	inline void set_s_Decimal_4(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Decimal_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Decimal_4), value);
	}

	inline static int32_t get_offset_of_s_Double_5() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Double_5)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Double_5() const { return ___s_Double_5; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Double_5() { return &___s_Double_5; }
	inline void set_s_Double_5(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Double_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_Double_5), value);
	}

	inline static int32_t get_offset_of_s_Int16_6() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Int16_6)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Int16_6() const { return ___s_Int16_6; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Int16_6() { return &___s_Int16_6; }
	inline void set_s_Int16_6(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Int16_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int16_6), value);
	}

	inline static int32_t get_offset_of_s_Int32_7() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Int32_7)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Int32_7() const { return ___s_Int32_7; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Int32_7() { return &___s_Int32_7; }
	inline void set_s_Int32_7(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Int32_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int32_7), value);
	}

	inline static int32_t get_offset_of_s_Int64_8() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Int64_8)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Int64_8() const { return ___s_Int64_8; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Int64_8() { return &___s_Int64_8; }
	inline void set_s_Int64_8(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Int64_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int64_8), value);
	}

	inline static int32_t get_offset_of_s_SByte_9() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_SByte_9)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_SByte_9() const { return ___s_SByte_9; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_SByte_9() { return &___s_SByte_9; }
	inline void set_s_SByte_9(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_SByte_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_SByte_9), value);
	}

	inline static int32_t get_offset_of_s_Single_10() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Single_10)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Single_10() const { return ___s_Single_10; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Single_10() { return &___s_Single_10; }
	inline void set_s_Single_10(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Single_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_Single_10), value);
	}

	inline static int32_t get_offset_of_s_String_11() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_String_11)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_String_11() const { return ___s_String_11; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_String_11() { return &___s_String_11; }
	inline void set_s_String_11(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_String_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_String_11), value);
	}

	inline static int32_t get_offset_of_s_UInt16_12() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_UInt16_12)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_UInt16_12() const { return ___s_UInt16_12; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_UInt16_12() { return &___s_UInt16_12; }
	inline void set_s_UInt16_12(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_UInt16_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt16_12), value);
	}

	inline static int32_t get_offset_of_s_UInt32_13() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_UInt32_13)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_UInt32_13() const { return ___s_UInt32_13; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_UInt32_13() { return &___s_UInt32_13; }
	inline void set_s_UInt32_13(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_UInt32_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt32_13), value);
	}

	inline static int32_t get_offset_of_s_UInt64_14() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_UInt64_14)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_UInt64_14() const { return ___s_UInt64_14; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_UInt64_14() { return &___s_UInt64_14; }
	inline void set_s_UInt64_14(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_UInt64_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt64_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASTINSTRUCTION_T9D7DD2D7AFA96134CE216AE247702DADA774E6BC_H
#ifndef CREATEDELEGATEINSTRUCTION_TF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5_H
#define CREATEDELEGATEINSTRUCTION_TF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CreateDelegateInstruction
struct  CreateDelegateInstruction_tF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.Linq.Expressions.Interpreter.LightDelegateCreator System.Linq.Expressions.Interpreter.CreateDelegateInstruction::_creator
	LightDelegateCreator_t8B2B6E125B98DB397DDF08153B3FD2E34C7FF132 * ____creator_0;

public:
	inline static int32_t get_offset_of__creator_0() { return static_cast<int32_t>(offsetof(CreateDelegateInstruction_tF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5, ____creator_0)); }
	inline LightDelegateCreator_t8B2B6E125B98DB397DDF08153B3FD2E34C7FF132 * get__creator_0() const { return ____creator_0; }
	inline LightDelegateCreator_t8B2B6E125B98DB397DDF08153B3FD2E34C7FF132 ** get_address_of__creator_0() { return &____creator_0; }
	inline void set__creator_0(LightDelegateCreator_t8B2B6E125B98DB397DDF08153B3FD2E34C7FF132 * value)
	{
		____creator_0 = value;
		Il2CppCodeGenWriteBarrier((&____creator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEDELEGATEINSTRUCTION_TF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5_H
#ifndef DUPINSTRUCTION_T35C73C83F8421A299FBC38A6C9189E88F7FD2512_H
#define DUPINSTRUCTION_T35C73C83F8421A299FBC38A6C9189E88F7FD2512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.DupInstruction
struct  DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.DupInstruction System.Linq.Expressions.Interpreter.DupInstruction::Instance
	DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512_StaticFields, ___Instance_0)); }
	inline DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512 * get_Instance_0() const { return ___Instance_0; }
	inline DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUPINSTRUCTION_T35C73C83F8421A299FBC38A6C9189E88F7FD2512_H
#ifndef LOADCACHEDOBJECTINSTRUCTION_T70C07C702DAC18B1BB6BE378B9036A440B359D32_H
#define LOADCACHEDOBJECTINSTRUCTION_T70C07C702DAC18B1BB6BE378B9036A440B359D32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.LoadCachedObjectInstruction
struct  LoadCachedObjectInstruction_t70C07C702DAC18B1BB6BE378B9036A440B359D32  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.UInt32 System.Linq.Expressions.Interpreter.LoadCachedObjectInstruction::_index
	uint32_t ____index_0;

public:
	inline static int32_t get_offset_of__index_0() { return static_cast<int32_t>(offsetof(LoadCachedObjectInstruction_t70C07C702DAC18B1BB6BE378B9036A440B359D32, ____index_0)); }
	inline uint32_t get__index_0() const { return ____index_0; }
	inline uint32_t* get_address_of__index_0() { return &____index_0; }
	inline void set__index_0(uint32_t value)
	{
		____index_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADCACHEDOBJECTINSTRUCTION_T70C07C702DAC18B1BB6BE378B9036A440B359D32_H
#ifndef LOADOBJECTINSTRUCTION_T2C8D43685496B24000A7E069FDAEE73D4E72F1A7_H
#define LOADOBJECTINSTRUCTION_T2C8D43685496B24000A7E069FDAEE73D4E72F1A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.LoadObjectInstruction
struct  LoadObjectInstruction_t2C8D43685496B24000A7E069FDAEE73D4E72F1A7  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.Object System.Linq.Expressions.Interpreter.LoadObjectInstruction::_value
	RuntimeObject * ____value_0;

public:
	inline static int32_t get_offset_of__value_0() { return static_cast<int32_t>(offsetof(LoadObjectInstruction_t2C8D43685496B24000A7E069FDAEE73D4E72F1A7, ____value_0)); }
	inline RuntimeObject * get__value_0() const { return ____value_0; }
	inline RuntimeObject ** get_address_of__value_0() { return &____value_0; }
	inline void set__value_0(RuntimeObject * value)
	{
		____value_0 = value;
		Il2CppCodeGenWriteBarrier((&____value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADOBJECTINSTRUCTION_T2C8D43685496B24000A7E069FDAEE73D4E72F1A7_H
#ifndef NULLABLEMETHODCALLINSTRUCTION_TB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_H
#define NULLABLEMETHODCALLINSTRUCTION_TB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction
struct  NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_hasValue
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_hasValue_0;
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_value
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_value_1;
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_equals
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_equals_2;
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_getHashCode
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_getHashCode_3;
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_getValueOrDefault1
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_getValueOrDefault1_4;
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_toString
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_toString_5;

public:
	inline static int32_t get_offset_of_s_hasValue_0() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_hasValue_0)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_hasValue_0() const { return ___s_hasValue_0; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_hasValue_0() { return &___s_hasValue_0; }
	inline void set_s_hasValue_0(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_hasValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_hasValue_0), value);
	}

	inline static int32_t get_offset_of_s_value_1() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_value_1)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_value_1() const { return ___s_value_1; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_value_1() { return &___s_value_1; }
	inline void set_s_value_1(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_value_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_value_1), value);
	}

	inline static int32_t get_offset_of_s_equals_2() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_equals_2)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_equals_2() const { return ___s_equals_2; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_equals_2() { return &___s_equals_2; }
	inline void set_s_equals_2(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_equals_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_equals_2), value);
	}

	inline static int32_t get_offset_of_s_getHashCode_3() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_getHashCode_3)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_getHashCode_3() const { return ___s_getHashCode_3; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_getHashCode_3() { return &___s_getHashCode_3; }
	inline void set_s_getHashCode_3(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_getHashCode_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_getHashCode_3), value);
	}

	inline static int32_t get_offset_of_s_getValueOrDefault1_4() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_getValueOrDefault1_4)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_getValueOrDefault1_4() const { return ___s_getValueOrDefault1_4; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_getValueOrDefault1_4() { return &___s_getValueOrDefault1_4; }
	inline void set_s_getValueOrDefault1_4(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_getValueOrDefault1_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_getValueOrDefault1_4), value);
	}

	inline static int32_t get_offset_of_s_toString_5() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_toString_5)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_toString_5() const { return ___s_toString_5; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_toString_5() { return &___s_toString_5; }
	inline void set_s_toString_5(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_toString_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_toString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLEMETHODCALLINSTRUCTION_TB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_H
#ifndef POPINSTRUCTION_TE203CE250FAF056C574B7AAE9D6422EE7A735C52_H
#define POPINSTRUCTION_TE203CE250FAF056C574B7AAE9D6422EE7A735C52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.PopInstruction
struct  PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.PopInstruction System.Linq.Expressions.Interpreter.PopInstruction::Instance
	PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52_StaticFields, ___Instance_0)); }
	inline PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52 * get_Instance_0() const { return ___Instance_0; }
	inline PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPINSTRUCTION_TE203CE250FAF056C574B7AAE9D6422EE7A735C52_H
#ifndef QUOTEINSTRUCTION_TCC0594851C52F750FDFF3185E0001E3D60CA9C1B_H
#define QUOTEINSTRUCTION_TCC0594851C52F750FDFF3185E0001E3D60CA9C1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.QuoteInstruction
struct  QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.Linq.Expressions.Expression System.Linq.Expressions.Interpreter.QuoteInstruction::_operand
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____operand_0;
	// System.Collections.Generic.Dictionary`2<System.Linq.Expressions.ParameterExpression,System.Linq.Expressions.Interpreter.LocalVariable> System.Linq.Expressions.Interpreter.QuoteInstruction::_hoistedVariables
	Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * ____hoistedVariables_1;

public:
	inline static int32_t get_offset_of__operand_0() { return static_cast<int32_t>(offsetof(QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B, ____operand_0)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__operand_0() const { return ____operand_0; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__operand_0() { return &____operand_0; }
	inline void set__operand_0(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____operand_0 = value;
		Il2CppCodeGenWriteBarrier((&____operand_0), value);
	}

	inline static int32_t get_offset_of__hoistedVariables_1() { return static_cast<int32_t>(offsetof(QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B, ____hoistedVariables_1)); }
	inline Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * get__hoistedVariables_1() const { return ____hoistedVariables_1; }
	inline Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 ** get_address_of__hoistedVariables_1() { return &____hoistedVariables_1; }
	inline void set__hoistedVariables_1(Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * value)
	{
		____hoistedVariables_1 = value;
		Il2CppCodeGenWriteBarrier((&____hoistedVariables_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUOTEINSTRUCTION_TCC0594851C52F750FDFF3185E0001E3D60CA9C1B_H
#ifndef EXPRESSIONQUOTER_TEAE5B88BBC6EB0B78A277DE39515AF585236DC08_H
#define EXPRESSIONQUOTER_TEAE5B88BBC6EB0B78A277DE39515AF585236DC08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.QuoteInstruction/ExpressionQuoter
struct  ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08  : public ExpressionVisitor_t2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF
{
public:
	// System.Collections.Generic.Dictionary`2<System.Linq.Expressions.ParameterExpression,System.Linq.Expressions.Interpreter.LocalVariable> System.Linq.Expressions.Interpreter.QuoteInstruction/ExpressionQuoter::_variables
	Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * ____variables_0;
	// System.Linq.Expressions.Interpreter.InterpretedFrame System.Linq.Expressions.Interpreter.QuoteInstruction/ExpressionQuoter::_frame
	InterpretedFrame_t9A6E1D0F21363A3BF21977829F9C3D88E5CF0A90 * ____frame_1;
	// System.Collections.Generic.Stack`1<System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>> System.Linq.Expressions.Interpreter.QuoteInstruction/ExpressionQuoter::_shadowedVars
	Stack_1_t60D719A2274AB28AA2E53FC3CCEE3CE8E41D8ECF * ____shadowedVars_2;

public:
	inline static int32_t get_offset_of__variables_0() { return static_cast<int32_t>(offsetof(ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08, ____variables_0)); }
	inline Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * get__variables_0() const { return ____variables_0; }
	inline Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 ** get_address_of__variables_0() { return &____variables_0; }
	inline void set__variables_0(Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * value)
	{
		____variables_0 = value;
		Il2CppCodeGenWriteBarrier((&____variables_0), value);
	}

	inline static int32_t get_offset_of__frame_1() { return static_cast<int32_t>(offsetof(ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08, ____frame_1)); }
	inline InterpretedFrame_t9A6E1D0F21363A3BF21977829F9C3D88E5CF0A90 * get__frame_1() const { return ____frame_1; }
	inline InterpretedFrame_t9A6E1D0F21363A3BF21977829F9C3D88E5CF0A90 ** get_address_of__frame_1() { return &____frame_1; }
	inline void set__frame_1(InterpretedFrame_t9A6E1D0F21363A3BF21977829F9C3D88E5CF0A90 * value)
	{
		____frame_1 = value;
		Il2CppCodeGenWriteBarrier((&____frame_1), value);
	}

	inline static int32_t get_offset_of__shadowedVars_2() { return static_cast<int32_t>(offsetof(ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08, ____shadowedVars_2)); }
	inline Stack_1_t60D719A2274AB28AA2E53FC3CCEE3CE8E41D8ECF * get__shadowedVars_2() const { return ____shadowedVars_2; }
	inline Stack_1_t60D719A2274AB28AA2E53FC3CCEE3CE8E41D8ECF ** get_address_of__shadowedVars_2() { return &____shadowedVars_2; }
	inline void set__shadowedVars_2(Stack_1_t60D719A2274AB28AA2E53FC3CCEE3CE8E41D8ECF * value)
	{
		____shadowedVars_2 = value;
		Il2CppCodeGenWriteBarrier((&____shadowedVars_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONQUOTER_TEAE5B88BBC6EB0B78A277DE39515AF585236DC08_H
#ifndef RIGHTSHIFTINSTRUCTION_T5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_H
#define RIGHTSHIFTINSTRUCTION_T5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction
struct  RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_SByte
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_SByte_0;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_Int16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int16_1;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_Int32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int32_2;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_Int64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int64_3;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_Byte
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Byte_4;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_UInt16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt16_5;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_UInt32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt32_6;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_UInt64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt64_7;

public:
	inline static int32_t get_offset_of_s_SByte_0() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_SByte_0)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_SByte_0() const { return ___s_SByte_0; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_SByte_0() { return &___s_SByte_0; }
	inline void set_s_SByte_0(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_SByte_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_SByte_0), value);
	}

	inline static int32_t get_offset_of_s_Int16_1() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_Int16_1)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int16_1() const { return ___s_Int16_1; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int16_1() { return &___s_Int16_1; }
	inline void set_s_Int16_1(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int16_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int16_1), value);
	}

	inline static int32_t get_offset_of_s_Int32_2() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_Int32_2)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int32_2() const { return ___s_Int32_2; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int32_2() { return &___s_Int32_2; }
	inline void set_s_Int32_2(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int32_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int32_2), value);
	}

	inline static int32_t get_offset_of_s_Int64_3() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_Int64_3)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int64_3() const { return ___s_Int64_3; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int64_3() { return &___s_Int64_3; }
	inline void set_s_Int64_3(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int64_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int64_3), value);
	}

	inline static int32_t get_offset_of_s_Byte_4() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_Byte_4)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Byte_4() const { return ___s_Byte_4; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Byte_4() { return &___s_Byte_4; }
	inline void set_s_Byte_4(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Byte_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Byte_4), value);
	}

	inline static int32_t get_offset_of_s_UInt16_5() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_UInt16_5)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt16_5() const { return ___s_UInt16_5; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt16_5() { return &___s_UInt16_5; }
	inline void set_s_UInt16_5(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt16_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt16_5), value);
	}

	inline static int32_t get_offset_of_s_UInt32_6() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_UInt32_6)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt32_6() const { return ___s_UInt32_6; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt32_6() { return &___s_UInt32_6; }
	inline void set_s_UInt32_6(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt32_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt32_6), value);
	}

	inline static int32_t get_offset_of_s_UInt64_7() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_UInt64_7)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt64_7() const { return ___s_UInt64_7; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt64_7() { return &___s_UInt64_7; }
	inline void set_s_UInt64_7(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt64_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt64_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTINSTRUCTION_T5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_H
#ifndef SUBINSTRUCTION_T734617B532D4FE50053C3CC64466A2599DEFD3F6_H
#define SUBINSTRUCTION_T734617B532D4FE50053C3CC64466A2599DEFD3F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction
struct  SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_Int16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int16_0;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_Int32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int32_1;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_Int64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int64_2;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_UInt16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt16_3;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_UInt32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt32_4;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_UInt64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt64_5;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_Single
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Single_6;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_Double
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Double_7;

public:
	inline static int32_t get_offset_of_s_Int16_0() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_Int16_0)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int16_0() const { return ___s_Int16_0; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int16_0() { return &___s_Int16_0; }
	inline void set_s_Int16_0(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int16_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int16_0), value);
	}

	inline static int32_t get_offset_of_s_Int32_1() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_Int32_1)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int32_1() const { return ___s_Int32_1; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int32_1() { return &___s_Int32_1; }
	inline void set_s_Int32_1(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int32_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int32_1), value);
	}

	inline static int32_t get_offset_of_s_Int64_2() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_Int64_2)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int64_2() const { return ___s_Int64_2; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int64_2() { return &___s_Int64_2; }
	inline void set_s_Int64_2(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int64_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int64_2), value);
	}

	inline static int32_t get_offset_of_s_UInt16_3() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_UInt16_3)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt16_3() const { return ___s_UInt16_3; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt16_3() { return &___s_UInt16_3; }
	inline void set_s_UInt16_3(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt16_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt16_3), value);
	}

	inline static int32_t get_offset_of_s_UInt32_4() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_UInt32_4)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt32_4() const { return ___s_UInt32_4; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt32_4() { return &___s_UInt32_4; }
	inline void set_s_UInt32_4(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt32_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt32_4), value);
	}

	inline static int32_t get_offset_of_s_UInt64_5() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_UInt64_5)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt64_5() const { return ___s_UInt64_5; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt64_5() { return &___s_UInt64_5; }
	inline void set_s_UInt64_5(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt64_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt64_5), value);
	}

	inline static int32_t get_offset_of_s_Single_6() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_Single_6)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Single_6() const { return ___s_Single_6; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Single_6() { return &___s_Single_6; }
	inline void set_s_Single_6(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Single_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Single_6), value);
	}

	inline static int32_t get_offset_of_s_Double_7() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_Double_7)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Double_7() const { return ___s_Double_7; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Double_7() { return &___s_Double_7; }
	inline void set_s_Double_7(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Double_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_Double_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBINSTRUCTION_T734617B532D4FE50053C3CC64466A2599DEFD3F6_H
#ifndef SUBOVFINSTRUCTION_TC360B9BB53E1001E5636FD18D7240A1A507216C3_H
#define SUBOVFINSTRUCTION_TC360B9BB53E1001E5636FD18D7240A1A507216C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction
struct  SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_Int16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int16_0;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_Int32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int32_1;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_Int64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int64_2;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_UInt16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt16_3;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_UInt32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt32_4;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_UInt64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt64_5;

public:
	inline static int32_t get_offset_of_s_Int16_0() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_Int16_0)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int16_0() const { return ___s_Int16_0; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int16_0() { return &___s_Int16_0; }
	inline void set_s_Int16_0(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int16_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int16_0), value);
	}

	inline static int32_t get_offset_of_s_Int32_1() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_Int32_1)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int32_1() const { return ___s_Int32_1; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int32_1() { return &___s_Int32_1; }
	inline void set_s_Int32_1(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int32_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int32_1), value);
	}

	inline static int32_t get_offset_of_s_Int64_2() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_Int64_2)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int64_2() const { return ___s_Int64_2; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int64_2() { return &___s_Int64_2; }
	inline void set_s_Int64_2(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int64_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int64_2), value);
	}

	inline static int32_t get_offset_of_s_UInt16_3() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_UInt16_3)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt16_3() const { return ___s_UInt16_3; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt16_3() { return &___s_UInt16_3; }
	inline void set_s_UInt16_3(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt16_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt16_3), value);
	}

	inline static int32_t get_offset_of_s_UInt32_4() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_UInt32_4)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt32_4() const { return ___s_UInt32_4; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt32_4() { return &___s_UInt32_4; }
	inline void set_s_UInt32_4(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt32_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt32_4), value);
	}

	inline static int32_t get_offset_of_s_UInt64_5() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_UInt64_5)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt64_5() const { return ___s_UInt64_5; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt64_5() { return &___s_UInt64_5; }
	inline void set_s_UInt64_5(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt64_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt64_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFINSTRUCTION_TC360B9BB53E1001E5636FD18D7240A1A507216C3_H
#ifndef TYPEASINSTRUCTION_TCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E_H
#define TYPEASINSTRUCTION_TCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.TypeAsInstruction
struct  TypeAsInstruction_tCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.Type System.Linq.Expressions.Interpreter.TypeAsInstruction::_type
	Type_t * ____type_0;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(TypeAsInstruction_tCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E, ____type_0)); }
	inline Type_t * get__type_0() const { return ____type_0; }
	inline Type_t ** get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(Type_t * value)
	{
		____type_0 = value;
		Il2CppCodeGenWriteBarrier((&____type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEASINSTRUCTION_TCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E_H
#ifndef TYPEEQUALSINSTRUCTION_TCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_H
#define TYPEEQUALSINSTRUCTION_TCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.TypeEqualsInstruction
struct  TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.TypeEqualsInstruction System.Linq.Expressions.Interpreter.TypeEqualsInstruction::Instance
	TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_StaticFields, ___Instance_0)); }
	inline TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260 * get_Instance_0() const { return ___Instance_0; }
	inline TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEEQUALSINSTRUCTION_TCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_H
#ifndef TYPEISINSTRUCTION_T9F7EF0BB05E8F7A713E452A49E9C1979344032D8_H
#define TYPEISINSTRUCTION_T9F7EF0BB05E8F7A713E452A49E9C1979344032D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.TypeIsInstruction
struct  TypeIsInstruction_t9F7EF0BB05E8F7A713E452A49E9C1979344032D8  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.Type System.Linq.Expressions.Interpreter.TypeIsInstruction::_type
	Type_t * ____type_0;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(TypeIsInstruction_t9F7EF0BB05E8F7A713E452A49E9C1979344032D8, ____type_0)); }
	inline Type_t * get__type_0() const { return ____type_0; }
	inline Type_t ** get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(Type_t * value)
	{
		____type_0 = value;
		Il2CppCodeGenWriteBarrier((&____type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEISINSTRUCTION_T9F7EF0BB05E8F7A713E452A49E9C1979344032D8_H
#ifndef DYNAMICATTRIBUTE_T64F231CF1B7C130C900D7C7F1C73D3BBAF8D28AD_H
#define DYNAMICATTRIBUTE_T64F231CF1B7C130C900D7C7F1C73D3BBAF8D28AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.DynamicAttribute
struct  DynamicAttribute_t64F231CF1B7C130C900D7C7F1C73D3BBAF8D28AD  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean[] System.Runtime.CompilerServices.DynamicAttribute::_transformFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ____transformFlags_0;

public:
	inline static int32_t get_offset_of__transformFlags_0() { return static_cast<int32_t>(offsetof(DynamicAttribute_t64F231CF1B7C130C900D7C7F1C73D3BBAF8D28AD, ____transformFlags_0)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get__transformFlags_0() const { return ____transformFlags_0; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of__transformFlags_0() { return &____transformFlags_0; }
	inline void set__transformFlags_0(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		____transformFlags_0 = value;
		Il2CppCodeGenWriteBarrier((&____transformFlags_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICATTRIBUTE_T64F231CF1B7C130C900D7C7F1C73D3BBAF8D28AD_H
#ifndef CONVERTBINDER_T6823370A867A471BA9B6BEA72AEE8C2B55C04A5D_H
#define CONVERTBINDER_T6823370A867A471BA9B6BEA72AEE8C2B55C04A5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ConvertBinder
struct  ConvertBinder_t6823370A867A471BA9B6BEA72AEE8C2B55C04A5D  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// System.Type System.Dynamic.ConvertBinder::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_2;
	// System.Boolean System.Dynamic.ConvertBinder::<Explicit>k__BackingField
	bool ___U3CExplicitU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConvertBinder_t6823370A867A471BA9B6BEA72AEE8C2B55C04A5D, ___U3CTypeU3Ek__BackingField_2)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CExplicitU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ConvertBinder_t6823370A867A471BA9B6BEA72AEE8C2B55C04A5D, ___U3CExplicitU3Ek__BackingField_3)); }
	inline bool get_U3CExplicitU3Ek__BackingField_3() const { return ___U3CExplicitU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CExplicitU3Ek__BackingField_3() { return &___U3CExplicitU3Ek__BackingField_3; }
	inline void set_U3CExplicitU3Ek__BackingField_3(bool value)
	{
		___U3CExplicitU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTBINDER_T6823370A867A471BA9B6BEA72AEE8C2B55C04A5D_H
#ifndef EXPRESSIONTYPE_TA0C19CAF997A8592C84F0EBFE791BC19BF5AC556_H
#define EXPRESSIONTYPE_TA0C19CAF997A8592C84F0EBFE791BC19BF5AC556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ExpressionType
struct  ExpressionType_tA0C19CAF997A8592C84F0EBFE791BC19BF5AC556 
{
public:
	// System.Int32 System.Linq.Expressions.ExpressionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExpressionType_tA0C19CAF997A8592C84F0EBFE791BC19BF5AC556, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONTYPE_TA0C19CAF997A8592C84F0EBFE791BC19BF5AC556_H
#ifndef CASTINSTRUCTIONNOT_T62341A20143F79E1E73E12DC86CA05B2EFFB953F_H
#define CASTINSTRUCTIONNOT_T62341A20143F79E1E73E12DC86CA05B2EFFB953F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastInstruction/CastInstructionNoT
struct  CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F  : public CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC
{
public:
	// System.Type System.Linq.Expressions.Interpreter.CastInstruction/CastInstructionNoT::_t
	Type_t * ____t_15;

public:
	inline static int32_t get_offset_of__t_15() { return static_cast<int32_t>(offsetof(CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F, ____t_15)); }
	inline Type_t * get__t_15() const { return ____t_15; }
	inline Type_t ** get_address_of__t_15() { return &____t_15; }
	inline void set__t_15(Type_t * value)
	{
		____t_15 = value;
		Il2CppCodeGenWriteBarrier((&____t_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASTINSTRUCTIONNOT_T62341A20143F79E1E73E12DC86CA05B2EFFB953F_H
#ifndef CASTREFERENCETOENUMINSTRUCTION_T00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3_H
#define CASTREFERENCETOENUMINSTRUCTION_T00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastReferenceToEnumInstruction
struct  CastReferenceToEnumInstruction_t00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3  : public CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC
{
public:
	// System.Type System.Linq.Expressions.Interpreter.CastReferenceToEnumInstruction::_t
	Type_t * ____t_15;

public:
	inline static int32_t get_offset_of__t_15() { return static_cast<int32_t>(offsetof(CastReferenceToEnumInstruction_t00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3, ____t_15)); }
	inline Type_t * get__t_15() const { return ____t_15; }
	inline Type_t ** get_address_of__t_15() { return &____t_15; }
	inline void set__t_15(Type_t * value)
	{
		____t_15 = value;
		Il2CppCodeGenWriteBarrier((&____t_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASTREFERENCETOENUMINSTRUCTION_T00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3_H
#ifndef CASTTOENUMINSTRUCTION_T0B2C6839A4237E8483BA3BFE32E258949D84E6C9_H
#define CASTTOENUMINSTRUCTION_T0B2C6839A4237E8483BA3BFE32E258949D84E6C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastToEnumInstruction
struct  CastToEnumInstruction_t0B2C6839A4237E8483BA3BFE32E258949D84E6C9  : public CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC
{
public:
	// System.Type System.Linq.Expressions.Interpreter.CastToEnumInstruction::_t
	Type_t * ____t_15;

public:
	inline static int32_t get_offset_of__t_15() { return static_cast<int32_t>(offsetof(CastToEnumInstruction_t0B2C6839A4237E8483BA3BFE32E258949D84E6C9, ____t_15)); }
	inline Type_t * get__t_15() const { return ____t_15; }
	inline Type_t ** get_address_of__t_15() { return &____t_15; }
	inline void set__t_15(Type_t * value)
	{
		____t_15 = value;
		Il2CppCodeGenWriteBarrier((&____t_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASTTOENUMINSTRUCTION_T0B2C6839A4237E8483BA3BFE32E258949D84E6C9_H
#ifndef EQUALSCLASS_TFDAE59D269E7605CA0801DA0EDD418A8697CED40_H
#define EQUALSCLASS_TFDAE59D269E7605CA0801DA0EDD418A8697CED40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction/EqualsClass
struct  EqualsClass_tFDAE59D269E7605CA0801DA0EDD418A8697CED40  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALSCLASS_TFDAE59D269E7605CA0801DA0EDD418A8697CED40_H
#ifndef GETHASHCODECLASS_T28E38FB0F34088B773CA466C021B6958F21BF4E4_H
#define GETHASHCODECLASS_T28E38FB0F34088B773CA466C021B6958F21BF4E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction/GetHashCodeClass
struct  GetHashCodeClass_t28E38FB0F34088B773CA466C021B6958F21BF4E4  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETHASHCODECLASS_T28E38FB0F34088B773CA466C021B6958F21BF4E4_H
#ifndef GETVALUE_T7E9842D3FCF7CC597E92DC0FC1A17E978BB02C46_H
#define GETVALUE_T7E9842D3FCF7CC597E92DC0FC1A17E978BB02C46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction/GetValue
struct  GetValue_t7E9842D3FCF7CC597E92DC0FC1A17E978BB02C46  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVALUE_T7E9842D3FCF7CC597E92DC0FC1A17E978BB02C46_H
#ifndef GETVALUEORDEFAULT_TEE5791D009B0981C214341396175F911C5823575_H
#define GETVALUEORDEFAULT_TEE5791D009B0981C214341396175F911C5823575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction/GetValueOrDefault
struct  GetValueOrDefault_tEE5791D009B0981C214341396175F911C5823575  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:
	// System.Type System.Linq.Expressions.Interpreter.NullableMethodCallInstruction/GetValueOrDefault::defaultValueType
	Type_t * ___defaultValueType_6;

public:
	inline static int32_t get_offset_of_defaultValueType_6() { return static_cast<int32_t>(offsetof(GetValueOrDefault_tEE5791D009B0981C214341396175F911C5823575, ___defaultValueType_6)); }
	inline Type_t * get_defaultValueType_6() const { return ___defaultValueType_6; }
	inline Type_t ** get_address_of_defaultValueType_6() { return &___defaultValueType_6; }
	inline void set_defaultValueType_6(Type_t * value)
	{
		___defaultValueType_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValueType_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVALUEORDEFAULT_TEE5791D009B0981C214341396175F911C5823575_H
#ifndef GETVALUEORDEFAULT1_TC8BCC21B8BB18D7FFBB5A1ADA7D7237E1A7142ED_H
#define GETVALUEORDEFAULT1_TC8BCC21B8BB18D7FFBB5A1ADA7D7237E1A7142ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction/GetValueOrDefault1
struct  GetValueOrDefault1_tC8BCC21B8BB18D7FFBB5A1ADA7D7237E1A7142ED  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVALUEORDEFAULT1_TC8BCC21B8BB18D7FFBB5A1ADA7D7237E1A7142ED_H
#ifndef HASVALUE_T41E8E385119E2017CD420D5625324106BE6B10FE_H
#define HASVALUE_T41E8E385119E2017CD420D5625324106BE6B10FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction/HasValue
struct  HasValue_t41E8E385119E2017CD420D5625324106BE6B10FE  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASVALUE_T41E8E385119E2017CD420D5625324106BE6B10FE_H
#ifndef TOSTRINGCLASS_TBAABC22667C2755D0BC05C445B7102D611ECAFBB_H
#define TOSTRINGCLASS_TBAABC22667C2755D0BC05C445B7102D611ECAFBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction/ToStringClass
struct  ToStringClass_tBAABC22667C2755D0BC05C445B7102D611ECAFBB  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOSTRINGCLASS_TBAABC22667C2755D0BC05C445B7102D611ECAFBB_H
#ifndef RIGHTSHIFTBYTE_T84B22E9B669FFDBAD4FDFDCBA3673A0BFC2F1534_H
#define RIGHTSHIFTBYTE_T84B22E9B669FFDBAD4FDFDCBA3673A0BFC2F1534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction/RightShiftByte
struct  RightShiftByte_t84B22E9B669FFDBAD4FDFDCBA3673A0BFC2F1534  : public RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTBYTE_T84B22E9B669FFDBAD4FDFDCBA3673A0BFC2F1534_H
#ifndef RIGHTSHIFTINT32_T053C723574F9CE85AFCF435A4C070FB9A0978C5D_H
#define RIGHTSHIFTINT32_T053C723574F9CE85AFCF435A4C070FB9A0978C5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction/RightShiftInt32
struct  RightShiftInt32_t053C723574F9CE85AFCF435A4C070FB9A0978C5D  : public RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTINT32_T053C723574F9CE85AFCF435A4C070FB9A0978C5D_H
#ifndef RIGHTSHIFTINT64_TD8DB36379DB16D0170A179FAFD139988E24B5FB9_H
#define RIGHTSHIFTINT64_TD8DB36379DB16D0170A179FAFD139988E24B5FB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction/RightShiftInt64
struct  RightShiftInt64_tD8DB36379DB16D0170A179FAFD139988E24B5FB9  : public RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTINT64_TD8DB36379DB16D0170A179FAFD139988E24B5FB9_H
#ifndef RIGHTSHIFTUINT16_T87ECF24BB0B9A1274076BFA37DA497CE0C28A412_H
#define RIGHTSHIFTUINT16_T87ECF24BB0B9A1274076BFA37DA497CE0C28A412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction/RightShiftUInt16
struct  RightShiftUInt16_t87ECF24BB0B9A1274076BFA37DA497CE0C28A412  : public RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTUINT16_T87ECF24BB0B9A1274076BFA37DA497CE0C28A412_H
#ifndef RIGHTSHIFTUINT32_T11C80109EF04C50C4101929B224EA7B0EAF235D2_H
#define RIGHTSHIFTUINT32_T11C80109EF04C50C4101929B224EA7B0EAF235D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction/RightShiftUInt32
struct  RightShiftUInt32_t11C80109EF04C50C4101929B224EA7B0EAF235D2  : public RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTUINT32_T11C80109EF04C50C4101929B224EA7B0EAF235D2_H
#ifndef RIGHTSHIFTUINT64_TC8842CC358181C4115BA9BE4CD965CA444830FBD_H
#define RIGHTSHIFTUINT64_TC8842CC358181C4115BA9BE4CD965CA444830FBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction/RightShiftUInt64
struct  RightShiftUInt64_tC8842CC358181C4115BA9BE4CD965CA444830FBD  : public RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTUINT64_TC8842CC358181C4115BA9BE4CD965CA444830FBD_H
#ifndef SUBDOUBLE_TB2C29B9EAD9C37321CFD15C141A7ADBEB44C87F1_H
#define SUBDOUBLE_TB2C29B9EAD9C37321CFD15C141A7ADBEB44C87F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction/SubDouble
struct  SubDouble_tB2C29B9EAD9C37321CFD15C141A7ADBEB44C87F1  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBDOUBLE_TB2C29B9EAD9C37321CFD15C141A7ADBEB44C87F1_H
#ifndef SUBINT16_T92DD86AE94B50DE3BE5D3E2EDD5E6DC7FA8EB345_H
#define SUBINT16_T92DD86AE94B50DE3BE5D3E2EDD5E6DC7FA8EB345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction/SubInt16
struct  SubInt16_t92DD86AE94B50DE3BE5D3E2EDD5E6DC7FA8EB345  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBINT16_T92DD86AE94B50DE3BE5D3E2EDD5E6DC7FA8EB345_H
#ifndef SUBINT32_T95C12EAB5EB227515DABCB151A9C6ADA102B688E_H
#define SUBINT32_T95C12EAB5EB227515DABCB151A9C6ADA102B688E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction/SubInt32
struct  SubInt32_t95C12EAB5EB227515DABCB151A9C6ADA102B688E  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBINT32_T95C12EAB5EB227515DABCB151A9C6ADA102B688E_H
#ifndef SUBINT64_T2FD1CE07CD14C7B5077AD6650EB7A0A000AA2EB0_H
#define SUBINT64_T2FD1CE07CD14C7B5077AD6650EB7A0A000AA2EB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction/SubInt64
struct  SubInt64_t2FD1CE07CD14C7B5077AD6650EB7A0A000AA2EB0  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBINT64_T2FD1CE07CD14C7B5077AD6650EB7A0A000AA2EB0_H
#ifndef SUBSINGLE_T700531C30E61E060FE8E30B1B3F29EB7585CFFD7_H
#define SUBSINGLE_T700531C30E61E060FE8E30B1B3F29EB7585CFFD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction/SubSingle
struct  SubSingle_t700531C30E61E060FE8E30B1B3F29EB7585CFFD7  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSINGLE_T700531C30E61E060FE8E30B1B3F29EB7585CFFD7_H
#ifndef SUBUINT16_TD048821DF287E4980F7C7946674CC2B198746656_H
#define SUBUINT16_TD048821DF287E4980F7C7946674CC2B198746656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction/SubUInt16
struct  SubUInt16_tD048821DF287E4980F7C7946674CC2B198746656  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBUINT16_TD048821DF287E4980F7C7946674CC2B198746656_H
#ifndef SUBUINT32_TBBCF390E857963D94E338E564300D0AD3CFCAB28_H
#define SUBUINT32_TBBCF390E857963D94E338E564300D0AD3CFCAB28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction/SubUInt32
struct  SubUInt32_tBBCF390E857963D94E338E564300D0AD3CFCAB28  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBUINT32_TBBCF390E857963D94E338E564300D0AD3CFCAB28_H
#ifndef SUBUINT64_T8D003A5B96186A8A435EB4EC14D9D116E6D4590B_H
#define SUBUINT64_T8D003A5B96186A8A435EB4EC14D9D116E6D4590B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction/SubUInt64
struct  SubUInt64_t8D003A5B96186A8A435EB4EC14D9D116E6D4590B  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBUINT64_T8D003A5B96186A8A435EB4EC14D9D116E6D4590B_H
#ifndef SUBOVFINT16_T36FFF7E11B91ACC6CAEE70C3EB9E82F13AA62489_H
#define SUBOVFINT16_T36FFF7E11B91ACC6CAEE70C3EB9E82F13AA62489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction/SubOvfInt16
struct  SubOvfInt16_t36FFF7E11B91ACC6CAEE70C3EB9E82F13AA62489  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFINT16_T36FFF7E11B91ACC6CAEE70C3EB9E82F13AA62489_H
#ifndef SUBOVFINT32_T1D3DFA3EC8958835551A37DFF188E6A5AF42450A_H
#define SUBOVFINT32_T1D3DFA3EC8958835551A37DFF188E6A5AF42450A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction/SubOvfInt32
struct  SubOvfInt32_t1D3DFA3EC8958835551A37DFF188E6A5AF42450A  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFINT32_T1D3DFA3EC8958835551A37DFF188E6A5AF42450A_H
#ifndef SUBOVFINT64_T5BD992E8D4E5730D3BD7348529B0CF08D1A1F499_H
#define SUBOVFINT64_T5BD992E8D4E5730D3BD7348529B0CF08D1A1F499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction/SubOvfInt64
struct  SubOvfInt64_t5BD992E8D4E5730D3BD7348529B0CF08D1A1F499  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFINT64_T5BD992E8D4E5730D3BD7348529B0CF08D1A1F499_H
#ifndef SUBOVFUINT16_T8C9521D689F8B7FFC881B9FFBCC90770E145908F_H
#define SUBOVFUINT16_T8C9521D689F8B7FFC881B9FFBCC90770E145908F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction/SubOvfUInt16
struct  SubOvfUInt16_t8C9521D689F8B7FFC881B9FFBCC90770E145908F  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFUINT16_T8C9521D689F8B7FFC881B9FFBCC90770E145908F_H
#ifndef SUBOVFUINT32_T8077FE8705CE9D497059032FA4900E705A37D4B1_H
#define SUBOVFUINT32_T8077FE8705CE9D497059032FA4900E705A37D4B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction/SubOvfUInt32
struct  SubOvfUInt32_t8077FE8705CE9D497059032FA4900E705A37D4B1  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFUINT32_T8077FE8705CE9D497059032FA4900E705A37D4B1_H
#ifndef SUBOVFUINT64_TA6DEA75AC26C5092FEE80671B02C9201193EA87B_H
#define SUBOVFUINT64_TA6DEA75AC26C5092FEE80671B02C9201193EA87B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction/SubOvfUInt64
struct  SubOvfUInt64_tA6DEA75AC26C5092FEE80671B02C9201193EA87B  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFUINT64_TA6DEA75AC26C5092FEE80671B02C9201193EA87B_H
#ifndef BINARYOPERATIONBINDER_T62EB955B6CBE8A50ADDD56ABF893CDA60319FC62_H
#define BINARYOPERATIONBINDER_T62EB955B6CBE8A50ADDD56ABF893CDA60319FC62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BinaryOperationBinder
struct  BinaryOperationBinder_t62EB955B6CBE8A50ADDD56ABF893CDA60319FC62  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// System.Linq.Expressions.ExpressionType System.Dynamic.BinaryOperationBinder::<Operation>k__BackingField
	int32_t ___U3COperationU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3COperationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BinaryOperationBinder_t62EB955B6CBE8A50ADDD56ABF893CDA60319FC62, ___U3COperationU3Ek__BackingField_2)); }
	inline int32_t get_U3COperationU3Ek__BackingField_2() const { return ___U3COperationU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3COperationU3Ek__BackingField_2() { return &___U3COperationU3Ek__BackingField_2; }
	inline void set_U3COperationU3Ek__BackingField_2(int32_t value)
	{
		___U3COperationU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYOPERATIONBINDER_T62EB955B6CBE8A50ADDD56ABF893CDA60319FC62_H
#ifndef REF_T02288254DB73F4EED20FD86423CF79F84C4D895C_H
#define REF_T02288254DB73F4EED20FD86423CF79F84C4D895C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastInstruction/CastInstructionNoT/Ref
struct  Ref_t02288254DB73F4EED20FD86423CF79F84C4D895C  : public CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REF_T02288254DB73F4EED20FD86423CF79F84C4D895C_H
#ifndef VALUE_T7062A897C92AA41B6F15AD68D8AE96108CC0BD6B_H
#define VALUE_T7062A897C92AA41B6F15AD68D8AE96108CC0BD6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastInstruction/CastInstructionNoT/Value
struct  Value_t7062A897C92AA41B6F15AD68D8AE96108CC0BD6B  : public CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUE_T7062A897C92AA41B6F15AD68D8AE96108CC0BD6B_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (RightShiftInt32_t053C723574F9CE85AFCF435A4C070FB9A0978C5D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (RightShiftInt64_tD8DB36379DB16D0170A179FAFD139988E24B5FB9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (RightShiftByte_t84B22E9B669FFDBAD4FDFDCBA3673A0BFC2F1534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (RightShiftUInt16_t87ECF24BB0B9A1274076BFA37DA497CE0C28A412), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (RightShiftUInt32_t11C80109EF04C50C4101929B224EA7B0EAF235D2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (RightShiftUInt64_tC8842CC358181C4115BA9BE4CD965CA444830FBD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (RuntimeVariables_t80F25BDAAAD3795F8687E62A259E14D255AC4E5F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3606[1] = 
{
	RuntimeVariables_t80F25BDAAAD3795F8687E62A259E14D255AC4E5F::get_offset_of__boxes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (LoadObjectInstruction_t2C8D43685496B24000A7E069FDAEE73D4E72F1A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3607[1] = 
{
	LoadObjectInstruction_t2C8D43685496B24000A7E069FDAEE73D4E72F1A7::get_offset_of__value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (LoadCachedObjectInstruction_t70C07C702DAC18B1BB6BE378B9036A440B359D32), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3608[1] = 
{
	LoadCachedObjectInstruction_t70C07C702DAC18B1BB6BE378B9036A440B359D32::get_offset_of__index_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52), -1, sizeof(PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3609[1] = 
{
	PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512), -1, sizeof(DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3610[1] = 
{
	DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6), -1, sizeof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3611[8] = 
{
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_Int16_0(),
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_Int32_1(),
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_Int64_2(),
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_UInt16_3(),
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_UInt32_4(),
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_UInt64_5(),
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_Single_6(),
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_Double_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (SubInt16_t92DD86AE94B50DE3BE5D3E2EDD5E6DC7FA8EB345), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { sizeof (SubInt32_t95C12EAB5EB227515DABCB151A9C6ADA102B688E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (SubInt64_t2FD1CE07CD14C7B5077AD6650EB7A0A000AA2EB0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { sizeof (SubUInt16_tD048821DF287E4980F7C7946674CC2B198746656), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { sizeof (SubUInt32_tBBCF390E857963D94E338E564300D0AD3CFCAB28), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3617 = { sizeof (SubUInt64_t8D003A5B96186A8A435EB4EC14D9D116E6D4590B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3618 = { sizeof (SubSingle_t700531C30E61E060FE8E30B1B3F29EB7585CFFD7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3619 = { sizeof (SubDouble_tB2C29B9EAD9C37321CFD15C141A7ADBEB44C87F1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3620 = { sizeof (SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3), -1, sizeof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3620[6] = 
{
	SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields::get_offset_of_s_Int16_0(),
	SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields::get_offset_of_s_Int32_1(),
	SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields::get_offset_of_s_Int64_2(),
	SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields::get_offset_of_s_UInt16_3(),
	SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields::get_offset_of_s_UInt32_4(),
	SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields::get_offset_of_s_UInt64_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3621 = { sizeof (SubOvfInt16_t36FFF7E11B91ACC6CAEE70C3EB9E82F13AA62489), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3622 = { sizeof (SubOvfInt32_t1D3DFA3EC8958835551A37DFF188E6A5AF42450A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3623 = { sizeof (SubOvfInt64_t5BD992E8D4E5730D3BD7348529B0CF08D1A1F499), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3624 = { sizeof (SubOvfUInt16_t8C9521D689F8B7FFC881B9FFBCC90770E145908F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3625 = { sizeof (SubOvfUInt32_t8077FE8705CE9D497059032FA4900E705A37D4B1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3626 = { sizeof (SubOvfUInt64_tA6DEA75AC26C5092FEE80671B02C9201193EA87B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3627 = { sizeof (CreateDelegateInstruction_tF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3627[1] = 
{
	CreateDelegateInstruction_tF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5::get_offset_of__creator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3628 = { sizeof (TypeIsInstruction_t9F7EF0BB05E8F7A713E452A49E9C1979344032D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3628[1] = 
{
	TypeIsInstruction_t9F7EF0BB05E8F7A713E452A49E9C1979344032D8::get_offset_of__type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3629 = { sizeof (TypeAsInstruction_tCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3629[1] = 
{
	TypeAsInstruction_tCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E::get_offset_of__type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3630 = { sizeof (TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260), -1, sizeof(TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3630[1] = 
{
	TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3631 = { sizeof (NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB), -1, sizeof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3631[6] = 
{
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_hasValue_0(),
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_value_1(),
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_equals_2(),
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_getHashCode_3(),
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_getValueOrDefault1_4(),
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_toString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3632 = { sizeof (HasValue_t41E8E385119E2017CD420D5625324106BE6B10FE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3633 = { sizeof (GetValue_t7E9842D3FCF7CC597E92DC0FC1A17E978BB02C46), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3634 = { sizeof (GetValueOrDefault_tEE5791D009B0981C214341396175F911C5823575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3634[1] = 
{
	GetValueOrDefault_tEE5791D009B0981C214341396175F911C5823575::get_offset_of_defaultValueType_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3635 = { sizeof (GetValueOrDefault1_tC8BCC21B8BB18D7FFBB5A1ADA7D7237E1A7142ED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3636 = { sizeof (EqualsClass_tFDAE59D269E7605CA0801DA0EDD418A8697CED40), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3637 = { sizeof (ToStringClass_tBAABC22667C2755D0BC05C445B7102D611ECAFBB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3638 = { sizeof (GetHashCodeClass_t28E38FB0F34088B773CA466C021B6958F21BF4E4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3639 = { sizeof (CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC), -1, sizeof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3639[15] = 
{
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Boolean_0(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Byte_1(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Char_2(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_DateTime_3(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Decimal_4(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Double_5(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Int16_6(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Int32_7(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Int64_8(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_SByte_9(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Single_10(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_String_11(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_UInt16_12(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_UInt32_13(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_UInt64_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3640 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3641 = { sizeof (CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3641[1] = 
{
	CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F::get_offset_of__t_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3642 = { sizeof (Ref_t02288254DB73F4EED20FD86423CF79F84C4D895C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3643 = { sizeof (Value_t7062A897C92AA41B6F15AD68D8AE96108CC0BD6B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3644 = { sizeof (CastToEnumInstruction_t0B2C6839A4237E8483BA3BFE32E258949D84E6C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3644[1] = 
{
	CastToEnumInstruction_t0B2C6839A4237E8483BA3BFE32E258949D84E6C9::get_offset_of__t_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3645 = { sizeof (CastReferenceToEnumInstruction_t00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3645[1] = 
{
	CastReferenceToEnumInstruction_t00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3::get_offset_of__t_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3646 = { sizeof (QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3646[2] = 
{
	QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B::get_offset_of__operand_0(),
	QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B::get_offset_of__hoistedVariables_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3647 = { sizeof (ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3647[3] = 
{
	ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08::get_offset_of__variables_0(),
	ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08::get_offset_of__frame_1(),
	ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08::get_offset_of__shadowedVars_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3648 = { sizeof (DelegateHelpers_tC220AEB31E24035C65BDC2A87576B293B189EBF5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3649 = { sizeof (U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40), -1, sizeof(U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3649[2] = 
{
	U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3650 = { sizeof (ScriptingRuntimeHelpers_t5EBE625EFE604DEF42AFBCA8F9EF3A808FF38DC7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3651 = { sizeof (ExceptionHelpers_t5E8A8C882B8B390FA25BB54BF282A31251678BB9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3652 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3652[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3653 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3653[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3654 = { sizeof (DelegateHelpers_tF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633), -1, sizeof(DelegateHelpers_tF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3654[1] = 
{
	DelegateHelpers_tF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633_StaticFields::get_offset_of__DelegateCache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3655 = { sizeof (TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3655[2] = 
{
	TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447::get_offset_of_DelegateType_0(),
	TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447::get_offset_of_TypeChain_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3656 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3657 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3658 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3659 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3660 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3661 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3662 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3663 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3664 = { sizeof (RuntimeOps_t4AB498C75C54378274A510D5CD7DC9152A23E777), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3665 = { sizeof (MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3665[3] = 
{
	MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD::get_offset_of__first_0(),
	MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD::get_offset_of__second_1(),
	MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD::get_offset_of__indexes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3666 = { sizeof (RuntimeVariables_t08E07CA7021C6692E793278352675C63B744D61A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3666[1] = 
{
	RuntimeVariables_t08E07CA7021C6692E793278352675C63B744D61A::get_offset_of__boxes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3667 = { sizeof (CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E), -1, sizeof(CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3667[3] = 
{
	CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E_StaticFields::get_offset_of_s_siteCtors_0(),
	CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E::get_offset_of__binder_1(),
	CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E::get_offset_of__match_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3668 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3668[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3669 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3669[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3670 = { sizeof (CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889), -1, sizeof(CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3670[2] = 
{
	CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889::get_offset_of_Cache_0(),
	CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889_StaticFields::get_offset_of_U3CUpdateLabelU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3671 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3671[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3672 = { sizeof (CallSiteOps_tD92C41D19E4798EFE9D07F0C3910A0914EB231B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3673 = { sizeof (DebugInfoGenerator_tD7E0E52E58589C53D21CDCA37EB68E89E040ECDF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3674 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3675 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3675[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3676 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3676[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3677 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3677[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3678 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3679 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3679[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3680 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3681 = { sizeof (DynamicAttribute_t64F231CF1B7C130C900D7C7F1C73D3BBAF8D28AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3681[1] = 
{
	DynamicAttribute_t64F231CF1B7C130C900D7C7F1C73D3BBAF8D28AD::get_offset_of__transformFlags_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3682 = { sizeof (BinaryOperationBinder_t62EB955B6CBE8A50ADDD56ABF893CDA60319FC62), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3682[1] = 
{
	BinaryOperationBinder_t62EB955B6CBE8A50ADDD56ABF893CDA60319FC62::get_offset_of_U3COperationU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3683 = { sizeof (BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3), -1, sizeof(BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3683[1] = 
{
	BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3_StaticFields::get_offset_of_Empty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3684 = { sizeof (TestBuilder_tC1A8F66697AB4E6E5506DE4ED0E07A94961551E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3684[2] = 
{
	TestBuilder_tC1A8F66697AB4E6E5506DE4ED0E07A94961551E8::get_offset_of__unique_0(),
	TestBuilder_tC1A8F66697AB4E6E5506DE4ED0E07A94961551E8::get_offset_of__tests_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3685 = { sizeof (AndNode_tCC48979AF5896DA4A881EE49E182099994188916)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3685[2] = 
{
	AndNode_tCC48979AF5896DA4A881EE49E182099994188916::get_offset_of_Depth_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AndNode_tCC48979AF5896DA4A881EE49E182099994188916::get_offset_of_Node_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3686 = { sizeof (MergedRestriction_tAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3686[2] = 
{
	MergedRestriction_tAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F::get_offset_of_Left_1(),
	MergedRestriction_tAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F::get_offset_of_Right_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3687 = { sizeof (CustomRestriction_t2CF2494AA202519C8EF7D3D8E16B0C5E446926C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3687[1] = 
{
	CustomRestriction_t2CF2494AA202519C8EF7D3D8E16B0C5E446926C1::get_offset_of__expression_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3688 = { sizeof (TypeRestriction_t636F9773C1635EB8CDC62B6D21B18DD743221E0D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3688[2] = 
{
	TypeRestriction_t636F9773C1635EB8CDC62B6D21B18DD743221E0D::get_offset_of__expression_1(),
	TypeRestriction_t636F9773C1635EB8CDC62B6D21B18DD743221E0D::get_offset_of__type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3689 = { sizeof (InstanceRestriction_t7EFC44E326B6ED49A28CA8A1E8009F18F54AF247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3689[2] = 
{
	InstanceRestriction_t7EFC44E326B6ED49A28CA8A1E8009F18F54AF247::get_offset_of__expression_1(),
	InstanceRestriction_t7EFC44E326B6ED49A28CA8A1E8009F18F54AF247::get_offset_of__instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3690 = { sizeof (BindingRestrictionsProxy_tDEC6CC1C0B17D4B5055F3C2F6B10E1469EF452E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3690[1] = 
{
	BindingRestrictionsProxy_tDEC6CC1C0B17D4B5055F3C2F6B10E1469EF452E1::get_offset_of__node_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3691 = { sizeof (CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3691[2] = 
{
	CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935::get_offset_of_U3CArgumentCountU3Ek__BackingField_0(),
	CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935::get_offset_of_U3CArgumentNamesU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3692 = { sizeof (ConvertBinder_t6823370A867A471BA9B6BEA72AEE8C2B55C04A5D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3692[2] = 
{
	ConvertBinder_t6823370A867A471BA9B6BEA72AEE8C2B55C04A5D::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	ConvertBinder_t6823370A867A471BA9B6BEA72AEE8C2B55C04A5D::get_offset_of_U3CExplicitU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3693 = { sizeof (DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41), -1, sizeof(DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3693[5] = 
{
	DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41_StaticFields::get_offset_of_EmptyMetaObjects_0(),
	DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41::get_offset_of_U3CExpressionU3Ek__BackingField_1(),
	DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41::get_offset_of_U3CRestrictionsU3Ek__BackingField_2(),
	DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41::get_offset_of_U3CValueU3Ek__BackingField_3(),
	DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41::get_offset_of_U3CHasValueU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3694 = { sizeof (DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3695 = { sizeof (ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D), -1, sizeof(ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3695[4] = 
{
	ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D::get_offset_of__keys_0(),
	ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D::get_offset_of__hashCode_1(),
	ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D::get_offset_of__transitions_2(),
	ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D_StaticFields::get_offset_of_Empty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3696 = { sizeof (ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88), -1, sizeof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3696[10] = 
{
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields::get_offset_of_ExpandoTryGetValue_0(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields::get_offset_of_ExpandoTrySetValue_1(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields::get_offset_of_ExpandoTryDeleteValue_2(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields::get_offset_of_ExpandoPromoteClass_3(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields::get_offset_of_ExpandoCheckVersion_4(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88::get_offset_of_LockObject_5(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88::get_offset_of__data_6(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88::get_offset_of__count_7(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields::get_offset_of_Uninitialized_8(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88::get_offset_of__propertyChanged_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3697 = { sizeof (KeyCollectionDebugView_t5465EA831B384616D6EDC88E8E772350A7C1DC40), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3698 = { sizeof (KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3698[4] = 
{
	KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230::get_offset_of__expando_0(),
	KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230::get_offset_of__expandoVersion_1(),
	KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230::get_offset_of__expandoCount_2(),
	KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230::get_offset_of__expandoData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3699 = { sizeof (U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3699[5] = 
{
	U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775::get_offset_of_U3CiU3E5__1_3(),
	U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775::get_offset_of_U3CnU3E5__2_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
