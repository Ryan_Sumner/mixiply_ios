﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// MS.Internal.Xml.XPath.AstNode
struct AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C;
// MS.Internal.Xml.XPath.Axis
struct Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.Queue
struct Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3;
// System.Collections.Stack
struct Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643;
// System.Decimal[]
struct DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43;
// System.Int64[]
struct Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.ObjectIDGenerator
struct ObjectIDGenerator_tD65209D0A1CDA08C9C709A4D1997D8091A93C1A1;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.Xml.IValidationEventHandling
struct IValidationEventHandling_tABD39B6B973C0A0DC259D55D8C4179A43ACAB41B;
// System.Xml.PositionInfo
struct PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684;
// System.Xml.Schema.ActiveAxis
struct ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E;
// System.Xml.Schema.Asttree
struct Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA;
// System.Xml.Schema.Asttree[]
struct AsttreeU5BU5D_tE98D3D56BDC1A5B380B40C34285F212D51BF37D3;
// System.Xml.Schema.BitSet
struct BitSet_t0E4C53EC600670A4B74C5671553596978880138C;
// System.Xml.Schema.BitSet[]
struct BitSetU5BU5D_tC61B9A480CE3460277DE0ABFA4551635BB337D69;
// System.Xml.Schema.CompiledIdentityConstraint
struct CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E;
// System.Xml.Schema.ConstraintStruct
struct ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54;
// System.Xml.Schema.DatatypeImplementation
struct DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836;
// System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap[]
struct SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7;
// System.Xml.Schema.DatatypeImplementation[]
struct DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36;
// System.Xml.Schema.DoubleLinkAxis
struct DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955;
// System.Xml.Schema.FacetsChecker
struct FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6;
// System.Xml.Schema.ForwardAxis
struct ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253;
// System.Xml.Schema.KeySequence
struct KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE;
// System.Xml.Schema.LocatedActiveAxis[]
struct LocatedActiveAxisU5BU5D_t4148EC17C851E170B819098896BDD191CDDC41D1;
// System.Xml.Schema.NamespaceList
struct NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C;
// System.Xml.Schema.Positions
struct Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D;
// System.Xml.Schema.RestrictionFacets
struct RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41;
// System.Xml.Schema.SchemaNames
struct SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89;
// System.Xml.Schema.SelectorActiveAxis
struct SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64;
// System.Xml.Schema.SequenceNode
struct SequenceNode_tAB18F790CB1B5BCD1D984EECC17C841B00881608;
// System.Xml.Schema.SymbolsDictionary
struct SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6;
// System.Xml.Schema.SyntaxTreeNode
struct SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C;
// System.Xml.Schema.TypedObject/DecimalStruct
struct DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11;
// System.Xml.Schema.TypedObject[]
struct TypedObjectU5BU5D_t5F58D2C06E283EF28F0674C78DEB47DA64DF9CE1;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF;
// System.Xml.Schema.ValidationState
struct ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B;
// System.Xml.Schema.XmlSchema
struct XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F;
// System.Xml.Schema.XmlSchemaCollection
struct XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5;
// System.Xml.Schema.XmlSchemaCompilationSettings
struct XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC;
// System.Xml.Schema.XmlSchemaComplexType
struct XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A;
// System.Xml.Schema.XmlSchemaSimpleType[]
struct XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9;
// System.Xml.Schema.XmlValueConverter
struct XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E;
// System.Xml.Serialization.ClassMap
struct ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA;
// System.Xml.Serialization.EnumMap/EnumMapMember[]
struct EnumMapMemberU5BU5D_t65FB66969212223ED79971B0473634E03D6C1B2E;
// System.Xml.Serialization.ListMap
struct ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742;
// System.Xml.Serialization.ObjectMap
struct ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677;
// System.Xml.Serialization.SerializationSource
struct SerializationSource_tAA44E4510C7A00BEF2802B7E4E1CD299F1C3F1CC;
// System.Xml.Serialization.TypeData
struct TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94;
// System.Xml.Serialization.UnreferencedObjectEventHandler
struct UnreferencedObjectEventHandler_t43A5BBC5B5EC952973CF7F9F191EAFF5C5DBCB9B;
// System.Xml.Serialization.XmlAttributeEventHandler
struct XmlAttributeEventHandler_t7DE5648EC8D9459D5DEA07DF556AFDF235C661A6;
// System.Xml.Serialization.XmlElementEventHandler
struct XmlElementEventHandler_tF116A337C283F6E1ECC94728C6F113676D9A8174;
// System.Xml.Serialization.XmlMapping
struct XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726;
// System.Xml.Serialization.XmlNodeEventHandler
struct XmlNodeEventHandler_tD870BE5167E49CD15CA2F1879CD1EC0229C40F49;
// System.Xml.Serialization.XmlSerializationCollectionFixupCallback
struct XmlSerializationCollectionFixupCallback_t3C0601557C988CCF12827291EB63B51F687463FB;
// System.Xml.Serialization.XmlSerializationFixupCallback
struct XmlSerializationFixupCallback_t044B3CA7B13D386365F0F40BEB656A8B0370C800;
// System.Xml.Serialization.XmlSerializationReadCallback
struct XmlSerializationReadCallback_tB4AF7384FD4DBE50B3CCC5C267F2C3CD04F22443;
// System.Xml.Serialization.XmlSerializationReaderInterpreter
struct XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C;
// System.Xml.Serialization.XmlSerializationWriteCallback
struct XmlSerializationWriteCallback_tC98B104DBDBC776D566F853B9DED7668089431FA;
// System.Xml.Serialization.XmlSerializationWriterInterpreter
struct XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221;
// System.Xml.Serialization.XmlSerializer
struct XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C;
// System.Xml.Serialization.XmlSerializer/SerializerData
struct SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8;
// System.Xml.Serialization.XmlSerializerImplementation
struct XmlSerializerImplementation_tF78BC93462D4003D83EAA821331747A0624F837D;
// System.Xml.Serialization.XmlTypeMapElementInfoList
struct XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0;
// System.Xml.Serialization.XmlTypeMapMember
struct XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D;
// System.Xml.Serialization.XmlTypeMapMemberAnyAttribute
struct XmlTypeMapMemberAnyAttribute_t0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE;
// System.Xml.Serialization.XmlTypeMapMemberAnyElement
struct XmlTypeMapMemberAnyElement_tEC6C4FFA530F292BC4C09145DA5937285A664472;
// System.Xml.Serialization.XmlTypeMapMemberAttribute[]
struct XmlTypeMapMemberAttributeU5BU5D_t1A3EF531DFF7CF757326012C417167F79DDE0D10;
// System.Xml.Serialization.XmlTypeMapMemberNamespaces
struct XmlTypeMapMemberNamespaces_t08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43;
// System.Xml.Serialization.XmlTypeMapping
struct XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA;
// System.Xml.XmlDocument
struct XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97;
// System.Xml.XmlNameTable
struct XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD;
// System.Xml.XmlReader
struct XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB;
// System.Xml.XmlResolver
struct XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280;
// System.Xml.XmlValidatingReaderImpl
struct XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E;
// System.Xml.XmlWriter
struct XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869;

struct Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 ;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASTNODE_TEB8196548F948D593E8705AE90AECD4B02D2D93C_H
#define ASTNODE_TEB8196548F948D593E8705AE90AECD4B02D2D93C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.AstNode
struct  AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTNODE_TEB8196548F948D593E8705AE90AECD4B02D2D93C_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef ARRAYLIST_T4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4_H
#define ARRAYLIST_T4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ArrayList
struct  ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4  : public RuntimeObject
{
public:
	// System.Object[] System.Collections.ArrayList::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_0;
	// System.Int32 System.Collections.ArrayList::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.ArrayList::_version
	int32_t ____version_2;
	// System.Object System.Collections.ArrayList::_syncRoot
	RuntimeObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__items_0() { return static_cast<int32_t>(offsetof(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4, ____items_0)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_0() const { return ____items_0; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_0() { return &____items_0; }
	inline void set__items_0(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_0 = value;
		Il2CppCodeGenWriteBarrier((&____items_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_3), value);
	}
};

struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4_StaticFields
{
public:
	// System.Object[] System.Collections.ArrayList::emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___emptyArray_4;

public:
	inline static int32_t get_offset_of_emptyArray_4() { return static_cast<int32_t>(offsetof(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4_StaticFields, ___emptyArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_emptyArray_4() const { return ___emptyArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_emptyArray_4() { return &___emptyArray_4; }
	inline void set_emptyArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___emptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___emptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYLIST_T4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ACTIVEAXIS_TD7958CF29FECEF35D850FA29624E57EF040E315E_H
#define ACTIVEAXIS_TD7958CF29FECEF35D850FA29624E57EF040E315E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ActiveAxis
struct  ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.ActiveAxis::currentDepth
	int32_t ___currentDepth_0;
	// System.Boolean System.Xml.Schema.ActiveAxis::isActive
	bool ___isActive_1;
	// System.Xml.Schema.Asttree System.Xml.Schema.ActiveAxis::axisTree
	Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA * ___axisTree_2;
	// System.Collections.ArrayList System.Xml.Schema.ActiveAxis::axisStack
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___axisStack_3;

public:
	inline static int32_t get_offset_of_currentDepth_0() { return static_cast<int32_t>(offsetof(ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E, ___currentDepth_0)); }
	inline int32_t get_currentDepth_0() const { return ___currentDepth_0; }
	inline int32_t* get_address_of_currentDepth_0() { return &___currentDepth_0; }
	inline void set_currentDepth_0(int32_t value)
	{
		___currentDepth_0 = value;
	}

	inline static int32_t get_offset_of_isActive_1() { return static_cast<int32_t>(offsetof(ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E, ___isActive_1)); }
	inline bool get_isActive_1() const { return ___isActive_1; }
	inline bool* get_address_of_isActive_1() { return &___isActive_1; }
	inline void set_isActive_1(bool value)
	{
		___isActive_1 = value;
	}

	inline static int32_t get_offset_of_axisTree_2() { return static_cast<int32_t>(offsetof(ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E, ___axisTree_2)); }
	inline Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA * get_axisTree_2() const { return ___axisTree_2; }
	inline Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA ** get_address_of_axisTree_2() { return &___axisTree_2; }
	inline void set_axisTree_2(Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA * value)
	{
		___axisTree_2 = value;
		Il2CppCodeGenWriteBarrier((&___axisTree_2), value);
	}

	inline static int32_t get_offset_of_axisStack_3() { return static_cast<int32_t>(offsetof(ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E, ___axisStack_3)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_axisStack_3() const { return ___axisStack_3; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_axisStack_3() { return &___axisStack_3; }
	inline void set_axisStack_3(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___axisStack_3 = value;
		Il2CppCodeGenWriteBarrier((&___axisStack_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEAXIS_TD7958CF29FECEF35D850FA29624E57EF040E315E_H
#ifndef ASTTREE_T9D8B0988F0376119C895FC560A1A81FB9C452FCA_H
#define ASTTREE_T9D8B0988F0376119C895FC560A1A81FB9C452FCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Asttree
struct  Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Xml.Schema.Asttree::fAxisArray
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___fAxisArray_0;
	// System.String System.Xml.Schema.Asttree::xpathexpr
	String_t* ___xpathexpr_1;
	// System.Boolean System.Xml.Schema.Asttree::isField
	bool ___isField_2;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.Asttree::nsmgr
	XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * ___nsmgr_3;

public:
	inline static int32_t get_offset_of_fAxisArray_0() { return static_cast<int32_t>(offsetof(Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA, ___fAxisArray_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_fAxisArray_0() const { return ___fAxisArray_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_fAxisArray_0() { return &___fAxisArray_0; }
	inline void set_fAxisArray_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___fAxisArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___fAxisArray_0), value);
	}

	inline static int32_t get_offset_of_xpathexpr_1() { return static_cast<int32_t>(offsetof(Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA, ___xpathexpr_1)); }
	inline String_t* get_xpathexpr_1() const { return ___xpathexpr_1; }
	inline String_t** get_address_of_xpathexpr_1() { return &___xpathexpr_1; }
	inline void set_xpathexpr_1(String_t* value)
	{
		___xpathexpr_1 = value;
		Il2CppCodeGenWriteBarrier((&___xpathexpr_1), value);
	}

	inline static int32_t get_offset_of_isField_2() { return static_cast<int32_t>(offsetof(Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA, ___isField_2)); }
	inline bool get_isField_2() const { return ___isField_2; }
	inline bool* get_address_of_isField_2() { return &___isField_2; }
	inline void set_isField_2(bool value)
	{
		___isField_2 = value;
	}

	inline static int32_t get_offset_of_nsmgr_3() { return static_cast<int32_t>(offsetof(Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA, ___nsmgr_3)); }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * get_nsmgr_3() const { return ___nsmgr_3; }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F ** get_address_of_nsmgr_3() { return &___nsmgr_3; }
	inline void set_nsmgr_3(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * value)
	{
		___nsmgr_3 = value;
		Il2CppCodeGenWriteBarrier((&___nsmgr_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTTREE_T9D8B0988F0376119C895FC560A1A81FB9C452FCA_H
#ifndef AXISELEMENT_T8DC31C9834DF2F50E9E4D245D07C1AE68F226D80_H
#define AXISELEMENT_T8DC31C9834DF2F50E9E4D245D07C1AE68F226D80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.AxisElement
struct  AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80  : public RuntimeObject
{
public:
	// System.Xml.Schema.DoubleLinkAxis System.Xml.Schema.AxisElement::curNode
	DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * ___curNode_0;
	// System.Int32 System.Xml.Schema.AxisElement::rootDepth
	int32_t ___rootDepth_1;
	// System.Int32 System.Xml.Schema.AxisElement::curDepth
	int32_t ___curDepth_2;
	// System.Boolean System.Xml.Schema.AxisElement::isMatch
	bool ___isMatch_3;

public:
	inline static int32_t get_offset_of_curNode_0() { return static_cast<int32_t>(offsetof(AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80, ___curNode_0)); }
	inline DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * get_curNode_0() const { return ___curNode_0; }
	inline DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 ** get_address_of_curNode_0() { return &___curNode_0; }
	inline void set_curNode_0(DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * value)
	{
		___curNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___curNode_0), value);
	}

	inline static int32_t get_offset_of_rootDepth_1() { return static_cast<int32_t>(offsetof(AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80, ___rootDepth_1)); }
	inline int32_t get_rootDepth_1() const { return ___rootDepth_1; }
	inline int32_t* get_address_of_rootDepth_1() { return &___rootDepth_1; }
	inline void set_rootDepth_1(int32_t value)
	{
		___rootDepth_1 = value;
	}

	inline static int32_t get_offset_of_curDepth_2() { return static_cast<int32_t>(offsetof(AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80, ___curDepth_2)); }
	inline int32_t get_curDepth_2() const { return ___curDepth_2; }
	inline int32_t* get_address_of_curDepth_2() { return &___curDepth_2; }
	inline void set_curDepth_2(int32_t value)
	{
		___curDepth_2 = value;
	}

	inline static int32_t get_offset_of_isMatch_3() { return static_cast<int32_t>(offsetof(AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80, ___isMatch_3)); }
	inline bool get_isMatch_3() const { return ___isMatch_3; }
	inline bool* get_address_of_isMatch_3() { return &___isMatch_3; }
	inline void set_isMatch_3(bool value)
	{
		___isMatch_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISELEMENT_T8DC31C9834DF2F50E9E4D245D07C1AE68F226D80_H
#ifndef AXISSTACK_T3AA7511C349203BE08B1ECF76EA6C0031534C4A2_H
#define AXISSTACK_T3AA7511C349203BE08B1ECF76EA6C0031534C4A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.AxisStack
struct  AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Xml.Schema.AxisStack::stack
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___stack_0;
	// System.Xml.Schema.ForwardAxis System.Xml.Schema.AxisStack::subtree
	ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253 * ___subtree_1;
	// System.Xml.Schema.ActiveAxis System.Xml.Schema.AxisStack::parent
	ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E * ___parent_2;

public:
	inline static int32_t get_offset_of_stack_0() { return static_cast<int32_t>(offsetof(AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2, ___stack_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_stack_0() const { return ___stack_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_stack_0() { return &___stack_0; }
	inline void set_stack_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___stack_0), value);
	}

	inline static int32_t get_offset_of_subtree_1() { return static_cast<int32_t>(offsetof(AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2, ___subtree_1)); }
	inline ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253 * get_subtree_1() const { return ___subtree_1; }
	inline ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253 ** get_address_of_subtree_1() { return &___subtree_1; }
	inline void set_subtree_1(ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253 * value)
	{
		___subtree_1 = value;
		Il2CppCodeGenWriteBarrier((&___subtree_1), value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2, ___parent_2)); }
	inline ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E * get_parent_2() const { return ___parent_2; }
	inline ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___parent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSTACK_T3AA7511C349203BE08B1ECF76EA6C0031534C4A2_H
#ifndef BASEPROCESSOR_T68F1D18B3D8A79746118A87DF029D5FA8E90D427_H
#define BASEPROCESSOR_T68F1D18B3D8A79746118A87DF029D5FA8E90D427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BaseProcessor
struct  BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.Schema.BaseProcessor::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_0;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.BaseProcessor::schemaNames
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * ___schemaNames_1;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.BaseProcessor::eventHandler
	ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * ___eventHandler_2;
	// System.Xml.Schema.XmlSchemaCompilationSettings System.Xml.Schema.BaseProcessor::compilationSettings
	XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC * ___compilationSettings_3;
	// System.Int32 System.Xml.Schema.BaseProcessor::errorCount
	int32_t ___errorCount_4;
	// System.String System.Xml.Schema.BaseProcessor::NsXml
	String_t* ___NsXml_5;

public:
	inline static int32_t get_offset_of_nameTable_0() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___nameTable_0)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_0() const { return ___nameTable_0; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_0() { return &___nameTable_0; }
	inline void set_nameTable_0(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_0), value);
	}

	inline static int32_t get_offset_of_schemaNames_1() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___schemaNames_1)); }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * get_schemaNames_1() const { return ___schemaNames_1; }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 ** get_address_of_schemaNames_1() { return &___schemaNames_1; }
	inline void set_schemaNames_1(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * value)
	{
		___schemaNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_1), value);
	}

	inline static int32_t get_offset_of_eventHandler_2() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___eventHandler_2)); }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * get_eventHandler_2() const { return ___eventHandler_2; }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF ** get_address_of_eventHandler_2() { return &___eventHandler_2; }
	inline void set_eventHandler_2(ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * value)
	{
		___eventHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandler_2), value);
	}

	inline static int32_t get_offset_of_compilationSettings_3() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___compilationSettings_3)); }
	inline XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC * get_compilationSettings_3() const { return ___compilationSettings_3; }
	inline XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC ** get_address_of_compilationSettings_3() { return &___compilationSettings_3; }
	inline void set_compilationSettings_3(XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC * value)
	{
		___compilationSettings_3 = value;
		Il2CppCodeGenWriteBarrier((&___compilationSettings_3), value);
	}

	inline static int32_t get_offset_of_errorCount_4() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___errorCount_4)); }
	inline int32_t get_errorCount_4() const { return ___errorCount_4; }
	inline int32_t* get_address_of_errorCount_4() { return &___errorCount_4; }
	inline void set_errorCount_4(int32_t value)
	{
		___errorCount_4 = value;
	}

	inline static int32_t get_offset_of_NsXml_5() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___NsXml_5)); }
	inline String_t* get_NsXml_5() const { return ___NsXml_5; }
	inline String_t** get_address_of_NsXml_5() { return &___NsXml_5; }
	inline void set_NsXml_5(String_t* value)
	{
		___NsXml_5 = value;
		Il2CppCodeGenWriteBarrier((&___NsXml_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEPROCESSOR_T68F1D18B3D8A79746118A87DF029D5FA8E90D427_H
#ifndef BASEVALIDATOR_TB0AD5FCB23C61EC8841C153CEB7C77E569246B43_H
#define BASEVALIDATOR_TB0AD5FCB23C61EC8841C153CEB7C77E569246B43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BaseValidator
struct  BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaCollection System.Xml.Schema.BaseValidator::schemaCollection
	XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * ___schemaCollection_0;
	// System.Xml.IValidationEventHandling System.Xml.Schema.BaseValidator::eventHandling
	RuntimeObject* ___eventHandling_1;
	// System.Xml.XmlNameTable System.Xml.Schema.BaseValidator::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_2;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.BaseValidator::schemaNames
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * ___schemaNames_3;
	// System.Xml.PositionInfo System.Xml.Schema.BaseValidator::positionInfo
	PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * ___positionInfo_4;
	// System.Xml.XmlResolver System.Xml.Schema.BaseValidator::xmlResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___xmlResolver_5;
	// System.Uri System.Xml.Schema.BaseValidator::baseUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___baseUri_6;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.BaseValidator::schemaInfo
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * ___schemaInfo_7;
	// System.Xml.XmlValidatingReaderImpl System.Xml.Schema.BaseValidator::reader
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * ___reader_8;
	// System.Xml.XmlQualifiedName System.Xml.Schema.BaseValidator::elementName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___elementName_9;
	// System.Xml.Schema.ValidationState System.Xml.Schema.BaseValidator::context
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * ___context_10;
	// System.Text.StringBuilder System.Xml.Schema.BaseValidator::textValue
	StringBuilder_t * ___textValue_11;
	// System.String System.Xml.Schema.BaseValidator::textString
	String_t* ___textString_12;
	// System.Boolean System.Xml.Schema.BaseValidator::hasSibling
	bool ___hasSibling_13;
	// System.Boolean System.Xml.Schema.BaseValidator::checkDatatype
	bool ___checkDatatype_14;

public:
	inline static int32_t get_offset_of_schemaCollection_0() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___schemaCollection_0)); }
	inline XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * get_schemaCollection_0() const { return ___schemaCollection_0; }
	inline XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 ** get_address_of_schemaCollection_0() { return &___schemaCollection_0; }
	inline void set_schemaCollection_0(XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * value)
	{
		___schemaCollection_0 = value;
		Il2CppCodeGenWriteBarrier((&___schemaCollection_0), value);
	}

	inline static int32_t get_offset_of_eventHandling_1() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___eventHandling_1)); }
	inline RuntimeObject* get_eventHandling_1() const { return ___eventHandling_1; }
	inline RuntimeObject** get_address_of_eventHandling_1() { return &___eventHandling_1; }
	inline void set_eventHandling_1(RuntimeObject* value)
	{
		___eventHandling_1 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandling_1), value);
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___nameTable_2)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_schemaNames_3() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___schemaNames_3)); }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * get_schemaNames_3() const { return ___schemaNames_3; }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 ** get_address_of_schemaNames_3() { return &___schemaNames_3; }
	inline void set_schemaNames_3(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * value)
	{
		___schemaNames_3 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_3), value);
	}

	inline static int32_t get_offset_of_positionInfo_4() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___positionInfo_4)); }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * get_positionInfo_4() const { return ___positionInfo_4; }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 ** get_address_of_positionInfo_4() { return &___positionInfo_4; }
	inline void set_positionInfo_4(PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * value)
	{
		___positionInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___positionInfo_4), value);
	}

	inline static int32_t get_offset_of_xmlResolver_5() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___xmlResolver_5)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_xmlResolver_5() const { return ___xmlResolver_5; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_xmlResolver_5() { return &___xmlResolver_5; }
	inline void set_xmlResolver_5(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___xmlResolver_5 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_5), value);
	}

	inline static int32_t get_offset_of_baseUri_6() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___baseUri_6)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_baseUri_6() const { return ___baseUri_6; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_baseUri_6() { return &___baseUri_6; }
	inline void set_baseUri_6(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___baseUri_6 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_6), value);
	}

	inline static int32_t get_offset_of_schemaInfo_7() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___schemaInfo_7)); }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * get_schemaInfo_7() const { return ___schemaInfo_7; }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 ** get_address_of_schemaInfo_7() { return &___schemaInfo_7; }
	inline void set_schemaInfo_7(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * value)
	{
		___schemaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_7), value);
	}

	inline static int32_t get_offset_of_reader_8() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___reader_8)); }
	inline XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * get_reader_8() const { return ___reader_8; }
	inline XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E ** get_address_of_reader_8() { return &___reader_8; }
	inline void set_reader_8(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * value)
	{
		___reader_8 = value;
		Il2CppCodeGenWriteBarrier((&___reader_8), value);
	}

	inline static int32_t get_offset_of_elementName_9() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___elementName_9)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_elementName_9() const { return ___elementName_9; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_elementName_9() { return &___elementName_9; }
	inline void set_elementName_9(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___elementName_9 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_9), value);
	}

	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___context_10)); }
	inline ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * get_context_10() const { return ___context_10; }
	inline ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier((&___context_10), value);
	}

	inline static int32_t get_offset_of_textValue_11() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___textValue_11)); }
	inline StringBuilder_t * get_textValue_11() const { return ___textValue_11; }
	inline StringBuilder_t ** get_address_of_textValue_11() { return &___textValue_11; }
	inline void set_textValue_11(StringBuilder_t * value)
	{
		___textValue_11 = value;
		Il2CppCodeGenWriteBarrier((&___textValue_11), value);
	}

	inline static int32_t get_offset_of_textString_12() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___textString_12)); }
	inline String_t* get_textString_12() const { return ___textString_12; }
	inline String_t** get_address_of_textString_12() { return &___textString_12; }
	inline void set_textString_12(String_t* value)
	{
		___textString_12 = value;
		Il2CppCodeGenWriteBarrier((&___textString_12), value);
	}

	inline static int32_t get_offset_of_hasSibling_13() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___hasSibling_13)); }
	inline bool get_hasSibling_13() const { return ___hasSibling_13; }
	inline bool* get_address_of_hasSibling_13() { return &___hasSibling_13; }
	inline void set_hasSibling_13(bool value)
	{
		___hasSibling_13 = value;
	}

	inline static int32_t get_offset_of_checkDatatype_14() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___checkDatatype_14)); }
	inline bool get_checkDatatype_14() const { return ___checkDatatype_14; }
	inline bool* get_address_of_checkDatatype_14() { return &___checkDatatype_14; }
	inline void set_checkDatatype_14(bool value)
	{
		___checkDatatype_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVALIDATOR_TB0AD5FCB23C61EC8841C153CEB7C77E569246B43_H
#ifndef BITSET_T0E4C53EC600670A4B74C5671553596978880138C_H
#define BITSET_T0E4C53EC600670A4B74C5671553596978880138C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BitSet
struct  BitSet_t0E4C53EC600670A4B74C5671553596978880138C  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.BitSet::count
	int32_t ___count_0;
	// System.UInt32[] System.Xml.Schema.BitSet::bits
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___bits_1;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(BitSet_t0E4C53EC600670A4B74C5671553596978880138C, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_bits_1() { return static_cast<int32_t>(offsetof(BitSet_t0E4C53EC600670A4B74C5671553596978880138C, ___bits_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_bits_1() const { return ___bits_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_bits_1() { return &___bits_1; }
	inline void set_bits_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___bits_1 = value;
		Il2CppCodeGenWriteBarrier((&___bits_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITSET_T0E4C53EC600670A4B74C5671553596978880138C_H
#ifndef CHAMELEONKEY_TB2DC0F8044027C691C7325267CB62C0BBDB1CB2D_H
#define CHAMELEONKEY_TB2DC0F8044027C691C7325267CB62C0BBDB1CB2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ChameleonKey
struct  ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D  : public RuntimeObject
{
public:
	// System.String System.Xml.Schema.ChameleonKey::targetNS
	String_t* ___targetNS_0;
	// System.Uri System.Xml.Schema.ChameleonKey::chameleonLocation
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___chameleonLocation_1;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.ChameleonKey::originalSchema
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___originalSchema_2;
	// System.Int32 System.Xml.Schema.ChameleonKey::hashCode
	int32_t ___hashCode_3;

public:
	inline static int32_t get_offset_of_targetNS_0() { return static_cast<int32_t>(offsetof(ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D, ___targetNS_0)); }
	inline String_t* get_targetNS_0() const { return ___targetNS_0; }
	inline String_t** get_address_of_targetNS_0() { return &___targetNS_0; }
	inline void set_targetNS_0(String_t* value)
	{
		___targetNS_0 = value;
		Il2CppCodeGenWriteBarrier((&___targetNS_0), value);
	}

	inline static int32_t get_offset_of_chameleonLocation_1() { return static_cast<int32_t>(offsetof(ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D, ___chameleonLocation_1)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_chameleonLocation_1() const { return ___chameleonLocation_1; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_chameleonLocation_1() { return &___chameleonLocation_1; }
	inline void set_chameleonLocation_1(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___chameleonLocation_1 = value;
		Il2CppCodeGenWriteBarrier((&___chameleonLocation_1), value);
	}

	inline static int32_t get_offset_of_originalSchema_2() { return static_cast<int32_t>(offsetof(ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D, ___originalSchema_2)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_originalSchema_2() const { return ___originalSchema_2; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_originalSchema_2() { return &___originalSchema_2; }
	inline void set_originalSchema_2(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___originalSchema_2 = value;
		Il2CppCodeGenWriteBarrier((&___originalSchema_2), value);
	}

	inline static int32_t get_offset_of_hashCode_3() { return static_cast<int32_t>(offsetof(ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D, ___hashCode_3)); }
	inline int32_t get_hashCode_3() const { return ___hashCode_3; }
	inline int32_t* get_address_of_hashCode_3() { return &___hashCode_3; }
	inline void set_hashCode_3(int32_t value)
	{
		___hashCode_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAMELEONKEY_TB2DC0F8044027C691C7325267CB62C0BBDB1CB2D_H
#ifndef CONSTRAINTSTRUCT_T3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54_H
#define CONSTRAINTSTRUCT_T3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ConstraintStruct
struct  ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54  : public RuntimeObject
{
public:
	// System.Xml.Schema.CompiledIdentityConstraint System.Xml.Schema.ConstraintStruct::constraint
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E * ___constraint_0;
	// System.Xml.Schema.SelectorActiveAxis System.Xml.Schema.ConstraintStruct::axisSelector
	SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64 * ___axisSelector_1;
	// System.Collections.ArrayList System.Xml.Schema.ConstraintStruct::axisFields
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___axisFields_2;
	// System.Collections.Hashtable System.Xml.Schema.ConstraintStruct::qualifiedTable
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___qualifiedTable_3;
	// System.Collections.Hashtable System.Xml.Schema.ConstraintStruct::keyrefTable
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___keyrefTable_4;
	// System.Int32 System.Xml.Schema.ConstraintStruct::tableDim
	int32_t ___tableDim_5;

public:
	inline static int32_t get_offset_of_constraint_0() { return static_cast<int32_t>(offsetof(ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54, ___constraint_0)); }
	inline CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E * get_constraint_0() const { return ___constraint_0; }
	inline CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E ** get_address_of_constraint_0() { return &___constraint_0; }
	inline void set_constraint_0(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E * value)
	{
		___constraint_0 = value;
		Il2CppCodeGenWriteBarrier((&___constraint_0), value);
	}

	inline static int32_t get_offset_of_axisSelector_1() { return static_cast<int32_t>(offsetof(ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54, ___axisSelector_1)); }
	inline SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64 * get_axisSelector_1() const { return ___axisSelector_1; }
	inline SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64 ** get_address_of_axisSelector_1() { return &___axisSelector_1; }
	inline void set_axisSelector_1(SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64 * value)
	{
		___axisSelector_1 = value;
		Il2CppCodeGenWriteBarrier((&___axisSelector_1), value);
	}

	inline static int32_t get_offset_of_axisFields_2() { return static_cast<int32_t>(offsetof(ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54, ___axisFields_2)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_axisFields_2() const { return ___axisFields_2; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_axisFields_2() { return &___axisFields_2; }
	inline void set_axisFields_2(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___axisFields_2 = value;
		Il2CppCodeGenWriteBarrier((&___axisFields_2), value);
	}

	inline static int32_t get_offset_of_qualifiedTable_3() { return static_cast<int32_t>(offsetof(ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54, ___qualifiedTable_3)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_qualifiedTable_3() const { return ___qualifiedTable_3; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_qualifiedTable_3() { return &___qualifiedTable_3; }
	inline void set_qualifiedTable_3(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___qualifiedTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___qualifiedTable_3), value);
	}

	inline static int32_t get_offset_of_keyrefTable_4() { return static_cast<int32_t>(offsetof(ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54, ___keyrefTable_4)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_keyrefTable_4() const { return ___keyrefTable_4; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_keyrefTable_4() { return &___keyrefTable_4; }
	inline void set_keyrefTable_4(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___keyrefTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___keyrefTable_4), value);
	}

	inline static int32_t get_offset_of_tableDim_5() { return static_cast<int32_t>(offsetof(ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54, ___tableDim_5)); }
	inline int32_t get_tableDim_5() const { return ___tableDim_5; }
	inline int32_t* get_address_of_tableDim_5() { return &___tableDim_5; }
	inline void set_tableDim_5(int32_t value)
	{
		___tableDim_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTSTRUCT_T3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54_H
#ifndef SCHEMADATATYPEMAP_T9480326FA260852E226E2D1A881698CFBE35988E_H
#define SCHEMADATATYPEMAP_T9480326FA260852E226E2D1A881698CFBE35988E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap
struct  SchemaDatatypeMap_t9480326FA260852E226E2D1A881698CFBE35988E  : public RuntimeObject
{
public:
	// System.String System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap::name
	String_t* ___name_0;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap::type
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___type_1;
	// System.Int32 System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap::parentIndex
	int32_t ___parentIndex_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SchemaDatatypeMap_t9480326FA260852E226E2D1A881698CFBE35988E, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(SchemaDatatypeMap_t9480326FA260852E226E2D1A881698CFBE35988E, ___type_1)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_type_1() const { return ___type_1; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}

	inline static int32_t get_offset_of_parentIndex_2() { return static_cast<int32_t>(offsetof(SchemaDatatypeMap_t9480326FA260852E226E2D1A881698CFBE35988E, ___parentIndex_2)); }
	inline int32_t get_parentIndex_2() const { return ___parentIndex_2; }
	inline int32_t* get_address_of_parentIndex_2() { return &___parentIndex_2; }
	inline void set_parentIndex_2(int32_t value)
	{
		___parentIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMADATATYPEMAP_T9480326FA260852E226E2D1A881698CFBE35988E_H
#ifndef FORWARDAXIS_TD7F42B3CDF95D130675D57307CC20296D6829253_H
#define FORWARDAXIS_TD7F42B3CDF95D130675D57307CC20296D6829253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ForwardAxis
struct  ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253  : public RuntimeObject
{
public:
	// System.Xml.Schema.DoubleLinkAxis System.Xml.Schema.ForwardAxis::topNode
	DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * ___topNode_0;
	// System.Xml.Schema.DoubleLinkAxis System.Xml.Schema.ForwardAxis::rootNode
	DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * ___rootNode_1;
	// System.Boolean System.Xml.Schema.ForwardAxis::isAttribute
	bool ___isAttribute_2;
	// System.Boolean System.Xml.Schema.ForwardAxis::isDss
	bool ___isDss_3;
	// System.Boolean System.Xml.Schema.ForwardAxis::isSelfAxis
	bool ___isSelfAxis_4;

public:
	inline static int32_t get_offset_of_topNode_0() { return static_cast<int32_t>(offsetof(ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253, ___topNode_0)); }
	inline DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * get_topNode_0() const { return ___topNode_0; }
	inline DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 ** get_address_of_topNode_0() { return &___topNode_0; }
	inline void set_topNode_0(DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * value)
	{
		___topNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___topNode_0), value);
	}

	inline static int32_t get_offset_of_rootNode_1() { return static_cast<int32_t>(offsetof(ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253, ___rootNode_1)); }
	inline DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * get_rootNode_1() const { return ___rootNode_1; }
	inline DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 ** get_address_of_rootNode_1() { return &___rootNode_1; }
	inline void set_rootNode_1(DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * value)
	{
		___rootNode_1 = value;
		Il2CppCodeGenWriteBarrier((&___rootNode_1), value);
	}

	inline static int32_t get_offset_of_isAttribute_2() { return static_cast<int32_t>(offsetof(ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253, ___isAttribute_2)); }
	inline bool get_isAttribute_2() const { return ___isAttribute_2; }
	inline bool* get_address_of_isAttribute_2() { return &___isAttribute_2; }
	inline void set_isAttribute_2(bool value)
	{
		___isAttribute_2 = value;
	}

	inline static int32_t get_offset_of_isDss_3() { return static_cast<int32_t>(offsetof(ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253, ___isDss_3)); }
	inline bool get_isDss_3() const { return ___isDss_3; }
	inline bool* get_address_of_isDss_3() { return &___isDss_3; }
	inline void set_isDss_3(bool value)
	{
		___isDss_3 = value;
	}

	inline static int32_t get_offset_of_isSelfAxis_4() { return static_cast<int32_t>(offsetof(ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253, ___isSelfAxis_4)); }
	inline bool get_isSelfAxis_4() const { return ___isSelfAxis_4; }
	inline bool* get_address_of_isSelfAxis_4() { return &___isSelfAxis_4; }
	inline void set_isSelfAxis_4(bool value)
	{
		___isSelfAxis_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORWARDAXIS_TD7F42B3CDF95D130675D57307CC20296D6829253_H
#ifndef KSSTRUCT_TB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E_H
#define KSSTRUCT_TB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.KSStruct
struct  KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.KSStruct::depth
	int32_t ___depth_0;
	// System.Xml.Schema.KeySequence System.Xml.Schema.KSStruct::ks
	KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE * ___ks_1;
	// System.Xml.Schema.LocatedActiveAxis[] System.Xml.Schema.KSStruct::fields
	LocatedActiveAxisU5BU5D_t4148EC17C851E170B819098896BDD191CDDC41D1* ___fields_2;

public:
	inline static int32_t get_offset_of_depth_0() { return static_cast<int32_t>(offsetof(KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E, ___depth_0)); }
	inline int32_t get_depth_0() const { return ___depth_0; }
	inline int32_t* get_address_of_depth_0() { return &___depth_0; }
	inline void set_depth_0(int32_t value)
	{
		___depth_0 = value;
	}

	inline static int32_t get_offset_of_ks_1() { return static_cast<int32_t>(offsetof(KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E, ___ks_1)); }
	inline KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE * get_ks_1() const { return ___ks_1; }
	inline KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE ** get_address_of_ks_1() { return &___ks_1; }
	inline void set_ks_1(KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE * value)
	{
		___ks_1 = value;
		Il2CppCodeGenWriteBarrier((&___ks_1), value);
	}

	inline static int32_t get_offset_of_fields_2() { return static_cast<int32_t>(offsetof(KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E, ___fields_2)); }
	inline LocatedActiveAxisU5BU5D_t4148EC17C851E170B819098896BDD191CDDC41D1* get_fields_2() const { return ___fields_2; }
	inline LocatedActiveAxisU5BU5D_t4148EC17C851E170B819098896BDD191CDDC41D1** get_address_of_fields_2() { return &___fields_2; }
	inline void set_fields_2(LocatedActiveAxisU5BU5D_t4148EC17C851E170B819098896BDD191CDDC41D1* value)
	{
		___fields_2 = value;
		Il2CppCodeGenWriteBarrier((&___fields_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KSSTRUCT_TB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E_H
#ifndef KEYSEQUENCE_TEDAF2417185973D2C57AB5B1A35D379652C484BE_H
#define KEYSEQUENCE_TEDAF2417185973D2C57AB5B1A35D379652C484BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.KeySequence
struct  KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE  : public RuntimeObject
{
public:
	// System.Xml.Schema.TypedObject[] System.Xml.Schema.KeySequence::ks
	TypedObjectU5BU5D_t5F58D2C06E283EF28F0674C78DEB47DA64DF9CE1* ___ks_0;
	// System.Int32 System.Xml.Schema.KeySequence::dim
	int32_t ___dim_1;
	// System.Int32 System.Xml.Schema.KeySequence::hashcode
	int32_t ___hashcode_2;
	// System.Int32 System.Xml.Schema.KeySequence::posline
	int32_t ___posline_3;
	// System.Int32 System.Xml.Schema.KeySequence::poscol
	int32_t ___poscol_4;

public:
	inline static int32_t get_offset_of_ks_0() { return static_cast<int32_t>(offsetof(KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE, ___ks_0)); }
	inline TypedObjectU5BU5D_t5F58D2C06E283EF28F0674C78DEB47DA64DF9CE1* get_ks_0() const { return ___ks_0; }
	inline TypedObjectU5BU5D_t5F58D2C06E283EF28F0674C78DEB47DA64DF9CE1** get_address_of_ks_0() { return &___ks_0; }
	inline void set_ks_0(TypedObjectU5BU5D_t5F58D2C06E283EF28F0674C78DEB47DA64DF9CE1* value)
	{
		___ks_0 = value;
		Il2CppCodeGenWriteBarrier((&___ks_0), value);
	}

	inline static int32_t get_offset_of_dim_1() { return static_cast<int32_t>(offsetof(KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE, ___dim_1)); }
	inline int32_t get_dim_1() const { return ___dim_1; }
	inline int32_t* get_address_of_dim_1() { return &___dim_1; }
	inline void set_dim_1(int32_t value)
	{
		___dim_1 = value;
	}

	inline static int32_t get_offset_of_hashcode_2() { return static_cast<int32_t>(offsetof(KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE, ___hashcode_2)); }
	inline int32_t get_hashcode_2() const { return ___hashcode_2; }
	inline int32_t* get_address_of_hashcode_2() { return &___hashcode_2; }
	inline void set_hashcode_2(int32_t value)
	{
		___hashcode_2 = value;
	}

	inline static int32_t get_offset_of_posline_3() { return static_cast<int32_t>(offsetof(KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE, ___posline_3)); }
	inline int32_t get_posline_3() const { return ___posline_3; }
	inline int32_t* get_address_of_posline_3() { return &___posline_3; }
	inline void set_posline_3(int32_t value)
	{
		___posline_3 = value;
	}

	inline static int32_t get_offset_of_poscol_4() { return static_cast<int32_t>(offsetof(KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE, ___poscol_4)); }
	inline int32_t get_poscol_4() const { return ___poscol_4; }
	inline int32_t* get_address_of_poscol_4() { return &___poscol_4; }
	inline void set_poscol_4(int32_t value)
	{
		___poscol_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYSEQUENCE_TEDAF2417185973D2C57AB5B1A35D379652C484BE_H
#ifndef POSITIONS_T527FBF1910108E9A01D1D42495E76E7192181A9D_H
#define POSITIONS_T527FBF1910108E9A01D1D42495E76E7192181A9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Positions
struct  Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Xml.Schema.Positions::positions
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___positions_0;

public:
	inline static int32_t get_offset_of_positions_0() { return static_cast<int32_t>(offsetof(Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D, ___positions_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_positions_0() const { return ___positions_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_positions_0() { return &___positions_0; }
	inline void set_positions_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___positions_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONS_T527FBF1910108E9A01D1D42495E76E7192181A9D_H
#ifndef SYMBOLSDICTIONARY_TD5FE8542DCD1430A2E53390D10EE48FCA52227A6_H
#define SYMBOLSDICTIONARY_TD5FE8542DCD1430A2E53390D10EE48FCA52227A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SymbolsDictionary
struct  SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.SymbolsDictionary::last
	int32_t ___last_0;
	// System.Collections.Hashtable System.Xml.Schema.SymbolsDictionary::names
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___names_1;
	// System.Collections.Hashtable System.Xml.Schema.SymbolsDictionary::wildcards
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___wildcards_2;
	// System.Collections.ArrayList System.Xml.Schema.SymbolsDictionary::particles
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___particles_3;
	// System.Object System.Xml.Schema.SymbolsDictionary::particleLast
	RuntimeObject * ___particleLast_4;
	// System.Boolean System.Xml.Schema.SymbolsDictionary::isUpaEnforced
	bool ___isUpaEnforced_5;

public:
	inline static int32_t get_offset_of_last_0() { return static_cast<int32_t>(offsetof(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6, ___last_0)); }
	inline int32_t get_last_0() const { return ___last_0; }
	inline int32_t* get_address_of_last_0() { return &___last_0; }
	inline void set_last_0(int32_t value)
	{
		___last_0 = value;
	}

	inline static int32_t get_offset_of_names_1() { return static_cast<int32_t>(offsetof(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6, ___names_1)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_names_1() const { return ___names_1; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_names_1() { return &___names_1; }
	inline void set_names_1(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___names_1 = value;
		Il2CppCodeGenWriteBarrier((&___names_1), value);
	}

	inline static int32_t get_offset_of_wildcards_2() { return static_cast<int32_t>(offsetof(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6, ___wildcards_2)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_wildcards_2() const { return ___wildcards_2; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_wildcards_2() { return &___wildcards_2; }
	inline void set_wildcards_2(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___wildcards_2 = value;
		Il2CppCodeGenWriteBarrier((&___wildcards_2), value);
	}

	inline static int32_t get_offset_of_particles_3() { return static_cast<int32_t>(offsetof(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6, ___particles_3)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_particles_3() const { return ___particles_3; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_particles_3() { return &___particles_3; }
	inline void set_particles_3(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___particles_3 = value;
		Il2CppCodeGenWriteBarrier((&___particles_3), value);
	}

	inline static int32_t get_offset_of_particleLast_4() { return static_cast<int32_t>(offsetof(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6, ___particleLast_4)); }
	inline RuntimeObject * get_particleLast_4() const { return ___particleLast_4; }
	inline RuntimeObject ** get_address_of_particleLast_4() { return &___particleLast_4; }
	inline void set_particleLast_4(RuntimeObject * value)
	{
		___particleLast_4 = value;
		Il2CppCodeGenWriteBarrier((&___particleLast_4), value);
	}

	inline static int32_t get_offset_of_isUpaEnforced_5() { return static_cast<int32_t>(offsetof(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6, ___isUpaEnforced_5)); }
	inline bool get_isUpaEnforced_5() const { return ___isUpaEnforced_5; }
	inline bool* get_address_of_isUpaEnforced_5() { return &___isUpaEnforced_5; }
	inline void set_isUpaEnforced_5(bool value)
	{
		___isUpaEnforced_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMBOLSDICTIONARY_TD5FE8542DCD1430A2E53390D10EE48FCA52227A6_H
#ifndef SYNTAXTREENODE_T7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C_H
#define SYNTAXTREENODE_T7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SyntaxTreeNode
struct  SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNTAXTREENODE_T7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C_H
#ifndef TYPEDOBJECT_T4DD26D496368DF76E600AAF06AADEE3E5E49FF9D_H
#define TYPEDOBJECT_T4DD26D496368DF76E600AAF06AADEE3E5E49FF9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.TypedObject
struct  TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D  : public RuntimeObject
{
public:
	// System.Xml.Schema.TypedObject/DecimalStruct System.Xml.Schema.TypedObject::dstruct
	DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11 * ___dstruct_0;
	// System.Object System.Xml.Schema.TypedObject::ovalue
	RuntimeObject * ___ovalue_1;
	// System.String System.Xml.Schema.TypedObject::svalue
	String_t* ___svalue_2;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.TypedObject::xsdtype
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___xsdtype_3;
	// System.Int32 System.Xml.Schema.TypedObject::dim
	int32_t ___dim_4;
	// System.Boolean System.Xml.Schema.TypedObject::isList
	bool ___isList_5;

public:
	inline static int32_t get_offset_of_dstruct_0() { return static_cast<int32_t>(offsetof(TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D, ___dstruct_0)); }
	inline DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11 * get_dstruct_0() const { return ___dstruct_0; }
	inline DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11 ** get_address_of_dstruct_0() { return &___dstruct_0; }
	inline void set_dstruct_0(DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11 * value)
	{
		___dstruct_0 = value;
		Il2CppCodeGenWriteBarrier((&___dstruct_0), value);
	}

	inline static int32_t get_offset_of_ovalue_1() { return static_cast<int32_t>(offsetof(TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D, ___ovalue_1)); }
	inline RuntimeObject * get_ovalue_1() const { return ___ovalue_1; }
	inline RuntimeObject ** get_address_of_ovalue_1() { return &___ovalue_1; }
	inline void set_ovalue_1(RuntimeObject * value)
	{
		___ovalue_1 = value;
		Il2CppCodeGenWriteBarrier((&___ovalue_1), value);
	}

	inline static int32_t get_offset_of_svalue_2() { return static_cast<int32_t>(offsetof(TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D, ___svalue_2)); }
	inline String_t* get_svalue_2() const { return ___svalue_2; }
	inline String_t** get_address_of_svalue_2() { return &___svalue_2; }
	inline void set_svalue_2(String_t* value)
	{
		___svalue_2 = value;
		Il2CppCodeGenWriteBarrier((&___svalue_2), value);
	}

	inline static int32_t get_offset_of_xsdtype_3() { return static_cast<int32_t>(offsetof(TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D, ___xsdtype_3)); }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * get_xsdtype_3() const { return ___xsdtype_3; }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 ** get_address_of_xsdtype_3() { return &___xsdtype_3; }
	inline void set_xsdtype_3(XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * value)
	{
		___xsdtype_3 = value;
		Il2CppCodeGenWriteBarrier((&___xsdtype_3), value);
	}

	inline static int32_t get_offset_of_dim_4() { return static_cast<int32_t>(offsetof(TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D, ___dim_4)); }
	inline int32_t get_dim_4() const { return ___dim_4; }
	inline int32_t* get_address_of_dim_4() { return &___dim_4; }
	inline void set_dim_4(int32_t value)
	{
		___dim_4 = value;
	}

	inline static int32_t get_offset_of_isList_5() { return static_cast<int32_t>(offsetof(TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D, ___isList_5)); }
	inline bool get_isList_5() const { return ___isList_5; }
	inline bool* get_address_of_isList_5() { return &___isList_5; }
	inline void set_isList_5(bool value)
	{
		___isList_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDOBJECT_T4DD26D496368DF76E600AAF06AADEE3E5E49FF9D_H
#ifndef DECIMALSTRUCT_TD351714FCA963CEC4A7E529F0066EE965EA7DD11_H
#define DECIMALSTRUCT_TD351714FCA963CEC4A7E529F0066EE965EA7DD11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.TypedObject/DecimalStruct
struct  DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Schema.TypedObject/DecimalStruct::isDecimal
	bool ___isDecimal_0;
	// System.Decimal[] System.Xml.Schema.TypedObject/DecimalStruct::dvalue
	DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F* ___dvalue_1;

public:
	inline static int32_t get_offset_of_isDecimal_0() { return static_cast<int32_t>(offsetof(DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11, ___isDecimal_0)); }
	inline bool get_isDecimal_0() const { return ___isDecimal_0; }
	inline bool* get_address_of_isDecimal_0() { return &___isDecimal_0; }
	inline void set_isDecimal_0(bool value)
	{
		___isDecimal_0 = value;
	}

	inline static int32_t get_offset_of_dvalue_1() { return static_cast<int32_t>(offsetof(DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11, ___dvalue_1)); }
	inline DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F* get_dvalue_1() const { return ___dvalue_1; }
	inline DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F** get_address_of_dvalue_1() { return &___dvalue_1; }
	inline void set_dvalue_1(DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F* value)
	{
		___dvalue_1 = value;
		Il2CppCodeGenWriteBarrier((&___dvalue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMALSTRUCT_TD351714FCA963CEC4A7E529F0066EE965EA7DD11_H
#ifndef XMLSCHEMADATATYPE_T6D9535C4B3780086DF21646303E2350D40A5A550_H
#define XMLSCHEMADATATYPE_T6D9535C4B3780086DF21646303E2350D40A5A550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDatatype
struct  XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADATATYPE_T6D9535C4B3780086DF21646303E2350D40A5A550_H
#ifndef XSDSIMPLEVALUE_T543244559B1F2C32855BAD8CC3EDB4AD5BED96CF_H
#define XSDSIMPLEVALUE_T543244559B1F2C32855BAD8CC3EDB4AD5BED96CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdSimpleValue
struct  XsdSimpleValue_t543244559B1F2C32855BAD8CC3EDB4AD5BED96CF  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XsdSimpleValue::xmlType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___xmlType_0;
	// System.Object System.Xml.Schema.XsdSimpleValue::typedValue
	RuntimeObject * ___typedValue_1;

public:
	inline static int32_t get_offset_of_xmlType_0() { return static_cast<int32_t>(offsetof(XsdSimpleValue_t543244559B1F2C32855BAD8CC3EDB4AD5BED96CF, ___xmlType_0)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_xmlType_0() const { return ___xmlType_0; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_xmlType_0() { return &___xmlType_0; }
	inline void set_xmlType_0(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___xmlType_0 = value;
		Il2CppCodeGenWriteBarrier((&___xmlType_0), value);
	}

	inline static int32_t get_offset_of_typedValue_1() { return static_cast<int32_t>(offsetof(XsdSimpleValue_t543244559B1F2C32855BAD8CC3EDB4AD5BED96CF, ___typedValue_1)); }
	inline RuntimeObject * get_typedValue_1() const { return ___typedValue_1; }
	inline RuntimeObject ** get_address_of_typedValue_1() { return &___typedValue_1; }
	inline void set_typedValue_1(RuntimeObject * value)
	{
		___typedValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___typedValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSIMPLEVALUE_T543244559B1F2C32855BAD8CC3EDB4AD5BED96CF_H
#ifndef ENUMMAPMEMBER_T508AEADF6DA5EFA4F7B0522FA689C3A8638118E3_H
#define ENUMMAPMEMBER_T508AEADF6DA5EFA4F7B0522FA689C3A8638118E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.EnumMap/EnumMapMember
struct  EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3  : public RuntimeObject
{
public:
	// System.String System.Xml.Serialization.EnumMap/EnumMapMember::_xmlName
	String_t* ____xmlName_0;
	// System.String System.Xml.Serialization.EnumMap/EnumMapMember::_enumName
	String_t* ____enumName_1;
	// System.Int64 System.Xml.Serialization.EnumMap/EnumMapMember::_value
	int64_t ____value_2;

public:
	inline static int32_t get_offset_of__xmlName_0() { return static_cast<int32_t>(offsetof(EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3, ____xmlName_0)); }
	inline String_t* get__xmlName_0() const { return ____xmlName_0; }
	inline String_t** get_address_of__xmlName_0() { return &____xmlName_0; }
	inline void set__xmlName_0(String_t* value)
	{
		____xmlName_0 = value;
		Il2CppCodeGenWriteBarrier((&____xmlName_0), value);
	}

	inline static int32_t get_offset_of__enumName_1() { return static_cast<int32_t>(offsetof(EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3, ____enumName_1)); }
	inline String_t* get__enumName_1() const { return ____enumName_1; }
	inline String_t** get_address_of__enumName_1() { return &____enumName_1; }
	inline void set__enumName_1(String_t* value)
	{
		____enumName_1 = value;
		Il2CppCodeGenWriteBarrier((&____enumName_1), value);
	}

	inline static int32_t get_offset_of__value_2() { return static_cast<int32_t>(offsetof(EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3, ____value_2)); }
	inline int64_t get__value_2() const { return ____value_2; }
	inline int64_t* get_address_of__value_2() { return &____value_2; }
	inline void set__value_2(int64_t value)
	{
		____value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMMAPMEMBER_T508AEADF6DA5EFA4F7B0522FA689C3A8638118E3_H
#ifndef OBJECTMAP_T53CD0E614E60A73AB445A2A757CCD3C356F31677_H
#define OBJECTMAP_T53CD0E614E60A73AB445A2A757CCD3C356F31677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.ObjectMap
struct  ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTMAP_T53CD0E614E60A73AB445A2A757CCD3C356F31677_H
#ifndef XMLSERIALIZATIONGENERATEDCODE_TD121073E4460FCF7F6B549E4621B0CB971D2E6CE_H
#define XMLSERIALIZATIONGENERATEDCODE_TD121073E4460FCF7F6B549E4621B0CB971D2E6CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationGeneratedCode
struct  XmlSerializationGeneratedCode_tD121073E4460FCF7F6B549E4621B0CB971D2E6CE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZATIONGENERATEDCODE_TD121073E4460FCF7F6B549E4621B0CB971D2E6CE_H
#ifndef COLLECTIONFIXUP_T2C3A0520CE6E7EC1C69E8A9B8EDED9E281097948_H
#define COLLECTIONFIXUP_T2C3A0520CE6E7EC1C69E8A9B8EDED9E281097948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationReader/CollectionFixup
struct  CollectionFixup_t2C3A0520CE6E7EC1C69E8A9B8EDED9E281097948  : public RuntimeObject
{
public:
	// System.Xml.Serialization.XmlSerializationCollectionFixupCallback System.Xml.Serialization.XmlSerializationReader/CollectionFixup::callback
	XmlSerializationCollectionFixupCallback_t3C0601557C988CCF12827291EB63B51F687463FB * ___callback_0;
	// System.Object System.Xml.Serialization.XmlSerializationReader/CollectionFixup::collection
	RuntimeObject * ___collection_1;
	// System.Object System.Xml.Serialization.XmlSerializationReader/CollectionFixup::collectionItems
	RuntimeObject * ___collectionItems_2;
	// System.String System.Xml.Serialization.XmlSerializationReader/CollectionFixup::id
	String_t* ___id_3;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(CollectionFixup_t2C3A0520CE6E7EC1C69E8A9B8EDED9E281097948, ___callback_0)); }
	inline XmlSerializationCollectionFixupCallback_t3C0601557C988CCF12827291EB63B51F687463FB * get_callback_0() const { return ___callback_0; }
	inline XmlSerializationCollectionFixupCallback_t3C0601557C988CCF12827291EB63B51F687463FB ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(XmlSerializationCollectionFixupCallback_t3C0601557C988CCF12827291EB63B51F687463FB * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_collection_1() { return static_cast<int32_t>(offsetof(CollectionFixup_t2C3A0520CE6E7EC1C69E8A9B8EDED9E281097948, ___collection_1)); }
	inline RuntimeObject * get_collection_1() const { return ___collection_1; }
	inline RuntimeObject ** get_address_of_collection_1() { return &___collection_1; }
	inline void set_collection_1(RuntimeObject * value)
	{
		___collection_1 = value;
		Il2CppCodeGenWriteBarrier((&___collection_1), value);
	}

	inline static int32_t get_offset_of_collectionItems_2() { return static_cast<int32_t>(offsetof(CollectionFixup_t2C3A0520CE6E7EC1C69E8A9B8EDED9E281097948, ___collectionItems_2)); }
	inline RuntimeObject * get_collectionItems_2() const { return ___collectionItems_2; }
	inline RuntimeObject ** get_address_of_collectionItems_2() { return &___collectionItems_2; }
	inline void set_collectionItems_2(RuntimeObject * value)
	{
		___collectionItems_2 = value;
		Il2CppCodeGenWriteBarrier((&___collectionItems_2), value);
	}

	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(CollectionFixup_t2C3A0520CE6E7EC1C69E8A9B8EDED9E281097948, ___id_3)); }
	inline String_t* get_id_3() const { return ___id_3; }
	inline String_t** get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(String_t* value)
	{
		___id_3 = value;
		Il2CppCodeGenWriteBarrier((&___id_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONFIXUP_T2C3A0520CE6E7EC1C69E8A9B8EDED9E281097948_H
#ifndef COLLECTIONITEMFIXUP_T8E53F773DA567B6109BE49A8CC3F85A349055569_H
#define COLLECTIONITEMFIXUP_T8E53F773DA567B6109BE49A8CC3F85A349055569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationReader/CollectionItemFixup
struct  CollectionItemFixup_t8E53F773DA567B6109BE49A8CC3F85A349055569  : public RuntimeObject
{
public:
	// System.Array System.Xml.Serialization.XmlSerializationReader/CollectionItemFixup::list
	RuntimeArray * ___list_0;
	// System.Int32 System.Xml.Serialization.XmlSerializationReader/CollectionItemFixup::index
	int32_t ___index_1;
	// System.String System.Xml.Serialization.XmlSerializationReader/CollectionItemFixup::id
	String_t* ___id_2;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionItemFixup_t8E53F773DA567B6109BE49A8CC3F85A349055569, ___list_0)); }
	inline RuntimeArray * get_list_0() const { return ___list_0; }
	inline RuntimeArray ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeArray * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(CollectionItemFixup_t8E53F773DA567B6109BE49A8CC3F85A349055569, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(CollectionItemFixup_t8E53F773DA567B6109BE49A8CC3F85A349055569, ___id_2)); }
	inline String_t* get_id_2() const { return ___id_2; }
	inline String_t** get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(String_t* value)
	{
		___id_2 = value;
		Il2CppCodeGenWriteBarrier((&___id_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONITEMFIXUP_T8E53F773DA567B6109BE49A8CC3F85A349055569_H
#ifndef FIXUP_TA77CFD3F2CFCEC1D6A71BB69AC3C98AFC9883EDF_H
#define FIXUP_TA77CFD3F2CFCEC1D6A71BB69AC3C98AFC9883EDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationReader/Fixup
struct  Fixup_tA77CFD3F2CFCEC1D6A71BB69AC3C98AFC9883EDF  : public RuntimeObject
{
public:
	// System.Object System.Xml.Serialization.XmlSerializationReader/Fixup::source
	RuntimeObject * ___source_0;
	// System.String[] System.Xml.Serialization.XmlSerializationReader/Fixup::ids
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___ids_1;
	// System.Xml.Serialization.XmlSerializationFixupCallback System.Xml.Serialization.XmlSerializationReader/Fixup::callback
	XmlSerializationFixupCallback_t044B3CA7B13D386365F0F40BEB656A8B0370C800 * ___callback_2;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(Fixup_tA77CFD3F2CFCEC1D6A71BB69AC3C98AFC9883EDF, ___source_0)); }
	inline RuntimeObject * get_source_0() const { return ___source_0; }
	inline RuntimeObject ** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(RuntimeObject * value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}

	inline static int32_t get_offset_of_ids_1() { return static_cast<int32_t>(offsetof(Fixup_tA77CFD3F2CFCEC1D6A71BB69AC3C98AFC9883EDF, ___ids_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_ids_1() const { return ___ids_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_ids_1() { return &___ids_1; }
	inline void set_ids_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___ids_1 = value;
		Il2CppCodeGenWriteBarrier((&___ids_1), value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(Fixup_tA77CFD3F2CFCEC1D6A71BB69AC3C98AFC9883EDF, ___callback_2)); }
	inline XmlSerializationFixupCallback_t044B3CA7B13D386365F0F40BEB656A8B0370C800 * get_callback_2() const { return ___callback_2; }
	inline XmlSerializationFixupCallback_t044B3CA7B13D386365F0F40BEB656A8B0370C800 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(XmlSerializationFixupCallback_t044B3CA7B13D386365F0F40BEB656A8B0370C800 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXUP_TA77CFD3F2CFCEC1D6A71BB69AC3C98AFC9883EDF_H
#ifndef WRITECALLBACKINFO_T05B6DF593FD8E2A5F05B109D252A4AD76EE3AF85_H
#define WRITECALLBACKINFO_T05B6DF593FD8E2A5F05B109D252A4AD76EE3AF85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationReader/WriteCallbackInfo
struct  WriteCallbackInfo_t05B6DF593FD8E2A5F05B109D252A4AD76EE3AF85  : public RuntimeObject
{
public:
	// System.Type System.Xml.Serialization.XmlSerializationReader/WriteCallbackInfo::Type
	Type_t * ___Type_0;
	// System.String System.Xml.Serialization.XmlSerializationReader/WriteCallbackInfo::TypeName
	String_t* ___TypeName_1;
	// System.String System.Xml.Serialization.XmlSerializationReader/WriteCallbackInfo::TypeNs
	String_t* ___TypeNs_2;
	// System.Xml.Serialization.XmlSerializationReadCallback System.Xml.Serialization.XmlSerializationReader/WriteCallbackInfo::Callback
	XmlSerializationReadCallback_tB4AF7384FD4DBE50B3CCC5C267F2C3CD04F22443 * ___Callback_3;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(WriteCallbackInfo_t05B6DF593FD8E2A5F05B109D252A4AD76EE3AF85, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier((&___Type_0), value);
	}

	inline static int32_t get_offset_of_TypeName_1() { return static_cast<int32_t>(offsetof(WriteCallbackInfo_t05B6DF593FD8E2A5F05B109D252A4AD76EE3AF85, ___TypeName_1)); }
	inline String_t* get_TypeName_1() const { return ___TypeName_1; }
	inline String_t** get_address_of_TypeName_1() { return &___TypeName_1; }
	inline void set_TypeName_1(String_t* value)
	{
		___TypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeName_1), value);
	}

	inline static int32_t get_offset_of_TypeNs_2() { return static_cast<int32_t>(offsetof(WriteCallbackInfo_t05B6DF593FD8E2A5F05B109D252A4AD76EE3AF85, ___TypeNs_2)); }
	inline String_t* get_TypeNs_2() const { return ___TypeNs_2; }
	inline String_t** get_address_of_TypeNs_2() { return &___TypeNs_2; }
	inline void set_TypeNs_2(String_t* value)
	{
		___TypeNs_2 = value;
		Il2CppCodeGenWriteBarrier((&___TypeNs_2), value);
	}

	inline static int32_t get_offset_of_Callback_3() { return static_cast<int32_t>(offsetof(WriteCallbackInfo_t05B6DF593FD8E2A5F05B109D252A4AD76EE3AF85, ___Callback_3)); }
	inline XmlSerializationReadCallback_tB4AF7384FD4DBE50B3CCC5C267F2C3CD04F22443 * get_Callback_3() const { return ___Callback_3; }
	inline XmlSerializationReadCallback_tB4AF7384FD4DBE50B3CCC5C267F2C3CD04F22443 ** get_address_of_Callback_3() { return &___Callback_3; }
	inline void set_Callback_3(XmlSerializationReadCallback_tB4AF7384FD4DBE50B3CCC5C267F2C3CD04F22443 * value)
	{
		___Callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___Callback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITECALLBACKINFO_T05B6DF593FD8E2A5F05B109D252A4AD76EE3AF85_H
#ifndef FIXUPCALLBACKINFO_T624A8800237A1808944906FDC24BE9244B65F71D_H
#define FIXUPCALLBACKINFO_T624A8800237A1808944906FDC24BE9244B65F71D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationReaderInterpreter/FixupCallbackInfo
struct  FixupCallbackInfo_t624A8800237A1808944906FDC24BE9244B65F71D  : public RuntimeObject
{
public:
	// System.Xml.Serialization.XmlSerializationReaderInterpreter System.Xml.Serialization.XmlSerializationReaderInterpreter/FixupCallbackInfo::_sri
	XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C * ____sri_0;
	// System.Xml.Serialization.ClassMap System.Xml.Serialization.XmlSerializationReaderInterpreter/FixupCallbackInfo::_map
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA * ____map_1;
	// System.Boolean System.Xml.Serialization.XmlSerializationReaderInterpreter/FixupCallbackInfo::_isValueList
	bool ____isValueList_2;

public:
	inline static int32_t get_offset_of__sri_0() { return static_cast<int32_t>(offsetof(FixupCallbackInfo_t624A8800237A1808944906FDC24BE9244B65F71D, ____sri_0)); }
	inline XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C * get__sri_0() const { return ____sri_0; }
	inline XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C ** get_address_of__sri_0() { return &____sri_0; }
	inline void set__sri_0(XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C * value)
	{
		____sri_0 = value;
		Il2CppCodeGenWriteBarrier((&____sri_0), value);
	}

	inline static int32_t get_offset_of__map_1() { return static_cast<int32_t>(offsetof(FixupCallbackInfo_t624A8800237A1808944906FDC24BE9244B65F71D, ____map_1)); }
	inline ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA * get__map_1() const { return ____map_1; }
	inline ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA ** get_address_of__map_1() { return &____map_1; }
	inline void set__map_1(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA * value)
	{
		____map_1 = value;
		Il2CppCodeGenWriteBarrier((&____map_1), value);
	}

	inline static int32_t get_offset_of__isValueList_2() { return static_cast<int32_t>(offsetof(FixupCallbackInfo_t624A8800237A1808944906FDC24BE9244B65F71D, ____isValueList_2)); }
	inline bool get__isValueList_2() const { return ____isValueList_2; }
	inline bool* get_address_of__isValueList_2() { return &____isValueList_2; }
	inline void set__isValueList_2(bool value)
	{
		____isValueList_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXUPCALLBACKINFO_T624A8800237A1808944906FDC24BE9244B65F71D_H
#ifndef READERCALLBACKINFO_T3649059057730362A2C3655BD0E4024E8DDC9C61_H
#define READERCALLBACKINFO_T3649059057730362A2C3655BD0E4024E8DDC9C61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationReaderInterpreter/ReaderCallbackInfo
struct  ReaderCallbackInfo_t3649059057730362A2C3655BD0E4024E8DDC9C61  : public RuntimeObject
{
public:
	// System.Xml.Serialization.XmlSerializationReaderInterpreter System.Xml.Serialization.XmlSerializationReaderInterpreter/ReaderCallbackInfo::_sri
	XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C * ____sri_0;
	// System.Xml.Serialization.XmlTypeMapping System.Xml.Serialization.XmlSerializationReaderInterpreter/ReaderCallbackInfo::_typeMap
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * ____typeMap_1;

public:
	inline static int32_t get_offset_of__sri_0() { return static_cast<int32_t>(offsetof(ReaderCallbackInfo_t3649059057730362A2C3655BD0E4024E8DDC9C61, ____sri_0)); }
	inline XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C * get__sri_0() const { return ____sri_0; }
	inline XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C ** get_address_of__sri_0() { return &____sri_0; }
	inline void set__sri_0(XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C * value)
	{
		____sri_0 = value;
		Il2CppCodeGenWriteBarrier((&____sri_0), value);
	}

	inline static int32_t get_offset_of__typeMap_1() { return static_cast<int32_t>(offsetof(ReaderCallbackInfo_t3649059057730362A2C3655BD0E4024E8DDC9C61, ____typeMap_1)); }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * get__typeMap_1() const { return ____typeMap_1; }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA ** get_address_of__typeMap_1() { return &____typeMap_1; }
	inline void set__typeMap_1(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * value)
	{
		____typeMap_1 = value;
		Il2CppCodeGenWriteBarrier((&____typeMap_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READERCALLBACKINFO_T3649059057730362A2C3655BD0E4024E8DDC9C61_H
#ifndef WRITECALLBACKINFO_T20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC_H
#define WRITECALLBACKINFO_T20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationWriter/WriteCallbackInfo
struct  WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC  : public RuntimeObject
{
public:
	// System.Type System.Xml.Serialization.XmlSerializationWriter/WriteCallbackInfo::Type
	Type_t * ___Type_0;
	// System.String System.Xml.Serialization.XmlSerializationWriter/WriteCallbackInfo::TypeName
	String_t* ___TypeName_1;
	// System.String System.Xml.Serialization.XmlSerializationWriter/WriteCallbackInfo::TypeNs
	String_t* ___TypeNs_2;
	// System.Xml.Serialization.XmlSerializationWriteCallback System.Xml.Serialization.XmlSerializationWriter/WriteCallbackInfo::Callback
	XmlSerializationWriteCallback_tC98B104DBDBC776D566F853B9DED7668089431FA * ___Callback_3;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier((&___Type_0), value);
	}

	inline static int32_t get_offset_of_TypeName_1() { return static_cast<int32_t>(offsetof(WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC, ___TypeName_1)); }
	inline String_t* get_TypeName_1() const { return ___TypeName_1; }
	inline String_t** get_address_of_TypeName_1() { return &___TypeName_1; }
	inline void set_TypeName_1(String_t* value)
	{
		___TypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeName_1), value);
	}

	inline static int32_t get_offset_of_TypeNs_2() { return static_cast<int32_t>(offsetof(WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC, ___TypeNs_2)); }
	inline String_t* get_TypeNs_2() const { return ___TypeNs_2; }
	inline String_t** get_address_of_TypeNs_2() { return &___TypeNs_2; }
	inline void set_TypeNs_2(String_t* value)
	{
		___TypeNs_2 = value;
		Il2CppCodeGenWriteBarrier((&___TypeNs_2), value);
	}

	inline static int32_t get_offset_of_Callback_3() { return static_cast<int32_t>(offsetof(WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC, ___Callback_3)); }
	inline XmlSerializationWriteCallback_tC98B104DBDBC776D566F853B9DED7668089431FA * get_Callback_3() const { return ___Callback_3; }
	inline XmlSerializationWriteCallback_tC98B104DBDBC776D566F853B9DED7668089431FA ** get_address_of_Callback_3() { return &___Callback_3; }
	inline void set_Callback_3(XmlSerializationWriteCallback_tC98B104DBDBC776D566F853B9DED7668089431FA * value)
	{
		___Callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___Callback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITECALLBACKINFO_T20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC_H
#ifndef CALLBACKINFO_T6FC419FB131647181747C58C4AA7642E18071292_H
#define CALLBACKINFO_T6FC419FB131647181747C58C4AA7642E18071292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationWriterInterpreter/CallbackInfo
struct  CallbackInfo_t6FC419FB131647181747C58C4AA7642E18071292  : public RuntimeObject
{
public:
	// System.Xml.Serialization.XmlSerializationWriterInterpreter System.Xml.Serialization.XmlSerializationWriterInterpreter/CallbackInfo::_swi
	XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221 * ____swi_0;
	// System.Xml.Serialization.XmlTypeMapping System.Xml.Serialization.XmlSerializationWriterInterpreter/CallbackInfo::_typeMap
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * ____typeMap_1;

public:
	inline static int32_t get_offset_of__swi_0() { return static_cast<int32_t>(offsetof(CallbackInfo_t6FC419FB131647181747C58C4AA7642E18071292, ____swi_0)); }
	inline XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221 * get__swi_0() const { return ____swi_0; }
	inline XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221 ** get_address_of__swi_0() { return &____swi_0; }
	inline void set__swi_0(XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221 * value)
	{
		____swi_0 = value;
		Il2CppCodeGenWriteBarrier((&____swi_0), value);
	}

	inline static int32_t get_offset_of__typeMap_1() { return static_cast<int32_t>(offsetof(CallbackInfo_t6FC419FB131647181747C58C4AA7642E18071292, ____typeMap_1)); }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * get__typeMap_1() const { return ____typeMap_1; }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA ** get_address_of__typeMap_1() { return &____typeMap_1; }
	inline void set__typeMap_1(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * value)
	{
		____typeMap_1 = value;
		Il2CppCodeGenWriteBarrier((&____typeMap_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKINFO_T6FC419FB131647181747C58C4AA7642E18071292_H
#ifndef XMLSERIALIZER_T01B5C784057A14663B4041A53328764327555D5C_H
#define XMLSERIALIZER_T01B5C784057A14663B4041A53328764327555D5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializer
struct  XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Serialization.XmlSerializer::customSerializer
	bool ___customSerializer_4;
	// System.Xml.Serialization.XmlMapping System.Xml.Serialization.XmlSerializer::typeMapping
	XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 * ___typeMapping_5;
	// System.Xml.Serialization.XmlSerializer/SerializerData System.Xml.Serialization.XmlSerializer::serializerData
	SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8 * ___serializerData_6;
	// System.Xml.Serialization.UnreferencedObjectEventHandler System.Xml.Serialization.XmlSerializer::onUnreferencedObject
	UnreferencedObjectEventHandler_t43A5BBC5B5EC952973CF7F9F191EAFF5C5DBCB9B * ___onUnreferencedObject_8;
	// System.Xml.Serialization.XmlAttributeEventHandler System.Xml.Serialization.XmlSerializer::onUnknownAttribute
	XmlAttributeEventHandler_t7DE5648EC8D9459D5DEA07DF556AFDF235C661A6 * ___onUnknownAttribute_9;
	// System.Xml.Serialization.XmlElementEventHandler System.Xml.Serialization.XmlSerializer::onUnknownElement
	XmlElementEventHandler_tF116A337C283F6E1ECC94728C6F113676D9A8174 * ___onUnknownElement_10;
	// System.Xml.Serialization.XmlNodeEventHandler System.Xml.Serialization.XmlSerializer::onUnknownNode
	XmlNodeEventHandler_tD870BE5167E49CD15CA2F1879CD1EC0229C40F49 * ___onUnknownNode_11;

public:
	inline static int32_t get_offset_of_customSerializer_4() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C, ___customSerializer_4)); }
	inline bool get_customSerializer_4() const { return ___customSerializer_4; }
	inline bool* get_address_of_customSerializer_4() { return &___customSerializer_4; }
	inline void set_customSerializer_4(bool value)
	{
		___customSerializer_4 = value;
	}

	inline static int32_t get_offset_of_typeMapping_5() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C, ___typeMapping_5)); }
	inline XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 * get_typeMapping_5() const { return ___typeMapping_5; }
	inline XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 ** get_address_of_typeMapping_5() { return &___typeMapping_5; }
	inline void set_typeMapping_5(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 * value)
	{
		___typeMapping_5 = value;
		Il2CppCodeGenWriteBarrier((&___typeMapping_5), value);
	}

	inline static int32_t get_offset_of_serializerData_6() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C, ___serializerData_6)); }
	inline SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8 * get_serializerData_6() const { return ___serializerData_6; }
	inline SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8 ** get_address_of_serializerData_6() { return &___serializerData_6; }
	inline void set_serializerData_6(SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8 * value)
	{
		___serializerData_6 = value;
		Il2CppCodeGenWriteBarrier((&___serializerData_6), value);
	}

	inline static int32_t get_offset_of_onUnreferencedObject_8() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C, ___onUnreferencedObject_8)); }
	inline UnreferencedObjectEventHandler_t43A5BBC5B5EC952973CF7F9F191EAFF5C5DBCB9B * get_onUnreferencedObject_8() const { return ___onUnreferencedObject_8; }
	inline UnreferencedObjectEventHandler_t43A5BBC5B5EC952973CF7F9F191EAFF5C5DBCB9B ** get_address_of_onUnreferencedObject_8() { return &___onUnreferencedObject_8; }
	inline void set_onUnreferencedObject_8(UnreferencedObjectEventHandler_t43A5BBC5B5EC952973CF7F9F191EAFF5C5DBCB9B * value)
	{
		___onUnreferencedObject_8 = value;
		Il2CppCodeGenWriteBarrier((&___onUnreferencedObject_8), value);
	}

	inline static int32_t get_offset_of_onUnknownAttribute_9() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C, ___onUnknownAttribute_9)); }
	inline XmlAttributeEventHandler_t7DE5648EC8D9459D5DEA07DF556AFDF235C661A6 * get_onUnknownAttribute_9() const { return ___onUnknownAttribute_9; }
	inline XmlAttributeEventHandler_t7DE5648EC8D9459D5DEA07DF556AFDF235C661A6 ** get_address_of_onUnknownAttribute_9() { return &___onUnknownAttribute_9; }
	inline void set_onUnknownAttribute_9(XmlAttributeEventHandler_t7DE5648EC8D9459D5DEA07DF556AFDF235C661A6 * value)
	{
		___onUnknownAttribute_9 = value;
		Il2CppCodeGenWriteBarrier((&___onUnknownAttribute_9), value);
	}

	inline static int32_t get_offset_of_onUnknownElement_10() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C, ___onUnknownElement_10)); }
	inline XmlElementEventHandler_tF116A337C283F6E1ECC94728C6F113676D9A8174 * get_onUnknownElement_10() const { return ___onUnknownElement_10; }
	inline XmlElementEventHandler_tF116A337C283F6E1ECC94728C6F113676D9A8174 ** get_address_of_onUnknownElement_10() { return &___onUnknownElement_10; }
	inline void set_onUnknownElement_10(XmlElementEventHandler_tF116A337C283F6E1ECC94728C6F113676D9A8174 * value)
	{
		___onUnknownElement_10 = value;
		Il2CppCodeGenWriteBarrier((&___onUnknownElement_10), value);
	}

	inline static int32_t get_offset_of_onUnknownNode_11() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C, ___onUnknownNode_11)); }
	inline XmlNodeEventHandler_tD870BE5167E49CD15CA2F1879CD1EC0229C40F49 * get_onUnknownNode_11() const { return ___onUnknownNode_11; }
	inline XmlNodeEventHandler_tD870BE5167E49CD15CA2F1879CD1EC0229C40F49 ** get_address_of_onUnknownNode_11() { return &___onUnknownNode_11; }
	inline void set_onUnknownNode_11(XmlNodeEventHandler_tD870BE5167E49CD15CA2F1879CD1EC0229C40F49 * value)
	{
		___onUnknownNode_11 = value;
		Il2CppCodeGenWriteBarrier((&___onUnknownNode_11), value);
	}
};

struct XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields
{
public:
	// System.Int32 System.Xml.Serialization.XmlSerializer::generationThreshold
	int32_t ___generationThreshold_0;
	// System.Boolean System.Xml.Serialization.XmlSerializer::backgroundGeneration
	bool ___backgroundGeneration_1;
	// System.Boolean System.Xml.Serialization.XmlSerializer::deleteTempFiles
	bool ___deleteTempFiles_2;
	// System.Boolean System.Xml.Serialization.XmlSerializer::generatorFallback
	bool ___generatorFallback_3;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializer::serializerTypes
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___serializerTypes_7;
	// System.Text.Encoding System.Xml.Serialization.XmlSerializer::DefaultEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___DefaultEncoding_12;

public:
	inline static int32_t get_offset_of_generationThreshold_0() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields, ___generationThreshold_0)); }
	inline int32_t get_generationThreshold_0() const { return ___generationThreshold_0; }
	inline int32_t* get_address_of_generationThreshold_0() { return &___generationThreshold_0; }
	inline void set_generationThreshold_0(int32_t value)
	{
		___generationThreshold_0 = value;
	}

	inline static int32_t get_offset_of_backgroundGeneration_1() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields, ___backgroundGeneration_1)); }
	inline bool get_backgroundGeneration_1() const { return ___backgroundGeneration_1; }
	inline bool* get_address_of_backgroundGeneration_1() { return &___backgroundGeneration_1; }
	inline void set_backgroundGeneration_1(bool value)
	{
		___backgroundGeneration_1 = value;
	}

	inline static int32_t get_offset_of_deleteTempFiles_2() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields, ___deleteTempFiles_2)); }
	inline bool get_deleteTempFiles_2() const { return ___deleteTempFiles_2; }
	inline bool* get_address_of_deleteTempFiles_2() { return &___deleteTempFiles_2; }
	inline void set_deleteTempFiles_2(bool value)
	{
		___deleteTempFiles_2 = value;
	}

	inline static int32_t get_offset_of_generatorFallback_3() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields, ___generatorFallback_3)); }
	inline bool get_generatorFallback_3() const { return ___generatorFallback_3; }
	inline bool* get_address_of_generatorFallback_3() { return &___generatorFallback_3; }
	inline void set_generatorFallback_3(bool value)
	{
		___generatorFallback_3 = value;
	}

	inline static int32_t get_offset_of_serializerTypes_7() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields, ___serializerTypes_7)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_serializerTypes_7() const { return ___serializerTypes_7; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_serializerTypes_7() { return &___serializerTypes_7; }
	inline void set_serializerTypes_7(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___serializerTypes_7 = value;
		Il2CppCodeGenWriteBarrier((&___serializerTypes_7), value);
	}

	inline static int32_t get_offset_of_DefaultEncoding_12() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields, ___DefaultEncoding_12)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_DefaultEncoding_12() const { return ___DefaultEncoding_12; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_DefaultEncoding_12() { return &___DefaultEncoding_12; }
	inline void set_DefaultEncoding_12(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___DefaultEncoding_12 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultEncoding_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZER_T01B5C784057A14663B4041A53328764327555D5C_H
#ifndef SERIALIZERDATA_TFD8E75C5BC50CD6C3A3F382789E88E422381A7A8_H
#define SERIALIZERDATA_TFD8E75C5BC50CD6C3A3F382789E88E422381A7A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializer/SerializerData
struct  SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo System.Xml.Serialization.XmlSerializer/SerializerData::ReaderMethod
	MethodInfo_t * ___ReaderMethod_0;
	// System.Type System.Xml.Serialization.XmlSerializer/SerializerData::WriterType
	Type_t * ___WriterType_1;
	// System.Reflection.MethodInfo System.Xml.Serialization.XmlSerializer/SerializerData::WriterMethod
	MethodInfo_t * ___WriterMethod_2;
	// System.Xml.Serialization.XmlSerializerImplementation System.Xml.Serialization.XmlSerializer/SerializerData::Implementation
	XmlSerializerImplementation_tF78BC93462D4003D83EAA821331747A0624F837D * ___Implementation_3;

public:
	inline static int32_t get_offset_of_ReaderMethod_0() { return static_cast<int32_t>(offsetof(SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8, ___ReaderMethod_0)); }
	inline MethodInfo_t * get_ReaderMethod_0() const { return ___ReaderMethod_0; }
	inline MethodInfo_t ** get_address_of_ReaderMethod_0() { return &___ReaderMethod_0; }
	inline void set_ReaderMethod_0(MethodInfo_t * value)
	{
		___ReaderMethod_0 = value;
		Il2CppCodeGenWriteBarrier((&___ReaderMethod_0), value);
	}

	inline static int32_t get_offset_of_WriterType_1() { return static_cast<int32_t>(offsetof(SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8, ___WriterType_1)); }
	inline Type_t * get_WriterType_1() const { return ___WriterType_1; }
	inline Type_t ** get_address_of_WriterType_1() { return &___WriterType_1; }
	inline void set_WriterType_1(Type_t * value)
	{
		___WriterType_1 = value;
		Il2CppCodeGenWriteBarrier((&___WriterType_1), value);
	}

	inline static int32_t get_offset_of_WriterMethod_2() { return static_cast<int32_t>(offsetof(SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8, ___WriterMethod_2)); }
	inline MethodInfo_t * get_WriterMethod_2() const { return ___WriterMethod_2; }
	inline MethodInfo_t ** get_address_of_WriterMethod_2() { return &___WriterMethod_2; }
	inline void set_WriterMethod_2(MethodInfo_t * value)
	{
		___WriterMethod_2 = value;
		Il2CppCodeGenWriteBarrier((&___WriterMethod_2), value);
	}

	inline static int32_t get_offset_of_Implementation_3() { return static_cast<int32_t>(offsetof(SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8, ___Implementation_3)); }
	inline XmlSerializerImplementation_tF78BC93462D4003D83EAA821331747A0624F837D * get_Implementation_3() const { return ___Implementation_3; }
	inline XmlSerializerImplementation_tF78BC93462D4003D83EAA821331747A0624F837D ** get_address_of_Implementation_3() { return &___Implementation_3; }
	inline void set_Implementation_3(XmlSerializerImplementation_tF78BC93462D4003D83EAA821331747A0624F837D * value)
	{
		___Implementation_3 = value;
		Il2CppCodeGenWriteBarrier((&___Implementation_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZERDATA_TFD8E75C5BC50CD6C3A3F382789E88E422381A7A8_H
#ifndef XMLSERIALIZERFACTORY_TEAB3633D18CDF075A1EC73FFDA7132978F308C74_H
#define XMLSERIALIZERFACTORY_TEAB3633D18CDF075A1EC73FFDA7132978F308C74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializerFactory
struct  XmlSerializerFactory_tEAB3633D18CDF075A1EC73FFDA7132978F308C74  : public RuntimeObject
{
public:

public:
};

struct XmlSerializerFactory_tEAB3633D18CDF075A1EC73FFDA7132978F308C74_StaticFields
{
public:
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializerFactory::serializersBySource
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___serializersBySource_0;

public:
	inline static int32_t get_offset_of_serializersBySource_0() { return static_cast<int32_t>(offsetof(XmlSerializerFactory_tEAB3633D18CDF075A1EC73FFDA7132978F308C74_StaticFields, ___serializersBySource_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_serializersBySource_0() const { return ___serializersBySource_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_serializersBySource_0() { return &___serializersBySource_0; }
	inline void set_serializersBySource_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___serializersBySource_0 = value;
		Il2CppCodeGenWriteBarrier((&___serializersBySource_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZERFACTORY_TEAB3633D18CDF075A1EC73FFDA7132978F308C74_H
#ifndef XMLSERIALIZERIMPLEMENTATION_TF78BC93462D4003D83EAA821331747A0624F837D_H
#define XMLSERIALIZERIMPLEMENTATION_TF78BC93462D4003D83EAA821331747A0624F837D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializerImplementation
struct  XmlSerializerImplementation_tF78BC93462D4003D83EAA821331747A0624F837D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZERIMPLEMENTATION_TF78BC93462D4003D83EAA821331747A0624F837D_H
#ifndef XMLTYPEMAPMEMBER_T8272D1E62F842F97D749F6FFC6B4AE57754C914D_H
#define XMLTYPEMAPMEMBER_T8272D1E62F842F97D749F6FFC6B4AE57754C914D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMember
struct  XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D  : public RuntimeObject
{
public:
	// System.String System.Xml.Serialization.XmlTypeMapMember::_name
	String_t* ____name_0;
	// System.Int32 System.Xml.Serialization.XmlTypeMapMember::_index
	int32_t ____index_1;
	// System.Int32 System.Xml.Serialization.XmlTypeMapMember::_globalIndex
	int32_t ____globalIndex_2;
	// System.Int32 System.Xml.Serialization.XmlTypeMapMember::_specifiedGlobalIndex
	int32_t ____specifiedGlobalIndex_3;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.XmlTypeMapMember::_typeData
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * ____typeData_4;
	// System.Reflection.MemberInfo System.Xml.Serialization.XmlTypeMapMember::_member
	MemberInfo_t * ____member_5;
	// System.Reflection.MemberInfo System.Xml.Serialization.XmlTypeMapMember::_specifiedMember
	MemberInfo_t * ____specifiedMember_6;
	// System.Reflection.MethodInfo System.Xml.Serialization.XmlTypeMapMember::_shouldSerialize
	MethodInfo_t * ____shouldSerialize_7;
	// System.Object System.Xml.Serialization.XmlTypeMapMember::_defaultValue
	RuntimeObject * ____defaultValue_8;
	// System.Int32 System.Xml.Serialization.XmlTypeMapMember::_flags
	int32_t ____flags_9;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__globalIndex_2() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____globalIndex_2)); }
	inline int32_t get__globalIndex_2() const { return ____globalIndex_2; }
	inline int32_t* get_address_of__globalIndex_2() { return &____globalIndex_2; }
	inline void set__globalIndex_2(int32_t value)
	{
		____globalIndex_2 = value;
	}

	inline static int32_t get_offset_of__specifiedGlobalIndex_3() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____specifiedGlobalIndex_3)); }
	inline int32_t get__specifiedGlobalIndex_3() const { return ____specifiedGlobalIndex_3; }
	inline int32_t* get_address_of__specifiedGlobalIndex_3() { return &____specifiedGlobalIndex_3; }
	inline void set__specifiedGlobalIndex_3(int32_t value)
	{
		____specifiedGlobalIndex_3 = value;
	}

	inline static int32_t get_offset_of__typeData_4() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____typeData_4)); }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * get__typeData_4() const { return ____typeData_4; }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 ** get_address_of__typeData_4() { return &____typeData_4; }
	inline void set__typeData_4(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * value)
	{
		____typeData_4 = value;
		Il2CppCodeGenWriteBarrier((&____typeData_4), value);
	}

	inline static int32_t get_offset_of__member_5() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____member_5)); }
	inline MemberInfo_t * get__member_5() const { return ____member_5; }
	inline MemberInfo_t ** get_address_of__member_5() { return &____member_5; }
	inline void set__member_5(MemberInfo_t * value)
	{
		____member_5 = value;
		Il2CppCodeGenWriteBarrier((&____member_5), value);
	}

	inline static int32_t get_offset_of__specifiedMember_6() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____specifiedMember_6)); }
	inline MemberInfo_t * get__specifiedMember_6() const { return ____specifiedMember_6; }
	inline MemberInfo_t ** get_address_of__specifiedMember_6() { return &____specifiedMember_6; }
	inline void set__specifiedMember_6(MemberInfo_t * value)
	{
		____specifiedMember_6 = value;
		Il2CppCodeGenWriteBarrier((&____specifiedMember_6), value);
	}

	inline static int32_t get_offset_of__shouldSerialize_7() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____shouldSerialize_7)); }
	inline MethodInfo_t * get__shouldSerialize_7() const { return ____shouldSerialize_7; }
	inline MethodInfo_t ** get_address_of__shouldSerialize_7() { return &____shouldSerialize_7; }
	inline void set__shouldSerialize_7(MethodInfo_t * value)
	{
		____shouldSerialize_7 = value;
		Il2CppCodeGenWriteBarrier((&____shouldSerialize_7), value);
	}

	inline static int32_t get_offset_of__defaultValue_8() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____defaultValue_8)); }
	inline RuntimeObject * get__defaultValue_8() const { return ____defaultValue_8; }
	inline RuntimeObject ** get_address_of__defaultValue_8() { return &____defaultValue_8; }
	inline void set__defaultValue_8(RuntimeObject * value)
	{
		____defaultValue_8 = value;
		Il2CppCodeGenWriteBarrier((&____defaultValue_8), value);
	}

	inline static int32_t get_offset_of__flags_9() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____flags_9)); }
	inline int32_t get__flags_9() const { return ____flags_9; }
	inline int32_t* get_address_of__flags_9() { return &____flags_9; }
	inline void set__flags_9(int32_t value)
	{
		____flags_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBER_T8272D1E62F842F97D749F6FFC6B4AE57754C914D_H
#ifndef DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#define DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier((&___Powers10_6), value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Zero_7)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___One_8)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_One_8() const { return ___One_8; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinusOne_9)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MaxValue_10)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinValue_11)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearPositiveZero_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef AUTOVALIDATOR_T348DF836BF1829DC3B8DBA19A6E0EFC1A6EE85EB_H
#define AUTOVALIDATOR_T348DF836BF1829DC3B8DBA19A6E0EFC1A6EE85EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.AutoValidator
struct  AutoValidator_t348DF836BF1829DC3B8DBA19A6E0EFC1A6EE85EB  : public BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOVALIDATOR_T348DF836BF1829DC3B8DBA19A6E0EFC1A6EE85EB_H
#ifndef INTERIORNODE_T274145A160D08BE97CA9437691E2AE9A4D30A51D_H
#define INTERIORNODE_T274145A160D08BE97CA9437691E2AE9A4D30A51D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.InteriorNode
struct  InteriorNode_t274145A160D08BE97CA9437691E2AE9A4D30A51D  : public SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C
{
public:
	// System.Xml.Schema.SyntaxTreeNode System.Xml.Schema.InteriorNode::leftChild
	SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C * ___leftChild_0;
	// System.Xml.Schema.SyntaxTreeNode System.Xml.Schema.InteriorNode::rightChild
	SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C * ___rightChild_1;

public:
	inline static int32_t get_offset_of_leftChild_0() { return static_cast<int32_t>(offsetof(InteriorNode_t274145A160D08BE97CA9437691E2AE9A4D30A51D, ___leftChild_0)); }
	inline SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C * get_leftChild_0() const { return ___leftChild_0; }
	inline SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C ** get_address_of_leftChild_0() { return &___leftChild_0; }
	inline void set_leftChild_0(SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C * value)
	{
		___leftChild_0 = value;
		Il2CppCodeGenWriteBarrier((&___leftChild_0), value);
	}

	inline static int32_t get_offset_of_rightChild_1() { return static_cast<int32_t>(offsetof(InteriorNode_t274145A160D08BE97CA9437691E2AE9A4D30A51D, ___rightChild_1)); }
	inline SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C * get_rightChild_1() const { return ___rightChild_1; }
	inline SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C ** get_address_of_rightChild_1() { return &___rightChild_1; }
	inline void set_rightChild_1(SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C * value)
	{
		___rightChild_1 = value;
		Il2CppCodeGenWriteBarrier((&___rightChild_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERIORNODE_T274145A160D08BE97CA9437691E2AE9A4D30A51D_H
#ifndef LEAFNODE_TE2C955D778068BA019B8D106487A698656F863E2_H
#define LEAFNODE_TE2C955D778068BA019B8D106487A698656F863E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.LeafNode
struct  LeafNode_tE2C955D778068BA019B8D106487A698656F863E2  : public SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C
{
public:
	// System.Int32 System.Xml.Schema.LeafNode::pos
	int32_t ___pos_0;

public:
	inline static int32_t get_offset_of_pos_0() { return static_cast<int32_t>(offsetof(LeafNode_tE2C955D778068BA019B8D106487A698656F863E2, ___pos_0)); }
	inline int32_t get_pos_0() const { return ___pos_0; }
	inline int32_t* get_address_of_pos_0() { return &___pos_0; }
	inline void set_pos_0(int32_t value)
	{
		___pos_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEAFNODE_TE2C955D778068BA019B8D106487A698656F863E2_H
#ifndef LOCATEDACTIVEAXIS_TCCF3C0E41A32AC342B9B40531789937908E510D5_H
#define LOCATEDACTIVEAXIS_TCCF3C0E41A32AC342B9B40531789937908E510D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.LocatedActiveAxis
struct  LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5  : public ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E
{
public:
	// System.Int32 System.Xml.Schema.LocatedActiveAxis::column
	int32_t ___column_4;
	// System.Boolean System.Xml.Schema.LocatedActiveAxis::isMatched
	bool ___isMatched_5;
	// System.Xml.Schema.KeySequence System.Xml.Schema.LocatedActiveAxis::Ks
	KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE * ___Ks_6;

public:
	inline static int32_t get_offset_of_column_4() { return static_cast<int32_t>(offsetof(LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5, ___column_4)); }
	inline int32_t get_column_4() const { return ___column_4; }
	inline int32_t* get_address_of_column_4() { return &___column_4; }
	inline void set_column_4(int32_t value)
	{
		___column_4 = value;
	}

	inline static int32_t get_offset_of_isMatched_5() { return static_cast<int32_t>(offsetof(LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5, ___isMatched_5)); }
	inline bool get_isMatched_5() const { return ___isMatched_5; }
	inline bool* get_address_of_isMatched_5() { return &___isMatched_5; }
	inline void set_isMatched_5(bool value)
	{
		___isMatched_5 = value;
	}

	inline static int32_t get_offset_of_Ks_6() { return static_cast<int32_t>(offsetof(LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5, ___Ks_6)); }
	inline KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE * get_Ks_6() const { return ___Ks_6; }
	inline KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE ** get_address_of_Ks_6() { return &___Ks_6; }
	inline void set_Ks_6(KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE * value)
	{
		___Ks_6 = value;
		Il2CppCodeGenWriteBarrier((&___Ks_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATEDACTIVEAXIS_TCCF3C0E41A32AC342B9B40531789937908E510D5_H
#ifndef NAMESPACELISTNODE_T7C392E0F03C23BE2D7261C64D3DB816CA6A8EC19_H
#define NAMESPACELISTNODE_T7C392E0F03C23BE2D7261C64D3DB816CA6A8EC19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.NamespaceListNode
struct  NamespaceListNode_t7C392E0F03C23BE2D7261C64D3DB816CA6A8EC19  : public SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C
{
public:
	// System.Xml.Schema.NamespaceList System.Xml.Schema.NamespaceListNode::namespaceList
	NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C * ___namespaceList_0;
	// System.Object System.Xml.Schema.NamespaceListNode::particle
	RuntimeObject * ___particle_1;

public:
	inline static int32_t get_offset_of_namespaceList_0() { return static_cast<int32_t>(offsetof(NamespaceListNode_t7C392E0F03C23BE2D7261C64D3DB816CA6A8EC19, ___namespaceList_0)); }
	inline NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C * get_namespaceList_0() const { return ___namespaceList_0; }
	inline NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C ** get_address_of_namespaceList_0() { return &___namespaceList_0; }
	inline void set_namespaceList_0(NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C * value)
	{
		___namespaceList_0 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceList_0), value);
	}

	inline static int32_t get_offset_of_particle_1() { return static_cast<int32_t>(offsetof(NamespaceListNode_t7C392E0F03C23BE2D7261C64D3DB816CA6A8EC19, ___particle_1)); }
	inline RuntimeObject * get_particle_1() const { return ___particle_1; }
	inline RuntimeObject ** get_address_of_particle_1() { return &___particle_1; }
	inline void set_particle_1(RuntimeObject * value)
	{
		___particle_1 = value;
		Il2CppCodeGenWriteBarrier((&___particle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACELISTNODE_T7C392E0F03C23BE2D7261C64D3DB816CA6A8EC19_H
#ifndef POSITION_T089976E4BEB3D345DA28CFA95786EE065063E228_H
#define POSITION_T089976E4BEB3D345DA28CFA95786EE065063E228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Position
struct  Position_t089976E4BEB3D345DA28CFA95786EE065063E228 
{
public:
	// System.Int32 System.Xml.Schema.Position::symbol
	int32_t ___symbol_0;
	// System.Object System.Xml.Schema.Position::particle
	RuntimeObject * ___particle_1;

public:
	inline static int32_t get_offset_of_symbol_0() { return static_cast<int32_t>(offsetof(Position_t089976E4BEB3D345DA28CFA95786EE065063E228, ___symbol_0)); }
	inline int32_t get_symbol_0() const { return ___symbol_0; }
	inline int32_t* get_address_of_symbol_0() { return &___symbol_0; }
	inline void set_symbol_0(int32_t value)
	{
		___symbol_0 = value;
	}

	inline static int32_t get_offset_of_particle_1() { return static_cast<int32_t>(offsetof(Position_t089976E4BEB3D345DA28CFA95786EE065063E228, ___particle_1)); }
	inline RuntimeObject * get_particle_1() const { return ___particle_1; }
	inline RuntimeObject ** get_address_of_particle_1() { return &___particle_1; }
	inline void set_particle_1(RuntimeObject * value)
	{
		___particle_1 = value;
		Il2CppCodeGenWriteBarrier((&___particle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.Position
struct Position_t089976E4BEB3D345DA28CFA95786EE065063E228_marshaled_pinvoke
{
	int32_t ___symbol_0;
	Il2CppIUnknown* ___particle_1;
};
// Native definition for COM marshalling of System.Xml.Schema.Position
struct Position_t089976E4BEB3D345DA28CFA95786EE065063E228_marshaled_com
{
	int32_t ___symbol_0;
	Il2CppIUnknown* ___particle_1;
};
#endif // POSITION_T089976E4BEB3D345DA28CFA95786EE065063E228_H
#ifndef RANGEPOSITIONINFO_TDCA2617E7E1292998A9700E38DBBA177330A80CA_H
#define RANGEPOSITIONINFO_TDCA2617E7E1292998A9700E38DBBA177330A80CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.RangePositionInfo
struct  RangePositionInfo_tDCA2617E7E1292998A9700E38DBBA177330A80CA 
{
public:
	// System.Xml.Schema.BitSet System.Xml.Schema.RangePositionInfo::curpos
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___curpos_0;
	// System.Decimal[] System.Xml.Schema.RangePositionInfo::rangeCounters
	DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F* ___rangeCounters_1;

public:
	inline static int32_t get_offset_of_curpos_0() { return static_cast<int32_t>(offsetof(RangePositionInfo_tDCA2617E7E1292998A9700E38DBBA177330A80CA, ___curpos_0)); }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C * get_curpos_0() const { return ___curpos_0; }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C ** get_address_of_curpos_0() { return &___curpos_0; }
	inline void set_curpos_0(BitSet_t0E4C53EC600670A4B74C5671553596978880138C * value)
	{
		___curpos_0 = value;
		Il2CppCodeGenWriteBarrier((&___curpos_0), value);
	}

	inline static int32_t get_offset_of_rangeCounters_1() { return static_cast<int32_t>(offsetof(RangePositionInfo_tDCA2617E7E1292998A9700E38DBBA177330A80CA, ___rangeCounters_1)); }
	inline DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F* get_rangeCounters_1() const { return ___rangeCounters_1; }
	inline DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F** get_address_of_rangeCounters_1() { return &___rangeCounters_1; }
	inline void set_rangeCounters_1(DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F* value)
	{
		___rangeCounters_1 = value;
		Il2CppCodeGenWriteBarrier((&___rangeCounters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.RangePositionInfo
struct RangePositionInfo_tDCA2617E7E1292998A9700E38DBBA177330A80CA_marshaled_pinvoke
{
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___curpos_0;
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * ___rangeCounters_1;
};
// Native definition for COM marshalling of System.Xml.Schema.RangePositionInfo
struct RangePositionInfo_tDCA2617E7E1292998A9700E38DBBA177330A80CA_marshaled_com
{
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___curpos_0;
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * ___rangeCounters_1;
};
#endif // RANGEPOSITIONINFO_TDCA2617E7E1292998A9700E38DBBA177330A80CA_H
#ifndef SELECTORACTIVEAXIS_T7EA444AA4AD068A5F4A8C12A756792925AF59A64_H
#define SELECTORACTIVEAXIS_T7EA444AA4AD068A5F4A8C12A756792925AF59A64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SelectorActiveAxis
struct  SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64  : public ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E
{
public:
	// System.Xml.Schema.ConstraintStruct System.Xml.Schema.SelectorActiveAxis::cs
	ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54 * ___cs_4;
	// System.Collections.ArrayList System.Xml.Schema.SelectorActiveAxis::KSs
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___KSs_5;
	// System.Int32 System.Xml.Schema.SelectorActiveAxis::KSpointer
	int32_t ___KSpointer_6;

public:
	inline static int32_t get_offset_of_cs_4() { return static_cast<int32_t>(offsetof(SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64, ___cs_4)); }
	inline ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54 * get_cs_4() const { return ___cs_4; }
	inline ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54 ** get_address_of_cs_4() { return &___cs_4; }
	inline void set_cs_4(ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54 * value)
	{
		___cs_4 = value;
		Il2CppCodeGenWriteBarrier((&___cs_4), value);
	}

	inline static int32_t get_offset_of_KSs_5() { return static_cast<int32_t>(offsetof(SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64, ___KSs_5)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_KSs_5() const { return ___KSs_5; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_KSs_5() { return &___KSs_5; }
	inline void set_KSs_5(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___KSs_5 = value;
		Il2CppCodeGenWriteBarrier((&___KSs_5), value);
	}

	inline static int32_t get_offset_of_KSpointer_6() { return static_cast<int32_t>(offsetof(SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64, ___KSpointer_6)); }
	inline int32_t get_KSpointer_6() const { return ___KSpointer_6; }
	inline int32_t* get_address_of_KSpointer_6() { return &___KSpointer_6; }
	inline void set_KSpointer_6(int32_t value)
	{
		___KSpointer_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTORACTIVEAXIS_T7EA444AA4AD068A5F4A8C12A756792925AF59A64_H
#ifndef SEQUENCECONSTRUCTPOSCONTEXT_T72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1_H
#define SEQUENCECONSTRUCTPOSCONTEXT_T72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SequenceNode/SequenceConstructPosContext
struct  SequenceConstructPosContext_t72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1 
{
public:
	// System.Xml.Schema.SequenceNode System.Xml.Schema.SequenceNode/SequenceConstructPosContext::this_
	SequenceNode_tAB18F790CB1B5BCD1D984EECC17C841B00881608 * ___this__0;
	// System.Xml.Schema.BitSet System.Xml.Schema.SequenceNode/SequenceConstructPosContext::firstpos
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___firstpos_1;
	// System.Xml.Schema.BitSet System.Xml.Schema.SequenceNode/SequenceConstructPosContext::lastpos
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___lastpos_2;
	// System.Xml.Schema.BitSet System.Xml.Schema.SequenceNode/SequenceConstructPosContext::lastposLeft
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___lastposLeft_3;
	// System.Xml.Schema.BitSet System.Xml.Schema.SequenceNode/SequenceConstructPosContext::firstposRight
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___firstposRight_4;

public:
	inline static int32_t get_offset_of_this__0() { return static_cast<int32_t>(offsetof(SequenceConstructPosContext_t72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1, ___this__0)); }
	inline SequenceNode_tAB18F790CB1B5BCD1D984EECC17C841B00881608 * get_this__0() const { return ___this__0; }
	inline SequenceNode_tAB18F790CB1B5BCD1D984EECC17C841B00881608 ** get_address_of_this__0() { return &___this__0; }
	inline void set_this__0(SequenceNode_tAB18F790CB1B5BCD1D984EECC17C841B00881608 * value)
	{
		___this__0 = value;
		Il2CppCodeGenWriteBarrier((&___this__0), value);
	}

	inline static int32_t get_offset_of_firstpos_1() { return static_cast<int32_t>(offsetof(SequenceConstructPosContext_t72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1, ___firstpos_1)); }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C * get_firstpos_1() const { return ___firstpos_1; }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C ** get_address_of_firstpos_1() { return &___firstpos_1; }
	inline void set_firstpos_1(BitSet_t0E4C53EC600670A4B74C5671553596978880138C * value)
	{
		___firstpos_1 = value;
		Il2CppCodeGenWriteBarrier((&___firstpos_1), value);
	}

	inline static int32_t get_offset_of_lastpos_2() { return static_cast<int32_t>(offsetof(SequenceConstructPosContext_t72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1, ___lastpos_2)); }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C * get_lastpos_2() const { return ___lastpos_2; }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C ** get_address_of_lastpos_2() { return &___lastpos_2; }
	inline void set_lastpos_2(BitSet_t0E4C53EC600670A4B74C5671553596978880138C * value)
	{
		___lastpos_2 = value;
		Il2CppCodeGenWriteBarrier((&___lastpos_2), value);
	}

	inline static int32_t get_offset_of_lastposLeft_3() { return static_cast<int32_t>(offsetof(SequenceConstructPosContext_t72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1, ___lastposLeft_3)); }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C * get_lastposLeft_3() const { return ___lastposLeft_3; }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C ** get_address_of_lastposLeft_3() { return &___lastposLeft_3; }
	inline void set_lastposLeft_3(BitSet_t0E4C53EC600670A4B74C5671553596978880138C * value)
	{
		___lastposLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___lastposLeft_3), value);
	}

	inline static int32_t get_offset_of_firstposRight_4() { return static_cast<int32_t>(offsetof(SequenceConstructPosContext_t72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1, ___firstposRight_4)); }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C * get_firstposRight_4() const { return ___firstposRight_4; }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C ** get_address_of_firstposRight_4() { return &___firstposRight_4; }
	inline void set_firstposRight_4(BitSet_t0E4C53EC600670A4B74C5671553596978880138C * value)
	{
		___firstposRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___firstposRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.SequenceNode/SequenceConstructPosContext
struct SequenceConstructPosContext_t72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1_marshaled_pinvoke
{
	SequenceNode_tAB18F790CB1B5BCD1D984EECC17C841B00881608 * ___this__0;
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___firstpos_1;
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___lastpos_2;
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___lastposLeft_3;
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___firstposRight_4;
};
// Native definition for COM marshalling of System.Xml.Schema.SequenceNode/SequenceConstructPosContext
struct SequenceConstructPosContext_t72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1_marshaled_com
{
	SequenceNode_tAB18F790CB1B5BCD1D984EECC17C841B00881608 * ___this__0;
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___firstpos_1;
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___lastpos_2;
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___lastposLeft_3;
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___firstposRight_4;
};
#endif // SEQUENCECONSTRUCTPOSCONTEXT_T72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1_H
#ifndef UPAEXCEPTION_TED69C4AC662CA80677ABA101231042E747107151_H
#define UPAEXCEPTION_TED69C4AC662CA80677ABA101231042E747107151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.UpaException
struct  UpaException_tED69C4AC662CA80677ABA101231042E747107151  : public Exception_t
{
public:
	// System.Object System.Xml.Schema.UpaException::particle1
	RuntimeObject * ___particle1_17;
	// System.Object System.Xml.Schema.UpaException::particle2
	RuntimeObject * ___particle2_18;

public:
	inline static int32_t get_offset_of_particle1_17() { return static_cast<int32_t>(offsetof(UpaException_tED69C4AC662CA80677ABA101231042E747107151, ___particle1_17)); }
	inline RuntimeObject * get_particle1_17() const { return ___particle1_17; }
	inline RuntimeObject ** get_address_of_particle1_17() { return &___particle1_17; }
	inline void set_particle1_17(RuntimeObject * value)
	{
		___particle1_17 = value;
		Il2CppCodeGenWriteBarrier((&___particle1_17), value);
	}

	inline static int32_t get_offset_of_particle2_18() { return static_cast<int32_t>(offsetof(UpaException_tED69C4AC662CA80677ABA101231042E747107151, ___particle2_18)); }
	inline RuntimeObject * get_particle2_18() const { return ___particle2_18; }
	inline RuntimeObject ** get_address_of_particle2_18() { return &___particle2_18; }
	inline void set_particle2_18(RuntimeObject * value)
	{
		___particle2_18 = value;
		Il2CppCodeGenWriteBarrier((&___particle2_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPAEXCEPTION_TED69C4AC662CA80677ABA101231042E747107151_H
#ifndef ENUMMAP_TD7B24FAC46ED105487BE3269534CBB5072D8652A_H
#define ENUMMAP_TD7B24FAC46ED105487BE3269534CBB5072D8652A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.EnumMap
struct  EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A  : public ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677
{
public:
	// System.Xml.Serialization.EnumMap/EnumMapMember[] System.Xml.Serialization.EnumMap::_members
	EnumMapMemberU5BU5D_t65FB66969212223ED79971B0473634E03D6C1B2E* ____members_0;
	// System.Boolean System.Xml.Serialization.EnumMap::_isFlags
	bool ____isFlags_1;
	// System.String[] System.Xml.Serialization.EnumMap::_enumNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____enumNames_2;
	// System.String[] System.Xml.Serialization.EnumMap::_xmlNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____xmlNames_3;
	// System.Int64[] System.Xml.Serialization.EnumMap::_values
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____values_4;

public:
	inline static int32_t get_offset_of__members_0() { return static_cast<int32_t>(offsetof(EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A, ____members_0)); }
	inline EnumMapMemberU5BU5D_t65FB66969212223ED79971B0473634E03D6C1B2E* get__members_0() const { return ____members_0; }
	inline EnumMapMemberU5BU5D_t65FB66969212223ED79971B0473634E03D6C1B2E** get_address_of__members_0() { return &____members_0; }
	inline void set__members_0(EnumMapMemberU5BU5D_t65FB66969212223ED79971B0473634E03D6C1B2E* value)
	{
		____members_0 = value;
		Il2CppCodeGenWriteBarrier((&____members_0), value);
	}

	inline static int32_t get_offset_of__isFlags_1() { return static_cast<int32_t>(offsetof(EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A, ____isFlags_1)); }
	inline bool get__isFlags_1() const { return ____isFlags_1; }
	inline bool* get_address_of__isFlags_1() { return &____isFlags_1; }
	inline void set__isFlags_1(bool value)
	{
		____isFlags_1 = value;
	}

	inline static int32_t get_offset_of__enumNames_2() { return static_cast<int32_t>(offsetof(EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A, ____enumNames_2)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__enumNames_2() const { return ____enumNames_2; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__enumNames_2() { return &____enumNames_2; }
	inline void set__enumNames_2(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____enumNames_2 = value;
		Il2CppCodeGenWriteBarrier((&____enumNames_2), value);
	}

	inline static int32_t get_offset_of__xmlNames_3() { return static_cast<int32_t>(offsetof(EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A, ____xmlNames_3)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__xmlNames_3() const { return ____xmlNames_3; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__xmlNames_3() { return &____xmlNames_3; }
	inline void set__xmlNames_3(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____xmlNames_3 = value;
		Il2CppCodeGenWriteBarrier((&____xmlNames_3), value);
	}

	inline static int32_t get_offset_of__values_4() { return static_cast<int32_t>(offsetof(EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A, ____values_4)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__values_4() const { return ____values_4; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__values_4() { return &____values_4; }
	inline void set__values_4(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____values_4 = value;
		Il2CppCodeGenWriteBarrier((&____values_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMMAP_TD7B24FAC46ED105487BE3269534CBB5072D8652A_H
#ifndef LISTMAP_TAE3FA433501B26F844EF9942DB4C054DA7CE2742_H
#define LISTMAP_TAE3FA433501B26F844EF9942DB4C054DA7CE2742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.ListMap
struct  ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742  : public ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677
{
public:
	// System.Xml.Serialization.XmlTypeMapElementInfoList System.Xml.Serialization.ListMap::_itemInfo
	XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 * ____itemInfo_0;
	// System.String System.Xml.Serialization.ListMap::_choiceMember
	String_t* ____choiceMember_1;

public:
	inline static int32_t get_offset_of__itemInfo_0() { return static_cast<int32_t>(offsetof(ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742, ____itemInfo_0)); }
	inline XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 * get__itemInfo_0() const { return ____itemInfo_0; }
	inline XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 ** get_address_of__itemInfo_0() { return &____itemInfo_0; }
	inline void set__itemInfo_0(XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 * value)
	{
		____itemInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____itemInfo_0), value);
	}

	inline static int32_t get_offset_of__choiceMember_1() { return static_cast<int32_t>(offsetof(ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742, ____choiceMember_1)); }
	inline String_t* get__choiceMember_1() const { return ____choiceMember_1; }
	inline String_t** get_address_of__choiceMember_1() { return &____choiceMember_1; }
	inline void set__choiceMember_1(String_t* value)
	{
		____choiceMember_1 = value;
		Il2CppCodeGenWriteBarrier((&____choiceMember_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTMAP_TAE3FA433501B26F844EF9942DB4C054DA7CE2742_H
#ifndef XMLSERIALIZATIONREADER_TDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88_H
#define XMLSERIALIZATIONREADER_TDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationReader
struct  XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88  : public XmlSerializationGeneratedCode_tD121073E4460FCF7F6B549E4621B0CB971D2E6CE
{
public:
	// System.Xml.XmlDocument System.Xml.Serialization.XmlSerializationReader::document
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ___document_0;
	// System.Xml.XmlReader System.Xml.Serialization.XmlSerializationReader::reader
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * ___reader_1;
	// System.Collections.ArrayList System.Xml.Serialization.XmlSerializationReader::fixups
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___fixups_2;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializationReader::collFixups
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___collFixups_3;
	// System.Collections.ArrayList System.Xml.Serialization.XmlSerializationReader::collItemFixups
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___collItemFixups_4;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializationReader::typesCallbacks
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___typesCallbacks_5;
	// System.Collections.ArrayList System.Xml.Serialization.XmlSerializationReader::noIDTargets
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___noIDTargets_6;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializationReader::targets
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___targets_7;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializationReader::delayedListFixups
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___delayedListFixups_8;
	// System.Xml.Serialization.XmlSerializer System.Xml.Serialization.XmlSerializationReader::eventSource
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C * ___eventSource_9;
	// System.Int32 System.Xml.Serialization.XmlSerializationReader::delayedFixupId
	int32_t ___delayedFixupId_10;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializationReader::referencedObjects
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___referencedObjects_11;
	// System.Int32 System.Xml.Serialization.XmlSerializationReader::readCount
	int32_t ___readCount_12;
	// System.Int32 System.Xml.Serialization.XmlSerializationReader::whileIterationCount
	int32_t ___whileIterationCount_13;
	// System.String System.Xml.Serialization.XmlSerializationReader::w3SchemaNS
	String_t* ___w3SchemaNS_14;
	// System.String System.Xml.Serialization.XmlSerializationReader::w3InstanceNS
	String_t* ___w3InstanceNS_15;
	// System.String System.Xml.Serialization.XmlSerializationReader::w3InstanceNS2000
	String_t* ___w3InstanceNS2000_16;
	// System.String System.Xml.Serialization.XmlSerializationReader::w3InstanceNS1999
	String_t* ___w3InstanceNS1999_17;
	// System.String System.Xml.Serialization.XmlSerializationReader::soapNS
	String_t* ___soapNS_18;
	// System.String System.Xml.Serialization.XmlSerializationReader::wsdlNS
	String_t* ___wsdlNS_19;
	// System.String System.Xml.Serialization.XmlSerializationReader::nullX
	String_t* ___nullX_20;
	// System.String System.Xml.Serialization.XmlSerializationReader::nil
	String_t* ___nil_21;
	// System.String System.Xml.Serialization.XmlSerializationReader::typeX
	String_t* ___typeX_22;
	// System.String System.Xml.Serialization.XmlSerializationReader::arrayType
	String_t* ___arrayType_23;
	// System.Xml.XmlQualifiedName System.Xml.Serialization.XmlSerializationReader::arrayQName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___arrayQName_24;

public:
	inline static int32_t get_offset_of_document_0() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___document_0)); }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * get_document_0() const { return ___document_0; }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 ** get_address_of_document_0() { return &___document_0; }
	inline void set_document_0(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * value)
	{
		___document_0 = value;
		Il2CppCodeGenWriteBarrier((&___document_0), value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___reader_1)); }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * get_reader_1() const { return ___reader_1; }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___reader_1), value);
	}

	inline static int32_t get_offset_of_fixups_2() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___fixups_2)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_fixups_2() const { return ___fixups_2; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_fixups_2() { return &___fixups_2; }
	inline void set_fixups_2(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___fixups_2 = value;
		Il2CppCodeGenWriteBarrier((&___fixups_2), value);
	}

	inline static int32_t get_offset_of_collFixups_3() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___collFixups_3)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_collFixups_3() const { return ___collFixups_3; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_collFixups_3() { return &___collFixups_3; }
	inline void set_collFixups_3(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___collFixups_3 = value;
		Il2CppCodeGenWriteBarrier((&___collFixups_3), value);
	}

	inline static int32_t get_offset_of_collItemFixups_4() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___collItemFixups_4)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_collItemFixups_4() const { return ___collItemFixups_4; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_collItemFixups_4() { return &___collItemFixups_4; }
	inline void set_collItemFixups_4(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___collItemFixups_4 = value;
		Il2CppCodeGenWriteBarrier((&___collItemFixups_4), value);
	}

	inline static int32_t get_offset_of_typesCallbacks_5() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___typesCallbacks_5)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_typesCallbacks_5() const { return ___typesCallbacks_5; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_typesCallbacks_5() { return &___typesCallbacks_5; }
	inline void set_typesCallbacks_5(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___typesCallbacks_5 = value;
		Il2CppCodeGenWriteBarrier((&___typesCallbacks_5), value);
	}

	inline static int32_t get_offset_of_noIDTargets_6() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___noIDTargets_6)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_noIDTargets_6() const { return ___noIDTargets_6; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_noIDTargets_6() { return &___noIDTargets_6; }
	inline void set_noIDTargets_6(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___noIDTargets_6 = value;
		Il2CppCodeGenWriteBarrier((&___noIDTargets_6), value);
	}

	inline static int32_t get_offset_of_targets_7() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___targets_7)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_targets_7() const { return ___targets_7; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_targets_7() { return &___targets_7; }
	inline void set_targets_7(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___targets_7 = value;
		Il2CppCodeGenWriteBarrier((&___targets_7), value);
	}

	inline static int32_t get_offset_of_delayedListFixups_8() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___delayedListFixups_8)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_delayedListFixups_8() const { return ___delayedListFixups_8; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_delayedListFixups_8() { return &___delayedListFixups_8; }
	inline void set_delayedListFixups_8(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___delayedListFixups_8 = value;
		Il2CppCodeGenWriteBarrier((&___delayedListFixups_8), value);
	}

	inline static int32_t get_offset_of_eventSource_9() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___eventSource_9)); }
	inline XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C * get_eventSource_9() const { return ___eventSource_9; }
	inline XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C ** get_address_of_eventSource_9() { return &___eventSource_9; }
	inline void set_eventSource_9(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C * value)
	{
		___eventSource_9 = value;
		Il2CppCodeGenWriteBarrier((&___eventSource_9), value);
	}

	inline static int32_t get_offset_of_delayedFixupId_10() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___delayedFixupId_10)); }
	inline int32_t get_delayedFixupId_10() const { return ___delayedFixupId_10; }
	inline int32_t* get_address_of_delayedFixupId_10() { return &___delayedFixupId_10; }
	inline void set_delayedFixupId_10(int32_t value)
	{
		___delayedFixupId_10 = value;
	}

	inline static int32_t get_offset_of_referencedObjects_11() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___referencedObjects_11)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_referencedObjects_11() const { return ___referencedObjects_11; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_referencedObjects_11() { return &___referencedObjects_11; }
	inline void set_referencedObjects_11(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___referencedObjects_11 = value;
		Il2CppCodeGenWriteBarrier((&___referencedObjects_11), value);
	}

	inline static int32_t get_offset_of_readCount_12() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___readCount_12)); }
	inline int32_t get_readCount_12() const { return ___readCount_12; }
	inline int32_t* get_address_of_readCount_12() { return &___readCount_12; }
	inline void set_readCount_12(int32_t value)
	{
		___readCount_12 = value;
	}

	inline static int32_t get_offset_of_whileIterationCount_13() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___whileIterationCount_13)); }
	inline int32_t get_whileIterationCount_13() const { return ___whileIterationCount_13; }
	inline int32_t* get_address_of_whileIterationCount_13() { return &___whileIterationCount_13; }
	inline void set_whileIterationCount_13(int32_t value)
	{
		___whileIterationCount_13 = value;
	}

	inline static int32_t get_offset_of_w3SchemaNS_14() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___w3SchemaNS_14)); }
	inline String_t* get_w3SchemaNS_14() const { return ___w3SchemaNS_14; }
	inline String_t** get_address_of_w3SchemaNS_14() { return &___w3SchemaNS_14; }
	inline void set_w3SchemaNS_14(String_t* value)
	{
		___w3SchemaNS_14 = value;
		Il2CppCodeGenWriteBarrier((&___w3SchemaNS_14), value);
	}

	inline static int32_t get_offset_of_w3InstanceNS_15() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___w3InstanceNS_15)); }
	inline String_t* get_w3InstanceNS_15() const { return ___w3InstanceNS_15; }
	inline String_t** get_address_of_w3InstanceNS_15() { return &___w3InstanceNS_15; }
	inline void set_w3InstanceNS_15(String_t* value)
	{
		___w3InstanceNS_15 = value;
		Il2CppCodeGenWriteBarrier((&___w3InstanceNS_15), value);
	}

	inline static int32_t get_offset_of_w3InstanceNS2000_16() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___w3InstanceNS2000_16)); }
	inline String_t* get_w3InstanceNS2000_16() const { return ___w3InstanceNS2000_16; }
	inline String_t** get_address_of_w3InstanceNS2000_16() { return &___w3InstanceNS2000_16; }
	inline void set_w3InstanceNS2000_16(String_t* value)
	{
		___w3InstanceNS2000_16 = value;
		Il2CppCodeGenWriteBarrier((&___w3InstanceNS2000_16), value);
	}

	inline static int32_t get_offset_of_w3InstanceNS1999_17() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___w3InstanceNS1999_17)); }
	inline String_t* get_w3InstanceNS1999_17() const { return ___w3InstanceNS1999_17; }
	inline String_t** get_address_of_w3InstanceNS1999_17() { return &___w3InstanceNS1999_17; }
	inline void set_w3InstanceNS1999_17(String_t* value)
	{
		___w3InstanceNS1999_17 = value;
		Il2CppCodeGenWriteBarrier((&___w3InstanceNS1999_17), value);
	}

	inline static int32_t get_offset_of_soapNS_18() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___soapNS_18)); }
	inline String_t* get_soapNS_18() const { return ___soapNS_18; }
	inline String_t** get_address_of_soapNS_18() { return &___soapNS_18; }
	inline void set_soapNS_18(String_t* value)
	{
		___soapNS_18 = value;
		Il2CppCodeGenWriteBarrier((&___soapNS_18), value);
	}

	inline static int32_t get_offset_of_wsdlNS_19() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___wsdlNS_19)); }
	inline String_t* get_wsdlNS_19() const { return ___wsdlNS_19; }
	inline String_t** get_address_of_wsdlNS_19() { return &___wsdlNS_19; }
	inline void set_wsdlNS_19(String_t* value)
	{
		___wsdlNS_19 = value;
		Il2CppCodeGenWriteBarrier((&___wsdlNS_19), value);
	}

	inline static int32_t get_offset_of_nullX_20() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___nullX_20)); }
	inline String_t* get_nullX_20() const { return ___nullX_20; }
	inline String_t** get_address_of_nullX_20() { return &___nullX_20; }
	inline void set_nullX_20(String_t* value)
	{
		___nullX_20 = value;
		Il2CppCodeGenWriteBarrier((&___nullX_20), value);
	}

	inline static int32_t get_offset_of_nil_21() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___nil_21)); }
	inline String_t* get_nil_21() const { return ___nil_21; }
	inline String_t** get_address_of_nil_21() { return &___nil_21; }
	inline void set_nil_21(String_t* value)
	{
		___nil_21 = value;
		Il2CppCodeGenWriteBarrier((&___nil_21), value);
	}

	inline static int32_t get_offset_of_typeX_22() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___typeX_22)); }
	inline String_t* get_typeX_22() const { return ___typeX_22; }
	inline String_t** get_address_of_typeX_22() { return &___typeX_22; }
	inline void set_typeX_22(String_t* value)
	{
		___typeX_22 = value;
		Il2CppCodeGenWriteBarrier((&___typeX_22), value);
	}

	inline static int32_t get_offset_of_arrayType_23() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___arrayType_23)); }
	inline String_t* get_arrayType_23() const { return ___arrayType_23; }
	inline String_t** get_address_of_arrayType_23() { return &___arrayType_23; }
	inline void set_arrayType_23(String_t* value)
	{
		___arrayType_23 = value;
		Il2CppCodeGenWriteBarrier((&___arrayType_23), value);
	}

	inline static int32_t get_offset_of_arrayQName_24() { return static_cast<int32_t>(offsetof(XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88, ___arrayQName_24)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_arrayQName_24() const { return ___arrayQName_24; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_arrayQName_24() { return &___arrayQName_24; }
	inline void set_arrayQName_24(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___arrayQName_24 = value;
		Il2CppCodeGenWriteBarrier((&___arrayQName_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZATIONREADER_TDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88_H
#ifndef XMLSERIALIZATIONWRITER_TD7F886F1AE76D54C208A41797D76E908AA2086F6_H
#define XMLSERIALIZATIONWRITER_TD7F886F1AE76D54C208A41797D76E908AA2086F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationWriter
struct  XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6  : public XmlSerializationGeneratedCode_tD121073E4460FCF7F6B549E4621B0CB971D2E6CE
{
public:
	// System.Runtime.Serialization.ObjectIDGenerator System.Xml.Serialization.XmlSerializationWriter::idGenerator
	ObjectIDGenerator_tD65209D0A1CDA08C9C709A4D1997D8091A93C1A1 * ___idGenerator_0;
	// System.Int32 System.Xml.Serialization.XmlSerializationWriter::qnameCount
	int32_t ___qnameCount_1;
	// System.Boolean System.Xml.Serialization.XmlSerializationWriter::topLevelElement
	bool ___topLevelElement_2;
	// System.Collections.ArrayList System.Xml.Serialization.XmlSerializationWriter::namespaces
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___namespaces_3;
	// System.Xml.XmlWriter System.Xml.Serialization.XmlSerializationWriter::writer
	XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * ___writer_4;
	// System.Collections.Queue System.Xml.Serialization.XmlSerializationWriter::referencedElements
	Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 * ___referencedElements_5;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializationWriter::callbacks
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___callbacks_6;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializationWriter::serializedObjects
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___serializedObjects_7;

public:
	inline static int32_t get_offset_of_idGenerator_0() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___idGenerator_0)); }
	inline ObjectIDGenerator_tD65209D0A1CDA08C9C709A4D1997D8091A93C1A1 * get_idGenerator_0() const { return ___idGenerator_0; }
	inline ObjectIDGenerator_tD65209D0A1CDA08C9C709A4D1997D8091A93C1A1 ** get_address_of_idGenerator_0() { return &___idGenerator_0; }
	inline void set_idGenerator_0(ObjectIDGenerator_tD65209D0A1CDA08C9C709A4D1997D8091A93C1A1 * value)
	{
		___idGenerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___idGenerator_0), value);
	}

	inline static int32_t get_offset_of_qnameCount_1() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___qnameCount_1)); }
	inline int32_t get_qnameCount_1() const { return ___qnameCount_1; }
	inline int32_t* get_address_of_qnameCount_1() { return &___qnameCount_1; }
	inline void set_qnameCount_1(int32_t value)
	{
		___qnameCount_1 = value;
	}

	inline static int32_t get_offset_of_topLevelElement_2() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___topLevelElement_2)); }
	inline bool get_topLevelElement_2() const { return ___topLevelElement_2; }
	inline bool* get_address_of_topLevelElement_2() { return &___topLevelElement_2; }
	inline void set_topLevelElement_2(bool value)
	{
		___topLevelElement_2 = value;
	}

	inline static int32_t get_offset_of_namespaces_3() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___namespaces_3)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_namespaces_3() const { return ___namespaces_3; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_namespaces_3() { return &___namespaces_3; }
	inline void set_namespaces_3(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___namespaces_3 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_3), value);
	}

	inline static int32_t get_offset_of_writer_4() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___writer_4)); }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * get_writer_4() const { return ___writer_4; }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 ** get_address_of_writer_4() { return &___writer_4; }
	inline void set_writer_4(XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * value)
	{
		___writer_4 = value;
		Il2CppCodeGenWriteBarrier((&___writer_4), value);
	}

	inline static int32_t get_offset_of_referencedElements_5() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___referencedElements_5)); }
	inline Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 * get_referencedElements_5() const { return ___referencedElements_5; }
	inline Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 ** get_address_of_referencedElements_5() { return &___referencedElements_5; }
	inline void set_referencedElements_5(Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 * value)
	{
		___referencedElements_5 = value;
		Il2CppCodeGenWriteBarrier((&___referencedElements_5), value);
	}

	inline static int32_t get_offset_of_callbacks_6() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___callbacks_6)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_callbacks_6() const { return ___callbacks_6; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_callbacks_6() { return &___callbacks_6; }
	inline void set_callbacks_6(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___callbacks_6 = value;
		Il2CppCodeGenWriteBarrier((&___callbacks_6), value);
	}

	inline static int32_t get_offset_of_serializedObjects_7() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___serializedObjects_7)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_serializedObjects_7() const { return ___serializedObjects_7; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_serializedObjects_7() { return &___serializedObjects_7; }
	inline void set_serializedObjects_7(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___serializedObjects_7 = value;
		Il2CppCodeGenWriteBarrier((&___serializedObjects_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZATIONWRITER_TD7F886F1AE76D54C208A41797D76E908AA2086F6_H
#ifndef XMLTEXTATTRIBUTE_T260DF760832CEB66E748BDEDE998C3480FB902A7_H
#define XMLTEXTATTRIBUTE_T260DF760832CEB66E748BDEDE998C3480FB902A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTextAttribute
struct  XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Xml.Serialization.XmlTextAttribute::dataType
	String_t* ___dataType_0;
	// System.Type System.Xml.Serialization.XmlTextAttribute::type
	Type_t * ___type_1;

public:
	inline static int32_t get_offset_of_dataType_0() { return static_cast<int32_t>(offsetof(XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7, ___dataType_0)); }
	inline String_t* get_dataType_0() const { return ___dataType_0; }
	inline String_t** get_address_of_dataType_0() { return &___dataType_0; }
	inline void set_dataType_0(String_t* value)
	{
		___dataType_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataType_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTATTRIBUTE_T260DF760832CEB66E748BDEDE998C3480FB902A7_H
#ifndef XMLTYPEATTRIBUTE_T8386F99942A9FABF3B9AF64324C28F674EE31E08_H
#define XMLTYPEATTRIBUTE_T8386F99942A9FABF3B9AF64324C28F674EE31E08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeAttribute
struct  XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.Xml.Serialization.XmlTypeAttribute::includeInSchema
	bool ___includeInSchema_0;
	// System.String System.Xml.Serialization.XmlTypeAttribute::ns
	String_t* ___ns_1;
	// System.String System.Xml.Serialization.XmlTypeAttribute::typeName
	String_t* ___typeName_2;

public:
	inline static int32_t get_offset_of_includeInSchema_0() { return static_cast<int32_t>(offsetof(XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08, ___includeInSchema_0)); }
	inline bool get_includeInSchema_0() const { return ___includeInSchema_0; }
	inline bool* get_address_of_includeInSchema_0() { return &___includeInSchema_0; }
	inline void set_includeInSchema_0(bool value)
	{
		___includeInSchema_0 = value;
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier((&___ns_1), value);
	}

	inline static int32_t get_offset_of_typeName_2() { return static_cast<int32_t>(offsetof(XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08, ___typeName_2)); }
	inline String_t* get_typeName_2() const { return ___typeName_2; }
	inline String_t** get_address_of_typeName_2() { return &___typeName_2; }
	inline void set_typeName_2(String_t* value)
	{
		___typeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEATTRIBUTE_T8386F99942A9FABF3B9AF64324C28F674EE31E08_H
#ifndef XMLTYPEMAPELEMENTINFOLIST_TCD7D387F78138B4E137D285319EC10532B14FBB0_H
#define XMLTYPEMAPELEMENTINFOLIST_TCD7D387F78138B4E137D285319EC10532B14FBB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapElementInfoList
struct  XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0  : public ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPELEMENTINFOLIST_TCD7D387F78138B4E137D285319EC10532B14FBB0_H
#ifndef XMLTYPEMAPMEMBERANYATTRIBUTE_T0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE_H
#define XMLTYPEMAPMEMBERANYATTRIBUTE_T0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberAnyAttribute
struct  XmlTypeMapMemberAnyAttribute_t0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE  : public XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBERANYATTRIBUTE_T0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE_H
#ifndef XMLTYPEMAPMEMBERELEMENT_T51AF456B8ECA454E76169688454E82735FB1AE00_H
#define XMLTYPEMAPMEMBERELEMENT_T51AF456B8ECA454E76169688454E82735FB1AE00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberElement
struct  XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00  : public XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D
{
public:
	// System.Xml.Serialization.XmlTypeMapElementInfoList System.Xml.Serialization.XmlTypeMapMemberElement::_elementInfo
	XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 * ____elementInfo_10;
	// System.String System.Xml.Serialization.XmlTypeMapMemberElement::_choiceMember
	String_t* ____choiceMember_11;
	// System.Boolean System.Xml.Serialization.XmlTypeMapMemberElement::_isTextCollector
	bool ____isTextCollector_12;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.XmlTypeMapMemberElement::_choiceTypeData
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * ____choiceTypeData_13;

public:
	inline static int32_t get_offset_of__elementInfo_10() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00, ____elementInfo_10)); }
	inline XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 * get__elementInfo_10() const { return ____elementInfo_10; }
	inline XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 ** get_address_of__elementInfo_10() { return &____elementInfo_10; }
	inline void set__elementInfo_10(XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 * value)
	{
		____elementInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&____elementInfo_10), value);
	}

	inline static int32_t get_offset_of__choiceMember_11() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00, ____choiceMember_11)); }
	inline String_t* get__choiceMember_11() const { return ____choiceMember_11; }
	inline String_t** get_address_of__choiceMember_11() { return &____choiceMember_11; }
	inline void set__choiceMember_11(String_t* value)
	{
		____choiceMember_11 = value;
		Il2CppCodeGenWriteBarrier((&____choiceMember_11), value);
	}

	inline static int32_t get_offset_of__isTextCollector_12() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00, ____isTextCollector_12)); }
	inline bool get__isTextCollector_12() const { return ____isTextCollector_12; }
	inline bool* get_address_of__isTextCollector_12() { return &____isTextCollector_12; }
	inline void set__isTextCollector_12(bool value)
	{
		____isTextCollector_12 = value;
	}

	inline static int32_t get_offset_of__choiceTypeData_13() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00, ____choiceTypeData_13)); }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * get__choiceTypeData_13() const { return ____choiceTypeData_13; }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 ** get_address_of__choiceTypeData_13() { return &____choiceTypeData_13; }
	inline void set__choiceTypeData_13(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * value)
	{
		____choiceTypeData_13 = value;
		Il2CppCodeGenWriteBarrier((&____choiceTypeData_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBERELEMENT_T51AF456B8ECA454E76169688454E82735FB1AE00_H
#ifndef XMLTYPEMAPMEMBERNAMESPACES_T08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43_H
#define XMLTYPEMAPMEMBERNAMESPACES_T08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberNamespaces
struct  XmlTypeMapMemberNamespaces_t08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43  : public XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBERNAMESPACES_T08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43_H
#ifndef AXISTYPE_T4CA4EB4650FD84E62398568AD4D97C8CE272F979_H
#define AXISTYPE_T4CA4EB4650FD84E62398568AD4D97C8CE272F979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Axis/AxisType
struct  AxisType_t4CA4EB4650FD84E62398568AD4D97C8CE272F979 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.Axis/AxisType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisType_t4CA4EB4650FD84E62398568AD4D97C8CE272F979, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTYPE_T4CA4EB4650FD84E62398568AD4D97C8CE272F979_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef CHOICENODE_T389906D9F3EDD9F3D3BA60CFCCD22670A7760F61_H
#define CHOICENODE_T389906D9F3EDD9F3D3BA60CFCCD22670A7760F61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ChoiceNode
struct  ChoiceNode_t389906D9F3EDD9F3D3BA60CFCCD22670A7760F61  : public InteriorNode_t274145A160D08BE97CA9437691E2AE9A4D30A51D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHOICENODE_T389906D9F3EDD9F3D3BA60CFCCD22670A7760F61_H
#ifndef CONSTRAINTROLE_TDF2C7F62C4BF46B0BDDB0F70B8FDE3DBF2BB4397_H
#define CONSTRAINTROLE_TDF2C7F62C4BF46B0BDDB0F70B8FDE3DBF2BB4397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.CompiledIdentityConstraint/ConstraintRole
struct  ConstraintRole_tDF2C7F62C4BF46B0BDDB0F70B8FDE3DBF2BB4397 
{
public:
	// System.Int32 System.Xml.Schema.CompiledIdentityConstraint/ConstraintRole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConstraintRole_tDF2C7F62C4BF46B0BDDB0F70B8FDE3DBF2BB4397, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTROLE_TDF2C7F62C4BF46B0BDDB0F70B8FDE3DBF2BB4397_H
#ifndef LEAFRANGENODE_T457FCA3B9C122C01958E0EBD8D1B9D54A5D723F5_H
#define LEAFRANGENODE_T457FCA3B9C122C01958E0EBD8D1B9D54A5D723F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.LeafRangeNode
struct  LeafRangeNode_t457FCA3B9C122C01958E0EBD8D1B9D54A5D723F5  : public LeafNode_tE2C955D778068BA019B8D106487A698656F863E2
{
public:
	// System.Decimal System.Xml.Schema.LeafRangeNode::min
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___min_1;
	// System.Decimal System.Xml.Schema.LeafRangeNode::max
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___max_2;
	// System.Xml.Schema.BitSet System.Xml.Schema.LeafRangeNode::nextIteration
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___nextIteration_3;

public:
	inline static int32_t get_offset_of_min_1() { return static_cast<int32_t>(offsetof(LeafRangeNode_t457FCA3B9C122C01958E0EBD8D1B9D54A5D723F5, ___min_1)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_min_1() const { return ___min_1; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_min_1() { return &___min_1; }
	inline void set_min_1(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___min_1 = value;
	}

	inline static int32_t get_offset_of_max_2() { return static_cast<int32_t>(offsetof(LeafRangeNode_t457FCA3B9C122C01958E0EBD8D1B9D54A5D723F5, ___max_2)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_max_2() const { return ___max_2; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_max_2() { return &___max_2; }
	inline void set_max_2(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___max_2 = value;
	}

	inline static int32_t get_offset_of_nextIteration_3() { return static_cast<int32_t>(offsetof(LeafRangeNode_t457FCA3B9C122C01958E0EBD8D1B9D54A5D723F5, ___nextIteration_3)); }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C * get_nextIteration_3() const { return ___nextIteration_3; }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C ** get_address_of_nextIteration_3() { return &___nextIteration_3; }
	inline void set_nextIteration_3(BitSet_t0E4C53EC600670A4B74C5671553596978880138C * value)
	{
		___nextIteration_3 = value;
		Il2CppCodeGenWriteBarrier((&___nextIteration_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEAFRANGENODE_T457FCA3B9C122C01958E0EBD8D1B9D54A5D723F5_H
#ifndef PLUSNODE_T906E55FC467F9EE2ADF99B88E9349101E28394B0_H
#define PLUSNODE_T906E55FC467F9EE2ADF99B88E9349101E28394B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.PlusNode
struct  PlusNode_t906E55FC467F9EE2ADF99B88E9349101E28394B0  : public InteriorNode_t274145A160D08BE97CA9437691E2AE9A4D30A51D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLUSNODE_T906E55FC467F9EE2ADF99B88E9349101E28394B0_H
#ifndef QMARKNODE_T42CEA81806E0B4CB67A2DA59BBF43F6279642716_H
#define QMARKNODE_T42CEA81806E0B4CB67A2DA59BBF43F6279642716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.QmarkNode
struct  QmarkNode_t42CEA81806E0B4CB67A2DA59BBF43F6279642716  : public InteriorNode_t274145A160D08BE97CA9437691E2AE9A4D30A51D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QMARKNODE_T42CEA81806E0B4CB67A2DA59BBF43F6279642716_H
#ifndef RESTRICTIONFLAGS_T638CE72420C3F9885392EE7E57E54B5C9DEA7D8D_H
#define RESTRICTIONFLAGS_T638CE72420C3F9885392EE7E57E54B5C9DEA7D8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.RestrictionFlags
struct  RestrictionFlags_t638CE72420C3F9885392EE7E57E54B5C9DEA7D8D 
{
public:
	// System.Int32 System.Xml.Schema.RestrictionFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RestrictionFlags_t638CE72420C3F9885392EE7E57E54B5C9DEA7D8D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTRICTIONFLAGS_T638CE72420C3F9885392EE7E57E54B5C9DEA7D8D_H
#ifndef SEQUENCENODE_TAB18F790CB1B5BCD1D984EECC17C841B00881608_H
#define SEQUENCENODE_TAB18F790CB1B5BCD1D984EECC17C841B00881608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SequenceNode
struct  SequenceNode_tAB18F790CB1B5BCD1D984EECC17C841B00881608  : public InteriorNode_t274145A160D08BE97CA9437691E2AE9A4D30A51D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENCENODE_TAB18F790CB1B5BCD1D984EECC17C841B00881608_H
#ifndef STARNODE_TDF30983D880A2C8707F144D32EA266A257045A30_H
#define STARNODE_TDF30983D880A2C8707F144D32EA266A257045A30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.StarNode
struct  StarNode_tDF30983D880A2C8707F144D32EA266A257045A30  : public InteriorNode_t274145A160D08BE97CA9437691E2AE9A4D30A51D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARNODE_TDF30983D880A2C8707F144D32EA266A257045A30_H
#ifndef XMLSCHEMACONTENTTYPE_TAAF4C8374963BFF5F4C3F71FAA1AE9E8944E03E5_H
#define XMLSCHEMACONTENTTYPE_TAAF4C8374963BFF5F4C3F71FAA1AE9E8944E03E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentType
struct  XmlSchemaContentType_tAAF4C8374963BFF5F4C3F71FAA1AE9E8944E03E5 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaContentType_tAAF4C8374963BFF5F4C3F71FAA1AE9E8944E03E5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTTYPE_TAAF4C8374963BFF5F4C3F71FAA1AE9E8944E03E5_H
#ifndef XMLSCHEMADATATYPEVARIETY_T26489B3A13FBF83AEBD1E94ECACD30AF091C9A02_H
#define XMLSCHEMADATATYPEVARIETY_T26489B3A13FBF83AEBD1E94ECACD30AF091C9A02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDatatypeVariety
struct  XmlSchemaDatatypeVariety_t26489B3A13FBF83AEBD1E94ECACD30AF091C9A02 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaDatatypeVariety::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaDatatypeVariety_t26489B3A13FBF83AEBD1E94ECACD30AF091C9A02, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADATATYPEVARIETY_T26489B3A13FBF83AEBD1E94ECACD30AF091C9A02_H
#ifndef XMLSCHEMAFORM_T86E3CD541907A35AC5B505EF118C3E20CC760276_H
#define XMLSCHEMAFORM_T86E3CD541907A35AC5B505EF118C3E20CC760276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaForm
struct  XmlSchemaForm_t86E3CD541907A35AC5B505EF118C3E20CC760276 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaForm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaForm_t86E3CD541907A35AC5B505EF118C3E20CC760276, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFORM_T86E3CD541907A35AC5B505EF118C3E20CC760276_H
#ifndef XMLSCHEMAWHITESPACE_T8E7AB044CFB43348933A839AB7D61D50501D6551_H
#define XMLSCHEMAWHITESPACE_T8E7AB044CFB43348933A839AB7D61D50501D6551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaWhiteSpace
struct  XmlSchemaWhiteSpace_t8E7AB044CFB43348933A839AB7D61D50501D6551 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaWhiteSpace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaWhiteSpace_t8E7AB044CFB43348933A839AB7D61D50501D6551, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAWHITESPACE_T8E7AB044CFB43348933A839AB7D61D50501D6551_H
#ifndef CLASSMAP_T7673ADE61E787987E671A668712C12D77FAAC6DA_H
#define CLASSMAP_T7673ADE61E787987E671A668712C12D77FAAC6DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.ClassMap
struct  ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA  : public ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677
{
public:
	// System.Collections.Hashtable System.Xml.Serialization.ClassMap::_elements
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____elements_0;
	// System.Collections.ArrayList System.Xml.Serialization.ClassMap::_elementMembers
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____elementMembers_1;
	// System.Collections.Hashtable System.Xml.Serialization.ClassMap::_attributeMembers
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____attributeMembers_2;
	// System.Xml.Serialization.XmlTypeMapMemberAttribute[] System.Xml.Serialization.ClassMap::_attributeMembersArray
	XmlTypeMapMemberAttributeU5BU5D_t1A3EF531DFF7CF757326012C417167F79DDE0D10* ____attributeMembersArray_3;
	// System.Collections.ArrayList System.Xml.Serialization.ClassMap::_flatLists
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____flatLists_4;
	// System.Collections.ArrayList System.Xml.Serialization.ClassMap::_allMembers
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____allMembers_5;
	// System.Collections.ArrayList System.Xml.Serialization.ClassMap::_membersWithDefault
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____membersWithDefault_6;
	// System.Collections.ArrayList System.Xml.Serialization.ClassMap::_listMembers
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____listMembers_7;
	// System.Xml.Serialization.XmlTypeMapMemberAnyElement System.Xml.Serialization.ClassMap::_defaultAnyElement
	XmlTypeMapMemberAnyElement_tEC6C4FFA530F292BC4C09145DA5937285A664472 * ____defaultAnyElement_8;
	// System.Xml.Serialization.XmlTypeMapMemberAnyAttribute System.Xml.Serialization.ClassMap::_defaultAnyAttribute
	XmlTypeMapMemberAnyAttribute_t0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE * ____defaultAnyAttribute_9;
	// System.Xml.Serialization.XmlTypeMapMemberNamespaces System.Xml.Serialization.ClassMap::_namespaceDeclarations
	XmlTypeMapMemberNamespaces_t08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43 * ____namespaceDeclarations_10;
	// System.Xml.Serialization.XmlTypeMapMember System.Xml.Serialization.ClassMap::_xmlTextCollector
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * ____xmlTextCollector_11;
	// System.Xml.Serialization.XmlTypeMapMember System.Xml.Serialization.ClassMap::_returnMember
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * ____returnMember_12;
	// System.Boolean System.Xml.Serialization.ClassMap::_ignoreMemberNamespace
	bool ____ignoreMemberNamespace_13;
	// System.Boolean System.Xml.Serialization.ClassMap::_canBeSimpleType
	bool ____canBeSimpleType_14;
	// System.Nullable`1<System.Boolean> System.Xml.Serialization.ClassMap::_isOrderDependentMap
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____isOrderDependentMap_15;

public:
	inline static int32_t get_offset_of__elements_0() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____elements_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__elements_0() const { return ____elements_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__elements_0() { return &____elements_0; }
	inline void set__elements_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____elements_0 = value;
		Il2CppCodeGenWriteBarrier((&____elements_0), value);
	}

	inline static int32_t get_offset_of__elementMembers_1() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____elementMembers_1)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__elementMembers_1() const { return ____elementMembers_1; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__elementMembers_1() { return &____elementMembers_1; }
	inline void set__elementMembers_1(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____elementMembers_1 = value;
		Il2CppCodeGenWriteBarrier((&____elementMembers_1), value);
	}

	inline static int32_t get_offset_of__attributeMembers_2() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____attributeMembers_2)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__attributeMembers_2() const { return ____attributeMembers_2; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__attributeMembers_2() { return &____attributeMembers_2; }
	inline void set__attributeMembers_2(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____attributeMembers_2 = value;
		Il2CppCodeGenWriteBarrier((&____attributeMembers_2), value);
	}

	inline static int32_t get_offset_of__attributeMembersArray_3() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____attributeMembersArray_3)); }
	inline XmlTypeMapMemberAttributeU5BU5D_t1A3EF531DFF7CF757326012C417167F79DDE0D10* get__attributeMembersArray_3() const { return ____attributeMembersArray_3; }
	inline XmlTypeMapMemberAttributeU5BU5D_t1A3EF531DFF7CF757326012C417167F79DDE0D10** get_address_of__attributeMembersArray_3() { return &____attributeMembersArray_3; }
	inline void set__attributeMembersArray_3(XmlTypeMapMemberAttributeU5BU5D_t1A3EF531DFF7CF757326012C417167F79DDE0D10* value)
	{
		____attributeMembersArray_3 = value;
		Il2CppCodeGenWriteBarrier((&____attributeMembersArray_3), value);
	}

	inline static int32_t get_offset_of__flatLists_4() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____flatLists_4)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__flatLists_4() const { return ____flatLists_4; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__flatLists_4() { return &____flatLists_4; }
	inline void set__flatLists_4(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____flatLists_4 = value;
		Il2CppCodeGenWriteBarrier((&____flatLists_4), value);
	}

	inline static int32_t get_offset_of__allMembers_5() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____allMembers_5)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__allMembers_5() const { return ____allMembers_5; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__allMembers_5() { return &____allMembers_5; }
	inline void set__allMembers_5(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____allMembers_5 = value;
		Il2CppCodeGenWriteBarrier((&____allMembers_5), value);
	}

	inline static int32_t get_offset_of__membersWithDefault_6() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____membersWithDefault_6)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__membersWithDefault_6() const { return ____membersWithDefault_6; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__membersWithDefault_6() { return &____membersWithDefault_6; }
	inline void set__membersWithDefault_6(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____membersWithDefault_6 = value;
		Il2CppCodeGenWriteBarrier((&____membersWithDefault_6), value);
	}

	inline static int32_t get_offset_of__listMembers_7() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____listMembers_7)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__listMembers_7() const { return ____listMembers_7; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__listMembers_7() { return &____listMembers_7; }
	inline void set__listMembers_7(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____listMembers_7 = value;
		Il2CppCodeGenWriteBarrier((&____listMembers_7), value);
	}

	inline static int32_t get_offset_of__defaultAnyElement_8() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____defaultAnyElement_8)); }
	inline XmlTypeMapMemberAnyElement_tEC6C4FFA530F292BC4C09145DA5937285A664472 * get__defaultAnyElement_8() const { return ____defaultAnyElement_8; }
	inline XmlTypeMapMemberAnyElement_tEC6C4FFA530F292BC4C09145DA5937285A664472 ** get_address_of__defaultAnyElement_8() { return &____defaultAnyElement_8; }
	inline void set__defaultAnyElement_8(XmlTypeMapMemberAnyElement_tEC6C4FFA530F292BC4C09145DA5937285A664472 * value)
	{
		____defaultAnyElement_8 = value;
		Il2CppCodeGenWriteBarrier((&____defaultAnyElement_8), value);
	}

	inline static int32_t get_offset_of__defaultAnyAttribute_9() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____defaultAnyAttribute_9)); }
	inline XmlTypeMapMemberAnyAttribute_t0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE * get__defaultAnyAttribute_9() const { return ____defaultAnyAttribute_9; }
	inline XmlTypeMapMemberAnyAttribute_t0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE ** get_address_of__defaultAnyAttribute_9() { return &____defaultAnyAttribute_9; }
	inline void set__defaultAnyAttribute_9(XmlTypeMapMemberAnyAttribute_t0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE * value)
	{
		____defaultAnyAttribute_9 = value;
		Il2CppCodeGenWriteBarrier((&____defaultAnyAttribute_9), value);
	}

	inline static int32_t get_offset_of__namespaceDeclarations_10() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____namespaceDeclarations_10)); }
	inline XmlTypeMapMemberNamespaces_t08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43 * get__namespaceDeclarations_10() const { return ____namespaceDeclarations_10; }
	inline XmlTypeMapMemberNamespaces_t08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43 ** get_address_of__namespaceDeclarations_10() { return &____namespaceDeclarations_10; }
	inline void set__namespaceDeclarations_10(XmlTypeMapMemberNamespaces_t08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43 * value)
	{
		____namespaceDeclarations_10 = value;
		Il2CppCodeGenWriteBarrier((&____namespaceDeclarations_10), value);
	}

	inline static int32_t get_offset_of__xmlTextCollector_11() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____xmlTextCollector_11)); }
	inline XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * get__xmlTextCollector_11() const { return ____xmlTextCollector_11; }
	inline XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D ** get_address_of__xmlTextCollector_11() { return &____xmlTextCollector_11; }
	inline void set__xmlTextCollector_11(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * value)
	{
		____xmlTextCollector_11 = value;
		Il2CppCodeGenWriteBarrier((&____xmlTextCollector_11), value);
	}

	inline static int32_t get_offset_of__returnMember_12() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____returnMember_12)); }
	inline XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * get__returnMember_12() const { return ____returnMember_12; }
	inline XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D ** get_address_of__returnMember_12() { return &____returnMember_12; }
	inline void set__returnMember_12(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * value)
	{
		____returnMember_12 = value;
		Il2CppCodeGenWriteBarrier((&____returnMember_12), value);
	}

	inline static int32_t get_offset_of__ignoreMemberNamespace_13() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____ignoreMemberNamespace_13)); }
	inline bool get__ignoreMemberNamespace_13() const { return ____ignoreMemberNamespace_13; }
	inline bool* get_address_of__ignoreMemberNamespace_13() { return &____ignoreMemberNamespace_13; }
	inline void set__ignoreMemberNamespace_13(bool value)
	{
		____ignoreMemberNamespace_13 = value;
	}

	inline static int32_t get_offset_of__canBeSimpleType_14() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____canBeSimpleType_14)); }
	inline bool get__canBeSimpleType_14() const { return ____canBeSimpleType_14; }
	inline bool* get_address_of__canBeSimpleType_14() { return &____canBeSimpleType_14; }
	inline void set__canBeSimpleType_14(bool value)
	{
		____canBeSimpleType_14 = value;
	}

	inline static int32_t get_offset_of__isOrderDependentMap_15() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____isOrderDependentMap_15)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__isOrderDependentMap_15() const { return ____isOrderDependentMap_15; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__isOrderDependentMap_15() { return &____isOrderDependentMap_15; }
	inline void set__isOrderDependentMap_15(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____isOrderDependentMap_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSMAP_T7673ADE61E787987E671A668712C12D77FAAC6DA_H
#ifndef SERIALIZATIONFORMAT_T0A1D537B68CA8F48ACBF510EEA30EFD70F1602A3_H
#define SERIALIZATIONFORMAT_T0A1D537B68CA8F48ACBF510EEA30EFD70F1602A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.SerializationFormat
struct  SerializationFormat_t0A1D537B68CA8F48ACBF510EEA30EFD70F1602A3 
{
public:
	// System.Int32 System.Xml.Serialization.SerializationFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SerializationFormat_t0A1D537B68CA8F48ACBF510EEA30EFD70F1602A3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONFORMAT_T0A1D537B68CA8F48ACBF510EEA30EFD70F1602A3_H
#ifndef XMLTYPEMAPMEMBEREXPANDABLE_TEFC74196D6503AB38EEA1AD371BA55EABC7C30ED_H
#define XMLTYPEMAPMEMBEREXPANDABLE_TEFC74196D6503AB38EEA1AD371BA55EABC7C30ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberExpandable
struct  XmlTypeMapMemberExpandable_tEFC74196D6503AB38EEA1AD371BA55EABC7C30ED  : public XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00
{
public:
	// System.Int32 System.Xml.Serialization.XmlTypeMapMemberExpandable::_flatArrayIndex
	int32_t ____flatArrayIndex_14;

public:
	inline static int32_t get_offset_of__flatArrayIndex_14() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberExpandable_tEFC74196D6503AB38EEA1AD371BA55EABC7C30ED, ____flatArrayIndex_14)); }
	inline int32_t get__flatArrayIndex_14() const { return ____flatArrayIndex_14; }
	inline int32_t* get_address_of__flatArrayIndex_14() { return &____flatArrayIndex_14; }
	inline void set__flatArrayIndex_14(int32_t value)
	{
		____flatArrayIndex_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBEREXPANDABLE_TEFC74196D6503AB38EEA1AD371BA55EABC7C30ED_H
#ifndef XMLTYPEMAPMEMBERLIST_T408EB21F4809E9C2E2059BDE5F77F28F503EA48F_H
#define XMLTYPEMAPMEMBERLIST_T408EB21F4809E9C2E2059BDE5F77F28F503EA48F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberList
struct  XmlTypeMapMemberList_t408EB21F4809E9C2E2059BDE5F77F28F503EA48F  : public XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBERLIST_T408EB21F4809E9C2E2059BDE5F77F28F503EA48F_H
#ifndef XPATHNODETYPE_TB1DB3F39502492756781F03A9645DD587B81BEF2_H
#define XPATHNODETYPE_TB1DB3F39502492756781F03A9645DD587B81BEF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNodeType
struct  XPathNodeType_tB1DB3F39502492756781F03A9645DD587B81BEF2 
{
public:
	// System.Int32 System.Xml.XPath.XPathNodeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XPathNodeType_tB1DB3F39502492756781F03A9645DD587B81BEF2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODETYPE_TB1DB3F39502492756781F03A9645DD587B81BEF2_H
#ifndef AXIS_T27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70_H
#define AXIS_T27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Axis
struct  Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70  : public AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C
{
public:
	// MS.Internal.Xml.XPath.Axis/AxisType MS.Internal.Xml.XPath.Axis::axisType
	int32_t ___axisType_0;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Axis::input
	AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C * ___input_1;
	// System.String MS.Internal.Xml.XPath.Axis::prefix
	String_t* ___prefix_2;
	// System.String MS.Internal.Xml.XPath.Axis::name
	String_t* ___name_3;
	// System.Xml.XPath.XPathNodeType MS.Internal.Xml.XPath.Axis::nodeType
	int32_t ___nodeType_4;
	// System.Boolean MS.Internal.Xml.XPath.Axis::abbrAxis
	bool ___abbrAxis_5;
	// System.String MS.Internal.Xml.XPath.Axis::urn
	String_t* ___urn_6;

public:
	inline static int32_t get_offset_of_axisType_0() { return static_cast<int32_t>(offsetof(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70, ___axisType_0)); }
	inline int32_t get_axisType_0() const { return ___axisType_0; }
	inline int32_t* get_address_of_axisType_0() { return &___axisType_0; }
	inline void set_axisType_0(int32_t value)
	{
		___axisType_0 = value;
	}

	inline static int32_t get_offset_of_input_1() { return static_cast<int32_t>(offsetof(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70, ___input_1)); }
	inline AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C * get_input_1() const { return ___input_1; }
	inline AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C ** get_address_of_input_1() { return &___input_1; }
	inline void set_input_1(AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C * value)
	{
		___input_1 = value;
		Il2CppCodeGenWriteBarrier((&___input_1), value);
	}

	inline static int32_t get_offset_of_prefix_2() { return static_cast<int32_t>(offsetof(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70, ___prefix_2)); }
	inline String_t* get_prefix_2() const { return ___prefix_2; }
	inline String_t** get_address_of_prefix_2() { return &___prefix_2; }
	inline void set_prefix_2(String_t* value)
	{
		___prefix_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_2), value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_nodeType_4() { return static_cast<int32_t>(offsetof(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70, ___nodeType_4)); }
	inline int32_t get_nodeType_4() const { return ___nodeType_4; }
	inline int32_t* get_address_of_nodeType_4() { return &___nodeType_4; }
	inline void set_nodeType_4(int32_t value)
	{
		___nodeType_4 = value;
	}

	inline static int32_t get_offset_of_abbrAxis_5() { return static_cast<int32_t>(offsetof(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70, ___abbrAxis_5)); }
	inline bool get_abbrAxis_5() const { return ___abbrAxis_5; }
	inline bool* get_address_of_abbrAxis_5() { return &___abbrAxis_5; }
	inline void set_abbrAxis_5(bool value)
	{
		___abbrAxis_5 = value;
	}

	inline static int32_t get_offset_of_urn_6() { return static_cast<int32_t>(offsetof(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70, ___urn_6)); }
	inline String_t* get_urn_6() const { return ___urn_6; }
	inline String_t** get_address_of_urn_6() { return &___urn_6; }
	inline void set_urn_6(String_t* value)
	{
		___urn_6 = value;
		Il2CppCodeGenWriteBarrier((&___urn_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPILEDIDENTITYCONSTRAINT_T6BF02C65AF10432D69198C6F7B1D6874BDB75A9E_H
#define COMPILEDIDENTITYCONSTRAINT_T6BF02C65AF10432D69198C6F7B1D6874BDB75A9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.CompiledIdentityConstraint
struct  CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E  : public RuntimeObject
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.CompiledIdentityConstraint::name
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___name_0;
	// System.Xml.Schema.CompiledIdentityConstraint/ConstraintRole System.Xml.Schema.CompiledIdentityConstraint::role
	int32_t ___role_1;
	// System.Xml.Schema.Asttree System.Xml.Schema.CompiledIdentityConstraint::selector
	Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA * ___selector_2;
	// System.Xml.Schema.Asttree[] System.Xml.Schema.CompiledIdentityConstraint::fields
	AsttreeU5BU5D_tE98D3D56BDC1A5B380B40C34285F212D51BF37D3* ___fields_3;
	// System.Xml.XmlQualifiedName System.Xml.Schema.CompiledIdentityConstraint::refer
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___refer_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E, ___name_0)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_name_0() const { return ___name_0; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_role_1() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E, ___role_1)); }
	inline int32_t get_role_1() const { return ___role_1; }
	inline int32_t* get_address_of_role_1() { return &___role_1; }
	inline void set_role_1(int32_t value)
	{
		___role_1 = value;
	}

	inline static int32_t get_offset_of_selector_2() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E, ___selector_2)); }
	inline Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA * get_selector_2() const { return ___selector_2; }
	inline Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA ** get_address_of_selector_2() { return &___selector_2; }
	inline void set_selector_2(Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA * value)
	{
		___selector_2 = value;
		Il2CppCodeGenWriteBarrier((&___selector_2), value);
	}

	inline static int32_t get_offset_of_fields_3() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E, ___fields_3)); }
	inline AsttreeU5BU5D_tE98D3D56BDC1A5B380B40C34285F212D51BF37D3* get_fields_3() const { return ___fields_3; }
	inline AsttreeU5BU5D_tE98D3D56BDC1A5B380B40C34285F212D51BF37D3** get_address_of_fields_3() { return &___fields_3; }
	inline void set_fields_3(AsttreeU5BU5D_tE98D3D56BDC1A5B380B40C34285F212D51BF37D3* value)
	{
		___fields_3 = value;
		Il2CppCodeGenWriteBarrier((&___fields_3), value);
	}

	inline static int32_t get_offset_of_refer_4() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E, ___refer_4)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_refer_4() const { return ___refer_4; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_refer_4() { return &___refer_4; }
	inline void set_refer_4(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___refer_4 = value;
		Il2CppCodeGenWriteBarrier((&___refer_4), value);
	}
};

struct CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E_StaticFields
{
public:
	// System.Xml.Schema.CompiledIdentityConstraint System.Xml.Schema.CompiledIdentityConstraint::Empty
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E * ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E_StaticFields, ___Empty_5)); }
	inline CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E * get_Empty_5() const { return ___Empty_5; }
	inline CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E ** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E * value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILEDIDENTITYCONSTRAINT_T6BF02C65AF10432D69198C6F7B1D6874BDB75A9E_H
#ifndef CONTENTVALIDATOR_TF701012C2DEDAD821FACDC630F99D45BFCCC03AA_H
#define CONTENTVALIDATOR_TF701012C2DEDAD821FACDC630F99D45BFCCC03AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ContentValidator
struct  ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaContentType System.Xml.Schema.ContentValidator::contentType
	int32_t ___contentType_0;
	// System.Boolean System.Xml.Schema.ContentValidator::isOpen
	bool ___isOpen_1;
	// System.Boolean System.Xml.Schema.ContentValidator::isEmptiable
	bool ___isEmptiable_2;

public:
	inline static int32_t get_offset_of_contentType_0() { return static_cast<int32_t>(offsetof(ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA, ___contentType_0)); }
	inline int32_t get_contentType_0() const { return ___contentType_0; }
	inline int32_t* get_address_of_contentType_0() { return &___contentType_0; }
	inline void set_contentType_0(int32_t value)
	{
		___contentType_0 = value;
	}

	inline static int32_t get_offset_of_isOpen_1() { return static_cast<int32_t>(offsetof(ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA, ___isOpen_1)); }
	inline bool get_isOpen_1() const { return ___isOpen_1; }
	inline bool* get_address_of_isOpen_1() { return &___isOpen_1; }
	inline void set_isOpen_1(bool value)
	{
		___isOpen_1 = value;
	}

	inline static int32_t get_offset_of_isEmptiable_2() { return static_cast<int32_t>(offsetof(ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA, ___isEmptiable_2)); }
	inline bool get_isEmptiable_2() const { return ___isEmptiable_2; }
	inline bool* get_address_of_isEmptiable_2() { return &___isEmptiable_2; }
	inline void set_isEmptiable_2(bool value)
	{
		___isEmptiable_2 = value;
	}
};

struct ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA_StaticFields
{
public:
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::Empty
	ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA * ___Empty_3;
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::TextOnly
	ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA * ___TextOnly_4;
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::Mixed
	ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA * ___Mixed_5;
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::Any
	ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA * ___Any_6;

public:
	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA_StaticFields, ___Empty_3)); }
	inline ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA * get_Empty_3() const { return ___Empty_3; }
	inline ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA ** get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA * value)
	{
		___Empty_3 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_3), value);
	}

	inline static int32_t get_offset_of_TextOnly_4() { return static_cast<int32_t>(offsetof(ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA_StaticFields, ___TextOnly_4)); }
	inline ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA * get_TextOnly_4() const { return ___TextOnly_4; }
	inline ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA ** get_address_of_TextOnly_4() { return &___TextOnly_4; }
	inline void set_TextOnly_4(ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA * value)
	{
		___TextOnly_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextOnly_4), value);
	}

	inline static int32_t get_offset_of_Mixed_5() { return static_cast<int32_t>(offsetof(ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA_StaticFields, ___Mixed_5)); }
	inline ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA * get_Mixed_5() const { return ___Mixed_5; }
	inline ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA ** get_address_of_Mixed_5() { return &___Mixed_5; }
	inline void set_Mixed_5(ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA * value)
	{
		___Mixed_5 = value;
		Il2CppCodeGenWriteBarrier((&___Mixed_5), value);
	}

	inline static int32_t get_offset_of_Any_6() { return static_cast<int32_t>(offsetof(ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA_StaticFields, ___Any_6)); }
	inline ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA * get_Any_6() const { return ___Any_6; }
	inline ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA ** get_address_of_Any_6() { return &___Any_6; }
	inline void set_Any_6(ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA * value)
	{
		___Any_6 = value;
		Il2CppCodeGenWriteBarrier((&___Any_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTVALIDATOR_TF701012C2DEDAD821FACDC630F99D45BFCCC03AA_H
#ifndef DATATYPEIMPLEMENTATION_TB5894E69998AF6871FDFC14B2A95AE5F07CBB836_H
#define DATATYPEIMPLEMENTATION_TB5894E69998AF6871FDFC14B2A95AE5F07CBB836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DatatypeImplementation
struct  DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836  : public XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550
{
public:
	// System.Xml.Schema.XmlSchemaDatatypeVariety System.Xml.Schema.DatatypeImplementation::variety
	int32_t ___variety_0;
	// System.Xml.Schema.RestrictionFacets System.Xml.Schema.DatatypeImplementation::restriction
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4 * ___restriction_1;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::baseType
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___baseType_2;
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.DatatypeImplementation::valueConverter
	XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * ___valueConverter_3;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.DatatypeImplementation::parentSchemaType
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * ___parentSchemaType_4;

public:
	inline static int32_t get_offset_of_variety_0() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836, ___variety_0)); }
	inline int32_t get_variety_0() const { return ___variety_0; }
	inline int32_t* get_address_of_variety_0() { return &___variety_0; }
	inline void set_variety_0(int32_t value)
	{
		___variety_0 = value;
	}

	inline static int32_t get_offset_of_restriction_1() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836, ___restriction_1)); }
	inline RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4 * get_restriction_1() const { return ___restriction_1; }
	inline RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4 ** get_address_of_restriction_1() { return &___restriction_1; }
	inline void set_restriction_1(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4 * value)
	{
		___restriction_1 = value;
		Il2CppCodeGenWriteBarrier((&___restriction_1), value);
	}

	inline static int32_t get_offset_of_baseType_2() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836, ___baseType_2)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_baseType_2() const { return ___baseType_2; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_baseType_2() { return &___baseType_2; }
	inline void set_baseType_2(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___baseType_2 = value;
		Il2CppCodeGenWriteBarrier((&___baseType_2), value);
	}

	inline static int32_t get_offset_of_valueConverter_3() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836, ___valueConverter_3)); }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * get_valueConverter_3() const { return ___valueConverter_3; }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E ** get_address_of_valueConverter_3() { return &___valueConverter_3; }
	inline void set_valueConverter_3(XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * value)
	{
		___valueConverter_3 = value;
		Il2CppCodeGenWriteBarrier((&___valueConverter_3), value);
	}

	inline static int32_t get_offset_of_parentSchemaType_4() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836, ___parentSchemaType_4)); }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * get_parentSchemaType_4() const { return ___parentSchemaType_4; }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 ** get_address_of_parentSchemaType_4() { return &___parentSchemaType_4; }
	inline void set_parentSchemaType_4(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * value)
	{
		___parentSchemaType_4 = value;
		Il2CppCodeGenWriteBarrier((&___parentSchemaType_4), value);
	}
};

struct DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields
{
public:
	// System.Collections.Hashtable System.Xml.Schema.DatatypeImplementation::builtinTypes
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___builtinTypes_5;
	// System.Xml.Schema.XmlSchemaSimpleType[] System.Xml.Schema.DatatypeImplementation::enumToTypeCode
	XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B* ___enumToTypeCode_6;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::anySimpleType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___anySimpleType_7;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::anyAtomicType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___anyAtomicType_8;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::untypedAtomicType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___untypedAtomicType_9;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::yearMonthDurationType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___yearMonthDurationType_10;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::dayTimeDurationType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___dayTimeDurationType_11;
	// System.Xml.Schema.XmlSchemaSimpleType modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.Schema.DatatypeImplementation::normalizedStringTypeV1Compat
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___normalizedStringTypeV1Compat_12;
	// System.Xml.Schema.XmlSchemaSimpleType modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.Schema.DatatypeImplementation::tokenTypeV1Compat
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___tokenTypeV1Compat_13;
	// System.Xml.XmlQualifiedName System.Xml.Schema.DatatypeImplementation::QnAnySimpleType
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnAnySimpleType_14;
	// System.Xml.XmlQualifiedName System.Xml.Schema.DatatypeImplementation::QnAnyType
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnAnyType_15;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::stringFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___stringFacetsChecker_16;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::miscFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___miscFacetsChecker_17;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::numeric2FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric2FacetsChecker_18;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::binaryFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___binaryFacetsChecker_19;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::dateTimeFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___dateTimeFacetsChecker_20;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::durationFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___durationFacetsChecker_21;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::listFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___listFacetsChecker_22;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::qnameFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___qnameFacetsChecker_23;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::unionFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___unionFacetsChecker_24;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_anySimpleType
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_anySimpleType_25;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_anyURI
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_anyURI_26;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_base64Binary
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_base64Binary_27;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_boolean
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_boolean_28;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_byte
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_byte_29;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_char
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_char_30;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_date
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_date_31;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dateTime
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_dateTime_32;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dateTimeNoTz
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_dateTimeNoTz_33;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dateTimeTz
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_dateTimeTz_34;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_day
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_day_35;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_decimal
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_decimal_36;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_double
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_double_37;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_doubleXdr
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_doubleXdr_38;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_duration
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_duration_39;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ENTITY
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_ENTITY_40;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ENTITIES
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_ENTITIES_41;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ENUMERATION
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_ENUMERATION_42;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_fixed
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_fixed_43;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_float
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_float_44;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_floatXdr
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_floatXdr_45;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_hexBinary
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_hexBinary_46;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ID
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_ID_47;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_IDREF
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_IDREF_48;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_IDREFS
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_IDREFS_49;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_int
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_int_50;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_integer
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_integer_51;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_language
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_language_52;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_long
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_long_53;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_month
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_month_54;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_monthDay
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_monthDay_55;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_Name
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_Name_56;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NCName
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_NCName_57;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_negativeInteger
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_negativeInteger_58;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NMTOKEN
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_NMTOKEN_59;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NMTOKENS
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_NMTOKENS_60;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_nonNegativeInteger
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_nonNegativeInteger_61;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_nonPositiveInteger
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_nonPositiveInteger_62;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_normalizedString
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_normalizedString_63;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NOTATION
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_NOTATION_64;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_positiveInteger
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_positiveInteger_65;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_QName
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_QName_66;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_QNameXdr
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_QNameXdr_67;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_short
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_short_68;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_string
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_string_69;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_time
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_time_70;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_timeNoTz
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_timeNoTz_71;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_timeTz
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_timeTz_72;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_token
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_token_73;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedByte
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_unsignedByte_74;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedInt
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_unsignedInt_75;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedLong
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_unsignedLong_76;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedShort
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_unsignedShort_77;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_uuid
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_uuid_78;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_year
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_year_79;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_yearMonth
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_yearMonth_80;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_normalizedStringV1Compat
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_normalizedStringV1Compat_81;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_tokenV1Compat
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_tokenV1Compat_82;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_anyAtomicType
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_anyAtomicType_83;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dayTimeDuration
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_dayTimeDuration_84;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_untypedAtomicType
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_untypedAtomicType_85;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_yearMonthDuration
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_yearMonthDuration_86;
	// System.Xml.Schema.DatatypeImplementation[] System.Xml.Schema.DatatypeImplementation::c_tokenizedTypes
	DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36* ___c_tokenizedTypes_87;
	// System.Xml.Schema.DatatypeImplementation[] System.Xml.Schema.DatatypeImplementation::c_tokenizedTypesXsd
	DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36* ___c_tokenizedTypesXsd_88;
	// System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap[] System.Xml.Schema.DatatypeImplementation::c_XdrTypes
	SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7* ___c_XdrTypes_89;
	// System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap[] System.Xml.Schema.DatatypeImplementation::c_XsdTypes
	SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7* ___c_XsdTypes_90;

public:
	inline static int32_t get_offset_of_builtinTypes_5() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___builtinTypes_5)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_builtinTypes_5() const { return ___builtinTypes_5; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_builtinTypes_5() { return &___builtinTypes_5; }
	inline void set_builtinTypes_5(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___builtinTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___builtinTypes_5), value);
	}

	inline static int32_t get_offset_of_enumToTypeCode_6() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___enumToTypeCode_6)); }
	inline XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B* get_enumToTypeCode_6() const { return ___enumToTypeCode_6; }
	inline XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B** get_address_of_enumToTypeCode_6() { return &___enumToTypeCode_6; }
	inline void set_enumToTypeCode_6(XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B* value)
	{
		___enumToTypeCode_6 = value;
		Il2CppCodeGenWriteBarrier((&___enumToTypeCode_6), value);
	}

	inline static int32_t get_offset_of_anySimpleType_7() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___anySimpleType_7)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_anySimpleType_7() const { return ___anySimpleType_7; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_anySimpleType_7() { return &___anySimpleType_7; }
	inline void set_anySimpleType_7(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___anySimpleType_7 = value;
		Il2CppCodeGenWriteBarrier((&___anySimpleType_7), value);
	}

	inline static int32_t get_offset_of_anyAtomicType_8() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___anyAtomicType_8)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_anyAtomicType_8() const { return ___anyAtomicType_8; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_anyAtomicType_8() { return &___anyAtomicType_8; }
	inline void set_anyAtomicType_8(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___anyAtomicType_8 = value;
		Il2CppCodeGenWriteBarrier((&___anyAtomicType_8), value);
	}

	inline static int32_t get_offset_of_untypedAtomicType_9() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___untypedAtomicType_9)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_untypedAtomicType_9() const { return ___untypedAtomicType_9; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_untypedAtomicType_9() { return &___untypedAtomicType_9; }
	inline void set_untypedAtomicType_9(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___untypedAtomicType_9 = value;
		Il2CppCodeGenWriteBarrier((&___untypedAtomicType_9), value);
	}

	inline static int32_t get_offset_of_yearMonthDurationType_10() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___yearMonthDurationType_10)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_yearMonthDurationType_10() const { return ___yearMonthDurationType_10; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_yearMonthDurationType_10() { return &___yearMonthDurationType_10; }
	inline void set_yearMonthDurationType_10(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___yearMonthDurationType_10 = value;
		Il2CppCodeGenWriteBarrier((&___yearMonthDurationType_10), value);
	}

	inline static int32_t get_offset_of_dayTimeDurationType_11() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___dayTimeDurationType_11)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_dayTimeDurationType_11() const { return ___dayTimeDurationType_11; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_dayTimeDurationType_11() { return &___dayTimeDurationType_11; }
	inline void set_dayTimeDurationType_11(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___dayTimeDurationType_11 = value;
		Il2CppCodeGenWriteBarrier((&___dayTimeDurationType_11), value);
	}

	inline static int32_t get_offset_of_normalizedStringTypeV1Compat_12() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___normalizedStringTypeV1Compat_12)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_normalizedStringTypeV1Compat_12() const { return ___normalizedStringTypeV1Compat_12; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_normalizedStringTypeV1Compat_12() { return &___normalizedStringTypeV1Compat_12; }
	inline void set_normalizedStringTypeV1Compat_12(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___normalizedStringTypeV1Compat_12 = value;
		Il2CppCodeGenWriteBarrier((&___normalizedStringTypeV1Compat_12), value);
	}

	inline static int32_t get_offset_of_tokenTypeV1Compat_13() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___tokenTypeV1Compat_13)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_tokenTypeV1Compat_13() const { return ___tokenTypeV1Compat_13; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_tokenTypeV1Compat_13() { return &___tokenTypeV1Compat_13; }
	inline void set_tokenTypeV1Compat_13(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___tokenTypeV1Compat_13 = value;
		Il2CppCodeGenWriteBarrier((&___tokenTypeV1Compat_13), value);
	}

	inline static int32_t get_offset_of_QnAnySimpleType_14() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___QnAnySimpleType_14)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnAnySimpleType_14() const { return ___QnAnySimpleType_14; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnAnySimpleType_14() { return &___QnAnySimpleType_14; }
	inline void set_QnAnySimpleType_14(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnAnySimpleType_14 = value;
		Il2CppCodeGenWriteBarrier((&___QnAnySimpleType_14), value);
	}

	inline static int32_t get_offset_of_QnAnyType_15() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___QnAnyType_15)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnAnyType_15() const { return ___QnAnyType_15; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnAnyType_15() { return &___QnAnyType_15; }
	inline void set_QnAnyType_15(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnAnyType_15 = value;
		Il2CppCodeGenWriteBarrier((&___QnAnyType_15), value);
	}

	inline static int32_t get_offset_of_stringFacetsChecker_16() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___stringFacetsChecker_16)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_stringFacetsChecker_16() const { return ___stringFacetsChecker_16; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_stringFacetsChecker_16() { return &___stringFacetsChecker_16; }
	inline void set_stringFacetsChecker_16(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___stringFacetsChecker_16 = value;
		Il2CppCodeGenWriteBarrier((&___stringFacetsChecker_16), value);
	}

	inline static int32_t get_offset_of_miscFacetsChecker_17() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___miscFacetsChecker_17)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_miscFacetsChecker_17() const { return ___miscFacetsChecker_17; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_miscFacetsChecker_17() { return &___miscFacetsChecker_17; }
	inline void set_miscFacetsChecker_17(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___miscFacetsChecker_17 = value;
		Il2CppCodeGenWriteBarrier((&___miscFacetsChecker_17), value);
	}

	inline static int32_t get_offset_of_numeric2FacetsChecker_18() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___numeric2FacetsChecker_18)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric2FacetsChecker_18() const { return ___numeric2FacetsChecker_18; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric2FacetsChecker_18() { return &___numeric2FacetsChecker_18; }
	inline void set_numeric2FacetsChecker_18(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric2FacetsChecker_18 = value;
		Il2CppCodeGenWriteBarrier((&___numeric2FacetsChecker_18), value);
	}

	inline static int32_t get_offset_of_binaryFacetsChecker_19() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___binaryFacetsChecker_19)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_binaryFacetsChecker_19() const { return ___binaryFacetsChecker_19; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_binaryFacetsChecker_19() { return &___binaryFacetsChecker_19; }
	inline void set_binaryFacetsChecker_19(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___binaryFacetsChecker_19 = value;
		Il2CppCodeGenWriteBarrier((&___binaryFacetsChecker_19), value);
	}

	inline static int32_t get_offset_of_dateTimeFacetsChecker_20() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___dateTimeFacetsChecker_20)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_dateTimeFacetsChecker_20() const { return ___dateTimeFacetsChecker_20; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_dateTimeFacetsChecker_20() { return &___dateTimeFacetsChecker_20; }
	inline void set_dateTimeFacetsChecker_20(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___dateTimeFacetsChecker_20 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeFacetsChecker_20), value);
	}

	inline static int32_t get_offset_of_durationFacetsChecker_21() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___durationFacetsChecker_21)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_durationFacetsChecker_21() const { return ___durationFacetsChecker_21; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_durationFacetsChecker_21() { return &___durationFacetsChecker_21; }
	inline void set_durationFacetsChecker_21(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___durationFacetsChecker_21 = value;
		Il2CppCodeGenWriteBarrier((&___durationFacetsChecker_21), value);
	}

	inline static int32_t get_offset_of_listFacetsChecker_22() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___listFacetsChecker_22)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_listFacetsChecker_22() const { return ___listFacetsChecker_22; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_listFacetsChecker_22() { return &___listFacetsChecker_22; }
	inline void set_listFacetsChecker_22(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___listFacetsChecker_22 = value;
		Il2CppCodeGenWriteBarrier((&___listFacetsChecker_22), value);
	}

	inline static int32_t get_offset_of_qnameFacetsChecker_23() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___qnameFacetsChecker_23)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_qnameFacetsChecker_23() const { return ___qnameFacetsChecker_23; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_qnameFacetsChecker_23() { return &___qnameFacetsChecker_23; }
	inline void set_qnameFacetsChecker_23(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___qnameFacetsChecker_23 = value;
		Il2CppCodeGenWriteBarrier((&___qnameFacetsChecker_23), value);
	}

	inline static int32_t get_offset_of_unionFacetsChecker_24() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___unionFacetsChecker_24)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_unionFacetsChecker_24() const { return ___unionFacetsChecker_24; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_unionFacetsChecker_24() { return &___unionFacetsChecker_24; }
	inline void set_unionFacetsChecker_24(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___unionFacetsChecker_24 = value;
		Il2CppCodeGenWriteBarrier((&___unionFacetsChecker_24), value);
	}

	inline static int32_t get_offset_of_c_anySimpleType_25() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_anySimpleType_25)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_anySimpleType_25() const { return ___c_anySimpleType_25; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_anySimpleType_25() { return &___c_anySimpleType_25; }
	inline void set_c_anySimpleType_25(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_anySimpleType_25 = value;
		Il2CppCodeGenWriteBarrier((&___c_anySimpleType_25), value);
	}

	inline static int32_t get_offset_of_c_anyURI_26() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_anyURI_26)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_anyURI_26() const { return ___c_anyURI_26; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_anyURI_26() { return &___c_anyURI_26; }
	inline void set_c_anyURI_26(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_anyURI_26 = value;
		Il2CppCodeGenWriteBarrier((&___c_anyURI_26), value);
	}

	inline static int32_t get_offset_of_c_base64Binary_27() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_base64Binary_27)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_base64Binary_27() const { return ___c_base64Binary_27; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_base64Binary_27() { return &___c_base64Binary_27; }
	inline void set_c_base64Binary_27(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_base64Binary_27 = value;
		Il2CppCodeGenWriteBarrier((&___c_base64Binary_27), value);
	}

	inline static int32_t get_offset_of_c_boolean_28() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_boolean_28)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_boolean_28() const { return ___c_boolean_28; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_boolean_28() { return &___c_boolean_28; }
	inline void set_c_boolean_28(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_boolean_28 = value;
		Il2CppCodeGenWriteBarrier((&___c_boolean_28), value);
	}

	inline static int32_t get_offset_of_c_byte_29() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_byte_29)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_byte_29() const { return ___c_byte_29; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_byte_29() { return &___c_byte_29; }
	inline void set_c_byte_29(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_byte_29 = value;
		Il2CppCodeGenWriteBarrier((&___c_byte_29), value);
	}

	inline static int32_t get_offset_of_c_char_30() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_char_30)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_char_30() const { return ___c_char_30; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_char_30() { return &___c_char_30; }
	inline void set_c_char_30(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_char_30 = value;
		Il2CppCodeGenWriteBarrier((&___c_char_30), value);
	}

	inline static int32_t get_offset_of_c_date_31() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_date_31)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_date_31() const { return ___c_date_31; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_date_31() { return &___c_date_31; }
	inline void set_c_date_31(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_date_31 = value;
		Il2CppCodeGenWriteBarrier((&___c_date_31), value);
	}

	inline static int32_t get_offset_of_c_dateTime_32() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_dateTime_32)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_dateTime_32() const { return ___c_dateTime_32; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_dateTime_32() { return &___c_dateTime_32; }
	inline void set_c_dateTime_32(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_dateTime_32 = value;
		Il2CppCodeGenWriteBarrier((&___c_dateTime_32), value);
	}

	inline static int32_t get_offset_of_c_dateTimeNoTz_33() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_dateTimeNoTz_33)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_dateTimeNoTz_33() const { return ___c_dateTimeNoTz_33; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_dateTimeNoTz_33() { return &___c_dateTimeNoTz_33; }
	inline void set_c_dateTimeNoTz_33(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_dateTimeNoTz_33 = value;
		Il2CppCodeGenWriteBarrier((&___c_dateTimeNoTz_33), value);
	}

	inline static int32_t get_offset_of_c_dateTimeTz_34() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_dateTimeTz_34)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_dateTimeTz_34() const { return ___c_dateTimeTz_34; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_dateTimeTz_34() { return &___c_dateTimeTz_34; }
	inline void set_c_dateTimeTz_34(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_dateTimeTz_34 = value;
		Il2CppCodeGenWriteBarrier((&___c_dateTimeTz_34), value);
	}

	inline static int32_t get_offset_of_c_day_35() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_day_35)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_day_35() const { return ___c_day_35; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_day_35() { return &___c_day_35; }
	inline void set_c_day_35(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_day_35 = value;
		Il2CppCodeGenWriteBarrier((&___c_day_35), value);
	}

	inline static int32_t get_offset_of_c_decimal_36() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_decimal_36)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_decimal_36() const { return ___c_decimal_36; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_decimal_36() { return &___c_decimal_36; }
	inline void set_c_decimal_36(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_decimal_36 = value;
		Il2CppCodeGenWriteBarrier((&___c_decimal_36), value);
	}

	inline static int32_t get_offset_of_c_double_37() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_double_37)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_double_37() const { return ___c_double_37; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_double_37() { return &___c_double_37; }
	inline void set_c_double_37(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_double_37 = value;
		Il2CppCodeGenWriteBarrier((&___c_double_37), value);
	}

	inline static int32_t get_offset_of_c_doubleXdr_38() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_doubleXdr_38)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_doubleXdr_38() const { return ___c_doubleXdr_38; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_doubleXdr_38() { return &___c_doubleXdr_38; }
	inline void set_c_doubleXdr_38(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_doubleXdr_38 = value;
		Il2CppCodeGenWriteBarrier((&___c_doubleXdr_38), value);
	}

	inline static int32_t get_offset_of_c_duration_39() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_duration_39)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_duration_39() const { return ___c_duration_39; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_duration_39() { return &___c_duration_39; }
	inline void set_c_duration_39(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_duration_39 = value;
		Il2CppCodeGenWriteBarrier((&___c_duration_39), value);
	}

	inline static int32_t get_offset_of_c_ENTITY_40() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_ENTITY_40)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_ENTITY_40() const { return ___c_ENTITY_40; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_ENTITY_40() { return &___c_ENTITY_40; }
	inline void set_c_ENTITY_40(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_ENTITY_40 = value;
		Il2CppCodeGenWriteBarrier((&___c_ENTITY_40), value);
	}

	inline static int32_t get_offset_of_c_ENTITIES_41() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_ENTITIES_41)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_ENTITIES_41() const { return ___c_ENTITIES_41; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_ENTITIES_41() { return &___c_ENTITIES_41; }
	inline void set_c_ENTITIES_41(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_ENTITIES_41 = value;
		Il2CppCodeGenWriteBarrier((&___c_ENTITIES_41), value);
	}

	inline static int32_t get_offset_of_c_ENUMERATION_42() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_ENUMERATION_42)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_ENUMERATION_42() const { return ___c_ENUMERATION_42; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_ENUMERATION_42() { return &___c_ENUMERATION_42; }
	inline void set_c_ENUMERATION_42(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_ENUMERATION_42 = value;
		Il2CppCodeGenWriteBarrier((&___c_ENUMERATION_42), value);
	}

	inline static int32_t get_offset_of_c_fixed_43() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_fixed_43)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_fixed_43() const { return ___c_fixed_43; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_fixed_43() { return &___c_fixed_43; }
	inline void set_c_fixed_43(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_fixed_43 = value;
		Il2CppCodeGenWriteBarrier((&___c_fixed_43), value);
	}

	inline static int32_t get_offset_of_c_float_44() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_float_44)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_float_44() const { return ___c_float_44; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_float_44() { return &___c_float_44; }
	inline void set_c_float_44(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_float_44 = value;
		Il2CppCodeGenWriteBarrier((&___c_float_44), value);
	}

	inline static int32_t get_offset_of_c_floatXdr_45() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_floatXdr_45)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_floatXdr_45() const { return ___c_floatXdr_45; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_floatXdr_45() { return &___c_floatXdr_45; }
	inline void set_c_floatXdr_45(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_floatXdr_45 = value;
		Il2CppCodeGenWriteBarrier((&___c_floatXdr_45), value);
	}

	inline static int32_t get_offset_of_c_hexBinary_46() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_hexBinary_46)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_hexBinary_46() const { return ___c_hexBinary_46; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_hexBinary_46() { return &___c_hexBinary_46; }
	inline void set_c_hexBinary_46(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_hexBinary_46 = value;
		Il2CppCodeGenWriteBarrier((&___c_hexBinary_46), value);
	}

	inline static int32_t get_offset_of_c_ID_47() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_ID_47)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_ID_47() const { return ___c_ID_47; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_ID_47() { return &___c_ID_47; }
	inline void set_c_ID_47(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_ID_47 = value;
		Il2CppCodeGenWriteBarrier((&___c_ID_47), value);
	}

	inline static int32_t get_offset_of_c_IDREF_48() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_IDREF_48)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_IDREF_48() const { return ___c_IDREF_48; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_IDREF_48() { return &___c_IDREF_48; }
	inline void set_c_IDREF_48(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_IDREF_48 = value;
		Il2CppCodeGenWriteBarrier((&___c_IDREF_48), value);
	}

	inline static int32_t get_offset_of_c_IDREFS_49() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_IDREFS_49)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_IDREFS_49() const { return ___c_IDREFS_49; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_IDREFS_49() { return &___c_IDREFS_49; }
	inline void set_c_IDREFS_49(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_IDREFS_49 = value;
		Il2CppCodeGenWriteBarrier((&___c_IDREFS_49), value);
	}

	inline static int32_t get_offset_of_c_int_50() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_int_50)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_int_50() const { return ___c_int_50; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_int_50() { return &___c_int_50; }
	inline void set_c_int_50(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_int_50 = value;
		Il2CppCodeGenWriteBarrier((&___c_int_50), value);
	}

	inline static int32_t get_offset_of_c_integer_51() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_integer_51)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_integer_51() const { return ___c_integer_51; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_integer_51() { return &___c_integer_51; }
	inline void set_c_integer_51(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_integer_51 = value;
		Il2CppCodeGenWriteBarrier((&___c_integer_51), value);
	}

	inline static int32_t get_offset_of_c_language_52() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_language_52)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_language_52() const { return ___c_language_52; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_language_52() { return &___c_language_52; }
	inline void set_c_language_52(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_language_52 = value;
		Il2CppCodeGenWriteBarrier((&___c_language_52), value);
	}

	inline static int32_t get_offset_of_c_long_53() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_long_53)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_long_53() const { return ___c_long_53; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_long_53() { return &___c_long_53; }
	inline void set_c_long_53(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_long_53 = value;
		Il2CppCodeGenWriteBarrier((&___c_long_53), value);
	}

	inline static int32_t get_offset_of_c_month_54() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_month_54)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_month_54() const { return ___c_month_54; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_month_54() { return &___c_month_54; }
	inline void set_c_month_54(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_month_54 = value;
		Il2CppCodeGenWriteBarrier((&___c_month_54), value);
	}

	inline static int32_t get_offset_of_c_monthDay_55() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_monthDay_55)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_monthDay_55() const { return ___c_monthDay_55; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_monthDay_55() { return &___c_monthDay_55; }
	inline void set_c_monthDay_55(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_monthDay_55 = value;
		Il2CppCodeGenWriteBarrier((&___c_monthDay_55), value);
	}

	inline static int32_t get_offset_of_c_Name_56() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_Name_56)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_Name_56() const { return ___c_Name_56; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_Name_56() { return &___c_Name_56; }
	inline void set_c_Name_56(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_Name_56 = value;
		Il2CppCodeGenWriteBarrier((&___c_Name_56), value);
	}

	inline static int32_t get_offset_of_c_NCName_57() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_NCName_57)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_NCName_57() const { return ___c_NCName_57; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_NCName_57() { return &___c_NCName_57; }
	inline void set_c_NCName_57(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_NCName_57 = value;
		Il2CppCodeGenWriteBarrier((&___c_NCName_57), value);
	}

	inline static int32_t get_offset_of_c_negativeInteger_58() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_negativeInteger_58)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_negativeInteger_58() const { return ___c_negativeInteger_58; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_negativeInteger_58() { return &___c_negativeInteger_58; }
	inline void set_c_negativeInteger_58(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_negativeInteger_58 = value;
		Il2CppCodeGenWriteBarrier((&___c_negativeInteger_58), value);
	}

	inline static int32_t get_offset_of_c_NMTOKEN_59() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_NMTOKEN_59)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_NMTOKEN_59() const { return ___c_NMTOKEN_59; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_NMTOKEN_59() { return &___c_NMTOKEN_59; }
	inline void set_c_NMTOKEN_59(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_NMTOKEN_59 = value;
		Il2CppCodeGenWriteBarrier((&___c_NMTOKEN_59), value);
	}

	inline static int32_t get_offset_of_c_NMTOKENS_60() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_NMTOKENS_60)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_NMTOKENS_60() const { return ___c_NMTOKENS_60; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_NMTOKENS_60() { return &___c_NMTOKENS_60; }
	inline void set_c_NMTOKENS_60(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_NMTOKENS_60 = value;
		Il2CppCodeGenWriteBarrier((&___c_NMTOKENS_60), value);
	}

	inline static int32_t get_offset_of_c_nonNegativeInteger_61() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_nonNegativeInteger_61)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_nonNegativeInteger_61() const { return ___c_nonNegativeInteger_61; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_nonNegativeInteger_61() { return &___c_nonNegativeInteger_61; }
	inline void set_c_nonNegativeInteger_61(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_nonNegativeInteger_61 = value;
		Il2CppCodeGenWriteBarrier((&___c_nonNegativeInteger_61), value);
	}

	inline static int32_t get_offset_of_c_nonPositiveInteger_62() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_nonPositiveInteger_62)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_nonPositiveInteger_62() const { return ___c_nonPositiveInteger_62; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_nonPositiveInteger_62() { return &___c_nonPositiveInteger_62; }
	inline void set_c_nonPositiveInteger_62(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_nonPositiveInteger_62 = value;
		Il2CppCodeGenWriteBarrier((&___c_nonPositiveInteger_62), value);
	}

	inline static int32_t get_offset_of_c_normalizedString_63() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_normalizedString_63)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_normalizedString_63() const { return ___c_normalizedString_63; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_normalizedString_63() { return &___c_normalizedString_63; }
	inline void set_c_normalizedString_63(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_normalizedString_63 = value;
		Il2CppCodeGenWriteBarrier((&___c_normalizedString_63), value);
	}

	inline static int32_t get_offset_of_c_NOTATION_64() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_NOTATION_64)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_NOTATION_64() const { return ___c_NOTATION_64; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_NOTATION_64() { return &___c_NOTATION_64; }
	inline void set_c_NOTATION_64(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_NOTATION_64 = value;
		Il2CppCodeGenWriteBarrier((&___c_NOTATION_64), value);
	}

	inline static int32_t get_offset_of_c_positiveInteger_65() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_positiveInteger_65)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_positiveInteger_65() const { return ___c_positiveInteger_65; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_positiveInteger_65() { return &___c_positiveInteger_65; }
	inline void set_c_positiveInteger_65(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_positiveInteger_65 = value;
		Il2CppCodeGenWriteBarrier((&___c_positiveInteger_65), value);
	}

	inline static int32_t get_offset_of_c_QName_66() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_QName_66)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_QName_66() const { return ___c_QName_66; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_QName_66() { return &___c_QName_66; }
	inline void set_c_QName_66(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_QName_66 = value;
		Il2CppCodeGenWriteBarrier((&___c_QName_66), value);
	}

	inline static int32_t get_offset_of_c_QNameXdr_67() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_QNameXdr_67)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_QNameXdr_67() const { return ___c_QNameXdr_67; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_QNameXdr_67() { return &___c_QNameXdr_67; }
	inline void set_c_QNameXdr_67(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_QNameXdr_67 = value;
		Il2CppCodeGenWriteBarrier((&___c_QNameXdr_67), value);
	}

	inline static int32_t get_offset_of_c_short_68() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_short_68)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_short_68() const { return ___c_short_68; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_short_68() { return &___c_short_68; }
	inline void set_c_short_68(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_short_68 = value;
		Il2CppCodeGenWriteBarrier((&___c_short_68), value);
	}

	inline static int32_t get_offset_of_c_string_69() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_string_69)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_string_69() const { return ___c_string_69; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_string_69() { return &___c_string_69; }
	inline void set_c_string_69(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_string_69 = value;
		Il2CppCodeGenWriteBarrier((&___c_string_69), value);
	}

	inline static int32_t get_offset_of_c_time_70() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_time_70)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_time_70() const { return ___c_time_70; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_time_70() { return &___c_time_70; }
	inline void set_c_time_70(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_time_70 = value;
		Il2CppCodeGenWriteBarrier((&___c_time_70), value);
	}

	inline static int32_t get_offset_of_c_timeNoTz_71() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_timeNoTz_71)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_timeNoTz_71() const { return ___c_timeNoTz_71; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_timeNoTz_71() { return &___c_timeNoTz_71; }
	inline void set_c_timeNoTz_71(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_timeNoTz_71 = value;
		Il2CppCodeGenWriteBarrier((&___c_timeNoTz_71), value);
	}

	inline static int32_t get_offset_of_c_timeTz_72() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_timeTz_72)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_timeTz_72() const { return ___c_timeTz_72; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_timeTz_72() { return &___c_timeTz_72; }
	inline void set_c_timeTz_72(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_timeTz_72 = value;
		Il2CppCodeGenWriteBarrier((&___c_timeTz_72), value);
	}

	inline static int32_t get_offset_of_c_token_73() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_token_73)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_token_73() const { return ___c_token_73; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_token_73() { return &___c_token_73; }
	inline void set_c_token_73(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_token_73 = value;
		Il2CppCodeGenWriteBarrier((&___c_token_73), value);
	}

	inline static int32_t get_offset_of_c_unsignedByte_74() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_unsignedByte_74)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_unsignedByte_74() const { return ___c_unsignedByte_74; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_unsignedByte_74() { return &___c_unsignedByte_74; }
	inline void set_c_unsignedByte_74(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_unsignedByte_74 = value;
		Il2CppCodeGenWriteBarrier((&___c_unsignedByte_74), value);
	}

	inline static int32_t get_offset_of_c_unsignedInt_75() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_unsignedInt_75)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_unsignedInt_75() const { return ___c_unsignedInt_75; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_unsignedInt_75() { return &___c_unsignedInt_75; }
	inline void set_c_unsignedInt_75(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_unsignedInt_75 = value;
		Il2CppCodeGenWriteBarrier((&___c_unsignedInt_75), value);
	}

	inline static int32_t get_offset_of_c_unsignedLong_76() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_unsignedLong_76)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_unsignedLong_76() const { return ___c_unsignedLong_76; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_unsignedLong_76() { return &___c_unsignedLong_76; }
	inline void set_c_unsignedLong_76(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_unsignedLong_76 = value;
		Il2CppCodeGenWriteBarrier((&___c_unsignedLong_76), value);
	}

	inline static int32_t get_offset_of_c_unsignedShort_77() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_unsignedShort_77)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_unsignedShort_77() const { return ___c_unsignedShort_77; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_unsignedShort_77() { return &___c_unsignedShort_77; }
	inline void set_c_unsignedShort_77(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_unsignedShort_77 = value;
		Il2CppCodeGenWriteBarrier((&___c_unsignedShort_77), value);
	}

	inline static int32_t get_offset_of_c_uuid_78() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_uuid_78)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_uuid_78() const { return ___c_uuid_78; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_uuid_78() { return &___c_uuid_78; }
	inline void set_c_uuid_78(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_uuid_78 = value;
		Il2CppCodeGenWriteBarrier((&___c_uuid_78), value);
	}

	inline static int32_t get_offset_of_c_year_79() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_year_79)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_year_79() const { return ___c_year_79; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_year_79() { return &___c_year_79; }
	inline void set_c_year_79(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_year_79 = value;
		Il2CppCodeGenWriteBarrier((&___c_year_79), value);
	}

	inline static int32_t get_offset_of_c_yearMonth_80() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_yearMonth_80)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_yearMonth_80() const { return ___c_yearMonth_80; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_yearMonth_80() { return &___c_yearMonth_80; }
	inline void set_c_yearMonth_80(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_yearMonth_80 = value;
		Il2CppCodeGenWriteBarrier((&___c_yearMonth_80), value);
	}

	inline static int32_t get_offset_of_c_normalizedStringV1Compat_81() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_normalizedStringV1Compat_81)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_normalizedStringV1Compat_81() const { return ___c_normalizedStringV1Compat_81; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_normalizedStringV1Compat_81() { return &___c_normalizedStringV1Compat_81; }
	inline void set_c_normalizedStringV1Compat_81(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_normalizedStringV1Compat_81 = value;
		Il2CppCodeGenWriteBarrier((&___c_normalizedStringV1Compat_81), value);
	}

	inline static int32_t get_offset_of_c_tokenV1Compat_82() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_tokenV1Compat_82)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_tokenV1Compat_82() const { return ___c_tokenV1Compat_82; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_tokenV1Compat_82() { return &___c_tokenV1Compat_82; }
	inline void set_c_tokenV1Compat_82(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_tokenV1Compat_82 = value;
		Il2CppCodeGenWriteBarrier((&___c_tokenV1Compat_82), value);
	}

	inline static int32_t get_offset_of_c_anyAtomicType_83() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_anyAtomicType_83)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_anyAtomicType_83() const { return ___c_anyAtomicType_83; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_anyAtomicType_83() { return &___c_anyAtomicType_83; }
	inline void set_c_anyAtomicType_83(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_anyAtomicType_83 = value;
		Il2CppCodeGenWriteBarrier((&___c_anyAtomicType_83), value);
	}

	inline static int32_t get_offset_of_c_dayTimeDuration_84() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_dayTimeDuration_84)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_dayTimeDuration_84() const { return ___c_dayTimeDuration_84; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_dayTimeDuration_84() { return &___c_dayTimeDuration_84; }
	inline void set_c_dayTimeDuration_84(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_dayTimeDuration_84 = value;
		Il2CppCodeGenWriteBarrier((&___c_dayTimeDuration_84), value);
	}

	inline static int32_t get_offset_of_c_untypedAtomicType_85() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_untypedAtomicType_85)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_untypedAtomicType_85() const { return ___c_untypedAtomicType_85; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_untypedAtomicType_85() { return &___c_untypedAtomicType_85; }
	inline void set_c_untypedAtomicType_85(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_untypedAtomicType_85 = value;
		Il2CppCodeGenWriteBarrier((&___c_untypedAtomicType_85), value);
	}

	inline static int32_t get_offset_of_c_yearMonthDuration_86() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_yearMonthDuration_86)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_yearMonthDuration_86() const { return ___c_yearMonthDuration_86; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_yearMonthDuration_86() { return &___c_yearMonthDuration_86; }
	inline void set_c_yearMonthDuration_86(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_yearMonthDuration_86 = value;
		Il2CppCodeGenWriteBarrier((&___c_yearMonthDuration_86), value);
	}

	inline static int32_t get_offset_of_c_tokenizedTypes_87() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_tokenizedTypes_87)); }
	inline DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36* get_c_tokenizedTypes_87() const { return ___c_tokenizedTypes_87; }
	inline DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36** get_address_of_c_tokenizedTypes_87() { return &___c_tokenizedTypes_87; }
	inline void set_c_tokenizedTypes_87(DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36* value)
	{
		___c_tokenizedTypes_87 = value;
		Il2CppCodeGenWriteBarrier((&___c_tokenizedTypes_87), value);
	}

	inline static int32_t get_offset_of_c_tokenizedTypesXsd_88() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_tokenizedTypesXsd_88)); }
	inline DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36* get_c_tokenizedTypesXsd_88() const { return ___c_tokenizedTypesXsd_88; }
	inline DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36** get_address_of_c_tokenizedTypesXsd_88() { return &___c_tokenizedTypesXsd_88; }
	inline void set_c_tokenizedTypesXsd_88(DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36* value)
	{
		___c_tokenizedTypesXsd_88 = value;
		Il2CppCodeGenWriteBarrier((&___c_tokenizedTypesXsd_88), value);
	}

	inline static int32_t get_offset_of_c_XdrTypes_89() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_XdrTypes_89)); }
	inline SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7* get_c_XdrTypes_89() const { return ___c_XdrTypes_89; }
	inline SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7** get_address_of_c_XdrTypes_89() { return &___c_XdrTypes_89; }
	inline void set_c_XdrTypes_89(SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7* value)
	{
		___c_XdrTypes_89 = value;
		Il2CppCodeGenWriteBarrier((&___c_XdrTypes_89), value);
	}

	inline static int32_t get_offset_of_c_XsdTypes_90() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_XsdTypes_90)); }
	inline SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7* get_c_XsdTypes_90() const { return ___c_XsdTypes_90; }
	inline SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7** get_address_of_c_XsdTypes_90() { return &___c_XsdTypes_90; }
	inline void set_c_XsdTypes_90(SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7* value)
	{
		___c_XsdTypes_90 = value;
		Il2CppCodeGenWriteBarrier((&___c_XsdTypes_90), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPEIMPLEMENTATION_TB5894E69998AF6871FDFC14B2A95AE5F07CBB836_H
#ifndef RESTRICTIONFACETS_TB440DD7CAE146E138972858A02C18D3D13282AB4_H
#define RESTRICTIONFACETS_TB440DD7CAE146E138972858A02C18D3D13282AB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.RestrictionFacets
struct  RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.RestrictionFacets::Length
	int32_t ___Length_0;
	// System.Int32 System.Xml.Schema.RestrictionFacets::MinLength
	int32_t ___MinLength_1;
	// System.Int32 System.Xml.Schema.RestrictionFacets::MaxLength
	int32_t ___MaxLength_2;
	// System.Collections.ArrayList System.Xml.Schema.RestrictionFacets::Patterns
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___Patterns_3;
	// System.Collections.ArrayList System.Xml.Schema.RestrictionFacets::Enumeration
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___Enumeration_4;
	// System.Xml.Schema.XmlSchemaWhiteSpace System.Xml.Schema.RestrictionFacets::WhiteSpace
	int32_t ___WhiteSpace_5;
	// System.Object System.Xml.Schema.RestrictionFacets::MaxInclusive
	RuntimeObject * ___MaxInclusive_6;
	// System.Object System.Xml.Schema.RestrictionFacets::MaxExclusive
	RuntimeObject * ___MaxExclusive_7;
	// System.Object System.Xml.Schema.RestrictionFacets::MinInclusive
	RuntimeObject * ___MinInclusive_8;
	// System.Object System.Xml.Schema.RestrictionFacets::MinExclusive
	RuntimeObject * ___MinExclusive_9;
	// System.Int32 System.Xml.Schema.RestrictionFacets::TotalDigits
	int32_t ___TotalDigits_10;
	// System.Int32 System.Xml.Schema.RestrictionFacets::FractionDigits
	int32_t ___FractionDigits_11;
	// System.Xml.Schema.RestrictionFlags System.Xml.Schema.RestrictionFacets::Flags
	int32_t ___Flags_12;
	// System.Xml.Schema.RestrictionFlags System.Xml.Schema.RestrictionFacets::FixedFlags
	int32_t ___FixedFlags_13;

public:
	inline static int32_t get_offset_of_Length_0() { return static_cast<int32_t>(offsetof(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4, ___Length_0)); }
	inline int32_t get_Length_0() const { return ___Length_0; }
	inline int32_t* get_address_of_Length_0() { return &___Length_0; }
	inline void set_Length_0(int32_t value)
	{
		___Length_0 = value;
	}

	inline static int32_t get_offset_of_MinLength_1() { return static_cast<int32_t>(offsetof(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4, ___MinLength_1)); }
	inline int32_t get_MinLength_1() const { return ___MinLength_1; }
	inline int32_t* get_address_of_MinLength_1() { return &___MinLength_1; }
	inline void set_MinLength_1(int32_t value)
	{
		___MinLength_1 = value;
	}

	inline static int32_t get_offset_of_MaxLength_2() { return static_cast<int32_t>(offsetof(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4, ___MaxLength_2)); }
	inline int32_t get_MaxLength_2() const { return ___MaxLength_2; }
	inline int32_t* get_address_of_MaxLength_2() { return &___MaxLength_2; }
	inline void set_MaxLength_2(int32_t value)
	{
		___MaxLength_2 = value;
	}

	inline static int32_t get_offset_of_Patterns_3() { return static_cast<int32_t>(offsetof(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4, ___Patterns_3)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_Patterns_3() const { return ___Patterns_3; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_Patterns_3() { return &___Patterns_3; }
	inline void set_Patterns_3(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___Patterns_3 = value;
		Il2CppCodeGenWriteBarrier((&___Patterns_3), value);
	}

	inline static int32_t get_offset_of_Enumeration_4() { return static_cast<int32_t>(offsetof(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4, ___Enumeration_4)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_Enumeration_4() const { return ___Enumeration_4; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_Enumeration_4() { return &___Enumeration_4; }
	inline void set_Enumeration_4(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___Enumeration_4 = value;
		Il2CppCodeGenWriteBarrier((&___Enumeration_4), value);
	}

	inline static int32_t get_offset_of_WhiteSpace_5() { return static_cast<int32_t>(offsetof(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4, ___WhiteSpace_5)); }
	inline int32_t get_WhiteSpace_5() const { return ___WhiteSpace_5; }
	inline int32_t* get_address_of_WhiteSpace_5() { return &___WhiteSpace_5; }
	inline void set_WhiteSpace_5(int32_t value)
	{
		___WhiteSpace_5 = value;
	}

	inline static int32_t get_offset_of_MaxInclusive_6() { return static_cast<int32_t>(offsetof(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4, ___MaxInclusive_6)); }
	inline RuntimeObject * get_MaxInclusive_6() const { return ___MaxInclusive_6; }
	inline RuntimeObject ** get_address_of_MaxInclusive_6() { return &___MaxInclusive_6; }
	inline void set_MaxInclusive_6(RuntimeObject * value)
	{
		___MaxInclusive_6 = value;
		Il2CppCodeGenWriteBarrier((&___MaxInclusive_6), value);
	}

	inline static int32_t get_offset_of_MaxExclusive_7() { return static_cast<int32_t>(offsetof(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4, ___MaxExclusive_7)); }
	inline RuntimeObject * get_MaxExclusive_7() const { return ___MaxExclusive_7; }
	inline RuntimeObject ** get_address_of_MaxExclusive_7() { return &___MaxExclusive_7; }
	inline void set_MaxExclusive_7(RuntimeObject * value)
	{
		___MaxExclusive_7 = value;
		Il2CppCodeGenWriteBarrier((&___MaxExclusive_7), value);
	}

	inline static int32_t get_offset_of_MinInclusive_8() { return static_cast<int32_t>(offsetof(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4, ___MinInclusive_8)); }
	inline RuntimeObject * get_MinInclusive_8() const { return ___MinInclusive_8; }
	inline RuntimeObject ** get_address_of_MinInclusive_8() { return &___MinInclusive_8; }
	inline void set_MinInclusive_8(RuntimeObject * value)
	{
		___MinInclusive_8 = value;
		Il2CppCodeGenWriteBarrier((&___MinInclusive_8), value);
	}

	inline static int32_t get_offset_of_MinExclusive_9() { return static_cast<int32_t>(offsetof(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4, ___MinExclusive_9)); }
	inline RuntimeObject * get_MinExclusive_9() const { return ___MinExclusive_9; }
	inline RuntimeObject ** get_address_of_MinExclusive_9() { return &___MinExclusive_9; }
	inline void set_MinExclusive_9(RuntimeObject * value)
	{
		___MinExclusive_9 = value;
		Il2CppCodeGenWriteBarrier((&___MinExclusive_9), value);
	}

	inline static int32_t get_offset_of_TotalDigits_10() { return static_cast<int32_t>(offsetof(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4, ___TotalDigits_10)); }
	inline int32_t get_TotalDigits_10() const { return ___TotalDigits_10; }
	inline int32_t* get_address_of_TotalDigits_10() { return &___TotalDigits_10; }
	inline void set_TotalDigits_10(int32_t value)
	{
		___TotalDigits_10 = value;
	}

	inline static int32_t get_offset_of_FractionDigits_11() { return static_cast<int32_t>(offsetof(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4, ___FractionDigits_11)); }
	inline int32_t get_FractionDigits_11() const { return ___FractionDigits_11; }
	inline int32_t* get_address_of_FractionDigits_11() { return &___FractionDigits_11; }
	inline void set_FractionDigits_11(int32_t value)
	{
		___FractionDigits_11 = value;
	}

	inline static int32_t get_offset_of_Flags_12() { return static_cast<int32_t>(offsetof(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4, ___Flags_12)); }
	inline int32_t get_Flags_12() const { return ___Flags_12; }
	inline int32_t* get_address_of_Flags_12() { return &___Flags_12; }
	inline void set_Flags_12(int32_t value)
	{
		___Flags_12 = value;
	}

	inline static int32_t get_offset_of_FixedFlags_13() { return static_cast<int32_t>(offsetof(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4, ___FixedFlags_13)); }
	inline int32_t get_FixedFlags_13() const { return ___FixedFlags_13; }
	inline int32_t* get_address_of_FixedFlags_13() { return &___FixedFlags_13; }
	inline void set_FixedFlags_13(int32_t value)
	{
		___FixedFlags_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTRICTIONFACETS_TB440DD7CAE146E138972858A02C18D3D13282AB4_H
#ifndef XMLMAPPING_TB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726_H
#define XMLMAPPING_TB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlMapping
struct  XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726  : public RuntimeObject
{
public:
	// System.Xml.Serialization.ObjectMap System.Xml.Serialization.XmlMapping::map
	ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677 * ___map_0;
	// System.Collections.ArrayList System.Xml.Serialization.XmlMapping::relatedMaps
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___relatedMaps_1;
	// System.Xml.Serialization.SerializationFormat System.Xml.Serialization.XmlMapping::format
	int32_t ___format_2;
	// System.Xml.Serialization.SerializationSource System.Xml.Serialization.XmlMapping::source
	SerializationSource_tAA44E4510C7A00BEF2802B7E4E1CD299F1C3F1CC * ___source_3;
	// System.String System.Xml.Serialization.XmlMapping::_elementName
	String_t* ____elementName_4;
	// System.String System.Xml.Serialization.XmlMapping::_namespace
	String_t* ____namespace_5;
	// System.String System.Xml.Serialization.XmlMapping::key
	String_t* ___key_6;

public:
	inline static int32_t get_offset_of_map_0() { return static_cast<int32_t>(offsetof(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726, ___map_0)); }
	inline ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677 * get_map_0() const { return ___map_0; }
	inline ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677 ** get_address_of_map_0() { return &___map_0; }
	inline void set_map_0(ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677 * value)
	{
		___map_0 = value;
		Il2CppCodeGenWriteBarrier((&___map_0), value);
	}

	inline static int32_t get_offset_of_relatedMaps_1() { return static_cast<int32_t>(offsetof(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726, ___relatedMaps_1)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_relatedMaps_1() const { return ___relatedMaps_1; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_relatedMaps_1() { return &___relatedMaps_1; }
	inline void set_relatedMaps_1(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___relatedMaps_1 = value;
		Il2CppCodeGenWriteBarrier((&___relatedMaps_1), value);
	}

	inline static int32_t get_offset_of_format_2() { return static_cast<int32_t>(offsetof(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726, ___format_2)); }
	inline int32_t get_format_2() const { return ___format_2; }
	inline int32_t* get_address_of_format_2() { return &___format_2; }
	inline void set_format_2(int32_t value)
	{
		___format_2 = value;
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726, ___source_3)); }
	inline SerializationSource_tAA44E4510C7A00BEF2802B7E4E1CD299F1C3F1CC * get_source_3() const { return ___source_3; }
	inline SerializationSource_tAA44E4510C7A00BEF2802B7E4E1CD299F1C3F1CC ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(SerializationSource_tAA44E4510C7A00BEF2802B7E4E1CD299F1C3F1CC * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((&___source_3), value);
	}

	inline static int32_t get_offset_of__elementName_4() { return static_cast<int32_t>(offsetof(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726, ____elementName_4)); }
	inline String_t* get__elementName_4() const { return ____elementName_4; }
	inline String_t** get_address_of__elementName_4() { return &____elementName_4; }
	inline void set__elementName_4(String_t* value)
	{
		____elementName_4 = value;
		Il2CppCodeGenWriteBarrier((&____elementName_4), value);
	}

	inline static int32_t get_offset_of__namespace_5() { return static_cast<int32_t>(offsetof(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726, ____namespace_5)); }
	inline String_t* get__namespace_5() const { return ____namespace_5; }
	inline String_t** get_address_of__namespace_5() { return &____namespace_5; }
	inline void set__namespace_5(String_t* value)
	{
		____namespace_5 = value;
		Il2CppCodeGenWriteBarrier((&____namespace_5), value);
	}

	inline static int32_t get_offset_of_key_6() { return static_cast<int32_t>(offsetof(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726, ___key_6)); }
	inline String_t* get_key_6() const { return ___key_6; }
	inline String_t** get_address_of_key_6() { return &___key_6; }
	inline void set_key_6(String_t* value)
	{
		___key_6 = value;
		Il2CppCodeGenWriteBarrier((&___key_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLMAPPING_TB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726_H
#ifndef XMLSERIALIZATIONREADERINTERPRETER_T8D5678E515DA59060E81B56B5052B97EEDC3B44C_H
#define XMLSERIALIZATIONREADERINTERPRETER_T8D5678E515DA59060E81B56B5052B97EEDC3B44C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationReaderInterpreter
struct  XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C  : public XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88
{
public:
	// System.Xml.Serialization.XmlMapping System.Xml.Serialization.XmlSerializationReaderInterpreter::_typeMap
	XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 * ____typeMap_25;
	// System.Xml.Serialization.SerializationFormat System.Xml.Serialization.XmlSerializationReaderInterpreter::_format
	int32_t ____format_26;

public:
	inline static int32_t get_offset_of__typeMap_25() { return static_cast<int32_t>(offsetof(XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C, ____typeMap_25)); }
	inline XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 * get__typeMap_25() const { return ____typeMap_25; }
	inline XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 ** get_address_of__typeMap_25() { return &____typeMap_25; }
	inline void set__typeMap_25(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 * value)
	{
		____typeMap_25 = value;
		Il2CppCodeGenWriteBarrier((&____typeMap_25), value);
	}

	inline static int32_t get_offset_of__format_26() { return static_cast<int32_t>(offsetof(XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C, ____format_26)); }
	inline int32_t get__format_26() const { return ____format_26; }
	inline int32_t* get_address_of__format_26() { return &____format_26; }
	inline void set__format_26(int32_t value)
	{
		____format_26 = value;
	}
};

struct XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C_StaticFields
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Serialization.XmlSerializationReaderInterpreter::AnyType
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___AnyType_27;
	// System.Object[] System.Xml.Serialization.XmlSerializationReaderInterpreter::empty_array
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___empty_array_28;

public:
	inline static int32_t get_offset_of_AnyType_27() { return static_cast<int32_t>(offsetof(XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C_StaticFields, ___AnyType_27)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_AnyType_27() const { return ___AnyType_27; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_AnyType_27() { return &___AnyType_27; }
	inline void set_AnyType_27(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___AnyType_27 = value;
		Il2CppCodeGenWriteBarrier((&___AnyType_27), value);
	}

	inline static int32_t get_offset_of_empty_array_28() { return static_cast<int32_t>(offsetof(XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C_StaticFields, ___empty_array_28)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_empty_array_28() const { return ___empty_array_28; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_empty_array_28() { return &___empty_array_28; }
	inline void set_empty_array_28(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___empty_array_28 = value;
		Il2CppCodeGenWriteBarrier((&___empty_array_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZATIONREADERINTERPRETER_T8D5678E515DA59060E81B56B5052B97EEDC3B44C_H
#ifndef XMLSERIALIZATIONWRITERINTERPRETER_T0820AC26DA279FAB2E359E7A655A6582506D3221_H
#define XMLSERIALIZATIONWRITERINTERPRETER_T0820AC26DA279FAB2E359E7A655A6582506D3221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationWriterInterpreter
struct  XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221  : public XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6
{
public:
	// System.Xml.Serialization.XmlMapping System.Xml.Serialization.XmlSerializationWriterInterpreter::_typeMap
	XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 * ____typeMap_8;
	// System.Xml.Serialization.SerializationFormat System.Xml.Serialization.XmlSerializationWriterInterpreter::_format
	int32_t ____format_9;

public:
	inline static int32_t get_offset_of__typeMap_8() { return static_cast<int32_t>(offsetof(XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221, ____typeMap_8)); }
	inline XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 * get__typeMap_8() const { return ____typeMap_8; }
	inline XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 ** get_address_of__typeMap_8() { return &____typeMap_8; }
	inline void set__typeMap_8(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 * value)
	{
		____typeMap_8 = value;
		Il2CppCodeGenWriteBarrier((&____typeMap_8), value);
	}

	inline static int32_t get_offset_of__format_9() { return static_cast<int32_t>(offsetof(XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221, ____format_9)); }
	inline int32_t get__format_9() const { return ____format_9; }
	inline int32_t* get_address_of__format_9() { return &____format_9; }
	inline void set__format_9(int32_t value)
	{
		____format_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZATIONWRITERINTERPRETER_T0820AC26DA279FAB2E359E7A655A6582506D3221_H
#ifndef XMLTYPEMAPELEMENTINFO_T78BD14FFCF51D7812853407BC715516F09FA083F_H
#define XMLTYPEMAPELEMENTINFO_T78BD14FFCF51D7812853407BC715516F09FA083F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapElementInfo
struct  XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F  : public RuntimeObject
{
public:
	// System.String System.Xml.Serialization.XmlTypeMapElementInfo::_elementName
	String_t* ____elementName_0;
	// System.String System.Xml.Serialization.XmlTypeMapElementInfo::_namespace
	String_t* ____namespace_1;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Serialization.XmlTypeMapElementInfo::_form
	int32_t ____form_2;
	// System.Xml.Serialization.XmlTypeMapMember System.Xml.Serialization.XmlTypeMapElementInfo::_member
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * ____member_3;
	// System.Object System.Xml.Serialization.XmlTypeMapElementInfo::_choiceValue
	RuntimeObject * ____choiceValue_4;
	// System.Boolean System.Xml.Serialization.XmlTypeMapElementInfo::_isNullable
	bool ____isNullable_5;
	// System.Int32 System.Xml.Serialization.XmlTypeMapElementInfo::_nestingLevel
	int32_t ____nestingLevel_6;
	// System.Xml.Serialization.XmlTypeMapping System.Xml.Serialization.XmlTypeMapElementInfo::_mappedType
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * ____mappedType_7;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.XmlTypeMapElementInfo::_type
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * ____type_8;
	// System.Boolean System.Xml.Serialization.XmlTypeMapElementInfo::_wrappedElement
	bool ____wrappedElement_9;
	// System.Int32 System.Xml.Serialization.XmlTypeMapElementInfo::_explicitOrder
	int32_t ____explicitOrder_10;

public:
	inline static int32_t get_offset_of__elementName_0() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____elementName_0)); }
	inline String_t* get__elementName_0() const { return ____elementName_0; }
	inline String_t** get_address_of__elementName_0() { return &____elementName_0; }
	inline void set__elementName_0(String_t* value)
	{
		____elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&____elementName_0), value);
	}

	inline static int32_t get_offset_of__namespace_1() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____namespace_1)); }
	inline String_t* get__namespace_1() const { return ____namespace_1; }
	inline String_t** get_address_of__namespace_1() { return &____namespace_1; }
	inline void set__namespace_1(String_t* value)
	{
		____namespace_1 = value;
		Il2CppCodeGenWriteBarrier((&____namespace_1), value);
	}

	inline static int32_t get_offset_of__form_2() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____form_2)); }
	inline int32_t get__form_2() const { return ____form_2; }
	inline int32_t* get_address_of__form_2() { return &____form_2; }
	inline void set__form_2(int32_t value)
	{
		____form_2 = value;
	}

	inline static int32_t get_offset_of__member_3() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____member_3)); }
	inline XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * get__member_3() const { return ____member_3; }
	inline XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D ** get_address_of__member_3() { return &____member_3; }
	inline void set__member_3(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * value)
	{
		____member_3 = value;
		Il2CppCodeGenWriteBarrier((&____member_3), value);
	}

	inline static int32_t get_offset_of__choiceValue_4() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____choiceValue_4)); }
	inline RuntimeObject * get__choiceValue_4() const { return ____choiceValue_4; }
	inline RuntimeObject ** get_address_of__choiceValue_4() { return &____choiceValue_4; }
	inline void set__choiceValue_4(RuntimeObject * value)
	{
		____choiceValue_4 = value;
		Il2CppCodeGenWriteBarrier((&____choiceValue_4), value);
	}

	inline static int32_t get_offset_of__isNullable_5() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____isNullable_5)); }
	inline bool get__isNullable_5() const { return ____isNullable_5; }
	inline bool* get_address_of__isNullable_5() { return &____isNullable_5; }
	inline void set__isNullable_5(bool value)
	{
		____isNullable_5 = value;
	}

	inline static int32_t get_offset_of__nestingLevel_6() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____nestingLevel_6)); }
	inline int32_t get__nestingLevel_6() const { return ____nestingLevel_6; }
	inline int32_t* get_address_of__nestingLevel_6() { return &____nestingLevel_6; }
	inline void set__nestingLevel_6(int32_t value)
	{
		____nestingLevel_6 = value;
	}

	inline static int32_t get_offset_of__mappedType_7() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____mappedType_7)); }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * get__mappedType_7() const { return ____mappedType_7; }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA ** get_address_of__mappedType_7() { return &____mappedType_7; }
	inline void set__mappedType_7(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * value)
	{
		____mappedType_7 = value;
		Il2CppCodeGenWriteBarrier((&____mappedType_7), value);
	}

	inline static int32_t get_offset_of__type_8() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____type_8)); }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * get__type_8() const { return ____type_8; }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 ** get_address_of__type_8() { return &____type_8; }
	inline void set__type_8(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * value)
	{
		____type_8 = value;
		Il2CppCodeGenWriteBarrier((&____type_8), value);
	}

	inline static int32_t get_offset_of__wrappedElement_9() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____wrappedElement_9)); }
	inline bool get__wrappedElement_9() const { return ____wrappedElement_9; }
	inline bool* get_address_of__wrappedElement_9() { return &____wrappedElement_9; }
	inline void set__wrappedElement_9(bool value)
	{
		____wrappedElement_9 = value;
	}

	inline static int32_t get_offset_of__explicitOrder_10() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____explicitOrder_10)); }
	inline int32_t get__explicitOrder_10() const { return ____explicitOrder_10; }
	inline int32_t* get_address_of__explicitOrder_10() { return &____explicitOrder_10; }
	inline void set__explicitOrder_10(int32_t value)
	{
		____explicitOrder_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPELEMENTINFO_T78BD14FFCF51D7812853407BC715516F09FA083F_H
#ifndef XMLTYPEMAPMEMBERANYELEMENT_TEC6C4FFA530F292BC4C09145DA5937285A664472_H
#define XMLTYPEMAPMEMBERANYELEMENT_TEC6C4FFA530F292BC4C09145DA5937285A664472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberAnyElement
struct  XmlTypeMapMemberAnyElement_tEC6C4FFA530F292BC4C09145DA5937285A664472  : public XmlTypeMapMemberExpandable_tEFC74196D6503AB38EEA1AD371BA55EABC7C30ED
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBERANYELEMENT_TEC6C4FFA530F292BC4C09145DA5937285A664472_H
#ifndef XMLTYPEMAPMEMBERATTRIBUTE_TD9692D2B3223A558EE5B6FADCED2F92183F16C6C_H
#define XMLTYPEMAPMEMBERATTRIBUTE_TD9692D2B3223A558EE5B6FADCED2F92183F16C6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberAttribute
struct  XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C  : public XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D
{
public:
	// System.String System.Xml.Serialization.XmlTypeMapMemberAttribute::_attributeName
	String_t* ____attributeName_10;
	// System.String System.Xml.Serialization.XmlTypeMapMemberAttribute::_namespace
	String_t* ____namespace_11;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Serialization.XmlTypeMapMemberAttribute::_form
	int32_t ____form_12;
	// System.Xml.Serialization.XmlTypeMapping System.Xml.Serialization.XmlTypeMapMemberAttribute::_mappedType
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * ____mappedType_13;

public:
	inline static int32_t get_offset_of__attributeName_10() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C, ____attributeName_10)); }
	inline String_t* get__attributeName_10() const { return ____attributeName_10; }
	inline String_t** get_address_of__attributeName_10() { return &____attributeName_10; }
	inline void set__attributeName_10(String_t* value)
	{
		____attributeName_10 = value;
		Il2CppCodeGenWriteBarrier((&____attributeName_10), value);
	}

	inline static int32_t get_offset_of__namespace_11() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C, ____namespace_11)); }
	inline String_t* get__namespace_11() const { return ____namespace_11; }
	inline String_t** get_address_of__namespace_11() { return &____namespace_11; }
	inline void set__namespace_11(String_t* value)
	{
		____namespace_11 = value;
		Il2CppCodeGenWriteBarrier((&____namespace_11), value);
	}

	inline static int32_t get_offset_of__form_12() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C, ____form_12)); }
	inline int32_t get__form_12() const { return ____form_12; }
	inline int32_t* get_address_of__form_12() { return &____form_12; }
	inline void set__form_12(int32_t value)
	{
		____form_12 = value;
	}

	inline static int32_t get_offset_of__mappedType_13() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C, ____mappedType_13)); }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * get__mappedType_13() const { return ____mappedType_13; }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA ** get_address_of__mappedType_13() { return &____mappedType_13; }
	inline void set__mappedType_13(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * value)
	{
		____mappedType_13 = value;
		Il2CppCodeGenWriteBarrier((&____mappedType_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBERATTRIBUTE_TD9692D2B3223A558EE5B6FADCED2F92183F16C6C_H
#ifndef XMLTYPEMAPMEMBERFLATLIST_TEA9F25C615A27D3B9761879A55C2BF74E394D31F_H
#define XMLTYPEMAPMEMBERFLATLIST_TEA9F25C615A27D3B9761879A55C2BF74E394D31F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberFlatList
struct  XmlTypeMapMemberFlatList_tEA9F25C615A27D3B9761879A55C2BF74E394D31F  : public XmlTypeMapMemberExpandable_tEFC74196D6503AB38EEA1AD371BA55EABC7C30ED
{
public:
	// System.Xml.Serialization.ListMap System.Xml.Serialization.XmlTypeMapMemberFlatList::_listMap
	ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742 * ____listMap_15;

public:
	inline static int32_t get_offset_of__listMap_15() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberFlatList_tEA9F25C615A27D3B9761879A55C2BF74E394D31F, ____listMap_15)); }
	inline ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742 * get__listMap_15() const { return ____listMap_15; }
	inline ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742 ** get_address_of__listMap_15() { return &____listMap_15; }
	inline void set__listMap_15(ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742 * value)
	{
		____listMap_15 = value;
		Il2CppCodeGenWriteBarrier((&____listMap_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBERFLATLIST_TEA9F25C615A27D3B9761879A55C2BF74E394D31F_H
#ifndef ALLELEMENTSCONTENTVALIDATOR_T6F87D7B72856382DAF3FA1EA055455AC525AA355_H
#define ALLELEMENTSCONTENTVALIDATOR_T6F87D7B72856382DAF3FA1EA055455AC525AA355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.AllElementsContentValidator
struct  AllElementsContentValidator_t6F87D7B72856382DAF3FA1EA055455AC525AA355  : public ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA
{
public:
	// System.Collections.Hashtable System.Xml.Schema.AllElementsContentValidator::elements
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___elements_7;
	// System.Object[] System.Xml.Schema.AllElementsContentValidator::particles
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___particles_8;
	// System.Xml.Schema.BitSet System.Xml.Schema.AllElementsContentValidator::isRequired
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___isRequired_9;
	// System.Int32 System.Xml.Schema.AllElementsContentValidator::countRequired
	int32_t ___countRequired_10;

public:
	inline static int32_t get_offset_of_elements_7() { return static_cast<int32_t>(offsetof(AllElementsContentValidator_t6F87D7B72856382DAF3FA1EA055455AC525AA355, ___elements_7)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_elements_7() const { return ___elements_7; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_elements_7() { return &___elements_7; }
	inline void set_elements_7(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___elements_7 = value;
		Il2CppCodeGenWriteBarrier((&___elements_7), value);
	}

	inline static int32_t get_offset_of_particles_8() { return static_cast<int32_t>(offsetof(AllElementsContentValidator_t6F87D7B72856382DAF3FA1EA055455AC525AA355, ___particles_8)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_particles_8() const { return ___particles_8; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_particles_8() { return &___particles_8; }
	inline void set_particles_8(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___particles_8 = value;
		Il2CppCodeGenWriteBarrier((&___particles_8), value);
	}

	inline static int32_t get_offset_of_isRequired_9() { return static_cast<int32_t>(offsetof(AllElementsContentValidator_t6F87D7B72856382DAF3FA1EA055455AC525AA355, ___isRequired_9)); }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C * get_isRequired_9() const { return ___isRequired_9; }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C ** get_address_of_isRequired_9() { return &___isRequired_9; }
	inline void set_isRequired_9(BitSet_t0E4C53EC600670A4B74C5671553596978880138C * value)
	{
		___isRequired_9 = value;
		Il2CppCodeGenWriteBarrier((&___isRequired_9), value);
	}

	inline static int32_t get_offset_of_countRequired_10() { return static_cast<int32_t>(offsetof(AllElementsContentValidator_t6F87D7B72856382DAF3FA1EA055455AC525AA355, ___countRequired_10)); }
	inline int32_t get_countRequired_10() const { return ___countRequired_10; }
	inline int32_t* get_address_of_countRequired_10() { return &___countRequired_10; }
	inline void set_countRequired_10(int32_t value)
	{
		___countRequired_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLELEMENTSCONTENTVALIDATOR_T6F87D7B72856382DAF3FA1EA055455AC525AA355_H
#ifndef DATATYPE_ANYSIMPLETYPE_TA6075F2CC91D824A74FCEEBA33F595BA46A8C232_H
#define DATATYPE_ANYSIMPLETYPE_TA6075F2CC91D824A74FCEEBA33F595BA46A8C232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_anySimpleType
struct  Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232  : public DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836
{
public:

public:
};

struct Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_anySimpleType::atomicValueType
	Type_t * ___atomicValueType_91;
	// System.Type System.Xml.Schema.Datatype_anySimpleType::listValueType
	Type_t * ___listValueType_92;

public:
	inline static int32_t get_offset_of_atomicValueType_91() { return static_cast<int32_t>(offsetof(Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232_StaticFields, ___atomicValueType_91)); }
	inline Type_t * get_atomicValueType_91() const { return ___atomicValueType_91; }
	inline Type_t ** get_address_of_atomicValueType_91() { return &___atomicValueType_91; }
	inline void set_atomicValueType_91(Type_t * value)
	{
		___atomicValueType_91 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_91), value);
	}

	inline static int32_t get_offset_of_listValueType_92() { return static_cast<int32_t>(offsetof(Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232_StaticFields, ___listValueType_92)); }
	inline Type_t * get_listValueType_92() const { return ___listValueType_92; }
	inline Type_t ** get_address_of_listValueType_92() { return &___listValueType_92; }
	inline void set_listValueType_92(Type_t * value)
	{
		___listValueType_92 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_92), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_ANYSIMPLETYPE_TA6075F2CC91D824A74FCEEBA33F595BA46A8C232_H
#ifndef DFACONTENTVALIDATOR_T1FBCE29B0AD46CA19740B8BB2815C95895BF9394_H
#define DFACONTENTVALIDATOR_T1FBCE29B0AD46CA19740B8BB2815C95895BF9394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DfaContentValidator
struct  DfaContentValidator_t1FBCE29B0AD46CA19740B8BB2815C95895BF9394  : public ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA
{
public:
	// System.Int32[][] System.Xml.Schema.DfaContentValidator::transitionTable
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___transitionTable_7;
	// System.Xml.Schema.SymbolsDictionary System.Xml.Schema.DfaContentValidator::symbols
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 * ___symbols_8;

public:
	inline static int32_t get_offset_of_transitionTable_7() { return static_cast<int32_t>(offsetof(DfaContentValidator_t1FBCE29B0AD46CA19740B8BB2815C95895BF9394, ___transitionTable_7)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_transitionTable_7() const { return ___transitionTable_7; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_transitionTable_7() { return &___transitionTable_7; }
	inline void set_transitionTable_7(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___transitionTable_7 = value;
		Il2CppCodeGenWriteBarrier((&___transitionTable_7), value);
	}

	inline static int32_t get_offset_of_symbols_8() { return static_cast<int32_t>(offsetof(DfaContentValidator_t1FBCE29B0AD46CA19740B8BB2815C95895BF9394, ___symbols_8)); }
	inline SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 * get_symbols_8() const { return ___symbols_8; }
	inline SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 ** get_address_of_symbols_8() { return &___symbols_8; }
	inline void set_symbols_8(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 * value)
	{
		___symbols_8 = value;
		Il2CppCodeGenWriteBarrier((&___symbols_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DFACONTENTVALIDATOR_T1FBCE29B0AD46CA19740B8BB2815C95895BF9394_H
#ifndef DOUBLELINKAXIS_T46C722C5523ACDE575BB767D7794D6CE1C1C5955_H
#define DOUBLELINKAXIS_T46C722C5523ACDE575BB767D7794D6CE1C1C5955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DoubleLinkAxis
struct  DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955  : public Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70
{
public:
	// MS.Internal.Xml.XPath.Axis System.Xml.Schema.DoubleLinkAxis::next
	Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70 * ___next_7;

public:
	inline static int32_t get_offset_of_next_7() { return static_cast<int32_t>(offsetof(DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955, ___next_7)); }
	inline Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70 * get_next_7() const { return ___next_7; }
	inline Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70 ** get_address_of_next_7() { return &___next_7; }
	inline void set_next_7(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70 * value)
	{
		___next_7 = value;
		Il2CppCodeGenWriteBarrier((&___next_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLELINKAXIS_T46C722C5523ACDE575BB767D7794D6CE1C1C5955_H
#ifndef NFACONTENTVALIDATOR_TC513066391441B47E0D46A11F7D48CF004F930DA_H
#define NFACONTENTVALIDATOR_TC513066391441B47E0D46A11F7D48CF004F930DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.NfaContentValidator
struct  NfaContentValidator_tC513066391441B47E0D46A11F7D48CF004F930DA  : public ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA
{
public:
	// System.Xml.Schema.BitSet System.Xml.Schema.NfaContentValidator::firstpos
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___firstpos_7;
	// System.Xml.Schema.BitSet[] System.Xml.Schema.NfaContentValidator::followpos
	BitSetU5BU5D_tC61B9A480CE3460277DE0ABFA4551635BB337D69* ___followpos_8;
	// System.Xml.Schema.SymbolsDictionary System.Xml.Schema.NfaContentValidator::symbols
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 * ___symbols_9;
	// System.Xml.Schema.Positions System.Xml.Schema.NfaContentValidator::positions
	Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D * ___positions_10;
	// System.Int32 System.Xml.Schema.NfaContentValidator::endMarkerPos
	int32_t ___endMarkerPos_11;

public:
	inline static int32_t get_offset_of_firstpos_7() { return static_cast<int32_t>(offsetof(NfaContentValidator_tC513066391441B47E0D46A11F7D48CF004F930DA, ___firstpos_7)); }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C * get_firstpos_7() const { return ___firstpos_7; }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C ** get_address_of_firstpos_7() { return &___firstpos_7; }
	inline void set_firstpos_7(BitSet_t0E4C53EC600670A4B74C5671553596978880138C * value)
	{
		___firstpos_7 = value;
		Il2CppCodeGenWriteBarrier((&___firstpos_7), value);
	}

	inline static int32_t get_offset_of_followpos_8() { return static_cast<int32_t>(offsetof(NfaContentValidator_tC513066391441B47E0D46A11F7D48CF004F930DA, ___followpos_8)); }
	inline BitSetU5BU5D_tC61B9A480CE3460277DE0ABFA4551635BB337D69* get_followpos_8() const { return ___followpos_8; }
	inline BitSetU5BU5D_tC61B9A480CE3460277DE0ABFA4551635BB337D69** get_address_of_followpos_8() { return &___followpos_8; }
	inline void set_followpos_8(BitSetU5BU5D_tC61B9A480CE3460277DE0ABFA4551635BB337D69* value)
	{
		___followpos_8 = value;
		Il2CppCodeGenWriteBarrier((&___followpos_8), value);
	}

	inline static int32_t get_offset_of_symbols_9() { return static_cast<int32_t>(offsetof(NfaContentValidator_tC513066391441B47E0D46A11F7D48CF004F930DA, ___symbols_9)); }
	inline SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 * get_symbols_9() const { return ___symbols_9; }
	inline SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 ** get_address_of_symbols_9() { return &___symbols_9; }
	inline void set_symbols_9(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 * value)
	{
		___symbols_9 = value;
		Il2CppCodeGenWriteBarrier((&___symbols_9), value);
	}

	inline static int32_t get_offset_of_positions_10() { return static_cast<int32_t>(offsetof(NfaContentValidator_tC513066391441B47E0D46A11F7D48CF004F930DA, ___positions_10)); }
	inline Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D * get_positions_10() const { return ___positions_10; }
	inline Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D ** get_address_of_positions_10() { return &___positions_10; }
	inline void set_positions_10(Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D * value)
	{
		___positions_10 = value;
		Il2CppCodeGenWriteBarrier((&___positions_10), value);
	}

	inline static int32_t get_offset_of_endMarkerPos_11() { return static_cast<int32_t>(offsetof(NfaContentValidator_tC513066391441B47E0D46A11F7D48CF004F930DA, ___endMarkerPos_11)); }
	inline int32_t get_endMarkerPos_11() const { return ___endMarkerPos_11; }
	inline int32_t* get_address_of_endMarkerPos_11() { return &___endMarkerPos_11; }
	inline void set_endMarkerPos_11(int32_t value)
	{
		___endMarkerPos_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NFACONTENTVALIDATOR_TC513066391441B47E0D46A11F7D48CF004F930DA_H
#ifndef PARTICLECONTENTVALIDATOR_TFF273453F5523E636B8BE9C446AEE1F4090B8D1D_H
#define PARTICLECONTENTVALIDATOR_TFF273453F5523E636B8BE9C446AEE1F4090B8D1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ParticleContentValidator
struct  ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D  : public ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA
{
public:
	// System.Xml.Schema.SymbolsDictionary System.Xml.Schema.ParticleContentValidator::symbols
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 * ___symbols_7;
	// System.Xml.Schema.Positions System.Xml.Schema.ParticleContentValidator::positions
	Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D * ___positions_8;
	// System.Collections.Stack System.Xml.Schema.ParticleContentValidator::stack
	Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 * ___stack_9;
	// System.Xml.Schema.SyntaxTreeNode System.Xml.Schema.ParticleContentValidator::contentNode
	SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C * ___contentNode_10;
	// System.Boolean System.Xml.Schema.ParticleContentValidator::isPartial
	bool ___isPartial_11;
	// System.Int32 System.Xml.Schema.ParticleContentValidator::minMaxNodesCount
	int32_t ___minMaxNodesCount_12;
	// System.Boolean System.Xml.Schema.ParticleContentValidator::enableUpaCheck
	bool ___enableUpaCheck_13;

public:
	inline static int32_t get_offset_of_symbols_7() { return static_cast<int32_t>(offsetof(ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D, ___symbols_7)); }
	inline SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 * get_symbols_7() const { return ___symbols_7; }
	inline SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 ** get_address_of_symbols_7() { return &___symbols_7; }
	inline void set_symbols_7(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 * value)
	{
		___symbols_7 = value;
		Il2CppCodeGenWriteBarrier((&___symbols_7), value);
	}

	inline static int32_t get_offset_of_positions_8() { return static_cast<int32_t>(offsetof(ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D, ___positions_8)); }
	inline Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D * get_positions_8() const { return ___positions_8; }
	inline Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D ** get_address_of_positions_8() { return &___positions_8; }
	inline void set_positions_8(Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D * value)
	{
		___positions_8 = value;
		Il2CppCodeGenWriteBarrier((&___positions_8), value);
	}

	inline static int32_t get_offset_of_stack_9() { return static_cast<int32_t>(offsetof(ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D, ___stack_9)); }
	inline Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 * get_stack_9() const { return ___stack_9; }
	inline Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 ** get_address_of_stack_9() { return &___stack_9; }
	inline void set_stack_9(Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 * value)
	{
		___stack_9 = value;
		Il2CppCodeGenWriteBarrier((&___stack_9), value);
	}

	inline static int32_t get_offset_of_contentNode_10() { return static_cast<int32_t>(offsetof(ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D, ___contentNode_10)); }
	inline SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C * get_contentNode_10() const { return ___contentNode_10; }
	inline SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C ** get_address_of_contentNode_10() { return &___contentNode_10; }
	inline void set_contentNode_10(SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C * value)
	{
		___contentNode_10 = value;
		Il2CppCodeGenWriteBarrier((&___contentNode_10), value);
	}

	inline static int32_t get_offset_of_isPartial_11() { return static_cast<int32_t>(offsetof(ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D, ___isPartial_11)); }
	inline bool get_isPartial_11() const { return ___isPartial_11; }
	inline bool* get_address_of_isPartial_11() { return &___isPartial_11; }
	inline void set_isPartial_11(bool value)
	{
		___isPartial_11 = value;
	}

	inline static int32_t get_offset_of_minMaxNodesCount_12() { return static_cast<int32_t>(offsetof(ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D, ___minMaxNodesCount_12)); }
	inline int32_t get_minMaxNodesCount_12() const { return ___minMaxNodesCount_12; }
	inline int32_t* get_address_of_minMaxNodesCount_12() { return &___minMaxNodesCount_12; }
	inline void set_minMaxNodesCount_12(int32_t value)
	{
		___minMaxNodesCount_12 = value;
	}

	inline static int32_t get_offset_of_enableUpaCheck_13() { return static_cast<int32_t>(offsetof(ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D, ___enableUpaCheck_13)); }
	inline bool get_enableUpaCheck_13() const { return ___enableUpaCheck_13; }
	inline bool* get_address_of_enableUpaCheck_13() { return &___enableUpaCheck_13; }
	inline void set_enableUpaCheck_13(bool value)
	{
		___enableUpaCheck_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLECONTENTVALIDATOR_TFF273453F5523E636B8BE9C446AEE1F4090B8D1D_H
#ifndef RANGECONTENTVALIDATOR_T82CC1CA58914E27415EC5FFE20DACD5955385EEF_H
#define RANGECONTENTVALIDATOR_T82CC1CA58914E27415EC5FFE20DACD5955385EEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.RangeContentValidator
struct  RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF  : public ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA
{
public:
	// System.Xml.Schema.BitSet System.Xml.Schema.RangeContentValidator::firstpos
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___firstpos_7;
	// System.Xml.Schema.BitSet[] System.Xml.Schema.RangeContentValidator::followpos
	BitSetU5BU5D_tC61B9A480CE3460277DE0ABFA4551635BB337D69* ___followpos_8;
	// System.Xml.Schema.BitSet System.Xml.Schema.RangeContentValidator::positionsWithRangeTerminals
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___positionsWithRangeTerminals_9;
	// System.Xml.Schema.SymbolsDictionary System.Xml.Schema.RangeContentValidator::symbols
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 * ___symbols_10;
	// System.Xml.Schema.Positions System.Xml.Schema.RangeContentValidator::positions
	Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D * ___positions_11;
	// System.Int32 System.Xml.Schema.RangeContentValidator::minMaxNodesCount
	int32_t ___minMaxNodesCount_12;
	// System.Int32 System.Xml.Schema.RangeContentValidator::endMarkerPos
	int32_t ___endMarkerPos_13;

public:
	inline static int32_t get_offset_of_firstpos_7() { return static_cast<int32_t>(offsetof(RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF, ___firstpos_7)); }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C * get_firstpos_7() const { return ___firstpos_7; }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C ** get_address_of_firstpos_7() { return &___firstpos_7; }
	inline void set_firstpos_7(BitSet_t0E4C53EC600670A4B74C5671553596978880138C * value)
	{
		___firstpos_7 = value;
		Il2CppCodeGenWriteBarrier((&___firstpos_7), value);
	}

	inline static int32_t get_offset_of_followpos_8() { return static_cast<int32_t>(offsetof(RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF, ___followpos_8)); }
	inline BitSetU5BU5D_tC61B9A480CE3460277DE0ABFA4551635BB337D69* get_followpos_8() const { return ___followpos_8; }
	inline BitSetU5BU5D_tC61B9A480CE3460277DE0ABFA4551635BB337D69** get_address_of_followpos_8() { return &___followpos_8; }
	inline void set_followpos_8(BitSetU5BU5D_tC61B9A480CE3460277DE0ABFA4551635BB337D69* value)
	{
		___followpos_8 = value;
		Il2CppCodeGenWriteBarrier((&___followpos_8), value);
	}

	inline static int32_t get_offset_of_positionsWithRangeTerminals_9() { return static_cast<int32_t>(offsetof(RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF, ___positionsWithRangeTerminals_9)); }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C * get_positionsWithRangeTerminals_9() const { return ___positionsWithRangeTerminals_9; }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C ** get_address_of_positionsWithRangeTerminals_9() { return &___positionsWithRangeTerminals_9; }
	inline void set_positionsWithRangeTerminals_9(BitSet_t0E4C53EC600670A4B74C5671553596978880138C * value)
	{
		___positionsWithRangeTerminals_9 = value;
		Il2CppCodeGenWriteBarrier((&___positionsWithRangeTerminals_9), value);
	}

	inline static int32_t get_offset_of_symbols_10() { return static_cast<int32_t>(offsetof(RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF, ___symbols_10)); }
	inline SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 * get_symbols_10() const { return ___symbols_10; }
	inline SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 ** get_address_of_symbols_10() { return &___symbols_10; }
	inline void set_symbols_10(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6 * value)
	{
		___symbols_10 = value;
		Il2CppCodeGenWriteBarrier((&___symbols_10), value);
	}

	inline static int32_t get_offset_of_positions_11() { return static_cast<int32_t>(offsetof(RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF, ___positions_11)); }
	inline Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D * get_positions_11() const { return ___positions_11; }
	inline Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D ** get_address_of_positions_11() { return &___positions_11; }
	inline void set_positions_11(Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D * value)
	{
		___positions_11 = value;
		Il2CppCodeGenWriteBarrier((&___positions_11), value);
	}

	inline static int32_t get_offset_of_minMaxNodesCount_12() { return static_cast<int32_t>(offsetof(RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF, ___minMaxNodesCount_12)); }
	inline int32_t get_minMaxNodesCount_12() const { return ___minMaxNodesCount_12; }
	inline int32_t* get_address_of_minMaxNodesCount_12() { return &___minMaxNodesCount_12; }
	inline void set_minMaxNodesCount_12(int32_t value)
	{
		___minMaxNodesCount_12 = value;
	}

	inline static int32_t get_offset_of_endMarkerPos_13() { return static_cast<int32_t>(offsetof(RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF, ___endMarkerPos_13)); }
	inline int32_t get_endMarkerPos_13() const { return ___endMarkerPos_13; }
	inline int32_t* get_address_of_endMarkerPos_13() { return &___endMarkerPos_13; }
	inline void set_endMarkerPos_13(int32_t value)
	{
		___endMarkerPos_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGECONTENTVALIDATOR_T82CC1CA58914E27415EC5FFE20DACD5955385EEF_H
#ifndef XMLSERIALIZATIONREADCALLBACK_TB4AF7384FD4DBE50B3CCC5C267F2C3CD04F22443_H
#define XMLSERIALIZATIONREADCALLBACK_TB4AF7384FD4DBE50B3CCC5C267F2C3CD04F22443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationReadCallback
struct  XmlSerializationReadCallback_tB4AF7384FD4DBE50B3CCC5C267F2C3CD04F22443  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZATIONREADCALLBACK_TB4AF7384FD4DBE50B3CCC5C267F2C3CD04F22443_H
#ifndef XMLSERIALIZATIONWRITECALLBACK_TC98B104DBDBC776D566F853B9DED7668089431FA_H
#define XMLSERIALIZATIONWRITECALLBACK_TC98B104DBDBC776D566F853B9DED7668089431FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationWriteCallback
struct  XmlSerializationWriteCallback_tC98B104DBDBC776D566F853B9DED7668089431FA  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZATIONWRITECALLBACK_TC98B104DBDBC776D566F853B9DED7668089431FA_H
#ifndef XMLTYPEMAPPING_T2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA_H
#define XMLTYPEMAPPING_T2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapping
struct  XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA  : public XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726
{
public:
	// System.String System.Xml.Serialization.XmlTypeMapping::xmlType
	String_t* ___xmlType_7;
	// System.String System.Xml.Serialization.XmlTypeMapping::xmlTypeNamespace
	String_t* ___xmlTypeNamespace_8;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.XmlTypeMapping::type
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * ___type_9;
	// System.Xml.Serialization.XmlTypeMapping System.Xml.Serialization.XmlTypeMapping::baseMap
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * ___baseMap_10;
	// System.Boolean System.Xml.Serialization.XmlTypeMapping::multiReferenceType
	bool ___multiReferenceType_11;
	// System.Boolean System.Xml.Serialization.XmlTypeMapping::includeInSchema
	bool ___includeInSchema_12;
	// System.Boolean System.Xml.Serialization.XmlTypeMapping::isNullable
	bool ___isNullable_13;
	// System.Boolean System.Xml.Serialization.XmlTypeMapping::isAny
	bool ___isAny_14;
	// System.Collections.ArrayList System.Xml.Serialization.XmlTypeMapping::_derivedTypes
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____derivedTypes_15;

public:
	inline static int32_t get_offset_of_xmlType_7() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___xmlType_7)); }
	inline String_t* get_xmlType_7() const { return ___xmlType_7; }
	inline String_t** get_address_of_xmlType_7() { return &___xmlType_7; }
	inline void set_xmlType_7(String_t* value)
	{
		___xmlType_7 = value;
		Il2CppCodeGenWriteBarrier((&___xmlType_7), value);
	}

	inline static int32_t get_offset_of_xmlTypeNamespace_8() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___xmlTypeNamespace_8)); }
	inline String_t* get_xmlTypeNamespace_8() const { return ___xmlTypeNamespace_8; }
	inline String_t** get_address_of_xmlTypeNamespace_8() { return &___xmlTypeNamespace_8; }
	inline void set_xmlTypeNamespace_8(String_t* value)
	{
		___xmlTypeNamespace_8 = value;
		Il2CppCodeGenWriteBarrier((&___xmlTypeNamespace_8), value);
	}

	inline static int32_t get_offset_of_type_9() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___type_9)); }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * get_type_9() const { return ___type_9; }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 ** get_address_of_type_9() { return &___type_9; }
	inline void set_type_9(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * value)
	{
		___type_9 = value;
		Il2CppCodeGenWriteBarrier((&___type_9), value);
	}

	inline static int32_t get_offset_of_baseMap_10() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___baseMap_10)); }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * get_baseMap_10() const { return ___baseMap_10; }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA ** get_address_of_baseMap_10() { return &___baseMap_10; }
	inline void set_baseMap_10(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * value)
	{
		___baseMap_10 = value;
		Il2CppCodeGenWriteBarrier((&___baseMap_10), value);
	}

	inline static int32_t get_offset_of_multiReferenceType_11() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___multiReferenceType_11)); }
	inline bool get_multiReferenceType_11() const { return ___multiReferenceType_11; }
	inline bool* get_address_of_multiReferenceType_11() { return &___multiReferenceType_11; }
	inline void set_multiReferenceType_11(bool value)
	{
		___multiReferenceType_11 = value;
	}

	inline static int32_t get_offset_of_includeInSchema_12() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___includeInSchema_12)); }
	inline bool get_includeInSchema_12() const { return ___includeInSchema_12; }
	inline bool* get_address_of_includeInSchema_12() { return &___includeInSchema_12; }
	inline void set_includeInSchema_12(bool value)
	{
		___includeInSchema_12 = value;
	}

	inline static int32_t get_offset_of_isNullable_13() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___isNullable_13)); }
	inline bool get_isNullable_13() const { return ___isNullable_13; }
	inline bool* get_address_of_isNullable_13() { return &___isNullable_13; }
	inline void set_isNullable_13(bool value)
	{
		___isNullable_13 = value;
	}

	inline static int32_t get_offset_of_isAny_14() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___isAny_14)); }
	inline bool get_isAny_14() const { return ___isAny_14; }
	inline bool* get_address_of_isAny_14() { return &___isAny_14; }
	inline void set_isAny_14(bool value)
	{
		___isAny_14 = value;
	}

	inline static int32_t get_offset_of__derivedTypes_15() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ____derivedTypes_15)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__derivedTypes_15() const { return ____derivedTypes_15; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__derivedTypes_15() { return &____derivedTypes_15; }
	inline void set__derivedTypes_15(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____derivedTypes_15 = value;
		Il2CppCodeGenWriteBarrier((&____derivedTypes_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPPING_T2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA_H
#ifndef DATATYPE_LIST_TD4CB4D33323B0D332A6C1990C4F1A891BBCB57DF_H
#define DATATYPE_LIST_TD4CB4D33323B0D332A6C1990C4F1A891BBCB57DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_List
struct  Datatype_List_tD4CB4D33323B0D332A6C1990C4F1A891BBCB57DF  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.Datatype_List::itemType
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___itemType_93;
	// System.Int32 System.Xml.Schema.Datatype_List::minListSize
	int32_t ___minListSize_94;

public:
	inline static int32_t get_offset_of_itemType_93() { return static_cast<int32_t>(offsetof(Datatype_List_tD4CB4D33323B0D332A6C1990C4F1A891BBCB57DF, ___itemType_93)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_itemType_93() const { return ___itemType_93; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_itemType_93() { return &___itemType_93; }
	inline void set_itemType_93(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___itemType_93 = value;
		Il2CppCodeGenWriteBarrier((&___itemType_93), value);
	}

	inline static int32_t get_offset_of_minListSize_94() { return static_cast<int32_t>(offsetof(Datatype_List_tD4CB4D33323B0D332A6C1990C4F1A891BBCB57DF, ___minListSize_94)); }
	inline int32_t get_minListSize_94() const { return ___minListSize_94; }
	inline int32_t* get_address_of_minListSize_94() { return &___minListSize_94; }
	inline void set_minListSize_94(int32_t value)
	{
		___minListSize_94 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_LIST_TD4CB4D33323B0D332A6C1990C4F1A891BBCB57DF_H
#ifndef DATATYPE_ANYATOMICTYPE_TD9993791C08ACD9B7F9AB91F05DBFF2B47C08142_H
#define DATATYPE_ANYATOMICTYPE_TD9993791C08ACD9B7F9AB91F05DBFF2B47C08142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_anyAtomicType
struct  Datatype_anyAtomicType_tD9993791C08ACD9B7F9AB91F05DBFF2B47C08142  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_ANYATOMICTYPE_TD9993791C08ACD9B7F9AB91F05DBFF2B47C08142_H
#ifndef DATATYPE_BOOLEAN_TB1805FB62F6452B8D69E9840D65139869BF76B62_H
#define DATATYPE_BOOLEAN_TB1805FB62F6452B8D69E9840D65139869BF76B62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_boolean
struct  Datatype_boolean_tB1805FB62F6452B8D69E9840D65139869BF76B62  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_boolean_tB1805FB62F6452B8D69E9840D65139869BF76B62_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_boolean::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_boolean::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_boolean_tB1805FB62F6452B8D69E9840D65139869BF76B62_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_boolean_tB1805FB62F6452B8D69E9840D65139869BF76B62_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_BOOLEAN_TB1805FB62F6452B8D69E9840D65139869BF76B62_H
#ifndef DATATYPE_DECIMAL_T3943079EC38B2D86AB31D0141D563FE2EC063F60_H
#define DATATYPE_DECIMAL_T3943079EC38B2D86AB31D0141D563FE2EC063F60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_decimal
struct  Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_decimal::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_decimal::listValueType
	Type_t * ___listValueType_94;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_decimal::numeric10FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric10FacetsChecker_95;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_95() { return static_cast<int32_t>(offsetof(Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60_StaticFields, ___numeric10FacetsChecker_95)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric10FacetsChecker_95() const { return ___numeric10FacetsChecker_95; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric10FacetsChecker_95() { return &___numeric10FacetsChecker_95; }
	inline void set_numeric10FacetsChecker_95(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric10FacetsChecker_95 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_95), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DECIMAL_T3943079EC38B2D86AB31D0141D563FE2EC063F60_H
#ifndef DATATYPE_DOUBLE_TD5564072018557D0CCFA5E2BFC9902E197F221F1_H
#define DATATYPE_DOUBLE_TD5564072018557D0CCFA5E2BFC9902E197F221F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_double
struct  Datatype_double_tD5564072018557D0CCFA5E2BFC9902E197F221F1  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_double_tD5564072018557D0CCFA5E2BFC9902E197F221F1_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_double::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_double::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_double_tD5564072018557D0CCFA5E2BFC9902E197F221F1_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_double_tD5564072018557D0CCFA5E2BFC9902E197F221F1_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DOUBLE_TD5564072018557D0CCFA5E2BFC9902E197F221F1_H
#ifndef DATATYPE_DURATION_T48CF7AD2DBEE484788997789311EACD432CCAC55_H
#define DATATYPE_DURATION_T48CF7AD2DBEE484788997789311EACD432CCAC55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_duration
struct  Datatype_duration_t48CF7AD2DBEE484788997789311EACD432CCAC55  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_duration_t48CF7AD2DBEE484788997789311EACD432CCAC55_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_duration::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_duration::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_duration_t48CF7AD2DBEE484788997789311EACD432CCAC55_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_duration_t48CF7AD2DBEE484788997789311EACD432CCAC55_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DURATION_T48CF7AD2DBEE484788997789311EACD432CCAC55_H
#ifndef DATATYPE_FLOAT_T368DEB66F38BC7C4420BEFD96E4E4396C690DC09_H
#define DATATYPE_FLOAT_T368DEB66F38BC7C4420BEFD96E4E4396C690DC09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_float
struct  Datatype_float_t368DEB66F38BC7C4420BEFD96E4E4396C690DC09  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_float_t368DEB66F38BC7C4420BEFD96E4E4396C690DC09_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_float::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_float::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_float_t368DEB66F38BC7C4420BEFD96E4E4396C690DC09_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_float_t368DEB66F38BC7C4420BEFD96E4E4396C690DC09_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_FLOAT_T368DEB66F38BC7C4420BEFD96E4E4396C690DC09_H
#ifndef DATATYPE_STRING_T99A2A08E58CE5D513A0C1BB3C4E9E06EE80935E2_H
#define DATATYPE_STRING_T99A2A08E58CE5D513A0C1BB3C4E9E06EE80935E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_string
struct  Datatype_string_t99A2A08E58CE5D513A0C1BB3C4E9E06EE80935E2  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_STRING_T99A2A08E58CE5D513A0C1BB3C4E9E06EE80935E2_H
#ifndef DATATYPE_UNION_TE2A2CBDE037535DC3CCE37411750AC4EF2009F40_H
#define DATATYPE_UNION_TE2A2CBDE037535DC3CCE37411750AC4EF2009F40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_union
struct  Datatype_union_tE2A2CBDE037535DC3CCE37411750AC4EF2009F40  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType[] System.Xml.Schema.Datatype_union::types
	XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B* ___types_95;

public:
	inline static int32_t get_offset_of_types_95() { return static_cast<int32_t>(offsetof(Datatype_union_tE2A2CBDE037535DC3CCE37411750AC4EF2009F40, ___types_95)); }
	inline XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B* get_types_95() const { return ___types_95; }
	inline XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B** get_address_of_types_95() { return &___types_95; }
	inline void set_types_95(XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B* value)
	{
		___types_95 = value;
		Il2CppCodeGenWriteBarrier((&___types_95), value);
	}
};

struct Datatype_union_tE2A2CBDE037535DC3CCE37411750AC4EF2009F40_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_union::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_union::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_union_tE2A2CBDE037535DC3CCE37411750AC4EF2009F40_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_union_tE2A2CBDE037535DC3CCE37411750AC4EF2009F40_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_UNION_TE2A2CBDE037535DC3CCE37411750AC4EF2009F40_H
#ifndef XMLSERIALIZABLEMAPPING_T7C5AB206754A0134E47A02D23DDBE336BE9DEB0D_H
#define XMLSERIALIZABLEMAPPING_T7C5AB206754A0134E47A02D23DDBE336BE9DEB0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializableMapping
struct  XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D  : public XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA
{
public:
	// System.Xml.Schema.XmlSchema System.Xml.Serialization.XmlSerializableMapping::_schema
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ____schema_16;
	// System.Xml.Schema.XmlSchemaComplexType System.Xml.Serialization.XmlSerializableMapping::_schemaType
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * ____schemaType_17;
	// System.Xml.XmlQualifiedName System.Xml.Serialization.XmlSerializableMapping::_schemaTypeName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ____schemaTypeName_18;

public:
	inline static int32_t get_offset_of__schema_16() { return static_cast<int32_t>(offsetof(XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D, ____schema_16)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get__schema_16() const { return ____schema_16; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of__schema_16() { return &____schema_16; }
	inline void set__schema_16(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		____schema_16 = value;
		Il2CppCodeGenWriteBarrier((&____schema_16), value);
	}

	inline static int32_t get_offset_of__schemaType_17() { return static_cast<int32_t>(offsetof(XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D, ____schemaType_17)); }
	inline XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * get__schemaType_17() const { return ____schemaType_17; }
	inline XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 ** get_address_of__schemaType_17() { return &____schemaType_17; }
	inline void set__schemaType_17(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * value)
	{
		____schemaType_17 = value;
		Il2CppCodeGenWriteBarrier((&____schemaType_17), value);
	}

	inline static int32_t get_offset_of__schemaTypeName_18() { return static_cast<int32_t>(offsetof(XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D, ____schemaTypeName_18)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get__schemaTypeName_18() const { return ____schemaTypeName_18; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of__schemaTypeName_18() { return &____schemaTypeName_18; }
	inline void set__schemaTypeName_18(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		____schemaTypeName_18 = value;
		Il2CppCodeGenWriteBarrier((&____schemaTypeName_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZABLEMAPPING_T7C5AB206754A0134E47A02D23DDBE336BE9DEB0D_H
#ifndef DATATYPE_DAYTIMEDURATION_TEFDAA51793B8ECAC56F6A769E15660728A220BAB_H
#define DATATYPE_DAYTIMEDURATION_TEFDAA51793B8ECAC56F6A769E15660728A220BAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_dayTimeDuration
struct  Datatype_dayTimeDuration_tEFDAA51793B8ECAC56F6A769E15660728A220BAB  : public Datatype_duration_t48CF7AD2DBEE484788997789311EACD432CCAC55
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DAYTIMEDURATION_TEFDAA51793B8ECAC56F6A769E15660728A220BAB_H
#ifndef DATATYPE_UNTYPEDATOMICTYPE_TFB0567D465A1F4D07CD22818D93C0B56E2FE3EDD_H
#define DATATYPE_UNTYPEDATOMICTYPE_TFB0567D465A1F4D07CD22818D93C0B56E2FE3EDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_untypedAtomicType
struct  Datatype_untypedAtomicType_tFB0567D465A1F4D07CD22818D93C0B56E2FE3EDD  : public Datatype_anyAtomicType_tD9993791C08ACD9B7F9AB91F05DBFF2B47C08142
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_UNTYPEDATOMICTYPE_TFB0567D465A1F4D07CD22818D93C0B56E2FE3EDD_H
#ifndef DATATYPE_YEARMONTHDURATION_T48DB455A054A48BA0C8F4E3553B567F6DC599F3E_H
#define DATATYPE_YEARMONTHDURATION_T48DB455A054A48BA0C8F4E3553B567F6DC599F3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_yearMonthDuration
struct  Datatype_yearMonthDuration_t48DB455A054A48BA0C8F4E3553B567F6DC599F3E  : public Datatype_duration_t48CF7AD2DBEE484788997789311EACD432CCAC55
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_YEARMONTHDURATION_T48DB455A054A48BA0C8F4E3553B567F6DC599F3E_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (XmlSerializationGeneratedCode_tD121073E4460FCF7F6B549E4621B0CB971D2E6CE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (XmlSerializationReadCallback_tB4AF7384FD4DBE50B3CCC5C267F2C3CD04F22443), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1902[25] = 
{
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_document_0(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_reader_1(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_fixups_2(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_collFixups_3(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_collItemFixups_4(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_typesCallbacks_5(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_noIDTargets_6(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_targets_7(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_delayedListFixups_8(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_eventSource_9(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_delayedFixupId_10(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_referencedObjects_11(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_readCount_12(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_whileIterationCount_13(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_w3SchemaNS_14(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_w3InstanceNS_15(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_w3InstanceNS2000_16(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_w3InstanceNS1999_17(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_soapNS_18(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_wsdlNS_19(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_nullX_20(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_nil_21(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_typeX_22(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_arrayType_23(),
	XmlSerializationReader_tDE18F45AFFD6D7F1C6527B4BFFEE35AC23EFBC88::get_offset_of_arrayQName_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (WriteCallbackInfo_t05B6DF593FD8E2A5F05B109D252A4AD76EE3AF85), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1903[4] = 
{
	WriteCallbackInfo_t05B6DF593FD8E2A5F05B109D252A4AD76EE3AF85::get_offset_of_Type_0(),
	WriteCallbackInfo_t05B6DF593FD8E2A5F05B109D252A4AD76EE3AF85::get_offset_of_TypeName_1(),
	WriteCallbackInfo_t05B6DF593FD8E2A5F05B109D252A4AD76EE3AF85::get_offset_of_TypeNs_2(),
	WriteCallbackInfo_t05B6DF593FD8E2A5F05B109D252A4AD76EE3AF85::get_offset_of_Callback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (CollectionFixup_t2C3A0520CE6E7EC1C69E8A9B8EDED9E281097948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1904[4] = 
{
	CollectionFixup_t2C3A0520CE6E7EC1C69E8A9B8EDED9E281097948::get_offset_of_callback_0(),
	CollectionFixup_t2C3A0520CE6E7EC1C69E8A9B8EDED9E281097948::get_offset_of_collection_1(),
	CollectionFixup_t2C3A0520CE6E7EC1C69E8A9B8EDED9E281097948::get_offset_of_collectionItems_2(),
	CollectionFixup_t2C3A0520CE6E7EC1C69E8A9B8EDED9E281097948::get_offset_of_id_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (Fixup_tA77CFD3F2CFCEC1D6A71BB69AC3C98AFC9883EDF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[3] = 
{
	Fixup_tA77CFD3F2CFCEC1D6A71BB69AC3C98AFC9883EDF::get_offset_of_source_0(),
	Fixup_tA77CFD3F2CFCEC1D6A71BB69AC3C98AFC9883EDF::get_offset_of_ids_1(),
	Fixup_tA77CFD3F2CFCEC1D6A71BB69AC3C98AFC9883EDF::get_offset_of_callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (CollectionItemFixup_t8E53F773DA567B6109BE49A8CC3F85A349055569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1906[3] = 
{
	CollectionItemFixup_t8E53F773DA567B6109BE49A8CC3F85A349055569::get_offset_of_list_0(),
	CollectionItemFixup_t8E53F773DA567B6109BE49A8CC3F85A349055569::get_offset_of_index_1(),
	CollectionItemFixup_t8E53F773DA567B6109BE49A8CC3F85A349055569::get_offset_of_id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C), -1, sizeof(XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1907[4] = 
{
	XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C::get_offset_of__typeMap_25(),
	XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C::get_offset_of__format_26(),
	XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C_StaticFields::get_offset_of_AnyType_27(),
	XmlSerializationReaderInterpreter_t8D5678E515DA59060E81B56B5052B97EEDC3B44C_StaticFields::get_offset_of_empty_array_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (FixupCallbackInfo_t624A8800237A1808944906FDC24BE9244B65F71D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[3] = 
{
	FixupCallbackInfo_t624A8800237A1808944906FDC24BE9244B65F71D::get_offset_of__sri_0(),
	FixupCallbackInfo_t624A8800237A1808944906FDC24BE9244B65F71D::get_offset_of__map_1(),
	FixupCallbackInfo_t624A8800237A1808944906FDC24BE9244B65F71D::get_offset_of__isValueList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (ReaderCallbackInfo_t3649059057730362A2C3655BD0E4024E8DDC9C61), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[2] = 
{
	ReaderCallbackInfo_t3649059057730362A2C3655BD0E4024E8DDC9C61::get_offset_of__sri_0(),
	ReaderCallbackInfo_t3649059057730362A2C3655BD0E4024E8DDC9C61::get_offset_of__typeMap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (XmlSerializationWriteCallback_tC98B104DBDBC776D566F853B9DED7668089431FA), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1911[8] = 
{
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_idGenerator_0(),
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_qnameCount_1(),
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_topLevelElement_2(),
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_namespaces_3(),
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_writer_4(),
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_referencedElements_5(),
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_callbacks_6(),
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_serializedObjects_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[4] = 
{
	WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC::get_offset_of_Type_0(),
	WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC::get_offset_of_TypeName_1(),
	WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC::get_offset_of_TypeNs_2(),
	WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC::get_offset_of_Callback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[2] = 
{
	XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221::get_offset_of__typeMap_8(),
	XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221::get_offset_of__format_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (CallbackInfo_t6FC419FB131647181747C58C4AA7642E18071292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1914[2] = 
{
	CallbackInfo_t6FC419FB131647181747C58C4AA7642E18071292::get_offset_of__swi_0(),
	CallbackInfo_t6FC419FB131647181747C58C4AA7642E18071292::get_offset_of__typeMap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C), -1, sizeof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1915[13] = 
{
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields::get_offset_of_generationThreshold_0(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields::get_offset_of_backgroundGeneration_1(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields::get_offset_of_deleteTempFiles_2(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields::get_offset_of_generatorFallback_3(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C::get_offset_of_customSerializer_4(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C::get_offset_of_typeMapping_5(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C::get_offset_of_serializerData_6(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields::get_offset_of_serializerTypes_7(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C::get_offset_of_onUnreferencedObject_8(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C::get_offset_of_onUnknownAttribute_9(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C::get_offset_of_onUnknownElement_10(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C::get_offset_of_onUnknownNode_11(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields::get_offset_of_DefaultEncoding_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1916[4] = 
{
	SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8::get_offset_of_ReaderMethod_0(),
	SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8::get_offset_of_WriterType_1(),
	SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8::get_offset_of_WriterMethod_2(),
	SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8::get_offset_of_Implementation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (XmlSerializerFactory_tEAB3633D18CDF075A1EC73FFDA7132978F308C74), -1, sizeof(XmlSerializerFactory_tEAB3633D18CDF075A1EC73FFDA7132978F308C74_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1917[1] = 
{
	XmlSerializerFactory_tEAB3633D18CDF075A1EC73FFDA7132978F308C74_StaticFields::get_offset_of_serializersBySource_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (XmlSerializerImplementation_tF78BC93462D4003D83EAA821331747A0624F837D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1919[2] = 
{
	XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7::get_offset_of_dataType_0(),
	XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1920[3] = 
{
	XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08::get_offset_of_includeInSchema_0(),
	XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08::get_offset_of_ns_1(),
	XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08::get_offset_of_typeName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[11] = 
{
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__elementName_0(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__namespace_1(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__form_2(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__member_3(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__choiceValue_4(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__isNullable_5(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__nestingLevel_6(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__mappedType_7(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__type_8(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__wrappedElement_9(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__explicitOrder_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[10] = 
{
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__name_0(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__index_1(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__globalIndex_2(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__specifiedGlobalIndex_3(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__typeData_4(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__member_5(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__specifiedMember_6(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__shouldSerialize_7(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__defaultValue_8(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__flags_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[4] = 
{
	XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C::get_offset_of__attributeName_10(),
	XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C::get_offset_of__namespace_11(),
	XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C::get_offset_of__form_12(),
	XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C::get_offset_of__mappedType_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1925[4] = 
{
	XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00::get_offset_of__elementInfo_10(),
	XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00::get_offset_of__choiceMember_11(),
	XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00::get_offset_of__isTextCollector_12(),
	XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00::get_offset_of__choiceTypeData_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (XmlTypeMapMemberList_t408EB21F4809E9C2E2059BDE5F77F28F503EA48F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (XmlTypeMapMemberExpandable_tEFC74196D6503AB38EEA1AD371BA55EABC7C30ED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1927[1] = 
{
	XmlTypeMapMemberExpandable_tEFC74196D6503AB38EEA1AD371BA55EABC7C30ED::get_offset_of__flatArrayIndex_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (XmlTypeMapMemberFlatList_tEA9F25C615A27D3B9761879A55C2BF74E394D31F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[1] = 
{
	XmlTypeMapMemberFlatList_tEA9F25C615A27D3B9761879A55C2BF74E394D31F::get_offset_of__listMap_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (XmlTypeMapMemberAnyElement_tEC6C4FFA530F292BC4C09145DA5937285A664472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (XmlTypeMapMemberAnyAttribute_t0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (XmlTypeMapMemberNamespaces_t08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[9] = 
{
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_xmlType_7(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_xmlTypeNamespace_8(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_type_9(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_baseMap_10(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_multiReferenceType_11(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_includeInSchema_12(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_isNullable_13(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_isAny_14(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of__derivedTypes_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1933[3] = 
{
	XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D::get_offset_of__schema_16(),
	XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D::get_offset_of__schemaType_17(),
	XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D::get_offset_of__schemaTypeName_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[16] = 
{
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__elements_0(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__elementMembers_1(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__attributeMembers_2(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__attributeMembersArray_3(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__flatLists_4(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__allMembers_5(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__membersWithDefault_6(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__listMembers_7(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__defaultAnyElement_8(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__defaultAnyAttribute_9(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__namespaceDeclarations_10(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__xmlTextCollector_11(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__returnMember_12(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__ignoreMemberNamespace_13(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__canBeSimpleType_14(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__isOrderDependentMap_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1935[2] = 
{
	ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742::get_offset_of__itemInfo_0(),
	ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742::get_offset_of__choiceMember_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[5] = 
{
	EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A::get_offset_of__members_0(),
	EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A::get_offset_of__isFlags_1(),
	EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A::get_offset_of__enumNames_2(),
	EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A::get_offset_of__xmlNames_3(),
	EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A::get_offset_of__values_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1937[3] = 
{
	EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3::get_offset_of__xmlName_0(),
	EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3::get_offset_of__enumName_1(),
	EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3::get_offset_of__value_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1938[4] = 
{
	AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80::get_offset_of_curNode_0(),
	AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80::get_offset_of_rootDepth_1(),
	AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80::get_offset_of_curDepth_2(),
	AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80::get_offset_of_isMatch_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1939[3] = 
{
	AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2::get_offset_of_stack_0(),
	AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2::get_offset_of_subtree_1(),
	AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2::get_offset_of_parent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1940[4] = 
{
	ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E::get_offset_of_currentDepth_0(),
	ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E::get_offset_of_isActive_1(),
	ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E::get_offset_of_axisTree_2(),
	ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E::get_offset_of_axisStack_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1941[1] = 
{
	DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955::get_offset_of_next_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1942[5] = 
{
	ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253::get_offset_of_topNode_0(),
	ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253::get_offset_of_rootNode_1(),
	ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253::get_offset_of_isAttribute_2(),
	ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253::get_offset_of_isDss_3(),
	ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253::get_offset_of_isSelfAxis_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[4] = 
{
	Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA::get_offset_of_fAxisArray_0(),
	Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA::get_offset_of_xpathexpr_1(),
	Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA::get_offset_of_isField_2(),
	Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA::get_offset_of_nsmgr_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (AutoValidator_t348DF836BF1829DC3B8DBA19A6E0EFC1A6EE85EB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1945[6] = 
{
	BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427::get_offset_of_nameTable_0(),
	BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427::get_offset_of_schemaNames_1(),
	BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427::get_offset_of_eventHandler_2(),
	BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427::get_offset_of_compilationSettings_3(),
	BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427::get_offset_of_errorCount_4(),
	BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427::get_offset_of_NsXml_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1946[15] = 
{
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_schemaCollection_0(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_eventHandling_1(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_nameTable_2(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_schemaNames_3(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_positionInfo_4(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_xmlResolver_5(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_baseUri_6(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_schemaInfo_7(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_reader_8(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_elementName_9(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_context_10(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_textValue_11(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_textString_12(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_hasSibling_13(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_checkDatatype_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (BitSet_t0E4C53EC600670A4B74C5671553596978880138C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1947[2] = 
{
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C::get_offset_of_count_0(),
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C::get_offset_of_bits_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1948[4] = 
{
	ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D::get_offset_of_targetNS_0(),
	ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D::get_offset_of_chameleonLocation_1(),
	ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D::get_offset_of_originalSchema_2(),
	ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D::get_offset_of_hashCode_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E), -1, sizeof(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1949[6] = 
{
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E::get_offset_of_name_0(),
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E::get_offset_of_role_1(),
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E::get_offset_of_selector_2(),
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E::get_offset_of_fields_3(),
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E::get_offset_of_refer_4(),
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E_StaticFields::get_offset_of_Empty_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (ConstraintRole_tDF2C7F62C4BF46B0BDDB0F70B8FDE3DBF2BB4397)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1950[4] = 
{
	ConstraintRole_tDF2C7F62C4BF46B0BDDB0F70B8FDE3DBF2BB4397::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[6] = 
{
	ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54::get_offset_of_constraint_0(),
	ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54::get_offset_of_axisSelector_1(),
	ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54::get_offset_of_axisFields_2(),
	ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54::get_offset_of_qualifiedTable_3(),
	ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54::get_offset_of_keyrefTable_4(),
	ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54::get_offset_of_tableDim_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[3] = 
{
	LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5::get_offset_of_column_4(),
	LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5::get_offset_of_isMatched_5(),
	LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5::get_offset_of_Ks_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[3] = 
{
	SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64::get_offset_of_cs_4(),
	SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64::get_offset_of_KSs_5(),
	SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64::get_offset_of_KSpointer_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1954[3] = 
{
	KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E::get_offset_of_depth_0(),
	KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E::get_offset_of_ks_1(),
	KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E::get_offset_of_fields_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1955[6] = 
{
	TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D::get_offset_of_dstruct_0(),
	TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D::get_offset_of_ovalue_1(),
	TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D::get_offset_of_svalue_2(),
	TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D::get_offset_of_xsdtype_3(),
	TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D::get_offset_of_dim_4(),
	TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D::get_offset_of_isList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[2] = 
{
	DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11::get_offset_of_isDecimal_0(),
	DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11::get_offset_of_dvalue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[5] = 
{
	KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE::get_offset_of_ks_0(),
	KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE::get_offset_of_dim_1(),
	KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE::get_offset_of_hashcode_2(),
	KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE::get_offset_of_posline_3(),
	KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE::get_offset_of_poscol_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (UpaException_tED69C4AC662CA80677ABA101231042E747107151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1958[2] = 
{
	UpaException_tED69C4AC662CA80677ABA101231042E747107151::get_offset_of_particle1_17(),
	UpaException_tED69C4AC662CA80677ABA101231042E747107151::get_offset_of_particle2_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[6] = 
{
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6::get_offset_of_last_0(),
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6::get_offset_of_names_1(),
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6::get_offset_of_wildcards_2(),
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6::get_offset_of_particles_3(),
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6::get_offset_of_particleLast_4(),
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6::get_offset_of_isUpaEnforced_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (Position_t089976E4BEB3D345DA28CFA95786EE065063E228)+ sizeof (RuntimeObject), sizeof(Position_t089976E4BEB3D345DA28CFA95786EE065063E228_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1960[2] = 
{
	Position_t089976E4BEB3D345DA28CFA95786EE065063E228::get_offset_of_symbol_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Position_t089976E4BEB3D345DA28CFA95786EE065063E228::get_offset_of_particle_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[1] = 
{
	Positions_t527FBF1910108E9A01D1D42495E76E7192181A9D::get_offset_of_positions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (SyntaxTreeNode_t7B5A3386C72D5DF7C5B26DF4468EDF5FADC2A08C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (LeafNode_tE2C955D778068BA019B8D106487A698656F863E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1963[1] = 
{
	LeafNode_tE2C955D778068BA019B8D106487A698656F863E2::get_offset_of_pos_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (NamespaceListNode_t7C392E0F03C23BE2D7261C64D3DB816CA6A8EC19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1964[2] = 
{
	NamespaceListNode_t7C392E0F03C23BE2D7261C64D3DB816CA6A8EC19::get_offset_of_namespaceList_0(),
	NamespaceListNode_t7C392E0F03C23BE2D7261C64D3DB816CA6A8EC19::get_offset_of_particle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (InteriorNode_t274145A160D08BE97CA9437691E2AE9A4D30A51D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1965[2] = 
{
	InteriorNode_t274145A160D08BE97CA9437691E2AE9A4D30A51D::get_offset_of_leftChild_0(),
	InteriorNode_t274145A160D08BE97CA9437691E2AE9A4D30A51D::get_offset_of_rightChild_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (SequenceNode_tAB18F790CB1B5BCD1D984EECC17C841B00881608), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (SequenceConstructPosContext_t72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1967[5] = 
{
	SequenceConstructPosContext_t72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1::get_offset_of_this__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SequenceConstructPosContext_t72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1::get_offset_of_firstpos_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SequenceConstructPosContext_t72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1::get_offset_of_lastpos_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SequenceConstructPosContext_t72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1::get_offset_of_lastposLeft_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SequenceConstructPosContext_t72DF930B1BE2676BD225E8D9622C78EF2B0DFAC1::get_offset_of_firstposRight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (ChoiceNode_t389906D9F3EDD9F3D3BA60CFCCD22670A7760F61), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (PlusNode_t906E55FC467F9EE2ADF99B88E9349101E28394B0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (QmarkNode_t42CEA81806E0B4CB67A2DA59BBF43F6279642716), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (StarNode_tDF30983D880A2C8707F144D32EA266A257045A30), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (LeafRangeNode_t457FCA3B9C122C01958E0EBD8D1B9D54A5D723F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1972[3] = 
{
	LeafRangeNode_t457FCA3B9C122C01958E0EBD8D1B9D54A5D723F5::get_offset_of_min_1(),
	LeafRangeNode_t457FCA3B9C122C01958E0EBD8D1B9D54A5D723F5::get_offset_of_max_2(),
	LeafRangeNode_t457FCA3B9C122C01958E0EBD8D1B9D54A5D723F5::get_offset_of_nextIteration_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA), -1, sizeof(ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1973[7] = 
{
	ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA::get_offset_of_contentType_0(),
	ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA::get_offset_of_isOpen_1(),
	ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA::get_offset_of_isEmptiable_2(),
	ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA_StaticFields::get_offset_of_Empty_3(),
	ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA_StaticFields::get_offset_of_TextOnly_4(),
	ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA_StaticFields::get_offset_of_Mixed_5(),
	ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA_StaticFields::get_offset_of_Any_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1974[7] = 
{
	ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D::get_offset_of_symbols_7(),
	ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D::get_offset_of_positions_8(),
	ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D::get_offset_of_stack_9(),
	ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D::get_offset_of_contentNode_10(),
	ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D::get_offset_of_isPartial_11(),
	ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D::get_offset_of_minMaxNodesCount_12(),
	ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D::get_offset_of_enableUpaCheck_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (DfaContentValidator_t1FBCE29B0AD46CA19740B8BB2815C95895BF9394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1975[2] = 
{
	DfaContentValidator_t1FBCE29B0AD46CA19740B8BB2815C95895BF9394::get_offset_of_transitionTable_7(),
	DfaContentValidator_t1FBCE29B0AD46CA19740B8BB2815C95895BF9394::get_offset_of_symbols_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (NfaContentValidator_tC513066391441B47E0D46A11F7D48CF004F930DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1976[5] = 
{
	NfaContentValidator_tC513066391441B47E0D46A11F7D48CF004F930DA::get_offset_of_firstpos_7(),
	NfaContentValidator_tC513066391441B47E0D46A11F7D48CF004F930DA::get_offset_of_followpos_8(),
	NfaContentValidator_tC513066391441B47E0D46A11F7D48CF004F930DA::get_offset_of_symbols_9(),
	NfaContentValidator_tC513066391441B47E0D46A11F7D48CF004F930DA::get_offset_of_positions_10(),
	NfaContentValidator_tC513066391441B47E0D46A11F7D48CF004F930DA::get_offset_of_endMarkerPos_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (RangePositionInfo_tDCA2617E7E1292998A9700E38DBBA177330A80CA)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1977[2] = 
{
	RangePositionInfo_tDCA2617E7E1292998A9700E38DBBA177330A80CA::get_offset_of_curpos_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RangePositionInfo_tDCA2617E7E1292998A9700E38DBBA177330A80CA::get_offset_of_rangeCounters_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1978[7] = 
{
	RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF::get_offset_of_firstpos_7(),
	RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF::get_offset_of_followpos_8(),
	RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF::get_offset_of_positionsWithRangeTerminals_9(),
	RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF::get_offset_of_symbols_10(),
	RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF::get_offset_of_positions_11(),
	RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF::get_offset_of_minMaxNodesCount_12(),
	RangeContentValidator_t82CC1CA58914E27415EC5FFE20DACD5955385EEF::get_offset_of_endMarkerPos_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (AllElementsContentValidator_t6F87D7B72856382DAF3FA1EA055455AC525AA355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1979[4] = 
{
	AllElementsContentValidator_t6F87D7B72856382DAF3FA1EA055455AC525AA355::get_offset_of_elements_7(),
	AllElementsContentValidator_t6F87D7B72856382DAF3FA1EA055455AC525AA355::get_offset_of_particles_8(),
	AllElementsContentValidator_t6F87D7B72856382DAF3FA1EA055455AC525AA355::get_offset_of_isRequired_9(),
	AllElementsContentValidator_t6F87D7B72856382DAF3FA1EA055455AC525AA355::get_offset_of_countRequired_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (XmlSchemaDatatypeVariety_t26489B3A13FBF83AEBD1E94ECACD30AF091C9A02)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1980[4] = 
{
	XmlSchemaDatatypeVariety_t26489B3A13FBF83AEBD1E94ECACD30AF091C9A02::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (XsdSimpleValue_t543244559B1F2C32855BAD8CC3EDB4AD5BED96CF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1981[2] = 
{
	XsdSimpleValue_t543244559B1F2C32855BAD8CC3EDB4AD5BED96CF::get_offset_of_xmlType_0(),
	XsdSimpleValue_t543244559B1F2C32855BAD8CC3EDB4AD5BED96CF::get_offset_of_typedValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (RestrictionFlags_t638CE72420C3F9885392EE7E57E54B5C9DEA7D8D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1982[13] = 
{
	RestrictionFlags_t638CE72420C3F9885392EE7E57E54B5C9DEA7D8D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (XmlSchemaWhiteSpace_t8E7AB044CFB43348933A839AB7D61D50501D6551)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1983[4] = 
{
	XmlSchemaWhiteSpace_t8E7AB044CFB43348933A839AB7D61D50501D6551::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1984[14] = 
{
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4::get_offset_of_Length_0(),
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4::get_offset_of_MinLength_1(),
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4::get_offset_of_MaxLength_2(),
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4::get_offset_of_Patterns_3(),
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4::get_offset_of_Enumeration_4(),
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4::get_offset_of_WhiteSpace_5(),
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4::get_offset_of_MaxInclusive_6(),
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4::get_offset_of_MaxExclusive_7(),
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4::get_offset_of_MinInclusive_8(),
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4::get_offset_of_MinExclusive_9(),
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4::get_offset_of_TotalDigits_10(),
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4::get_offset_of_FractionDigits_11(),
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4::get_offset_of_Flags_12(),
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4::get_offset_of_FixedFlags_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836), -1, sizeof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1985[91] = 
{
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836::get_offset_of_variety_0(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836::get_offset_of_restriction_1(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836::get_offset_of_baseType_2(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836::get_offset_of_valueConverter_3(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836::get_offset_of_parentSchemaType_4(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_builtinTypes_5(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_enumToTypeCode_6(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_anySimpleType_7(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_anyAtomicType_8(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_untypedAtomicType_9(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_yearMonthDurationType_10(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_dayTimeDurationType_11(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_normalizedStringTypeV1Compat_12(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_tokenTypeV1Compat_13(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_QnAnySimpleType_14(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_QnAnyType_15(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_stringFacetsChecker_16(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_miscFacetsChecker_17(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_numeric2FacetsChecker_18(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_binaryFacetsChecker_19(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_dateTimeFacetsChecker_20(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_durationFacetsChecker_21(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_listFacetsChecker_22(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_qnameFacetsChecker_23(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_unionFacetsChecker_24(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_anySimpleType_25(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_anyURI_26(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_base64Binary_27(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_boolean_28(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_byte_29(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_char_30(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_date_31(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_dateTime_32(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_dateTimeNoTz_33(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_dateTimeTz_34(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_day_35(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_decimal_36(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_double_37(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_doubleXdr_38(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_duration_39(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_ENTITY_40(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_ENTITIES_41(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_ENUMERATION_42(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_fixed_43(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_float_44(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_floatXdr_45(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_hexBinary_46(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_ID_47(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_IDREF_48(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_IDREFS_49(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_int_50(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_integer_51(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_language_52(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_long_53(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_month_54(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_monthDay_55(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_Name_56(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_NCName_57(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_negativeInteger_58(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_NMTOKEN_59(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_NMTOKENS_60(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_nonNegativeInteger_61(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_nonPositiveInteger_62(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_normalizedString_63(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_NOTATION_64(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_positiveInteger_65(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_QName_66(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_QNameXdr_67(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_short_68(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_string_69(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_time_70(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_timeNoTz_71(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_timeTz_72(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_token_73(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_unsignedByte_74(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_unsignedInt_75(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_unsignedLong_76(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_unsignedShort_77(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_uuid_78(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_year_79(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_yearMonth_80(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_normalizedStringV1Compat_81(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_tokenV1Compat_82(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_anyAtomicType_83(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_dayTimeDuration_84(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_untypedAtomicType_85(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_yearMonthDuration_86(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_tokenizedTypes_87(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_tokenizedTypesXsd_88(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_XdrTypes_89(),
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields::get_offset_of_c_XsdTypes_90(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (SchemaDatatypeMap_t9480326FA260852E226E2D1A881698CFBE35988E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1986[3] = 
{
	SchemaDatatypeMap_t9480326FA260852E226E2D1A881698CFBE35988E::get_offset_of_name_0(),
	SchemaDatatypeMap_t9480326FA260852E226E2D1A881698CFBE35988E::get_offset_of_type_1(),
	SchemaDatatypeMap_t9480326FA260852E226E2D1A881698CFBE35988E::get_offset_of_parentIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (Datatype_List_tD4CB4D33323B0D332A6C1990C4F1A891BBCB57DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1987[2] = 
{
	Datatype_List_tD4CB4D33323B0D332A6C1990C4F1A891BBCB57DF::get_offset_of_itemType_93(),
	Datatype_List_tD4CB4D33323B0D332A6C1990C4F1A891BBCB57DF::get_offset_of_minListSize_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (Datatype_union_tE2A2CBDE037535DC3CCE37411750AC4EF2009F40), -1, sizeof(Datatype_union_tE2A2CBDE037535DC3CCE37411750AC4EF2009F40_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1988[3] = 
{
	Datatype_union_tE2A2CBDE037535DC3CCE37411750AC4EF2009F40_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_union_tE2A2CBDE037535DC3CCE37411750AC4EF2009F40_StaticFields::get_offset_of_listValueType_94(),
	Datatype_union_tE2A2CBDE037535DC3CCE37411750AC4EF2009F40::get_offset_of_types_95(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232), -1, sizeof(Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1989[2] = 
{
	Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232_StaticFields::get_offset_of_atomicValueType_91(),
	Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232_StaticFields::get_offset_of_listValueType_92(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (Datatype_anyAtomicType_tD9993791C08ACD9B7F9AB91F05DBFF2B47C08142), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (Datatype_untypedAtomicType_tFB0567D465A1F4D07CD22818D93C0B56E2FE3EDD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (Datatype_string_t99A2A08E58CE5D513A0C1BB3C4E9E06EE80935E2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (Datatype_boolean_tB1805FB62F6452B8D69E9840D65139869BF76B62), -1, sizeof(Datatype_boolean_tB1805FB62F6452B8D69E9840D65139869BF76B62_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1993[2] = 
{
	Datatype_boolean_tB1805FB62F6452B8D69E9840D65139869BF76B62_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_boolean_tB1805FB62F6452B8D69E9840D65139869BF76B62_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (Datatype_float_t368DEB66F38BC7C4420BEFD96E4E4396C690DC09), -1, sizeof(Datatype_float_t368DEB66F38BC7C4420BEFD96E4E4396C690DC09_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1994[2] = 
{
	Datatype_float_t368DEB66F38BC7C4420BEFD96E4E4396C690DC09_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_float_t368DEB66F38BC7C4420BEFD96E4E4396C690DC09_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (Datatype_double_tD5564072018557D0CCFA5E2BFC9902E197F221F1), -1, sizeof(Datatype_double_tD5564072018557D0CCFA5E2BFC9902E197F221F1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1995[2] = 
{
	Datatype_double_tD5564072018557D0CCFA5E2BFC9902E197F221F1_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_double_tD5564072018557D0CCFA5E2BFC9902E197F221F1_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60), -1, sizeof(Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1996[3] = 
{
	Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60_StaticFields::get_offset_of_listValueType_94(),
	Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60_StaticFields::get_offset_of_numeric10FacetsChecker_95(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (Datatype_duration_t48CF7AD2DBEE484788997789311EACD432CCAC55), -1, sizeof(Datatype_duration_t48CF7AD2DBEE484788997789311EACD432CCAC55_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1997[2] = 
{
	Datatype_duration_t48CF7AD2DBEE484788997789311EACD432CCAC55_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_duration_t48CF7AD2DBEE484788997789311EACD432CCAC55_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (Datatype_yearMonthDuration_t48DB455A054A48BA0C8F4E3553B567F6DC599F3E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (Datatype_dayTimeDuration_tEFDAA51793B8ECAC56F6A769E15660728A220BAB), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
