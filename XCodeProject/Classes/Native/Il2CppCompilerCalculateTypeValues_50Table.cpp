﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Newtonsoft.Json.IArrayPool`1<System.Char>
struct IArrayPool_1_t5F933E1B7023F640EFFC95420D62CECD45A9A613;
// Newtonsoft.Json.JsonConverterCollection
struct JsonConverterCollection_tE01B17B4897D2254EC50F208B6AA43A938298ABF;
// Newtonsoft.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_tAA60D6EAFB1C28A616AE27F13A62595380353D8F;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_t2047669577EF6EFE700D5900AEEB4267BEE1861F;
// Newtonsoft.Json.Serialization.ITraceWriter
struct ITraceWriter_tB41E33E7400454A55DD6062387CF209637B08028;
// Newtonsoft.Json.Serialization.NamingStrategy
struct NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8;
// Newtonsoft.Json.Utilities.Base64Encoder
struct Base64Encoder_tA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835;
// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>
struct MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC;
// Newtonsoft.Json.Utilities.PropertyNameTable
struct PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028;
// Newtonsoft.Json.Utilities.PropertyNameTable/Entry[]
struct EntryU5BU5D_t8D7537231C709DEDF6E76ADEAE2A2CF968EBF615;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>>
struct ThreadSafeStore_2_tBDD7E3D5907C0A9F1415D71745FA342D833AB4B4;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>>
struct ThreadSafeStore_2_tC0CF55F30FBC93B3CBB6654FC7A4BD184EFD2520;
// Newtonsoft.Json.Utilities.TypeInformation[]
struct TypeInformationU5BU5D_tC1E474EF28F0AB600C6700585FA3D2E7C4A308C9;
// System.Action`2<System.Object,System.Object>
struct Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct Dictionary_2_t5063A2A0A164411F24A0CC253E3109FC1677A502;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember>
struct IDictionary_2_t135174E157E5A7C8792EDC8C4F16D2FC252E1F44;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>
struct IList_1_t22980F2073712C9CAEF3E897114A94AB79C54495;
// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>
struct List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t3102D0F5BABD60224F6DFF4815BCA1045831FB7C;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_tD0863E67C9A50D35387D2C3E7DF644C8E96F5B9A;
// System.Func`1<Newtonsoft.Json.JsonSerializerSettings>
struct Func_1_t4E1225B63054F96B0D656E7AA959D9DFAFD719D8;
// System.Func`1<Newtonsoft.Json.Serialization.IReferenceResolver>
struct Func_1_t08DED721C4774B7A9EB8B6792D1895E5E0B7A64A;
// System.Func`1<System.Object>
struct Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386;
// System.Func`2<System.Object,System.Object>
struct Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D;
// System.Func`2<System.Runtime.Serialization.EnumMemberAttribute,System.String>
struct Func_2_tF9BA149EA0568200F919F620EF33F7AA546E3B56;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IO.TextReader
struct TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A;
// System.IO.TextWriter
struct TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF;
// System.Reflection.MethodBase
struct MethodBase_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef JSONCONVERT_TACC7285CFAFB2143F710970F90E3C520D3C19A17_H
#define JSONCONVERT_TACC7285CFAFB2143F710970F90E3C520D3C19A17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConvert
struct  JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17  : public RuntimeObject
{
public:

public:
};

struct JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields
{
public:
	// System.Func`1<Newtonsoft.Json.JsonSerializerSettings> Newtonsoft.Json.JsonConvert::<DefaultSettings>k__BackingField
	Func_1_t4E1225B63054F96B0D656E7AA959D9DFAFD719D8 * ___U3CDefaultSettingsU3Ek__BackingField_0;
	// System.String Newtonsoft.Json.JsonConvert::True
	String_t* ___True_1;
	// System.String Newtonsoft.Json.JsonConvert::False
	String_t* ___False_2;
	// System.String Newtonsoft.Json.JsonConvert::Null
	String_t* ___Null_3;
	// System.String Newtonsoft.Json.JsonConvert::Undefined
	String_t* ___Undefined_4;
	// System.String Newtonsoft.Json.JsonConvert::PositiveInfinity
	String_t* ___PositiveInfinity_5;
	// System.String Newtonsoft.Json.JsonConvert::NegativeInfinity
	String_t* ___NegativeInfinity_6;
	// System.String Newtonsoft.Json.JsonConvert::NaN
	String_t* ___NaN_7;

public:
	inline static int32_t get_offset_of_U3CDefaultSettingsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields, ___U3CDefaultSettingsU3Ek__BackingField_0)); }
	inline Func_1_t4E1225B63054F96B0D656E7AA959D9DFAFD719D8 * get_U3CDefaultSettingsU3Ek__BackingField_0() const { return ___U3CDefaultSettingsU3Ek__BackingField_0; }
	inline Func_1_t4E1225B63054F96B0D656E7AA959D9DFAFD719D8 ** get_address_of_U3CDefaultSettingsU3Ek__BackingField_0() { return &___U3CDefaultSettingsU3Ek__BackingField_0; }
	inline void set_U3CDefaultSettingsU3Ek__BackingField_0(Func_1_t4E1225B63054F96B0D656E7AA959D9DFAFD719D8 * value)
	{
		___U3CDefaultSettingsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultSettingsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_True_1() { return static_cast<int32_t>(offsetof(JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields, ___True_1)); }
	inline String_t* get_True_1() const { return ___True_1; }
	inline String_t** get_address_of_True_1() { return &___True_1; }
	inline void set_True_1(String_t* value)
	{
		___True_1 = value;
		Il2CppCodeGenWriteBarrier((&___True_1), value);
	}

	inline static int32_t get_offset_of_False_2() { return static_cast<int32_t>(offsetof(JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields, ___False_2)); }
	inline String_t* get_False_2() const { return ___False_2; }
	inline String_t** get_address_of_False_2() { return &___False_2; }
	inline void set_False_2(String_t* value)
	{
		___False_2 = value;
		Il2CppCodeGenWriteBarrier((&___False_2), value);
	}

	inline static int32_t get_offset_of_Null_3() { return static_cast<int32_t>(offsetof(JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields, ___Null_3)); }
	inline String_t* get_Null_3() const { return ___Null_3; }
	inline String_t** get_address_of_Null_3() { return &___Null_3; }
	inline void set_Null_3(String_t* value)
	{
		___Null_3 = value;
		Il2CppCodeGenWriteBarrier((&___Null_3), value);
	}

	inline static int32_t get_offset_of_Undefined_4() { return static_cast<int32_t>(offsetof(JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields, ___Undefined_4)); }
	inline String_t* get_Undefined_4() const { return ___Undefined_4; }
	inline String_t** get_address_of_Undefined_4() { return &___Undefined_4; }
	inline void set_Undefined_4(String_t* value)
	{
		___Undefined_4 = value;
		Il2CppCodeGenWriteBarrier((&___Undefined_4), value);
	}

	inline static int32_t get_offset_of_PositiveInfinity_5() { return static_cast<int32_t>(offsetof(JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields, ___PositiveInfinity_5)); }
	inline String_t* get_PositiveInfinity_5() const { return ___PositiveInfinity_5; }
	inline String_t** get_address_of_PositiveInfinity_5() { return &___PositiveInfinity_5; }
	inline void set_PositiveInfinity_5(String_t* value)
	{
		___PositiveInfinity_5 = value;
		Il2CppCodeGenWriteBarrier((&___PositiveInfinity_5), value);
	}

	inline static int32_t get_offset_of_NegativeInfinity_6() { return static_cast<int32_t>(offsetof(JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields, ___NegativeInfinity_6)); }
	inline String_t* get_NegativeInfinity_6() const { return ___NegativeInfinity_6; }
	inline String_t** get_address_of_NegativeInfinity_6() { return &___NegativeInfinity_6; }
	inline void set_NegativeInfinity_6(String_t* value)
	{
		___NegativeInfinity_6 = value;
		Il2CppCodeGenWriteBarrier((&___NegativeInfinity_6), value);
	}

	inline static int32_t get_offset_of_NaN_7() { return static_cast<int32_t>(offsetof(JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields, ___NaN_7)); }
	inline String_t* get_NaN_7() const { return ___NaN_7; }
	inline String_t** get_address_of_NaN_7() { return &___NaN_7; }
	inline void set_NaN_7(String_t* value)
	{
		___NaN_7 = value;
		Il2CppCodeGenWriteBarrier((&___NaN_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERT_TACC7285CFAFB2143F710970F90E3C520D3C19A17_H
#ifndef JSONCONVERTER_T1CC2EC16B3208095A97A3CAE15286A5E16C3DD59_H
#define JSONCONVERTER_T1CC2EC16B3208095A97A3CAE15286A5E16C3DD59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverter
struct  JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTER_T1CC2EC16B3208095A97A3CAE15286A5E16C3DD59_H
#ifndef BASE64ENCODER_TA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835_H
#define BASE64ENCODER_TA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.Base64Encoder
struct  Base64Encoder_tA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835  : public RuntimeObject
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.Base64Encoder::_charsLine
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____charsLine_0;
	// System.IO.TextWriter Newtonsoft.Json.Utilities.Base64Encoder::_writer
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ____writer_1;
	// System.Byte[] Newtonsoft.Json.Utilities.Base64Encoder::_leftOverBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____leftOverBytes_2;
	// System.Int32 Newtonsoft.Json.Utilities.Base64Encoder::_leftOverBytesCount
	int32_t ____leftOverBytesCount_3;

public:
	inline static int32_t get_offset_of__charsLine_0() { return static_cast<int32_t>(offsetof(Base64Encoder_tA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835, ____charsLine_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__charsLine_0() const { return ____charsLine_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__charsLine_0() { return &____charsLine_0; }
	inline void set__charsLine_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____charsLine_0 = value;
		Il2CppCodeGenWriteBarrier((&____charsLine_0), value);
	}

	inline static int32_t get_offset_of__writer_1() { return static_cast<int32_t>(offsetof(Base64Encoder_tA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835, ____writer_1)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get__writer_1() const { return ____writer_1; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of__writer_1() { return &____writer_1; }
	inline void set__writer_1(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		____writer_1 = value;
		Il2CppCodeGenWriteBarrier((&____writer_1), value);
	}

	inline static int32_t get_offset_of__leftOverBytes_2() { return static_cast<int32_t>(offsetof(Base64Encoder_tA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835, ____leftOverBytes_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__leftOverBytes_2() const { return ____leftOverBytes_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__leftOverBytes_2() { return &____leftOverBytes_2; }
	inline void set__leftOverBytes_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____leftOverBytes_2 = value;
		Il2CppCodeGenWriteBarrier((&____leftOverBytes_2), value);
	}

	inline static int32_t get_offset_of__leftOverBytesCount_3() { return static_cast<int32_t>(offsetof(Base64Encoder_tA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835, ____leftOverBytesCount_3)); }
	inline int32_t get__leftOverBytesCount_3() const { return ____leftOverBytesCount_3; }
	inline int32_t* get_address_of__leftOverBytesCount_3() { return &____leftOverBytesCount_3; }
	inline void set__leftOverBytesCount_3(int32_t value)
	{
		____leftOverBytesCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE64ENCODER_TA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835_H
#ifndef BUFFERUTILS_TD68481564DC300E3C048C5A6234836BCD4386913_H
#define BUFFERUTILS_TD68481564DC300E3C048C5A6234836BCD4386913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.BufferUtils
struct  BufferUtils_tD68481564DC300E3C048C5A6234836BCD4386913  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERUTILS_TD68481564DC300E3C048C5A6234836BCD4386913_H
#ifndef COLLECTIONUTILS_TC862666093F54FDBF00D5FF1361500B0557A2D30_H
#define COLLECTIONUTILS_TC862666093F54FDBF00D5FF1361500B0557A2D30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.CollectionUtils
struct  CollectionUtils_tC862666093F54FDBF00D5FF1361500B0557A2D30  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONUTILS_TC862666093F54FDBF00D5FF1361500B0557A2D30_H
#ifndef CONVERTUTILS_T862E9BF443A5CAF9A0C47C1021A0EE43691CA016_H
#define CONVERTUTILS_T862E9BF443A5CAF9A0C47C1021A0EE43691CA016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils
struct  ConvertUtils_t862E9BF443A5CAF9A0C47C1021A0EE43691CA016  : public RuntimeObject
{
public:

public:
};

struct ConvertUtils_t862E9BF443A5CAF9A0C47C1021A0EE43691CA016_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode> Newtonsoft.Json.Utilities.ConvertUtils::TypeCodeMap
	Dictionary_2_t5063A2A0A164411F24A0CC253E3109FC1677A502 * ___TypeCodeMap_0;
	// Newtonsoft.Json.Utilities.TypeInformation[] Newtonsoft.Json.Utilities.ConvertUtils::PrimitiveTypeCodes
	TypeInformationU5BU5D_tC1E474EF28F0AB600C6700585FA3D2E7C4A308C9* ___PrimitiveTypeCodes_1;
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>> Newtonsoft.Json.Utilities.ConvertUtils::CastConverters
	ThreadSafeStore_2_tBDD7E3D5907C0A9F1415D71745FA342D833AB4B4 * ___CastConverters_2;

public:
	inline static int32_t get_offset_of_TypeCodeMap_0() { return static_cast<int32_t>(offsetof(ConvertUtils_t862E9BF443A5CAF9A0C47C1021A0EE43691CA016_StaticFields, ___TypeCodeMap_0)); }
	inline Dictionary_2_t5063A2A0A164411F24A0CC253E3109FC1677A502 * get_TypeCodeMap_0() const { return ___TypeCodeMap_0; }
	inline Dictionary_2_t5063A2A0A164411F24A0CC253E3109FC1677A502 ** get_address_of_TypeCodeMap_0() { return &___TypeCodeMap_0; }
	inline void set_TypeCodeMap_0(Dictionary_2_t5063A2A0A164411F24A0CC253E3109FC1677A502 * value)
	{
		___TypeCodeMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___TypeCodeMap_0), value);
	}

	inline static int32_t get_offset_of_PrimitiveTypeCodes_1() { return static_cast<int32_t>(offsetof(ConvertUtils_t862E9BF443A5CAF9A0C47C1021A0EE43691CA016_StaticFields, ___PrimitiveTypeCodes_1)); }
	inline TypeInformationU5BU5D_tC1E474EF28F0AB600C6700585FA3D2E7C4A308C9* get_PrimitiveTypeCodes_1() const { return ___PrimitiveTypeCodes_1; }
	inline TypeInformationU5BU5D_tC1E474EF28F0AB600C6700585FA3D2E7C4A308C9** get_address_of_PrimitiveTypeCodes_1() { return &___PrimitiveTypeCodes_1; }
	inline void set_PrimitiveTypeCodes_1(TypeInformationU5BU5D_tC1E474EF28F0AB600C6700585FA3D2E7C4A308C9* value)
	{
		___PrimitiveTypeCodes_1 = value;
		Il2CppCodeGenWriteBarrier((&___PrimitiveTypeCodes_1), value);
	}

	inline static int32_t get_offset_of_CastConverters_2() { return static_cast<int32_t>(offsetof(ConvertUtils_t862E9BF443A5CAF9A0C47C1021A0EE43691CA016_StaticFields, ___CastConverters_2)); }
	inline ThreadSafeStore_2_tBDD7E3D5907C0A9F1415D71745FA342D833AB4B4 * get_CastConverters_2() const { return ___CastConverters_2; }
	inline ThreadSafeStore_2_tBDD7E3D5907C0A9F1415D71745FA342D833AB4B4 ** get_address_of_CastConverters_2() { return &___CastConverters_2; }
	inline void set_CastConverters_2(ThreadSafeStore_2_tBDD7E3D5907C0A9F1415D71745FA342D833AB4B4 * value)
	{
		___CastConverters_2 = value;
		Il2CppCodeGenWriteBarrier((&___CastConverters_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTUTILS_T862E9BF443A5CAF9A0C47C1021A0EE43691CA016_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_T7E458B20E211215AF4007BFE5894BF3CFDE2A0E1_H
#define U3CU3EC__DISPLAYCLASS9_0_T7E458B20E211215AF4007BFE5894BF3CFDE2A0E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils/<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t7E458B20E211215AF4007BFE5894BF3CFDE2A0E1  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ConvertUtils/<>c__DisplayClass9_0::call
	MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t7E458B20E211215AF4007BFE5894BF3CFDE2A0E1, ___call_0)); }
	inline MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC * get_call_0() const { return ___call_0; }
	inline MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_T7E458B20E211215AF4007BFE5894BF3CFDE2A0E1_H
#ifndef DATETIMEUTILS_T0CC5C5DD3CA0C68FD0AA0F44D529127E80444853_H
#define DATETIMEUTILS_T0CC5C5DD3CA0C68FD0AA0F44D529127E80444853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.DateTimeUtils
struct  DateTimeUtils_t0CC5C5DD3CA0C68FD0AA0F44D529127E80444853  : public RuntimeObject
{
public:

public:
};

struct DateTimeUtils_t0CC5C5DD3CA0C68FD0AA0F44D529127E80444853_StaticFields
{
public:
	// System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::InitialJavaScriptDateTicks
	int64_t ___InitialJavaScriptDateTicks_0;
	// System.Int32[] Newtonsoft.Json.Utilities.DateTimeUtils::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_1;
	// System.Int32[] Newtonsoft.Json.Utilities.DateTimeUtils::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_2;

public:
	inline static int32_t get_offset_of_InitialJavaScriptDateTicks_0() { return static_cast<int32_t>(offsetof(DateTimeUtils_t0CC5C5DD3CA0C68FD0AA0F44D529127E80444853_StaticFields, ___InitialJavaScriptDateTicks_0)); }
	inline int64_t get_InitialJavaScriptDateTicks_0() const { return ___InitialJavaScriptDateTicks_0; }
	inline int64_t* get_address_of_InitialJavaScriptDateTicks_0() { return &___InitialJavaScriptDateTicks_0; }
	inline void set_InitialJavaScriptDateTicks_0(int64_t value)
	{
		___InitialJavaScriptDateTicks_0 = value;
	}

	inline static int32_t get_offset_of_DaysToMonth365_1() { return static_cast<int32_t>(offsetof(DateTimeUtils_t0CC5C5DD3CA0C68FD0AA0F44D529127E80444853_StaticFields, ___DaysToMonth365_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_1() const { return ___DaysToMonth365_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_1() { return &___DaysToMonth365_1; }
	inline void set_DaysToMonth365_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_1 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_1), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_2() { return static_cast<int32_t>(offsetof(DateTimeUtils_t0CC5C5DD3CA0C68FD0AA0F44D529127E80444853_StaticFields, ___DaysToMonth366_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_2() const { return ___DaysToMonth366_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_2() { return &___DaysToMonth366_2; }
	inline void set_DaysToMonth366_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_2 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEUTILS_T0CC5C5DD3CA0C68FD0AA0F44D529127E80444853_H
#ifndef ENUMUTILS_TE8C1135B9B3146B1CB38890F204B5DA062F9E547_H
#define ENUMUTILS_TE8C1135B9B3146B1CB38890F204B5DA062F9E547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.EnumUtils
struct  EnumUtils_tE8C1135B9B3146B1CB38890F204B5DA062F9E547  : public RuntimeObject
{
public:

public:
};

struct EnumUtils_tE8C1135B9B3146B1CB38890F204B5DA062F9E547_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>> Newtonsoft.Json.Utilities.EnumUtils::EnumMemberNamesPerType
	ThreadSafeStore_2_tC0CF55F30FBC93B3CBB6654FC7A4BD184EFD2520 * ___EnumMemberNamesPerType_0;

public:
	inline static int32_t get_offset_of_EnumMemberNamesPerType_0() { return static_cast<int32_t>(offsetof(EnumUtils_tE8C1135B9B3146B1CB38890F204B5DA062F9E547_StaticFields, ___EnumMemberNamesPerType_0)); }
	inline ThreadSafeStore_2_tC0CF55F30FBC93B3CBB6654FC7A4BD184EFD2520 * get_EnumMemberNamesPerType_0() const { return ___EnumMemberNamesPerType_0; }
	inline ThreadSafeStore_2_tC0CF55F30FBC93B3CBB6654FC7A4BD184EFD2520 ** get_address_of_EnumMemberNamesPerType_0() { return &___EnumMemberNamesPerType_0; }
	inline void set_EnumMemberNamesPerType_0(ThreadSafeStore_2_tC0CF55F30FBC93B3CBB6654FC7A4BD184EFD2520 * value)
	{
		___EnumMemberNamesPerType_0 = value;
		Il2CppCodeGenWriteBarrier((&___EnumMemberNamesPerType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMUTILS_TE8C1135B9B3146B1CB38890F204B5DA062F9E547_H
#ifndef U3CU3EC_TCC47B1EBC33F3C0DA87C84C3039915C339AF6E07_H
#define U3CU3EC_TCC47B1EBC33F3C0DA87C84C3039915C339AF6E07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.EnumUtils/<>c
struct  U3CU3Ec_tCC47B1EBC33F3C0DA87C84C3039915C339AF6E07  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tCC47B1EBC33F3C0DA87C84C3039915C339AF6E07_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.EnumUtils/<>c Newtonsoft.Json.Utilities.EnumUtils/<>c::<>9
	U3CU3Ec_tCC47B1EBC33F3C0DA87C84C3039915C339AF6E07 * ___U3CU3E9_0;
	// System.Func`2<System.Runtime.Serialization.EnumMemberAttribute,System.String> Newtonsoft.Json.Utilities.EnumUtils/<>c::<>9__1_0
	Func_2_tF9BA149EA0568200F919F620EF33F7AA546E3B56 * ___U3CU3E9__1_0_1;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> Newtonsoft.Json.Utilities.EnumUtils/<>c::<>9__5_0
	Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * ___U3CU3E9__5_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCC47B1EBC33F3C0DA87C84C3039915C339AF6E07_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tCC47B1EBC33F3C0DA87C84C3039915C339AF6E07 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tCC47B1EBC33F3C0DA87C84C3039915C339AF6E07 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tCC47B1EBC33F3C0DA87C84C3039915C339AF6E07 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCC47B1EBC33F3C0DA87C84C3039915C339AF6E07_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_tF9BA149EA0568200F919F620EF33F7AA546E3B56 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_tF9BA149EA0568200F919F620EF33F7AA546E3B56 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_tF9BA149EA0568200F919F620EF33F7AA546E3B56 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCC47B1EBC33F3C0DA87C84C3039915C339AF6E07_StaticFields, ___U3CU3E9__5_0_2)); }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * get_U3CU3E9__5_0_2() const { return ___U3CU3E9__5_0_2; }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D ** get_address_of_U3CU3E9__5_0_2() { return &___U3CU3E9__5_0_2; }
	inline void set_U3CU3E9__5_0_2(Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * value)
	{
		___U3CU3E9__5_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TCC47B1EBC33F3C0DA87C84C3039915C339AF6E07_H
#ifndef ILGENERATOREXTENSIONS_TF3030321DC784510B858F8711835EBDACCF542E7_H
#define ILGENERATOREXTENSIONS_TF3030321DC784510B858F8711835EBDACCF542E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ILGeneratorExtensions
struct  ILGeneratorExtensions_tF3030321DC784510B858F8711835EBDACCF542E7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ILGENERATOREXTENSIONS_TF3030321DC784510B858F8711835EBDACCF542E7_H
#ifndef JAVASCRIPTUTILS_T5B0D5EA3EAA2081A0A175539B2F320924F274084_H
#define JAVASCRIPTUTILS_T5B0D5EA3EAA2081A0A175539B2F320924F274084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.JavaScriptUtils
struct  JavaScriptUtils_t5B0D5EA3EAA2081A0A175539B2F320924F274084  : public RuntimeObject
{
public:

public:
};

struct JavaScriptUtils_t5B0D5EA3EAA2081A0A175539B2F320924F274084_StaticFields
{
public:
	// System.Boolean[] Newtonsoft.Json.Utilities.JavaScriptUtils::SingleQuoteCharEscapeFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___SingleQuoteCharEscapeFlags_0;
	// System.Boolean[] Newtonsoft.Json.Utilities.JavaScriptUtils::DoubleQuoteCharEscapeFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___DoubleQuoteCharEscapeFlags_1;
	// System.Boolean[] Newtonsoft.Json.Utilities.JavaScriptUtils::HtmlCharEscapeFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___HtmlCharEscapeFlags_2;

public:
	inline static int32_t get_offset_of_SingleQuoteCharEscapeFlags_0() { return static_cast<int32_t>(offsetof(JavaScriptUtils_t5B0D5EA3EAA2081A0A175539B2F320924F274084_StaticFields, ___SingleQuoteCharEscapeFlags_0)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_SingleQuoteCharEscapeFlags_0() const { return ___SingleQuoteCharEscapeFlags_0; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_SingleQuoteCharEscapeFlags_0() { return &___SingleQuoteCharEscapeFlags_0; }
	inline void set_SingleQuoteCharEscapeFlags_0(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___SingleQuoteCharEscapeFlags_0 = value;
		Il2CppCodeGenWriteBarrier((&___SingleQuoteCharEscapeFlags_0), value);
	}

	inline static int32_t get_offset_of_DoubleQuoteCharEscapeFlags_1() { return static_cast<int32_t>(offsetof(JavaScriptUtils_t5B0D5EA3EAA2081A0A175539B2F320924F274084_StaticFields, ___DoubleQuoteCharEscapeFlags_1)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_DoubleQuoteCharEscapeFlags_1() const { return ___DoubleQuoteCharEscapeFlags_1; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_DoubleQuoteCharEscapeFlags_1() { return &___DoubleQuoteCharEscapeFlags_1; }
	inline void set_DoubleQuoteCharEscapeFlags_1(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___DoubleQuoteCharEscapeFlags_1 = value;
		Il2CppCodeGenWriteBarrier((&___DoubleQuoteCharEscapeFlags_1), value);
	}

	inline static int32_t get_offset_of_HtmlCharEscapeFlags_2() { return static_cast<int32_t>(offsetof(JavaScriptUtils_t5B0D5EA3EAA2081A0A175539B2F320924F274084_StaticFields, ___HtmlCharEscapeFlags_2)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_HtmlCharEscapeFlags_2() const { return ___HtmlCharEscapeFlags_2; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_HtmlCharEscapeFlags_2() { return &___HtmlCharEscapeFlags_2; }
	inline void set_HtmlCharEscapeFlags_2(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___HtmlCharEscapeFlags_2 = value;
		Il2CppCodeGenWriteBarrier((&___HtmlCharEscapeFlags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JAVASCRIPTUTILS_T5B0D5EA3EAA2081A0A175539B2F320924F274084_H
#ifndef JSONTOKENUTILS_TE509D495457404BEA1F30127F239E6FDACFF270E_H
#define JSONTOKENUTILS_TE509D495457404BEA1F30127F239E6FDACFF270E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.JsonTokenUtils
struct  JsonTokenUtils_tE509D495457404BEA1F30127F239E6FDACFF270E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKENUTILS_TE509D495457404BEA1F30127F239E6FDACFF270E_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T17630845F0BEDD186940FEC2970757DEB8D1C699_H
#define U3CU3EC__DISPLAYCLASS3_0_T17630845F0BEDD186940FEC2970757DEB8D1C699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t17630845F0BEDD186940FEC2970757DEB8D1C699  : public RuntimeObject
{
public:
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass3_0::c
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ___c_0;
	// System.Reflection.MethodBase Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass3_0::method
	MethodBase_t * ___method_1;

public:
	inline static int32_t get_offset_of_c_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t17630845F0BEDD186940FEC2970757DEB8D1C699, ___c_0)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get_c_0() const { return ___c_0; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of_c_0() { return &___c_0; }
	inline void set_c_0(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		___c_0 = value;
		Il2CppCodeGenWriteBarrier((&___c_0), value);
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t17630845F0BEDD186940FEC2970757DEB8D1C699, ___method_1)); }
	inline MethodBase_t * get_method_1() const { return ___method_1; }
	inline MethodBase_t ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(MethodBase_t * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier((&___method_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T17630845F0BEDD186940FEC2970757DEB8D1C699_H
#ifndef MATHUTILS_T5642EFECB8D59A1BBFD6EAE6472F3B540F8ED42A_H
#define MATHUTILS_T5642EFECB8D59A1BBFD6EAE6472F3B540F8ED42A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.MathUtils
struct  MathUtils_t5642EFECB8D59A1BBFD6EAE6472F3B540F8ED42A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTILS_T5642EFECB8D59A1BBFD6EAE6472F3B540F8ED42A_H
#ifndef MISCELLANEOUSUTILS_T90451FF8022A7838FB44B1567D61CC4C880EA51B_H
#define MISCELLANEOUSUTILS_T90451FF8022A7838FB44B1567D61CC4C880EA51B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.MiscellaneousUtils
struct  MiscellaneousUtils_t90451FF8022A7838FB44B1567D61CC4C880EA51B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISCELLANEOUSUTILS_T90451FF8022A7838FB44B1567D61CC4C880EA51B_H
#ifndef PROPERTYNAMETABLE_TD2DF6B5B8271568C7CFA78F401DC06370B77B028_H
#define PROPERTYNAMETABLE_TD2DF6B5B8271568C7CFA78F401DC06370B77B028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PropertyNameTable
struct  PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable::_count
	int32_t ____count_1;
	// Newtonsoft.Json.Utilities.PropertyNameTable/Entry[] Newtonsoft.Json.Utilities.PropertyNameTable::_entries
	EntryU5BU5D_t8D7537231C709DEDF6E76ADEAE2A2CF968EBF615* ____entries_2;
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable::_mask
	int32_t ____mask_3;

public:
	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}

	inline static int32_t get_offset_of__entries_2() { return static_cast<int32_t>(offsetof(PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028, ____entries_2)); }
	inline EntryU5BU5D_t8D7537231C709DEDF6E76ADEAE2A2CF968EBF615* get__entries_2() const { return ____entries_2; }
	inline EntryU5BU5D_t8D7537231C709DEDF6E76ADEAE2A2CF968EBF615** get_address_of__entries_2() { return &____entries_2; }
	inline void set__entries_2(EntryU5BU5D_t8D7537231C709DEDF6E76ADEAE2A2CF968EBF615* value)
	{
		____entries_2 = value;
		Il2CppCodeGenWriteBarrier((&____entries_2), value);
	}

	inline static int32_t get_offset_of__mask_3() { return static_cast<int32_t>(offsetof(PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028, ____mask_3)); }
	inline int32_t get__mask_3() const { return ____mask_3; }
	inline int32_t* get_address_of__mask_3() { return &____mask_3; }
	inline void set__mask_3(int32_t value)
	{
		____mask_3 = value;
	}
};

struct PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028_StaticFields
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable::HashCodeRandomizer
	int32_t ___HashCodeRandomizer_0;

public:
	inline static int32_t get_offset_of_HashCodeRandomizer_0() { return static_cast<int32_t>(offsetof(PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028_StaticFields, ___HashCodeRandomizer_0)); }
	inline int32_t get_HashCodeRandomizer_0() const { return ___HashCodeRandomizer_0; }
	inline int32_t* get_address_of_HashCodeRandomizer_0() { return &___HashCodeRandomizer_0; }
	inline void set_HashCodeRandomizer_0(int32_t value)
	{
		___HashCodeRandomizer_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAMETABLE_TD2DF6B5B8271568C7CFA78F401DC06370B77B028_H
#ifndef ENTRY_T050E6E9796D2C75B3D6B3BF6012A70653E1FDB1A_H
#define ENTRY_T050E6E9796D2C75B3D6B3BF6012A70653E1FDB1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PropertyNameTable/Entry
struct  Entry_t050E6E9796D2C75B3D6B3BF6012A70653E1FDB1A  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Utilities.PropertyNameTable/Entry::Value
	String_t* ___Value_0;
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable/Entry::HashCode
	int32_t ___HashCode_1;
	// Newtonsoft.Json.Utilities.PropertyNameTable/Entry Newtonsoft.Json.Utilities.PropertyNameTable/Entry::Next
	Entry_t050E6E9796D2C75B3D6B3BF6012A70653E1FDB1A * ___Next_2;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(Entry_t050E6E9796D2C75B3D6B3BF6012A70653E1FDB1A, ___Value_0)); }
	inline String_t* get_Value_0() const { return ___Value_0; }
	inline String_t** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(String_t* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((&___Value_0), value);
	}

	inline static int32_t get_offset_of_HashCode_1() { return static_cast<int32_t>(offsetof(Entry_t050E6E9796D2C75B3D6B3BF6012A70653E1FDB1A, ___HashCode_1)); }
	inline int32_t get_HashCode_1() const { return ___HashCode_1; }
	inline int32_t* get_address_of_HashCode_1() { return &___HashCode_1; }
	inline void set_HashCode_1(int32_t value)
	{
		___HashCode_1 = value;
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(Entry_t050E6E9796D2C75B3D6B3BF6012A70653E1FDB1A, ___Next_2)); }
	inline Entry_t050E6E9796D2C75B3D6B3BF6012A70653E1FDB1A * get_Next_2() const { return ___Next_2; }
	inline Entry_t050E6E9796D2C75B3D6B3BF6012A70653E1FDB1A ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(Entry_t050E6E9796D2C75B3D6B3BF6012A70653E1FDB1A * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier((&___Next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T050E6E9796D2C75B3D6B3BF6012A70653E1FDB1A_H
#ifndef REFLECTIONDELEGATEFACTORY_T0DE9140A9473925387D2DEBE9B893930C95939D2_H
#define REFLECTIONDELEGATEFACTORY_T0DE9140A9473925387D2DEBE9B893930C95939D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionDelegateFactory
struct  ReflectionDelegateFactory_t0DE9140A9473925387D2DEBE9B893930C95939D2  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONDELEGATEFACTORY_T0DE9140A9473925387D2DEBE9B893930C95939D2_H
#ifndef REFLECTIONMEMBER_TF4529F7912052432E9C4B11481663F206EAA28B1_H
#define REFLECTIONMEMBER_TF4529F7912052432E9C4B11481663F206EAA28B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionMember
struct  ReflectionMember_tF4529F7912052432E9C4B11481663F206EAA28B1  : public RuntimeObject
{
public:
	// System.Type Newtonsoft.Json.Utilities.ReflectionMember::<MemberType>k__BackingField
	Type_t * ___U3CMemberTypeU3Ek__BackingField_0;
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionMember::<Getter>k__BackingField
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___U3CGetterU3Ek__BackingField_1;
	// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionMember::<Setter>k__BackingField
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___U3CSetterU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CMemberTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ReflectionMember_tF4529F7912052432E9C4B11481663F206EAA28B1, ___U3CMemberTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CMemberTypeU3Ek__BackingField_0() const { return ___U3CMemberTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CMemberTypeU3Ek__BackingField_0() { return &___U3CMemberTypeU3Ek__BackingField_0; }
	inline void set_U3CMemberTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CMemberTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CGetterU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReflectionMember_tF4529F7912052432E9C4B11481663F206EAA28B1, ___U3CGetterU3Ek__BackingField_1)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get_U3CGetterU3Ek__BackingField_1() const { return ___U3CGetterU3Ek__BackingField_1; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of_U3CGetterU3Ek__BackingField_1() { return &___U3CGetterU3Ek__BackingField_1; }
	inline void set_U3CGetterU3Ek__BackingField_1(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		___U3CGetterU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetterU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSetterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ReflectionMember_tF4529F7912052432E9C4B11481663F206EAA28B1, ___U3CSetterU3Ek__BackingField_2)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get_U3CSetterU3Ek__BackingField_2() const { return ___U3CSetterU3Ek__BackingField_2; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of_U3CSetterU3Ek__BackingField_2() { return &___U3CSetterU3Ek__BackingField_2; }
	inline void set_U3CSetterU3Ek__BackingField_2(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		___U3CSetterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSetterU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMEMBER_TF4529F7912052432E9C4B11481663F206EAA28B1_H
#ifndef REFLECTIONOBJECT_T986F1380251FCF1B016CA41A331CA95CD4606105_H
#define REFLECTIONOBJECT_T986F1380251FCF1B016CA41A331CA95CD4606105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject
struct  ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.ReflectionObject::<Creator>k__BackingField
	ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * ___U3CCreatorU3Ek__BackingField_0;
	// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember> Newtonsoft.Json.Utilities.ReflectionObject::<Members>k__BackingField
	RuntimeObject* ___U3CMembersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCreatorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105, ___U3CCreatorU3Ek__BackingField_0)); }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * get_U3CCreatorU3Ek__BackingField_0() const { return ___U3CCreatorU3Ek__BackingField_0; }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 ** get_address_of_U3CCreatorU3Ek__BackingField_0() { return &___U3CCreatorU3Ek__BackingField_0; }
	inline void set_U3CCreatorU3Ek__BackingField_0(ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * value)
	{
		___U3CCreatorU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCreatorU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CMembersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105, ___U3CMembersU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CMembersU3Ek__BackingField_1() const { return ___U3CMembersU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CMembersU3Ek__BackingField_1() { return &___U3CMembersU3Ek__BackingField_1; }
	inline void set_U3CMembersU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CMembersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMembersU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONOBJECT_T986F1380251FCF1B016CA41A331CA95CD4606105_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_TBB52F26120E9DBFA2F8C1A78549CD6D50318F5E4_H
#define U3CU3EC__DISPLAYCLASS13_0_TBB52F26120E9DBFA2F8C1A78549CD6D50318F5E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_tBB52F26120E9DBFA2F8C1A78549CD6D50318F5E4  : public RuntimeObject
{
public:
	// System.Func`1<System.Object> Newtonsoft.Json.Utilities.ReflectionObject/<>c__DisplayClass13_0::ctor
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___ctor_0;

public:
	inline static int32_t get_offset_of_ctor_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_tBB52F26120E9DBFA2F8C1A78549CD6D50318F5E4, ___ctor_0)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get_ctor_0() const { return ___ctor_0; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of_ctor_0() { return &___ctor_0; }
	inline void set_ctor_0(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		___ctor_0 = value;
		Il2CppCodeGenWriteBarrier((&___ctor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_TBB52F26120E9DBFA2F8C1A78549CD6D50318F5E4_H
#ifndef U3CU3EC__DISPLAYCLASS13_1_TBB787649C33BB0DB373D3E550A8AEBBBA4941F2F_H
#define U3CU3EC__DISPLAYCLASS13_1_TBB787649C33BB0DB373D3E550A8AEBBBA4941F2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject/<>c__DisplayClass13_1
struct  U3CU3Ec__DisplayClass13_1_tBB787649C33BB0DB373D3E550A8AEBBBA4941F2F  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionObject/<>c__DisplayClass13_1::call
	MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_1_tBB787649C33BB0DB373D3E550A8AEBBBA4941F2F, ___call_0)); }
	inline MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC * get_call_0() const { return ___call_0; }
	inline MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_1_TBB787649C33BB0DB373D3E550A8AEBBBA4941F2F_H
#ifndef U3CU3EC__DISPLAYCLASS13_2_T05D684DD57ADD903BED599BCB469E1DEF7E6B94C_H
#define U3CU3EC__DISPLAYCLASS13_2_T05D684DD57ADD903BED599BCB469E1DEF7E6B94C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject/<>c__DisplayClass13_2
struct  U3CU3Ec__DisplayClass13_2_t05D684DD57ADD903BED599BCB469E1DEF7E6B94C  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionObject/<>c__DisplayClass13_2::call
	MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_2_t05D684DD57ADD903BED599BCB469E1DEF7E6B94C, ___call_0)); }
	inline MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC * get_call_0() const { return ___call_0; }
	inline MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_2_T05D684DD57ADD903BED599BCB469E1DEF7E6B94C_H
#ifndef REFLECTIONUTILS_TF7E4E160A27E479C2C9CED8C169438E6B63A3E4F_H
#define REFLECTIONUTILS_TF7E4E160A27E479C2C9CED8C169438E6B63A3E4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils
struct  ReflectionUtils_tF7E4E160A27E479C2C9CED8C169438E6B63A3E4F  : public RuntimeObject
{
public:

public:
};

struct ReflectionUtils_tF7E4E160A27E479C2C9CED8C169438E6B63A3E4F_StaticFields
{
public:
	// System.Type[] Newtonsoft.Json.Utilities.ReflectionUtils::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_0;

public:
	inline static int32_t get_offset_of_EmptyTypes_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_tF7E4E160A27E479C2C9CED8C169438E6B63A3E4F_StaticFields, ___EmptyTypes_0)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_0() const { return ___EmptyTypes_0; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_0() { return &___EmptyTypes_0; }
	inline void set_EmptyTypes_0(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONUTILS_TF7E4E160A27E479C2C9CED8C169438E6B63A3E4F_H
#ifndef STRINGREFERENCEEXTENSIONS_T9D90E2A0A7D4B833A8F8B3C0220AC38CCB19B926_H
#define STRINGREFERENCEEXTENSIONS_T9D90E2A0A7D4B833A8F8B3C0220AC38CCB19B926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringReferenceExtensions
struct  StringReferenceExtensions_t9D90E2A0A7D4B833A8F8B3C0220AC38CCB19B926  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGREFERENCEEXTENSIONS_T9D90E2A0A7D4B833A8F8B3C0220AC38CCB19B926_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef COLLECTION_1_T9B877F2F1B59130CFE0CCA921F1B28F6B4CAF189_H
#define COLLECTION_1_T9B877F2F1B59130CFE0CCA921F1B28F6B4CAF189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonConverter>
struct  Collection_1_t9B877F2F1B59130CFE0CCA921F1B28F6B4CAF189  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::items
	RuntimeObject* ___items_0;
	// System.Object System.Collections.ObjectModel.Collection`1::_syncRoot
	RuntimeObject * ____syncRoot_1;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(Collection_1_t9B877F2F1B59130CFE0CCA921F1B28F6B4CAF189, ___items_0)); }
	inline RuntimeObject* get_items_0() const { return ___items_0; }
	inline RuntimeObject** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(RuntimeObject* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}

	inline static int32_t get_offset_of__syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_t9B877F2F1B59130CFE0CCA921F1B28F6B4CAF189, ____syncRoot_1)); }
	inline RuntimeObject * get__syncRoot_1() const { return ____syncRoot_1; }
	inline RuntimeObject ** get_address_of__syncRoot_1() { return &____syncRoot_1; }
	inline void set__syncRoot_1(RuntimeObject * value)
	{
		____syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_T9B877F2F1B59130CFE0CCA921F1B28F6B4CAF189_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef JSONCONSTRUCTORATTRIBUTE_T99AF2AA01C0D9986F79E8F10B8FF80E59566F535_H
#define JSONCONSTRUCTORATTRIBUTE_T99AF2AA01C0D9986F79E8F10B8FF80E59566F535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConstructorAttribute
struct  JsonConstructorAttribute_t99AF2AA01C0D9986F79E8F10B8FF80E59566F535  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONSTRUCTORATTRIBUTE_T99AF2AA01C0D9986F79E8F10B8FF80E59566F535_H
#ifndef JSONCONVERTERATTRIBUTE_T2D407785561C66A63162D4AFBD8C48450EF6934E_H
#define JSONCONVERTERATTRIBUTE_T2D407785561C66A63162D4AFBD8C48450EF6934E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverterAttribute
struct  JsonConverterAttribute_t2D407785561C66A63162D4AFBD8C48450EF6934E  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type Newtonsoft.Json.JsonConverterAttribute::_converterType
	Type_t * ____converterType_0;
	// System.Object[] Newtonsoft.Json.JsonConverterAttribute::<ConverterParameters>k__BackingField
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___U3CConverterParametersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of__converterType_0() { return static_cast<int32_t>(offsetof(JsonConverterAttribute_t2D407785561C66A63162D4AFBD8C48450EF6934E, ____converterType_0)); }
	inline Type_t * get__converterType_0() const { return ____converterType_0; }
	inline Type_t ** get_address_of__converterType_0() { return &____converterType_0; }
	inline void set__converterType_0(Type_t * value)
	{
		____converterType_0 = value;
		Il2CppCodeGenWriteBarrier((&____converterType_0), value);
	}

	inline static int32_t get_offset_of_U3CConverterParametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonConverterAttribute_t2D407785561C66A63162D4AFBD8C48450EF6934E, ___U3CConverterParametersU3Ek__BackingField_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_U3CConverterParametersU3Ek__BackingField_1() const { return ___U3CConverterParametersU3Ek__BackingField_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_U3CConverterParametersU3Ek__BackingField_1() { return &___U3CConverterParametersU3Ek__BackingField_1; }
	inline void set_U3CConverterParametersU3Ek__BackingField_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___U3CConverterParametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConverterParametersU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTERATTRIBUTE_T2D407785561C66A63162D4AFBD8C48450EF6934E_H
#ifndef JSONCONVERTERCOLLECTION_TE01B17B4897D2254EC50F208B6AA43A938298ABF_H
#define JSONCONVERTERCOLLECTION_TE01B17B4897D2254EC50F208B6AA43A938298ABF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverterCollection
struct  JsonConverterCollection_tE01B17B4897D2254EC50F208B6AA43A938298ABF  : public Collection_1_t9B877F2F1B59130CFE0CCA921F1B28F6B4CAF189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTERCOLLECTION_TE01B17B4897D2254EC50F208B6AA43A938298ABF_H
#ifndef JSONEXCEPTION_TE9DA91D672710AF3C23049EEC7872C5B4C9C89D8_H
#define JSONEXCEPTION_TE9DA91D672710AF3C23049EEC7872C5B4C9C89D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonException
struct  JsonException_tE9DA91D672710AF3C23049EEC7872C5B4C9C89D8  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONEXCEPTION_TE9DA91D672710AF3C23049EEC7872C5B4C9C89D8_H
#ifndef JSONEXTENSIONDATAATTRIBUTE_T172C2D68229E647E79DEECAF1DD994EE52A5B98C_H
#define JSONEXTENSIONDATAATTRIBUTE_T172C2D68229E647E79DEECAF1DD994EE52A5B98C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonExtensionDataAttribute
struct  JsonExtensionDataAttribute_t172C2D68229E647E79DEECAF1DD994EE52A5B98C  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Newtonsoft.Json.JsonExtensionDataAttribute::<WriteData>k__BackingField
	bool ___U3CWriteDataU3Ek__BackingField_0;
	// System.Boolean Newtonsoft.Json.JsonExtensionDataAttribute::<ReadData>k__BackingField
	bool ___U3CReadDataU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CWriteDataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonExtensionDataAttribute_t172C2D68229E647E79DEECAF1DD994EE52A5B98C, ___U3CWriteDataU3Ek__BackingField_0)); }
	inline bool get_U3CWriteDataU3Ek__BackingField_0() const { return ___U3CWriteDataU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CWriteDataU3Ek__BackingField_0() { return &___U3CWriteDataU3Ek__BackingField_0; }
	inline void set_U3CWriteDataU3Ek__BackingField_0(bool value)
	{
		___U3CWriteDataU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CReadDataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonExtensionDataAttribute_t172C2D68229E647E79DEECAF1DD994EE52A5B98C, ___U3CReadDataU3Ek__BackingField_1)); }
	inline bool get_U3CReadDataU3Ek__BackingField_1() const { return ___U3CReadDataU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CReadDataU3Ek__BackingField_1() { return &___U3CReadDataU3Ek__BackingField_1; }
	inline void set_U3CReadDataU3Ek__BackingField_1(bool value)
	{
		___U3CReadDataU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONEXTENSIONDATAATTRIBUTE_T172C2D68229E647E79DEECAF1DD994EE52A5B98C_H
#ifndef JSONIGNOREATTRIBUTE_T9B3B6F92369BC8EBEED668765C47AB795DC2F1DD_H
#define JSONIGNOREATTRIBUTE_T9B3B6F92369BC8EBEED668765C47AB795DC2F1DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonIgnoreAttribute
struct  JsonIgnoreAttribute_t9B3B6F92369BC8EBEED668765C47AB795DC2F1DD  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONIGNOREATTRIBUTE_T9B3B6F92369BC8EBEED668765C47AB795DC2F1DD_H
#ifndef JSONREQUIREDATTRIBUTE_TB3DCDC06F01C68AF2D2E8681949F39EE1B018CA0_H
#define JSONREQUIREDATTRIBUTE_TB3DCDC06F01C68AF2D2E8681949F39EE1B018CA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonRequiredAttribute
struct  JsonRequiredAttribute_tB3DCDC06F01C68AF2D2E8681949F39EE1B018CA0  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREQUIREDATTRIBUTE_TB3DCDC06F01C68AF2D2E8681949F39EE1B018CA0_H
#ifndef TYPECONVERTKEY_T0B516C7A7A5D68F04DE3346468A484166E3AAEC3_H
#define TYPECONVERTKEY_T0B516C7A7A5D68F04DE3346468A484166E3AAEC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct  TypeConvertKey_t0B516C7A7A5D68F04DE3346468A484166E3AAEC3 
{
public:
	// System.Type Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey::_initialType
	Type_t * ____initialType_0;
	// System.Type Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey::_targetType
	Type_t * ____targetType_1;

public:
	inline static int32_t get_offset_of__initialType_0() { return static_cast<int32_t>(offsetof(TypeConvertKey_t0B516C7A7A5D68F04DE3346468A484166E3AAEC3, ____initialType_0)); }
	inline Type_t * get__initialType_0() const { return ____initialType_0; }
	inline Type_t ** get_address_of__initialType_0() { return &____initialType_0; }
	inline void set__initialType_0(Type_t * value)
	{
		____initialType_0 = value;
		Il2CppCodeGenWriteBarrier((&____initialType_0), value);
	}

	inline static int32_t get_offset_of__targetType_1() { return static_cast<int32_t>(offsetof(TypeConvertKey_t0B516C7A7A5D68F04DE3346468A484166E3AAEC3, ____targetType_1)); }
	inline Type_t * get__targetType_1() const { return ____targetType_1; }
	inline Type_t ** get_address_of__targetType_1() { return &____targetType_1; }
	inline void set__targetType_1(Type_t * value)
	{
		____targetType_1 = value;
		Il2CppCodeGenWriteBarrier((&____targetType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t0B516C7A7A5D68F04DE3346468A484166E3AAEC3_marshaled_pinvoke
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t0B516C7A7A5D68F04DE3346468A484166E3AAEC3_marshaled_com
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
#endif // TYPECONVERTKEY_T0B516C7A7A5D68F04DE3346468A484166E3AAEC3_H
#ifndef DYNAMICREFLECTIONDELEGATEFACTORY_T64129C72C25891E1E9A8EF714B2768AE8534D176_H
#define DYNAMICREFLECTIONDELEGATEFACTORY_T64129C72C25891E1E9A8EF714B2768AE8534D176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory
struct  DynamicReflectionDelegateFactory_t64129C72C25891E1E9A8EF714B2768AE8534D176  : public ReflectionDelegateFactory_t0DE9140A9473925387D2DEBE9B893930C95939D2
{
public:

public:
};

struct DynamicReflectionDelegateFactory_t64129C72C25891E1E9A8EF714B2768AE8534D176_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::Instance
	DynamicReflectionDelegateFactory_t64129C72C25891E1E9A8EF714B2768AE8534D176 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(DynamicReflectionDelegateFactory_t64129C72C25891E1E9A8EF714B2768AE8534D176_StaticFields, ___Instance_0)); }
	inline DynamicReflectionDelegateFactory_t64129C72C25891E1E9A8EF714B2768AE8534D176 * get_Instance_0() const { return ___Instance_0; }
	inline DynamicReflectionDelegateFactory_t64129C72C25891E1E9A8EF714B2768AE8534D176 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(DynamicReflectionDelegateFactory_t64129C72C25891E1E9A8EF714B2768AE8534D176 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICREFLECTIONDELEGATEFACTORY_T64129C72C25891E1E9A8EF714B2768AE8534D176_H
#ifndef LATEBOUNDREFLECTIONDELEGATEFACTORY_T0C3812FD1D79FE26A409C8F5F9E9011BB87B14C2_H
#define LATEBOUNDREFLECTIONDELEGATEFACTORY_T0C3812FD1D79FE26A409C8F5F9E9011BB87B14C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory
struct  LateBoundReflectionDelegateFactory_t0C3812FD1D79FE26A409C8F5F9E9011BB87B14C2  : public ReflectionDelegateFactory_t0DE9140A9473925387D2DEBE9B893930C95939D2
{
public:

public:
};

struct LateBoundReflectionDelegateFactory_t0C3812FD1D79FE26A409C8F5F9E9011BB87B14C2_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::_instance
	LateBoundReflectionDelegateFactory_t0C3812FD1D79FE26A409C8F5F9E9011BB87B14C2 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(LateBoundReflectionDelegateFactory_t0C3812FD1D79FE26A409C8F5F9E9011BB87B14C2_StaticFields, ____instance_0)); }
	inline LateBoundReflectionDelegateFactory_t0C3812FD1D79FE26A409C8F5F9E9011BB87B14C2 * get__instance_0() const { return ____instance_0; }
	inline LateBoundReflectionDelegateFactory_t0C3812FD1D79FE26A409C8F5F9E9011BB87B14C2 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(LateBoundReflectionDelegateFactory_t0C3812FD1D79FE26A409C8F5F9E9011BB87B14C2 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATEBOUNDREFLECTIONDELEGATEFACTORY_T0C3812FD1D79FE26A409C8F5F9E9011BB87B14C2_H
#ifndef STRINGBUFFER_T3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89_H
#define STRINGBUFFER_T3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringBuffer
struct  StringBuffer_t3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89 
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.StringBuffer::_buffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____buffer_0;
	// System.Int32 Newtonsoft.Json.Utilities.StringBuffer::_position
	int32_t ____position_1;

public:
	inline static int32_t get_offset_of__buffer_0() { return static_cast<int32_t>(offsetof(StringBuffer_t3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89, ____buffer_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__buffer_0() const { return ____buffer_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__buffer_0() { return &____buffer_0; }
	inline void set__buffer_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_0), value);
	}

	inline static int32_t get_offset_of__position_1() { return static_cast<int32_t>(offsetof(StringBuffer_t3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89, ____position_1)); }
	inline int32_t get__position_1() const { return ____position_1; }
	inline int32_t* get_address_of__position_1() { return &____position_1; }
	inline void set__position_1(int32_t value)
	{
		____position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.StringBuffer
struct StringBuffer_t3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89_marshaled_pinvoke
{
	uint8_t* ____buffer_0;
	int32_t ____position_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.StringBuffer
struct StringBuffer_t3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89_marshaled_com
{
	uint8_t* ____buffer_0;
	int32_t ____position_1;
};
#endif // STRINGBUFFER_T3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89_H
#ifndef STRINGREFERENCE_T26CAF004EA4ED81837111868CA1FFE682E987DE1_H
#define STRINGREFERENCE_T26CAF004EA4ED81837111868CA1FFE682E987DE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringReference
struct  StringReference_t26CAF004EA4ED81837111868CA1FFE682E987DE1 
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.StringReference::_chars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____chars_0;
	// System.Int32 Newtonsoft.Json.Utilities.StringReference::_startIndex
	int32_t ____startIndex_1;
	// System.Int32 Newtonsoft.Json.Utilities.StringReference::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__chars_0() { return static_cast<int32_t>(offsetof(StringReference_t26CAF004EA4ED81837111868CA1FFE682E987DE1, ____chars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__chars_0() const { return ____chars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__chars_0() { return &____chars_0; }
	inline void set__chars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____chars_0 = value;
		Il2CppCodeGenWriteBarrier((&____chars_0), value);
	}

	inline static int32_t get_offset_of__startIndex_1() { return static_cast<int32_t>(offsetof(StringReference_t26CAF004EA4ED81837111868CA1FFE682E987DE1, ____startIndex_1)); }
	inline int32_t get__startIndex_1() const { return ____startIndex_1; }
	inline int32_t* get_address_of__startIndex_1() { return &____startIndex_1; }
	inline void set__startIndex_1(int32_t value)
	{
		____startIndex_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(StringReference_t26CAF004EA4ED81837111868CA1FFE682E987DE1, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.StringReference
struct StringReference_t26CAF004EA4ED81837111868CA1FFE682E987DE1_marshaled_pinvoke
{
	uint8_t* ____chars_0;
	int32_t ____startIndex_1;
	int32_t ____length_2;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.StringReference
struct StringReference_t26CAF004EA4ED81837111868CA1FFE682E987DE1_marshaled_com
{
	uint8_t* ____chars_0;
	int32_t ____startIndex_1;
	int32_t ____length_2;
};
#endif // STRINGREFERENCE_T26CAF004EA4ED81837111868CA1FFE682E987DE1_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#define NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifndef CONSTRUCTORHANDLING_T24063773444D2C4382EB581D086B8F078E533371_H
#define CONSTRUCTORHANDLING_T24063773444D2C4382EB581D086B8F078E533371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ConstructorHandling
struct  ConstructorHandling_t24063773444D2C4382EB581D086B8F078E533371 
{
public:
	// System.Int32 Newtonsoft.Json.ConstructorHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConstructorHandling_t24063773444D2C4382EB581D086B8F078E533371, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORHANDLING_T24063773444D2C4382EB581D086B8F078E533371_H
#ifndef DATEFORMATHANDLING_T0D54266C0C7CD81612959E7CB8174F6F8AA21424_H
#define DATEFORMATHANDLING_T0D54266C0C7CD81612959E7CB8174F6F8AA21424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateFormatHandling
struct  DateFormatHandling_t0D54266C0C7CD81612959E7CB8174F6F8AA21424 
{
public:
	// System.Int32 Newtonsoft.Json.DateFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateFormatHandling_t0D54266C0C7CD81612959E7CB8174F6F8AA21424, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEFORMATHANDLING_T0D54266C0C7CD81612959E7CB8174F6F8AA21424_H
#ifndef DATEPARSEHANDLING_T0428B8AFAEE2CBEF5C0E1D3CEE0F5F1123E1E22C_H
#define DATEPARSEHANDLING_T0428B8AFAEE2CBEF5C0E1D3CEE0F5F1123E1E22C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateParseHandling
struct  DateParseHandling_t0428B8AFAEE2CBEF5C0E1D3CEE0F5F1123E1E22C 
{
public:
	// System.Int32 Newtonsoft.Json.DateParseHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateParseHandling_t0428B8AFAEE2CBEF5C0E1D3CEE0F5F1123E1E22C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEPARSEHANDLING_T0428B8AFAEE2CBEF5C0E1D3CEE0F5F1123E1E22C_H
#ifndef DATETIMEZONEHANDLING_T4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD_H
#define DATETIMEZONEHANDLING_T4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateTimeZoneHandling
struct  DateTimeZoneHandling_t4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD 
{
public:
	// System.Int32 Newtonsoft.Json.DateTimeZoneHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeZoneHandling_t4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEZONEHANDLING_T4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD_H
#ifndef DEFAULTVALUEHANDLING_T85DC44F1FBDD9D31309A45A0334A4518B5C08696_H
#define DEFAULTVALUEHANDLING_T85DC44F1FBDD9D31309A45A0334A4518B5C08696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DefaultValueHandling
struct  DefaultValueHandling_t85DC44F1FBDD9D31309A45A0334A4518B5C08696 
{
public:
	// System.Int32 Newtonsoft.Json.DefaultValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DefaultValueHandling_t85DC44F1FBDD9D31309A45A0334A4518B5C08696, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEHANDLING_T85DC44F1FBDD9D31309A45A0334A4518B5C08696_H
#ifndef FLOATFORMATHANDLING_TE78C0C14E4CFE64BDF71370419573A3ED8B68289_H
#define FLOATFORMATHANDLING_TE78C0C14E4CFE64BDF71370419573A3ED8B68289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatFormatHandling
struct  FloatFormatHandling_tE78C0C14E4CFE64BDF71370419573A3ED8B68289 
{
public:
	// System.Int32 Newtonsoft.Json.FloatFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatFormatHandling_tE78C0C14E4CFE64BDF71370419573A3ED8B68289, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFORMATHANDLING_TE78C0C14E4CFE64BDF71370419573A3ED8B68289_H
#ifndef FLOATPARSEHANDLING_T4571C78B0F87871B93E071E07321989BEB7633B1_H
#define FLOATPARSEHANDLING_T4571C78B0F87871B93E071E07321989BEB7633B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatParseHandling
struct  FloatParseHandling_t4571C78B0F87871B93E071E07321989BEB7633B1 
{
public:
	// System.Int32 Newtonsoft.Json.FloatParseHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatParseHandling_t4571C78B0F87871B93E071E07321989BEB7633B1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPARSEHANDLING_T4571C78B0F87871B93E071E07321989BEB7633B1_H
#ifndef FORMATTING_TE2524DB9105FC27E1B956F5AF5F211981B22E3E1_H
#define FORMATTING_TE2524DB9105FC27E1B956F5AF5F211981B22E3E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Formatting
struct  Formatting_tE2524DB9105FC27E1B956F5AF5F211981B22E3E1 
{
public:
	// System.Int32 Newtonsoft.Json.Formatting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Formatting_tE2524DB9105FC27E1B956F5AF5F211981B22E3E1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_TE2524DB9105FC27E1B956F5AF5F211981B22E3E1_H
#ifndef JSONCONTAINERTYPE_T1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6_H
#define JSONCONTAINERTYPE_T1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerType
struct  JsonContainerType_t1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6 
{
public:
	// System.Int32 Newtonsoft.Json.JsonContainerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonContainerType_t1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERTYPE_T1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6_H
#ifndef STATE_TD13934C0FF1DA5189F8BD9DC33D77391AB371944_H
#define STATE_TD13934C0FF1DA5189F8BD9DC33D77391AB371944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader/State
struct  State_tD13934C0FF1DA5189F8BD9DC33D77391AB371944 
{
public:
	// System.Int32 Newtonsoft.Json.JsonReader/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_tD13934C0FF1DA5189F8BD9DC33D77391AB371944, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_TD13934C0FF1DA5189F8BD9DC33D77391AB371944_H
#ifndef JSONREADEREXCEPTION_T44F84BC4AF81F378D1901DEAD54371B9C55A9708_H
#define JSONREADEREXCEPTION_T44F84BC4AF81F378D1901DEAD54371B9C55A9708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReaderException
struct  JsonReaderException_t44F84BC4AF81F378D1901DEAD54371B9C55A9708  : public JsonException_tE9DA91D672710AF3C23049EEC7872C5B4C9C89D8
{
public:
	// System.Int32 Newtonsoft.Json.JsonReaderException::<LineNumber>k__BackingField
	int32_t ___U3CLineNumberU3Ek__BackingField_17;
	// System.Int32 Newtonsoft.Json.JsonReaderException::<LinePosition>k__BackingField
	int32_t ___U3CLinePositionU3Ek__BackingField_18;
	// System.String Newtonsoft.Json.JsonReaderException::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_U3CLineNumberU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonReaderException_t44F84BC4AF81F378D1901DEAD54371B9C55A9708, ___U3CLineNumberU3Ek__BackingField_17)); }
	inline int32_t get_U3CLineNumberU3Ek__BackingField_17() const { return ___U3CLineNumberU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CLineNumberU3Ek__BackingField_17() { return &___U3CLineNumberU3Ek__BackingField_17; }
	inline void set_U3CLineNumberU3Ek__BackingField_17(int32_t value)
	{
		___U3CLineNumberU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CLinePositionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonReaderException_t44F84BC4AF81F378D1901DEAD54371B9C55A9708, ___U3CLinePositionU3Ek__BackingField_18)); }
	inline int32_t get_U3CLinePositionU3Ek__BackingField_18() const { return ___U3CLinePositionU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CLinePositionU3Ek__BackingField_18() { return &___U3CLinePositionU3Ek__BackingField_18; }
	inline void set_U3CLinePositionU3Ek__BackingField_18(int32_t value)
	{
		___U3CLinePositionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonReaderException_t44F84BC4AF81F378D1901DEAD54371B9C55A9708, ___U3CPathU3Ek__BackingField_19)); }
	inline String_t* get_U3CPathU3Ek__BackingField_19() const { return ___U3CPathU3Ek__BackingField_19; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_19() { return &___U3CPathU3Ek__BackingField_19; }
	inline void set_U3CPathU3Ek__BackingField_19(String_t* value)
	{
		___U3CPathU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADEREXCEPTION_T44F84BC4AF81F378D1901DEAD54371B9C55A9708_H
#ifndef JSONSERIALIZATIONEXCEPTION_T9971A0E26D360F6FBF0E3543751D37D64325FB59_H
#define JSONSERIALIZATIONEXCEPTION_T9971A0E26D360F6FBF0E3543751D37D64325FB59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonSerializationException
struct  JsonSerializationException_t9971A0E26D360F6FBF0E3543751D37D64325FB59  : public JsonException_tE9DA91D672710AF3C23049EEC7872C5B4C9C89D8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZATIONEXCEPTION_T9971A0E26D360F6FBF0E3543751D37D64325FB59_H
#ifndef JSONTOKEN_T6ADDEFDDA044FF7A3AAFC9E7CBD0E568B418410D_H
#define JSONTOKEN_T6ADDEFDDA044FF7A3AAFC9E7CBD0E568B418410D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonToken
struct  JsonToken_t6ADDEFDDA044FF7A3AAFC9E7CBD0E568B418410D 
{
public:
	// System.Int32 Newtonsoft.Json.JsonToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonToken_t6ADDEFDDA044FF7A3AAFC9E7CBD0E568B418410D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKEN_T6ADDEFDDA044FF7A3AAFC9E7CBD0E568B418410D_H
#ifndef STATE_T1DE392B52BABD9BF376B03FE41F672F7113AE11F_H
#define STATE_T1DE392B52BABD9BF376B03FE41F672F7113AE11F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter/State
struct  State_t1DE392B52BABD9BF376B03FE41F672F7113AE11F 
{
public:
	// System.Int32 Newtonsoft.Json.JsonWriter/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t1DE392B52BABD9BF376B03FE41F672F7113AE11F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T1DE392B52BABD9BF376B03FE41F672F7113AE11F_H
#ifndef JSONWRITEREXCEPTION_T5265D2B2FDE5BC816FC891D65229A75A26806CE2_H
#define JSONWRITEREXCEPTION_T5265D2B2FDE5BC816FC891D65229A75A26806CE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriterException
struct  JsonWriterException_t5265D2B2FDE5BC816FC891D65229A75A26806CE2  : public JsonException_tE9DA91D672710AF3C23049EEC7872C5B4C9C89D8
{
public:
	// System.String Newtonsoft.Json.JsonWriterException::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonWriterException_t5265D2B2FDE5BC816FC891D65229A75A26806CE2, ___U3CPathU3Ek__BackingField_17)); }
	inline String_t* get_U3CPathU3Ek__BackingField_17() const { return ___U3CPathU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_17() { return &___U3CPathU3Ek__BackingField_17; }
	inline void set_U3CPathU3Ek__BackingField_17(String_t* value)
	{
		___U3CPathU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITEREXCEPTION_T5265D2B2FDE5BC816FC891D65229A75A26806CE2_H
#ifndef MEMBERSERIALIZATION_TDB2DD34DBC5EF12D4740B667F601358C7DA90212_H
#define MEMBERSERIALIZATION_TDB2DD34DBC5EF12D4740B667F601358C7DA90212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MemberSerialization
struct  MemberSerialization_tDB2DD34DBC5EF12D4740B667F601358C7DA90212 
{
public:
	// System.Int32 Newtonsoft.Json.MemberSerialization::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MemberSerialization_tDB2DD34DBC5EF12D4740B667F601358C7DA90212, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERSERIALIZATION_TDB2DD34DBC5EF12D4740B667F601358C7DA90212_H
#ifndef METADATAPROPERTYHANDLING_TD6239BDB5CF9C10FA2CC900DE4BC30C9BD253AF7_H
#define METADATAPROPERTYHANDLING_TD6239BDB5CF9C10FA2CC900DE4BC30C9BD253AF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MetadataPropertyHandling
struct  MetadataPropertyHandling_tD6239BDB5CF9C10FA2CC900DE4BC30C9BD253AF7 
{
public:
	// System.Int32 Newtonsoft.Json.MetadataPropertyHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MetadataPropertyHandling_tD6239BDB5CF9C10FA2CC900DE4BC30C9BD253AF7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATAPROPERTYHANDLING_TD6239BDB5CF9C10FA2CC900DE4BC30C9BD253AF7_H
#ifndef MISSINGMEMBERHANDLING_T1F9543EA590BC359A9E4684D9D12F6E1098E276D_H
#define MISSINGMEMBERHANDLING_T1F9543EA590BC359A9E4684D9D12F6E1098E276D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MissingMemberHandling
struct  MissingMemberHandling_t1F9543EA590BC359A9E4684D9D12F6E1098E276D 
{
public:
	// System.Int32 Newtonsoft.Json.MissingMemberHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MissingMemberHandling_t1F9543EA590BC359A9E4684D9D12F6E1098E276D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSINGMEMBERHANDLING_T1F9543EA590BC359A9E4684D9D12F6E1098E276D_H
#ifndef NULLVALUEHANDLING_TD5E3B6B0BF979D9DA6A17E676FA5394A5493430D_H
#define NULLVALUEHANDLING_TD5E3B6B0BF979D9DA6A17E676FA5394A5493430D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.NullValueHandling
struct  NullValueHandling_tD5E3B6B0BF979D9DA6A17E676FA5394A5493430D 
{
public:
	// System.Int32 Newtonsoft.Json.NullValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NullValueHandling_tD5E3B6B0BF979D9DA6A17E676FA5394A5493430D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLVALUEHANDLING_TD5E3B6B0BF979D9DA6A17E676FA5394A5493430D_H
#ifndef OBJECTCREATIONHANDLING_TC02C49234F03D7C4A800245AA414325496355B2C_H
#define OBJECTCREATIONHANDLING_TC02C49234F03D7C4A800245AA414325496355B2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ObjectCreationHandling
struct  ObjectCreationHandling_tC02C49234F03D7C4A800245AA414325496355B2C 
{
public:
	// System.Int32 Newtonsoft.Json.ObjectCreationHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectCreationHandling_tC02C49234F03D7C4A800245AA414325496355B2C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCREATIONHANDLING_TC02C49234F03D7C4A800245AA414325496355B2C_H
#ifndef PRESERVEREFERENCESHANDLING_TBF8B928632AB094AB854A8C8060F4A0DE246BDE0_H
#define PRESERVEREFERENCESHANDLING_TBF8B928632AB094AB854A8C8060F4A0DE246BDE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.PreserveReferencesHandling
struct  PreserveReferencesHandling_tBF8B928632AB094AB854A8C8060F4A0DE246BDE0 
{
public:
	// System.Int32 Newtonsoft.Json.PreserveReferencesHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PreserveReferencesHandling_tBF8B928632AB094AB854A8C8060F4A0DE246BDE0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEREFERENCESHANDLING_TBF8B928632AB094AB854A8C8060F4A0DE246BDE0_H
#ifndef READTYPE_T0139C8C2354D6D4A51B10FEE12C9E9549C2C247A_H
#define READTYPE_T0139C8C2354D6D4A51B10FEE12C9E9549C2C247A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReadType
struct  ReadType_t0139C8C2354D6D4A51B10FEE12C9E9549C2C247A 
{
public:
	// System.Int32 Newtonsoft.Json.ReadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReadType_t0139C8C2354D6D4A51B10FEE12C9E9549C2C247A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READTYPE_T0139C8C2354D6D4A51B10FEE12C9E9549C2C247A_H
#ifndef REFERENCELOOPHANDLING_T1F4E04DDA8FDB18AAA2B32B2E8FAC32ADA9AF672_H
#define REFERENCELOOPHANDLING_T1F4E04DDA8FDB18AAA2B32B2E8FAC32ADA9AF672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReferenceLoopHandling
struct  ReferenceLoopHandling_t1F4E04DDA8FDB18AAA2B32B2E8FAC32ADA9AF672 
{
public:
	// System.Int32 Newtonsoft.Json.ReferenceLoopHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReferenceLoopHandling_t1F4E04DDA8FDB18AAA2B32B2E8FAC32ADA9AF672, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCELOOPHANDLING_T1F4E04DDA8FDB18AAA2B32B2E8FAC32ADA9AF672_H
#ifndef REQUIRED_TB3FC6E6E91A682BD5E3D527FCE4979AFE2DD58B3_H
#define REQUIRED_TB3FC6E6E91A682BD5E3D527FCE4979AFE2DD58B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Required
struct  Required_tB3FC6E6E91A682BD5E3D527FCE4979AFE2DD58B3 
{
public:
	// System.Int32 Newtonsoft.Json.Required::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Required_tB3FC6E6E91A682BD5E3D527FCE4979AFE2DD58B3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRED_TB3FC6E6E91A682BD5E3D527FCE4979AFE2DD58B3_H
#ifndef STRINGESCAPEHANDLING_T3E67D3B0A780B24A2D57DF8921B9438CC507029A_H
#define STRINGESCAPEHANDLING_T3E67D3B0A780B24A2D57DF8921B9438CC507029A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.StringEscapeHandling
struct  StringEscapeHandling_t3E67D3B0A780B24A2D57DF8921B9438CC507029A 
{
public:
	// System.Int32 Newtonsoft.Json.StringEscapeHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StringEscapeHandling_t3E67D3B0A780B24A2D57DF8921B9438CC507029A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGESCAPEHANDLING_T3E67D3B0A780B24A2D57DF8921B9438CC507029A_H
#ifndef TYPENAMEHANDLING_T0DC2566A7FAFA823002ADAD5175DFF387F7D1ED9_H
#define TYPENAMEHANDLING_T0DC2566A7FAFA823002ADAD5175DFF387F7D1ED9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.TypeNameHandling
struct  TypeNameHandling_t0DC2566A7FAFA823002ADAD5175DFF387F7D1ED9 
{
public:
	// System.Int32 Newtonsoft.Json.TypeNameHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeNameHandling_t0DC2566A7FAFA823002ADAD5175DFF387F7D1ED9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPENAMEHANDLING_T0DC2566A7FAFA823002ADAD5175DFF387F7D1ED9_H
#ifndef CONVERTRESULT_T5764DADF61DF75048A4BA040ABE21C9BB85E9F5D_H
#define CONVERTRESULT_T5764DADF61DF75048A4BA040ABE21C9BB85E9F5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils/ConvertResult
struct  ConvertResult_t5764DADF61DF75048A4BA040ABE21C9BB85E9F5D 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.ConvertUtils/ConvertResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConvertResult_t5764DADF61DF75048A4BA040ABE21C9BB85E9F5D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTRESULT_T5764DADF61DF75048A4BA040ABE21C9BB85E9F5D_H
#ifndef PARSERESULT_TB1C99EF551F81C1C9EEBFF0995B1C67057AE8173_H
#define PARSERESULT_TB1C99EF551F81C1C9EEBFF0995B1C67057AE8173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ParseResult
struct  ParseResult_tB1C99EF551F81C1C9EEBFF0995B1C67057AE8173 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.ParseResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParseResult_tB1C99EF551F81C1C9EEBFF0995B1C67057AE8173, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSERESULT_TB1C99EF551F81C1C9EEBFF0995B1C67057AE8173_H
#ifndef PARSERTIMEZONE_T0942D7F8FA70C2B7B4B00C5F58D1B0D0E13018E9_H
#define PARSERTIMEZONE_T0942D7F8FA70C2B7B4B00C5F58D1B0D0E13018E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ParserTimeZone
struct  ParserTimeZone_t0942D7F8FA70C2B7B4B00C5F58D1B0D0E13018E9 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.ParserTimeZone::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParserTimeZone_t0942D7F8FA70C2B7B4B00C5F58D1B0D0E13018E9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSERTIMEZONE_T0942D7F8FA70C2B7B4B00C5F58D1B0D0E13018E9_H
#ifndef PRIMITIVETYPECODE_T895AAD3E5F0FFA0FE45CE4B8A0F981F2815567C4_H
#define PRIMITIVETYPECODE_T895AAD3E5F0FFA0FE45CE4B8A0F981F2815567C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PrimitiveTypeCode
struct  PrimitiveTypeCode_t895AAD3E5F0FFA0FE45CE4B8A0F981F2815567C4 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PrimitiveTypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PrimitiveTypeCode_t895AAD3E5F0FFA0FE45CE4B8A0F981F2815567C4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPECODE_T895AAD3E5F0FFA0FE45CE4B8A0F981F2815567C4_H
#ifndef WRITESTATE_T6C7BA768B993A34A6E74A7AEB5CA535833FAA1E2_H
#define WRITESTATE_T6C7BA768B993A34A6E74A7AEB5CA535833FAA1E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.WriteState
struct  WriteState_t6C7BA768B993A34A6E74A7AEB5CA535833FAA1E2 
{
public:
	// System.Int32 Newtonsoft.Json.WriteState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WriteState_t6C7BA768B993A34A6E74A7AEB5CA535833FAA1E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATE_T6C7BA768B993A34A6E74A7AEB5CA535833FAA1E2_H
#ifndef FORMATTERASSEMBLYSTYLE_TA1E8A300026362A0AE091830C5DBDEFCBCD5213A_H
#define FORMATTERASSEMBLYSTYLE_TA1E8A300026362A0AE091830C5DBDEFCBCD5213A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
struct  FormatterAssemblyStyle_tA1E8A300026362A0AE091830C5DBDEFCBCD5213A 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.FormatterAssemblyStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FormatterAssemblyStyle_tA1E8A300026362A0AE091830C5DBDEFCBCD5213A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERASSEMBLYSTYLE_TA1E8A300026362A0AE091830C5DBDEFCBCD5213A_H
#ifndef STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#define STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#ifndef JSONPOSITION_T7C381FA9E9591B7782B67363333C5CFA331E8EAD_H
#define JSONPOSITION_T7C381FA9E9591B7782B67363333C5CFA331E8EAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPosition
struct  JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD 
{
public:
	// Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonPosition::Type
	int32_t ___Type_1;
	// System.Int32 Newtonsoft.Json.JsonPosition::Position
	int32_t ___Position_2;
	// System.String Newtonsoft.Json.JsonPosition::PropertyName
	String_t* ___PropertyName_3;
	// System.Boolean Newtonsoft.Json.JsonPosition::HasIndex
	bool ___HasIndex_4;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_HasIndex_4() { return static_cast<int32_t>(offsetof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD, ___HasIndex_4)); }
	inline bool get_HasIndex_4() const { return ___HasIndex_4; }
	inline bool* get_address_of_HasIndex_4() { return &___HasIndex_4; }
	inline void set_HasIndex_4(bool value)
	{
		___HasIndex_4 = value;
	}
};

struct JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD_StaticFields
{
public:
	// System.Char[] Newtonsoft.Json.JsonPosition::SpecialCharacters
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___SpecialCharacters_0;

public:
	inline static int32_t get_offset_of_SpecialCharacters_0() { return static_cast<int32_t>(offsetof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD_StaticFields, ___SpecialCharacters_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_SpecialCharacters_0() const { return ___SpecialCharacters_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_SpecialCharacters_0() { return &___SpecialCharacters_0; }
	inline void set_SpecialCharacters_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___SpecialCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialCharacters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD_marshaled_pinvoke
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	char* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
// Native definition for COM marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD_marshaled_com
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	Il2CppChar* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
#endif // JSONPOSITION_T7C381FA9E9591B7782B67363333C5CFA331E8EAD_H
#ifndef DATETIMEPARSER_TC68D649158A233987D1B636F5ABB3E630E2477A3_H
#define DATETIMEPARSER_TC68D649158A233987D1B636F5ABB3E630E2477A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.DateTimeParser
struct  DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Year
	int32_t ___Year_0;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Month
	int32_t ___Month_1;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Day
	int32_t ___Day_2;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Hour
	int32_t ___Hour_3;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Minute
	int32_t ___Minute_4;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Second
	int32_t ___Second_5;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Fraction
	int32_t ___Fraction_6;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::ZoneHour
	int32_t ___ZoneHour_7;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::ZoneMinute
	int32_t ___ZoneMinute_8;
	// Newtonsoft.Json.Utilities.ParserTimeZone Newtonsoft.Json.Utilities.DateTimeParser::Zone
	int32_t ___Zone_9;
	// System.Char[] Newtonsoft.Json.Utilities.DateTimeParser::_text
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____text_10;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::_end
	int32_t ____end_11;

public:
	inline static int32_t get_offset_of_Year_0() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3, ___Year_0)); }
	inline int32_t get_Year_0() const { return ___Year_0; }
	inline int32_t* get_address_of_Year_0() { return &___Year_0; }
	inline void set_Year_0(int32_t value)
	{
		___Year_0 = value;
	}

	inline static int32_t get_offset_of_Month_1() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3, ___Month_1)); }
	inline int32_t get_Month_1() const { return ___Month_1; }
	inline int32_t* get_address_of_Month_1() { return &___Month_1; }
	inline void set_Month_1(int32_t value)
	{
		___Month_1 = value;
	}

	inline static int32_t get_offset_of_Day_2() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3, ___Day_2)); }
	inline int32_t get_Day_2() const { return ___Day_2; }
	inline int32_t* get_address_of_Day_2() { return &___Day_2; }
	inline void set_Day_2(int32_t value)
	{
		___Day_2 = value;
	}

	inline static int32_t get_offset_of_Hour_3() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3, ___Hour_3)); }
	inline int32_t get_Hour_3() const { return ___Hour_3; }
	inline int32_t* get_address_of_Hour_3() { return &___Hour_3; }
	inline void set_Hour_3(int32_t value)
	{
		___Hour_3 = value;
	}

	inline static int32_t get_offset_of_Minute_4() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3, ___Minute_4)); }
	inline int32_t get_Minute_4() const { return ___Minute_4; }
	inline int32_t* get_address_of_Minute_4() { return &___Minute_4; }
	inline void set_Minute_4(int32_t value)
	{
		___Minute_4 = value;
	}

	inline static int32_t get_offset_of_Second_5() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3, ___Second_5)); }
	inline int32_t get_Second_5() const { return ___Second_5; }
	inline int32_t* get_address_of_Second_5() { return &___Second_5; }
	inline void set_Second_5(int32_t value)
	{
		___Second_5 = value;
	}

	inline static int32_t get_offset_of_Fraction_6() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3, ___Fraction_6)); }
	inline int32_t get_Fraction_6() const { return ___Fraction_6; }
	inline int32_t* get_address_of_Fraction_6() { return &___Fraction_6; }
	inline void set_Fraction_6(int32_t value)
	{
		___Fraction_6 = value;
	}

	inline static int32_t get_offset_of_ZoneHour_7() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3, ___ZoneHour_7)); }
	inline int32_t get_ZoneHour_7() const { return ___ZoneHour_7; }
	inline int32_t* get_address_of_ZoneHour_7() { return &___ZoneHour_7; }
	inline void set_ZoneHour_7(int32_t value)
	{
		___ZoneHour_7 = value;
	}

	inline static int32_t get_offset_of_ZoneMinute_8() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3, ___ZoneMinute_8)); }
	inline int32_t get_ZoneMinute_8() const { return ___ZoneMinute_8; }
	inline int32_t* get_address_of_ZoneMinute_8() { return &___ZoneMinute_8; }
	inline void set_ZoneMinute_8(int32_t value)
	{
		___ZoneMinute_8 = value;
	}

	inline static int32_t get_offset_of_Zone_9() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3, ___Zone_9)); }
	inline int32_t get_Zone_9() const { return ___Zone_9; }
	inline int32_t* get_address_of_Zone_9() { return &___Zone_9; }
	inline void set_Zone_9(int32_t value)
	{
		___Zone_9 = value;
	}

	inline static int32_t get_offset_of__text_10() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3, ____text_10)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__text_10() const { return ____text_10; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__text_10() { return &____text_10; }
	inline void set__text_10(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____text_10 = value;
		Il2CppCodeGenWriteBarrier((&____text_10), value);
	}

	inline static int32_t get_offset_of__end_11() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3, ____end_11)); }
	inline int32_t get__end_11() const { return ____end_11; }
	inline int32_t* get_address_of__end_11() { return &____end_11; }
	inline void set__end_11(int32_t value)
	{
		____end_11 = value;
	}
};

struct DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields
{
public:
	// System.Int32[] Newtonsoft.Json.Utilities.DateTimeParser::Power10
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Power10_12;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy
	int32_t ___Lzyyyy_13;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_
	int32_t ___Lzyyyy__14;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM
	int32_t ___Lzyyyy_MM_15;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM_
	int32_t ___Lzyyyy_MM__16;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM_dd
	int32_t ___Lzyyyy_MM_dd_17;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM_ddT
	int32_t ___Lzyyyy_MM_ddT_18;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH
	int32_t ___LzHH_19;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_
	int32_t ___LzHH__20;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_mm
	int32_t ___LzHH_mm_21;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_mm_
	int32_t ___LzHH_mm__22;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_mm_ss
	int32_t ___LzHH_mm_ss_23;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lz_
	int32_t ___Lz__24;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lz_zz
	int32_t ___Lz_zz_25;

public:
	inline static int32_t get_offset_of_Power10_12() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields, ___Power10_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Power10_12() const { return ___Power10_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Power10_12() { return &___Power10_12; }
	inline void set_Power10_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Power10_12 = value;
		Il2CppCodeGenWriteBarrier((&___Power10_12), value);
	}

	inline static int32_t get_offset_of_Lzyyyy_13() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields, ___Lzyyyy_13)); }
	inline int32_t get_Lzyyyy_13() const { return ___Lzyyyy_13; }
	inline int32_t* get_address_of_Lzyyyy_13() { return &___Lzyyyy_13; }
	inline void set_Lzyyyy_13(int32_t value)
	{
		___Lzyyyy_13 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy__14() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields, ___Lzyyyy__14)); }
	inline int32_t get_Lzyyyy__14() const { return ___Lzyyyy__14; }
	inline int32_t* get_address_of_Lzyyyy__14() { return &___Lzyyyy__14; }
	inline void set_Lzyyyy__14(int32_t value)
	{
		___Lzyyyy__14 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_15() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields, ___Lzyyyy_MM_15)); }
	inline int32_t get_Lzyyyy_MM_15() const { return ___Lzyyyy_MM_15; }
	inline int32_t* get_address_of_Lzyyyy_MM_15() { return &___Lzyyyy_MM_15; }
	inline void set_Lzyyyy_MM_15(int32_t value)
	{
		___Lzyyyy_MM_15 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM__16() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields, ___Lzyyyy_MM__16)); }
	inline int32_t get_Lzyyyy_MM__16() const { return ___Lzyyyy_MM__16; }
	inline int32_t* get_address_of_Lzyyyy_MM__16() { return &___Lzyyyy_MM__16; }
	inline void set_Lzyyyy_MM__16(int32_t value)
	{
		___Lzyyyy_MM__16 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_dd_17() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields, ___Lzyyyy_MM_dd_17)); }
	inline int32_t get_Lzyyyy_MM_dd_17() const { return ___Lzyyyy_MM_dd_17; }
	inline int32_t* get_address_of_Lzyyyy_MM_dd_17() { return &___Lzyyyy_MM_dd_17; }
	inline void set_Lzyyyy_MM_dd_17(int32_t value)
	{
		___Lzyyyy_MM_dd_17 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_ddT_18() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields, ___Lzyyyy_MM_ddT_18)); }
	inline int32_t get_Lzyyyy_MM_ddT_18() const { return ___Lzyyyy_MM_ddT_18; }
	inline int32_t* get_address_of_Lzyyyy_MM_ddT_18() { return &___Lzyyyy_MM_ddT_18; }
	inline void set_Lzyyyy_MM_ddT_18(int32_t value)
	{
		___Lzyyyy_MM_ddT_18 = value;
	}

	inline static int32_t get_offset_of_LzHH_19() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields, ___LzHH_19)); }
	inline int32_t get_LzHH_19() const { return ___LzHH_19; }
	inline int32_t* get_address_of_LzHH_19() { return &___LzHH_19; }
	inline void set_LzHH_19(int32_t value)
	{
		___LzHH_19 = value;
	}

	inline static int32_t get_offset_of_LzHH__20() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields, ___LzHH__20)); }
	inline int32_t get_LzHH__20() const { return ___LzHH__20; }
	inline int32_t* get_address_of_LzHH__20() { return &___LzHH__20; }
	inline void set_LzHH__20(int32_t value)
	{
		___LzHH__20 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm_21() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields, ___LzHH_mm_21)); }
	inline int32_t get_LzHH_mm_21() const { return ___LzHH_mm_21; }
	inline int32_t* get_address_of_LzHH_mm_21() { return &___LzHH_mm_21; }
	inline void set_LzHH_mm_21(int32_t value)
	{
		___LzHH_mm_21 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm__22() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields, ___LzHH_mm__22)); }
	inline int32_t get_LzHH_mm__22() const { return ___LzHH_mm__22; }
	inline int32_t* get_address_of_LzHH_mm__22() { return &___LzHH_mm__22; }
	inline void set_LzHH_mm__22(int32_t value)
	{
		___LzHH_mm__22 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm_ss_23() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields, ___LzHH_mm_ss_23)); }
	inline int32_t get_LzHH_mm_ss_23() const { return ___LzHH_mm_ss_23; }
	inline int32_t* get_address_of_LzHH_mm_ss_23() { return &___LzHH_mm_ss_23; }
	inline void set_LzHH_mm_ss_23(int32_t value)
	{
		___LzHH_mm_ss_23 = value;
	}

	inline static int32_t get_offset_of_Lz__24() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields, ___Lz__24)); }
	inline int32_t get_Lz__24() const { return ___Lz__24; }
	inline int32_t* get_address_of_Lz__24() { return &___Lz__24; }
	inline void set_Lz__24(int32_t value)
	{
		___Lz__24 = value;
	}

	inline static int32_t get_offset_of_Lz_zz_25() { return static_cast<int32_t>(offsetof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields, ___Lz_zz_25)); }
	inline int32_t get_Lz_zz_25() const { return ___Lz_zz_25; }
	inline int32_t* get_address_of_Lz_zz_25() { return &___Lz_zz_25; }
	inline void set_Lz_zz_25(int32_t value)
	{
		___Lz_zz_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.DateTimeParser
struct DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_marshaled_pinvoke
{
	int32_t ___Year_0;
	int32_t ___Month_1;
	int32_t ___Day_2;
	int32_t ___Hour_3;
	int32_t ___Minute_4;
	int32_t ___Second_5;
	int32_t ___Fraction_6;
	int32_t ___ZoneHour_7;
	int32_t ___ZoneMinute_8;
	int32_t ___Zone_9;
	uint8_t* ____text_10;
	int32_t ____end_11;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.DateTimeParser
struct DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_marshaled_com
{
	int32_t ___Year_0;
	int32_t ___Month_1;
	int32_t ___Day_2;
	int32_t ___Hour_3;
	int32_t ___Minute_4;
	int32_t ___Second_5;
	int32_t ___Fraction_6;
	int32_t ___ZoneHour_7;
	int32_t ___ZoneMinute_8;
	int32_t ___Zone_9;
	uint8_t* ____text_10;
	int32_t ____end_11;
};
#endif // DATETIMEPARSER_TC68D649158A233987D1B636F5ABB3E630E2477A3_H
#ifndef TYPEINFORMATION_TB8AF729595EBB002094570C06F7D950F081D3BB7_H
#define TYPEINFORMATION_TB8AF729595EBB002094570C06F7D950F081D3BB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.TypeInformation
struct  TypeInformation_tB8AF729595EBB002094570C06F7D950F081D3BB7  : public RuntimeObject
{
public:
	// System.Type Newtonsoft.Json.Utilities.TypeInformation::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_0;
	// Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Utilities.TypeInformation::<TypeCode>k__BackingField
	int32_t ___U3CTypeCodeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TypeInformation_tB8AF729595EBB002094570C06F7D950F081D3BB7, ___U3CTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTypeCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TypeInformation_tB8AF729595EBB002094570C06F7D950F081D3BB7, ___U3CTypeCodeU3Ek__BackingField_1)); }
	inline int32_t get_U3CTypeCodeU3Ek__BackingField_1() const { return ___U3CTypeCodeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTypeCodeU3Ek__BackingField_1() { return &___U3CTypeCodeU3Ek__BackingField_1; }
	inline void set_U3CTypeCodeU3Ek__BackingField_1(int32_t value)
	{
		___U3CTypeCodeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFORMATION_TB8AF729595EBB002094570C06F7D950F081D3BB7_H
#ifndef NULLABLE_1_T386D7565CF3D349D5FEFDBB687254FC491B70AD5_H
#define NULLABLE_1_T386D7565CF3D349D5FEFDBB687254FC491B70AD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ConstructorHandling>
struct  Nullable_1_t386D7565CF3D349D5FEFDBB687254FC491B70AD5 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t386D7565CF3D349D5FEFDBB687254FC491B70AD5, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t386D7565CF3D349D5FEFDBB687254FC491B70AD5, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T386D7565CF3D349D5FEFDBB687254FC491B70AD5_H
#ifndef NULLABLE_1_T23DBCFD9D5A38259E91BB91E0DA2F310925C5660_H
#define NULLABLE_1_T23DBCFD9D5A38259E91BB91E0DA2F310925C5660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateFormatHandling>
struct  Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T23DBCFD9D5A38259E91BB91E0DA2F310925C5660_H
#ifndef NULLABLE_1_T0043F1937EEC03CE189188BDA0D7755CFAB90BDF_H
#define NULLABLE_1_T0043F1937EEC03CE189188BDA0D7755CFAB90BDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateParseHandling>
struct  Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0043F1937EEC03CE189188BDA0D7755CFAB90BDF_H
#ifndef NULLABLE_1_TCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE_H
#define NULLABLE_1_TCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>
struct  Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE_H
#ifndef NULLABLE_1_T29A6EA9275C7673344AD9B7F5F44F6126748C0FA_H
#define NULLABLE_1_T29A6EA9275C7673344AD9B7F5F44F6126748C0FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>
struct  Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T29A6EA9275C7673344AD9B7F5F44F6126748C0FA_H
#ifndef NULLABLE_1_T11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810_H
#define NULLABLE_1_T11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>
struct  Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810_H
#ifndef NULLABLE_1_T7F088871846AAE8EF21D751D8B97430B761673C6_H
#define NULLABLE_1_T7F088871846AAE8EF21D751D8B97430B761673C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.FloatParseHandling>
struct  Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T7F088871846AAE8EF21D751D8B97430B761673C6_H
#ifndef NULLABLE_1_T941F23108D6567DD26A44A84616CC08E05CD0059_H
#define NULLABLE_1_T941F23108D6567DD26A44A84616CC08E05CD0059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Formatting>
struct  Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T941F23108D6567DD26A44A84616CC08E05CD0059_H
#ifndef NULLABLE_1_TF330C1A3D6D96AD89924DABE832ED2C68921413B_H
#define NULLABLE_1_TF330C1A3D6D96AD89924DABE832ED2C68921413B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.MetadataPropertyHandling>
struct  Nullable_1_tF330C1A3D6D96AD89924DABE832ED2C68921413B 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tF330C1A3D6D96AD89924DABE832ED2C68921413B, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tF330C1A3D6D96AD89924DABE832ED2C68921413B, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TF330C1A3D6D96AD89924DABE832ED2C68921413B_H
#ifndef NULLABLE_1_T3ECC38B72B3C0270EC5B32F255134E67C8EE5013_H
#define NULLABLE_1_T3ECC38B72B3C0270EC5B32F255134E67C8EE5013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.MissingMemberHandling>
struct  Nullable_1_t3ECC38B72B3C0270EC5B32F255134E67C8EE5013 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3ECC38B72B3C0270EC5B32F255134E67C8EE5013, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3ECC38B72B3C0270EC5B32F255134E67C8EE5013, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3ECC38B72B3C0270EC5B32F255134E67C8EE5013_H
#ifndef NULLABLE_1_TBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA_H
#define NULLABLE_1_TBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.NullValueHandling>
struct  Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA_H
#ifndef NULLABLE_1_T43CD81743444619B643C0A3912749713399518C5_H
#define NULLABLE_1_T43CD81743444619B643C0A3912749713399518C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>
struct  Nullable_1_t43CD81743444619B643C0A3912749713399518C5 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t43CD81743444619B643C0A3912749713399518C5, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t43CD81743444619B643C0A3912749713399518C5, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T43CD81743444619B643C0A3912749713399518C5_H
#ifndef NULLABLE_1_T4DC01BF01ED0BB2AA2B73540F63FC0D48E3B387A_H
#define NULLABLE_1_T4DC01BF01ED0BB2AA2B73540F63FC0D48E3B387A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.PreserveReferencesHandling>
struct  Nullable_1_t4DC01BF01ED0BB2AA2B73540F63FC0D48E3B387A 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t4DC01BF01ED0BB2AA2B73540F63FC0D48E3B387A, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t4DC01BF01ED0BB2AA2B73540F63FC0D48E3B387A, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T4DC01BF01ED0BB2AA2B73540F63FC0D48E3B387A_H
#ifndef NULLABLE_1_T916E09184D3B63637815710054D9D54B992F5F33_H
#define NULLABLE_1_T916E09184D3B63637815710054D9D54B992F5F33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>
struct  Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T916E09184D3B63637815710054D9D54B992F5F33_H
#ifndef NULLABLE_1_T77B4FDCF98614C924B1310C1941E0897738EA65D_H
#define NULLABLE_1_T77B4FDCF98614C924B1310C1941E0897738EA65D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Required>
struct  Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T77B4FDCF98614C924B1310C1941E0897738EA65D_H
#ifndef NULLABLE_1_T5FD184676887BA5E49A49440E56E8C7ADFC19C6E_H
#define NULLABLE_1_T5FD184676887BA5E49A49440E56E8C7ADFC19C6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling>
struct  Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T5FD184676887BA5E49A49440E56E8C7ADFC19C6E_H
#ifndef NULLABLE_1_T86F69995F4B87E51DBFA76271E780A42E928B184_H
#define NULLABLE_1_T86F69995F4B87E51DBFA76271E780A42E928B184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.TypeNameHandling>
struct  Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T86F69995F4B87E51DBFA76271E780A42E928B184_H
#ifndef NULLABLE_1_TCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74_H
#define NULLABLE_1_TCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>
struct  Nullable_1_tCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74_H
#ifndef STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#define STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 
{
public:
	// System.Object System.Runtime.Serialization.StreamingContext::m_additionalContext
	RuntimeObject * ___m_additionalContext_0;
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::m_state
	int32_t ___m_state_1;

public:
	inline static int32_t get_offset_of_m_additionalContext_0() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_additionalContext_0)); }
	inline RuntimeObject * get_m_additionalContext_0() const { return ___m_additionalContext_0; }
	inline RuntimeObject ** get_address_of_m_additionalContext_0() { return &___m_additionalContext_0; }
	inline void set_m_additionalContext_0(RuntimeObject * value)
	{
		___m_additionalContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_additionalContext_0), value);
	}

	inline static int32_t get_offset_of_m_state_1() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_state_1)); }
	inline int32_t get_m_state_1() const { return ___m_state_1; }
	inline int32_t* get_address_of_m_state_1() { return &___m_state_1; }
	inline void set_m_state_1(int32_t value)
	{
		___m_state_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_pinvoke
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_com
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
#endif // STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#ifndef JSONCONTAINERATTRIBUTE_T2465568F23D145E0C80963D63F93771A8A605C10_H
#define JSONCONTAINERATTRIBUTE_T2465568F23D145E0C80963D63F93771A8A605C10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerAttribute
struct  JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type Newtonsoft.Json.JsonContainerAttribute::<ItemConverterType>k__BackingField
	Type_t * ___U3CItemConverterTypeU3Ek__BackingField_0;
	// System.Object[] Newtonsoft.Json.JsonContainerAttribute::<ItemConverterParameters>k__BackingField
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___U3CItemConverterParametersU3Ek__BackingField_1;
	// Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.JsonContainerAttribute::<NamingStrategyInstance>k__BackingField
	NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19 * ___U3CNamingStrategyInstanceU3Ek__BackingField_2;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonContainerAttribute::_isReference
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____isReference_3;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonContainerAttribute::_itemIsReference
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____itemIsReference_4;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.JsonContainerAttribute::_itemReferenceLoopHandling
	Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  ____itemReferenceLoopHandling_5;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.JsonContainerAttribute::_itemTypeNameHandling
	Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  ____itemTypeNameHandling_6;
	// System.Type Newtonsoft.Json.JsonContainerAttribute::_namingStrategyType
	Type_t * ____namingStrategyType_7;
	// System.Object[] Newtonsoft.Json.JsonContainerAttribute::_namingStrategyParameters
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____namingStrategyParameters_8;

public:
	inline static int32_t get_offset_of_U3CItemConverterTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10, ___U3CItemConverterTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CItemConverterTypeU3Ek__BackingField_0() const { return ___U3CItemConverterTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CItemConverterTypeU3Ek__BackingField_0() { return &___U3CItemConverterTypeU3Ek__BackingField_0; }
	inline void set_U3CItemConverterTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CItemConverterTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CItemConverterParametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10, ___U3CItemConverterParametersU3Ek__BackingField_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_U3CItemConverterParametersU3Ek__BackingField_1() const { return ___U3CItemConverterParametersU3Ek__BackingField_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_U3CItemConverterParametersU3Ek__BackingField_1() { return &___U3CItemConverterParametersU3Ek__BackingField_1; }
	inline void set_U3CItemConverterParametersU3Ek__BackingField_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___U3CItemConverterParametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterParametersU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CNamingStrategyInstanceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10, ___U3CNamingStrategyInstanceU3Ek__BackingField_2)); }
	inline NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19 * get_U3CNamingStrategyInstanceU3Ek__BackingField_2() const { return ___U3CNamingStrategyInstanceU3Ek__BackingField_2; }
	inline NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19 ** get_address_of_U3CNamingStrategyInstanceU3Ek__BackingField_2() { return &___U3CNamingStrategyInstanceU3Ek__BackingField_2; }
	inline void set_U3CNamingStrategyInstanceU3Ek__BackingField_2(NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19 * value)
	{
		___U3CNamingStrategyInstanceU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNamingStrategyInstanceU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of__isReference_3() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10, ____isReference_3)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__isReference_3() const { return ____isReference_3; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__isReference_3() { return &____isReference_3; }
	inline void set__isReference_3(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____isReference_3 = value;
	}

	inline static int32_t get_offset_of__itemIsReference_4() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10, ____itemIsReference_4)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__itemIsReference_4() const { return ____itemIsReference_4; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__itemIsReference_4() { return &____itemIsReference_4; }
	inline void set__itemIsReference_4(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____itemIsReference_4 = value;
	}

	inline static int32_t get_offset_of__itemReferenceLoopHandling_5() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10, ____itemReferenceLoopHandling_5)); }
	inline Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  get__itemReferenceLoopHandling_5() const { return ____itemReferenceLoopHandling_5; }
	inline Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33 * get_address_of__itemReferenceLoopHandling_5() { return &____itemReferenceLoopHandling_5; }
	inline void set__itemReferenceLoopHandling_5(Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  value)
	{
		____itemReferenceLoopHandling_5 = value;
	}

	inline static int32_t get_offset_of__itemTypeNameHandling_6() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10, ____itemTypeNameHandling_6)); }
	inline Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  get__itemTypeNameHandling_6() const { return ____itemTypeNameHandling_6; }
	inline Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184 * get_address_of__itemTypeNameHandling_6() { return &____itemTypeNameHandling_6; }
	inline void set__itemTypeNameHandling_6(Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  value)
	{
		____itemTypeNameHandling_6 = value;
	}

	inline static int32_t get_offset_of__namingStrategyType_7() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10, ____namingStrategyType_7)); }
	inline Type_t * get__namingStrategyType_7() const { return ____namingStrategyType_7; }
	inline Type_t ** get_address_of__namingStrategyType_7() { return &____namingStrategyType_7; }
	inline void set__namingStrategyType_7(Type_t * value)
	{
		____namingStrategyType_7 = value;
		Il2CppCodeGenWriteBarrier((&____namingStrategyType_7), value);
	}

	inline static int32_t get_offset_of__namingStrategyParameters_8() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10, ____namingStrategyParameters_8)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__namingStrategyParameters_8() const { return ____namingStrategyParameters_8; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__namingStrategyParameters_8() { return &____namingStrategyParameters_8; }
	inline void set__namingStrategyParameters_8(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____namingStrategyParameters_8 = value;
		Il2CppCodeGenWriteBarrier((&____namingStrategyParameters_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERATTRIBUTE_T2465568F23D145E0C80963D63F93771A8A605C10_H
#ifndef JSONPROPERTYATTRIBUTE_TA048054356547F8D7F503C0768A3EE277BA9DD69_H
#define JSONPROPERTYATTRIBUTE_TA048054356547F8D7F503C0768A3EE277BA9DD69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPropertyAttribute
struct  JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.JsonPropertyAttribute::_nullValueHandling
	Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA  ____nullValueHandling_0;
	// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.JsonPropertyAttribute::_defaultValueHandling
	Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA  ____defaultValueHandling_1;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.JsonPropertyAttribute::_referenceLoopHandling
	Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  ____referenceLoopHandling_2;
	// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.JsonPropertyAttribute::_objectCreationHandling
	Nullable_1_t43CD81743444619B643C0A3912749713399518C5  ____objectCreationHandling_3;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.JsonPropertyAttribute::_typeNameHandling
	Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  ____typeNameHandling_4;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonPropertyAttribute::_isReference
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____isReference_5;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonPropertyAttribute::_order
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ____order_6;
	// System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.JsonPropertyAttribute::_required
	Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D  ____required_7;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonPropertyAttribute::_itemIsReference
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____itemIsReference_8;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.JsonPropertyAttribute::_itemReferenceLoopHandling
	Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  ____itemReferenceLoopHandling_9;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.JsonPropertyAttribute::_itemTypeNameHandling
	Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  ____itemTypeNameHandling_10;
	// System.Type Newtonsoft.Json.JsonPropertyAttribute::<ItemConverterType>k__BackingField
	Type_t * ___U3CItemConverterTypeU3Ek__BackingField_11;
	// System.Object[] Newtonsoft.Json.JsonPropertyAttribute::<ItemConverterParameters>k__BackingField
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___U3CItemConverterParametersU3Ek__BackingField_12;
	// System.Type Newtonsoft.Json.JsonPropertyAttribute::<NamingStrategyType>k__BackingField
	Type_t * ___U3CNamingStrategyTypeU3Ek__BackingField_13;
	// System.Object[] Newtonsoft.Json.JsonPropertyAttribute::<NamingStrategyParameters>k__BackingField
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___U3CNamingStrategyParametersU3Ek__BackingField_14;
	// System.String Newtonsoft.Json.JsonPropertyAttribute::<PropertyName>k__BackingField
	String_t* ___U3CPropertyNameU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of__nullValueHandling_0() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ____nullValueHandling_0)); }
	inline Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA  get__nullValueHandling_0() const { return ____nullValueHandling_0; }
	inline Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA * get_address_of__nullValueHandling_0() { return &____nullValueHandling_0; }
	inline void set__nullValueHandling_0(Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA  value)
	{
		____nullValueHandling_0 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_1() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ____defaultValueHandling_1)); }
	inline Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA  get__defaultValueHandling_1() const { return ____defaultValueHandling_1; }
	inline Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA * get_address_of__defaultValueHandling_1() { return &____defaultValueHandling_1; }
	inline void set__defaultValueHandling_1(Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA  value)
	{
		____defaultValueHandling_1 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_2() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ____referenceLoopHandling_2)); }
	inline Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  get__referenceLoopHandling_2() const { return ____referenceLoopHandling_2; }
	inline Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33 * get_address_of__referenceLoopHandling_2() { return &____referenceLoopHandling_2; }
	inline void set__referenceLoopHandling_2(Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  value)
	{
		____referenceLoopHandling_2 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_3() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ____objectCreationHandling_3)); }
	inline Nullable_1_t43CD81743444619B643C0A3912749713399518C5  get__objectCreationHandling_3() const { return ____objectCreationHandling_3; }
	inline Nullable_1_t43CD81743444619B643C0A3912749713399518C5 * get_address_of__objectCreationHandling_3() { return &____objectCreationHandling_3; }
	inline void set__objectCreationHandling_3(Nullable_1_t43CD81743444619B643C0A3912749713399518C5  value)
	{
		____objectCreationHandling_3 = value;
	}

	inline static int32_t get_offset_of__typeNameHandling_4() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ____typeNameHandling_4)); }
	inline Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  get__typeNameHandling_4() const { return ____typeNameHandling_4; }
	inline Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184 * get_address_of__typeNameHandling_4() { return &____typeNameHandling_4; }
	inline void set__typeNameHandling_4(Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  value)
	{
		____typeNameHandling_4 = value;
	}

	inline static int32_t get_offset_of__isReference_5() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ____isReference_5)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__isReference_5() const { return ____isReference_5; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__isReference_5() { return &____isReference_5; }
	inline void set__isReference_5(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____isReference_5 = value;
	}

	inline static int32_t get_offset_of__order_6() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ____order_6)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get__order_6() const { return ____order_6; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of__order_6() { return &____order_6; }
	inline void set__order_6(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		____order_6 = value;
	}

	inline static int32_t get_offset_of__required_7() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ____required_7)); }
	inline Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D  get__required_7() const { return ____required_7; }
	inline Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D * get_address_of__required_7() { return &____required_7; }
	inline void set__required_7(Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D  value)
	{
		____required_7 = value;
	}

	inline static int32_t get_offset_of__itemIsReference_8() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ____itemIsReference_8)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__itemIsReference_8() const { return ____itemIsReference_8; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__itemIsReference_8() { return &____itemIsReference_8; }
	inline void set__itemIsReference_8(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____itemIsReference_8 = value;
	}

	inline static int32_t get_offset_of__itemReferenceLoopHandling_9() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ____itemReferenceLoopHandling_9)); }
	inline Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  get__itemReferenceLoopHandling_9() const { return ____itemReferenceLoopHandling_9; }
	inline Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33 * get_address_of__itemReferenceLoopHandling_9() { return &____itemReferenceLoopHandling_9; }
	inline void set__itemReferenceLoopHandling_9(Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  value)
	{
		____itemReferenceLoopHandling_9 = value;
	}

	inline static int32_t get_offset_of__itemTypeNameHandling_10() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ____itemTypeNameHandling_10)); }
	inline Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  get__itemTypeNameHandling_10() const { return ____itemTypeNameHandling_10; }
	inline Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184 * get_address_of__itemTypeNameHandling_10() { return &____itemTypeNameHandling_10; }
	inline void set__itemTypeNameHandling_10(Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  value)
	{
		____itemTypeNameHandling_10 = value;
	}

	inline static int32_t get_offset_of_U3CItemConverterTypeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ___U3CItemConverterTypeU3Ek__BackingField_11)); }
	inline Type_t * get_U3CItemConverterTypeU3Ek__BackingField_11() const { return ___U3CItemConverterTypeU3Ek__BackingField_11; }
	inline Type_t ** get_address_of_U3CItemConverterTypeU3Ek__BackingField_11() { return &___U3CItemConverterTypeU3Ek__BackingField_11; }
	inline void set_U3CItemConverterTypeU3Ek__BackingField_11(Type_t * value)
	{
		___U3CItemConverterTypeU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterTypeU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CItemConverterParametersU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ___U3CItemConverterParametersU3Ek__BackingField_12)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_U3CItemConverterParametersU3Ek__BackingField_12() const { return ___U3CItemConverterParametersU3Ek__BackingField_12; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_U3CItemConverterParametersU3Ek__BackingField_12() { return &___U3CItemConverterParametersU3Ek__BackingField_12; }
	inline void set_U3CItemConverterParametersU3Ek__BackingField_12(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___U3CItemConverterParametersU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterParametersU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CNamingStrategyTypeU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ___U3CNamingStrategyTypeU3Ek__BackingField_13)); }
	inline Type_t * get_U3CNamingStrategyTypeU3Ek__BackingField_13() const { return ___U3CNamingStrategyTypeU3Ek__BackingField_13; }
	inline Type_t ** get_address_of_U3CNamingStrategyTypeU3Ek__BackingField_13() { return &___U3CNamingStrategyTypeU3Ek__BackingField_13; }
	inline void set_U3CNamingStrategyTypeU3Ek__BackingField_13(Type_t * value)
	{
		___U3CNamingStrategyTypeU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNamingStrategyTypeU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CNamingStrategyParametersU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ___U3CNamingStrategyParametersU3Ek__BackingField_14)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_U3CNamingStrategyParametersU3Ek__BackingField_14() const { return ___U3CNamingStrategyParametersU3Ek__BackingField_14; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_U3CNamingStrategyParametersU3Ek__BackingField_14() { return &___U3CNamingStrategyParametersU3Ek__BackingField_14; }
	inline void set_U3CNamingStrategyParametersU3Ek__BackingField_14(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___U3CNamingStrategyParametersU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNamingStrategyParametersU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CPropertyNameU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69, ___U3CPropertyNameU3Ek__BackingField_15)); }
	inline String_t* get_U3CPropertyNameU3Ek__BackingField_15() const { return ___U3CPropertyNameU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CPropertyNameU3Ek__BackingField_15() { return &___U3CPropertyNameU3Ek__BackingField_15; }
	inline void set_U3CPropertyNameU3Ek__BackingField_15(String_t* value)
	{
		___U3CPropertyNameU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertyNameU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPROPERTYATTRIBUTE_TA048054356547F8D7F503C0768A3EE277BA9DD69_H
#ifndef JSONREADER_TF29A2AB439002FDEFB9592AA3147F02ADB3841DC_H
#define JSONREADER_TF29A2AB439002FDEFB9592AA3147F02ADB3841DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader
struct  JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC  : public RuntimeObject
{
public:
	// Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::_tokenType
	int32_t ____tokenType_0;
	// System.Object Newtonsoft.Json.JsonReader::_value
	RuntimeObject * ____value_1;
	// System.Char Newtonsoft.Json.JsonReader::_quoteChar
	Il2CppChar ____quoteChar_2;
	// Newtonsoft.Json.JsonReader/State Newtonsoft.Json.JsonReader::_currentState
	int32_t ____currentState_3;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonReader::_currentPosition
	JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD  ____currentPosition_4;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonReader::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_5;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonReader::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_6;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::_maxDepth
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ____maxDepth_7;
	// System.Boolean Newtonsoft.Json.JsonReader::_hasExceededMaxDepth
	bool ____hasExceededMaxDepth_8;
	// Newtonsoft.Json.DateParseHandling Newtonsoft.Json.JsonReader::_dateParseHandling
	int32_t ____dateParseHandling_9;
	// Newtonsoft.Json.FloatParseHandling Newtonsoft.Json.JsonReader::_floatParseHandling
	int32_t ____floatParseHandling_10;
	// System.String Newtonsoft.Json.JsonReader::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonReader::_stack
	List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 * ____stack_12;
	// System.Boolean Newtonsoft.Json.JsonReader::<CloseInput>k__BackingField
	bool ___U3CCloseInputU3Ek__BackingField_13;
	// System.Boolean Newtonsoft.Json.JsonReader::<SupportMultipleContent>k__BackingField
	bool ___U3CSupportMultipleContentU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of__tokenType_0() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____tokenType_0)); }
	inline int32_t get__tokenType_0() const { return ____tokenType_0; }
	inline int32_t* get_address_of__tokenType_0() { return &____tokenType_0; }
	inline void set__tokenType_0(int32_t value)
	{
		____tokenType_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}

	inline static int32_t get_offset_of__quoteChar_2() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____quoteChar_2)); }
	inline Il2CppChar get__quoteChar_2() const { return ____quoteChar_2; }
	inline Il2CppChar* get_address_of__quoteChar_2() { return &____quoteChar_2; }
	inline void set__quoteChar_2(Il2CppChar value)
	{
		____quoteChar_2 = value;
	}

	inline static int32_t get_offset_of__currentState_3() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____currentState_3)); }
	inline int32_t get__currentState_3() const { return ____currentState_3; }
	inline int32_t* get_address_of__currentState_3() { return &____currentState_3; }
	inline void set__currentState_3(int32_t value)
	{
		____currentState_3 = value;
	}

	inline static int32_t get_offset_of__currentPosition_4() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____currentPosition_4)); }
	inline JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD  get__currentPosition_4() const { return ____currentPosition_4; }
	inline JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD * get_address_of__currentPosition_4() { return &____currentPosition_4; }
	inline void set__currentPosition_4(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD  value)
	{
		____currentPosition_4 = value;
	}

	inline static int32_t get_offset_of__culture_5() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____culture_5)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_5() const { return ____culture_5; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_5() { return &____culture_5; }
	inline void set__culture_5(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_5 = value;
		Il2CppCodeGenWriteBarrier((&____culture_5), value);
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_6() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____dateTimeZoneHandling_6)); }
	inline int32_t get__dateTimeZoneHandling_6() const { return ____dateTimeZoneHandling_6; }
	inline int32_t* get_address_of__dateTimeZoneHandling_6() { return &____dateTimeZoneHandling_6; }
	inline void set__dateTimeZoneHandling_6(int32_t value)
	{
		____dateTimeZoneHandling_6 = value;
	}

	inline static int32_t get_offset_of__maxDepth_7() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____maxDepth_7)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get__maxDepth_7() const { return ____maxDepth_7; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of__maxDepth_7() { return &____maxDepth_7; }
	inline void set__maxDepth_7(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		____maxDepth_7 = value;
	}

	inline static int32_t get_offset_of__hasExceededMaxDepth_8() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____hasExceededMaxDepth_8)); }
	inline bool get__hasExceededMaxDepth_8() const { return ____hasExceededMaxDepth_8; }
	inline bool* get_address_of__hasExceededMaxDepth_8() { return &____hasExceededMaxDepth_8; }
	inline void set__hasExceededMaxDepth_8(bool value)
	{
		____hasExceededMaxDepth_8 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_9() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____dateParseHandling_9)); }
	inline int32_t get__dateParseHandling_9() const { return ____dateParseHandling_9; }
	inline int32_t* get_address_of__dateParseHandling_9() { return &____dateParseHandling_9; }
	inline void set__dateParseHandling_9(int32_t value)
	{
		____dateParseHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_10() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____floatParseHandling_10)); }
	inline int32_t get__floatParseHandling_10() const { return ____floatParseHandling_10; }
	inline int32_t* get_address_of__floatParseHandling_10() { return &____floatParseHandling_10; }
	inline void set__floatParseHandling_10(int32_t value)
	{
		____floatParseHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__stack_12() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____stack_12)); }
	inline List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 * get__stack_12() const { return ____stack_12; }
	inline List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 ** get_address_of__stack_12() { return &____stack_12; }
	inline void set__stack_12(List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 * value)
	{
		____stack_12 = value;
		Il2CppCodeGenWriteBarrier((&____stack_12), value);
	}

	inline static int32_t get_offset_of_U3CCloseInputU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ___U3CCloseInputU3Ek__BackingField_13)); }
	inline bool get_U3CCloseInputU3Ek__BackingField_13() const { return ___U3CCloseInputU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CCloseInputU3Ek__BackingField_13() { return &___U3CCloseInputU3Ek__BackingField_13; }
	inline void set_U3CCloseInputU3Ek__BackingField_13(bool value)
	{
		___U3CCloseInputU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ___U3CSupportMultipleContentU3Ek__BackingField_14)); }
	inline bool get_U3CSupportMultipleContentU3Ek__BackingField_14() const { return ___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return &___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline void set_U3CSupportMultipleContentU3Ek__BackingField_14(bool value)
	{
		___U3CSupportMultipleContentU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADER_TF29A2AB439002FDEFB9592AA3147F02ADB3841DC_H
#ifndef JSONSERIALIZER_T6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB_H
#define JSONSERIALIZER_T6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonSerializer
struct  JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB  : public RuntimeObject
{
public:
	// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializer::_typeNameHandling
	int32_t ____typeNameHandling_0;
	// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle Newtonsoft.Json.JsonSerializer::_typeNameAssemblyFormat
	int32_t ____typeNameAssemblyFormat_1;
	// Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.JsonSerializer::_preserveReferencesHandling
	int32_t ____preserveReferencesHandling_2;
	// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializer::_referenceLoopHandling
	int32_t ____referenceLoopHandling_3;
	// Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.JsonSerializer::_missingMemberHandling
	int32_t ____missingMemberHandling_4;
	// Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonSerializer::_objectCreationHandling
	int32_t ____objectCreationHandling_5;
	// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializer::_nullValueHandling
	int32_t ____nullValueHandling_6;
	// Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonSerializer::_defaultValueHandling
	int32_t ____defaultValueHandling_7;
	// Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializer::_constructorHandling
	int32_t ____constructorHandling_8;
	// Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.JsonSerializer::_metadataPropertyHandling
	int32_t ____metadataPropertyHandling_9;
	// Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.JsonSerializer::_converters
	JsonConverterCollection_tE01B17B4897D2254EC50F208B6AA43A938298ABF * ____converters_10;
	// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializer::_contractResolver
	RuntimeObject* ____contractResolver_11;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializer::_traceWriter
	RuntimeObject* ____traceWriter_12;
	// System.Collections.IEqualityComparer Newtonsoft.Json.JsonSerializer::_equalityComparer
	RuntimeObject* ____equalityComparer_13;
	// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializer::_binder
	SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * ____binder_14;
	// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializer::_context
	StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  ____context_15;
	// Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializer::_referenceResolver
	RuntimeObject* ____referenceResolver_16;
	// System.Nullable`1<Newtonsoft.Json.Formatting> Newtonsoft.Json.JsonSerializer::_formatting
	Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059  ____formatting_17;
	// System.Nullable`1<Newtonsoft.Json.DateFormatHandling> Newtonsoft.Json.JsonSerializer::_dateFormatHandling
	Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660  ____dateFormatHandling_18;
	// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling> Newtonsoft.Json.JsonSerializer::_dateTimeZoneHandling
	Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE  ____dateTimeZoneHandling_19;
	// System.Nullable`1<Newtonsoft.Json.DateParseHandling> Newtonsoft.Json.JsonSerializer::_dateParseHandling
	Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF  ____dateParseHandling_20;
	// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling> Newtonsoft.Json.JsonSerializer::_floatFormatHandling
	Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810  ____floatFormatHandling_21;
	// System.Nullable`1<Newtonsoft.Json.FloatParseHandling> Newtonsoft.Json.JsonSerializer::_floatParseHandling
	Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6  ____floatParseHandling_22;
	// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling> Newtonsoft.Json.JsonSerializer::_stringEscapeHandling
	Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E  ____stringEscapeHandling_23;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonSerializer::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_24;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonSerializer::_maxDepth
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ____maxDepth_25;
	// System.Boolean Newtonsoft.Json.JsonSerializer::_maxDepthSet
	bool ____maxDepthSet_26;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonSerializer::_checkAdditionalContent
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____checkAdditionalContent_27;
	// System.String Newtonsoft.Json.JsonSerializer::_dateFormatString
	String_t* ____dateFormatString_28;
	// System.Boolean Newtonsoft.Json.JsonSerializer::_dateFormatStringSet
	bool ____dateFormatStringSet_29;
	// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializer::Error
	EventHandler_1_tD0863E67C9A50D35387D2C3E7DF644C8E96F5B9A * ___Error_30;

public:
	inline static int32_t get_offset_of__typeNameHandling_0() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____typeNameHandling_0)); }
	inline int32_t get__typeNameHandling_0() const { return ____typeNameHandling_0; }
	inline int32_t* get_address_of__typeNameHandling_0() { return &____typeNameHandling_0; }
	inline void set__typeNameHandling_0(int32_t value)
	{
		____typeNameHandling_0 = value;
	}

	inline static int32_t get_offset_of__typeNameAssemblyFormat_1() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____typeNameAssemblyFormat_1)); }
	inline int32_t get__typeNameAssemblyFormat_1() const { return ____typeNameAssemblyFormat_1; }
	inline int32_t* get_address_of__typeNameAssemblyFormat_1() { return &____typeNameAssemblyFormat_1; }
	inline void set__typeNameAssemblyFormat_1(int32_t value)
	{
		____typeNameAssemblyFormat_1 = value;
	}

	inline static int32_t get_offset_of__preserveReferencesHandling_2() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____preserveReferencesHandling_2)); }
	inline int32_t get__preserveReferencesHandling_2() const { return ____preserveReferencesHandling_2; }
	inline int32_t* get_address_of__preserveReferencesHandling_2() { return &____preserveReferencesHandling_2; }
	inline void set__preserveReferencesHandling_2(int32_t value)
	{
		____preserveReferencesHandling_2 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_3() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____referenceLoopHandling_3)); }
	inline int32_t get__referenceLoopHandling_3() const { return ____referenceLoopHandling_3; }
	inline int32_t* get_address_of__referenceLoopHandling_3() { return &____referenceLoopHandling_3; }
	inline void set__referenceLoopHandling_3(int32_t value)
	{
		____referenceLoopHandling_3 = value;
	}

	inline static int32_t get_offset_of__missingMemberHandling_4() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____missingMemberHandling_4)); }
	inline int32_t get__missingMemberHandling_4() const { return ____missingMemberHandling_4; }
	inline int32_t* get_address_of__missingMemberHandling_4() { return &____missingMemberHandling_4; }
	inline void set__missingMemberHandling_4(int32_t value)
	{
		____missingMemberHandling_4 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_5() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____objectCreationHandling_5)); }
	inline int32_t get__objectCreationHandling_5() const { return ____objectCreationHandling_5; }
	inline int32_t* get_address_of__objectCreationHandling_5() { return &____objectCreationHandling_5; }
	inline void set__objectCreationHandling_5(int32_t value)
	{
		____objectCreationHandling_5 = value;
	}

	inline static int32_t get_offset_of__nullValueHandling_6() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____nullValueHandling_6)); }
	inline int32_t get__nullValueHandling_6() const { return ____nullValueHandling_6; }
	inline int32_t* get_address_of__nullValueHandling_6() { return &____nullValueHandling_6; }
	inline void set__nullValueHandling_6(int32_t value)
	{
		____nullValueHandling_6 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_7() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____defaultValueHandling_7)); }
	inline int32_t get__defaultValueHandling_7() const { return ____defaultValueHandling_7; }
	inline int32_t* get_address_of__defaultValueHandling_7() { return &____defaultValueHandling_7; }
	inline void set__defaultValueHandling_7(int32_t value)
	{
		____defaultValueHandling_7 = value;
	}

	inline static int32_t get_offset_of__constructorHandling_8() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____constructorHandling_8)); }
	inline int32_t get__constructorHandling_8() const { return ____constructorHandling_8; }
	inline int32_t* get_address_of__constructorHandling_8() { return &____constructorHandling_8; }
	inline void set__constructorHandling_8(int32_t value)
	{
		____constructorHandling_8 = value;
	}

	inline static int32_t get_offset_of__metadataPropertyHandling_9() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____metadataPropertyHandling_9)); }
	inline int32_t get__metadataPropertyHandling_9() const { return ____metadataPropertyHandling_9; }
	inline int32_t* get_address_of__metadataPropertyHandling_9() { return &____metadataPropertyHandling_9; }
	inline void set__metadataPropertyHandling_9(int32_t value)
	{
		____metadataPropertyHandling_9 = value;
	}

	inline static int32_t get_offset_of__converters_10() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____converters_10)); }
	inline JsonConverterCollection_tE01B17B4897D2254EC50F208B6AA43A938298ABF * get__converters_10() const { return ____converters_10; }
	inline JsonConverterCollection_tE01B17B4897D2254EC50F208B6AA43A938298ABF ** get_address_of__converters_10() { return &____converters_10; }
	inline void set__converters_10(JsonConverterCollection_tE01B17B4897D2254EC50F208B6AA43A938298ABF * value)
	{
		____converters_10 = value;
		Il2CppCodeGenWriteBarrier((&____converters_10), value);
	}

	inline static int32_t get_offset_of__contractResolver_11() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____contractResolver_11)); }
	inline RuntimeObject* get__contractResolver_11() const { return ____contractResolver_11; }
	inline RuntimeObject** get_address_of__contractResolver_11() { return &____contractResolver_11; }
	inline void set__contractResolver_11(RuntimeObject* value)
	{
		____contractResolver_11 = value;
		Il2CppCodeGenWriteBarrier((&____contractResolver_11), value);
	}

	inline static int32_t get_offset_of__traceWriter_12() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____traceWriter_12)); }
	inline RuntimeObject* get__traceWriter_12() const { return ____traceWriter_12; }
	inline RuntimeObject** get_address_of__traceWriter_12() { return &____traceWriter_12; }
	inline void set__traceWriter_12(RuntimeObject* value)
	{
		____traceWriter_12 = value;
		Il2CppCodeGenWriteBarrier((&____traceWriter_12), value);
	}

	inline static int32_t get_offset_of__equalityComparer_13() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____equalityComparer_13)); }
	inline RuntimeObject* get__equalityComparer_13() const { return ____equalityComparer_13; }
	inline RuntimeObject** get_address_of__equalityComparer_13() { return &____equalityComparer_13; }
	inline void set__equalityComparer_13(RuntimeObject* value)
	{
		____equalityComparer_13 = value;
		Il2CppCodeGenWriteBarrier((&____equalityComparer_13), value);
	}

	inline static int32_t get_offset_of__binder_14() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____binder_14)); }
	inline SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * get__binder_14() const { return ____binder_14; }
	inline SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 ** get_address_of__binder_14() { return &____binder_14; }
	inline void set__binder_14(SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * value)
	{
		____binder_14 = value;
		Il2CppCodeGenWriteBarrier((&____binder_14), value);
	}

	inline static int32_t get_offset_of__context_15() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____context_15)); }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  get__context_15() const { return ____context_15; }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 * get_address_of__context_15() { return &____context_15; }
	inline void set__context_15(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  value)
	{
		____context_15 = value;
	}

	inline static int32_t get_offset_of__referenceResolver_16() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____referenceResolver_16)); }
	inline RuntimeObject* get__referenceResolver_16() const { return ____referenceResolver_16; }
	inline RuntimeObject** get_address_of__referenceResolver_16() { return &____referenceResolver_16; }
	inline void set__referenceResolver_16(RuntimeObject* value)
	{
		____referenceResolver_16 = value;
		Il2CppCodeGenWriteBarrier((&____referenceResolver_16), value);
	}

	inline static int32_t get_offset_of__formatting_17() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____formatting_17)); }
	inline Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059  get__formatting_17() const { return ____formatting_17; }
	inline Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059 * get_address_of__formatting_17() { return &____formatting_17; }
	inline void set__formatting_17(Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059  value)
	{
		____formatting_17 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_18() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____dateFormatHandling_18)); }
	inline Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660  get__dateFormatHandling_18() const { return ____dateFormatHandling_18; }
	inline Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660 * get_address_of__dateFormatHandling_18() { return &____dateFormatHandling_18; }
	inline void set__dateFormatHandling_18(Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660  value)
	{
		____dateFormatHandling_18 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_19() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____dateTimeZoneHandling_19)); }
	inline Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE  get__dateTimeZoneHandling_19() const { return ____dateTimeZoneHandling_19; }
	inline Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE * get_address_of__dateTimeZoneHandling_19() { return &____dateTimeZoneHandling_19; }
	inline void set__dateTimeZoneHandling_19(Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE  value)
	{
		____dateTimeZoneHandling_19 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_20() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____dateParseHandling_20)); }
	inline Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF  get__dateParseHandling_20() const { return ____dateParseHandling_20; }
	inline Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF * get_address_of__dateParseHandling_20() { return &____dateParseHandling_20; }
	inline void set__dateParseHandling_20(Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF  value)
	{
		____dateParseHandling_20 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_21() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____floatFormatHandling_21)); }
	inline Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810  get__floatFormatHandling_21() const { return ____floatFormatHandling_21; }
	inline Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810 * get_address_of__floatFormatHandling_21() { return &____floatFormatHandling_21; }
	inline void set__floatFormatHandling_21(Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810  value)
	{
		____floatFormatHandling_21 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_22() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____floatParseHandling_22)); }
	inline Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6  get__floatParseHandling_22() const { return ____floatParseHandling_22; }
	inline Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6 * get_address_of__floatParseHandling_22() { return &____floatParseHandling_22; }
	inline void set__floatParseHandling_22(Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6  value)
	{
		____floatParseHandling_22 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_23() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____stringEscapeHandling_23)); }
	inline Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E  get__stringEscapeHandling_23() const { return ____stringEscapeHandling_23; }
	inline Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E * get_address_of__stringEscapeHandling_23() { return &____stringEscapeHandling_23; }
	inline void set__stringEscapeHandling_23(Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E  value)
	{
		____stringEscapeHandling_23 = value;
	}

	inline static int32_t get_offset_of__culture_24() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____culture_24)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_24() const { return ____culture_24; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_24() { return &____culture_24; }
	inline void set__culture_24(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_24 = value;
		Il2CppCodeGenWriteBarrier((&____culture_24), value);
	}

	inline static int32_t get_offset_of__maxDepth_25() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____maxDepth_25)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get__maxDepth_25() const { return ____maxDepth_25; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of__maxDepth_25() { return &____maxDepth_25; }
	inline void set__maxDepth_25(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		____maxDepth_25 = value;
	}

	inline static int32_t get_offset_of__maxDepthSet_26() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____maxDepthSet_26)); }
	inline bool get__maxDepthSet_26() const { return ____maxDepthSet_26; }
	inline bool* get_address_of__maxDepthSet_26() { return &____maxDepthSet_26; }
	inline void set__maxDepthSet_26(bool value)
	{
		____maxDepthSet_26 = value;
	}

	inline static int32_t get_offset_of__checkAdditionalContent_27() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____checkAdditionalContent_27)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__checkAdditionalContent_27() const { return ____checkAdditionalContent_27; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__checkAdditionalContent_27() { return &____checkAdditionalContent_27; }
	inline void set__checkAdditionalContent_27(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____checkAdditionalContent_27 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_28() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____dateFormatString_28)); }
	inline String_t* get__dateFormatString_28() const { return ____dateFormatString_28; }
	inline String_t** get_address_of__dateFormatString_28() { return &____dateFormatString_28; }
	inline void set__dateFormatString_28(String_t* value)
	{
		____dateFormatString_28 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_28), value);
	}

	inline static int32_t get_offset_of__dateFormatStringSet_29() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____dateFormatStringSet_29)); }
	inline bool get__dateFormatStringSet_29() const { return ____dateFormatStringSet_29; }
	inline bool* get_address_of__dateFormatStringSet_29() { return &____dateFormatStringSet_29; }
	inline void set__dateFormatStringSet_29(bool value)
	{
		____dateFormatStringSet_29 = value;
	}

	inline static int32_t get_offset_of_Error_30() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ___Error_30)); }
	inline EventHandler_1_tD0863E67C9A50D35387D2C3E7DF644C8E96F5B9A * get_Error_30() const { return ___Error_30; }
	inline EventHandler_1_tD0863E67C9A50D35387D2C3E7DF644C8E96F5B9A ** get_address_of_Error_30() { return &___Error_30; }
	inline void set_Error_30(EventHandler_1_tD0863E67C9A50D35387D2C3E7DF644C8E96F5B9A * value)
	{
		___Error_30 = value;
		Il2CppCodeGenWriteBarrier((&___Error_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZER_T6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB_H
#ifndef JSONWRITER_T4CF8C6EC696955A38B904D71B2AB1C6ED6155091_H
#define JSONWRITER_T4CF8C6EC696955A38B904D71B2AB1C6ED6155091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter
struct  JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonWriter::_stack
	List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 * ____stack_2;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonWriter::_currentPosition
	JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD  ____currentPosition_3;
	// Newtonsoft.Json.JsonWriter/State Newtonsoft.Json.JsonWriter::_currentState
	int32_t ____currentState_4;
	// Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::_formatting
	int32_t ____formatting_5;
	// System.Boolean Newtonsoft.Json.JsonWriter::<CloseOutput>k__BackingField
	bool ___U3CCloseOutputU3Ek__BackingField_6;
	// Newtonsoft.Json.DateFormatHandling Newtonsoft.Json.JsonWriter::_dateFormatHandling
	int32_t ____dateFormatHandling_7;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonWriter::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_8;
	// Newtonsoft.Json.StringEscapeHandling Newtonsoft.Json.JsonWriter::_stringEscapeHandling
	int32_t ____stringEscapeHandling_9;
	// Newtonsoft.Json.FloatFormatHandling Newtonsoft.Json.JsonWriter::_floatFormatHandling
	int32_t ____floatFormatHandling_10;
	// System.String Newtonsoft.Json.JsonWriter::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonWriter::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_12;

public:
	inline static int32_t get_offset_of__stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____stack_2)); }
	inline List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 * get__stack_2() const { return ____stack_2; }
	inline List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 ** get_address_of__stack_2() { return &____stack_2; }
	inline void set__stack_2(List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 * value)
	{
		____stack_2 = value;
		Il2CppCodeGenWriteBarrier((&____stack_2), value);
	}

	inline static int32_t get_offset_of__currentPosition_3() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____currentPosition_3)); }
	inline JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD  get__currentPosition_3() const { return ____currentPosition_3; }
	inline JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD * get_address_of__currentPosition_3() { return &____currentPosition_3; }
	inline void set__currentPosition_3(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD  value)
	{
		____currentPosition_3 = value;
	}

	inline static int32_t get_offset_of__currentState_4() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____currentState_4)); }
	inline int32_t get__currentState_4() const { return ____currentState_4; }
	inline int32_t* get_address_of__currentState_4() { return &____currentState_4; }
	inline void set__currentState_4(int32_t value)
	{
		____currentState_4 = value;
	}

	inline static int32_t get_offset_of__formatting_5() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____formatting_5)); }
	inline int32_t get__formatting_5() const { return ____formatting_5; }
	inline int32_t* get_address_of__formatting_5() { return &____formatting_5; }
	inline void set__formatting_5(int32_t value)
	{
		____formatting_5 = value;
	}

	inline static int32_t get_offset_of_U3CCloseOutputU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ___U3CCloseOutputU3Ek__BackingField_6)); }
	inline bool get_U3CCloseOutputU3Ek__BackingField_6() const { return ___U3CCloseOutputU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CCloseOutputU3Ek__BackingField_6() { return &___U3CCloseOutputU3Ek__BackingField_6; }
	inline void set_U3CCloseOutputU3Ek__BackingField_6(bool value)
	{
		___U3CCloseOutputU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_7() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____dateFormatHandling_7)); }
	inline int32_t get__dateFormatHandling_7() const { return ____dateFormatHandling_7; }
	inline int32_t* get_address_of__dateFormatHandling_7() { return &____dateFormatHandling_7; }
	inline void set__dateFormatHandling_7(int32_t value)
	{
		____dateFormatHandling_7 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_8() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____dateTimeZoneHandling_8)); }
	inline int32_t get__dateTimeZoneHandling_8() const { return ____dateTimeZoneHandling_8; }
	inline int32_t* get_address_of__dateTimeZoneHandling_8() { return &____dateTimeZoneHandling_8; }
	inline void set__dateTimeZoneHandling_8(int32_t value)
	{
		____dateTimeZoneHandling_8 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_9() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____stringEscapeHandling_9)); }
	inline int32_t get__stringEscapeHandling_9() const { return ____stringEscapeHandling_9; }
	inline int32_t* get_address_of__stringEscapeHandling_9() { return &____stringEscapeHandling_9; }
	inline void set__stringEscapeHandling_9(int32_t value)
	{
		____stringEscapeHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_10() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____floatFormatHandling_10)); }
	inline int32_t get__floatFormatHandling_10() const { return ____floatFormatHandling_10; }
	inline int32_t* get_address_of__floatFormatHandling_10() { return &____floatFormatHandling_10; }
	inline void set__floatFormatHandling_10(int32_t value)
	{
		____floatFormatHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__culture_12() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____culture_12)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_12() const { return ____culture_12; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_12() { return &____culture_12; }
	inline void set__culture_12(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_12 = value;
		Il2CppCodeGenWriteBarrier((&____culture_12), value);
	}
};

struct JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091_StaticFields
{
public:
	// Newtonsoft.Json.JsonWriter/State[][] Newtonsoft.Json.JsonWriter::StateArray
	StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* ___StateArray_0;
	// Newtonsoft.Json.JsonWriter/State[][] Newtonsoft.Json.JsonWriter::StateArrayTempate
	StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* ___StateArrayTempate_1;

public:
	inline static int32_t get_offset_of_StateArray_0() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091_StaticFields, ___StateArray_0)); }
	inline StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* get_StateArray_0() const { return ___StateArray_0; }
	inline StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84** get_address_of_StateArray_0() { return &___StateArray_0; }
	inline void set_StateArray_0(StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* value)
	{
		___StateArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___StateArray_0), value);
	}

	inline static int32_t get_offset_of_StateArrayTempate_1() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091_StaticFields, ___StateArrayTempate_1)); }
	inline StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* get_StateArrayTempate_1() const { return ___StateArrayTempate_1; }
	inline StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84** get_address_of_StateArrayTempate_1() { return &___StateArrayTempate_1; }
	inline void set_StateArrayTempate_1(StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* value)
	{
		___StateArrayTempate_1 = value;
		Il2CppCodeGenWriteBarrier((&___StateArrayTempate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T4CF8C6EC696955A38B904D71B2AB1C6ED6155091_H
#ifndef NULLABLE_1_TCF22087D771FCA8102A51C976A8992F7F8AE27B8_H
#define NULLABLE_1_TCF22087D771FCA8102A51C976A8992F7F8AE27B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Runtime.Serialization.StreamingContext>
struct  Nullable_1_tCF22087D771FCA8102A51C976A8992F7F8AE27B8 
{
public:
	// T System.Nullable`1::value
	StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tCF22087D771FCA8102A51C976A8992F7F8AE27B8, ___value_0)); }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  get_value_0() const { return ___value_0; }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tCF22087D771FCA8102A51C976A8992F7F8AE27B8, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TCF22087D771FCA8102A51C976A8992F7F8AE27B8_H
#ifndef JSONARRAYATTRIBUTE_TDC53BD61D6BC8946022A92716BC3E905688CE52E_H
#define JSONARRAYATTRIBUTE_TDC53BD61D6BC8946022A92716BC3E905688CE52E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonArrayAttribute
struct  JsonArrayAttribute_tDC53BD61D6BC8946022A92716BC3E905688CE52E  : public JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAYATTRIBUTE_TDC53BD61D6BC8946022A92716BC3E905688CE52E_H
#ifndef JSONDICTIONARYATTRIBUTE_TAEA3F708F0DAE3AAE16AFE68603A68F6DE770FA5_H
#define JSONDICTIONARYATTRIBUTE_TAEA3F708F0DAE3AAE16AFE68603A68F6DE770FA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonDictionaryAttribute
struct  JsonDictionaryAttribute_tAEA3F708F0DAE3AAE16AFE68603A68F6DE770FA5  : public JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDICTIONARYATTRIBUTE_TAEA3F708F0DAE3AAE16AFE68603A68F6DE770FA5_H
#ifndef JSONOBJECTATTRIBUTE_TC8E1A560528FCFC01AA594F9830039913CEEEFCC_H
#define JSONOBJECTATTRIBUTE_TC8E1A560528FCFC01AA594F9830039913CEEEFCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonObjectAttribute
struct  JsonObjectAttribute_tC8E1A560528FCFC01AA594F9830039913CEEEFCC  : public JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10
{
public:
	// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.JsonObjectAttribute::_memberSerialization
	int32_t ____memberSerialization_9;
	// System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.JsonObjectAttribute::_itemRequired
	Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D  ____itemRequired_10;

public:
	inline static int32_t get_offset_of__memberSerialization_9() { return static_cast<int32_t>(offsetof(JsonObjectAttribute_tC8E1A560528FCFC01AA594F9830039913CEEEFCC, ____memberSerialization_9)); }
	inline int32_t get__memberSerialization_9() const { return ____memberSerialization_9; }
	inline int32_t* get_address_of__memberSerialization_9() { return &____memberSerialization_9; }
	inline void set__memberSerialization_9(int32_t value)
	{
		____memberSerialization_9 = value;
	}

	inline static int32_t get_offset_of__itemRequired_10() { return static_cast<int32_t>(offsetof(JsonObjectAttribute_tC8E1A560528FCFC01AA594F9830039913CEEEFCC, ____itemRequired_10)); }
	inline Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D  get__itemRequired_10() const { return ____itemRequired_10; }
	inline Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D * get_address_of__itemRequired_10() { return &____itemRequired_10; }
	inline void set__itemRequired_10(Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D  value)
	{
		____itemRequired_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECTATTRIBUTE_TC8E1A560528FCFC01AA594F9830039913CEEEFCC_H
#ifndef JSONSERIALIZERSETTINGS_T616936620D9972F1A53B6F6D6E35E91A87DD11F0_H
#define JSONSERIALIZERSETTINGS_T616936620D9972F1A53B6F6D6E35E91A87DD11F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonSerializerSettings
struct  JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0  : public RuntimeObject
{
public:
	// System.Nullable`1<Newtonsoft.Json.Formatting> Newtonsoft.Json.JsonSerializerSettings::_formatting
	Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059  ____formatting_2;
	// System.Nullable`1<Newtonsoft.Json.DateFormatHandling> Newtonsoft.Json.JsonSerializerSettings::_dateFormatHandling
	Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660  ____dateFormatHandling_3;
	// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling> Newtonsoft.Json.JsonSerializerSettings::_dateTimeZoneHandling
	Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE  ____dateTimeZoneHandling_4;
	// System.Nullable`1<Newtonsoft.Json.DateParseHandling> Newtonsoft.Json.JsonSerializerSettings::_dateParseHandling
	Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF  ____dateParseHandling_5;
	// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling> Newtonsoft.Json.JsonSerializerSettings::_floatFormatHandling
	Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810  ____floatFormatHandling_6;
	// System.Nullable`1<Newtonsoft.Json.FloatParseHandling> Newtonsoft.Json.JsonSerializerSettings::_floatParseHandling
	Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6  ____floatParseHandling_7;
	// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling> Newtonsoft.Json.JsonSerializerSettings::_stringEscapeHandling
	Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E  ____stringEscapeHandling_8;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonSerializerSettings::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_9;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonSerializerSettings::_checkAdditionalContent
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____checkAdditionalContent_10;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonSerializerSettings::_maxDepth
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ____maxDepth_11;
	// System.Boolean Newtonsoft.Json.JsonSerializerSettings::_maxDepthSet
	bool ____maxDepthSet_12;
	// System.String Newtonsoft.Json.JsonSerializerSettings::_dateFormatString
	String_t* ____dateFormatString_13;
	// System.Boolean Newtonsoft.Json.JsonSerializerSettings::_dateFormatStringSet
	bool ____dateFormatStringSet_14;
	// System.Nullable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle> Newtonsoft.Json.JsonSerializerSettings::_typeNameAssemblyFormat
	Nullable_1_tCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74  ____typeNameAssemblyFormat_15;
	// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.JsonSerializerSettings::_defaultValueHandling
	Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA  ____defaultValueHandling_16;
	// System.Nullable`1<Newtonsoft.Json.PreserveReferencesHandling> Newtonsoft.Json.JsonSerializerSettings::_preserveReferencesHandling
	Nullable_1_t4DC01BF01ED0BB2AA2B73540F63FC0D48E3B387A  ____preserveReferencesHandling_17;
	// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.JsonSerializerSettings::_nullValueHandling
	Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA  ____nullValueHandling_18;
	// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.JsonSerializerSettings::_objectCreationHandling
	Nullable_1_t43CD81743444619B643C0A3912749713399518C5  ____objectCreationHandling_19;
	// System.Nullable`1<Newtonsoft.Json.MissingMemberHandling> Newtonsoft.Json.JsonSerializerSettings::_missingMemberHandling
	Nullable_1_t3ECC38B72B3C0270EC5B32F255134E67C8EE5013  ____missingMemberHandling_20;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.JsonSerializerSettings::_referenceLoopHandling
	Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  ____referenceLoopHandling_21;
	// System.Nullable`1<System.Runtime.Serialization.StreamingContext> Newtonsoft.Json.JsonSerializerSettings::_context
	Nullable_1_tCF22087D771FCA8102A51C976A8992F7F8AE27B8  ____context_22;
	// System.Nullable`1<Newtonsoft.Json.ConstructorHandling> Newtonsoft.Json.JsonSerializerSettings::_constructorHandling
	Nullable_1_t386D7565CF3D349D5FEFDBB687254FC491B70AD5  ____constructorHandling_23;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.JsonSerializerSettings::_typeNameHandling
	Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  ____typeNameHandling_24;
	// System.Nullable`1<Newtonsoft.Json.MetadataPropertyHandling> Newtonsoft.Json.JsonSerializerSettings::_metadataPropertyHandling
	Nullable_1_tF330C1A3D6D96AD89924DABE832ED2C68921413B  ____metadataPropertyHandling_25;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter> Newtonsoft.Json.JsonSerializerSettings::<Converters>k__BackingField
	RuntimeObject* ___U3CConvertersU3Ek__BackingField_26;
	// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializerSettings::<ContractResolver>k__BackingField
	RuntimeObject* ___U3CContractResolverU3Ek__BackingField_27;
	// System.Collections.IEqualityComparer Newtonsoft.Json.JsonSerializerSettings::<EqualityComparer>k__BackingField
	RuntimeObject* ___U3CEqualityComparerU3Ek__BackingField_28;
	// System.Func`1<Newtonsoft.Json.Serialization.IReferenceResolver> Newtonsoft.Json.JsonSerializerSettings::<ReferenceResolverProvider>k__BackingField
	Func_1_t08DED721C4774B7A9EB8B6792D1895E5E0B7A64A * ___U3CReferenceResolverProviderU3Ek__BackingField_29;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializerSettings::<TraceWriter>k__BackingField
	RuntimeObject* ___U3CTraceWriterU3Ek__BackingField_30;
	// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializerSettings::<Binder>k__BackingField
	SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * ___U3CBinderU3Ek__BackingField_31;
	// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializerSettings::<Error>k__BackingField
	EventHandler_1_tD0863E67C9A50D35387D2C3E7DF644C8E96F5B9A * ___U3CErrorU3Ek__BackingField_32;

public:
	inline static int32_t get_offset_of__formatting_2() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____formatting_2)); }
	inline Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059  get__formatting_2() const { return ____formatting_2; }
	inline Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059 * get_address_of__formatting_2() { return &____formatting_2; }
	inline void set__formatting_2(Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059  value)
	{
		____formatting_2 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_3() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____dateFormatHandling_3)); }
	inline Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660  get__dateFormatHandling_3() const { return ____dateFormatHandling_3; }
	inline Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660 * get_address_of__dateFormatHandling_3() { return &____dateFormatHandling_3; }
	inline void set__dateFormatHandling_3(Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660  value)
	{
		____dateFormatHandling_3 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_4() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____dateTimeZoneHandling_4)); }
	inline Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE  get__dateTimeZoneHandling_4() const { return ____dateTimeZoneHandling_4; }
	inline Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE * get_address_of__dateTimeZoneHandling_4() { return &____dateTimeZoneHandling_4; }
	inline void set__dateTimeZoneHandling_4(Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE  value)
	{
		____dateTimeZoneHandling_4 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_5() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____dateParseHandling_5)); }
	inline Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF  get__dateParseHandling_5() const { return ____dateParseHandling_5; }
	inline Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF * get_address_of__dateParseHandling_5() { return &____dateParseHandling_5; }
	inline void set__dateParseHandling_5(Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF  value)
	{
		____dateParseHandling_5 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_6() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____floatFormatHandling_6)); }
	inline Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810  get__floatFormatHandling_6() const { return ____floatFormatHandling_6; }
	inline Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810 * get_address_of__floatFormatHandling_6() { return &____floatFormatHandling_6; }
	inline void set__floatFormatHandling_6(Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810  value)
	{
		____floatFormatHandling_6 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_7() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____floatParseHandling_7)); }
	inline Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6  get__floatParseHandling_7() const { return ____floatParseHandling_7; }
	inline Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6 * get_address_of__floatParseHandling_7() { return &____floatParseHandling_7; }
	inline void set__floatParseHandling_7(Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6  value)
	{
		____floatParseHandling_7 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_8() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____stringEscapeHandling_8)); }
	inline Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E  get__stringEscapeHandling_8() const { return ____stringEscapeHandling_8; }
	inline Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E * get_address_of__stringEscapeHandling_8() { return &____stringEscapeHandling_8; }
	inline void set__stringEscapeHandling_8(Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E  value)
	{
		____stringEscapeHandling_8 = value;
	}

	inline static int32_t get_offset_of__culture_9() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____culture_9)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_9() const { return ____culture_9; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_9() { return &____culture_9; }
	inline void set__culture_9(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_9 = value;
		Il2CppCodeGenWriteBarrier((&____culture_9), value);
	}

	inline static int32_t get_offset_of__checkAdditionalContent_10() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____checkAdditionalContent_10)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__checkAdditionalContent_10() const { return ____checkAdditionalContent_10; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__checkAdditionalContent_10() { return &____checkAdditionalContent_10; }
	inline void set__checkAdditionalContent_10(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____checkAdditionalContent_10 = value;
	}

	inline static int32_t get_offset_of__maxDepth_11() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____maxDepth_11)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get__maxDepth_11() const { return ____maxDepth_11; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of__maxDepth_11() { return &____maxDepth_11; }
	inline void set__maxDepth_11(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		____maxDepth_11 = value;
	}

	inline static int32_t get_offset_of__maxDepthSet_12() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____maxDepthSet_12)); }
	inline bool get__maxDepthSet_12() const { return ____maxDepthSet_12; }
	inline bool* get_address_of__maxDepthSet_12() { return &____maxDepthSet_12; }
	inline void set__maxDepthSet_12(bool value)
	{
		____maxDepthSet_12 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_13() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____dateFormatString_13)); }
	inline String_t* get__dateFormatString_13() const { return ____dateFormatString_13; }
	inline String_t** get_address_of__dateFormatString_13() { return &____dateFormatString_13; }
	inline void set__dateFormatString_13(String_t* value)
	{
		____dateFormatString_13 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_13), value);
	}

	inline static int32_t get_offset_of__dateFormatStringSet_14() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____dateFormatStringSet_14)); }
	inline bool get__dateFormatStringSet_14() const { return ____dateFormatStringSet_14; }
	inline bool* get_address_of__dateFormatStringSet_14() { return &____dateFormatStringSet_14; }
	inline void set__dateFormatStringSet_14(bool value)
	{
		____dateFormatStringSet_14 = value;
	}

	inline static int32_t get_offset_of__typeNameAssemblyFormat_15() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____typeNameAssemblyFormat_15)); }
	inline Nullable_1_tCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74  get__typeNameAssemblyFormat_15() const { return ____typeNameAssemblyFormat_15; }
	inline Nullable_1_tCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74 * get_address_of__typeNameAssemblyFormat_15() { return &____typeNameAssemblyFormat_15; }
	inline void set__typeNameAssemblyFormat_15(Nullable_1_tCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74  value)
	{
		____typeNameAssemblyFormat_15 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_16() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____defaultValueHandling_16)); }
	inline Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA  get__defaultValueHandling_16() const { return ____defaultValueHandling_16; }
	inline Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA * get_address_of__defaultValueHandling_16() { return &____defaultValueHandling_16; }
	inline void set__defaultValueHandling_16(Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA  value)
	{
		____defaultValueHandling_16 = value;
	}

	inline static int32_t get_offset_of__preserveReferencesHandling_17() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____preserveReferencesHandling_17)); }
	inline Nullable_1_t4DC01BF01ED0BB2AA2B73540F63FC0D48E3B387A  get__preserveReferencesHandling_17() const { return ____preserveReferencesHandling_17; }
	inline Nullable_1_t4DC01BF01ED0BB2AA2B73540F63FC0D48E3B387A * get_address_of__preserveReferencesHandling_17() { return &____preserveReferencesHandling_17; }
	inline void set__preserveReferencesHandling_17(Nullable_1_t4DC01BF01ED0BB2AA2B73540F63FC0D48E3B387A  value)
	{
		____preserveReferencesHandling_17 = value;
	}

	inline static int32_t get_offset_of__nullValueHandling_18() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____nullValueHandling_18)); }
	inline Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA  get__nullValueHandling_18() const { return ____nullValueHandling_18; }
	inline Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA * get_address_of__nullValueHandling_18() { return &____nullValueHandling_18; }
	inline void set__nullValueHandling_18(Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA  value)
	{
		____nullValueHandling_18 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_19() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____objectCreationHandling_19)); }
	inline Nullable_1_t43CD81743444619B643C0A3912749713399518C5  get__objectCreationHandling_19() const { return ____objectCreationHandling_19; }
	inline Nullable_1_t43CD81743444619B643C0A3912749713399518C5 * get_address_of__objectCreationHandling_19() { return &____objectCreationHandling_19; }
	inline void set__objectCreationHandling_19(Nullable_1_t43CD81743444619B643C0A3912749713399518C5  value)
	{
		____objectCreationHandling_19 = value;
	}

	inline static int32_t get_offset_of__missingMemberHandling_20() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____missingMemberHandling_20)); }
	inline Nullable_1_t3ECC38B72B3C0270EC5B32F255134E67C8EE5013  get__missingMemberHandling_20() const { return ____missingMemberHandling_20; }
	inline Nullable_1_t3ECC38B72B3C0270EC5B32F255134E67C8EE5013 * get_address_of__missingMemberHandling_20() { return &____missingMemberHandling_20; }
	inline void set__missingMemberHandling_20(Nullable_1_t3ECC38B72B3C0270EC5B32F255134E67C8EE5013  value)
	{
		____missingMemberHandling_20 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_21() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____referenceLoopHandling_21)); }
	inline Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  get__referenceLoopHandling_21() const { return ____referenceLoopHandling_21; }
	inline Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33 * get_address_of__referenceLoopHandling_21() { return &____referenceLoopHandling_21; }
	inline void set__referenceLoopHandling_21(Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  value)
	{
		____referenceLoopHandling_21 = value;
	}

	inline static int32_t get_offset_of__context_22() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____context_22)); }
	inline Nullable_1_tCF22087D771FCA8102A51C976A8992F7F8AE27B8  get__context_22() const { return ____context_22; }
	inline Nullable_1_tCF22087D771FCA8102A51C976A8992F7F8AE27B8 * get_address_of__context_22() { return &____context_22; }
	inline void set__context_22(Nullable_1_tCF22087D771FCA8102A51C976A8992F7F8AE27B8  value)
	{
		____context_22 = value;
	}

	inline static int32_t get_offset_of__constructorHandling_23() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____constructorHandling_23)); }
	inline Nullable_1_t386D7565CF3D349D5FEFDBB687254FC491B70AD5  get__constructorHandling_23() const { return ____constructorHandling_23; }
	inline Nullable_1_t386D7565CF3D349D5FEFDBB687254FC491B70AD5 * get_address_of__constructorHandling_23() { return &____constructorHandling_23; }
	inline void set__constructorHandling_23(Nullable_1_t386D7565CF3D349D5FEFDBB687254FC491B70AD5  value)
	{
		____constructorHandling_23 = value;
	}

	inline static int32_t get_offset_of__typeNameHandling_24() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____typeNameHandling_24)); }
	inline Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  get__typeNameHandling_24() const { return ____typeNameHandling_24; }
	inline Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184 * get_address_of__typeNameHandling_24() { return &____typeNameHandling_24; }
	inline void set__typeNameHandling_24(Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  value)
	{
		____typeNameHandling_24 = value;
	}

	inline static int32_t get_offset_of__metadataPropertyHandling_25() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ____metadataPropertyHandling_25)); }
	inline Nullable_1_tF330C1A3D6D96AD89924DABE832ED2C68921413B  get__metadataPropertyHandling_25() const { return ____metadataPropertyHandling_25; }
	inline Nullable_1_tF330C1A3D6D96AD89924DABE832ED2C68921413B * get_address_of__metadataPropertyHandling_25() { return &____metadataPropertyHandling_25; }
	inline void set__metadataPropertyHandling_25(Nullable_1_tF330C1A3D6D96AD89924DABE832ED2C68921413B  value)
	{
		____metadataPropertyHandling_25 = value;
	}

	inline static int32_t get_offset_of_U3CConvertersU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ___U3CConvertersU3Ek__BackingField_26)); }
	inline RuntimeObject* get_U3CConvertersU3Ek__BackingField_26() const { return ___U3CConvertersU3Ek__BackingField_26; }
	inline RuntimeObject** get_address_of_U3CConvertersU3Ek__BackingField_26() { return &___U3CConvertersU3Ek__BackingField_26; }
	inline void set_U3CConvertersU3Ek__BackingField_26(RuntimeObject* value)
	{
		___U3CConvertersU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConvertersU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CContractResolverU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ___U3CContractResolverU3Ek__BackingField_27)); }
	inline RuntimeObject* get_U3CContractResolverU3Ek__BackingField_27() const { return ___U3CContractResolverU3Ek__BackingField_27; }
	inline RuntimeObject** get_address_of_U3CContractResolverU3Ek__BackingField_27() { return &___U3CContractResolverU3Ek__BackingField_27; }
	inline void set_U3CContractResolverU3Ek__BackingField_27(RuntimeObject* value)
	{
		___U3CContractResolverU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContractResolverU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CEqualityComparerU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ___U3CEqualityComparerU3Ek__BackingField_28)); }
	inline RuntimeObject* get_U3CEqualityComparerU3Ek__BackingField_28() const { return ___U3CEqualityComparerU3Ek__BackingField_28; }
	inline RuntimeObject** get_address_of_U3CEqualityComparerU3Ek__BackingField_28() { return &___U3CEqualityComparerU3Ek__BackingField_28; }
	inline void set_U3CEqualityComparerU3Ek__BackingField_28(RuntimeObject* value)
	{
		___U3CEqualityComparerU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEqualityComparerU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CReferenceResolverProviderU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ___U3CReferenceResolverProviderU3Ek__BackingField_29)); }
	inline Func_1_t08DED721C4774B7A9EB8B6792D1895E5E0B7A64A * get_U3CReferenceResolverProviderU3Ek__BackingField_29() const { return ___U3CReferenceResolverProviderU3Ek__BackingField_29; }
	inline Func_1_t08DED721C4774B7A9EB8B6792D1895E5E0B7A64A ** get_address_of_U3CReferenceResolverProviderU3Ek__BackingField_29() { return &___U3CReferenceResolverProviderU3Ek__BackingField_29; }
	inline void set_U3CReferenceResolverProviderU3Ek__BackingField_29(Func_1_t08DED721C4774B7A9EB8B6792D1895E5E0B7A64A * value)
	{
		___U3CReferenceResolverProviderU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CReferenceResolverProviderU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CTraceWriterU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ___U3CTraceWriterU3Ek__BackingField_30)); }
	inline RuntimeObject* get_U3CTraceWriterU3Ek__BackingField_30() const { return ___U3CTraceWriterU3Ek__BackingField_30; }
	inline RuntimeObject** get_address_of_U3CTraceWriterU3Ek__BackingField_30() { return &___U3CTraceWriterU3Ek__BackingField_30; }
	inline void set_U3CTraceWriterU3Ek__BackingField_30(RuntimeObject* value)
	{
		___U3CTraceWriterU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTraceWriterU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CBinderU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ___U3CBinderU3Ek__BackingField_31)); }
	inline SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * get_U3CBinderU3Ek__BackingField_31() const { return ___U3CBinderU3Ek__BackingField_31; }
	inline SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 ** get_address_of_U3CBinderU3Ek__BackingField_31() { return &___U3CBinderU3Ek__BackingField_31; }
	inline void set_U3CBinderU3Ek__BackingField_31(SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * value)
	{
		___U3CBinderU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBinderU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0, ___U3CErrorU3Ek__BackingField_32)); }
	inline EventHandler_1_tD0863E67C9A50D35387D2C3E7DF644C8E96F5B9A * get_U3CErrorU3Ek__BackingField_32() const { return ___U3CErrorU3Ek__BackingField_32; }
	inline EventHandler_1_tD0863E67C9A50D35387D2C3E7DF644C8E96F5B9A ** get_address_of_U3CErrorU3Ek__BackingField_32() { return &___U3CErrorU3Ek__BackingField_32; }
	inline void set_U3CErrorU3Ek__BackingField_32(EventHandler_1_tD0863E67C9A50D35387D2C3E7DF644C8E96F5B9A * value)
	{
		___U3CErrorU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorU3Ek__BackingField_32), value);
	}
};

struct JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0_StaticFields
{
public:
	// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializerSettings::DefaultContext
	StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  ___DefaultContext_0;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonSerializerSettings::DefaultCulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___DefaultCulture_1;

public:
	inline static int32_t get_offset_of_DefaultContext_0() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0_StaticFields, ___DefaultContext_0)); }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  get_DefaultContext_0() const { return ___DefaultContext_0; }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 * get_address_of_DefaultContext_0() { return &___DefaultContext_0; }
	inline void set_DefaultContext_0(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  value)
	{
		___DefaultContext_0 = value;
	}

	inline static int32_t get_offset_of_DefaultCulture_1() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0_StaticFields, ___DefaultCulture_1)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_DefaultCulture_1() const { return ___DefaultCulture_1; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_DefaultCulture_1() { return &___DefaultCulture_1; }
	inline void set_DefaultCulture_1(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___DefaultCulture_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultCulture_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERSETTINGS_T616936620D9972F1A53B6F6D6E35E91A87DD11F0_H
#ifndef JSONTEXTREADER_T237A2776CC59E2840A79D31EBD665B7617709BE8_H
#define JSONTEXTREADER_T237A2776CC59E2840A79D31EBD665B7617709BE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonTextReader
struct  JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8  : public JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC
{
public:
	// System.IO.TextReader Newtonsoft.Json.JsonTextReader::_reader
	TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * ____reader_15;
	// System.Char[] Newtonsoft.Json.JsonTextReader::_chars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____chars_16;
	// System.Int32 Newtonsoft.Json.JsonTextReader::_charsUsed
	int32_t ____charsUsed_17;
	// System.Int32 Newtonsoft.Json.JsonTextReader::_charPos
	int32_t ____charPos_18;
	// System.Int32 Newtonsoft.Json.JsonTextReader::_lineStartPos
	int32_t ____lineStartPos_19;
	// System.Int32 Newtonsoft.Json.JsonTextReader::_lineNumber
	int32_t ____lineNumber_20;
	// System.Boolean Newtonsoft.Json.JsonTextReader::_isEndOfFile
	bool ____isEndOfFile_21;
	// Newtonsoft.Json.Utilities.StringBuffer Newtonsoft.Json.JsonTextReader::_stringBuffer
	StringBuffer_t3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89  ____stringBuffer_22;
	// Newtonsoft.Json.Utilities.StringReference Newtonsoft.Json.JsonTextReader::_stringReference
	StringReference_t26CAF004EA4ED81837111868CA1FFE682E987DE1  ____stringReference_23;
	// Newtonsoft.Json.IArrayPool`1<System.Char> Newtonsoft.Json.JsonTextReader::_arrayPool
	RuntimeObject* ____arrayPool_24;
	// Newtonsoft.Json.Utilities.PropertyNameTable Newtonsoft.Json.JsonTextReader::NameTable
	PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028 * ___NameTable_25;

public:
	inline static int32_t get_offset_of__reader_15() { return static_cast<int32_t>(offsetof(JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8, ____reader_15)); }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * get__reader_15() const { return ____reader_15; }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A ** get_address_of__reader_15() { return &____reader_15; }
	inline void set__reader_15(TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * value)
	{
		____reader_15 = value;
		Il2CppCodeGenWriteBarrier((&____reader_15), value);
	}

	inline static int32_t get_offset_of__chars_16() { return static_cast<int32_t>(offsetof(JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8, ____chars_16)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__chars_16() const { return ____chars_16; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__chars_16() { return &____chars_16; }
	inline void set__chars_16(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____chars_16 = value;
		Il2CppCodeGenWriteBarrier((&____chars_16), value);
	}

	inline static int32_t get_offset_of__charsUsed_17() { return static_cast<int32_t>(offsetof(JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8, ____charsUsed_17)); }
	inline int32_t get__charsUsed_17() const { return ____charsUsed_17; }
	inline int32_t* get_address_of__charsUsed_17() { return &____charsUsed_17; }
	inline void set__charsUsed_17(int32_t value)
	{
		____charsUsed_17 = value;
	}

	inline static int32_t get_offset_of__charPos_18() { return static_cast<int32_t>(offsetof(JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8, ____charPos_18)); }
	inline int32_t get__charPos_18() const { return ____charPos_18; }
	inline int32_t* get_address_of__charPos_18() { return &____charPos_18; }
	inline void set__charPos_18(int32_t value)
	{
		____charPos_18 = value;
	}

	inline static int32_t get_offset_of__lineStartPos_19() { return static_cast<int32_t>(offsetof(JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8, ____lineStartPos_19)); }
	inline int32_t get__lineStartPos_19() const { return ____lineStartPos_19; }
	inline int32_t* get_address_of__lineStartPos_19() { return &____lineStartPos_19; }
	inline void set__lineStartPos_19(int32_t value)
	{
		____lineStartPos_19 = value;
	}

	inline static int32_t get_offset_of__lineNumber_20() { return static_cast<int32_t>(offsetof(JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8, ____lineNumber_20)); }
	inline int32_t get__lineNumber_20() const { return ____lineNumber_20; }
	inline int32_t* get_address_of__lineNumber_20() { return &____lineNumber_20; }
	inline void set__lineNumber_20(int32_t value)
	{
		____lineNumber_20 = value;
	}

	inline static int32_t get_offset_of__isEndOfFile_21() { return static_cast<int32_t>(offsetof(JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8, ____isEndOfFile_21)); }
	inline bool get__isEndOfFile_21() const { return ____isEndOfFile_21; }
	inline bool* get_address_of__isEndOfFile_21() { return &____isEndOfFile_21; }
	inline void set__isEndOfFile_21(bool value)
	{
		____isEndOfFile_21 = value;
	}

	inline static int32_t get_offset_of__stringBuffer_22() { return static_cast<int32_t>(offsetof(JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8, ____stringBuffer_22)); }
	inline StringBuffer_t3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89  get__stringBuffer_22() const { return ____stringBuffer_22; }
	inline StringBuffer_t3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89 * get_address_of__stringBuffer_22() { return &____stringBuffer_22; }
	inline void set__stringBuffer_22(StringBuffer_t3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89  value)
	{
		____stringBuffer_22 = value;
	}

	inline static int32_t get_offset_of__stringReference_23() { return static_cast<int32_t>(offsetof(JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8, ____stringReference_23)); }
	inline StringReference_t26CAF004EA4ED81837111868CA1FFE682E987DE1  get__stringReference_23() const { return ____stringReference_23; }
	inline StringReference_t26CAF004EA4ED81837111868CA1FFE682E987DE1 * get_address_of__stringReference_23() { return &____stringReference_23; }
	inline void set__stringReference_23(StringReference_t26CAF004EA4ED81837111868CA1FFE682E987DE1  value)
	{
		____stringReference_23 = value;
	}

	inline static int32_t get_offset_of__arrayPool_24() { return static_cast<int32_t>(offsetof(JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8, ____arrayPool_24)); }
	inline RuntimeObject* get__arrayPool_24() const { return ____arrayPool_24; }
	inline RuntimeObject** get_address_of__arrayPool_24() { return &____arrayPool_24; }
	inline void set__arrayPool_24(RuntimeObject* value)
	{
		____arrayPool_24 = value;
		Il2CppCodeGenWriteBarrier((&____arrayPool_24), value);
	}

	inline static int32_t get_offset_of_NameTable_25() { return static_cast<int32_t>(offsetof(JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8, ___NameTable_25)); }
	inline PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028 * get_NameTable_25() const { return ___NameTable_25; }
	inline PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028 ** get_address_of_NameTable_25() { return &___NameTable_25; }
	inline void set_NameTable_25(PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028 * value)
	{
		___NameTable_25 = value;
		Il2CppCodeGenWriteBarrier((&___NameTable_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTEXTREADER_T237A2776CC59E2840A79D31EBD665B7617709BE8_H
#ifndef JSONTEXTWRITER_T7CF50AEC1B1511909ABCDC7542E0D34DB7922225_H
#define JSONTEXTWRITER_T7CF50AEC1B1511909ABCDC7542E0D34DB7922225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonTextWriter
struct  JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225  : public JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091
{
public:
	// System.IO.TextWriter Newtonsoft.Json.JsonTextWriter::_writer
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ____writer_13;
	// Newtonsoft.Json.Utilities.Base64Encoder Newtonsoft.Json.JsonTextWriter::_base64Encoder
	Base64Encoder_tA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835 * ____base64Encoder_14;
	// System.Char Newtonsoft.Json.JsonTextWriter::_indentChar
	Il2CppChar ____indentChar_15;
	// System.Int32 Newtonsoft.Json.JsonTextWriter::_indentation
	int32_t ____indentation_16;
	// System.Char Newtonsoft.Json.JsonTextWriter::_quoteChar
	Il2CppChar ____quoteChar_17;
	// System.Boolean Newtonsoft.Json.JsonTextWriter::_quoteName
	bool ____quoteName_18;
	// System.Boolean[] Newtonsoft.Json.JsonTextWriter::_charEscapeFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ____charEscapeFlags_19;
	// System.Char[] Newtonsoft.Json.JsonTextWriter::_writeBuffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____writeBuffer_20;
	// Newtonsoft.Json.IArrayPool`1<System.Char> Newtonsoft.Json.JsonTextWriter::_arrayPool
	RuntimeObject* ____arrayPool_21;
	// System.Char[] Newtonsoft.Json.JsonTextWriter::_indentChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____indentChars_22;

public:
	inline static int32_t get_offset_of__writer_13() { return static_cast<int32_t>(offsetof(JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225, ____writer_13)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get__writer_13() const { return ____writer_13; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of__writer_13() { return &____writer_13; }
	inline void set__writer_13(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		____writer_13 = value;
		Il2CppCodeGenWriteBarrier((&____writer_13), value);
	}

	inline static int32_t get_offset_of__base64Encoder_14() { return static_cast<int32_t>(offsetof(JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225, ____base64Encoder_14)); }
	inline Base64Encoder_tA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835 * get__base64Encoder_14() const { return ____base64Encoder_14; }
	inline Base64Encoder_tA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835 ** get_address_of__base64Encoder_14() { return &____base64Encoder_14; }
	inline void set__base64Encoder_14(Base64Encoder_tA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835 * value)
	{
		____base64Encoder_14 = value;
		Il2CppCodeGenWriteBarrier((&____base64Encoder_14), value);
	}

	inline static int32_t get_offset_of__indentChar_15() { return static_cast<int32_t>(offsetof(JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225, ____indentChar_15)); }
	inline Il2CppChar get__indentChar_15() const { return ____indentChar_15; }
	inline Il2CppChar* get_address_of__indentChar_15() { return &____indentChar_15; }
	inline void set__indentChar_15(Il2CppChar value)
	{
		____indentChar_15 = value;
	}

	inline static int32_t get_offset_of__indentation_16() { return static_cast<int32_t>(offsetof(JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225, ____indentation_16)); }
	inline int32_t get__indentation_16() const { return ____indentation_16; }
	inline int32_t* get_address_of__indentation_16() { return &____indentation_16; }
	inline void set__indentation_16(int32_t value)
	{
		____indentation_16 = value;
	}

	inline static int32_t get_offset_of__quoteChar_17() { return static_cast<int32_t>(offsetof(JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225, ____quoteChar_17)); }
	inline Il2CppChar get__quoteChar_17() const { return ____quoteChar_17; }
	inline Il2CppChar* get_address_of__quoteChar_17() { return &____quoteChar_17; }
	inline void set__quoteChar_17(Il2CppChar value)
	{
		____quoteChar_17 = value;
	}

	inline static int32_t get_offset_of__quoteName_18() { return static_cast<int32_t>(offsetof(JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225, ____quoteName_18)); }
	inline bool get__quoteName_18() const { return ____quoteName_18; }
	inline bool* get_address_of__quoteName_18() { return &____quoteName_18; }
	inline void set__quoteName_18(bool value)
	{
		____quoteName_18 = value;
	}

	inline static int32_t get_offset_of__charEscapeFlags_19() { return static_cast<int32_t>(offsetof(JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225, ____charEscapeFlags_19)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get__charEscapeFlags_19() const { return ____charEscapeFlags_19; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of__charEscapeFlags_19() { return &____charEscapeFlags_19; }
	inline void set__charEscapeFlags_19(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		____charEscapeFlags_19 = value;
		Il2CppCodeGenWriteBarrier((&____charEscapeFlags_19), value);
	}

	inline static int32_t get_offset_of__writeBuffer_20() { return static_cast<int32_t>(offsetof(JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225, ____writeBuffer_20)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__writeBuffer_20() const { return ____writeBuffer_20; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__writeBuffer_20() { return &____writeBuffer_20; }
	inline void set__writeBuffer_20(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____writeBuffer_20 = value;
		Il2CppCodeGenWriteBarrier((&____writeBuffer_20), value);
	}

	inline static int32_t get_offset_of__arrayPool_21() { return static_cast<int32_t>(offsetof(JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225, ____arrayPool_21)); }
	inline RuntimeObject* get__arrayPool_21() const { return ____arrayPool_21; }
	inline RuntimeObject** get_address_of__arrayPool_21() { return &____arrayPool_21; }
	inline void set__arrayPool_21(RuntimeObject* value)
	{
		____arrayPool_21 = value;
		Il2CppCodeGenWriteBarrier((&____arrayPool_21), value);
	}

	inline static int32_t get_offset_of__indentChars_22() { return static_cast<int32_t>(offsetof(JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225, ____indentChars_22)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__indentChars_22() const { return ____indentChars_22; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__indentChars_22() { return &____indentChars_22; }
	inline void set__indentChars_22(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____indentChars_22 = value;
		Il2CppCodeGenWriteBarrier((&____indentChars_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTEXTWRITER_T7CF50AEC1B1511909ABCDC7542E0D34DB7922225_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5000 = { sizeof (DateFormatHandling_t0D54266C0C7CD81612959E7CB8174F6F8AA21424)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5000[3] = 
{
	DateFormatHandling_t0D54266C0C7CD81612959E7CB8174F6F8AA21424::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5001 = { sizeof (DateParseHandling_t0428B8AFAEE2CBEF5C0E1D3CEE0F5F1123E1E22C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5001[4] = 
{
	DateParseHandling_t0428B8AFAEE2CBEF5C0E1D3CEE0F5F1123E1E22C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5002 = { sizeof (DateTimeZoneHandling_t4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5002[5] = 
{
	DateTimeZoneHandling_t4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5003 = { sizeof (FloatFormatHandling_tE78C0C14E4CFE64BDF71370419573A3ED8B68289)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5003[4] = 
{
	FloatFormatHandling_tE78C0C14E4CFE64BDF71370419573A3ED8B68289::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5004 = { sizeof (FloatParseHandling_t4571C78B0F87871B93E071E07321989BEB7633B1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5004[3] = 
{
	FloatParseHandling_t4571C78B0F87871B93E071E07321989BEB7633B1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5005 = { sizeof (Formatting_tE2524DB9105FC27E1B956F5AF5F211981B22E3E1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5005[3] = 
{
	Formatting_tE2524DB9105FC27E1B956F5AF5F211981B22E3E1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5006 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5007 = { sizeof (JsonConstructorAttribute_t99AF2AA01C0D9986F79E8F10B8FF80E59566F535), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5008 = { sizeof (JsonDictionaryAttribute_tAEA3F708F0DAE3AAE16AFE68603A68F6DE770FA5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5009 = { sizeof (JsonException_tE9DA91D672710AF3C23049EEC7872C5B4C9C89D8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5010 = { sizeof (JsonExtensionDataAttribute_t172C2D68229E647E79DEECAF1DD994EE52A5B98C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5010[2] = 
{
	JsonExtensionDataAttribute_t172C2D68229E647E79DEECAF1DD994EE52A5B98C::get_offset_of_U3CWriteDataU3Ek__BackingField_0(),
	JsonExtensionDataAttribute_t172C2D68229E647E79DEECAF1DD994EE52A5B98C::get_offset_of_U3CReadDataU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5011 = { sizeof (JsonContainerType_t1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5011[5] = 
{
	JsonContainerType_t1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5012 = { sizeof (JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD)+ sizeof (RuntimeObject), sizeof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD_marshaled_pinvoke), sizeof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5012[5] = 
{
	JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD_StaticFields::get_offset_of_SpecialCharacters_0(),
	JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD::get_offset_of_Type_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD::get_offset_of_Position_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD::get_offset_of_PropertyName_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD::get_offset_of_HasIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5013 = { sizeof (JsonRequiredAttribute_tB3DCDC06F01C68AF2D2E8681949F39EE1B018CA0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5014 = { sizeof (MetadataPropertyHandling_tD6239BDB5CF9C10FA2CC900DE4BC30C9BD253AF7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5014[4] = 
{
	MetadataPropertyHandling_tD6239BDB5CF9C10FA2CC900DE4BC30C9BD253AF7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5015 = { sizeof (StringEscapeHandling_t3E67D3B0A780B24A2D57DF8921B9438CC507029A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5015[4] = 
{
	StringEscapeHandling_t3E67D3B0A780B24A2D57DF8921B9438CC507029A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5016 = { sizeof (Required_tB3FC6E6E91A682BD5E3D527FCE4979AFE2DD58B3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5016[5] = 
{
	Required_tB3FC6E6E91A682BD5E3D527FCE4979AFE2DD58B3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5017 = { sizeof (PreserveReferencesHandling_tBF8B928632AB094AB854A8C8060F4A0DE246BDE0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5017[5] = 
{
	PreserveReferencesHandling_tBF8B928632AB094AB854A8C8060F4A0DE246BDE0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5018 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5019 = { sizeof (JsonArrayAttribute_tDC53BD61D6BC8946022A92716BC3E905688CE52E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5020 = { sizeof (JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5020[9] = 
{
	JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10::get_offset_of_U3CItemConverterTypeU3Ek__BackingField_0(),
	JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10::get_offset_of_U3CItemConverterParametersU3Ek__BackingField_1(),
	JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10::get_offset_of_U3CNamingStrategyInstanceU3Ek__BackingField_2(),
	JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10::get_offset_of__isReference_3(),
	JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10::get_offset_of__itemIsReference_4(),
	JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10::get_offset_of__itemReferenceLoopHandling_5(),
	JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10::get_offset_of__itemTypeNameHandling_6(),
	JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10::get_offset_of__namingStrategyType_7(),
	JsonContainerAttribute_t2465568F23D145E0C80963D63F93771A8A605C10::get_offset_of__namingStrategyParameters_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5021 = { sizeof (DefaultValueHandling_t85DC44F1FBDD9D31309A45A0334A4518B5C08696)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5021[5] = 
{
	DefaultValueHandling_t85DC44F1FBDD9D31309A45A0334A4518B5C08696::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5022 = { sizeof (JsonConverterAttribute_t2D407785561C66A63162D4AFBD8C48450EF6934E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5022[2] = 
{
	JsonConverterAttribute_t2D407785561C66A63162D4AFBD8C48450EF6934E::get_offset_of__converterType_0(),
	JsonConverterAttribute_t2D407785561C66A63162D4AFBD8C48450EF6934E::get_offset_of_U3CConverterParametersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5023 = { sizeof (JsonObjectAttribute_tC8E1A560528FCFC01AA594F9830039913CEEEFCC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5023[2] = 
{
	JsonObjectAttribute_tC8E1A560528FCFC01AA594F9830039913CEEEFCC::get_offset_of__memberSerialization_9(),
	JsonObjectAttribute_tC8E1A560528FCFC01AA594F9830039913CEEEFCC::get_offset_of__itemRequired_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5024 = { sizeof (JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0), -1, sizeof(JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5024[33] = 
{
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0_StaticFields::get_offset_of_DefaultContext_0(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0_StaticFields::get_offset_of_DefaultCulture_1(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__formatting_2(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__dateFormatHandling_3(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__dateTimeZoneHandling_4(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__dateParseHandling_5(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__floatFormatHandling_6(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__floatParseHandling_7(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__stringEscapeHandling_8(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__culture_9(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__checkAdditionalContent_10(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__maxDepth_11(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__maxDepthSet_12(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__dateFormatString_13(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__dateFormatStringSet_14(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__typeNameAssemblyFormat_15(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__defaultValueHandling_16(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__preserveReferencesHandling_17(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__nullValueHandling_18(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__objectCreationHandling_19(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__missingMemberHandling_20(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__referenceLoopHandling_21(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__context_22(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__constructorHandling_23(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__typeNameHandling_24(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of__metadataPropertyHandling_25(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of_U3CConvertersU3Ek__BackingField_26(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of_U3CContractResolverU3Ek__BackingField_27(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of_U3CEqualityComparerU3Ek__BackingField_28(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of_U3CReferenceResolverProviderU3Ek__BackingField_29(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of_U3CTraceWriterU3Ek__BackingField_30(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of_U3CBinderU3Ek__BackingField_31(),
	JsonSerializerSettings_t616936620D9972F1A53B6F6D6E35E91A87DD11F0::get_offset_of_U3CErrorU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5025 = { sizeof (MemberSerialization_tDB2DD34DBC5EF12D4740B667F601358C7DA90212)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5025[4] = 
{
	MemberSerialization_tDB2DD34DBC5EF12D4740B667F601358C7DA90212::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5026 = { sizeof (ObjectCreationHandling_tC02C49234F03D7C4A800245AA414325496355B2C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5026[4] = 
{
	ObjectCreationHandling_tC02C49234F03D7C4A800245AA414325496355B2C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5027 = { sizeof (ReadType_t0139C8C2354D6D4A51B10FEE12C9E9549C2C247A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5027[10] = 
{
	ReadType_t0139C8C2354D6D4A51B10FEE12C9E9549C2C247A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5028 = { sizeof (JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5028[11] = 
{
	JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8::get_offset_of__reader_15(),
	JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8::get_offset_of__chars_16(),
	JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8::get_offset_of__charsUsed_17(),
	JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8::get_offset_of__charPos_18(),
	JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8::get_offset_of__lineStartPos_19(),
	JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8::get_offset_of__lineNumber_20(),
	JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8::get_offset_of__isEndOfFile_21(),
	JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8::get_offset_of__stringBuffer_22(),
	JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8::get_offset_of__stringReference_23(),
	JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8::get_offset_of__arrayPool_24(),
	JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8::get_offset_of_NameTable_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5029 = { sizeof (JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5029[16] = 
{
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of__nullValueHandling_0(),
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of__defaultValueHandling_1(),
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of__referenceLoopHandling_2(),
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of__objectCreationHandling_3(),
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of__typeNameHandling_4(),
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of__isReference_5(),
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of__order_6(),
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of__required_7(),
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of__itemIsReference_8(),
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of__itemReferenceLoopHandling_9(),
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of__itemTypeNameHandling_10(),
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of_U3CItemConverterTypeU3Ek__BackingField_11(),
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of_U3CItemConverterParametersU3Ek__BackingField_12(),
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of_U3CNamingStrategyTypeU3Ek__BackingField_13(),
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of_U3CNamingStrategyParametersU3Ek__BackingField_14(),
	JsonPropertyAttribute_tA048054356547F8D7F503C0768A3EE277BA9DD69::get_offset_of_U3CPropertyNameU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5030 = { sizeof (JsonIgnoreAttribute_t9B3B6F92369BC8EBEED668765C47AB795DC2F1DD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5031 = { sizeof (JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5031[10] = 
{
	JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225::get_offset_of__writer_13(),
	JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225::get_offset_of__base64Encoder_14(),
	JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225::get_offset_of__indentChar_15(),
	JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225::get_offset_of__indentation_16(),
	JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225::get_offset_of__quoteChar_17(),
	JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225::get_offset_of__quoteName_18(),
	JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225::get_offset_of__charEscapeFlags_19(),
	JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225::get_offset_of__writeBuffer_20(),
	JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225::get_offset_of__arrayPool_21(),
	JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225::get_offset_of__indentChars_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5032 = { sizeof (JsonWriterException_t5265D2B2FDE5BC816FC891D65229A75A26806CE2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5032[1] = 
{
	JsonWriterException_t5265D2B2FDE5BC816FC891D65229A75A26806CE2::get_offset_of_U3CPathU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5033 = { sizeof (JsonReaderException_t44F84BC4AF81F378D1901DEAD54371B9C55A9708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5033[3] = 
{
	JsonReaderException_t44F84BC4AF81F378D1901DEAD54371B9C55A9708::get_offset_of_U3CLineNumberU3Ek__BackingField_17(),
	JsonReaderException_t44F84BC4AF81F378D1901DEAD54371B9C55A9708::get_offset_of_U3CLinePositionU3Ek__BackingField_18(),
	JsonReaderException_t44F84BC4AF81F378D1901DEAD54371B9C55A9708::get_offset_of_U3CPathU3Ek__BackingField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5034 = { sizeof (JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5035 = { sizeof (JsonConverterCollection_tE01B17B4897D2254EC50F208B6AA43A938298ABF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5036 = { sizeof (JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5036[15] = 
{
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC::get_offset_of__tokenType_0(),
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC::get_offset_of__value_1(),
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC::get_offset_of__quoteChar_2(),
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC::get_offset_of__currentState_3(),
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC::get_offset_of__currentPosition_4(),
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC::get_offset_of__culture_5(),
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC::get_offset_of__dateTimeZoneHandling_6(),
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC::get_offset_of__maxDepth_7(),
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC::get_offset_of__hasExceededMaxDepth_8(),
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC::get_offset_of__dateParseHandling_9(),
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC::get_offset_of__floatParseHandling_10(),
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC::get_offset_of__dateFormatString_11(),
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC::get_offset_of__stack_12(),
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC::get_offset_of_U3CCloseInputU3Ek__BackingField_13(),
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC::get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5037 = { sizeof (State_tD13934C0FF1DA5189F8BD9DC33D77391AB371944)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5037[14] = 
{
	State_tD13934C0FF1DA5189F8BD9DC33D77391AB371944::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5038 = { sizeof (JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17), -1, sizeof(JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5038[8] = 
{
	JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields::get_offset_of_U3CDefaultSettingsU3Ek__BackingField_0(),
	JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields::get_offset_of_True_1(),
	JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields::get_offset_of_False_2(),
	JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields::get_offset_of_Null_3(),
	JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields::get_offset_of_Undefined_4(),
	JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields::get_offset_of_PositiveInfinity_5(),
	JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields::get_offset_of_NegativeInfinity_6(),
	JsonConvert_tACC7285CFAFB2143F710970F90E3C520D3C19A17_StaticFields::get_offset_of_NaN_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5039 = { sizeof (JsonSerializationException_t9971A0E26D360F6FBF0E3543751D37D64325FB59), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5040 = { sizeof (JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5040[31] = 
{
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__typeNameHandling_0(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__typeNameAssemblyFormat_1(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__preserveReferencesHandling_2(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__referenceLoopHandling_3(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__missingMemberHandling_4(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__objectCreationHandling_5(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__nullValueHandling_6(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__defaultValueHandling_7(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__constructorHandling_8(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__metadataPropertyHandling_9(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__converters_10(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__contractResolver_11(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__traceWriter_12(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__equalityComparer_13(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__binder_14(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__context_15(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__referenceResolver_16(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__formatting_17(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__dateFormatHandling_18(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__dateTimeZoneHandling_19(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__dateParseHandling_20(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__floatFormatHandling_21(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__floatParseHandling_22(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__stringEscapeHandling_23(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__culture_24(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__maxDepth_25(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__maxDepthSet_26(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__checkAdditionalContent_27(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__dateFormatString_28(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of__dateFormatStringSet_29(),
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB::get_offset_of_Error_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5041 = { sizeof (MissingMemberHandling_t1F9543EA590BC359A9E4684D9D12F6E1098E276D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5041[3] = 
{
	MissingMemberHandling_t1F9543EA590BC359A9E4684D9D12F6E1098E276D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5042 = { sizeof (NullValueHandling_tD5E3B6B0BF979D9DA6A17E676FA5394A5493430D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5042[3] = 
{
	NullValueHandling_tD5E3B6B0BF979D9DA6A17E676FA5394A5493430D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5043 = { sizeof (ReferenceLoopHandling_t1F4E04DDA8FDB18AAA2B32B2E8FAC32ADA9AF672)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5043[4] = 
{
	ReferenceLoopHandling_t1F4E04DDA8FDB18AAA2B32B2E8FAC32ADA9AF672::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5044 = { sizeof (TypeNameHandling_t0DC2566A7FAFA823002ADAD5175DFF387F7D1ED9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5044[6] = 
{
	TypeNameHandling_t0DC2566A7FAFA823002ADAD5175DFF387F7D1ED9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5045 = { sizeof (JsonToken_t6ADDEFDDA044FF7A3AAFC9E7CBD0E568B418410D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5045[19] = 
{
	JsonToken_t6ADDEFDDA044FF7A3AAFC9E7CBD0E568B418410D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5046 = { sizeof (JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091), -1, sizeof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5046[13] = 
{
	JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091_StaticFields::get_offset_of_StateArray_0(),
	JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091_StaticFields::get_offset_of_StateArrayTempate_1(),
	JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091::get_offset_of__stack_2(),
	JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091::get_offset_of__currentPosition_3(),
	JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091::get_offset_of__currentState_4(),
	JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091::get_offset_of__formatting_5(),
	JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091::get_offset_of_U3CCloseOutputU3Ek__BackingField_6(),
	JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091::get_offset_of__dateFormatHandling_7(),
	JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091::get_offset_of__dateTimeZoneHandling_8(),
	JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091::get_offset_of__stringEscapeHandling_9(),
	JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091::get_offset_of__floatFormatHandling_10(),
	JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091::get_offset_of__dateFormatString_11(),
	JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091::get_offset_of__culture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5047 = { sizeof (State_t1DE392B52BABD9BF376B03FE41F672F7113AE11F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5047[11] = 
{
	State_t1DE392B52BABD9BF376B03FE41F672F7113AE11F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5048 = { sizeof (WriteState_t6C7BA768B993A34A6E74A7AEB5CA535833FAA1E2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5048[8] = 
{
	WriteState_t6C7BA768B993A34A6E74A7AEB5CA535833FAA1E2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5049 = { sizeof (ParserTimeZone_t0942D7F8FA70C2B7B4B00C5F58D1B0D0E13018E9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5049[5] = 
{
	ParserTimeZone_t0942D7F8FA70C2B7B4B00C5F58D1B0D0E13018E9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5050 = { sizeof (DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3)+ sizeof (RuntimeObject), sizeof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_marshaled_pinvoke), sizeof(DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5050[26] = 
{
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3::get_offset_of_Year_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3::get_offset_of_Month_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3::get_offset_of_Day_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3::get_offset_of_Hour_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3::get_offset_of_Minute_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3::get_offset_of_Second_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3::get_offset_of_Fraction_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3::get_offset_of_ZoneHour_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3::get_offset_of_ZoneMinute_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3::get_offset_of_Zone_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3::get_offset_of__text_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3::get_offset_of__end_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields::get_offset_of_Power10_12(),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields::get_offset_of_Lzyyyy_13(),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields::get_offset_of_Lzyyyy__14(),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields::get_offset_of_Lzyyyy_MM_15(),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields::get_offset_of_Lzyyyy_MM__16(),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields::get_offset_of_Lzyyyy_MM_dd_17(),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields::get_offset_of_Lzyyyy_MM_ddT_18(),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields::get_offset_of_LzHH_19(),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields::get_offset_of_LzHH__20(),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields::get_offset_of_LzHH_mm_21(),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields::get_offset_of_LzHH_mm__22(),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields::get_offset_of_LzHH_mm_ss_23(),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields::get_offset_of_Lz__24(),
	DateTimeParser_tC68D649158A233987D1B636F5ABB3E630E2477A3_StaticFields::get_offset_of_Lz_zz_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5051 = { sizeof (Base64Encoder_tA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5051[4] = 
{
	Base64Encoder_tA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835::get_offset_of__charsLine_0(),
	Base64Encoder_tA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835::get_offset_of__writer_1(),
	Base64Encoder_tA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835::get_offset_of__leftOverBytes_2(),
	Base64Encoder_tA3BBBBA925C46A1AB1B9E1C22B3A8E60412BD835::get_offset_of__leftOverBytesCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5052 = { sizeof (DynamicReflectionDelegateFactory_t64129C72C25891E1E9A8EF714B2768AE8534D176), -1, sizeof(DynamicReflectionDelegateFactory_t64129C72C25891E1E9A8EF714B2768AE8534D176_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5052[1] = 
{
	DynamicReflectionDelegateFactory_t64129C72C25891E1E9A8EF714B2768AE8534D176_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5053 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5053[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5054 = { sizeof (ILGeneratorExtensions_tF3030321DC784510B858F8711835EBDACCF542E7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5055 = { sizeof (JsonTokenUtils_tE509D495457404BEA1F30127F239E6FDACFF270E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5056 = { sizeof (PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028), -1, sizeof(PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5056[4] = 
{
	PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028_StaticFields::get_offset_of_HashCodeRandomizer_0(),
	PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028::get_offset_of__count_1(),
	PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028::get_offset_of__entries_2(),
	PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028::get_offset_of__mask_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5057 = { sizeof (Entry_t050E6E9796D2C75B3D6B3BF6012A70653E1FDB1A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5057[3] = 
{
	Entry_t050E6E9796D2C75B3D6B3BF6012A70653E1FDB1A::get_offset_of_Value_0(),
	Entry_t050E6E9796D2C75B3D6B3BF6012A70653E1FDB1A::get_offset_of_HashCode_1(),
	Entry_t050E6E9796D2C75B3D6B3BF6012A70653E1FDB1A::get_offset_of_Next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5058 = { sizeof (ReflectionDelegateFactory_t0DE9140A9473925387D2DEBE9B893930C95939D2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5059 = { sizeof (LateBoundReflectionDelegateFactory_t0C3812FD1D79FE26A409C8F5F9E9011BB87B14C2), -1, sizeof(LateBoundReflectionDelegateFactory_t0C3812FD1D79FE26A409C8F5F9E9011BB87B14C2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5059[1] = 
{
	LateBoundReflectionDelegateFactory_t0C3812FD1D79FE26A409C8F5F9E9011BB87B14C2_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5060 = { sizeof (U3CU3Ec__DisplayClass3_0_t17630845F0BEDD186940FEC2970757DEB8D1C699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5060[2] = 
{
	U3CU3Ec__DisplayClass3_0_t17630845F0BEDD186940FEC2970757DEB8D1C699::get_offset_of_c_0(),
	U3CU3Ec__DisplayClass3_0_t17630845F0BEDD186940FEC2970757DEB8D1C699::get_offset_of_method_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5061 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5061[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5062 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5062[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5063 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5063[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5064 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5064[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5065 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5065[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5066 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5066[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5067 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5068 = { sizeof (ReflectionMember_tF4529F7912052432E9C4B11481663F206EAA28B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5068[3] = 
{
	ReflectionMember_tF4529F7912052432E9C4B11481663F206EAA28B1::get_offset_of_U3CMemberTypeU3Ek__BackingField_0(),
	ReflectionMember_tF4529F7912052432E9C4B11481663F206EAA28B1::get_offset_of_U3CGetterU3Ek__BackingField_1(),
	ReflectionMember_tF4529F7912052432E9C4B11481663F206EAA28B1::get_offset_of_U3CSetterU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5069 = { sizeof (ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5069[2] = 
{
	ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105::get_offset_of_U3CCreatorU3Ek__BackingField_0(),
	ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105::get_offset_of_U3CMembersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5070 = { sizeof (U3CU3Ec__DisplayClass13_0_tBB52F26120E9DBFA2F8C1A78549CD6D50318F5E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5070[1] = 
{
	U3CU3Ec__DisplayClass13_0_tBB52F26120E9DBFA2F8C1A78549CD6D50318F5E4::get_offset_of_ctor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5071 = { sizeof (U3CU3Ec__DisplayClass13_1_tBB787649C33BB0DB373D3E550A8AEBBBA4941F2F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5071[1] = 
{
	U3CU3Ec__DisplayClass13_1_tBB787649C33BB0DB373D3E550A8AEBBBA4941F2F::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5072 = { sizeof (U3CU3Ec__DisplayClass13_2_t05D684DD57ADD903BED599BCB469E1DEF7E6B94C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5072[1] = 
{
	U3CU3Ec__DisplayClass13_2_t05D684DD57ADD903BED599BCB469E1DEF7E6B94C::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5073 = { sizeof (StringReference_t26CAF004EA4ED81837111868CA1FFE682E987DE1)+ sizeof (RuntimeObject), sizeof(StringReference_t26CAF004EA4ED81837111868CA1FFE682E987DE1_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5073[3] = 
{
	StringReference_t26CAF004EA4ED81837111868CA1FFE682E987DE1::get_offset_of__chars_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringReference_t26CAF004EA4ED81837111868CA1FFE682E987DE1::get_offset_of__startIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringReference_t26CAF004EA4ED81837111868CA1FFE682E987DE1::get_offset_of__length_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5074 = { sizeof (StringReferenceExtensions_t9D90E2A0A7D4B833A8F8B3C0220AC38CCB19B926), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5075 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5075[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5076 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5076[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5077 = { sizeof (PrimitiveTypeCode_t895AAD3E5F0FFA0FE45CE4B8A0F981F2815567C4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5077[43] = 
{
	PrimitiveTypeCode_t895AAD3E5F0FFA0FE45CE4B8A0F981F2815567C4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5078 = { sizeof (TypeInformation_tB8AF729595EBB002094570C06F7D950F081D3BB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5078[2] = 
{
	TypeInformation_tB8AF729595EBB002094570C06F7D950F081D3BB7::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	TypeInformation_tB8AF729595EBB002094570C06F7D950F081D3BB7::get_offset_of_U3CTypeCodeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5079 = { sizeof (ParseResult_tB1C99EF551F81C1C9EEBFF0995B1C67057AE8173)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5079[5] = 
{
	ParseResult_tB1C99EF551F81C1C9EEBFF0995B1C67057AE8173::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5080 = { sizeof (ConvertUtils_t862E9BF443A5CAF9A0C47C1021A0EE43691CA016), -1, sizeof(ConvertUtils_t862E9BF443A5CAF9A0C47C1021A0EE43691CA016_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5080[3] = 
{
	ConvertUtils_t862E9BF443A5CAF9A0C47C1021A0EE43691CA016_StaticFields::get_offset_of_TypeCodeMap_0(),
	ConvertUtils_t862E9BF443A5CAF9A0C47C1021A0EE43691CA016_StaticFields::get_offset_of_PrimitiveTypeCodes_1(),
	ConvertUtils_t862E9BF443A5CAF9A0C47C1021A0EE43691CA016_StaticFields::get_offset_of_CastConverters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5081 = { sizeof (TypeConvertKey_t0B516C7A7A5D68F04DE3346468A484166E3AAEC3)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5081[2] = 
{
	TypeConvertKey_t0B516C7A7A5D68F04DE3346468A484166E3AAEC3::get_offset_of__initialType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TypeConvertKey_t0B516C7A7A5D68F04DE3346468A484166E3AAEC3::get_offset_of__targetType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5082 = { sizeof (ConvertResult_t5764DADF61DF75048A4BA040ABE21C9BB85E9F5D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5082[5] = 
{
	ConvertResult_t5764DADF61DF75048A4BA040ABE21C9BB85E9F5D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5083 = { sizeof (U3CU3Ec__DisplayClass9_0_t7E458B20E211215AF4007BFE5894BF3CFDE2A0E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5083[1] = 
{
	U3CU3Ec__DisplayClass9_0_t7E458B20E211215AF4007BFE5894BF3CFDE2A0E1::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5084 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5085 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5085[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5086 = { sizeof (DateTimeUtils_t0CC5C5DD3CA0C68FD0AA0F44D529127E80444853), -1, sizeof(DateTimeUtils_t0CC5C5DD3CA0C68FD0AA0F44D529127E80444853_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5086[3] = 
{
	DateTimeUtils_t0CC5C5DD3CA0C68FD0AA0F44D529127E80444853_StaticFields::get_offset_of_InitialJavaScriptDateTicks_0(),
	DateTimeUtils_t0CC5C5DD3CA0C68FD0AA0F44D529127E80444853_StaticFields::get_offset_of_DaysToMonth365_1(),
	DateTimeUtils_t0CC5C5DD3CA0C68FD0AA0F44D529127E80444853_StaticFields::get_offset_of_DaysToMonth366_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5087 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5088 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5088[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5089 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5089[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5090 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5090[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5091 = { sizeof (EnumUtils_tE8C1135B9B3146B1CB38890F204B5DA062F9E547), -1, sizeof(EnumUtils_tE8C1135B9B3146B1CB38890F204B5DA062F9E547_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5091[1] = 
{
	EnumUtils_tE8C1135B9B3146B1CB38890F204B5DA062F9E547_StaticFields::get_offset_of_EnumMemberNamesPerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5092 = { sizeof (U3CU3Ec_tCC47B1EBC33F3C0DA87C84C3039915C339AF6E07), -1, sizeof(U3CU3Ec_tCC47B1EBC33F3C0DA87C84C3039915C339AF6E07_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5092[3] = 
{
	U3CU3Ec_tCC47B1EBC33F3C0DA87C84C3039915C339AF6E07_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tCC47B1EBC33F3C0DA87C84C3039915C339AF6E07_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
	U3CU3Ec_tCC47B1EBC33F3C0DA87C84C3039915C339AF6E07_StaticFields::get_offset_of_U3CU3E9__5_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5093 = { sizeof (BufferUtils_tD68481564DC300E3C048C5A6234836BCD4386913), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5094 = { sizeof (JavaScriptUtils_t5B0D5EA3EAA2081A0A175539B2F320924F274084), -1, sizeof(JavaScriptUtils_t5B0D5EA3EAA2081A0A175539B2F320924F274084_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5094[3] = 
{
	JavaScriptUtils_t5B0D5EA3EAA2081A0A175539B2F320924F274084_StaticFields::get_offset_of_SingleQuoteCharEscapeFlags_0(),
	JavaScriptUtils_t5B0D5EA3EAA2081A0A175539B2F320924F274084_StaticFields::get_offset_of_DoubleQuoteCharEscapeFlags_1(),
	JavaScriptUtils_t5B0D5EA3EAA2081A0A175539B2F320924F274084_StaticFields::get_offset_of_HtmlCharEscapeFlags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5095 = { sizeof (StringBuffer_t3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89)+ sizeof (RuntimeObject), sizeof(StringBuffer_t3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5095[2] = 
{
	StringBuffer_t3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89::get_offset_of__buffer_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringBuffer_t3B78FE29F4AC8E23FB6B32E9FF0C975B17E69A89::get_offset_of__position_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5096 = { sizeof (CollectionUtils_tC862666093F54FDBF00D5FF1361500B0557A2D30), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5097 = { sizeof (MathUtils_t5642EFECB8D59A1BBFD6EAE6472F3B540F8ED42A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5098 = { sizeof (MiscellaneousUtils_t90451FF8022A7838FB44B1567D61CC4C880EA51B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5099 = { sizeof (ReflectionUtils_tF7E4E160A27E479C2C9CED8C169438E6B63A3E4F), -1, sizeof(ReflectionUtils_tF7E4E160A27E479C2C9CED8C169438E6B63A3E4F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5099[1] = 
{
	ReflectionUtils_tF7E4E160A27E479C2C9CED8C169438E6B63A3E4F_StaticFields::get_offset_of_EmptyTypes_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
