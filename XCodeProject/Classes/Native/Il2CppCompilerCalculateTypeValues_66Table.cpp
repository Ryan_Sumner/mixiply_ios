﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ModestTree.Util.ReflectionUtil/IMemberInfo
struct IMemberInfo_t57E735307BC47251E097D6406283EF6233E30B8D;
// System.Attribute
struct Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Reflection.Assembly,System.Type[]>
struct Dictionary_2_tC371B9A7E8B4A801BC5A4A04E678C970C09DA4FB;
// System.Collections.Generic.IEnumerable`1<System.Reflection.Assembly>
struct IEnumerable_1_tB8810CAC403B495501CB1EFFE19817AB440D0CF2;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t31EF1520A3A805598500BB6033C14ABDA7116D5E;
// System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>
struct IEnumerator_1_tC65D35659A0144DCE2D08E61BD96C952A80AE3EC;
// System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo>
struct IEnumerator_1_t7A8759013AA69D1FE49168C24F3A6826B9FB3D3F;
// System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>
struct IEnumerator_1_t537D797E2644AF9046B1E4CAAC405D8CE9D01534;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t9F60FD6E97DB04D8FC2044CDD1C2F74C8FC3678E;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform>
struct IEnumerator_1_t087F3F887ADBAA0E204C4398571F003BED673D66;
// System.Collections.Generic.List`1<System.Func`2<System.Reflection.Assembly,System.Boolean>>
struct List_1_t808B4CBCA4E9C3FB196A98237AFA7B11A2C4FE6B;
// System.Collections.Generic.List`1<System.Func`2<System.Type,System.Boolean>>
struct List_1_t949487C81A6A48214F1F028DEDA38942444C592F;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Func`2<System.Type,System.Reflection.Assembly>
struct Func_2_t751A0CA37094CFBF9643B9A1A5381D7F7706AD84;
// System.Func`2<UnityEngine.Component,System.Int32>
struct Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47;
// System.Func`2<UnityEngine.GameObject,System.Boolean>
struct Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273;
// System.Func`2<UnityEngine.SceneManagement.Scene,System.Boolean>
struct Func_2_t62BEF835FEEE197E60620EC309AE8C51F47FCE54;
// System.Func`2<UnityEngine.Transform,UnityEngine.GameObject>
struct Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF;
// System.Reflection.Assembly
struct Assembly_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// Zenject.BindFinalizerWrapper
struct BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6;
// Zenject.BindInfo
struct BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1;
// Zenject.ConventionBindInfo
struct ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757;
// Zenject.InjectContext
struct InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649;




#ifndef U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#define U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASSERT_T1D409464297E17E6BAFCDF305224F40F9AE91C4E_H
#define ASSERT_T1D409464297E17E6BAFCDF305224F40F9AE91C4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Assert
struct  Assert_t1D409464297E17E6BAFCDF305224F40F9AE91C4E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSERT_T1D409464297E17E6BAFCDF305224F40F9AE91C4E_H
#ifndef LINQEXTENSIONS_T18ED2D71F925F67E80E13CC7A35D1A07A1EA6756_H
#define LINQEXTENSIONS_T18ED2D71F925F67E80E13CC7A35D1A07A1EA6756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.LinqExtensions
struct  LinqExtensions_t18ED2D71F925F67E80E13CC7A35D1A07A1EA6756  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINQEXTENSIONS_T18ED2D71F925F67E80E13CC7A35D1A07A1EA6756_H
#ifndef LOG_TBEB2A9D01160FC4BDAEF167CA277F88E845B4122_H
#define LOG_TBEB2A9D01160FC4BDAEF167CA277F88E845B4122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Log
struct  Log_tBEB2A9D01160FC4BDAEF167CA277F88E845B4122  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOG_TBEB2A9D01160FC4BDAEF167CA277F88E845B4122_H
#ifndef MISCEXTENSIONS_TA08EEED147F29A323A776DB66DCEBF7E8DBFE90F_H
#define MISCEXTENSIONS_TA08EEED147F29A323A776DB66DCEBF7E8DBFE90F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.MiscExtensions
struct  MiscExtensions_tA08EEED147F29A323A776DB66DCEBF7E8DBFE90F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISCEXTENSIONS_TA08EEED147F29A323A776DB66DCEBF7E8DBFE90F_H
#ifndef TYPEEXTENSIONS_T28E8496E615D0C548B6325A5FFC0A1D0C273A618_H
#define TYPEEXTENSIONS_T28E8496E615D0C548B6325A5FFC0A1D0C273A618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions
struct  TypeExtensions_t28E8496E615D0C548B6325A5FFC0A1D0C273A618  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEEXTENSIONS_T28E8496E615D0C548B6325A5FFC0A1D0C273A618_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_TD105F0859917A937A010EB05978E691B4085EF27_H
#define U3CU3EC__DISPLAYCLASS35_0_TD105F0859917A937A010EB05978E691B4085EF27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions/<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_tD105F0859917A937A010EB05978E691B4085EF27  : public RuntimeObject
{
public:
	// System.Type[] ModestTree.TypeExtensions/<>c__DisplayClass35_0::attributeTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___attributeTypes_0;

public:
	inline static int32_t get_offset_of_attributeTypes_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_tD105F0859917A937A010EB05978E691B4085EF27, ___attributeTypes_0)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_attributeTypes_0() const { return ___attributeTypes_0; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_attributeTypes_0() { return &___attributeTypes_0; }
	inline void set_attributeTypes_0(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___attributeTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributeTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_TD105F0859917A937A010EB05978E691B4085EF27_H
#ifndef U3CU3EC__DISPLAYCLASS35_1_TB7BD6D99E82DA965FE024902ED2CD5574D4C6B3E_H
#define U3CU3EC__DISPLAYCLASS35_1_TB7BD6D99E82DA965FE024902ED2CD5574D4C6B3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions/<>c__DisplayClass35_1
struct  U3CU3Ec__DisplayClass35_1_tB7BD6D99E82DA965FE024902ED2CD5574D4C6B3E  : public RuntimeObject
{
public:
	// System.Attribute ModestTree.TypeExtensions/<>c__DisplayClass35_1::a
	Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_1_tB7BD6D99E82DA965FE024902ED2CD5574D4C6B3E, ___a_0)); }
	inline Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * get_a_0() const { return ___a_0; }
	inline Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_1_TB7BD6D99E82DA965FE024902ED2CD5574D4C6B3E_H
#ifndef U3CU3EC__DISPLAYCLASS39_0_TCEA349F5BEE44478656EA9E1DA5308913F1ADF0B_H
#define U3CU3EC__DISPLAYCLASS39_0_TCEA349F5BEE44478656EA9E1DA5308913F1ADF0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions/<>c__DisplayClass39_0
struct  U3CU3Ec__DisplayClass39_0_tCEA349F5BEE44478656EA9E1DA5308913F1ADF0B  : public RuntimeObject
{
public:
	// System.Type[] ModestTree.TypeExtensions/<>c__DisplayClass39_0::attributeTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___attributeTypes_0;

public:
	inline static int32_t get_offset_of_attributeTypes_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_tCEA349F5BEE44478656EA9E1DA5308913F1ADF0B, ___attributeTypes_0)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_attributeTypes_0() const { return ___attributeTypes_0; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_attributeTypes_0() { return &___attributeTypes_0; }
	inline void set_attributeTypes_0(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___attributeTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributeTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS39_0_TCEA349F5BEE44478656EA9E1DA5308913F1ADF0B_H
#ifndef U3CU3EC__DISPLAYCLASS39_1_T2A7417B67AA1DAFB430C62DD67BF51F12914E338_H
#define U3CU3EC__DISPLAYCLASS39_1_T2A7417B67AA1DAFB430C62DD67BF51F12914E338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions/<>c__DisplayClass39_1
struct  U3CU3Ec__DisplayClass39_1_t2A7417B67AA1DAFB430C62DD67BF51F12914E338  : public RuntimeObject
{
public:
	// System.Attribute ModestTree.TypeExtensions/<>c__DisplayClass39_1::a
	Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_1_t2A7417B67AA1DAFB430C62DD67BF51F12914E338, ___a_0)); }
	inline Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * get_a_0() const { return ___a_0; }
	inline Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS39_1_T2A7417B67AA1DAFB430C62DD67BF51F12914E338_H
#ifndef U3CGETALLINSTANCEFIELDSU3ED__25_T0EC5EC457FB44071D166A14BA4FC5F6BE341372D_H
#define U3CGETALLINSTANCEFIELDSU3ED__25_T0EC5EC457FB44071D166A14BA4FC5F6BE341372D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions/<GetAllInstanceFields>d__25
struct  U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.TypeExtensions/<GetAllInstanceFields>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Reflection.FieldInfo ModestTree.TypeExtensions/<GetAllInstanceFields>d__25::<>2__current
	FieldInfo_t * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.TypeExtensions/<GetAllInstanceFields>d__25::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type ModestTree.TypeExtensions/<GetAllInstanceFields>d__25::type
	Type_t * ___type_3;
	// System.Type ModestTree.TypeExtensions/<GetAllInstanceFields>d__25::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Reflection.FieldInfo[] ModestTree.TypeExtensions/<GetAllInstanceFields>d__25::<>7__wrap1
	FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* ___U3CU3E7__wrap1_5;
	// System.Int32 ModestTree.TypeExtensions/<GetAllInstanceFields>d__25::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_6;
	// System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo> ModestTree.TypeExtensions/<GetAllInstanceFields>d__25::<>7__wrap3
	RuntimeObject* ___U3CU3E7__wrap3_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D, ___U3CU3E2__current_1)); }
	inline FieldInfo_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline FieldInfo_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(FieldInfo_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D, ___U3CU3E7__wrap1_5)); }
	inline FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D, ___U3CU3E7__wrap2_6)); }
	inline int32_t get_U3CU3E7__wrap2_6() const { return ___U3CU3E7__wrap2_6; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_6() { return &___U3CU3E7__wrap2_6; }
	inline void set_U3CU3E7__wrap2_6(int32_t value)
	{
		___U3CU3E7__wrap2_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap3_7() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D, ___U3CU3E7__wrap3_7)); }
	inline RuntimeObject* get_U3CU3E7__wrap3_7() const { return ___U3CU3E7__wrap3_7; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap3_7() { return &___U3CU3E7__wrap3_7; }
	inline void set_U3CU3E7__wrap3_7(RuntimeObject* value)
	{
		___U3CU3E7__wrap3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap3_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCEFIELDSU3ED__25_T0EC5EC457FB44071D166A14BA4FC5F6BE341372D_H
#ifndef U3CGETALLINSTANCEMETHODSU3ED__27_TB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6_H
#define U3CGETALLINSTANCEMETHODSU3ED__27_TB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions/<GetAllInstanceMethods>d__27
struct  U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.TypeExtensions/<GetAllInstanceMethods>d__27::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Reflection.MethodInfo ModestTree.TypeExtensions/<GetAllInstanceMethods>d__27::<>2__current
	MethodInfo_t * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.TypeExtensions/<GetAllInstanceMethods>d__27::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type ModestTree.TypeExtensions/<GetAllInstanceMethods>d__27::type
	Type_t * ___type_3;
	// System.Type ModestTree.TypeExtensions/<GetAllInstanceMethods>d__27::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Reflection.MethodInfo[] ModestTree.TypeExtensions/<GetAllInstanceMethods>d__27::<>7__wrap1
	MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* ___U3CU3E7__wrap1_5;
	// System.Int32 ModestTree.TypeExtensions/<GetAllInstanceMethods>d__27::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_6;
	// System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo> ModestTree.TypeExtensions/<GetAllInstanceMethods>d__27::<>7__wrap3
	RuntimeObject* ___U3CU3E7__wrap3_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6, ___U3CU3E2__current_1)); }
	inline MethodInfo_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline MethodInfo_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(MethodInfo_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6, ___U3CU3E7__wrap1_5)); }
	inline MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6, ___U3CU3E7__wrap2_6)); }
	inline int32_t get_U3CU3E7__wrap2_6() const { return ___U3CU3E7__wrap2_6; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_6() { return &___U3CU3E7__wrap2_6; }
	inline void set_U3CU3E7__wrap2_6(int32_t value)
	{
		___U3CU3E7__wrap2_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap3_7() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6, ___U3CU3E7__wrap3_7)); }
	inline RuntimeObject* get_U3CU3E7__wrap3_7() const { return ___U3CU3E7__wrap3_7; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap3_7() { return &___U3CU3E7__wrap3_7; }
	inline void set_U3CU3E7__wrap3_7(RuntimeObject* value)
	{
		___U3CU3E7__wrap3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap3_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCEMETHODSU3ED__27_TB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6_H
#ifndef U3CGETALLINSTANCEPROPERTIESU3ED__26_T6A44E91AAC0A12872A97708C9E4CEEE90F974758_H
#define U3CGETALLINSTANCEPROPERTIESU3ED__26_T6A44E91AAC0A12872A97708C9E4CEEE90F974758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions/<GetAllInstanceProperties>d__26
struct  U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.TypeExtensions/<GetAllInstanceProperties>d__26::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Reflection.PropertyInfo ModestTree.TypeExtensions/<GetAllInstanceProperties>d__26::<>2__current
	PropertyInfo_t * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.TypeExtensions/<GetAllInstanceProperties>d__26::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type ModestTree.TypeExtensions/<GetAllInstanceProperties>d__26::type
	Type_t * ___type_3;
	// System.Type ModestTree.TypeExtensions/<GetAllInstanceProperties>d__26::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Reflection.PropertyInfo[] ModestTree.TypeExtensions/<GetAllInstanceProperties>d__26::<>7__wrap1
	PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E* ___U3CU3E7__wrap1_5;
	// System.Int32 ModestTree.TypeExtensions/<GetAllInstanceProperties>d__26::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_6;
	// System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo> ModestTree.TypeExtensions/<GetAllInstanceProperties>d__26::<>7__wrap3
	RuntimeObject* ___U3CU3E7__wrap3_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758, ___U3CU3E2__current_1)); }
	inline PropertyInfo_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline PropertyInfo_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(PropertyInfo_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758, ___U3CU3E7__wrap1_5)); }
	inline PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758, ___U3CU3E7__wrap2_6)); }
	inline int32_t get_U3CU3E7__wrap2_6() const { return ___U3CU3E7__wrap2_6; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_6() { return &___U3CU3E7__wrap2_6; }
	inline void set_U3CU3E7__wrap2_6(int32_t value)
	{
		___U3CU3E7__wrap2_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap3_7() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758, ___U3CU3E7__wrap3_7)); }
	inline RuntimeObject* get_U3CU3E7__wrap3_7() const { return ___U3CU3E7__wrap3_7; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap3_7() { return &___U3CU3E7__wrap3_7; }
	inline void set_U3CU3E7__wrap3_7(RuntimeObject* value)
	{
		___U3CU3E7__wrap3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap3_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCEPROPERTIESU3ED__26_T6A44E91AAC0A12872A97708C9E4CEEE90F974758_H
#ifndef U3CGETPARENTTYPESU3ED__22_TD3493BD30A0DF8981E61EEF94164ECBD09F73536_H
#define U3CGETPARENTTYPESU3ED__22_TD3493BD30A0DF8981E61EEF94164ECBD09F73536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions/<GetParentTypes>d__22
struct  U3CGetParentTypesU3Ed__22_tD3493BD30A0DF8981E61EEF94164ECBD09F73536  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.TypeExtensions/<GetParentTypes>d__22::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Type ModestTree.TypeExtensions/<GetParentTypes>d__22::<>2__current
	Type_t * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.TypeExtensions/<GetParentTypes>d__22::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type ModestTree.TypeExtensions/<GetParentTypes>d__22::type
	Type_t * ___type_3;
	// System.Type ModestTree.TypeExtensions/<GetParentTypes>d__22::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Collections.Generic.IEnumerator`1<System.Type> ModestTree.TypeExtensions/<GetParentTypes>d__22::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ed__22_tD3493BD30A0DF8981E61EEF94164ECBD09F73536, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ed__22_tD3493BD30A0DF8981E61EEF94164ECBD09F73536, ___U3CU3E2__current_1)); }
	inline Type_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Type_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Type_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ed__22_tD3493BD30A0DF8981E61EEF94164ECBD09F73536, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ed__22_tD3493BD30A0DF8981E61EEF94164ECBD09F73536, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ed__22_tD3493BD30A0DF8981E61EEF94164ECBD09F73536, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ed__22_tD3493BD30A0DF8981E61EEF94164ECBD09F73536, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPARENTTYPESU3ED__22_TD3493BD30A0DF8981E61EEF94164ECBD09F73536_H
#ifndef REFLECTIONUTIL_TF6D9CF8AB3379B68701E8A6CC722245F0D3F7398_H
#define REFLECTIONUTIL_TF6D9CF8AB3379B68701E8A6CC722245F0D3F7398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.ReflectionUtil
struct  ReflectionUtil_tF6D9CF8AB3379B68701E8A6CC722245F0D3F7398  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONUTIL_TF6D9CF8AB3379B68701E8A6CC722245F0D3F7398_H
#ifndef FIELDMEMBERINFO_TA6FDF3FCEC37981017599C2FA4D39B914896F742_H
#define FIELDMEMBERINFO_TA6FDF3FCEC37981017599C2FA4D39B914896F742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.ReflectionUtil/FieldMemberInfo
struct  FieldMemberInfo_tA6FDF3FCEC37981017599C2FA4D39B914896F742  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo ModestTree.Util.ReflectionUtil/FieldMemberInfo::_fieldInfo
	FieldInfo_t * ____fieldInfo_0;

public:
	inline static int32_t get_offset_of__fieldInfo_0() { return static_cast<int32_t>(offsetof(FieldMemberInfo_tA6FDF3FCEC37981017599C2FA4D39B914896F742, ____fieldInfo_0)); }
	inline FieldInfo_t * get__fieldInfo_0() const { return ____fieldInfo_0; }
	inline FieldInfo_t ** get_address_of__fieldInfo_0() { return &____fieldInfo_0; }
	inline void set__fieldInfo_0(FieldInfo_t * value)
	{
		____fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDMEMBERINFO_TA6FDF3FCEC37981017599C2FA4D39B914896F742_H
#ifndef PROPERTYMEMBERINFO_TA3305BB1D65076A61B05007962E78A8233A14690_H
#define PROPERTYMEMBERINFO_TA3305BB1D65076A61B05007962E78A8233A14690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.ReflectionUtil/PropertyMemberInfo
struct  PropertyMemberInfo_tA3305BB1D65076A61B05007962E78A8233A14690  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo ModestTree.Util.ReflectionUtil/PropertyMemberInfo::_propInfo
	PropertyInfo_t * ____propInfo_0;

public:
	inline static int32_t get_offset_of__propInfo_0() { return static_cast<int32_t>(offsetof(PropertyMemberInfo_tA3305BB1D65076A61B05007962E78A8233A14690, ____propInfo_0)); }
	inline PropertyInfo_t * get__propInfo_0() const { return ____propInfo_0; }
	inline PropertyInfo_t ** get_address_of__propInfo_0() { return &____propInfo_0; }
	inline void set__propInfo_0(PropertyInfo_t * value)
	{
		____propInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____propInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYMEMBERINFO_TA3305BB1D65076A61B05007962E78A8233A14690_H
#ifndef UNITYUTIL_T672DBCFAD039DEA1AC89AAAEF7C782A70F3CFADB_H
#define UNITYUTIL_T672DBCFAD039DEA1AC89AAAEF7C782A70F3CFADB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.UnityUtil
struct  UnityUtil_t672DBCFAD039DEA1AC89AAAEF7C782A70F3CFADB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYUTIL_T672DBCFAD039DEA1AC89AAAEF7C782A70F3CFADB_H
#ifndef U3CU3EC_T9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_H
#define U3CU3EC_T9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.UnityUtil/<>c
struct  U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields
{
public:
	// ModestTree.Util.UnityUtil/<>c ModestTree.Util.UnityUtil/<>c::<>9
	U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.SceneManagement.Scene,System.Boolean> ModestTree.Util.UnityUtil/<>c::<>9__3_0
	Func_2_t62BEF835FEEE197E60620EC309AE8C51F47FCE54 * ___U3CU3E9__3_0_1;
	// System.Func`2<UnityEngine.Transform,UnityEngine.GameObject> ModestTree.Util.UnityUtil/<>c::<>9__15_0
	Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF * ___U3CU3E9__15_0_2;
	// System.Func`2<UnityEngine.Component,System.Int32> ModestTree.Util.UnityUtil/<>c::<>9__18_0
	Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 * ___U3CU3E9__18_0_3;
	// System.Func`2<UnityEngine.Component,System.Int32> ModestTree.Util.UnityUtil/<>c::<>9__19_0
	Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 * ___U3CU3E9__19_0_4;
	// System.Func`2<UnityEngine.Transform,UnityEngine.GameObject> ModestTree.Util.UnityUtil/<>c::<>9__22_0
	Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF * ___U3CU3E9__22_0_5;
	// System.Func`2<UnityEngine.GameObject,System.Boolean> ModestTree.Util.UnityUtil/<>c::<>9__23_0
	Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 * ___U3CU3E9__23_0_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_2_t62BEF835FEEE197E60620EC309AE8C51F47FCE54 * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_2_t62BEF835FEEE197E60620EC309AE8C51F47FCE54 ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_2_t62BEF835FEEE197E60620EC309AE8C51F47FCE54 * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields, ___U3CU3E9__15_0_2)); }
	inline Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF * get_U3CU3E9__15_0_2() const { return ___U3CU3E9__15_0_2; }
	inline Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF ** get_address_of_U3CU3E9__15_0_2() { return &___U3CU3E9__15_0_2; }
	inline void set_U3CU3E9__15_0_2(Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF * value)
	{
		___U3CU3E9__15_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__15_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields, ___U3CU3E9__18_0_3)); }
	inline Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 * get_U3CU3E9__18_0_3() const { return ___U3CU3E9__18_0_3; }
	inline Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 ** get_address_of_U3CU3E9__18_0_3() { return &___U3CU3E9__18_0_3; }
	inline void set_U3CU3E9__18_0_3(Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 * value)
	{
		___U3CU3E9__18_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__19_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields, ___U3CU3E9__19_0_4)); }
	inline Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 * get_U3CU3E9__19_0_4() const { return ___U3CU3E9__19_0_4; }
	inline Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 ** get_address_of_U3CU3E9__19_0_4() { return &___U3CU3E9__19_0_4; }
	inline void set_U3CU3E9__19_0_4(Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 * value)
	{
		___U3CU3E9__19_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__19_0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields, ___U3CU3E9__22_0_5)); }
	inline Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF * get_U3CU3E9__22_0_5() const { return ___U3CU3E9__22_0_5; }
	inline Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF ** get_address_of_U3CU3E9__22_0_5() { return &___U3CU3E9__22_0_5; }
	inline void set_U3CU3E9__22_0_5(Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF * value)
	{
		___U3CU3E9__22_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__22_0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__23_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields, ___U3CU3E9__23_0_6)); }
	inline Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 * get_U3CU3E9__23_0_6() const { return ___U3CU3E9__23_0_6; }
	inline Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 ** get_address_of_U3CU3E9__23_0_6() { return &___U3CU3E9__23_0_6; }
	inline void set_U3CU3E9__23_0_6(Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 * value)
	{
		___U3CU3E9__23_0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__23_0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_H
#ifndef U3CGETDIRECTCHILDRENU3ED__21_TC03F8478B7343DE6415986513B9F0000AAB056F1_H
#define U3CGETDIRECTCHILDRENU3ED__21_TC03F8478B7343DE6415986513B9F0000AAB056F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.UnityUtil/<GetDirectChildren>d__21
struct  U3CGetDirectChildrenU3Ed__21_tC03F8478B7343DE6415986513B9F0000AAB056F1  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.Util.UnityUtil/<GetDirectChildren>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.GameObject ModestTree.Util.UnityUtil/<GetDirectChildren>d__21::<>2__current
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.Util.UnityUtil/<GetDirectChildren>d__21::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.GameObject ModestTree.Util.UnityUtil/<GetDirectChildren>d__21::obj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_3;
	// UnityEngine.GameObject ModestTree.Util.UnityUtil/<GetDirectChildren>d__21::<>3__obj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CU3E3__obj_4;
	// System.Collections.IEnumerator ModestTree.Util.UnityUtil/<GetDirectChildren>d__21::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenU3Ed__21_tC03F8478B7343DE6415986513B9F0000AAB056F1, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenU3Ed__21_tC03F8478B7343DE6415986513B9F0000AAB056F1, ___U3CU3E2__current_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenU3Ed__21_tC03F8478B7343DE6415986513B9F0000AAB056F1, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_obj_3() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenU3Ed__21_tC03F8478B7343DE6415986513B9F0000AAB056F1, ___obj_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_3() const { return ___obj_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_3() { return &___obj_3; }
	inline void set_obj_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_3 = value;
		Il2CppCodeGenWriteBarrier((&___obj_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__obj_4() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenU3Ed__21_tC03F8478B7343DE6415986513B9F0000AAB056F1, ___U3CU3E3__obj_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CU3E3__obj_4() const { return ___U3CU3E3__obj_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CU3E3__obj_4() { return &___U3CU3E3__obj_4; }
	inline void set_U3CU3E3__obj_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CU3E3__obj_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__obj_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenU3Ed__21_tC03F8478B7343DE6415986513B9F0000AAB056F1, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETDIRECTCHILDRENU3ED__21_TC03F8478B7343DE6415986513B9F0000AAB056F1_H
#ifndef U3CGETDIRECTCHILDRENANDSELFU3ED__20_T5B7D4937C4166565E1DF30D8E977D348E933249E_H
#define U3CGETDIRECTCHILDRENANDSELFU3ED__20_T5B7D4937C4166565E1DF30D8E977D348E933249E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.UnityUtil/<GetDirectChildrenAndSelf>d__20
struct  U3CGetDirectChildrenAndSelfU3Ed__20_t5B7D4937C4166565E1DF30D8E977D348E933249E  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.Util.UnityUtil/<GetDirectChildrenAndSelf>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.GameObject ModestTree.Util.UnityUtil/<GetDirectChildrenAndSelf>d__20::<>2__current
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.Util.UnityUtil/<GetDirectChildrenAndSelf>d__20::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.GameObject ModestTree.Util.UnityUtil/<GetDirectChildrenAndSelf>d__20::obj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_3;
	// UnityEngine.GameObject ModestTree.Util.UnityUtil/<GetDirectChildrenAndSelf>d__20::<>3__obj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CU3E3__obj_4;
	// System.Collections.IEnumerator ModestTree.Util.UnityUtil/<GetDirectChildrenAndSelf>d__20::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenAndSelfU3Ed__20_t5B7D4937C4166565E1DF30D8E977D348E933249E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenAndSelfU3Ed__20_t5B7D4937C4166565E1DF30D8E977D348E933249E, ___U3CU3E2__current_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenAndSelfU3Ed__20_t5B7D4937C4166565E1DF30D8E977D348E933249E, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_obj_3() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenAndSelfU3Ed__20_t5B7D4937C4166565E1DF30D8E977D348E933249E, ___obj_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_3() const { return ___obj_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_3() { return &___obj_3; }
	inline void set_obj_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_3 = value;
		Il2CppCodeGenWriteBarrier((&___obj_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__obj_4() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenAndSelfU3Ed__20_t5B7D4937C4166565E1DF30D8E977D348E933249E, ___U3CU3E3__obj_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CU3E3__obj_4() const { return ___U3CU3E3__obj_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CU3E3__obj_4() { return &___U3CU3E3__obj_4; }
	inline void set_U3CU3E3__obj_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CU3E3__obj_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__obj_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenAndSelfU3Ed__20_t5B7D4937C4166565E1DF30D8E977D348E933249E, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETDIRECTCHILDRENANDSELFU3ED__20_T5B7D4937C4166565E1DF30D8E977D348E933249E_H
#ifndef U3CGETPARENTSU3ED__16_TCE22A4DD1807053D4B004E17C1D8EF2ED7231C81_H
#define U3CGETPARENTSU3ED__16_TCE22A4DD1807053D4B004E17C1D8EF2ED7231C81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.UnityUtil/<GetParents>d__16
struct  U3CGetParentsU3Ed__16_tCE22A4DD1807053D4B004E17C1D8EF2ED7231C81  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.Util.UnityUtil/<GetParents>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Transform ModestTree.Util.UnityUtil/<GetParents>d__16::<>2__current
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.Util.UnityUtil/<GetParents>d__16::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Transform ModestTree.Util.UnityUtil/<GetParents>d__16::transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform_3;
	// UnityEngine.Transform ModestTree.Util.UnityUtil/<GetParents>d__16::<>3__transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CU3E3__transform_4;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> ModestTree.Util.UnityUtil/<GetParents>d__16::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetParentsU3Ed__16_tCE22A4DD1807053D4B004E17C1D8EF2ED7231C81, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetParentsU3Ed__16_tCE22A4DD1807053D4B004E17C1D8EF2ED7231C81, ___U3CU3E2__current_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetParentsU3Ed__16_tCE22A4DD1807053D4B004E17C1D8EF2ED7231C81, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_transform_3() { return static_cast<int32_t>(offsetof(U3CGetParentsU3Ed__16_tCE22A4DD1807053D4B004E17C1D8EF2ED7231C81, ___transform_3)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_transform_3() const { return ___transform_3; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_transform_3() { return &___transform_3; }
	inline void set_transform_3(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___transform_3 = value;
		Il2CppCodeGenWriteBarrier((&___transform_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__transform_4() { return static_cast<int32_t>(offsetof(U3CGetParentsU3Ed__16_tCE22A4DD1807053D4B004E17C1D8EF2ED7231C81, ___U3CU3E3__transform_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CU3E3__transform_4() const { return ___U3CU3E3__transform_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CU3E3__transform_4() { return &___U3CU3E3__transform_4; }
	inline void set_U3CU3E3__transform_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CU3E3__transform_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__transform_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetParentsU3Ed__16_tCE22A4DD1807053D4B004E17C1D8EF2ED7231C81, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPARENTSU3ED__16_TCE22A4DD1807053D4B004E17C1D8EF2ED7231C81_H
#ifndef U3CGETPARENTSANDSELFU3ED__17_T8E038DAE36078185EEC75BB09A12F00C4393222F_H
#define U3CGETPARENTSANDSELFU3ED__17_T8E038DAE36078185EEC75BB09A12F00C4393222F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.UnityUtil/<GetParentsAndSelf>d__17
struct  U3CGetParentsAndSelfU3Ed__17_t8E038DAE36078185EEC75BB09A12F00C4393222F  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.Util.UnityUtil/<GetParentsAndSelf>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Transform ModestTree.Util.UnityUtil/<GetParentsAndSelf>d__17::<>2__current
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.Util.UnityUtil/<GetParentsAndSelf>d__17::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Transform ModestTree.Util.UnityUtil/<GetParentsAndSelf>d__17::transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform_3;
	// UnityEngine.Transform ModestTree.Util.UnityUtil/<GetParentsAndSelf>d__17::<>3__transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CU3E3__transform_4;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> ModestTree.Util.UnityUtil/<GetParentsAndSelf>d__17::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetParentsAndSelfU3Ed__17_t8E038DAE36078185EEC75BB09A12F00C4393222F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetParentsAndSelfU3Ed__17_t8E038DAE36078185EEC75BB09A12F00C4393222F, ___U3CU3E2__current_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetParentsAndSelfU3Ed__17_t8E038DAE36078185EEC75BB09A12F00C4393222F, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_transform_3() { return static_cast<int32_t>(offsetof(U3CGetParentsAndSelfU3Ed__17_t8E038DAE36078185EEC75BB09A12F00C4393222F, ___transform_3)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_transform_3() const { return ___transform_3; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_transform_3() { return &___transform_3; }
	inline void set_transform_3(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___transform_3 = value;
		Il2CppCodeGenWriteBarrier((&___transform_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__transform_4() { return static_cast<int32_t>(offsetof(U3CGetParentsAndSelfU3Ed__17_t8E038DAE36078185EEC75BB09A12F00C4393222F, ___U3CU3E3__transform_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CU3E3__transform_4() const { return ___U3CU3E3__transform_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CU3E3__transform_4() { return &___U3CU3E3__transform_4; }
	inline void set_U3CU3E3__transform_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CU3E3__transform_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__transform_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetParentsAndSelfU3Ed__17_t8E038DAE36078185EEC75BB09A12F00C4393222F, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPARENTSANDSELFU3ED__17_T8E038DAE36078185EEC75BB09A12F00C4393222F_H
#ifndef VALUEPAIR_T8A8BF8371ACB1577D803259E8806F08789DA9790_H
#define VALUEPAIR_T8A8BF8371ACB1577D803259E8806F08789DA9790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.ValuePair
struct  ValuePair_t8A8BF8371ACB1577D803259E8806F08789DA9790  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEPAIR_T8A8BF8371ACB1577D803259E8806F08789DA9790_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_TDD8AA92B02159AE254DB694D2355FACAC8ECE8F8_H
#define U3CU3EC__DISPLAYCLASS2_0_TDD8AA92B02159AE254DB694D2355FACAC8ECE8F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConditionCopyNonLazyBinder/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_tDD8AA92B02159AE254DB694D2355FACAC8ECE8F8  : public RuntimeObject
{
public:
	// System.Object Zenject.ConditionCopyNonLazyBinder/<>c__DisplayClass2_0::instance
	RuntimeObject * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tDD8AA92B02159AE254DB694D2355FACAC8ECE8F8, ___instance_0)); }
	inline RuntimeObject * get_instance_0() const { return ___instance_0; }
	inline RuntimeObject ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(RuntimeObject * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_TDD8AA92B02159AE254DB694D2355FACAC8ECE8F8_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T3D5A232390CE7E36DE46E2363E2E54C61139847E_H
#define U3CU3EC__DISPLAYCLASS3_0_T3D5A232390CE7E36DE46E2363E2E54C61139847E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConditionCopyNonLazyBinder/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t3D5A232390CE7E36DE46E2363E2E54C61139847E  : public RuntimeObject
{
public:
	// System.Type[] Zenject.ConditionCopyNonLazyBinder/<>c__DisplayClass3_0::targets
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___targets_0;

public:
	inline static int32_t get_offset_of_targets_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t3D5A232390CE7E36DE46E2363E2E54C61139847E, ___targets_0)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_targets_0() const { return ___targets_0; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_targets_0() { return &___targets_0; }
	inline void set_targets_0(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___targets_0 = value;
		Il2CppCodeGenWriteBarrier((&___targets_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T3D5A232390CE7E36DE46E2363E2E54C61139847E_H
#ifndef U3CU3EC__DISPLAYCLASS3_1_T8C5F61FF5A726750B091149FA1677E71AABD791A_H
#define U3CU3EC__DISPLAYCLASS3_1_T8C5F61FF5A726750B091149FA1677E71AABD791A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConditionCopyNonLazyBinder/<>c__DisplayClass3_1
struct  U3CU3Ec__DisplayClass3_1_t8C5F61FF5A726750B091149FA1677E71AABD791A  : public RuntimeObject
{
public:
	// Zenject.InjectContext Zenject.ConditionCopyNonLazyBinder/<>c__DisplayClass3_1::r
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___r_0;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_1_t8C5F61FF5A726750B091149FA1677E71AABD791A, ___r_0)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_r_0() const { return ___r_0; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___r_0 = value;
		Il2CppCodeGenWriteBarrier((&___r_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_1_T8C5F61FF5A726750B091149FA1677E71AABD791A_H
#ifndef CONVENTIONASSEMBLYSELECTIONBINDER_T121902B526295C714455135BDE7F872C01973C4E_H
#define CONVENTIONASSEMBLYSELECTIONBINDER_T121902B526295C714455135BDE7F872C01973C4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionAssemblySelectionBinder
struct  ConventionAssemblySelectionBinder_t121902B526295C714455135BDE7F872C01973C4E  : public RuntimeObject
{
public:
	// Zenject.ConventionBindInfo Zenject.ConventionAssemblySelectionBinder::<BindInfo>k__BackingField
	ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757 * ___U3CBindInfoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBindInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConventionAssemblySelectionBinder_t121902B526295C714455135BDE7F872C01973C4E, ___U3CBindInfoU3Ek__BackingField_0)); }
	inline ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757 * get_U3CBindInfoU3Ek__BackingField_0() const { return ___U3CBindInfoU3Ek__BackingField_0; }
	inline ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757 ** get_address_of_U3CBindInfoU3Ek__BackingField_0() { return &___U3CBindInfoU3Ek__BackingField_0; }
	inline void set_U3CBindInfoU3Ek__BackingField_0(ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757 * value)
	{
		___U3CBindInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindInfoU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVENTIONASSEMBLYSELECTIONBINDER_T121902B526295C714455135BDE7F872C01973C4E_H
#ifndef U3CU3EC_T2E35C3D6CC4AB1E6E4B44A4F27D5C180BF9B4D60_H
#define U3CU3EC_T2E35C3D6CC4AB1E6E4B44A4F27D5C180BF9B4D60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionAssemblySelectionBinder/<>c
struct  U3CU3Ec_t2E35C3D6CC4AB1E6E4B44A4F27D5C180BF9B4D60  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2E35C3D6CC4AB1E6E4B44A4F27D5C180BF9B4D60_StaticFields
{
public:
	// Zenject.ConventionAssemblySelectionBinder/<>c Zenject.ConventionAssemblySelectionBinder/<>c::<>9
	U3CU3Ec_t2E35C3D6CC4AB1E6E4B44A4F27D5C180BF9B4D60 * ___U3CU3E9_0;
	// System.Func`2<System.Type,System.Reflection.Assembly> Zenject.ConventionAssemblySelectionBinder/<>c::<>9__8_0
	Func_2_t751A0CA37094CFBF9643B9A1A5381D7F7706AD84 * ___U3CU3E9__8_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2E35C3D6CC4AB1E6E4B44A4F27D5C180BF9B4D60_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2E35C3D6CC4AB1E6E4B44A4F27D5C180BF9B4D60 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2E35C3D6CC4AB1E6E4B44A4F27D5C180BF9B4D60 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2E35C3D6CC4AB1E6E4B44A4F27D5C180BF9B4D60 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2E35C3D6CC4AB1E6E4B44A4F27D5C180BF9B4D60_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_t751A0CA37094CFBF9643B9A1A5381D7F7706AD84 * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_t751A0CA37094CFBF9643B9A1A5381D7F7706AD84 ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_t751A0CA37094CFBF9643B9A1A5381D7F7706AD84 * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2E35C3D6CC4AB1E6E4B44A4F27D5C180BF9B4D60_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_TE0D505E95E7C51EE130DEFCAB49C3064031474C8_H
#define U3CU3EC__DISPLAYCLASS12_0_TE0D505E95E7C51EE130DEFCAB49C3064031474C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionAssemblySelectionBinder/<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_tE0D505E95E7C51EE130DEFCAB49C3064031474C8  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerable`1<System.Reflection.Assembly> Zenject.ConventionAssemblySelectionBinder/<>c__DisplayClass12_0::assemblies
	RuntimeObject* ___assemblies_0;

public:
	inline static int32_t get_offset_of_assemblies_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_tE0D505E95E7C51EE130DEFCAB49C3064031474C8, ___assemblies_0)); }
	inline RuntimeObject* get_assemblies_0() const { return ___assemblies_0; }
	inline RuntimeObject** get_address_of_assemblies_0() { return &___assemblies_0; }
	inline void set_assemblies_0(RuntimeObject* value)
	{
		___assemblies_0 = value;
		Il2CppCodeGenWriteBarrier((&___assemblies_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_TE0D505E95E7C51EE130DEFCAB49C3064031474C8_H
#ifndef CONVENTIONBINDINFO_T0C1BF3F6EB989B628E3D30925957E0CDC7629757_H
#define CONVENTIONBINDINFO_T0C1BF3F6EB989B628E3D30925957E0CDC7629757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionBindInfo
struct  ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Func`2<System.Type,System.Boolean>> Zenject.ConventionBindInfo::_typeFilters
	List_1_t949487C81A6A48214F1F028DEDA38942444C592F * ____typeFilters_0;
	// System.Collections.Generic.List`1<System.Func`2<System.Reflection.Assembly,System.Boolean>> Zenject.ConventionBindInfo::_assemblyFilters
	List_1_t808B4CBCA4E9C3FB196A98237AFA7B11A2C4FE6B * ____assemblyFilters_1;

public:
	inline static int32_t get_offset_of__typeFilters_0() { return static_cast<int32_t>(offsetof(ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757, ____typeFilters_0)); }
	inline List_1_t949487C81A6A48214F1F028DEDA38942444C592F * get__typeFilters_0() const { return ____typeFilters_0; }
	inline List_1_t949487C81A6A48214F1F028DEDA38942444C592F ** get_address_of__typeFilters_0() { return &____typeFilters_0; }
	inline void set__typeFilters_0(List_1_t949487C81A6A48214F1F028DEDA38942444C592F * value)
	{
		____typeFilters_0 = value;
		Il2CppCodeGenWriteBarrier((&____typeFilters_0), value);
	}

	inline static int32_t get_offset_of__assemblyFilters_1() { return static_cast<int32_t>(offsetof(ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757, ____assemblyFilters_1)); }
	inline List_1_t808B4CBCA4E9C3FB196A98237AFA7B11A2C4FE6B * get__assemblyFilters_1() const { return ____assemblyFilters_1; }
	inline List_1_t808B4CBCA4E9C3FB196A98237AFA7B11A2C4FE6B ** get_address_of__assemblyFilters_1() { return &____assemblyFilters_1; }
	inline void set__assemblyFilters_1(List_1_t808B4CBCA4E9C3FB196A98237AFA7B11A2C4FE6B * value)
	{
		____assemblyFilters_1 = value;
		Il2CppCodeGenWriteBarrier((&____assemblyFilters_1), value);
	}
};

struct ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Reflection.Assembly,System.Type[]> Zenject.ConventionBindInfo::_assemblyTypeCache
	Dictionary_2_tC371B9A7E8B4A801BC5A4A04E678C970C09DA4FB * ____assemblyTypeCache_2;

public:
	inline static int32_t get_offset_of__assemblyTypeCache_2() { return static_cast<int32_t>(offsetof(ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757_StaticFields, ____assemblyTypeCache_2)); }
	inline Dictionary_2_tC371B9A7E8B4A801BC5A4A04E678C970C09DA4FB * get__assemblyTypeCache_2() const { return ____assemblyTypeCache_2; }
	inline Dictionary_2_tC371B9A7E8B4A801BC5A4A04E678C970C09DA4FB ** get_address_of__assemblyTypeCache_2() { return &____assemblyTypeCache_2; }
	inline void set__assemblyTypeCache_2(Dictionary_2_tC371B9A7E8B4A801BC5A4A04E678C970C09DA4FB * value)
	{
		____assemblyTypeCache_2 = value;
		Il2CppCodeGenWriteBarrier((&____assemblyTypeCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVENTIONBINDINFO_T0C1BF3F6EB989B628E3D30925957E0CDC7629757_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T4F5E81FB770833279195B39E065B825D995CD14B_H
#define U3CU3EC__DISPLAYCLASS6_0_T4F5E81FB770833279195B39E065B825D995CD14B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionBindInfo/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t4F5E81FB770833279195B39E065B825D995CD14B  : public RuntimeObject
{
public:
	// System.Reflection.Assembly Zenject.ConventionBindInfo/<>c__DisplayClass6_0::assembly
	Assembly_t * ___assembly_0;

public:
	inline static int32_t get_offset_of_assembly_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t4F5E81FB770833279195B39E065B825D995CD14B, ___assembly_0)); }
	inline Assembly_t * get_assembly_0() const { return ___assembly_0; }
	inline Assembly_t ** get_address_of_assembly_0() { return &___assembly_0; }
	inline void set_assembly_0(Assembly_t * value)
	{
		___assembly_0 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T4F5E81FB770833279195B39E065B825D995CD14B_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_TFBF4C56720FBAD32FDAD7E677F3204385AF2C2D7_H
#define U3CU3EC__DISPLAYCLASS7_0_TFBF4C56720FBAD32FDAD7E677F3204385AF2C2D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionBindInfo/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_tFBF4C56720FBAD32FDAD7E677F3204385AF2C2D7  : public RuntimeObject
{
public:
	// System.Type Zenject.ConventionBindInfo/<>c__DisplayClass7_0::type
	Type_t * ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tFBF4C56720FBAD32FDAD7E677F3204385AF2C2D7, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_TFBF4C56720FBAD32FDAD7E677F3204385AF2C2D7_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_TB529CB5E5FAB62C0BBD452574775942975D1B0DF_H
#define U3CU3EC__DISPLAYCLASS13_0_TB529CB5E5FAB62C0BBD452574775942975D1B0DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_tB529CB5E5FAB62C0BBD452574775942975D1B0DF  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerable`1<System.String> Zenject.ConventionFilterTypesBinder/<>c__DisplayClass13_0::namespaces
	RuntimeObject* ___namespaces_0;

public:
	inline static int32_t get_offset_of_namespaces_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_tB529CB5E5FAB62C0BBD452574775942975D1B0DF, ___namespaces_0)); }
	inline RuntimeObject* get_namespaces_0() const { return ___namespaces_0; }
	inline RuntimeObject** get_address_of_namespaces_0() { return &___namespaces_0; }
	inline void set_namespaces_0(RuntimeObject* value)
	{
		___namespaces_0 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_TB529CB5E5FAB62C0BBD452574775942975D1B0DF_H
#ifndef U3CU3EC__DISPLAYCLASS13_1_T8142531E2756CF1D82F8407930BBB1450D76F9D2_H
#define U3CU3EC__DISPLAYCLASS13_1_T8142531E2756CF1D82F8407930BBB1450D76F9D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder/<>c__DisplayClass13_1
struct  U3CU3Ec__DisplayClass13_1_t8142531E2756CF1D82F8407930BBB1450D76F9D2  : public RuntimeObject
{
public:
	// System.Type Zenject.ConventionFilterTypesBinder/<>c__DisplayClass13_1::t
	Type_t * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_1_t8142531E2756CF1D82F8407930BBB1450D76F9D2, ___t_0)); }
	inline Type_t * get_t_0() const { return ___t_0; }
	inline Type_t ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Type_t * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_1_T8142531E2756CF1D82F8407930BBB1450D76F9D2_H
#ifndef U3CU3EC__DISPLAYCLASS14_0_T7EDCAE4EA7ADA19BB9D8BBFD043EEF6251D634E5_H
#define U3CU3EC__DISPLAYCLASS14_0_T7EDCAE4EA7ADA19BB9D8BBFD043EEF6251D634E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder/<>c__DisplayClass14_0
struct  U3CU3Ec__DisplayClass14_0_t7EDCAE4EA7ADA19BB9D8BBFD043EEF6251D634E5  : public RuntimeObject
{
public:
	// System.String Zenject.ConventionFilterTypesBinder/<>c__DisplayClass14_0::suffix
	String_t* ___suffix_0;

public:
	inline static int32_t get_offset_of_suffix_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_t7EDCAE4EA7ADA19BB9D8BBFD043EEF6251D634E5, ___suffix_0)); }
	inline String_t* get_suffix_0() const { return ___suffix_0; }
	inline String_t** get_address_of_suffix_0() { return &___suffix_0; }
	inline void set_suffix_0(String_t* value)
	{
		___suffix_0 = value;
		Il2CppCodeGenWriteBarrier((&___suffix_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS14_0_T7EDCAE4EA7ADA19BB9D8BBFD043EEF6251D634E5_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_TC6FC39E06ED184859C44E4F42FAB6F4DCE3AEA27_H
#define U3CU3EC__DISPLAYCLASS2_0_TC6FC39E06ED184859C44E4F42FAB6F4DCE3AEA27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_tC6FC39E06ED184859C44E4F42FAB6F4DCE3AEA27  : public RuntimeObject
{
public:
	// System.Type Zenject.ConventionFilterTypesBinder/<>c__DisplayClass2_0::parentType
	Type_t * ___parentType_0;

public:
	inline static int32_t get_offset_of_parentType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tC6FC39E06ED184859C44E4F42FAB6F4DCE3AEA27, ___parentType_0)); }
	inline Type_t * get_parentType_0() const { return ___parentType_0; }
	inline Type_t ** get_address_of_parentType_0() { return &___parentType_0; }
	inline void set_parentType_0(Type_t * value)
	{
		___parentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___parentType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_TC6FC39E06ED184859C44E4F42FAB6F4DCE3AEA27_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T8AA8574610421E942338B731692B32AD70CC30CD_H
#define U3CU3EC__DISPLAYCLASS4_0_T8AA8574610421E942338B731692B32AD70CC30CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t8AA8574610421E942338B731692B32AD70CC30CD  : public RuntimeObject
{
public:
	// System.Type Zenject.ConventionFilterTypesBinder/<>c__DisplayClass4_0::parentType
	Type_t * ___parentType_0;

public:
	inline static int32_t get_offset_of_parentType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t8AA8574610421E942338B731692B32AD70CC30CD, ___parentType_0)); }
	inline Type_t * get_parentType_0() const { return ___parentType_0; }
	inline Type_t ** get_address_of_parentType_0() { return &___parentType_0; }
	inline void set_parentType_0(Type_t * value)
	{
		___parentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___parentType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T8AA8574610421E942338B731692B32AD70CC30CD_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T6982D00595C2D49606252AA15E4BA5618557FB04_H
#define U3CU3EC__DISPLAYCLASS6_0_T6982D00595C2D49606252AA15E4BA5618557FB04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t6982D00595C2D49606252AA15E4BA5618557FB04  : public RuntimeObject
{
public:
	// System.Type Zenject.ConventionFilterTypesBinder/<>c__DisplayClass6_0::attribute
	Type_t * ___attribute_0;

public:
	inline static int32_t get_offset_of_attribute_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t6982D00595C2D49606252AA15E4BA5618557FB04, ___attribute_0)); }
	inline Type_t * get_attribute_0() const { return ___attribute_0; }
	inline Type_t ** get_address_of_attribute_0() { return &___attribute_0; }
	inline void set_attribute_0(Type_t * value)
	{
		___attribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___attribute_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T6982D00595C2D49606252AA15E4BA5618557FB04_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T776FF7B9D10A35EE0867E6932E21DF9DF8EBD8C5_H
#define U3CU3EC__DISPLAYCLASS8_0_T776FF7B9D10A35EE0867E6932E21DF9DF8EBD8C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t776FF7B9D10A35EE0867E6932E21DF9DF8EBD8C5  : public RuntimeObject
{
public:
	// System.Type Zenject.ConventionFilterTypesBinder/<>c__DisplayClass8_0::attribute
	Type_t * ___attribute_0;

public:
	inline static int32_t get_offset_of_attribute_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t776FF7B9D10A35EE0867E6932E21DF9DF8EBD8C5, ___attribute_0)); }
	inline Type_t * get_attribute_0() const { return ___attribute_0; }
	inline Type_t ** get_address_of_attribute_0() { return &___attribute_0; }
	inline void set_attribute_0(Type_t * value)
	{
		___attribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___attribute_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T776FF7B9D10A35EE0867E6932E21DF9DF8EBD8C5_H
#ifndef NONLAZYBINDER_TD353CE02C951C95FD045F6C21516882D489A0B8E_H
#define NONLAZYBINDER_TD353CE02C951C95FD045F6C21516882D489A0B8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NonLazyBinder
struct  NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.NonLazyBinder::<BindInfo>k__BackingField
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * ___U3CBindInfoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBindInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E, ___U3CBindInfoU3Ek__BackingField_0)); }
	inline BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * get_U3CBindInfoU3Ek__BackingField_0() const { return ___U3CBindInfoU3Ek__BackingField_0; }
	inline BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 ** get_address_of_U3CBindInfoU3Ek__BackingField_0() { return &___U3CBindInfoU3Ek__BackingField_0; }
	inline void set_U3CBindInfoU3Ek__BackingField_0(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * value)
	{
		___U3CBindInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindInfoU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONLAZYBINDER_TD353CE02C951C95FD045F6C21516882D489A0B8E_H
#ifndef __STATICARRAYINITTYPESIZEU3D11148_TA414153821EB70E6FCD456B1E2DB309D58CE1619_H
#define __STATICARRAYINITTYPESIZEU3D11148_TA414153821EB70E6FCD456B1E2DB309D58CE1619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=11148
struct  __StaticArrayInitTypeSizeU3D11148_tA414153821EB70E6FCD456B1E2DB309D58CE1619 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D11148_tA414153821EB70E6FCD456B1E2DB309D58CE1619__padding[11148];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D11148_TA414153821EB70E6FCD456B1E2DB309D58CE1619_H
#ifndef __STATICARRAYINITTYPESIZEU3D120_T3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12_H
#define __STATICARRAYINITTYPESIZEU3D120_T3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120
struct  __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12__padding[120];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D120_T3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12_H
#ifndef __STATICARRAYINITTYPESIZEU3D136_TF639EB419908A28312DAF13451C27B4ED03E8674_H
#define __STATICARRAYINITTYPESIZEU3D136_TF639EB419908A28312DAF13451C27B4ED03E8674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=136
struct  __StaticArrayInitTypeSizeU3D136_tF639EB419908A28312DAF13451C27B4ED03E8674 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D136_tF639EB419908A28312DAF13451C27B4ED03E8674__padding[136];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D136_TF639EB419908A28312DAF13451C27B4ED03E8674_H
#ifndef __STATICARRAYINITTYPESIZEU3D148_T0A73993E265ED7BA3349D48D2563680FA231189B_H
#define __STATICARRAYINITTYPESIZEU3D148_T0A73993E265ED7BA3349D48D2563680FA231189B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=148
struct  __StaticArrayInitTypeSizeU3D148_t0A73993E265ED7BA3349D48D2563680FA231189B 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D148_t0A73993E265ED7BA3349D48D2563680FA231189B__padding[148];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D148_T0A73993E265ED7BA3349D48D2563680FA231189B_H
#ifndef __STATICARRAYINITTYPESIZEU3D156_TD444739979C2F1A4BE28790BC71DD6FB876CCD25_H
#define __STATICARRAYINITTYPESIZEU3D156_TD444739979C2F1A4BE28790BC71DD6FB876CCD25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=156
struct  __StaticArrayInitTypeSizeU3D156_tD444739979C2F1A4BE28790BC71DD6FB876CCD25 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D156_tD444739979C2F1A4BE28790BC71DD6FB876CCD25__padding[156];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D156_TD444739979C2F1A4BE28790BC71DD6FB876CCD25_H
#ifndef __STATICARRAYINITTYPESIZEU3D176_T9A793B7CC486996BB07636533D64428F9FE21236_H
#define __STATICARRAYINITTYPESIZEU3D176_T9A793B7CC486996BB07636533D64428F9FE21236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=176
struct  __StaticArrayInitTypeSizeU3D176_t9A793B7CC486996BB07636533D64428F9FE21236 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D176_t9A793B7CC486996BB07636533D64428F9FE21236__padding[176];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D176_T9A793B7CC486996BB07636533D64428F9FE21236_H
#ifndef __STATICARRAYINITTYPESIZEU3D192_TCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36_H
#define __STATICARRAYINITTYPESIZEU3D192_TCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=192
struct  __StaticArrayInitTypeSizeU3D192_tCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D192_tCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36__padding[192];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D192_TCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36_H
#ifndef __STATICARRAYINITTYPESIZEU3D2574_T94A56C994C4EA1F17B7D51314A88B67247C9BC15_H
#define __STATICARRAYINITTYPESIZEU3D2574_T94A56C994C4EA1F17B7D51314A88B67247C9BC15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=2574
struct  __StaticArrayInitTypeSizeU3D2574_t94A56C994C4EA1F17B7D51314A88B67247C9BC15 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D2574_t94A56C994C4EA1F17B7D51314A88B67247C9BC15__padding[2574];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D2574_T94A56C994C4EA1F17B7D51314A88B67247C9BC15_H
#ifndef __STATICARRAYINITTYPESIZEU3D28_TFDDEB43D291084240980227C00328EBA1C48F445_H
#define __STATICARRAYINITTYPESIZEU3D28_TFDDEB43D291084240980227C00328EBA1C48F445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28
struct  __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445__padding[28];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D28_TFDDEB43D291084240980227C00328EBA1C48F445_H
#ifndef __STATICARRAYINITTYPESIZEU3D30_T14568882CF4903E97998A46DDDF1E20E37839016_H
#define __STATICARRAYINITTYPESIZEU3D30_T14568882CF4903E97998A46DDDF1E20E37839016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=30
struct  __StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016__padding[30];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D30_T14568882CF4903E97998A46DDDF1E20E37839016_H
#ifndef __STATICARRAYINITTYPESIZEU3D32_T33CBB6B98D456285860AB1149E6E0B652C81F14C_H
#define __STATICARRAYINITTYPESIZEU3D32_T33CBB6B98D456285860AB1149E6E0B652C81F14C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32
struct  __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C__padding[32];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D32_T33CBB6B98D456285860AB1149E6E0B652C81F14C_H
#ifndef __STATICARRAYINITTYPESIZEU3D36_T2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17_H
#define __STATICARRAYINITTYPESIZEU3D36_T2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36
struct  __StaticArrayInitTypeSizeU3D36_t2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D36_t2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17__padding[36];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D36_T2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17_H
#ifndef __STATICARRAYINITTYPESIZEU3D384_T6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3_H
#define __STATICARRAYINITTYPESIZEU3D384_T6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=384
struct  __StaticArrayInitTypeSizeU3D384_t6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D384_t6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3__padding[384];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D384_T6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3_H
#ifndef __STATICARRAYINITTYPESIZEU3D40_TB5E642B07376B8F47DB15AAA7C0561CB42F6591F_H
#define __STATICARRAYINITTYPESIZEU3D40_TB5E642B07376B8F47DB15AAA7C0561CB42F6591F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40
struct  __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F__padding[40];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D40_TB5E642B07376B8F47DB15AAA7C0561CB42F6591F_H
#ifndef __STATICARRAYINITTYPESIZEU3D44_TE280582A6EEC87F26198EDC0F6EC802BBCD3D015_H
#define __STATICARRAYINITTYPESIZEU3D44_TE280582A6EEC87F26198EDC0F6EC802BBCD3D015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44
struct  __StaticArrayInitTypeSizeU3D44_tE280582A6EEC87F26198EDC0F6EC802BBCD3D015 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D44_tE280582A6EEC87F26198EDC0F6EC802BBCD3D015__padding[44];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D44_TE280582A6EEC87F26198EDC0F6EC802BBCD3D015_H
#ifndef __STATICARRAYINITTYPESIZEU3D52_T94FED58B7B80A7E497773366DB2DD42193888C54_H
#define __STATICARRAYINITTYPESIZEU3D52_T94FED58B7B80A7E497773366DB2DD42193888C54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52
struct  __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54__padding[52];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D52_T94FED58B7B80A7E497773366DB2DD42193888C54_H
#ifndef __STATICARRAYINITTYPESIZEU3D54_T1ACEB6759550166F62217D005B9DC6C35D54C265_H
#define __STATICARRAYINITTYPESIZEU3D54_T1ACEB6759550166F62217D005B9DC6C35D54C265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=54
struct  __StaticArrayInitTypeSizeU3D54_t1ACEB6759550166F62217D005B9DC6C35D54C265 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D54_t1ACEB6759550166F62217D005B9DC6C35D54C265__padding[54];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D54_T1ACEB6759550166F62217D005B9DC6C35D54C265_H
#ifndef __STATICARRAYINITTYPESIZEU3D64_T2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF_H
#define __STATICARRAYINITTYPESIZEU3D64_T2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64
struct  __StaticArrayInitTypeSizeU3D64_t2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D64_t2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF__padding[64];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D64_T2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF_H
#ifndef __STATICARRAYINITTYPESIZEU3D80_T2C4CCE0AD65A37C6C949742DB0FF117776478260_H
#define __STATICARRAYINITTYPESIZEU3D80_T2C4CCE0AD65A37C6C949742DB0FF117776478260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=80
struct  __StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260__padding[80];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D80_T2C4CCE0AD65A37C6C949742DB0FF117776478260_H
#ifndef PRESERVEATTRIBUTE_TE222384D2C1A3A56A61976D23DC21C7DD6B39A57_H
#define PRESERVEATTRIBUTE_TE222384D2C1A3A56A61976D23DC21C7DD6B39A57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.PreserveAttribute
struct  PreserveAttribute_tE222384D2C1A3A56A61976D23DC21C7DD6B39A57  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEATTRIBUTE_TE222384D2C1A3A56A61976D23DC21C7DD6B39A57_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef SCENE_T942E023788C2BC9FBB7EC8356B4FB0088B2CFED2_H
#define SCENE_T942E023788C2BC9FBB7EC8356B4FB0088B2CFED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T942E023788C2BC9FBB7EC8356B4FB0088B2CFED2_H
#ifndef ARGNONLAZYBINDER_T81E8AB6F5213E5C3CC7CA6087C9A98CD843DC88F_H
#define ARGNONLAZYBINDER_T81E8AB6F5213E5C3CC7CA6087C9A98CD843DC88F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ArgNonLazyBinder
struct  ArgNonLazyBinder_t81E8AB6F5213E5C3CC7CA6087C9A98CD843DC88F  : public NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGNONLAZYBINDER_T81E8AB6F5213E5C3CC7CA6087C9A98CD843DC88F_H
#ifndef CONVENTIONFILTERTYPESBINDER_TC16F7C396F8CFC441EDDB3885D43092E2EA733CB_H
#define CONVENTIONFILTERTYPESBINDER_TC16F7C396F8CFC441EDDB3885D43092E2EA733CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder
struct  ConventionFilterTypesBinder_tC16F7C396F8CFC441EDDB3885D43092E2EA733CB  : public ConventionAssemblySelectionBinder_t121902B526295C714455135BDE7F872C01973C4E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVENTIONFILTERTYPESBINDER_TC16F7C396F8CFC441EDDB3885D43092E2EA733CB_H
#ifndef COPYNONLAZYBINDER_T4C212B7E1BE267539D091F759AA6A0A9A4029A67_H
#define COPYNONLAZYBINDER_T4C212B7E1BE267539D091F759AA6A0A9A4029A67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.CopyNonLazyBinder
struct  CopyNonLazyBinder_t4C212B7E1BE267539D091F759AA6A0A9A4029A67  : public NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYNONLAZYBINDER_T4C212B7E1BE267539D091F759AA6A0A9A4029A67_H
#ifndef U3CGET_ALLSCENESU3ED__1_T02FA8B51C6F5F12210058F6716CE74FE66012521_H
#define U3CGET_ALLSCENESU3ED__1_T02FA8B51C6F5F12210058F6716CE74FE66012521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.UnityUtil/<get_AllScenes>d__1
struct  U3Cget_AllScenesU3Ed__1_t02FA8B51C6F5F12210058F6716CE74FE66012521  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.Util.UnityUtil/<get_AllScenes>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.SceneManagement.Scene ModestTree.Util.UnityUtil/<get_AllScenes>d__1::<>2__current
	Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  ___U3CU3E2__current_1;
	// System.Int32 ModestTree.Util.UnityUtil/<get_AllScenes>d__1::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Int32 ModestTree.Util.UnityUtil/<get_AllScenes>d__1::<i>5__2
	int32_t ___U3CiU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3Cget_AllScenesU3Ed__1_t02FA8B51C6F5F12210058F6716CE74FE66012521, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3Cget_AllScenesU3Ed__1_t02FA8B51C6F5F12210058F6716CE74FE66012521, ___U3CU3E2__current_1)); }
	inline Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3Cget_AllScenesU3Ed__1_t02FA8B51C6F5F12210058F6716CE74FE66012521, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_3() { return static_cast<int32_t>(offsetof(U3Cget_AllScenesU3Ed__1_t02FA8B51C6F5F12210058F6716CE74FE66012521, ___U3CiU3E5__2_3)); }
	inline int32_t get_U3CiU3E5__2_3() const { return ___U3CiU3E5__2_3; }
	inline int32_t* get_address_of_U3CiU3E5__2_3() { return &___U3CiU3E5__2_3; }
	inline void set_U3CiU3E5__2_3(int32_t value)
	{
		___U3CiU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGET_ALLSCENESU3ED__1_T02FA8B51C6F5F12210058F6716CE74FE66012521_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef CONDITIONCOPYNONLAZYBINDER_TA82CD5BABE0526375567A1A0064DAE8035AA37C7_H
#define CONDITIONCOPYNONLAZYBINDER_TA82CD5BABE0526375567A1A0064DAE8035AA37C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConditionCopyNonLazyBinder
struct  ConditionCopyNonLazyBinder_tA82CD5BABE0526375567A1A0064DAE8035AA37C7  : public CopyNonLazyBinder_t4C212B7E1BE267539D091F759AA6A0A9A4029A67
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONCOPYNONLAZYBINDER_TA82CD5BABE0526375567A1A0064DAE8035AA37C7_H
#ifndef U3CGETFIELDSANDPROPERTIESU3ED__6_TADF047DC0A867E49CB4D2CB50ABBCC75F3F82496_H
#define U3CGETFIELDSANDPROPERTIESU3ED__6_TADF047DC0A867E49CB4D2CB50ABBCC75F3F82496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.ReflectionUtil/<GetFieldsAndProperties>d__6
struct  U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.Util.ReflectionUtil/<GetFieldsAndProperties>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// ModestTree.Util.ReflectionUtil/IMemberInfo ModestTree.Util.ReflectionUtil/<GetFieldsAndProperties>d__6::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// System.Int32 ModestTree.Util.ReflectionUtil/<GetFieldsAndProperties>d__6::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type ModestTree.Util.ReflectionUtil/<GetFieldsAndProperties>d__6::type
	Type_t * ___type_3;
	// System.Type ModestTree.Util.ReflectionUtil/<GetFieldsAndProperties>d__6::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Reflection.BindingFlags ModestTree.Util.ReflectionUtil/<GetFieldsAndProperties>d__6::flags
	int32_t ___flags_5;
	// System.Reflection.BindingFlags ModestTree.Util.ReflectionUtil/<GetFieldsAndProperties>d__6::<>3__flags
	int32_t ___U3CU3E3__flags_6;
	// System.Reflection.PropertyInfo[] ModestTree.Util.ReflectionUtil/<GetFieldsAndProperties>d__6::<>7__wrap1
	PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E* ___U3CU3E7__wrap1_7;
	// System.Int32 ModestTree.Util.ReflectionUtil/<GetFieldsAndProperties>d__6::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_8;
	// System.Reflection.FieldInfo[] ModestTree.Util.ReflectionUtil/<GetFieldsAndProperties>d__6::<>7__wrap3
	FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* ___U3CU3E7__wrap3_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496, ___U3CU3E2__current_1)); }
	inline RuntimeObject* get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject* value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496, ___flags_5)); }
	inline int32_t get_flags_5() const { return ___flags_5; }
	inline int32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(int32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__flags_6() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496, ___U3CU3E3__flags_6)); }
	inline int32_t get_U3CU3E3__flags_6() const { return ___U3CU3E3__flags_6; }
	inline int32_t* get_address_of_U3CU3E3__flags_6() { return &___U3CU3E3__flags_6; }
	inline void set_U3CU3E3__flags_6(int32_t value)
	{
		___U3CU3E3__flags_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_7() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496, ___U3CU3E7__wrap1_7)); }
	inline PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E* get_U3CU3E7__wrap1_7() const { return ___U3CU3E7__wrap1_7; }
	inline PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E** get_address_of_U3CU3E7__wrap1_7() { return &___U3CU3E7__wrap1_7; }
	inline void set_U3CU3E7__wrap1_7(PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E* value)
	{
		___U3CU3E7__wrap1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_8() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496, ___U3CU3E7__wrap2_8)); }
	inline int32_t get_U3CU3E7__wrap2_8() const { return ___U3CU3E7__wrap2_8; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_8() { return &___U3CU3E7__wrap2_8; }
	inline void set_U3CU3E7__wrap2_8(int32_t value)
	{
		___U3CU3E7__wrap2_8 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap3_9() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496, ___U3CU3E7__wrap3_9)); }
	inline FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* get_U3CU3E7__wrap3_9() const { return ___U3CU3E7__wrap3_9; }
	inline FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE** get_address_of_U3CU3E7__wrap3_9() { return &___U3CU3E7__wrap3_9; }
	inline void set_U3CU3E7__wrap3_9(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* value)
	{
		___U3CU3E7__wrap3_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap3_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETFIELDSANDPROPERTIESU3ED__6_TADF047DC0A867E49CB4D2CB50ABBCC75F3F82496_H
#ifndef ARGCONDITIONCOPYNONLAZYBINDER_TB4B41047F4CCC5148A492F92ABD4A9DAAC588327_H
#define ARGCONDITIONCOPYNONLAZYBINDER_TB4B41047F4CCC5148A492F92ABD4A9DAAC588327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ArgConditionCopyNonLazyBinder
struct  ArgConditionCopyNonLazyBinder_tB4B41047F4CCC5148A492F92ABD4A9DAAC588327  : public ConditionCopyNonLazyBinder_tA82CD5BABE0526375567A1A0064DAE8035AA37C7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGCONDITIONCOPYNONLAZYBINDER_TB4B41047F4CCC5148A492F92ABD4A9DAAC588327_H
#ifndef SCOPEARGCONDITIONCOPYNONLAZYBINDER_TF12679CE42CB0E48D76D73799596B64C1A66F73F_H
#define SCOPEARGCONDITIONCOPYNONLAZYBINDER_TF12679CE42CB0E48D76D73799596B64C1A66F73F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeArgConditionCopyNonLazyBinder
struct  ScopeArgConditionCopyNonLazyBinder_tF12679CE42CB0E48D76D73799596B64C1A66F73F  : public ArgConditionCopyNonLazyBinder_tB4B41047F4CCC5148A492F92ABD4A9DAAC588327
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPEARGCONDITIONCOPYNONLAZYBINDER_TF12679CE42CB0E48D76D73799596B64C1A66F73F_H
#ifndef FROMBINDER_T888ACFF1EB1CD668F1547CE1943F1AAF2B135493_H
#define FROMBINDER_T888ACFF1EB1CD668F1547CE1943F1AAF2B135493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder
struct  FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493  : public ScopeArgConditionCopyNonLazyBinder_tF12679CE42CB0E48D76D73799596B64C1A66F73F
{
public:
	// Zenject.BindFinalizerWrapper Zenject.FromBinder::<FinalizerWrapper>k__BackingField
	BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 * ___U3CFinalizerWrapperU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFinalizerWrapperU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493, ___U3CFinalizerWrapperU3Ek__BackingField_1)); }
	inline BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 * get_U3CFinalizerWrapperU3Ek__BackingField_1() const { return ___U3CFinalizerWrapperU3Ek__BackingField_1; }
	inline BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 ** get_address_of_U3CFinalizerWrapperU3Ek__BackingField_1() { return &___U3CFinalizerWrapperU3Ek__BackingField_1; }
	inline void set_U3CFinalizerWrapperU3Ek__BackingField_1(BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 * value)
	{
		___U3CFinalizerWrapperU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFinalizerWrapperU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMBINDER_T888ACFF1EB1CD668F1547CE1943F1AAF2B135493_H
#ifndef FROMBINDERNONGENERIC_T3509B89300095EFE71CAC8AC15B8D2A4BDC85AF1_H
#define FROMBINDERNONGENERIC_T3509B89300095EFE71CAC8AC15B8D2A4BDC85AF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinderNonGeneric
struct  FromBinderNonGeneric_t3509B89300095EFE71CAC8AC15B8D2A4BDC85AF1  : public FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMBINDERNONGENERIC_T3509B89300095EFE71CAC8AC15B8D2A4BDC85AF1_H
#ifndef CONCRETEBINDERNONGENERIC_TF75241C0315ED9C837ECED6EFCBE0A735D40B29C_H
#define CONCRETEBINDERNONGENERIC_TF75241C0315ED9C837ECED6EFCBE0A735D40B29C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConcreteBinderNonGeneric
struct  ConcreteBinderNonGeneric_tF75241C0315ED9C837ECED6EFCBE0A735D40B29C  : public FromBinderNonGeneric_t3509B89300095EFE71CAC8AC15B8D2A4BDC85AF1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONCRETEBINDERNONGENERIC_TF75241C0315ED9C837ECED6EFCBE0A735D40B29C_H
#ifndef CONCRETEIDBINDERNONGENERIC_TCA5AC4DA62704A97BFE33E7788D05224886223F6_H
#define CONCRETEIDBINDERNONGENERIC_TCA5AC4DA62704A97BFE33E7788D05224886223F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConcreteIdBinderNonGeneric
struct  ConcreteIdBinderNonGeneric_tCA5AC4DA62704A97BFE33E7788D05224886223F6  : public ConcreteBinderNonGeneric_tF75241C0315ED9C837ECED6EFCBE0A735D40B29C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONCRETEIDBINDERNONGENERIC_TCA5AC4DA62704A97BFE33E7788D05224886223F6_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6600 = { sizeof (__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6601 = { sizeof (__StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6602 = { sizeof (__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6603 = { sizeof (__StaticArrayInitTypeSizeU3D36_t2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D36_t2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6604 = { sizeof (__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6605 = { sizeof (__StaticArrayInitTypeSizeU3D44_tE280582A6EEC87F26198EDC0F6EC802BBCD3D015)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D44_tE280582A6EEC87F26198EDC0F6EC802BBCD3D015 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6606 = { sizeof (__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6607 = { sizeof (__StaticArrayInitTypeSizeU3D54_t1ACEB6759550166F62217D005B9DC6C35D54C265)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D54_t1ACEB6759550166F62217D005B9DC6C35D54C265 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6608 = { sizeof (__StaticArrayInitTypeSizeU3D64_t2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D64_t2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6609 = { sizeof (__StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6610 = { sizeof (__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6611 = { sizeof (__StaticArrayInitTypeSizeU3D136_tF639EB419908A28312DAF13451C27B4ED03E8674)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D136_tF639EB419908A28312DAF13451C27B4ED03E8674 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6612 = { sizeof (__StaticArrayInitTypeSizeU3D148_t0A73993E265ED7BA3349D48D2563680FA231189B)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D148_t0A73993E265ED7BA3349D48D2563680FA231189B ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6613 = { sizeof (__StaticArrayInitTypeSizeU3D156_tD444739979C2F1A4BE28790BC71DD6FB876CCD25)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D156_tD444739979C2F1A4BE28790BC71DD6FB876CCD25 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6614 = { sizeof (__StaticArrayInitTypeSizeU3D176_t9A793B7CC486996BB07636533D64428F9FE21236)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D176_t9A793B7CC486996BB07636533D64428F9FE21236 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6615 = { sizeof (__StaticArrayInitTypeSizeU3D192_tCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D192_tCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6616 = { sizeof (__StaticArrayInitTypeSizeU3D384_t6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D384_t6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6617 = { sizeof (__StaticArrayInitTypeSizeU3D2574_t94A56C994C4EA1F17B7D51314A88B67247C9BC15)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D2574_t94A56C994C4EA1F17B7D51314A88B67247C9BC15 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6618 = { sizeof (__StaticArrayInitTypeSizeU3D11148_tA414153821EB70E6FCD456B1E2DB309D58CE1619)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D11148_tA414153821EB70E6FCD456B1E2DB309D58CE1619 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6619 = { sizeof (U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6620 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6620[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6621 = { sizeof (Assert_t1D409464297E17E6BAFCDF305224F40F9AE91C4E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6622 = { sizeof (LinqExtensions_t18ED2D71F925F67E80E13CC7A35D1A07A1EA6756), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6623 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6623[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6624 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6624[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6625 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6625[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6626 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6626[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6627 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6627[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6628 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6628[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6629 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6629[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6630 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6630[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6631 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6631[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6632 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6632[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6633 = { sizeof (Log_tBEB2A9D01160FC4BDAEF167CA277F88E845B4122), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6634 = { sizeof (MiscExtensions_tA08EEED147F29A323A776DB66DCEBF7E8DBFE90F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6635 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6635[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6636 = { sizeof (TypeExtensions_t28E8496E615D0C548B6325A5FFC0A1D0C273A618), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6637 = { sizeof (U3CGetParentTypesU3Ed__22_tD3493BD30A0DF8981E61EEF94164ECBD09F73536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6637[6] = 
{
	U3CGetParentTypesU3Ed__22_tD3493BD30A0DF8981E61EEF94164ECBD09F73536::get_offset_of_U3CU3E1__state_0(),
	U3CGetParentTypesU3Ed__22_tD3493BD30A0DF8981E61EEF94164ECBD09F73536::get_offset_of_U3CU3E2__current_1(),
	U3CGetParentTypesU3Ed__22_tD3493BD30A0DF8981E61EEF94164ECBD09F73536::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetParentTypesU3Ed__22_tD3493BD30A0DF8981E61EEF94164ECBD09F73536::get_offset_of_type_3(),
	U3CGetParentTypesU3Ed__22_tD3493BD30A0DF8981E61EEF94164ECBD09F73536::get_offset_of_U3CU3E3__type_4(),
	U3CGetParentTypesU3Ed__22_tD3493BD30A0DF8981E61EEF94164ECBD09F73536::get_offset_of_U3CU3E7__wrap1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6638 = { sizeof (U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6638[8] = 
{
	U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D::get_offset_of_type_3(),
	U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D::get_offset_of_U3CU3E3__type_4(),
	U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D::get_offset_of_U3CU3E7__wrap1_5(),
	U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D::get_offset_of_U3CU3E7__wrap2_6(),
	U3CGetAllInstanceFieldsU3Ed__25_t0EC5EC457FB44071D166A14BA4FC5F6BE341372D::get_offset_of_U3CU3E7__wrap3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6639 = { sizeof (U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6639[8] = 
{
	U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758::get_offset_of_type_3(),
	U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758::get_offset_of_U3CU3E3__type_4(),
	U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758::get_offset_of_U3CU3E7__wrap1_5(),
	U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758::get_offset_of_U3CU3E7__wrap2_6(),
	U3CGetAllInstancePropertiesU3Ed__26_t6A44E91AAC0A12872A97708C9E4CEEE90F974758::get_offset_of_U3CU3E7__wrap3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6640 = { sizeof (U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6640[8] = 
{
	U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6::get_offset_of_type_3(),
	U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6::get_offset_of_U3CU3E3__type_4(),
	U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6::get_offset_of_U3CU3E7__wrap1_5(),
	U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6::get_offset_of_U3CU3E7__wrap2_6(),
	U3CGetAllInstanceMethodsU3Ed__27_tB302A2C6EAF16FF2C7D8493BDFC0EC7C3B7C24D6::get_offset_of_U3CU3E7__wrap3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6641 = { sizeof (U3CU3Ec__DisplayClass35_0_tD105F0859917A937A010EB05978E691B4085EF27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6641[1] = 
{
	U3CU3Ec__DisplayClass35_0_tD105F0859917A937A010EB05978E691B4085EF27::get_offset_of_attributeTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6642 = { sizeof (U3CU3Ec__DisplayClass35_1_tB7BD6D99E82DA965FE024902ED2CD5574D4C6B3E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6642[1] = 
{
	U3CU3Ec__DisplayClass35_1_tB7BD6D99E82DA965FE024902ED2CD5574D4C6B3E::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6643 = { sizeof (U3CU3Ec__DisplayClass39_0_tCEA349F5BEE44478656EA9E1DA5308913F1ADF0B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6643[1] = 
{
	U3CU3Ec__DisplayClass39_0_tCEA349F5BEE44478656EA9E1DA5308913F1ADF0B::get_offset_of_attributeTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6644 = { sizeof (U3CU3Ec__DisplayClass39_1_t2A7417B67AA1DAFB430C62DD67BF51F12914E338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6644[1] = 
{
	U3CU3Ec__DisplayClass39_1_t2A7417B67AA1DAFB430C62DD67BF51F12914E338::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6645 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6646 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6647 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6648 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6649 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6650 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6651 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6652 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6653 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6654 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6655 = { sizeof (PreserveAttribute_tE222384D2C1A3A56A61976D23DC21C7DD6B39A57), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6656 = { sizeof (ReflectionUtil_tF6D9CF8AB3379B68701E8A6CC722245F0D3F7398), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6657 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6658 = { sizeof (PropertyMemberInfo_tA3305BB1D65076A61B05007962E78A8233A14690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6658[1] = 
{
	PropertyMemberInfo_tA3305BB1D65076A61B05007962E78A8233A14690::get_offset_of__propInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6659 = { sizeof (FieldMemberInfo_tA6FDF3FCEC37981017599C2FA4D39B914896F742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6659[1] = 
{
	FieldMemberInfo_tA6FDF3FCEC37981017599C2FA4D39B914896F742::get_offset_of__fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6660 = { sizeof (U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6660[10] = 
{
	U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496::get_offset_of_U3CU3E1__state_0(),
	U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496::get_offset_of_U3CU3E2__current_1(),
	U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496::get_offset_of_type_3(),
	U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496::get_offset_of_U3CU3E3__type_4(),
	U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496::get_offset_of_flags_5(),
	U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496::get_offset_of_U3CU3E3__flags_6(),
	U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496::get_offset_of_U3CU3E7__wrap1_7(),
	U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496::get_offset_of_U3CU3E7__wrap2_8(),
	U3CGetFieldsAndPropertiesU3Ed__6_tADF047DC0A867E49CB4D2CB50ABBCC75F3F82496::get_offset_of_U3CU3E7__wrap3_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6661 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6661[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6662 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6662[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6663 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6663[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6664 = { sizeof (ValuePair_t8A8BF8371ACB1577D803259E8806F08789DA9790), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6665 = { sizeof (UnityUtil_t672DBCFAD039DEA1AC89AAAEF7C782A70F3CFADB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6666 = { sizeof (U3Cget_AllScenesU3Ed__1_t02FA8B51C6F5F12210058F6716CE74FE66012521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6666[4] = 
{
	U3Cget_AllScenesU3Ed__1_t02FA8B51C6F5F12210058F6716CE74FE66012521::get_offset_of_U3CU3E1__state_0(),
	U3Cget_AllScenesU3Ed__1_t02FA8B51C6F5F12210058F6716CE74FE66012521::get_offset_of_U3CU3E2__current_1(),
	U3Cget_AllScenesU3Ed__1_t02FA8B51C6F5F12210058F6716CE74FE66012521::get_offset_of_U3CU3El__initialThreadId_2(),
	U3Cget_AllScenesU3Ed__1_t02FA8B51C6F5F12210058F6716CE74FE66012521::get_offset_of_U3CiU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6667 = { sizeof (U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4), -1, sizeof(U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6667[7] = 
{
	U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
	U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields::get_offset_of_U3CU3E9__15_0_2(),
	U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields::get_offset_of_U3CU3E9__18_0_3(),
	U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields::get_offset_of_U3CU3E9__19_0_4(),
	U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields::get_offset_of_U3CU3E9__22_0_5(),
	U3CU3Ec_t9BA2E7AF26A5E490BC0FF0325E38C90B68BE1CE4_StaticFields::get_offset_of_U3CU3E9__23_0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6668 = { sizeof (U3CGetParentsU3Ed__16_tCE22A4DD1807053D4B004E17C1D8EF2ED7231C81), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6668[6] = 
{
	U3CGetParentsU3Ed__16_tCE22A4DD1807053D4B004E17C1D8EF2ED7231C81::get_offset_of_U3CU3E1__state_0(),
	U3CGetParentsU3Ed__16_tCE22A4DD1807053D4B004E17C1D8EF2ED7231C81::get_offset_of_U3CU3E2__current_1(),
	U3CGetParentsU3Ed__16_tCE22A4DD1807053D4B004E17C1D8EF2ED7231C81::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetParentsU3Ed__16_tCE22A4DD1807053D4B004E17C1D8EF2ED7231C81::get_offset_of_transform_3(),
	U3CGetParentsU3Ed__16_tCE22A4DD1807053D4B004E17C1D8EF2ED7231C81::get_offset_of_U3CU3E3__transform_4(),
	U3CGetParentsU3Ed__16_tCE22A4DD1807053D4B004E17C1D8EF2ED7231C81::get_offset_of_U3CU3E7__wrap1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6669 = { sizeof (U3CGetParentsAndSelfU3Ed__17_t8E038DAE36078185EEC75BB09A12F00C4393222F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6669[6] = 
{
	U3CGetParentsAndSelfU3Ed__17_t8E038DAE36078185EEC75BB09A12F00C4393222F::get_offset_of_U3CU3E1__state_0(),
	U3CGetParentsAndSelfU3Ed__17_t8E038DAE36078185EEC75BB09A12F00C4393222F::get_offset_of_U3CU3E2__current_1(),
	U3CGetParentsAndSelfU3Ed__17_t8E038DAE36078185EEC75BB09A12F00C4393222F::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetParentsAndSelfU3Ed__17_t8E038DAE36078185EEC75BB09A12F00C4393222F::get_offset_of_transform_3(),
	U3CGetParentsAndSelfU3Ed__17_t8E038DAE36078185EEC75BB09A12F00C4393222F::get_offset_of_U3CU3E3__transform_4(),
	U3CGetParentsAndSelfU3Ed__17_t8E038DAE36078185EEC75BB09A12F00C4393222F::get_offset_of_U3CU3E7__wrap1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6670 = { sizeof (U3CGetDirectChildrenAndSelfU3Ed__20_t5B7D4937C4166565E1DF30D8E977D348E933249E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6670[6] = 
{
	U3CGetDirectChildrenAndSelfU3Ed__20_t5B7D4937C4166565E1DF30D8E977D348E933249E::get_offset_of_U3CU3E1__state_0(),
	U3CGetDirectChildrenAndSelfU3Ed__20_t5B7D4937C4166565E1DF30D8E977D348E933249E::get_offset_of_U3CU3E2__current_1(),
	U3CGetDirectChildrenAndSelfU3Ed__20_t5B7D4937C4166565E1DF30D8E977D348E933249E::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetDirectChildrenAndSelfU3Ed__20_t5B7D4937C4166565E1DF30D8E977D348E933249E::get_offset_of_obj_3(),
	U3CGetDirectChildrenAndSelfU3Ed__20_t5B7D4937C4166565E1DF30D8E977D348E933249E::get_offset_of_U3CU3E3__obj_4(),
	U3CGetDirectChildrenAndSelfU3Ed__20_t5B7D4937C4166565E1DF30D8E977D348E933249E::get_offset_of_U3CU3E7__wrap1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6671 = { sizeof (U3CGetDirectChildrenU3Ed__21_tC03F8478B7343DE6415986513B9F0000AAB056F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6671[6] = 
{
	U3CGetDirectChildrenU3Ed__21_tC03F8478B7343DE6415986513B9F0000AAB056F1::get_offset_of_U3CU3E1__state_0(),
	U3CGetDirectChildrenU3Ed__21_tC03F8478B7343DE6415986513B9F0000AAB056F1::get_offset_of_U3CU3E2__current_1(),
	U3CGetDirectChildrenU3Ed__21_tC03F8478B7343DE6415986513B9F0000AAB056F1::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetDirectChildrenU3Ed__21_tC03F8478B7343DE6415986513B9F0000AAB056F1::get_offset_of_obj_3(),
	U3CGetDirectChildrenU3Ed__21_tC03F8478B7343DE6415986513B9F0000AAB056F1::get_offset_of_U3CU3E3__obj_4(),
	U3CGetDirectChildrenU3Ed__21_tC03F8478B7343DE6415986513B9F0000AAB056F1::get_offset_of_U3CU3E7__wrap1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6672 = { sizeof (ArgConditionCopyNonLazyBinder_tB4B41047F4CCC5148A492F92ABD4A9DAAC588327), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6673 = { sizeof (ArgNonLazyBinder_t81E8AB6F5213E5C3CC7CA6087C9A98CD843DC88F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6674 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6675 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6675[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6676 = { sizeof (ConcreteBinderNonGeneric_tF75241C0315ED9C837ECED6EFCBE0A735D40B29C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6677 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6678 = { sizeof (ConcreteIdBinderNonGeneric_tCA5AC4DA62704A97BFE33E7788D05224886223F6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6679 = { sizeof (ConditionCopyNonLazyBinder_tA82CD5BABE0526375567A1A0064DAE8035AA37C7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6680 = { sizeof (U3CU3Ec__DisplayClass2_0_tDD8AA92B02159AE254DB694D2355FACAC8ECE8F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6680[1] = 
{
	U3CU3Ec__DisplayClass2_0_tDD8AA92B02159AE254DB694D2355FACAC8ECE8F8::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6681 = { sizeof (U3CU3Ec__DisplayClass3_0_t3D5A232390CE7E36DE46E2363E2E54C61139847E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6681[1] = 
{
	U3CU3Ec__DisplayClass3_0_t3D5A232390CE7E36DE46E2363E2E54C61139847E::get_offset_of_targets_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6682 = { sizeof (U3CU3Ec__DisplayClass3_1_t8C5F61FF5A726750B091149FA1677E71AABD791A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6682[1] = 
{
	U3CU3Ec__DisplayClass3_1_t8C5F61FF5A726750B091149FA1677E71AABD791A::get_offset_of_r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6683 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6683[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6684 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6684[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6685 = { sizeof (ConventionAssemblySelectionBinder_t121902B526295C714455135BDE7F872C01973C4E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6685[1] = 
{
	ConventionAssemblySelectionBinder_t121902B526295C714455135BDE7F872C01973C4E::get_offset_of_U3CBindInfoU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6686 = { sizeof (U3CU3Ec_t2E35C3D6CC4AB1E6E4B44A4F27D5C180BF9B4D60), -1, sizeof(U3CU3Ec_t2E35C3D6CC4AB1E6E4B44A4F27D5C180BF9B4D60_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6686[2] = 
{
	U3CU3Ec_t2E35C3D6CC4AB1E6E4B44A4F27D5C180BF9B4D60_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2E35C3D6CC4AB1E6E4B44A4F27D5C180BF9B4D60_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6687 = { sizeof (U3CU3Ec__DisplayClass12_0_tE0D505E95E7C51EE130DEFCAB49C3064031474C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6687[1] = 
{
	U3CU3Ec__DisplayClass12_0_tE0D505E95E7C51EE130DEFCAB49C3064031474C8::get_offset_of_assemblies_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6688 = { sizeof (ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757), -1, sizeof(ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6688[3] = 
{
	ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757::get_offset_of__typeFilters_0(),
	ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757::get_offset_of__assemblyFilters_1(),
	ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757_StaticFields::get_offset_of__assemblyTypeCache_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6689 = { sizeof (U3CU3Ec__DisplayClass6_0_t4F5E81FB770833279195B39E065B825D995CD14B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6689[1] = 
{
	U3CU3Ec__DisplayClass6_0_t4F5E81FB770833279195B39E065B825D995CD14B::get_offset_of_assembly_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6690 = { sizeof (U3CU3Ec__DisplayClass7_0_tFBF4C56720FBAD32FDAD7E677F3204385AF2C2D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6690[1] = 
{
	U3CU3Ec__DisplayClass7_0_tFBF4C56720FBAD32FDAD7E677F3204385AF2C2D7::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6691 = { sizeof (ConventionFilterTypesBinder_tC16F7C396F8CFC441EDDB3885D43092E2EA733CB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6692 = { sizeof (U3CU3Ec__DisplayClass2_0_tC6FC39E06ED184859C44E4F42FAB6F4DCE3AEA27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6692[1] = 
{
	U3CU3Ec__DisplayClass2_0_tC6FC39E06ED184859C44E4F42FAB6F4DCE3AEA27::get_offset_of_parentType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6693 = { sizeof (U3CU3Ec__DisplayClass4_0_t8AA8574610421E942338B731692B32AD70CC30CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6693[1] = 
{
	U3CU3Ec__DisplayClass4_0_t8AA8574610421E942338B731692B32AD70CC30CD::get_offset_of_parentType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6694 = { sizeof (U3CU3Ec__DisplayClass6_0_t6982D00595C2D49606252AA15E4BA5618557FB04), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6694[1] = 
{
	U3CU3Ec__DisplayClass6_0_t6982D00595C2D49606252AA15E4BA5618557FB04::get_offset_of_attribute_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6695 = { sizeof (U3CU3Ec__DisplayClass8_0_t776FF7B9D10A35EE0867E6932E21DF9DF8EBD8C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6695[1] = 
{
	U3CU3Ec__DisplayClass8_0_t776FF7B9D10A35EE0867E6932E21DF9DF8EBD8C5::get_offset_of_attribute_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6696 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6696[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6697 = { sizeof (U3CU3Ec__DisplayClass13_0_tB529CB5E5FAB62C0BBD452574775942975D1B0DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6697[1] = 
{
	U3CU3Ec__DisplayClass13_0_tB529CB5E5FAB62C0BBD452574775942975D1B0DF::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6698 = { sizeof (U3CU3Ec__DisplayClass13_1_t8142531E2756CF1D82F8407930BBB1450D76F9D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6698[1] = 
{
	U3CU3Ec__DisplayClass13_1_t8142531E2756CF1D82F8407930BBB1450D76F9D2::get_offset_of_t_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6699 = { sizeof (U3CU3Ec__DisplayClass14_0_t7EDCAE4EA7ADA19BB9D8BBFD043EEF6251D634E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6699[1] = 
{
	U3CU3Ec__DisplayClass14_0_t7EDCAE4EA7ADA19BB9D8BBFD043EEF6251D634E5::get_offset_of_suffix_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
