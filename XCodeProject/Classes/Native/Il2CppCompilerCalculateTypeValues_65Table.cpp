﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action`2<System.Object,System.EventArgs>
struct Action_2_tC27C27AF2F38BC3A52153621D61F10F9421E87D2;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Char[][]
struct CharU5BU5DU5BU5D_t509D6DD2B5D36FCA00421F0FDECD58289E41A574;
// System.Collections.Generic.IDictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>
struct IDictionary_2_tD21C9C91A43823B8C58F92358FBFBD5EF673746D;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>
struct IDictionary_2_t4BA061A629B701D5E5FAF7870E20AF0C68331CD5;
// System.Collections.Generic.IDictionary`2<System.Int32,ZXing.Common.CharacterSetECI>
struct IDictionary_2_tA4CAEF7EA1B676BC14567B876578DD36A432230C;
// System.Collections.Generic.IDictionary`2<System.String,System.Object[]>
struct IDictionary_2_tE7658CD172F37A7C20407EA2E4C3B3F76B3A652A;
// System.Collections.Generic.IDictionary`2<System.String,ZXing.Common.CharacterSetECI>
struct IDictionary_2_tAD5F6FC2CC1A67A97E304C74EFCBCA1E719E50FE;
// System.Collections.Generic.IDictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.String[]>
struct IDictionary_2_tC4CC322DC037ACA2B9BB45C4C2398FC3E0CCD6A9;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_tEB80604A95ECCEC9791230106E95F71A86DEE6AF;
// System.Collections.Generic.IList`1<System.Byte[]>
struct IList_1_t18E09EC85FA64834FAA193798D3324D81C73DC78;
// System.Collections.Generic.IList`1<ZXing.Common.ReedSolomon.GenericGFPoly>
struct IList_1_t00D7461C078638E3107F2D48E23C019F593DC66F;
// System.Collections.Generic.IList`1<ZXing.OneD.OneDReader>
struct IList_1_t0A70AC4E8DB554D35712936E8ED701F01E4F638C;
// System.Collections.Generic.List`1<System.Int32[]>
struct List_1_t0326783A2D4214C62E41A4FD97968B991077A79B;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>
struct List_1_t8968621E959ACCB19941483BDCBBB6443FC5E673;
// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedRow>
struct List_1_t578F33CA35163DC0E0EA696C27D917CDB7460461;
// System.Collections.Generic.List`1<ZXing.OneD.RSS.Pair>
struct List_1_tC9492C021DF8F731EE582F7E256743193B7391E2;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// ZXing.Aztec.Internal.AztecDetectorResult
struct AztecDetectorResult_tDE03D185E80A00BE03617EE579AE2DB5F832D133;
// ZXing.BinaryBitmap
struct BinaryBitmap_tCF98378D7F884CDFBC4985A0ACCFFBE31FC51461;
// ZXing.Common.BitArray
struct BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869;
// ZXing.Common.BitMatrix
struct BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2;
// ZXing.Common.Detector.WhiteRectangleDetector
struct WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4;
// ZXing.Common.ReedSolomon.GenericGF
struct GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F;
// ZXing.Common.ReedSolomon.GenericGFPoly
struct GenericGFPoly_t9D1F5D26F3EF2C3E8150C6A603ABCC096433316A;
// ZXing.Common.ReedSolomon.ReedSolomonDecoder
struct ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427;
// ZXing.Datamatrix.Internal.Decoder
struct Decoder_t0EFE53F08BE5D99F2C8ECDF5F42CF637F3A7CFBB;
// ZXing.Datamatrix.Internal.Version
struct Version_tD8C947A936B05965195D7C3BBA526285521EFD8F;
// ZXing.Datamatrix.Internal.Version/ECB[]
struct ECBU5BU5D_tBEAFBB9238B452E011AAF47BDE0B874E285E4B40;
// ZXing.Datamatrix.Internal.Version/ECBlocks
struct ECBlocks_t223B873D45E32B404C4228B45948B63FB39B7F19;
// ZXing.Datamatrix.Internal.Version[]
struct VersionU5BU5D_t3223B88809EAF0D3B0C9E68E9F2519F3A3E0128D;
// ZXing.LuminanceSource
struct LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1;
// ZXing.Maxicode.Internal.Decoder
struct Decoder_tDB4AB543D8B3BC1F37B79E1E08B9FA58A34EF8AC;
// ZXing.OneD.EANManufacturerOrgSupport
struct EANManufacturerOrgSupport_t1BFBF3CDCC49219358CD56548F8FD730DD2C35A7;
// ZXing.OneD.RSS.DataCharacter
struct DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB;
// ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState
struct CurrentParsingState_t69D94A0BACE62CD40F623625D2A86AF7EFF70BC1;
// ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation
struct DecodedInformation_tD144F663AE3C49E43CF53AEA9027371BC3D8F04A;
// ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder
struct GeneralAppIdDecoder_t19AB438FE36F080A3109E1115E92D2E7BE988077;
// ZXing.OneD.RSS.FinderPattern
struct FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF;
// ZXing.OneD.UPCEANExtension2Support
struct UPCEANExtension2Support_t1FAB0D3ED96CEF129DD7BECBF9DCD16C1EEEB7CA;
// ZXing.OneD.UPCEANExtension5Support
struct UPCEANExtension5Support_tAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD;
// ZXing.OneD.UPCEANExtensionSupport
struct UPCEANExtensionSupport_tCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B;
// ZXing.OneD.UPCEANReader
struct UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D;
// ZXing.OneD.UPCEANReader[]
struct UPCEANReaderU5BU5D_t2B242DEDCF6486BB490E279E57C3C2C952DE1A00;
// ZXing.ResultPoint
struct ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166;
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef AZTECREADER_TFC2F1E1EAB2AB0055551B812448ABF6C0E005EBF_H
#define AZTECREADER_TFC2F1E1EAB2AB0055551B812448ABF6C0E005EBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.AztecReader
struct  AztecReader_tFC2F1E1EAB2AB0055551B812448ABF6C0E005EBF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AZTECREADER_TFC2F1E1EAB2AB0055551B812448ABF6C0E005EBF_H
#ifndef AZTECRESULTMETADATA_TE5FDBE80BFE0F2046505F9EAD9043EDF5CA6FBBA_H
#define AZTECRESULTMETADATA_TE5FDBE80BFE0F2046505F9EAD9043EDF5CA6FBBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.AztecResultMetadata
struct  AztecResultMetadata_tE5FDBE80BFE0F2046505F9EAD9043EDF5CA6FBBA  : public RuntimeObject
{
public:
	// System.Boolean ZXing.Aztec.AztecResultMetadata::<Compact>k__BackingField
	bool ___U3CCompactU3Ek__BackingField_0;
	// System.Int32 ZXing.Aztec.AztecResultMetadata::<Datablocks>k__BackingField
	int32_t ___U3CDatablocksU3Ek__BackingField_1;
	// System.Int32 ZXing.Aztec.AztecResultMetadata::<Layers>k__BackingField
	int32_t ___U3CLayersU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCompactU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AztecResultMetadata_tE5FDBE80BFE0F2046505F9EAD9043EDF5CA6FBBA, ___U3CCompactU3Ek__BackingField_0)); }
	inline bool get_U3CCompactU3Ek__BackingField_0() const { return ___U3CCompactU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CCompactU3Ek__BackingField_0() { return &___U3CCompactU3Ek__BackingField_0; }
	inline void set_U3CCompactU3Ek__BackingField_0(bool value)
	{
		___U3CCompactU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CDatablocksU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AztecResultMetadata_tE5FDBE80BFE0F2046505F9EAD9043EDF5CA6FBBA, ___U3CDatablocksU3Ek__BackingField_1)); }
	inline int32_t get_U3CDatablocksU3Ek__BackingField_1() const { return ___U3CDatablocksU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CDatablocksU3Ek__BackingField_1() { return &___U3CDatablocksU3Ek__BackingField_1; }
	inline void set_U3CDatablocksU3Ek__BackingField_1(int32_t value)
	{
		___U3CDatablocksU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CLayersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AztecResultMetadata_tE5FDBE80BFE0F2046505F9EAD9043EDF5CA6FBBA, ___U3CLayersU3Ek__BackingField_2)); }
	inline int32_t get_U3CLayersU3Ek__BackingField_2() const { return ___U3CLayersU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CLayersU3Ek__BackingField_2() { return &___U3CLayersU3Ek__BackingField_2; }
	inline void set_U3CLayersU3Ek__BackingField_2(int32_t value)
	{
		___U3CLayersU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AZTECRESULTMETADATA_TE5FDBE80BFE0F2046505F9EAD9043EDF5CA6FBBA_H
#ifndef DECODER_TD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_H
#define DECODER_TD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.Internal.Decoder
struct  Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299  : public RuntimeObject
{
public:
	// ZXing.Aztec.Internal.AztecDetectorResult ZXing.Aztec.Internal.Decoder::ddata
	AztecDetectorResult_tDE03D185E80A00BE03617EE579AE2DB5F832D133 * ___ddata_7;

public:
	inline static int32_t get_offset_of_ddata_7() { return static_cast<int32_t>(offsetof(Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299, ___ddata_7)); }
	inline AztecDetectorResult_tDE03D185E80A00BE03617EE579AE2DB5F832D133 * get_ddata_7() const { return ___ddata_7; }
	inline AztecDetectorResult_tDE03D185E80A00BE03617EE579AE2DB5F832D133 ** get_address_of_ddata_7() { return &___ddata_7; }
	inline void set_ddata_7(AztecDetectorResult_tDE03D185E80A00BE03617EE579AE2DB5F832D133 * value)
	{
		___ddata_7 = value;
		Il2CppCodeGenWriteBarrier((&___ddata_7), value);
	}
};

struct Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields
{
public:
	// System.String[] ZXing.Aztec.Internal.Decoder::UPPER_TABLE
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___UPPER_TABLE_0;
	// System.String[] ZXing.Aztec.Internal.Decoder::LOWER_TABLE
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___LOWER_TABLE_1;
	// System.String[] ZXing.Aztec.Internal.Decoder::MIXED_TABLE
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___MIXED_TABLE_2;
	// System.String[] ZXing.Aztec.Internal.Decoder::PUNCT_TABLE
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___PUNCT_TABLE_3;
	// System.String[] ZXing.Aztec.Internal.Decoder::DIGIT_TABLE
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___DIGIT_TABLE_4;
	// System.Collections.Generic.IDictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.String[]> ZXing.Aztec.Internal.Decoder::codeTables
	RuntimeObject* ___codeTables_5;
	// System.Collections.Generic.IDictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table> ZXing.Aztec.Internal.Decoder::codeTableMap
	RuntimeObject* ___codeTableMap_6;

public:
	inline static int32_t get_offset_of_UPPER_TABLE_0() { return static_cast<int32_t>(offsetof(Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields, ___UPPER_TABLE_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_UPPER_TABLE_0() const { return ___UPPER_TABLE_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_UPPER_TABLE_0() { return &___UPPER_TABLE_0; }
	inline void set_UPPER_TABLE_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___UPPER_TABLE_0 = value;
		Il2CppCodeGenWriteBarrier((&___UPPER_TABLE_0), value);
	}

	inline static int32_t get_offset_of_LOWER_TABLE_1() { return static_cast<int32_t>(offsetof(Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields, ___LOWER_TABLE_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_LOWER_TABLE_1() const { return ___LOWER_TABLE_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_LOWER_TABLE_1() { return &___LOWER_TABLE_1; }
	inline void set_LOWER_TABLE_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___LOWER_TABLE_1 = value;
		Il2CppCodeGenWriteBarrier((&___LOWER_TABLE_1), value);
	}

	inline static int32_t get_offset_of_MIXED_TABLE_2() { return static_cast<int32_t>(offsetof(Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields, ___MIXED_TABLE_2)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_MIXED_TABLE_2() const { return ___MIXED_TABLE_2; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_MIXED_TABLE_2() { return &___MIXED_TABLE_2; }
	inline void set_MIXED_TABLE_2(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___MIXED_TABLE_2 = value;
		Il2CppCodeGenWriteBarrier((&___MIXED_TABLE_2), value);
	}

	inline static int32_t get_offset_of_PUNCT_TABLE_3() { return static_cast<int32_t>(offsetof(Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields, ___PUNCT_TABLE_3)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_PUNCT_TABLE_3() const { return ___PUNCT_TABLE_3; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_PUNCT_TABLE_3() { return &___PUNCT_TABLE_3; }
	inline void set_PUNCT_TABLE_3(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___PUNCT_TABLE_3 = value;
		Il2CppCodeGenWriteBarrier((&___PUNCT_TABLE_3), value);
	}

	inline static int32_t get_offset_of_DIGIT_TABLE_4() { return static_cast<int32_t>(offsetof(Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields, ___DIGIT_TABLE_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_DIGIT_TABLE_4() const { return ___DIGIT_TABLE_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_DIGIT_TABLE_4() { return &___DIGIT_TABLE_4; }
	inline void set_DIGIT_TABLE_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___DIGIT_TABLE_4 = value;
		Il2CppCodeGenWriteBarrier((&___DIGIT_TABLE_4), value);
	}

	inline static int32_t get_offset_of_codeTables_5() { return static_cast<int32_t>(offsetof(Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields, ___codeTables_5)); }
	inline RuntimeObject* get_codeTables_5() const { return ___codeTables_5; }
	inline RuntimeObject** get_address_of_codeTables_5() { return &___codeTables_5; }
	inline void set_codeTables_5(RuntimeObject* value)
	{
		___codeTables_5 = value;
		Il2CppCodeGenWriteBarrier((&___codeTables_5), value);
	}

	inline static int32_t get_offset_of_codeTableMap_6() { return static_cast<int32_t>(offsetof(Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields, ___codeTableMap_6)); }
	inline RuntimeObject* get_codeTableMap_6() const { return ___codeTableMap_6; }
	inline RuntimeObject** get_address_of_codeTableMap_6() { return &___codeTableMap_6; }
	inline void set_codeTableMap_6(RuntimeObject* value)
	{
		___codeTableMap_6 = value;
		Il2CppCodeGenWriteBarrier((&___codeTableMap_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODER_TD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_H
#ifndef DETECTOR_T477B046E776FFED542B73805D784710D541601DE_H
#define DETECTOR_T477B046E776FFED542B73805D784710D541601DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.Internal.Detector
struct  Detector_t477B046E776FFED542B73805D784710D541601DE  : public RuntimeObject
{
public:
	// ZXing.Common.BitMatrix ZXing.Aztec.Internal.Detector::image
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * ___image_1;
	// System.Boolean ZXing.Aztec.Internal.Detector::compact
	bool ___compact_2;
	// System.Int32 ZXing.Aztec.Internal.Detector::nbLayers
	int32_t ___nbLayers_3;
	// System.Int32 ZXing.Aztec.Internal.Detector::nbDataBlocks
	int32_t ___nbDataBlocks_4;
	// System.Int32 ZXing.Aztec.Internal.Detector::nbCenterLayers
	int32_t ___nbCenterLayers_5;
	// System.Int32 ZXing.Aztec.Internal.Detector::shift
	int32_t ___shift_6;

public:
	inline static int32_t get_offset_of_image_1() { return static_cast<int32_t>(offsetof(Detector_t477B046E776FFED542B73805D784710D541601DE, ___image_1)); }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * get_image_1() const { return ___image_1; }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 ** get_address_of_image_1() { return &___image_1; }
	inline void set_image_1(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * value)
	{
		___image_1 = value;
		Il2CppCodeGenWriteBarrier((&___image_1), value);
	}

	inline static int32_t get_offset_of_compact_2() { return static_cast<int32_t>(offsetof(Detector_t477B046E776FFED542B73805D784710D541601DE, ___compact_2)); }
	inline bool get_compact_2() const { return ___compact_2; }
	inline bool* get_address_of_compact_2() { return &___compact_2; }
	inline void set_compact_2(bool value)
	{
		___compact_2 = value;
	}

	inline static int32_t get_offset_of_nbLayers_3() { return static_cast<int32_t>(offsetof(Detector_t477B046E776FFED542B73805D784710D541601DE, ___nbLayers_3)); }
	inline int32_t get_nbLayers_3() const { return ___nbLayers_3; }
	inline int32_t* get_address_of_nbLayers_3() { return &___nbLayers_3; }
	inline void set_nbLayers_3(int32_t value)
	{
		___nbLayers_3 = value;
	}

	inline static int32_t get_offset_of_nbDataBlocks_4() { return static_cast<int32_t>(offsetof(Detector_t477B046E776FFED542B73805D784710D541601DE, ___nbDataBlocks_4)); }
	inline int32_t get_nbDataBlocks_4() const { return ___nbDataBlocks_4; }
	inline int32_t* get_address_of_nbDataBlocks_4() { return &___nbDataBlocks_4; }
	inline void set_nbDataBlocks_4(int32_t value)
	{
		___nbDataBlocks_4 = value;
	}

	inline static int32_t get_offset_of_nbCenterLayers_5() { return static_cast<int32_t>(offsetof(Detector_t477B046E776FFED542B73805D784710D541601DE, ___nbCenterLayers_5)); }
	inline int32_t get_nbCenterLayers_5() const { return ___nbCenterLayers_5; }
	inline int32_t* get_address_of_nbCenterLayers_5() { return &___nbCenterLayers_5; }
	inline void set_nbCenterLayers_5(int32_t value)
	{
		___nbCenterLayers_5 = value;
	}

	inline static int32_t get_offset_of_shift_6() { return static_cast<int32_t>(offsetof(Detector_t477B046E776FFED542B73805D784710D541601DE, ___shift_6)); }
	inline int32_t get_shift_6() const { return ___shift_6; }
	inline int32_t* get_address_of_shift_6() { return &___shift_6; }
	inline void set_shift_6(int32_t value)
	{
		___shift_6 = value;
	}
};

struct Detector_t477B046E776FFED542B73805D784710D541601DE_StaticFields
{
public:
	// System.Int32[] ZXing.Aztec.Internal.Detector::EXPECTED_CORNER_BITS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___EXPECTED_CORNER_BITS_0;

public:
	inline static int32_t get_offset_of_EXPECTED_CORNER_BITS_0() { return static_cast<int32_t>(offsetof(Detector_t477B046E776FFED542B73805D784710D541601DE_StaticFields, ___EXPECTED_CORNER_BITS_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_EXPECTED_CORNER_BITS_0() const { return ___EXPECTED_CORNER_BITS_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_EXPECTED_CORNER_BITS_0() { return &___EXPECTED_CORNER_BITS_0; }
	inline void set_EXPECTED_CORNER_BITS_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___EXPECTED_CORNER_BITS_0 = value;
		Il2CppCodeGenWriteBarrier((&___EXPECTED_CORNER_BITS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTOR_T477B046E776FFED542B73805D784710D541601DE_H
#ifndef POINT_T14A3899002BFDE712E74CAA755F46631AE225BB7_H
#define POINT_T14A3899002BFDE712E74CAA755F46631AE225BB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.Internal.Detector/Point
struct  Point_t14A3899002BFDE712E74CAA755F46631AE225BB7  : public RuntimeObject
{
public:
	// System.Int32 ZXing.Aztec.Internal.Detector/Point::<X>k__BackingField
	int32_t ___U3CXU3Ek__BackingField_0;
	// System.Int32 ZXing.Aztec.Internal.Detector/Point::<Y>k__BackingField
	int32_t ___U3CYU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Point_t14A3899002BFDE712E74CAA755F46631AE225BB7, ___U3CXU3Ek__BackingField_0)); }
	inline int32_t get_U3CXU3Ek__BackingField_0() const { return ___U3CXU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CXU3Ek__BackingField_0() { return &___U3CXU3Ek__BackingField_0; }
	inline void set_U3CXU3Ek__BackingField_0(int32_t value)
	{
		___U3CXU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Point_t14A3899002BFDE712E74CAA755F46631AE225BB7, ___U3CYU3Ek__BackingField_1)); }
	inline int32_t get_U3CYU3Ek__BackingField_1() const { return ___U3CYU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CYU3Ek__BackingField_1() { return &___U3CYU3Ek__BackingField_1; }
	inline void set_U3CYU3Ek__BackingField_1(int32_t value)
	{
		___U3CYU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT_T14A3899002BFDE712E74CAA755F46631AE225BB7_H
#ifndef BINARIZER_T5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309_H
#define BINARIZER_T5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Binarizer
struct  Binarizer_t5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309  : public RuntimeObject
{
public:
	// ZXing.LuminanceSource ZXing.Binarizer::source
	LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1 * ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(Binarizer_t5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309, ___source_0)); }
	inline LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1 * get_source_0() const { return ___source_0; }
	inline LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1 ** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1 * value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARIZER_T5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309_H
#ifndef BITARRAY_T5F0A833AE6EA599E51DC8406FEFBC6736A431869_H
#define BITARRAY_T5F0A833AE6EA599E51DC8406FEFBC6736A431869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.BitArray
struct  BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869  : public RuntimeObject
{
public:
	// System.Int32[] ZXing.Common.BitArray::bits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___bits_0;
	// System.Int32 ZXing.Common.BitArray::size
	int32_t ___size_1;

public:
	inline static int32_t get_offset_of_bits_0() { return static_cast<int32_t>(offsetof(BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869, ___bits_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_bits_0() const { return ___bits_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_bits_0() { return &___bits_0; }
	inline void set_bits_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___bits_0 = value;
		Il2CppCodeGenWriteBarrier((&___bits_0), value);
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869, ___size_1)); }
	inline int32_t get_size_1() const { return ___size_1; }
	inline int32_t* get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(int32_t value)
	{
		___size_1 = value;
	}
};

struct BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869_StaticFields
{
public:
	// System.Int32[] ZXing.Common.BitArray::_lookup
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____lookup_2;

public:
	inline static int32_t get_offset_of__lookup_2() { return static_cast<int32_t>(offsetof(BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869_StaticFields, ____lookup_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__lookup_2() const { return ____lookup_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__lookup_2() { return &____lookup_2; }
	inline void set__lookup_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____lookup_2 = value;
		Il2CppCodeGenWriteBarrier((&____lookup_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITARRAY_T5F0A833AE6EA599E51DC8406FEFBC6736A431869_H
#ifndef BITMATRIX_T35850E7E834FAECFF48E762116DAFFC85822CDE2_H
#define BITMATRIX_T35850E7E834FAECFF48E762116DAFFC85822CDE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.BitMatrix
struct  BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2  : public RuntimeObject
{
public:
	// System.Int32 ZXing.Common.BitMatrix::width
	int32_t ___width_0;
	// System.Int32 ZXing.Common.BitMatrix::height
	int32_t ___height_1;
	// System.Int32 ZXing.Common.BitMatrix::rowSize
	int32_t ___rowSize_2;
	// System.Int32[] ZXing.Common.BitMatrix::bits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___bits_3;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_rowSize_2() { return static_cast<int32_t>(offsetof(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2, ___rowSize_2)); }
	inline int32_t get_rowSize_2() const { return ___rowSize_2; }
	inline int32_t* get_address_of_rowSize_2() { return &___rowSize_2; }
	inline void set_rowSize_2(int32_t value)
	{
		___rowSize_2 = value;
	}

	inline static int32_t get_offset_of_bits_3() { return static_cast<int32_t>(offsetof(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2, ___bits_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_bits_3() const { return ___bits_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_bits_3() { return &___bits_3; }
	inline void set_bits_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___bits_3 = value;
		Il2CppCodeGenWriteBarrier((&___bits_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITMATRIX_T35850E7E834FAECFF48E762116DAFFC85822CDE2_H
#ifndef BITSOURCE_T152BE85213C09752548F04B7AD62CD527C87A448_H
#define BITSOURCE_T152BE85213C09752548F04B7AD62CD527C87A448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.BitSource
struct  BitSource_t152BE85213C09752548F04B7AD62CD527C87A448  : public RuntimeObject
{
public:
	// System.Byte[] ZXing.Common.BitSource::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_0;
	// System.Int32 ZXing.Common.BitSource::byteOffset
	int32_t ___byteOffset_1;
	// System.Int32 ZXing.Common.BitSource::bitOffset
	int32_t ___bitOffset_2;

public:
	inline static int32_t get_offset_of_bytes_0() { return static_cast<int32_t>(offsetof(BitSource_t152BE85213C09752548F04B7AD62CD527C87A448, ___bytes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_0() const { return ___bytes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_0() { return &___bytes_0; }
	inline void set_bytes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_0), value);
	}

	inline static int32_t get_offset_of_byteOffset_1() { return static_cast<int32_t>(offsetof(BitSource_t152BE85213C09752548F04B7AD62CD527C87A448, ___byteOffset_1)); }
	inline int32_t get_byteOffset_1() const { return ___byteOffset_1; }
	inline int32_t* get_address_of_byteOffset_1() { return &___byteOffset_1; }
	inline void set_byteOffset_1(int32_t value)
	{
		___byteOffset_1 = value;
	}

	inline static int32_t get_offset_of_bitOffset_2() { return static_cast<int32_t>(offsetof(BitSource_t152BE85213C09752548F04B7AD62CD527C87A448, ___bitOffset_2)); }
	inline int32_t get_bitOffset_2() const { return ___bitOffset_2; }
	inline int32_t* get_address_of_bitOffset_2() { return &___bitOffset_2; }
	inline void set_bitOffset_2(int32_t value)
	{
		___bitOffset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITSOURCE_T152BE85213C09752548F04B7AD62CD527C87A448_H
#ifndef DECODERRESULT_TE12201ED0A19AEAE781E2A67641D4A6FCC198CD7_H
#define DECODERRESULT_TE12201ED0A19AEAE781E2A67641D4A6FCC198CD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.DecoderResult
struct  DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7  : public RuntimeObject
{
public:
	// System.Byte[] ZXing.Common.DecoderResult::<RawBytes>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CRawBytesU3Ek__BackingField_0;
	// System.Int32 ZXing.Common.DecoderResult::<NumBits>k__BackingField
	int32_t ___U3CNumBitsU3Ek__BackingField_1;
	// System.String ZXing.Common.DecoderResult::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_2;
	// System.Collections.Generic.IList`1<System.Byte[]> ZXing.Common.DecoderResult::<ByteSegments>k__BackingField
	RuntimeObject* ___U3CByteSegmentsU3Ek__BackingField_3;
	// System.String ZXing.Common.DecoderResult::<ECLevel>k__BackingField
	String_t* ___U3CECLevelU3Ek__BackingField_4;
	// System.Int32 ZXing.Common.DecoderResult::<ErrorsCorrected>k__BackingField
	int32_t ___U3CErrorsCorrectedU3Ek__BackingField_5;
	// System.Int32 ZXing.Common.DecoderResult::<StructuredAppendSequenceNumber>k__BackingField
	int32_t ___U3CStructuredAppendSequenceNumberU3Ek__BackingField_6;
	// System.Int32 ZXing.Common.DecoderResult::<Erasures>k__BackingField
	int32_t ___U3CErasuresU3Ek__BackingField_7;
	// System.Int32 ZXing.Common.DecoderResult::<StructuredAppendParity>k__BackingField
	int32_t ___U3CStructuredAppendParityU3Ek__BackingField_8;
	// System.Object ZXing.Common.DecoderResult::<Other>k__BackingField
	RuntimeObject * ___U3COtherU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CRawBytesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7, ___U3CRawBytesU3Ek__BackingField_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CRawBytesU3Ek__BackingField_0() const { return ___U3CRawBytesU3Ek__BackingField_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CRawBytesU3Ek__BackingField_0() { return &___U3CRawBytesU3Ek__BackingField_0; }
	inline void set_U3CRawBytesU3Ek__BackingField_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CRawBytesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawBytesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CNumBitsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7, ___U3CNumBitsU3Ek__BackingField_1)); }
	inline int32_t get_U3CNumBitsU3Ek__BackingField_1() const { return ___U3CNumBitsU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CNumBitsU3Ek__BackingField_1() { return &___U3CNumBitsU3Ek__BackingField_1; }
	inline void set_U3CNumBitsU3Ek__BackingField_1(int32_t value)
	{
		___U3CNumBitsU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7, ___U3CTextU3Ek__BackingField_2)); }
	inline String_t* get_U3CTextU3Ek__BackingField_2() const { return ___U3CTextU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_2() { return &___U3CTextU3Ek__BackingField_2; }
	inline void set_U3CTextU3Ek__BackingField_2(String_t* value)
	{
		___U3CTextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CByteSegmentsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7, ___U3CByteSegmentsU3Ek__BackingField_3)); }
	inline RuntimeObject* get_U3CByteSegmentsU3Ek__BackingField_3() const { return ___U3CByteSegmentsU3Ek__BackingField_3; }
	inline RuntimeObject** get_address_of_U3CByteSegmentsU3Ek__BackingField_3() { return &___U3CByteSegmentsU3Ek__BackingField_3; }
	inline void set_U3CByteSegmentsU3Ek__BackingField_3(RuntimeObject* value)
	{
		___U3CByteSegmentsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CByteSegmentsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CECLevelU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7, ___U3CECLevelU3Ek__BackingField_4)); }
	inline String_t* get_U3CECLevelU3Ek__BackingField_4() const { return ___U3CECLevelU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CECLevelU3Ek__BackingField_4() { return &___U3CECLevelU3Ek__BackingField_4; }
	inline void set_U3CECLevelU3Ek__BackingField_4(String_t* value)
	{
		___U3CECLevelU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CECLevelU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CErrorsCorrectedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7, ___U3CErrorsCorrectedU3Ek__BackingField_5)); }
	inline int32_t get_U3CErrorsCorrectedU3Ek__BackingField_5() const { return ___U3CErrorsCorrectedU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CErrorsCorrectedU3Ek__BackingField_5() { return &___U3CErrorsCorrectedU3Ek__BackingField_5; }
	inline void set_U3CErrorsCorrectedU3Ek__BackingField_5(int32_t value)
	{
		___U3CErrorsCorrectedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CStructuredAppendSequenceNumberU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7, ___U3CStructuredAppendSequenceNumberU3Ek__BackingField_6)); }
	inline int32_t get_U3CStructuredAppendSequenceNumberU3Ek__BackingField_6() const { return ___U3CStructuredAppendSequenceNumberU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CStructuredAppendSequenceNumberU3Ek__BackingField_6() { return &___U3CStructuredAppendSequenceNumberU3Ek__BackingField_6; }
	inline void set_U3CStructuredAppendSequenceNumberU3Ek__BackingField_6(int32_t value)
	{
		___U3CStructuredAppendSequenceNumberU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CErasuresU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7, ___U3CErasuresU3Ek__BackingField_7)); }
	inline int32_t get_U3CErasuresU3Ek__BackingField_7() const { return ___U3CErasuresU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CErasuresU3Ek__BackingField_7() { return &___U3CErasuresU3Ek__BackingField_7; }
	inline void set_U3CErasuresU3Ek__BackingField_7(int32_t value)
	{
		___U3CErasuresU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CStructuredAppendParityU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7, ___U3CStructuredAppendParityU3Ek__BackingField_8)); }
	inline int32_t get_U3CStructuredAppendParityU3Ek__BackingField_8() const { return ___U3CStructuredAppendParityU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CStructuredAppendParityU3Ek__BackingField_8() { return &___U3CStructuredAppendParityU3Ek__BackingField_8; }
	inline void set_U3CStructuredAppendParityU3Ek__BackingField_8(int32_t value)
	{
		___U3CStructuredAppendParityU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3COtherU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7, ___U3COtherU3Ek__BackingField_9)); }
	inline RuntimeObject * get_U3COtherU3Ek__BackingField_9() const { return ___U3COtherU3Ek__BackingField_9; }
	inline RuntimeObject ** get_address_of_U3COtherU3Ek__BackingField_9() { return &___U3COtherU3Ek__BackingField_9; }
	inline void set_U3COtherU3Ek__BackingField_9(RuntimeObject * value)
	{
		___U3COtherU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3COtherU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODERRESULT_TE12201ED0A19AEAE781E2A67641D4A6FCC198CD7_H
#ifndef DECODINGOPTIONS_T47C061C31B88DE9FB76187F5289E251C3B2E35B7_H
#define DECODINGOPTIONS_T47C061C31B88DE9FB76187F5289E251C3B2E35B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.DecodingOptions
struct  DecodingOptions_t47C061C31B88DE9FB76187F5289E251C3B2E35B7  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object> ZXing.Common.DecodingOptions::<Hints>k__BackingField
	RuntimeObject* ___U3CHintsU3Ek__BackingField_0;
	// System.Action`2<System.Object,System.EventArgs> ZXing.Common.DecodingOptions::ValueChanged
	Action_2_tC27C27AF2F38BC3A52153621D61F10F9421E87D2 * ___ValueChanged_1;

public:
	inline static int32_t get_offset_of_U3CHintsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DecodingOptions_t47C061C31B88DE9FB76187F5289E251C3B2E35B7, ___U3CHintsU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CHintsU3Ek__BackingField_0() const { return ___U3CHintsU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CHintsU3Ek__BackingField_0() { return &___U3CHintsU3Ek__BackingField_0; }
	inline void set_U3CHintsU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CHintsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHintsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_ValueChanged_1() { return static_cast<int32_t>(offsetof(DecodingOptions_t47C061C31B88DE9FB76187F5289E251C3B2E35B7, ___ValueChanged_1)); }
	inline Action_2_tC27C27AF2F38BC3A52153621D61F10F9421E87D2 * get_ValueChanged_1() const { return ___ValueChanged_1; }
	inline Action_2_tC27C27AF2F38BC3A52153621D61F10F9421E87D2 ** get_address_of_ValueChanged_1() { return &___ValueChanged_1; }
	inline void set_ValueChanged_1(Action_2_tC27C27AF2F38BC3A52153621D61F10F9421E87D2 * value)
	{
		___ValueChanged_1 = value;
		Il2CppCodeGenWriteBarrier((&___ValueChanged_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODINGOPTIONS_T47C061C31B88DE9FB76187F5289E251C3B2E35B7_H
#ifndef MATHUTILS_T15FC2B42DF83BEF551EF6D9DDF43D546CF5B1E9B_H
#define MATHUTILS_T15FC2B42DF83BEF551EF6D9DDF43D546CF5B1E9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.Detector.MathUtils
struct  MathUtils_t15FC2B42DF83BEF551EF6D9DDF43D546CF5B1E9B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTILS_T15FC2B42DF83BEF551EF6D9DDF43D546CF5B1E9B_H
#ifndef WHITERECTANGLEDETECTOR_T45C0E6821055358C7556A1BEA2D735E80E2609A4_H
#define WHITERECTANGLEDETECTOR_T45C0E6821055358C7556A1BEA2D735E80E2609A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.Detector.WhiteRectangleDetector
struct  WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4  : public RuntimeObject
{
public:
	// ZXing.Common.BitMatrix ZXing.Common.Detector.WhiteRectangleDetector::image
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * ___image_0;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::height
	int32_t ___height_1;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::width
	int32_t ___width_2;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::leftInit
	int32_t ___leftInit_3;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::rightInit
	int32_t ___rightInit_4;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::downInit
	int32_t ___downInit_5;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::upInit
	int32_t ___upInit_6;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4, ___image_0)); }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * get_image_0() const { return ___image_0; }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier((&___image_0), value);
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4, ___width_2)); }
	inline int32_t get_width_2() const { return ___width_2; }
	inline int32_t* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(int32_t value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_leftInit_3() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4, ___leftInit_3)); }
	inline int32_t get_leftInit_3() const { return ___leftInit_3; }
	inline int32_t* get_address_of_leftInit_3() { return &___leftInit_3; }
	inline void set_leftInit_3(int32_t value)
	{
		___leftInit_3 = value;
	}

	inline static int32_t get_offset_of_rightInit_4() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4, ___rightInit_4)); }
	inline int32_t get_rightInit_4() const { return ___rightInit_4; }
	inline int32_t* get_address_of_rightInit_4() { return &___rightInit_4; }
	inline void set_rightInit_4(int32_t value)
	{
		___rightInit_4 = value;
	}

	inline static int32_t get_offset_of_downInit_5() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4, ___downInit_5)); }
	inline int32_t get_downInit_5() const { return ___downInit_5; }
	inline int32_t* get_address_of_downInit_5() { return &___downInit_5; }
	inline void set_downInit_5(int32_t value)
	{
		___downInit_5 = value;
	}

	inline static int32_t get_offset_of_upInit_6() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4, ___upInit_6)); }
	inline int32_t get_upInit_6() const { return ___upInit_6; }
	inline int32_t* get_address_of_upInit_6() { return &___upInit_6; }
	inline void set_upInit_6(int32_t value)
	{
		___upInit_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHITERECTANGLEDETECTOR_T45C0E6821055358C7556A1BEA2D735E80E2609A4_H
#ifndef DETECTORRESULT_T87E236CAB9ED47A76E59D1809CE6C7DD6D8E9E2C_H
#define DETECTORRESULT_T87E236CAB9ED47A76E59D1809CE6C7DD6D8E9E2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.DetectorResult
struct  DetectorResult_t87E236CAB9ED47A76E59D1809CE6C7DD6D8E9E2C  : public RuntimeObject
{
public:
	// ZXing.Common.BitMatrix ZXing.Common.DetectorResult::<Bits>k__BackingField
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * ___U3CBitsU3Ek__BackingField_0;
	// ZXing.ResultPoint[] ZXing.Common.DetectorResult::<Points>k__BackingField
	ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* ___U3CPointsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CBitsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DetectorResult_t87E236CAB9ED47A76E59D1809CE6C7DD6D8E9E2C, ___U3CBitsU3Ek__BackingField_0)); }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * get_U3CBitsU3Ek__BackingField_0() const { return ___U3CBitsU3Ek__BackingField_0; }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 ** get_address_of_U3CBitsU3Ek__BackingField_0() { return &___U3CBitsU3Ek__BackingField_0; }
	inline void set_U3CBitsU3Ek__BackingField_0(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * value)
	{
		___U3CBitsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBitsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPointsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DetectorResult_t87E236CAB9ED47A76E59D1809CE6C7DD6D8E9E2C, ___U3CPointsU3Ek__BackingField_1)); }
	inline ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* get_U3CPointsU3Ek__BackingField_1() const { return ___U3CPointsU3Ek__BackingField_1; }
	inline ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C** get_address_of_U3CPointsU3Ek__BackingField_1() { return &___U3CPointsU3Ek__BackingField_1; }
	inline void set_U3CPointsU3Ek__BackingField_1(ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* value)
	{
		___U3CPointsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPointsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTORRESULT_T87E236CAB9ED47A76E59D1809CE6C7DD6D8E9E2C_H
#ifndef ECI_TEF9EAED8C9F3BFA6043AC72BEE50849BD3124A1A_H
#define ECI_TEF9EAED8C9F3BFA6043AC72BEE50849BD3124A1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.ECI
struct  ECI_tEF9EAED8C9F3BFA6043AC72BEE50849BD3124A1A  : public RuntimeObject
{
public:
	// System.Int32 ZXing.Common.ECI::<Value>k__BackingField
	int32_t ___U3CValueU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ECI_tEF9EAED8C9F3BFA6043AC72BEE50849BD3124A1A, ___U3CValueU3Ek__BackingField_0)); }
	inline int32_t get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(int32_t value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECI_TEF9EAED8C9F3BFA6043AC72BEE50849BD3124A1A_H
#ifndef GRIDSAMPLER_T8DD0C5250D72AC8D9F5DFD0F417F2FE212BE91D4_H
#define GRIDSAMPLER_T8DD0C5250D72AC8D9F5DFD0F417F2FE212BE91D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.GridSampler
struct  GridSampler_t8DD0C5250D72AC8D9F5DFD0F417F2FE212BE91D4  : public RuntimeObject
{
public:

public:
};

struct GridSampler_t8DD0C5250D72AC8D9F5DFD0F417F2FE212BE91D4_StaticFields
{
public:
	// ZXing.Common.GridSampler ZXing.Common.GridSampler::gridSampler
	GridSampler_t8DD0C5250D72AC8D9F5DFD0F417F2FE212BE91D4 * ___gridSampler_0;

public:
	inline static int32_t get_offset_of_gridSampler_0() { return static_cast<int32_t>(offsetof(GridSampler_t8DD0C5250D72AC8D9F5DFD0F417F2FE212BE91D4_StaticFields, ___gridSampler_0)); }
	inline GridSampler_t8DD0C5250D72AC8D9F5DFD0F417F2FE212BE91D4 * get_gridSampler_0() const { return ___gridSampler_0; }
	inline GridSampler_t8DD0C5250D72AC8D9F5DFD0F417F2FE212BE91D4 ** get_address_of_gridSampler_0() { return &___gridSampler_0; }
	inline void set_gridSampler_0(GridSampler_t8DD0C5250D72AC8D9F5DFD0F417F2FE212BE91D4 * value)
	{
		___gridSampler_0 = value;
		Il2CppCodeGenWriteBarrier((&___gridSampler_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRIDSAMPLER_T8DD0C5250D72AC8D9F5DFD0F417F2FE212BE91D4_H
#ifndef PERSPECTIVETRANSFORM_T411B3056590AB547648BD95C69DAAE53E890246E_H
#define PERSPECTIVETRANSFORM_T411B3056590AB547648BD95C69DAAE53E890246E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.PerspectiveTransform
struct  PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E  : public RuntimeObject
{
public:
	// System.Single ZXing.Common.PerspectiveTransform::a11
	float ___a11_0;
	// System.Single ZXing.Common.PerspectiveTransform::a12
	float ___a12_1;
	// System.Single ZXing.Common.PerspectiveTransform::a13
	float ___a13_2;
	// System.Single ZXing.Common.PerspectiveTransform::a21
	float ___a21_3;
	// System.Single ZXing.Common.PerspectiveTransform::a22
	float ___a22_4;
	// System.Single ZXing.Common.PerspectiveTransform::a23
	float ___a23_5;
	// System.Single ZXing.Common.PerspectiveTransform::a31
	float ___a31_6;
	// System.Single ZXing.Common.PerspectiveTransform::a32
	float ___a32_7;
	// System.Single ZXing.Common.PerspectiveTransform::a33
	float ___a33_8;

public:
	inline static int32_t get_offset_of_a11_0() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E, ___a11_0)); }
	inline float get_a11_0() const { return ___a11_0; }
	inline float* get_address_of_a11_0() { return &___a11_0; }
	inline void set_a11_0(float value)
	{
		___a11_0 = value;
	}

	inline static int32_t get_offset_of_a12_1() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E, ___a12_1)); }
	inline float get_a12_1() const { return ___a12_1; }
	inline float* get_address_of_a12_1() { return &___a12_1; }
	inline void set_a12_1(float value)
	{
		___a12_1 = value;
	}

	inline static int32_t get_offset_of_a13_2() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E, ___a13_2)); }
	inline float get_a13_2() const { return ___a13_2; }
	inline float* get_address_of_a13_2() { return &___a13_2; }
	inline void set_a13_2(float value)
	{
		___a13_2 = value;
	}

	inline static int32_t get_offset_of_a21_3() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E, ___a21_3)); }
	inline float get_a21_3() const { return ___a21_3; }
	inline float* get_address_of_a21_3() { return &___a21_3; }
	inline void set_a21_3(float value)
	{
		___a21_3 = value;
	}

	inline static int32_t get_offset_of_a22_4() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E, ___a22_4)); }
	inline float get_a22_4() const { return ___a22_4; }
	inline float* get_address_of_a22_4() { return &___a22_4; }
	inline void set_a22_4(float value)
	{
		___a22_4 = value;
	}

	inline static int32_t get_offset_of_a23_5() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E, ___a23_5)); }
	inline float get_a23_5() const { return ___a23_5; }
	inline float* get_address_of_a23_5() { return &___a23_5; }
	inline void set_a23_5(float value)
	{
		___a23_5 = value;
	}

	inline static int32_t get_offset_of_a31_6() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E, ___a31_6)); }
	inline float get_a31_6() const { return ___a31_6; }
	inline float* get_address_of_a31_6() { return &___a31_6; }
	inline void set_a31_6(float value)
	{
		___a31_6 = value;
	}

	inline static int32_t get_offset_of_a32_7() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E, ___a32_7)); }
	inline float get_a32_7() const { return ___a32_7; }
	inline float* get_address_of_a32_7() { return &___a32_7; }
	inline void set_a32_7(float value)
	{
		___a32_7 = value;
	}

	inline static int32_t get_offset_of_a33_8() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E, ___a33_8)); }
	inline float get_a33_8() const { return ___a33_8; }
	inline float* get_address_of_a33_8() { return &___a33_8; }
	inline void set_a33_8(float value)
	{
		___a33_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSPECTIVETRANSFORM_T411B3056590AB547648BD95C69DAAE53E890246E_H
#ifndef GENERICGF_TE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_H
#define GENERICGF_TE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.ReedSolomon.GenericGF
struct  GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F  : public RuntimeObject
{
public:
	// System.Int32[] ZXing.Common.ReedSolomon.GenericGF::expTable
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___expTable_8;
	// System.Int32[] ZXing.Common.ReedSolomon.GenericGF::logTable
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___logTable_9;
	// ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGF::zero
	GenericGFPoly_t9D1F5D26F3EF2C3E8150C6A603ABCC096433316A * ___zero_10;
	// ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGF::one
	GenericGFPoly_t9D1F5D26F3EF2C3E8150C6A603ABCC096433316A * ___one_11;
	// System.Int32 ZXing.Common.ReedSolomon.GenericGF::size
	int32_t ___size_12;
	// System.Int32 ZXing.Common.ReedSolomon.GenericGF::primitive
	int32_t ___primitive_13;
	// System.Int32 ZXing.Common.ReedSolomon.GenericGF::generatorBase
	int32_t ___generatorBase_14;

public:
	inline static int32_t get_offset_of_expTable_8() { return static_cast<int32_t>(offsetof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F, ___expTable_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_expTable_8() const { return ___expTable_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_expTable_8() { return &___expTable_8; }
	inline void set_expTable_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___expTable_8 = value;
		Il2CppCodeGenWriteBarrier((&___expTable_8), value);
	}

	inline static int32_t get_offset_of_logTable_9() { return static_cast<int32_t>(offsetof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F, ___logTable_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_logTable_9() const { return ___logTable_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_logTable_9() { return &___logTable_9; }
	inline void set_logTable_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___logTable_9 = value;
		Il2CppCodeGenWriteBarrier((&___logTable_9), value);
	}

	inline static int32_t get_offset_of_zero_10() { return static_cast<int32_t>(offsetof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F, ___zero_10)); }
	inline GenericGFPoly_t9D1F5D26F3EF2C3E8150C6A603ABCC096433316A * get_zero_10() const { return ___zero_10; }
	inline GenericGFPoly_t9D1F5D26F3EF2C3E8150C6A603ABCC096433316A ** get_address_of_zero_10() { return &___zero_10; }
	inline void set_zero_10(GenericGFPoly_t9D1F5D26F3EF2C3E8150C6A603ABCC096433316A * value)
	{
		___zero_10 = value;
		Il2CppCodeGenWriteBarrier((&___zero_10), value);
	}

	inline static int32_t get_offset_of_one_11() { return static_cast<int32_t>(offsetof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F, ___one_11)); }
	inline GenericGFPoly_t9D1F5D26F3EF2C3E8150C6A603ABCC096433316A * get_one_11() const { return ___one_11; }
	inline GenericGFPoly_t9D1F5D26F3EF2C3E8150C6A603ABCC096433316A ** get_address_of_one_11() { return &___one_11; }
	inline void set_one_11(GenericGFPoly_t9D1F5D26F3EF2C3E8150C6A603ABCC096433316A * value)
	{
		___one_11 = value;
		Il2CppCodeGenWriteBarrier((&___one_11), value);
	}

	inline static int32_t get_offset_of_size_12() { return static_cast<int32_t>(offsetof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F, ___size_12)); }
	inline int32_t get_size_12() const { return ___size_12; }
	inline int32_t* get_address_of_size_12() { return &___size_12; }
	inline void set_size_12(int32_t value)
	{
		___size_12 = value;
	}

	inline static int32_t get_offset_of_primitive_13() { return static_cast<int32_t>(offsetof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F, ___primitive_13)); }
	inline int32_t get_primitive_13() const { return ___primitive_13; }
	inline int32_t* get_address_of_primitive_13() { return &___primitive_13; }
	inline void set_primitive_13(int32_t value)
	{
		___primitive_13 = value;
	}

	inline static int32_t get_offset_of_generatorBase_14() { return static_cast<int32_t>(offsetof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F, ___generatorBase_14)); }
	inline int32_t get_generatorBase_14() const { return ___generatorBase_14; }
	inline int32_t* get_address_of_generatorBase_14() { return &___generatorBase_14; }
	inline void set_generatorBase_14(int32_t value)
	{
		___generatorBase_14 = value;
	}
};

struct GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields
{
public:
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::AZTEC_DATA_12
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * ___AZTEC_DATA_12_0;
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::AZTEC_DATA_10
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * ___AZTEC_DATA_10_1;
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::AZTEC_DATA_6
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * ___AZTEC_DATA_6_2;
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::AZTEC_PARAM
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * ___AZTEC_PARAM_3;
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::QR_CODE_FIELD_256
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * ___QR_CODE_FIELD_256_4;
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::DATA_MATRIX_FIELD_256
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * ___DATA_MATRIX_FIELD_256_5;
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::AZTEC_DATA_8
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * ___AZTEC_DATA_8_6;
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::MAXICODE_FIELD_64
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * ___MAXICODE_FIELD_64_7;

public:
	inline static int32_t get_offset_of_AZTEC_DATA_12_0() { return static_cast<int32_t>(offsetof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields, ___AZTEC_DATA_12_0)); }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * get_AZTEC_DATA_12_0() const { return ___AZTEC_DATA_12_0; }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F ** get_address_of_AZTEC_DATA_12_0() { return &___AZTEC_DATA_12_0; }
	inline void set_AZTEC_DATA_12_0(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * value)
	{
		___AZTEC_DATA_12_0 = value;
		Il2CppCodeGenWriteBarrier((&___AZTEC_DATA_12_0), value);
	}

	inline static int32_t get_offset_of_AZTEC_DATA_10_1() { return static_cast<int32_t>(offsetof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields, ___AZTEC_DATA_10_1)); }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * get_AZTEC_DATA_10_1() const { return ___AZTEC_DATA_10_1; }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F ** get_address_of_AZTEC_DATA_10_1() { return &___AZTEC_DATA_10_1; }
	inline void set_AZTEC_DATA_10_1(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * value)
	{
		___AZTEC_DATA_10_1 = value;
		Il2CppCodeGenWriteBarrier((&___AZTEC_DATA_10_1), value);
	}

	inline static int32_t get_offset_of_AZTEC_DATA_6_2() { return static_cast<int32_t>(offsetof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields, ___AZTEC_DATA_6_2)); }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * get_AZTEC_DATA_6_2() const { return ___AZTEC_DATA_6_2; }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F ** get_address_of_AZTEC_DATA_6_2() { return &___AZTEC_DATA_6_2; }
	inline void set_AZTEC_DATA_6_2(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * value)
	{
		___AZTEC_DATA_6_2 = value;
		Il2CppCodeGenWriteBarrier((&___AZTEC_DATA_6_2), value);
	}

	inline static int32_t get_offset_of_AZTEC_PARAM_3() { return static_cast<int32_t>(offsetof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields, ___AZTEC_PARAM_3)); }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * get_AZTEC_PARAM_3() const { return ___AZTEC_PARAM_3; }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F ** get_address_of_AZTEC_PARAM_3() { return &___AZTEC_PARAM_3; }
	inline void set_AZTEC_PARAM_3(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * value)
	{
		___AZTEC_PARAM_3 = value;
		Il2CppCodeGenWriteBarrier((&___AZTEC_PARAM_3), value);
	}

	inline static int32_t get_offset_of_QR_CODE_FIELD_256_4() { return static_cast<int32_t>(offsetof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields, ___QR_CODE_FIELD_256_4)); }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * get_QR_CODE_FIELD_256_4() const { return ___QR_CODE_FIELD_256_4; }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F ** get_address_of_QR_CODE_FIELD_256_4() { return &___QR_CODE_FIELD_256_4; }
	inline void set_QR_CODE_FIELD_256_4(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * value)
	{
		___QR_CODE_FIELD_256_4 = value;
		Il2CppCodeGenWriteBarrier((&___QR_CODE_FIELD_256_4), value);
	}

	inline static int32_t get_offset_of_DATA_MATRIX_FIELD_256_5() { return static_cast<int32_t>(offsetof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields, ___DATA_MATRIX_FIELD_256_5)); }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * get_DATA_MATRIX_FIELD_256_5() const { return ___DATA_MATRIX_FIELD_256_5; }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F ** get_address_of_DATA_MATRIX_FIELD_256_5() { return &___DATA_MATRIX_FIELD_256_5; }
	inline void set_DATA_MATRIX_FIELD_256_5(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * value)
	{
		___DATA_MATRIX_FIELD_256_5 = value;
		Il2CppCodeGenWriteBarrier((&___DATA_MATRIX_FIELD_256_5), value);
	}

	inline static int32_t get_offset_of_AZTEC_DATA_8_6() { return static_cast<int32_t>(offsetof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields, ___AZTEC_DATA_8_6)); }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * get_AZTEC_DATA_8_6() const { return ___AZTEC_DATA_8_6; }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F ** get_address_of_AZTEC_DATA_8_6() { return &___AZTEC_DATA_8_6; }
	inline void set_AZTEC_DATA_8_6(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * value)
	{
		___AZTEC_DATA_8_6 = value;
		Il2CppCodeGenWriteBarrier((&___AZTEC_DATA_8_6), value);
	}

	inline static int32_t get_offset_of_MAXICODE_FIELD_64_7() { return static_cast<int32_t>(offsetof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields, ___MAXICODE_FIELD_64_7)); }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * get_MAXICODE_FIELD_64_7() const { return ___MAXICODE_FIELD_64_7; }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F ** get_address_of_MAXICODE_FIELD_64_7() { return &___MAXICODE_FIELD_64_7; }
	inline void set_MAXICODE_FIELD_64_7(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * value)
	{
		___MAXICODE_FIELD_64_7 = value;
		Il2CppCodeGenWriteBarrier((&___MAXICODE_FIELD_64_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICGF_TE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_H
#ifndef GENERICGFPOLY_T9D1F5D26F3EF2C3E8150C6A603ABCC096433316A_H
#define GENERICGFPOLY_T9D1F5D26F3EF2C3E8150C6A603ABCC096433316A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.ReedSolomon.GenericGFPoly
struct  GenericGFPoly_t9D1F5D26F3EF2C3E8150C6A603ABCC096433316A  : public RuntimeObject
{
public:
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGFPoly::field
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * ___field_0;
	// System.Int32[] ZXing.Common.ReedSolomon.GenericGFPoly::coefficients
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___coefficients_1;

public:
	inline static int32_t get_offset_of_field_0() { return static_cast<int32_t>(offsetof(GenericGFPoly_t9D1F5D26F3EF2C3E8150C6A603ABCC096433316A, ___field_0)); }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * get_field_0() const { return ___field_0; }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F ** get_address_of_field_0() { return &___field_0; }
	inline void set_field_0(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * value)
	{
		___field_0 = value;
		Il2CppCodeGenWriteBarrier((&___field_0), value);
	}

	inline static int32_t get_offset_of_coefficients_1() { return static_cast<int32_t>(offsetof(GenericGFPoly_t9D1F5D26F3EF2C3E8150C6A603ABCC096433316A, ___coefficients_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_coefficients_1() const { return ___coefficients_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_coefficients_1() { return &___coefficients_1; }
	inline void set_coefficients_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___coefficients_1 = value;
		Il2CppCodeGenWriteBarrier((&___coefficients_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICGFPOLY_T9D1F5D26F3EF2C3E8150C6A603ABCC096433316A_H
#ifndef REEDSOLOMONDECODER_T254937BB15BEE22F18671B0FA89DDBE9060A4427_H
#define REEDSOLOMONDECODER_T254937BB15BEE22F18671B0FA89DDBE9060A4427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.ReedSolomon.ReedSolomonDecoder
struct  ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427  : public RuntimeObject
{
public:
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.ReedSolomonDecoder::field
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * ___field_0;

public:
	inline static int32_t get_offset_of_field_0() { return static_cast<int32_t>(offsetof(ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427, ___field_0)); }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * get_field_0() const { return ___field_0; }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F ** get_address_of_field_0() { return &___field_0; }
	inline void set_field_0(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * value)
	{
		___field_0 = value;
		Il2CppCodeGenWriteBarrier((&___field_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REEDSOLOMONDECODER_T254937BB15BEE22F18671B0FA89DDBE9060A4427_H
#ifndef REEDSOLOMONENCODER_T31EBCD471B9E5411E55FEDFC6C61B3D95BC630E3_H
#define REEDSOLOMONENCODER_T31EBCD471B9E5411E55FEDFC6C61B3D95BC630E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.ReedSolomon.ReedSolomonEncoder
struct  ReedSolomonEncoder_t31EBCD471B9E5411E55FEDFC6C61B3D95BC630E3  : public RuntimeObject
{
public:
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.ReedSolomonEncoder::field
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * ___field_0;
	// System.Collections.Generic.IList`1<ZXing.Common.ReedSolomon.GenericGFPoly> ZXing.Common.ReedSolomon.ReedSolomonEncoder::cachedGenerators
	RuntimeObject* ___cachedGenerators_1;

public:
	inline static int32_t get_offset_of_field_0() { return static_cast<int32_t>(offsetof(ReedSolomonEncoder_t31EBCD471B9E5411E55FEDFC6C61B3D95BC630E3, ___field_0)); }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * get_field_0() const { return ___field_0; }
	inline GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F ** get_address_of_field_0() { return &___field_0; }
	inline void set_field_0(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F * value)
	{
		___field_0 = value;
		Il2CppCodeGenWriteBarrier((&___field_0), value);
	}

	inline static int32_t get_offset_of_cachedGenerators_1() { return static_cast<int32_t>(offsetof(ReedSolomonEncoder_t31EBCD471B9E5411E55FEDFC6C61B3D95BC630E3, ___cachedGenerators_1)); }
	inline RuntimeObject* get_cachedGenerators_1() const { return ___cachedGenerators_1; }
	inline RuntimeObject** get_address_of_cachedGenerators_1() { return &___cachedGenerators_1; }
	inline void set_cachedGenerators_1(RuntimeObject* value)
	{
		___cachedGenerators_1 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGenerators_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REEDSOLOMONENCODER_T31EBCD471B9E5411E55FEDFC6C61B3D95BC630E3_H
#ifndef STRINGUTILS_T49EDE148BE359FFB850E9D1A27AEBA5B7136EA92_H
#define STRINGUTILS_T49EDE148BE359FFB850E9D1A27AEBA5B7136EA92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.StringUtils
struct  StringUtils_t49EDE148BE359FFB850E9D1A27AEBA5B7136EA92  : public RuntimeObject
{
public:

public:
};

struct StringUtils_t49EDE148BE359FFB850E9D1A27AEBA5B7136EA92_StaticFields
{
public:
	// System.String ZXing.Common.StringUtils::SHIFT_JIS
	String_t* ___SHIFT_JIS_0;
	// System.String ZXing.Common.StringUtils::GB2312
	String_t* ___GB2312_1;
	// System.Boolean ZXing.Common.StringUtils::ASSUME_SHIFT_JIS
	bool ___ASSUME_SHIFT_JIS_2;

public:
	inline static int32_t get_offset_of_SHIFT_JIS_0() { return static_cast<int32_t>(offsetof(StringUtils_t49EDE148BE359FFB850E9D1A27AEBA5B7136EA92_StaticFields, ___SHIFT_JIS_0)); }
	inline String_t* get_SHIFT_JIS_0() const { return ___SHIFT_JIS_0; }
	inline String_t** get_address_of_SHIFT_JIS_0() { return &___SHIFT_JIS_0; }
	inline void set_SHIFT_JIS_0(String_t* value)
	{
		___SHIFT_JIS_0 = value;
		Il2CppCodeGenWriteBarrier((&___SHIFT_JIS_0), value);
	}

	inline static int32_t get_offset_of_GB2312_1() { return static_cast<int32_t>(offsetof(StringUtils_t49EDE148BE359FFB850E9D1A27AEBA5B7136EA92_StaticFields, ___GB2312_1)); }
	inline String_t* get_GB2312_1() const { return ___GB2312_1; }
	inline String_t** get_address_of_GB2312_1() { return &___GB2312_1; }
	inline void set_GB2312_1(String_t* value)
	{
		___GB2312_1 = value;
		Il2CppCodeGenWriteBarrier((&___GB2312_1), value);
	}

	inline static int32_t get_offset_of_ASSUME_SHIFT_JIS_2() { return static_cast<int32_t>(offsetof(StringUtils_t49EDE148BE359FFB850E9D1A27AEBA5B7136EA92_StaticFields, ___ASSUME_SHIFT_JIS_2)); }
	inline bool get_ASSUME_SHIFT_JIS_2() const { return ___ASSUME_SHIFT_JIS_2; }
	inline bool* get_address_of_ASSUME_SHIFT_JIS_2() { return &___ASSUME_SHIFT_JIS_2; }
	inline void set_ASSUME_SHIFT_JIS_2(bool value)
	{
		___ASSUME_SHIFT_JIS_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILS_T49EDE148BE359FFB850E9D1A27AEBA5B7136EA92_H
#ifndef DATAMATRIXREADER_TC3F824C7E939E205F6ACE210AB0A7F743B256B0E_H
#define DATAMATRIXREADER_TC3F824C7E939E205F6ACE210AB0A7F743B256B0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.DataMatrixReader
struct  DataMatrixReader_tC3F824C7E939E205F6ACE210AB0A7F743B256B0E  : public RuntimeObject
{
public:
	// ZXing.Datamatrix.Internal.Decoder ZXing.Datamatrix.DataMatrixReader::decoder
	Decoder_t0EFE53F08BE5D99F2C8ECDF5F42CF637F3A7CFBB * ___decoder_1;

public:
	inline static int32_t get_offset_of_decoder_1() { return static_cast<int32_t>(offsetof(DataMatrixReader_tC3F824C7E939E205F6ACE210AB0A7F743B256B0E, ___decoder_1)); }
	inline Decoder_t0EFE53F08BE5D99F2C8ECDF5F42CF637F3A7CFBB * get_decoder_1() const { return ___decoder_1; }
	inline Decoder_t0EFE53F08BE5D99F2C8ECDF5F42CF637F3A7CFBB ** get_address_of_decoder_1() { return &___decoder_1; }
	inline void set_decoder_1(Decoder_t0EFE53F08BE5D99F2C8ECDF5F42CF637F3A7CFBB * value)
	{
		___decoder_1 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_1), value);
	}
};

struct DataMatrixReader_tC3F824C7E939E205F6ACE210AB0A7F743B256B0E_StaticFields
{
public:
	// ZXing.ResultPoint[] ZXing.Datamatrix.DataMatrixReader::NO_POINTS
	ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* ___NO_POINTS_0;

public:
	inline static int32_t get_offset_of_NO_POINTS_0() { return static_cast<int32_t>(offsetof(DataMatrixReader_tC3F824C7E939E205F6ACE210AB0A7F743B256B0E_StaticFields, ___NO_POINTS_0)); }
	inline ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* get_NO_POINTS_0() const { return ___NO_POINTS_0; }
	inline ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C** get_address_of_NO_POINTS_0() { return &___NO_POINTS_0; }
	inline void set_NO_POINTS_0(ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* value)
	{
		___NO_POINTS_0 = value;
		Il2CppCodeGenWriteBarrier((&___NO_POINTS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAMATRIXREADER_TC3F824C7E939E205F6ACE210AB0A7F743B256B0E_H
#ifndef BITMATRIXPARSER_TD8BA978B30ED653846ECDA9F103A20D754031AF6_H
#define BITMATRIXPARSER_TD8BA978B30ED653846ECDA9F103A20D754031AF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.BitMatrixParser
struct  BitMatrixParser_tD8BA978B30ED653846ECDA9F103A20D754031AF6  : public RuntimeObject
{
public:
	// ZXing.Common.BitMatrix ZXing.Datamatrix.Internal.BitMatrixParser::mappingBitMatrix
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * ___mappingBitMatrix_0;
	// ZXing.Common.BitMatrix ZXing.Datamatrix.Internal.BitMatrixParser::readMappingMatrix
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * ___readMappingMatrix_1;
	// ZXing.Datamatrix.Internal.Version ZXing.Datamatrix.Internal.BitMatrixParser::version
	Version_tD8C947A936B05965195D7C3BBA526285521EFD8F * ___version_2;

public:
	inline static int32_t get_offset_of_mappingBitMatrix_0() { return static_cast<int32_t>(offsetof(BitMatrixParser_tD8BA978B30ED653846ECDA9F103A20D754031AF6, ___mappingBitMatrix_0)); }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * get_mappingBitMatrix_0() const { return ___mappingBitMatrix_0; }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 ** get_address_of_mappingBitMatrix_0() { return &___mappingBitMatrix_0; }
	inline void set_mappingBitMatrix_0(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * value)
	{
		___mappingBitMatrix_0 = value;
		Il2CppCodeGenWriteBarrier((&___mappingBitMatrix_0), value);
	}

	inline static int32_t get_offset_of_readMappingMatrix_1() { return static_cast<int32_t>(offsetof(BitMatrixParser_tD8BA978B30ED653846ECDA9F103A20D754031AF6, ___readMappingMatrix_1)); }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * get_readMappingMatrix_1() const { return ___readMappingMatrix_1; }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 ** get_address_of_readMappingMatrix_1() { return &___readMappingMatrix_1; }
	inline void set_readMappingMatrix_1(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * value)
	{
		___readMappingMatrix_1 = value;
		Il2CppCodeGenWriteBarrier((&___readMappingMatrix_1), value);
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(BitMatrixParser_tD8BA978B30ED653846ECDA9F103A20D754031AF6, ___version_2)); }
	inline Version_tD8C947A936B05965195D7C3BBA526285521EFD8F * get_version_2() const { return ___version_2; }
	inline Version_tD8C947A936B05965195D7C3BBA526285521EFD8F ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(Version_tD8C947A936B05965195D7C3BBA526285521EFD8F * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITMATRIXPARSER_TD8BA978B30ED653846ECDA9F103A20D754031AF6_H
#ifndef DATABLOCK_TA871FAD1D9A6C41C1404AA867EB28924F36CD23B_H
#define DATABLOCK_TA871FAD1D9A6C41C1404AA867EB28924F36CD23B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.DataBlock
struct  DataBlock_tA871FAD1D9A6C41C1404AA867EB28924F36CD23B  : public RuntimeObject
{
public:
	// System.Int32 ZXing.Datamatrix.Internal.DataBlock::numDataCodewords
	int32_t ___numDataCodewords_0;
	// System.Byte[] ZXing.Datamatrix.Internal.DataBlock::codewords
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___codewords_1;

public:
	inline static int32_t get_offset_of_numDataCodewords_0() { return static_cast<int32_t>(offsetof(DataBlock_tA871FAD1D9A6C41C1404AA867EB28924F36CD23B, ___numDataCodewords_0)); }
	inline int32_t get_numDataCodewords_0() const { return ___numDataCodewords_0; }
	inline int32_t* get_address_of_numDataCodewords_0() { return &___numDataCodewords_0; }
	inline void set_numDataCodewords_0(int32_t value)
	{
		___numDataCodewords_0 = value;
	}

	inline static int32_t get_offset_of_codewords_1() { return static_cast<int32_t>(offsetof(DataBlock_tA871FAD1D9A6C41C1404AA867EB28924F36CD23B, ___codewords_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_codewords_1() const { return ___codewords_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_codewords_1() { return &___codewords_1; }
	inline void set_codewords_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___codewords_1 = value;
		Il2CppCodeGenWriteBarrier((&___codewords_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATABLOCK_TA871FAD1D9A6C41C1404AA867EB28924F36CD23B_H
#ifndef DECODEDBITSTREAMPARSER_T75E6F6A326EEDC72CCAF230558CE09829D7AA6A8_H
#define DECODEDBITSTREAMPARSER_T75E6F6A326EEDC72CCAF230558CE09829D7AA6A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.DecodedBitStreamParser
struct  DecodedBitStreamParser_t75E6F6A326EEDC72CCAF230558CE09829D7AA6A8  : public RuntimeObject
{
public:

public:
};

struct DecodedBitStreamParser_t75E6F6A326EEDC72CCAF230558CE09829D7AA6A8_StaticFields
{
public:
	// System.Char[] ZXing.Datamatrix.Internal.DecodedBitStreamParser::C40_BASIC_SET_CHARS
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___C40_BASIC_SET_CHARS_0;
	// System.Char[] ZXing.Datamatrix.Internal.DecodedBitStreamParser::C40_SHIFT2_SET_CHARS
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___C40_SHIFT2_SET_CHARS_1;
	// System.Char[] ZXing.Datamatrix.Internal.DecodedBitStreamParser::TEXT_BASIC_SET_CHARS
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___TEXT_BASIC_SET_CHARS_2;
	// System.Char[] ZXing.Datamatrix.Internal.DecodedBitStreamParser::TEXT_SHIFT2_SET_CHARS
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___TEXT_SHIFT2_SET_CHARS_3;
	// System.Char[] ZXing.Datamatrix.Internal.DecodedBitStreamParser::TEXT_SHIFT3_SET_CHARS
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___TEXT_SHIFT3_SET_CHARS_4;

public:
	inline static int32_t get_offset_of_C40_BASIC_SET_CHARS_0() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t75E6F6A326EEDC72CCAF230558CE09829D7AA6A8_StaticFields, ___C40_BASIC_SET_CHARS_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_C40_BASIC_SET_CHARS_0() const { return ___C40_BASIC_SET_CHARS_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_C40_BASIC_SET_CHARS_0() { return &___C40_BASIC_SET_CHARS_0; }
	inline void set_C40_BASIC_SET_CHARS_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___C40_BASIC_SET_CHARS_0 = value;
		Il2CppCodeGenWriteBarrier((&___C40_BASIC_SET_CHARS_0), value);
	}

	inline static int32_t get_offset_of_C40_SHIFT2_SET_CHARS_1() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t75E6F6A326EEDC72CCAF230558CE09829D7AA6A8_StaticFields, ___C40_SHIFT2_SET_CHARS_1)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_C40_SHIFT2_SET_CHARS_1() const { return ___C40_SHIFT2_SET_CHARS_1; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_C40_SHIFT2_SET_CHARS_1() { return &___C40_SHIFT2_SET_CHARS_1; }
	inline void set_C40_SHIFT2_SET_CHARS_1(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___C40_SHIFT2_SET_CHARS_1 = value;
		Il2CppCodeGenWriteBarrier((&___C40_SHIFT2_SET_CHARS_1), value);
	}

	inline static int32_t get_offset_of_TEXT_BASIC_SET_CHARS_2() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t75E6F6A326EEDC72CCAF230558CE09829D7AA6A8_StaticFields, ___TEXT_BASIC_SET_CHARS_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_TEXT_BASIC_SET_CHARS_2() const { return ___TEXT_BASIC_SET_CHARS_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_TEXT_BASIC_SET_CHARS_2() { return &___TEXT_BASIC_SET_CHARS_2; }
	inline void set_TEXT_BASIC_SET_CHARS_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___TEXT_BASIC_SET_CHARS_2 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_BASIC_SET_CHARS_2), value);
	}

	inline static int32_t get_offset_of_TEXT_SHIFT2_SET_CHARS_3() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t75E6F6A326EEDC72CCAF230558CE09829D7AA6A8_StaticFields, ___TEXT_SHIFT2_SET_CHARS_3)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_TEXT_SHIFT2_SET_CHARS_3() const { return ___TEXT_SHIFT2_SET_CHARS_3; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_TEXT_SHIFT2_SET_CHARS_3() { return &___TEXT_SHIFT2_SET_CHARS_3; }
	inline void set_TEXT_SHIFT2_SET_CHARS_3(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___TEXT_SHIFT2_SET_CHARS_3 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_SHIFT2_SET_CHARS_3), value);
	}

	inline static int32_t get_offset_of_TEXT_SHIFT3_SET_CHARS_4() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t75E6F6A326EEDC72CCAF230558CE09829D7AA6A8_StaticFields, ___TEXT_SHIFT3_SET_CHARS_4)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_TEXT_SHIFT3_SET_CHARS_4() const { return ___TEXT_SHIFT3_SET_CHARS_4; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_TEXT_SHIFT3_SET_CHARS_4() { return &___TEXT_SHIFT3_SET_CHARS_4; }
	inline void set_TEXT_SHIFT3_SET_CHARS_4(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___TEXT_SHIFT3_SET_CHARS_4 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_SHIFT3_SET_CHARS_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODEDBITSTREAMPARSER_T75E6F6A326EEDC72CCAF230558CE09829D7AA6A8_H
#ifndef DECODER_T0EFE53F08BE5D99F2C8ECDF5F42CF637F3A7CFBB_H
#define DECODER_T0EFE53F08BE5D99F2C8ECDF5F42CF637F3A7CFBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.Decoder
struct  Decoder_t0EFE53F08BE5D99F2C8ECDF5F42CF637F3A7CFBB  : public RuntimeObject
{
public:
	// ZXing.Common.ReedSolomon.ReedSolomonDecoder ZXing.Datamatrix.Internal.Decoder::rsDecoder
	ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427 * ___rsDecoder_0;

public:
	inline static int32_t get_offset_of_rsDecoder_0() { return static_cast<int32_t>(offsetof(Decoder_t0EFE53F08BE5D99F2C8ECDF5F42CF637F3A7CFBB, ___rsDecoder_0)); }
	inline ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427 * get_rsDecoder_0() const { return ___rsDecoder_0; }
	inline ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427 ** get_address_of_rsDecoder_0() { return &___rsDecoder_0; }
	inline void set_rsDecoder_0(ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427 * value)
	{
		___rsDecoder_0 = value;
		Il2CppCodeGenWriteBarrier((&___rsDecoder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODER_T0EFE53F08BE5D99F2C8ECDF5F42CF637F3A7CFBB_H
#ifndef DETECTOR_TAF46BB964D7C58CF3451CFA84F631507DB4EAA67_H
#define DETECTOR_TAF46BB964D7C58CF3451CFA84F631507DB4EAA67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.Detector
struct  Detector_tAF46BB964D7C58CF3451CFA84F631507DB4EAA67  : public RuntimeObject
{
public:
	// ZXing.Common.BitMatrix ZXing.Datamatrix.Internal.Detector::image
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * ___image_0;
	// ZXing.Common.Detector.WhiteRectangleDetector ZXing.Datamatrix.Internal.Detector::rectangleDetector
	WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4 * ___rectangleDetector_1;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(Detector_tAF46BB964D7C58CF3451CFA84F631507DB4EAA67, ___image_0)); }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * get_image_0() const { return ___image_0; }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier((&___image_0), value);
	}

	inline static int32_t get_offset_of_rectangleDetector_1() { return static_cast<int32_t>(offsetof(Detector_tAF46BB964D7C58CF3451CFA84F631507DB4EAA67, ___rectangleDetector_1)); }
	inline WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4 * get_rectangleDetector_1() const { return ___rectangleDetector_1; }
	inline WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4 ** get_address_of_rectangleDetector_1() { return &___rectangleDetector_1; }
	inline void set_rectangleDetector_1(WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4 * value)
	{
		___rectangleDetector_1 = value;
		Il2CppCodeGenWriteBarrier((&___rectangleDetector_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTOR_TAF46BB964D7C58CF3451CFA84F631507DB4EAA67_H
#ifndef RESULTPOINTSANDTRANSITIONS_TE4236428649FE431618C18325509B8E2DDBBC9C7_H
#define RESULTPOINTSANDTRANSITIONS_TE4236428649FE431618C18325509B8E2DDBBC9C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions
struct  ResultPointsAndTransitions_tE4236428649FE431618C18325509B8E2DDBBC9C7  : public RuntimeObject
{
public:
	// ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::<From>k__BackingField
	ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * ___U3CFromU3Ek__BackingField_0;
	// ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::<To>k__BackingField
	ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * ___U3CToU3Ek__BackingField_1;
	// System.Int32 ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::<Transitions>k__BackingField
	int32_t ___U3CTransitionsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CFromU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ResultPointsAndTransitions_tE4236428649FE431618C18325509B8E2DDBBC9C7, ___U3CFromU3Ek__BackingField_0)); }
	inline ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * get_U3CFromU3Ek__BackingField_0() const { return ___U3CFromU3Ek__BackingField_0; }
	inline ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 ** get_address_of_U3CFromU3Ek__BackingField_0() { return &___U3CFromU3Ek__BackingField_0; }
	inline void set_U3CFromU3Ek__BackingField_0(ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * value)
	{
		___U3CFromU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFromU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CToU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ResultPointsAndTransitions_tE4236428649FE431618C18325509B8E2DDBBC9C7, ___U3CToU3Ek__BackingField_1)); }
	inline ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * get_U3CToU3Ek__BackingField_1() const { return ___U3CToU3Ek__BackingField_1; }
	inline ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 ** get_address_of_U3CToU3Ek__BackingField_1() { return &___U3CToU3Ek__BackingField_1; }
	inline void set_U3CToU3Ek__BackingField_1(ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * value)
	{
		___U3CToU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CToU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTransitionsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ResultPointsAndTransitions_tE4236428649FE431618C18325509B8E2DDBBC9C7, ___U3CTransitionsU3Ek__BackingField_2)); }
	inline int32_t get_U3CTransitionsU3Ek__BackingField_2() const { return ___U3CTransitionsU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CTransitionsU3Ek__BackingField_2() { return &___U3CTransitionsU3Ek__BackingField_2; }
	inline void set_U3CTransitionsU3Ek__BackingField_2(int32_t value)
	{
		___U3CTransitionsU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTPOINTSANDTRANSITIONS_TE4236428649FE431618C18325509B8E2DDBBC9C7_H
#ifndef RESULTPOINTSANDTRANSITIONSCOMPARATOR_T4F9EA4ED899B67839A5E4C67655FFF4C5BF0D090_H
#define RESULTPOINTSANDTRANSITIONSCOMPARATOR_T4F9EA4ED899B67839A5E4C67655FFF4C5BF0D090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitionsComparator
struct  ResultPointsAndTransitionsComparator_t4F9EA4ED899B67839A5E4C67655FFF4C5BF0D090  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTPOINTSANDTRANSITIONSCOMPARATOR_T4F9EA4ED899B67839A5E4C67655FFF4C5BF0D090_H
#ifndef VERSION_TD8C947A936B05965195D7C3BBA526285521EFD8F_H
#define VERSION_TD8C947A936B05965195D7C3BBA526285521EFD8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.Version
struct  Version_tD8C947A936B05965195D7C3BBA526285521EFD8F  : public RuntimeObject
{
public:
	// System.Int32 ZXing.Datamatrix.Internal.Version::versionNumber
	int32_t ___versionNumber_1;
	// System.Int32 ZXing.Datamatrix.Internal.Version::symbolSizeRows
	int32_t ___symbolSizeRows_2;
	// System.Int32 ZXing.Datamatrix.Internal.Version::symbolSizeColumns
	int32_t ___symbolSizeColumns_3;
	// System.Int32 ZXing.Datamatrix.Internal.Version::dataRegionSizeRows
	int32_t ___dataRegionSizeRows_4;
	// System.Int32 ZXing.Datamatrix.Internal.Version::dataRegionSizeColumns
	int32_t ___dataRegionSizeColumns_5;
	// ZXing.Datamatrix.Internal.Version/ECBlocks ZXing.Datamatrix.Internal.Version::ecBlocks
	ECBlocks_t223B873D45E32B404C4228B45948B63FB39B7F19 * ___ecBlocks_6;
	// System.Int32 ZXing.Datamatrix.Internal.Version::totalCodewords
	int32_t ___totalCodewords_7;

public:
	inline static int32_t get_offset_of_versionNumber_1() { return static_cast<int32_t>(offsetof(Version_tD8C947A936B05965195D7C3BBA526285521EFD8F, ___versionNumber_1)); }
	inline int32_t get_versionNumber_1() const { return ___versionNumber_1; }
	inline int32_t* get_address_of_versionNumber_1() { return &___versionNumber_1; }
	inline void set_versionNumber_1(int32_t value)
	{
		___versionNumber_1 = value;
	}

	inline static int32_t get_offset_of_symbolSizeRows_2() { return static_cast<int32_t>(offsetof(Version_tD8C947A936B05965195D7C3BBA526285521EFD8F, ___symbolSizeRows_2)); }
	inline int32_t get_symbolSizeRows_2() const { return ___symbolSizeRows_2; }
	inline int32_t* get_address_of_symbolSizeRows_2() { return &___symbolSizeRows_2; }
	inline void set_symbolSizeRows_2(int32_t value)
	{
		___symbolSizeRows_2 = value;
	}

	inline static int32_t get_offset_of_symbolSizeColumns_3() { return static_cast<int32_t>(offsetof(Version_tD8C947A936B05965195D7C3BBA526285521EFD8F, ___symbolSizeColumns_3)); }
	inline int32_t get_symbolSizeColumns_3() const { return ___symbolSizeColumns_3; }
	inline int32_t* get_address_of_symbolSizeColumns_3() { return &___symbolSizeColumns_3; }
	inline void set_symbolSizeColumns_3(int32_t value)
	{
		___symbolSizeColumns_3 = value;
	}

	inline static int32_t get_offset_of_dataRegionSizeRows_4() { return static_cast<int32_t>(offsetof(Version_tD8C947A936B05965195D7C3BBA526285521EFD8F, ___dataRegionSizeRows_4)); }
	inline int32_t get_dataRegionSizeRows_4() const { return ___dataRegionSizeRows_4; }
	inline int32_t* get_address_of_dataRegionSizeRows_4() { return &___dataRegionSizeRows_4; }
	inline void set_dataRegionSizeRows_4(int32_t value)
	{
		___dataRegionSizeRows_4 = value;
	}

	inline static int32_t get_offset_of_dataRegionSizeColumns_5() { return static_cast<int32_t>(offsetof(Version_tD8C947A936B05965195D7C3BBA526285521EFD8F, ___dataRegionSizeColumns_5)); }
	inline int32_t get_dataRegionSizeColumns_5() const { return ___dataRegionSizeColumns_5; }
	inline int32_t* get_address_of_dataRegionSizeColumns_5() { return &___dataRegionSizeColumns_5; }
	inline void set_dataRegionSizeColumns_5(int32_t value)
	{
		___dataRegionSizeColumns_5 = value;
	}

	inline static int32_t get_offset_of_ecBlocks_6() { return static_cast<int32_t>(offsetof(Version_tD8C947A936B05965195D7C3BBA526285521EFD8F, ___ecBlocks_6)); }
	inline ECBlocks_t223B873D45E32B404C4228B45948B63FB39B7F19 * get_ecBlocks_6() const { return ___ecBlocks_6; }
	inline ECBlocks_t223B873D45E32B404C4228B45948B63FB39B7F19 ** get_address_of_ecBlocks_6() { return &___ecBlocks_6; }
	inline void set_ecBlocks_6(ECBlocks_t223B873D45E32B404C4228B45948B63FB39B7F19 * value)
	{
		___ecBlocks_6 = value;
		Il2CppCodeGenWriteBarrier((&___ecBlocks_6), value);
	}

	inline static int32_t get_offset_of_totalCodewords_7() { return static_cast<int32_t>(offsetof(Version_tD8C947A936B05965195D7C3BBA526285521EFD8F, ___totalCodewords_7)); }
	inline int32_t get_totalCodewords_7() const { return ___totalCodewords_7; }
	inline int32_t* get_address_of_totalCodewords_7() { return &___totalCodewords_7; }
	inline void set_totalCodewords_7(int32_t value)
	{
		___totalCodewords_7 = value;
	}
};

struct Version_tD8C947A936B05965195D7C3BBA526285521EFD8F_StaticFields
{
public:
	// ZXing.Datamatrix.Internal.Version[] ZXing.Datamatrix.Internal.Version::VERSIONS
	VersionU5BU5D_t3223B88809EAF0D3B0C9E68E9F2519F3A3E0128D* ___VERSIONS_0;

public:
	inline static int32_t get_offset_of_VERSIONS_0() { return static_cast<int32_t>(offsetof(Version_tD8C947A936B05965195D7C3BBA526285521EFD8F_StaticFields, ___VERSIONS_0)); }
	inline VersionU5BU5D_t3223B88809EAF0D3B0C9E68E9F2519F3A3E0128D* get_VERSIONS_0() const { return ___VERSIONS_0; }
	inline VersionU5BU5D_t3223B88809EAF0D3B0C9E68E9F2519F3A3E0128D** get_address_of_VERSIONS_0() { return &___VERSIONS_0; }
	inline void set_VERSIONS_0(VersionU5BU5D_t3223B88809EAF0D3B0C9E68E9F2519F3A3E0128D* value)
	{
		___VERSIONS_0 = value;
		Il2CppCodeGenWriteBarrier((&___VERSIONS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSION_TD8C947A936B05965195D7C3BBA526285521EFD8F_H
#ifndef ECB_T357B13B476DDA42A5E0E37AB50F17833282E6902_H
#define ECB_T357B13B476DDA42A5E0E37AB50F17833282E6902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.Version/ECB
struct  ECB_t357B13B476DDA42A5E0E37AB50F17833282E6902  : public RuntimeObject
{
public:
	// System.Int32 ZXing.Datamatrix.Internal.Version/ECB::count
	int32_t ___count_0;
	// System.Int32 ZXing.Datamatrix.Internal.Version/ECB::dataCodewords
	int32_t ___dataCodewords_1;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(ECB_t357B13B476DDA42A5E0E37AB50F17833282E6902, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_dataCodewords_1() { return static_cast<int32_t>(offsetof(ECB_t357B13B476DDA42A5E0E37AB50F17833282E6902, ___dataCodewords_1)); }
	inline int32_t get_dataCodewords_1() const { return ___dataCodewords_1; }
	inline int32_t* get_address_of_dataCodewords_1() { return &___dataCodewords_1; }
	inline void set_dataCodewords_1(int32_t value)
	{
		___dataCodewords_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECB_T357B13B476DDA42A5E0E37AB50F17833282E6902_H
#ifndef ECBLOCKS_T223B873D45E32B404C4228B45948B63FB39B7F19_H
#define ECBLOCKS_T223B873D45E32B404C4228B45948B63FB39B7F19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.Version/ECBlocks
struct  ECBlocks_t223B873D45E32B404C4228B45948B63FB39B7F19  : public RuntimeObject
{
public:
	// System.Int32 ZXing.Datamatrix.Internal.Version/ECBlocks::ecCodewords
	int32_t ___ecCodewords_0;
	// ZXing.Datamatrix.Internal.Version/ECB[] ZXing.Datamatrix.Internal.Version/ECBlocks::_ecBlocksValue
	ECBU5BU5D_tBEAFBB9238B452E011AAF47BDE0B874E285E4B40* ____ecBlocksValue_1;

public:
	inline static int32_t get_offset_of_ecCodewords_0() { return static_cast<int32_t>(offsetof(ECBlocks_t223B873D45E32B404C4228B45948B63FB39B7F19, ___ecCodewords_0)); }
	inline int32_t get_ecCodewords_0() const { return ___ecCodewords_0; }
	inline int32_t* get_address_of_ecCodewords_0() { return &___ecCodewords_0; }
	inline void set_ecCodewords_0(int32_t value)
	{
		___ecCodewords_0 = value;
	}

	inline static int32_t get_offset_of__ecBlocksValue_1() { return static_cast<int32_t>(offsetof(ECBlocks_t223B873D45E32B404C4228B45948B63FB39B7F19, ____ecBlocksValue_1)); }
	inline ECBU5BU5D_tBEAFBB9238B452E011AAF47BDE0B874E285E4B40* get__ecBlocksValue_1() const { return ____ecBlocksValue_1; }
	inline ECBU5BU5D_tBEAFBB9238B452E011AAF47BDE0B874E285E4B40** get_address_of__ecBlocksValue_1() { return &____ecBlocksValue_1; }
	inline void set__ecBlocksValue_1(ECBU5BU5D_tBEAFBB9238B452E011AAF47BDE0B874E285E4B40* value)
	{
		____ecBlocksValue_1 = value;
		Il2CppCodeGenWriteBarrier((&____ecBlocksValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECBLOCKS_T223B873D45E32B404C4228B45948B63FB39B7F19_H
#ifndef BITMATRIXPARSER_TDB34994B03A78BAA2BC3B717575CAA546A79A8C3_H
#define BITMATRIXPARSER_TDB34994B03A78BAA2BC3B717575CAA546A79A8C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Maxicode.Internal.BitMatrixParser
struct  BitMatrixParser_tDB34994B03A78BAA2BC3B717575CAA546A79A8C3  : public RuntimeObject
{
public:
	// ZXing.Common.BitMatrix ZXing.Maxicode.Internal.BitMatrixParser::bitMatrix
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * ___bitMatrix_1;

public:
	inline static int32_t get_offset_of_bitMatrix_1() { return static_cast<int32_t>(offsetof(BitMatrixParser_tDB34994B03A78BAA2BC3B717575CAA546A79A8C3, ___bitMatrix_1)); }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * get_bitMatrix_1() const { return ___bitMatrix_1; }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 ** get_address_of_bitMatrix_1() { return &___bitMatrix_1; }
	inline void set_bitMatrix_1(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * value)
	{
		___bitMatrix_1 = value;
		Il2CppCodeGenWriteBarrier((&___bitMatrix_1), value);
	}
};

struct BitMatrixParser_tDB34994B03A78BAA2BC3B717575CAA546A79A8C3_StaticFields
{
public:
	// System.Int32[][] ZXing.Maxicode.Internal.BitMatrixParser::BITNR
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___BITNR_0;

public:
	inline static int32_t get_offset_of_BITNR_0() { return static_cast<int32_t>(offsetof(BitMatrixParser_tDB34994B03A78BAA2BC3B717575CAA546A79A8C3_StaticFields, ___BITNR_0)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_BITNR_0() const { return ___BITNR_0; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_BITNR_0() { return &___BITNR_0; }
	inline void set_BITNR_0(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___BITNR_0 = value;
		Il2CppCodeGenWriteBarrier((&___BITNR_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITMATRIXPARSER_TDB34994B03A78BAA2BC3B717575CAA546A79A8C3_H
#ifndef DECODEDBITSTREAMPARSER_TE5085CF8BD40018339ED5FCB995819C27BB842AC_H
#define DECODEDBITSTREAMPARSER_TE5085CF8BD40018339ED5FCB995819C27BB842AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Maxicode.Internal.DecodedBitStreamParser
struct  DecodedBitStreamParser_tE5085CF8BD40018339ED5FCB995819C27BB842AC  : public RuntimeObject
{
public:

public:
};

struct DecodedBitStreamParser_tE5085CF8BD40018339ED5FCB995819C27BB842AC_StaticFields
{
public:
	// System.String[] ZXing.Maxicode.Internal.DecodedBitStreamParser::SETS
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___SETS_0;

public:
	inline static int32_t get_offset_of_SETS_0() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_tE5085CF8BD40018339ED5FCB995819C27BB842AC_StaticFields, ___SETS_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_SETS_0() const { return ___SETS_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_SETS_0() { return &___SETS_0; }
	inline void set_SETS_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___SETS_0 = value;
		Il2CppCodeGenWriteBarrier((&___SETS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODEDBITSTREAMPARSER_TE5085CF8BD40018339ED5FCB995819C27BB842AC_H
#ifndef DECODER_TDB4AB543D8B3BC1F37B79E1E08B9FA58A34EF8AC_H
#define DECODER_TDB4AB543D8B3BC1F37B79E1E08B9FA58A34EF8AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Maxicode.Internal.Decoder
struct  Decoder_tDB4AB543D8B3BC1F37B79E1E08B9FA58A34EF8AC  : public RuntimeObject
{
public:
	// ZXing.Common.ReedSolomon.ReedSolomonDecoder ZXing.Maxicode.Internal.Decoder::rsDecoder
	ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427 * ___rsDecoder_0;

public:
	inline static int32_t get_offset_of_rsDecoder_0() { return static_cast<int32_t>(offsetof(Decoder_tDB4AB543D8B3BC1F37B79E1E08B9FA58A34EF8AC, ___rsDecoder_0)); }
	inline ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427 * get_rsDecoder_0() const { return ___rsDecoder_0; }
	inline ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427 ** get_address_of_rsDecoder_0() { return &___rsDecoder_0; }
	inline void set_rsDecoder_0(ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427 * value)
	{
		___rsDecoder_0 = value;
		Il2CppCodeGenWriteBarrier((&___rsDecoder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODER_TDB4AB543D8B3BC1F37B79E1E08B9FA58A34EF8AC_H
#ifndef MAXICODEREADER_T5C701BBBD7748DAD7C518649C88DE5BCDF9EA74E_H
#define MAXICODEREADER_T5C701BBBD7748DAD7C518649C88DE5BCDF9EA74E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Maxicode.MaxiCodeReader
struct  MaxiCodeReader_t5C701BBBD7748DAD7C518649C88DE5BCDF9EA74E  : public RuntimeObject
{
public:
	// ZXing.Maxicode.Internal.Decoder ZXing.Maxicode.MaxiCodeReader::decoder
	Decoder_tDB4AB543D8B3BC1F37B79E1E08B9FA58A34EF8AC * ___decoder_1;

public:
	inline static int32_t get_offset_of_decoder_1() { return static_cast<int32_t>(offsetof(MaxiCodeReader_t5C701BBBD7748DAD7C518649C88DE5BCDF9EA74E, ___decoder_1)); }
	inline Decoder_tDB4AB543D8B3BC1F37B79E1E08B9FA58A34EF8AC * get_decoder_1() const { return ___decoder_1; }
	inline Decoder_tDB4AB543D8B3BC1F37B79E1E08B9FA58A34EF8AC ** get_address_of_decoder_1() { return &___decoder_1; }
	inline void set_decoder_1(Decoder_tDB4AB543D8B3BC1F37B79E1E08B9FA58A34EF8AC * value)
	{
		___decoder_1 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_1), value);
	}
};

struct MaxiCodeReader_t5C701BBBD7748DAD7C518649C88DE5BCDF9EA74E_StaticFields
{
public:
	// ZXing.ResultPoint[] ZXing.Maxicode.MaxiCodeReader::NO_POINTS
	ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* ___NO_POINTS_0;

public:
	inline static int32_t get_offset_of_NO_POINTS_0() { return static_cast<int32_t>(offsetof(MaxiCodeReader_t5C701BBBD7748DAD7C518649C88DE5BCDF9EA74E_StaticFields, ___NO_POINTS_0)); }
	inline ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* get_NO_POINTS_0() const { return ___NO_POINTS_0; }
	inline ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C** get_address_of_NO_POINTS_0() { return &___NO_POINTS_0; }
	inline void set_NO_POINTS_0(ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* value)
	{
		___NO_POINTS_0 = value;
		Il2CppCodeGenWriteBarrier((&___NO_POINTS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAXICODEREADER_T5C701BBBD7748DAD7C518649C88DE5BCDF9EA74E_H
#ifndef EANMANUFACTURERORGSUPPORT_T1BFBF3CDCC49219358CD56548F8FD730DD2C35A7_H
#define EANMANUFACTURERORGSUPPORT_T1BFBF3CDCC49219358CD56548F8FD730DD2C35A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.EANManufacturerOrgSupport
struct  EANManufacturerOrgSupport_t1BFBF3CDCC49219358CD56548F8FD730DD2C35A7  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Int32[]> ZXing.OneD.EANManufacturerOrgSupport::ranges
	List_1_t0326783A2D4214C62E41A4FD97968B991077A79B * ___ranges_0;
	// System.Collections.Generic.List`1<System.String> ZXing.OneD.EANManufacturerOrgSupport::countryIdentifiers
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___countryIdentifiers_1;

public:
	inline static int32_t get_offset_of_ranges_0() { return static_cast<int32_t>(offsetof(EANManufacturerOrgSupport_t1BFBF3CDCC49219358CD56548F8FD730DD2C35A7, ___ranges_0)); }
	inline List_1_t0326783A2D4214C62E41A4FD97968B991077A79B * get_ranges_0() const { return ___ranges_0; }
	inline List_1_t0326783A2D4214C62E41A4FD97968B991077A79B ** get_address_of_ranges_0() { return &___ranges_0; }
	inline void set_ranges_0(List_1_t0326783A2D4214C62E41A4FD97968B991077A79B * value)
	{
		___ranges_0 = value;
		Il2CppCodeGenWriteBarrier((&___ranges_0), value);
	}

	inline static int32_t get_offset_of_countryIdentifiers_1() { return static_cast<int32_t>(offsetof(EANManufacturerOrgSupport_t1BFBF3CDCC49219358CD56548F8FD730DD2C35A7, ___countryIdentifiers_1)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_countryIdentifiers_1() const { return ___countryIdentifiers_1; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_countryIdentifiers_1() { return &___countryIdentifiers_1; }
	inline void set_countryIdentifiers_1(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___countryIdentifiers_1 = value;
		Il2CppCodeGenWriteBarrier((&___countryIdentifiers_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EANMANUFACTURERORGSUPPORT_T1BFBF3CDCC49219358CD56548F8FD730DD2C35A7_H
#ifndef ONEDREADER_TBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC_H
#define ONEDREADER_TBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.OneDReader
struct  OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC  : public RuntimeObject
{
public:

public:
};

struct OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC_StaticFields
{
public:
	// System.Int32 ZXing.OneD.OneDReader::INTEGER_MATH_SHIFT
	int32_t ___INTEGER_MATH_SHIFT_0;
	// System.Int32 ZXing.OneD.OneDReader::PATTERN_MATCH_RESULT_SCALE_FACTOR
	int32_t ___PATTERN_MATCH_RESULT_SCALE_FACTOR_1;

public:
	inline static int32_t get_offset_of_INTEGER_MATH_SHIFT_0() { return static_cast<int32_t>(offsetof(OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC_StaticFields, ___INTEGER_MATH_SHIFT_0)); }
	inline int32_t get_INTEGER_MATH_SHIFT_0() const { return ___INTEGER_MATH_SHIFT_0; }
	inline int32_t* get_address_of_INTEGER_MATH_SHIFT_0() { return &___INTEGER_MATH_SHIFT_0; }
	inline void set_INTEGER_MATH_SHIFT_0(int32_t value)
	{
		___INTEGER_MATH_SHIFT_0 = value;
	}

	inline static int32_t get_offset_of_PATTERN_MATCH_RESULT_SCALE_FACTOR_1() { return static_cast<int32_t>(offsetof(OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC_StaticFields, ___PATTERN_MATCH_RESULT_SCALE_FACTOR_1)); }
	inline int32_t get_PATTERN_MATCH_RESULT_SCALE_FACTOR_1() const { return ___PATTERN_MATCH_RESULT_SCALE_FACTOR_1; }
	inline int32_t* get_address_of_PATTERN_MATCH_RESULT_SCALE_FACTOR_1() { return &___PATTERN_MATCH_RESULT_SCALE_FACTOR_1; }
	inline void set_PATTERN_MATCH_RESULT_SCALE_FACTOR_1(int32_t value)
	{
		___PATTERN_MATCH_RESULT_SCALE_FACTOR_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONEDREADER_TBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC_H
#ifndef DATACHARACTER_T91EF692DAFCC9E507CF376A4710967C52ABD5EBB_H
#define DATACHARACTER_T91EF692DAFCC9E507CF376A4710967C52ABD5EBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.DataCharacter
struct  DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB  : public RuntimeObject
{
public:
	// System.Int32 ZXing.OneD.RSS.DataCharacter::<Value>k__BackingField
	int32_t ___U3CValueU3Ek__BackingField_0;
	// System.Int32 ZXing.OneD.RSS.DataCharacter::<ChecksumPortion>k__BackingField
	int32_t ___U3CChecksumPortionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB, ___U3CValueU3Ek__BackingField_0)); }
	inline int32_t get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(int32_t value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CChecksumPortionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB, ___U3CChecksumPortionU3Ek__BackingField_1)); }
	inline int32_t get_U3CChecksumPortionU3Ek__BackingField_1() const { return ___U3CChecksumPortionU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CChecksumPortionU3Ek__BackingField_1() { return &___U3CChecksumPortionU3Ek__BackingField_1; }
	inline void set_U3CChecksumPortionU3Ek__BackingField_1(int32_t value)
	{
		___U3CChecksumPortionU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACHARACTER_T91EF692DAFCC9E507CF376A4710967C52ABD5EBB_H
#ifndef BITARRAYBUILDER_T8CBD8072A1AD84DCD163866A738492629130FA7C_H
#define BITARRAYBUILDER_T8CBD8072A1AD84DCD163866A738492629130FA7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.BitArrayBuilder
struct  BitArrayBuilder_t8CBD8072A1AD84DCD163866A738492629130FA7C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITARRAYBUILDER_T8CBD8072A1AD84DCD163866A738492629130FA7C_H
#ifndef ABSTRACTEXPANDEDDECODER_T641084A96338858401AFFF4BFCFEFAD0E5F014E6_H
#define ABSTRACTEXPANDEDDECODER_T641084A96338858401AFFF4BFCFEFAD0E5F014E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder
struct  AbstractExpandedDecoder_t641084A96338858401AFFF4BFCFEFAD0E5F014E6  : public RuntimeObject
{
public:
	// ZXing.Common.BitArray ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::information
	BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869 * ___information_0;
	// ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::generalDecoder
	GeneralAppIdDecoder_t19AB438FE36F080A3109E1115E92D2E7BE988077 * ___generalDecoder_1;

public:
	inline static int32_t get_offset_of_information_0() { return static_cast<int32_t>(offsetof(AbstractExpandedDecoder_t641084A96338858401AFFF4BFCFEFAD0E5F014E6, ___information_0)); }
	inline BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869 * get_information_0() const { return ___information_0; }
	inline BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869 ** get_address_of_information_0() { return &___information_0; }
	inline void set_information_0(BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869 * value)
	{
		___information_0 = value;
		Il2CppCodeGenWriteBarrier((&___information_0), value);
	}

	inline static int32_t get_offset_of_generalDecoder_1() { return static_cast<int32_t>(offsetof(AbstractExpandedDecoder_t641084A96338858401AFFF4BFCFEFAD0E5F014E6, ___generalDecoder_1)); }
	inline GeneralAppIdDecoder_t19AB438FE36F080A3109E1115E92D2E7BE988077 * get_generalDecoder_1() const { return ___generalDecoder_1; }
	inline GeneralAppIdDecoder_t19AB438FE36F080A3109E1115E92D2E7BE988077 ** get_address_of_generalDecoder_1() { return &___generalDecoder_1; }
	inline void set_generalDecoder_1(GeneralAppIdDecoder_t19AB438FE36F080A3109E1115E92D2E7BE988077 * value)
	{
		___generalDecoder_1 = value;
		Il2CppCodeGenWriteBarrier((&___generalDecoder_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEXPANDEDDECODER_T641084A96338858401AFFF4BFCFEFAD0E5F014E6_H
#ifndef BLOCKPARSEDRESULT_T14D7939BAE1374EBFC72FAC7E26487B5C4B51057_H
#define BLOCKPARSEDRESULT_T14D7939BAE1374EBFC72FAC7E26487B5C4B51057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult
struct  BlockParsedResult_t14D7939BAE1374EBFC72FAC7E26487B5C4B51057  : public RuntimeObject
{
public:
	// ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::decodedInformation
	DecodedInformation_tD144F663AE3C49E43CF53AEA9027371BC3D8F04A * ___decodedInformation_0;
	// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::finished
	bool ___finished_1;

public:
	inline static int32_t get_offset_of_decodedInformation_0() { return static_cast<int32_t>(offsetof(BlockParsedResult_t14D7939BAE1374EBFC72FAC7E26487B5C4B51057, ___decodedInformation_0)); }
	inline DecodedInformation_tD144F663AE3C49E43CF53AEA9027371BC3D8F04A * get_decodedInformation_0() const { return ___decodedInformation_0; }
	inline DecodedInformation_tD144F663AE3C49E43CF53AEA9027371BC3D8F04A ** get_address_of_decodedInformation_0() { return &___decodedInformation_0; }
	inline void set_decodedInformation_0(DecodedInformation_tD144F663AE3C49E43CF53AEA9027371BC3D8F04A * value)
	{
		___decodedInformation_0 = value;
		Il2CppCodeGenWriteBarrier((&___decodedInformation_0), value);
	}

	inline static int32_t get_offset_of_finished_1() { return static_cast<int32_t>(offsetof(BlockParsedResult_t14D7939BAE1374EBFC72FAC7E26487B5C4B51057, ___finished_1)); }
	inline bool get_finished_1() const { return ___finished_1; }
	inline bool* get_address_of_finished_1() { return &___finished_1; }
	inline void set_finished_1(bool value)
	{
		___finished_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKPARSEDRESULT_T14D7939BAE1374EBFC72FAC7E26487B5C4B51057_H
#ifndef DECODEDOBJECT_T57BA4BF95F496A5C794D66D54123FC45B10CAD9F_H
#define DECODEDOBJECT_T57BA4BF95F496A5C794D66D54123FC45B10CAD9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.DecodedObject
struct  DecodedObject_t57BA4BF95F496A5C794D66D54123FC45B10CAD9F  : public RuntimeObject
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedObject::<NewPosition>k__BackingField
	int32_t ___U3CNewPositionU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNewPositionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DecodedObject_t57BA4BF95F496A5C794D66D54123FC45B10CAD9F, ___U3CNewPositionU3Ek__BackingField_0)); }
	inline int32_t get_U3CNewPositionU3Ek__BackingField_0() const { return ___U3CNewPositionU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CNewPositionU3Ek__BackingField_0() { return &___U3CNewPositionU3Ek__BackingField_0; }
	inline void set_U3CNewPositionU3Ek__BackingField_0(int32_t value)
	{
		___U3CNewPositionU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODEDOBJECT_T57BA4BF95F496A5C794D66D54123FC45B10CAD9F_H
#ifndef FIELDPARSER_T3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5_H
#define FIELDPARSER_T3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.FieldParser
struct  FieldParser_t3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5  : public RuntimeObject
{
public:

public:
};

struct FieldParser_t3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5_StaticFields
{
public:
	// System.Object ZXing.OneD.RSS.Expanded.Decoders.FieldParser::VARIABLE_LENGTH
	RuntimeObject * ___VARIABLE_LENGTH_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object[]> ZXing.OneD.RSS.Expanded.Decoders.FieldParser::TWO_DIGIT_DATA_LENGTH
	RuntimeObject* ___TWO_DIGIT_DATA_LENGTH_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object[]> ZXing.OneD.RSS.Expanded.Decoders.FieldParser::THREE_DIGIT_DATA_LENGTH
	RuntimeObject* ___THREE_DIGIT_DATA_LENGTH_2;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object[]> ZXing.OneD.RSS.Expanded.Decoders.FieldParser::THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH
	RuntimeObject* ___THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object[]> ZXing.OneD.RSS.Expanded.Decoders.FieldParser::FOUR_DIGIT_DATA_LENGTH
	RuntimeObject* ___FOUR_DIGIT_DATA_LENGTH_4;

public:
	inline static int32_t get_offset_of_VARIABLE_LENGTH_0() { return static_cast<int32_t>(offsetof(FieldParser_t3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5_StaticFields, ___VARIABLE_LENGTH_0)); }
	inline RuntimeObject * get_VARIABLE_LENGTH_0() const { return ___VARIABLE_LENGTH_0; }
	inline RuntimeObject ** get_address_of_VARIABLE_LENGTH_0() { return &___VARIABLE_LENGTH_0; }
	inline void set_VARIABLE_LENGTH_0(RuntimeObject * value)
	{
		___VARIABLE_LENGTH_0 = value;
		Il2CppCodeGenWriteBarrier((&___VARIABLE_LENGTH_0), value);
	}

	inline static int32_t get_offset_of_TWO_DIGIT_DATA_LENGTH_1() { return static_cast<int32_t>(offsetof(FieldParser_t3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5_StaticFields, ___TWO_DIGIT_DATA_LENGTH_1)); }
	inline RuntimeObject* get_TWO_DIGIT_DATA_LENGTH_1() const { return ___TWO_DIGIT_DATA_LENGTH_1; }
	inline RuntimeObject** get_address_of_TWO_DIGIT_DATA_LENGTH_1() { return &___TWO_DIGIT_DATA_LENGTH_1; }
	inline void set_TWO_DIGIT_DATA_LENGTH_1(RuntimeObject* value)
	{
		___TWO_DIGIT_DATA_LENGTH_1 = value;
		Il2CppCodeGenWriteBarrier((&___TWO_DIGIT_DATA_LENGTH_1), value);
	}

	inline static int32_t get_offset_of_THREE_DIGIT_DATA_LENGTH_2() { return static_cast<int32_t>(offsetof(FieldParser_t3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5_StaticFields, ___THREE_DIGIT_DATA_LENGTH_2)); }
	inline RuntimeObject* get_THREE_DIGIT_DATA_LENGTH_2() const { return ___THREE_DIGIT_DATA_LENGTH_2; }
	inline RuntimeObject** get_address_of_THREE_DIGIT_DATA_LENGTH_2() { return &___THREE_DIGIT_DATA_LENGTH_2; }
	inline void set_THREE_DIGIT_DATA_LENGTH_2(RuntimeObject* value)
	{
		___THREE_DIGIT_DATA_LENGTH_2 = value;
		Il2CppCodeGenWriteBarrier((&___THREE_DIGIT_DATA_LENGTH_2), value);
	}

	inline static int32_t get_offset_of_THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3() { return static_cast<int32_t>(offsetof(FieldParser_t3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5_StaticFields, ___THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3)); }
	inline RuntimeObject* get_THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3() const { return ___THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3; }
	inline RuntimeObject** get_address_of_THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3() { return &___THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3; }
	inline void set_THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3(RuntimeObject* value)
	{
		___THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3 = value;
		Il2CppCodeGenWriteBarrier((&___THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3), value);
	}

	inline static int32_t get_offset_of_FOUR_DIGIT_DATA_LENGTH_4() { return static_cast<int32_t>(offsetof(FieldParser_t3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5_StaticFields, ___FOUR_DIGIT_DATA_LENGTH_4)); }
	inline RuntimeObject* get_FOUR_DIGIT_DATA_LENGTH_4() const { return ___FOUR_DIGIT_DATA_LENGTH_4; }
	inline RuntimeObject** get_address_of_FOUR_DIGIT_DATA_LENGTH_4() { return &___FOUR_DIGIT_DATA_LENGTH_4; }
	inline void set_FOUR_DIGIT_DATA_LENGTH_4(RuntimeObject* value)
	{
		___FOUR_DIGIT_DATA_LENGTH_4 = value;
		Il2CppCodeGenWriteBarrier((&___FOUR_DIGIT_DATA_LENGTH_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDPARSER_T3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5_H
#ifndef GENERALAPPIDDECODER_T19AB438FE36F080A3109E1115E92D2E7BE988077_H
#define GENERALAPPIDDECODER_T19AB438FE36F080A3109E1115E92D2E7BE988077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder
struct  GeneralAppIdDecoder_t19AB438FE36F080A3109E1115E92D2E7BE988077  : public RuntimeObject
{
public:
	// ZXing.Common.BitArray ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::information
	BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869 * ___information_0;
	// ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::current
	CurrentParsingState_t69D94A0BACE62CD40F623625D2A86AF7EFF70BC1 * ___current_1;
	// System.Text.StringBuilder ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::buffer
	StringBuilder_t * ___buffer_2;

public:
	inline static int32_t get_offset_of_information_0() { return static_cast<int32_t>(offsetof(GeneralAppIdDecoder_t19AB438FE36F080A3109E1115E92D2E7BE988077, ___information_0)); }
	inline BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869 * get_information_0() const { return ___information_0; }
	inline BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869 ** get_address_of_information_0() { return &___information_0; }
	inline void set_information_0(BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869 * value)
	{
		___information_0 = value;
		Il2CppCodeGenWriteBarrier((&___information_0), value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(GeneralAppIdDecoder_t19AB438FE36F080A3109E1115E92D2E7BE988077, ___current_1)); }
	inline CurrentParsingState_t69D94A0BACE62CD40F623625D2A86AF7EFF70BC1 * get_current_1() const { return ___current_1; }
	inline CurrentParsingState_t69D94A0BACE62CD40F623625D2A86AF7EFF70BC1 ** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(CurrentParsingState_t69D94A0BACE62CD40F623625D2A86AF7EFF70BC1 * value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier((&___current_1), value);
	}

	inline static int32_t get_offset_of_buffer_2() { return static_cast<int32_t>(offsetof(GeneralAppIdDecoder_t19AB438FE36F080A3109E1115E92D2E7BE988077, ___buffer_2)); }
	inline StringBuilder_t * get_buffer_2() const { return ___buffer_2; }
	inline StringBuilder_t ** get_address_of_buffer_2() { return &___buffer_2; }
	inline void set_buffer_2(StringBuilder_t * value)
	{
		___buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALAPPIDDECODER_T19AB438FE36F080A3109E1115E92D2E7BE988077_H
#ifndef EXPANDEDPAIR_TE17CFA34DD6966D8F3033E6661DD541E35C02712_H
#define EXPANDEDPAIR_TE17CFA34DD6966D8F3033E6661DD541E35C02712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.ExpandedPair
struct  ExpandedPair_tE17CFA34DD6966D8F3033E6661DD541E35C02712  : public RuntimeObject
{
public:
	// System.Boolean ZXing.OneD.RSS.Expanded.ExpandedPair::<MayBeLast>k__BackingField
	bool ___U3CMayBeLastU3Ek__BackingField_0;
	// ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.Expanded.ExpandedPair::<LeftChar>k__BackingField
	DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB * ___U3CLeftCharU3Ek__BackingField_1;
	// ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.Expanded.ExpandedPair::<RightChar>k__BackingField
	DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB * ___U3CRightCharU3Ek__BackingField_2;
	// ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.Expanded.ExpandedPair::<FinderPattern>k__BackingField
	FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF * ___U3CFinderPatternU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CMayBeLastU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ExpandedPair_tE17CFA34DD6966D8F3033E6661DD541E35C02712, ___U3CMayBeLastU3Ek__BackingField_0)); }
	inline bool get_U3CMayBeLastU3Ek__BackingField_0() const { return ___U3CMayBeLastU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CMayBeLastU3Ek__BackingField_0() { return &___U3CMayBeLastU3Ek__BackingField_0; }
	inline void set_U3CMayBeLastU3Ek__BackingField_0(bool value)
	{
		___U3CMayBeLastU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CLeftCharU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExpandedPair_tE17CFA34DD6966D8F3033E6661DD541E35C02712, ___U3CLeftCharU3Ek__BackingField_1)); }
	inline DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB * get_U3CLeftCharU3Ek__BackingField_1() const { return ___U3CLeftCharU3Ek__BackingField_1; }
	inline DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB ** get_address_of_U3CLeftCharU3Ek__BackingField_1() { return &___U3CLeftCharU3Ek__BackingField_1; }
	inline void set_U3CLeftCharU3Ek__BackingField_1(DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB * value)
	{
		___U3CLeftCharU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLeftCharU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CRightCharU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ExpandedPair_tE17CFA34DD6966D8F3033E6661DD541E35C02712, ___U3CRightCharU3Ek__BackingField_2)); }
	inline DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB * get_U3CRightCharU3Ek__BackingField_2() const { return ___U3CRightCharU3Ek__BackingField_2; }
	inline DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB ** get_address_of_U3CRightCharU3Ek__BackingField_2() { return &___U3CRightCharU3Ek__BackingField_2; }
	inline void set_U3CRightCharU3Ek__BackingField_2(DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB * value)
	{
		___U3CRightCharU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRightCharU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CFinderPatternU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ExpandedPair_tE17CFA34DD6966D8F3033E6661DD541E35C02712, ___U3CFinderPatternU3Ek__BackingField_3)); }
	inline FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF * get_U3CFinderPatternU3Ek__BackingField_3() const { return ___U3CFinderPatternU3Ek__BackingField_3; }
	inline FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF ** get_address_of_U3CFinderPatternU3Ek__BackingField_3() { return &___U3CFinderPatternU3Ek__BackingField_3; }
	inline void set_U3CFinderPatternU3Ek__BackingField_3(FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF * value)
	{
		___U3CFinderPatternU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFinderPatternU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPANDEDPAIR_TE17CFA34DD6966D8F3033E6661DD541E35C02712_H
#ifndef EXPANDEDROW_T7C0073CB9F8326FD736529D22D8849C9287F1714_H
#define EXPANDEDROW_T7C0073CB9F8326FD736529D22D8849C9287F1714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.ExpandedRow
struct  ExpandedRow_t7C0073CB9F8326FD736529D22D8849C9287F1714  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair> ZXing.OneD.RSS.Expanded.ExpandedRow::<Pairs>k__BackingField
	List_1_t8968621E959ACCB19941483BDCBBB6443FC5E673 * ___U3CPairsU3Ek__BackingField_0;
	// System.Int32 ZXing.OneD.RSS.Expanded.ExpandedRow::<RowNumber>k__BackingField
	int32_t ___U3CRowNumberU3Ek__BackingField_1;
	// System.Boolean ZXing.OneD.RSS.Expanded.ExpandedRow::<IsReversed>k__BackingField
	bool ___U3CIsReversedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CPairsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ExpandedRow_t7C0073CB9F8326FD736529D22D8849C9287F1714, ___U3CPairsU3Ek__BackingField_0)); }
	inline List_1_t8968621E959ACCB19941483BDCBBB6443FC5E673 * get_U3CPairsU3Ek__BackingField_0() const { return ___U3CPairsU3Ek__BackingField_0; }
	inline List_1_t8968621E959ACCB19941483BDCBBB6443FC5E673 ** get_address_of_U3CPairsU3Ek__BackingField_0() { return &___U3CPairsU3Ek__BackingField_0; }
	inline void set_U3CPairsU3Ek__BackingField_0(List_1_t8968621E959ACCB19941483BDCBBB6443FC5E673 * value)
	{
		___U3CPairsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPairsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CRowNumberU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExpandedRow_t7C0073CB9F8326FD736529D22D8849C9287F1714, ___U3CRowNumberU3Ek__BackingField_1)); }
	inline int32_t get_U3CRowNumberU3Ek__BackingField_1() const { return ___U3CRowNumberU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CRowNumberU3Ek__BackingField_1() { return &___U3CRowNumberU3Ek__BackingField_1; }
	inline void set_U3CRowNumberU3Ek__BackingField_1(int32_t value)
	{
		___U3CRowNumberU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CIsReversedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ExpandedRow_t7C0073CB9F8326FD736529D22D8849C9287F1714, ___U3CIsReversedU3Ek__BackingField_2)); }
	inline bool get_U3CIsReversedU3Ek__BackingField_2() const { return ___U3CIsReversedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsReversedU3Ek__BackingField_2() { return &___U3CIsReversedU3Ek__BackingField_2; }
	inline void set_U3CIsReversedU3Ek__BackingField_2(bool value)
	{
		___U3CIsReversedU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPANDEDROW_T7C0073CB9F8326FD736529D22D8849C9287F1714_H
#ifndef FINDERPATTERN_T0C8016402A6CBE864FBD43910A4D7925570E3CAF_H
#define FINDERPATTERN_T0C8016402A6CBE864FBD43910A4D7925570E3CAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.FinderPattern
struct  FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF  : public RuntimeObject
{
public:
	// System.Int32 ZXing.OneD.RSS.FinderPattern::<Value>k__BackingField
	int32_t ___U3CValueU3Ek__BackingField_0;
	// System.Int32[] ZXing.OneD.RSS.FinderPattern::<StartEnd>k__BackingField
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___U3CStartEndU3Ek__BackingField_1;
	// ZXing.ResultPoint[] ZXing.OneD.RSS.FinderPattern::<ResultPoints>k__BackingField
	ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* ___U3CResultPointsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF, ___U3CValueU3Ek__BackingField_0)); }
	inline int32_t get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(int32_t value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStartEndU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF, ___U3CStartEndU3Ek__BackingField_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_U3CStartEndU3Ek__BackingField_1() const { return ___U3CStartEndU3Ek__BackingField_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_U3CStartEndU3Ek__BackingField_1() { return &___U3CStartEndU3Ek__BackingField_1; }
	inline void set_U3CStartEndU3Ek__BackingField_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___U3CStartEndU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStartEndU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CResultPointsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF, ___U3CResultPointsU3Ek__BackingField_2)); }
	inline ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* get_U3CResultPointsU3Ek__BackingField_2() const { return ___U3CResultPointsU3Ek__BackingField_2; }
	inline ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C** get_address_of_U3CResultPointsU3Ek__BackingField_2() { return &___U3CResultPointsU3Ek__BackingField_2; }
	inline void set_U3CResultPointsU3Ek__BackingField_2(ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* value)
	{
		___U3CResultPointsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultPointsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDERPATTERN_T0C8016402A6CBE864FBD43910A4D7925570E3CAF_H
#ifndef RSSUTILS_T9785CB76FAF54ABA19C14EED7360F25A1BC26CDC_H
#define RSSUTILS_T9785CB76FAF54ABA19C14EED7360F25A1BC26CDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.RSSUtils
struct  RSSUtils_t9785CB76FAF54ABA19C14EED7360F25A1BC26CDC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSSUTILS_T9785CB76FAF54ABA19C14EED7360F25A1BC26CDC_H
#ifndef UPCEANEXTENSION2SUPPORT_T1FAB0D3ED96CEF129DD7BECBF9DCD16C1EEEB7CA_H
#define UPCEANEXTENSION2SUPPORT_T1FAB0D3ED96CEF129DD7BECBF9DCD16C1EEEB7CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.UPCEANExtension2Support
struct  UPCEANExtension2Support_t1FAB0D3ED96CEF129DD7BECBF9DCD16C1EEEB7CA  : public RuntimeObject
{
public:
	// System.Int32[] ZXing.OneD.UPCEANExtension2Support::decodeMiddleCounters
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___decodeMiddleCounters_0;
	// System.Text.StringBuilder ZXing.OneD.UPCEANExtension2Support::decodeRowStringBuffer
	StringBuilder_t * ___decodeRowStringBuffer_1;

public:
	inline static int32_t get_offset_of_decodeMiddleCounters_0() { return static_cast<int32_t>(offsetof(UPCEANExtension2Support_t1FAB0D3ED96CEF129DD7BECBF9DCD16C1EEEB7CA, ___decodeMiddleCounters_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_decodeMiddleCounters_0() const { return ___decodeMiddleCounters_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_decodeMiddleCounters_0() { return &___decodeMiddleCounters_0; }
	inline void set_decodeMiddleCounters_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___decodeMiddleCounters_0 = value;
		Il2CppCodeGenWriteBarrier((&___decodeMiddleCounters_0), value);
	}

	inline static int32_t get_offset_of_decodeRowStringBuffer_1() { return static_cast<int32_t>(offsetof(UPCEANExtension2Support_t1FAB0D3ED96CEF129DD7BECBF9DCD16C1EEEB7CA, ___decodeRowStringBuffer_1)); }
	inline StringBuilder_t * get_decodeRowStringBuffer_1() const { return ___decodeRowStringBuffer_1; }
	inline StringBuilder_t ** get_address_of_decodeRowStringBuffer_1() { return &___decodeRowStringBuffer_1; }
	inline void set_decodeRowStringBuffer_1(StringBuilder_t * value)
	{
		___decodeRowStringBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___decodeRowStringBuffer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPCEANEXTENSION2SUPPORT_T1FAB0D3ED96CEF129DD7BECBF9DCD16C1EEEB7CA_H
#ifndef UPCEANEXTENSION5SUPPORT_TAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD_H
#define UPCEANEXTENSION5SUPPORT_TAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.UPCEANExtension5Support
struct  UPCEANExtension5Support_tAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD  : public RuntimeObject
{
public:
	// System.Int32[] ZXing.OneD.UPCEANExtension5Support::decodeMiddleCounters
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___decodeMiddleCounters_1;
	// System.Text.StringBuilder ZXing.OneD.UPCEANExtension5Support::decodeRowStringBuffer
	StringBuilder_t * ___decodeRowStringBuffer_2;

public:
	inline static int32_t get_offset_of_decodeMiddleCounters_1() { return static_cast<int32_t>(offsetof(UPCEANExtension5Support_tAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD, ___decodeMiddleCounters_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_decodeMiddleCounters_1() const { return ___decodeMiddleCounters_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_decodeMiddleCounters_1() { return &___decodeMiddleCounters_1; }
	inline void set_decodeMiddleCounters_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___decodeMiddleCounters_1 = value;
		Il2CppCodeGenWriteBarrier((&___decodeMiddleCounters_1), value);
	}

	inline static int32_t get_offset_of_decodeRowStringBuffer_2() { return static_cast<int32_t>(offsetof(UPCEANExtension5Support_tAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD, ___decodeRowStringBuffer_2)); }
	inline StringBuilder_t * get_decodeRowStringBuffer_2() const { return ___decodeRowStringBuffer_2; }
	inline StringBuilder_t ** get_address_of_decodeRowStringBuffer_2() { return &___decodeRowStringBuffer_2; }
	inline void set_decodeRowStringBuffer_2(StringBuilder_t * value)
	{
		___decodeRowStringBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___decodeRowStringBuffer_2), value);
	}
};

struct UPCEANExtension5Support_tAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD_StaticFields
{
public:
	// System.Int32[] ZXing.OneD.UPCEANExtension5Support::CHECK_DIGIT_ENCODINGS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___CHECK_DIGIT_ENCODINGS_0;

public:
	inline static int32_t get_offset_of_CHECK_DIGIT_ENCODINGS_0() { return static_cast<int32_t>(offsetof(UPCEANExtension5Support_tAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD_StaticFields, ___CHECK_DIGIT_ENCODINGS_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_CHECK_DIGIT_ENCODINGS_0() const { return ___CHECK_DIGIT_ENCODINGS_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_CHECK_DIGIT_ENCODINGS_0() { return &___CHECK_DIGIT_ENCODINGS_0; }
	inline void set_CHECK_DIGIT_ENCODINGS_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___CHECK_DIGIT_ENCODINGS_0 = value;
		Il2CppCodeGenWriteBarrier((&___CHECK_DIGIT_ENCODINGS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPCEANEXTENSION5SUPPORT_TAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD_H
#ifndef UPCEANEXTENSIONSUPPORT_TCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B_H
#define UPCEANEXTENSIONSUPPORT_TCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.UPCEANExtensionSupport
struct  UPCEANExtensionSupport_tCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B  : public RuntimeObject
{
public:
	// ZXing.OneD.UPCEANExtension2Support ZXing.OneD.UPCEANExtensionSupport::twoSupport
	UPCEANExtension2Support_t1FAB0D3ED96CEF129DD7BECBF9DCD16C1EEEB7CA * ___twoSupport_1;
	// ZXing.OneD.UPCEANExtension5Support ZXing.OneD.UPCEANExtensionSupport::fiveSupport
	UPCEANExtension5Support_tAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD * ___fiveSupport_2;

public:
	inline static int32_t get_offset_of_twoSupport_1() { return static_cast<int32_t>(offsetof(UPCEANExtensionSupport_tCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B, ___twoSupport_1)); }
	inline UPCEANExtension2Support_t1FAB0D3ED96CEF129DD7BECBF9DCD16C1EEEB7CA * get_twoSupport_1() const { return ___twoSupport_1; }
	inline UPCEANExtension2Support_t1FAB0D3ED96CEF129DD7BECBF9DCD16C1EEEB7CA ** get_address_of_twoSupport_1() { return &___twoSupport_1; }
	inline void set_twoSupport_1(UPCEANExtension2Support_t1FAB0D3ED96CEF129DD7BECBF9DCD16C1EEEB7CA * value)
	{
		___twoSupport_1 = value;
		Il2CppCodeGenWriteBarrier((&___twoSupport_1), value);
	}

	inline static int32_t get_offset_of_fiveSupport_2() { return static_cast<int32_t>(offsetof(UPCEANExtensionSupport_tCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B, ___fiveSupport_2)); }
	inline UPCEANExtension5Support_tAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD * get_fiveSupport_2() const { return ___fiveSupport_2; }
	inline UPCEANExtension5Support_tAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD ** get_address_of_fiveSupport_2() { return &___fiveSupport_2; }
	inline void set_fiveSupport_2(UPCEANExtension5Support_tAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD * value)
	{
		___fiveSupport_2 = value;
		Il2CppCodeGenWriteBarrier((&___fiveSupport_2), value);
	}
};

struct UPCEANExtensionSupport_tCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B_StaticFields
{
public:
	// System.Int32[] ZXing.OneD.UPCEANExtensionSupport::EXTENSION_START_PATTERN
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___EXTENSION_START_PATTERN_0;

public:
	inline static int32_t get_offset_of_EXTENSION_START_PATTERN_0() { return static_cast<int32_t>(offsetof(UPCEANExtensionSupport_tCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B_StaticFields, ___EXTENSION_START_PATTERN_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_EXTENSION_START_PATTERN_0() const { return ___EXTENSION_START_PATTERN_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_EXTENSION_START_PATTERN_0() { return &___EXTENSION_START_PATTERN_0; }
	inline void set_EXTENSION_START_PATTERN_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___EXTENSION_START_PATTERN_0 = value;
		Il2CppCodeGenWriteBarrier((&___EXTENSION_START_PATTERN_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPCEANEXTENSIONSUPPORT_TCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B_H
#ifndef __STATICARRAYINITTYPESIZEU3D10_TB641FD73E10817DC3A5A0B6CDD83506C3C2B53C7_H
#define __STATICARRAYINITTYPESIZEU3D10_TB641FD73E10817DC3A5A0B6CDD83506C3C2B53C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10
struct  __StaticArrayInitTypeSizeU3D10_tB641FD73E10817DC3A5A0B6CDD83506C3C2B53C7 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D10_tB641FD73E10817DC3A5A0B6CDD83506C3C2B53C7__padding[10];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D10_TB641FD73E10817DC3A5A0B6CDD83506C3C2B53C7_H
#ifndef __STATICARRAYINITTYPESIZEU3D11148_TA414153821EB70E6FCD456B1E2DB309D58CE1619_H
#define __STATICARRAYINITTYPESIZEU3D11148_TA414153821EB70E6FCD456B1E2DB309D58CE1619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=11148
struct  __StaticArrayInitTypeSizeU3D11148_tA414153821EB70E6FCD456B1E2DB309D58CE1619 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D11148_tA414153821EB70E6FCD456B1E2DB309D58CE1619__padding[11148];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D11148_TA414153821EB70E6FCD456B1E2DB309D58CE1619_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_TFBADEC3E9505138D884DDB68442D2D48D268FBF4_H
#define __STATICARRAYINITTYPESIZEU3D12_TFBADEC3E9505138D884DDB68442D2D48D268FBF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_TFBADEC3E9505138D884DDB68442D2D48D268FBF4_H
#ifndef __STATICARRAYINITTYPESIZEU3D120_T3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12_H
#define __STATICARRAYINITTYPESIZEU3D120_T3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120
struct  __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12__padding[120];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D120_T3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12_H
#ifndef __STATICARRAYINITTYPESIZEU3D136_TF639EB419908A28312DAF13451C27B4ED03E8674_H
#define __STATICARRAYINITTYPESIZEU3D136_TF639EB419908A28312DAF13451C27B4ED03E8674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=136
struct  __StaticArrayInitTypeSizeU3D136_tF639EB419908A28312DAF13451C27B4ED03E8674 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D136_tF639EB419908A28312DAF13451C27B4ED03E8674__padding[136];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D136_TF639EB419908A28312DAF13451C27B4ED03E8674_H
#ifndef __STATICARRAYINITTYPESIZEU3D148_T0A73993E265ED7BA3349D48D2563680FA231189B_H
#define __STATICARRAYINITTYPESIZEU3D148_T0A73993E265ED7BA3349D48D2563680FA231189B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=148
struct  __StaticArrayInitTypeSizeU3D148_t0A73993E265ED7BA3349D48D2563680FA231189B 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D148_t0A73993E265ED7BA3349D48D2563680FA231189B__padding[148];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D148_T0A73993E265ED7BA3349D48D2563680FA231189B_H
#ifndef __STATICARRAYINITTYPESIZEU3D156_TD444739979C2F1A4BE28790BC71DD6FB876CCD25_H
#define __STATICARRAYINITTYPESIZEU3D156_TD444739979C2F1A4BE28790BC71DD6FB876CCD25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=156
struct  __StaticArrayInitTypeSizeU3D156_tD444739979C2F1A4BE28790BC71DD6FB876CCD25 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D156_tD444739979C2F1A4BE28790BC71DD6FB876CCD25__padding[156];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D156_TD444739979C2F1A4BE28790BC71DD6FB876CCD25_H
#ifndef __STATICARRAYINITTYPESIZEU3D16_TE833B82A97F69BFEB7419FA93CA61B924F6F63B0_H
#define __STATICARRAYINITTYPESIZEU3D16_TE833B82A97F69BFEB7419FA93CA61B924F6F63B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16
struct  __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0__padding[16];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D16_TE833B82A97F69BFEB7419FA93CA61B924F6F63B0_H
#ifndef __STATICARRAYINITTYPESIZEU3D176_T9A793B7CC486996BB07636533D64428F9FE21236_H
#define __STATICARRAYINITTYPESIZEU3D176_T9A793B7CC486996BB07636533D64428F9FE21236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=176
struct  __StaticArrayInitTypeSizeU3D176_t9A793B7CC486996BB07636533D64428F9FE21236 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D176_t9A793B7CC486996BB07636533D64428F9FE21236__padding[176];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D176_T9A793B7CC486996BB07636533D64428F9FE21236_H
#ifndef __STATICARRAYINITTYPESIZEU3D192_TCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36_H
#define __STATICARRAYINITTYPESIZEU3D192_TCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=192
struct  __StaticArrayInitTypeSizeU3D192_tCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D192_tCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36__padding[192];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D192_TCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36_H
#ifndef __STATICARRAYINITTYPESIZEU3D20_TE6AB0DAD941BE28D336223604DCCC6D73B2287B5_H
#define __STATICARRAYINITTYPESIZEU3D20_TE6AB0DAD941BE28D336223604DCCC6D73B2287B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20
struct  __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5__padding[20];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D20_TE6AB0DAD941BE28D336223604DCCC6D73B2287B5_H
#ifndef __STATICARRAYINITTYPESIZEU3D24_T1D2172AAD08C1ECBF6783D2872306DBB2F180436_H
#define __STATICARRAYINITTYPESIZEU3D24_T1D2172AAD08C1ECBF6783D2872306DBB2F180436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D24_T1D2172AAD08C1ECBF6783D2872306DBB2F180436_H
#ifndef __STATICARRAYINITTYPESIZEU3D2574_T94A56C994C4EA1F17B7D51314A88B67247C9BC15_H
#define __STATICARRAYINITTYPESIZEU3D2574_T94A56C994C4EA1F17B7D51314A88B67247C9BC15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=2574
struct  __StaticArrayInitTypeSizeU3D2574_t94A56C994C4EA1F17B7D51314A88B67247C9BC15 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D2574_t94A56C994C4EA1F17B7D51314A88B67247C9BC15__padding[2574];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D2574_T94A56C994C4EA1F17B7D51314A88B67247C9BC15_H
#ifndef __STATICARRAYINITTYPESIZEU3D26_T1300BB2EFCCD978826087890170DE54BB813F8E7_H
#define __STATICARRAYINITTYPESIZEU3D26_T1300BB2EFCCD978826087890170DE54BB813F8E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26
struct  __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7__padding[26];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D26_T1300BB2EFCCD978826087890170DE54BB813F8E7_H
#ifndef __STATICARRAYINITTYPESIZEU3D28_TFDDEB43D291084240980227C00328EBA1C48F445_H
#define __STATICARRAYINITTYPESIZEU3D28_TFDDEB43D291084240980227C00328EBA1C48F445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28
struct  __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445__padding[28];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D28_TFDDEB43D291084240980227C00328EBA1C48F445_H
#ifndef __STATICARRAYINITTYPESIZEU3D30_T14568882CF4903E97998A46DDDF1E20E37839016_H
#define __STATICARRAYINITTYPESIZEU3D30_T14568882CF4903E97998A46DDDF1E20E37839016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=30
struct  __StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016__padding[30];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D30_T14568882CF4903E97998A46DDDF1E20E37839016_H
#ifndef __STATICARRAYINITTYPESIZEU3D32_T33CBB6B98D456285860AB1149E6E0B652C81F14C_H
#define __STATICARRAYINITTYPESIZEU3D32_T33CBB6B98D456285860AB1149E6E0B652C81F14C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32
struct  __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C__padding[32];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D32_T33CBB6B98D456285860AB1149E6E0B652C81F14C_H
#ifndef __STATICARRAYINITTYPESIZEU3D36_T2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17_H
#define __STATICARRAYINITTYPESIZEU3D36_T2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36
struct  __StaticArrayInitTypeSizeU3D36_t2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D36_t2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17__padding[36];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D36_T2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17_H
#ifndef __STATICARRAYINITTYPESIZEU3D384_T6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3_H
#define __STATICARRAYINITTYPESIZEU3D384_T6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=384
struct  __StaticArrayInitTypeSizeU3D384_t6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D384_t6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3__padding[384];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D384_T6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3_H
#ifndef __STATICARRAYINITTYPESIZEU3D40_TB5E642B07376B8F47DB15AAA7C0561CB42F6591F_H
#define __STATICARRAYINITTYPESIZEU3D40_TB5E642B07376B8F47DB15AAA7C0561CB42F6591F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40
struct  __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F__padding[40];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D40_TB5E642B07376B8F47DB15AAA7C0561CB42F6591F_H
#ifndef __STATICARRAYINITTYPESIZEU3D44_TE280582A6EEC87F26198EDC0F6EC802BBCD3D015_H
#define __STATICARRAYINITTYPESIZEU3D44_TE280582A6EEC87F26198EDC0F6EC802BBCD3D015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44
struct  __StaticArrayInitTypeSizeU3D44_tE280582A6EEC87F26198EDC0F6EC802BBCD3D015 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D44_tE280582A6EEC87F26198EDC0F6EC802BBCD3D015__padding[44];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D44_TE280582A6EEC87F26198EDC0F6EC802BBCD3D015_H
#ifndef __STATICARRAYINITTYPESIZEU3D52_T94FED58B7B80A7E497773366DB2DD42193888C54_H
#define __STATICARRAYINITTYPESIZEU3D52_T94FED58B7B80A7E497773366DB2DD42193888C54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52
struct  __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54__padding[52];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D52_T94FED58B7B80A7E497773366DB2DD42193888C54_H
#ifndef __STATICARRAYINITTYPESIZEU3D54_T1ACEB6759550166F62217D005B9DC6C35D54C265_H
#define __STATICARRAYINITTYPESIZEU3D54_T1ACEB6759550166F62217D005B9DC6C35D54C265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=54
struct  __StaticArrayInitTypeSizeU3D54_t1ACEB6759550166F62217D005B9DC6C35D54C265 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D54_t1ACEB6759550166F62217D005B9DC6C35D54C265__padding[54];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D54_T1ACEB6759550166F62217D005B9DC6C35D54C265_H
#ifndef __STATICARRAYINITTYPESIZEU3D6_T51B71C53A0BC4176315E48ACD4457D9A44EB4BDB_H
#define __STATICARRAYINITTYPESIZEU3D6_T51B71C53A0BC4176315E48ACD4457D9A44EB4BDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6
struct  __StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB__padding[6];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D6_T51B71C53A0BC4176315E48ACD4457D9A44EB4BDB_H
#ifndef __STATICARRAYINITTYPESIZEU3D64_T2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF_H
#define __STATICARRAYINITTYPESIZEU3D64_T2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64
struct  __StaticArrayInitTypeSizeU3D64_t2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D64_t2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF__padding[64];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D64_T2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF_H
#ifndef __STATICARRAYINITTYPESIZEU3D80_T2C4CCE0AD65A37C6C949742DB0FF117776478260_H
#define __STATICARRAYINITTYPESIZEU3D80_T2C4CCE0AD65A37C6C949742DB0FF117776478260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=80
struct  __StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260__padding[80];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D80_T2C4CCE0AD65A37C6C949742DB0FF117776478260_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef AZTECDETECTORRESULT_TDE03D185E80A00BE03617EE579AE2DB5F832D133_H
#define AZTECDETECTORRESULT_TDE03D185E80A00BE03617EE579AE2DB5F832D133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.Internal.AztecDetectorResult
struct  AztecDetectorResult_tDE03D185E80A00BE03617EE579AE2DB5F832D133  : public DetectorResult_t87E236CAB9ED47A76E59D1809CE6C7DD6D8E9E2C
{
public:
	// System.Boolean ZXing.Aztec.Internal.AztecDetectorResult::<Compact>k__BackingField
	bool ___U3CCompactU3Ek__BackingField_2;
	// System.Int32 ZXing.Aztec.Internal.AztecDetectorResult::<NbDatablocks>k__BackingField
	int32_t ___U3CNbDatablocksU3Ek__BackingField_3;
	// System.Int32 ZXing.Aztec.Internal.AztecDetectorResult::<NbLayers>k__BackingField
	int32_t ___U3CNbLayersU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CCompactU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AztecDetectorResult_tDE03D185E80A00BE03617EE579AE2DB5F832D133, ___U3CCompactU3Ek__BackingField_2)); }
	inline bool get_U3CCompactU3Ek__BackingField_2() const { return ___U3CCompactU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CCompactU3Ek__BackingField_2() { return &___U3CCompactU3Ek__BackingField_2; }
	inline void set_U3CCompactU3Ek__BackingField_2(bool value)
	{
		___U3CCompactU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CNbDatablocksU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AztecDetectorResult_tDE03D185E80A00BE03617EE579AE2DB5F832D133, ___U3CNbDatablocksU3Ek__BackingField_3)); }
	inline int32_t get_U3CNbDatablocksU3Ek__BackingField_3() const { return ___U3CNbDatablocksU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CNbDatablocksU3Ek__BackingField_3() { return &___U3CNbDatablocksU3Ek__BackingField_3; }
	inline void set_U3CNbDatablocksU3Ek__BackingField_3(int32_t value)
	{
		___U3CNbDatablocksU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CNbLayersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AztecDetectorResult_tDE03D185E80A00BE03617EE579AE2DB5F832D133, ___U3CNbLayersU3Ek__BackingField_4)); }
	inline int32_t get_U3CNbLayersU3Ek__BackingField_4() const { return ___U3CNbLayersU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CNbLayersU3Ek__BackingField_4() { return &___U3CNbLayersU3Ek__BackingField_4; }
	inline void set_U3CNbLayersU3Ek__BackingField_4(int32_t value)
	{
		___U3CNbLayersU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AZTECDETECTORRESULT_TDE03D185E80A00BE03617EE579AE2DB5F832D133_H
#ifndef CHARACTERSETECI_TEA5814F6001515FC05BFDFAE3E8721124B48BC37_H
#define CHARACTERSETECI_TEA5814F6001515FC05BFDFAE3E8721124B48BC37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.CharacterSetECI
struct  CharacterSetECI_tEA5814F6001515FC05BFDFAE3E8721124B48BC37  : public ECI_tEF9EAED8C9F3BFA6043AC72BEE50849BD3124A1A
{
public:
	// System.String ZXing.Common.CharacterSetECI::encodingName
	String_t* ___encodingName_3;

public:
	inline static int32_t get_offset_of_encodingName_3() { return static_cast<int32_t>(offsetof(CharacterSetECI_tEA5814F6001515FC05BFDFAE3E8721124B48BC37, ___encodingName_3)); }
	inline String_t* get_encodingName_3() const { return ___encodingName_3; }
	inline String_t** get_address_of_encodingName_3() { return &___encodingName_3; }
	inline void set_encodingName_3(String_t* value)
	{
		___encodingName_3 = value;
		Il2CppCodeGenWriteBarrier((&___encodingName_3), value);
	}
};

struct CharacterSetECI_tEA5814F6001515FC05BFDFAE3E8721124B48BC37_StaticFields
{
public:
	// System.Collections.Generic.IDictionary`2<System.Int32,ZXing.Common.CharacterSetECI> ZXing.Common.CharacterSetECI::VALUE_TO_ECI
	RuntimeObject* ___VALUE_TO_ECI_1;
	// System.Collections.Generic.IDictionary`2<System.String,ZXing.Common.CharacterSetECI> ZXing.Common.CharacterSetECI::NAME_TO_ECI
	RuntimeObject* ___NAME_TO_ECI_2;

public:
	inline static int32_t get_offset_of_VALUE_TO_ECI_1() { return static_cast<int32_t>(offsetof(CharacterSetECI_tEA5814F6001515FC05BFDFAE3E8721124B48BC37_StaticFields, ___VALUE_TO_ECI_1)); }
	inline RuntimeObject* get_VALUE_TO_ECI_1() const { return ___VALUE_TO_ECI_1; }
	inline RuntimeObject** get_address_of_VALUE_TO_ECI_1() { return &___VALUE_TO_ECI_1; }
	inline void set_VALUE_TO_ECI_1(RuntimeObject* value)
	{
		___VALUE_TO_ECI_1 = value;
		Il2CppCodeGenWriteBarrier((&___VALUE_TO_ECI_1), value);
	}

	inline static int32_t get_offset_of_NAME_TO_ECI_2() { return static_cast<int32_t>(offsetof(CharacterSetECI_tEA5814F6001515FC05BFDFAE3E8721124B48BC37_StaticFields, ___NAME_TO_ECI_2)); }
	inline RuntimeObject* get_NAME_TO_ECI_2() const { return ___NAME_TO_ECI_2; }
	inline RuntimeObject** get_address_of_NAME_TO_ECI_2() { return &___NAME_TO_ECI_2; }
	inline void set_NAME_TO_ECI_2(RuntimeObject* value)
	{
		___NAME_TO_ECI_2 = value;
		Il2CppCodeGenWriteBarrier((&___NAME_TO_ECI_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERSETECI_TEA5814F6001515FC05BFDFAE3E8721124B48BC37_H
#ifndef DEFAULTGRIDSAMPLER_TB2AE863567F11EFCEA1A85BD24C0306F0DA866D0_H
#define DEFAULTGRIDSAMPLER_TB2AE863567F11EFCEA1A85BD24C0306F0DA866D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.DefaultGridSampler
struct  DefaultGridSampler_tB2AE863567F11EFCEA1A85BD24C0306F0DA866D0  : public GridSampler_t8DD0C5250D72AC8D9F5DFD0F417F2FE212BE91D4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTGRIDSAMPLER_TB2AE863567F11EFCEA1A85BD24C0306F0DA866D0_H
#ifndef GLOBALHISTOGRAMBINARIZER_T757A4C6316A8E4C0F07D36369F4D8CA17D8FF760_H
#define GLOBALHISTOGRAMBINARIZER_T757A4C6316A8E4C0F07D36369F4D8CA17D8FF760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.GlobalHistogramBinarizer
struct  GlobalHistogramBinarizer_t757A4C6316A8E4C0F07D36369F4D8CA17D8FF760  : public Binarizer_t5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309
{
public:
	// System.Byte[] ZXing.Common.GlobalHistogramBinarizer::luminances
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___luminances_2;
	// System.Int32[] ZXing.Common.GlobalHistogramBinarizer::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_3;

public:
	inline static int32_t get_offset_of_luminances_2() { return static_cast<int32_t>(offsetof(GlobalHistogramBinarizer_t757A4C6316A8E4C0F07D36369F4D8CA17D8FF760, ___luminances_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_luminances_2() const { return ___luminances_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_luminances_2() { return &___luminances_2; }
	inline void set_luminances_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___luminances_2 = value;
		Il2CppCodeGenWriteBarrier((&___luminances_2), value);
	}

	inline static int32_t get_offset_of_buckets_3() { return static_cast<int32_t>(offsetof(GlobalHistogramBinarizer_t757A4C6316A8E4C0F07D36369F4D8CA17D8FF760, ___buckets_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_3() const { return ___buckets_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_3() { return &___buckets_3; }
	inline void set_buckets_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_3 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_3), value);
	}
};

struct GlobalHistogramBinarizer_t757A4C6316A8E4C0F07D36369F4D8CA17D8FF760_StaticFields
{
public:
	// System.Byte[] ZXing.Common.GlobalHistogramBinarizer::EMPTY
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EMPTY_1;

public:
	inline static int32_t get_offset_of_EMPTY_1() { return static_cast<int32_t>(offsetof(GlobalHistogramBinarizer_t757A4C6316A8E4C0F07D36369F4D8CA17D8FF760_StaticFields, ___EMPTY_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EMPTY_1() const { return ___EMPTY_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EMPTY_1() { return &___EMPTY_1; }
	inline void set_EMPTY_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EMPTY_1 = value;
		Il2CppCodeGenWriteBarrier((&___EMPTY_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALHISTOGRAMBINARIZER_T757A4C6316A8E4C0F07D36369F4D8CA17D8FF760_H
#ifndef IMBREADER_T92AB5FECB2ED489E59F463D44F6DFE7560C5C841_H
#define IMBREADER_T92AB5FECB2ED489E59F463D44F6DFE7560C5C841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.IMB.IMBReader
struct  IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841  : public OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC
{
public:
	// ZXing.BinaryBitmap ZXing.IMB.IMBReader::currentBitmap
	BinaryBitmap_tCF98378D7F884CDFBC4985A0ACCFFBE31FC51461 * ___currentBitmap_26;

public:
	inline static int32_t get_offset_of_currentBitmap_26() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841, ___currentBitmap_26)); }
	inline BinaryBitmap_tCF98378D7F884CDFBC4985A0ACCFFBE31FC51461 * get_currentBitmap_26() const { return ___currentBitmap_26; }
	inline BinaryBitmap_tCF98378D7F884CDFBC4985A0ACCFFBE31FC51461 ** get_address_of_currentBitmap_26() { return &___currentBitmap_26; }
	inline void set_currentBitmap_26(BinaryBitmap_tCF98378D7F884CDFBC4985A0ACCFFBE31FC51461 * value)
	{
		___currentBitmap_26 = value;
		Il2CppCodeGenWriteBarrier((&___currentBitmap_26), value);
	}
};

struct IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields
{
public:
	// System.Int32[] ZXing.IMB.IMBReader::barPosA
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___barPosA_2;
	// System.Int32[] ZXing.IMB.IMBReader::barPosB
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___barPosB_3;
	// System.Int32[] ZXing.IMB.IMBReader::barPosC
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___barPosC_4;
	// System.Int32[] ZXing.IMB.IMBReader::barPosD
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___barPosD_5;
	// System.Int32[] ZXing.IMB.IMBReader::barPosE
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___barPosE_6;
	// System.Int32[] ZXing.IMB.IMBReader::barPosF
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___barPosF_7;
	// System.Int32[] ZXing.IMB.IMBReader::barPosG
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___barPosG_8;
	// System.Int32[] ZXing.IMB.IMBReader::barPosH
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___barPosH_9;
	// System.Int32[] ZXing.IMB.IMBReader::barPosI
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___barPosI_10;
	// System.Int32[] ZXing.IMB.IMBReader::barPosJ
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___barPosJ_11;
	// System.Int32[][] ZXing.IMB.IMBReader::barPos
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___barPos_12;
	// System.Char[] ZXing.IMB.IMBReader::barTypeA
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___barTypeA_13;
	// System.Char[] ZXing.IMB.IMBReader::barTypeB
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___barTypeB_14;
	// System.Char[] ZXing.IMB.IMBReader::barTypeC
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___barTypeC_15;
	// System.Char[] ZXing.IMB.IMBReader::barTypeD
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___barTypeD_16;
	// System.Char[] ZXing.IMB.IMBReader::barTypeE
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___barTypeE_17;
	// System.Char[] ZXing.IMB.IMBReader::barTypeF
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___barTypeF_18;
	// System.Char[] ZXing.IMB.IMBReader::barTypeG
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___barTypeG_19;
	// System.Char[] ZXing.IMB.IMBReader::barTypeH
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___barTypeH_20;
	// System.Char[] ZXing.IMB.IMBReader::barTypeI
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___barTypeI_21;
	// System.Char[] ZXing.IMB.IMBReader::barTypeJ
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___barTypeJ_22;
	// System.Char[][] ZXing.IMB.IMBReader::barType
	CharU5BU5DU5BU5D_t509D6DD2B5D36FCA00421F0FDECD58289E41A574* ___barType_23;
	// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32> ZXing.IMB.IMBReader::table1Check
	RuntimeObject* ___table1Check_24;
	// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32> ZXing.IMB.IMBReader::table2Check
	RuntimeObject* ___table2Check_25;

public:
	inline static int32_t get_offset_of_barPosA_2() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barPosA_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_barPosA_2() const { return ___barPosA_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_barPosA_2() { return &___barPosA_2; }
	inline void set_barPosA_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___barPosA_2 = value;
		Il2CppCodeGenWriteBarrier((&___barPosA_2), value);
	}

	inline static int32_t get_offset_of_barPosB_3() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barPosB_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_barPosB_3() const { return ___barPosB_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_barPosB_3() { return &___barPosB_3; }
	inline void set_barPosB_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___barPosB_3 = value;
		Il2CppCodeGenWriteBarrier((&___barPosB_3), value);
	}

	inline static int32_t get_offset_of_barPosC_4() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barPosC_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_barPosC_4() const { return ___barPosC_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_barPosC_4() { return &___barPosC_4; }
	inline void set_barPosC_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___barPosC_4 = value;
		Il2CppCodeGenWriteBarrier((&___barPosC_4), value);
	}

	inline static int32_t get_offset_of_barPosD_5() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barPosD_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_barPosD_5() const { return ___barPosD_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_barPosD_5() { return &___barPosD_5; }
	inline void set_barPosD_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___barPosD_5 = value;
		Il2CppCodeGenWriteBarrier((&___barPosD_5), value);
	}

	inline static int32_t get_offset_of_barPosE_6() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barPosE_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_barPosE_6() const { return ___barPosE_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_barPosE_6() { return &___barPosE_6; }
	inline void set_barPosE_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___barPosE_6 = value;
		Il2CppCodeGenWriteBarrier((&___barPosE_6), value);
	}

	inline static int32_t get_offset_of_barPosF_7() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barPosF_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_barPosF_7() const { return ___barPosF_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_barPosF_7() { return &___barPosF_7; }
	inline void set_barPosF_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___barPosF_7 = value;
		Il2CppCodeGenWriteBarrier((&___barPosF_7), value);
	}

	inline static int32_t get_offset_of_barPosG_8() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barPosG_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_barPosG_8() const { return ___barPosG_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_barPosG_8() { return &___barPosG_8; }
	inline void set_barPosG_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___barPosG_8 = value;
		Il2CppCodeGenWriteBarrier((&___barPosG_8), value);
	}

	inline static int32_t get_offset_of_barPosH_9() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barPosH_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_barPosH_9() const { return ___barPosH_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_barPosH_9() { return &___barPosH_9; }
	inline void set_barPosH_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___barPosH_9 = value;
		Il2CppCodeGenWriteBarrier((&___barPosH_9), value);
	}

	inline static int32_t get_offset_of_barPosI_10() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barPosI_10)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_barPosI_10() const { return ___barPosI_10; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_barPosI_10() { return &___barPosI_10; }
	inline void set_barPosI_10(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___barPosI_10 = value;
		Il2CppCodeGenWriteBarrier((&___barPosI_10), value);
	}

	inline static int32_t get_offset_of_barPosJ_11() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barPosJ_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_barPosJ_11() const { return ___barPosJ_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_barPosJ_11() { return &___barPosJ_11; }
	inline void set_barPosJ_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___barPosJ_11 = value;
		Il2CppCodeGenWriteBarrier((&___barPosJ_11), value);
	}

	inline static int32_t get_offset_of_barPos_12() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barPos_12)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_barPos_12() const { return ___barPos_12; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_barPos_12() { return &___barPos_12; }
	inline void set_barPos_12(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___barPos_12 = value;
		Il2CppCodeGenWriteBarrier((&___barPos_12), value);
	}

	inline static int32_t get_offset_of_barTypeA_13() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barTypeA_13)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_barTypeA_13() const { return ___barTypeA_13; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_barTypeA_13() { return &___barTypeA_13; }
	inline void set_barTypeA_13(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___barTypeA_13 = value;
		Il2CppCodeGenWriteBarrier((&___barTypeA_13), value);
	}

	inline static int32_t get_offset_of_barTypeB_14() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barTypeB_14)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_barTypeB_14() const { return ___barTypeB_14; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_barTypeB_14() { return &___barTypeB_14; }
	inline void set_barTypeB_14(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___barTypeB_14 = value;
		Il2CppCodeGenWriteBarrier((&___barTypeB_14), value);
	}

	inline static int32_t get_offset_of_barTypeC_15() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barTypeC_15)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_barTypeC_15() const { return ___barTypeC_15; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_barTypeC_15() { return &___barTypeC_15; }
	inline void set_barTypeC_15(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___barTypeC_15 = value;
		Il2CppCodeGenWriteBarrier((&___barTypeC_15), value);
	}

	inline static int32_t get_offset_of_barTypeD_16() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barTypeD_16)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_barTypeD_16() const { return ___barTypeD_16; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_barTypeD_16() { return &___barTypeD_16; }
	inline void set_barTypeD_16(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___barTypeD_16 = value;
		Il2CppCodeGenWriteBarrier((&___barTypeD_16), value);
	}

	inline static int32_t get_offset_of_barTypeE_17() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barTypeE_17)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_barTypeE_17() const { return ___barTypeE_17; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_barTypeE_17() { return &___barTypeE_17; }
	inline void set_barTypeE_17(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___barTypeE_17 = value;
		Il2CppCodeGenWriteBarrier((&___barTypeE_17), value);
	}

	inline static int32_t get_offset_of_barTypeF_18() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barTypeF_18)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_barTypeF_18() const { return ___barTypeF_18; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_barTypeF_18() { return &___barTypeF_18; }
	inline void set_barTypeF_18(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___barTypeF_18 = value;
		Il2CppCodeGenWriteBarrier((&___barTypeF_18), value);
	}

	inline static int32_t get_offset_of_barTypeG_19() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barTypeG_19)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_barTypeG_19() const { return ___barTypeG_19; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_barTypeG_19() { return &___barTypeG_19; }
	inline void set_barTypeG_19(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___barTypeG_19 = value;
		Il2CppCodeGenWriteBarrier((&___barTypeG_19), value);
	}

	inline static int32_t get_offset_of_barTypeH_20() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barTypeH_20)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_barTypeH_20() const { return ___barTypeH_20; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_barTypeH_20() { return &___barTypeH_20; }
	inline void set_barTypeH_20(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___barTypeH_20 = value;
		Il2CppCodeGenWriteBarrier((&___barTypeH_20), value);
	}

	inline static int32_t get_offset_of_barTypeI_21() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barTypeI_21)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_barTypeI_21() const { return ___barTypeI_21; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_barTypeI_21() { return &___barTypeI_21; }
	inline void set_barTypeI_21(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___barTypeI_21 = value;
		Il2CppCodeGenWriteBarrier((&___barTypeI_21), value);
	}

	inline static int32_t get_offset_of_barTypeJ_22() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barTypeJ_22)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_barTypeJ_22() const { return ___barTypeJ_22; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_barTypeJ_22() { return &___barTypeJ_22; }
	inline void set_barTypeJ_22(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___barTypeJ_22 = value;
		Il2CppCodeGenWriteBarrier((&___barTypeJ_22), value);
	}

	inline static int32_t get_offset_of_barType_23() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___barType_23)); }
	inline CharU5BU5DU5BU5D_t509D6DD2B5D36FCA00421F0FDECD58289E41A574* get_barType_23() const { return ___barType_23; }
	inline CharU5BU5DU5BU5D_t509D6DD2B5D36FCA00421F0FDECD58289E41A574** get_address_of_barType_23() { return &___barType_23; }
	inline void set_barType_23(CharU5BU5DU5BU5D_t509D6DD2B5D36FCA00421F0FDECD58289E41A574* value)
	{
		___barType_23 = value;
		Il2CppCodeGenWriteBarrier((&___barType_23), value);
	}

	inline static int32_t get_offset_of_table1Check_24() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___table1Check_24)); }
	inline RuntimeObject* get_table1Check_24() const { return ___table1Check_24; }
	inline RuntimeObject** get_address_of_table1Check_24() { return &___table1Check_24; }
	inline void set_table1Check_24(RuntimeObject* value)
	{
		___table1Check_24 = value;
		Il2CppCodeGenWriteBarrier((&___table1Check_24), value);
	}

	inline static int32_t get_offset_of_table2Check_25() { return static_cast<int32_t>(offsetof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields, ___table2Check_25)); }
	inline RuntimeObject* get_table2Check_25() const { return ___table2Check_25; }
	inline RuntimeObject** get_address_of_table2Check_25() { return &___table2Check_25; }
	inline void set_table2Check_25(RuntimeObject* value)
	{
		___table2Check_25 = value;
		Il2CppCodeGenWriteBarrier((&___table2Check_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMBREADER_T92AB5FECB2ED489E59F463D44F6DFE7560C5C841_H
#ifndef CODE39READER_T43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74_H
#define CODE39READER_T43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.Code39Reader
struct  Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74  : public OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC
{
public:
	// System.Boolean ZXing.OneD.Code39Reader::usingCheckDigit
	bool ___usingCheckDigit_6;
	// System.Boolean ZXing.OneD.Code39Reader::extendedMode
	bool ___extendedMode_7;
	// System.Text.StringBuilder ZXing.OneD.Code39Reader::decodeRowResult
	StringBuilder_t * ___decodeRowResult_8;
	// System.Int32[] ZXing.OneD.Code39Reader::counters
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___counters_9;

public:
	inline static int32_t get_offset_of_usingCheckDigit_6() { return static_cast<int32_t>(offsetof(Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74, ___usingCheckDigit_6)); }
	inline bool get_usingCheckDigit_6() const { return ___usingCheckDigit_6; }
	inline bool* get_address_of_usingCheckDigit_6() { return &___usingCheckDigit_6; }
	inline void set_usingCheckDigit_6(bool value)
	{
		___usingCheckDigit_6 = value;
	}

	inline static int32_t get_offset_of_extendedMode_7() { return static_cast<int32_t>(offsetof(Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74, ___extendedMode_7)); }
	inline bool get_extendedMode_7() const { return ___extendedMode_7; }
	inline bool* get_address_of_extendedMode_7() { return &___extendedMode_7; }
	inline void set_extendedMode_7(bool value)
	{
		___extendedMode_7 = value;
	}

	inline static int32_t get_offset_of_decodeRowResult_8() { return static_cast<int32_t>(offsetof(Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74, ___decodeRowResult_8)); }
	inline StringBuilder_t * get_decodeRowResult_8() const { return ___decodeRowResult_8; }
	inline StringBuilder_t ** get_address_of_decodeRowResult_8() { return &___decodeRowResult_8; }
	inline void set_decodeRowResult_8(StringBuilder_t * value)
	{
		___decodeRowResult_8 = value;
		Il2CppCodeGenWriteBarrier((&___decodeRowResult_8), value);
	}

	inline static int32_t get_offset_of_counters_9() { return static_cast<int32_t>(offsetof(Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74, ___counters_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_counters_9() const { return ___counters_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_counters_9() { return &___counters_9; }
	inline void set_counters_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___counters_9 = value;
		Il2CppCodeGenWriteBarrier((&___counters_9), value);
	}
};

struct Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74_StaticFields
{
public:
	// System.String ZXing.OneD.Code39Reader::ALPHABET_STRING
	String_t* ___ALPHABET_STRING_2;
	// System.String ZXing.OneD.Code39Reader::CHECK_DIGIT_STRING
	String_t* ___CHECK_DIGIT_STRING_3;
	// System.Int32[] ZXing.OneD.Code39Reader::CHARACTER_ENCODINGS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___CHARACTER_ENCODINGS_4;
	// System.Int32 ZXing.OneD.Code39Reader::ASTERISK_ENCODING
	int32_t ___ASTERISK_ENCODING_5;

public:
	inline static int32_t get_offset_of_ALPHABET_STRING_2() { return static_cast<int32_t>(offsetof(Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74_StaticFields, ___ALPHABET_STRING_2)); }
	inline String_t* get_ALPHABET_STRING_2() const { return ___ALPHABET_STRING_2; }
	inline String_t** get_address_of_ALPHABET_STRING_2() { return &___ALPHABET_STRING_2; }
	inline void set_ALPHABET_STRING_2(String_t* value)
	{
		___ALPHABET_STRING_2 = value;
		Il2CppCodeGenWriteBarrier((&___ALPHABET_STRING_2), value);
	}

	inline static int32_t get_offset_of_CHECK_DIGIT_STRING_3() { return static_cast<int32_t>(offsetof(Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74_StaticFields, ___CHECK_DIGIT_STRING_3)); }
	inline String_t* get_CHECK_DIGIT_STRING_3() const { return ___CHECK_DIGIT_STRING_3; }
	inline String_t** get_address_of_CHECK_DIGIT_STRING_3() { return &___CHECK_DIGIT_STRING_3; }
	inline void set_CHECK_DIGIT_STRING_3(String_t* value)
	{
		___CHECK_DIGIT_STRING_3 = value;
		Il2CppCodeGenWriteBarrier((&___CHECK_DIGIT_STRING_3), value);
	}

	inline static int32_t get_offset_of_CHARACTER_ENCODINGS_4() { return static_cast<int32_t>(offsetof(Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74_StaticFields, ___CHARACTER_ENCODINGS_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_CHARACTER_ENCODINGS_4() const { return ___CHARACTER_ENCODINGS_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_CHARACTER_ENCODINGS_4() { return &___CHARACTER_ENCODINGS_4; }
	inline void set_CHARACTER_ENCODINGS_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___CHARACTER_ENCODINGS_4 = value;
		Il2CppCodeGenWriteBarrier((&___CHARACTER_ENCODINGS_4), value);
	}

	inline static int32_t get_offset_of_ASTERISK_ENCODING_5() { return static_cast<int32_t>(offsetof(Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74_StaticFields, ___ASTERISK_ENCODING_5)); }
	inline int32_t get_ASTERISK_ENCODING_5() const { return ___ASTERISK_ENCODING_5; }
	inline int32_t* get_address_of_ASTERISK_ENCODING_5() { return &___ASTERISK_ENCODING_5; }
	inline void set_ASTERISK_ENCODING_5(int32_t value)
	{
		___ASTERISK_ENCODING_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODE39READER_T43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74_H
#ifndef CODE93READER_T07729DD387AC6113DEFCCF365713BA1F494C65CF_H
#define CODE93READER_T07729DD387AC6113DEFCCF365713BA1F494C65CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.Code93Reader
struct  Code93Reader_t07729DD387AC6113DEFCCF365713BA1F494C65CF  : public OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC
{
public:
	// System.Text.StringBuilder ZXing.OneD.Code93Reader::decodeRowResult
	StringBuilder_t * ___decodeRowResult_5;
	// System.Int32[] ZXing.OneD.Code93Reader::counters
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___counters_6;

public:
	inline static int32_t get_offset_of_decodeRowResult_5() { return static_cast<int32_t>(offsetof(Code93Reader_t07729DD387AC6113DEFCCF365713BA1F494C65CF, ___decodeRowResult_5)); }
	inline StringBuilder_t * get_decodeRowResult_5() const { return ___decodeRowResult_5; }
	inline StringBuilder_t ** get_address_of_decodeRowResult_5() { return &___decodeRowResult_5; }
	inline void set_decodeRowResult_5(StringBuilder_t * value)
	{
		___decodeRowResult_5 = value;
		Il2CppCodeGenWriteBarrier((&___decodeRowResult_5), value);
	}

	inline static int32_t get_offset_of_counters_6() { return static_cast<int32_t>(offsetof(Code93Reader_t07729DD387AC6113DEFCCF365713BA1F494C65CF, ___counters_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_counters_6() const { return ___counters_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_counters_6() { return &___counters_6; }
	inline void set_counters_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___counters_6 = value;
		Il2CppCodeGenWriteBarrier((&___counters_6), value);
	}
};

struct Code93Reader_t07729DD387AC6113DEFCCF365713BA1F494C65CF_StaticFields
{
public:
	// System.Char[] ZXing.OneD.Code93Reader::ALPHABET
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___ALPHABET_2;
	// System.Int32[] ZXing.OneD.Code93Reader::CHARACTER_ENCODINGS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___CHARACTER_ENCODINGS_3;
	// System.Int32 ZXing.OneD.Code93Reader::ASTERISK_ENCODING
	int32_t ___ASTERISK_ENCODING_4;

public:
	inline static int32_t get_offset_of_ALPHABET_2() { return static_cast<int32_t>(offsetof(Code93Reader_t07729DD387AC6113DEFCCF365713BA1F494C65CF_StaticFields, ___ALPHABET_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_ALPHABET_2() const { return ___ALPHABET_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_ALPHABET_2() { return &___ALPHABET_2; }
	inline void set_ALPHABET_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___ALPHABET_2 = value;
		Il2CppCodeGenWriteBarrier((&___ALPHABET_2), value);
	}

	inline static int32_t get_offset_of_CHARACTER_ENCODINGS_3() { return static_cast<int32_t>(offsetof(Code93Reader_t07729DD387AC6113DEFCCF365713BA1F494C65CF_StaticFields, ___CHARACTER_ENCODINGS_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_CHARACTER_ENCODINGS_3() const { return ___CHARACTER_ENCODINGS_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_CHARACTER_ENCODINGS_3() { return &___CHARACTER_ENCODINGS_3; }
	inline void set_CHARACTER_ENCODINGS_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___CHARACTER_ENCODINGS_3 = value;
		Il2CppCodeGenWriteBarrier((&___CHARACTER_ENCODINGS_3), value);
	}

	inline static int32_t get_offset_of_ASTERISK_ENCODING_4() { return static_cast<int32_t>(offsetof(Code93Reader_t07729DD387AC6113DEFCCF365713BA1F494C65CF_StaticFields, ___ASTERISK_ENCODING_4)); }
	inline int32_t get_ASTERISK_ENCODING_4() const { return ___ASTERISK_ENCODING_4; }
	inline int32_t* get_address_of_ASTERISK_ENCODING_4() { return &___ASTERISK_ENCODING_4; }
	inline void set_ASTERISK_ENCODING_4(int32_t value)
	{
		___ASTERISK_ENCODING_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODE93READER_T07729DD387AC6113DEFCCF365713BA1F494C65CF_H
#ifndef ITFREADER_TF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_H
#define ITFREADER_TF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.ITFReader
struct  ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C  : public OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC
{
public:
	// System.Int32 ZXing.OneD.ITFReader::narrowLineWidth
	int32_t ___narrowLineWidth_5;

public:
	inline static int32_t get_offset_of_narrowLineWidth_5() { return static_cast<int32_t>(offsetof(ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C, ___narrowLineWidth_5)); }
	inline int32_t get_narrowLineWidth_5() const { return ___narrowLineWidth_5; }
	inline int32_t* get_address_of_narrowLineWidth_5() { return &___narrowLineWidth_5; }
	inline void set_narrowLineWidth_5(int32_t value)
	{
		___narrowLineWidth_5 = value;
	}
};

struct ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_StaticFields
{
public:
	// System.Int32 ZXing.OneD.ITFReader::MAX_AVG_VARIANCE
	int32_t ___MAX_AVG_VARIANCE_2;
	// System.Int32 ZXing.OneD.ITFReader::MAX_INDIVIDUAL_VARIANCE
	int32_t ___MAX_INDIVIDUAL_VARIANCE_3;
	// System.Int32[] ZXing.OneD.ITFReader::DEFAULT_ALLOWED_LENGTHS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DEFAULT_ALLOWED_LENGTHS_4;
	// System.Int32[] ZXing.OneD.ITFReader::START_PATTERN
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___START_PATTERN_6;
	// System.Int32[] ZXing.OneD.ITFReader::END_PATTERN_REVERSED
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___END_PATTERN_REVERSED_7;
	// System.Int32[][] ZXing.OneD.ITFReader::PATTERNS
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___PATTERNS_8;

public:
	inline static int32_t get_offset_of_MAX_AVG_VARIANCE_2() { return static_cast<int32_t>(offsetof(ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_StaticFields, ___MAX_AVG_VARIANCE_2)); }
	inline int32_t get_MAX_AVG_VARIANCE_2() const { return ___MAX_AVG_VARIANCE_2; }
	inline int32_t* get_address_of_MAX_AVG_VARIANCE_2() { return &___MAX_AVG_VARIANCE_2; }
	inline void set_MAX_AVG_VARIANCE_2(int32_t value)
	{
		___MAX_AVG_VARIANCE_2 = value;
	}

	inline static int32_t get_offset_of_MAX_INDIVIDUAL_VARIANCE_3() { return static_cast<int32_t>(offsetof(ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_StaticFields, ___MAX_INDIVIDUAL_VARIANCE_3)); }
	inline int32_t get_MAX_INDIVIDUAL_VARIANCE_3() const { return ___MAX_INDIVIDUAL_VARIANCE_3; }
	inline int32_t* get_address_of_MAX_INDIVIDUAL_VARIANCE_3() { return &___MAX_INDIVIDUAL_VARIANCE_3; }
	inline void set_MAX_INDIVIDUAL_VARIANCE_3(int32_t value)
	{
		___MAX_INDIVIDUAL_VARIANCE_3 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_ALLOWED_LENGTHS_4() { return static_cast<int32_t>(offsetof(ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_StaticFields, ___DEFAULT_ALLOWED_LENGTHS_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DEFAULT_ALLOWED_LENGTHS_4() const { return ___DEFAULT_ALLOWED_LENGTHS_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DEFAULT_ALLOWED_LENGTHS_4() { return &___DEFAULT_ALLOWED_LENGTHS_4; }
	inline void set_DEFAULT_ALLOWED_LENGTHS_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DEFAULT_ALLOWED_LENGTHS_4 = value;
		Il2CppCodeGenWriteBarrier((&___DEFAULT_ALLOWED_LENGTHS_4), value);
	}

	inline static int32_t get_offset_of_START_PATTERN_6() { return static_cast<int32_t>(offsetof(ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_StaticFields, ___START_PATTERN_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_START_PATTERN_6() const { return ___START_PATTERN_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_START_PATTERN_6() { return &___START_PATTERN_6; }
	inline void set_START_PATTERN_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___START_PATTERN_6 = value;
		Il2CppCodeGenWriteBarrier((&___START_PATTERN_6), value);
	}

	inline static int32_t get_offset_of_END_PATTERN_REVERSED_7() { return static_cast<int32_t>(offsetof(ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_StaticFields, ___END_PATTERN_REVERSED_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_END_PATTERN_REVERSED_7() const { return ___END_PATTERN_REVERSED_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_END_PATTERN_REVERSED_7() { return &___END_PATTERN_REVERSED_7; }
	inline void set_END_PATTERN_REVERSED_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___END_PATTERN_REVERSED_7 = value;
		Il2CppCodeGenWriteBarrier((&___END_PATTERN_REVERSED_7), value);
	}

	inline static int32_t get_offset_of_PATTERNS_8() { return static_cast<int32_t>(offsetof(ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_StaticFields, ___PATTERNS_8)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_PATTERNS_8() const { return ___PATTERNS_8; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_PATTERNS_8() { return &___PATTERNS_8; }
	inline void set_PATTERNS_8(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___PATTERNS_8 = value;
		Il2CppCodeGenWriteBarrier((&___PATTERNS_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITFREADER_TF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_H
#ifndef MSIREADER_T382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5_H
#define MSIREADER_T382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.MSIReader
struct  MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5  : public OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC
{
public:
	// System.Boolean ZXing.OneD.MSIReader::usingCheckDigit
	bool ___usingCheckDigit_5;
	// System.Text.StringBuilder ZXing.OneD.MSIReader::decodeRowResult
	StringBuilder_t * ___decodeRowResult_6;
	// System.Int32[] ZXing.OneD.MSIReader::counters
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___counters_7;
	// System.Int32 ZXing.OneD.MSIReader::averageCounterWidth
	int32_t ___averageCounterWidth_8;

public:
	inline static int32_t get_offset_of_usingCheckDigit_5() { return static_cast<int32_t>(offsetof(MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5, ___usingCheckDigit_5)); }
	inline bool get_usingCheckDigit_5() const { return ___usingCheckDigit_5; }
	inline bool* get_address_of_usingCheckDigit_5() { return &___usingCheckDigit_5; }
	inline void set_usingCheckDigit_5(bool value)
	{
		___usingCheckDigit_5 = value;
	}

	inline static int32_t get_offset_of_decodeRowResult_6() { return static_cast<int32_t>(offsetof(MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5, ___decodeRowResult_6)); }
	inline StringBuilder_t * get_decodeRowResult_6() const { return ___decodeRowResult_6; }
	inline StringBuilder_t ** get_address_of_decodeRowResult_6() { return &___decodeRowResult_6; }
	inline void set_decodeRowResult_6(StringBuilder_t * value)
	{
		___decodeRowResult_6 = value;
		Il2CppCodeGenWriteBarrier((&___decodeRowResult_6), value);
	}

	inline static int32_t get_offset_of_counters_7() { return static_cast<int32_t>(offsetof(MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5, ___counters_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_counters_7() const { return ___counters_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_counters_7() { return &___counters_7; }
	inline void set_counters_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___counters_7 = value;
		Il2CppCodeGenWriteBarrier((&___counters_7), value);
	}

	inline static int32_t get_offset_of_averageCounterWidth_8() { return static_cast<int32_t>(offsetof(MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5, ___averageCounterWidth_8)); }
	inline int32_t get_averageCounterWidth_8() const { return ___averageCounterWidth_8; }
	inline int32_t* get_address_of_averageCounterWidth_8() { return &___averageCounterWidth_8; }
	inline void set_averageCounterWidth_8(int32_t value)
	{
		___averageCounterWidth_8 = value;
	}
};

struct MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5_StaticFields
{
public:
	// System.String ZXing.OneD.MSIReader::ALPHABET_STRING
	String_t* ___ALPHABET_STRING_2;
	// System.Char[] ZXing.OneD.MSIReader::ALPHABET
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___ALPHABET_3;
	// System.Int32[] ZXing.OneD.MSIReader::CHARACTER_ENCODINGS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___CHARACTER_ENCODINGS_4;
	// System.Int32[] ZXing.OneD.MSIReader::doubleAndCrossSum
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___doubleAndCrossSum_9;

public:
	inline static int32_t get_offset_of_ALPHABET_STRING_2() { return static_cast<int32_t>(offsetof(MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5_StaticFields, ___ALPHABET_STRING_2)); }
	inline String_t* get_ALPHABET_STRING_2() const { return ___ALPHABET_STRING_2; }
	inline String_t** get_address_of_ALPHABET_STRING_2() { return &___ALPHABET_STRING_2; }
	inline void set_ALPHABET_STRING_2(String_t* value)
	{
		___ALPHABET_STRING_2 = value;
		Il2CppCodeGenWriteBarrier((&___ALPHABET_STRING_2), value);
	}

	inline static int32_t get_offset_of_ALPHABET_3() { return static_cast<int32_t>(offsetof(MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5_StaticFields, ___ALPHABET_3)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_ALPHABET_3() const { return ___ALPHABET_3; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_ALPHABET_3() { return &___ALPHABET_3; }
	inline void set_ALPHABET_3(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___ALPHABET_3 = value;
		Il2CppCodeGenWriteBarrier((&___ALPHABET_3), value);
	}

	inline static int32_t get_offset_of_CHARACTER_ENCODINGS_4() { return static_cast<int32_t>(offsetof(MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5_StaticFields, ___CHARACTER_ENCODINGS_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_CHARACTER_ENCODINGS_4() const { return ___CHARACTER_ENCODINGS_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_CHARACTER_ENCODINGS_4() { return &___CHARACTER_ENCODINGS_4; }
	inline void set_CHARACTER_ENCODINGS_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___CHARACTER_ENCODINGS_4 = value;
		Il2CppCodeGenWriteBarrier((&___CHARACTER_ENCODINGS_4), value);
	}

	inline static int32_t get_offset_of_doubleAndCrossSum_9() { return static_cast<int32_t>(offsetof(MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5_StaticFields, ___doubleAndCrossSum_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_doubleAndCrossSum_9() const { return ___doubleAndCrossSum_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_doubleAndCrossSum_9() { return &___doubleAndCrossSum_9; }
	inline void set_doubleAndCrossSum_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___doubleAndCrossSum_9 = value;
		Il2CppCodeGenWriteBarrier((&___doubleAndCrossSum_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MSIREADER_T382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5_H
#ifndef MULTIFORMATONEDREADER_T229E3DAC0D1C4281A4431E03A56CC773ED39D85D_H
#define MULTIFORMATONEDREADER_T229E3DAC0D1C4281A4431E03A56CC773ED39D85D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.MultiFormatOneDReader
struct  MultiFormatOneDReader_t229E3DAC0D1C4281A4431E03A56CC773ED39D85D  : public OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC
{
public:
	// System.Collections.Generic.IList`1<ZXing.OneD.OneDReader> ZXing.OneD.MultiFormatOneDReader::readers
	RuntimeObject* ___readers_2;

public:
	inline static int32_t get_offset_of_readers_2() { return static_cast<int32_t>(offsetof(MultiFormatOneDReader_t229E3DAC0D1C4281A4431E03A56CC773ED39D85D, ___readers_2)); }
	inline RuntimeObject* get_readers_2() const { return ___readers_2; }
	inline RuntimeObject** get_address_of_readers_2() { return &___readers_2; }
	inline void set_readers_2(RuntimeObject* value)
	{
		___readers_2 = value;
		Il2CppCodeGenWriteBarrier((&___readers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTIFORMATONEDREADER_T229E3DAC0D1C4281A4431E03A56CC773ED39D85D_H
#ifndef MULTIFORMATUPCEANREADER_T8964C48B44FF2BF72C8F58DB64E22E9BAFACCCBA_H
#define MULTIFORMATUPCEANREADER_T8964C48B44FF2BF72C8F58DB64E22E9BAFACCCBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.MultiFormatUPCEANReader
struct  MultiFormatUPCEANReader_t8964C48B44FF2BF72C8F58DB64E22E9BAFACCCBA  : public OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC
{
public:
	// ZXing.OneD.UPCEANReader[] ZXing.OneD.MultiFormatUPCEANReader::readers
	UPCEANReaderU5BU5D_t2B242DEDCF6486BB490E279E57C3C2C952DE1A00* ___readers_2;

public:
	inline static int32_t get_offset_of_readers_2() { return static_cast<int32_t>(offsetof(MultiFormatUPCEANReader_t8964C48B44FF2BF72C8F58DB64E22E9BAFACCCBA, ___readers_2)); }
	inline UPCEANReaderU5BU5D_t2B242DEDCF6486BB490E279E57C3C2C952DE1A00* get_readers_2() const { return ___readers_2; }
	inline UPCEANReaderU5BU5D_t2B242DEDCF6486BB490E279E57C3C2C952DE1A00** get_address_of_readers_2() { return &___readers_2; }
	inline void set_readers_2(UPCEANReaderU5BU5D_t2B242DEDCF6486BB490E279E57C3C2C952DE1A00* value)
	{
		___readers_2 = value;
		Il2CppCodeGenWriteBarrier((&___readers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTIFORMATUPCEANREADER_T8964C48B44FF2BF72C8F58DB64E22E9BAFACCCBA_H
#ifndef ABSTRACTRSSREADER_T05F5FA7968C3A85A6B9463741B94CE2A1285635C_H
#define ABSTRACTRSSREADER_T05F5FA7968C3A85A6B9463741B94CE2A1285635C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.AbstractRSSReader
struct  AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C  : public OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC
{
public:
	// System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::decodeFinderCounters
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___decodeFinderCounters_4;
	// System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::dataCharacterCounters
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___dataCharacterCounters_5;
	// System.Single[] ZXing.OneD.RSS.AbstractRSSReader::oddRoundingErrors
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___oddRoundingErrors_6;
	// System.Single[] ZXing.OneD.RSS.AbstractRSSReader::evenRoundingErrors
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___evenRoundingErrors_7;
	// System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::oddCounts
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___oddCounts_8;
	// System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::evenCounts
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___evenCounts_9;

public:
	inline static int32_t get_offset_of_decodeFinderCounters_4() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C, ___decodeFinderCounters_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_decodeFinderCounters_4() const { return ___decodeFinderCounters_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_decodeFinderCounters_4() { return &___decodeFinderCounters_4; }
	inline void set_decodeFinderCounters_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___decodeFinderCounters_4 = value;
		Il2CppCodeGenWriteBarrier((&___decodeFinderCounters_4), value);
	}

	inline static int32_t get_offset_of_dataCharacterCounters_5() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C, ___dataCharacterCounters_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_dataCharacterCounters_5() const { return ___dataCharacterCounters_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_dataCharacterCounters_5() { return &___dataCharacterCounters_5; }
	inline void set_dataCharacterCounters_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___dataCharacterCounters_5 = value;
		Il2CppCodeGenWriteBarrier((&___dataCharacterCounters_5), value);
	}

	inline static int32_t get_offset_of_oddRoundingErrors_6() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C, ___oddRoundingErrors_6)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_oddRoundingErrors_6() const { return ___oddRoundingErrors_6; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_oddRoundingErrors_6() { return &___oddRoundingErrors_6; }
	inline void set_oddRoundingErrors_6(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___oddRoundingErrors_6 = value;
		Il2CppCodeGenWriteBarrier((&___oddRoundingErrors_6), value);
	}

	inline static int32_t get_offset_of_evenRoundingErrors_7() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C, ___evenRoundingErrors_7)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_evenRoundingErrors_7() const { return ___evenRoundingErrors_7; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_evenRoundingErrors_7() { return &___evenRoundingErrors_7; }
	inline void set_evenRoundingErrors_7(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___evenRoundingErrors_7 = value;
		Il2CppCodeGenWriteBarrier((&___evenRoundingErrors_7), value);
	}

	inline static int32_t get_offset_of_oddCounts_8() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C, ___oddCounts_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_oddCounts_8() const { return ___oddCounts_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_oddCounts_8() { return &___oddCounts_8; }
	inline void set_oddCounts_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___oddCounts_8 = value;
		Il2CppCodeGenWriteBarrier((&___oddCounts_8), value);
	}

	inline static int32_t get_offset_of_evenCounts_9() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C, ___evenCounts_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_evenCounts_9() const { return ___evenCounts_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_evenCounts_9() { return &___evenCounts_9; }
	inline void set_evenCounts_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___evenCounts_9 = value;
		Il2CppCodeGenWriteBarrier((&___evenCounts_9), value);
	}
};

struct AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C_StaticFields
{
public:
	// System.Int32 ZXing.OneD.RSS.AbstractRSSReader::MAX_AVG_VARIANCE
	int32_t ___MAX_AVG_VARIANCE_2;
	// System.Int32 ZXing.OneD.RSS.AbstractRSSReader::MAX_INDIVIDUAL_VARIANCE
	int32_t ___MAX_INDIVIDUAL_VARIANCE_3;

public:
	inline static int32_t get_offset_of_MAX_AVG_VARIANCE_2() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C_StaticFields, ___MAX_AVG_VARIANCE_2)); }
	inline int32_t get_MAX_AVG_VARIANCE_2() const { return ___MAX_AVG_VARIANCE_2; }
	inline int32_t* get_address_of_MAX_AVG_VARIANCE_2() { return &___MAX_AVG_VARIANCE_2; }
	inline void set_MAX_AVG_VARIANCE_2(int32_t value)
	{
		___MAX_AVG_VARIANCE_2 = value;
	}

	inline static int32_t get_offset_of_MAX_INDIVIDUAL_VARIANCE_3() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C_StaticFields, ___MAX_INDIVIDUAL_VARIANCE_3)); }
	inline int32_t get_MAX_INDIVIDUAL_VARIANCE_3() const { return ___MAX_INDIVIDUAL_VARIANCE_3; }
	inline int32_t* get_address_of_MAX_INDIVIDUAL_VARIANCE_3() { return &___MAX_INDIVIDUAL_VARIANCE_3; }
	inline void set_MAX_INDIVIDUAL_VARIANCE_3(int32_t value)
	{
		___MAX_INDIVIDUAL_VARIANCE_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTRSSREADER_T05F5FA7968C3A85A6B9463741B94CE2A1285635C_H
#ifndef AI01DECODER_TE988CB5C4AD990EB7AFBBBFD88EE931EE4BC153E_H
#define AI01DECODER_TE988CB5C4AD990EB7AFBBBFD88EE931EE4BC153E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AI01decoder
struct  AI01decoder_tE988CB5C4AD990EB7AFBBBFD88EE931EE4BC153E  : public AbstractExpandedDecoder_t641084A96338858401AFFF4BFCFEFAD0E5F014E6
{
public:

public:
};

struct AI01decoder_tE988CB5C4AD990EB7AFBBBFD88EE931EE4BC153E_StaticFields
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::GTIN_SIZE
	int32_t ___GTIN_SIZE_2;

public:
	inline static int32_t get_offset_of_GTIN_SIZE_2() { return static_cast<int32_t>(offsetof(AI01decoder_tE988CB5C4AD990EB7AFBBBFD88EE931EE4BC153E_StaticFields, ___GTIN_SIZE_2)); }
	inline int32_t get_GTIN_SIZE_2() const { return ___GTIN_SIZE_2; }
	inline int32_t* get_address_of_GTIN_SIZE_2() { return &___GTIN_SIZE_2; }
	inline void set_GTIN_SIZE_2(int32_t value)
	{
		___GTIN_SIZE_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AI01DECODER_TE988CB5C4AD990EB7AFBBBFD88EE931EE4BC153E_H
#ifndef ANYAIDECODER_T638C5EE5E5642BB5C6A87FD7054E7DABA6DD79C8_H
#define ANYAIDECODER_T638C5EE5E5642BB5C6A87FD7054E7DABA6DD79C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AnyAIDecoder
struct  AnyAIDecoder_t638C5EE5E5642BB5C6A87FD7054E7DABA6DD79C8  : public AbstractExpandedDecoder_t641084A96338858401AFFF4BFCFEFAD0E5F014E6
{
public:

public:
};

struct AnyAIDecoder_t638C5EE5E5642BB5C6A87FD7054E7DABA6DD79C8_StaticFields
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AnyAIDecoder::HEADER_SIZE
	int32_t ___HEADER_SIZE_2;

public:
	inline static int32_t get_offset_of_HEADER_SIZE_2() { return static_cast<int32_t>(offsetof(AnyAIDecoder_t638C5EE5E5642BB5C6A87FD7054E7DABA6DD79C8_StaticFields, ___HEADER_SIZE_2)); }
	inline int32_t get_HEADER_SIZE_2() const { return ___HEADER_SIZE_2; }
	inline int32_t* get_address_of_HEADER_SIZE_2() { return &___HEADER_SIZE_2; }
	inline void set_HEADER_SIZE_2(int32_t value)
	{
		___HEADER_SIZE_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANYAIDECODER_T638C5EE5E5642BB5C6A87FD7054E7DABA6DD79C8_H
#ifndef DECODEDCHAR_TDEEEC3791ABB83FD5A00FF56B6E5485B2A7449F5_H
#define DECODEDCHAR_TDEEEC3791ABB83FD5A00FF56B6E5485B2A7449F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.DecodedChar
struct  DecodedChar_tDEEEC3791ABB83FD5A00FF56B6E5485B2A7449F5  : public DecodedObject_t57BA4BF95F496A5C794D66D54123FC45B10CAD9F
{
public:
	// System.Char ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::value
	Il2CppChar ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(DecodedChar_tDEEEC3791ABB83FD5A00FF56B6E5485B2A7449F5, ___value_1)); }
	inline Il2CppChar get_value_1() const { return ___value_1; }
	inline Il2CppChar* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Il2CppChar value)
	{
		___value_1 = value;
	}
};

struct DecodedChar_tDEEEC3791ABB83FD5A00FF56B6E5485B2A7449F5_StaticFields
{
public:
	// System.Char ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::FNC1
	Il2CppChar ___FNC1_2;

public:
	inline static int32_t get_offset_of_FNC1_2() { return static_cast<int32_t>(offsetof(DecodedChar_tDEEEC3791ABB83FD5A00FF56B6E5485B2A7449F5_StaticFields, ___FNC1_2)); }
	inline Il2CppChar get_FNC1_2() const { return ___FNC1_2; }
	inline Il2CppChar* get_address_of_FNC1_2() { return &___FNC1_2; }
	inline void set_FNC1_2(Il2CppChar value)
	{
		___FNC1_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODEDCHAR_TDEEEC3791ABB83FD5A00FF56B6E5485B2A7449F5_H
#ifndef DECODEDINFORMATION_TD144F663AE3C49E43CF53AEA9027371BC3D8F04A_H
#define DECODEDINFORMATION_TD144F663AE3C49E43CF53AEA9027371BC3D8F04A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation
struct  DecodedInformation_tD144F663AE3C49E43CF53AEA9027371BC3D8F04A  : public DecodedObject_t57BA4BF95F496A5C794D66D54123FC45B10CAD9F
{
public:
	// System.String ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::newString
	String_t* ___newString_1;
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::remainingValue
	int32_t ___remainingValue_2;
	// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::remaining
	bool ___remaining_3;

public:
	inline static int32_t get_offset_of_newString_1() { return static_cast<int32_t>(offsetof(DecodedInformation_tD144F663AE3C49E43CF53AEA9027371BC3D8F04A, ___newString_1)); }
	inline String_t* get_newString_1() const { return ___newString_1; }
	inline String_t** get_address_of_newString_1() { return &___newString_1; }
	inline void set_newString_1(String_t* value)
	{
		___newString_1 = value;
		Il2CppCodeGenWriteBarrier((&___newString_1), value);
	}

	inline static int32_t get_offset_of_remainingValue_2() { return static_cast<int32_t>(offsetof(DecodedInformation_tD144F663AE3C49E43CF53AEA9027371BC3D8F04A, ___remainingValue_2)); }
	inline int32_t get_remainingValue_2() const { return ___remainingValue_2; }
	inline int32_t* get_address_of_remainingValue_2() { return &___remainingValue_2; }
	inline void set_remainingValue_2(int32_t value)
	{
		___remainingValue_2 = value;
	}

	inline static int32_t get_offset_of_remaining_3() { return static_cast<int32_t>(offsetof(DecodedInformation_tD144F663AE3C49E43CF53AEA9027371BC3D8F04A, ___remaining_3)); }
	inline bool get_remaining_3() const { return ___remaining_3; }
	inline bool* get_address_of_remaining_3() { return &___remaining_3; }
	inline void set_remaining_3(bool value)
	{
		___remaining_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODEDINFORMATION_TD144F663AE3C49E43CF53AEA9027371BC3D8F04A_H
#ifndef DECODEDNUMERIC_T936D82B0E56368429CFA9A75971DC972B75415D7_H
#define DECODEDNUMERIC_T936D82B0E56368429CFA9A75971DC972B75415D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric
struct  DecodedNumeric_t936D82B0E56368429CFA9A75971DC972B75415D7  : public DecodedObject_t57BA4BF95F496A5C794D66D54123FC45B10CAD9F
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::firstDigit
	int32_t ___firstDigit_1;
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::secondDigit
	int32_t ___secondDigit_2;

public:
	inline static int32_t get_offset_of_firstDigit_1() { return static_cast<int32_t>(offsetof(DecodedNumeric_t936D82B0E56368429CFA9A75971DC972B75415D7, ___firstDigit_1)); }
	inline int32_t get_firstDigit_1() const { return ___firstDigit_1; }
	inline int32_t* get_address_of_firstDigit_1() { return &___firstDigit_1; }
	inline void set_firstDigit_1(int32_t value)
	{
		___firstDigit_1 = value;
	}

	inline static int32_t get_offset_of_secondDigit_2() { return static_cast<int32_t>(offsetof(DecodedNumeric_t936D82B0E56368429CFA9A75971DC972B75415D7, ___secondDigit_2)); }
	inline int32_t get_secondDigit_2() const { return ___secondDigit_2; }
	inline int32_t* get_address_of_secondDigit_2() { return &___secondDigit_2; }
	inline void set_secondDigit_2(int32_t value)
	{
		___secondDigit_2 = value;
	}
};

struct DecodedNumeric_t936D82B0E56368429CFA9A75971DC972B75415D7_StaticFields
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::FNC1
	int32_t ___FNC1_3;

public:
	inline static int32_t get_offset_of_FNC1_3() { return static_cast<int32_t>(offsetof(DecodedNumeric_t936D82B0E56368429CFA9A75971DC972B75415D7_StaticFields, ___FNC1_3)); }
	inline int32_t get_FNC1_3() const { return ___FNC1_3; }
	inline int32_t* get_address_of_FNC1_3() { return &___FNC1_3; }
	inline void set_FNC1_3(int32_t value)
	{
		___FNC1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODEDNUMERIC_T936D82B0E56368429CFA9A75971DC972B75415D7_H
#ifndef PAIR_TB6F73D2714D821ED407BDA773D0B4A82803DB886_H
#define PAIR_TB6F73D2714D821ED407BDA773D0B4A82803DB886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Pair
struct  Pair_tB6F73D2714D821ED407BDA773D0B4A82803DB886  : public DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB
{
public:
	// ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.Pair::<FinderPattern>k__BackingField
	FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF * ___U3CFinderPatternU3Ek__BackingField_2;
	// System.Int32 ZXing.OneD.RSS.Pair::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CFinderPatternU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Pair_tB6F73D2714D821ED407BDA773D0B4A82803DB886, ___U3CFinderPatternU3Ek__BackingField_2)); }
	inline FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF * get_U3CFinderPatternU3Ek__BackingField_2() const { return ___U3CFinderPatternU3Ek__BackingField_2; }
	inline FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF ** get_address_of_U3CFinderPatternU3Ek__BackingField_2() { return &___U3CFinderPatternU3Ek__BackingField_2; }
	inline void set_U3CFinderPatternU3Ek__BackingField_2(FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF * value)
	{
		___U3CFinderPatternU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFinderPatternU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Pair_tB6F73D2714D821ED407BDA773D0B4A82803DB886, ___U3CCountU3Ek__BackingField_3)); }
	inline int32_t get_U3CCountU3Ek__BackingField_3() const { return ___U3CCountU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_3() { return &___U3CCountU3Ek__BackingField_3; }
	inline void set_U3CCountU3Ek__BackingField_3(int32_t value)
	{
		___U3CCountU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIR_TB6F73D2714D821ED407BDA773D0B4A82803DB886_H
#ifndef UPCEANREADER_TCF0226E6411C30A8399CF5A7341C02236F096B6D_H
#define UPCEANREADER_TCF0226E6411C30A8399CF5A7341C02236F096B6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.UPCEANReader
struct  UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D  : public OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC
{
public:
	// System.Text.StringBuilder ZXing.OneD.UPCEANReader::decodeRowStringBuffer
	StringBuilder_t * ___decodeRowStringBuffer_9;
	// ZXing.OneD.UPCEANExtensionSupport ZXing.OneD.UPCEANReader::extensionReader
	UPCEANExtensionSupport_tCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B * ___extensionReader_10;
	// ZXing.OneD.EANManufacturerOrgSupport ZXing.OneD.UPCEANReader::eanManSupport
	EANManufacturerOrgSupport_t1BFBF3CDCC49219358CD56548F8FD730DD2C35A7 * ___eanManSupport_11;

public:
	inline static int32_t get_offset_of_decodeRowStringBuffer_9() { return static_cast<int32_t>(offsetof(UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D, ___decodeRowStringBuffer_9)); }
	inline StringBuilder_t * get_decodeRowStringBuffer_9() const { return ___decodeRowStringBuffer_9; }
	inline StringBuilder_t ** get_address_of_decodeRowStringBuffer_9() { return &___decodeRowStringBuffer_9; }
	inline void set_decodeRowStringBuffer_9(StringBuilder_t * value)
	{
		___decodeRowStringBuffer_9 = value;
		Il2CppCodeGenWriteBarrier((&___decodeRowStringBuffer_9), value);
	}

	inline static int32_t get_offset_of_extensionReader_10() { return static_cast<int32_t>(offsetof(UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D, ___extensionReader_10)); }
	inline UPCEANExtensionSupport_tCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B * get_extensionReader_10() const { return ___extensionReader_10; }
	inline UPCEANExtensionSupport_tCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B ** get_address_of_extensionReader_10() { return &___extensionReader_10; }
	inline void set_extensionReader_10(UPCEANExtensionSupport_tCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B * value)
	{
		___extensionReader_10 = value;
		Il2CppCodeGenWriteBarrier((&___extensionReader_10), value);
	}

	inline static int32_t get_offset_of_eanManSupport_11() { return static_cast<int32_t>(offsetof(UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D, ___eanManSupport_11)); }
	inline EANManufacturerOrgSupport_t1BFBF3CDCC49219358CD56548F8FD730DD2C35A7 * get_eanManSupport_11() const { return ___eanManSupport_11; }
	inline EANManufacturerOrgSupport_t1BFBF3CDCC49219358CD56548F8FD730DD2C35A7 ** get_address_of_eanManSupport_11() { return &___eanManSupport_11; }
	inline void set_eanManSupport_11(EANManufacturerOrgSupport_t1BFBF3CDCC49219358CD56548F8FD730DD2C35A7 * value)
	{
		___eanManSupport_11 = value;
		Il2CppCodeGenWriteBarrier((&___eanManSupport_11), value);
	}
};

struct UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields
{
public:
	// System.Int32 ZXing.OneD.UPCEANReader::MAX_AVG_VARIANCE
	int32_t ___MAX_AVG_VARIANCE_2;
	// System.Int32 ZXing.OneD.UPCEANReader::MAX_INDIVIDUAL_VARIANCE
	int32_t ___MAX_INDIVIDUAL_VARIANCE_3;
	// System.Int32[] ZXing.OneD.UPCEANReader::START_END_PATTERN
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___START_END_PATTERN_4;
	// System.Int32[] ZXing.OneD.UPCEANReader::MIDDLE_PATTERN
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___MIDDLE_PATTERN_5;
	// System.Int32[] ZXing.OneD.UPCEANReader::END_PATTERN
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___END_PATTERN_6;
	// System.Int32[][] ZXing.OneD.UPCEANReader::L_PATTERNS
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___L_PATTERNS_7;
	// System.Int32[][] ZXing.OneD.UPCEANReader::L_AND_G_PATTERNS
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___L_AND_G_PATTERNS_8;

public:
	inline static int32_t get_offset_of_MAX_AVG_VARIANCE_2() { return static_cast<int32_t>(offsetof(UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields, ___MAX_AVG_VARIANCE_2)); }
	inline int32_t get_MAX_AVG_VARIANCE_2() const { return ___MAX_AVG_VARIANCE_2; }
	inline int32_t* get_address_of_MAX_AVG_VARIANCE_2() { return &___MAX_AVG_VARIANCE_2; }
	inline void set_MAX_AVG_VARIANCE_2(int32_t value)
	{
		___MAX_AVG_VARIANCE_2 = value;
	}

	inline static int32_t get_offset_of_MAX_INDIVIDUAL_VARIANCE_3() { return static_cast<int32_t>(offsetof(UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields, ___MAX_INDIVIDUAL_VARIANCE_3)); }
	inline int32_t get_MAX_INDIVIDUAL_VARIANCE_3() const { return ___MAX_INDIVIDUAL_VARIANCE_3; }
	inline int32_t* get_address_of_MAX_INDIVIDUAL_VARIANCE_3() { return &___MAX_INDIVIDUAL_VARIANCE_3; }
	inline void set_MAX_INDIVIDUAL_VARIANCE_3(int32_t value)
	{
		___MAX_INDIVIDUAL_VARIANCE_3 = value;
	}

	inline static int32_t get_offset_of_START_END_PATTERN_4() { return static_cast<int32_t>(offsetof(UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields, ___START_END_PATTERN_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_START_END_PATTERN_4() const { return ___START_END_PATTERN_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_START_END_PATTERN_4() { return &___START_END_PATTERN_4; }
	inline void set_START_END_PATTERN_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___START_END_PATTERN_4 = value;
		Il2CppCodeGenWriteBarrier((&___START_END_PATTERN_4), value);
	}

	inline static int32_t get_offset_of_MIDDLE_PATTERN_5() { return static_cast<int32_t>(offsetof(UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields, ___MIDDLE_PATTERN_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_MIDDLE_PATTERN_5() const { return ___MIDDLE_PATTERN_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_MIDDLE_PATTERN_5() { return &___MIDDLE_PATTERN_5; }
	inline void set_MIDDLE_PATTERN_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___MIDDLE_PATTERN_5 = value;
		Il2CppCodeGenWriteBarrier((&___MIDDLE_PATTERN_5), value);
	}

	inline static int32_t get_offset_of_END_PATTERN_6() { return static_cast<int32_t>(offsetof(UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields, ___END_PATTERN_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_END_PATTERN_6() const { return ___END_PATTERN_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_END_PATTERN_6() { return &___END_PATTERN_6; }
	inline void set_END_PATTERN_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___END_PATTERN_6 = value;
		Il2CppCodeGenWriteBarrier((&___END_PATTERN_6), value);
	}

	inline static int32_t get_offset_of_L_PATTERNS_7() { return static_cast<int32_t>(offsetof(UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields, ___L_PATTERNS_7)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_L_PATTERNS_7() const { return ___L_PATTERNS_7; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_L_PATTERNS_7() { return &___L_PATTERNS_7; }
	inline void set_L_PATTERNS_7(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___L_PATTERNS_7 = value;
		Il2CppCodeGenWriteBarrier((&___L_PATTERNS_7), value);
	}

	inline static int32_t get_offset_of_L_AND_G_PATTERNS_8() { return static_cast<int32_t>(offsetof(UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields, ___L_AND_G_PATTERNS_8)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_L_AND_G_PATTERNS_8() const { return ___L_AND_G_PATTERNS_8; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_L_AND_G_PATTERNS_8() { return &___L_AND_G_PATTERNS_8; }
	inline void set_L_AND_G_PATTERNS_8(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___L_AND_G_PATTERNS_8 = value;
		Il2CppCodeGenWriteBarrier((&___L_AND_G_PATTERNS_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPCEANREADER_TCF0226E6411C30A8399CF5A7341C02236F096B6D_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::00E14C77230AEF0974CFF3930481157AABDA07B4
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___00E14C77230AEF0974CFF3930481157AABDA07B4_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::01898B76F7C45E723996C7E8EDF45429F10AB352
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___01898B76F7C45E723996C7E8EDF45429F10AB352_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::01A7BB0BE820D25E52A42C3D28DE27822EABCD4C
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___01A7BB0BE820D25E52A42C3D28DE27822EABCD4C_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::0222E151247DAE31A8D697EAA14F43F718BD1F1C
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___0222E151247DAE31A8D697EAA14F43F718BD1F1C_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::03AB6BB65AC7B8ECF309D37CACECE4B53B38D824
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___03AB6BB65AC7B8ECF309D37CACECE4B53B38D824_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=80 <PrivateImplementationDetails>::0443C73B92DF9C67128EF5F0702DCBC469EDD2EA
	__StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260  ___0443C73B92DF9C67128EF5F0702DCBC469EDD2EA_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::061E1E805CBF51D839C3B91E8ACD0326E5C77FFE
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::07438A85BDEE90305A6125B92084A5F3F2FD5F18
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___07438A85BDEE90305A6125B92084A5F3F2FD5F18_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::094D7DFD41161273E784D60EAE6D844127D77F0B
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___094D7DFD41161273E784D60EAE6D844127D77F0B_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::09E1F33A3C65971119BBEF1ED121FE70EA9CFA5C
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___09E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::0BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___0BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::0BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___0BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::0C7D347178E5302733AED896AA3A7B5DDEDC3A90
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___0C7D347178E5302733AED896AA3A7B5DDEDC3A90_12;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::0D0AD555C6DA0BD4268F8573DC103F455F62A610
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___0D0AD555C6DA0BD4268F8573DC103F455F62A610_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44 <PrivateImplementationDetails>::0E4585982EA19664C33EA213EE69EC548F7322B0
	__StaticArrayInitTypeSizeU3D44_tE280582A6EEC87F26198EDC0F6EC802BBCD3D015  ___0E4585982EA19664C33EA213EE69EC548F7322B0_14;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::10111111FB24B2D331FB3F698193046CBCB8AAFE
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___10111111FB24B2D331FB3F698193046CBCB8AAFE_15;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::1037DC6EAB9D7D3850660726ED132EAD961A41E9
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___1037DC6EAB9D7D3850660726ED132EAD961A41E9_16;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::10486CAECBA5E73DA05910F94513409DEB6316FD
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___10486CAECBA5E73DA05910F94513409DEB6316FD_17;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::11C3F78703162740B961B75ED808973DD68F7FA8
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___11C3F78703162740B961B75ED808973DD68F7FA8_18;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::11D4D9FC526D047E01891BEE9949EA42408A800E
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___11D4D9FC526D047E01891BEE9949EA42408A800E_19;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::12625AC94FB97DAA377449A877D25786C8ADFFC5
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___12625AC94FB97DAA377449A877D25786C8ADFFC5_20;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::12F1DB2AC68CD455638374E8DF9E9D604B15F693
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___12F1DB2AC68CD455638374E8DF9E9D604B15F693_21;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::142E1469231D592823559059166B32655910D0CF
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___142E1469231D592823559059166B32655910D0CF_22;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::185FAB3B3D8AC0F1CBECFFCD7DEA45EE4AC9E357
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___185FAB3B3D8AC0F1CBECFFCD7DEA45EE4AC9E357_23;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::186E7643E22CF0E2479E92C738FEBEF56B275E95
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___186E7643E22CF0E2479E92C738FEBEF56B275E95_24;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::188E7A480B871C554F3F805D3E1184673960EED5
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___188E7A480B871C554F3F805D3E1184673960EED5_25;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::193330123B5DF8BA7DDD1FD73E3941FD113C71C7
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___193330123B5DF8BA7DDD1FD73E3941FD113C71C7_26;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::1A3C01816F6E750EC65FDEF4FEB7758235456890
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___1A3C01816F6E750EC65FDEF4FEB7758235456890_27;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::1A85140B8FCA30191EE9347F53782234F0781F79
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___1A85140B8FCA30191EE9347F53782234F0781F79_28;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::1BCA1A912FF22DA69AA3E91A68492D4BA5179162
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___1BCA1A912FF22DA69AA3E91A68492D4BA5179162_29;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::1D0501D14FE31729CD75A4073F4404AF7E53B024
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___1D0501D14FE31729CD75A4073F4404AF7E53B024_30;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::1D562AD827598FC3F5C82FC2091E65C8F8A07ADC
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___1D562AD827598FC3F5C82FC2091E65C8F8A07ADC_31;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::1DB4EC083D80319093844E5CD9A7EB62EB1DC8D9
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___1DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_32;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::1DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___1DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_33;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::1E45F828069EAF87A0B553BE1E65B98F20E377ED
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___1E45F828069EAF87A0B553BE1E65B98F20E377ED_34;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::1EB30BCD6A1576A001C2906D3AF45955E19C48BC
	__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  ___1EB30BCD6A1576A001C2906D3AF45955E19C48BC_35;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::1F5771BF2009B14CC123999096615351A0BE9527
	__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  ___1F5771BF2009B14CC123999096615351A0BE9527_36;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::1FFDEC0F27BC9DCF708F067786205F164F8A6D32
	__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  ___1FFDEC0F27BC9DCF708F067786205F164F8A6D32_37;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::20D740A7E661D50B06DC32EF6577367D264F6235
	__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  ___20D740A7E661D50B06DC32EF6577367D264F6235_38;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::227C649F55292B0A293EBB2D56AB8ABAECC7F629
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___227C649F55292B0A293EBB2D56AB8ABAECC7F629_39;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::22BF5B86DC5E0B87331903141C5973C117DA716E
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___22BF5B86DC5E0B87331903141C5973C117DA716E_40;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::2309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___2309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_41;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::2357B210FC2522A4A9D0F465AEAC250A79FA9449
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___2357B210FC2522A4A9D0F465AEAC250A79FA9449_42;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::23D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___23D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_43;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::241FC42CDC9A5435B317D1D425FB9AB8A93B16CD
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_44;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::24E7D35141138E4668DE547A98CE3A84AF0E6FEB
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___24E7D35141138E4668DE547A98CE3A84AF0E6FEB_45;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::259B7EF54A535F34001B95480B4BB11245EA41B5
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___259B7EF54A535F34001B95480B4BB11245EA41B5_46;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::2861CF9E030EE4DA3C1BA4824C08FA40377E2C1A
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___2861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_47;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::28D1E3B5188FE552D37912314E419E7F30ABD844
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___28D1E3B5188FE552D37912314E419E7F30ABD844_48;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::28F991026781E1BC119F02498D75FB3A84F4ED37
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___28F991026781E1BC119F02498D75FB3A84F4ED37_49;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::2923D4191FF7FBBF11BFC643B6D93CC8074D31E1
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___2923D4191FF7FBBF11BFC643B6D93CC8074D31E1_50;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::29AB17251F5A730B539AB351FFCB45B9F2FA8027
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___29AB17251F5A730B539AB351FFCB45B9F2FA8027_51;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::2A84324D54FDC0473BE021004C50F363ADCD89AF
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___2A84324D54FDC0473BE021004C50F363ADCD89AF_52;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::2AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___2AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_53;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::2D3DF989BE3BBA2B353F845C37992DFF85579505
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___2D3DF989BE3BBA2B353F845C37992DFF85579505_54;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::2DFCD7C13A7339CC41C473220CE80CDD5933C64C
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___2DFCD7C13A7339CC41C473220CE80CDD5933C64C_55;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::2E8B473A712423E983C2B2EDA43927EB0A14F5B4
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___2E8B473A712423E983C2B2EDA43927EB0A14F5B4_56;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::2EC69895F74DBAE74600E960EFFB6448597BDEEB
	__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  ___2EC69895F74DBAE74600E960EFFB6448597BDEEB_57;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D
	__StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  ___300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_58;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::3102377CA043644BFD52C56AE2A9F554723263FD
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___3102377CA043644BFD52C56AE2A9F554723263FD_59;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::3199DD35AA6A041AF9F66BFBAEC3563E838D3731
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___3199DD35AA6A041AF9F66BFBAEC3563E838D3731_60;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::322BC3569CD0270EC3179B30382F24148B533D10
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___322BC3569CD0270EC3179B30382F24148B533D10_61;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=156 <PrivateImplementationDetails>::33B8540230E7366A64AC520E4F819103EB00ECD0
	__StaticArrayInitTypeSizeU3D156_tD444739979C2F1A4BE28790BC71DD6FB876CCD25  ___33B8540230E7366A64AC520E4F819103EB00ECD0_62;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::34617E1F9E901BD9DD9FEB1839483F5ED406E7B3
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___34617E1F9E901BD9DD9FEB1839483F5ED406E7B3_63;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::35859E8329070E55C5D9282930023DD6B1594141
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___35859E8329070E55C5D9282930023DD6B1594141_64;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::35E8FEC2F20CD20788D4696BE84FFE01B7F8CF49
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___35E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_65;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::35FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8
	__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  ___35FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_66;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::38017E50AF503681BFC2BD0CDED5F7565471B574
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___38017E50AF503681BFC2BD0CDED5F7565471B574_67;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::38CAE5BC1B31E24A88061CEC362093BB4D8F0523
	__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  ___38CAE5BC1B31E24A88061CEC362093BB4D8F0523_68;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=54 <PrivateImplementationDetails>::3A6FC30DA7BC0C272E78EACD5C7EC8448B09019C
	__StaticArrayInitTypeSizeU3D54_t1ACEB6759550166F62217D005B9DC6C35D54C265  ___3A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_69;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::3C22A633B79B510613E37BC5A02FF91A4AB006ED
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___3C22A633B79B510613E37BC5A02FF91A4AB006ED_70;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::3D3C46CC589F65C2443688952CBCD9C499839622
	__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  ___3D3C46CC589F65C2443688952CBCD9C499839622_71;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::3D8521A958BBFCC708EE0728993AE8CAFEB154D6
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___3D8521A958BBFCC708EE0728993AE8CAFEB154D6_72;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::3DF543E51AB43F1F561B11B8789C82E26C427F72
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___3DF543E51AB43F1F561B11B8789C82E26C427F72_73;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=11148 <PrivateImplementationDetails>::3E12851A90C6A08ACCD1D67634AC7FD1639B898D
	__StaticArrayInitTypeSizeU3D11148_tA414153821EB70E6FCD456B1E2DB309D58CE1619  ___3E12851A90C6A08ACCD1D67634AC7FD1639B898D_74;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::3E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD
	__StaticArrayInitTypeSizeU3D64_t2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF  ___3E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_75;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::3E8C3886467AEC93954B0B1F23B4969CE5FDB3A1
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___3E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_76;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::3F152B3DFAF72A55D78439E5015C55A20967621A
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___3F152B3DFAF72A55D78439E5015C55A20967621A_77;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::3F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___3F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_78;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::3F6E4F79F4A57C7F0F4467AA7116C5C46FDFB54E
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___3F6E4F79F4A57C7F0F4467AA7116C5C46FDFB54E_79;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::4065D9695EFBED47DDA95610295BA85121D5A17B
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___4065D9695EFBED47DDA95610295BA85121D5A17B_80;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=136 <PrivateImplementationDetails>::40B82CCF037B966E2A6B5F4BAC86904796965A1C
	__StaticArrayInitTypeSizeU3D136_tF639EB419908A28312DAF13451C27B4ED03E8674  ___40B82CCF037B966E2A6B5F4BAC86904796965A1C_81;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::40FEF102AB67B5038E6D9E2EE2BE16854A49810D
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___40FEF102AB67B5038E6D9E2EE2BE16854A49810D_82;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=176 <PrivateImplementationDetails>::42A99199D41E8832E72CD942706DDA326C0BB356
	__StaticArrayInitTypeSizeU3D176_t9A793B7CC486996BB07636533D64428F9FE21236  ___42A99199D41E8832E72CD942706DDA326C0BB356_83;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::445E0C138E7D9647FCD08184FB23A833C4D00D0D
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___445E0C138E7D9647FCD08184FB23A833C4D00D0D_84;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::44DB2483C3365FFD6E86B5C807C599AAF3EEE238
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___44DB2483C3365FFD6E86B5C807C599AAF3EEE238_85;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_86;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::463C3B3709985BB32747ACC9AE3406BB6836368B
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___463C3B3709985BB32747ACC9AE3406BB6836368B_87;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::465B4A79F3CD93D22FD6AFE37CB544AC27728D1D
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_88;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::471648086F9480CB553B7802323E0A75FFE09C5A
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___471648086F9480CB553B7802323E0A75FFE09C5A_89;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::484B207F3CBCE8338BDE26F04E87EE256891BE0F
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___484B207F3CBCE8338BDE26F04E87EE256891BE0F_90;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::48F073C6CC618A8FDA6FD23ABFDC828CC6A5812E
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___48F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_91;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::48F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___48F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_92;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::49594851C4B6C279C35B162F0236A99E7C31DD36
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___49594851C4B6C279C35B162F0236A99E7C31DD36_93;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::49E2034AE9BD046DF074F7FA4ACAE05980D1DC6F
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___49E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_94;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::4B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___4B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_95;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::4CC0A0F757356B175679AE3FDB722375172ABA96
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___4CC0A0F757356B175679AE3FDB722375172ABA96_96;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::4CCAF842695DCCF7F7A9EC0D641DC36B38937FA3
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___4CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_97;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::4D317202204E9741354BC1A1628BC10A05E2AB7B
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___4D317202204E9741354BC1A1628BC10A05E2AB7B_98;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::4DE742FEB32578958FCFC33D6732EFF616C8A94B
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___4DE742FEB32578958FCFC33D6732EFF616C8A94B_99;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::501D7C551BDFD3885F36478424EEE5B72766C842
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___501D7C551BDFD3885F36478424EEE5B72766C842_100;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::50DACD1562B0163C9FC5B673478D4231A4B37B1F
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___50DACD1562B0163C9FC5B673478D4231A4B37B1F_101;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::51109782EA6E98E8E277657055A598684D21520E
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___51109782EA6E98E8E277657055A598684D21520E_102;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::526BB16C9A1C821B19858E18142A1F7073BC648D
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___526BB16C9A1C821B19858E18142A1F7073BC648D_103;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::52EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___52EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_104;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::53173106B161530CA26C69983B44BBE2FAB890D0
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___53173106B161530CA26C69983B44BBE2FAB890D0_105;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::539B26D17C2DEB88FB9E07611D164408E23C35AB
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___539B26D17C2DEB88FB9E07611D164408E23C35AB_106;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::53D98E6046BB88C14DD740E4F77B16402271243B
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___53D98E6046BB88C14DD740E4F77B16402271243B_107;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C
	__StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  ___553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_108;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::55A6E22CC24364881F62CCF67CBE9A348DC602FA
	__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  ___55A6E22CC24364881F62CCF67CBE9A348DC602FA_109;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::56A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___56A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_110;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::56F75F925774608B6BF88F69D6124CB44DDF42E1
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___56F75F925774608B6BF88F69D6124CB44DDF42E1_111;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::5741ABC064FCDEF4C5EE02F31DB992C205EA7231
	__StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  ___5741ABC064FCDEF4C5EE02F31DB992C205EA7231_112;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::583A438479F42C7D8DE8C6699B0C37A4A026A643
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___583A438479F42C7D8DE8C6699B0C37A4A026A643_113;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::58B2CCE2545886B61E58C3C60831C2B049814E09
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___58B2CCE2545886B61E58C3C60831C2B049814E09_114;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::5984617C3B1EF1E6C9F13221059A1E78C2226ADF
	__StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  ___5984617C3B1EF1E6C9F13221059A1E78C2226ADF_115;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::59A84F660FFFFA4B9C037456F9D56E26ACF3663E
	__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  ___59A84F660FFFFA4B9C037456F9D56E26ACF3663E_116;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::5B02FED4CA0B61476F1AAA5C6445F024374B075A
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___5B02FED4CA0B61476F1AAA5C6445F024374B075A_117;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::5B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___5B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_118;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::5BCFEA149A36030B8B3EB1FAE31AF9173B76B87C
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___5BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_119;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD
	__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  ___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_120;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::5C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___5C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_121;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::5C563BFAB6400B2824F5E41D78FB3097E8244C26
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___5C563BFAB6400B2824F5E41D78FB3097E8244C26_122;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::5CB924E7461B9011EC8197A30815EA263595B3A4
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___5CB924E7461B9011EC8197A30815EA263595B3A4_123;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::5D160627930BB84F7867AEEC7143FB2399C7CA79
	__StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  ___5D160627930BB84F7867AEEC7143FB2399C7CA79_124;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::5DD69D93CBAA5623AE286E97265890A7693A0AC2
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___5DD69D93CBAA5623AE286E97265890A7693A0AC2_125;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::5E3F56450E340B21A3A13E39A66941353F8C7C1A
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___5E3F56450E340B21A3A13E39A66941353F8C7C1A_126;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::5FD971B7E6A431E6F3F91D5A95F2006A24F48780
	__StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  ___5FD971B7E6A431E6F3F91D5A95F2006A24F48780_127;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::60E3C19D774F8C210299534335ED5D3154717218
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___60E3C19D774F8C210299534335ED5D3154717218_128;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::61A1021606CAD0A5990E00FD305478E75F1983D6
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___61A1021606CAD0A5990E00FD305478E75F1983D6_129;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::62AB9401F4CB2B0A7FF72D3F3F56AA2F14E34FE0
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___62AB9401F4CB2B0A7FF72D3F3F56AA2F14E34FE0_130;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::63B762373DE81B4580E1909D7168A21A989A182A
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___63B762373DE81B4580E1909D7168A21A989A182A_131;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::648C489642395164C2A2AA2379A6E7F44CAB018B
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___648C489642395164C2A2AA2379A6E7F44CAB018B_132;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::661F9E9FEA16C3A9CDF452AB61743578471F7447
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___661F9E9FEA16C3A9CDF452AB61743578471F7447_133;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::67013075958AAD75C4545796BF80B16683B33A59
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___67013075958AAD75C4545796BF80B16683B33A59_134;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::6727D77DB74B08CC9003029AC92B7AC68E258811
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___6727D77DB74B08CC9003029AC92B7AC68E258811_135;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::6882914B984B24C4880E3BEA646DC549B3C34ABC
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___6882914B984B24C4880E3BEA646DC549B3C34ABC_136;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::6891D97C19F4C291AD821EF05E4C5D1BD3C34796
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___6891D97C19F4C291AD821EF05E4C5D1BD3C34796_137;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::69499AF6438C222AAADFC92FF3D3DF3902A454FC
	__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  ___69499AF6438C222AAADFC92FF3D3DF3902A454FC_138;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::6957A133E0DC21707BCAAF62194EAF4FF75E0768
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___6957A133E0DC21707BCAAF62194EAF4FF75E0768_139;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::69D489A8DADAF0948858B02F2CC7258B1530D843
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___69D489A8DADAF0948858B02F2CC7258B1530D843_140;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::6A09CB785087F5A9F6EC748C7C10F98FA2683534
	__StaticArrayInitTypeSizeU3D36_t2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17  ___6A09CB785087F5A9F6EC748C7C10F98FA2683534_141;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::6AA66657BB3292572C8F56AB3D015A7990C43606
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___6AA66657BB3292572C8F56AB3D015A7990C43606_142;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::6AF652181FBB423ED995C5CF63D5B83726DE74CF
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___6AF652181FBB423ED995C5CF63D5B83726DE74CF_143;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::6B38D84409B2091CBC93686439AC2C04C76DB96E
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___6B38D84409B2091CBC93686439AC2C04C76DB96E_144;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::6CC1359B9ED72500183D5566D70BBEE1BBD46FFF
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___6CC1359B9ED72500183D5566D70BBEE1BBD46FFF_145;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::6CEDD7504235CF66C53FB70A17ACBDD440D77C62
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___6CEDD7504235CF66C53FB70A17ACBDD440D77C62_146;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::6DE89499E8F85867289E1690E176E1DE58EE6DD3
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___6DE89499E8F85867289E1690E176E1DE58EE6DD3_147;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::6E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___6E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_148;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::6F604C33212E20CBFCE9F2221492DC6FD823857C
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___6F604C33212E20CBFCE9F2221492DC6FD823857C_149;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::70CFE27207DDB1C62DD3A9DA22F21F3B1831D7D5
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___70CFE27207DDB1C62DD3A9DA22F21F3B1831D7D5_150;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::7102A07EDF52C052D5625D73275A579DC6E76C92
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___7102A07EDF52C052D5625D73275A579DC6E76C92_151;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::712B24DD3C17671C78BE33C83316576AD9285273
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___712B24DD3C17671C78BE33C83316576AD9285273_152;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::71D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___71D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_153;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::71F4D547D52F31EDEC55B1909114455FDAE5A97E
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___71F4D547D52F31EDEC55B1909114455FDAE5A97E_154;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::72CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___72CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_155;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::72FF92F22AB5BD5A344C652E176645C52D825AEC
	__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  ___72FF92F22AB5BD5A344C652E176645C52D825AEC_156;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::73B101BBEDC241CE765CA03BB6E4D467FA769312
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___73B101BBEDC241CE765CA03BB6E4D467FA769312_157;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::749EB212DEC39495349481F49F3DEF480777A197
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___749EB212DEC39495349481F49F3DEF480777A197_158;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=2574 <PrivateImplementationDetails>::74AE47B2D78B62583BA50045F6B288CACC24EA7D
	__StaticArrayInitTypeSizeU3D2574_t94A56C994C4EA1F17B7D51314A88B67247C9BC15  ___74AE47B2D78B62583BA50045F6B288CACC24EA7D_159;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::7616E4F44CA76DF539C11B9C8FD911D667FEFC52
	__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  ___7616E4F44CA76DF539C11B9C8FD911D667FEFC52_160;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::761EC2969B9D1960DB3CB7FC05EEC96944B59BC6
	__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  ___761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_161;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::76D7CB1729978C5437409A8C56AA1C7E24890D5C
	__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  ___76D7CB1729978C5437409A8C56AA1C7E24890D5C_162;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::770616E567083077933DC0B29A8F8368FC2EB431
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___770616E567083077933DC0B29A8F8368FC2EB431_163;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::776EDFD0860A21D5119CA197C89BD2AC7D6F9283
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___776EDFD0860A21D5119CA197C89BD2AC7D6F9283_164;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319
	__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  ___783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_165;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::79123EF44D00F5B0ED365348D1FD49F9FA2CB222
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___79123EF44D00F5B0ED365348D1FD49F9FA2CB222_166;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::79A4B717DAF4312A78F1A75A72221DB9690D1B25
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___79A4B717DAF4312A78F1A75A72221DB9690D1B25_167;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::79BEC582299CE5F46902B9C091C19931E26D9323
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___79BEC582299CE5F46902B9C091C19931E26D9323_168;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::79F14B68470B4B7AA3BCDCA6758072F2B9320320
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___79F14B68470B4B7AA3BCDCA6758072F2B9320320_169;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::79F9F064F81690C950BF4EFFAA5245BA966607C9
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___79F9F064F81690C950BF4EFFAA5245BA966607C9_170;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::7BC6404D48DA137303466471F7131DC1C9B13BAA
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___7BC6404D48DA137303466471F7131DC1C9B13BAA_171;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::7CF92A83536E2264EF41D8C407D246E41E2DBCD6
	__StaticArrayInitTypeSizeU3D36_t2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17  ___7CF92A83536E2264EF41D8C407D246E41E2DBCD6_172;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::7DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___7DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_173;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=192 <PrivateImplementationDetails>::7E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739
	__StaticArrayInitTypeSizeU3D192_tCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36  ___7E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_174;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::7ECDF8C5F474CFF2B0970E93C397B504DD793D2C
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___7ECDF8C5F474CFF2B0970E93C397B504DD793D2C_175;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::7FDC1CA6ED532684E45DDAE8484274399FCE4C59
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___7FDC1CA6ED532684E45DDAE8484274399FCE4C59_176;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::7FF393DF5D10580FA38BF854328B3072169E0A1A
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___7FF393DF5D10580FA38BF854328B3072169E0A1A_177;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::802AD21B222E489DAFEADD5E18868203D9FC2A89
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___802AD21B222E489DAFEADD5E18868203D9FC2A89_178;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::806D200653BD7335D0077F2759E715D8BE8DF9BF
	__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  ___806D200653BD7335D0077F2759E715D8BE8DF9BF_179;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::808AD19CA818077F71100880E7D7AA21147E987E
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___808AD19CA818077F71100880E7D7AA21147E987E_180;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::80DD6A282D563836522C10A78AAA17F0B865EB5A
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___80DD6A282D563836522C10A78AAA17F0B865EB5A_181;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::80E21576779C61DA5B185D15A732CF7A8BD6F88C
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___80E21576779C61DA5B185D15A732CF7A8BD6F88C_182;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::80E9217618F8C2C7FED61E4E7653CD0E66C74872
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___80E9217618F8C2C7FED61E4E7653CD0E66C74872_183;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::80F763BA90E27F755F2DEFA7FE720AD9371D6E6B
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___80F763BA90E27F755F2DEFA7FE720AD9371D6E6B_184;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::836DA48724BC66F3257E70FFF165ACCE037666A6
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___836DA48724BC66F3257E70FFF165ACCE037666A6_185;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::8523C737ED49D8A2E9BB85218190E4E83B902E28
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___8523C737ED49D8A2E9BB85218190E4E83B902E28_186;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::85666B25692AB814E91F685384EEA0389F147E70
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___85666B25692AB814E91F685384EEA0389F147E70_187;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::866652192B68681CC538B2CB21C5979544DF35AB
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___866652192B68681CC538B2CB21C5979544DF35AB_188;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::8683FBE90F936E0F2A1080E053EBA3EE9F44A02B
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___8683FBE90F936E0F2A1080E053EBA3EE9F44A02B_189;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::868C8972C4058443A2D131C22083401956DF81C7
	__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  ___868C8972C4058443A2D131C22083401956DF81C7_190;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::86EA004891DE06B357581B1885C9C0EFD73DE2E9
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___86EA004891DE06B357581B1885C9C0EFD73DE2E9_191;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::881C2E8F71772ECA0CFC4DB357196C4EB788D6F8
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_192;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::88B6AF1079C72775E54D0AC3BEEDCCC6506B6269
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___88B6AF1079C72775E54D0AC3BEEDCCC6506B6269_193;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::88CFCB31ED9AFFB90483973D02D5DEE8A82A4892
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___88CFCB31ED9AFFB90483973D02D5DEE8A82A4892_194;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::8909772CC771C2FF1D61415555452F2ED4ADA536
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___8909772CC771C2FF1D61415555452F2ED4ADA536_195;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::8971045D9FA23669DC4A204D478DC63888A9B420
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___8971045D9FA23669DC4A204D478DC63888A9B420_196;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=30 <PrivateImplementationDetails>::8AC7C1F7AAD5E1D2EA280038197C66272ED82BBE
	__StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016  ___8AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_197;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=30 <PrivateImplementationDetails>::8C301DDB45068B145D6C11F79591061EEE2AABA8
	__StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016  ___8C301DDB45068B145D6C11F79591061EEE2AABA8_198;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::8D1369A0D8832C941BD6D09849525D65D807A1DD
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___8D1369A0D8832C941BD6D09849525D65D807A1DD_199;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::8DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE
	__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  ___8DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_200;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::8DCF3F57BD5AAF8D097375E4290A41B13B2C8B04
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___8DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_201;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::8EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___8EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_202;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::8EED33571FFED1BBCA4954A909498A1D3316EDC8
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___8EED33571FFED1BBCA4954A909498A1D3316EDC8_203;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::8EEE72A955DF384253C5C458726A8A7299A05164
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___8EEE72A955DF384253C5C458726A8A7299A05164_204;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::8FD55399F538FAF9608E938EAB08D4BC88D0E131
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___8FD55399F538FAF9608E938EAB08D4BC88D0E131_205;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::9032D4E1ED9EBAC2417A10654FF0C658D2CC9825
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___9032D4E1ED9EBAC2417A10654FF0C658D2CC9825_206;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::910681E5316200ECA5484DF31EEAE1CF5E2F3439
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___910681E5316200ECA5484DF31EEAE1CF5E2F3439_207;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::91EA1528B7E2B59DB78E5E7A749312DD20AA43F1
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___91EA1528B7E2B59DB78E5E7A749312DD20AA43F1_208;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::95C66836A6DB5A262B674463792A9A67241AEC25
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___95C66836A6DB5A262B674463792A9A67241AEC25_209;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::95CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE
	__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  ___95CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_210;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::9623BB2649942957F680075A5AD3A16DE427F7A2
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___9623BB2649942957F680075A5AD3A16DE427F7A2_211;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::97BFC7D9D43B639F03919A5AE13B1A6CA2B05A08
	__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  ___97BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_212;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=384 <PrivateImplementationDetails>::9848EC8C1177B700CDB98F1A7C07F2A9C9EDE444
	__StaticArrayInitTypeSizeU3D384_t6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3  ___9848EC8C1177B700CDB98F1A7C07F2A9C9EDE444_213;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::9858ACE49B9B39354CFCB05E54A9790070E48D39
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___9858ACE49B9B39354CFCB05E54A9790070E48D39_214;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::99073F844984F28C1A9FC1E1490E5631272E26A0
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___99073F844984F28C1A9FC1E1490E5631272E26A0_215;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::993DC5FF70241D5149EB5EAF1CDD957CA0B23454
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___993DC5FF70241D5149EB5EAF1CDD957CA0B23454_216;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_217;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::99CC0827BC16027263E3737FF59411A5D4BFDE31
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___99CC0827BC16027263E3737FF59411A5D4BFDE31_218;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::99D70A082BCB145B3134DDB7BEC4FDB5BDEFA756
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___99D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_219;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::9AA52F510245480023EB2EF8CF779F6008E93D7A
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___9AA52F510245480023EB2EF8CF779F6008E93D7A_220;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::9ACE167D7580B82857BBF66A5A9BC1E2D139353C
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___9ACE167D7580B82857BBF66A5A9BC1E2D139353C_221;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::9B3E34C734351ED3DE1984119EAB9572C48FA15E
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___9B3E34C734351ED3DE1984119EAB9572C48FA15E_222;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::9BBAB86ACB95EF8C028708733458CDBAC4715197
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___9BBAB86ACB95EF8C028708733458CDBAC4715197_223;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::9BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___9BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_224;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::9C89FB9160DB0A983D5BE5D510BF9E801F0136A6
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___9C89FB9160DB0A983D5BE5D510BF9E801F0136A6_225;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::9D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601
	__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  ___9D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_226;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::9E548860392B66258417232F41EF03D48EBDD686
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___9E548860392B66258417232F41EF03D48EBDD686_227;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::9F6BDC14CCFDFFBF8F1FE3539741F79424F33E16
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___9F6BDC14CCFDFFBF8F1FE3539741F79424F33E16_228;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::9F8CAB0A98B00335FDDBCC16C748FEA524A25435
	__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  ___9F8CAB0A98B00335FDDBCC16C748FEA524A25435_229;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::A0324640CCF369A6794A9A8BB204ED22651ECBCC
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___A0324640CCF369A6794A9A8BB204ED22651ECBCC_230;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::A2978A4F76015511E3741ABF9B58117B348B19C2
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___A2978A4F76015511E3741ABF9B58117B348B19C2_231;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10 <PrivateImplementationDetails>::A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86
	__StaticArrayInitTypeSizeU3D10_tB641FD73E10817DC3A5A0B6CDD83506C3C2B53C7  ___A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_232;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::A350EF43D37B02EFF537EB2B57DD4CF05DD72C47
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_233;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::A3F55D69F9A97411DB4065F0FA414D912A181B69
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___A3F55D69F9A97411DB4065F0FA414D912A181B69_234;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=80 <PrivateImplementationDetails>::A6E975E72D8A987E6B9AA9400DE3025C1E91BE96
	__StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260  ___A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_235;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::A7977502040EE08933752F579060577814723508
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___A7977502040EE08933752F579060577814723508_236;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_237;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::A98B8F0C923F5DFC97332FCC80F65C96F9F27181
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___A98B8F0C923F5DFC97332FCC80F65C96F9F27181_238;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::AA607BA0FA20D3E80F456E68768C517D17D073D5
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___AA607BA0FA20D3E80F456E68768C517D17D073D5_239;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::AB44F69765F4A8ECD5D47354EA625039C43CC4D6
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___AB44F69765F4A8ECD5D47354EA625039C43CC4D6_240;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0
	__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  ___AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_241;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::AE2DCC8D6F51ED60D95A61A92C489BB896645BEA
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_242;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::AECEE3733C380177F0A39ACF3AF99BFE360C156F
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___AECEE3733C380177F0A39ACF3AF99BFE360C156F_243;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::AEE03822C3725428610E6E7DAAF7970DE35F7675
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___AEE03822C3725428610E6E7DAAF7970DE35F7675_244;
	// System.Int64 <PrivateImplementationDetails>::AF35D391D56E5153255BA0E0810F3EF49FC4CE6A
	int64_t ___AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_245;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::B15D37D4738DBDC32C38C2E1B6A1833322C22868
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___B15D37D4738DBDC32C38C2E1B6A1833322C22868_246;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::B166D50D0891455E483E2C6E1DB11C3DC03CAFFA
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___B166D50D0891455E483E2C6E1DB11C3DC03CAFFA_247;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::B19381C60E723A535EF856EC118DB867503FCC63
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___B19381C60E723A535EF856EC118DB867503FCC63_248;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_249;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::B3FDEC1908C5093BFC5CFB12749EE2650C8612C3
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___B3FDEC1908C5093BFC5CFB12749EE2650C8612C3_250;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::B5DDF34D20E879DBA76358A13661F61E6682B937
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___B5DDF34D20E879DBA76358A13661F61E6682B937_251;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_252;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::B6BCDFDEDD07A19F1028F226DE019E32E7FB2333
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_253;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::BB40482B7503DCF14BB6DAE300C2717C3021C7B2
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___BB40482B7503DCF14BB6DAE300C2717C3021C7B2_254;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::BBA3BFEF194A76998C4874D107EBEA8A81357762
	__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  ___BBA3BFEF194A76998C4874D107EBEA8A81357762_255;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::BBAA25706062E8AA738F96C541E3A6D6357F92E3
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___BBAA25706062E8AA738F96C541E3A6D6357F92E3_256;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::BBE42ED1645F9D77581D1D5B6EE11B933512F832
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___BBE42ED1645F9D77581D1D5B6EE11B933512F832_257;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::BC1ACC3AC4459BD0D19A953A64E05727DA98279A
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___BC1ACC3AC4459BD0D19A953A64E05727DA98279A_258;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::BC5EFA0F409033551EB01FD9CAA887739FBB267C
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___BC5EFA0F409033551EB01FD9CAA887739FBB267C_259;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_260;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::BDA0E80EABB31C2B339BA084415FF30AE14ECF9F
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_261;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::BF4FBD059EA47B09FA514A81515DD83A7F22A498
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___BF4FBD059EA47B09FA514A81515DD83A7F22A498_262;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_263;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::C04B3C75D3B52DBFE727A123E3E6A157786BBD6F
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_264;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::C0935C173C7A144FE24DA09584F4892153D01F3C
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___C0935C173C7A144FE24DA09584F4892153D01F3C_265;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_266;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::C4449C115C6F0E639E5F53D593F6E85D55D18DB0
	__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  ___C4449C115C6F0E639E5F53D593F6E85D55D18DB0_267;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::C530C580972A28DBE79D791F61E5916A541E6A1A
	__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  ___C530C580972A28DBE79D791F61E5916A541E6A1A_268;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932
	__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  ___C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_269;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::C62717FC35BA54563F42385BF79E44DD0A510124
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___C62717FC35BA54563F42385BF79E44DD0A510124_270;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::C6B6783FDC75202733D0518585E649906008DBC5
	__StaticArrayInitTypeSizeU3D64_t2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF  ___C6B6783FDC75202733D0518585E649906008DBC5_271;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::C6CA93B7FFF2C18F722DDBD501761C1381E2E55E
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_272;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::C7787E3098145FA28D38DE952D634BA862284127
	__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  ___C7787E3098145FA28D38DE952D634BA862284127_273;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::C7BD442E02179DC75AB7FEF343C297E1F46A7806
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___C7BD442E02179DC75AB7FEF343C297E1F46A7806_274;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::C7FBA0F208A16658206056EC89481A5EE74A7259
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___C7FBA0F208A16658206056EC89481A5EE74A7259_275;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::C82F7744F78AA5830EDBD9AD774CFF76737F7D40
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___C82F7744F78AA5830EDBD9AD774CFF76737F7D40_276;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::C92559E411FAF1D47ADD6CCA7374DAF066069D00
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___C92559E411FAF1D47ADD6CCA7374DAF066069D00_277;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_278;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::CA03F848D4A9DD85B5CF092E36317D483E3B0864
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___CA03F848D4A9DD85B5CF092E36317D483E3B0864_279;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::CB780513B9E4B4E2590F7104B066F632E7BC3E38
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___CB780513B9E4B4E2590F7104B066F632E7BC3E38_280;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::CC74BF89CF621AAC29665426A911E4B8BBB60B91
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___CC74BF89CF621AAC29665426A911E4B8BBB60B91_281;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10 <PrivateImplementationDetails>::CC95A63361E95F5A0BDF7771CE1AF37BE35817C6
	__StaticArrayInitTypeSizeU3D10_tB641FD73E10817DC3A5A0B6CDD83506C3C2B53C7  ___CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_282;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::CC9EE2CF247B3534102C1324C2679503686EF031
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___CC9EE2CF247B3534102C1324C2679503686EF031_283;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::CDDC3D4212D35722E3E8BAA7B5275932A6567037
	__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  ___CDDC3D4212D35722E3E8BAA7B5275932A6567037_284;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::CED1DB702E32A812D587254A4E1BAAA409E30859
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___CED1DB702E32A812D587254A4E1BAAA409E30859_285;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::CF1C9B07580D3992A644F840EBBB156E0E92B758
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___CF1C9B07580D3992A644F840EBBB156E0E92B758_286;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::D10229C07961891A434737F06293F1E0CC0FA33C
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___D10229C07961891A434737F06293F1E0CC0FA33C_287;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::D1A2B61FD08BB2242E12F4CAF6B512029C0DC177
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___D1A2B61FD08BB2242E12F4CAF6B512029C0DC177_288;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_289;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::D2BCB25441A9A8F3DE94D1EF27A870BB77553833
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___D2BCB25441A9A8F3DE94D1EF27A870BB77553833_290;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::D41FD917303D010F26005BDAF1F31ABC8A512A13
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___D41FD917303D010F26005BDAF1F31ABC8A512A13_291;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::D5B6993D49960DF01393AD8095091B12332C2FA3
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___D5B6993D49960DF01393AD8095091B12332C2FA3_292;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::D6B85A234C187D0FC82CE310F6423237C934B93A
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___D6B85A234C187D0FC82CE310F6423237C934B93A_293;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::D6D9691C10001AC45C1A8A68D4B08412F42D9F5D
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_294;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::D7207A48A00E499EF63FC2990337CC8B05C7AE9C
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___D7207A48A00E499EF63FC2990337CC8B05C7AE9C_295;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::D73BDC60FA2857E7CE2F742530B056A1866C2177
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___D73BDC60FA2857E7CE2F742530B056A1866C2177_296;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::D7AE4E850949E37E7359595699CCDC751E1C45E4
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___D7AE4E850949E37E7359595699CCDC751E1C45E4_297;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70
	__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  ___D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_298;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::D923777F48CDA51F21CABCED4FE36A2D620288A2
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___D923777F48CDA51F21CABCED4FE36A2D620288A2_299;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::DA8D7694A42602F7DC0356F30B9E1DFF8196724A
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___DA8D7694A42602F7DC0356F30B9E1DFF8196724A_300;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::DBF9A772127E1119D6E557AF812B6078438CE970
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___DBF9A772127E1119D6E557AF812B6078438CE970_301;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::DC7D58FD4F690A68811E226229F74D0089F018B3
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___DC7D58FD4F690A68811E226229F74D0089F018B3_302;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::DD04181543E071971F6379A33D631064A9B051D8
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___DD04181543E071971F6379A33D631064A9B051D8_303;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::DD2B45529EC6CEEBB86596D35461F63244FB3BC1
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___DD2B45529EC6CEEBB86596D35461F63244FB3BC1_304;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::DEDEF9E8D2052710E15C0CC080004BFB33F47665
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___DEDEF9E8D2052710E15C0CC080004BFB33F47665_305;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=11148 <PrivateImplementationDetails>::E02E3EC94504AFEEB34CE409836A3797D7E43964
	__StaticArrayInitTypeSizeU3D11148_tA414153821EB70E6FCD456B1E2DB309D58CE1619  ___E02E3EC94504AFEEB34CE409836A3797D7E43964_306;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::E1A877959C9E369159A2433060A6781C6C5C1D5C
	__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  ___E1A877959C9E369159A2433060A6781C6C5C1D5C_307;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::E1C849B129FBB4352239208AEFE34F3631A27813
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___E1C849B129FBB4352239208AEFE34F3631A27813_308;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::E2654B2181D84518EB5EE7BC342CFFD1F528E908
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___E2654B2181D84518EB5EE7BC342CFFD1F528E908_309;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_310;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::E2C0239EBBF1FD45925FAA7909458304DFD37A84
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___E2C0239EBBF1FD45925FAA7909458304DFD37A84_311;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_312;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_313;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_314;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_315;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::E5395170F0D79C58C3AF15E7AA917ACA2307CE5F
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___E5395170F0D79C58C3AF15E7AA917ACA2307CE5F_316;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::E5AD60E2BE68FA0155888A7E87B34F03905E7584
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___E5AD60E2BE68FA0155888A7E87B34F03905E7584_317;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::E62D0D793D6BCA39079E6441B162E59F33ED2B07
	__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  ___E62D0D793D6BCA39079E6441B162E59F33ED2B07_318;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::E6C34D5D4381C0E6151B26FB9A314D8A57292366
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___E6C34D5D4381C0E6151B26FB9A314D8A57292366_319;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::E9D2FA0CF959FCDF5A50AE1515251477F11EE719
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___E9D2FA0CF959FCDF5A50AE1515251477F11EE719_320;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::EA86E31C9F6288681516E2AA22EF6B5CD441AB94
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___EA86E31C9F6288681516E2AA22EF6B5CD441AB94_321;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::EB5C89140591CED42C39A8AE698A41F3593C3C62
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___EB5C89140591CED42C39A8AE698A41F3593C3C62_322;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=148 <PrivateImplementationDetails>::EB774035880443AEFBF8B61E7760DA5273A8A44C
	__StaticArrayInitTypeSizeU3D148_t0A73993E265ED7BA3349D48D2563680FA231189B  ___EB774035880443AEFBF8B61E7760DA5273A8A44C_323;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::EBB69348CF182DB7CB8021B09F8BBC2E8E87887C
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___EBB69348CF182DB7CB8021B09F8BBC2E8E87887C_324;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::EDF6280B8AEAF6DDC736AF7B745668475CE8552F
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___EDF6280B8AEAF6DDC736AF7B745668475CE8552F_325;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_326;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::EF1366407DE423137F5977D7AF18B0C058147698
	__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  ___EF1366407DE423137F5977D7AF18B0C058147698_327;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::EFAA45999AF190FC718690DC0121EB49DE7FFCE5
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___EFAA45999AF190FC718690DC0121EB49DE7FFCE5_328;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::F09F029091837E12B4E60500BED0749D79F77FC2
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___F09F029091837E12B4E60500BED0749D79F77FC2_329;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_330;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::F0E174E706EA1C2F8D181561F84976C942B1DE4B
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___F0E174E706EA1C2F8D181561F84976C942B1DE4B_331;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::F100C709518544CED81B98D3F1C862F094DAF46A
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___F100C709518544CED81B98D3F1C862F094DAF46A_332;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::F1F22748A7C1A4CC79E97C2063C48D7FF070D941
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___F1F22748A7C1A4CC79E97C2063C48D7FF070D941_333;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::F20B4E4CCB47B4F0631333A4DEE30DB71336110C
	__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  ___F20B4E4CCB47B4F0631333A4DEE30DB71336110C_334;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::F240F486E15B43E152AE7F2B692A7FA361FA7776
	__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  ___F240F486E15B43E152AE7F2B692A7FA361FA7776_335;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_336;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_337;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=30 <PrivateImplementationDetails>::F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55
	__StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016  ___F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_338;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::F44B79D8208EDC17F9C96AA7DEA4983114BF698D
	__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  ___F44B79D8208EDC17F9C96AA7DEA4983114BF698D_339;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::F5183037A4B449D12439C624D76065B310A6614A
	__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  ___F5183037A4B449D12439C624D76065B310A6614A_340;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::F65CD55AD5CBB0D49B45E05C5C0369616C658AB1
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_341;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_342;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::F865498CC0010E6ACE166D01EEA92F0F3C87743D
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___F865498CC0010E6ACE166D01EEA92F0F3C87743D_343;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::F86A89C59555BE5241482CB0FD35624F05D127CD
	__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  ___F86A89C59555BE5241482CB0FD35624F05D127CD_344;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::F975859F5AFA5A4AE7703BA3B943AFC04919E9B7
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_345;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::FA200C2489E9868CC920129D1E2D694172C2BA49
	__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  ___FA200C2489E9868CC920129D1E2D694172C2BA49_346;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575
	__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  ___FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_347;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B
	__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  ___FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_348;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=80 <PrivateImplementationDetails>::FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA
	__StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260  ___FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_349;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::FB977BD3A3BBAD68C2BBDBC28188F1084531E78E
	__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  ___FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_350;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=26 <PrivateImplementationDetails>::FCDF24D1C633613549296EFD3DC9250D840CA996
	__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  ___FCDF24D1C633613549296EFD3DC9250D840CA996_351;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::FCF9CE737F38E152A767F1291260BF9E15F80078
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___FCF9CE737F38E152A767F1291260BF9E15F80078_352;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97
	__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  ___FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_353;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::FE68796AD0908776C9F23F326A4181C1F53CF4DF
	__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  ___FE68796AD0908776C9F23F326A4181C1F53CF4DF_354;

public:
	inline static int32_t get_offset_of_U300E14C77230AEF0974CFF3930481157AABDA07B4_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___00E14C77230AEF0974CFF3930481157AABDA07B4_0)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U300E14C77230AEF0974CFF3930481157AABDA07B4_0() const { return ___00E14C77230AEF0974CFF3930481157AABDA07B4_0; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U300E14C77230AEF0974CFF3930481157AABDA07B4_0() { return &___00E14C77230AEF0974CFF3930481157AABDA07B4_0; }
	inline void set_U300E14C77230AEF0974CFF3930481157AABDA07B4_0(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___00E14C77230AEF0974CFF3930481157AABDA07B4_0 = value;
	}

	inline static int32_t get_offset_of_U301898B76F7C45E723996C7E8EDF45429F10AB352_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___01898B76F7C45E723996C7E8EDF45429F10AB352_1)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U301898B76F7C45E723996C7E8EDF45429F10AB352_1() const { return ___01898B76F7C45E723996C7E8EDF45429F10AB352_1; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U301898B76F7C45E723996C7E8EDF45429F10AB352_1() { return &___01898B76F7C45E723996C7E8EDF45429F10AB352_1; }
	inline void set_U301898B76F7C45E723996C7E8EDF45429F10AB352_1(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___01898B76F7C45E723996C7E8EDF45429F10AB352_1 = value;
	}

	inline static int32_t get_offset_of_U301A7BB0BE820D25E52A42C3D28DE27822EABCD4C_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___01A7BB0BE820D25E52A42C3D28DE27822EABCD4C_2)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U301A7BB0BE820D25E52A42C3D28DE27822EABCD4C_2() const { return ___01A7BB0BE820D25E52A42C3D28DE27822EABCD4C_2; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U301A7BB0BE820D25E52A42C3D28DE27822EABCD4C_2() { return &___01A7BB0BE820D25E52A42C3D28DE27822EABCD4C_2; }
	inline void set_U301A7BB0BE820D25E52A42C3D28DE27822EABCD4C_2(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___01A7BB0BE820D25E52A42C3D28DE27822EABCD4C_2 = value;
	}

	inline static int32_t get_offset_of_U30222E151247DAE31A8D697EAA14F43F718BD1F1C_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___0222E151247DAE31A8D697EAA14F43F718BD1F1C_3)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U30222E151247DAE31A8D697EAA14F43F718BD1F1C_3() const { return ___0222E151247DAE31A8D697EAA14F43F718BD1F1C_3; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U30222E151247DAE31A8D697EAA14F43F718BD1F1C_3() { return &___0222E151247DAE31A8D697EAA14F43F718BD1F1C_3; }
	inline void set_U30222E151247DAE31A8D697EAA14F43F718BD1F1C_3(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___0222E151247DAE31A8D697EAA14F43F718BD1F1C_3 = value;
	}

	inline static int32_t get_offset_of_U303AB6BB65AC7B8ECF309D37CACECE4B53B38D824_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___03AB6BB65AC7B8ECF309D37CACECE4B53B38D824_4)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U303AB6BB65AC7B8ECF309D37CACECE4B53B38D824_4() const { return ___03AB6BB65AC7B8ECF309D37CACECE4B53B38D824_4; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U303AB6BB65AC7B8ECF309D37CACECE4B53B38D824_4() { return &___03AB6BB65AC7B8ECF309D37CACECE4B53B38D824_4; }
	inline void set_U303AB6BB65AC7B8ECF309D37CACECE4B53B38D824_4(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___03AB6BB65AC7B8ECF309D37CACECE4B53B38D824_4 = value;
	}

	inline static int32_t get_offset_of_U30443C73B92DF9C67128EF5F0702DCBC469EDD2EA_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___0443C73B92DF9C67128EF5F0702DCBC469EDD2EA_5)); }
	inline __StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260  get_U30443C73B92DF9C67128EF5F0702DCBC469EDD2EA_5() const { return ___0443C73B92DF9C67128EF5F0702DCBC469EDD2EA_5; }
	inline __StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260 * get_address_of_U30443C73B92DF9C67128EF5F0702DCBC469EDD2EA_5() { return &___0443C73B92DF9C67128EF5F0702DCBC469EDD2EA_5; }
	inline void set_U30443C73B92DF9C67128EF5F0702DCBC469EDD2EA_5(__StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260  value)
	{
		___0443C73B92DF9C67128EF5F0702DCBC469EDD2EA_5 = value;
	}

	inline static int32_t get_offset_of_U3061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_6)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U3061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_6() const { return ___061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_6; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U3061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_6() { return &___061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_6; }
	inline void set_U3061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_6(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_6 = value;
	}

	inline static int32_t get_offset_of_U307438A85BDEE90305A6125B92084A5F3F2FD5F18_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___07438A85BDEE90305A6125B92084A5F3F2FD5F18_7)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U307438A85BDEE90305A6125B92084A5F3F2FD5F18_7() const { return ___07438A85BDEE90305A6125B92084A5F3F2FD5F18_7; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U307438A85BDEE90305A6125B92084A5F3F2FD5F18_7() { return &___07438A85BDEE90305A6125B92084A5F3F2FD5F18_7; }
	inline void set_U307438A85BDEE90305A6125B92084A5F3F2FD5F18_7(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___07438A85BDEE90305A6125B92084A5F3F2FD5F18_7 = value;
	}

	inline static int32_t get_offset_of_U3094D7DFD41161273E784D60EAE6D844127D77F0B_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___094D7DFD41161273E784D60EAE6D844127D77F0B_8)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U3094D7DFD41161273E784D60EAE6D844127D77F0B_8() const { return ___094D7DFD41161273E784D60EAE6D844127D77F0B_8; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U3094D7DFD41161273E784D60EAE6D844127D77F0B_8() { return &___094D7DFD41161273E784D60EAE6D844127D77F0B_8; }
	inline void set_U3094D7DFD41161273E784D60EAE6D844127D77F0B_8(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___094D7DFD41161273E784D60EAE6D844127D77F0B_8 = value;
	}

	inline static int32_t get_offset_of_U309E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___09E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_9)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U309E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_9() const { return ___09E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_9; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U309E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_9() { return &___09E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_9; }
	inline void set_U309E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_9(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___09E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_9 = value;
	}

	inline static int32_t get_offset_of_U30BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___0BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_10)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U30BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_10() const { return ___0BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_10; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U30BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_10() { return &___0BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_10; }
	inline void set_U30BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_10(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___0BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_10 = value;
	}

	inline static int32_t get_offset_of_U30BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___0BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_11)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U30BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_11() const { return ___0BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_11; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U30BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_11() { return &___0BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_11; }
	inline void set_U30BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_11(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___0BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_11 = value;
	}

	inline static int32_t get_offset_of_U30C7D347178E5302733AED896AA3A7B5DDEDC3A90_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___0C7D347178E5302733AED896AA3A7B5DDEDC3A90_12)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U30C7D347178E5302733AED896AA3A7B5DDEDC3A90_12() const { return ___0C7D347178E5302733AED896AA3A7B5DDEDC3A90_12; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U30C7D347178E5302733AED896AA3A7B5DDEDC3A90_12() { return &___0C7D347178E5302733AED896AA3A7B5DDEDC3A90_12; }
	inline void set_U30C7D347178E5302733AED896AA3A7B5DDEDC3A90_12(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___0C7D347178E5302733AED896AA3A7B5DDEDC3A90_12 = value;
	}

	inline static int32_t get_offset_of_U30D0AD555C6DA0BD4268F8573DC103F455F62A610_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___0D0AD555C6DA0BD4268F8573DC103F455F62A610_13)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U30D0AD555C6DA0BD4268F8573DC103F455F62A610_13() const { return ___0D0AD555C6DA0BD4268F8573DC103F455F62A610_13; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U30D0AD555C6DA0BD4268F8573DC103F455F62A610_13() { return &___0D0AD555C6DA0BD4268F8573DC103F455F62A610_13; }
	inline void set_U30D0AD555C6DA0BD4268F8573DC103F455F62A610_13(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___0D0AD555C6DA0BD4268F8573DC103F455F62A610_13 = value;
	}

	inline static int32_t get_offset_of_U30E4585982EA19664C33EA213EE69EC548F7322B0_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___0E4585982EA19664C33EA213EE69EC548F7322B0_14)); }
	inline __StaticArrayInitTypeSizeU3D44_tE280582A6EEC87F26198EDC0F6EC802BBCD3D015  get_U30E4585982EA19664C33EA213EE69EC548F7322B0_14() const { return ___0E4585982EA19664C33EA213EE69EC548F7322B0_14; }
	inline __StaticArrayInitTypeSizeU3D44_tE280582A6EEC87F26198EDC0F6EC802BBCD3D015 * get_address_of_U30E4585982EA19664C33EA213EE69EC548F7322B0_14() { return &___0E4585982EA19664C33EA213EE69EC548F7322B0_14; }
	inline void set_U30E4585982EA19664C33EA213EE69EC548F7322B0_14(__StaticArrayInitTypeSizeU3D44_tE280582A6EEC87F26198EDC0F6EC802BBCD3D015  value)
	{
		___0E4585982EA19664C33EA213EE69EC548F7322B0_14 = value;
	}

	inline static int32_t get_offset_of_U310111111FB24B2D331FB3F698193046CBCB8AAFE_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___10111111FB24B2D331FB3F698193046CBCB8AAFE_15)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U310111111FB24B2D331FB3F698193046CBCB8AAFE_15() const { return ___10111111FB24B2D331FB3F698193046CBCB8AAFE_15; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U310111111FB24B2D331FB3F698193046CBCB8AAFE_15() { return &___10111111FB24B2D331FB3F698193046CBCB8AAFE_15; }
	inline void set_U310111111FB24B2D331FB3F698193046CBCB8AAFE_15(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___10111111FB24B2D331FB3F698193046CBCB8AAFE_15 = value;
	}

	inline static int32_t get_offset_of_U31037DC6EAB9D7D3850660726ED132EAD961A41E9_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___1037DC6EAB9D7D3850660726ED132EAD961A41E9_16)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U31037DC6EAB9D7D3850660726ED132EAD961A41E9_16() const { return ___1037DC6EAB9D7D3850660726ED132EAD961A41E9_16; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U31037DC6EAB9D7D3850660726ED132EAD961A41E9_16() { return &___1037DC6EAB9D7D3850660726ED132EAD961A41E9_16; }
	inline void set_U31037DC6EAB9D7D3850660726ED132EAD961A41E9_16(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___1037DC6EAB9D7D3850660726ED132EAD961A41E9_16 = value;
	}

	inline static int32_t get_offset_of_U310486CAECBA5E73DA05910F94513409DEB6316FD_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___10486CAECBA5E73DA05910F94513409DEB6316FD_17)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U310486CAECBA5E73DA05910F94513409DEB6316FD_17() const { return ___10486CAECBA5E73DA05910F94513409DEB6316FD_17; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U310486CAECBA5E73DA05910F94513409DEB6316FD_17() { return &___10486CAECBA5E73DA05910F94513409DEB6316FD_17; }
	inline void set_U310486CAECBA5E73DA05910F94513409DEB6316FD_17(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___10486CAECBA5E73DA05910F94513409DEB6316FD_17 = value;
	}

	inline static int32_t get_offset_of_U311C3F78703162740B961B75ED808973DD68F7FA8_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___11C3F78703162740B961B75ED808973DD68F7FA8_18)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U311C3F78703162740B961B75ED808973DD68F7FA8_18() const { return ___11C3F78703162740B961B75ED808973DD68F7FA8_18; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U311C3F78703162740B961B75ED808973DD68F7FA8_18() { return &___11C3F78703162740B961B75ED808973DD68F7FA8_18; }
	inline void set_U311C3F78703162740B961B75ED808973DD68F7FA8_18(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___11C3F78703162740B961B75ED808973DD68F7FA8_18 = value;
	}

	inline static int32_t get_offset_of_U311D4D9FC526D047E01891BEE9949EA42408A800E_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___11D4D9FC526D047E01891BEE9949EA42408A800E_19)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U311D4D9FC526D047E01891BEE9949EA42408A800E_19() const { return ___11D4D9FC526D047E01891BEE9949EA42408A800E_19; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U311D4D9FC526D047E01891BEE9949EA42408A800E_19() { return &___11D4D9FC526D047E01891BEE9949EA42408A800E_19; }
	inline void set_U311D4D9FC526D047E01891BEE9949EA42408A800E_19(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___11D4D9FC526D047E01891BEE9949EA42408A800E_19 = value;
	}

	inline static int32_t get_offset_of_U312625AC94FB97DAA377449A877D25786C8ADFFC5_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___12625AC94FB97DAA377449A877D25786C8ADFFC5_20)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U312625AC94FB97DAA377449A877D25786C8ADFFC5_20() const { return ___12625AC94FB97DAA377449A877D25786C8ADFFC5_20; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U312625AC94FB97DAA377449A877D25786C8ADFFC5_20() { return &___12625AC94FB97DAA377449A877D25786C8ADFFC5_20; }
	inline void set_U312625AC94FB97DAA377449A877D25786C8ADFFC5_20(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___12625AC94FB97DAA377449A877D25786C8ADFFC5_20 = value;
	}

	inline static int32_t get_offset_of_U312F1DB2AC68CD455638374E8DF9E9D604B15F693_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___12F1DB2AC68CD455638374E8DF9E9D604B15F693_21)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U312F1DB2AC68CD455638374E8DF9E9D604B15F693_21() const { return ___12F1DB2AC68CD455638374E8DF9E9D604B15F693_21; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U312F1DB2AC68CD455638374E8DF9E9D604B15F693_21() { return &___12F1DB2AC68CD455638374E8DF9E9D604B15F693_21; }
	inline void set_U312F1DB2AC68CD455638374E8DF9E9D604B15F693_21(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___12F1DB2AC68CD455638374E8DF9E9D604B15F693_21 = value;
	}

	inline static int32_t get_offset_of_U3142E1469231D592823559059166B32655910D0CF_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___142E1469231D592823559059166B32655910D0CF_22)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U3142E1469231D592823559059166B32655910D0CF_22() const { return ___142E1469231D592823559059166B32655910D0CF_22; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U3142E1469231D592823559059166B32655910D0CF_22() { return &___142E1469231D592823559059166B32655910D0CF_22; }
	inline void set_U3142E1469231D592823559059166B32655910D0CF_22(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___142E1469231D592823559059166B32655910D0CF_22 = value;
	}

	inline static int32_t get_offset_of_U3185FAB3B3D8AC0F1CBECFFCD7DEA45EE4AC9E357_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___185FAB3B3D8AC0F1CBECFFCD7DEA45EE4AC9E357_23)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U3185FAB3B3D8AC0F1CBECFFCD7DEA45EE4AC9E357_23() const { return ___185FAB3B3D8AC0F1CBECFFCD7DEA45EE4AC9E357_23; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U3185FAB3B3D8AC0F1CBECFFCD7DEA45EE4AC9E357_23() { return &___185FAB3B3D8AC0F1CBECFFCD7DEA45EE4AC9E357_23; }
	inline void set_U3185FAB3B3D8AC0F1CBECFFCD7DEA45EE4AC9E357_23(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___185FAB3B3D8AC0F1CBECFFCD7DEA45EE4AC9E357_23 = value;
	}

	inline static int32_t get_offset_of_U3186E7643E22CF0E2479E92C738FEBEF56B275E95_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___186E7643E22CF0E2479E92C738FEBEF56B275E95_24)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U3186E7643E22CF0E2479E92C738FEBEF56B275E95_24() const { return ___186E7643E22CF0E2479E92C738FEBEF56B275E95_24; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U3186E7643E22CF0E2479E92C738FEBEF56B275E95_24() { return &___186E7643E22CF0E2479E92C738FEBEF56B275E95_24; }
	inline void set_U3186E7643E22CF0E2479E92C738FEBEF56B275E95_24(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___186E7643E22CF0E2479E92C738FEBEF56B275E95_24 = value;
	}

	inline static int32_t get_offset_of_U3188E7A480B871C554F3F805D3E1184673960EED5_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___188E7A480B871C554F3F805D3E1184673960EED5_25)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U3188E7A480B871C554F3F805D3E1184673960EED5_25() const { return ___188E7A480B871C554F3F805D3E1184673960EED5_25; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U3188E7A480B871C554F3F805D3E1184673960EED5_25() { return &___188E7A480B871C554F3F805D3E1184673960EED5_25; }
	inline void set_U3188E7A480B871C554F3F805D3E1184673960EED5_25(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___188E7A480B871C554F3F805D3E1184673960EED5_25 = value;
	}

	inline static int32_t get_offset_of_U3193330123B5DF8BA7DDD1FD73E3941FD113C71C7_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___193330123B5DF8BA7DDD1FD73E3941FD113C71C7_26)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U3193330123B5DF8BA7DDD1FD73E3941FD113C71C7_26() const { return ___193330123B5DF8BA7DDD1FD73E3941FD113C71C7_26; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U3193330123B5DF8BA7DDD1FD73E3941FD113C71C7_26() { return &___193330123B5DF8BA7DDD1FD73E3941FD113C71C7_26; }
	inline void set_U3193330123B5DF8BA7DDD1FD73E3941FD113C71C7_26(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___193330123B5DF8BA7DDD1FD73E3941FD113C71C7_26 = value;
	}

	inline static int32_t get_offset_of_U31A3C01816F6E750EC65FDEF4FEB7758235456890_27() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___1A3C01816F6E750EC65FDEF4FEB7758235456890_27)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U31A3C01816F6E750EC65FDEF4FEB7758235456890_27() const { return ___1A3C01816F6E750EC65FDEF4FEB7758235456890_27; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U31A3C01816F6E750EC65FDEF4FEB7758235456890_27() { return &___1A3C01816F6E750EC65FDEF4FEB7758235456890_27; }
	inline void set_U31A3C01816F6E750EC65FDEF4FEB7758235456890_27(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___1A3C01816F6E750EC65FDEF4FEB7758235456890_27 = value;
	}

	inline static int32_t get_offset_of_U31A85140B8FCA30191EE9347F53782234F0781F79_28() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___1A85140B8FCA30191EE9347F53782234F0781F79_28)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U31A85140B8FCA30191EE9347F53782234F0781F79_28() const { return ___1A85140B8FCA30191EE9347F53782234F0781F79_28; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U31A85140B8FCA30191EE9347F53782234F0781F79_28() { return &___1A85140B8FCA30191EE9347F53782234F0781F79_28; }
	inline void set_U31A85140B8FCA30191EE9347F53782234F0781F79_28(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___1A85140B8FCA30191EE9347F53782234F0781F79_28 = value;
	}

	inline static int32_t get_offset_of_U31BCA1A912FF22DA69AA3E91A68492D4BA5179162_29() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___1BCA1A912FF22DA69AA3E91A68492D4BA5179162_29)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U31BCA1A912FF22DA69AA3E91A68492D4BA5179162_29() const { return ___1BCA1A912FF22DA69AA3E91A68492D4BA5179162_29; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U31BCA1A912FF22DA69AA3E91A68492D4BA5179162_29() { return &___1BCA1A912FF22DA69AA3E91A68492D4BA5179162_29; }
	inline void set_U31BCA1A912FF22DA69AA3E91A68492D4BA5179162_29(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___1BCA1A912FF22DA69AA3E91A68492D4BA5179162_29 = value;
	}

	inline static int32_t get_offset_of_U31D0501D14FE31729CD75A4073F4404AF7E53B024_30() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___1D0501D14FE31729CD75A4073F4404AF7E53B024_30)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U31D0501D14FE31729CD75A4073F4404AF7E53B024_30() const { return ___1D0501D14FE31729CD75A4073F4404AF7E53B024_30; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U31D0501D14FE31729CD75A4073F4404AF7E53B024_30() { return &___1D0501D14FE31729CD75A4073F4404AF7E53B024_30; }
	inline void set_U31D0501D14FE31729CD75A4073F4404AF7E53B024_30(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___1D0501D14FE31729CD75A4073F4404AF7E53B024_30 = value;
	}

	inline static int32_t get_offset_of_U31D562AD827598FC3F5C82FC2091E65C8F8A07ADC_31() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___1D562AD827598FC3F5C82FC2091E65C8F8A07ADC_31)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U31D562AD827598FC3F5C82FC2091E65C8F8A07ADC_31() const { return ___1D562AD827598FC3F5C82FC2091E65C8F8A07ADC_31; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U31D562AD827598FC3F5C82FC2091E65C8F8A07ADC_31() { return &___1D562AD827598FC3F5C82FC2091E65C8F8A07ADC_31; }
	inline void set_U31D562AD827598FC3F5C82FC2091E65C8F8A07ADC_31(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___1D562AD827598FC3F5C82FC2091E65C8F8A07ADC_31 = value;
	}

	inline static int32_t get_offset_of_U31DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_32() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___1DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_32)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U31DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_32() const { return ___1DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_32; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U31DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_32() { return &___1DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_32; }
	inline void set_U31DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_32(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___1DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_32 = value;
	}

	inline static int32_t get_offset_of_U31DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_33() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___1DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_33)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U31DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_33() const { return ___1DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_33; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U31DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_33() { return &___1DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_33; }
	inline void set_U31DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_33(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___1DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_33 = value;
	}

	inline static int32_t get_offset_of_U31E45F828069EAF87A0B553BE1E65B98F20E377ED_34() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___1E45F828069EAF87A0B553BE1E65B98F20E377ED_34)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U31E45F828069EAF87A0B553BE1E65B98F20E377ED_34() const { return ___1E45F828069EAF87A0B553BE1E65B98F20E377ED_34; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U31E45F828069EAF87A0B553BE1E65B98F20E377ED_34() { return &___1E45F828069EAF87A0B553BE1E65B98F20E377ED_34; }
	inline void set_U31E45F828069EAF87A0B553BE1E65B98F20E377ED_34(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___1E45F828069EAF87A0B553BE1E65B98F20E377ED_34 = value;
	}

	inline static int32_t get_offset_of_U31EB30BCD6A1576A001C2906D3AF45955E19C48BC_35() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___1EB30BCD6A1576A001C2906D3AF45955E19C48BC_35)); }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  get_U31EB30BCD6A1576A001C2906D3AF45955E19C48BC_35() const { return ___1EB30BCD6A1576A001C2906D3AF45955E19C48BC_35; }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54 * get_address_of_U31EB30BCD6A1576A001C2906D3AF45955E19C48BC_35() { return &___1EB30BCD6A1576A001C2906D3AF45955E19C48BC_35; }
	inline void set_U31EB30BCD6A1576A001C2906D3AF45955E19C48BC_35(__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  value)
	{
		___1EB30BCD6A1576A001C2906D3AF45955E19C48BC_35 = value;
	}

	inline static int32_t get_offset_of_U31F5771BF2009B14CC123999096615351A0BE9527_36() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___1F5771BF2009B14CC123999096615351A0BE9527_36)); }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  get_U31F5771BF2009B14CC123999096615351A0BE9527_36() const { return ___1F5771BF2009B14CC123999096615351A0BE9527_36; }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7 * get_address_of_U31F5771BF2009B14CC123999096615351A0BE9527_36() { return &___1F5771BF2009B14CC123999096615351A0BE9527_36; }
	inline void set_U31F5771BF2009B14CC123999096615351A0BE9527_36(__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  value)
	{
		___1F5771BF2009B14CC123999096615351A0BE9527_36 = value;
	}

	inline static int32_t get_offset_of_U31FFDEC0F27BC9DCF708F067786205F164F8A6D32_37() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___1FFDEC0F27BC9DCF708F067786205F164F8A6D32_37)); }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  get_U31FFDEC0F27BC9DCF708F067786205F164F8A6D32_37() const { return ___1FFDEC0F27BC9DCF708F067786205F164F8A6D32_37; }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7 * get_address_of_U31FFDEC0F27BC9DCF708F067786205F164F8A6D32_37() { return &___1FFDEC0F27BC9DCF708F067786205F164F8A6D32_37; }
	inline void set_U31FFDEC0F27BC9DCF708F067786205F164F8A6D32_37(__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  value)
	{
		___1FFDEC0F27BC9DCF708F067786205F164F8A6D32_37 = value;
	}

	inline static int32_t get_offset_of_U320D740A7E661D50B06DC32EF6577367D264F6235_38() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___20D740A7E661D50B06DC32EF6577367D264F6235_38)); }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  get_U320D740A7E661D50B06DC32EF6577367D264F6235_38() const { return ___20D740A7E661D50B06DC32EF6577367D264F6235_38; }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 * get_address_of_U320D740A7E661D50B06DC32EF6577367D264F6235_38() { return &___20D740A7E661D50B06DC32EF6577367D264F6235_38; }
	inline void set_U320D740A7E661D50B06DC32EF6577367D264F6235_38(__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  value)
	{
		___20D740A7E661D50B06DC32EF6577367D264F6235_38 = value;
	}

	inline static int32_t get_offset_of_U3227C649F55292B0A293EBB2D56AB8ABAECC7F629_39() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___227C649F55292B0A293EBB2D56AB8ABAECC7F629_39)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U3227C649F55292B0A293EBB2D56AB8ABAECC7F629_39() const { return ___227C649F55292B0A293EBB2D56AB8ABAECC7F629_39; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U3227C649F55292B0A293EBB2D56AB8ABAECC7F629_39() { return &___227C649F55292B0A293EBB2D56AB8ABAECC7F629_39; }
	inline void set_U3227C649F55292B0A293EBB2D56AB8ABAECC7F629_39(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___227C649F55292B0A293EBB2D56AB8ABAECC7F629_39 = value;
	}

	inline static int32_t get_offset_of_U322BF5B86DC5E0B87331903141C5973C117DA716E_40() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___22BF5B86DC5E0B87331903141C5973C117DA716E_40)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U322BF5B86DC5E0B87331903141C5973C117DA716E_40() const { return ___22BF5B86DC5E0B87331903141C5973C117DA716E_40; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U322BF5B86DC5E0B87331903141C5973C117DA716E_40() { return &___22BF5B86DC5E0B87331903141C5973C117DA716E_40; }
	inline void set_U322BF5B86DC5E0B87331903141C5973C117DA716E_40(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___22BF5B86DC5E0B87331903141C5973C117DA716E_40 = value;
	}

	inline static int32_t get_offset_of_U32309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_41() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___2309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_41)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U32309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_41() const { return ___2309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_41; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U32309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_41() { return &___2309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_41; }
	inline void set_U32309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_41(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___2309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_41 = value;
	}

	inline static int32_t get_offset_of_U32357B210FC2522A4A9D0F465AEAC250A79FA9449_42() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___2357B210FC2522A4A9D0F465AEAC250A79FA9449_42)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U32357B210FC2522A4A9D0F465AEAC250A79FA9449_42() const { return ___2357B210FC2522A4A9D0F465AEAC250A79FA9449_42; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U32357B210FC2522A4A9D0F465AEAC250A79FA9449_42() { return &___2357B210FC2522A4A9D0F465AEAC250A79FA9449_42; }
	inline void set_U32357B210FC2522A4A9D0F465AEAC250A79FA9449_42(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___2357B210FC2522A4A9D0F465AEAC250A79FA9449_42 = value;
	}

	inline static int32_t get_offset_of_U323D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_43() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___23D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_43)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U323D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_43() const { return ___23D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_43; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U323D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_43() { return &___23D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_43; }
	inline void set_U323D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_43(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___23D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_43 = value;
	}

	inline static int32_t get_offset_of_U3241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_44() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_44)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U3241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_44() const { return ___241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_44; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U3241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_44() { return &___241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_44; }
	inline void set_U3241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_44(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_44 = value;
	}

	inline static int32_t get_offset_of_U324E7D35141138E4668DE547A98CE3A84AF0E6FEB_45() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___24E7D35141138E4668DE547A98CE3A84AF0E6FEB_45)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U324E7D35141138E4668DE547A98CE3A84AF0E6FEB_45() const { return ___24E7D35141138E4668DE547A98CE3A84AF0E6FEB_45; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U324E7D35141138E4668DE547A98CE3A84AF0E6FEB_45() { return &___24E7D35141138E4668DE547A98CE3A84AF0E6FEB_45; }
	inline void set_U324E7D35141138E4668DE547A98CE3A84AF0E6FEB_45(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___24E7D35141138E4668DE547A98CE3A84AF0E6FEB_45 = value;
	}

	inline static int32_t get_offset_of_U3259B7EF54A535F34001B95480B4BB11245EA41B5_46() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___259B7EF54A535F34001B95480B4BB11245EA41B5_46)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U3259B7EF54A535F34001B95480B4BB11245EA41B5_46() const { return ___259B7EF54A535F34001B95480B4BB11245EA41B5_46; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U3259B7EF54A535F34001B95480B4BB11245EA41B5_46() { return &___259B7EF54A535F34001B95480B4BB11245EA41B5_46; }
	inline void set_U3259B7EF54A535F34001B95480B4BB11245EA41B5_46(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___259B7EF54A535F34001B95480B4BB11245EA41B5_46 = value;
	}

	inline static int32_t get_offset_of_U32861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_47() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___2861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_47)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U32861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_47() const { return ___2861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_47; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U32861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_47() { return &___2861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_47; }
	inline void set_U32861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_47(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___2861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_47 = value;
	}

	inline static int32_t get_offset_of_U328D1E3B5188FE552D37912314E419E7F30ABD844_48() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___28D1E3B5188FE552D37912314E419E7F30ABD844_48)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U328D1E3B5188FE552D37912314E419E7F30ABD844_48() const { return ___28D1E3B5188FE552D37912314E419E7F30ABD844_48; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U328D1E3B5188FE552D37912314E419E7F30ABD844_48() { return &___28D1E3B5188FE552D37912314E419E7F30ABD844_48; }
	inline void set_U328D1E3B5188FE552D37912314E419E7F30ABD844_48(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___28D1E3B5188FE552D37912314E419E7F30ABD844_48 = value;
	}

	inline static int32_t get_offset_of_U328F991026781E1BC119F02498D75FB3A84F4ED37_49() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___28F991026781E1BC119F02498D75FB3A84F4ED37_49)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U328F991026781E1BC119F02498D75FB3A84F4ED37_49() const { return ___28F991026781E1BC119F02498D75FB3A84F4ED37_49; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U328F991026781E1BC119F02498D75FB3A84F4ED37_49() { return &___28F991026781E1BC119F02498D75FB3A84F4ED37_49; }
	inline void set_U328F991026781E1BC119F02498D75FB3A84F4ED37_49(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___28F991026781E1BC119F02498D75FB3A84F4ED37_49 = value;
	}

	inline static int32_t get_offset_of_U32923D4191FF7FBBF11BFC643B6D93CC8074D31E1_50() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___2923D4191FF7FBBF11BFC643B6D93CC8074D31E1_50)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U32923D4191FF7FBBF11BFC643B6D93CC8074D31E1_50() const { return ___2923D4191FF7FBBF11BFC643B6D93CC8074D31E1_50; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U32923D4191FF7FBBF11BFC643B6D93CC8074D31E1_50() { return &___2923D4191FF7FBBF11BFC643B6D93CC8074D31E1_50; }
	inline void set_U32923D4191FF7FBBF11BFC643B6D93CC8074D31E1_50(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___2923D4191FF7FBBF11BFC643B6D93CC8074D31E1_50 = value;
	}

	inline static int32_t get_offset_of_U329AB17251F5A730B539AB351FFCB45B9F2FA8027_51() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___29AB17251F5A730B539AB351FFCB45B9F2FA8027_51)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U329AB17251F5A730B539AB351FFCB45B9F2FA8027_51() const { return ___29AB17251F5A730B539AB351FFCB45B9F2FA8027_51; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U329AB17251F5A730B539AB351FFCB45B9F2FA8027_51() { return &___29AB17251F5A730B539AB351FFCB45B9F2FA8027_51; }
	inline void set_U329AB17251F5A730B539AB351FFCB45B9F2FA8027_51(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___29AB17251F5A730B539AB351FFCB45B9F2FA8027_51 = value;
	}

	inline static int32_t get_offset_of_U32A84324D54FDC0473BE021004C50F363ADCD89AF_52() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___2A84324D54FDC0473BE021004C50F363ADCD89AF_52)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U32A84324D54FDC0473BE021004C50F363ADCD89AF_52() const { return ___2A84324D54FDC0473BE021004C50F363ADCD89AF_52; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U32A84324D54FDC0473BE021004C50F363ADCD89AF_52() { return &___2A84324D54FDC0473BE021004C50F363ADCD89AF_52; }
	inline void set_U32A84324D54FDC0473BE021004C50F363ADCD89AF_52(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___2A84324D54FDC0473BE021004C50F363ADCD89AF_52 = value;
	}

	inline static int32_t get_offset_of_U32AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_53() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___2AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_53)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U32AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_53() const { return ___2AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_53; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U32AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_53() { return &___2AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_53; }
	inline void set_U32AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_53(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___2AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_53 = value;
	}

	inline static int32_t get_offset_of_U32D3DF989BE3BBA2B353F845C37992DFF85579505_54() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___2D3DF989BE3BBA2B353F845C37992DFF85579505_54)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U32D3DF989BE3BBA2B353F845C37992DFF85579505_54() const { return ___2D3DF989BE3BBA2B353F845C37992DFF85579505_54; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U32D3DF989BE3BBA2B353F845C37992DFF85579505_54() { return &___2D3DF989BE3BBA2B353F845C37992DFF85579505_54; }
	inline void set_U32D3DF989BE3BBA2B353F845C37992DFF85579505_54(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___2D3DF989BE3BBA2B353F845C37992DFF85579505_54 = value;
	}

	inline static int32_t get_offset_of_U32DFCD7C13A7339CC41C473220CE80CDD5933C64C_55() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___2DFCD7C13A7339CC41C473220CE80CDD5933C64C_55)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U32DFCD7C13A7339CC41C473220CE80CDD5933C64C_55() const { return ___2DFCD7C13A7339CC41C473220CE80CDD5933C64C_55; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U32DFCD7C13A7339CC41C473220CE80CDD5933C64C_55() { return &___2DFCD7C13A7339CC41C473220CE80CDD5933C64C_55; }
	inline void set_U32DFCD7C13A7339CC41C473220CE80CDD5933C64C_55(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___2DFCD7C13A7339CC41C473220CE80CDD5933C64C_55 = value;
	}

	inline static int32_t get_offset_of_U32E8B473A712423E983C2B2EDA43927EB0A14F5B4_56() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___2E8B473A712423E983C2B2EDA43927EB0A14F5B4_56)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U32E8B473A712423E983C2B2EDA43927EB0A14F5B4_56() const { return ___2E8B473A712423E983C2B2EDA43927EB0A14F5B4_56; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U32E8B473A712423E983C2B2EDA43927EB0A14F5B4_56() { return &___2E8B473A712423E983C2B2EDA43927EB0A14F5B4_56; }
	inline void set_U32E8B473A712423E983C2B2EDA43927EB0A14F5B4_56(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___2E8B473A712423E983C2B2EDA43927EB0A14F5B4_56 = value;
	}

	inline static int32_t get_offset_of_U32EC69895F74DBAE74600E960EFFB6448597BDEEB_57() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___2EC69895F74DBAE74600E960EFFB6448597BDEEB_57)); }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  get_U32EC69895F74DBAE74600E960EFFB6448597BDEEB_57() const { return ___2EC69895F74DBAE74600E960EFFB6448597BDEEB_57; }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54 * get_address_of_U32EC69895F74DBAE74600E960EFFB6448597BDEEB_57() { return &___2EC69895F74DBAE74600E960EFFB6448597BDEEB_57; }
	inline void set_U32EC69895F74DBAE74600E960EFFB6448597BDEEB_57(__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  value)
	{
		___2EC69895F74DBAE74600E960EFFB6448597BDEEB_57 = value;
	}

	inline static int32_t get_offset_of_U3300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_58() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_58)); }
	inline __StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  get_U3300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_58() const { return ___300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_58; }
	inline __StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB * get_address_of_U3300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_58() { return &___300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_58; }
	inline void set_U3300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_58(__StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  value)
	{
		___300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_58 = value;
	}

	inline static int32_t get_offset_of_U33102377CA043644BFD52C56AE2A9F554723263FD_59() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___3102377CA043644BFD52C56AE2A9F554723263FD_59)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U33102377CA043644BFD52C56AE2A9F554723263FD_59() const { return ___3102377CA043644BFD52C56AE2A9F554723263FD_59; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U33102377CA043644BFD52C56AE2A9F554723263FD_59() { return &___3102377CA043644BFD52C56AE2A9F554723263FD_59; }
	inline void set_U33102377CA043644BFD52C56AE2A9F554723263FD_59(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___3102377CA043644BFD52C56AE2A9F554723263FD_59 = value;
	}

	inline static int32_t get_offset_of_U33199DD35AA6A041AF9F66BFBAEC3563E838D3731_60() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___3199DD35AA6A041AF9F66BFBAEC3563E838D3731_60)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U33199DD35AA6A041AF9F66BFBAEC3563E838D3731_60() const { return ___3199DD35AA6A041AF9F66BFBAEC3563E838D3731_60; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U33199DD35AA6A041AF9F66BFBAEC3563E838D3731_60() { return &___3199DD35AA6A041AF9F66BFBAEC3563E838D3731_60; }
	inline void set_U33199DD35AA6A041AF9F66BFBAEC3563E838D3731_60(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___3199DD35AA6A041AF9F66BFBAEC3563E838D3731_60 = value;
	}

	inline static int32_t get_offset_of_U3322BC3569CD0270EC3179B30382F24148B533D10_61() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___322BC3569CD0270EC3179B30382F24148B533D10_61)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U3322BC3569CD0270EC3179B30382F24148B533D10_61() const { return ___322BC3569CD0270EC3179B30382F24148B533D10_61; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U3322BC3569CD0270EC3179B30382F24148B533D10_61() { return &___322BC3569CD0270EC3179B30382F24148B533D10_61; }
	inline void set_U3322BC3569CD0270EC3179B30382F24148B533D10_61(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___322BC3569CD0270EC3179B30382F24148B533D10_61 = value;
	}

	inline static int32_t get_offset_of_U333B8540230E7366A64AC520E4F819103EB00ECD0_62() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___33B8540230E7366A64AC520E4F819103EB00ECD0_62)); }
	inline __StaticArrayInitTypeSizeU3D156_tD444739979C2F1A4BE28790BC71DD6FB876CCD25  get_U333B8540230E7366A64AC520E4F819103EB00ECD0_62() const { return ___33B8540230E7366A64AC520E4F819103EB00ECD0_62; }
	inline __StaticArrayInitTypeSizeU3D156_tD444739979C2F1A4BE28790BC71DD6FB876CCD25 * get_address_of_U333B8540230E7366A64AC520E4F819103EB00ECD0_62() { return &___33B8540230E7366A64AC520E4F819103EB00ECD0_62; }
	inline void set_U333B8540230E7366A64AC520E4F819103EB00ECD0_62(__StaticArrayInitTypeSizeU3D156_tD444739979C2F1A4BE28790BC71DD6FB876CCD25  value)
	{
		___33B8540230E7366A64AC520E4F819103EB00ECD0_62 = value;
	}

	inline static int32_t get_offset_of_U334617E1F9E901BD9DD9FEB1839483F5ED406E7B3_63() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___34617E1F9E901BD9DD9FEB1839483F5ED406E7B3_63)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U334617E1F9E901BD9DD9FEB1839483F5ED406E7B3_63() const { return ___34617E1F9E901BD9DD9FEB1839483F5ED406E7B3_63; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U334617E1F9E901BD9DD9FEB1839483F5ED406E7B3_63() { return &___34617E1F9E901BD9DD9FEB1839483F5ED406E7B3_63; }
	inline void set_U334617E1F9E901BD9DD9FEB1839483F5ED406E7B3_63(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___34617E1F9E901BD9DD9FEB1839483F5ED406E7B3_63 = value;
	}

	inline static int32_t get_offset_of_U335859E8329070E55C5D9282930023DD6B1594141_64() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___35859E8329070E55C5D9282930023DD6B1594141_64)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U335859E8329070E55C5D9282930023DD6B1594141_64() const { return ___35859E8329070E55C5D9282930023DD6B1594141_64; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U335859E8329070E55C5D9282930023DD6B1594141_64() { return &___35859E8329070E55C5D9282930023DD6B1594141_64; }
	inline void set_U335859E8329070E55C5D9282930023DD6B1594141_64(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___35859E8329070E55C5D9282930023DD6B1594141_64 = value;
	}

	inline static int32_t get_offset_of_U335E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_65() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___35E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_65)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U335E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_65() const { return ___35E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_65; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U335E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_65() { return &___35E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_65; }
	inline void set_U335E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_65(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___35E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_65 = value;
	}

	inline static int32_t get_offset_of_U335FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_66() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___35FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_66)); }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  get_U335FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_66() const { return ___35FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_66; }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7 * get_address_of_U335FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_66() { return &___35FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_66; }
	inline void set_U335FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_66(__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  value)
	{
		___35FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_66 = value;
	}

	inline static int32_t get_offset_of_U338017E50AF503681BFC2BD0CDED5F7565471B574_67() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___38017E50AF503681BFC2BD0CDED5F7565471B574_67)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U338017E50AF503681BFC2BD0CDED5F7565471B574_67() const { return ___38017E50AF503681BFC2BD0CDED5F7565471B574_67; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U338017E50AF503681BFC2BD0CDED5F7565471B574_67() { return &___38017E50AF503681BFC2BD0CDED5F7565471B574_67; }
	inline void set_U338017E50AF503681BFC2BD0CDED5F7565471B574_67(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___38017E50AF503681BFC2BD0CDED5F7565471B574_67 = value;
	}

	inline static int32_t get_offset_of_U338CAE5BC1B31E24A88061CEC362093BB4D8F0523_68() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___38CAE5BC1B31E24A88061CEC362093BB4D8F0523_68)); }
	inline __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  get_U338CAE5BC1B31E24A88061CEC362093BB4D8F0523_68() const { return ___38CAE5BC1B31E24A88061CEC362093BB4D8F0523_68; }
	inline __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F * get_address_of_U338CAE5BC1B31E24A88061CEC362093BB4D8F0523_68() { return &___38CAE5BC1B31E24A88061CEC362093BB4D8F0523_68; }
	inline void set_U338CAE5BC1B31E24A88061CEC362093BB4D8F0523_68(__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  value)
	{
		___38CAE5BC1B31E24A88061CEC362093BB4D8F0523_68 = value;
	}

	inline static int32_t get_offset_of_U33A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_69() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___3A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_69)); }
	inline __StaticArrayInitTypeSizeU3D54_t1ACEB6759550166F62217D005B9DC6C35D54C265  get_U33A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_69() const { return ___3A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_69; }
	inline __StaticArrayInitTypeSizeU3D54_t1ACEB6759550166F62217D005B9DC6C35D54C265 * get_address_of_U33A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_69() { return &___3A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_69; }
	inline void set_U33A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_69(__StaticArrayInitTypeSizeU3D54_t1ACEB6759550166F62217D005B9DC6C35D54C265  value)
	{
		___3A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_69 = value;
	}

	inline static int32_t get_offset_of_U33C22A633B79B510613E37BC5A02FF91A4AB006ED_70() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___3C22A633B79B510613E37BC5A02FF91A4AB006ED_70)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U33C22A633B79B510613E37BC5A02FF91A4AB006ED_70() const { return ___3C22A633B79B510613E37BC5A02FF91A4AB006ED_70; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U33C22A633B79B510613E37BC5A02FF91A4AB006ED_70() { return &___3C22A633B79B510613E37BC5A02FF91A4AB006ED_70; }
	inline void set_U33C22A633B79B510613E37BC5A02FF91A4AB006ED_70(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___3C22A633B79B510613E37BC5A02FF91A4AB006ED_70 = value;
	}

	inline static int32_t get_offset_of_U33D3C46CC589F65C2443688952CBCD9C499839622_71() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___3D3C46CC589F65C2443688952CBCD9C499839622_71)); }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  get_U33D3C46CC589F65C2443688952CBCD9C499839622_71() const { return ___3D3C46CC589F65C2443688952CBCD9C499839622_71; }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7 * get_address_of_U33D3C46CC589F65C2443688952CBCD9C499839622_71() { return &___3D3C46CC589F65C2443688952CBCD9C499839622_71; }
	inline void set_U33D3C46CC589F65C2443688952CBCD9C499839622_71(__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  value)
	{
		___3D3C46CC589F65C2443688952CBCD9C499839622_71 = value;
	}

	inline static int32_t get_offset_of_U33D8521A958BBFCC708EE0728993AE8CAFEB154D6_72() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___3D8521A958BBFCC708EE0728993AE8CAFEB154D6_72)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U33D8521A958BBFCC708EE0728993AE8CAFEB154D6_72() const { return ___3D8521A958BBFCC708EE0728993AE8CAFEB154D6_72; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U33D8521A958BBFCC708EE0728993AE8CAFEB154D6_72() { return &___3D8521A958BBFCC708EE0728993AE8CAFEB154D6_72; }
	inline void set_U33D8521A958BBFCC708EE0728993AE8CAFEB154D6_72(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___3D8521A958BBFCC708EE0728993AE8CAFEB154D6_72 = value;
	}

	inline static int32_t get_offset_of_U33DF543E51AB43F1F561B11B8789C82E26C427F72_73() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___3DF543E51AB43F1F561B11B8789C82E26C427F72_73)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U33DF543E51AB43F1F561B11B8789C82E26C427F72_73() const { return ___3DF543E51AB43F1F561B11B8789C82E26C427F72_73; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U33DF543E51AB43F1F561B11B8789C82E26C427F72_73() { return &___3DF543E51AB43F1F561B11B8789C82E26C427F72_73; }
	inline void set_U33DF543E51AB43F1F561B11B8789C82E26C427F72_73(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___3DF543E51AB43F1F561B11B8789C82E26C427F72_73 = value;
	}

	inline static int32_t get_offset_of_U33E12851A90C6A08ACCD1D67634AC7FD1639B898D_74() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___3E12851A90C6A08ACCD1D67634AC7FD1639B898D_74)); }
	inline __StaticArrayInitTypeSizeU3D11148_tA414153821EB70E6FCD456B1E2DB309D58CE1619  get_U33E12851A90C6A08ACCD1D67634AC7FD1639B898D_74() const { return ___3E12851A90C6A08ACCD1D67634AC7FD1639B898D_74; }
	inline __StaticArrayInitTypeSizeU3D11148_tA414153821EB70E6FCD456B1E2DB309D58CE1619 * get_address_of_U33E12851A90C6A08ACCD1D67634AC7FD1639B898D_74() { return &___3E12851A90C6A08ACCD1D67634AC7FD1639B898D_74; }
	inline void set_U33E12851A90C6A08ACCD1D67634AC7FD1639B898D_74(__StaticArrayInitTypeSizeU3D11148_tA414153821EB70E6FCD456B1E2DB309D58CE1619  value)
	{
		___3E12851A90C6A08ACCD1D67634AC7FD1639B898D_74 = value;
	}

	inline static int32_t get_offset_of_U33E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_75() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___3E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_75)); }
	inline __StaticArrayInitTypeSizeU3D64_t2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF  get_U33E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_75() const { return ___3E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_75; }
	inline __StaticArrayInitTypeSizeU3D64_t2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF * get_address_of_U33E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_75() { return &___3E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_75; }
	inline void set_U33E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_75(__StaticArrayInitTypeSizeU3D64_t2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF  value)
	{
		___3E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_75 = value;
	}

	inline static int32_t get_offset_of_U33E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_76() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___3E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_76)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U33E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_76() const { return ___3E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_76; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U33E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_76() { return &___3E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_76; }
	inline void set_U33E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_76(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___3E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_76 = value;
	}

	inline static int32_t get_offset_of_U33F152B3DFAF72A55D78439E5015C55A20967621A_77() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___3F152B3DFAF72A55D78439E5015C55A20967621A_77)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U33F152B3DFAF72A55D78439E5015C55A20967621A_77() const { return ___3F152B3DFAF72A55D78439E5015C55A20967621A_77; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U33F152B3DFAF72A55D78439E5015C55A20967621A_77() { return &___3F152B3DFAF72A55D78439E5015C55A20967621A_77; }
	inline void set_U33F152B3DFAF72A55D78439E5015C55A20967621A_77(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___3F152B3DFAF72A55D78439E5015C55A20967621A_77 = value;
	}

	inline static int32_t get_offset_of_U33F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_78() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___3F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_78)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U33F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_78() const { return ___3F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_78; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U33F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_78() { return &___3F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_78; }
	inline void set_U33F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_78(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___3F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_78 = value;
	}

	inline static int32_t get_offset_of_U33F6E4F79F4A57C7F0F4467AA7116C5C46FDFB54E_79() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___3F6E4F79F4A57C7F0F4467AA7116C5C46FDFB54E_79)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U33F6E4F79F4A57C7F0F4467AA7116C5C46FDFB54E_79() const { return ___3F6E4F79F4A57C7F0F4467AA7116C5C46FDFB54E_79; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U33F6E4F79F4A57C7F0F4467AA7116C5C46FDFB54E_79() { return &___3F6E4F79F4A57C7F0F4467AA7116C5C46FDFB54E_79; }
	inline void set_U33F6E4F79F4A57C7F0F4467AA7116C5C46FDFB54E_79(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___3F6E4F79F4A57C7F0F4467AA7116C5C46FDFB54E_79 = value;
	}

	inline static int32_t get_offset_of_U34065D9695EFBED47DDA95610295BA85121D5A17B_80() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___4065D9695EFBED47DDA95610295BA85121D5A17B_80)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U34065D9695EFBED47DDA95610295BA85121D5A17B_80() const { return ___4065D9695EFBED47DDA95610295BA85121D5A17B_80; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U34065D9695EFBED47DDA95610295BA85121D5A17B_80() { return &___4065D9695EFBED47DDA95610295BA85121D5A17B_80; }
	inline void set_U34065D9695EFBED47DDA95610295BA85121D5A17B_80(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___4065D9695EFBED47DDA95610295BA85121D5A17B_80 = value;
	}

	inline static int32_t get_offset_of_U340B82CCF037B966E2A6B5F4BAC86904796965A1C_81() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___40B82CCF037B966E2A6B5F4BAC86904796965A1C_81)); }
	inline __StaticArrayInitTypeSizeU3D136_tF639EB419908A28312DAF13451C27B4ED03E8674  get_U340B82CCF037B966E2A6B5F4BAC86904796965A1C_81() const { return ___40B82CCF037B966E2A6B5F4BAC86904796965A1C_81; }
	inline __StaticArrayInitTypeSizeU3D136_tF639EB419908A28312DAF13451C27B4ED03E8674 * get_address_of_U340B82CCF037B966E2A6B5F4BAC86904796965A1C_81() { return &___40B82CCF037B966E2A6B5F4BAC86904796965A1C_81; }
	inline void set_U340B82CCF037B966E2A6B5F4BAC86904796965A1C_81(__StaticArrayInitTypeSizeU3D136_tF639EB419908A28312DAF13451C27B4ED03E8674  value)
	{
		___40B82CCF037B966E2A6B5F4BAC86904796965A1C_81 = value;
	}

	inline static int32_t get_offset_of_U340FEF102AB67B5038E6D9E2EE2BE16854A49810D_82() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___40FEF102AB67B5038E6D9E2EE2BE16854A49810D_82)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U340FEF102AB67B5038E6D9E2EE2BE16854A49810D_82() const { return ___40FEF102AB67B5038E6D9E2EE2BE16854A49810D_82; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U340FEF102AB67B5038E6D9E2EE2BE16854A49810D_82() { return &___40FEF102AB67B5038E6D9E2EE2BE16854A49810D_82; }
	inline void set_U340FEF102AB67B5038E6D9E2EE2BE16854A49810D_82(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___40FEF102AB67B5038E6D9E2EE2BE16854A49810D_82 = value;
	}

	inline static int32_t get_offset_of_U342A99199D41E8832E72CD942706DDA326C0BB356_83() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___42A99199D41E8832E72CD942706DDA326C0BB356_83)); }
	inline __StaticArrayInitTypeSizeU3D176_t9A793B7CC486996BB07636533D64428F9FE21236  get_U342A99199D41E8832E72CD942706DDA326C0BB356_83() const { return ___42A99199D41E8832E72CD942706DDA326C0BB356_83; }
	inline __StaticArrayInitTypeSizeU3D176_t9A793B7CC486996BB07636533D64428F9FE21236 * get_address_of_U342A99199D41E8832E72CD942706DDA326C0BB356_83() { return &___42A99199D41E8832E72CD942706DDA326C0BB356_83; }
	inline void set_U342A99199D41E8832E72CD942706DDA326C0BB356_83(__StaticArrayInitTypeSizeU3D176_t9A793B7CC486996BB07636533D64428F9FE21236  value)
	{
		___42A99199D41E8832E72CD942706DDA326C0BB356_83 = value;
	}

	inline static int32_t get_offset_of_U3445E0C138E7D9647FCD08184FB23A833C4D00D0D_84() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___445E0C138E7D9647FCD08184FB23A833C4D00D0D_84)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U3445E0C138E7D9647FCD08184FB23A833C4D00D0D_84() const { return ___445E0C138E7D9647FCD08184FB23A833C4D00D0D_84; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U3445E0C138E7D9647FCD08184FB23A833C4D00D0D_84() { return &___445E0C138E7D9647FCD08184FB23A833C4D00D0D_84; }
	inline void set_U3445E0C138E7D9647FCD08184FB23A833C4D00D0D_84(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___445E0C138E7D9647FCD08184FB23A833C4D00D0D_84 = value;
	}

	inline static int32_t get_offset_of_U344DB2483C3365FFD6E86B5C807C599AAF3EEE238_85() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___44DB2483C3365FFD6E86B5C807C599AAF3EEE238_85)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U344DB2483C3365FFD6E86B5C807C599AAF3EEE238_85() const { return ___44DB2483C3365FFD6E86B5C807C599AAF3EEE238_85; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U344DB2483C3365FFD6E86B5C807C599AAF3EEE238_85() { return &___44DB2483C3365FFD6E86B5C807C599AAF3EEE238_85; }
	inline void set_U344DB2483C3365FFD6E86B5C807C599AAF3EEE238_85(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___44DB2483C3365FFD6E86B5C807C599AAF3EEE238_85 = value;
	}

	inline static int32_t get_offset_of_U3454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_86() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_86)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U3454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_86() const { return ___454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_86; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U3454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_86() { return &___454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_86; }
	inline void set_U3454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_86(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_86 = value;
	}

	inline static int32_t get_offset_of_U3463C3B3709985BB32747ACC9AE3406BB6836368B_87() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___463C3B3709985BB32747ACC9AE3406BB6836368B_87)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U3463C3B3709985BB32747ACC9AE3406BB6836368B_87() const { return ___463C3B3709985BB32747ACC9AE3406BB6836368B_87; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U3463C3B3709985BB32747ACC9AE3406BB6836368B_87() { return &___463C3B3709985BB32747ACC9AE3406BB6836368B_87; }
	inline void set_U3463C3B3709985BB32747ACC9AE3406BB6836368B_87(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___463C3B3709985BB32747ACC9AE3406BB6836368B_87 = value;
	}

	inline static int32_t get_offset_of_U3465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_88() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_88)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U3465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_88() const { return ___465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_88; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U3465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_88() { return &___465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_88; }
	inline void set_U3465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_88(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_88 = value;
	}

	inline static int32_t get_offset_of_U3471648086F9480CB553B7802323E0A75FFE09C5A_89() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___471648086F9480CB553B7802323E0A75FFE09C5A_89)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U3471648086F9480CB553B7802323E0A75FFE09C5A_89() const { return ___471648086F9480CB553B7802323E0A75FFE09C5A_89; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U3471648086F9480CB553B7802323E0A75FFE09C5A_89() { return &___471648086F9480CB553B7802323E0A75FFE09C5A_89; }
	inline void set_U3471648086F9480CB553B7802323E0A75FFE09C5A_89(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___471648086F9480CB553B7802323E0A75FFE09C5A_89 = value;
	}

	inline static int32_t get_offset_of_U3484B207F3CBCE8338BDE26F04E87EE256891BE0F_90() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___484B207F3CBCE8338BDE26F04E87EE256891BE0F_90)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U3484B207F3CBCE8338BDE26F04E87EE256891BE0F_90() const { return ___484B207F3CBCE8338BDE26F04E87EE256891BE0F_90; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U3484B207F3CBCE8338BDE26F04E87EE256891BE0F_90() { return &___484B207F3CBCE8338BDE26F04E87EE256891BE0F_90; }
	inline void set_U3484B207F3CBCE8338BDE26F04E87EE256891BE0F_90(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___484B207F3CBCE8338BDE26F04E87EE256891BE0F_90 = value;
	}

	inline static int32_t get_offset_of_U348F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_91() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___48F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_91)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U348F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_91() const { return ___48F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_91; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U348F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_91() { return &___48F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_91; }
	inline void set_U348F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_91(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___48F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_91 = value;
	}

	inline static int32_t get_offset_of_U348F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_92() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___48F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_92)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U348F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_92() const { return ___48F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_92; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U348F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_92() { return &___48F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_92; }
	inline void set_U348F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_92(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___48F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_92 = value;
	}

	inline static int32_t get_offset_of_U349594851C4B6C279C35B162F0236A99E7C31DD36_93() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___49594851C4B6C279C35B162F0236A99E7C31DD36_93)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U349594851C4B6C279C35B162F0236A99E7C31DD36_93() const { return ___49594851C4B6C279C35B162F0236A99E7C31DD36_93; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U349594851C4B6C279C35B162F0236A99E7C31DD36_93() { return &___49594851C4B6C279C35B162F0236A99E7C31DD36_93; }
	inline void set_U349594851C4B6C279C35B162F0236A99E7C31DD36_93(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___49594851C4B6C279C35B162F0236A99E7C31DD36_93 = value;
	}

	inline static int32_t get_offset_of_U349E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_94() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___49E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_94)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U349E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_94() const { return ___49E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_94; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U349E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_94() { return &___49E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_94; }
	inline void set_U349E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_94(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___49E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_94 = value;
	}

	inline static int32_t get_offset_of_U34B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_95() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___4B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_95)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U34B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_95() const { return ___4B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_95; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U34B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_95() { return &___4B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_95; }
	inline void set_U34B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_95(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___4B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_95 = value;
	}

	inline static int32_t get_offset_of_U34CC0A0F757356B175679AE3FDB722375172ABA96_96() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___4CC0A0F757356B175679AE3FDB722375172ABA96_96)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U34CC0A0F757356B175679AE3FDB722375172ABA96_96() const { return ___4CC0A0F757356B175679AE3FDB722375172ABA96_96; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U34CC0A0F757356B175679AE3FDB722375172ABA96_96() { return &___4CC0A0F757356B175679AE3FDB722375172ABA96_96; }
	inline void set_U34CC0A0F757356B175679AE3FDB722375172ABA96_96(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___4CC0A0F757356B175679AE3FDB722375172ABA96_96 = value;
	}

	inline static int32_t get_offset_of_U34CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_97() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___4CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_97)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U34CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_97() const { return ___4CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_97; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U34CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_97() { return &___4CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_97; }
	inline void set_U34CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_97(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___4CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_97 = value;
	}

	inline static int32_t get_offset_of_U34D317202204E9741354BC1A1628BC10A05E2AB7B_98() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___4D317202204E9741354BC1A1628BC10A05E2AB7B_98)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U34D317202204E9741354BC1A1628BC10A05E2AB7B_98() const { return ___4D317202204E9741354BC1A1628BC10A05E2AB7B_98; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U34D317202204E9741354BC1A1628BC10A05E2AB7B_98() { return &___4D317202204E9741354BC1A1628BC10A05E2AB7B_98; }
	inline void set_U34D317202204E9741354BC1A1628BC10A05E2AB7B_98(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___4D317202204E9741354BC1A1628BC10A05E2AB7B_98 = value;
	}

	inline static int32_t get_offset_of_U34DE742FEB32578958FCFC33D6732EFF616C8A94B_99() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___4DE742FEB32578958FCFC33D6732EFF616C8A94B_99)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U34DE742FEB32578958FCFC33D6732EFF616C8A94B_99() const { return ___4DE742FEB32578958FCFC33D6732EFF616C8A94B_99; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U34DE742FEB32578958FCFC33D6732EFF616C8A94B_99() { return &___4DE742FEB32578958FCFC33D6732EFF616C8A94B_99; }
	inline void set_U34DE742FEB32578958FCFC33D6732EFF616C8A94B_99(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___4DE742FEB32578958FCFC33D6732EFF616C8A94B_99 = value;
	}

	inline static int32_t get_offset_of_U3501D7C551BDFD3885F36478424EEE5B72766C842_100() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___501D7C551BDFD3885F36478424EEE5B72766C842_100)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U3501D7C551BDFD3885F36478424EEE5B72766C842_100() const { return ___501D7C551BDFD3885F36478424EEE5B72766C842_100; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U3501D7C551BDFD3885F36478424EEE5B72766C842_100() { return &___501D7C551BDFD3885F36478424EEE5B72766C842_100; }
	inline void set_U3501D7C551BDFD3885F36478424EEE5B72766C842_100(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___501D7C551BDFD3885F36478424EEE5B72766C842_100 = value;
	}

	inline static int32_t get_offset_of_U350DACD1562B0163C9FC5B673478D4231A4B37B1F_101() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___50DACD1562B0163C9FC5B673478D4231A4B37B1F_101)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U350DACD1562B0163C9FC5B673478D4231A4B37B1F_101() const { return ___50DACD1562B0163C9FC5B673478D4231A4B37B1F_101; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U350DACD1562B0163C9FC5B673478D4231A4B37B1F_101() { return &___50DACD1562B0163C9FC5B673478D4231A4B37B1F_101; }
	inline void set_U350DACD1562B0163C9FC5B673478D4231A4B37B1F_101(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___50DACD1562B0163C9FC5B673478D4231A4B37B1F_101 = value;
	}

	inline static int32_t get_offset_of_U351109782EA6E98E8E277657055A598684D21520E_102() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___51109782EA6E98E8E277657055A598684D21520E_102)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U351109782EA6E98E8E277657055A598684D21520E_102() const { return ___51109782EA6E98E8E277657055A598684D21520E_102; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U351109782EA6E98E8E277657055A598684D21520E_102() { return &___51109782EA6E98E8E277657055A598684D21520E_102; }
	inline void set_U351109782EA6E98E8E277657055A598684D21520E_102(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___51109782EA6E98E8E277657055A598684D21520E_102 = value;
	}

	inline static int32_t get_offset_of_U3526BB16C9A1C821B19858E18142A1F7073BC648D_103() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___526BB16C9A1C821B19858E18142A1F7073BC648D_103)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U3526BB16C9A1C821B19858E18142A1F7073BC648D_103() const { return ___526BB16C9A1C821B19858E18142A1F7073BC648D_103; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U3526BB16C9A1C821B19858E18142A1F7073BC648D_103() { return &___526BB16C9A1C821B19858E18142A1F7073BC648D_103; }
	inline void set_U3526BB16C9A1C821B19858E18142A1F7073BC648D_103(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___526BB16C9A1C821B19858E18142A1F7073BC648D_103 = value;
	}

	inline static int32_t get_offset_of_U352EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_104() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___52EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_104)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U352EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_104() const { return ___52EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_104; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U352EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_104() { return &___52EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_104; }
	inline void set_U352EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_104(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___52EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_104 = value;
	}

	inline static int32_t get_offset_of_U353173106B161530CA26C69983B44BBE2FAB890D0_105() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___53173106B161530CA26C69983B44BBE2FAB890D0_105)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U353173106B161530CA26C69983B44BBE2FAB890D0_105() const { return ___53173106B161530CA26C69983B44BBE2FAB890D0_105; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U353173106B161530CA26C69983B44BBE2FAB890D0_105() { return &___53173106B161530CA26C69983B44BBE2FAB890D0_105; }
	inline void set_U353173106B161530CA26C69983B44BBE2FAB890D0_105(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___53173106B161530CA26C69983B44BBE2FAB890D0_105 = value;
	}

	inline static int32_t get_offset_of_U3539B26D17C2DEB88FB9E07611D164408E23C35AB_106() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___539B26D17C2DEB88FB9E07611D164408E23C35AB_106)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U3539B26D17C2DEB88FB9E07611D164408E23C35AB_106() const { return ___539B26D17C2DEB88FB9E07611D164408E23C35AB_106; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U3539B26D17C2DEB88FB9E07611D164408E23C35AB_106() { return &___539B26D17C2DEB88FB9E07611D164408E23C35AB_106; }
	inline void set_U3539B26D17C2DEB88FB9E07611D164408E23C35AB_106(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___539B26D17C2DEB88FB9E07611D164408E23C35AB_106 = value;
	}

	inline static int32_t get_offset_of_U353D98E6046BB88C14DD740E4F77B16402271243B_107() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___53D98E6046BB88C14DD740E4F77B16402271243B_107)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U353D98E6046BB88C14DD740E4F77B16402271243B_107() const { return ___53D98E6046BB88C14DD740E4F77B16402271243B_107; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U353D98E6046BB88C14DD740E4F77B16402271243B_107() { return &___53D98E6046BB88C14DD740E4F77B16402271243B_107; }
	inline void set_U353D98E6046BB88C14DD740E4F77B16402271243B_107(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___53D98E6046BB88C14DD740E4F77B16402271243B_107 = value;
	}

	inline static int32_t get_offset_of_U3553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_108() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_108)); }
	inline __StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  get_U3553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_108() const { return ___553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_108; }
	inline __StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB * get_address_of_U3553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_108() { return &___553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_108; }
	inline void set_U3553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_108(__StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  value)
	{
		___553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_108 = value;
	}

	inline static int32_t get_offset_of_U355A6E22CC24364881F62CCF67CBE9A348DC602FA_109() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___55A6E22CC24364881F62CCF67CBE9A348DC602FA_109)); }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  get_U355A6E22CC24364881F62CCF67CBE9A348DC602FA_109() const { return ___55A6E22CC24364881F62CCF67CBE9A348DC602FA_109; }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 * get_address_of_U355A6E22CC24364881F62CCF67CBE9A348DC602FA_109() { return &___55A6E22CC24364881F62CCF67CBE9A348DC602FA_109; }
	inline void set_U355A6E22CC24364881F62CCF67CBE9A348DC602FA_109(__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  value)
	{
		___55A6E22CC24364881F62CCF67CBE9A348DC602FA_109 = value;
	}

	inline static int32_t get_offset_of_U356A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_110() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___56A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_110)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U356A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_110() const { return ___56A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_110; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U356A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_110() { return &___56A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_110; }
	inline void set_U356A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_110(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___56A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_110 = value;
	}

	inline static int32_t get_offset_of_U356F75F925774608B6BF88F69D6124CB44DDF42E1_111() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___56F75F925774608B6BF88F69D6124CB44DDF42E1_111)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U356F75F925774608B6BF88F69D6124CB44DDF42E1_111() const { return ___56F75F925774608B6BF88F69D6124CB44DDF42E1_111; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U356F75F925774608B6BF88F69D6124CB44DDF42E1_111() { return &___56F75F925774608B6BF88F69D6124CB44DDF42E1_111; }
	inline void set_U356F75F925774608B6BF88F69D6124CB44DDF42E1_111(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___56F75F925774608B6BF88F69D6124CB44DDF42E1_111 = value;
	}

	inline static int32_t get_offset_of_U35741ABC064FCDEF4C5EE02F31DB992C205EA7231_112() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___5741ABC064FCDEF4C5EE02F31DB992C205EA7231_112)); }
	inline __StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  get_U35741ABC064FCDEF4C5EE02F31DB992C205EA7231_112() const { return ___5741ABC064FCDEF4C5EE02F31DB992C205EA7231_112; }
	inline __StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB * get_address_of_U35741ABC064FCDEF4C5EE02F31DB992C205EA7231_112() { return &___5741ABC064FCDEF4C5EE02F31DB992C205EA7231_112; }
	inline void set_U35741ABC064FCDEF4C5EE02F31DB992C205EA7231_112(__StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  value)
	{
		___5741ABC064FCDEF4C5EE02F31DB992C205EA7231_112 = value;
	}

	inline static int32_t get_offset_of_U3583A438479F42C7D8DE8C6699B0C37A4A026A643_113() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___583A438479F42C7D8DE8C6699B0C37A4A026A643_113)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U3583A438479F42C7D8DE8C6699B0C37A4A026A643_113() const { return ___583A438479F42C7D8DE8C6699B0C37A4A026A643_113; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U3583A438479F42C7D8DE8C6699B0C37A4A026A643_113() { return &___583A438479F42C7D8DE8C6699B0C37A4A026A643_113; }
	inline void set_U3583A438479F42C7D8DE8C6699B0C37A4A026A643_113(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___583A438479F42C7D8DE8C6699B0C37A4A026A643_113 = value;
	}

	inline static int32_t get_offset_of_U358B2CCE2545886B61E58C3C60831C2B049814E09_114() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___58B2CCE2545886B61E58C3C60831C2B049814E09_114)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U358B2CCE2545886B61E58C3C60831C2B049814E09_114() const { return ___58B2CCE2545886B61E58C3C60831C2B049814E09_114; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U358B2CCE2545886B61E58C3C60831C2B049814E09_114() { return &___58B2CCE2545886B61E58C3C60831C2B049814E09_114; }
	inline void set_U358B2CCE2545886B61E58C3C60831C2B049814E09_114(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___58B2CCE2545886B61E58C3C60831C2B049814E09_114 = value;
	}

	inline static int32_t get_offset_of_U35984617C3B1EF1E6C9F13221059A1E78C2226ADF_115() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___5984617C3B1EF1E6C9F13221059A1E78C2226ADF_115)); }
	inline __StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  get_U35984617C3B1EF1E6C9F13221059A1E78C2226ADF_115() const { return ___5984617C3B1EF1E6C9F13221059A1E78C2226ADF_115; }
	inline __StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB * get_address_of_U35984617C3B1EF1E6C9F13221059A1E78C2226ADF_115() { return &___5984617C3B1EF1E6C9F13221059A1E78C2226ADF_115; }
	inline void set_U35984617C3B1EF1E6C9F13221059A1E78C2226ADF_115(__StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  value)
	{
		___5984617C3B1EF1E6C9F13221059A1E78C2226ADF_115 = value;
	}

	inline static int32_t get_offset_of_U359A84F660FFFFA4B9C037456F9D56E26ACF3663E_116() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___59A84F660FFFFA4B9C037456F9D56E26ACF3663E_116)); }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  get_U359A84F660FFFFA4B9C037456F9D56E26ACF3663E_116() const { return ___59A84F660FFFFA4B9C037456F9D56E26ACF3663E_116; }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 * get_address_of_U359A84F660FFFFA4B9C037456F9D56E26ACF3663E_116() { return &___59A84F660FFFFA4B9C037456F9D56E26ACF3663E_116; }
	inline void set_U359A84F660FFFFA4B9C037456F9D56E26ACF3663E_116(__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  value)
	{
		___59A84F660FFFFA4B9C037456F9D56E26ACF3663E_116 = value;
	}

	inline static int32_t get_offset_of_U35B02FED4CA0B61476F1AAA5C6445F024374B075A_117() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___5B02FED4CA0B61476F1AAA5C6445F024374B075A_117)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U35B02FED4CA0B61476F1AAA5C6445F024374B075A_117() const { return ___5B02FED4CA0B61476F1AAA5C6445F024374B075A_117; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U35B02FED4CA0B61476F1AAA5C6445F024374B075A_117() { return &___5B02FED4CA0B61476F1AAA5C6445F024374B075A_117; }
	inline void set_U35B02FED4CA0B61476F1AAA5C6445F024374B075A_117(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___5B02FED4CA0B61476F1AAA5C6445F024374B075A_117 = value;
	}

	inline static int32_t get_offset_of_U35B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_118() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___5B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_118)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U35B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_118() const { return ___5B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_118; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U35B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_118() { return &___5B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_118; }
	inline void set_U35B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_118(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___5B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_118 = value;
	}

	inline static int32_t get_offset_of_U35BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_119() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___5BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_119)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U35BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_119() const { return ___5BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_119; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U35BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_119() { return &___5BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_119; }
	inline void set_U35BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_119(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___5BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_119 = value;
	}

	inline static int32_t get_offset_of_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_120() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_120)); }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  get_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_120() const { return ___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_120; }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 * get_address_of_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_120() { return &___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_120; }
	inline void set_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_120(__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  value)
	{
		___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_120 = value;
	}

	inline static int32_t get_offset_of_U35C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_121() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___5C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_121)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U35C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_121() const { return ___5C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_121; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U35C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_121() { return &___5C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_121; }
	inline void set_U35C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_121(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___5C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_121 = value;
	}

	inline static int32_t get_offset_of_U35C563BFAB6400B2824F5E41D78FB3097E8244C26_122() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___5C563BFAB6400B2824F5E41D78FB3097E8244C26_122)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U35C563BFAB6400B2824F5E41D78FB3097E8244C26_122() const { return ___5C563BFAB6400B2824F5E41D78FB3097E8244C26_122; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U35C563BFAB6400B2824F5E41D78FB3097E8244C26_122() { return &___5C563BFAB6400B2824F5E41D78FB3097E8244C26_122; }
	inline void set_U35C563BFAB6400B2824F5E41D78FB3097E8244C26_122(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___5C563BFAB6400B2824F5E41D78FB3097E8244C26_122 = value;
	}

	inline static int32_t get_offset_of_U35CB924E7461B9011EC8197A30815EA263595B3A4_123() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___5CB924E7461B9011EC8197A30815EA263595B3A4_123)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U35CB924E7461B9011EC8197A30815EA263595B3A4_123() const { return ___5CB924E7461B9011EC8197A30815EA263595B3A4_123; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U35CB924E7461B9011EC8197A30815EA263595B3A4_123() { return &___5CB924E7461B9011EC8197A30815EA263595B3A4_123; }
	inline void set_U35CB924E7461B9011EC8197A30815EA263595B3A4_123(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___5CB924E7461B9011EC8197A30815EA263595B3A4_123 = value;
	}

	inline static int32_t get_offset_of_U35D160627930BB84F7867AEEC7143FB2399C7CA79_124() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___5D160627930BB84F7867AEEC7143FB2399C7CA79_124)); }
	inline __StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  get_U35D160627930BB84F7867AEEC7143FB2399C7CA79_124() const { return ___5D160627930BB84F7867AEEC7143FB2399C7CA79_124; }
	inline __StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB * get_address_of_U35D160627930BB84F7867AEEC7143FB2399C7CA79_124() { return &___5D160627930BB84F7867AEEC7143FB2399C7CA79_124; }
	inline void set_U35D160627930BB84F7867AEEC7143FB2399C7CA79_124(__StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  value)
	{
		___5D160627930BB84F7867AEEC7143FB2399C7CA79_124 = value;
	}

	inline static int32_t get_offset_of_U35DD69D93CBAA5623AE286E97265890A7693A0AC2_125() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___5DD69D93CBAA5623AE286E97265890A7693A0AC2_125)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U35DD69D93CBAA5623AE286E97265890A7693A0AC2_125() const { return ___5DD69D93CBAA5623AE286E97265890A7693A0AC2_125; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U35DD69D93CBAA5623AE286E97265890A7693A0AC2_125() { return &___5DD69D93CBAA5623AE286E97265890A7693A0AC2_125; }
	inline void set_U35DD69D93CBAA5623AE286E97265890A7693A0AC2_125(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___5DD69D93CBAA5623AE286E97265890A7693A0AC2_125 = value;
	}

	inline static int32_t get_offset_of_U35E3F56450E340B21A3A13E39A66941353F8C7C1A_126() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___5E3F56450E340B21A3A13E39A66941353F8C7C1A_126)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U35E3F56450E340B21A3A13E39A66941353F8C7C1A_126() const { return ___5E3F56450E340B21A3A13E39A66941353F8C7C1A_126; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U35E3F56450E340B21A3A13E39A66941353F8C7C1A_126() { return &___5E3F56450E340B21A3A13E39A66941353F8C7C1A_126; }
	inline void set_U35E3F56450E340B21A3A13E39A66941353F8C7C1A_126(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___5E3F56450E340B21A3A13E39A66941353F8C7C1A_126 = value;
	}

	inline static int32_t get_offset_of_U35FD971B7E6A431E6F3F91D5A95F2006A24F48780_127() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___5FD971B7E6A431E6F3F91D5A95F2006A24F48780_127)); }
	inline __StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  get_U35FD971B7E6A431E6F3F91D5A95F2006A24F48780_127() const { return ___5FD971B7E6A431E6F3F91D5A95F2006A24F48780_127; }
	inline __StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB * get_address_of_U35FD971B7E6A431E6F3F91D5A95F2006A24F48780_127() { return &___5FD971B7E6A431E6F3F91D5A95F2006A24F48780_127; }
	inline void set_U35FD971B7E6A431E6F3F91D5A95F2006A24F48780_127(__StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB  value)
	{
		___5FD971B7E6A431E6F3F91D5A95F2006A24F48780_127 = value;
	}

	inline static int32_t get_offset_of_U360E3C19D774F8C210299534335ED5D3154717218_128() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___60E3C19D774F8C210299534335ED5D3154717218_128)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U360E3C19D774F8C210299534335ED5D3154717218_128() const { return ___60E3C19D774F8C210299534335ED5D3154717218_128; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U360E3C19D774F8C210299534335ED5D3154717218_128() { return &___60E3C19D774F8C210299534335ED5D3154717218_128; }
	inline void set_U360E3C19D774F8C210299534335ED5D3154717218_128(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___60E3C19D774F8C210299534335ED5D3154717218_128 = value;
	}

	inline static int32_t get_offset_of_U361A1021606CAD0A5990E00FD305478E75F1983D6_129() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___61A1021606CAD0A5990E00FD305478E75F1983D6_129)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U361A1021606CAD0A5990E00FD305478E75F1983D6_129() const { return ___61A1021606CAD0A5990E00FD305478E75F1983D6_129; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U361A1021606CAD0A5990E00FD305478E75F1983D6_129() { return &___61A1021606CAD0A5990E00FD305478E75F1983D6_129; }
	inline void set_U361A1021606CAD0A5990E00FD305478E75F1983D6_129(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___61A1021606CAD0A5990E00FD305478E75F1983D6_129 = value;
	}

	inline static int32_t get_offset_of_U362AB9401F4CB2B0A7FF72D3F3F56AA2F14E34FE0_130() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___62AB9401F4CB2B0A7FF72D3F3F56AA2F14E34FE0_130)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U362AB9401F4CB2B0A7FF72D3F3F56AA2F14E34FE0_130() const { return ___62AB9401F4CB2B0A7FF72D3F3F56AA2F14E34FE0_130; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U362AB9401F4CB2B0A7FF72D3F3F56AA2F14E34FE0_130() { return &___62AB9401F4CB2B0A7FF72D3F3F56AA2F14E34FE0_130; }
	inline void set_U362AB9401F4CB2B0A7FF72D3F3F56AA2F14E34FE0_130(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___62AB9401F4CB2B0A7FF72D3F3F56AA2F14E34FE0_130 = value;
	}

	inline static int32_t get_offset_of_U363B762373DE81B4580E1909D7168A21A989A182A_131() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___63B762373DE81B4580E1909D7168A21A989A182A_131)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U363B762373DE81B4580E1909D7168A21A989A182A_131() const { return ___63B762373DE81B4580E1909D7168A21A989A182A_131; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U363B762373DE81B4580E1909D7168A21A989A182A_131() { return &___63B762373DE81B4580E1909D7168A21A989A182A_131; }
	inline void set_U363B762373DE81B4580E1909D7168A21A989A182A_131(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___63B762373DE81B4580E1909D7168A21A989A182A_131 = value;
	}

	inline static int32_t get_offset_of_U3648C489642395164C2A2AA2379A6E7F44CAB018B_132() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___648C489642395164C2A2AA2379A6E7F44CAB018B_132)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U3648C489642395164C2A2AA2379A6E7F44CAB018B_132() const { return ___648C489642395164C2A2AA2379A6E7F44CAB018B_132; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U3648C489642395164C2A2AA2379A6E7F44CAB018B_132() { return &___648C489642395164C2A2AA2379A6E7F44CAB018B_132; }
	inline void set_U3648C489642395164C2A2AA2379A6E7F44CAB018B_132(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___648C489642395164C2A2AA2379A6E7F44CAB018B_132 = value;
	}

	inline static int32_t get_offset_of_U3661F9E9FEA16C3A9CDF452AB61743578471F7447_133() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___661F9E9FEA16C3A9CDF452AB61743578471F7447_133)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U3661F9E9FEA16C3A9CDF452AB61743578471F7447_133() const { return ___661F9E9FEA16C3A9CDF452AB61743578471F7447_133; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U3661F9E9FEA16C3A9CDF452AB61743578471F7447_133() { return &___661F9E9FEA16C3A9CDF452AB61743578471F7447_133; }
	inline void set_U3661F9E9FEA16C3A9CDF452AB61743578471F7447_133(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___661F9E9FEA16C3A9CDF452AB61743578471F7447_133 = value;
	}

	inline static int32_t get_offset_of_U367013075958AAD75C4545796BF80B16683B33A59_134() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___67013075958AAD75C4545796BF80B16683B33A59_134)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U367013075958AAD75C4545796BF80B16683B33A59_134() const { return ___67013075958AAD75C4545796BF80B16683B33A59_134; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U367013075958AAD75C4545796BF80B16683B33A59_134() { return &___67013075958AAD75C4545796BF80B16683B33A59_134; }
	inline void set_U367013075958AAD75C4545796BF80B16683B33A59_134(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___67013075958AAD75C4545796BF80B16683B33A59_134 = value;
	}

	inline static int32_t get_offset_of_U36727D77DB74B08CC9003029AC92B7AC68E258811_135() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___6727D77DB74B08CC9003029AC92B7AC68E258811_135)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U36727D77DB74B08CC9003029AC92B7AC68E258811_135() const { return ___6727D77DB74B08CC9003029AC92B7AC68E258811_135; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U36727D77DB74B08CC9003029AC92B7AC68E258811_135() { return &___6727D77DB74B08CC9003029AC92B7AC68E258811_135; }
	inline void set_U36727D77DB74B08CC9003029AC92B7AC68E258811_135(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___6727D77DB74B08CC9003029AC92B7AC68E258811_135 = value;
	}

	inline static int32_t get_offset_of_U36882914B984B24C4880E3BEA646DC549B3C34ABC_136() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___6882914B984B24C4880E3BEA646DC549B3C34ABC_136)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U36882914B984B24C4880E3BEA646DC549B3C34ABC_136() const { return ___6882914B984B24C4880E3BEA646DC549B3C34ABC_136; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U36882914B984B24C4880E3BEA646DC549B3C34ABC_136() { return &___6882914B984B24C4880E3BEA646DC549B3C34ABC_136; }
	inline void set_U36882914B984B24C4880E3BEA646DC549B3C34ABC_136(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___6882914B984B24C4880E3BEA646DC549B3C34ABC_136 = value;
	}

	inline static int32_t get_offset_of_U36891D97C19F4C291AD821EF05E4C5D1BD3C34796_137() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___6891D97C19F4C291AD821EF05E4C5D1BD3C34796_137)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U36891D97C19F4C291AD821EF05E4C5D1BD3C34796_137() const { return ___6891D97C19F4C291AD821EF05E4C5D1BD3C34796_137; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U36891D97C19F4C291AD821EF05E4C5D1BD3C34796_137() { return &___6891D97C19F4C291AD821EF05E4C5D1BD3C34796_137; }
	inline void set_U36891D97C19F4C291AD821EF05E4C5D1BD3C34796_137(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___6891D97C19F4C291AD821EF05E4C5D1BD3C34796_137 = value;
	}

	inline static int32_t get_offset_of_U369499AF6438C222AAADFC92FF3D3DF3902A454FC_138() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___69499AF6438C222AAADFC92FF3D3DF3902A454FC_138)); }
	inline __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  get_U369499AF6438C222AAADFC92FF3D3DF3902A454FC_138() const { return ___69499AF6438C222AAADFC92FF3D3DF3902A454FC_138; }
	inline __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F * get_address_of_U369499AF6438C222AAADFC92FF3D3DF3902A454FC_138() { return &___69499AF6438C222AAADFC92FF3D3DF3902A454FC_138; }
	inline void set_U369499AF6438C222AAADFC92FF3D3DF3902A454FC_138(__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  value)
	{
		___69499AF6438C222AAADFC92FF3D3DF3902A454FC_138 = value;
	}

	inline static int32_t get_offset_of_U36957A133E0DC21707BCAAF62194EAF4FF75E0768_139() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___6957A133E0DC21707BCAAF62194EAF4FF75E0768_139)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U36957A133E0DC21707BCAAF62194EAF4FF75E0768_139() const { return ___6957A133E0DC21707BCAAF62194EAF4FF75E0768_139; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U36957A133E0DC21707BCAAF62194EAF4FF75E0768_139() { return &___6957A133E0DC21707BCAAF62194EAF4FF75E0768_139; }
	inline void set_U36957A133E0DC21707BCAAF62194EAF4FF75E0768_139(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___6957A133E0DC21707BCAAF62194EAF4FF75E0768_139 = value;
	}

	inline static int32_t get_offset_of_U369D489A8DADAF0948858B02F2CC7258B1530D843_140() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___69D489A8DADAF0948858B02F2CC7258B1530D843_140)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U369D489A8DADAF0948858B02F2CC7258B1530D843_140() const { return ___69D489A8DADAF0948858B02F2CC7258B1530D843_140; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U369D489A8DADAF0948858B02F2CC7258B1530D843_140() { return &___69D489A8DADAF0948858B02F2CC7258B1530D843_140; }
	inline void set_U369D489A8DADAF0948858B02F2CC7258B1530D843_140(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___69D489A8DADAF0948858B02F2CC7258B1530D843_140 = value;
	}

	inline static int32_t get_offset_of_U36A09CB785087F5A9F6EC748C7C10F98FA2683534_141() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___6A09CB785087F5A9F6EC748C7C10F98FA2683534_141)); }
	inline __StaticArrayInitTypeSizeU3D36_t2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17  get_U36A09CB785087F5A9F6EC748C7C10F98FA2683534_141() const { return ___6A09CB785087F5A9F6EC748C7C10F98FA2683534_141; }
	inline __StaticArrayInitTypeSizeU3D36_t2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17 * get_address_of_U36A09CB785087F5A9F6EC748C7C10F98FA2683534_141() { return &___6A09CB785087F5A9F6EC748C7C10F98FA2683534_141; }
	inline void set_U36A09CB785087F5A9F6EC748C7C10F98FA2683534_141(__StaticArrayInitTypeSizeU3D36_t2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17  value)
	{
		___6A09CB785087F5A9F6EC748C7C10F98FA2683534_141 = value;
	}

	inline static int32_t get_offset_of_U36AA66657BB3292572C8F56AB3D015A7990C43606_142() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___6AA66657BB3292572C8F56AB3D015A7990C43606_142)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U36AA66657BB3292572C8F56AB3D015A7990C43606_142() const { return ___6AA66657BB3292572C8F56AB3D015A7990C43606_142; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U36AA66657BB3292572C8F56AB3D015A7990C43606_142() { return &___6AA66657BB3292572C8F56AB3D015A7990C43606_142; }
	inline void set_U36AA66657BB3292572C8F56AB3D015A7990C43606_142(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___6AA66657BB3292572C8F56AB3D015A7990C43606_142 = value;
	}

	inline static int32_t get_offset_of_U36AF652181FBB423ED995C5CF63D5B83726DE74CF_143() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___6AF652181FBB423ED995C5CF63D5B83726DE74CF_143)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U36AF652181FBB423ED995C5CF63D5B83726DE74CF_143() const { return ___6AF652181FBB423ED995C5CF63D5B83726DE74CF_143; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U36AF652181FBB423ED995C5CF63D5B83726DE74CF_143() { return &___6AF652181FBB423ED995C5CF63D5B83726DE74CF_143; }
	inline void set_U36AF652181FBB423ED995C5CF63D5B83726DE74CF_143(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___6AF652181FBB423ED995C5CF63D5B83726DE74CF_143 = value;
	}

	inline static int32_t get_offset_of_U36B38D84409B2091CBC93686439AC2C04C76DB96E_144() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___6B38D84409B2091CBC93686439AC2C04C76DB96E_144)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U36B38D84409B2091CBC93686439AC2C04C76DB96E_144() const { return ___6B38D84409B2091CBC93686439AC2C04C76DB96E_144; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U36B38D84409B2091CBC93686439AC2C04C76DB96E_144() { return &___6B38D84409B2091CBC93686439AC2C04C76DB96E_144; }
	inline void set_U36B38D84409B2091CBC93686439AC2C04C76DB96E_144(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___6B38D84409B2091CBC93686439AC2C04C76DB96E_144 = value;
	}

	inline static int32_t get_offset_of_U36CC1359B9ED72500183D5566D70BBEE1BBD46FFF_145() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___6CC1359B9ED72500183D5566D70BBEE1BBD46FFF_145)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U36CC1359B9ED72500183D5566D70BBEE1BBD46FFF_145() const { return ___6CC1359B9ED72500183D5566D70BBEE1BBD46FFF_145; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U36CC1359B9ED72500183D5566D70BBEE1BBD46FFF_145() { return &___6CC1359B9ED72500183D5566D70BBEE1BBD46FFF_145; }
	inline void set_U36CC1359B9ED72500183D5566D70BBEE1BBD46FFF_145(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___6CC1359B9ED72500183D5566D70BBEE1BBD46FFF_145 = value;
	}

	inline static int32_t get_offset_of_U36CEDD7504235CF66C53FB70A17ACBDD440D77C62_146() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___6CEDD7504235CF66C53FB70A17ACBDD440D77C62_146)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U36CEDD7504235CF66C53FB70A17ACBDD440D77C62_146() const { return ___6CEDD7504235CF66C53FB70A17ACBDD440D77C62_146; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U36CEDD7504235CF66C53FB70A17ACBDD440D77C62_146() { return &___6CEDD7504235CF66C53FB70A17ACBDD440D77C62_146; }
	inline void set_U36CEDD7504235CF66C53FB70A17ACBDD440D77C62_146(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___6CEDD7504235CF66C53FB70A17ACBDD440D77C62_146 = value;
	}

	inline static int32_t get_offset_of_U36DE89499E8F85867289E1690E176E1DE58EE6DD3_147() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___6DE89499E8F85867289E1690E176E1DE58EE6DD3_147)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U36DE89499E8F85867289E1690E176E1DE58EE6DD3_147() const { return ___6DE89499E8F85867289E1690E176E1DE58EE6DD3_147; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U36DE89499E8F85867289E1690E176E1DE58EE6DD3_147() { return &___6DE89499E8F85867289E1690E176E1DE58EE6DD3_147; }
	inline void set_U36DE89499E8F85867289E1690E176E1DE58EE6DD3_147(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___6DE89499E8F85867289E1690E176E1DE58EE6DD3_147 = value;
	}

	inline static int32_t get_offset_of_U36E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_148() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___6E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_148)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U36E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_148() const { return ___6E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_148; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U36E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_148() { return &___6E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_148; }
	inline void set_U36E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_148(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___6E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_148 = value;
	}

	inline static int32_t get_offset_of_U36F604C33212E20CBFCE9F2221492DC6FD823857C_149() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___6F604C33212E20CBFCE9F2221492DC6FD823857C_149)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U36F604C33212E20CBFCE9F2221492DC6FD823857C_149() const { return ___6F604C33212E20CBFCE9F2221492DC6FD823857C_149; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U36F604C33212E20CBFCE9F2221492DC6FD823857C_149() { return &___6F604C33212E20CBFCE9F2221492DC6FD823857C_149; }
	inline void set_U36F604C33212E20CBFCE9F2221492DC6FD823857C_149(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___6F604C33212E20CBFCE9F2221492DC6FD823857C_149 = value;
	}

	inline static int32_t get_offset_of_U370CFE27207DDB1C62DD3A9DA22F21F3B1831D7D5_150() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___70CFE27207DDB1C62DD3A9DA22F21F3B1831D7D5_150)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U370CFE27207DDB1C62DD3A9DA22F21F3B1831D7D5_150() const { return ___70CFE27207DDB1C62DD3A9DA22F21F3B1831D7D5_150; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U370CFE27207DDB1C62DD3A9DA22F21F3B1831D7D5_150() { return &___70CFE27207DDB1C62DD3A9DA22F21F3B1831D7D5_150; }
	inline void set_U370CFE27207DDB1C62DD3A9DA22F21F3B1831D7D5_150(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___70CFE27207DDB1C62DD3A9DA22F21F3B1831D7D5_150 = value;
	}

	inline static int32_t get_offset_of_U37102A07EDF52C052D5625D73275A579DC6E76C92_151() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___7102A07EDF52C052D5625D73275A579DC6E76C92_151)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U37102A07EDF52C052D5625D73275A579DC6E76C92_151() const { return ___7102A07EDF52C052D5625D73275A579DC6E76C92_151; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U37102A07EDF52C052D5625D73275A579DC6E76C92_151() { return &___7102A07EDF52C052D5625D73275A579DC6E76C92_151; }
	inline void set_U37102A07EDF52C052D5625D73275A579DC6E76C92_151(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___7102A07EDF52C052D5625D73275A579DC6E76C92_151 = value;
	}

	inline static int32_t get_offset_of_U3712B24DD3C17671C78BE33C83316576AD9285273_152() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___712B24DD3C17671C78BE33C83316576AD9285273_152)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U3712B24DD3C17671C78BE33C83316576AD9285273_152() const { return ___712B24DD3C17671C78BE33C83316576AD9285273_152; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U3712B24DD3C17671C78BE33C83316576AD9285273_152() { return &___712B24DD3C17671C78BE33C83316576AD9285273_152; }
	inline void set_U3712B24DD3C17671C78BE33C83316576AD9285273_152(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___712B24DD3C17671C78BE33C83316576AD9285273_152 = value;
	}

	inline static int32_t get_offset_of_U371D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_153() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___71D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_153)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U371D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_153() const { return ___71D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_153; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U371D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_153() { return &___71D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_153; }
	inline void set_U371D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_153(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___71D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_153 = value;
	}

	inline static int32_t get_offset_of_U371F4D547D52F31EDEC55B1909114455FDAE5A97E_154() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___71F4D547D52F31EDEC55B1909114455FDAE5A97E_154)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U371F4D547D52F31EDEC55B1909114455FDAE5A97E_154() const { return ___71F4D547D52F31EDEC55B1909114455FDAE5A97E_154; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U371F4D547D52F31EDEC55B1909114455FDAE5A97E_154() { return &___71F4D547D52F31EDEC55B1909114455FDAE5A97E_154; }
	inline void set_U371F4D547D52F31EDEC55B1909114455FDAE5A97E_154(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___71F4D547D52F31EDEC55B1909114455FDAE5A97E_154 = value;
	}

	inline static int32_t get_offset_of_U372CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_155() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___72CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_155)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U372CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_155() const { return ___72CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_155; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U372CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_155() { return &___72CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_155; }
	inline void set_U372CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_155(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___72CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_155 = value;
	}

	inline static int32_t get_offset_of_U372FF92F22AB5BD5A344C652E176645C52D825AEC_156() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___72FF92F22AB5BD5A344C652E176645C52D825AEC_156)); }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  get_U372FF92F22AB5BD5A344C652E176645C52D825AEC_156() const { return ___72FF92F22AB5BD5A344C652E176645C52D825AEC_156; }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7 * get_address_of_U372FF92F22AB5BD5A344C652E176645C52D825AEC_156() { return &___72FF92F22AB5BD5A344C652E176645C52D825AEC_156; }
	inline void set_U372FF92F22AB5BD5A344C652E176645C52D825AEC_156(__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  value)
	{
		___72FF92F22AB5BD5A344C652E176645C52D825AEC_156 = value;
	}

	inline static int32_t get_offset_of_U373B101BBEDC241CE765CA03BB6E4D467FA769312_157() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___73B101BBEDC241CE765CA03BB6E4D467FA769312_157)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U373B101BBEDC241CE765CA03BB6E4D467FA769312_157() const { return ___73B101BBEDC241CE765CA03BB6E4D467FA769312_157; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U373B101BBEDC241CE765CA03BB6E4D467FA769312_157() { return &___73B101BBEDC241CE765CA03BB6E4D467FA769312_157; }
	inline void set_U373B101BBEDC241CE765CA03BB6E4D467FA769312_157(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___73B101BBEDC241CE765CA03BB6E4D467FA769312_157 = value;
	}

	inline static int32_t get_offset_of_U3749EB212DEC39495349481F49F3DEF480777A197_158() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___749EB212DEC39495349481F49F3DEF480777A197_158)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U3749EB212DEC39495349481F49F3DEF480777A197_158() const { return ___749EB212DEC39495349481F49F3DEF480777A197_158; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U3749EB212DEC39495349481F49F3DEF480777A197_158() { return &___749EB212DEC39495349481F49F3DEF480777A197_158; }
	inline void set_U3749EB212DEC39495349481F49F3DEF480777A197_158(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___749EB212DEC39495349481F49F3DEF480777A197_158 = value;
	}

	inline static int32_t get_offset_of_U374AE47B2D78B62583BA50045F6B288CACC24EA7D_159() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___74AE47B2D78B62583BA50045F6B288CACC24EA7D_159)); }
	inline __StaticArrayInitTypeSizeU3D2574_t94A56C994C4EA1F17B7D51314A88B67247C9BC15  get_U374AE47B2D78B62583BA50045F6B288CACC24EA7D_159() const { return ___74AE47B2D78B62583BA50045F6B288CACC24EA7D_159; }
	inline __StaticArrayInitTypeSizeU3D2574_t94A56C994C4EA1F17B7D51314A88B67247C9BC15 * get_address_of_U374AE47B2D78B62583BA50045F6B288CACC24EA7D_159() { return &___74AE47B2D78B62583BA50045F6B288CACC24EA7D_159; }
	inline void set_U374AE47B2D78B62583BA50045F6B288CACC24EA7D_159(__StaticArrayInitTypeSizeU3D2574_t94A56C994C4EA1F17B7D51314A88B67247C9BC15  value)
	{
		___74AE47B2D78B62583BA50045F6B288CACC24EA7D_159 = value;
	}

	inline static int32_t get_offset_of_U37616E4F44CA76DF539C11B9C8FD911D667FEFC52_160() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___7616E4F44CA76DF539C11B9C8FD911D667FEFC52_160)); }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  get_U37616E4F44CA76DF539C11B9C8FD911D667FEFC52_160() const { return ___7616E4F44CA76DF539C11B9C8FD911D667FEFC52_160; }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 * get_address_of_U37616E4F44CA76DF539C11B9C8FD911D667FEFC52_160() { return &___7616E4F44CA76DF539C11B9C8FD911D667FEFC52_160; }
	inline void set_U37616E4F44CA76DF539C11B9C8FD911D667FEFC52_160(__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  value)
	{
		___7616E4F44CA76DF539C11B9C8FD911D667FEFC52_160 = value;
	}

	inline static int32_t get_offset_of_U3761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_161() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_161)); }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  get_U3761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_161() const { return ___761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_161; }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 * get_address_of_U3761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_161() { return &___761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_161; }
	inline void set_U3761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_161(__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  value)
	{
		___761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_161 = value;
	}

	inline static int32_t get_offset_of_U376D7CB1729978C5437409A8C56AA1C7E24890D5C_162() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___76D7CB1729978C5437409A8C56AA1C7E24890D5C_162)); }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  get_U376D7CB1729978C5437409A8C56AA1C7E24890D5C_162() const { return ___76D7CB1729978C5437409A8C56AA1C7E24890D5C_162; }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 * get_address_of_U376D7CB1729978C5437409A8C56AA1C7E24890D5C_162() { return &___76D7CB1729978C5437409A8C56AA1C7E24890D5C_162; }
	inline void set_U376D7CB1729978C5437409A8C56AA1C7E24890D5C_162(__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  value)
	{
		___76D7CB1729978C5437409A8C56AA1C7E24890D5C_162 = value;
	}

	inline static int32_t get_offset_of_U3770616E567083077933DC0B29A8F8368FC2EB431_163() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___770616E567083077933DC0B29A8F8368FC2EB431_163)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U3770616E567083077933DC0B29A8F8368FC2EB431_163() const { return ___770616E567083077933DC0B29A8F8368FC2EB431_163; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U3770616E567083077933DC0B29A8F8368FC2EB431_163() { return &___770616E567083077933DC0B29A8F8368FC2EB431_163; }
	inline void set_U3770616E567083077933DC0B29A8F8368FC2EB431_163(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___770616E567083077933DC0B29A8F8368FC2EB431_163 = value;
	}

	inline static int32_t get_offset_of_U3776EDFD0860A21D5119CA197C89BD2AC7D6F9283_164() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___776EDFD0860A21D5119CA197C89BD2AC7D6F9283_164)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U3776EDFD0860A21D5119CA197C89BD2AC7D6F9283_164() const { return ___776EDFD0860A21D5119CA197C89BD2AC7D6F9283_164; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U3776EDFD0860A21D5119CA197C89BD2AC7D6F9283_164() { return &___776EDFD0860A21D5119CA197C89BD2AC7D6F9283_164; }
	inline void set_U3776EDFD0860A21D5119CA197C89BD2AC7D6F9283_164(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___776EDFD0860A21D5119CA197C89BD2AC7D6F9283_164 = value;
	}

	inline static int32_t get_offset_of_U3783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_165() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_165)); }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  get_U3783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_165() const { return ___783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_165; }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 * get_address_of_U3783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_165() { return &___783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_165; }
	inline void set_U3783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_165(__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  value)
	{
		___783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_165 = value;
	}

	inline static int32_t get_offset_of_U379123EF44D00F5B0ED365348D1FD49F9FA2CB222_166() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___79123EF44D00F5B0ED365348D1FD49F9FA2CB222_166)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U379123EF44D00F5B0ED365348D1FD49F9FA2CB222_166() const { return ___79123EF44D00F5B0ED365348D1FD49F9FA2CB222_166; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U379123EF44D00F5B0ED365348D1FD49F9FA2CB222_166() { return &___79123EF44D00F5B0ED365348D1FD49F9FA2CB222_166; }
	inline void set_U379123EF44D00F5B0ED365348D1FD49F9FA2CB222_166(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___79123EF44D00F5B0ED365348D1FD49F9FA2CB222_166 = value;
	}

	inline static int32_t get_offset_of_U379A4B717DAF4312A78F1A75A72221DB9690D1B25_167() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___79A4B717DAF4312A78F1A75A72221DB9690D1B25_167)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U379A4B717DAF4312A78F1A75A72221DB9690D1B25_167() const { return ___79A4B717DAF4312A78F1A75A72221DB9690D1B25_167; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U379A4B717DAF4312A78F1A75A72221DB9690D1B25_167() { return &___79A4B717DAF4312A78F1A75A72221DB9690D1B25_167; }
	inline void set_U379A4B717DAF4312A78F1A75A72221DB9690D1B25_167(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___79A4B717DAF4312A78F1A75A72221DB9690D1B25_167 = value;
	}

	inline static int32_t get_offset_of_U379BEC582299CE5F46902B9C091C19931E26D9323_168() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___79BEC582299CE5F46902B9C091C19931E26D9323_168)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U379BEC582299CE5F46902B9C091C19931E26D9323_168() const { return ___79BEC582299CE5F46902B9C091C19931E26D9323_168; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U379BEC582299CE5F46902B9C091C19931E26D9323_168() { return &___79BEC582299CE5F46902B9C091C19931E26D9323_168; }
	inline void set_U379BEC582299CE5F46902B9C091C19931E26D9323_168(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___79BEC582299CE5F46902B9C091C19931E26D9323_168 = value;
	}

	inline static int32_t get_offset_of_U379F14B68470B4B7AA3BCDCA6758072F2B9320320_169() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___79F14B68470B4B7AA3BCDCA6758072F2B9320320_169)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U379F14B68470B4B7AA3BCDCA6758072F2B9320320_169() const { return ___79F14B68470B4B7AA3BCDCA6758072F2B9320320_169; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U379F14B68470B4B7AA3BCDCA6758072F2B9320320_169() { return &___79F14B68470B4B7AA3BCDCA6758072F2B9320320_169; }
	inline void set_U379F14B68470B4B7AA3BCDCA6758072F2B9320320_169(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___79F14B68470B4B7AA3BCDCA6758072F2B9320320_169 = value;
	}

	inline static int32_t get_offset_of_U379F9F064F81690C950BF4EFFAA5245BA966607C9_170() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___79F9F064F81690C950BF4EFFAA5245BA966607C9_170)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U379F9F064F81690C950BF4EFFAA5245BA966607C9_170() const { return ___79F9F064F81690C950BF4EFFAA5245BA966607C9_170; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U379F9F064F81690C950BF4EFFAA5245BA966607C9_170() { return &___79F9F064F81690C950BF4EFFAA5245BA966607C9_170; }
	inline void set_U379F9F064F81690C950BF4EFFAA5245BA966607C9_170(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___79F9F064F81690C950BF4EFFAA5245BA966607C9_170 = value;
	}

	inline static int32_t get_offset_of_U37BC6404D48DA137303466471F7131DC1C9B13BAA_171() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___7BC6404D48DA137303466471F7131DC1C9B13BAA_171)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U37BC6404D48DA137303466471F7131DC1C9B13BAA_171() const { return ___7BC6404D48DA137303466471F7131DC1C9B13BAA_171; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U37BC6404D48DA137303466471F7131DC1C9B13BAA_171() { return &___7BC6404D48DA137303466471F7131DC1C9B13BAA_171; }
	inline void set_U37BC6404D48DA137303466471F7131DC1C9B13BAA_171(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___7BC6404D48DA137303466471F7131DC1C9B13BAA_171 = value;
	}

	inline static int32_t get_offset_of_U37CF92A83536E2264EF41D8C407D246E41E2DBCD6_172() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___7CF92A83536E2264EF41D8C407D246E41E2DBCD6_172)); }
	inline __StaticArrayInitTypeSizeU3D36_t2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17  get_U37CF92A83536E2264EF41D8C407D246E41E2DBCD6_172() const { return ___7CF92A83536E2264EF41D8C407D246E41E2DBCD6_172; }
	inline __StaticArrayInitTypeSizeU3D36_t2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17 * get_address_of_U37CF92A83536E2264EF41D8C407D246E41E2DBCD6_172() { return &___7CF92A83536E2264EF41D8C407D246E41E2DBCD6_172; }
	inline void set_U37CF92A83536E2264EF41D8C407D246E41E2DBCD6_172(__StaticArrayInitTypeSizeU3D36_t2C4F4E03FA7E8156F8EE8FCB1D4863D6F410CB17  value)
	{
		___7CF92A83536E2264EF41D8C407D246E41E2DBCD6_172 = value;
	}

	inline static int32_t get_offset_of_U37DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_173() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___7DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_173)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U37DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_173() const { return ___7DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_173; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U37DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_173() { return &___7DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_173; }
	inline void set_U37DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_173(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___7DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_173 = value;
	}

	inline static int32_t get_offset_of_U37E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_174() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___7E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_174)); }
	inline __StaticArrayInitTypeSizeU3D192_tCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36  get_U37E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_174() const { return ___7E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_174; }
	inline __StaticArrayInitTypeSizeU3D192_tCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36 * get_address_of_U37E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_174() { return &___7E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_174; }
	inline void set_U37E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_174(__StaticArrayInitTypeSizeU3D192_tCCAD8CDD5E1891A9A3050C3EF220163F0C82FD36  value)
	{
		___7E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_174 = value;
	}

	inline static int32_t get_offset_of_U37ECDF8C5F474CFF2B0970E93C397B504DD793D2C_175() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___7ECDF8C5F474CFF2B0970E93C397B504DD793D2C_175)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U37ECDF8C5F474CFF2B0970E93C397B504DD793D2C_175() const { return ___7ECDF8C5F474CFF2B0970E93C397B504DD793D2C_175; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U37ECDF8C5F474CFF2B0970E93C397B504DD793D2C_175() { return &___7ECDF8C5F474CFF2B0970E93C397B504DD793D2C_175; }
	inline void set_U37ECDF8C5F474CFF2B0970E93C397B504DD793D2C_175(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___7ECDF8C5F474CFF2B0970E93C397B504DD793D2C_175 = value;
	}

	inline static int32_t get_offset_of_U37FDC1CA6ED532684E45DDAE8484274399FCE4C59_176() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___7FDC1CA6ED532684E45DDAE8484274399FCE4C59_176)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U37FDC1CA6ED532684E45DDAE8484274399FCE4C59_176() const { return ___7FDC1CA6ED532684E45DDAE8484274399FCE4C59_176; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U37FDC1CA6ED532684E45DDAE8484274399FCE4C59_176() { return &___7FDC1CA6ED532684E45DDAE8484274399FCE4C59_176; }
	inline void set_U37FDC1CA6ED532684E45DDAE8484274399FCE4C59_176(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___7FDC1CA6ED532684E45DDAE8484274399FCE4C59_176 = value;
	}

	inline static int32_t get_offset_of_U37FF393DF5D10580FA38BF854328B3072169E0A1A_177() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___7FF393DF5D10580FA38BF854328B3072169E0A1A_177)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U37FF393DF5D10580FA38BF854328B3072169E0A1A_177() const { return ___7FF393DF5D10580FA38BF854328B3072169E0A1A_177; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U37FF393DF5D10580FA38BF854328B3072169E0A1A_177() { return &___7FF393DF5D10580FA38BF854328B3072169E0A1A_177; }
	inline void set_U37FF393DF5D10580FA38BF854328B3072169E0A1A_177(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___7FF393DF5D10580FA38BF854328B3072169E0A1A_177 = value;
	}

	inline static int32_t get_offset_of_U3802AD21B222E489DAFEADD5E18868203D9FC2A89_178() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___802AD21B222E489DAFEADD5E18868203D9FC2A89_178)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U3802AD21B222E489DAFEADD5E18868203D9FC2A89_178() const { return ___802AD21B222E489DAFEADD5E18868203D9FC2A89_178; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U3802AD21B222E489DAFEADD5E18868203D9FC2A89_178() { return &___802AD21B222E489DAFEADD5E18868203D9FC2A89_178; }
	inline void set_U3802AD21B222E489DAFEADD5E18868203D9FC2A89_178(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___802AD21B222E489DAFEADD5E18868203D9FC2A89_178 = value;
	}

	inline static int32_t get_offset_of_U3806D200653BD7335D0077F2759E715D8BE8DF9BF_179() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___806D200653BD7335D0077F2759E715D8BE8DF9BF_179)); }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  get_U3806D200653BD7335D0077F2759E715D8BE8DF9BF_179() const { return ___806D200653BD7335D0077F2759E715D8BE8DF9BF_179; }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54 * get_address_of_U3806D200653BD7335D0077F2759E715D8BE8DF9BF_179() { return &___806D200653BD7335D0077F2759E715D8BE8DF9BF_179; }
	inline void set_U3806D200653BD7335D0077F2759E715D8BE8DF9BF_179(__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  value)
	{
		___806D200653BD7335D0077F2759E715D8BE8DF9BF_179 = value;
	}

	inline static int32_t get_offset_of_U3808AD19CA818077F71100880E7D7AA21147E987E_180() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___808AD19CA818077F71100880E7D7AA21147E987E_180)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U3808AD19CA818077F71100880E7D7AA21147E987E_180() const { return ___808AD19CA818077F71100880E7D7AA21147E987E_180; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U3808AD19CA818077F71100880E7D7AA21147E987E_180() { return &___808AD19CA818077F71100880E7D7AA21147E987E_180; }
	inline void set_U3808AD19CA818077F71100880E7D7AA21147E987E_180(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___808AD19CA818077F71100880E7D7AA21147E987E_180 = value;
	}

	inline static int32_t get_offset_of_U380DD6A282D563836522C10A78AAA17F0B865EB5A_181() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___80DD6A282D563836522C10A78AAA17F0B865EB5A_181)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U380DD6A282D563836522C10A78AAA17F0B865EB5A_181() const { return ___80DD6A282D563836522C10A78AAA17F0B865EB5A_181; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U380DD6A282D563836522C10A78AAA17F0B865EB5A_181() { return &___80DD6A282D563836522C10A78AAA17F0B865EB5A_181; }
	inline void set_U380DD6A282D563836522C10A78AAA17F0B865EB5A_181(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___80DD6A282D563836522C10A78AAA17F0B865EB5A_181 = value;
	}

	inline static int32_t get_offset_of_U380E21576779C61DA5B185D15A732CF7A8BD6F88C_182() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___80E21576779C61DA5B185D15A732CF7A8BD6F88C_182)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U380E21576779C61DA5B185D15A732CF7A8BD6F88C_182() const { return ___80E21576779C61DA5B185D15A732CF7A8BD6F88C_182; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U380E21576779C61DA5B185D15A732CF7A8BD6F88C_182() { return &___80E21576779C61DA5B185D15A732CF7A8BD6F88C_182; }
	inline void set_U380E21576779C61DA5B185D15A732CF7A8BD6F88C_182(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___80E21576779C61DA5B185D15A732CF7A8BD6F88C_182 = value;
	}

	inline static int32_t get_offset_of_U380E9217618F8C2C7FED61E4E7653CD0E66C74872_183() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___80E9217618F8C2C7FED61E4E7653CD0E66C74872_183)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U380E9217618F8C2C7FED61E4E7653CD0E66C74872_183() const { return ___80E9217618F8C2C7FED61E4E7653CD0E66C74872_183; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U380E9217618F8C2C7FED61E4E7653CD0E66C74872_183() { return &___80E9217618F8C2C7FED61E4E7653CD0E66C74872_183; }
	inline void set_U380E9217618F8C2C7FED61E4E7653CD0E66C74872_183(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___80E9217618F8C2C7FED61E4E7653CD0E66C74872_183 = value;
	}

	inline static int32_t get_offset_of_U380F763BA90E27F755F2DEFA7FE720AD9371D6E6B_184() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___80F763BA90E27F755F2DEFA7FE720AD9371D6E6B_184)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U380F763BA90E27F755F2DEFA7FE720AD9371D6E6B_184() const { return ___80F763BA90E27F755F2DEFA7FE720AD9371D6E6B_184; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U380F763BA90E27F755F2DEFA7FE720AD9371D6E6B_184() { return &___80F763BA90E27F755F2DEFA7FE720AD9371D6E6B_184; }
	inline void set_U380F763BA90E27F755F2DEFA7FE720AD9371D6E6B_184(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___80F763BA90E27F755F2DEFA7FE720AD9371D6E6B_184 = value;
	}

	inline static int32_t get_offset_of_U3836DA48724BC66F3257E70FFF165ACCE037666A6_185() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___836DA48724BC66F3257E70FFF165ACCE037666A6_185)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_U3836DA48724BC66F3257E70FFF165ACCE037666A6_185() const { return ___836DA48724BC66F3257E70FFF165ACCE037666A6_185; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_U3836DA48724BC66F3257E70FFF165ACCE037666A6_185() { return &___836DA48724BC66F3257E70FFF165ACCE037666A6_185; }
	inline void set_U3836DA48724BC66F3257E70FFF165ACCE037666A6_185(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___836DA48724BC66F3257E70FFF165ACCE037666A6_185 = value;
	}

	inline static int32_t get_offset_of_U38523C737ED49D8A2E9BB85218190E4E83B902E28_186() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___8523C737ED49D8A2E9BB85218190E4E83B902E28_186)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U38523C737ED49D8A2E9BB85218190E4E83B902E28_186() const { return ___8523C737ED49D8A2E9BB85218190E4E83B902E28_186; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U38523C737ED49D8A2E9BB85218190E4E83B902E28_186() { return &___8523C737ED49D8A2E9BB85218190E4E83B902E28_186; }
	inline void set_U38523C737ED49D8A2E9BB85218190E4E83B902E28_186(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___8523C737ED49D8A2E9BB85218190E4E83B902E28_186 = value;
	}

	inline static int32_t get_offset_of_U385666B25692AB814E91F685384EEA0389F147E70_187() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___85666B25692AB814E91F685384EEA0389F147E70_187)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U385666B25692AB814E91F685384EEA0389F147E70_187() const { return ___85666B25692AB814E91F685384EEA0389F147E70_187; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U385666B25692AB814E91F685384EEA0389F147E70_187() { return &___85666B25692AB814E91F685384EEA0389F147E70_187; }
	inline void set_U385666B25692AB814E91F685384EEA0389F147E70_187(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___85666B25692AB814E91F685384EEA0389F147E70_187 = value;
	}

	inline static int32_t get_offset_of_U3866652192B68681CC538B2CB21C5979544DF35AB_188() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___866652192B68681CC538B2CB21C5979544DF35AB_188)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U3866652192B68681CC538B2CB21C5979544DF35AB_188() const { return ___866652192B68681CC538B2CB21C5979544DF35AB_188; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U3866652192B68681CC538B2CB21C5979544DF35AB_188() { return &___866652192B68681CC538B2CB21C5979544DF35AB_188; }
	inline void set_U3866652192B68681CC538B2CB21C5979544DF35AB_188(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___866652192B68681CC538B2CB21C5979544DF35AB_188 = value;
	}

	inline static int32_t get_offset_of_U38683FBE90F936E0F2A1080E053EBA3EE9F44A02B_189() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___8683FBE90F936E0F2A1080E053EBA3EE9F44A02B_189)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U38683FBE90F936E0F2A1080E053EBA3EE9F44A02B_189() const { return ___8683FBE90F936E0F2A1080E053EBA3EE9F44A02B_189; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U38683FBE90F936E0F2A1080E053EBA3EE9F44A02B_189() { return &___8683FBE90F936E0F2A1080E053EBA3EE9F44A02B_189; }
	inline void set_U38683FBE90F936E0F2A1080E053EBA3EE9F44A02B_189(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___8683FBE90F936E0F2A1080E053EBA3EE9F44A02B_189 = value;
	}

	inline static int32_t get_offset_of_U3868C8972C4058443A2D131C22083401956DF81C7_190() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___868C8972C4058443A2D131C22083401956DF81C7_190)); }
	inline __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  get_U3868C8972C4058443A2D131C22083401956DF81C7_190() const { return ___868C8972C4058443A2D131C22083401956DF81C7_190; }
	inline __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F * get_address_of_U3868C8972C4058443A2D131C22083401956DF81C7_190() { return &___868C8972C4058443A2D131C22083401956DF81C7_190; }
	inline void set_U3868C8972C4058443A2D131C22083401956DF81C7_190(__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  value)
	{
		___868C8972C4058443A2D131C22083401956DF81C7_190 = value;
	}

	inline static int32_t get_offset_of_U386EA004891DE06B357581B1885C9C0EFD73DE2E9_191() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___86EA004891DE06B357581B1885C9C0EFD73DE2E9_191)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U386EA004891DE06B357581B1885C9C0EFD73DE2E9_191() const { return ___86EA004891DE06B357581B1885C9C0EFD73DE2E9_191; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U386EA004891DE06B357581B1885C9C0EFD73DE2E9_191() { return &___86EA004891DE06B357581B1885C9C0EFD73DE2E9_191; }
	inline void set_U386EA004891DE06B357581B1885C9C0EFD73DE2E9_191(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___86EA004891DE06B357581B1885C9C0EFD73DE2E9_191 = value;
	}

	inline static int32_t get_offset_of_U3881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_192() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_192)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U3881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_192() const { return ___881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_192; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U3881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_192() { return &___881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_192; }
	inline void set_U3881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_192(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_192 = value;
	}

	inline static int32_t get_offset_of_U388B6AF1079C72775E54D0AC3BEEDCCC6506B6269_193() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___88B6AF1079C72775E54D0AC3BEEDCCC6506B6269_193)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U388B6AF1079C72775E54D0AC3BEEDCCC6506B6269_193() const { return ___88B6AF1079C72775E54D0AC3BEEDCCC6506B6269_193; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U388B6AF1079C72775E54D0AC3BEEDCCC6506B6269_193() { return &___88B6AF1079C72775E54D0AC3BEEDCCC6506B6269_193; }
	inline void set_U388B6AF1079C72775E54D0AC3BEEDCCC6506B6269_193(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___88B6AF1079C72775E54D0AC3BEEDCCC6506B6269_193 = value;
	}

	inline static int32_t get_offset_of_U388CFCB31ED9AFFB90483973D02D5DEE8A82A4892_194() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___88CFCB31ED9AFFB90483973D02D5DEE8A82A4892_194)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U388CFCB31ED9AFFB90483973D02D5DEE8A82A4892_194() const { return ___88CFCB31ED9AFFB90483973D02D5DEE8A82A4892_194; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U388CFCB31ED9AFFB90483973D02D5DEE8A82A4892_194() { return &___88CFCB31ED9AFFB90483973D02D5DEE8A82A4892_194; }
	inline void set_U388CFCB31ED9AFFB90483973D02D5DEE8A82A4892_194(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___88CFCB31ED9AFFB90483973D02D5DEE8A82A4892_194 = value;
	}

	inline static int32_t get_offset_of_U38909772CC771C2FF1D61415555452F2ED4ADA536_195() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___8909772CC771C2FF1D61415555452F2ED4ADA536_195)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U38909772CC771C2FF1D61415555452F2ED4ADA536_195() const { return ___8909772CC771C2FF1D61415555452F2ED4ADA536_195; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U38909772CC771C2FF1D61415555452F2ED4ADA536_195() { return &___8909772CC771C2FF1D61415555452F2ED4ADA536_195; }
	inline void set_U38909772CC771C2FF1D61415555452F2ED4ADA536_195(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___8909772CC771C2FF1D61415555452F2ED4ADA536_195 = value;
	}

	inline static int32_t get_offset_of_U38971045D9FA23669DC4A204D478DC63888A9B420_196() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___8971045D9FA23669DC4A204D478DC63888A9B420_196)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U38971045D9FA23669DC4A204D478DC63888A9B420_196() const { return ___8971045D9FA23669DC4A204D478DC63888A9B420_196; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U38971045D9FA23669DC4A204D478DC63888A9B420_196() { return &___8971045D9FA23669DC4A204D478DC63888A9B420_196; }
	inline void set_U38971045D9FA23669DC4A204D478DC63888A9B420_196(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___8971045D9FA23669DC4A204D478DC63888A9B420_196 = value;
	}

	inline static int32_t get_offset_of_U38AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_197() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___8AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_197)); }
	inline __StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016  get_U38AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_197() const { return ___8AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_197; }
	inline __StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016 * get_address_of_U38AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_197() { return &___8AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_197; }
	inline void set_U38AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_197(__StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016  value)
	{
		___8AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_197 = value;
	}

	inline static int32_t get_offset_of_U38C301DDB45068B145D6C11F79591061EEE2AABA8_198() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___8C301DDB45068B145D6C11F79591061EEE2AABA8_198)); }
	inline __StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016  get_U38C301DDB45068B145D6C11F79591061EEE2AABA8_198() const { return ___8C301DDB45068B145D6C11F79591061EEE2AABA8_198; }
	inline __StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016 * get_address_of_U38C301DDB45068B145D6C11F79591061EEE2AABA8_198() { return &___8C301DDB45068B145D6C11F79591061EEE2AABA8_198; }
	inline void set_U38C301DDB45068B145D6C11F79591061EEE2AABA8_198(__StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016  value)
	{
		___8C301DDB45068B145D6C11F79591061EEE2AABA8_198 = value;
	}

	inline static int32_t get_offset_of_U38D1369A0D8832C941BD6D09849525D65D807A1DD_199() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___8D1369A0D8832C941BD6D09849525D65D807A1DD_199)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U38D1369A0D8832C941BD6D09849525D65D807A1DD_199() const { return ___8D1369A0D8832C941BD6D09849525D65D807A1DD_199; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U38D1369A0D8832C941BD6D09849525D65D807A1DD_199() { return &___8D1369A0D8832C941BD6D09849525D65D807A1DD_199; }
	inline void set_U38D1369A0D8832C941BD6D09849525D65D807A1DD_199(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___8D1369A0D8832C941BD6D09849525D65D807A1DD_199 = value;
	}

	inline static int32_t get_offset_of_U38DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_200() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___8DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_200)); }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  get_U38DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_200() const { return ___8DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_200; }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 * get_address_of_U38DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_200() { return &___8DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_200; }
	inline void set_U38DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_200(__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  value)
	{
		___8DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_200 = value;
	}

	inline static int32_t get_offset_of_U38DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_201() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___8DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_201)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U38DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_201() const { return ___8DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_201; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U38DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_201() { return &___8DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_201; }
	inline void set_U38DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_201(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___8DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_201 = value;
	}

	inline static int32_t get_offset_of_U38EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_202() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___8EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_202)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U38EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_202() const { return ___8EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_202; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U38EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_202() { return &___8EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_202; }
	inline void set_U38EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_202(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___8EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_202 = value;
	}

	inline static int32_t get_offset_of_U38EED33571FFED1BBCA4954A909498A1D3316EDC8_203() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___8EED33571FFED1BBCA4954A909498A1D3316EDC8_203)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U38EED33571FFED1BBCA4954A909498A1D3316EDC8_203() const { return ___8EED33571FFED1BBCA4954A909498A1D3316EDC8_203; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U38EED33571FFED1BBCA4954A909498A1D3316EDC8_203() { return &___8EED33571FFED1BBCA4954A909498A1D3316EDC8_203; }
	inline void set_U38EED33571FFED1BBCA4954A909498A1D3316EDC8_203(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___8EED33571FFED1BBCA4954A909498A1D3316EDC8_203 = value;
	}

	inline static int32_t get_offset_of_U38EEE72A955DF384253C5C458726A8A7299A05164_204() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___8EEE72A955DF384253C5C458726A8A7299A05164_204)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U38EEE72A955DF384253C5C458726A8A7299A05164_204() const { return ___8EEE72A955DF384253C5C458726A8A7299A05164_204; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U38EEE72A955DF384253C5C458726A8A7299A05164_204() { return &___8EEE72A955DF384253C5C458726A8A7299A05164_204; }
	inline void set_U38EEE72A955DF384253C5C458726A8A7299A05164_204(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___8EEE72A955DF384253C5C458726A8A7299A05164_204 = value;
	}

	inline static int32_t get_offset_of_U38FD55399F538FAF9608E938EAB08D4BC88D0E131_205() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___8FD55399F538FAF9608E938EAB08D4BC88D0E131_205)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U38FD55399F538FAF9608E938EAB08D4BC88D0E131_205() const { return ___8FD55399F538FAF9608E938EAB08D4BC88D0E131_205; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U38FD55399F538FAF9608E938EAB08D4BC88D0E131_205() { return &___8FD55399F538FAF9608E938EAB08D4BC88D0E131_205; }
	inline void set_U38FD55399F538FAF9608E938EAB08D4BC88D0E131_205(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___8FD55399F538FAF9608E938EAB08D4BC88D0E131_205 = value;
	}

	inline static int32_t get_offset_of_U39032D4E1ED9EBAC2417A10654FF0C658D2CC9825_206() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___9032D4E1ED9EBAC2417A10654FF0C658D2CC9825_206)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U39032D4E1ED9EBAC2417A10654FF0C658D2CC9825_206() const { return ___9032D4E1ED9EBAC2417A10654FF0C658D2CC9825_206; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U39032D4E1ED9EBAC2417A10654FF0C658D2CC9825_206() { return &___9032D4E1ED9EBAC2417A10654FF0C658D2CC9825_206; }
	inline void set_U39032D4E1ED9EBAC2417A10654FF0C658D2CC9825_206(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___9032D4E1ED9EBAC2417A10654FF0C658D2CC9825_206 = value;
	}

	inline static int32_t get_offset_of_U3910681E5316200ECA5484DF31EEAE1CF5E2F3439_207() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___910681E5316200ECA5484DF31EEAE1CF5E2F3439_207)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U3910681E5316200ECA5484DF31EEAE1CF5E2F3439_207() const { return ___910681E5316200ECA5484DF31EEAE1CF5E2F3439_207; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U3910681E5316200ECA5484DF31EEAE1CF5E2F3439_207() { return &___910681E5316200ECA5484DF31EEAE1CF5E2F3439_207; }
	inline void set_U3910681E5316200ECA5484DF31EEAE1CF5E2F3439_207(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___910681E5316200ECA5484DF31EEAE1CF5E2F3439_207 = value;
	}

	inline static int32_t get_offset_of_U391EA1528B7E2B59DB78E5E7A749312DD20AA43F1_208() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___91EA1528B7E2B59DB78E5E7A749312DD20AA43F1_208)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U391EA1528B7E2B59DB78E5E7A749312DD20AA43F1_208() const { return ___91EA1528B7E2B59DB78E5E7A749312DD20AA43F1_208; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U391EA1528B7E2B59DB78E5E7A749312DD20AA43F1_208() { return &___91EA1528B7E2B59DB78E5E7A749312DD20AA43F1_208; }
	inline void set_U391EA1528B7E2B59DB78E5E7A749312DD20AA43F1_208(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___91EA1528B7E2B59DB78E5E7A749312DD20AA43F1_208 = value;
	}

	inline static int32_t get_offset_of_U395C66836A6DB5A262B674463792A9A67241AEC25_209() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___95C66836A6DB5A262B674463792A9A67241AEC25_209)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U395C66836A6DB5A262B674463792A9A67241AEC25_209() const { return ___95C66836A6DB5A262B674463792A9A67241AEC25_209; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U395C66836A6DB5A262B674463792A9A67241AEC25_209() { return &___95C66836A6DB5A262B674463792A9A67241AEC25_209; }
	inline void set_U395C66836A6DB5A262B674463792A9A67241AEC25_209(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___95C66836A6DB5A262B674463792A9A67241AEC25_209 = value;
	}

	inline static int32_t get_offset_of_U395CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_210() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___95CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_210)); }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  get_U395CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_210() const { return ___95CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_210; }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7 * get_address_of_U395CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_210() { return &___95CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_210; }
	inline void set_U395CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_210(__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  value)
	{
		___95CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_210 = value;
	}

	inline static int32_t get_offset_of_U39623BB2649942957F680075A5AD3A16DE427F7A2_211() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___9623BB2649942957F680075A5AD3A16DE427F7A2_211)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U39623BB2649942957F680075A5AD3A16DE427F7A2_211() const { return ___9623BB2649942957F680075A5AD3A16DE427F7A2_211; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U39623BB2649942957F680075A5AD3A16DE427F7A2_211() { return &___9623BB2649942957F680075A5AD3A16DE427F7A2_211; }
	inline void set_U39623BB2649942957F680075A5AD3A16DE427F7A2_211(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___9623BB2649942957F680075A5AD3A16DE427F7A2_211 = value;
	}

	inline static int32_t get_offset_of_U397BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_212() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___97BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_212)); }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  get_U397BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_212() const { return ___97BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_212; }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54 * get_address_of_U397BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_212() { return &___97BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_212; }
	inline void set_U397BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_212(__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  value)
	{
		___97BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_212 = value;
	}

	inline static int32_t get_offset_of_U39848EC8C1177B700CDB98F1A7C07F2A9C9EDE444_213() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___9848EC8C1177B700CDB98F1A7C07F2A9C9EDE444_213)); }
	inline __StaticArrayInitTypeSizeU3D384_t6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3  get_U39848EC8C1177B700CDB98F1A7C07F2A9C9EDE444_213() const { return ___9848EC8C1177B700CDB98F1A7C07F2A9C9EDE444_213; }
	inline __StaticArrayInitTypeSizeU3D384_t6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3 * get_address_of_U39848EC8C1177B700CDB98F1A7C07F2A9C9EDE444_213() { return &___9848EC8C1177B700CDB98F1A7C07F2A9C9EDE444_213; }
	inline void set_U39848EC8C1177B700CDB98F1A7C07F2A9C9EDE444_213(__StaticArrayInitTypeSizeU3D384_t6DA4BE13521A3C46C2ABBC01492CDFAAF39E74D3  value)
	{
		___9848EC8C1177B700CDB98F1A7C07F2A9C9EDE444_213 = value;
	}

	inline static int32_t get_offset_of_U39858ACE49B9B39354CFCB05E54A9790070E48D39_214() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___9858ACE49B9B39354CFCB05E54A9790070E48D39_214)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U39858ACE49B9B39354CFCB05E54A9790070E48D39_214() const { return ___9858ACE49B9B39354CFCB05E54A9790070E48D39_214; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U39858ACE49B9B39354CFCB05E54A9790070E48D39_214() { return &___9858ACE49B9B39354CFCB05E54A9790070E48D39_214; }
	inline void set_U39858ACE49B9B39354CFCB05E54A9790070E48D39_214(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___9858ACE49B9B39354CFCB05E54A9790070E48D39_214 = value;
	}

	inline static int32_t get_offset_of_U399073F844984F28C1A9FC1E1490E5631272E26A0_215() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___99073F844984F28C1A9FC1E1490E5631272E26A0_215)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U399073F844984F28C1A9FC1E1490E5631272E26A0_215() const { return ___99073F844984F28C1A9FC1E1490E5631272E26A0_215; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U399073F844984F28C1A9FC1E1490E5631272E26A0_215() { return &___99073F844984F28C1A9FC1E1490E5631272E26A0_215; }
	inline void set_U399073F844984F28C1A9FC1E1490E5631272E26A0_215(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___99073F844984F28C1A9FC1E1490E5631272E26A0_215 = value;
	}

	inline static int32_t get_offset_of_U3993DC5FF70241D5149EB5EAF1CDD957CA0B23454_216() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___993DC5FF70241D5149EB5EAF1CDD957CA0B23454_216)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U3993DC5FF70241D5149EB5EAF1CDD957CA0B23454_216() const { return ___993DC5FF70241D5149EB5EAF1CDD957CA0B23454_216; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U3993DC5FF70241D5149EB5EAF1CDD957CA0B23454_216() { return &___993DC5FF70241D5149EB5EAF1CDD957CA0B23454_216; }
	inline void set_U3993DC5FF70241D5149EB5EAF1CDD957CA0B23454_216(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___993DC5FF70241D5149EB5EAF1CDD957CA0B23454_216 = value;
	}

	inline static int32_t get_offset_of_U3994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_217() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_217)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U3994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_217() const { return ___994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_217; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U3994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_217() { return &___994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_217; }
	inline void set_U3994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_217(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_217 = value;
	}

	inline static int32_t get_offset_of_U399CC0827BC16027263E3737FF59411A5D4BFDE31_218() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___99CC0827BC16027263E3737FF59411A5D4BFDE31_218)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_U399CC0827BC16027263E3737FF59411A5D4BFDE31_218() const { return ___99CC0827BC16027263E3737FF59411A5D4BFDE31_218; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_U399CC0827BC16027263E3737FF59411A5D4BFDE31_218() { return &___99CC0827BC16027263E3737FF59411A5D4BFDE31_218; }
	inline void set_U399CC0827BC16027263E3737FF59411A5D4BFDE31_218(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___99CC0827BC16027263E3737FF59411A5D4BFDE31_218 = value;
	}

	inline static int32_t get_offset_of_U399D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_219() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___99D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_219)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_U399D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_219() const { return ___99D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_219; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_U399D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_219() { return &___99D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_219; }
	inline void set_U399D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_219(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___99D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_219 = value;
	}

	inline static int32_t get_offset_of_U39AA52F510245480023EB2EF8CF779F6008E93D7A_220() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___9AA52F510245480023EB2EF8CF779F6008E93D7A_220)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U39AA52F510245480023EB2EF8CF779F6008E93D7A_220() const { return ___9AA52F510245480023EB2EF8CF779F6008E93D7A_220; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U39AA52F510245480023EB2EF8CF779F6008E93D7A_220() { return &___9AA52F510245480023EB2EF8CF779F6008E93D7A_220; }
	inline void set_U39AA52F510245480023EB2EF8CF779F6008E93D7A_220(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___9AA52F510245480023EB2EF8CF779F6008E93D7A_220 = value;
	}

	inline static int32_t get_offset_of_U39ACE167D7580B82857BBF66A5A9BC1E2D139353C_221() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___9ACE167D7580B82857BBF66A5A9BC1E2D139353C_221)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U39ACE167D7580B82857BBF66A5A9BC1E2D139353C_221() const { return ___9ACE167D7580B82857BBF66A5A9BC1E2D139353C_221; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U39ACE167D7580B82857BBF66A5A9BC1E2D139353C_221() { return &___9ACE167D7580B82857BBF66A5A9BC1E2D139353C_221; }
	inline void set_U39ACE167D7580B82857BBF66A5A9BC1E2D139353C_221(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___9ACE167D7580B82857BBF66A5A9BC1E2D139353C_221 = value;
	}

	inline static int32_t get_offset_of_U39B3E34C734351ED3DE1984119EAB9572C48FA15E_222() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___9B3E34C734351ED3DE1984119EAB9572C48FA15E_222)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U39B3E34C734351ED3DE1984119EAB9572C48FA15E_222() const { return ___9B3E34C734351ED3DE1984119EAB9572C48FA15E_222; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U39B3E34C734351ED3DE1984119EAB9572C48FA15E_222() { return &___9B3E34C734351ED3DE1984119EAB9572C48FA15E_222; }
	inline void set_U39B3E34C734351ED3DE1984119EAB9572C48FA15E_222(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___9B3E34C734351ED3DE1984119EAB9572C48FA15E_222 = value;
	}

	inline static int32_t get_offset_of_U39BBAB86ACB95EF8C028708733458CDBAC4715197_223() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___9BBAB86ACB95EF8C028708733458CDBAC4715197_223)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U39BBAB86ACB95EF8C028708733458CDBAC4715197_223() const { return ___9BBAB86ACB95EF8C028708733458CDBAC4715197_223; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U39BBAB86ACB95EF8C028708733458CDBAC4715197_223() { return &___9BBAB86ACB95EF8C028708733458CDBAC4715197_223; }
	inline void set_U39BBAB86ACB95EF8C028708733458CDBAC4715197_223(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___9BBAB86ACB95EF8C028708733458CDBAC4715197_223 = value;
	}

	inline static int32_t get_offset_of_U39BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_224() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___9BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_224)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_U39BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_224() const { return ___9BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_224; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_U39BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_224() { return &___9BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_224; }
	inline void set_U39BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_224(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___9BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_224 = value;
	}

	inline static int32_t get_offset_of_U39C89FB9160DB0A983D5BE5D510BF9E801F0136A6_225() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___9C89FB9160DB0A983D5BE5D510BF9E801F0136A6_225)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U39C89FB9160DB0A983D5BE5D510BF9E801F0136A6_225() const { return ___9C89FB9160DB0A983D5BE5D510BF9E801F0136A6_225; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U39C89FB9160DB0A983D5BE5D510BF9E801F0136A6_225() { return &___9C89FB9160DB0A983D5BE5D510BF9E801F0136A6_225; }
	inline void set_U39C89FB9160DB0A983D5BE5D510BF9E801F0136A6_225(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___9C89FB9160DB0A983D5BE5D510BF9E801F0136A6_225 = value;
	}

	inline static int32_t get_offset_of_U39D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_226() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___9D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_226)); }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  get_U39D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_226() const { return ___9D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_226; }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 * get_address_of_U39D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_226() { return &___9D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_226; }
	inline void set_U39D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_226(__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  value)
	{
		___9D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_226 = value;
	}

	inline static int32_t get_offset_of_U39E548860392B66258417232F41EF03D48EBDD686_227() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___9E548860392B66258417232F41EF03D48EBDD686_227)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_U39E548860392B66258417232F41EF03D48EBDD686_227() const { return ___9E548860392B66258417232F41EF03D48EBDD686_227; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_U39E548860392B66258417232F41EF03D48EBDD686_227() { return &___9E548860392B66258417232F41EF03D48EBDD686_227; }
	inline void set_U39E548860392B66258417232F41EF03D48EBDD686_227(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___9E548860392B66258417232F41EF03D48EBDD686_227 = value;
	}

	inline static int32_t get_offset_of_U39F6BDC14CCFDFFBF8F1FE3539741F79424F33E16_228() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___9F6BDC14CCFDFFBF8F1FE3539741F79424F33E16_228)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_U39F6BDC14CCFDFFBF8F1FE3539741F79424F33E16_228() const { return ___9F6BDC14CCFDFFBF8F1FE3539741F79424F33E16_228; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_U39F6BDC14CCFDFFBF8F1FE3539741F79424F33E16_228() { return &___9F6BDC14CCFDFFBF8F1FE3539741F79424F33E16_228; }
	inline void set_U39F6BDC14CCFDFFBF8F1FE3539741F79424F33E16_228(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___9F6BDC14CCFDFFBF8F1FE3539741F79424F33E16_228 = value;
	}

	inline static int32_t get_offset_of_U39F8CAB0A98B00335FDDBCC16C748FEA524A25435_229() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___9F8CAB0A98B00335FDDBCC16C748FEA524A25435_229)); }
	inline __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  get_U39F8CAB0A98B00335FDDBCC16C748FEA524A25435_229() const { return ___9F8CAB0A98B00335FDDBCC16C748FEA524A25435_229; }
	inline __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F * get_address_of_U39F8CAB0A98B00335FDDBCC16C748FEA524A25435_229() { return &___9F8CAB0A98B00335FDDBCC16C748FEA524A25435_229; }
	inline void set_U39F8CAB0A98B00335FDDBCC16C748FEA524A25435_229(__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  value)
	{
		___9F8CAB0A98B00335FDDBCC16C748FEA524A25435_229 = value;
	}

	inline static int32_t get_offset_of_A0324640CCF369A6794A9A8BB204ED22651ECBCC_230() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___A0324640CCF369A6794A9A8BB204ED22651ECBCC_230)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_A0324640CCF369A6794A9A8BB204ED22651ECBCC_230() const { return ___A0324640CCF369A6794A9A8BB204ED22651ECBCC_230; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_A0324640CCF369A6794A9A8BB204ED22651ECBCC_230() { return &___A0324640CCF369A6794A9A8BB204ED22651ECBCC_230; }
	inline void set_A0324640CCF369A6794A9A8BB204ED22651ECBCC_230(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___A0324640CCF369A6794A9A8BB204ED22651ECBCC_230 = value;
	}

	inline static int32_t get_offset_of_A2978A4F76015511E3741ABF9B58117B348B19C2_231() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___A2978A4F76015511E3741ABF9B58117B348B19C2_231)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_A2978A4F76015511E3741ABF9B58117B348B19C2_231() const { return ___A2978A4F76015511E3741ABF9B58117B348B19C2_231; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_A2978A4F76015511E3741ABF9B58117B348B19C2_231() { return &___A2978A4F76015511E3741ABF9B58117B348B19C2_231; }
	inline void set_A2978A4F76015511E3741ABF9B58117B348B19C2_231(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___A2978A4F76015511E3741ABF9B58117B348B19C2_231 = value;
	}

	inline static int32_t get_offset_of_A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_232() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_232)); }
	inline __StaticArrayInitTypeSizeU3D10_tB641FD73E10817DC3A5A0B6CDD83506C3C2B53C7  get_A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_232() const { return ___A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_232; }
	inline __StaticArrayInitTypeSizeU3D10_tB641FD73E10817DC3A5A0B6CDD83506C3C2B53C7 * get_address_of_A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_232() { return &___A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_232; }
	inline void set_A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_232(__StaticArrayInitTypeSizeU3D10_tB641FD73E10817DC3A5A0B6CDD83506C3C2B53C7  value)
	{
		___A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_232 = value;
	}

	inline static int32_t get_offset_of_A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_233() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_233)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_233() const { return ___A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_233; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_233() { return &___A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_233; }
	inline void set_A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_233(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_233 = value;
	}

	inline static int32_t get_offset_of_A3F55D69F9A97411DB4065F0FA414D912A181B69_234() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___A3F55D69F9A97411DB4065F0FA414D912A181B69_234)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_A3F55D69F9A97411DB4065F0FA414D912A181B69_234() const { return ___A3F55D69F9A97411DB4065F0FA414D912A181B69_234; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_A3F55D69F9A97411DB4065F0FA414D912A181B69_234() { return &___A3F55D69F9A97411DB4065F0FA414D912A181B69_234; }
	inline void set_A3F55D69F9A97411DB4065F0FA414D912A181B69_234(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___A3F55D69F9A97411DB4065F0FA414D912A181B69_234 = value;
	}

	inline static int32_t get_offset_of_A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_235() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_235)); }
	inline __StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260  get_A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_235() const { return ___A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_235; }
	inline __StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260 * get_address_of_A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_235() { return &___A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_235; }
	inline void set_A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_235(__StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260  value)
	{
		___A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_235 = value;
	}

	inline static int32_t get_offset_of_A7977502040EE08933752F579060577814723508_236() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___A7977502040EE08933752F579060577814723508_236)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_A7977502040EE08933752F579060577814723508_236() const { return ___A7977502040EE08933752F579060577814723508_236; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_A7977502040EE08933752F579060577814723508_236() { return &___A7977502040EE08933752F579060577814723508_236; }
	inline void set_A7977502040EE08933752F579060577814723508_236(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___A7977502040EE08933752F579060577814723508_236 = value;
	}

	inline static int32_t get_offset_of_A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_237() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_237)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_237() const { return ___A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_237; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_237() { return &___A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_237; }
	inline void set_A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_237(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_237 = value;
	}

	inline static int32_t get_offset_of_A98B8F0C923F5DFC97332FCC80F65C96F9F27181_238() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___A98B8F0C923F5DFC97332FCC80F65C96F9F27181_238)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_A98B8F0C923F5DFC97332FCC80F65C96F9F27181_238() const { return ___A98B8F0C923F5DFC97332FCC80F65C96F9F27181_238; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_A98B8F0C923F5DFC97332FCC80F65C96F9F27181_238() { return &___A98B8F0C923F5DFC97332FCC80F65C96F9F27181_238; }
	inline void set_A98B8F0C923F5DFC97332FCC80F65C96F9F27181_238(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___A98B8F0C923F5DFC97332FCC80F65C96F9F27181_238 = value;
	}

	inline static int32_t get_offset_of_AA607BA0FA20D3E80F456E68768C517D17D073D5_239() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___AA607BA0FA20D3E80F456E68768C517D17D073D5_239)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_AA607BA0FA20D3E80F456E68768C517D17D073D5_239() const { return ___AA607BA0FA20D3E80F456E68768C517D17D073D5_239; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_AA607BA0FA20D3E80F456E68768C517D17D073D5_239() { return &___AA607BA0FA20D3E80F456E68768C517D17D073D5_239; }
	inline void set_AA607BA0FA20D3E80F456E68768C517D17D073D5_239(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___AA607BA0FA20D3E80F456E68768C517D17D073D5_239 = value;
	}

	inline static int32_t get_offset_of_AB44F69765F4A8ECD5D47354EA625039C43CC4D6_240() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___AB44F69765F4A8ECD5D47354EA625039C43CC4D6_240)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_AB44F69765F4A8ECD5D47354EA625039C43CC4D6_240() const { return ___AB44F69765F4A8ECD5D47354EA625039C43CC4D6_240; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_AB44F69765F4A8ECD5D47354EA625039C43CC4D6_240() { return &___AB44F69765F4A8ECD5D47354EA625039C43CC4D6_240; }
	inline void set_AB44F69765F4A8ECD5D47354EA625039C43CC4D6_240(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___AB44F69765F4A8ECD5D47354EA625039C43CC4D6_240 = value;
	}

	inline static int32_t get_offset_of_AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_241() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_241)); }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  get_AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_241() const { return ___AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_241; }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54 * get_address_of_AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_241() { return &___AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_241; }
	inline void set_AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_241(__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  value)
	{
		___AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_241 = value;
	}

	inline static int32_t get_offset_of_AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_242() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_242)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_242() const { return ___AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_242; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_242() { return &___AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_242; }
	inline void set_AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_242(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_242 = value;
	}

	inline static int32_t get_offset_of_AECEE3733C380177F0A39ACF3AF99BFE360C156F_243() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___AECEE3733C380177F0A39ACF3AF99BFE360C156F_243)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_AECEE3733C380177F0A39ACF3AF99BFE360C156F_243() const { return ___AECEE3733C380177F0A39ACF3AF99BFE360C156F_243; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_AECEE3733C380177F0A39ACF3AF99BFE360C156F_243() { return &___AECEE3733C380177F0A39ACF3AF99BFE360C156F_243; }
	inline void set_AECEE3733C380177F0A39ACF3AF99BFE360C156F_243(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___AECEE3733C380177F0A39ACF3AF99BFE360C156F_243 = value;
	}

	inline static int32_t get_offset_of_AEE03822C3725428610E6E7DAAF7970DE35F7675_244() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___AEE03822C3725428610E6E7DAAF7970DE35F7675_244)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_AEE03822C3725428610E6E7DAAF7970DE35F7675_244() const { return ___AEE03822C3725428610E6E7DAAF7970DE35F7675_244; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_AEE03822C3725428610E6E7DAAF7970DE35F7675_244() { return &___AEE03822C3725428610E6E7DAAF7970DE35F7675_244; }
	inline void set_AEE03822C3725428610E6E7DAAF7970DE35F7675_244(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___AEE03822C3725428610E6E7DAAF7970DE35F7675_244 = value;
	}

	inline static int32_t get_offset_of_AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_245() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_245)); }
	inline int64_t get_AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_245() const { return ___AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_245; }
	inline int64_t* get_address_of_AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_245() { return &___AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_245; }
	inline void set_AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_245(int64_t value)
	{
		___AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_245 = value;
	}

	inline static int32_t get_offset_of_B15D37D4738DBDC32C38C2E1B6A1833322C22868_246() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___B15D37D4738DBDC32C38C2E1B6A1833322C22868_246)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_B15D37D4738DBDC32C38C2E1B6A1833322C22868_246() const { return ___B15D37D4738DBDC32C38C2E1B6A1833322C22868_246; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_B15D37D4738DBDC32C38C2E1B6A1833322C22868_246() { return &___B15D37D4738DBDC32C38C2E1B6A1833322C22868_246; }
	inline void set_B15D37D4738DBDC32C38C2E1B6A1833322C22868_246(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___B15D37D4738DBDC32C38C2E1B6A1833322C22868_246 = value;
	}

	inline static int32_t get_offset_of_B166D50D0891455E483E2C6E1DB11C3DC03CAFFA_247() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___B166D50D0891455E483E2C6E1DB11C3DC03CAFFA_247)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_B166D50D0891455E483E2C6E1DB11C3DC03CAFFA_247() const { return ___B166D50D0891455E483E2C6E1DB11C3DC03CAFFA_247; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_B166D50D0891455E483E2C6E1DB11C3DC03CAFFA_247() { return &___B166D50D0891455E483E2C6E1DB11C3DC03CAFFA_247; }
	inline void set_B166D50D0891455E483E2C6E1DB11C3DC03CAFFA_247(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___B166D50D0891455E483E2C6E1DB11C3DC03CAFFA_247 = value;
	}

	inline static int32_t get_offset_of_B19381C60E723A535EF856EC118DB867503FCC63_248() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___B19381C60E723A535EF856EC118DB867503FCC63_248)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_B19381C60E723A535EF856EC118DB867503FCC63_248() const { return ___B19381C60E723A535EF856EC118DB867503FCC63_248; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_B19381C60E723A535EF856EC118DB867503FCC63_248() { return &___B19381C60E723A535EF856EC118DB867503FCC63_248; }
	inline void set_B19381C60E723A535EF856EC118DB867503FCC63_248(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___B19381C60E723A535EF856EC118DB867503FCC63_248 = value;
	}

	inline static int32_t get_offset_of_B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_249() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_249)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_249() const { return ___B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_249; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_249() { return &___B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_249; }
	inline void set_B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_249(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_249 = value;
	}

	inline static int32_t get_offset_of_B3FDEC1908C5093BFC5CFB12749EE2650C8612C3_250() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___B3FDEC1908C5093BFC5CFB12749EE2650C8612C3_250)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_B3FDEC1908C5093BFC5CFB12749EE2650C8612C3_250() const { return ___B3FDEC1908C5093BFC5CFB12749EE2650C8612C3_250; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_B3FDEC1908C5093BFC5CFB12749EE2650C8612C3_250() { return &___B3FDEC1908C5093BFC5CFB12749EE2650C8612C3_250; }
	inline void set_B3FDEC1908C5093BFC5CFB12749EE2650C8612C3_250(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___B3FDEC1908C5093BFC5CFB12749EE2650C8612C3_250 = value;
	}

	inline static int32_t get_offset_of_B5DDF34D20E879DBA76358A13661F61E6682B937_251() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___B5DDF34D20E879DBA76358A13661F61E6682B937_251)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_B5DDF34D20E879DBA76358A13661F61E6682B937_251() const { return ___B5DDF34D20E879DBA76358A13661F61E6682B937_251; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_B5DDF34D20E879DBA76358A13661F61E6682B937_251() { return &___B5DDF34D20E879DBA76358A13661F61E6682B937_251; }
	inline void set_B5DDF34D20E879DBA76358A13661F61E6682B937_251(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___B5DDF34D20E879DBA76358A13661F61E6682B937_251 = value;
	}

	inline static int32_t get_offset_of_B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_252() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_252)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_252() const { return ___B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_252; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_252() { return &___B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_252; }
	inline void set_B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_252(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_252 = value;
	}

	inline static int32_t get_offset_of_B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_253() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_253)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_253() const { return ___B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_253; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_253() { return &___B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_253; }
	inline void set_B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_253(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_253 = value;
	}

	inline static int32_t get_offset_of_BB40482B7503DCF14BB6DAE300C2717C3021C7B2_254() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___BB40482B7503DCF14BB6DAE300C2717C3021C7B2_254)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_BB40482B7503DCF14BB6DAE300C2717C3021C7B2_254() const { return ___BB40482B7503DCF14BB6DAE300C2717C3021C7B2_254; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_BB40482B7503DCF14BB6DAE300C2717C3021C7B2_254() { return &___BB40482B7503DCF14BB6DAE300C2717C3021C7B2_254; }
	inline void set_BB40482B7503DCF14BB6DAE300C2717C3021C7B2_254(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___BB40482B7503DCF14BB6DAE300C2717C3021C7B2_254 = value;
	}

	inline static int32_t get_offset_of_BBA3BFEF194A76998C4874D107EBEA8A81357762_255() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___BBA3BFEF194A76998C4874D107EBEA8A81357762_255)); }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  get_BBA3BFEF194A76998C4874D107EBEA8A81357762_255() const { return ___BBA3BFEF194A76998C4874D107EBEA8A81357762_255; }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7 * get_address_of_BBA3BFEF194A76998C4874D107EBEA8A81357762_255() { return &___BBA3BFEF194A76998C4874D107EBEA8A81357762_255; }
	inline void set_BBA3BFEF194A76998C4874D107EBEA8A81357762_255(__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  value)
	{
		___BBA3BFEF194A76998C4874D107EBEA8A81357762_255 = value;
	}

	inline static int32_t get_offset_of_BBAA25706062E8AA738F96C541E3A6D6357F92E3_256() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___BBAA25706062E8AA738F96C541E3A6D6357F92E3_256)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_BBAA25706062E8AA738F96C541E3A6D6357F92E3_256() const { return ___BBAA25706062E8AA738F96C541E3A6D6357F92E3_256; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_BBAA25706062E8AA738F96C541E3A6D6357F92E3_256() { return &___BBAA25706062E8AA738F96C541E3A6D6357F92E3_256; }
	inline void set_BBAA25706062E8AA738F96C541E3A6D6357F92E3_256(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___BBAA25706062E8AA738F96C541E3A6D6357F92E3_256 = value;
	}

	inline static int32_t get_offset_of_BBE42ED1645F9D77581D1D5B6EE11B933512F832_257() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___BBE42ED1645F9D77581D1D5B6EE11B933512F832_257)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_BBE42ED1645F9D77581D1D5B6EE11B933512F832_257() const { return ___BBE42ED1645F9D77581D1D5B6EE11B933512F832_257; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_BBE42ED1645F9D77581D1D5B6EE11B933512F832_257() { return &___BBE42ED1645F9D77581D1D5B6EE11B933512F832_257; }
	inline void set_BBE42ED1645F9D77581D1D5B6EE11B933512F832_257(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___BBE42ED1645F9D77581D1D5B6EE11B933512F832_257 = value;
	}

	inline static int32_t get_offset_of_BC1ACC3AC4459BD0D19A953A64E05727DA98279A_258() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___BC1ACC3AC4459BD0D19A953A64E05727DA98279A_258)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_BC1ACC3AC4459BD0D19A953A64E05727DA98279A_258() const { return ___BC1ACC3AC4459BD0D19A953A64E05727DA98279A_258; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_BC1ACC3AC4459BD0D19A953A64E05727DA98279A_258() { return &___BC1ACC3AC4459BD0D19A953A64E05727DA98279A_258; }
	inline void set_BC1ACC3AC4459BD0D19A953A64E05727DA98279A_258(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___BC1ACC3AC4459BD0D19A953A64E05727DA98279A_258 = value;
	}

	inline static int32_t get_offset_of_BC5EFA0F409033551EB01FD9CAA887739FBB267C_259() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___BC5EFA0F409033551EB01FD9CAA887739FBB267C_259)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_BC5EFA0F409033551EB01FD9CAA887739FBB267C_259() const { return ___BC5EFA0F409033551EB01FD9CAA887739FBB267C_259; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_BC5EFA0F409033551EB01FD9CAA887739FBB267C_259() { return &___BC5EFA0F409033551EB01FD9CAA887739FBB267C_259; }
	inline void set_BC5EFA0F409033551EB01FD9CAA887739FBB267C_259(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___BC5EFA0F409033551EB01FD9CAA887739FBB267C_259 = value;
	}

	inline static int32_t get_offset_of_BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_260() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_260)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_260() const { return ___BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_260; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_260() { return &___BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_260; }
	inline void set_BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_260(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_260 = value;
	}

	inline static int32_t get_offset_of_BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_261() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_261)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_261() const { return ___BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_261; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_261() { return &___BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_261; }
	inline void set_BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_261(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_261 = value;
	}

	inline static int32_t get_offset_of_BF4FBD059EA47B09FA514A81515DD83A7F22A498_262() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___BF4FBD059EA47B09FA514A81515DD83A7F22A498_262)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_BF4FBD059EA47B09FA514A81515DD83A7F22A498_262() const { return ___BF4FBD059EA47B09FA514A81515DD83A7F22A498_262; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_BF4FBD059EA47B09FA514A81515DD83A7F22A498_262() { return &___BF4FBD059EA47B09FA514A81515DD83A7F22A498_262; }
	inline void set_BF4FBD059EA47B09FA514A81515DD83A7F22A498_262(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___BF4FBD059EA47B09FA514A81515DD83A7F22A498_262 = value;
	}

	inline static int32_t get_offset_of_BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_263() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_263)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_263() const { return ___BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_263; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_263() { return &___BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_263; }
	inline void set_BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_263(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_263 = value;
	}

	inline static int32_t get_offset_of_C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_264() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_264)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_264() const { return ___C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_264; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_264() { return &___C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_264; }
	inline void set_C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_264(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_264 = value;
	}

	inline static int32_t get_offset_of_C0935C173C7A144FE24DA09584F4892153D01F3C_265() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___C0935C173C7A144FE24DA09584F4892153D01F3C_265)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_C0935C173C7A144FE24DA09584F4892153D01F3C_265() const { return ___C0935C173C7A144FE24DA09584F4892153D01F3C_265; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_C0935C173C7A144FE24DA09584F4892153D01F3C_265() { return &___C0935C173C7A144FE24DA09584F4892153D01F3C_265; }
	inline void set_C0935C173C7A144FE24DA09584F4892153D01F3C_265(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___C0935C173C7A144FE24DA09584F4892153D01F3C_265 = value;
	}

	inline static int32_t get_offset_of_C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_266() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_266)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_266() const { return ___C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_266; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_266() { return &___C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_266; }
	inline void set_C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_266(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_266 = value;
	}

	inline static int32_t get_offset_of_C4449C115C6F0E639E5F53D593F6E85D55D18DB0_267() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___C4449C115C6F0E639E5F53D593F6E85D55D18DB0_267)); }
	inline __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  get_C4449C115C6F0E639E5F53D593F6E85D55D18DB0_267() const { return ___C4449C115C6F0E639E5F53D593F6E85D55D18DB0_267; }
	inline __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F * get_address_of_C4449C115C6F0E639E5F53D593F6E85D55D18DB0_267() { return &___C4449C115C6F0E639E5F53D593F6E85D55D18DB0_267; }
	inline void set_C4449C115C6F0E639E5F53D593F6E85D55D18DB0_267(__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  value)
	{
		___C4449C115C6F0E639E5F53D593F6E85D55D18DB0_267 = value;
	}

	inline static int32_t get_offset_of_C530C580972A28DBE79D791F61E5916A541E6A1A_268() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___C530C580972A28DBE79D791F61E5916A541E6A1A_268)); }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  get_C530C580972A28DBE79D791F61E5916A541E6A1A_268() const { return ___C530C580972A28DBE79D791F61E5916A541E6A1A_268; }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 * get_address_of_C530C580972A28DBE79D791F61E5916A541E6A1A_268() { return &___C530C580972A28DBE79D791F61E5916A541E6A1A_268; }
	inline void set_C530C580972A28DBE79D791F61E5916A541E6A1A_268(__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  value)
	{
		___C530C580972A28DBE79D791F61E5916A541E6A1A_268 = value;
	}

	inline static int32_t get_offset_of_C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_269() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_269)); }
	inline __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  get_C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_269() const { return ___C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_269; }
	inline __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F * get_address_of_C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_269() { return &___C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_269; }
	inline void set_C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_269(__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  value)
	{
		___C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_269 = value;
	}

	inline static int32_t get_offset_of_C62717FC35BA54563F42385BF79E44DD0A510124_270() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___C62717FC35BA54563F42385BF79E44DD0A510124_270)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_C62717FC35BA54563F42385BF79E44DD0A510124_270() const { return ___C62717FC35BA54563F42385BF79E44DD0A510124_270; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_C62717FC35BA54563F42385BF79E44DD0A510124_270() { return &___C62717FC35BA54563F42385BF79E44DD0A510124_270; }
	inline void set_C62717FC35BA54563F42385BF79E44DD0A510124_270(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___C62717FC35BA54563F42385BF79E44DD0A510124_270 = value;
	}

	inline static int32_t get_offset_of_C6B6783FDC75202733D0518585E649906008DBC5_271() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___C6B6783FDC75202733D0518585E649906008DBC5_271)); }
	inline __StaticArrayInitTypeSizeU3D64_t2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF  get_C6B6783FDC75202733D0518585E649906008DBC5_271() const { return ___C6B6783FDC75202733D0518585E649906008DBC5_271; }
	inline __StaticArrayInitTypeSizeU3D64_t2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF * get_address_of_C6B6783FDC75202733D0518585E649906008DBC5_271() { return &___C6B6783FDC75202733D0518585E649906008DBC5_271; }
	inline void set_C6B6783FDC75202733D0518585E649906008DBC5_271(__StaticArrayInitTypeSizeU3D64_t2ED7EB1008573AB4C244DF1F08063F97FCC5ACBF  value)
	{
		___C6B6783FDC75202733D0518585E649906008DBC5_271 = value;
	}

	inline static int32_t get_offset_of_C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_272() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_272)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_272() const { return ___C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_272; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_272() { return &___C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_272; }
	inline void set_C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_272(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_272 = value;
	}

	inline static int32_t get_offset_of_C7787E3098145FA28D38DE952D634BA862284127_273() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___C7787E3098145FA28D38DE952D634BA862284127_273)); }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  get_C7787E3098145FA28D38DE952D634BA862284127_273() const { return ___C7787E3098145FA28D38DE952D634BA862284127_273; }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7 * get_address_of_C7787E3098145FA28D38DE952D634BA862284127_273() { return &___C7787E3098145FA28D38DE952D634BA862284127_273; }
	inline void set_C7787E3098145FA28D38DE952D634BA862284127_273(__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  value)
	{
		___C7787E3098145FA28D38DE952D634BA862284127_273 = value;
	}

	inline static int32_t get_offset_of_C7BD442E02179DC75AB7FEF343C297E1F46A7806_274() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___C7BD442E02179DC75AB7FEF343C297E1F46A7806_274)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_C7BD442E02179DC75AB7FEF343C297E1F46A7806_274() const { return ___C7BD442E02179DC75AB7FEF343C297E1F46A7806_274; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_C7BD442E02179DC75AB7FEF343C297E1F46A7806_274() { return &___C7BD442E02179DC75AB7FEF343C297E1F46A7806_274; }
	inline void set_C7BD442E02179DC75AB7FEF343C297E1F46A7806_274(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___C7BD442E02179DC75AB7FEF343C297E1F46A7806_274 = value;
	}

	inline static int32_t get_offset_of_C7FBA0F208A16658206056EC89481A5EE74A7259_275() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___C7FBA0F208A16658206056EC89481A5EE74A7259_275)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_C7FBA0F208A16658206056EC89481A5EE74A7259_275() const { return ___C7FBA0F208A16658206056EC89481A5EE74A7259_275; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_C7FBA0F208A16658206056EC89481A5EE74A7259_275() { return &___C7FBA0F208A16658206056EC89481A5EE74A7259_275; }
	inline void set_C7FBA0F208A16658206056EC89481A5EE74A7259_275(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___C7FBA0F208A16658206056EC89481A5EE74A7259_275 = value;
	}

	inline static int32_t get_offset_of_C82F7744F78AA5830EDBD9AD774CFF76737F7D40_276() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___C82F7744F78AA5830EDBD9AD774CFF76737F7D40_276)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_C82F7744F78AA5830EDBD9AD774CFF76737F7D40_276() const { return ___C82F7744F78AA5830EDBD9AD774CFF76737F7D40_276; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_C82F7744F78AA5830EDBD9AD774CFF76737F7D40_276() { return &___C82F7744F78AA5830EDBD9AD774CFF76737F7D40_276; }
	inline void set_C82F7744F78AA5830EDBD9AD774CFF76737F7D40_276(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___C82F7744F78AA5830EDBD9AD774CFF76737F7D40_276 = value;
	}

	inline static int32_t get_offset_of_C92559E411FAF1D47ADD6CCA7374DAF066069D00_277() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___C92559E411FAF1D47ADD6CCA7374DAF066069D00_277)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_C92559E411FAF1D47ADD6CCA7374DAF066069D00_277() const { return ___C92559E411FAF1D47ADD6CCA7374DAF066069D00_277; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_C92559E411FAF1D47ADD6CCA7374DAF066069D00_277() { return &___C92559E411FAF1D47ADD6CCA7374DAF066069D00_277; }
	inline void set_C92559E411FAF1D47ADD6CCA7374DAF066069D00_277(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___C92559E411FAF1D47ADD6CCA7374DAF066069D00_277 = value;
	}

	inline static int32_t get_offset_of_C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_278() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_278)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_278() const { return ___C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_278; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_278() { return &___C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_278; }
	inline void set_C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_278(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_278 = value;
	}

	inline static int32_t get_offset_of_CA03F848D4A9DD85B5CF092E36317D483E3B0864_279() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___CA03F848D4A9DD85B5CF092E36317D483E3B0864_279)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_CA03F848D4A9DD85B5CF092E36317D483E3B0864_279() const { return ___CA03F848D4A9DD85B5CF092E36317D483E3B0864_279; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_CA03F848D4A9DD85B5CF092E36317D483E3B0864_279() { return &___CA03F848D4A9DD85B5CF092E36317D483E3B0864_279; }
	inline void set_CA03F848D4A9DD85B5CF092E36317D483E3B0864_279(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___CA03F848D4A9DD85B5CF092E36317D483E3B0864_279 = value;
	}

	inline static int32_t get_offset_of_CB780513B9E4B4E2590F7104B066F632E7BC3E38_280() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___CB780513B9E4B4E2590F7104B066F632E7BC3E38_280)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_CB780513B9E4B4E2590F7104B066F632E7BC3E38_280() const { return ___CB780513B9E4B4E2590F7104B066F632E7BC3E38_280; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_CB780513B9E4B4E2590F7104B066F632E7BC3E38_280() { return &___CB780513B9E4B4E2590F7104B066F632E7BC3E38_280; }
	inline void set_CB780513B9E4B4E2590F7104B066F632E7BC3E38_280(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___CB780513B9E4B4E2590F7104B066F632E7BC3E38_280 = value;
	}

	inline static int32_t get_offset_of_CC74BF89CF621AAC29665426A911E4B8BBB60B91_281() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___CC74BF89CF621AAC29665426A911E4B8BBB60B91_281)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_CC74BF89CF621AAC29665426A911E4B8BBB60B91_281() const { return ___CC74BF89CF621AAC29665426A911E4B8BBB60B91_281; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_CC74BF89CF621AAC29665426A911E4B8BBB60B91_281() { return &___CC74BF89CF621AAC29665426A911E4B8BBB60B91_281; }
	inline void set_CC74BF89CF621AAC29665426A911E4B8BBB60B91_281(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___CC74BF89CF621AAC29665426A911E4B8BBB60B91_281 = value;
	}

	inline static int32_t get_offset_of_CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_282() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_282)); }
	inline __StaticArrayInitTypeSizeU3D10_tB641FD73E10817DC3A5A0B6CDD83506C3C2B53C7  get_CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_282() const { return ___CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_282; }
	inline __StaticArrayInitTypeSizeU3D10_tB641FD73E10817DC3A5A0B6CDD83506C3C2B53C7 * get_address_of_CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_282() { return &___CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_282; }
	inline void set_CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_282(__StaticArrayInitTypeSizeU3D10_tB641FD73E10817DC3A5A0B6CDD83506C3C2B53C7  value)
	{
		___CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_282 = value;
	}

	inline static int32_t get_offset_of_CC9EE2CF247B3534102C1324C2679503686EF031_283() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___CC9EE2CF247B3534102C1324C2679503686EF031_283)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_CC9EE2CF247B3534102C1324C2679503686EF031_283() const { return ___CC9EE2CF247B3534102C1324C2679503686EF031_283; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_CC9EE2CF247B3534102C1324C2679503686EF031_283() { return &___CC9EE2CF247B3534102C1324C2679503686EF031_283; }
	inline void set_CC9EE2CF247B3534102C1324C2679503686EF031_283(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___CC9EE2CF247B3534102C1324C2679503686EF031_283 = value;
	}

	inline static int32_t get_offset_of_CDDC3D4212D35722E3E8BAA7B5275932A6567037_284() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___CDDC3D4212D35722E3E8BAA7B5275932A6567037_284)); }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  get_CDDC3D4212D35722E3E8BAA7B5275932A6567037_284() const { return ___CDDC3D4212D35722E3E8BAA7B5275932A6567037_284; }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54 * get_address_of_CDDC3D4212D35722E3E8BAA7B5275932A6567037_284() { return &___CDDC3D4212D35722E3E8BAA7B5275932A6567037_284; }
	inline void set_CDDC3D4212D35722E3E8BAA7B5275932A6567037_284(__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  value)
	{
		___CDDC3D4212D35722E3E8BAA7B5275932A6567037_284 = value;
	}

	inline static int32_t get_offset_of_CED1DB702E32A812D587254A4E1BAAA409E30859_285() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___CED1DB702E32A812D587254A4E1BAAA409E30859_285)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_CED1DB702E32A812D587254A4E1BAAA409E30859_285() const { return ___CED1DB702E32A812D587254A4E1BAAA409E30859_285; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_CED1DB702E32A812D587254A4E1BAAA409E30859_285() { return &___CED1DB702E32A812D587254A4E1BAAA409E30859_285; }
	inline void set_CED1DB702E32A812D587254A4E1BAAA409E30859_285(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___CED1DB702E32A812D587254A4E1BAAA409E30859_285 = value;
	}

	inline static int32_t get_offset_of_CF1C9B07580D3992A644F840EBBB156E0E92B758_286() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___CF1C9B07580D3992A644F840EBBB156E0E92B758_286)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_CF1C9B07580D3992A644F840EBBB156E0E92B758_286() const { return ___CF1C9B07580D3992A644F840EBBB156E0E92B758_286; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_CF1C9B07580D3992A644F840EBBB156E0E92B758_286() { return &___CF1C9B07580D3992A644F840EBBB156E0E92B758_286; }
	inline void set_CF1C9B07580D3992A644F840EBBB156E0E92B758_286(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___CF1C9B07580D3992A644F840EBBB156E0E92B758_286 = value;
	}

	inline static int32_t get_offset_of_D10229C07961891A434737F06293F1E0CC0FA33C_287() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___D10229C07961891A434737F06293F1E0CC0FA33C_287)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_D10229C07961891A434737F06293F1E0CC0FA33C_287() const { return ___D10229C07961891A434737F06293F1E0CC0FA33C_287; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_D10229C07961891A434737F06293F1E0CC0FA33C_287() { return &___D10229C07961891A434737F06293F1E0CC0FA33C_287; }
	inline void set_D10229C07961891A434737F06293F1E0CC0FA33C_287(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___D10229C07961891A434737F06293F1E0CC0FA33C_287 = value;
	}

	inline static int32_t get_offset_of_D1A2B61FD08BB2242E12F4CAF6B512029C0DC177_288() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___D1A2B61FD08BB2242E12F4CAF6B512029C0DC177_288)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_D1A2B61FD08BB2242E12F4CAF6B512029C0DC177_288() const { return ___D1A2B61FD08BB2242E12F4CAF6B512029C0DC177_288; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_D1A2B61FD08BB2242E12F4CAF6B512029C0DC177_288() { return &___D1A2B61FD08BB2242E12F4CAF6B512029C0DC177_288; }
	inline void set_D1A2B61FD08BB2242E12F4CAF6B512029C0DC177_288(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___D1A2B61FD08BB2242E12F4CAF6B512029C0DC177_288 = value;
	}

	inline static int32_t get_offset_of_D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_289() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_289)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_289() const { return ___D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_289; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_289() { return &___D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_289; }
	inline void set_D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_289(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_289 = value;
	}

	inline static int32_t get_offset_of_D2BCB25441A9A8F3DE94D1EF27A870BB77553833_290() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___D2BCB25441A9A8F3DE94D1EF27A870BB77553833_290)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_D2BCB25441A9A8F3DE94D1EF27A870BB77553833_290() const { return ___D2BCB25441A9A8F3DE94D1EF27A870BB77553833_290; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_D2BCB25441A9A8F3DE94D1EF27A870BB77553833_290() { return &___D2BCB25441A9A8F3DE94D1EF27A870BB77553833_290; }
	inline void set_D2BCB25441A9A8F3DE94D1EF27A870BB77553833_290(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___D2BCB25441A9A8F3DE94D1EF27A870BB77553833_290 = value;
	}

	inline static int32_t get_offset_of_D41FD917303D010F26005BDAF1F31ABC8A512A13_291() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___D41FD917303D010F26005BDAF1F31ABC8A512A13_291)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_D41FD917303D010F26005BDAF1F31ABC8A512A13_291() const { return ___D41FD917303D010F26005BDAF1F31ABC8A512A13_291; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_D41FD917303D010F26005BDAF1F31ABC8A512A13_291() { return &___D41FD917303D010F26005BDAF1F31ABC8A512A13_291; }
	inline void set_D41FD917303D010F26005BDAF1F31ABC8A512A13_291(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___D41FD917303D010F26005BDAF1F31ABC8A512A13_291 = value;
	}

	inline static int32_t get_offset_of_D5B6993D49960DF01393AD8095091B12332C2FA3_292() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___D5B6993D49960DF01393AD8095091B12332C2FA3_292)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_D5B6993D49960DF01393AD8095091B12332C2FA3_292() const { return ___D5B6993D49960DF01393AD8095091B12332C2FA3_292; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_D5B6993D49960DF01393AD8095091B12332C2FA3_292() { return &___D5B6993D49960DF01393AD8095091B12332C2FA3_292; }
	inline void set_D5B6993D49960DF01393AD8095091B12332C2FA3_292(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___D5B6993D49960DF01393AD8095091B12332C2FA3_292 = value;
	}

	inline static int32_t get_offset_of_D6B85A234C187D0FC82CE310F6423237C934B93A_293() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___D6B85A234C187D0FC82CE310F6423237C934B93A_293)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_D6B85A234C187D0FC82CE310F6423237C934B93A_293() const { return ___D6B85A234C187D0FC82CE310F6423237C934B93A_293; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_D6B85A234C187D0FC82CE310F6423237C934B93A_293() { return &___D6B85A234C187D0FC82CE310F6423237C934B93A_293; }
	inline void set_D6B85A234C187D0FC82CE310F6423237C934B93A_293(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___D6B85A234C187D0FC82CE310F6423237C934B93A_293 = value;
	}

	inline static int32_t get_offset_of_D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_294() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_294)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_294() const { return ___D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_294; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_294() { return &___D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_294; }
	inline void set_D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_294(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_294 = value;
	}

	inline static int32_t get_offset_of_D7207A48A00E499EF63FC2990337CC8B05C7AE9C_295() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___D7207A48A00E499EF63FC2990337CC8B05C7AE9C_295)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_D7207A48A00E499EF63FC2990337CC8B05C7AE9C_295() const { return ___D7207A48A00E499EF63FC2990337CC8B05C7AE9C_295; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_D7207A48A00E499EF63FC2990337CC8B05C7AE9C_295() { return &___D7207A48A00E499EF63FC2990337CC8B05C7AE9C_295; }
	inline void set_D7207A48A00E499EF63FC2990337CC8B05C7AE9C_295(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___D7207A48A00E499EF63FC2990337CC8B05C7AE9C_295 = value;
	}

	inline static int32_t get_offset_of_D73BDC60FA2857E7CE2F742530B056A1866C2177_296() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___D73BDC60FA2857E7CE2F742530B056A1866C2177_296)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_D73BDC60FA2857E7CE2F742530B056A1866C2177_296() const { return ___D73BDC60FA2857E7CE2F742530B056A1866C2177_296; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_D73BDC60FA2857E7CE2F742530B056A1866C2177_296() { return &___D73BDC60FA2857E7CE2F742530B056A1866C2177_296; }
	inline void set_D73BDC60FA2857E7CE2F742530B056A1866C2177_296(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___D73BDC60FA2857E7CE2F742530B056A1866C2177_296 = value;
	}

	inline static int32_t get_offset_of_D7AE4E850949E37E7359595699CCDC751E1C45E4_297() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___D7AE4E850949E37E7359595699CCDC751E1C45E4_297)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_D7AE4E850949E37E7359595699CCDC751E1C45E4_297() const { return ___D7AE4E850949E37E7359595699CCDC751E1C45E4_297; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_D7AE4E850949E37E7359595699CCDC751E1C45E4_297() { return &___D7AE4E850949E37E7359595699CCDC751E1C45E4_297; }
	inline void set_D7AE4E850949E37E7359595699CCDC751E1C45E4_297(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___D7AE4E850949E37E7359595699CCDC751E1C45E4_297 = value;
	}

	inline static int32_t get_offset_of_D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_298() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_298)); }
	inline __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  get_D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_298() const { return ___D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_298; }
	inline __StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F * get_address_of_D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_298() { return &___D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_298; }
	inline void set_D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_298(__StaticArrayInitTypeSizeU3D40_tB5E642B07376B8F47DB15AAA7C0561CB42F6591F  value)
	{
		___D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_298 = value;
	}

	inline static int32_t get_offset_of_D923777F48CDA51F21CABCED4FE36A2D620288A2_299() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___D923777F48CDA51F21CABCED4FE36A2D620288A2_299)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_D923777F48CDA51F21CABCED4FE36A2D620288A2_299() const { return ___D923777F48CDA51F21CABCED4FE36A2D620288A2_299; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_D923777F48CDA51F21CABCED4FE36A2D620288A2_299() { return &___D923777F48CDA51F21CABCED4FE36A2D620288A2_299; }
	inline void set_D923777F48CDA51F21CABCED4FE36A2D620288A2_299(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___D923777F48CDA51F21CABCED4FE36A2D620288A2_299 = value;
	}

	inline static int32_t get_offset_of_DA8D7694A42602F7DC0356F30B9E1DFF8196724A_300() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___DA8D7694A42602F7DC0356F30B9E1DFF8196724A_300)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_DA8D7694A42602F7DC0356F30B9E1DFF8196724A_300() const { return ___DA8D7694A42602F7DC0356F30B9E1DFF8196724A_300; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_DA8D7694A42602F7DC0356F30B9E1DFF8196724A_300() { return &___DA8D7694A42602F7DC0356F30B9E1DFF8196724A_300; }
	inline void set_DA8D7694A42602F7DC0356F30B9E1DFF8196724A_300(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___DA8D7694A42602F7DC0356F30B9E1DFF8196724A_300 = value;
	}

	inline static int32_t get_offset_of_DBF9A772127E1119D6E557AF812B6078438CE970_301() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___DBF9A772127E1119D6E557AF812B6078438CE970_301)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_DBF9A772127E1119D6E557AF812B6078438CE970_301() const { return ___DBF9A772127E1119D6E557AF812B6078438CE970_301; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_DBF9A772127E1119D6E557AF812B6078438CE970_301() { return &___DBF9A772127E1119D6E557AF812B6078438CE970_301; }
	inline void set_DBF9A772127E1119D6E557AF812B6078438CE970_301(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___DBF9A772127E1119D6E557AF812B6078438CE970_301 = value;
	}

	inline static int32_t get_offset_of_DC7D58FD4F690A68811E226229F74D0089F018B3_302() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___DC7D58FD4F690A68811E226229F74D0089F018B3_302)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_DC7D58FD4F690A68811E226229F74D0089F018B3_302() const { return ___DC7D58FD4F690A68811E226229F74D0089F018B3_302; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_DC7D58FD4F690A68811E226229F74D0089F018B3_302() { return &___DC7D58FD4F690A68811E226229F74D0089F018B3_302; }
	inline void set_DC7D58FD4F690A68811E226229F74D0089F018B3_302(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___DC7D58FD4F690A68811E226229F74D0089F018B3_302 = value;
	}

	inline static int32_t get_offset_of_DD04181543E071971F6379A33D631064A9B051D8_303() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___DD04181543E071971F6379A33D631064A9B051D8_303)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_DD04181543E071971F6379A33D631064A9B051D8_303() const { return ___DD04181543E071971F6379A33D631064A9B051D8_303; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_DD04181543E071971F6379A33D631064A9B051D8_303() { return &___DD04181543E071971F6379A33D631064A9B051D8_303; }
	inline void set_DD04181543E071971F6379A33D631064A9B051D8_303(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___DD04181543E071971F6379A33D631064A9B051D8_303 = value;
	}

	inline static int32_t get_offset_of_DD2B45529EC6CEEBB86596D35461F63244FB3BC1_304() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___DD2B45529EC6CEEBB86596D35461F63244FB3BC1_304)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_DD2B45529EC6CEEBB86596D35461F63244FB3BC1_304() const { return ___DD2B45529EC6CEEBB86596D35461F63244FB3BC1_304; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_DD2B45529EC6CEEBB86596D35461F63244FB3BC1_304() { return &___DD2B45529EC6CEEBB86596D35461F63244FB3BC1_304; }
	inline void set_DD2B45529EC6CEEBB86596D35461F63244FB3BC1_304(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___DD2B45529EC6CEEBB86596D35461F63244FB3BC1_304 = value;
	}

	inline static int32_t get_offset_of_DEDEF9E8D2052710E15C0CC080004BFB33F47665_305() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___DEDEF9E8D2052710E15C0CC080004BFB33F47665_305)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_DEDEF9E8D2052710E15C0CC080004BFB33F47665_305() const { return ___DEDEF9E8D2052710E15C0CC080004BFB33F47665_305; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_DEDEF9E8D2052710E15C0CC080004BFB33F47665_305() { return &___DEDEF9E8D2052710E15C0CC080004BFB33F47665_305; }
	inline void set_DEDEF9E8D2052710E15C0CC080004BFB33F47665_305(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___DEDEF9E8D2052710E15C0CC080004BFB33F47665_305 = value;
	}

	inline static int32_t get_offset_of_E02E3EC94504AFEEB34CE409836A3797D7E43964_306() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___E02E3EC94504AFEEB34CE409836A3797D7E43964_306)); }
	inline __StaticArrayInitTypeSizeU3D11148_tA414153821EB70E6FCD456B1E2DB309D58CE1619  get_E02E3EC94504AFEEB34CE409836A3797D7E43964_306() const { return ___E02E3EC94504AFEEB34CE409836A3797D7E43964_306; }
	inline __StaticArrayInitTypeSizeU3D11148_tA414153821EB70E6FCD456B1E2DB309D58CE1619 * get_address_of_E02E3EC94504AFEEB34CE409836A3797D7E43964_306() { return &___E02E3EC94504AFEEB34CE409836A3797D7E43964_306; }
	inline void set_E02E3EC94504AFEEB34CE409836A3797D7E43964_306(__StaticArrayInitTypeSizeU3D11148_tA414153821EB70E6FCD456B1E2DB309D58CE1619  value)
	{
		___E02E3EC94504AFEEB34CE409836A3797D7E43964_306 = value;
	}

	inline static int32_t get_offset_of_E1A877959C9E369159A2433060A6781C6C5C1D5C_307() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___E1A877959C9E369159A2433060A6781C6C5C1D5C_307)); }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  get_E1A877959C9E369159A2433060A6781C6C5C1D5C_307() const { return ___E1A877959C9E369159A2433060A6781C6C5C1D5C_307; }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7 * get_address_of_E1A877959C9E369159A2433060A6781C6C5C1D5C_307() { return &___E1A877959C9E369159A2433060A6781C6C5C1D5C_307; }
	inline void set_E1A877959C9E369159A2433060A6781C6C5C1D5C_307(__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  value)
	{
		___E1A877959C9E369159A2433060A6781C6C5C1D5C_307 = value;
	}

	inline static int32_t get_offset_of_E1C849B129FBB4352239208AEFE34F3631A27813_308() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___E1C849B129FBB4352239208AEFE34F3631A27813_308)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_E1C849B129FBB4352239208AEFE34F3631A27813_308() const { return ___E1C849B129FBB4352239208AEFE34F3631A27813_308; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_E1C849B129FBB4352239208AEFE34F3631A27813_308() { return &___E1C849B129FBB4352239208AEFE34F3631A27813_308; }
	inline void set_E1C849B129FBB4352239208AEFE34F3631A27813_308(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___E1C849B129FBB4352239208AEFE34F3631A27813_308 = value;
	}

	inline static int32_t get_offset_of_E2654B2181D84518EB5EE7BC342CFFD1F528E908_309() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___E2654B2181D84518EB5EE7BC342CFFD1F528E908_309)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_E2654B2181D84518EB5EE7BC342CFFD1F528E908_309() const { return ___E2654B2181D84518EB5EE7BC342CFFD1F528E908_309; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_E2654B2181D84518EB5EE7BC342CFFD1F528E908_309() { return &___E2654B2181D84518EB5EE7BC342CFFD1F528E908_309; }
	inline void set_E2654B2181D84518EB5EE7BC342CFFD1F528E908_309(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___E2654B2181D84518EB5EE7BC342CFFD1F528E908_309 = value;
	}

	inline static int32_t get_offset_of_E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_310() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_310)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_310() const { return ___E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_310; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_310() { return &___E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_310; }
	inline void set_E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_310(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_310 = value;
	}

	inline static int32_t get_offset_of_E2C0239EBBF1FD45925FAA7909458304DFD37A84_311() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___E2C0239EBBF1FD45925FAA7909458304DFD37A84_311)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_E2C0239EBBF1FD45925FAA7909458304DFD37A84_311() const { return ___E2C0239EBBF1FD45925FAA7909458304DFD37A84_311; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_E2C0239EBBF1FD45925FAA7909458304DFD37A84_311() { return &___E2C0239EBBF1FD45925FAA7909458304DFD37A84_311; }
	inline void set_E2C0239EBBF1FD45925FAA7909458304DFD37A84_311(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___E2C0239EBBF1FD45925FAA7909458304DFD37A84_311 = value;
	}

	inline static int32_t get_offset_of_E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_312() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_312)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_312() const { return ___E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_312; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_312() { return &___E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_312; }
	inline void set_E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_312(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_312 = value;
	}

	inline static int32_t get_offset_of_E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_313() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_313)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_313() const { return ___E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_313; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_313() { return &___E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_313; }
	inline void set_E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_313(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_313 = value;
	}

	inline static int32_t get_offset_of_E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_314() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_314)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_314() const { return ___E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_314; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_314() { return &___E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_314; }
	inline void set_E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_314(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_314 = value;
	}

	inline static int32_t get_offset_of_E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_315() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_315)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_315() const { return ___E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_315; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_315() { return &___E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_315; }
	inline void set_E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_315(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_315 = value;
	}

	inline static int32_t get_offset_of_E5395170F0D79C58C3AF15E7AA917ACA2307CE5F_316() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___E5395170F0D79C58C3AF15E7AA917ACA2307CE5F_316)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_E5395170F0D79C58C3AF15E7AA917ACA2307CE5F_316() const { return ___E5395170F0D79C58C3AF15E7AA917ACA2307CE5F_316; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_E5395170F0D79C58C3AF15E7AA917ACA2307CE5F_316() { return &___E5395170F0D79C58C3AF15E7AA917ACA2307CE5F_316; }
	inline void set_E5395170F0D79C58C3AF15E7AA917ACA2307CE5F_316(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___E5395170F0D79C58C3AF15E7AA917ACA2307CE5F_316 = value;
	}

	inline static int32_t get_offset_of_E5AD60E2BE68FA0155888A7E87B34F03905E7584_317() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___E5AD60E2BE68FA0155888A7E87B34F03905E7584_317)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_E5AD60E2BE68FA0155888A7E87B34F03905E7584_317() const { return ___E5AD60E2BE68FA0155888A7E87B34F03905E7584_317; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_E5AD60E2BE68FA0155888A7E87B34F03905E7584_317() { return &___E5AD60E2BE68FA0155888A7E87B34F03905E7584_317; }
	inline void set_E5AD60E2BE68FA0155888A7E87B34F03905E7584_317(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___E5AD60E2BE68FA0155888A7E87B34F03905E7584_317 = value;
	}

	inline static int32_t get_offset_of_E62D0D793D6BCA39079E6441B162E59F33ED2B07_318() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___E62D0D793D6BCA39079E6441B162E59F33ED2B07_318)); }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  get_E62D0D793D6BCA39079E6441B162E59F33ED2B07_318() const { return ___E62D0D793D6BCA39079E6441B162E59F33ED2B07_318; }
	inline __StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 * get_address_of_E62D0D793D6BCA39079E6441B162E59F33ED2B07_318() { return &___E62D0D793D6BCA39079E6441B162E59F33ED2B07_318; }
	inline void set_E62D0D793D6BCA39079E6441B162E59F33ED2B07_318(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5  value)
	{
		___E62D0D793D6BCA39079E6441B162E59F33ED2B07_318 = value;
	}

	inline static int32_t get_offset_of_E6C34D5D4381C0E6151B26FB9A314D8A57292366_319() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___E6C34D5D4381C0E6151B26FB9A314D8A57292366_319)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_E6C34D5D4381C0E6151B26FB9A314D8A57292366_319() const { return ___E6C34D5D4381C0E6151B26FB9A314D8A57292366_319; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_E6C34D5D4381C0E6151B26FB9A314D8A57292366_319() { return &___E6C34D5D4381C0E6151B26FB9A314D8A57292366_319; }
	inline void set_E6C34D5D4381C0E6151B26FB9A314D8A57292366_319(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___E6C34D5D4381C0E6151B26FB9A314D8A57292366_319 = value;
	}

	inline static int32_t get_offset_of_E9D2FA0CF959FCDF5A50AE1515251477F11EE719_320() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___E9D2FA0CF959FCDF5A50AE1515251477F11EE719_320)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_E9D2FA0CF959FCDF5A50AE1515251477F11EE719_320() const { return ___E9D2FA0CF959FCDF5A50AE1515251477F11EE719_320; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_E9D2FA0CF959FCDF5A50AE1515251477F11EE719_320() { return &___E9D2FA0CF959FCDF5A50AE1515251477F11EE719_320; }
	inline void set_E9D2FA0CF959FCDF5A50AE1515251477F11EE719_320(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___E9D2FA0CF959FCDF5A50AE1515251477F11EE719_320 = value;
	}

	inline static int32_t get_offset_of_EA86E31C9F6288681516E2AA22EF6B5CD441AB94_321() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___EA86E31C9F6288681516E2AA22EF6B5CD441AB94_321)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_EA86E31C9F6288681516E2AA22EF6B5CD441AB94_321() const { return ___EA86E31C9F6288681516E2AA22EF6B5CD441AB94_321; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_EA86E31C9F6288681516E2AA22EF6B5CD441AB94_321() { return &___EA86E31C9F6288681516E2AA22EF6B5CD441AB94_321; }
	inline void set_EA86E31C9F6288681516E2AA22EF6B5CD441AB94_321(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___EA86E31C9F6288681516E2AA22EF6B5CD441AB94_321 = value;
	}

	inline static int32_t get_offset_of_EB5C89140591CED42C39A8AE698A41F3593C3C62_322() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___EB5C89140591CED42C39A8AE698A41F3593C3C62_322)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_EB5C89140591CED42C39A8AE698A41F3593C3C62_322() const { return ___EB5C89140591CED42C39A8AE698A41F3593C3C62_322; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_EB5C89140591CED42C39A8AE698A41F3593C3C62_322() { return &___EB5C89140591CED42C39A8AE698A41F3593C3C62_322; }
	inline void set_EB5C89140591CED42C39A8AE698A41F3593C3C62_322(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___EB5C89140591CED42C39A8AE698A41F3593C3C62_322 = value;
	}

	inline static int32_t get_offset_of_EB774035880443AEFBF8B61E7760DA5273A8A44C_323() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___EB774035880443AEFBF8B61E7760DA5273A8A44C_323)); }
	inline __StaticArrayInitTypeSizeU3D148_t0A73993E265ED7BA3349D48D2563680FA231189B  get_EB774035880443AEFBF8B61E7760DA5273A8A44C_323() const { return ___EB774035880443AEFBF8B61E7760DA5273A8A44C_323; }
	inline __StaticArrayInitTypeSizeU3D148_t0A73993E265ED7BA3349D48D2563680FA231189B * get_address_of_EB774035880443AEFBF8B61E7760DA5273A8A44C_323() { return &___EB774035880443AEFBF8B61E7760DA5273A8A44C_323; }
	inline void set_EB774035880443AEFBF8B61E7760DA5273A8A44C_323(__StaticArrayInitTypeSizeU3D148_t0A73993E265ED7BA3349D48D2563680FA231189B  value)
	{
		___EB774035880443AEFBF8B61E7760DA5273A8A44C_323 = value;
	}

	inline static int32_t get_offset_of_EBB69348CF182DB7CB8021B09F8BBC2E8E87887C_324() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___EBB69348CF182DB7CB8021B09F8BBC2E8E87887C_324)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_EBB69348CF182DB7CB8021B09F8BBC2E8E87887C_324() const { return ___EBB69348CF182DB7CB8021B09F8BBC2E8E87887C_324; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_EBB69348CF182DB7CB8021B09F8BBC2E8E87887C_324() { return &___EBB69348CF182DB7CB8021B09F8BBC2E8E87887C_324; }
	inline void set_EBB69348CF182DB7CB8021B09F8BBC2E8E87887C_324(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___EBB69348CF182DB7CB8021B09F8BBC2E8E87887C_324 = value;
	}

	inline static int32_t get_offset_of_EDF6280B8AEAF6DDC736AF7B745668475CE8552F_325() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___EDF6280B8AEAF6DDC736AF7B745668475CE8552F_325)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_EDF6280B8AEAF6DDC736AF7B745668475CE8552F_325() const { return ___EDF6280B8AEAF6DDC736AF7B745668475CE8552F_325; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_EDF6280B8AEAF6DDC736AF7B745668475CE8552F_325() { return &___EDF6280B8AEAF6DDC736AF7B745668475CE8552F_325; }
	inline void set_EDF6280B8AEAF6DDC736AF7B745668475CE8552F_325(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___EDF6280B8AEAF6DDC736AF7B745668475CE8552F_325 = value;
	}

	inline static int32_t get_offset_of_EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_326() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_326)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_326() const { return ___EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_326; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_326() { return &___EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_326; }
	inline void set_EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_326(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_326 = value;
	}

	inline static int32_t get_offset_of_EF1366407DE423137F5977D7AF18B0C058147698_327() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___EF1366407DE423137F5977D7AF18B0C058147698_327)); }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  get_EF1366407DE423137F5977D7AF18B0C058147698_327() const { return ___EF1366407DE423137F5977D7AF18B0C058147698_327; }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54 * get_address_of_EF1366407DE423137F5977D7AF18B0C058147698_327() { return &___EF1366407DE423137F5977D7AF18B0C058147698_327; }
	inline void set_EF1366407DE423137F5977D7AF18B0C058147698_327(__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  value)
	{
		___EF1366407DE423137F5977D7AF18B0C058147698_327 = value;
	}

	inline static int32_t get_offset_of_EFAA45999AF190FC718690DC0121EB49DE7FFCE5_328() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___EFAA45999AF190FC718690DC0121EB49DE7FFCE5_328)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_EFAA45999AF190FC718690DC0121EB49DE7FFCE5_328() const { return ___EFAA45999AF190FC718690DC0121EB49DE7FFCE5_328; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_EFAA45999AF190FC718690DC0121EB49DE7FFCE5_328() { return &___EFAA45999AF190FC718690DC0121EB49DE7FFCE5_328; }
	inline void set_EFAA45999AF190FC718690DC0121EB49DE7FFCE5_328(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___EFAA45999AF190FC718690DC0121EB49DE7FFCE5_328 = value;
	}

	inline static int32_t get_offset_of_F09F029091837E12B4E60500BED0749D79F77FC2_329() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F09F029091837E12B4E60500BED0749D79F77FC2_329)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_F09F029091837E12B4E60500BED0749D79F77FC2_329() const { return ___F09F029091837E12B4E60500BED0749D79F77FC2_329; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_F09F029091837E12B4E60500BED0749D79F77FC2_329() { return &___F09F029091837E12B4E60500BED0749D79F77FC2_329; }
	inline void set_F09F029091837E12B4E60500BED0749D79F77FC2_329(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___F09F029091837E12B4E60500BED0749D79F77FC2_329 = value;
	}

	inline static int32_t get_offset_of_F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_330() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_330)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_330() const { return ___F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_330; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_330() { return &___F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_330; }
	inline void set_F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_330(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_330 = value;
	}

	inline static int32_t get_offset_of_F0E174E706EA1C2F8D181561F84976C942B1DE4B_331() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F0E174E706EA1C2F8D181561F84976C942B1DE4B_331)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_F0E174E706EA1C2F8D181561F84976C942B1DE4B_331() const { return ___F0E174E706EA1C2F8D181561F84976C942B1DE4B_331; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_F0E174E706EA1C2F8D181561F84976C942B1DE4B_331() { return &___F0E174E706EA1C2F8D181561F84976C942B1DE4B_331; }
	inline void set_F0E174E706EA1C2F8D181561F84976C942B1DE4B_331(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___F0E174E706EA1C2F8D181561F84976C942B1DE4B_331 = value;
	}

	inline static int32_t get_offset_of_F100C709518544CED81B98D3F1C862F094DAF46A_332() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F100C709518544CED81B98D3F1C862F094DAF46A_332)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_F100C709518544CED81B98D3F1C862F094DAF46A_332() const { return ___F100C709518544CED81B98D3F1C862F094DAF46A_332; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_F100C709518544CED81B98D3F1C862F094DAF46A_332() { return &___F100C709518544CED81B98D3F1C862F094DAF46A_332; }
	inline void set_F100C709518544CED81B98D3F1C862F094DAF46A_332(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___F100C709518544CED81B98D3F1C862F094DAF46A_332 = value;
	}

	inline static int32_t get_offset_of_F1F22748A7C1A4CC79E97C2063C48D7FF070D941_333() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F1F22748A7C1A4CC79E97C2063C48D7FF070D941_333)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_F1F22748A7C1A4CC79E97C2063C48D7FF070D941_333() const { return ___F1F22748A7C1A4CC79E97C2063C48D7FF070D941_333; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_F1F22748A7C1A4CC79E97C2063C48D7FF070D941_333() { return &___F1F22748A7C1A4CC79E97C2063C48D7FF070D941_333; }
	inline void set_F1F22748A7C1A4CC79E97C2063C48D7FF070D941_333(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___F1F22748A7C1A4CC79E97C2063C48D7FF070D941_333 = value;
	}

	inline static int32_t get_offset_of_F20B4E4CCB47B4F0631333A4DEE30DB71336110C_334() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F20B4E4CCB47B4F0631333A4DEE30DB71336110C_334)); }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  get_F20B4E4CCB47B4F0631333A4DEE30DB71336110C_334() const { return ___F20B4E4CCB47B4F0631333A4DEE30DB71336110C_334; }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 * get_address_of_F20B4E4CCB47B4F0631333A4DEE30DB71336110C_334() { return &___F20B4E4CCB47B4F0631333A4DEE30DB71336110C_334; }
	inline void set_F20B4E4CCB47B4F0631333A4DEE30DB71336110C_334(__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  value)
	{
		___F20B4E4CCB47B4F0631333A4DEE30DB71336110C_334 = value;
	}

	inline static int32_t get_offset_of_F240F486E15B43E152AE7F2B692A7FA361FA7776_335() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F240F486E15B43E152AE7F2B692A7FA361FA7776_335)); }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  get_F240F486E15B43E152AE7F2B692A7FA361FA7776_335() const { return ___F240F486E15B43E152AE7F2B692A7FA361FA7776_335; }
	inline __StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445 * get_address_of_F240F486E15B43E152AE7F2B692A7FA361FA7776_335() { return &___F240F486E15B43E152AE7F2B692A7FA361FA7776_335; }
	inline void set_F240F486E15B43E152AE7F2B692A7FA361FA7776_335(__StaticArrayInitTypeSizeU3D28_tFDDEB43D291084240980227C00328EBA1C48F445  value)
	{
		___F240F486E15B43E152AE7F2B692A7FA361FA7776_335 = value;
	}

	inline static int32_t get_offset_of_F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_336() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_336)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_336() const { return ___F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_336; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_336() { return &___F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_336; }
	inline void set_F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_336(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_336 = value;
	}

	inline static int32_t get_offset_of_F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_337() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_337)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_337() const { return ___F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_337; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_337() { return &___F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_337; }
	inline void set_F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_337(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_337 = value;
	}

	inline static int32_t get_offset_of_F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_338() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_338)); }
	inline __StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016  get_F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_338() const { return ___F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_338; }
	inline __StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016 * get_address_of_F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_338() { return &___F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_338; }
	inline void set_F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_338(__StaticArrayInitTypeSizeU3D30_t14568882CF4903E97998A46DDDF1E20E37839016  value)
	{
		___F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_338 = value;
	}

	inline static int32_t get_offset_of_F44B79D8208EDC17F9C96AA7DEA4983114BF698D_339() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F44B79D8208EDC17F9C96AA7DEA4983114BF698D_339)); }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  get_F44B79D8208EDC17F9C96AA7DEA4983114BF698D_339() const { return ___F44B79D8208EDC17F9C96AA7DEA4983114BF698D_339; }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 * get_address_of_F44B79D8208EDC17F9C96AA7DEA4983114BF698D_339() { return &___F44B79D8208EDC17F9C96AA7DEA4983114BF698D_339; }
	inline void set_F44B79D8208EDC17F9C96AA7DEA4983114BF698D_339(__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  value)
	{
		___F44B79D8208EDC17F9C96AA7DEA4983114BF698D_339 = value;
	}

	inline static int32_t get_offset_of_F5183037A4B449D12439C624D76065B310A6614A_340() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F5183037A4B449D12439C624D76065B310A6614A_340)); }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  get_F5183037A4B449D12439C624D76065B310A6614A_340() const { return ___F5183037A4B449D12439C624D76065B310A6614A_340; }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54 * get_address_of_F5183037A4B449D12439C624D76065B310A6614A_340() { return &___F5183037A4B449D12439C624D76065B310A6614A_340; }
	inline void set_F5183037A4B449D12439C624D76065B310A6614A_340(__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  value)
	{
		___F5183037A4B449D12439C624D76065B310A6614A_340 = value;
	}

	inline static int32_t get_offset_of_F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_341() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_341)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_341() const { return ___F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_341; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_341() { return &___F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_341; }
	inline void set_F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_341(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_341 = value;
	}

	inline static int32_t get_offset_of_F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_342() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_342)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_342() const { return ___F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_342; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_342() { return &___F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_342; }
	inline void set_F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_342(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_342 = value;
	}

	inline static int32_t get_offset_of_F865498CC0010E6ACE166D01EEA92F0F3C87743D_343() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F865498CC0010E6ACE166D01EEA92F0F3C87743D_343)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_F865498CC0010E6ACE166D01EEA92F0F3C87743D_343() const { return ___F865498CC0010E6ACE166D01EEA92F0F3C87743D_343; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_F865498CC0010E6ACE166D01EEA92F0F3C87743D_343() { return &___F865498CC0010E6ACE166D01EEA92F0F3C87743D_343; }
	inline void set_F865498CC0010E6ACE166D01EEA92F0F3C87743D_343(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___F865498CC0010E6ACE166D01EEA92F0F3C87743D_343 = value;
	}

	inline static int32_t get_offset_of_F86A89C59555BE5241482CB0FD35624F05D127CD_344() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F86A89C59555BE5241482CB0FD35624F05D127CD_344)); }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  get_F86A89C59555BE5241482CB0FD35624F05D127CD_344() const { return ___F86A89C59555BE5241482CB0FD35624F05D127CD_344; }
	inline __StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C * get_address_of_F86A89C59555BE5241482CB0FD35624F05D127CD_344() { return &___F86A89C59555BE5241482CB0FD35624F05D127CD_344; }
	inline void set_F86A89C59555BE5241482CB0FD35624F05D127CD_344(__StaticArrayInitTypeSizeU3D32_t33CBB6B98D456285860AB1149E6E0B652C81F14C  value)
	{
		___F86A89C59555BE5241482CB0FD35624F05D127CD_344 = value;
	}

	inline static int32_t get_offset_of_F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_345() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_345)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_345() const { return ___F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_345; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_345() { return &___F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_345; }
	inline void set_F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_345(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_345 = value;
	}

	inline static int32_t get_offset_of_FA200C2489E9868CC920129D1E2D694172C2BA49_346() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___FA200C2489E9868CC920129D1E2D694172C2BA49_346)); }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  get_FA200C2489E9868CC920129D1E2D694172C2BA49_346() const { return ___FA200C2489E9868CC920129D1E2D694172C2BA49_346; }
	inline __StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12 * get_address_of_FA200C2489E9868CC920129D1E2D694172C2BA49_346() { return &___FA200C2489E9868CC920129D1E2D694172C2BA49_346; }
	inline void set_FA200C2489E9868CC920129D1E2D694172C2BA49_346(__StaticArrayInitTypeSizeU3D120_t3BE0D03C6A8780D8A2A25DA373AAEAD03844CA12  value)
	{
		___FA200C2489E9868CC920129D1E2D694172C2BA49_346 = value;
	}

	inline static int32_t get_offset_of_FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_347() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_347)); }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  get_FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_347() const { return ___FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_347; }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54 * get_address_of_FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_347() { return &___FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_347; }
	inline void set_FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_347(__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  value)
	{
		___FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_347 = value;
	}

	inline static int32_t get_offset_of_FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_348() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_348)); }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  get_FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_348() const { return ___FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_348; }
	inline __StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54 * get_address_of_FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_348() { return &___FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_348; }
	inline void set_FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_348(__StaticArrayInitTypeSizeU3D52_t94FED58B7B80A7E497773366DB2DD42193888C54  value)
	{
		___FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_348 = value;
	}

	inline static int32_t get_offset_of_FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_349() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_349)); }
	inline __StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260  get_FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_349() const { return ___FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_349; }
	inline __StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260 * get_address_of_FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_349() { return &___FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_349; }
	inline void set_FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_349(__StaticArrayInitTypeSizeU3D80_t2C4CCE0AD65A37C6C949742DB0FF117776478260  value)
	{
		___FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_349 = value;
	}

	inline static int32_t get_offset_of_FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_350() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_350)); }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  get_FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_350() const { return ___FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_350; }
	inline __StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 * get_address_of_FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_350() { return &___FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_350; }
	inline void set_FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_350(__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4  value)
	{
		___FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_350 = value;
	}

	inline static int32_t get_offset_of_FCDF24D1C633613549296EFD3DC9250D840CA996_351() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___FCDF24D1C633613549296EFD3DC9250D840CA996_351)); }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  get_FCDF24D1C633613549296EFD3DC9250D840CA996_351() const { return ___FCDF24D1C633613549296EFD3DC9250D840CA996_351; }
	inline __StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7 * get_address_of_FCDF24D1C633613549296EFD3DC9250D840CA996_351() { return &___FCDF24D1C633613549296EFD3DC9250D840CA996_351; }
	inline void set_FCDF24D1C633613549296EFD3DC9250D840CA996_351(__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7  value)
	{
		___FCDF24D1C633613549296EFD3DC9250D840CA996_351 = value;
	}

	inline static int32_t get_offset_of_FCF9CE737F38E152A767F1291260BF9E15F80078_352() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___FCF9CE737F38E152A767F1291260BF9E15F80078_352)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_FCF9CE737F38E152A767F1291260BF9E15F80078_352() const { return ___FCF9CE737F38E152A767F1291260BF9E15F80078_352; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_FCF9CE737F38E152A767F1291260BF9E15F80078_352() { return &___FCF9CE737F38E152A767F1291260BF9E15F80078_352; }
	inline void set_FCF9CE737F38E152A767F1291260BF9E15F80078_352(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___FCF9CE737F38E152A767F1291260BF9E15F80078_352 = value;
	}

	inline static int32_t get_offset_of_FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_353() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_353)); }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  get_FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_353() const { return ___FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_353; }
	inline __StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 * get_address_of_FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_353() { return &___FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_353; }
	inline void set_FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_353(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0  value)
	{
		___FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_353 = value;
	}

	inline static int32_t get_offset_of_FE68796AD0908776C9F23F326A4181C1F53CF4DF_354() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields, ___FE68796AD0908776C9F23F326A4181C1F53CF4DF_354)); }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  get_FE68796AD0908776C9F23F326A4181C1F53CF4DF_354() const { return ___FE68796AD0908776C9F23F326A4181C1F53CF4DF_354; }
	inline __StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 * get_address_of_FE68796AD0908776C9F23F326A4181C1F53CF4DF_354() { return &___FE68796AD0908776C9F23F326A4181C1F53CF4DF_354; }
	inline void set_FE68796AD0908776C9F23F326A4181C1F53CF4DF_354(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436  value)
	{
		___FE68796AD0908776C9F23F326A4181C1F53CF4DF_354 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_H
#ifndef TABLE_TC89A3020A8D420688E32AE449F26893B3E6DBEE3_H
#define TABLE_TC89A3020A8D420688E32AE449F26893B3E6DBEE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.Internal.Decoder/Table
struct  Table_tC89A3020A8D420688E32AE449F26893B3E6DBEE3 
{
public:
	// System.Int32 ZXing.Aztec.Internal.Decoder/Table::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Table_tC89A3020A8D420688E32AE449F26893B3E6DBEE3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLE_TC89A3020A8D420688E32AE449F26893B3E6DBEE3_H
#ifndef HYBRIDBINARIZER_TB2BCFE24952947F400B6375EEA7BE71B273024F3_H
#define HYBRIDBINARIZER_TB2BCFE24952947F400B6375EEA7BE71B273024F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.HybridBinarizer
struct  HybridBinarizer_tB2BCFE24952947F400B6375EEA7BE71B273024F3  : public GlobalHistogramBinarizer_t757A4C6316A8E4C0F07D36369F4D8CA17D8FF760
{
public:
	// ZXing.Common.BitMatrix ZXing.Common.HybridBinarizer::matrix
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * ___matrix_4;

public:
	inline static int32_t get_offset_of_matrix_4() { return static_cast<int32_t>(offsetof(HybridBinarizer_tB2BCFE24952947F400B6375EEA7BE71B273024F3, ___matrix_4)); }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * get_matrix_4() const { return ___matrix_4; }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 ** get_address_of_matrix_4() { return &___matrix_4; }
	inline void set_matrix_4(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * value)
	{
		___matrix_4 = value;
		Il2CppCodeGenWriteBarrier((&___matrix_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HYBRIDBINARIZER_TB2BCFE24952947F400B6375EEA7BE71B273024F3_H
#ifndef MODE_T9EF908AA02A10A3CDF91101AB27BF7A882BD641D_H
#define MODE_T9EF908AA02A10A3CDF91101AB27BF7A882BD641D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.DecodedBitStreamParser/Mode
struct  Mode_t9EF908AA02A10A3CDF91101AB27BF7A882BD641D 
{
public:
	// System.Int32 ZXing.Datamatrix.Internal.DecodedBitStreamParser/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t9EF908AA02A10A3CDF91101AB27BF7A882BD641D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T9EF908AA02A10A3CDF91101AB27BF7A882BD641D_H
#ifndef EAN13READER_T62395C520DAC9056ADF103C596CDA24B92804858_H
#define EAN13READER_T62395C520DAC9056ADF103C596CDA24B92804858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.EAN13Reader
struct  EAN13Reader_t62395C520DAC9056ADF103C596CDA24B92804858  : public UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D
{
public:
	// System.Int32[] ZXing.OneD.EAN13Reader::decodeMiddleCounters
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___decodeMiddleCounters_13;

public:
	inline static int32_t get_offset_of_decodeMiddleCounters_13() { return static_cast<int32_t>(offsetof(EAN13Reader_t62395C520DAC9056ADF103C596CDA24B92804858, ___decodeMiddleCounters_13)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_decodeMiddleCounters_13() const { return ___decodeMiddleCounters_13; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_decodeMiddleCounters_13() { return &___decodeMiddleCounters_13; }
	inline void set_decodeMiddleCounters_13(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___decodeMiddleCounters_13 = value;
		Il2CppCodeGenWriteBarrier((&___decodeMiddleCounters_13), value);
	}
};

struct EAN13Reader_t62395C520DAC9056ADF103C596CDA24B92804858_StaticFields
{
public:
	// System.Int32[] ZXing.OneD.EAN13Reader::FIRST_DIGIT_ENCODINGS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___FIRST_DIGIT_ENCODINGS_12;

public:
	inline static int32_t get_offset_of_FIRST_DIGIT_ENCODINGS_12() { return static_cast<int32_t>(offsetof(EAN13Reader_t62395C520DAC9056ADF103C596CDA24B92804858_StaticFields, ___FIRST_DIGIT_ENCODINGS_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_FIRST_DIGIT_ENCODINGS_12() const { return ___FIRST_DIGIT_ENCODINGS_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_FIRST_DIGIT_ENCODINGS_12() { return &___FIRST_DIGIT_ENCODINGS_12; }
	inline void set_FIRST_DIGIT_ENCODINGS_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___FIRST_DIGIT_ENCODINGS_12 = value;
		Il2CppCodeGenWriteBarrier((&___FIRST_DIGIT_ENCODINGS_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAN13READER_T62395C520DAC9056ADF103C596CDA24B92804858_H
#ifndef EAN8READER_T91FADB7F9595ED7C24E652851BC9A0D1A43D795D_H
#define EAN8READER_T91FADB7F9595ED7C24E652851BC9A0D1A43D795D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.EAN8Reader
struct  EAN8Reader_t91FADB7F9595ED7C24E652851BC9A0D1A43D795D  : public UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D
{
public:
	// System.Int32[] ZXing.OneD.EAN8Reader::decodeMiddleCounters
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___decodeMiddleCounters_12;

public:
	inline static int32_t get_offset_of_decodeMiddleCounters_12() { return static_cast<int32_t>(offsetof(EAN8Reader_t91FADB7F9595ED7C24E652851BC9A0D1A43D795D, ___decodeMiddleCounters_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_decodeMiddleCounters_12() const { return ___decodeMiddleCounters_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_decodeMiddleCounters_12() { return &___decodeMiddleCounters_12; }
	inline void set_decodeMiddleCounters_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___decodeMiddleCounters_12 = value;
		Il2CppCodeGenWriteBarrier((&___decodeMiddleCounters_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAN8READER_T91FADB7F9595ED7C24E652851BC9A0D1A43D795D_H
#ifndef AI01392XDECODER_TD39960DBE83D8E165C0512B57F594CF086D96700_H
#define AI01392XDECODER_TD39960DBE83D8E165C0512B57F594CF086D96700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AI01392xDecoder
struct  AI01392xDecoder_tD39960DBE83D8E165C0512B57F594CF086D96700  : public AI01decoder_tE988CB5C4AD990EB7AFBBBFD88EE931EE4BC153E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AI01392XDECODER_TD39960DBE83D8E165C0512B57F594CF086D96700_H
#ifndef AI01393XDECODER_TFADDE2973DA11F60E74D87D3B4E0A0BBB21F0AD6_H
#define AI01393XDECODER_TFADDE2973DA11F60E74D87D3B4E0A0BBB21F0AD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder
struct  AI01393xDecoder_tFADDE2973DA11F60E74D87D3B4E0A0BBB21F0AD6  : public AI01decoder_tE988CB5C4AD990EB7AFBBBFD88EE931EE4BC153E
{
public:

public:
};

struct AI01393xDecoder_tFADDE2973DA11F60E74D87D3B4E0A0BBB21F0AD6_StaticFields
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder::HEADER_SIZE
	int32_t ___HEADER_SIZE_3;
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder::LAST_DIGIT_SIZE
	int32_t ___LAST_DIGIT_SIZE_4;
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder::FIRST_THREE_DIGITS_SIZE
	int32_t ___FIRST_THREE_DIGITS_SIZE_5;

public:
	inline static int32_t get_offset_of_HEADER_SIZE_3() { return static_cast<int32_t>(offsetof(AI01393xDecoder_tFADDE2973DA11F60E74D87D3B4E0A0BBB21F0AD6_StaticFields, ___HEADER_SIZE_3)); }
	inline int32_t get_HEADER_SIZE_3() const { return ___HEADER_SIZE_3; }
	inline int32_t* get_address_of_HEADER_SIZE_3() { return &___HEADER_SIZE_3; }
	inline void set_HEADER_SIZE_3(int32_t value)
	{
		___HEADER_SIZE_3 = value;
	}

	inline static int32_t get_offset_of_LAST_DIGIT_SIZE_4() { return static_cast<int32_t>(offsetof(AI01393xDecoder_tFADDE2973DA11F60E74D87D3B4E0A0BBB21F0AD6_StaticFields, ___LAST_DIGIT_SIZE_4)); }
	inline int32_t get_LAST_DIGIT_SIZE_4() const { return ___LAST_DIGIT_SIZE_4; }
	inline int32_t* get_address_of_LAST_DIGIT_SIZE_4() { return &___LAST_DIGIT_SIZE_4; }
	inline void set_LAST_DIGIT_SIZE_4(int32_t value)
	{
		___LAST_DIGIT_SIZE_4 = value;
	}

	inline static int32_t get_offset_of_FIRST_THREE_DIGITS_SIZE_5() { return static_cast<int32_t>(offsetof(AI01393xDecoder_tFADDE2973DA11F60E74D87D3B4E0A0BBB21F0AD6_StaticFields, ___FIRST_THREE_DIGITS_SIZE_5)); }
	inline int32_t get_FIRST_THREE_DIGITS_SIZE_5() const { return ___FIRST_THREE_DIGITS_SIZE_5; }
	inline int32_t* get_address_of_FIRST_THREE_DIGITS_SIZE_5() { return &___FIRST_THREE_DIGITS_SIZE_5; }
	inline void set_FIRST_THREE_DIGITS_SIZE_5(int32_t value)
	{
		___FIRST_THREE_DIGITS_SIZE_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AI01393XDECODER_TFADDE2973DA11F60E74D87D3B4E0A0BBB21F0AD6_H
#ifndef AI01ANDOTHERAIS_TF8B2B7D8A34248014D14C54D12FA284BBEF4A660_H
#define AI01ANDOTHERAIS_TF8B2B7D8A34248014D14C54D12FA284BBEF4A660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AI01AndOtherAIs
struct  AI01AndOtherAIs_tF8B2B7D8A34248014D14C54D12FA284BBEF4A660  : public AI01decoder_tE988CB5C4AD990EB7AFBBBFD88EE931EE4BC153E
{
public:

public:
};

struct AI01AndOtherAIs_tF8B2B7D8A34248014D14C54D12FA284BBEF4A660_StaticFields
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI01AndOtherAIs::HEADER_SIZE
	int32_t ___HEADER_SIZE_3;

public:
	inline static int32_t get_offset_of_HEADER_SIZE_3() { return static_cast<int32_t>(offsetof(AI01AndOtherAIs_tF8B2B7D8A34248014D14C54D12FA284BBEF4A660_StaticFields, ___HEADER_SIZE_3)); }
	inline int32_t get_HEADER_SIZE_3() const { return ___HEADER_SIZE_3; }
	inline int32_t* get_address_of_HEADER_SIZE_3() { return &___HEADER_SIZE_3; }
	inline void set_HEADER_SIZE_3(int32_t value)
	{
		___HEADER_SIZE_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AI01ANDOTHERAIS_TF8B2B7D8A34248014D14C54D12FA284BBEF4A660_H
#ifndef AI01WEIGHTDECODER_T78BD30151A14BFC2DAD1C8B0607235384BA0F665_H
#define AI01WEIGHTDECODER_T78BD30151A14BFC2DAD1C8B0607235384BA0F665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AI01weightDecoder
struct  AI01weightDecoder_t78BD30151A14BFC2DAD1C8B0607235384BA0F665  : public AI01decoder_tE988CB5C4AD990EB7AFBBBFD88EE931EE4BC153E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AI01WEIGHTDECODER_T78BD30151A14BFC2DAD1C8B0607235384BA0F665_H
#ifndef STATE_TC50F546A8F03FC3887346A1F6959F542B48796F5_H
#define STATE_TC50F546A8F03FC3887346A1F6959F542B48796F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState/State
struct  State_tC50F546A8F03FC3887346A1F6959F542B48796F5 
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_tC50F546A8F03FC3887346A1F6959F542B48796F5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_TC50F546A8F03FC3887346A1F6959F542B48796F5_H
#ifndef RSSEXPANDEDREADER_T8A39337BB3828379B173E2FA97C70E01E7B3782A_H
#define RSSEXPANDEDREADER_T8A39337BB3828379B173E2FA97C70E01E7B3782A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.RSSExpandedReader
struct  RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A  : public AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C
{
public:
	// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair> ZXing.OneD.RSS.Expanded.RSSExpandedReader::pairs
	List_1_t8968621E959ACCB19941483BDCBBB6443FC5E673 * ___pairs_16;
	// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedRow> ZXing.OneD.RSS.Expanded.RSSExpandedReader::rows
	List_1_t578F33CA35163DC0E0EA696C27D917CDB7460461 * ___rows_17;
	// System.Int32[] ZXing.OneD.RSS.Expanded.RSSExpandedReader::startEnd
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___startEnd_18;
	// System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::startFromEven
	bool ___startFromEven_19;

public:
	inline static int32_t get_offset_of_pairs_16() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A, ___pairs_16)); }
	inline List_1_t8968621E959ACCB19941483BDCBBB6443FC5E673 * get_pairs_16() const { return ___pairs_16; }
	inline List_1_t8968621E959ACCB19941483BDCBBB6443FC5E673 ** get_address_of_pairs_16() { return &___pairs_16; }
	inline void set_pairs_16(List_1_t8968621E959ACCB19941483BDCBBB6443FC5E673 * value)
	{
		___pairs_16 = value;
		Il2CppCodeGenWriteBarrier((&___pairs_16), value);
	}

	inline static int32_t get_offset_of_rows_17() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A, ___rows_17)); }
	inline List_1_t578F33CA35163DC0E0EA696C27D917CDB7460461 * get_rows_17() const { return ___rows_17; }
	inline List_1_t578F33CA35163DC0E0EA696C27D917CDB7460461 ** get_address_of_rows_17() { return &___rows_17; }
	inline void set_rows_17(List_1_t578F33CA35163DC0E0EA696C27D917CDB7460461 * value)
	{
		___rows_17 = value;
		Il2CppCodeGenWriteBarrier((&___rows_17), value);
	}

	inline static int32_t get_offset_of_startEnd_18() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A, ___startEnd_18)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_startEnd_18() const { return ___startEnd_18; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_startEnd_18() { return &___startEnd_18; }
	inline void set_startEnd_18(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___startEnd_18 = value;
		Il2CppCodeGenWriteBarrier((&___startEnd_18), value);
	}

	inline static int32_t get_offset_of_startFromEven_19() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A, ___startFromEven_19)); }
	inline bool get_startFromEven_19() const { return ___startFromEven_19; }
	inline bool* get_address_of_startFromEven_19() { return &___startFromEven_19; }
	inline void set_startFromEven_19(bool value)
	{
		___startFromEven_19 = value;
	}
};

struct RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A_StaticFields
{
public:
	// System.Int32[] ZXing.OneD.RSS.Expanded.RSSExpandedReader::SYMBOL_WIDEST
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___SYMBOL_WIDEST_10;
	// System.Int32[] ZXing.OneD.RSS.Expanded.RSSExpandedReader::EVEN_TOTAL_SUBSET
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___EVEN_TOTAL_SUBSET_11;
	// System.Int32[] ZXing.OneD.RSS.Expanded.RSSExpandedReader::GSUM
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___GSUM_12;
	// System.Int32[][] ZXing.OneD.RSS.Expanded.RSSExpandedReader::FINDER_PATTERNS
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___FINDER_PATTERNS_13;
	// System.Int32[][] ZXing.OneD.RSS.Expanded.RSSExpandedReader::WEIGHTS
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___WEIGHTS_14;
	// System.Int32[][] ZXing.OneD.RSS.Expanded.RSSExpandedReader::FINDER_PATTERN_SEQUENCES
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___FINDER_PATTERN_SEQUENCES_15;

public:
	inline static int32_t get_offset_of_SYMBOL_WIDEST_10() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A_StaticFields, ___SYMBOL_WIDEST_10)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_SYMBOL_WIDEST_10() const { return ___SYMBOL_WIDEST_10; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_SYMBOL_WIDEST_10() { return &___SYMBOL_WIDEST_10; }
	inline void set_SYMBOL_WIDEST_10(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___SYMBOL_WIDEST_10 = value;
		Il2CppCodeGenWriteBarrier((&___SYMBOL_WIDEST_10), value);
	}

	inline static int32_t get_offset_of_EVEN_TOTAL_SUBSET_11() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A_StaticFields, ___EVEN_TOTAL_SUBSET_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_EVEN_TOTAL_SUBSET_11() const { return ___EVEN_TOTAL_SUBSET_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_EVEN_TOTAL_SUBSET_11() { return &___EVEN_TOTAL_SUBSET_11; }
	inline void set_EVEN_TOTAL_SUBSET_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___EVEN_TOTAL_SUBSET_11 = value;
		Il2CppCodeGenWriteBarrier((&___EVEN_TOTAL_SUBSET_11), value);
	}

	inline static int32_t get_offset_of_GSUM_12() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A_StaticFields, ___GSUM_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_GSUM_12() const { return ___GSUM_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_GSUM_12() { return &___GSUM_12; }
	inline void set_GSUM_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___GSUM_12 = value;
		Il2CppCodeGenWriteBarrier((&___GSUM_12), value);
	}

	inline static int32_t get_offset_of_FINDER_PATTERNS_13() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A_StaticFields, ___FINDER_PATTERNS_13)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_FINDER_PATTERNS_13() const { return ___FINDER_PATTERNS_13; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_FINDER_PATTERNS_13() { return &___FINDER_PATTERNS_13; }
	inline void set_FINDER_PATTERNS_13(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___FINDER_PATTERNS_13 = value;
		Il2CppCodeGenWriteBarrier((&___FINDER_PATTERNS_13), value);
	}

	inline static int32_t get_offset_of_WEIGHTS_14() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A_StaticFields, ___WEIGHTS_14)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_WEIGHTS_14() const { return ___WEIGHTS_14; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_WEIGHTS_14() { return &___WEIGHTS_14; }
	inline void set_WEIGHTS_14(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___WEIGHTS_14 = value;
		Il2CppCodeGenWriteBarrier((&___WEIGHTS_14), value);
	}

	inline static int32_t get_offset_of_FINDER_PATTERN_SEQUENCES_15() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A_StaticFields, ___FINDER_PATTERN_SEQUENCES_15)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_FINDER_PATTERN_SEQUENCES_15() const { return ___FINDER_PATTERN_SEQUENCES_15; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_FINDER_PATTERN_SEQUENCES_15() { return &___FINDER_PATTERN_SEQUENCES_15; }
	inline void set_FINDER_PATTERN_SEQUENCES_15(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___FINDER_PATTERN_SEQUENCES_15 = value;
		Il2CppCodeGenWriteBarrier((&___FINDER_PATTERN_SEQUENCES_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSSEXPANDEDREADER_T8A39337BB3828379B173E2FA97C70E01E7B3782A_H
#ifndef RSS14READER_T9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_H
#define RSS14READER_T9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.RSS14Reader
struct  RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9  : public AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C
{
public:
	// System.Collections.Generic.List`1<ZXing.OneD.RSS.Pair> ZXing.OneD.RSS.RSS14Reader::possibleLeftPairs
	List_1_tC9492C021DF8F731EE582F7E256743193B7391E2 * ___possibleLeftPairs_17;
	// System.Collections.Generic.List`1<ZXing.OneD.RSS.Pair> ZXing.OneD.RSS.RSS14Reader::possibleRightPairs
	List_1_tC9492C021DF8F731EE582F7E256743193B7391E2 * ___possibleRightPairs_18;

public:
	inline static int32_t get_offset_of_possibleLeftPairs_17() { return static_cast<int32_t>(offsetof(RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9, ___possibleLeftPairs_17)); }
	inline List_1_tC9492C021DF8F731EE582F7E256743193B7391E2 * get_possibleLeftPairs_17() const { return ___possibleLeftPairs_17; }
	inline List_1_tC9492C021DF8F731EE582F7E256743193B7391E2 ** get_address_of_possibleLeftPairs_17() { return &___possibleLeftPairs_17; }
	inline void set_possibleLeftPairs_17(List_1_tC9492C021DF8F731EE582F7E256743193B7391E2 * value)
	{
		___possibleLeftPairs_17 = value;
		Il2CppCodeGenWriteBarrier((&___possibleLeftPairs_17), value);
	}

	inline static int32_t get_offset_of_possibleRightPairs_18() { return static_cast<int32_t>(offsetof(RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9, ___possibleRightPairs_18)); }
	inline List_1_tC9492C021DF8F731EE582F7E256743193B7391E2 * get_possibleRightPairs_18() const { return ___possibleRightPairs_18; }
	inline List_1_tC9492C021DF8F731EE582F7E256743193B7391E2 ** get_address_of_possibleRightPairs_18() { return &___possibleRightPairs_18; }
	inline void set_possibleRightPairs_18(List_1_tC9492C021DF8F731EE582F7E256743193B7391E2 * value)
	{
		___possibleRightPairs_18 = value;
		Il2CppCodeGenWriteBarrier((&___possibleRightPairs_18), value);
	}
};

struct RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields
{
public:
	// System.Int32[] ZXing.OneD.RSS.RSS14Reader::OUTSIDE_EVEN_TOTAL_SUBSET
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___OUTSIDE_EVEN_TOTAL_SUBSET_10;
	// System.Int32[] ZXing.OneD.RSS.RSS14Reader::INSIDE_ODD_TOTAL_SUBSET
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___INSIDE_ODD_TOTAL_SUBSET_11;
	// System.Int32[] ZXing.OneD.RSS.RSS14Reader::OUTSIDE_GSUM
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___OUTSIDE_GSUM_12;
	// System.Int32[] ZXing.OneD.RSS.RSS14Reader::INSIDE_GSUM
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___INSIDE_GSUM_13;
	// System.Int32[] ZXing.OneD.RSS.RSS14Reader::OUTSIDE_ODD_WIDEST
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___OUTSIDE_ODD_WIDEST_14;
	// System.Int32[] ZXing.OneD.RSS.RSS14Reader::INSIDE_ODD_WIDEST
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___INSIDE_ODD_WIDEST_15;
	// System.Int32[][] ZXing.OneD.RSS.RSS14Reader::FINDER_PATTERNS
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___FINDER_PATTERNS_16;

public:
	inline static int32_t get_offset_of_OUTSIDE_EVEN_TOTAL_SUBSET_10() { return static_cast<int32_t>(offsetof(RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields, ___OUTSIDE_EVEN_TOTAL_SUBSET_10)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_OUTSIDE_EVEN_TOTAL_SUBSET_10() const { return ___OUTSIDE_EVEN_TOTAL_SUBSET_10; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_OUTSIDE_EVEN_TOTAL_SUBSET_10() { return &___OUTSIDE_EVEN_TOTAL_SUBSET_10; }
	inline void set_OUTSIDE_EVEN_TOTAL_SUBSET_10(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___OUTSIDE_EVEN_TOTAL_SUBSET_10 = value;
		Il2CppCodeGenWriteBarrier((&___OUTSIDE_EVEN_TOTAL_SUBSET_10), value);
	}

	inline static int32_t get_offset_of_INSIDE_ODD_TOTAL_SUBSET_11() { return static_cast<int32_t>(offsetof(RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields, ___INSIDE_ODD_TOTAL_SUBSET_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_INSIDE_ODD_TOTAL_SUBSET_11() const { return ___INSIDE_ODD_TOTAL_SUBSET_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_INSIDE_ODD_TOTAL_SUBSET_11() { return &___INSIDE_ODD_TOTAL_SUBSET_11; }
	inline void set_INSIDE_ODD_TOTAL_SUBSET_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___INSIDE_ODD_TOTAL_SUBSET_11 = value;
		Il2CppCodeGenWriteBarrier((&___INSIDE_ODD_TOTAL_SUBSET_11), value);
	}

	inline static int32_t get_offset_of_OUTSIDE_GSUM_12() { return static_cast<int32_t>(offsetof(RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields, ___OUTSIDE_GSUM_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_OUTSIDE_GSUM_12() const { return ___OUTSIDE_GSUM_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_OUTSIDE_GSUM_12() { return &___OUTSIDE_GSUM_12; }
	inline void set_OUTSIDE_GSUM_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___OUTSIDE_GSUM_12 = value;
		Il2CppCodeGenWriteBarrier((&___OUTSIDE_GSUM_12), value);
	}

	inline static int32_t get_offset_of_INSIDE_GSUM_13() { return static_cast<int32_t>(offsetof(RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields, ___INSIDE_GSUM_13)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_INSIDE_GSUM_13() const { return ___INSIDE_GSUM_13; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_INSIDE_GSUM_13() { return &___INSIDE_GSUM_13; }
	inline void set_INSIDE_GSUM_13(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___INSIDE_GSUM_13 = value;
		Il2CppCodeGenWriteBarrier((&___INSIDE_GSUM_13), value);
	}

	inline static int32_t get_offset_of_OUTSIDE_ODD_WIDEST_14() { return static_cast<int32_t>(offsetof(RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields, ___OUTSIDE_ODD_WIDEST_14)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_OUTSIDE_ODD_WIDEST_14() const { return ___OUTSIDE_ODD_WIDEST_14; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_OUTSIDE_ODD_WIDEST_14() { return &___OUTSIDE_ODD_WIDEST_14; }
	inline void set_OUTSIDE_ODD_WIDEST_14(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___OUTSIDE_ODD_WIDEST_14 = value;
		Il2CppCodeGenWriteBarrier((&___OUTSIDE_ODD_WIDEST_14), value);
	}

	inline static int32_t get_offset_of_INSIDE_ODD_WIDEST_15() { return static_cast<int32_t>(offsetof(RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields, ___INSIDE_ODD_WIDEST_15)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_INSIDE_ODD_WIDEST_15() const { return ___INSIDE_ODD_WIDEST_15; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_INSIDE_ODD_WIDEST_15() { return &___INSIDE_ODD_WIDEST_15; }
	inline void set_INSIDE_ODD_WIDEST_15(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___INSIDE_ODD_WIDEST_15 = value;
		Il2CppCodeGenWriteBarrier((&___INSIDE_ODD_WIDEST_15), value);
	}

	inline static int32_t get_offset_of_FINDER_PATTERNS_16() { return static_cast<int32_t>(offsetof(RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields, ___FINDER_PATTERNS_16)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_FINDER_PATTERNS_16() const { return ___FINDER_PATTERNS_16; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_FINDER_PATTERNS_16() { return &___FINDER_PATTERNS_16; }
	inline void set_FINDER_PATTERNS_16(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___FINDER_PATTERNS_16 = value;
		Il2CppCodeGenWriteBarrier((&___FINDER_PATTERNS_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSS14READER_T9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_H
#ifndef UPCAREADER_TE9593DB6BB56128186774737605C817BD6D5EE15_H
#define UPCAREADER_TE9593DB6BB56128186774737605C817BD6D5EE15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.UPCAReader
struct  UPCAReader_tE9593DB6BB56128186774737605C817BD6D5EE15  : public UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D
{
public:
	// ZXing.OneD.UPCEANReader ZXing.OneD.UPCAReader::ean13Reader
	UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D * ___ean13Reader_12;

public:
	inline static int32_t get_offset_of_ean13Reader_12() { return static_cast<int32_t>(offsetof(UPCAReader_tE9593DB6BB56128186774737605C817BD6D5EE15, ___ean13Reader_12)); }
	inline UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D * get_ean13Reader_12() const { return ___ean13Reader_12; }
	inline UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D ** get_address_of_ean13Reader_12() { return &___ean13Reader_12; }
	inline void set_ean13Reader_12(UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D * value)
	{
		___ean13Reader_12 = value;
		Il2CppCodeGenWriteBarrier((&___ean13Reader_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPCAREADER_TE9593DB6BB56128186774737605C817BD6D5EE15_H
#ifndef UPCEREADER_T848B368283DE3FDE72978E8F274CAE13077A7E5A_H
#define UPCEREADER_T848B368283DE3FDE72978E8F274CAE13077A7E5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.UPCEReader
struct  UPCEReader_t848B368283DE3FDE72978E8F274CAE13077A7E5A  : public UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D
{
public:
	// System.Int32[] ZXing.OneD.UPCEReader::decodeMiddleCounters
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___decodeMiddleCounters_14;

public:
	inline static int32_t get_offset_of_decodeMiddleCounters_14() { return static_cast<int32_t>(offsetof(UPCEReader_t848B368283DE3FDE72978E8F274CAE13077A7E5A, ___decodeMiddleCounters_14)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_decodeMiddleCounters_14() const { return ___decodeMiddleCounters_14; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_decodeMiddleCounters_14() { return &___decodeMiddleCounters_14; }
	inline void set_decodeMiddleCounters_14(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___decodeMiddleCounters_14 = value;
		Il2CppCodeGenWriteBarrier((&___decodeMiddleCounters_14), value);
	}
};

struct UPCEReader_t848B368283DE3FDE72978E8F274CAE13077A7E5A_StaticFields
{
public:
	// System.Int32[] ZXing.OneD.UPCEReader::MIDDLE_END_PATTERN
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___MIDDLE_END_PATTERN_12;
	// System.Int32[][] ZXing.OneD.UPCEReader::NUMSYS_AND_CHECK_DIGIT_PATTERNS
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___NUMSYS_AND_CHECK_DIGIT_PATTERNS_13;

public:
	inline static int32_t get_offset_of_MIDDLE_END_PATTERN_12() { return static_cast<int32_t>(offsetof(UPCEReader_t848B368283DE3FDE72978E8F274CAE13077A7E5A_StaticFields, ___MIDDLE_END_PATTERN_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_MIDDLE_END_PATTERN_12() const { return ___MIDDLE_END_PATTERN_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_MIDDLE_END_PATTERN_12() { return &___MIDDLE_END_PATTERN_12; }
	inline void set_MIDDLE_END_PATTERN_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___MIDDLE_END_PATTERN_12 = value;
		Il2CppCodeGenWriteBarrier((&___MIDDLE_END_PATTERN_12), value);
	}

	inline static int32_t get_offset_of_NUMSYS_AND_CHECK_DIGIT_PATTERNS_13() { return static_cast<int32_t>(offsetof(UPCEReader_t848B368283DE3FDE72978E8F274CAE13077A7E5A_StaticFields, ___NUMSYS_AND_CHECK_DIGIT_PATTERNS_13)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_NUMSYS_AND_CHECK_DIGIT_PATTERNS_13() const { return ___NUMSYS_AND_CHECK_DIGIT_PATTERNS_13; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_NUMSYS_AND_CHECK_DIGIT_PATTERNS_13() { return &___NUMSYS_AND_CHECK_DIGIT_PATTERNS_13; }
	inline void set_NUMSYS_AND_CHECK_DIGIT_PATTERNS_13(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___NUMSYS_AND_CHECK_DIGIT_PATTERNS_13 = value;
		Il2CppCodeGenWriteBarrier((&___NUMSYS_AND_CHECK_DIGIT_PATTERNS_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPCEREADER_T848B368283DE3FDE72978E8F274CAE13077A7E5A_H
#ifndef AI013X0X1XDECODER_T7315E408DCB93F9C6F0324428A61A4CCC25A8520_H
#define AI013X0X1XDECODER_T7315E408DCB93F9C6F0324428A61A4CCC25A8520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder
struct  AI013x0x1xDecoder_t7315E408DCB93F9C6F0324428A61A4CCC25A8520  : public AI01weightDecoder_t78BD30151A14BFC2DAD1C8B0607235384BA0F665
{
public:
	// System.String ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::dateCode
	String_t* ___dateCode_6;
	// System.String ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::firstAIdigits
	String_t* ___firstAIdigits_7;

public:
	inline static int32_t get_offset_of_dateCode_6() { return static_cast<int32_t>(offsetof(AI013x0x1xDecoder_t7315E408DCB93F9C6F0324428A61A4CCC25A8520, ___dateCode_6)); }
	inline String_t* get_dateCode_6() const { return ___dateCode_6; }
	inline String_t** get_address_of_dateCode_6() { return &___dateCode_6; }
	inline void set_dateCode_6(String_t* value)
	{
		___dateCode_6 = value;
		Il2CppCodeGenWriteBarrier((&___dateCode_6), value);
	}

	inline static int32_t get_offset_of_firstAIdigits_7() { return static_cast<int32_t>(offsetof(AI013x0x1xDecoder_t7315E408DCB93F9C6F0324428A61A4CCC25A8520, ___firstAIdigits_7)); }
	inline String_t* get_firstAIdigits_7() const { return ___firstAIdigits_7; }
	inline String_t** get_address_of_firstAIdigits_7() { return &___firstAIdigits_7; }
	inline void set_firstAIdigits_7(String_t* value)
	{
		___firstAIdigits_7 = value;
		Il2CppCodeGenWriteBarrier((&___firstAIdigits_7), value);
	}
};

struct AI013x0x1xDecoder_t7315E408DCB93F9C6F0324428A61A4CCC25A8520_StaticFields
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::HEADER_SIZE
	int32_t ___HEADER_SIZE_3;
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::WEIGHT_SIZE
	int32_t ___WEIGHT_SIZE_4;
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::DATE_SIZE
	int32_t ___DATE_SIZE_5;

public:
	inline static int32_t get_offset_of_HEADER_SIZE_3() { return static_cast<int32_t>(offsetof(AI013x0x1xDecoder_t7315E408DCB93F9C6F0324428A61A4CCC25A8520_StaticFields, ___HEADER_SIZE_3)); }
	inline int32_t get_HEADER_SIZE_3() const { return ___HEADER_SIZE_3; }
	inline int32_t* get_address_of_HEADER_SIZE_3() { return &___HEADER_SIZE_3; }
	inline void set_HEADER_SIZE_3(int32_t value)
	{
		___HEADER_SIZE_3 = value;
	}

	inline static int32_t get_offset_of_WEIGHT_SIZE_4() { return static_cast<int32_t>(offsetof(AI013x0x1xDecoder_t7315E408DCB93F9C6F0324428A61A4CCC25A8520_StaticFields, ___WEIGHT_SIZE_4)); }
	inline int32_t get_WEIGHT_SIZE_4() const { return ___WEIGHT_SIZE_4; }
	inline int32_t* get_address_of_WEIGHT_SIZE_4() { return &___WEIGHT_SIZE_4; }
	inline void set_WEIGHT_SIZE_4(int32_t value)
	{
		___WEIGHT_SIZE_4 = value;
	}

	inline static int32_t get_offset_of_DATE_SIZE_5() { return static_cast<int32_t>(offsetof(AI013x0x1xDecoder_t7315E408DCB93F9C6F0324428A61A4CCC25A8520_StaticFields, ___DATE_SIZE_5)); }
	inline int32_t get_DATE_SIZE_5() const { return ___DATE_SIZE_5; }
	inline int32_t* get_address_of_DATE_SIZE_5() { return &___DATE_SIZE_5; }
	inline void set_DATE_SIZE_5(int32_t value)
	{
		___DATE_SIZE_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AI013X0X1XDECODER_T7315E408DCB93F9C6F0324428A61A4CCC25A8520_H
#ifndef AI013X0XDECODER_T8B2B04E3609D9D1D29786C79BBD2D9DB1766ED3D_H
#define AI013X0XDECODER_T8B2B04E3609D9D1D29786C79BBD2D9DB1766ED3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder
struct  AI013x0xDecoder_t8B2B04E3609D9D1D29786C79BBD2D9DB1766ED3D  : public AI01weightDecoder_t78BD30151A14BFC2DAD1C8B0607235384BA0F665
{
public:

public:
};

struct AI013x0xDecoder_t8B2B04E3609D9D1D29786C79BBD2D9DB1766ED3D_StaticFields
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder::HEADER_SIZE
	int32_t ___HEADER_SIZE_3;
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder::WEIGHT_SIZE
	int32_t ___WEIGHT_SIZE_4;

public:
	inline static int32_t get_offset_of_HEADER_SIZE_3() { return static_cast<int32_t>(offsetof(AI013x0xDecoder_t8B2B04E3609D9D1D29786C79BBD2D9DB1766ED3D_StaticFields, ___HEADER_SIZE_3)); }
	inline int32_t get_HEADER_SIZE_3() const { return ___HEADER_SIZE_3; }
	inline int32_t* get_address_of_HEADER_SIZE_3() { return &___HEADER_SIZE_3; }
	inline void set_HEADER_SIZE_3(int32_t value)
	{
		___HEADER_SIZE_3 = value;
	}

	inline static int32_t get_offset_of_WEIGHT_SIZE_4() { return static_cast<int32_t>(offsetof(AI013x0xDecoder_t8B2B04E3609D9D1D29786C79BBD2D9DB1766ED3D_StaticFields, ___WEIGHT_SIZE_4)); }
	inline int32_t get_WEIGHT_SIZE_4() const { return ___WEIGHT_SIZE_4; }
	inline int32_t* get_address_of_WEIGHT_SIZE_4() { return &___WEIGHT_SIZE_4; }
	inline void set_WEIGHT_SIZE_4(int32_t value)
	{
		___WEIGHT_SIZE_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AI013X0XDECODER_T8B2B04E3609D9D1D29786C79BBD2D9DB1766ED3D_H
#ifndef CURRENTPARSINGSTATE_T69D94A0BACE62CD40F623625D2A86AF7EFF70BC1_H
#define CURRENTPARSINGSTATE_T69D94A0BACE62CD40F623625D2A86AF7EFF70BC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState
struct  CurrentParsingState_t69D94A0BACE62CD40F623625D2A86AF7EFF70BC1  : public RuntimeObject
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::position
	int32_t ___position_0;
	// ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState/State ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::encoding
	int32_t ___encoding_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(CurrentParsingState_t69D94A0BACE62CD40F623625D2A86AF7EFF70BC1, ___position_0)); }
	inline int32_t get_position_0() const { return ___position_0; }
	inline int32_t* get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(int32_t value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_encoding_1() { return static_cast<int32_t>(offsetof(CurrentParsingState_t69D94A0BACE62CD40F623625D2A86AF7EFF70BC1, ___encoding_1)); }
	inline int32_t get_encoding_1() const { return ___encoding_1; }
	inline int32_t* get_address_of_encoding_1() { return &___encoding_1; }
	inline void set_encoding_1(int32_t value)
	{
		___encoding_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTPARSINGSTATE_T69D94A0BACE62CD40F623625D2A86AF7EFF70BC1_H
#ifndef AI013103DECODER_T8D25C8201B46192370433D3BB441C2DBE1FE4A58_H
#define AI013103DECODER_T8D25C8201B46192370433D3BB441C2DBE1FE4A58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AI013103decoder
struct  AI013103decoder_t8D25C8201B46192370433D3BB441C2DBE1FE4A58  : public AI013x0xDecoder_t8B2B04E3609D9D1D29786C79BBD2D9DB1766ED3D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AI013103DECODER_T8D25C8201B46192370433D3BB441C2DBE1FE4A58_H
#ifndef AI01320XDECODER_T85C8202451E236034DBA9A4D324448F026784B32_H
#define AI01320XDECODER_T85C8202451E236034DBA9A4D324448F026784B32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AI01320xDecoder
struct  AI01320xDecoder_t85C8202451E236034DBA9A4D324448F026784B32  : public AI013x0xDecoder_t8B2B04E3609D9D1D29786C79BBD2D9DB1766ED3D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AI01320XDECODER_T85C8202451E236034DBA9A4D324448F026784B32_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6500 = { sizeof (Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74), -1, sizeof(Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6500[8] = 
{
	Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74_StaticFields::get_offset_of_ALPHABET_STRING_2(),
	Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74_StaticFields::get_offset_of_CHECK_DIGIT_STRING_3(),
	Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74_StaticFields::get_offset_of_CHARACTER_ENCODINGS_4(),
	Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74_StaticFields::get_offset_of_ASTERISK_ENCODING_5(),
	Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74::get_offset_of_usingCheckDigit_6(),
	Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74::get_offset_of_extendedMode_7(),
	Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74::get_offset_of_decodeRowResult_8(),
	Code39Reader_t43CEF4CCD406E4C3CF8D4AD0B9991FE2905ECF74::get_offset_of_counters_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6501 = { sizeof (Code93Reader_t07729DD387AC6113DEFCCF365713BA1F494C65CF), -1, sizeof(Code93Reader_t07729DD387AC6113DEFCCF365713BA1F494C65CF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6501[5] = 
{
	Code93Reader_t07729DD387AC6113DEFCCF365713BA1F494C65CF_StaticFields::get_offset_of_ALPHABET_2(),
	Code93Reader_t07729DD387AC6113DEFCCF365713BA1F494C65CF_StaticFields::get_offset_of_CHARACTER_ENCODINGS_3(),
	Code93Reader_t07729DD387AC6113DEFCCF365713BA1F494C65CF_StaticFields::get_offset_of_ASTERISK_ENCODING_4(),
	Code93Reader_t07729DD387AC6113DEFCCF365713BA1F494C65CF::get_offset_of_decodeRowResult_5(),
	Code93Reader_t07729DD387AC6113DEFCCF365713BA1F494C65CF::get_offset_of_counters_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6502 = { sizeof (EAN13Reader_t62395C520DAC9056ADF103C596CDA24B92804858), -1, sizeof(EAN13Reader_t62395C520DAC9056ADF103C596CDA24B92804858_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6502[2] = 
{
	EAN13Reader_t62395C520DAC9056ADF103C596CDA24B92804858_StaticFields::get_offset_of_FIRST_DIGIT_ENCODINGS_12(),
	EAN13Reader_t62395C520DAC9056ADF103C596CDA24B92804858::get_offset_of_decodeMiddleCounters_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6503 = { sizeof (EAN8Reader_t91FADB7F9595ED7C24E652851BC9A0D1A43D795D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6503[1] = 
{
	EAN8Reader_t91FADB7F9595ED7C24E652851BC9A0D1A43D795D::get_offset_of_decodeMiddleCounters_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6504 = { sizeof (EANManufacturerOrgSupport_t1BFBF3CDCC49219358CD56548F8FD730DD2C35A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6504[2] = 
{
	EANManufacturerOrgSupport_t1BFBF3CDCC49219358CD56548F8FD730DD2C35A7::get_offset_of_ranges_0(),
	EANManufacturerOrgSupport_t1BFBF3CDCC49219358CD56548F8FD730DD2C35A7::get_offset_of_countryIdentifiers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6505 = { sizeof (ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C), -1, sizeof(ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6505[7] = 
{
	ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_StaticFields::get_offset_of_MAX_AVG_VARIANCE_2(),
	ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_StaticFields::get_offset_of_MAX_INDIVIDUAL_VARIANCE_3(),
	ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_StaticFields::get_offset_of_DEFAULT_ALLOWED_LENGTHS_4(),
	ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C::get_offset_of_narrowLineWidth_5(),
	ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_StaticFields::get_offset_of_START_PATTERN_6(),
	ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_StaticFields::get_offset_of_END_PATTERN_REVERSED_7(),
	ITFReader_tF056CBB4BED9D66EE3A8A76DD1178CFE4655A91C_StaticFields::get_offset_of_PATTERNS_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6506 = { sizeof (MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5), -1, sizeof(MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6506[8] = 
{
	MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5_StaticFields::get_offset_of_ALPHABET_STRING_2(),
	MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5_StaticFields::get_offset_of_ALPHABET_3(),
	MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5_StaticFields::get_offset_of_CHARACTER_ENCODINGS_4(),
	MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5::get_offset_of_usingCheckDigit_5(),
	MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5::get_offset_of_decodeRowResult_6(),
	MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5::get_offset_of_counters_7(),
	MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5::get_offset_of_averageCounterWidth_8(),
	MSIReader_t382C7A07A633AF3D14C9512270EBBBBD2F9E8BC5_StaticFields::get_offset_of_doubleAndCrossSum_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6507 = { sizeof (MultiFormatOneDReader_t229E3DAC0D1C4281A4431E03A56CC773ED39D85D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6507[1] = 
{
	MultiFormatOneDReader_t229E3DAC0D1C4281A4431E03A56CC773ED39D85D::get_offset_of_readers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6508 = { sizeof (MultiFormatUPCEANReader_t8964C48B44FF2BF72C8F58DB64E22E9BAFACCCBA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6508[1] = 
{
	MultiFormatUPCEANReader_t8964C48B44FF2BF72C8F58DB64E22E9BAFACCCBA::get_offset_of_readers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6509 = { sizeof (OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC), -1, sizeof(OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6509[2] = 
{
	OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC_StaticFields::get_offset_of_INTEGER_MATH_SHIFT_0(),
	OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC_StaticFields::get_offset_of_PATTERN_MATCH_RESULT_SCALE_FACTOR_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6510 = { sizeof (UPCAReader_tE9593DB6BB56128186774737605C817BD6D5EE15), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6510[1] = 
{
	UPCAReader_tE9593DB6BB56128186774737605C817BD6D5EE15::get_offset_of_ean13Reader_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6511 = { sizeof (UPCEANExtension2Support_t1FAB0D3ED96CEF129DD7BECBF9DCD16C1EEEB7CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6511[2] = 
{
	UPCEANExtension2Support_t1FAB0D3ED96CEF129DD7BECBF9DCD16C1EEEB7CA::get_offset_of_decodeMiddleCounters_0(),
	UPCEANExtension2Support_t1FAB0D3ED96CEF129DD7BECBF9DCD16C1EEEB7CA::get_offset_of_decodeRowStringBuffer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6512 = { sizeof (UPCEANExtension5Support_tAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD), -1, sizeof(UPCEANExtension5Support_tAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6512[3] = 
{
	UPCEANExtension5Support_tAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD_StaticFields::get_offset_of_CHECK_DIGIT_ENCODINGS_0(),
	UPCEANExtension5Support_tAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD::get_offset_of_decodeMiddleCounters_1(),
	UPCEANExtension5Support_tAEEECFF13C4358BD58DEE3590C4CE3A23F2EDDFD::get_offset_of_decodeRowStringBuffer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6513 = { sizeof (UPCEANExtensionSupport_tCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B), -1, sizeof(UPCEANExtensionSupport_tCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6513[3] = 
{
	UPCEANExtensionSupport_tCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B_StaticFields::get_offset_of_EXTENSION_START_PATTERN_0(),
	UPCEANExtensionSupport_tCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B::get_offset_of_twoSupport_1(),
	UPCEANExtensionSupport_tCB4E45F71248F61B3FAACCBF011EF70B6AB60A6B::get_offset_of_fiveSupport_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6514 = { sizeof (UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D), -1, sizeof(UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6514[10] = 
{
	UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields::get_offset_of_MAX_AVG_VARIANCE_2(),
	UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields::get_offset_of_MAX_INDIVIDUAL_VARIANCE_3(),
	UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields::get_offset_of_START_END_PATTERN_4(),
	UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields::get_offset_of_MIDDLE_PATTERN_5(),
	UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields::get_offset_of_END_PATTERN_6(),
	UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields::get_offset_of_L_PATTERNS_7(),
	UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D_StaticFields::get_offset_of_L_AND_G_PATTERNS_8(),
	UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D::get_offset_of_decodeRowStringBuffer_9(),
	UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D::get_offset_of_extensionReader_10(),
	UPCEANReader_tCF0226E6411C30A8399CF5A7341C02236F096B6D::get_offset_of_eanManSupport_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6515 = { sizeof (UPCEReader_t848B368283DE3FDE72978E8F274CAE13077A7E5A), -1, sizeof(UPCEReader_t848B368283DE3FDE72978E8F274CAE13077A7E5A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6515[3] = 
{
	UPCEReader_t848B368283DE3FDE72978E8F274CAE13077A7E5A_StaticFields::get_offset_of_MIDDLE_END_PATTERN_12(),
	UPCEReader_t848B368283DE3FDE72978E8F274CAE13077A7E5A_StaticFields::get_offset_of_NUMSYS_AND_CHECK_DIGIT_PATTERNS_13(),
	UPCEReader_t848B368283DE3FDE72978E8F274CAE13077A7E5A::get_offset_of_decodeMiddleCounters_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6516 = { sizeof (AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C), -1, sizeof(AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6516[8] = 
{
	AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C_StaticFields::get_offset_of_MAX_AVG_VARIANCE_2(),
	AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C_StaticFields::get_offset_of_MAX_INDIVIDUAL_VARIANCE_3(),
	AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C::get_offset_of_decodeFinderCounters_4(),
	AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C::get_offset_of_dataCharacterCounters_5(),
	AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C::get_offset_of_oddRoundingErrors_6(),
	AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C::get_offset_of_evenRoundingErrors_7(),
	AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C::get_offset_of_oddCounts_8(),
	AbstractRSSReader_t05F5FA7968C3A85A6B9463741B94CE2A1285635C::get_offset_of_evenCounts_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6517 = { sizeof (DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6517[2] = 
{
	DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB::get_offset_of_U3CValueU3Ek__BackingField_0(),
	DataCharacter_t91EF692DAFCC9E507CF376A4710967C52ABD5EBB::get_offset_of_U3CChecksumPortionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6518 = { sizeof (FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6518[3] = 
{
	FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF::get_offset_of_U3CValueU3Ek__BackingField_0(),
	FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF::get_offset_of_U3CStartEndU3Ek__BackingField_1(),
	FinderPattern_t0C8016402A6CBE864FBD43910A4D7925570E3CAF::get_offset_of_U3CResultPointsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6519 = { sizeof (Pair_tB6F73D2714D821ED407BDA773D0B4A82803DB886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6519[2] = 
{
	Pair_tB6F73D2714D821ED407BDA773D0B4A82803DB886::get_offset_of_U3CFinderPatternU3Ek__BackingField_2(),
	Pair_tB6F73D2714D821ED407BDA773D0B4A82803DB886::get_offset_of_U3CCountU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6520 = { sizeof (RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9), -1, sizeof(RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6520[9] = 
{
	RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields::get_offset_of_OUTSIDE_EVEN_TOTAL_SUBSET_10(),
	RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields::get_offset_of_INSIDE_ODD_TOTAL_SUBSET_11(),
	RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields::get_offset_of_OUTSIDE_GSUM_12(),
	RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields::get_offset_of_INSIDE_GSUM_13(),
	RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields::get_offset_of_OUTSIDE_ODD_WIDEST_14(),
	RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields::get_offset_of_INSIDE_ODD_WIDEST_15(),
	RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9_StaticFields::get_offset_of_FINDER_PATTERNS_16(),
	RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9::get_offset_of_possibleLeftPairs_17(),
	RSS14Reader_t9FC07CBF52021171D0C9CF29BB0D7499A56F9FD9::get_offset_of_possibleRightPairs_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6521 = { sizeof (RSSUtils_t9785CB76FAF54ABA19C14EED7360F25A1BC26CDC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6522 = { sizeof (BitArrayBuilder_t8CBD8072A1AD84DCD163866A738492629130FA7C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6523 = { sizeof (ExpandedPair_tE17CFA34DD6966D8F3033E6661DD541E35C02712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6523[4] = 
{
	ExpandedPair_tE17CFA34DD6966D8F3033E6661DD541E35C02712::get_offset_of_U3CMayBeLastU3Ek__BackingField_0(),
	ExpandedPair_tE17CFA34DD6966D8F3033E6661DD541E35C02712::get_offset_of_U3CLeftCharU3Ek__BackingField_1(),
	ExpandedPair_tE17CFA34DD6966D8F3033E6661DD541E35C02712::get_offset_of_U3CRightCharU3Ek__BackingField_2(),
	ExpandedPair_tE17CFA34DD6966D8F3033E6661DD541E35C02712::get_offset_of_U3CFinderPatternU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6524 = { sizeof (ExpandedRow_t7C0073CB9F8326FD736529D22D8849C9287F1714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6524[3] = 
{
	ExpandedRow_t7C0073CB9F8326FD736529D22D8849C9287F1714::get_offset_of_U3CPairsU3Ek__BackingField_0(),
	ExpandedRow_t7C0073CB9F8326FD736529D22D8849C9287F1714::get_offset_of_U3CRowNumberU3Ek__BackingField_1(),
	ExpandedRow_t7C0073CB9F8326FD736529D22D8849C9287F1714::get_offset_of_U3CIsReversedU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6525 = { sizeof (RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A), -1, sizeof(RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6525[10] = 
{
	RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A_StaticFields::get_offset_of_SYMBOL_WIDEST_10(),
	RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A_StaticFields::get_offset_of_EVEN_TOTAL_SUBSET_11(),
	RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A_StaticFields::get_offset_of_GSUM_12(),
	RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A_StaticFields::get_offset_of_FINDER_PATTERNS_13(),
	RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A_StaticFields::get_offset_of_WEIGHTS_14(),
	RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A_StaticFields::get_offset_of_FINDER_PATTERN_SEQUENCES_15(),
	RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A::get_offset_of_pairs_16(),
	RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A::get_offset_of_rows_17(),
	RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A::get_offset_of_startEnd_18(),
	RSSExpandedReader_t8A39337BB3828379B173E2FA97C70E01E7B3782A::get_offset_of_startFromEven_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6526 = { sizeof (AbstractExpandedDecoder_t641084A96338858401AFFF4BFCFEFAD0E5F014E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6526[2] = 
{
	AbstractExpandedDecoder_t641084A96338858401AFFF4BFCFEFAD0E5F014E6::get_offset_of_information_0(),
	AbstractExpandedDecoder_t641084A96338858401AFFF4BFCFEFAD0E5F014E6::get_offset_of_generalDecoder_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6527 = { sizeof (AI013103decoder_t8D25C8201B46192370433D3BB441C2DBE1FE4A58), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6528 = { sizeof (AI01320xDecoder_t85C8202451E236034DBA9A4D324448F026784B32), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6529 = { sizeof (AI01392xDecoder_tD39960DBE83D8E165C0512B57F594CF086D96700), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6530 = { sizeof (AI01393xDecoder_tFADDE2973DA11F60E74D87D3B4E0A0BBB21F0AD6), -1, sizeof(AI01393xDecoder_tFADDE2973DA11F60E74D87D3B4E0A0BBB21F0AD6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6530[3] = 
{
	AI01393xDecoder_tFADDE2973DA11F60E74D87D3B4E0A0BBB21F0AD6_StaticFields::get_offset_of_HEADER_SIZE_3(),
	AI01393xDecoder_tFADDE2973DA11F60E74D87D3B4E0A0BBB21F0AD6_StaticFields::get_offset_of_LAST_DIGIT_SIZE_4(),
	AI01393xDecoder_tFADDE2973DA11F60E74D87D3B4E0A0BBB21F0AD6_StaticFields::get_offset_of_FIRST_THREE_DIGITS_SIZE_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6531 = { sizeof (AI013x0x1xDecoder_t7315E408DCB93F9C6F0324428A61A4CCC25A8520), -1, sizeof(AI013x0x1xDecoder_t7315E408DCB93F9C6F0324428A61A4CCC25A8520_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6531[5] = 
{
	AI013x0x1xDecoder_t7315E408DCB93F9C6F0324428A61A4CCC25A8520_StaticFields::get_offset_of_HEADER_SIZE_3(),
	AI013x0x1xDecoder_t7315E408DCB93F9C6F0324428A61A4CCC25A8520_StaticFields::get_offset_of_WEIGHT_SIZE_4(),
	AI013x0x1xDecoder_t7315E408DCB93F9C6F0324428A61A4CCC25A8520_StaticFields::get_offset_of_DATE_SIZE_5(),
	AI013x0x1xDecoder_t7315E408DCB93F9C6F0324428A61A4CCC25A8520::get_offset_of_dateCode_6(),
	AI013x0x1xDecoder_t7315E408DCB93F9C6F0324428A61A4CCC25A8520::get_offset_of_firstAIdigits_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6532 = { sizeof (AI013x0xDecoder_t8B2B04E3609D9D1D29786C79BBD2D9DB1766ED3D), -1, sizeof(AI013x0xDecoder_t8B2B04E3609D9D1D29786C79BBD2D9DB1766ED3D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6532[2] = 
{
	AI013x0xDecoder_t8B2B04E3609D9D1D29786C79BBD2D9DB1766ED3D_StaticFields::get_offset_of_HEADER_SIZE_3(),
	AI013x0xDecoder_t8B2B04E3609D9D1D29786C79BBD2D9DB1766ED3D_StaticFields::get_offset_of_WEIGHT_SIZE_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6533 = { sizeof (AI01AndOtherAIs_tF8B2B7D8A34248014D14C54D12FA284BBEF4A660), -1, sizeof(AI01AndOtherAIs_tF8B2B7D8A34248014D14C54D12FA284BBEF4A660_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6533[1] = 
{
	AI01AndOtherAIs_tF8B2B7D8A34248014D14C54D12FA284BBEF4A660_StaticFields::get_offset_of_HEADER_SIZE_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6534 = { sizeof (AI01decoder_tE988CB5C4AD990EB7AFBBBFD88EE931EE4BC153E), -1, sizeof(AI01decoder_tE988CB5C4AD990EB7AFBBBFD88EE931EE4BC153E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6534[1] = 
{
	AI01decoder_tE988CB5C4AD990EB7AFBBBFD88EE931EE4BC153E_StaticFields::get_offset_of_GTIN_SIZE_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6535 = { sizeof (AI01weightDecoder_t78BD30151A14BFC2DAD1C8B0607235384BA0F665), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6536 = { sizeof (AnyAIDecoder_t638C5EE5E5642BB5C6A87FD7054E7DABA6DD79C8), -1, sizeof(AnyAIDecoder_t638C5EE5E5642BB5C6A87FD7054E7DABA6DD79C8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6536[1] = 
{
	AnyAIDecoder_t638C5EE5E5642BB5C6A87FD7054E7DABA6DD79C8_StaticFields::get_offset_of_HEADER_SIZE_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6537 = { sizeof (BlockParsedResult_t14D7939BAE1374EBFC72FAC7E26487B5C4B51057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6537[2] = 
{
	BlockParsedResult_t14D7939BAE1374EBFC72FAC7E26487B5C4B51057::get_offset_of_decodedInformation_0(),
	BlockParsedResult_t14D7939BAE1374EBFC72FAC7E26487B5C4B51057::get_offset_of_finished_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6538 = { sizeof (CurrentParsingState_t69D94A0BACE62CD40F623625D2A86AF7EFF70BC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6538[2] = 
{
	CurrentParsingState_t69D94A0BACE62CD40F623625D2A86AF7EFF70BC1::get_offset_of_position_0(),
	CurrentParsingState_t69D94A0BACE62CD40F623625D2A86AF7EFF70BC1::get_offset_of_encoding_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6539 = { sizeof (State_tC50F546A8F03FC3887346A1F6959F542B48796F5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6539[4] = 
{
	State_tC50F546A8F03FC3887346A1F6959F542B48796F5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6540 = { sizeof (DecodedChar_tDEEEC3791ABB83FD5A00FF56B6E5485B2A7449F5), -1, sizeof(DecodedChar_tDEEEC3791ABB83FD5A00FF56B6E5485B2A7449F5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6540[2] = 
{
	DecodedChar_tDEEEC3791ABB83FD5A00FF56B6E5485B2A7449F5::get_offset_of_value_1(),
	DecodedChar_tDEEEC3791ABB83FD5A00FF56B6E5485B2A7449F5_StaticFields::get_offset_of_FNC1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6541 = { sizeof (DecodedInformation_tD144F663AE3C49E43CF53AEA9027371BC3D8F04A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6541[3] = 
{
	DecodedInformation_tD144F663AE3C49E43CF53AEA9027371BC3D8F04A::get_offset_of_newString_1(),
	DecodedInformation_tD144F663AE3C49E43CF53AEA9027371BC3D8F04A::get_offset_of_remainingValue_2(),
	DecodedInformation_tD144F663AE3C49E43CF53AEA9027371BC3D8F04A::get_offset_of_remaining_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6542 = { sizeof (DecodedNumeric_t936D82B0E56368429CFA9A75971DC972B75415D7), -1, sizeof(DecodedNumeric_t936D82B0E56368429CFA9A75971DC972B75415D7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6542[3] = 
{
	DecodedNumeric_t936D82B0E56368429CFA9A75971DC972B75415D7::get_offset_of_firstDigit_1(),
	DecodedNumeric_t936D82B0E56368429CFA9A75971DC972B75415D7::get_offset_of_secondDigit_2(),
	DecodedNumeric_t936D82B0E56368429CFA9A75971DC972B75415D7_StaticFields::get_offset_of_FNC1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6543 = { sizeof (DecodedObject_t57BA4BF95F496A5C794D66D54123FC45B10CAD9F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6543[1] = 
{
	DecodedObject_t57BA4BF95F496A5C794D66D54123FC45B10CAD9F::get_offset_of_U3CNewPositionU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6544 = { sizeof (FieldParser_t3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5), -1, sizeof(FieldParser_t3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6544[5] = 
{
	FieldParser_t3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5_StaticFields::get_offset_of_VARIABLE_LENGTH_0(),
	FieldParser_t3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5_StaticFields::get_offset_of_TWO_DIGIT_DATA_LENGTH_1(),
	FieldParser_t3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5_StaticFields::get_offset_of_THREE_DIGIT_DATA_LENGTH_2(),
	FieldParser_t3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5_StaticFields::get_offset_of_THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3(),
	FieldParser_t3BF62FBFEBC6951E0FC7AC33F665F7CADABDE1C5_StaticFields::get_offset_of_FOUR_DIGIT_DATA_LENGTH_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6545 = { sizeof (GeneralAppIdDecoder_t19AB438FE36F080A3109E1115E92D2E7BE988077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6545[3] = 
{
	GeneralAppIdDecoder_t19AB438FE36F080A3109E1115E92D2E7BE988077::get_offset_of_information_0(),
	GeneralAppIdDecoder_t19AB438FE36F080A3109E1115E92D2E7BE988077::get_offset_of_current_1(),
	GeneralAppIdDecoder_t19AB438FE36F080A3109E1115E92D2E7BE988077::get_offset_of_buffer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6546 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6547 = { sizeof (MaxiCodeReader_t5C701BBBD7748DAD7C518649C88DE5BCDF9EA74E), -1, sizeof(MaxiCodeReader_t5C701BBBD7748DAD7C518649C88DE5BCDF9EA74E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6547[2] = 
{
	MaxiCodeReader_t5C701BBBD7748DAD7C518649C88DE5BCDF9EA74E_StaticFields::get_offset_of_NO_POINTS_0(),
	MaxiCodeReader_t5C701BBBD7748DAD7C518649C88DE5BCDF9EA74E::get_offset_of_decoder_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6548 = { sizeof (BitMatrixParser_tDB34994B03A78BAA2BC3B717575CAA546A79A8C3), -1, sizeof(BitMatrixParser_tDB34994B03A78BAA2BC3B717575CAA546A79A8C3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6548[2] = 
{
	BitMatrixParser_tDB34994B03A78BAA2BC3B717575CAA546A79A8C3_StaticFields::get_offset_of_BITNR_0(),
	BitMatrixParser_tDB34994B03A78BAA2BC3B717575CAA546A79A8C3::get_offset_of_bitMatrix_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6549 = { sizeof (DecodedBitStreamParser_tE5085CF8BD40018339ED5FCB995819C27BB842AC), -1, sizeof(DecodedBitStreamParser_tE5085CF8BD40018339ED5FCB995819C27BB842AC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6549[1] = 
{
	DecodedBitStreamParser_tE5085CF8BD40018339ED5FCB995819C27BB842AC_StaticFields::get_offset_of_SETS_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6550 = { sizeof (Decoder_tDB4AB543D8B3BC1F37B79E1E08B9FA58A34EF8AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6550[1] = 
{
	Decoder_tDB4AB543D8B3BC1F37B79E1E08B9FA58A34EF8AC::get_offset_of_rsDecoder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6551 = { sizeof (IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841), -1, sizeof(IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6551[25] = 
{
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barPosA_2(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barPosB_3(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barPosC_4(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barPosD_5(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barPosE_6(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barPosF_7(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barPosG_8(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barPosH_9(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barPosI_10(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barPosJ_11(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barPos_12(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barTypeA_13(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barTypeB_14(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barTypeC_15(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barTypeD_16(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barTypeE_17(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barTypeF_18(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barTypeG_19(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barTypeH_20(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barTypeI_21(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barTypeJ_22(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_barType_23(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_table1Check_24(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841_StaticFields::get_offset_of_table2Check_25(),
	IMBReader_t92AB5FECB2ED489E59F463D44F6DFE7560C5C841::get_offset_of_currentBitmap_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6552 = { sizeof (DataMatrixReader_tC3F824C7E939E205F6ACE210AB0A7F743B256B0E), -1, sizeof(DataMatrixReader_tC3F824C7E939E205F6ACE210AB0A7F743B256B0E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6552[2] = 
{
	DataMatrixReader_tC3F824C7E939E205F6ACE210AB0A7F743B256B0E_StaticFields::get_offset_of_NO_POINTS_0(),
	DataMatrixReader_tC3F824C7E939E205F6ACE210AB0A7F743B256B0E::get_offset_of_decoder_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6553 = { sizeof (BitMatrixParser_tD8BA978B30ED653846ECDA9F103A20D754031AF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6553[3] = 
{
	BitMatrixParser_tD8BA978B30ED653846ECDA9F103A20D754031AF6::get_offset_of_mappingBitMatrix_0(),
	BitMatrixParser_tD8BA978B30ED653846ECDA9F103A20D754031AF6::get_offset_of_readMappingMatrix_1(),
	BitMatrixParser_tD8BA978B30ED653846ECDA9F103A20D754031AF6::get_offset_of_version_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6554 = { sizeof (DataBlock_tA871FAD1D9A6C41C1404AA867EB28924F36CD23B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6554[2] = 
{
	DataBlock_tA871FAD1D9A6C41C1404AA867EB28924F36CD23B::get_offset_of_numDataCodewords_0(),
	DataBlock_tA871FAD1D9A6C41C1404AA867EB28924F36CD23B::get_offset_of_codewords_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6555 = { sizeof (DecodedBitStreamParser_t75E6F6A326EEDC72CCAF230558CE09829D7AA6A8), -1, sizeof(DecodedBitStreamParser_t75E6F6A326EEDC72CCAF230558CE09829D7AA6A8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6555[5] = 
{
	DecodedBitStreamParser_t75E6F6A326EEDC72CCAF230558CE09829D7AA6A8_StaticFields::get_offset_of_C40_BASIC_SET_CHARS_0(),
	DecodedBitStreamParser_t75E6F6A326EEDC72CCAF230558CE09829D7AA6A8_StaticFields::get_offset_of_C40_SHIFT2_SET_CHARS_1(),
	DecodedBitStreamParser_t75E6F6A326EEDC72CCAF230558CE09829D7AA6A8_StaticFields::get_offset_of_TEXT_BASIC_SET_CHARS_2(),
	DecodedBitStreamParser_t75E6F6A326EEDC72CCAF230558CE09829D7AA6A8_StaticFields::get_offset_of_TEXT_SHIFT2_SET_CHARS_3(),
	DecodedBitStreamParser_t75E6F6A326EEDC72CCAF230558CE09829D7AA6A8_StaticFields::get_offset_of_TEXT_SHIFT3_SET_CHARS_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6556 = { sizeof (Mode_t9EF908AA02A10A3CDF91101AB27BF7A882BD641D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6556[8] = 
{
	Mode_t9EF908AA02A10A3CDF91101AB27BF7A882BD641D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6557 = { sizeof (Decoder_t0EFE53F08BE5D99F2C8ECDF5F42CF637F3A7CFBB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6557[1] = 
{
	Decoder_t0EFE53F08BE5D99F2C8ECDF5F42CF637F3A7CFBB::get_offset_of_rsDecoder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6558 = { sizeof (Version_tD8C947A936B05965195D7C3BBA526285521EFD8F), -1, sizeof(Version_tD8C947A936B05965195D7C3BBA526285521EFD8F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6558[8] = 
{
	Version_tD8C947A936B05965195D7C3BBA526285521EFD8F_StaticFields::get_offset_of_VERSIONS_0(),
	Version_tD8C947A936B05965195D7C3BBA526285521EFD8F::get_offset_of_versionNumber_1(),
	Version_tD8C947A936B05965195D7C3BBA526285521EFD8F::get_offset_of_symbolSizeRows_2(),
	Version_tD8C947A936B05965195D7C3BBA526285521EFD8F::get_offset_of_symbolSizeColumns_3(),
	Version_tD8C947A936B05965195D7C3BBA526285521EFD8F::get_offset_of_dataRegionSizeRows_4(),
	Version_tD8C947A936B05965195D7C3BBA526285521EFD8F::get_offset_of_dataRegionSizeColumns_5(),
	Version_tD8C947A936B05965195D7C3BBA526285521EFD8F::get_offset_of_ecBlocks_6(),
	Version_tD8C947A936B05965195D7C3BBA526285521EFD8F::get_offset_of_totalCodewords_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6559 = { sizeof (ECBlocks_t223B873D45E32B404C4228B45948B63FB39B7F19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6559[2] = 
{
	ECBlocks_t223B873D45E32B404C4228B45948B63FB39B7F19::get_offset_of_ecCodewords_0(),
	ECBlocks_t223B873D45E32B404C4228B45948B63FB39B7F19::get_offset_of__ecBlocksValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6560 = { sizeof (ECB_t357B13B476DDA42A5E0E37AB50F17833282E6902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6560[2] = 
{
	ECB_t357B13B476DDA42A5E0E37AB50F17833282E6902::get_offset_of_count_0(),
	ECB_t357B13B476DDA42A5E0E37AB50F17833282E6902::get_offset_of_dataCodewords_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6561 = { sizeof (Detector_tAF46BB964D7C58CF3451CFA84F631507DB4EAA67), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6561[2] = 
{
	Detector_tAF46BB964D7C58CF3451CFA84F631507DB4EAA67::get_offset_of_image_0(),
	Detector_tAF46BB964D7C58CF3451CFA84F631507DB4EAA67::get_offset_of_rectangleDetector_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6562 = { sizeof (ResultPointsAndTransitions_tE4236428649FE431618C18325509B8E2DDBBC9C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6562[3] = 
{
	ResultPointsAndTransitions_tE4236428649FE431618C18325509B8E2DDBBC9C7::get_offset_of_U3CFromU3Ek__BackingField_0(),
	ResultPointsAndTransitions_tE4236428649FE431618C18325509B8E2DDBBC9C7::get_offset_of_U3CToU3Ek__BackingField_1(),
	ResultPointsAndTransitions_tE4236428649FE431618C18325509B8E2DDBBC9C7::get_offset_of_U3CTransitionsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6563 = { sizeof (ResultPointsAndTransitionsComparator_t4F9EA4ED899B67839A5E4C67655FFF4C5BF0D090), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6564 = { sizeof (BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869), -1, sizeof(BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6564[3] = 
{
	BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869::get_offset_of_bits_0(),
	BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869::get_offset_of_size_1(),
	BitArray_t5F0A833AE6EA599E51DC8406FEFBC6736A431869_StaticFields::get_offset_of__lookup_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6565 = { sizeof (BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6565[4] = 
{
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2::get_offset_of_width_0(),
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2::get_offset_of_height_1(),
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2::get_offset_of_rowSize_2(),
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2::get_offset_of_bits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6566 = { sizeof (BitSource_t152BE85213C09752548F04B7AD62CD527C87A448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6566[3] = 
{
	BitSource_t152BE85213C09752548F04B7AD62CD527C87A448::get_offset_of_bytes_0(),
	BitSource_t152BE85213C09752548F04B7AD62CD527C87A448::get_offset_of_byteOffset_1(),
	BitSource_t152BE85213C09752548F04B7AD62CD527C87A448::get_offset_of_bitOffset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6567 = { sizeof (CharacterSetECI_tEA5814F6001515FC05BFDFAE3E8721124B48BC37), -1, sizeof(CharacterSetECI_tEA5814F6001515FC05BFDFAE3E8721124B48BC37_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6567[3] = 
{
	CharacterSetECI_tEA5814F6001515FC05BFDFAE3E8721124B48BC37_StaticFields::get_offset_of_VALUE_TO_ECI_1(),
	CharacterSetECI_tEA5814F6001515FC05BFDFAE3E8721124B48BC37_StaticFields::get_offset_of_NAME_TO_ECI_2(),
	CharacterSetECI_tEA5814F6001515FC05BFDFAE3E8721124B48BC37::get_offset_of_encodingName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6568 = { sizeof (DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6568[10] = 
{
	DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7::get_offset_of_U3CRawBytesU3Ek__BackingField_0(),
	DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7::get_offset_of_U3CNumBitsU3Ek__BackingField_1(),
	DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7::get_offset_of_U3CTextU3Ek__BackingField_2(),
	DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7::get_offset_of_U3CByteSegmentsU3Ek__BackingField_3(),
	DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7::get_offset_of_U3CECLevelU3Ek__BackingField_4(),
	DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7::get_offset_of_U3CErrorsCorrectedU3Ek__BackingField_5(),
	DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7::get_offset_of_U3CStructuredAppendSequenceNumberU3Ek__BackingField_6(),
	DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7::get_offset_of_U3CErasuresU3Ek__BackingField_7(),
	DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7::get_offset_of_U3CStructuredAppendParityU3Ek__BackingField_8(),
	DecoderResult_tE12201ED0A19AEAE781E2A67641D4A6FCC198CD7::get_offset_of_U3COtherU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6569 = { sizeof (DecodingOptions_t47C061C31B88DE9FB76187F5289E251C3B2E35B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6569[2] = 
{
	DecodingOptions_t47C061C31B88DE9FB76187F5289E251C3B2E35B7::get_offset_of_U3CHintsU3Ek__BackingField_0(),
	DecodingOptions_t47C061C31B88DE9FB76187F5289E251C3B2E35B7::get_offset_of_ValueChanged_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6570 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6570[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6571 = { sizeof (DefaultGridSampler_tB2AE863567F11EFCEA1A85BD24C0306F0DA866D0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6572 = { sizeof (DetectorResult_t87E236CAB9ED47A76E59D1809CE6C7DD6D8E9E2C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6572[2] = 
{
	DetectorResult_t87E236CAB9ED47A76E59D1809CE6C7DD6D8E9E2C::get_offset_of_U3CBitsU3Ek__BackingField_0(),
	DetectorResult_t87E236CAB9ED47A76E59D1809CE6C7DD6D8E9E2C::get_offset_of_U3CPointsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6573 = { sizeof (ECI_tEF9EAED8C9F3BFA6043AC72BEE50849BD3124A1A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6573[1] = 
{
	ECI_tEF9EAED8C9F3BFA6043AC72BEE50849BD3124A1A::get_offset_of_U3CValueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6574 = { sizeof (GlobalHistogramBinarizer_t757A4C6316A8E4C0F07D36369F4D8CA17D8FF760), -1, sizeof(GlobalHistogramBinarizer_t757A4C6316A8E4C0F07D36369F4D8CA17D8FF760_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6574[3] = 
{
	GlobalHistogramBinarizer_t757A4C6316A8E4C0F07D36369F4D8CA17D8FF760_StaticFields::get_offset_of_EMPTY_1(),
	GlobalHistogramBinarizer_t757A4C6316A8E4C0F07D36369F4D8CA17D8FF760::get_offset_of_luminances_2(),
	GlobalHistogramBinarizer_t757A4C6316A8E4C0F07D36369F4D8CA17D8FF760::get_offset_of_buckets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6575 = { sizeof (GridSampler_t8DD0C5250D72AC8D9F5DFD0F417F2FE212BE91D4), -1, sizeof(GridSampler_t8DD0C5250D72AC8D9F5DFD0F417F2FE212BE91D4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6575[1] = 
{
	GridSampler_t8DD0C5250D72AC8D9F5DFD0F417F2FE212BE91D4_StaticFields::get_offset_of_gridSampler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6576 = { sizeof (HybridBinarizer_tB2BCFE24952947F400B6375EEA7BE71B273024F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6576[1] = 
{
	HybridBinarizer_tB2BCFE24952947F400B6375EEA7BE71B273024F3::get_offset_of_matrix_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6577 = { sizeof (PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6577[9] = 
{
	PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E::get_offset_of_a11_0(),
	PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E::get_offset_of_a12_1(),
	PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E::get_offset_of_a13_2(),
	PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E::get_offset_of_a21_3(),
	PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E::get_offset_of_a22_4(),
	PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E::get_offset_of_a23_5(),
	PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E::get_offset_of_a31_6(),
	PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E::get_offset_of_a32_7(),
	PerspectiveTransform_t411B3056590AB547648BD95C69DAAE53E890246E::get_offset_of_a33_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6578 = { sizeof (StringUtils_t49EDE148BE359FFB850E9D1A27AEBA5B7136EA92), -1, sizeof(StringUtils_t49EDE148BE359FFB850E9D1A27AEBA5B7136EA92_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6578[3] = 
{
	StringUtils_t49EDE148BE359FFB850E9D1A27AEBA5B7136EA92_StaticFields::get_offset_of_SHIFT_JIS_0(),
	StringUtils_t49EDE148BE359FFB850E9D1A27AEBA5B7136EA92_StaticFields::get_offset_of_GB2312_1(),
	StringUtils_t49EDE148BE359FFB850E9D1A27AEBA5B7136EA92_StaticFields::get_offset_of_ASSUME_SHIFT_JIS_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6579 = { sizeof (GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F), -1, sizeof(GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6579[15] = 
{
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields::get_offset_of_AZTEC_DATA_12_0(),
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields::get_offset_of_AZTEC_DATA_10_1(),
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields::get_offset_of_AZTEC_DATA_6_2(),
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields::get_offset_of_AZTEC_PARAM_3(),
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields::get_offset_of_QR_CODE_FIELD_256_4(),
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields::get_offset_of_DATA_MATRIX_FIELD_256_5(),
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields::get_offset_of_AZTEC_DATA_8_6(),
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F_StaticFields::get_offset_of_MAXICODE_FIELD_64_7(),
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F::get_offset_of_expTable_8(),
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F::get_offset_of_logTable_9(),
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F::get_offset_of_zero_10(),
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F::get_offset_of_one_11(),
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F::get_offset_of_size_12(),
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F::get_offset_of_primitive_13(),
	GenericGF_tE77A30A7BDD355A97FEFDB02A32930CF15D32B6F::get_offset_of_generatorBase_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6580 = { sizeof (GenericGFPoly_t9D1F5D26F3EF2C3E8150C6A603ABCC096433316A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6580[2] = 
{
	GenericGFPoly_t9D1F5D26F3EF2C3E8150C6A603ABCC096433316A::get_offset_of_field_0(),
	GenericGFPoly_t9D1F5D26F3EF2C3E8150C6A603ABCC096433316A::get_offset_of_coefficients_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6581 = { sizeof (ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6581[1] = 
{
	ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427::get_offset_of_field_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6582 = { sizeof (ReedSolomonEncoder_t31EBCD471B9E5411E55FEDFC6C61B3D95BC630E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6582[2] = 
{
	ReedSolomonEncoder_t31EBCD471B9E5411E55FEDFC6C61B3D95BC630E3::get_offset_of_field_0(),
	ReedSolomonEncoder_t31EBCD471B9E5411E55FEDFC6C61B3D95BC630E3::get_offset_of_cachedGenerators_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6583 = { sizeof (MathUtils_t15FC2B42DF83BEF551EF6D9DDF43D546CF5B1E9B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6584 = { sizeof (WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6584[7] = 
{
	WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4::get_offset_of_image_0(),
	WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4::get_offset_of_height_1(),
	WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4::get_offset_of_width_2(),
	WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4::get_offset_of_leftInit_3(),
	WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4::get_offset_of_rightInit_4(),
	WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4::get_offset_of_downInit_5(),
	WhiteRectangleDetector_t45C0E6821055358C7556A1BEA2D735E80E2609A4::get_offset_of_upInit_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6585 = { sizeof (AztecReader_tFC2F1E1EAB2AB0055551B812448ABF6C0E005EBF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6586 = { sizeof (AztecResultMetadata_tE5FDBE80BFE0F2046505F9EAD9043EDF5CA6FBBA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6586[3] = 
{
	AztecResultMetadata_tE5FDBE80BFE0F2046505F9EAD9043EDF5CA6FBBA::get_offset_of_U3CCompactU3Ek__BackingField_0(),
	AztecResultMetadata_tE5FDBE80BFE0F2046505F9EAD9043EDF5CA6FBBA::get_offset_of_U3CDatablocksU3Ek__BackingField_1(),
	AztecResultMetadata_tE5FDBE80BFE0F2046505F9EAD9043EDF5CA6FBBA::get_offset_of_U3CLayersU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6587 = { sizeof (AztecDetectorResult_tDE03D185E80A00BE03617EE579AE2DB5F832D133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6587[3] = 
{
	AztecDetectorResult_tDE03D185E80A00BE03617EE579AE2DB5F832D133::get_offset_of_U3CCompactU3Ek__BackingField_2(),
	AztecDetectorResult_tDE03D185E80A00BE03617EE579AE2DB5F832D133::get_offset_of_U3CNbDatablocksU3Ek__BackingField_3(),
	AztecDetectorResult_tDE03D185E80A00BE03617EE579AE2DB5F832D133::get_offset_of_U3CNbLayersU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6588 = { sizeof (Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299), -1, sizeof(Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6588[8] = 
{
	Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields::get_offset_of_UPPER_TABLE_0(),
	Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields::get_offset_of_LOWER_TABLE_1(),
	Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields::get_offset_of_MIXED_TABLE_2(),
	Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields::get_offset_of_PUNCT_TABLE_3(),
	Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields::get_offset_of_DIGIT_TABLE_4(),
	Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields::get_offset_of_codeTables_5(),
	Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299_StaticFields::get_offset_of_codeTableMap_6(),
	Decoder_tD0E032E97F5F725E023ECCA3ABE7AC5CD986D299::get_offset_of_ddata_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6589 = { sizeof (Table_tC89A3020A8D420688E32AE449F26893B3E6DBEE3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6589[7] = 
{
	Table_tC89A3020A8D420688E32AE449F26893B3E6DBEE3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6590 = { sizeof (Detector_t477B046E776FFED542B73805D784710D541601DE), -1, sizeof(Detector_t477B046E776FFED542B73805D784710D541601DE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6590[7] = 
{
	Detector_t477B046E776FFED542B73805D784710D541601DE_StaticFields::get_offset_of_EXPECTED_CORNER_BITS_0(),
	Detector_t477B046E776FFED542B73805D784710D541601DE::get_offset_of_image_1(),
	Detector_t477B046E776FFED542B73805D784710D541601DE::get_offset_of_compact_2(),
	Detector_t477B046E776FFED542B73805D784710D541601DE::get_offset_of_nbLayers_3(),
	Detector_t477B046E776FFED542B73805D784710D541601DE::get_offset_of_nbDataBlocks_4(),
	Detector_t477B046E776FFED542B73805D784710D541601DE::get_offset_of_nbCenterLayers_5(),
	Detector_t477B046E776FFED542B73805D784710D541601DE::get_offset_of_shift_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6591 = { sizeof (Point_t14A3899002BFDE712E74CAA755F46631AE225BB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6591[2] = 
{
	Point_t14A3899002BFDE712E74CAA755F46631AE225BB7::get_offset_of_U3CXU3Ek__BackingField_0(),
	Point_t14A3899002BFDE712E74CAA755F46631AE225BB7::get_offset_of_U3CYU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6592 = { sizeof (U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC), -1, sizeof(U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6592[355] = 
{
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U300E14C77230AEF0974CFF3930481157AABDA07B4_0(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U301898B76F7C45E723996C7E8EDF45429F10AB352_1(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U301A7BB0BE820D25E52A42C3D28DE27822EABCD4C_2(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U30222E151247DAE31A8D697EAA14F43F718BD1F1C_3(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U303AB6BB65AC7B8ECF309D37CACECE4B53B38D824_4(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U30443C73B92DF9C67128EF5F0702DCBC469EDD2EA_5(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3061E1E805CBF51D839C3B91E8ACD0326E5C77FFE_6(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U307438A85BDEE90305A6125B92084A5F3F2FD5F18_7(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3094D7DFD41161273E784D60EAE6D844127D77F0B_8(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U309E1F33A3C65971119BBEF1ED121FE70EA9CFA5C_9(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U30BDCFD3264DD1A949E825D9EBC8E14CB1FB19F49_10(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U30BF07D5C458ED1EF2F2CAD7D1DA239CF12952AB6_11(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U30C7D347178E5302733AED896AA3A7B5DDEDC3A90_12(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U30D0AD555C6DA0BD4268F8573DC103F455F62A610_13(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U30E4585982EA19664C33EA213EE69EC548F7322B0_14(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U310111111FB24B2D331FB3F698193046CBCB8AAFE_15(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U31037DC6EAB9D7D3850660726ED132EAD961A41E9_16(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U310486CAECBA5E73DA05910F94513409DEB6316FD_17(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U311C3F78703162740B961B75ED808973DD68F7FA8_18(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U311D4D9FC526D047E01891BEE9949EA42408A800E_19(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U312625AC94FB97DAA377449A877D25786C8ADFFC5_20(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U312F1DB2AC68CD455638374E8DF9E9D604B15F693_21(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3142E1469231D592823559059166B32655910D0CF_22(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3185FAB3B3D8AC0F1CBECFFCD7DEA45EE4AC9E357_23(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3186E7643E22CF0E2479E92C738FEBEF56B275E95_24(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3188E7A480B871C554F3F805D3E1184673960EED5_25(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3193330123B5DF8BA7DDD1FD73E3941FD113C71C7_26(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U31A3C01816F6E750EC65FDEF4FEB7758235456890_27(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U31A85140B8FCA30191EE9347F53782234F0781F79_28(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U31BCA1A912FF22DA69AA3E91A68492D4BA5179162_29(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U31D0501D14FE31729CD75A4073F4404AF7E53B024_30(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U31D562AD827598FC3F5C82FC2091E65C8F8A07ADC_31(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U31DB4EC083D80319093844E5CD9A7EB62EB1DC8D9_32(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U31DDB4E5FC90455E6D0C0C82A30C6CB0EC456C9A9_33(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U31E45F828069EAF87A0B553BE1E65B98F20E377ED_34(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U31EB30BCD6A1576A001C2906D3AF45955E19C48BC_35(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U31F5771BF2009B14CC123999096615351A0BE9527_36(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U31FFDEC0F27BC9DCF708F067786205F164F8A6D32_37(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U320D740A7E661D50B06DC32EF6577367D264F6235_38(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3227C649F55292B0A293EBB2D56AB8ABAECC7F629_39(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U322BF5B86DC5E0B87331903141C5973C117DA716E_40(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U32309DA7AF39CA2B05C77DC6399A4EFF2563EC3EB_41(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U32357B210FC2522A4A9D0F465AEAC250A79FA9449_42(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U323D0C20AB9ABA25A5B61CC87AF33BBE7DDF6E2DD_43(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3241FC42CDC9A5435B317D1D425FB9AB8A93B16CD_44(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U324E7D35141138E4668DE547A98CE3A84AF0E6FEB_45(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3259B7EF54A535F34001B95480B4BB11245EA41B5_46(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U32861CF9E030EE4DA3C1BA4824C08FA40377E2C1A_47(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U328D1E3B5188FE552D37912314E419E7F30ABD844_48(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U328F991026781E1BC119F02498D75FB3A84F4ED37_49(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U32923D4191FF7FBBF11BFC643B6D93CC8074D31E1_50(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U329AB17251F5A730B539AB351FFCB45B9F2FA8027_51(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U32A84324D54FDC0473BE021004C50F363ADCD89AF_52(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U32AA5A01E857E2BB4C8B4DF06D1CA848E68BE04A2_53(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U32D3DF989BE3BBA2B353F845C37992DFF85579505_54(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U32DFCD7C13A7339CC41C473220CE80CDD5933C64C_55(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U32E8B473A712423E983C2B2EDA43927EB0A14F5B4_56(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U32EC69895F74DBAE74600E960EFFB6448597BDEEB_57(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3300F78B3CE3C272479EDB686B2ABE3E4EA59FE2D_58(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U33102377CA043644BFD52C56AE2A9F554723263FD_59(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U33199DD35AA6A041AF9F66BFBAEC3563E838D3731_60(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3322BC3569CD0270EC3179B30382F24148B533D10_61(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U333B8540230E7366A64AC520E4F819103EB00ECD0_62(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U334617E1F9E901BD9DD9FEB1839483F5ED406E7B3_63(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U335859E8329070E55C5D9282930023DD6B1594141_64(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U335E8FEC2F20CD20788D4696BE84FFE01B7F8CF49_65(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U335FD69D1B2AA03F7DD7DF9E989BF548D7066BCE8_66(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U338017E50AF503681BFC2BD0CDED5F7565471B574_67(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U338CAE5BC1B31E24A88061CEC362093BB4D8F0523_68(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U33A6FC30DA7BC0C272E78EACD5C7EC8448B09019C_69(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U33C22A633B79B510613E37BC5A02FF91A4AB006ED_70(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U33D3C46CC589F65C2443688952CBCD9C499839622_71(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U33D8521A958BBFCC708EE0728993AE8CAFEB154D6_72(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U33DF543E51AB43F1F561B11B8789C82E26C427F72_73(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U33E12851A90C6A08ACCD1D67634AC7FD1639B898D_74(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U33E4BBF9D0CDD2E34F78AA7A9A3979DCE1F7B02BD_75(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U33E8C3886467AEC93954B0B1F23B4969CE5FDB3A1_76(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U33F152B3DFAF72A55D78439E5015C55A20967621A_77(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U33F6BF7D0B62CB3F477969D34B9EE004C7B4B7B5A_78(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U33F6E4F79F4A57C7F0F4467AA7116C5C46FDFB54E_79(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U34065D9695EFBED47DDA95610295BA85121D5A17B_80(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U340B82CCF037B966E2A6B5F4BAC86904796965A1C_81(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U340FEF102AB67B5038E6D9E2EE2BE16854A49810D_82(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U342A99199D41E8832E72CD942706DDA326C0BB356_83(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3445E0C138E7D9647FCD08184FB23A833C4D00D0D_84(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U344DB2483C3365FFD6E86B5C807C599AAF3EEE238_85(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3454BEECC44D2D67A27C16F6E7CD7D3C3BEA3CE8E_86(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3463C3B3709985BB32747ACC9AE3406BB6836368B_87(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3465B4A79F3CD93D22FD6AFE37CB544AC27728D1D_88(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3471648086F9480CB553B7802323E0A75FFE09C5A_89(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3484B207F3CBCE8338BDE26F04E87EE256891BE0F_90(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U348F073C6CC618A8FDA6FD23ABFDC828CC6A5812E_91(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U348F53BDF84D98A3F4EF0C09A8F9E77C8EA67216D_92(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U349594851C4B6C279C35B162F0236A99E7C31DD36_93(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U349E2034AE9BD046DF074F7FA4ACAE05980D1DC6F_94(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U34B811DD8E2B83E6826F8004D4A5CE44AFACAEB3E_95(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U34CC0A0F757356B175679AE3FDB722375172ABA96_96(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U34CCAF842695DCCF7F7A9EC0D641DC36B38937FA3_97(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U34D317202204E9741354BC1A1628BC10A05E2AB7B_98(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U34DE742FEB32578958FCFC33D6732EFF616C8A94B_99(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3501D7C551BDFD3885F36478424EEE5B72766C842_100(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U350DACD1562B0163C9FC5B673478D4231A4B37B1F_101(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U351109782EA6E98E8E277657055A598684D21520E_102(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3526BB16C9A1C821B19858E18142A1F7073BC648D_103(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U352EFBB0B7A687E5D277CB80D62D4BDEA8C9C5AAF_104(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U353173106B161530CA26C69983B44BBE2FAB890D0_105(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3539B26D17C2DEB88FB9E07611D164408E23C35AB_106(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U353D98E6046BB88C14DD740E4F77B16402271243B_107(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3553D4B8922B99AF29EBBC9798C29AAA4A07DEB3C_108(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U355A6E22CC24364881F62CCF67CBE9A348DC602FA_109(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U356A5590CEC5A9DF3AE13EDFBA60AAC3CB21CA9FE_110(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U356F75F925774608B6BF88F69D6124CB44DDF42E1_111(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U35741ABC064FCDEF4C5EE02F31DB992C205EA7231_112(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3583A438479F42C7D8DE8C6699B0C37A4A026A643_113(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U358B2CCE2545886B61E58C3C60831C2B049814E09_114(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U35984617C3B1EF1E6C9F13221059A1E78C2226ADF_115(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U359A84F660FFFFA4B9C037456F9D56E26ACF3663E_116(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U35B02FED4CA0B61476F1AAA5C6445F024374B075A_117(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U35B09FAED4CEFCE4CFF42D910767BD0D40C69AB6C_118(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U35BCFEA149A36030B8B3EB1FAE31AF9173B76B87C_119(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_120(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U35C35E2C7D0F17CBE31F7F544FF3837EA3A2EE963_121(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U35C563BFAB6400B2824F5E41D78FB3097E8244C26_122(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U35CB924E7461B9011EC8197A30815EA263595B3A4_123(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U35D160627930BB84F7867AEEC7143FB2399C7CA79_124(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U35DD69D93CBAA5623AE286E97265890A7693A0AC2_125(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U35E3F56450E340B21A3A13E39A66941353F8C7C1A_126(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U35FD971B7E6A431E6F3F91D5A95F2006A24F48780_127(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U360E3C19D774F8C210299534335ED5D3154717218_128(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U361A1021606CAD0A5990E00FD305478E75F1983D6_129(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U362AB9401F4CB2B0A7FF72D3F3F56AA2F14E34FE0_130(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U363B762373DE81B4580E1909D7168A21A989A182A_131(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3648C489642395164C2A2AA2379A6E7F44CAB018B_132(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3661F9E9FEA16C3A9CDF452AB61743578471F7447_133(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U367013075958AAD75C4545796BF80B16683B33A59_134(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U36727D77DB74B08CC9003029AC92B7AC68E258811_135(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U36882914B984B24C4880E3BEA646DC549B3C34ABC_136(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U36891D97C19F4C291AD821EF05E4C5D1BD3C34796_137(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U369499AF6438C222AAADFC92FF3D3DF3902A454FC_138(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U36957A133E0DC21707BCAAF62194EAF4FF75E0768_139(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U369D489A8DADAF0948858B02F2CC7258B1530D843_140(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U36A09CB785087F5A9F6EC748C7C10F98FA2683534_141(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U36AA66657BB3292572C8F56AB3D015A7990C43606_142(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U36AF652181FBB423ED995C5CF63D5B83726DE74CF_143(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U36B38D84409B2091CBC93686439AC2C04C76DB96E_144(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U36CC1359B9ED72500183D5566D70BBEE1BBD46FFF_145(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U36CEDD7504235CF66C53FB70A17ACBDD440D77C62_146(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U36DE89499E8F85867289E1690E176E1DE58EE6DD3_147(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U36E1E2FE69810DD67F59A4BAC7367A17D2F1B9BDA_148(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U36F604C33212E20CBFCE9F2221492DC6FD823857C_149(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U370CFE27207DDB1C62DD3A9DA22F21F3B1831D7D5_150(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U37102A07EDF52C052D5625D73275A579DC6E76C92_151(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3712B24DD3C17671C78BE33C83316576AD9285273_152(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U371D2BEF10D4C6CA98FC143E3E4B7DB154F9FB890_153(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U371F4D547D52F31EDEC55B1909114455FDAE5A97E_154(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U372CDCBDCF34EFD3E09C3F7057DB715DAAFCE06E2_155(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U372FF92F22AB5BD5A344C652E176645C52D825AEC_156(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U373B101BBEDC241CE765CA03BB6E4D467FA769312_157(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3749EB212DEC39495349481F49F3DEF480777A197_158(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U374AE47B2D78B62583BA50045F6B288CACC24EA7D_159(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U37616E4F44CA76DF539C11B9C8FD911D667FEFC52_160(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3761EC2969B9D1960DB3CB7FC05EEC96944B59BC6_161(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U376D7CB1729978C5437409A8C56AA1C7E24890D5C_162(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3770616E567083077933DC0B29A8F8368FC2EB431_163(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3776EDFD0860A21D5119CA197C89BD2AC7D6F9283_164(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3783FD3C801560D4BA59B6ADFEB9C9F8A4EBC4319_165(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U379123EF44D00F5B0ED365348D1FD49F9FA2CB222_166(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U379A4B717DAF4312A78F1A75A72221DB9690D1B25_167(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U379BEC582299CE5F46902B9C091C19931E26D9323_168(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U379F14B68470B4B7AA3BCDCA6758072F2B9320320_169(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U379F9F064F81690C950BF4EFFAA5245BA966607C9_170(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U37BC6404D48DA137303466471F7131DC1C9B13BAA_171(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U37CF92A83536E2264EF41D8C407D246E41E2DBCD6_172(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U37DAC049D7C6EB133A805C7CE9ACBB961B3E65A8D_173(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U37E1BD4E95DA87F65A3B99B774AE2F2F3B56E4739_174(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U37ECDF8C5F474CFF2B0970E93C397B504DD793D2C_175(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U37FDC1CA6ED532684E45DDAE8484274399FCE4C59_176(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U37FF393DF5D10580FA38BF854328B3072169E0A1A_177(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3802AD21B222E489DAFEADD5E18868203D9FC2A89_178(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3806D200653BD7335D0077F2759E715D8BE8DF9BF_179(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3808AD19CA818077F71100880E7D7AA21147E987E_180(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U380DD6A282D563836522C10A78AAA17F0B865EB5A_181(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U380E21576779C61DA5B185D15A732CF7A8BD6F88C_182(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U380E9217618F8C2C7FED61E4E7653CD0E66C74872_183(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U380F763BA90E27F755F2DEFA7FE720AD9371D6E6B_184(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3836DA48724BC66F3257E70FFF165ACCE037666A6_185(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U38523C737ED49D8A2E9BB85218190E4E83B902E28_186(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U385666B25692AB814E91F685384EEA0389F147E70_187(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3866652192B68681CC538B2CB21C5979544DF35AB_188(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U38683FBE90F936E0F2A1080E053EBA3EE9F44A02B_189(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3868C8972C4058443A2D131C22083401956DF81C7_190(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U386EA004891DE06B357581B1885C9C0EFD73DE2E9_191(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3881C2E8F71772ECA0CFC4DB357196C4EB788D6F8_192(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U388B6AF1079C72775E54D0AC3BEEDCCC6506B6269_193(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U388CFCB31ED9AFFB90483973D02D5DEE8A82A4892_194(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U38909772CC771C2FF1D61415555452F2ED4ADA536_195(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U38971045D9FA23669DC4A204D478DC63888A9B420_196(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U38AC7C1F7AAD5E1D2EA280038197C66272ED82BBE_197(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U38C301DDB45068B145D6C11F79591061EEE2AABA8_198(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U38D1369A0D8832C941BD6D09849525D65D807A1DD_199(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U38DB47DDFFC81E0976DEF3EFF3CA9EFC8465CFEEE_200(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U38DCF3F57BD5AAF8D097375E4290A41B13B2C8B04_201(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U38EB2BFBB9F53D86C1ADDFFA44DEE5F61A85BE91F_202(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U38EED33571FFED1BBCA4954A909498A1D3316EDC8_203(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U38EEE72A955DF384253C5C458726A8A7299A05164_204(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U38FD55399F538FAF9608E938EAB08D4BC88D0E131_205(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U39032D4E1ED9EBAC2417A10654FF0C658D2CC9825_206(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3910681E5316200ECA5484DF31EEAE1CF5E2F3439_207(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U391EA1528B7E2B59DB78E5E7A749312DD20AA43F1_208(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U395C66836A6DB5A262B674463792A9A67241AEC25_209(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U395CE8E84F0E42D775BCBD2FD64020F5FCE99E5EE_210(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U39623BB2649942957F680075A5AD3A16DE427F7A2_211(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U397BFC7D9D43B639F03919A5AE13B1A6CA2B05A08_212(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U39848EC8C1177B700CDB98F1A7C07F2A9C9EDE444_213(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U39858ACE49B9B39354CFCB05E54A9790070E48D39_214(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U399073F844984F28C1A9FC1E1490E5631272E26A0_215(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3993DC5FF70241D5149EB5EAF1CDD957CA0B23454_216(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U3994037270AFD7BB2BBAFCFBF2C5901853D0AEBBB_217(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U399CC0827BC16027263E3737FF59411A5D4BFDE31_218(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U399D70A082BCB145B3134DDB7BEC4FDB5BDEFA756_219(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U39AA52F510245480023EB2EF8CF779F6008E93D7A_220(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U39ACE167D7580B82857BBF66A5A9BC1E2D139353C_221(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U39B3E34C734351ED3DE1984119EAB9572C48FA15E_222(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U39BBAB86ACB95EF8C028708733458CDBAC4715197_223(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U39BBB53FFCACBBEE118BC0F5D16EDC4DF37E55093_224(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U39C89FB9160DB0A983D5BE5D510BF9E801F0136A6_225(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U39D6E2A703E2F8C69C9C7A0B8DB8F6A8035E84601_226(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U39E548860392B66258417232F41EF03D48EBDD686_227(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U39F6BDC14CCFDFFBF8F1FE3539741F79424F33E16_228(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_U39F8CAB0A98B00335FDDBCC16C748FEA524A25435_229(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_A0324640CCF369A6794A9A8BB204ED22651ECBCC_230(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_A2978A4F76015511E3741ABF9B58117B348B19C2_231(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_A2D008F8B63A6D553DEDE7D3610DC6F6F7359D86_232(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_A350EF43D37B02EFF537EB2B57DD4CF05DD72C47_233(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_A3F55D69F9A97411DB4065F0FA414D912A181B69_234(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_A6E975E72D8A987E6B9AA9400DE3025C1E91BE96_235(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_A7977502040EE08933752F579060577814723508_236(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_A94E3BD2B46BBC16C9F5C0AD295E9B2C62B6B7A4_237(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_A98B8F0C923F5DFC97332FCC80F65C96F9F27181_238(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_AA607BA0FA20D3E80F456E68768C517D17D073D5_239(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_AB44F69765F4A8ECD5D47354EA625039C43CC4D6_240(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_AB59D1B3DA1B1A65D94F0DBD1C66958A4D228CA0_241(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_AE2DCC8D6F51ED60D95A61A92C489BB896645BEA_242(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_AECEE3733C380177F0A39ACF3AF99BFE360C156F_243(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_AEE03822C3725428610E6E7DAAF7970DE35F7675_244(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_AF35D391D56E5153255BA0E0810F3EF49FC4CE6A_245(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_B15D37D4738DBDC32C38C2E1B6A1833322C22868_246(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_B166D50D0891455E483E2C6E1DB11C3DC03CAFFA_247(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_B19381C60E723A535EF856EC118DB867503FCC63_248(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_B29A2328618A7D8133205EA8F9E0EE1D9D2A7E8C_249(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_B3FDEC1908C5093BFC5CFB12749EE2650C8612C3_250(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_B5DDF34D20E879DBA76358A13661F61E6682B937_251(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_B5E2D0ECF599F0A1D05EA2245C99A83E064F0B2A_252(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_B6BCDFDEDD07A19F1028F226DE019E32E7FB2333_253(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_BB40482B7503DCF14BB6DAE300C2717C3021C7B2_254(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_BBA3BFEF194A76998C4874D107EBEA8A81357762_255(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_BBAA25706062E8AA738F96C541E3A6D6357F92E3_256(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_BBE42ED1645F9D77581D1D5B6EE11B933512F832_257(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_BC1ACC3AC4459BD0D19A953A64E05727DA98279A_258(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_BC5EFA0F409033551EB01FD9CAA887739FBB267C_259(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_BCF92ED388B84323DFDE7D40E59EA1AEB742CB0A_260(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_BDA0E80EABB31C2B339BA084415FF30AE14ECF9F_261(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_BF4FBD059EA47B09FA514A81515DD83A7F22A498_262(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_BFF88CC5DA20A8D6A16ABACAA74E0573BBA528FA_263(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_C04B3C75D3B52DBFE727A123E3E6A157786BBD6F_264(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_C0935C173C7A144FE24DA09584F4892153D01F3C_265(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_C29C4F31F34306CA62AC81D96AE1FB141EC2A8B6_266(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_C4449C115C6F0E639E5F53D593F6E85D55D18DB0_267(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_C530C580972A28DBE79D791F61E5916A541E6A1A_268(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_C5D5B1E32C9C12C0006F02AA8C3B84BFF480F932_269(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_C62717FC35BA54563F42385BF79E44DD0A510124_270(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_C6B6783FDC75202733D0518585E649906008DBC5_271(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_C6CA93B7FFF2C18F722DDBD501761C1381E2E55E_272(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_C7787E3098145FA28D38DE952D634BA862284127_273(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_C7BD442E02179DC75AB7FEF343C297E1F46A7806_274(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_C7FBA0F208A16658206056EC89481A5EE74A7259_275(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_C82F7744F78AA5830EDBD9AD774CFF76737F7D40_276(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_C92559E411FAF1D47ADD6CCA7374DAF066069D00_277(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_C9E3D331C8CC7E17B017EC19E05F5868E4A2D00B_278(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_CA03F848D4A9DD85B5CF092E36317D483E3B0864_279(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_CB780513B9E4B4E2590F7104B066F632E7BC3E38_280(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_CC74BF89CF621AAC29665426A911E4B8BBB60B91_281(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_CC95A63361E95F5A0BDF7771CE1AF37BE35817C6_282(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_CC9EE2CF247B3534102C1324C2679503686EF031_283(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_CDDC3D4212D35722E3E8BAA7B5275932A6567037_284(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_CED1DB702E32A812D587254A4E1BAAA409E30859_285(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_CF1C9B07580D3992A644F840EBBB156E0E92B758_286(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_D10229C07961891A434737F06293F1E0CC0FA33C_287(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_D1A2B61FD08BB2242E12F4CAF6B512029C0DC177_288(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_D1C5CA089B2632B5ECD27F78A8E343FB6088AF2F_289(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_D2BCB25441A9A8F3DE94D1EF27A870BB77553833_290(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_D41FD917303D010F26005BDAF1F31ABC8A512A13_291(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_D5B6993D49960DF01393AD8095091B12332C2FA3_292(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_D6B85A234C187D0FC82CE310F6423237C934B93A_293(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_D6D9691C10001AC45C1A8A68D4B08412F42D9F5D_294(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_D7207A48A00E499EF63FC2990337CC8B05C7AE9C_295(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_D73BDC60FA2857E7CE2F742530B056A1866C2177_296(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_D7AE4E850949E37E7359595699CCDC751E1C45E4_297(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_D8E058E700187A5AE3C7ED9D62CD09EDA04E8C70_298(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_D923777F48CDA51F21CABCED4FE36A2D620288A2_299(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_DA8D7694A42602F7DC0356F30B9E1DFF8196724A_300(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_DBF9A772127E1119D6E557AF812B6078438CE970_301(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_DC7D58FD4F690A68811E226229F74D0089F018B3_302(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_DD04181543E071971F6379A33D631064A9B051D8_303(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_DD2B45529EC6CEEBB86596D35461F63244FB3BC1_304(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_DEDEF9E8D2052710E15C0CC080004BFB33F47665_305(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_E02E3EC94504AFEEB34CE409836A3797D7E43964_306(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_E1A877959C9E369159A2433060A6781C6C5C1D5C_307(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_E1C849B129FBB4352239208AEFE34F3631A27813_308(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_E2654B2181D84518EB5EE7BC342CFFD1F528E908_309(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_E29A089A3B7A1786D6BD82DBB5C8E7E5785542ED_310(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_E2C0239EBBF1FD45925FAA7909458304DFD37A84_311(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_E32CCC113D4CCCF7DD1FB13D5F02A7BE8D1C58B2_312(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_E3F60CFE7E50B9DEE37D8E598AEF39CB00B560ED_313(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_E4728252FCD7F68FDFA39BD57E3E2CA9D7FEA1C2_314(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_E52D7E54D3429CAFB3EDE14F949FC54F5CA8DB5E_315(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_E5395170F0D79C58C3AF15E7AA917ACA2307CE5F_316(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_E5AD60E2BE68FA0155888A7E87B34F03905E7584_317(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_E62D0D793D6BCA39079E6441B162E59F33ED2B07_318(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_E6C34D5D4381C0E6151B26FB9A314D8A57292366_319(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_E9D2FA0CF959FCDF5A50AE1515251477F11EE719_320(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_EA86E31C9F6288681516E2AA22EF6B5CD441AB94_321(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_EB5C89140591CED42C39A8AE698A41F3593C3C62_322(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_EB774035880443AEFBF8B61E7760DA5273A8A44C_323(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_EBB69348CF182DB7CB8021B09F8BBC2E8E87887C_324(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_EDF6280B8AEAF6DDC736AF7B745668475CE8552F_325(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_EEADCBA5CAFE3FDC2C2B1FAEEB0B474F3E9668D5_326(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_EF1366407DE423137F5977D7AF18B0C058147698_327(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_EFAA45999AF190FC718690DC0121EB49DE7FFCE5_328(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F09F029091837E12B4E60500BED0749D79F77FC2_329(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F0B8FC0DD1D15142D7FE83F9E3295413AB138E5A_330(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F0E174E706EA1C2F8D181561F84976C942B1DE4B_331(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F100C709518544CED81B98D3F1C862F094DAF46A_332(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F1F22748A7C1A4CC79E97C2063C48D7FF070D941_333(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F20B4E4CCB47B4F0631333A4DEE30DB71336110C_334(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F240F486E15B43E152AE7F2B692A7FA361FA7776_335(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F2A80E19AE1B044B9792F5F7C2936FB5C5D8581F_336(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F3C13ED8775EDD51E351EE8A9EE972F228E4D6DE_337(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F3C6DCAA5B6BA8D1D2221D4AC63BE67D15DBDE55_338(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F44B79D8208EDC17F9C96AA7DEA4983114BF698D_339(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F5183037A4B449D12439C624D76065B310A6614A_340(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F65CD55AD5CBB0D49B45E05C5C0369616C658AB1_341(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F7A6F8B9AF2E302CB6C36EC03780155D9A9C5CC4_342(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F865498CC0010E6ACE166D01EEA92F0F3C87743D_343(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F86A89C59555BE5241482CB0FD35624F05D127CD_344(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_F975859F5AFA5A4AE7703BA3B943AFC04919E9B7_345(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_FA200C2489E9868CC920129D1E2D694172C2BA49_346(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_FA9635D8BB3CF86AE5BE0571A24C4B9534D5D575_347(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_FAD1541B3C844C3D2BDBA16B80AB090DD1621A4B_348(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_FAEB0F6FFFA8C7CB94DD23E11A954BB9D67801CA_349(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_FB977BD3A3BBAD68C2BBDBC28188F1084531E78E_350(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_FCDF24D1C633613549296EFD3DC9250D840CA996_351(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_FCF9CE737F38E152A767F1291260BF9E15F80078_352(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_FE195C5B0D58FB23FADD5D69D2F59D86FA4E7C97_353(),
	U3CPrivateImplementationDetailsU3E_tFD2CFA874AFBD986DBD771B5CE98E75CD7FCAEFC_StaticFields::get_offset_of_FE68796AD0908776C9F23F326A4181C1F53CF4DF_354(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6593 = { sizeof (__StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D6_t51B71C53A0BC4176315E48ACD4457D9A44EB4BDB ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6594 = { sizeof (__StaticArrayInitTypeSizeU3D10_tB641FD73E10817DC3A5A0B6CDD83506C3C2B53C7)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D10_tB641FD73E10817DC3A5A0B6CDD83506C3C2B53C7 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6595 = { sizeof (__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_tFBADEC3E9505138D884DDB68442D2D48D268FBF4 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6596 = { sizeof (__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D16_tE833B82A97F69BFEB7419FA93CA61B924F6F63B0 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6597 = { sizeof (__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D20_tE6AB0DAD941BE28D336223604DCCC6D73B2287B5 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6598 = { sizeof (__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_t1D2172AAD08C1ECBF6783D2872306DBB2F180436 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6599 = { sizeof (__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D26_t1300BB2EFCCD978826087890170DE54BB813F8E7 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
