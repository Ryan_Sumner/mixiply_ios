﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// GLTF.Math.Color[]
struct ColorU5BU5D_tAC03762E5A0861EA248597DDB8B645A9C927D92E;
// GLTF.Math.Matrix4x4
struct Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9;
// GLTF.Math.Matrix4x4[]
struct Matrix4x4U5BU5D_tE3A13CB78E1B2CBDCD537180E7C6D83E18B0D9F0;
// GLTF.Math.Vector2[]
struct Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35;
// GLTF.Math.Vector3[]
struct Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540;
// GLTF.Math.Vector4[]
struct Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033;
// GLTF.Schema.AccessorId
struct AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F;
// GLTF.Schema.AccessorSparseIndices
struct AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9;
// GLTF.Schema.AccessorSparseValues
struct AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3;
// GLTF.Schema.AnimationChannelTarget
struct AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3;
// GLTF.Schema.Asset
struct Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B;
// GLTF.Schema.BufferId
struct BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4;
// GLTF.Schema.BufferViewId
struct BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871;
// GLTF.Schema.CameraId
struct CameraId_t473F137B518358316FD6D1100DFF1D190E849BAC;
// GLTF.Schema.CameraOrthographic
struct CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A;
// GLTF.Schema.CameraPerspective
struct CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A;
// GLTF.Schema.DefaultExtensionFactory
struct DefaultExtensionFactory_t6F7054C0E34948CE71E4DB22B285190D70C9B3E8;
// GLTF.Schema.GLTFRoot
struct GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B;
// GLTF.Schema.ImageId
struct ImageId_t6B75A8719AF05FF5EF157163A485CAB340DDE68B;
// GLTF.Schema.MaterialCommonConstant
struct MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6;
// GLTF.Schema.MaterialId
struct MaterialId_tD6B61586B1AE066F319C2118D3B9452637A36AF2;
// GLTF.Schema.MeshId
struct MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231;
// GLTF.Schema.NodeId
struct NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0;
// GLTF.Schema.NormalTextureInfo
struct NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E;
// GLTF.Schema.OcclusionTextureInfo
struct OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999;
// GLTF.Schema.PbrMetallicRoughness
struct PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2;
// GLTF.Schema.SamplerId
struct SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4;
// GLTF.Schema.SceneId
struct SceneId_t67CF37B99AF8D98A11C03A45BDD51773EE6D605C;
// GLTF.Schema.SkinId
struct SkinId_t500172B9076F6784EF4CF0CD5CF6F87516549F02;
// GLTF.Schema.TextureId
struct TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91;
// GLTF.Schema.TextureInfo
struct TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66;
// Jint.Engine
struct Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D;
// Jint.Engine/BreakDelegate
struct BreakDelegate_tCA6935CACA104BE87718D10A6CB84A635B87F5FD;
// Jint.Engine/DebugStepDelegate
struct DebugStepDelegate_t71D4D1019B035251C57C7527AF515F8D509D1284;
// Jint.Native.Array.ArrayConstructor
struct ArrayConstructor_t3EE97DD1C34DD817D0D8540BBEC734D56BB7D9C8;
// Jint.Native.Boolean.BooleanConstructor
struct BooleanConstructor_t4500B0A4DA339E11D5E6E983A0168B8D98678CDB;
// Jint.Native.Date.DateConstructor
struct DateConstructor_t5098B976636DBEB0847A7217566BD868C59701D0;
// Jint.Native.Error.ErrorConstructor
struct ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E;
// Jint.Native.Function.EvalFunctionInstance
struct EvalFunctionInstance_t14923E8CAC9C999E8100B15A3D5D07AA4F903268;
// Jint.Native.Function.FunctionConstructor
struct FunctionConstructor_tFE3FF73EB5E5CE01E3D44BC0F4680317010CA9F5;
// Jint.Native.Global.GlobalObject
struct GlobalObject_tD5B044775D4BA82AD01F71F4F605F090811A5D27;
// Jint.Native.JsValue
struct JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19;
// Jint.Native.JsValue[]
struct JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88;
// Jint.Native.Json.JsonInstance
struct JsonInstance_tAAC3C2498512BF0ADC8391178660107A034B91AE;
// Jint.Native.Math.MathInstance
struct MathInstance_t3512482B63B6D54FC3F9105785754D409F54F4C0;
// Jint.Native.Number.NumberConstructor
struct NumberConstructor_tE5FB6BAB098D799610B0161A628216517C7ADADC;
// Jint.Native.Object.ObjectConstructor
struct ObjectConstructor_tEA6936468E07A954DA98B3D6FA258133E1992BB0;
// Jint.Native.RegExp.RegExpConstructor
struct RegExpConstructor_t959535B1C26428C9B2A92C0B914883DE4BEB7508;
// Jint.Native.String.StringConstructor
struct StringConstructor_tE617B827C95B0979B5C5D21C8F40D48D2D1C0215;
// Jint.Options
struct Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48;
// Jint.Parser.Ast.CallExpression
struct CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB;
// Jint.Parser.Ast.SyntaxNode
struct SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B;
// Jint.Parser.Location
struct Location_t9639547EECD85868A9B1627343BF1C80DE46B783;
// Jint.Runtime.CallStack.JintCallStack
struct JintCallStack_tC35F241423CC5B5C3185463D6476DF0223D133E6;
// Jint.Runtime.Debugger.DebugHandler
struct DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296;
// Jint.Runtime.Debugger.DebugInformation
struct DebugInformation_t1C019A6FE9EC7D5A367D194949D83E3E49EDCB8C;
// Jint.Runtime.Environments.LexicalEnvironment
struct LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026;
// Jint.Runtime.ExpressionInterpreter
struct ExpressionInterpreter_tD102C31F31458F6E6EDF49CFD7A7684CE208DF1F;
// Jint.Runtime.Interop.DefaultTypeConverter
struct DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087;
// Jint.Runtime.Interop.DefaultTypeConverter/<>c__DisplayClass7_2
struct U3CU3Ec__DisplayClass7_2_t660F155E66573E853DEED2D23186154AD85FA4D1;
// Jint.Runtime.Interop.IReferenceResolver
struct IReferenceResolver_t3ACCB93A57D78EB79DCD01B922CC3F49DBA0AABD;
// Jint.Runtime.Interop.ITypeConverter
struct ITypeConverter_tF0794175CEF7F1742B7DCE6B5883D6C337F592F4;
// Jint.Runtime.StatementInterpreter
struct StatementInterpreter_t09AE267789508AD9BC3E14EAFE8262642B534B44;
// Jint.Runtime.TypeConverter/<>c__DisplayClass12_0
struct U3CU3Ec__DisplayClass12_0_t47AD677AC45EF44A6929008C6F40F9BA8FE33C19;
// Newtonsoft.Json.JsonReader
struct JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC;
// Newtonsoft.Json.JsonTextReader
struct JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8;
// Newtonsoft.Json.Linq.JProperty
struct JProperty_tBC934D9DB69AF069F9684CDAA0FAC25D3804233B;
// Newtonsoft.Json.Linq.JToken
struct JToken_t06FA337591EC253506CE2A342A01C19CA990982B;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>
struct Dictionary_2_tFA101F0B1ABA5C9CC83DEA8915A01ED1E64CB071;
// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.ExtensionFactory>
struct Dictionary_2_t830F6C39663C1BE98F5067E288E43BC4DE14CB36;
// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.IExtension>
struct Dictionary_2_t0578074A603DC52F0391DEDD7ECE1AA44D3377E8;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_tC299681D95BE2E81CC7CBA606C4E9D07A00FA35B;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A;
// System.Collections.Generic.Dictionary`2<System.Type,System.Func`3<Jint.Engine,System.Object,Jint.Native.JsValue>>
struct Dictionary_2_tA1573C6F056C71CA19B9EA39F42257803E748739;
// System.Collections.Generic.IDictionary`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>
struct IDictionary_2_tA1DF5A6B6E4E121455B7694170DC28D1CBB47AED;
// System.Collections.Generic.List`1<GLTF.Schema.Accessor>
struct List_1_t1D43B87DCB1AE56CB55C174223A36A1EB6D9C49C;
// System.Collections.Generic.List`1<GLTF.Schema.AnimationChannel>
struct List_1_t9B68BE4E3D434E796C16C60C3F8E4AA066AE4FDB;
// System.Collections.Generic.List`1<GLTF.Schema.AnimationSampler>
struct List_1_t8A428600CE711C507A4972EBBEF6D04DEAA8FB65;
// System.Collections.Generic.List`1<GLTF.Schema.BufferView>
struct List_1_t0B6CA53804AD47B0392209FA3ACF7E438A089103;
// System.Collections.Generic.List`1<GLTF.Schema.GLTFAnimation>
struct List_1_tD8A55D9A512ACB5ED7DD0B60F6C698FFE3B45E31;
// System.Collections.Generic.List`1<GLTF.Schema.GLTFBuffer>
struct List_1_tF06DC5C3087205AE2F10AF9AC71D1269056533AB;
// System.Collections.Generic.List`1<GLTF.Schema.GLTFCamera>
struct List_1_t7428FD3F36775D25A7259C170DB7744093DAB21A;
// System.Collections.Generic.List`1<GLTF.Schema.GLTFImage>
struct List_1_t2011D662ECF2F170439E8C1761A7CA25340B8D4D;
// System.Collections.Generic.List`1<GLTF.Schema.GLTFMaterial>
struct List_1_t23A108C07696BA8C00032540F033FC43C906DD59;
// System.Collections.Generic.List`1<GLTF.Schema.GLTFMesh>
struct List_1_tE1B23A0E5E130978135AD98A31C0681898E3E848;
// System.Collections.Generic.List`1<GLTF.Schema.GLTFScene>
struct List_1_tAD6C1545E6E6F1ECD0B394490A03C15DB1DB3155;
// System.Collections.Generic.List`1<GLTF.Schema.GLTFTexture>
struct List_1_t672F6CB7A561409CD238343E02BBC74100D3FF6B;
// System.Collections.Generic.List`1<GLTF.Schema.MeshPrimitive>
struct List_1_t9FEC43E6B0207AA9A23659F024A1BE48ABC67A31;
// System.Collections.Generic.List`1<GLTF.Schema.Node>
struct List_1_tF08BAB889585EE649E734FE29BBA9A3054BD11E7;
// System.Collections.Generic.List`1<GLTF.Schema.NodeId>
struct List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA;
// System.Collections.Generic.List`1<GLTF.Schema.Sampler>
struct List_1_t5819984D691421754B9A3EB79CDF449D70DE4A01;
// System.Collections.Generic.List`1<GLTF.Schema.Skin>
struct List_1_tC787C2CEC6D43FC21576E063D82DB77D6A4FCED4;
// System.Collections.Generic.List`1<Jint.Runtime.Debugger.BreakPoint>
struct List_1_tDA2C9AE189EC3E3E9EA95A259117F88450997652;
// System.Collections.Generic.List`1<Jint.Runtime.Interop.IObjectConverter>
struct List_1_tC5F29A3852862B17B14A3CDFE61A42230EE4D7D9;
// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>>
struct List_1_tE97E4E597C42A24D18E09F33F48B49705CF33B72;
// System.Collections.Generic.List`1<System.Double>
struct List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E;
// System.Collections.Generic.List`1<System.Reflection.Assembly>
struct List_1_t0B17DCDEBCE728AA1CAFF9D7D1441755B5949EED;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.Stack`1<Jint.Runtime.Environments.ExecutionContext>
struct Stack_1_t021FB6425C1F751F00593574BFC67C4B8DB091D3;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`1<GLTF.Schema.Accessor>
struct Func_1_t363B49ADF53E6A044F77875E60751ED45EC2A743;
// System.Func`1<GLTF.Schema.AccessorId>
struct Func_1_t34E653644D6970D6647E85FB4AD717A595541B31;
// System.Func`1<GLTF.Schema.AnimationChannel>
struct Func_1_t5CD346831FF88268289E3D92A47C1E679FD9829E;
// System.Func`1<GLTF.Schema.AnimationSampler>
struct Func_1_tE3D849EA51FC20F5357C8A5FDB89617513F9B7C5;
// System.Func`1<GLTF.Schema.BufferView>
struct Func_1_t59223ADCE797C44CA0465E53D3A6CE78FDF09D2E;
// System.Func`1<GLTF.Schema.GLTFAnimation>
struct Func_1_tA94382267FA97DEAE4E2DF06E26D8D5A57426C7B;
// System.Func`1<GLTF.Schema.GLTFBuffer>
struct Func_1_t5FCD3BC02582AC15932E86A229CDB6A1EE11C33F;
// System.Func`1<GLTF.Schema.GLTFCamera>
struct Func_1_t0207FAD43DF76E6AAEBD421CD0B253F58F3C8F63;
// System.Func`1<GLTF.Schema.GLTFImage>
struct Func_1_tFC79610EBECD41EB2EDF2BCBA00D8D4B8C06110C;
// System.Func`1<GLTF.Schema.GLTFMaterial>
struct Func_1_tB53326FAC7CDC48D466E4765800A5EB33FEEB132;
// System.Func`1<GLTF.Schema.GLTFMesh>
struct Func_1_tF063B0F8E7EAD921914E7D247104CFFE6C12AA32;
// System.Func`1<GLTF.Schema.GLTFScene>
struct Func_1_t83F72E178B259EF62975DE4E9548CF1FB436A475;
// System.Func`1<GLTF.Schema.GLTFTexture>
struct Func_1_t241277EA0AF5C84B76839D7AA2461F45AD73D043;
// System.Func`1<GLTF.Schema.MeshPrimitive>
struct Func_1_t53E28CADA7E8F64F765D22B7FAC985DA37F42E1A;
// System.Func`1<GLTF.Schema.Node>
struct Func_1_t95D10E9568FF8790C758334F75873BFAFA0123BB;
// System.Func`1<GLTF.Schema.Sampler>
struct Func_1_tB4A197565B93CA65344E5A8D31E9963120CD575C;
// System.Func`1<GLTF.Schema.Skin>
struct Func_1_t9EC8688B671B237B45177417F594E3E519D1FD97;
// System.Func`1<System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>>
struct Func_1_t066DA7D04B09BA10D9A665179D37E5A0EA4B81AD;
// System.Func`2<Jint.Native.JsValue,Jint.Native.JsValue>
struct Func_2_t7AC4704B1010794CE0FF7D63E97681D54F52A3C9;
// System.Func`2<Jint.Native.JsValue,System.Object>
struct Func_2_tF2150A815C34C577F510DEC721A30AA620438CF2;
// System.Func`2<Jint.Parser.Ast.Expression,System.Boolean>
struct Func_2_t3F6C36BB02A8D91C7BA8CA6041BCFA7E6CB98089;
// System.Func`2<Jint.Parser.Ast.VariableDeclaration,System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.VariableDeclarator>>
struct Func_2_tF0A1B416C0AA3DF3C7220B91CA0A5044C8646C5E;
// System.Func`2<System.Reflection.ParameterInfo,System.Boolean>
struct Func_2_t61E54F48AD8F1DC354B46A3826709996DB41375A;
// System.Func`3<Jint.Native.JsValue,Jint.Native.JsValue[],Jint.Native.JsValue>
struct Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IFormatProvider
struct IFormatProvider_t4247E13AE2D97A079B88D594B7ABABF313259901;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Predicate`1<System.Exception>
struct Predicate_1_t26A4B8466526D596ABAD09AEC4CE1E40FB5CC700;
// System.Reflection.MethodBase
struct MethodBase_t;
// System.Reflection.MethodBase[]
struct MethodBaseU5BU5D_t57FAB09E20C4CE1DD48A19FB3E74821FDFD34914;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.TimeZoneInfo
struct TimeZoneInfo_t46EF9BAEAA787846F1A1EC419BE75CFEFAFF6777;
// System.Type
struct Type_t;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 ;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8 ;
struct Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C ;
struct Vector4_t239657374664B132BBB44122F237F461D91809ED ;



#ifndef U3CMODULEU3E_T74F25D240759C02997A4700D8A698D8EA73AA7B8_H
#define U3CMODULEU3E_T74F25D240759C02997A4700D8A698D8EA73AA7B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t74F25D240759C02997A4700D8A698D8EA73AA7B8 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T74F25D240759C02997A4700D8A698D8EA73AA7B8_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3408FB995FEF331FE68B64E76A23640867228A02_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3408FB995FEF331FE68B64E76A23640867228A02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3408FB995FEF331FE68B64E76A23640867228A02  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3408FB995FEF331FE68B64E76A23640867228A02_H
#ifndef DEFAULTEXTENSION_TC6AFCFA81A2D287E9664F49E49A9DA580F76FBAD_H
#define DEFAULTEXTENSION_TC6AFCFA81A2D287E9664F49E49A9DA580F76FBAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.DefaultExtension
struct  DefaultExtension_tC6AFCFA81A2D287E9664F49E49A9DA580F76FBAD  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JProperty GLTF.Schema.DefaultExtension::<ExtensionData>k__BackingField
	JProperty_tBC934D9DB69AF069F9684CDAA0FAC25D3804233B * ___U3CExtensionDataU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CExtensionDataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DefaultExtension_tC6AFCFA81A2D287E9664F49E49A9DA580F76FBAD, ___U3CExtensionDataU3Ek__BackingField_0)); }
	inline JProperty_tBC934D9DB69AF069F9684CDAA0FAC25D3804233B * get_U3CExtensionDataU3Ek__BackingField_0() const { return ___U3CExtensionDataU3Ek__BackingField_0; }
	inline JProperty_tBC934D9DB69AF069F9684CDAA0FAC25D3804233B ** get_address_of_U3CExtensionDataU3Ek__BackingField_0() { return &___U3CExtensionDataU3Ek__BackingField_0; }
	inline void set_U3CExtensionDataU3Ek__BackingField_0(JProperty_tBC934D9DB69AF069F9684CDAA0FAC25D3804233B * value)
	{
		___U3CExtensionDataU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionDataU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTEXTENSION_TC6AFCFA81A2D287E9664F49E49A9DA580F76FBAD_H
#ifndef EXTENSIONFACTORY_T9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7_H
#define EXTENSIONFACTORY_T9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.ExtensionFactory
struct  ExtensionFactory_t9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7  : public RuntimeObject
{
public:
	// System.String GLTF.Schema.ExtensionFactory::ExtensionName
	String_t* ___ExtensionName_0;

public:
	inline static int32_t get_offset_of_ExtensionName_0() { return static_cast<int32_t>(offsetof(ExtensionFactory_t9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7, ___ExtensionName_0)); }
	inline String_t* get_ExtensionName_0() const { return ___ExtensionName_0; }
	inline String_t** get_address_of_ExtensionName_0() { return &___ExtensionName_0; }
	inline void set_ExtensionName_0(String_t* value)
	{
		___ExtensionName_0 = value;
		Il2CppCodeGenWriteBarrier((&___ExtensionName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONFACTORY_T9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_TCBC9C5921528275966BC1DC33758B464100BDD92_H
#define U3CU3EC__DISPLAYCLASS2_0_TCBC9C5921528275966BC1DC33758B464100BDD92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFAnimation/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_tCBC9C5921528275966BC1DC33758B464100BDD92  : public RuntimeObject
{
public:
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFAnimation/<>c__DisplayClass2_0::root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___root_0;
	// Newtonsoft.Json.JsonReader GLTF.Schema.GLTFAnimation/<>c__DisplayClass2_0::reader
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC * ___reader_1;
	// System.Func`1<GLTF.Schema.AnimationChannel> GLTF.Schema.GLTFAnimation/<>c__DisplayClass2_0::<>9__0
	Func_1_t5CD346831FF88268289E3D92A47C1E679FD9829E * ___U3CU3E9__0_2;
	// System.Func`1<GLTF.Schema.AnimationSampler> GLTF.Schema.GLTFAnimation/<>c__DisplayClass2_0::<>9__1
	Func_1_tE3D849EA51FC20F5357C8A5FDB89617513F9B7C5 * ___U3CU3E9__1_3;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tCBC9C5921528275966BC1DC33758B464100BDD92, ___root_0)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_root_0() const { return ___root_0; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tCBC9C5921528275966BC1DC33758B464100BDD92, ___reader_1)); }
	inline JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC * get_reader_1() const { return ___reader_1; }
	inline JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___reader_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tCBC9C5921528275966BC1DC33758B464100BDD92, ___U3CU3E9__0_2)); }
	inline Func_1_t5CD346831FF88268289E3D92A47C1E679FD9829E * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Func_1_t5CD346831FF88268289E3D92A47C1E679FD9829E ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Func_1_t5CD346831FF88268289E3D92A47C1E679FD9829E * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tCBC9C5921528275966BC1DC33758B464100BDD92, ___U3CU3E9__1_3)); }
	inline Func_1_tE3D849EA51FC20F5357C8A5FDB89617513F9B7C5 * get_U3CU3E9__1_3() const { return ___U3CU3E9__1_3; }
	inline Func_1_tE3D849EA51FC20F5357C8A5FDB89617513F9B7C5 ** get_address_of_U3CU3E9__1_3() { return &___U3CU3E9__1_3; }
	inline void set_U3CU3E9__1_3(Func_1_tE3D849EA51FC20F5357C8A5FDB89617513F9B7C5 * value)
	{
		___U3CU3E9__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_TCBC9C5921528275966BC1DC33758B464100BDD92_H
#ifndef GLTFID_1_T03C6D4307DA5F7D32A8A310F6F43A4992728AEC7_H
#define GLTFID_1_T03C6D4307DA5F7D32A8A310F6F43A4992728AEC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.Accessor>
struct  GLTFId_1_t03C6D4307DA5F7D32A8A310F6F43A4992728AEC7  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t03C6D4307DA5F7D32A8A310F6F43A4992728AEC7, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t03C6D4307DA5F7D32A8A310F6F43A4992728AEC7, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T03C6D4307DA5F7D32A8A310F6F43A4992728AEC7_H
#ifndef GLTFID_1_T42D30DE0867573822EFF276155616B8DA56E3E8F_H
#define GLTFID_1_T42D30DE0867573822EFF276155616B8DA56E3E8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.BufferView>
struct  GLTFId_1_t42D30DE0867573822EFF276155616B8DA56E3E8F  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t42D30DE0867573822EFF276155616B8DA56E3E8F, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t42D30DE0867573822EFF276155616B8DA56E3E8F, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T42D30DE0867573822EFF276155616B8DA56E3E8F_H
#ifndef GLTFID_1_T050A1D4340BD22B23EED14DBBBDEE97C66D39D50_H
#define GLTFID_1_T050A1D4340BD22B23EED14DBBBDEE97C66D39D50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.GLTFBuffer>
struct  GLTFId_1_t050A1D4340BD22B23EED14DBBBDEE97C66D39D50  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t050A1D4340BD22B23EED14DBBBDEE97C66D39D50, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t050A1D4340BD22B23EED14DBBBDEE97C66D39D50, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T050A1D4340BD22B23EED14DBBBDEE97C66D39D50_H
#ifndef GLTFID_1_T7D04B2FBE14BCDA08AD90BBDE17E15826C0C7609_H
#define GLTFID_1_T7D04B2FBE14BCDA08AD90BBDE17E15826C0C7609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.GLTFCamera>
struct  GLTFId_1_t7D04B2FBE14BCDA08AD90BBDE17E15826C0C7609  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t7D04B2FBE14BCDA08AD90BBDE17E15826C0C7609, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t7D04B2FBE14BCDA08AD90BBDE17E15826C0C7609, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T7D04B2FBE14BCDA08AD90BBDE17E15826C0C7609_H
#ifndef GLTFID_1_T2D2234F7D6DF10992093F7D9A1B129AE76F1D761_H
#define GLTFID_1_T2D2234F7D6DF10992093F7D9A1B129AE76F1D761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.GLTFImage>
struct  GLTFId_1_t2D2234F7D6DF10992093F7D9A1B129AE76F1D761  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t2D2234F7D6DF10992093F7D9A1B129AE76F1D761, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t2D2234F7D6DF10992093F7D9A1B129AE76F1D761, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T2D2234F7D6DF10992093F7D9A1B129AE76F1D761_H
#ifndef GLTFID_1_TCD0A6B945F8FBA41667A444A94C374C2CEFD918A_H
#define GLTFID_1_TCD0A6B945F8FBA41667A444A94C374C2CEFD918A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.GLTFMaterial>
struct  GLTFId_1_tCD0A6B945F8FBA41667A444A94C374C2CEFD918A  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_tCD0A6B945F8FBA41667A444A94C374C2CEFD918A, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_tCD0A6B945F8FBA41667A444A94C374C2CEFD918A, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_TCD0A6B945F8FBA41667A444A94C374C2CEFD918A_H
#ifndef GLTFID_1_T98733FC08E44C4842A58F514BA48F0C4AED0597A_H
#define GLTFID_1_T98733FC08E44C4842A58F514BA48F0C4AED0597A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.GLTFMesh>
struct  GLTFId_1_t98733FC08E44C4842A58F514BA48F0C4AED0597A  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t98733FC08E44C4842A58F514BA48F0C4AED0597A, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t98733FC08E44C4842A58F514BA48F0C4AED0597A, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T98733FC08E44C4842A58F514BA48F0C4AED0597A_H
#ifndef GLTFID_1_T4F80A8748E825E378AAB535C5F99CF020CEF74C1_H
#define GLTFID_1_T4F80A8748E825E378AAB535C5F99CF020CEF74C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.GLTFScene>
struct  GLTFId_1_t4F80A8748E825E378AAB535C5F99CF020CEF74C1  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t4F80A8748E825E378AAB535C5F99CF020CEF74C1, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t4F80A8748E825E378AAB535C5F99CF020CEF74C1, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T4F80A8748E825E378AAB535C5F99CF020CEF74C1_H
#ifndef GLTFID_1_TD8AB7490D46668FF3AB24325C4412B6D597D849B_H
#define GLTFID_1_TD8AB7490D46668FF3AB24325C4412B6D597D849B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.GLTFTexture>
struct  GLTFId_1_tD8AB7490D46668FF3AB24325C4412B6D597D849B  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_tD8AB7490D46668FF3AB24325C4412B6D597D849B, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_tD8AB7490D46668FF3AB24325C4412B6D597D849B, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_TD8AB7490D46668FF3AB24325C4412B6D597D849B_H
#ifndef GLTFID_1_T03A4FA3C937E0B4939DA3B8F26A7FFF95A506783_H
#define GLTFID_1_T03A4FA3C937E0B4939DA3B8F26A7FFF95A506783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.Node>
struct  GLTFId_1_t03A4FA3C937E0B4939DA3B8F26A7FFF95A506783  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t03A4FA3C937E0B4939DA3B8F26A7FFF95A506783, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t03A4FA3C937E0B4939DA3B8F26A7FFF95A506783, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T03A4FA3C937E0B4939DA3B8F26A7FFF95A506783_H
#ifndef GLTFID_1_T76D4B531E6B79E90DC1A4CC5B684341E28AA04C6_H
#define GLTFID_1_T76D4B531E6B79E90DC1A4CC5B684341E28AA04C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.Sampler>
struct  GLTFId_1_t76D4B531E6B79E90DC1A4CC5B684341E28AA04C6  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t76D4B531E6B79E90DC1A4CC5B684341E28AA04C6, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t76D4B531E6B79E90DC1A4CC5B684341E28AA04C6, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T76D4B531E6B79E90DC1A4CC5B684341E28AA04C6_H
#ifndef GLTFID_1_TFEAB99802FD43E2656D9B894A370966029B54F6C_H
#define GLTFID_1_TFEAB99802FD43E2656D9B894A370966029B54F6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.Skin>
struct  GLTFId_1_tFEAB99802FD43E2656D9B894A370966029B54F6C  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_tFEAB99802FD43E2656D9B894A370966029B54F6C, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_tFEAB99802FD43E2656D9B894A370966029B54F6C, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_TFEAB99802FD43E2656D9B894A370966029B54F6C_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T079FEDDF0C13DB6738A275D9130A8EE28D7D6082_H
#define U3CU3EC__DISPLAYCLASS4_0_T079FEDDF0C13DB6738A275D9130A8EE28D7D6082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFMesh/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t079FEDDF0C13DB6738A275D9130A8EE28D7D6082  : public RuntimeObject
{
public:
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFMesh/<>c__DisplayClass4_0::root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___root_0;
	// Newtonsoft.Json.JsonReader GLTF.Schema.GLTFMesh/<>c__DisplayClass4_0::reader
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC * ___reader_1;
	// System.Func`1<GLTF.Schema.MeshPrimitive> GLTF.Schema.GLTFMesh/<>c__DisplayClass4_0::<>9__0
	Func_1_t53E28CADA7E8F64F765D22B7FAC985DA37F42E1A * ___U3CU3E9__0_2;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t079FEDDF0C13DB6738A275D9130A8EE28D7D6082, ___root_0)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_root_0() const { return ___root_0; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t079FEDDF0C13DB6738A275D9130A8EE28D7D6082, ___reader_1)); }
	inline JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC * get_reader_1() const { return ___reader_1; }
	inline JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___reader_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t079FEDDF0C13DB6738A275D9130A8EE28D7D6082, ___U3CU3E9__0_2)); }
	inline Func_1_t53E28CADA7E8F64F765D22B7FAC985DA37F42E1A * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Func_1_t53E28CADA7E8F64F765D22B7FAC985DA37F42E1A ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Func_1_t53E28CADA7E8F64F765D22B7FAC985DA37F42E1A * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T079FEDDF0C13DB6738A275D9130A8EE28D7D6082_H
#ifndef GLTFPROPERTY_T40AFD52A8EF43AFD933314F5D289DD0109492979_H
#define GLTFPROPERTY_T40AFD52A8EF43AFD933314F5D289DD0109492979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFProperty
struct  GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.IExtension> GLTF.Schema.GLTFProperty::Extensions
	Dictionary_2_t0578074A603DC52F0391DEDD7ECE1AA44D3377E8 * ___Extensions_2;
	// Newtonsoft.Json.Linq.JToken GLTF.Schema.GLTFProperty::Extras
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B * ___Extras_3;

public:
	inline static int32_t get_offset_of_Extensions_2() { return static_cast<int32_t>(offsetof(GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979, ___Extensions_2)); }
	inline Dictionary_2_t0578074A603DC52F0391DEDD7ECE1AA44D3377E8 * get_Extensions_2() const { return ___Extensions_2; }
	inline Dictionary_2_t0578074A603DC52F0391DEDD7ECE1AA44D3377E8 ** get_address_of_Extensions_2() { return &___Extensions_2; }
	inline void set_Extensions_2(Dictionary_2_t0578074A603DC52F0391DEDD7ECE1AA44D3377E8 * value)
	{
		___Extensions_2 = value;
		Il2CppCodeGenWriteBarrier((&___Extensions_2), value);
	}

	inline static int32_t get_offset_of_Extras_3() { return static_cast<int32_t>(offsetof(GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979, ___Extras_3)); }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B * get_Extras_3() const { return ___Extras_3; }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B ** get_address_of_Extras_3() { return &___Extras_3; }
	inline void set_Extras_3(JToken_t06FA337591EC253506CE2A342A01C19CA990982B * value)
	{
		___Extras_3 = value;
		Il2CppCodeGenWriteBarrier((&___Extras_3), value);
	}
};

struct GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.ExtensionFactory> GLTF.Schema.GLTFProperty::_extensionRegistry
	Dictionary_2_t830F6C39663C1BE98F5067E288E43BC4DE14CB36 * ____extensionRegistry_0;
	// GLTF.Schema.DefaultExtensionFactory GLTF.Schema.GLTFProperty::_defaultExtensionFactory
	DefaultExtensionFactory_t6F7054C0E34948CE71E4DB22B285190D70C9B3E8 * ____defaultExtensionFactory_1;

public:
	inline static int32_t get_offset_of__extensionRegistry_0() { return static_cast<int32_t>(offsetof(GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields, ____extensionRegistry_0)); }
	inline Dictionary_2_t830F6C39663C1BE98F5067E288E43BC4DE14CB36 * get__extensionRegistry_0() const { return ____extensionRegistry_0; }
	inline Dictionary_2_t830F6C39663C1BE98F5067E288E43BC4DE14CB36 ** get_address_of__extensionRegistry_0() { return &____extensionRegistry_0; }
	inline void set__extensionRegistry_0(Dictionary_2_t830F6C39663C1BE98F5067E288E43BC4DE14CB36 * value)
	{
		____extensionRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____extensionRegistry_0), value);
	}

	inline static int32_t get_offset_of__defaultExtensionFactory_1() { return static_cast<int32_t>(offsetof(GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields, ____defaultExtensionFactory_1)); }
	inline DefaultExtensionFactory_t6F7054C0E34948CE71E4DB22B285190D70C9B3E8 * get__defaultExtensionFactory_1() const { return ____defaultExtensionFactory_1; }
	inline DefaultExtensionFactory_t6F7054C0E34948CE71E4DB22B285190D70C9B3E8 ** get_address_of__defaultExtensionFactory_1() { return &____defaultExtensionFactory_1; }
	inline void set__defaultExtensionFactory_1(DefaultExtensionFactory_t6F7054C0E34948CE71E4DB22B285190D70C9B3E8 * value)
	{
		____defaultExtensionFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&____defaultExtensionFactory_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFPROPERTY_T40AFD52A8EF43AFD933314F5D289DD0109492979_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_TA33076FF6567D91AE6765179BE8BE2D7DD746396_H
#define U3CU3EC__DISPLAYCLASS21_0_TA33076FF6567D91AE6765179BE8BE2D7DD746396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396  : public RuntimeObject
{
public:
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0::root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___root_0;
	// Newtonsoft.Json.JsonTextReader GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0::jsonReader
	JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8 * ___jsonReader_1;
	// System.Func`1<GLTF.Schema.Accessor> GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0::<>9__0
	Func_1_t363B49ADF53E6A044F77875E60751ED45EC2A743 * ___U3CU3E9__0_2;
	// System.Func`1<GLTF.Schema.GLTFAnimation> GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0::<>9__1
	Func_1_tA94382267FA97DEAE4E2DF06E26D8D5A57426C7B * ___U3CU3E9__1_3;
	// System.Func`1<GLTF.Schema.GLTFBuffer> GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0::<>9__2
	Func_1_t5FCD3BC02582AC15932E86A229CDB6A1EE11C33F * ___U3CU3E9__2_4;
	// System.Func`1<GLTF.Schema.BufferView> GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0::<>9__3
	Func_1_t59223ADCE797C44CA0465E53D3A6CE78FDF09D2E * ___U3CU3E9__3_5;
	// System.Func`1<GLTF.Schema.GLTFCamera> GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0::<>9__4
	Func_1_t0207FAD43DF76E6AAEBD421CD0B253F58F3C8F63 * ___U3CU3E9__4_6;
	// System.Func`1<GLTF.Schema.GLTFImage> GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0::<>9__5
	Func_1_tFC79610EBECD41EB2EDF2BCBA00D8D4B8C06110C * ___U3CU3E9__5_7;
	// System.Func`1<GLTF.Schema.GLTFMaterial> GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0::<>9__6
	Func_1_tB53326FAC7CDC48D466E4765800A5EB33FEEB132 * ___U3CU3E9__6_8;
	// System.Func`1<GLTF.Schema.GLTFMesh> GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0::<>9__7
	Func_1_tF063B0F8E7EAD921914E7D247104CFFE6C12AA32 * ___U3CU3E9__7_9;
	// System.Func`1<GLTF.Schema.Node> GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0::<>9__8
	Func_1_t95D10E9568FF8790C758334F75873BFAFA0123BB * ___U3CU3E9__8_10;
	// System.Func`1<GLTF.Schema.Sampler> GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0::<>9__9
	Func_1_tB4A197565B93CA65344E5A8D31E9963120CD575C * ___U3CU3E9__9_11;
	// System.Func`1<GLTF.Schema.GLTFScene> GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0::<>9__10
	Func_1_t83F72E178B259EF62975DE4E9548CF1FB436A475 * ___U3CU3E9__10_12;
	// System.Func`1<GLTF.Schema.Skin> GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0::<>9__11
	Func_1_t9EC8688B671B237B45177417F594E3E519D1FD97 * ___U3CU3E9__11_13;
	// System.Func`1<GLTF.Schema.GLTFTexture> GLTF.Schema.GLTFRoot/<>c__DisplayClass21_0::<>9__12
	Func_1_t241277EA0AF5C84B76839D7AA2461F45AD73D043 * ___U3CU3E9__12_14;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396, ___root_0)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_root_0() const { return ___root_0; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_jsonReader_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396, ___jsonReader_1)); }
	inline JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8 * get_jsonReader_1() const { return ___jsonReader_1; }
	inline JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8 ** get_address_of_jsonReader_1() { return &___jsonReader_1; }
	inline void set_jsonReader_1(JsonTextReader_t237A2776CC59E2840A79D31EBD665B7617709BE8 * value)
	{
		___jsonReader_1 = value;
		Il2CppCodeGenWriteBarrier((&___jsonReader_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396, ___U3CU3E9__0_2)); }
	inline Func_1_t363B49ADF53E6A044F77875E60751ED45EC2A743 * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Func_1_t363B49ADF53E6A044F77875E60751ED45EC2A743 ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Func_1_t363B49ADF53E6A044F77875E60751ED45EC2A743 * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396, ___U3CU3E9__1_3)); }
	inline Func_1_tA94382267FA97DEAE4E2DF06E26D8D5A57426C7B * get_U3CU3E9__1_3() const { return ___U3CU3E9__1_3; }
	inline Func_1_tA94382267FA97DEAE4E2DF06E26D8D5A57426C7B ** get_address_of_U3CU3E9__1_3() { return &___U3CU3E9__1_3; }
	inline void set_U3CU3E9__1_3(Func_1_tA94382267FA97DEAE4E2DF06E26D8D5A57426C7B * value)
	{
		___U3CU3E9__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396, ___U3CU3E9__2_4)); }
	inline Func_1_t5FCD3BC02582AC15932E86A229CDB6A1EE11C33F * get_U3CU3E9__2_4() const { return ___U3CU3E9__2_4; }
	inline Func_1_t5FCD3BC02582AC15932E86A229CDB6A1EE11C33F ** get_address_of_U3CU3E9__2_4() { return &___U3CU3E9__2_4; }
	inline void set_U3CU3E9__2_4(Func_1_t5FCD3BC02582AC15932E86A229CDB6A1EE11C33F * value)
	{
		___U3CU3E9__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396, ___U3CU3E9__3_5)); }
	inline Func_1_t59223ADCE797C44CA0465E53D3A6CE78FDF09D2E * get_U3CU3E9__3_5() const { return ___U3CU3E9__3_5; }
	inline Func_1_t59223ADCE797C44CA0465E53D3A6CE78FDF09D2E ** get_address_of_U3CU3E9__3_5() { return &___U3CU3E9__3_5; }
	inline void set_U3CU3E9__3_5(Func_1_t59223ADCE797C44CA0465E53D3A6CE78FDF09D2E * value)
	{
		___U3CU3E9__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396, ___U3CU3E9__4_6)); }
	inline Func_1_t0207FAD43DF76E6AAEBD421CD0B253F58F3C8F63 * get_U3CU3E9__4_6() const { return ___U3CU3E9__4_6; }
	inline Func_1_t0207FAD43DF76E6AAEBD421CD0B253F58F3C8F63 ** get_address_of_U3CU3E9__4_6() { return &___U3CU3E9__4_6; }
	inline void set_U3CU3E9__4_6(Func_1_t0207FAD43DF76E6AAEBD421CD0B253F58F3C8F63 * value)
	{
		___U3CU3E9__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396, ___U3CU3E9__5_7)); }
	inline Func_1_tFC79610EBECD41EB2EDF2BCBA00D8D4B8C06110C * get_U3CU3E9__5_7() const { return ___U3CU3E9__5_7; }
	inline Func_1_tFC79610EBECD41EB2EDF2BCBA00D8D4B8C06110C ** get_address_of_U3CU3E9__5_7() { return &___U3CU3E9__5_7; }
	inline void set_U3CU3E9__5_7(Func_1_tFC79610EBECD41EB2EDF2BCBA00D8D4B8C06110C * value)
	{
		___U3CU3E9__5_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396, ___U3CU3E9__6_8)); }
	inline Func_1_tB53326FAC7CDC48D466E4765800A5EB33FEEB132 * get_U3CU3E9__6_8() const { return ___U3CU3E9__6_8; }
	inline Func_1_tB53326FAC7CDC48D466E4765800A5EB33FEEB132 ** get_address_of_U3CU3E9__6_8() { return &___U3CU3E9__6_8; }
	inline void set_U3CU3E9__6_8(Func_1_tB53326FAC7CDC48D466E4765800A5EB33FEEB132 * value)
	{
		___U3CU3E9__6_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396, ___U3CU3E9__7_9)); }
	inline Func_1_tF063B0F8E7EAD921914E7D247104CFFE6C12AA32 * get_U3CU3E9__7_9() const { return ___U3CU3E9__7_9; }
	inline Func_1_tF063B0F8E7EAD921914E7D247104CFFE6C12AA32 ** get_address_of_U3CU3E9__7_9() { return &___U3CU3E9__7_9; }
	inline void set_U3CU3E9__7_9(Func_1_tF063B0F8E7EAD921914E7D247104CFFE6C12AA32 * value)
	{
		___U3CU3E9__7_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_10() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396, ___U3CU3E9__8_10)); }
	inline Func_1_t95D10E9568FF8790C758334F75873BFAFA0123BB * get_U3CU3E9__8_10() const { return ___U3CU3E9__8_10; }
	inline Func_1_t95D10E9568FF8790C758334F75873BFAFA0123BB ** get_address_of_U3CU3E9__8_10() { return &___U3CU3E9__8_10; }
	inline void set_U3CU3E9__8_10(Func_1_t95D10E9568FF8790C758334F75873BFAFA0123BB * value)
	{
		___U3CU3E9__8_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_10), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_11() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396, ___U3CU3E9__9_11)); }
	inline Func_1_tB4A197565B93CA65344E5A8D31E9963120CD575C * get_U3CU3E9__9_11() const { return ___U3CU3E9__9_11; }
	inline Func_1_tB4A197565B93CA65344E5A8D31E9963120CD575C ** get_address_of_U3CU3E9__9_11() { return &___U3CU3E9__9_11; }
	inline void set_U3CU3E9__9_11(Func_1_tB4A197565B93CA65344E5A8D31E9963120CD575C * value)
	{
		___U3CU3E9__9_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__9_11), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_12() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396, ___U3CU3E9__10_12)); }
	inline Func_1_t83F72E178B259EF62975DE4E9548CF1FB436A475 * get_U3CU3E9__10_12() const { return ___U3CU3E9__10_12; }
	inline Func_1_t83F72E178B259EF62975DE4E9548CF1FB436A475 ** get_address_of_U3CU3E9__10_12() { return &___U3CU3E9__10_12; }
	inline void set_U3CU3E9__10_12(Func_1_t83F72E178B259EF62975DE4E9548CF1FB436A475 * value)
	{
		___U3CU3E9__10_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__10_12), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_13() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396, ___U3CU3E9__11_13)); }
	inline Func_1_t9EC8688B671B237B45177417F594E3E519D1FD97 * get_U3CU3E9__11_13() const { return ___U3CU3E9__11_13; }
	inline Func_1_t9EC8688B671B237B45177417F594E3E519D1FD97 ** get_address_of_U3CU3E9__11_13() { return &___U3CU3E9__11_13; }
	inline void set_U3CU3E9__11_13(Func_1_t9EC8688B671B237B45177417F594E3E519D1FD97 * value)
	{
		___U3CU3E9__11_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__11_13), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_14() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396, ___U3CU3E9__12_14)); }
	inline Func_1_t241277EA0AF5C84B76839D7AA2461F45AD73D043 * get_U3CU3E9__12_14() const { return ___U3CU3E9__12_14; }
	inline Func_1_t241277EA0AF5C84B76839D7AA2461F45AD73D043 ** get_address_of_U3CU3E9__12_14() { return &___U3CU3E9__12_14; }
	inline void set_U3CU3E9__12_14(Func_1_t241277EA0AF5C84B76839D7AA2461F45AD73D043 * value)
	{
		___U3CU3E9__12_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_TA33076FF6567D91AE6765179BE8BE2D7DD746396_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_TCDC0E1118521C31A55EBD5738406E9F590434323_H
#define U3CU3EC__DISPLAYCLASS8_0_TCDC0E1118521C31A55EBD5738406E9F590434323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MeshPrimitive/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323  : public RuntimeObject
{
public:
	// Newtonsoft.Json.JsonReader GLTF.Schema.MeshPrimitive/<>c__DisplayClass8_0::reader
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC * ___reader_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.MeshPrimitive/<>c__DisplayClass8_0::root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___root_1;
	// System.Func`1<GLTF.Schema.AccessorId> GLTF.Schema.MeshPrimitive/<>c__DisplayClass8_0::<>9__0
	Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 * ___U3CU3E9__0_2;
	// System.Func`1<GLTF.Schema.AccessorId> GLTF.Schema.MeshPrimitive/<>c__DisplayClass8_0::<>9__2
	Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 * ___U3CU3E9__2_3;
	// System.Func`1<System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>> GLTF.Schema.MeshPrimitive/<>c__DisplayClass8_0::<>9__1
	Func_1_t066DA7D04B09BA10D9A665179D37E5A0EA4B81AD * ___U3CU3E9__1_4;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323, ___reader_0)); }
	inline JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC * get_reader_0() const { return ___reader_0; }
	inline JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier((&___reader_0), value);
	}

	inline static int32_t get_offset_of_root_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323, ___root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_root_1() const { return ___root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_root_1() { return &___root_1; }
	inline void set_root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___root_1 = value;
		Il2CppCodeGenWriteBarrier((&___root_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323, ___U3CU3E9__0_2)); }
	inline Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323, ___U3CU3E9__2_3)); }
	inline Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 * get_U3CU3E9__2_3() const { return ___U3CU3E9__2_3; }
	inline Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 ** get_address_of_U3CU3E9__2_3() { return &___U3CU3E9__2_3; }
	inline void set_U3CU3E9__2_3(Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 * value)
	{
		___U3CU3E9__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323, ___U3CU3E9__1_4)); }
	inline Func_1_t066DA7D04B09BA10D9A665179D37E5A0EA4B81AD * get_U3CU3E9__1_4() const { return ___U3CU3E9__1_4; }
	inline Func_1_t066DA7D04B09BA10D9A665179D37E5A0EA4B81AD ** get_address_of_U3CU3E9__1_4() { return &___U3CU3E9__1_4; }
	inline void set_U3CU3E9__1_4(Func_1_t066DA7D04B09BA10D9A665179D37E5A0EA4B81AD * value)
	{
		___U3CU3E9__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_TCDC0E1118521C31A55EBD5738406E9F590434323_H
#ifndef SEMANTICPROPERTIES_T8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_H
#define SEMANTICPROPERTIES_T8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.SemanticProperties
struct  SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A  : public RuntimeObject
{
public:

public:
};

struct SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields
{
public:
	// System.String GLTF.Schema.SemanticProperties::POSITION
	String_t* ___POSITION_0;
	// System.String GLTF.Schema.SemanticProperties::NORMAL
	String_t* ___NORMAL_1;
	// System.String GLTF.Schema.SemanticProperties::JOINT
	String_t* ___JOINT_2;
	// System.String GLTF.Schema.SemanticProperties::WEIGHT
	String_t* ___WEIGHT_3;
	// System.String GLTF.Schema.SemanticProperties::TANGENT
	String_t* ___TANGENT_4;
	// System.String GLTF.Schema.SemanticProperties::INDICES
	String_t* ___INDICES_5;

public:
	inline static int32_t get_offset_of_POSITION_0() { return static_cast<int32_t>(offsetof(SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields, ___POSITION_0)); }
	inline String_t* get_POSITION_0() const { return ___POSITION_0; }
	inline String_t** get_address_of_POSITION_0() { return &___POSITION_0; }
	inline void set_POSITION_0(String_t* value)
	{
		___POSITION_0 = value;
		Il2CppCodeGenWriteBarrier((&___POSITION_0), value);
	}

	inline static int32_t get_offset_of_NORMAL_1() { return static_cast<int32_t>(offsetof(SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields, ___NORMAL_1)); }
	inline String_t* get_NORMAL_1() const { return ___NORMAL_1; }
	inline String_t** get_address_of_NORMAL_1() { return &___NORMAL_1; }
	inline void set_NORMAL_1(String_t* value)
	{
		___NORMAL_1 = value;
		Il2CppCodeGenWriteBarrier((&___NORMAL_1), value);
	}

	inline static int32_t get_offset_of_JOINT_2() { return static_cast<int32_t>(offsetof(SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields, ___JOINT_2)); }
	inline String_t* get_JOINT_2() const { return ___JOINT_2; }
	inline String_t** get_address_of_JOINT_2() { return &___JOINT_2; }
	inline void set_JOINT_2(String_t* value)
	{
		___JOINT_2 = value;
		Il2CppCodeGenWriteBarrier((&___JOINT_2), value);
	}

	inline static int32_t get_offset_of_WEIGHT_3() { return static_cast<int32_t>(offsetof(SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields, ___WEIGHT_3)); }
	inline String_t* get_WEIGHT_3() const { return ___WEIGHT_3; }
	inline String_t** get_address_of_WEIGHT_3() { return &___WEIGHT_3; }
	inline void set_WEIGHT_3(String_t* value)
	{
		___WEIGHT_3 = value;
		Il2CppCodeGenWriteBarrier((&___WEIGHT_3), value);
	}

	inline static int32_t get_offset_of_TANGENT_4() { return static_cast<int32_t>(offsetof(SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields, ___TANGENT_4)); }
	inline String_t* get_TANGENT_4() const { return ___TANGENT_4; }
	inline String_t** get_address_of_TANGENT_4() { return &___TANGENT_4; }
	inline void set_TANGENT_4(String_t* value)
	{
		___TANGENT_4 = value;
		Il2CppCodeGenWriteBarrier((&___TANGENT_4), value);
	}

	inline static int32_t get_offset_of_INDICES_5() { return static_cast<int32_t>(offsetof(SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields, ___INDICES_5)); }
	inline String_t* get_INDICES_5() const { return ___INDICES_5; }
	inline String_t** get_address_of_INDICES_5() { return &___INDICES_5; }
	inline void set_INDICES_5(String_t* value)
	{
		___INDICES_5 = value;
		Il2CppCodeGenWriteBarrier((&___INDICES_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEMANTICPROPERTIES_T8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_H
#ifndef ENGINE_T8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D_H
#define ENGINE_T8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Engine
struct  Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D  : public RuntimeObject
{
public:
	// Jint.Runtime.ExpressionInterpreter Jint.Engine::_expressions
	ExpressionInterpreter_tD102C31F31458F6E6EDF49CFD7A7684CE208DF1F * ____expressions_0;
	// Jint.Runtime.StatementInterpreter Jint.Engine::_statements
	StatementInterpreter_t09AE267789508AD9BC3E14EAFE8262642B534B44 * ____statements_1;
	// System.Collections.Generic.Stack`1<Jint.Runtime.Environments.ExecutionContext> Jint.Engine::_executionContexts
	Stack_1_t021FB6425C1F751F00593574BFC67C4B8DB091D3 * ____executionContexts_2;
	// Jint.Native.JsValue Jint.Engine::_completionValue
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ____completionValue_3;
	// System.Int32 Jint.Engine::_statementsCount
	int32_t ____statementsCount_4;
	// System.Int64 Jint.Engine::_timeoutTicks
	int64_t ____timeoutTicks_5;
	// Jint.Parser.Ast.SyntaxNode Jint.Engine::_lastSyntaxNode
	SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B * ____lastSyntaxNode_6;
	// Jint.Runtime.Interop.ITypeConverter Jint.Engine::ClrTypeConverter
	RuntimeObject* ___ClrTypeConverter_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> Jint.Engine::TypeCache
	Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A * ___TypeCache_8;
	// Jint.Runtime.CallStack.JintCallStack Jint.Engine::CallStack
	JintCallStack_tC35F241423CC5B5C3185463D6476DF0223D133E6 * ___CallStack_10;
	// Jint.Runtime.Environments.LexicalEnvironment Jint.Engine::GlobalEnvironment
	LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * ___GlobalEnvironment_11;
	// Jint.Native.Global.GlobalObject Jint.Engine::<Global>k__BackingField
	GlobalObject_tD5B044775D4BA82AD01F71F4F605F090811A5D27 * ___U3CGlobalU3Ek__BackingField_12;
	// Jint.Native.Object.ObjectConstructor Jint.Engine::<Object>k__BackingField
	ObjectConstructor_tEA6936468E07A954DA98B3D6FA258133E1992BB0 * ___U3CObjectU3Ek__BackingField_13;
	// Jint.Native.Function.FunctionConstructor Jint.Engine::<Function>k__BackingField
	FunctionConstructor_tFE3FF73EB5E5CE01E3D44BC0F4680317010CA9F5 * ___U3CFunctionU3Ek__BackingField_14;
	// Jint.Native.Array.ArrayConstructor Jint.Engine::<Array>k__BackingField
	ArrayConstructor_t3EE97DD1C34DD817D0D8540BBEC734D56BB7D9C8 * ___U3CArrayU3Ek__BackingField_15;
	// Jint.Native.String.StringConstructor Jint.Engine::<String>k__BackingField
	StringConstructor_tE617B827C95B0979B5C5D21C8F40D48D2D1C0215 * ___U3CStringU3Ek__BackingField_16;
	// Jint.Native.RegExp.RegExpConstructor Jint.Engine::<RegExp>k__BackingField
	RegExpConstructor_t959535B1C26428C9B2A92C0B914883DE4BEB7508 * ___U3CRegExpU3Ek__BackingField_17;
	// Jint.Native.Boolean.BooleanConstructor Jint.Engine::<Boolean>k__BackingField
	BooleanConstructor_t4500B0A4DA339E11D5E6E983A0168B8D98678CDB * ___U3CBooleanU3Ek__BackingField_18;
	// Jint.Native.Number.NumberConstructor Jint.Engine::<Number>k__BackingField
	NumberConstructor_tE5FB6BAB098D799610B0161A628216517C7ADADC * ___U3CNumberU3Ek__BackingField_19;
	// Jint.Native.Date.DateConstructor Jint.Engine::<Date>k__BackingField
	DateConstructor_t5098B976636DBEB0847A7217566BD868C59701D0 * ___U3CDateU3Ek__BackingField_20;
	// Jint.Native.Math.MathInstance Jint.Engine::<Math>k__BackingField
	MathInstance_t3512482B63B6D54FC3F9105785754D409F54F4C0 * ___U3CMathU3Ek__BackingField_21;
	// Jint.Native.Json.JsonInstance Jint.Engine::<Json>k__BackingField
	JsonInstance_tAAC3C2498512BF0ADC8391178660107A034B91AE * ___U3CJsonU3Ek__BackingField_22;
	// Jint.Native.Function.EvalFunctionInstance Jint.Engine::<Eval>k__BackingField
	EvalFunctionInstance_t14923E8CAC9C999E8100B15A3D5D07AA4F903268 * ___U3CEvalU3Ek__BackingField_23;
	// Jint.Native.Error.ErrorConstructor Jint.Engine::<Error>k__BackingField
	ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * ___U3CErrorU3Ek__BackingField_24;
	// Jint.Native.Error.ErrorConstructor Jint.Engine::<EvalError>k__BackingField
	ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * ___U3CEvalErrorU3Ek__BackingField_25;
	// Jint.Native.Error.ErrorConstructor Jint.Engine::<SyntaxError>k__BackingField
	ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * ___U3CSyntaxErrorU3Ek__BackingField_26;
	// Jint.Native.Error.ErrorConstructor Jint.Engine::<TypeError>k__BackingField
	ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * ___U3CTypeErrorU3Ek__BackingField_27;
	// Jint.Native.Error.ErrorConstructor Jint.Engine::<RangeError>k__BackingField
	ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * ___U3CRangeErrorU3Ek__BackingField_28;
	// Jint.Native.Error.ErrorConstructor Jint.Engine::<ReferenceError>k__BackingField
	ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * ___U3CReferenceErrorU3Ek__BackingField_29;
	// Jint.Native.Error.ErrorConstructor Jint.Engine::<UriError>k__BackingField
	ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * ___U3CUriErrorU3Ek__BackingField_30;
	// Jint.Options Jint.Engine::<Options>k__BackingField
	Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48 * ___U3COptionsU3Ek__BackingField_31;
	// Jint.Engine/DebugStepDelegate Jint.Engine::Step
	DebugStepDelegate_t71D4D1019B035251C57C7527AF515F8D509D1284 * ___Step_32;
	// Jint.Engine/BreakDelegate Jint.Engine::Break
	BreakDelegate_tCA6935CACA104BE87718D10A6CB84A635B87F5FD * ___Break_33;
	// Jint.Runtime.Debugger.DebugHandler Jint.Engine::<DebugHandler>k__BackingField
	DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296 * ___U3CDebugHandlerU3Ek__BackingField_34;
	// System.Collections.Generic.List`1<Jint.Runtime.Debugger.BreakPoint> Jint.Engine::<BreakPoints>k__BackingField
	List_1_tDA2C9AE189EC3E3E9EA95A259117F88450997652 * ___U3CBreakPointsU3Ek__BackingField_35;

public:
	inline static int32_t get_offset_of__expressions_0() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ____expressions_0)); }
	inline ExpressionInterpreter_tD102C31F31458F6E6EDF49CFD7A7684CE208DF1F * get__expressions_0() const { return ____expressions_0; }
	inline ExpressionInterpreter_tD102C31F31458F6E6EDF49CFD7A7684CE208DF1F ** get_address_of__expressions_0() { return &____expressions_0; }
	inline void set__expressions_0(ExpressionInterpreter_tD102C31F31458F6E6EDF49CFD7A7684CE208DF1F * value)
	{
		____expressions_0 = value;
		Il2CppCodeGenWriteBarrier((&____expressions_0), value);
	}

	inline static int32_t get_offset_of__statements_1() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ____statements_1)); }
	inline StatementInterpreter_t09AE267789508AD9BC3E14EAFE8262642B534B44 * get__statements_1() const { return ____statements_1; }
	inline StatementInterpreter_t09AE267789508AD9BC3E14EAFE8262642B534B44 ** get_address_of__statements_1() { return &____statements_1; }
	inline void set__statements_1(StatementInterpreter_t09AE267789508AD9BC3E14EAFE8262642B534B44 * value)
	{
		____statements_1 = value;
		Il2CppCodeGenWriteBarrier((&____statements_1), value);
	}

	inline static int32_t get_offset_of__executionContexts_2() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ____executionContexts_2)); }
	inline Stack_1_t021FB6425C1F751F00593574BFC67C4B8DB091D3 * get__executionContexts_2() const { return ____executionContexts_2; }
	inline Stack_1_t021FB6425C1F751F00593574BFC67C4B8DB091D3 ** get_address_of__executionContexts_2() { return &____executionContexts_2; }
	inline void set__executionContexts_2(Stack_1_t021FB6425C1F751F00593574BFC67C4B8DB091D3 * value)
	{
		____executionContexts_2 = value;
		Il2CppCodeGenWriteBarrier((&____executionContexts_2), value);
	}

	inline static int32_t get_offset_of__completionValue_3() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ____completionValue_3)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get__completionValue_3() const { return ____completionValue_3; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of__completionValue_3() { return &____completionValue_3; }
	inline void set__completionValue_3(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		____completionValue_3 = value;
		Il2CppCodeGenWriteBarrier((&____completionValue_3), value);
	}

	inline static int32_t get_offset_of__statementsCount_4() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ____statementsCount_4)); }
	inline int32_t get__statementsCount_4() const { return ____statementsCount_4; }
	inline int32_t* get_address_of__statementsCount_4() { return &____statementsCount_4; }
	inline void set__statementsCount_4(int32_t value)
	{
		____statementsCount_4 = value;
	}

	inline static int32_t get_offset_of__timeoutTicks_5() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ____timeoutTicks_5)); }
	inline int64_t get__timeoutTicks_5() const { return ____timeoutTicks_5; }
	inline int64_t* get_address_of__timeoutTicks_5() { return &____timeoutTicks_5; }
	inline void set__timeoutTicks_5(int64_t value)
	{
		____timeoutTicks_5 = value;
	}

	inline static int32_t get_offset_of__lastSyntaxNode_6() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ____lastSyntaxNode_6)); }
	inline SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B * get__lastSyntaxNode_6() const { return ____lastSyntaxNode_6; }
	inline SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B ** get_address_of__lastSyntaxNode_6() { return &____lastSyntaxNode_6; }
	inline void set__lastSyntaxNode_6(SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B * value)
	{
		____lastSyntaxNode_6 = value;
		Il2CppCodeGenWriteBarrier((&____lastSyntaxNode_6), value);
	}

	inline static int32_t get_offset_of_ClrTypeConverter_7() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___ClrTypeConverter_7)); }
	inline RuntimeObject* get_ClrTypeConverter_7() const { return ___ClrTypeConverter_7; }
	inline RuntimeObject** get_address_of_ClrTypeConverter_7() { return &___ClrTypeConverter_7; }
	inline void set_ClrTypeConverter_7(RuntimeObject* value)
	{
		___ClrTypeConverter_7 = value;
		Il2CppCodeGenWriteBarrier((&___ClrTypeConverter_7), value);
	}

	inline static int32_t get_offset_of_TypeCache_8() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___TypeCache_8)); }
	inline Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A * get_TypeCache_8() const { return ___TypeCache_8; }
	inline Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A ** get_address_of_TypeCache_8() { return &___TypeCache_8; }
	inline void set_TypeCache_8(Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A * value)
	{
		___TypeCache_8 = value;
		Il2CppCodeGenWriteBarrier((&___TypeCache_8), value);
	}

	inline static int32_t get_offset_of_CallStack_10() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___CallStack_10)); }
	inline JintCallStack_tC35F241423CC5B5C3185463D6476DF0223D133E6 * get_CallStack_10() const { return ___CallStack_10; }
	inline JintCallStack_tC35F241423CC5B5C3185463D6476DF0223D133E6 ** get_address_of_CallStack_10() { return &___CallStack_10; }
	inline void set_CallStack_10(JintCallStack_tC35F241423CC5B5C3185463D6476DF0223D133E6 * value)
	{
		___CallStack_10 = value;
		Il2CppCodeGenWriteBarrier((&___CallStack_10), value);
	}

	inline static int32_t get_offset_of_GlobalEnvironment_11() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___GlobalEnvironment_11)); }
	inline LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * get_GlobalEnvironment_11() const { return ___GlobalEnvironment_11; }
	inline LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 ** get_address_of_GlobalEnvironment_11() { return &___GlobalEnvironment_11; }
	inline void set_GlobalEnvironment_11(LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * value)
	{
		___GlobalEnvironment_11 = value;
		Il2CppCodeGenWriteBarrier((&___GlobalEnvironment_11), value);
	}

	inline static int32_t get_offset_of_U3CGlobalU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CGlobalU3Ek__BackingField_12)); }
	inline GlobalObject_tD5B044775D4BA82AD01F71F4F605F090811A5D27 * get_U3CGlobalU3Ek__BackingField_12() const { return ___U3CGlobalU3Ek__BackingField_12; }
	inline GlobalObject_tD5B044775D4BA82AD01F71F4F605F090811A5D27 ** get_address_of_U3CGlobalU3Ek__BackingField_12() { return &___U3CGlobalU3Ek__BackingField_12; }
	inline void set_U3CGlobalU3Ek__BackingField_12(GlobalObject_tD5B044775D4BA82AD01F71F4F605F090811A5D27 * value)
	{
		___U3CGlobalU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGlobalU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CObjectU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CObjectU3Ek__BackingField_13)); }
	inline ObjectConstructor_tEA6936468E07A954DA98B3D6FA258133E1992BB0 * get_U3CObjectU3Ek__BackingField_13() const { return ___U3CObjectU3Ek__BackingField_13; }
	inline ObjectConstructor_tEA6936468E07A954DA98B3D6FA258133E1992BB0 ** get_address_of_U3CObjectU3Ek__BackingField_13() { return &___U3CObjectU3Ek__BackingField_13; }
	inline void set_U3CObjectU3Ek__BackingField_13(ObjectConstructor_tEA6936468E07A954DA98B3D6FA258133E1992BB0 * value)
	{
		___U3CObjectU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CFunctionU3Ek__BackingField_14)); }
	inline FunctionConstructor_tFE3FF73EB5E5CE01E3D44BC0F4680317010CA9F5 * get_U3CFunctionU3Ek__BackingField_14() const { return ___U3CFunctionU3Ek__BackingField_14; }
	inline FunctionConstructor_tFE3FF73EB5E5CE01E3D44BC0F4680317010CA9F5 ** get_address_of_U3CFunctionU3Ek__BackingField_14() { return &___U3CFunctionU3Ek__BackingField_14; }
	inline void set_U3CFunctionU3Ek__BackingField_14(FunctionConstructor_tFE3FF73EB5E5CE01E3D44BC0F4680317010CA9F5 * value)
	{
		___U3CFunctionU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CArrayU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CArrayU3Ek__BackingField_15)); }
	inline ArrayConstructor_t3EE97DD1C34DD817D0D8540BBEC734D56BB7D9C8 * get_U3CArrayU3Ek__BackingField_15() const { return ___U3CArrayU3Ek__BackingField_15; }
	inline ArrayConstructor_t3EE97DD1C34DD817D0D8540BBEC734D56BB7D9C8 ** get_address_of_U3CArrayU3Ek__BackingField_15() { return &___U3CArrayU3Ek__BackingField_15; }
	inline void set_U3CArrayU3Ek__BackingField_15(ArrayConstructor_t3EE97DD1C34DD817D0D8540BBEC734D56BB7D9C8 * value)
	{
		___U3CArrayU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArrayU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CStringU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CStringU3Ek__BackingField_16)); }
	inline StringConstructor_tE617B827C95B0979B5C5D21C8F40D48D2D1C0215 * get_U3CStringU3Ek__BackingField_16() const { return ___U3CStringU3Ek__BackingField_16; }
	inline StringConstructor_tE617B827C95B0979B5C5D21C8F40D48D2D1C0215 ** get_address_of_U3CStringU3Ek__BackingField_16() { return &___U3CStringU3Ek__BackingField_16; }
	inline void set_U3CStringU3Ek__BackingField_16(StringConstructor_tE617B827C95B0979B5C5D21C8F40D48D2D1C0215 * value)
	{
		___U3CStringU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStringU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CRegExpU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CRegExpU3Ek__BackingField_17)); }
	inline RegExpConstructor_t959535B1C26428C9B2A92C0B914883DE4BEB7508 * get_U3CRegExpU3Ek__BackingField_17() const { return ___U3CRegExpU3Ek__BackingField_17; }
	inline RegExpConstructor_t959535B1C26428C9B2A92C0B914883DE4BEB7508 ** get_address_of_U3CRegExpU3Ek__BackingField_17() { return &___U3CRegExpU3Ek__BackingField_17; }
	inline void set_U3CRegExpU3Ek__BackingField_17(RegExpConstructor_t959535B1C26428C9B2A92C0B914883DE4BEB7508 * value)
	{
		___U3CRegExpU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRegExpU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CBooleanU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CBooleanU3Ek__BackingField_18)); }
	inline BooleanConstructor_t4500B0A4DA339E11D5E6E983A0168B8D98678CDB * get_U3CBooleanU3Ek__BackingField_18() const { return ___U3CBooleanU3Ek__BackingField_18; }
	inline BooleanConstructor_t4500B0A4DA339E11D5E6E983A0168B8D98678CDB ** get_address_of_U3CBooleanU3Ek__BackingField_18() { return &___U3CBooleanU3Ek__BackingField_18; }
	inline void set_U3CBooleanU3Ek__BackingField_18(BooleanConstructor_t4500B0A4DA339E11D5E6E983A0168B8D98678CDB * value)
	{
		___U3CBooleanU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBooleanU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CNumberU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CNumberU3Ek__BackingField_19)); }
	inline NumberConstructor_tE5FB6BAB098D799610B0161A628216517C7ADADC * get_U3CNumberU3Ek__BackingField_19() const { return ___U3CNumberU3Ek__BackingField_19; }
	inline NumberConstructor_tE5FB6BAB098D799610B0161A628216517C7ADADC ** get_address_of_U3CNumberU3Ek__BackingField_19() { return &___U3CNumberU3Ek__BackingField_19; }
	inline void set_U3CNumberU3Ek__BackingField_19(NumberConstructor_tE5FB6BAB098D799610B0161A628216517C7ADADC * value)
	{
		___U3CNumberU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNumberU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CDateU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CDateU3Ek__BackingField_20)); }
	inline DateConstructor_t5098B976636DBEB0847A7217566BD868C59701D0 * get_U3CDateU3Ek__BackingField_20() const { return ___U3CDateU3Ek__BackingField_20; }
	inline DateConstructor_t5098B976636DBEB0847A7217566BD868C59701D0 ** get_address_of_U3CDateU3Ek__BackingField_20() { return &___U3CDateU3Ek__BackingField_20; }
	inline void set_U3CDateU3Ek__BackingField_20(DateConstructor_t5098B976636DBEB0847A7217566BD868C59701D0 * value)
	{
		___U3CDateU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDateU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CMathU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CMathU3Ek__BackingField_21)); }
	inline MathInstance_t3512482B63B6D54FC3F9105785754D409F54F4C0 * get_U3CMathU3Ek__BackingField_21() const { return ___U3CMathU3Ek__BackingField_21; }
	inline MathInstance_t3512482B63B6D54FC3F9105785754D409F54F4C0 ** get_address_of_U3CMathU3Ek__BackingField_21() { return &___U3CMathU3Ek__BackingField_21; }
	inline void set_U3CMathU3Ek__BackingField_21(MathInstance_t3512482B63B6D54FC3F9105785754D409F54F4C0 * value)
	{
		___U3CMathU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMathU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3CJsonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CJsonU3Ek__BackingField_22)); }
	inline JsonInstance_tAAC3C2498512BF0ADC8391178660107A034B91AE * get_U3CJsonU3Ek__BackingField_22() const { return ___U3CJsonU3Ek__BackingField_22; }
	inline JsonInstance_tAAC3C2498512BF0ADC8391178660107A034B91AE ** get_address_of_U3CJsonU3Ek__BackingField_22() { return &___U3CJsonU3Ek__BackingField_22; }
	inline void set_U3CJsonU3Ek__BackingField_22(JsonInstance_tAAC3C2498512BF0ADC8391178660107A034B91AE * value)
	{
		___U3CJsonU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJsonU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_U3CEvalU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CEvalU3Ek__BackingField_23)); }
	inline EvalFunctionInstance_t14923E8CAC9C999E8100B15A3D5D07AA4F903268 * get_U3CEvalU3Ek__BackingField_23() const { return ___U3CEvalU3Ek__BackingField_23; }
	inline EvalFunctionInstance_t14923E8CAC9C999E8100B15A3D5D07AA4F903268 ** get_address_of_U3CEvalU3Ek__BackingField_23() { return &___U3CEvalU3Ek__BackingField_23; }
	inline void set_U3CEvalU3Ek__BackingField_23(EvalFunctionInstance_t14923E8CAC9C999E8100B15A3D5D07AA4F903268 * value)
	{
		___U3CEvalU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEvalU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CErrorU3Ek__BackingField_24)); }
	inline ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * get_U3CErrorU3Ek__BackingField_24() const { return ___U3CErrorU3Ek__BackingField_24; }
	inline ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E ** get_address_of_U3CErrorU3Ek__BackingField_24() { return &___U3CErrorU3Ek__BackingField_24; }
	inline void set_U3CErrorU3Ek__BackingField_24(ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * value)
	{
		___U3CErrorU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorU3Ek__BackingField_24), value);
	}

	inline static int32_t get_offset_of_U3CEvalErrorU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CEvalErrorU3Ek__BackingField_25)); }
	inline ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * get_U3CEvalErrorU3Ek__BackingField_25() const { return ___U3CEvalErrorU3Ek__BackingField_25; }
	inline ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E ** get_address_of_U3CEvalErrorU3Ek__BackingField_25() { return &___U3CEvalErrorU3Ek__BackingField_25; }
	inline void set_U3CEvalErrorU3Ek__BackingField_25(ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * value)
	{
		___U3CEvalErrorU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEvalErrorU3Ek__BackingField_25), value);
	}

	inline static int32_t get_offset_of_U3CSyntaxErrorU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CSyntaxErrorU3Ek__BackingField_26)); }
	inline ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * get_U3CSyntaxErrorU3Ek__BackingField_26() const { return ___U3CSyntaxErrorU3Ek__BackingField_26; }
	inline ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E ** get_address_of_U3CSyntaxErrorU3Ek__BackingField_26() { return &___U3CSyntaxErrorU3Ek__BackingField_26; }
	inline void set_U3CSyntaxErrorU3Ek__BackingField_26(ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * value)
	{
		___U3CSyntaxErrorU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSyntaxErrorU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CTypeErrorU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CTypeErrorU3Ek__BackingField_27)); }
	inline ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * get_U3CTypeErrorU3Ek__BackingField_27() const { return ___U3CTypeErrorU3Ek__BackingField_27; }
	inline ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E ** get_address_of_U3CTypeErrorU3Ek__BackingField_27() { return &___U3CTypeErrorU3Ek__BackingField_27; }
	inline void set_U3CTypeErrorU3Ek__BackingField_27(ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * value)
	{
		___U3CTypeErrorU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeErrorU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CRangeErrorU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CRangeErrorU3Ek__BackingField_28)); }
	inline ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * get_U3CRangeErrorU3Ek__BackingField_28() const { return ___U3CRangeErrorU3Ek__BackingField_28; }
	inline ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E ** get_address_of_U3CRangeErrorU3Ek__BackingField_28() { return &___U3CRangeErrorU3Ek__BackingField_28; }
	inline void set_U3CRangeErrorU3Ek__BackingField_28(ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * value)
	{
		___U3CRangeErrorU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRangeErrorU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CReferenceErrorU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CReferenceErrorU3Ek__BackingField_29)); }
	inline ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * get_U3CReferenceErrorU3Ek__BackingField_29() const { return ___U3CReferenceErrorU3Ek__BackingField_29; }
	inline ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E ** get_address_of_U3CReferenceErrorU3Ek__BackingField_29() { return &___U3CReferenceErrorU3Ek__BackingField_29; }
	inline void set_U3CReferenceErrorU3Ek__BackingField_29(ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * value)
	{
		___U3CReferenceErrorU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CReferenceErrorU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CUriErrorU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CUriErrorU3Ek__BackingField_30)); }
	inline ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * get_U3CUriErrorU3Ek__BackingField_30() const { return ___U3CUriErrorU3Ek__BackingField_30; }
	inline ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E ** get_address_of_U3CUriErrorU3Ek__BackingField_30() { return &___U3CUriErrorU3Ek__BackingField_30; }
	inline void set_U3CUriErrorU3Ek__BackingField_30(ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E * value)
	{
		___U3CUriErrorU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUriErrorU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3COptionsU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3COptionsU3Ek__BackingField_31)); }
	inline Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48 * get_U3COptionsU3Ek__BackingField_31() const { return ___U3COptionsU3Ek__BackingField_31; }
	inline Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48 ** get_address_of_U3COptionsU3Ek__BackingField_31() { return &___U3COptionsU3Ek__BackingField_31; }
	inline void set_U3COptionsU3Ek__BackingField_31(Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48 * value)
	{
		___U3COptionsU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3COptionsU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_Step_32() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___Step_32)); }
	inline DebugStepDelegate_t71D4D1019B035251C57C7527AF515F8D509D1284 * get_Step_32() const { return ___Step_32; }
	inline DebugStepDelegate_t71D4D1019B035251C57C7527AF515F8D509D1284 ** get_address_of_Step_32() { return &___Step_32; }
	inline void set_Step_32(DebugStepDelegate_t71D4D1019B035251C57C7527AF515F8D509D1284 * value)
	{
		___Step_32 = value;
		Il2CppCodeGenWriteBarrier((&___Step_32), value);
	}

	inline static int32_t get_offset_of_Break_33() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___Break_33)); }
	inline BreakDelegate_tCA6935CACA104BE87718D10A6CB84A635B87F5FD * get_Break_33() const { return ___Break_33; }
	inline BreakDelegate_tCA6935CACA104BE87718D10A6CB84A635B87F5FD ** get_address_of_Break_33() { return &___Break_33; }
	inline void set_Break_33(BreakDelegate_tCA6935CACA104BE87718D10A6CB84A635B87F5FD * value)
	{
		___Break_33 = value;
		Il2CppCodeGenWriteBarrier((&___Break_33), value);
	}

	inline static int32_t get_offset_of_U3CDebugHandlerU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CDebugHandlerU3Ek__BackingField_34)); }
	inline DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296 * get_U3CDebugHandlerU3Ek__BackingField_34() const { return ___U3CDebugHandlerU3Ek__BackingField_34; }
	inline DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296 ** get_address_of_U3CDebugHandlerU3Ek__BackingField_34() { return &___U3CDebugHandlerU3Ek__BackingField_34; }
	inline void set_U3CDebugHandlerU3Ek__BackingField_34(DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296 * value)
	{
		___U3CDebugHandlerU3Ek__BackingField_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDebugHandlerU3Ek__BackingField_34), value);
	}

	inline static int32_t get_offset_of_U3CBreakPointsU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D, ___U3CBreakPointsU3Ek__BackingField_35)); }
	inline List_1_tDA2C9AE189EC3E3E9EA95A259117F88450997652 * get_U3CBreakPointsU3Ek__BackingField_35() const { return ___U3CBreakPointsU3Ek__BackingField_35; }
	inline List_1_tDA2C9AE189EC3E3E9EA95A259117F88450997652 ** get_address_of_U3CBreakPointsU3Ek__BackingField_35() { return &___U3CBreakPointsU3Ek__BackingField_35; }
	inline void set_U3CBreakPointsU3Ek__BackingField_35(List_1_tDA2C9AE189EC3E3E9EA95A259117F88450997652 * value)
	{
		___U3CBreakPointsU3Ek__BackingField_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBreakPointsU3Ek__BackingField_35), value);
	}
};

struct Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Func`3<Jint.Engine,System.Object,Jint.Native.JsValue>> Jint.Engine::TypeMappers
	Dictionary_2_tA1573C6F056C71CA19B9EA39F42257803E748739 * ___TypeMappers_9;

public:
	inline static int32_t get_offset_of_TypeMappers_9() { return static_cast<int32_t>(offsetof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D_StaticFields, ___TypeMappers_9)); }
	inline Dictionary_2_tA1573C6F056C71CA19B9EA39F42257803E748739 * get_TypeMappers_9() const { return ___TypeMappers_9; }
	inline Dictionary_2_tA1573C6F056C71CA19B9EA39F42257803E748739 ** get_address_of_TypeMappers_9() { return &___TypeMappers_9; }
	inline void set_TypeMappers_9(Dictionary_2_tA1573C6F056C71CA19B9EA39F42257803E748739 * value)
	{
		___TypeMappers_9 = value;
		Il2CppCodeGenWriteBarrier((&___TypeMappers_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENGINE_T8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D_H
#ifndef U3CU3EC_TDE3B66DA56D8CBD6E7159670C19F68253FDB9EA3_H
#define U3CU3EC_TDE3B66DA56D8CBD6E7159670C19F68253FDB9EA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Engine/<>c
struct  U3CU3Ec_tDE3B66DA56D8CBD6E7159670C19F68253FDB9EA3  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tDE3B66DA56D8CBD6E7159670C19F68253FDB9EA3_StaticFields
{
public:
	// Jint.Engine/<>c Jint.Engine/<>c::<>9
	U3CU3Ec_tDE3B66DA56D8CBD6E7159670C19F68253FDB9EA3 * ___U3CU3E9_0;
	// System.Func`2<Jint.Parser.Ast.VariableDeclaration,System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.VariableDeclarator>> Jint.Engine/<>c::<>9__142_0
	Func_2_tF0A1B416C0AA3DF3C7220B91CA0A5044C8646C5E * ___U3CU3E9__142_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDE3B66DA56D8CBD6E7159670C19F68253FDB9EA3_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tDE3B66DA56D8CBD6E7159670C19F68253FDB9EA3 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tDE3B66DA56D8CBD6E7159670C19F68253FDB9EA3 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tDE3B66DA56D8CBD6E7159670C19F68253FDB9EA3 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__142_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDE3B66DA56D8CBD6E7159670C19F68253FDB9EA3_StaticFields, ___U3CU3E9__142_0_1)); }
	inline Func_2_tF0A1B416C0AA3DF3C7220B91CA0A5044C8646C5E * get_U3CU3E9__142_0_1() const { return ___U3CU3E9__142_0_1; }
	inline Func_2_tF0A1B416C0AA3DF3C7220B91CA0A5044C8646C5E ** get_address_of_U3CU3E9__142_0_1() { return &___U3CU3E9__142_0_1; }
	inline void set_U3CU3E9__142_0_1(Func_2_tF0A1B416C0AA3DF3C7220B91CA0A5044C8646C5E * value)
	{
		___U3CU3E9__142_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__142_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TDE3B66DA56D8CBD6E7159670C19F68253FDB9EA3_H
#ifndef EVALCODESCOPE_T0E707EEB9CE345520B6856F1378523ED3EBF02F8_H
#define EVALCODESCOPE_T0E707EEB9CE345520B6856F1378523ED3EBF02F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.EvalCodeScope
struct  EvalCodeScope_t0E707EEB9CE345520B6856F1378523ED3EBF02F8  : public RuntimeObject
{
public:
	// System.Boolean Jint.EvalCodeScope::_eval
	bool ____eval_0;
	// System.Boolean Jint.EvalCodeScope::_force
	bool ____force_1;
	// System.Int32 Jint.EvalCodeScope::_forcedRefCount
	int32_t ____forcedRefCount_2;

public:
	inline static int32_t get_offset_of__eval_0() { return static_cast<int32_t>(offsetof(EvalCodeScope_t0E707EEB9CE345520B6856F1378523ED3EBF02F8, ____eval_0)); }
	inline bool get__eval_0() const { return ____eval_0; }
	inline bool* get_address_of__eval_0() { return &____eval_0; }
	inline void set__eval_0(bool value)
	{
		____eval_0 = value;
	}

	inline static int32_t get_offset_of__force_1() { return static_cast<int32_t>(offsetof(EvalCodeScope_t0E707EEB9CE345520B6856F1378523ED3EBF02F8, ____force_1)); }
	inline bool get__force_1() const { return ____force_1; }
	inline bool* get_address_of__force_1() { return &____force_1; }
	inline void set__force_1(bool value)
	{
		____force_1 = value;
	}

	inline static int32_t get_offset_of__forcedRefCount_2() { return static_cast<int32_t>(offsetof(EvalCodeScope_t0E707EEB9CE345520B6856F1378523ED3EBF02F8, ____forcedRefCount_2)); }
	inline int32_t get__forcedRefCount_2() const { return ____forcedRefCount_2; }
	inline int32_t* get_address_of__forcedRefCount_2() { return &____forcedRefCount_2; }
	inline void set__forcedRefCount_2(int32_t value)
	{
		____forcedRefCount_2 = value;
	}
};

struct EvalCodeScope_t0E707EEB9CE345520B6856F1378523ED3EBF02F8_ThreadStaticFields
{
public:
	// System.Int32 Jint.EvalCodeScope::_refCount
	int32_t ____refCount_3;

public:
	inline static int32_t get_offset_of__refCount_3() { return static_cast<int32_t>(offsetof(EvalCodeScope_t0E707EEB9CE345520B6856F1378523ED3EBF02F8_ThreadStaticFields, ____refCount_3)); }
	inline int32_t get__refCount_3() const { return ____refCount_3; }
	inline int32_t* get_address_of__refCount_3() { return &____refCount_3; }
	inline void set__refCount_3(int32_t value)
	{
		____refCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVALCODESCOPE_T0E707EEB9CE345520B6856F1378523ED3EBF02F8_H
#ifndef OBJECTINSTANCE_T9A33413FF3D113AEBA2D280D5498C04222E32900_H
#define OBJECTINSTANCE_T9A33413FF3D113AEBA2D280D5498C04222E32900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Object.ObjectInstance
struct  ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900  : public RuntimeObject
{
public:
	// Jint.Engine Jint.Native.Object.ObjectInstance::<Engine>k__BackingField
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ___U3CEngineU3Ek__BackingField_0;
	// System.Collections.Generic.IDictionary`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor> Jint.Native.Object.ObjectInstance::<Properties>k__BackingField
	RuntimeObject* ___U3CPropertiesU3Ek__BackingField_1;
	// Jint.Native.Object.ObjectInstance Jint.Native.Object.ObjectInstance::<Prototype>k__BackingField
	ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * ___U3CPrototypeU3Ek__BackingField_2;
	// System.Boolean Jint.Native.Object.ObjectInstance::<Extensible>k__BackingField
	bool ___U3CExtensibleU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CEngineU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900, ___U3CEngineU3Ek__BackingField_0)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get_U3CEngineU3Ek__BackingField_0() const { return ___U3CEngineU3Ek__BackingField_0; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of_U3CEngineU3Ek__BackingField_0() { return &___U3CEngineU3Ek__BackingField_0; }
	inline void set_U3CEngineU3Ek__BackingField_0(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		___U3CEngineU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEngineU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900, ___U3CPropertiesU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CPropertiesU3Ek__BackingField_1() const { return ___U3CPropertiesU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CPropertiesU3Ek__BackingField_1() { return &___U3CPropertiesU3Ek__BackingField_1; }
	inline void set_U3CPropertiesU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CPropertiesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertiesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CPrototypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900, ___U3CPrototypeU3Ek__BackingField_2)); }
	inline ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * get_U3CPrototypeU3Ek__BackingField_2() const { return ___U3CPrototypeU3Ek__BackingField_2; }
	inline ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 ** get_address_of_U3CPrototypeU3Ek__BackingField_2() { return &___U3CPrototypeU3Ek__BackingField_2; }
	inline void set_U3CPrototypeU3Ek__BackingField_2(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * value)
	{
		___U3CPrototypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrototypeU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CExtensibleU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900, ___U3CExtensibleU3Ek__BackingField_3)); }
	inline bool get_U3CExtensibleU3Ek__BackingField_3() const { return ___U3CExtensibleU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CExtensibleU3Ek__BackingField_3() { return &___U3CExtensibleU3Ek__BackingField_3; }
	inline void set_U3CExtensibleU3Ek__BackingField_3(bool value)
	{
		___U3CExtensibleU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTINSTANCE_T9A33413FF3D113AEBA2D280D5498C04222E32900_H
#ifndef REFLECTIONEXTENSIONS_T0A3499E3EBC2F4A91F107F819BE4D72C759AC6D0_H
#define REFLECTIONEXTENSIONS_T0A3499E3EBC2F4A91F107F819BE4D72C759AC6D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.ReflectionExtensions
struct  ReflectionExtensions_t0A3499E3EBC2F4A91F107F819BE4D72C759AC6D0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONEXTENSIONS_T0A3499E3EBC2F4A91F107F819BE4D72C759AC6D0_H
#ifndef ARGUMENTS_T99C4B1FF8199EF05F80BBEC1627767ACF3225AAE_H
#define ARGUMENTS_T99C4B1FF8199EF05F80BBEC1627767ACF3225AAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Arguments
struct  Arguments_t99C4B1FF8199EF05F80BBEC1627767ACF3225AAE  : public RuntimeObject
{
public:

public:
};

struct Arguments_t99C4B1FF8199EF05F80BBEC1627767ACF3225AAE_StaticFields
{
public:
	// Jint.Native.JsValue[] Jint.Runtime.Arguments::Empty
	JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Arguments_t99C4B1FF8199EF05F80BBEC1627767ACF3225AAE_StaticFields, ___Empty_0)); }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* get_Empty_0() const { return ___Empty_0; }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTS_T99C4B1FF8199EF05F80BBEC1627767ACF3225AAE_H
#ifndef CALLSTACKELEMENT_TCA2B7D32517A783A13494E5F7723DD76A656A762_H
#define CALLSTACKELEMENT_TCA2B7D32517A783A13494E5F7723DD76A656A762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.CallStackElement
struct  CallStackElement_tCA2B7D32517A783A13494E5F7723DD76A656A762  : public RuntimeObject
{
public:
	// System.String Jint.Runtime.CallStackElement::_shortDescription
	String_t* ____shortDescription_0;
	// Jint.Parser.Ast.CallExpression Jint.Runtime.CallStackElement::<CallExpression>k__BackingField
	CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB * ___U3CCallExpressionU3Ek__BackingField_1;
	// Jint.Native.JsValue Jint.Runtime.CallStackElement::<Function>k__BackingField
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___U3CFunctionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__shortDescription_0() { return static_cast<int32_t>(offsetof(CallStackElement_tCA2B7D32517A783A13494E5F7723DD76A656A762, ____shortDescription_0)); }
	inline String_t* get__shortDescription_0() const { return ____shortDescription_0; }
	inline String_t** get_address_of__shortDescription_0() { return &____shortDescription_0; }
	inline void set__shortDescription_0(String_t* value)
	{
		____shortDescription_0 = value;
		Il2CppCodeGenWriteBarrier((&____shortDescription_0), value);
	}

	inline static int32_t get_offset_of_U3CCallExpressionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CallStackElement_tCA2B7D32517A783A13494E5F7723DD76A656A762, ___U3CCallExpressionU3Ek__BackingField_1)); }
	inline CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB * get_U3CCallExpressionU3Ek__BackingField_1() const { return ___U3CCallExpressionU3Ek__BackingField_1; }
	inline CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB ** get_address_of_U3CCallExpressionU3Ek__BackingField_1() { return &___U3CCallExpressionU3Ek__BackingField_1; }
	inline void set_U3CCallExpressionU3Ek__BackingField_1(CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB * value)
	{
		___U3CCallExpressionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallExpressionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CallStackElement_tCA2B7D32517A783A13494E5F7723DD76A656A762, ___U3CFunctionU3Ek__BackingField_2)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_U3CFunctionU3Ek__BackingField_2() const { return ___U3CFunctionU3Ek__BackingField_2; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_U3CFunctionU3Ek__BackingField_2() { return &___U3CFunctionU3Ek__BackingField_2; }
	inline void set_U3CFunctionU3Ek__BackingField_2(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___U3CFunctionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLSTACKELEMENT_TCA2B7D32517A783A13494E5F7723DD76A656A762_H
#ifndef COMPLETION_T59B39A7DE285AF3FA5456B76364F3C6B9EC85B74_H
#define COMPLETION_T59B39A7DE285AF3FA5456B76364F3C6B9EC85B74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Completion
struct  Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74  : public RuntimeObject
{
public:
	// System.String Jint.Runtime.Completion::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_5;
	// Jint.Native.JsValue Jint.Runtime.Completion::<Value>k__BackingField
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___U3CValueU3Ek__BackingField_6;
	// System.String Jint.Runtime.Completion::<Identifier>k__BackingField
	String_t* ___U3CIdentifierU3Ek__BackingField_7;
	// Jint.Parser.Location Jint.Runtime.Completion::<Location>k__BackingField
	Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * ___U3CLocationU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74, ___U3CTypeU3Ek__BackingField_5)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_5() const { return ___U3CTypeU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_5() { return &___U3CTypeU3Ek__BackingField_5; }
	inline void set_U3CTypeU3Ek__BackingField_5(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74, ___U3CValueU3Ek__BackingField_6)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_U3CValueU3Ek__BackingField_6() const { return ___U3CValueU3Ek__BackingField_6; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_U3CValueU3Ek__BackingField_6() { return &___U3CValueU3Ek__BackingField_6; }
	inline void set_U3CValueU3Ek__BackingField_6(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___U3CValueU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CIdentifierU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74, ___U3CIdentifierU3Ek__BackingField_7)); }
	inline String_t* get_U3CIdentifierU3Ek__BackingField_7() const { return ___U3CIdentifierU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CIdentifierU3Ek__BackingField_7() { return &___U3CIdentifierU3Ek__BackingField_7; }
	inline void set_U3CIdentifierU3Ek__BackingField_7(String_t* value)
	{
		___U3CIdentifierU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdentifierU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CLocationU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74, ___U3CLocationU3Ek__BackingField_8)); }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * get_U3CLocationU3Ek__BackingField_8() const { return ___U3CLocationU3Ek__BackingField_8; }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 ** get_address_of_U3CLocationU3Ek__BackingField_8() { return &___U3CLocationU3Ek__BackingField_8; }
	inline void set_U3CLocationU3Ek__BackingField_8(Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * value)
	{
		___U3CLocationU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLocationU3Ek__BackingField_8), value);
	}
};

struct Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74_StaticFields
{
public:
	// System.String Jint.Runtime.Completion::Normal
	String_t* ___Normal_0;
	// System.String Jint.Runtime.Completion::Break
	String_t* ___Break_1;
	// System.String Jint.Runtime.Completion::Continue
	String_t* ___Continue_2;
	// System.String Jint.Runtime.Completion::Return
	String_t* ___Return_3;
	// System.String Jint.Runtime.Completion::Throw
	String_t* ___Throw_4;

public:
	inline static int32_t get_offset_of_Normal_0() { return static_cast<int32_t>(offsetof(Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74_StaticFields, ___Normal_0)); }
	inline String_t* get_Normal_0() const { return ___Normal_0; }
	inline String_t** get_address_of_Normal_0() { return &___Normal_0; }
	inline void set_Normal_0(String_t* value)
	{
		___Normal_0 = value;
		Il2CppCodeGenWriteBarrier((&___Normal_0), value);
	}

	inline static int32_t get_offset_of_Break_1() { return static_cast<int32_t>(offsetof(Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74_StaticFields, ___Break_1)); }
	inline String_t* get_Break_1() const { return ___Break_1; }
	inline String_t** get_address_of_Break_1() { return &___Break_1; }
	inline void set_Break_1(String_t* value)
	{
		___Break_1 = value;
		Il2CppCodeGenWriteBarrier((&___Break_1), value);
	}

	inline static int32_t get_offset_of_Continue_2() { return static_cast<int32_t>(offsetof(Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74_StaticFields, ___Continue_2)); }
	inline String_t* get_Continue_2() const { return ___Continue_2; }
	inline String_t** get_address_of_Continue_2() { return &___Continue_2; }
	inline void set_Continue_2(String_t* value)
	{
		___Continue_2 = value;
		Il2CppCodeGenWriteBarrier((&___Continue_2), value);
	}

	inline static int32_t get_offset_of_Return_3() { return static_cast<int32_t>(offsetof(Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74_StaticFields, ___Return_3)); }
	inline String_t* get_Return_3() const { return ___Return_3; }
	inline String_t** get_address_of_Return_3() { return &___Return_3; }
	inline void set_Return_3(String_t* value)
	{
		___Return_3 = value;
		Il2CppCodeGenWriteBarrier((&___Return_3), value);
	}

	inline static int32_t get_offset_of_Throw_4() { return static_cast<int32_t>(offsetof(Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74_StaticFields, ___Throw_4)); }
	inline String_t* get_Throw_4() const { return ___Throw_4; }
	inline String_t** get_address_of_Throw_4() { return &___Throw_4; }
	inline void set_Throw_4(String_t* value)
	{
		___Throw_4 = value;
		Il2CppCodeGenWriteBarrier((&___Throw_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPLETION_T59B39A7DE285AF3FA5456B76364F3C6B9EC85B74_H
#ifndef EXPRESSIONINTERPRETER_TD102C31F31458F6E6EDF49CFD7A7684CE208DF1F_H
#define EXPRESSIONINTERPRETER_TD102C31F31458F6E6EDF49CFD7A7684CE208DF1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.ExpressionInterpreter
struct  ExpressionInterpreter_tD102C31F31458F6E6EDF49CFD7A7684CE208DF1F  : public RuntimeObject
{
public:
	// Jint.Engine Jint.Runtime.ExpressionInterpreter::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_0;

public:
	inline static int32_t get_offset_of__engine_0() { return static_cast<int32_t>(offsetof(ExpressionInterpreter_tD102C31F31458F6E6EDF49CFD7A7684CE208DF1F, ____engine_0)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_0() const { return ____engine_0; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_0() { return &____engine_0; }
	inline void set__engine_0(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_0 = value;
		Il2CppCodeGenWriteBarrier((&____engine_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONINTERPRETER_TD102C31F31458F6E6EDF49CFD7A7684CE208DF1F_H
#ifndef U3CU3EC_TB1F01E2AF2CF25F9D136A94A6AD008FC4193D541_H
#define U3CU3EC_TB1F01E2AF2CF25F9D136A94A6AD008FC4193D541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.ExpressionInterpreter/<>c
struct  U3CU3Ec_tB1F01E2AF2CF25F9D136A94A6AD008FC4193D541  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB1F01E2AF2CF25F9D136A94A6AD008FC4193D541_StaticFields
{
public:
	// Jint.Runtime.ExpressionInterpreter/<>c Jint.Runtime.ExpressionInterpreter/<>c::<>9
	U3CU3Ec_tB1F01E2AF2CF25F9D136A94A6AD008FC4193D541 * ___U3CU3E9_0;
	// System.Func`2<Jint.Parser.Ast.Expression,System.Boolean> Jint.Runtime.ExpressionInterpreter/<>c::<>9__17_0
	Func_2_t3F6C36BB02A8D91C7BA8CA6041BCFA7E6CB98089 * ___U3CU3E9__17_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB1F01E2AF2CF25F9D136A94A6AD008FC4193D541_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB1F01E2AF2CF25F9D136A94A6AD008FC4193D541 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB1F01E2AF2CF25F9D136A94A6AD008FC4193D541 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB1F01E2AF2CF25F9D136A94A6AD008FC4193D541 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB1F01E2AF2CF25F9D136A94A6AD008FC4193D541_StaticFields, ___U3CU3E9__17_0_1)); }
	inline Func_2_t3F6C36BB02A8D91C7BA8CA6041BCFA7E6CB98089 * get_U3CU3E9__17_0_1() const { return ___U3CU3E9__17_0_1; }
	inline Func_2_t3F6C36BB02A8D91C7BA8CA6041BCFA7E6CB98089 ** get_address_of_U3CU3E9__17_0_1() { return &___U3CU3E9__17_0_1; }
	inline void set_U3CU3E9__17_0_1(Func_2_t3F6C36BB02A8D91C7BA8CA6041BCFA7E6CB98089 * value)
	{
		___U3CU3E9__17_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TB1F01E2AF2CF25F9D136A94A6AD008FC4193D541_H
#ifndef DEFAULTTYPECONVERTER_TC48984CBA042A943B40BAE5960F493DA33962087_H
#define DEFAULTTYPECONVERTER_TC48984CBA042A943B40BAE5960F493DA33962087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.DefaultTypeConverter
struct  DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087  : public RuntimeObject
{
public:
	// Jint.Engine Jint.Runtime.Interop.DefaultTypeConverter::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_0;

public:
	inline static int32_t get_offset_of__engine_0() { return static_cast<int32_t>(offsetof(DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087, ____engine_0)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_0() const { return ____engine_0; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_0() { return &____engine_0; }
	inline void set__engine_0(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_0 = value;
		Il2CppCodeGenWriteBarrier((&____engine_0), value);
	}
};

struct DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> Jint.Runtime.Interop.DefaultTypeConverter::_knownConversions
	Dictionary_2_tC299681D95BE2E81CC7CBA606C4E9D07A00FA35B * ____knownConversions_1;
	// System.Object Jint.Runtime.Interop.DefaultTypeConverter::_lockObject
	RuntimeObject * ____lockObject_2;
	// System.Reflection.MethodInfo Jint.Runtime.Interop.DefaultTypeConverter::convertChangeType
	MethodInfo_t * ___convertChangeType_3;
	// System.Reflection.MethodInfo Jint.Runtime.Interop.DefaultTypeConverter::jsValueFromObject
	MethodInfo_t * ___jsValueFromObject_4;
	// System.Reflection.MethodInfo Jint.Runtime.Interop.DefaultTypeConverter::jsValueToObject
	MethodInfo_t * ___jsValueToObject_5;

public:
	inline static int32_t get_offset_of__knownConversions_1() { return static_cast<int32_t>(offsetof(DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087_StaticFields, ____knownConversions_1)); }
	inline Dictionary_2_tC299681D95BE2E81CC7CBA606C4E9D07A00FA35B * get__knownConversions_1() const { return ____knownConversions_1; }
	inline Dictionary_2_tC299681D95BE2E81CC7CBA606C4E9D07A00FA35B ** get_address_of__knownConversions_1() { return &____knownConversions_1; }
	inline void set__knownConversions_1(Dictionary_2_tC299681D95BE2E81CC7CBA606C4E9D07A00FA35B * value)
	{
		____knownConversions_1 = value;
		Il2CppCodeGenWriteBarrier((&____knownConversions_1), value);
	}

	inline static int32_t get_offset_of__lockObject_2() { return static_cast<int32_t>(offsetof(DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087_StaticFields, ____lockObject_2)); }
	inline RuntimeObject * get__lockObject_2() const { return ____lockObject_2; }
	inline RuntimeObject ** get_address_of__lockObject_2() { return &____lockObject_2; }
	inline void set__lockObject_2(RuntimeObject * value)
	{
		____lockObject_2 = value;
		Il2CppCodeGenWriteBarrier((&____lockObject_2), value);
	}

	inline static int32_t get_offset_of_convertChangeType_3() { return static_cast<int32_t>(offsetof(DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087_StaticFields, ___convertChangeType_3)); }
	inline MethodInfo_t * get_convertChangeType_3() const { return ___convertChangeType_3; }
	inline MethodInfo_t ** get_address_of_convertChangeType_3() { return &___convertChangeType_3; }
	inline void set_convertChangeType_3(MethodInfo_t * value)
	{
		___convertChangeType_3 = value;
		Il2CppCodeGenWriteBarrier((&___convertChangeType_3), value);
	}

	inline static int32_t get_offset_of_jsValueFromObject_4() { return static_cast<int32_t>(offsetof(DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087_StaticFields, ___jsValueFromObject_4)); }
	inline MethodInfo_t * get_jsValueFromObject_4() const { return ___jsValueFromObject_4; }
	inline MethodInfo_t ** get_address_of_jsValueFromObject_4() { return &___jsValueFromObject_4; }
	inline void set_jsValueFromObject_4(MethodInfo_t * value)
	{
		___jsValueFromObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___jsValueFromObject_4), value);
	}

	inline static int32_t get_offset_of_jsValueToObject_5() { return static_cast<int32_t>(offsetof(DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087_StaticFields, ___jsValueToObject_5)); }
	inline MethodInfo_t * get_jsValueToObject_5() const { return ___jsValueToObject_5; }
	inline MethodInfo_t ** get_address_of_jsValueToObject_5() { return &___jsValueToObject_5; }
	inline void set_jsValueToObject_5(MethodInfo_t * value)
	{
		___jsValueToObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___jsValueToObject_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTYPECONVERTER_TC48984CBA042A943B40BAE5960F493DA33962087_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T17D82877DC876D51F55BE1E79A2B1470A8C356BB_H
#define U3CU3EC__DISPLAYCLASS7_0_T17D82877DC876D51F55BE1E79A2B1470A8C356BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.DefaultTypeConverter/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t17D82877DC876D51F55BE1E79A2B1470A8C356BB  : public RuntimeObject
{
public:
	// System.Func`3<Jint.Native.JsValue,Jint.Native.JsValue[],Jint.Native.JsValue> Jint.Runtime.Interop.DefaultTypeConverter/<>c__DisplayClass7_0::function
	Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 * ___function_0;

public:
	inline static int32_t get_offset_of_function_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t17D82877DC876D51F55BE1E79A2B1470A8C356BB, ___function_0)); }
	inline Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 * get_function_0() const { return ___function_0; }
	inline Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 ** get_address_of_function_0() { return &___function_0; }
	inline void set_function_0(Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 * value)
	{
		___function_0 = value;
		Il2CppCodeGenWriteBarrier((&___function_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T17D82877DC876D51F55BE1E79A2B1470A8C356BB_H
#ifndef U3CU3EC__DISPLAYCLASS7_1_TB5C451BAEB026731D23627D50F68724297E6577B_H
#define U3CU3EC__DISPLAYCLASS7_1_TB5C451BAEB026731D23627D50F68724297E6577B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.DefaultTypeConverter/<>c__DisplayClass7_1
struct  U3CU3Ec__DisplayClass7_1_tB5C451BAEB026731D23627D50F68724297E6577B  : public RuntimeObject
{
public:
	// System.Type Jint.Runtime.Interop.DefaultTypeConverter/<>c__DisplayClass7_1::targetElementType
	Type_t * ___targetElementType_0;
	// Jint.Runtime.Interop.DefaultTypeConverter/<>c__DisplayClass7_2 Jint.Runtime.Interop.DefaultTypeConverter/<>c__DisplayClass7_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass7_2_t660F155E66573E853DEED2D23186154AD85FA4D1 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_targetElementType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_1_tB5C451BAEB026731D23627D50F68724297E6577B, ___targetElementType_0)); }
	inline Type_t * get_targetElementType_0() const { return ___targetElementType_0; }
	inline Type_t ** get_address_of_targetElementType_0() { return &___targetElementType_0; }
	inline void set_targetElementType_0(Type_t * value)
	{
		___targetElementType_0 = value;
		Il2CppCodeGenWriteBarrier((&___targetElementType_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_1_tB5C451BAEB026731D23627D50F68724297E6577B, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass7_2_t660F155E66573E853DEED2D23186154AD85FA4D1 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass7_2_t660F155E66573E853DEED2D23186154AD85FA4D1 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass7_2_t660F155E66573E853DEED2D23186154AD85FA4D1 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_1_TB5C451BAEB026731D23627D50F68724297E6577B_H
#ifndef U3CU3EC__DISPLAYCLASS7_2_T660F155E66573E853DEED2D23186154AD85FA4D1_H
#define U3CU3EC__DISPLAYCLASS7_2_T660F155E66573E853DEED2D23186154AD85FA4D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.DefaultTypeConverter/<>c__DisplayClass7_2
struct  U3CU3Ec__DisplayClass7_2_t660F155E66573E853DEED2D23186154AD85FA4D1  : public RuntimeObject
{
public:
	// System.IFormatProvider Jint.Runtime.Interop.DefaultTypeConverter/<>c__DisplayClass7_2::formatProvider
	RuntimeObject* ___formatProvider_0;
	// Jint.Runtime.Interop.DefaultTypeConverter Jint.Runtime.Interop.DefaultTypeConverter/<>c__DisplayClass7_2::<>4__this
	DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_formatProvider_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_2_t660F155E66573E853DEED2D23186154AD85FA4D1, ___formatProvider_0)); }
	inline RuntimeObject* get_formatProvider_0() const { return ___formatProvider_0; }
	inline RuntimeObject** get_address_of_formatProvider_0() { return &___formatProvider_0; }
	inline void set_formatProvider_0(RuntimeObject* value)
	{
		___formatProvider_0 = value;
		Il2CppCodeGenWriteBarrier((&___formatProvider_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_2_t660F155E66573E853DEED2D23186154AD85FA4D1, ___U3CU3E4__this_1)); }
	inline DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_2_T660F155E66573E853DEED2D23186154AD85FA4D1_H
#ifndef U3CU3EC_T75BF8A7C7D2A6B60F55126EA3553F5D68D449F28_H
#define U3CU3EC_T75BF8A7C7D2A6B60F55126EA3553F5D68D449F28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.DelegateWrapper/<>c
struct  U3CU3Ec_t75BF8A7C7D2A6B60F55126EA3553F5D68D449F28  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t75BF8A7C7D2A6B60F55126EA3553F5D68D449F28_StaticFields
{
public:
	// Jint.Runtime.Interop.DelegateWrapper/<>c Jint.Runtime.Interop.DelegateWrapper/<>c::<>9
	U3CU3Ec_t75BF8A7C7D2A6B60F55126EA3553F5D68D449F28 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.ParameterInfo,System.Boolean> Jint.Runtime.Interop.DelegateWrapper/<>c::<>9__2_0
	Func_2_t61E54F48AD8F1DC354B46A3826709996DB41375A * ___U3CU3E9__2_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t75BF8A7C7D2A6B60F55126EA3553F5D68D449F28_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t75BF8A7C7D2A6B60F55126EA3553F5D68D449F28 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t75BF8A7C7D2A6B60F55126EA3553F5D68D449F28 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t75BF8A7C7D2A6B60F55126EA3553F5D68D449F28 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t75BF8A7C7D2A6B60F55126EA3553F5D68D449F28_StaticFields, ___U3CU3E9__2_0_1)); }
	inline Func_2_t61E54F48AD8F1DC354B46A3826709996DB41375A * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline Func_2_t61E54F48AD8F1DC354B46A3826709996DB41375A ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(Func_2_t61E54F48AD8F1DC354B46A3826709996DB41375A * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T75BF8A7C7D2A6B60F55126EA3553F5D68D449F28_H
#ifndef REFERENCE_T9C84C563E43DCAACF816A789C6561EE27CD96BA1_H
#define REFERENCE_T9C84C563E43DCAACF816A789C6561EE27CD96BA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.References.Reference
struct  Reference_t9C84C563E43DCAACF816A789C6561EE27CD96BA1  : public RuntimeObject
{
public:
	// Jint.Native.JsValue Jint.Runtime.References.Reference::_baseValue
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ____baseValue_0;
	// System.String Jint.Runtime.References.Reference::_name
	String_t* ____name_1;
	// System.Boolean Jint.Runtime.References.Reference::_strict
	bool ____strict_2;

public:
	inline static int32_t get_offset_of__baseValue_0() { return static_cast<int32_t>(offsetof(Reference_t9C84C563E43DCAACF816A789C6561EE27CD96BA1, ____baseValue_0)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get__baseValue_0() const { return ____baseValue_0; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of__baseValue_0() { return &____baseValue_0; }
	inline void set__baseValue_0(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		____baseValue_0 = value;
		Il2CppCodeGenWriteBarrier((&____baseValue_0), value);
	}

	inline static int32_t get_offset_of__name_1() { return static_cast<int32_t>(offsetof(Reference_t9C84C563E43DCAACF816A789C6561EE27CD96BA1, ____name_1)); }
	inline String_t* get__name_1() const { return ____name_1; }
	inline String_t** get_address_of__name_1() { return &____name_1; }
	inline void set__name_1(String_t* value)
	{
		____name_1 = value;
		Il2CppCodeGenWriteBarrier((&____name_1), value);
	}

	inline static int32_t get_offset_of__strict_2() { return static_cast<int32_t>(offsetof(Reference_t9C84C563E43DCAACF816A789C6561EE27CD96BA1, ____strict_2)); }
	inline bool get__strict_2() const { return ____strict_2; }
	inline bool* get_address_of__strict_2() { return &____strict_2; }
	inline void set__strict_2(bool value)
	{
		____strict_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCE_T9C84C563E43DCAACF816A789C6561EE27CD96BA1_H
#ifndef STATEMENTINTERPRETER_T09AE267789508AD9BC3E14EAFE8262642B534B44_H
#define STATEMENTINTERPRETER_T09AE267789508AD9BC3E14EAFE8262642B534B44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.StatementInterpreter
struct  StatementInterpreter_t09AE267789508AD9BC3E14EAFE8262642B534B44  : public RuntimeObject
{
public:
	// Jint.Engine Jint.Runtime.StatementInterpreter::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_0;

public:
	inline static int32_t get_offset_of__engine_0() { return static_cast<int32_t>(offsetof(StatementInterpreter_t09AE267789508AD9BC3E14EAFE8262642B534B44, ____engine_0)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_0() const { return ____engine_0; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_0() { return &____engine_0; }
	inline void set__engine_0(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_0 = value;
		Il2CppCodeGenWriteBarrier((&____engine_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMENTINTERPRETER_T09AE267789508AD9BC3E14EAFE8262642B534B44_H
#ifndef TYPECONVERTER_TE6FEF1D028AF98204D37D7FED4E148C137AA0621_H
#define TYPECONVERTER_TE6FEF1D028AF98204D37D7FED4E148C137AA0621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.TypeConverter
struct  TypeConverter_tE6FEF1D028AF98204D37D7FED4E148C137AA0621  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPECONVERTER_TE6FEF1D028AF98204D37D7FED4E148C137AA0621_H
#ifndef U3CU3EC_TCF159A02D6FF7D7C726DAA26C516157A941E0008_H
#define U3CU3EC_TCF159A02D6FF7D7C726DAA26C516157A941E0008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.TypeConverter/<>c
struct  U3CU3Ec_tCF159A02D6FF7D7C726DAA26C516157A941E0008  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tCF159A02D6FF7D7C726DAA26C516157A941E0008_StaticFields
{
public:
	// Jint.Runtime.TypeConverter/<>c Jint.Runtime.TypeConverter/<>c::<>9
	U3CU3Ec_tCF159A02D6FF7D7C726DAA26C516157A941E0008 * ___U3CU3E9_0;
	// System.Func`2<Jint.Native.JsValue,System.Object> Jint.Runtime.TypeConverter/<>c::<>9__12_1
	Func_2_tF2150A815C34C577F510DEC721A30AA620438CF2 * ___U3CU3E9__12_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCF159A02D6FF7D7C726DAA26C516157A941E0008_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tCF159A02D6FF7D7C726DAA26C516157A941E0008 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tCF159A02D6FF7D7C726DAA26C516157A941E0008 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tCF159A02D6FF7D7C726DAA26C516157A941E0008 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCF159A02D6FF7D7C726DAA26C516157A941E0008_StaticFields, ___U3CU3E9__12_1_1)); }
	inline Func_2_tF2150A815C34C577F510DEC721A30AA620438CF2 * get_U3CU3E9__12_1_1() const { return ___U3CU3E9__12_1_1; }
	inline Func_2_tF2150A815C34C577F510DEC721A30AA620438CF2 ** get_address_of_U3CU3E9__12_1_1() { return &___U3CU3E9__12_1_1; }
	inline void set_U3CU3E9__12_1_1(Func_2_tF2150A815C34C577F510DEC721A30AA620438CF2 * value)
	{
		___U3CU3E9__12_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TCF159A02D6FF7D7C726DAA26C516157A941E0008_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T47AD677AC45EF44A6929008C6F40F9BA8FE33C19_H
#define U3CU3EC__DISPLAYCLASS12_0_T47AD677AC45EF44A6929008C6F40F9BA8FE33C19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.TypeConverter/<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t47AD677AC45EF44A6929008C6F40F9BA8FE33C19  : public RuntimeObject
{
public:
	// Jint.Native.JsValue[] Jint.Runtime.TypeConverter/<>c__DisplayClass12_0::arguments
	JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* ___arguments_0;

public:
	inline static int32_t get_offset_of_arguments_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t47AD677AC45EF44A6929008C6F40F9BA8FE33C19, ___arguments_0)); }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* get_arguments_0() const { return ___arguments_0; }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88** get_address_of_arguments_0() { return &___arguments_0; }
	inline void set_arguments_0(JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* value)
	{
		___arguments_0 = value;
		Il2CppCodeGenWriteBarrier((&___arguments_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T47AD677AC45EF44A6929008C6F40F9BA8FE33C19_H
#ifndef U3CFINDBESTMATCHU3ED__12_T6DCCBA58159B71022BAC8133FC1BA608E4272872_H
#define U3CFINDBESTMATCHU3ED__12_T6DCCBA58159B71022BAC8133FC1BA608E4272872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.TypeConverter/<FindBestMatch>d__12
struct  U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872  : public RuntimeObject
{
public:
	// System.Int32 Jint.Runtime.TypeConverter/<FindBestMatch>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Reflection.MethodBase Jint.Runtime.TypeConverter/<FindBestMatch>d__12::<>2__current
	MethodBase_t * ___U3CU3E2__current_1;
	// System.Int32 Jint.Runtime.TypeConverter/<FindBestMatch>d__12::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Jint.Native.JsValue[] Jint.Runtime.TypeConverter/<FindBestMatch>d__12::arguments
	JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* ___arguments_3;
	// Jint.Native.JsValue[] Jint.Runtime.TypeConverter/<FindBestMatch>d__12::<>3__arguments
	JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* ___U3CU3E3__arguments_4;
	// System.Reflection.MethodBase[] Jint.Runtime.TypeConverter/<FindBestMatch>d__12::methods
	MethodBaseU5BU5D_t57FAB09E20C4CE1DD48A19FB3E74821FDFD34914* ___methods_5;
	// System.Reflection.MethodBase[] Jint.Runtime.TypeConverter/<FindBestMatch>d__12::<>3__methods
	MethodBaseU5BU5D_t57FAB09E20C4CE1DD48A19FB3E74821FDFD34914* ___U3CU3E3__methods_6;
	// Jint.Runtime.TypeConverter/<>c__DisplayClass12_0 Jint.Runtime.TypeConverter/<FindBestMatch>d__12::<>8__1
	U3CU3Ec__DisplayClass12_0_t47AD677AC45EF44A6929008C6F40F9BA8FE33C19 * ___U3CU3E8__1_7;
	// System.Object[] Jint.Runtime.TypeConverter/<FindBestMatch>d__12::<objectArguments>5__2
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___U3CobjectArgumentsU3E5__2_8;
	// System.Reflection.MethodBase[] Jint.Runtime.TypeConverter/<FindBestMatch>d__12::<>7__wrap1
	MethodBaseU5BU5D_t57FAB09E20C4CE1DD48A19FB3E74821FDFD34914* ___U3CU3E7__wrap1_9;
	// System.Int32 Jint.Runtime.TypeConverter/<FindBestMatch>d__12::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872, ___U3CU3E2__current_1)); }
	inline MethodBase_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline MethodBase_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(MethodBase_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_arguments_3() { return static_cast<int32_t>(offsetof(U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872, ___arguments_3)); }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* get_arguments_3() const { return ___arguments_3; }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88** get_address_of_arguments_3() { return &___arguments_3; }
	inline void set_arguments_3(JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* value)
	{
		___arguments_3 = value;
		Il2CppCodeGenWriteBarrier((&___arguments_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__arguments_4() { return static_cast<int32_t>(offsetof(U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872, ___U3CU3E3__arguments_4)); }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* get_U3CU3E3__arguments_4() const { return ___U3CU3E3__arguments_4; }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88** get_address_of_U3CU3E3__arguments_4() { return &___U3CU3E3__arguments_4; }
	inline void set_U3CU3E3__arguments_4(JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* value)
	{
		___U3CU3E3__arguments_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__arguments_4), value);
	}

	inline static int32_t get_offset_of_methods_5() { return static_cast<int32_t>(offsetof(U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872, ___methods_5)); }
	inline MethodBaseU5BU5D_t57FAB09E20C4CE1DD48A19FB3E74821FDFD34914* get_methods_5() const { return ___methods_5; }
	inline MethodBaseU5BU5D_t57FAB09E20C4CE1DD48A19FB3E74821FDFD34914** get_address_of_methods_5() { return &___methods_5; }
	inline void set_methods_5(MethodBaseU5BU5D_t57FAB09E20C4CE1DD48A19FB3E74821FDFD34914* value)
	{
		___methods_5 = value;
		Il2CppCodeGenWriteBarrier((&___methods_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__methods_6() { return static_cast<int32_t>(offsetof(U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872, ___U3CU3E3__methods_6)); }
	inline MethodBaseU5BU5D_t57FAB09E20C4CE1DD48A19FB3E74821FDFD34914* get_U3CU3E3__methods_6() const { return ___U3CU3E3__methods_6; }
	inline MethodBaseU5BU5D_t57FAB09E20C4CE1DD48A19FB3E74821FDFD34914** get_address_of_U3CU3E3__methods_6() { return &___U3CU3E3__methods_6; }
	inline void set_U3CU3E3__methods_6(MethodBaseU5BU5D_t57FAB09E20C4CE1DD48A19FB3E74821FDFD34914* value)
	{
		___U3CU3E3__methods_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__methods_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_7() { return static_cast<int32_t>(offsetof(U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872, ___U3CU3E8__1_7)); }
	inline U3CU3Ec__DisplayClass12_0_t47AD677AC45EF44A6929008C6F40F9BA8FE33C19 * get_U3CU3E8__1_7() const { return ___U3CU3E8__1_7; }
	inline U3CU3Ec__DisplayClass12_0_t47AD677AC45EF44A6929008C6F40F9BA8FE33C19 ** get_address_of_U3CU3E8__1_7() { return &___U3CU3E8__1_7; }
	inline void set_U3CU3E8__1_7(U3CU3Ec__DisplayClass12_0_t47AD677AC45EF44A6929008C6F40F9BA8FE33C19 * value)
	{
		___U3CU3E8__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_7), value);
	}

	inline static int32_t get_offset_of_U3CobjectArgumentsU3E5__2_8() { return static_cast<int32_t>(offsetof(U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872, ___U3CobjectArgumentsU3E5__2_8)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_U3CobjectArgumentsU3E5__2_8() const { return ___U3CobjectArgumentsU3E5__2_8; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_U3CobjectArgumentsU3E5__2_8() { return &___U3CobjectArgumentsU3E5__2_8; }
	inline void set_U3CobjectArgumentsU3E5__2_8(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___U3CobjectArgumentsU3E5__2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CobjectArgumentsU3E5__2_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_9() { return static_cast<int32_t>(offsetof(U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872, ___U3CU3E7__wrap1_9)); }
	inline MethodBaseU5BU5D_t57FAB09E20C4CE1DD48A19FB3E74821FDFD34914* get_U3CU3E7__wrap1_9() const { return ___U3CU3E7__wrap1_9; }
	inline MethodBaseU5BU5D_t57FAB09E20C4CE1DD48A19FB3E74821FDFD34914** get_address_of_U3CU3E7__wrap1_9() { return &___U3CU3E7__wrap1_9; }
	inline void set_U3CU3E7__wrap1_9(MethodBaseU5BU5D_t57FAB09E20C4CE1DD48A19FB3E74821FDFD34914* value)
	{
		___U3CU3E7__wrap1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_10() { return static_cast<int32_t>(offsetof(U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872, ___U3CU3E7__wrap2_10)); }
	inline int32_t get_U3CU3E7__wrap2_10() const { return ___U3CU3E7__wrap2_10; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_10() { return &___U3CU3E7__wrap2_10; }
	inline void set_U3CU3E7__wrap2_10(int32_t value)
	{
		___U3CU3E7__wrap2_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDBESTMATCHU3ED__12_T6DCCBA58159B71022BAC8133FC1BA608E4272872_H
#ifndef STRICTMODESCOPE_T0361C75C19C33F43BBFA4EB68CA32044589A43A4_H
#define STRICTMODESCOPE_T0361C75C19C33F43BBFA4EB68CA32044589A43A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.StrictModeScope
struct  StrictModeScope_t0361C75C19C33F43BBFA4EB68CA32044589A43A4  : public RuntimeObject
{
public:
	// System.Boolean Jint.StrictModeScope::_strict
	bool ____strict_0;
	// System.Boolean Jint.StrictModeScope::_force
	bool ____force_1;
	// System.Int32 Jint.StrictModeScope::_forcedRefCount
	int32_t ____forcedRefCount_2;

public:
	inline static int32_t get_offset_of__strict_0() { return static_cast<int32_t>(offsetof(StrictModeScope_t0361C75C19C33F43BBFA4EB68CA32044589A43A4, ____strict_0)); }
	inline bool get__strict_0() const { return ____strict_0; }
	inline bool* get_address_of__strict_0() { return &____strict_0; }
	inline void set__strict_0(bool value)
	{
		____strict_0 = value;
	}

	inline static int32_t get_offset_of__force_1() { return static_cast<int32_t>(offsetof(StrictModeScope_t0361C75C19C33F43BBFA4EB68CA32044589A43A4, ____force_1)); }
	inline bool get__force_1() const { return ____force_1; }
	inline bool* get_address_of__force_1() { return &____force_1; }
	inline void set__force_1(bool value)
	{
		____force_1 = value;
	}

	inline static int32_t get_offset_of__forcedRefCount_2() { return static_cast<int32_t>(offsetof(StrictModeScope_t0361C75C19C33F43BBFA4EB68CA32044589A43A4, ____forcedRefCount_2)); }
	inline int32_t get__forcedRefCount_2() const { return ____forcedRefCount_2; }
	inline int32_t* get_address_of__forcedRefCount_2() { return &____forcedRefCount_2; }
	inline void set__forcedRefCount_2(int32_t value)
	{
		____forcedRefCount_2 = value;
	}
};

struct StrictModeScope_t0361C75C19C33F43BBFA4EB68CA32044589A43A4_ThreadStaticFields
{
public:
	// System.Int32 Jint.StrictModeScope::_refCount
	int32_t ____refCount_3;

public:
	inline static int32_t get_offset_of__refCount_3() { return static_cast<int32_t>(offsetof(StrictModeScope_t0361C75C19C33F43BBFA4EB68CA32044589A43A4_ThreadStaticFields, ____refCount_3)); }
	inline int32_t get__refCount_3() const { return ____refCount_3; }
	inline int32_t* get_address_of__refCount_3() { return &____refCount_3; }
	inline void set__refCount_3(int32_t value)
	{
		____refCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRICTMODESCOPE_T0361C75C19C33F43BBFA4EB68CA32044589A43A4_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef COLOR_T70494B978F490884EFB36116AD8C25F5E943C3E0_H
#define COLOR_T70494B978F490884EFB36116AD8C25F5E943C3E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Color
struct  Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 
{
public:
	// System.Single GLTF.Math.Color::<R>k__BackingField
	float ___U3CRU3Ek__BackingField_0;
	// System.Single GLTF.Math.Color::<G>k__BackingField
	float ___U3CGU3Ek__BackingField_1;
	// System.Single GLTF.Math.Color::<B>k__BackingField
	float ___U3CBU3Ek__BackingField_2;
	// System.Single GLTF.Math.Color::<A>k__BackingField
	float ___U3CAU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CRU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0, ___U3CRU3Ek__BackingField_0)); }
	inline float get_U3CRU3Ek__BackingField_0() const { return ___U3CRU3Ek__BackingField_0; }
	inline float* get_address_of_U3CRU3Ek__BackingField_0() { return &___U3CRU3Ek__BackingField_0; }
	inline void set_U3CRU3Ek__BackingField_0(float value)
	{
		___U3CRU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CGU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0, ___U3CGU3Ek__BackingField_1)); }
	inline float get_U3CGU3Ek__BackingField_1() const { return ___U3CGU3Ek__BackingField_1; }
	inline float* get_address_of_U3CGU3Ek__BackingField_1() { return &___U3CGU3Ek__BackingField_1; }
	inline void set_U3CGU3Ek__BackingField_1(float value)
	{
		___U3CGU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CBU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0, ___U3CBU3Ek__BackingField_2)); }
	inline float get_U3CBU3Ek__BackingField_2() const { return ___U3CBU3Ek__BackingField_2; }
	inline float* get_address_of_U3CBU3Ek__BackingField_2() { return &___U3CBU3Ek__BackingField_2; }
	inline void set_U3CBU3Ek__BackingField_2(float value)
	{
		___U3CBU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CAU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0, ___U3CAU3Ek__BackingField_3)); }
	inline float get_U3CAU3Ek__BackingField_3() const { return ___U3CAU3Ek__BackingField_3; }
	inline float* get_address_of_U3CAU3Ek__BackingField_3() { return &___U3CAU3Ek__BackingField_3; }
	inline void set_U3CAU3Ek__BackingField_3(float value)
	{
		___U3CAU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T70494B978F490884EFB36116AD8C25F5E943C3E0_H
#ifndef QUATERNION_TDD11C744DF75B596B009A479F067D431EAB4C2A8_H
#define QUATERNION_TDD11C744DF75B596B009A479F067D431EAB4C2A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Quaternion
struct  Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8 
{
public:
	// System.Single GLTF.Math.Quaternion::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_1;
	// System.Single GLTF.Math.Quaternion::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_2;
	// System.Single GLTF.Math.Quaternion::<Z>k__BackingField
	float ___U3CZU3Ek__BackingField_3;
	// System.Single GLTF.Math.Quaternion::<W>k__BackingField
	float ___U3CWU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8, ___U3CXU3Ek__BackingField_1)); }
	inline float get_U3CXU3Ek__BackingField_1() const { return ___U3CXU3Ek__BackingField_1; }
	inline float* get_address_of_U3CXU3Ek__BackingField_1() { return &___U3CXU3Ek__BackingField_1; }
	inline void set_U3CXU3Ek__BackingField_1(float value)
	{
		___U3CXU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8, ___U3CYU3Ek__BackingField_2)); }
	inline float get_U3CYU3Ek__BackingField_2() const { return ___U3CYU3Ek__BackingField_2; }
	inline float* get_address_of_U3CYU3Ek__BackingField_2() { return &___U3CYU3Ek__BackingField_2; }
	inline void set_U3CYU3Ek__BackingField_2(float value)
	{
		___U3CYU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CZU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8, ___U3CZU3Ek__BackingField_3)); }
	inline float get_U3CZU3Ek__BackingField_3() const { return ___U3CZU3Ek__BackingField_3; }
	inline float* get_address_of_U3CZU3Ek__BackingField_3() { return &___U3CZU3Ek__BackingField_3; }
	inline void set_U3CZU3Ek__BackingField_3(float value)
	{
		___U3CZU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CWU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8, ___U3CWU3Ek__BackingField_4)); }
	inline float get_U3CWU3Ek__BackingField_4() const { return ___U3CWU3Ek__BackingField_4; }
	inline float* get_address_of_U3CWU3Ek__BackingField_4() { return &___U3CWU3Ek__BackingField_4; }
	inline void set_U3CWU3Ek__BackingField_4(float value)
	{
		___U3CWU3Ek__BackingField_4 = value;
	}
};

struct Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8_StaticFields
{
public:
	// GLTF.Math.Quaternion GLTF.Math.Quaternion::Identity
	Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8  ___Identity_0;

public:
	inline static int32_t get_offset_of_Identity_0() { return static_cast<int32_t>(offsetof(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8_StaticFields, ___Identity_0)); }
	inline Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8  get_Identity_0() const { return ___Identity_0; }
	inline Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8 * get_address_of_Identity_0() { return &___Identity_0; }
	inline void set_Identity_0(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8  value)
	{
		___Identity_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_TDD11C744DF75B596B009A479F067D431EAB4C2A8_H
#ifndef VECTOR3_T23F8955612D53E66C78FE37F87B1C09E9D78B28C_H
#define VECTOR3_T23F8955612D53E66C78FE37F87B1C09E9D78B28C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Vector3
struct  Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C 
{
public:
	// System.Single GLTF.Math.Vector3::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_2;
	// System.Single GLTF.Math.Vector3::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_3;
	// System.Single GLTF.Math.Vector3::<Z>k__BackingField
	float ___U3CZU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C, ___U3CXU3Ek__BackingField_2)); }
	inline float get_U3CXU3Ek__BackingField_2() const { return ___U3CXU3Ek__BackingField_2; }
	inline float* get_address_of_U3CXU3Ek__BackingField_2() { return &___U3CXU3Ek__BackingField_2; }
	inline void set_U3CXU3Ek__BackingField_2(float value)
	{
		___U3CXU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C, ___U3CYU3Ek__BackingField_3)); }
	inline float get_U3CYU3Ek__BackingField_3() const { return ___U3CYU3Ek__BackingField_3; }
	inline float* get_address_of_U3CYU3Ek__BackingField_3() { return &___U3CYU3Ek__BackingField_3; }
	inline void set_U3CYU3Ek__BackingField_3(float value)
	{
		___U3CYU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CZU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C, ___U3CZU3Ek__BackingField_4)); }
	inline float get_U3CZU3Ek__BackingField_4() const { return ___U3CZU3Ek__BackingField_4; }
	inline float* get_address_of_U3CZU3Ek__BackingField_4() { return &___U3CZU3Ek__BackingField_4; }
	inline void set_U3CZU3Ek__BackingField_4(float value)
	{
		___U3CZU3Ek__BackingField_4 = value;
	}
};

struct Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C_StaticFields
{
public:
	// GLTF.Math.Vector3 GLTF.Math.Vector3::Zero
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___Zero_0;
	// GLTF.Math.Vector3 GLTF.Math.Vector3::One
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___One_1;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C_StaticFields, ___Zero_0)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_Zero_0() const { return ___Zero_0; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___Zero_0 = value;
	}

	inline static int32_t get_offset_of_One_1() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C_StaticFields, ___One_1)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_One_1() const { return ___One_1; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_One_1() { return &___One_1; }
	inline void set_One_1(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___One_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T23F8955612D53E66C78FE37F87B1C09E9D78B28C_H
#ifndef ACCESSORID_T6C51102D50B95D309D47FD946FE62ED76C16B83F_H
#define ACCESSORID_T6C51102D50B95D309D47FD946FE62ED76C16B83F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AccessorId
struct  AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F  : public GLTFId_1_t03C6D4307DA5F7D32A8A310F6F43A4992728AEC7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSORID_T6C51102D50B95D309D47FD946FE62ED76C16B83F_H
#ifndef ACCESSORSPARSE_T503861445674C5161C3AF2F1D15EA13BA2F6A69C_H
#define ACCESSORSPARSE_T503861445674C5161C3AF2F1D15EA13BA2F6A69C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AccessorSparse
struct  AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// System.Int32 GLTF.Schema.AccessorSparse::Count
	int32_t ___Count_4;
	// GLTF.Schema.AccessorSparseIndices GLTF.Schema.AccessorSparse::Indices
	AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9 * ___Indices_5;
	// GLTF.Schema.AccessorSparseValues GLTF.Schema.AccessorSparse::Values
	AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3 * ___Values_6;

public:
	inline static int32_t get_offset_of_Count_4() { return static_cast<int32_t>(offsetof(AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C, ___Count_4)); }
	inline int32_t get_Count_4() const { return ___Count_4; }
	inline int32_t* get_address_of_Count_4() { return &___Count_4; }
	inline void set_Count_4(int32_t value)
	{
		___Count_4 = value;
	}

	inline static int32_t get_offset_of_Indices_5() { return static_cast<int32_t>(offsetof(AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C, ___Indices_5)); }
	inline AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9 * get_Indices_5() const { return ___Indices_5; }
	inline AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9 ** get_address_of_Indices_5() { return &___Indices_5; }
	inline void set_Indices_5(AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9 * value)
	{
		___Indices_5 = value;
		Il2CppCodeGenWriteBarrier((&___Indices_5), value);
	}

	inline static int32_t get_offset_of_Values_6() { return static_cast<int32_t>(offsetof(AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C, ___Values_6)); }
	inline AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3 * get_Values_6() const { return ___Values_6; }
	inline AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3 ** get_address_of_Values_6() { return &___Values_6; }
	inline void set_Values_6(AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3 * value)
	{
		___Values_6 = value;
		Il2CppCodeGenWriteBarrier((&___Values_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSORSPARSE_T503861445674C5161C3AF2F1D15EA13BA2F6A69C_H
#ifndef ACCESSORSPARSEVALUES_TE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3_H
#define ACCESSORSPARSEVALUES_TE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AccessorSparseValues
struct  AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Schema.BufferViewId GLTF.Schema.AccessorSparseValues::BufferView
	BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * ___BufferView_4;
	// System.Int32 GLTF.Schema.AccessorSparseValues::ByteOffset
	int32_t ___ByteOffset_5;

public:
	inline static int32_t get_offset_of_BufferView_4() { return static_cast<int32_t>(offsetof(AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3, ___BufferView_4)); }
	inline BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * get_BufferView_4() const { return ___BufferView_4; }
	inline BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 ** get_address_of_BufferView_4() { return &___BufferView_4; }
	inline void set_BufferView_4(BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * value)
	{
		___BufferView_4 = value;
		Il2CppCodeGenWriteBarrier((&___BufferView_4), value);
	}

	inline static int32_t get_offset_of_ByteOffset_5() { return static_cast<int32_t>(offsetof(AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3, ___ByteOffset_5)); }
	inline int32_t get_ByteOffset_5() const { return ___ByteOffset_5; }
	inline int32_t* get_address_of_ByteOffset_5() { return &___ByteOffset_5; }
	inline void set_ByteOffset_5(int32_t value)
	{
		___ByteOffset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSORSPARSEVALUES_TE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3_H
#ifndef ANIMATIONCHANNEL_T904A44258FFDCEBCDE71AFA92ED2514EE891BFC1_H
#define ANIMATIONCHANNEL_T904A44258FFDCEBCDE71AFA92ED2514EE891BFC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AnimationChannel
struct  AnimationChannel_t904A44258FFDCEBCDE71AFA92ED2514EE891BFC1  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Schema.SamplerId GLTF.Schema.AnimationChannel::Sampler
	SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 * ___Sampler_4;
	// GLTF.Schema.AnimationChannelTarget GLTF.Schema.AnimationChannel::Target
	AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3 * ___Target_5;

public:
	inline static int32_t get_offset_of_Sampler_4() { return static_cast<int32_t>(offsetof(AnimationChannel_t904A44258FFDCEBCDE71AFA92ED2514EE891BFC1, ___Sampler_4)); }
	inline SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 * get_Sampler_4() const { return ___Sampler_4; }
	inline SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 ** get_address_of_Sampler_4() { return &___Sampler_4; }
	inline void set_Sampler_4(SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 * value)
	{
		___Sampler_4 = value;
		Il2CppCodeGenWriteBarrier((&___Sampler_4), value);
	}

	inline static int32_t get_offset_of_Target_5() { return static_cast<int32_t>(offsetof(AnimationChannel_t904A44258FFDCEBCDE71AFA92ED2514EE891BFC1, ___Target_5)); }
	inline AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3 * get_Target_5() const { return ___Target_5; }
	inline AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3 ** get_address_of_Target_5() { return &___Target_5; }
	inline void set_Target_5(AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3 * value)
	{
		___Target_5 = value;
		Il2CppCodeGenWriteBarrier((&___Target_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCHANNEL_T904A44258FFDCEBCDE71AFA92ED2514EE891BFC1_H
#ifndef ASSET_T11E5855206F01C1EC0CBA2520D06BD265A17D72B_H
#define ASSET_T11E5855206F01C1EC0CBA2520D06BD265A17D72B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Asset
struct  Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// System.String GLTF.Schema.Asset::Copyright
	String_t* ___Copyright_4;
	// System.String GLTF.Schema.Asset::Generator
	String_t* ___Generator_5;
	// System.String GLTF.Schema.Asset::Version
	String_t* ___Version_6;
	// System.String GLTF.Schema.Asset::MinVersion
	String_t* ___MinVersion_7;

public:
	inline static int32_t get_offset_of_Copyright_4() { return static_cast<int32_t>(offsetof(Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B, ___Copyright_4)); }
	inline String_t* get_Copyright_4() const { return ___Copyright_4; }
	inline String_t** get_address_of_Copyright_4() { return &___Copyright_4; }
	inline void set_Copyright_4(String_t* value)
	{
		___Copyright_4 = value;
		Il2CppCodeGenWriteBarrier((&___Copyright_4), value);
	}

	inline static int32_t get_offset_of_Generator_5() { return static_cast<int32_t>(offsetof(Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B, ___Generator_5)); }
	inline String_t* get_Generator_5() const { return ___Generator_5; }
	inline String_t** get_address_of_Generator_5() { return &___Generator_5; }
	inline void set_Generator_5(String_t* value)
	{
		___Generator_5 = value;
		Il2CppCodeGenWriteBarrier((&___Generator_5), value);
	}

	inline static int32_t get_offset_of_Version_6() { return static_cast<int32_t>(offsetof(Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B, ___Version_6)); }
	inline String_t* get_Version_6() const { return ___Version_6; }
	inline String_t** get_address_of_Version_6() { return &___Version_6; }
	inline void set_Version_6(String_t* value)
	{
		___Version_6 = value;
		Il2CppCodeGenWriteBarrier((&___Version_6), value);
	}

	inline static int32_t get_offset_of_MinVersion_7() { return static_cast<int32_t>(offsetof(Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B, ___MinVersion_7)); }
	inline String_t* get_MinVersion_7() const { return ___MinVersion_7; }
	inline String_t** get_address_of_MinVersion_7() { return &___MinVersion_7; }
	inline void set_MinVersion_7(String_t* value)
	{
		___MinVersion_7 = value;
		Il2CppCodeGenWriteBarrier((&___MinVersion_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSET_T11E5855206F01C1EC0CBA2520D06BD265A17D72B_H
#ifndef BUFFERID_T1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4_H
#define BUFFERID_T1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.BufferId
struct  BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4  : public GLTFId_1_t050A1D4340BD22B23EED14DBBBDEE97C66D39D50
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERID_T1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4_H
#ifndef BUFFERVIEWID_TAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871_H
#define BUFFERVIEWID_TAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.BufferViewId
struct  BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871  : public GLTFId_1_t42D30DE0867573822EFF276155616B8DA56E3E8F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERVIEWID_TAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871_H
#ifndef CAMERAID_T473F137B518358316FD6D1100DFF1D190E849BAC_H
#define CAMERAID_T473F137B518358316FD6D1100DFF1D190E849BAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.CameraId
struct  CameraId_t473F137B518358316FD6D1100DFF1D190E849BAC  : public GLTFId_1_t7D04B2FBE14BCDA08AD90BBDE17E15826C0C7609
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAID_T473F137B518358316FD6D1100DFF1D190E849BAC_H
#ifndef CAMERAORTHOGRAPHIC_TD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A_H
#define CAMERAORTHOGRAPHIC_TD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.CameraOrthographic
struct  CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// System.Double GLTF.Schema.CameraOrthographic::XMag
	double ___XMag_4;
	// System.Double GLTF.Schema.CameraOrthographic::YMag
	double ___YMag_5;
	// System.Double GLTF.Schema.CameraOrthographic::ZFar
	double ___ZFar_6;
	// System.Double GLTF.Schema.CameraOrthographic::ZNear
	double ___ZNear_7;

public:
	inline static int32_t get_offset_of_XMag_4() { return static_cast<int32_t>(offsetof(CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A, ___XMag_4)); }
	inline double get_XMag_4() const { return ___XMag_4; }
	inline double* get_address_of_XMag_4() { return &___XMag_4; }
	inline void set_XMag_4(double value)
	{
		___XMag_4 = value;
	}

	inline static int32_t get_offset_of_YMag_5() { return static_cast<int32_t>(offsetof(CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A, ___YMag_5)); }
	inline double get_YMag_5() const { return ___YMag_5; }
	inline double* get_address_of_YMag_5() { return &___YMag_5; }
	inline void set_YMag_5(double value)
	{
		___YMag_5 = value;
	}

	inline static int32_t get_offset_of_ZFar_6() { return static_cast<int32_t>(offsetof(CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A, ___ZFar_6)); }
	inline double get_ZFar_6() const { return ___ZFar_6; }
	inline double* get_address_of_ZFar_6() { return &___ZFar_6; }
	inline void set_ZFar_6(double value)
	{
		___ZFar_6 = value;
	}

	inline static int32_t get_offset_of_ZNear_7() { return static_cast<int32_t>(offsetof(CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A, ___ZNear_7)); }
	inline double get_ZNear_7() const { return ___ZNear_7; }
	inline double* get_address_of_ZNear_7() { return &___ZNear_7; }
	inline void set_ZNear_7(double value)
	{
		___ZNear_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAORTHOGRAPHIC_TD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A_H
#ifndef CAMERAPERSPECTIVE_T3797529B399B6B6CE2135A91F19968A7AD356F2A_H
#define CAMERAPERSPECTIVE_T3797529B399B6B6CE2135A91F19968A7AD356F2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.CameraPerspective
struct  CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// System.Double GLTF.Schema.CameraPerspective::AspectRatio
	double ___AspectRatio_4;
	// System.Double GLTF.Schema.CameraPerspective::YFov
	double ___YFov_5;
	// System.Double GLTF.Schema.CameraPerspective::ZFar
	double ___ZFar_6;
	// System.Double GLTF.Schema.CameraPerspective::ZNear
	double ___ZNear_7;

public:
	inline static int32_t get_offset_of_AspectRatio_4() { return static_cast<int32_t>(offsetof(CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A, ___AspectRatio_4)); }
	inline double get_AspectRatio_4() const { return ___AspectRatio_4; }
	inline double* get_address_of_AspectRatio_4() { return &___AspectRatio_4; }
	inline void set_AspectRatio_4(double value)
	{
		___AspectRatio_4 = value;
	}

	inline static int32_t get_offset_of_YFov_5() { return static_cast<int32_t>(offsetof(CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A, ___YFov_5)); }
	inline double get_YFov_5() const { return ___YFov_5; }
	inline double* get_address_of_YFov_5() { return &___YFov_5; }
	inline void set_YFov_5(double value)
	{
		___YFov_5 = value;
	}

	inline static int32_t get_offset_of_ZFar_6() { return static_cast<int32_t>(offsetof(CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A, ___ZFar_6)); }
	inline double get_ZFar_6() const { return ___ZFar_6; }
	inline double* get_address_of_ZFar_6() { return &___ZFar_6; }
	inline void set_ZFar_6(double value)
	{
		___ZFar_6 = value;
	}

	inline static int32_t get_offset_of_ZNear_7() { return static_cast<int32_t>(offsetof(CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A, ___ZNear_7)); }
	inline double get_ZNear_7() const { return ___ZNear_7; }
	inline double* get_address_of_ZNear_7() { return &___ZNear_7; }
	inline void set_ZNear_7(double value)
	{
		___ZNear_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAPERSPECTIVE_T3797529B399B6B6CE2135A91F19968A7AD356F2A_H
#ifndef DEFAULTEXTENSIONFACTORY_T6F7054C0E34948CE71E4DB22B285190D70C9B3E8_H
#define DEFAULTEXTENSIONFACTORY_T6F7054C0E34948CE71E4DB22B285190D70C9B3E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.DefaultExtensionFactory
struct  DefaultExtensionFactory_t6F7054C0E34948CE71E4DB22B285190D70C9B3E8  : public ExtensionFactory_t9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTEXTENSIONFACTORY_T6F7054C0E34948CE71E4DB22B285190D70C9B3E8_H
#ifndef GLTFCHILDOFROOTPROPERTY_T162116062E3D11D0057F6780F387ED5EBE6C268C_H
#define GLTFCHILDOFROOTPROPERTY_T162116062E3D11D0057F6780F387ED5EBE6C268C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFChildOfRootProperty
struct  GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// System.String GLTF.Schema.GLTFChildOfRootProperty::Name
	String_t* ___Name_4;

public:
	inline static int32_t get_offset_of_Name_4() { return static_cast<int32_t>(offsetof(GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C, ___Name_4)); }
	inline String_t* get_Name_4() const { return ___Name_4; }
	inline String_t** get_address_of_Name_4() { return &___Name_4; }
	inline void set_Name_4(String_t* value)
	{
		___Name_4 = value;
		Il2CppCodeGenWriteBarrier((&___Name_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFCHILDOFROOTPROPERTY_T162116062E3D11D0057F6780F387ED5EBE6C268C_H
#ifndef GLTFROOT_T3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B_H
#define GLTFROOT_T3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFRoot
struct  GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// System.Collections.Generic.List`1<System.String> GLTF.Schema.GLTFRoot::ExtensionsUsed
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___ExtensionsUsed_4;
	// System.Collections.Generic.List`1<System.String> GLTF.Schema.GLTFRoot::ExtensionsRequired
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___ExtensionsRequired_5;
	// System.Collections.Generic.List`1<GLTF.Schema.Accessor> GLTF.Schema.GLTFRoot::Accessors
	List_1_t1D43B87DCB1AE56CB55C174223A36A1EB6D9C49C * ___Accessors_6;
	// System.Collections.Generic.List`1<GLTF.Schema.GLTFAnimation> GLTF.Schema.GLTFRoot::Animations
	List_1_tD8A55D9A512ACB5ED7DD0B60F6C698FFE3B45E31 * ___Animations_7;
	// GLTF.Schema.Asset GLTF.Schema.GLTFRoot::Asset
	Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B * ___Asset_8;
	// System.Collections.Generic.List`1<GLTF.Schema.GLTFBuffer> GLTF.Schema.GLTFRoot::Buffers
	List_1_tF06DC5C3087205AE2F10AF9AC71D1269056533AB * ___Buffers_9;
	// System.Collections.Generic.List`1<GLTF.Schema.BufferView> GLTF.Schema.GLTFRoot::BufferViews
	List_1_t0B6CA53804AD47B0392209FA3ACF7E438A089103 * ___BufferViews_10;
	// System.Collections.Generic.List`1<GLTF.Schema.GLTFCamera> GLTF.Schema.GLTFRoot::Cameras
	List_1_t7428FD3F36775D25A7259C170DB7744093DAB21A * ___Cameras_11;
	// System.Collections.Generic.List`1<GLTF.Schema.GLTFImage> GLTF.Schema.GLTFRoot::Images
	List_1_t2011D662ECF2F170439E8C1761A7CA25340B8D4D * ___Images_12;
	// System.Collections.Generic.List`1<GLTF.Schema.GLTFMaterial> GLTF.Schema.GLTFRoot::Materials
	List_1_t23A108C07696BA8C00032540F033FC43C906DD59 * ___Materials_13;
	// System.Collections.Generic.List`1<GLTF.Schema.GLTFMesh> GLTF.Schema.GLTFRoot::Meshes
	List_1_tE1B23A0E5E130978135AD98A31C0681898E3E848 * ___Meshes_14;
	// System.Collections.Generic.List`1<GLTF.Schema.Node> GLTF.Schema.GLTFRoot::Nodes
	List_1_tF08BAB889585EE649E734FE29BBA9A3054BD11E7 * ___Nodes_15;
	// System.Collections.Generic.List`1<GLTF.Schema.Sampler> GLTF.Schema.GLTFRoot::Samplers
	List_1_t5819984D691421754B9A3EB79CDF449D70DE4A01 * ___Samplers_16;
	// GLTF.Schema.SceneId GLTF.Schema.GLTFRoot::Scene
	SceneId_t67CF37B99AF8D98A11C03A45BDD51773EE6D605C * ___Scene_17;
	// System.Collections.Generic.List`1<GLTF.Schema.GLTFScene> GLTF.Schema.GLTFRoot::Scenes
	List_1_tAD6C1545E6E6F1ECD0B394490A03C15DB1DB3155 * ___Scenes_18;
	// System.Collections.Generic.List`1<GLTF.Schema.Skin> GLTF.Schema.GLTFRoot::Skins
	List_1_tC787C2CEC6D43FC21576E063D82DB77D6A4FCED4 * ___Skins_19;
	// System.Collections.Generic.List`1<GLTF.Schema.GLTFTexture> GLTF.Schema.GLTFRoot::Textures
	List_1_t672F6CB7A561409CD238343E02BBC74100D3FF6B * ___Textures_20;
	// System.Boolean GLTF.Schema.GLTFRoot::IsGLB
	bool ___IsGLB_21;

public:
	inline static int32_t get_offset_of_ExtensionsUsed_4() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___ExtensionsUsed_4)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_ExtensionsUsed_4() const { return ___ExtensionsUsed_4; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_ExtensionsUsed_4() { return &___ExtensionsUsed_4; }
	inline void set_ExtensionsUsed_4(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___ExtensionsUsed_4 = value;
		Il2CppCodeGenWriteBarrier((&___ExtensionsUsed_4), value);
	}

	inline static int32_t get_offset_of_ExtensionsRequired_5() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___ExtensionsRequired_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_ExtensionsRequired_5() const { return ___ExtensionsRequired_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_ExtensionsRequired_5() { return &___ExtensionsRequired_5; }
	inline void set_ExtensionsRequired_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___ExtensionsRequired_5 = value;
		Il2CppCodeGenWriteBarrier((&___ExtensionsRequired_5), value);
	}

	inline static int32_t get_offset_of_Accessors_6() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Accessors_6)); }
	inline List_1_t1D43B87DCB1AE56CB55C174223A36A1EB6D9C49C * get_Accessors_6() const { return ___Accessors_6; }
	inline List_1_t1D43B87DCB1AE56CB55C174223A36A1EB6D9C49C ** get_address_of_Accessors_6() { return &___Accessors_6; }
	inline void set_Accessors_6(List_1_t1D43B87DCB1AE56CB55C174223A36A1EB6D9C49C * value)
	{
		___Accessors_6 = value;
		Il2CppCodeGenWriteBarrier((&___Accessors_6), value);
	}

	inline static int32_t get_offset_of_Animations_7() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Animations_7)); }
	inline List_1_tD8A55D9A512ACB5ED7DD0B60F6C698FFE3B45E31 * get_Animations_7() const { return ___Animations_7; }
	inline List_1_tD8A55D9A512ACB5ED7DD0B60F6C698FFE3B45E31 ** get_address_of_Animations_7() { return &___Animations_7; }
	inline void set_Animations_7(List_1_tD8A55D9A512ACB5ED7DD0B60F6C698FFE3B45E31 * value)
	{
		___Animations_7 = value;
		Il2CppCodeGenWriteBarrier((&___Animations_7), value);
	}

	inline static int32_t get_offset_of_Asset_8() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Asset_8)); }
	inline Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B * get_Asset_8() const { return ___Asset_8; }
	inline Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B ** get_address_of_Asset_8() { return &___Asset_8; }
	inline void set_Asset_8(Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B * value)
	{
		___Asset_8 = value;
		Il2CppCodeGenWriteBarrier((&___Asset_8), value);
	}

	inline static int32_t get_offset_of_Buffers_9() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Buffers_9)); }
	inline List_1_tF06DC5C3087205AE2F10AF9AC71D1269056533AB * get_Buffers_9() const { return ___Buffers_9; }
	inline List_1_tF06DC5C3087205AE2F10AF9AC71D1269056533AB ** get_address_of_Buffers_9() { return &___Buffers_9; }
	inline void set_Buffers_9(List_1_tF06DC5C3087205AE2F10AF9AC71D1269056533AB * value)
	{
		___Buffers_9 = value;
		Il2CppCodeGenWriteBarrier((&___Buffers_9), value);
	}

	inline static int32_t get_offset_of_BufferViews_10() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___BufferViews_10)); }
	inline List_1_t0B6CA53804AD47B0392209FA3ACF7E438A089103 * get_BufferViews_10() const { return ___BufferViews_10; }
	inline List_1_t0B6CA53804AD47B0392209FA3ACF7E438A089103 ** get_address_of_BufferViews_10() { return &___BufferViews_10; }
	inline void set_BufferViews_10(List_1_t0B6CA53804AD47B0392209FA3ACF7E438A089103 * value)
	{
		___BufferViews_10 = value;
		Il2CppCodeGenWriteBarrier((&___BufferViews_10), value);
	}

	inline static int32_t get_offset_of_Cameras_11() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Cameras_11)); }
	inline List_1_t7428FD3F36775D25A7259C170DB7744093DAB21A * get_Cameras_11() const { return ___Cameras_11; }
	inline List_1_t7428FD3F36775D25A7259C170DB7744093DAB21A ** get_address_of_Cameras_11() { return &___Cameras_11; }
	inline void set_Cameras_11(List_1_t7428FD3F36775D25A7259C170DB7744093DAB21A * value)
	{
		___Cameras_11 = value;
		Il2CppCodeGenWriteBarrier((&___Cameras_11), value);
	}

	inline static int32_t get_offset_of_Images_12() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Images_12)); }
	inline List_1_t2011D662ECF2F170439E8C1761A7CA25340B8D4D * get_Images_12() const { return ___Images_12; }
	inline List_1_t2011D662ECF2F170439E8C1761A7CA25340B8D4D ** get_address_of_Images_12() { return &___Images_12; }
	inline void set_Images_12(List_1_t2011D662ECF2F170439E8C1761A7CA25340B8D4D * value)
	{
		___Images_12 = value;
		Il2CppCodeGenWriteBarrier((&___Images_12), value);
	}

	inline static int32_t get_offset_of_Materials_13() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Materials_13)); }
	inline List_1_t23A108C07696BA8C00032540F033FC43C906DD59 * get_Materials_13() const { return ___Materials_13; }
	inline List_1_t23A108C07696BA8C00032540F033FC43C906DD59 ** get_address_of_Materials_13() { return &___Materials_13; }
	inline void set_Materials_13(List_1_t23A108C07696BA8C00032540F033FC43C906DD59 * value)
	{
		___Materials_13 = value;
		Il2CppCodeGenWriteBarrier((&___Materials_13), value);
	}

	inline static int32_t get_offset_of_Meshes_14() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Meshes_14)); }
	inline List_1_tE1B23A0E5E130978135AD98A31C0681898E3E848 * get_Meshes_14() const { return ___Meshes_14; }
	inline List_1_tE1B23A0E5E130978135AD98A31C0681898E3E848 ** get_address_of_Meshes_14() { return &___Meshes_14; }
	inline void set_Meshes_14(List_1_tE1B23A0E5E130978135AD98A31C0681898E3E848 * value)
	{
		___Meshes_14 = value;
		Il2CppCodeGenWriteBarrier((&___Meshes_14), value);
	}

	inline static int32_t get_offset_of_Nodes_15() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Nodes_15)); }
	inline List_1_tF08BAB889585EE649E734FE29BBA9A3054BD11E7 * get_Nodes_15() const { return ___Nodes_15; }
	inline List_1_tF08BAB889585EE649E734FE29BBA9A3054BD11E7 ** get_address_of_Nodes_15() { return &___Nodes_15; }
	inline void set_Nodes_15(List_1_tF08BAB889585EE649E734FE29BBA9A3054BD11E7 * value)
	{
		___Nodes_15 = value;
		Il2CppCodeGenWriteBarrier((&___Nodes_15), value);
	}

	inline static int32_t get_offset_of_Samplers_16() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Samplers_16)); }
	inline List_1_t5819984D691421754B9A3EB79CDF449D70DE4A01 * get_Samplers_16() const { return ___Samplers_16; }
	inline List_1_t5819984D691421754B9A3EB79CDF449D70DE4A01 ** get_address_of_Samplers_16() { return &___Samplers_16; }
	inline void set_Samplers_16(List_1_t5819984D691421754B9A3EB79CDF449D70DE4A01 * value)
	{
		___Samplers_16 = value;
		Il2CppCodeGenWriteBarrier((&___Samplers_16), value);
	}

	inline static int32_t get_offset_of_Scene_17() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Scene_17)); }
	inline SceneId_t67CF37B99AF8D98A11C03A45BDD51773EE6D605C * get_Scene_17() const { return ___Scene_17; }
	inline SceneId_t67CF37B99AF8D98A11C03A45BDD51773EE6D605C ** get_address_of_Scene_17() { return &___Scene_17; }
	inline void set_Scene_17(SceneId_t67CF37B99AF8D98A11C03A45BDD51773EE6D605C * value)
	{
		___Scene_17 = value;
		Il2CppCodeGenWriteBarrier((&___Scene_17), value);
	}

	inline static int32_t get_offset_of_Scenes_18() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Scenes_18)); }
	inline List_1_tAD6C1545E6E6F1ECD0B394490A03C15DB1DB3155 * get_Scenes_18() const { return ___Scenes_18; }
	inline List_1_tAD6C1545E6E6F1ECD0B394490A03C15DB1DB3155 ** get_address_of_Scenes_18() { return &___Scenes_18; }
	inline void set_Scenes_18(List_1_tAD6C1545E6E6F1ECD0B394490A03C15DB1DB3155 * value)
	{
		___Scenes_18 = value;
		Il2CppCodeGenWriteBarrier((&___Scenes_18), value);
	}

	inline static int32_t get_offset_of_Skins_19() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Skins_19)); }
	inline List_1_tC787C2CEC6D43FC21576E063D82DB77D6A4FCED4 * get_Skins_19() const { return ___Skins_19; }
	inline List_1_tC787C2CEC6D43FC21576E063D82DB77D6A4FCED4 ** get_address_of_Skins_19() { return &___Skins_19; }
	inline void set_Skins_19(List_1_tC787C2CEC6D43FC21576E063D82DB77D6A4FCED4 * value)
	{
		___Skins_19 = value;
		Il2CppCodeGenWriteBarrier((&___Skins_19), value);
	}

	inline static int32_t get_offset_of_Textures_20() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Textures_20)); }
	inline List_1_t672F6CB7A561409CD238343E02BBC74100D3FF6B * get_Textures_20() const { return ___Textures_20; }
	inline List_1_t672F6CB7A561409CD238343E02BBC74100D3FF6B ** get_address_of_Textures_20() { return &___Textures_20; }
	inline void set_Textures_20(List_1_t672F6CB7A561409CD238343E02BBC74100D3FF6B * value)
	{
		___Textures_20 = value;
		Il2CppCodeGenWriteBarrier((&___Textures_20), value);
	}

	inline static int32_t get_offset_of_IsGLB_21() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___IsGLB_21)); }
	inline bool get_IsGLB_21() const { return ___IsGLB_21; }
	inline bool* get_address_of_IsGLB_21() { return &___IsGLB_21; }
	inline void set_IsGLB_21(bool value)
	{
		___IsGLB_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFROOT_T3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B_H
#ifndef IMAGEID_T6B75A8719AF05FF5EF157163A485CAB340DDE68B_H
#define IMAGEID_T6B75A8719AF05FF5EF157163A485CAB340DDE68B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.ImageId
struct  ImageId_t6B75A8719AF05FF5EF157163A485CAB340DDE68B  : public GLTFId_1_t2D2234F7D6DF10992093F7D9A1B129AE76F1D761
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEID_T6B75A8719AF05FF5EF157163A485CAB340DDE68B_H
#ifndef MATERIALID_TD6B61586B1AE066F319C2118D3B9452637A36AF2_H
#define MATERIALID_TD6B61586B1AE066F319C2118D3B9452637A36AF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MaterialId
struct  MaterialId_tD6B61586B1AE066F319C2118D3B9452637A36AF2  : public GLTFId_1_tCD0A6B945F8FBA41667A444A94C374C2CEFD918A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALID_TD6B61586B1AE066F319C2118D3B9452637A36AF2_H
#ifndef MESHID_T435B31924051B8BAA4362AF1AB74F9F50EC6D231_H
#define MESHID_T435B31924051B8BAA4362AF1AB74F9F50EC6D231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MeshId
struct  MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231  : public GLTFId_1_t98733FC08E44C4842A58F514BA48F0C4AED0597A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHID_T435B31924051B8BAA4362AF1AB74F9F50EC6D231_H
#ifndef NODEID_TB863E2705852710AA05166CD9250E50B3A9FB8F0_H
#define NODEID_TB863E2705852710AA05166CD9250E50B3A9FB8F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.NodeId
struct  NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0  : public GLTFId_1_t03A4FA3C937E0B4939DA3B8F26A7FFF95A506783
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODEID_TB863E2705852710AA05166CD9250E50B3A9FB8F0_H
#ifndef NUMERICARRAY_T4941F537DC57A0602218632AABDF228987E030E9_H
#define NUMERICARRAY_T4941F537DC57A0602218632AABDF228987E030E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.NumericArray
struct  NumericArray_t4941F537DC57A0602218632AABDF228987E030E9 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt32[] GLTF.Schema.NumericArray::AsUInts
			UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___AsUInts_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___AsUInts_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single[] GLTF.Schema.NumericArray::AsFloats
			SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___AsFloats_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___AsFloats_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Vector2[] GLTF.Schema.NumericArray::AsVec2s
			Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* ___AsVec2s_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* ___AsVec2s_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Vector3[] GLTF.Schema.NumericArray::AsVec3s
			Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* ___AsVec3s_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* ___AsVec3s_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Vector4[] GLTF.Schema.NumericArray::AsVec4s
			Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* ___AsVec4s_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* ___AsVec4s_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Matrix4x4[] GLTF.Schema.NumericArray::AsMatrix4x4s
			Matrix4x4U5BU5D_tE3A13CB78E1B2CBDCD537180E7C6D83E18B0D9F0* ___AsMatrix4x4s_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			Matrix4x4U5BU5D_tE3A13CB78E1B2CBDCD537180E7C6D83E18B0D9F0* ___AsMatrix4x4s_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Color[] GLTF.Schema.NumericArray::AsColors
			ColorU5BU5D_tAC03762E5A0861EA248597DDB8B645A9C927D92E* ___AsColors_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			ColorU5BU5D_tAC03762E5A0861EA248597DDB8B645A9C927D92E* ___AsColors_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Vector2[] GLTF.Schema.NumericArray::AsTexcoords
			Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* ___AsTexcoords_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* ___AsTexcoords_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Vector3[] GLTF.Schema.NumericArray::AsVertices
			Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* ___AsVertices_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* ___AsVertices_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Vector3[] GLTF.Schema.NumericArray::AsNormals
			Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* ___AsNormals_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* ___AsNormals_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Vector4[] GLTF.Schema.NumericArray::AsTangents
			Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* ___AsTangents_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* ___AsTangents_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt32[] GLTF.Schema.NumericArray::AsTriangles
			UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___AsTriangles_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___AsTriangles_11_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_AsUInts_0() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsUInts_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_AsUInts_0() const { return ___AsUInts_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_AsUInts_0() { return &___AsUInts_0; }
	inline void set_AsUInts_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___AsUInts_0 = value;
		Il2CppCodeGenWriteBarrier((&___AsUInts_0), value);
	}

	inline static int32_t get_offset_of_AsFloats_1() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsFloats_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_AsFloats_1() const { return ___AsFloats_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_AsFloats_1() { return &___AsFloats_1; }
	inline void set_AsFloats_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___AsFloats_1 = value;
		Il2CppCodeGenWriteBarrier((&___AsFloats_1), value);
	}

	inline static int32_t get_offset_of_AsVec2s_2() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsVec2s_2)); }
	inline Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* get_AsVec2s_2() const { return ___AsVec2s_2; }
	inline Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35** get_address_of_AsVec2s_2() { return &___AsVec2s_2; }
	inline void set_AsVec2s_2(Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* value)
	{
		___AsVec2s_2 = value;
		Il2CppCodeGenWriteBarrier((&___AsVec2s_2), value);
	}

	inline static int32_t get_offset_of_AsVec3s_3() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsVec3s_3)); }
	inline Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* get_AsVec3s_3() const { return ___AsVec3s_3; }
	inline Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540** get_address_of_AsVec3s_3() { return &___AsVec3s_3; }
	inline void set_AsVec3s_3(Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* value)
	{
		___AsVec3s_3 = value;
		Il2CppCodeGenWriteBarrier((&___AsVec3s_3), value);
	}

	inline static int32_t get_offset_of_AsVec4s_4() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsVec4s_4)); }
	inline Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* get_AsVec4s_4() const { return ___AsVec4s_4; }
	inline Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033** get_address_of_AsVec4s_4() { return &___AsVec4s_4; }
	inline void set_AsVec4s_4(Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* value)
	{
		___AsVec4s_4 = value;
		Il2CppCodeGenWriteBarrier((&___AsVec4s_4), value);
	}

	inline static int32_t get_offset_of_AsMatrix4x4s_5() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsMatrix4x4s_5)); }
	inline Matrix4x4U5BU5D_tE3A13CB78E1B2CBDCD537180E7C6D83E18B0D9F0* get_AsMatrix4x4s_5() const { return ___AsMatrix4x4s_5; }
	inline Matrix4x4U5BU5D_tE3A13CB78E1B2CBDCD537180E7C6D83E18B0D9F0** get_address_of_AsMatrix4x4s_5() { return &___AsMatrix4x4s_5; }
	inline void set_AsMatrix4x4s_5(Matrix4x4U5BU5D_tE3A13CB78E1B2CBDCD537180E7C6D83E18B0D9F0* value)
	{
		___AsMatrix4x4s_5 = value;
		Il2CppCodeGenWriteBarrier((&___AsMatrix4x4s_5), value);
	}

	inline static int32_t get_offset_of_AsColors_6() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsColors_6)); }
	inline ColorU5BU5D_tAC03762E5A0861EA248597DDB8B645A9C927D92E* get_AsColors_6() const { return ___AsColors_6; }
	inline ColorU5BU5D_tAC03762E5A0861EA248597DDB8B645A9C927D92E** get_address_of_AsColors_6() { return &___AsColors_6; }
	inline void set_AsColors_6(ColorU5BU5D_tAC03762E5A0861EA248597DDB8B645A9C927D92E* value)
	{
		___AsColors_6 = value;
		Il2CppCodeGenWriteBarrier((&___AsColors_6), value);
	}

	inline static int32_t get_offset_of_AsTexcoords_7() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsTexcoords_7)); }
	inline Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* get_AsTexcoords_7() const { return ___AsTexcoords_7; }
	inline Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35** get_address_of_AsTexcoords_7() { return &___AsTexcoords_7; }
	inline void set_AsTexcoords_7(Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* value)
	{
		___AsTexcoords_7 = value;
		Il2CppCodeGenWriteBarrier((&___AsTexcoords_7), value);
	}

	inline static int32_t get_offset_of_AsVertices_8() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsVertices_8)); }
	inline Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* get_AsVertices_8() const { return ___AsVertices_8; }
	inline Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540** get_address_of_AsVertices_8() { return &___AsVertices_8; }
	inline void set_AsVertices_8(Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* value)
	{
		___AsVertices_8 = value;
		Il2CppCodeGenWriteBarrier((&___AsVertices_8), value);
	}

	inline static int32_t get_offset_of_AsNormals_9() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsNormals_9)); }
	inline Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* get_AsNormals_9() const { return ___AsNormals_9; }
	inline Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540** get_address_of_AsNormals_9() { return &___AsNormals_9; }
	inline void set_AsNormals_9(Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* value)
	{
		___AsNormals_9 = value;
		Il2CppCodeGenWriteBarrier((&___AsNormals_9), value);
	}

	inline static int32_t get_offset_of_AsTangents_10() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsTangents_10)); }
	inline Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* get_AsTangents_10() const { return ___AsTangents_10; }
	inline Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033** get_address_of_AsTangents_10() { return &___AsTangents_10; }
	inline void set_AsTangents_10(Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* value)
	{
		___AsTangents_10 = value;
		Il2CppCodeGenWriteBarrier((&___AsTangents_10), value);
	}

	inline static int32_t get_offset_of_AsTriangles_11() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsTriangles_11)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_AsTriangles_11() const { return ___AsTriangles_11; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_AsTriangles_11() { return &___AsTriangles_11; }
	inline void set_AsTriangles_11(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___AsTriangles_11 = value;
		Il2CppCodeGenWriteBarrier((&___AsTriangles_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GLTF.Schema.NumericArray
struct NumericArray_t4941F537DC57A0602218632AABDF228987E030E9_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			uint32_t* ___AsUInts_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t* ___AsUInts_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			float* ___AsFloats_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			float* ___AsFloats_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8 * ___AsVec2s_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8 * ___AsVec2s_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * ___AsVec3s_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * ___AsVec3s_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Vector4_t239657374664B132BBB44122F237F461D91809ED * ___AsVec4s_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector4_t239657374664B132BBB44122F237F461D91809ED * ___AsVec4s_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Matrix4x4U5BU5D_tE3A13CB78E1B2CBDCD537180E7C6D83E18B0D9F0* ___AsMatrix4x4s_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			Matrix4x4U5BU5D_tE3A13CB78E1B2CBDCD537180E7C6D83E18B0D9F0* ___AsMatrix4x4s_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 * ___AsColors_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 * ___AsColors_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8 * ___AsTexcoords_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8 * ___AsTexcoords_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * ___AsVertices_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * ___AsVertices_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * ___AsNormals_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * ___AsNormals_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Vector4_t239657374664B132BBB44122F237F461D91809ED * ___AsTangents_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector4_t239657374664B132BBB44122F237F461D91809ED * ___AsTangents_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint32_t* ___AsTriangles_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t* ___AsTriangles_11_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of GLTF.Schema.NumericArray
struct NumericArray_t4941F537DC57A0602218632AABDF228987E030E9_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			uint32_t* ___AsUInts_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t* ___AsUInts_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			float* ___AsFloats_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			float* ___AsFloats_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8 * ___AsVec2s_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8 * ___AsVec2s_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * ___AsVec3s_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * ___AsVec3s_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Vector4_t239657374664B132BBB44122F237F461D91809ED * ___AsVec4s_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector4_t239657374664B132BBB44122F237F461D91809ED * ___AsVec4s_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Matrix4x4U5BU5D_tE3A13CB78E1B2CBDCD537180E7C6D83E18B0D9F0* ___AsMatrix4x4s_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			Matrix4x4U5BU5D_tE3A13CB78E1B2CBDCD537180E7C6D83E18B0D9F0* ___AsMatrix4x4s_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 * ___AsColors_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 * ___AsColors_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8 * ___AsTexcoords_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8 * ___AsTexcoords_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * ___AsVertices_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * ___AsVertices_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * ___AsNormals_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * ___AsNormals_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Vector4_t239657374664B132BBB44122F237F461D91809ED * ___AsTangents_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector4_t239657374664B132BBB44122F237F461D91809ED * ___AsTangents_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint32_t* ___AsTriangles_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t* ___AsTriangles_11_forAlignmentOnly;
		};
	};
};
#endif // NUMERICARRAY_T4941F537DC57A0602218632AABDF228987E030E9_H
#ifndef SAMPLERID_T72B86914DEF9D622B3D7F8CF2812317F81478EF4_H
#define SAMPLERID_T72B86914DEF9D622B3D7F8CF2812317F81478EF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.SamplerId
struct  SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4  : public GLTFId_1_t76D4B531E6B79E90DC1A4CC5B684341E28AA04C6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLERID_T72B86914DEF9D622B3D7F8CF2812317F81478EF4_H
#ifndef SCENEID_T67CF37B99AF8D98A11C03A45BDD51773EE6D605C_H
#define SCENEID_T67CF37B99AF8D98A11C03A45BDD51773EE6D605C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.SceneId
struct  SceneId_t67CF37B99AF8D98A11C03A45BDD51773EE6D605C  : public GLTFId_1_t4F80A8748E825E378AAB535C5F99CF020CEF74C1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEID_T67CF37B99AF8D98A11C03A45BDD51773EE6D605C_H
#ifndef SKINID_T500172B9076F6784EF4CF0CD5CF6F87516549F02_H
#define SKINID_T500172B9076F6784EF4CF0CD5CF6F87516549F02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.SkinId
struct  SkinId_t500172B9076F6784EF4CF0CD5CF6F87516549F02  : public GLTFId_1_tFEAB99802FD43E2656D9B894A370966029B54F6C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKINID_T500172B9076F6784EF4CF0CD5CF6F87516549F02_H
#ifndef TEXTUREID_TB2D7E38C97266F3A5618E82CF3124A5CA0B78A91_H
#define TEXTUREID_TB2D7E38C97266F3A5618E82CF3124A5CA0B78A91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.TextureId
struct  TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91  : public GLTFId_1_tD8AB7490D46668FF3AB24325C4412B6D597D849B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREID_TB2D7E38C97266F3A5618E82CF3124A5CA0B78A91_H
#ifndef TEXTUREINFO_TFC76DF775412F86374021446E62198EB8AD3AD66_H
#define TEXTUREINFO_TFC76DF775412F86374021446E62198EB8AD3AD66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.TextureInfo
struct  TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Schema.TextureId GLTF.Schema.TextureInfo::Index
	TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91 * ___Index_4;
	// System.Int32 GLTF.Schema.TextureInfo::TexCoord
	int32_t ___TexCoord_5;

public:
	inline static int32_t get_offset_of_Index_4() { return static_cast<int32_t>(offsetof(TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66, ___Index_4)); }
	inline TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91 * get_Index_4() const { return ___Index_4; }
	inline TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91 ** get_address_of_Index_4() { return &___Index_4; }
	inline void set_Index_4(TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91 * value)
	{
		___Index_4 = value;
		Il2CppCodeGenWriteBarrier((&___Index_4), value);
	}

	inline static int32_t get_offset_of_TexCoord_5() { return static_cast<int32_t>(offsetof(TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66, ___TexCoord_5)); }
	inline int32_t get_TexCoord_5() const { return ___TexCoord_5; }
	inline int32_t* get_address_of_TexCoord_5() { return &___TexCoord_5; }
	inline void set_TexCoord_5(int32_t value)
	{
		___TexCoord_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREINFO_TFC76DF775412F86374021446E62198EB8AD3AD66_H
#ifndef FUNCTIONINSTANCE_TE38D99DCDA862B7930D886A656437288185E5458_H
#define FUNCTIONINSTANCE_TE38D99DCDA862B7930D886A656437288185E5458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Function.FunctionInstance
struct  FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:
	// Jint.Engine Jint.Native.Function.FunctionInstance::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_4;
	// Jint.Runtime.Environments.LexicalEnvironment Jint.Native.Function.FunctionInstance::<Scope>k__BackingField
	LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * ___U3CScopeU3Ek__BackingField_5;
	// System.String[] Jint.Native.Function.FunctionInstance::<FormalParameters>k__BackingField
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___U3CFormalParametersU3Ek__BackingField_6;
	// System.Boolean Jint.Native.Function.FunctionInstance::<Strict>k__BackingField
	bool ___U3CStrictU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of__engine_4() { return static_cast<int32_t>(offsetof(FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458, ____engine_4)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_4() const { return ____engine_4; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_4() { return &____engine_4; }
	inline void set__engine_4(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_4 = value;
		Il2CppCodeGenWriteBarrier((&____engine_4), value);
	}

	inline static int32_t get_offset_of_U3CScopeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458, ___U3CScopeU3Ek__BackingField_5)); }
	inline LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * get_U3CScopeU3Ek__BackingField_5() const { return ___U3CScopeU3Ek__BackingField_5; }
	inline LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 ** get_address_of_U3CScopeU3Ek__BackingField_5() { return &___U3CScopeU3Ek__BackingField_5; }
	inline void set_U3CScopeU3Ek__BackingField_5(LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * value)
	{
		___U3CScopeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScopeU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CFormalParametersU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458, ___U3CFormalParametersU3Ek__BackingField_6)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_U3CFormalParametersU3Ek__BackingField_6() const { return ___U3CFormalParametersU3Ek__BackingField_6; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_U3CFormalParametersU3Ek__BackingField_6() { return &___U3CFormalParametersU3Ek__BackingField_6; }
	inline void set_U3CFormalParametersU3Ek__BackingField_6(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___U3CFormalParametersU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormalParametersU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CStrictU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458, ___U3CStrictU3Ek__BackingField_7)); }
	inline bool get_U3CStrictU3Ek__BackingField_7() const { return ___U3CStrictU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CStrictU3Ek__BackingField_7() { return &___U3CStrictU3Ek__BackingField_7; }
	inline void set_U3CStrictU3Ek__BackingField_7(bool value)
	{
		___U3CStrictU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONINSTANCE_TE38D99DCDA862B7930D886A656437288185E5458_H
#ifndef JAVASCRIPTEXCEPTION_TECA7359FC824B4EC27229DB2AEE05BB8C1488990_H
#define JAVASCRIPTEXCEPTION_TECA7359FC824B4EC27229DB2AEE05BB8C1488990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.JavaScriptException
struct  JavaScriptException_tECA7359FC824B4EC27229DB2AEE05BB8C1488990  : public Exception_t
{
public:
	// Jint.Native.JsValue Jint.Runtime.JavaScriptException::_errorObject
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ____errorObject_17;
	// System.String Jint.Runtime.JavaScriptException::_callStack
	String_t* ____callStack_18;
	// Jint.Parser.Location Jint.Runtime.JavaScriptException::<Location>k__BackingField
	Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * ___U3CLocationU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of__errorObject_17() { return static_cast<int32_t>(offsetof(JavaScriptException_tECA7359FC824B4EC27229DB2AEE05BB8C1488990, ____errorObject_17)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get__errorObject_17() const { return ____errorObject_17; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of__errorObject_17() { return &____errorObject_17; }
	inline void set__errorObject_17(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		____errorObject_17 = value;
		Il2CppCodeGenWriteBarrier((&____errorObject_17), value);
	}

	inline static int32_t get_offset_of__callStack_18() { return static_cast<int32_t>(offsetof(JavaScriptException_tECA7359FC824B4EC27229DB2AEE05BB8C1488990, ____callStack_18)); }
	inline String_t* get__callStack_18() const { return ____callStack_18; }
	inline String_t** get_address_of__callStack_18() { return &____callStack_18; }
	inline void set__callStack_18(String_t* value)
	{
		____callStack_18 = value;
		Il2CppCodeGenWriteBarrier((&____callStack_18), value);
	}

	inline static int32_t get_offset_of_U3CLocationU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JavaScriptException_tECA7359FC824B4EC27229DB2AEE05BB8C1488990, ___U3CLocationU3Ek__BackingField_19)); }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * get_U3CLocationU3Ek__BackingField_19() const { return ___U3CLocationU3Ek__BackingField_19; }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 ** get_address_of_U3CLocationU3Ek__BackingField_19() { return &___U3CLocationU3Ek__BackingField_19; }
	inline void set_U3CLocationU3Ek__BackingField_19(Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * value)
	{
		___U3CLocationU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLocationU3Ek__BackingField_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JAVASCRIPTEXCEPTION_TECA7359FC824B4EC27229DB2AEE05BB8C1488990_H
#ifndef RECURSIONDEPTHOVERFLOWEXCEPTION_T4469F4DD10F8CD0B9097762F4BCF7817C36EEAA1_H
#define RECURSIONDEPTHOVERFLOWEXCEPTION_T4469F4DD10F8CD0B9097762F4BCF7817C36EEAA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.RecursionDepthOverflowException
struct  RecursionDepthOverflowException_t4469F4DD10F8CD0B9097762F4BCF7817C36EEAA1  : public Exception_t
{
public:
	// System.String Jint.Runtime.RecursionDepthOverflowException::<CallChain>k__BackingField
	String_t* ___U3CCallChainU3Ek__BackingField_17;
	// System.String Jint.Runtime.RecursionDepthOverflowException::<CallExpressionReference>k__BackingField
	String_t* ___U3CCallExpressionReferenceU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_U3CCallChainU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(RecursionDepthOverflowException_t4469F4DD10F8CD0B9097762F4BCF7817C36EEAA1, ___U3CCallChainU3Ek__BackingField_17)); }
	inline String_t* get_U3CCallChainU3Ek__BackingField_17() const { return ___U3CCallChainU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CCallChainU3Ek__BackingField_17() { return &___U3CCallChainU3Ek__BackingField_17; }
	inline void set_U3CCallChainU3Ek__BackingField_17(String_t* value)
	{
		___U3CCallChainU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallChainU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CCallExpressionReferenceU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(RecursionDepthOverflowException_t4469F4DD10F8CD0B9097762F4BCF7817C36EEAA1, ___U3CCallExpressionReferenceU3Ek__BackingField_18)); }
	inline String_t* get_U3CCallExpressionReferenceU3Ek__BackingField_18() const { return ___U3CCallExpressionReferenceU3Ek__BackingField_18; }
	inline String_t** get_address_of_U3CCallExpressionReferenceU3Ek__BackingField_18() { return &___U3CCallExpressionReferenceU3Ek__BackingField_18; }
	inline void set_U3CCallExpressionReferenceU3Ek__BackingField_18(String_t* value)
	{
		___U3CCallExpressionReferenceU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallExpressionReferenceU3Ek__BackingField_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECURSIONDEPTHOVERFLOWEXCEPTION_T4469F4DD10F8CD0B9097762F4BCF7817C36EEAA1_H
#ifndef STATEMENTSCOUNTOVERFLOWEXCEPTION_TB6AB2AAE8C222089DA11059B50CCEBF16760849D_H
#define STATEMENTSCOUNTOVERFLOWEXCEPTION_TB6AB2AAE8C222089DA11059B50CCEBF16760849D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.StatementsCountOverflowException
struct  StatementsCountOverflowException_tB6AB2AAE8C222089DA11059B50CCEBF16760849D  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMENTSCOUNTOVERFLOWEXCEPTION_TB6AB2AAE8C222089DA11059B50CCEBF16760849D_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ALPHAMODE_T41015BEC6E2BB18C0A2688E6CF441F148BA768E6_H
#define ALPHAMODE_T41015BEC6E2BB18C0A2688E6CF441F148BA768E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AlphaMode
struct  AlphaMode_t41015BEC6E2BB18C0A2688E6CF441F148BA768E6 
{
public:
	// System.Int32 GLTF.Schema.AlphaMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AlphaMode_t41015BEC6E2BB18C0A2688E6CF441F148BA768E6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALPHAMODE_T41015BEC6E2BB18C0A2688E6CF441F148BA768E6_H
#ifndef BUFFERVIEWTARGET_T56AAAFD9880E8FE1098EE7B4F2F7DC54893FB87E_H
#define BUFFERVIEWTARGET_T56AAAFD9880E8FE1098EE7B4F2F7DC54893FB87E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.BufferViewTarget
struct  BufferViewTarget_t56AAAFD9880E8FE1098EE7B4F2F7DC54893FB87E 
{
public:
	// System.Int32 GLTF.Schema.BufferViewTarget::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BufferViewTarget_t56AAAFD9880E8FE1098EE7B4F2F7DC54893FB87E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERVIEWTARGET_T56AAAFD9880E8FE1098EE7B4F2F7DC54893FB87E_H
#ifndef CAMERATYPE_T1A6E126E159E7471427ACB5C621161AC95024DDE_H
#define CAMERATYPE_T1A6E126E159E7471427ACB5C621161AC95024DDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.CameraType
struct  CameraType_t1A6E126E159E7471427ACB5C621161AC95024DDE 
{
public:
	// System.Int32 GLTF.Schema.CameraType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraType_t1A6E126E159E7471427ACB5C621161AC95024DDE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERATYPE_T1A6E126E159E7471427ACB5C621161AC95024DDE_H
#ifndef DRAWMODE_T2A826FD4C32BB9350CE2130C2DDC5AF6D44A430B_H
#define DRAWMODE_T2A826FD4C32BB9350CE2130C2DDC5AF6D44A430B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.DrawMode
struct  DrawMode_t2A826FD4C32BB9350CE2130C2DDC5AF6D44A430B 
{
public:
	// System.Int32 GLTF.Schema.DrawMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DrawMode_t2A826FD4C32BB9350CE2130C2DDC5AF6D44A430B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWMODE_T2A826FD4C32BB9350CE2130C2DDC5AF6D44A430B_H
#ifndef GLTFACCESSORATTRIBUTETYPE_T733A462A97AE55D3A765746932C23C9AF0A9A67E_H
#define GLTFACCESSORATTRIBUTETYPE_T733A462A97AE55D3A765746932C23C9AF0A9A67E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFAccessorAttributeType
struct  GLTFAccessorAttributeType_t733A462A97AE55D3A765746932C23C9AF0A9A67E 
{
public:
	// System.Int32 GLTF.Schema.GLTFAccessorAttributeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GLTFAccessorAttributeType_t733A462A97AE55D3A765746932C23C9AF0A9A67E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFACCESSORATTRIBUTETYPE_T733A462A97AE55D3A765746932C23C9AF0A9A67E_H
#ifndef GLTFANIMATION_TF5162217FBA9DA23C125D87C6335764CED3B84D0_H
#define GLTFANIMATION_TF5162217FBA9DA23C125D87C6335764CED3B84D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFAnimation
struct  GLTFAnimation_tF5162217FBA9DA23C125D87C6335764CED3B84D0  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// System.Collections.Generic.List`1<GLTF.Schema.AnimationChannel> GLTF.Schema.GLTFAnimation::Channels
	List_1_t9B68BE4E3D434E796C16C60C3F8E4AA066AE4FDB * ___Channels_5;
	// System.Collections.Generic.List`1<GLTF.Schema.AnimationSampler> GLTF.Schema.GLTFAnimation::Samplers
	List_1_t8A428600CE711C507A4972EBBEF6D04DEAA8FB65 * ___Samplers_6;

public:
	inline static int32_t get_offset_of_Channels_5() { return static_cast<int32_t>(offsetof(GLTFAnimation_tF5162217FBA9DA23C125D87C6335764CED3B84D0, ___Channels_5)); }
	inline List_1_t9B68BE4E3D434E796C16C60C3F8E4AA066AE4FDB * get_Channels_5() const { return ___Channels_5; }
	inline List_1_t9B68BE4E3D434E796C16C60C3F8E4AA066AE4FDB ** get_address_of_Channels_5() { return &___Channels_5; }
	inline void set_Channels_5(List_1_t9B68BE4E3D434E796C16C60C3F8E4AA066AE4FDB * value)
	{
		___Channels_5 = value;
		Il2CppCodeGenWriteBarrier((&___Channels_5), value);
	}

	inline static int32_t get_offset_of_Samplers_6() { return static_cast<int32_t>(offsetof(GLTFAnimation_tF5162217FBA9DA23C125D87C6335764CED3B84D0, ___Samplers_6)); }
	inline List_1_t8A428600CE711C507A4972EBBEF6D04DEAA8FB65 * get_Samplers_6() const { return ___Samplers_6; }
	inline List_1_t8A428600CE711C507A4972EBBEF6D04DEAA8FB65 ** get_address_of_Samplers_6() { return &___Samplers_6; }
	inline void set_Samplers_6(List_1_t8A428600CE711C507A4972EBBEF6D04DEAA8FB65 * value)
	{
		___Samplers_6 = value;
		Il2CppCodeGenWriteBarrier((&___Samplers_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFANIMATION_TF5162217FBA9DA23C125D87C6335764CED3B84D0_H
#ifndef GLTFANIMATIONCHANNELPATH_T6C68975EA59C1B193742B9FBF09EF26192171FE8_H
#define GLTFANIMATIONCHANNELPATH_T6C68975EA59C1B193742B9FBF09EF26192171FE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFAnimationChannelPath
struct  GLTFAnimationChannelPath_t6C68975EA59C1B193742B9FBF09EF26192171FE8 
{
public:
	// System.Int32 GLTF.Schema.GLTFAnimationChannelPath::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GLTFAnimationChannelPath_t6C68975EA59C1B193742B9FBF09EF26192171FE8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFANIMATIONCHANNELPATH_T6C68975EA59C1B193742B9FBF09EF26192171FE8_H
#ifndef GLTFBUFFER_T093686A8CEC027B3EFCCCC89EF874F3BE6738CA1_H
#define GLTFBUFFER_T093686A8CEC027B3EFCCCC89EF874F3BE6738CA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFBuffer
struct  GLTFBuffer_t093686A8CEC027B3EFCCCC89EF874F3BE6738CA1  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// System.String GLTF.Schema.GLTFBuffer::Uri
	String_t* ___Uri_5;
	// System.UInt32 GLTF.Schema.GLTFBuffer::ByteLength
	uint32_t ___ByteLength_6;

public:
	inline static int32_t get_offset_of_Uri_5() { return static_cast<int32_t>(offsetof(GLTFBuffer_t093686A8CEC027B3EFCCCC89EF874F3BE6738CA1, ___Uri_5)); }
	inline String_t* get_Uri_5() const { return ___Uri_5; }
	inline String_t** get_address_of_Uri_5() { return &___Uri_5; }
	inline void set_Uri_5(String_t* value)
	{
		___Uri_5 = value;
		Il2CppCodeGenWriteBarrier((&___Uri_5), value);
	}

	inline static int32_t get_offset_of_ByteLength_6() { return static_cast<int32_t>(offsetof(GLTFBuffer_t093686A8CEC027B3EFCCCC89EF874F3BE6738CA1, ___ByteLength_6)); }
	inline uint32_t get_ByteLength_6() const { return ___ByteLength_6; }
	inline uint32_t* get_address_of_ByteLength_6() { return &___ByteLength_6; }
	inline void set_ByteLength_6(uint32_t value)
	{
		___ByteLength_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFBUFFER_T093686A8CEC027B3EFCCCC89EF874F3BE6738CA1_H
#ifndef GLTFCOMPONENTTYPE_T2DB090AB0A0D5173E990FA29FE3B7C7C710F68E4_H
#define GLTFCOMPONENTTYPE_T2DB090AB0A0D5173E990FA29FE3B7C7C710F68E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFComponentType
struct  GLTFComponentType_t2DB090AB0A0D5173E990FA29FE3B7C7C710F68E4 
{
public:
	// System.Int32 GLTF.Schema.GLTFComponentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GLTFComponentType_t2DB090AB0A0D5173E990FA29FE3B7C7C710F68E4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFCOMPONENTTYPE_T2DB090AB0A0D5173E990FA29FE3B7C7C710F68E4_H
#ifndef GLTFIMAGE_TA3F1517270CE1DC26739C076F757BFA7B651A4EE_H
#define GLTFIMAGE_TA3F1517270CE1DC26739C076F757BFA7B651A4EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFImage
struct  GLTFImage_tA3F1517270CE1DC26739C076F757BFA7B651A4EE  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// System.String GLTF.Schema.GLTFImage::Uri
	String_t* ___Uri_5;
	// System.String GLTF.Schema.GLTFImage::MimeType
	String_t* ___MimeType_6;
	// GLTF.Schema.BufferViewId GLTF.Schema.GLTFImage::BufferView
	BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * ___BufferView_7;

public:
	inline static int32_t get_offset_of_Uri_5() { return static_cast<int32_t>(offsetof(GLTFImage_tA3F1517270CE1DC26739C076F757BFA7B651A4EE, ___Uri_5)); }
	inline String_t* get_Uri_5() const { return ___Uri_5; }
	inline String_t** get_address_of_Uri_5() { return &___Uri_5; }
	inline void set_Uri_5(String_t* value)
	{
		___Uri_5 = value;
		Il2CppCodeGenWriteBarrier((&___Uri_5), value);
	}

	inline static int32_t get_offset_of_MimeType_6() { return static_cast<int32_t>(offsetof(GLTFImage_tA3F1517270CE1DC26739C076F757BFA7B651A4EE, ___MimeType_6)); }
	inline String_t* get_MimeType_6() const { return ___MimeType_6; }
	inline String_t** get_address_of_MimeType_6() { return &___MimeType_6; }
	inline void set_MimeType_6(String_t* value)
	{
		___MimeType_6 = value;
		Il2CppCodeGenWriteBarrier((&___MimeType_6), value);
	}

	inline static int32_t get_offset_of_BufferView_7() { return static_cast<int32_t>(offsetof(GLTFImage_tA3F1517270CE1DC26739C076F757BFA7B651A4EE, ___BufferView_7)); }
	inline BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * get_BufferView_7() const { return ___BufferView_7; }
	inline BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 ** get_address_of_BufferView_7() { return &___BufferView_7; }
	inline void set_BufferView_7(BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * value)
	{
		___BufferView_7 = value;
		Il2CppCodeGenWriteBarrier((&___BufferView_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFIMAGE_TA3F1517270CE1DC26739C076F757BFA7B651A4EE_H
#ifndef GLTFMESH_TD3F424D9F92A52F43406967EA5B985F2735DD1AD_H
#define GLTFMESH_TD3F424D9F92A52F43406967EA5B985F2735DD1AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFMesh
struct  GLTFMesh_tD3F424D9F92A52F43406967EA5B985F2735DD1AD  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// System.Collections.Generic.List`1<GLTF.Schema.MeshPrimitive> GLTF.Schema.GLTFMesh::Primitives
	List_1_t9FEC43E6B0207AA9A23659F024A1BE48ABC67A31 * ___Primitives_5;
	// System.Collections.Generic.List`1<System.Double> GLTF.Schema.GLTFMesh::Weights
	List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * ___Weights_6;

public:
	inline static int32_t get_offset_of_Primitives_5() { return static_cast<int32_t>(offsetof(GLTFMesh_tD3F424D9F92A52F43406967EA5B985F2735DD1AD, ___Primitives_5)); }
	inline List_1_t9FEC43E6B0207AA9A23659F024A1BE48ABC67A31 * get_Primitives_5() const { return ___Primitives_5; }
	inline List_1_t9FEC43E6B0207AA9A23659F024A1BE48ABC67A31 ** get_address_of_Primitives_5() { return &___Primitives_5; }
	inline void set_Primitives_5(List_1_t9FEC43E6B0207AA9A23659F024A1BE48ABC67A31 * value)
	{
		___Primitives_5 = value;
		Il2CppCodeGenWriteBarrier((&___Primitives_5), value);
	}

	inline static int32_t get_offset_of_Weights_6() { return static_cast<int32_t>(offsetof(GLTFMesh_tD3F424D9F92A52F43406967EA5B985F2735DD1AD, ___Weights_6)); }
	inline List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * get_Weights_6() const { return ___Weights_6; }
	inline List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E ** get_address_of_Weights_6() { return &___Weights_6; }
	inline void set_Weights_6(List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * value)
	{
		___Weights_6 = value;
		Il2CppCodeGenWriteBarrier((&___Weights_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFMESH_TD3F424D9F92A52F43406967EA5B985F2735DD1AD_H
#ifndef GLTFSCENE_T668D116E319247178F4641B2F3A4DF33C6343BB6_H
#define GLTFSCENE_T668D116E319247178F4641B2F3A4DF33C6343BB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFScene
struct  GLTFScene_t668D116E319247178F4641B2F3A4DF33C6343BB6  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// System.Collections.Generic.List`1<GLTF.Schema.NodeId> GLTF.Schema.GLTFScene::Nodes
	List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * ___Nodes_5;

public:
	inline static int32_t get_offset_of_Nodes_5() { return static_cast<int32_t>(offsetof(GLTFScene_t668D116E319247178F4641B2F3A4DF33C6343BB6, ___Nodes_5)); }
	inline List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * get_Nodes_5() const { return ___Nodes_5; }
	inline List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA ** get_address_of_Nodes_5() { return &___Nodes_5; }
	inline void set_Nodes_5(List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * value)
	{
		___Nodes_5 = value;
		Il2CppCodeGenWriteBarrier((&___Nodes_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFSCENE_T668D116E319247178F4641B2F3A4DF33C6343BB6_H
#ifndef GLTFTEXTURE_T2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65_H
#define GLTFTEXTURE_T2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFTexture
struct  GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// GLTF.Schema.SamplerId GLTF.Schema.GLTFTexture::Sampler
	SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 * ___Sampler_5;
	// GLTF.Schema.ImageId GLTF.Schema.GLTFTexture::Source
	ImageId_t6B75A8719AF05FF5EF157163A485CAB340DDE68B * ___Source_6;

public:
	inline static int32_t get_offset_of_Sampler_5() { return static_cast<int32_t>(offsetof(GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65, ___Sampler_5)); }
	inline SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 * get_Sampler_5() const { return ___Sampler_5; }
	inline SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 ** get_address_of_Sampler_5() { return &___Sampler_5; }
	inline void set_Sampler_5(SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 * value)
	{
		___Sampler_5 = value;
		Il2CppCodeGenWriteBarrier((&___Sampler_5), value);
	}

	inline static int32_t get_offset_of_Source_6() { return static_cast<int32_t>(offsetof(GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65, ___Source_6)); }
	inline ImageId_t6B75A8719AF05FF5EF157163A485CAB340DDE68B * get_Source_6() const { return ___Source_6; }
	inline ImageId_t6B75A8719AF05FF5EF157163A485CAB340DDE68B ** get_address_of_Source_6() { return &___Source_6; }
	inline void set_Source_6(ImageId_t6B75A8719AF05FF5EF157163A485CAB340DDE68B * value)
	{
		___Source_6 = value;
		Il2CppCodeGenWriteBarrier((&___Source_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFTEXTURE_T2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65_H
#ifndef INTERPOLATIONTYPE_T31C5CDC2D3A825B4A0287213B304384CC34CEB28_H
#define INTERPOLATIONTYPE_T31C5CDC2D3A825B4A0287213B304384CC34CEB28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.InterpolationType
struct  InterpolationType_t31C5CDC2D3A825B4A0287213B304384CC34CEB28 
{
public:
	// System.Int32 GLTF.Schema.InterpolationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InterpolationType_t31C5CDC2D3A825B4A0287213B304384CC34CEB28, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATIONTYPE_T31C5CDC2D3A825B4A0287213B304384CC34CEB28_H
#ifndef MAGFILTERMODE_T41718D0BF404CAE8E6F2DAB45747B04C74EB8F5D_H
#define MAGFILTERMODE_T41718D0BF404CAE8E6F2DAB45747B04C74EB8F5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MagFilterMode
struct  MagFilterMode_t41718D0BF404CAE8E6F2DAB45747B04C74EB8F5D 
{
public:
	// System.Int32 GLTF.Schema.MagFilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MagFilterMode_t41718D0BF404CAE8E6F2DAB45747B04C74EB8F5D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAGFILTERMODE_T41718D0BF404CAE8E6F2DAB45747B04C74EB8F5D_H
#ifndef MATERIALCOMMONCONSTANT_T97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6_H
#define MATERIALCOMMONCONSTANT_T97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MaterialCommonConstant
struct  MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Math.Color GLTF.Schema.MaterialCommonConstant::AmbientFactor
	Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  ___AmbientFactor_4;
	// GLTF.Schema.TextureInfo GLTF.Schema.MaterialCommonConstant::LightmapTexture
	TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * ___LightmapTexture_5;
	// GLTF.Math.Color GLTF.Schema.MaterialCommonConstant::LightmapFactor
	Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  ___LightmapFactor_6;

public:
	inline static int32_t get_offset_of_AmbientFactor_4() { return static_cast<int32_t>(offsetof(MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6, ___AmbientFactor_4)); }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  get_AmbientFactor_4() const { return ___AmbientFactor_4; }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 * get_address_of_AmbientFactor_4() { return &___AmbientFactor_4; }
	inline void set_AmbientFactor_4(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  value)
	{
		___AmbientFactor_4 = value;
	}

	inline static int32_t get_offset_of_LightmapTexture_5() { return static_cast<int32_t>(offsetof(MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6, ___LightmapTexture_5)); }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * get_LightmapTexture_5() const { return ___LightmapTexture_5; }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 ** get_address_of_LightmapTexture_5() { return &___LightmapTexture_5; }
	inline void set_LightmapTexture_5(TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * value)
	{
		___LightmapTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___LightmapTexture_5), value);
	}

	inline static int32_t get_offset_of_LightmapFactor_6() { return static_cast<int32_t>(offsetof(MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6, ___LightmapFactor_6)); }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  get_LightmapFactor_6() const { return ___LightmapFactor_6; }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 * get_address_of_LightmapFactor_6() { return &___LightmapFactor_6; }
	inline void set_LightmapFactor_6(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  value)
	{
		___LightmapFactor_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALCOMMONCONSTANT_T97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6_H
#ifndef MINFILTERMODE_T9C07AF4837EC02E1903D6631D2ED23FB38DC778B_H
#define MINFILTERMODE_T9C07AF4837EC02E1903D6631D2ED23FB38DC778B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MinFilterMode
struct  MinFilterMode_t9C07AF4837EC02E1903D6631D2ED23FB38DC778B 
{
public:
	// System.Int32 GLTF.Schema.MinFilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MinFilterMode_t9C07AF4837EC02E1903D6631D2ED23FB38DC778B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINFILTERMODE_T9C07AF4837EC02E1903D6631D2ED23FB38DC778B_H
#ifndef NODE_TA2E234FC7393139A18C5F5641EE372B7F2DFB744_H
#define NODE_TA2E234FC7393139A18C5F5641EE372B7F2DFB744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Node
struct  Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// System.Boolean GLTF.Schema.Node::UseTRS
	bool ___UseTRS_5;
	// GLTF.Schema.CameraId GLTF.Schema.Node::Camera
	CameraId_t473F137B518358316FD6D1100DFF1D190E849BAC * ___Camera_6;
	// System.Collections.Generic.List`1<GLTF.Schema.NodeId> GLTF.Schema.Node::Children
	List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * ___Children_7;
	// GLTF.Schema.SkinId GLTF.Schema.Node::Skin
	SkinId_t500172B9076F6784EF4CF0CD5CF6F87516549F02 * ___Skin_8;
	// GLTF.Math.Matrix4x4 GLTF.Schema.Node::Matrix
	Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9 * ___Matrix_9;
	// GLTF.Schema.MeshId GLTF.Schema.Node::Mesh
	MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231 * ___Mesh_10;
	// GLTF.Math.Quaternion GLTF.Schema.Node::Rotation
	Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8  ___Rotation_11;
	// GLTF.Math.Vector3 GLTF.Schema.Node::Scale
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___Scale_12;
	// GLTF.Math.Vector3 GLTF.Schema.Node::Translation
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___Translation_13;
	// System.Collections.Generic.List`1<System.Double> GLTF.Schema.Node::Weights
	List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * ___Weights_14;

public:
	inline static int32_t get_offset_of_UseTRS_5() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___UseTRS_5)); }
	inline bool get_UseTRS_5() const { return ___UseTRS_5; }
	inline bool* get_address_of_UseTRS_5() { return &___UseTRS_5; }
	inline void set_UseTRS_5(bool value)
	{
		___UseTRS_5 = value;
	}

	inline static int32_t get_offset_of_Camera_6() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Camera_6)); }
	inline CameraId_t473F137B518358316FD6D1100DFF1D190E849BAC * get_Camera_6() const { return ___Camera_6; }
	inline CameraId_t473F137B518358316FD6D1100DFF1D190E849BAC ** get_address_of_Camera_6() { return &___Camera_6; }
	inline void set_Camera_6(CameraId_t473F137B518358316FD6D1100DFF1D190E849BAC * value)
	{
		___Camera_6 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_6), value);
	}

	inline static int32_t get_offset_of_Children_7() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Children_7)); }
	inline List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * get_Children_7() const { return ___Children_7; }
	inline List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA ** get_address_of_Children_7() { return &___Children_7; }
	inline void set_Children_7(List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * value)
	{
		___Children_7 = value;
		Il2CppCodeGenWriteBarrier((&___Children_7), value);
	}

	inline static int32_t get_offset_of_Skin_8() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Skin_8)); }
	inline SkinId_t500172B9076F6784EF4CF0CD5CF6F87516549F02 * get_Skin_8() const { return ___Skin_8; }
	inline SkinId_t500172B9076F6784EF4CF0CD5CF6F87516549F02 ** get_address_of_Skin_8() { return &___Skin_8; }
	inline void set_Skin_8(SkinId_t500172B9076F6784EF4CF0CD5CF6F87516549F02 * value)
	{
		___Skin_8 = value;
		Il2CppCodeGenWriteBarrier((&___Skin_8), value);
	}

	inline static int32_t get_offset_of_Matrix_9() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Matrix_9)); }
	inline Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9 * get_Matrix_9() const { return ___Matrix_9; }
	inline Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9 ** get_address_of_Matrix_9() { return &___Matrix_9; }
	inline void set_Matrix_9(Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9 * value)
	{
		___Matrix_9 = value;
		Il2CppCodeGenWriteBarrier((&___Matrix_9), value);
	}

	inline static int32_t get_offset_of_Mesh_10() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Mesh_10)); }
	inline MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231 * get_Mesh_10() const { return ___Mesh_10; }
	inline MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231 ** get_address_of_Mesh_10() { return &___Mesh_10; }
	inline void set_Mesh_10(MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231 * value)
	{
		___Mesh_10 = value;
		Il2CppCodeGenWriteBarrier((&___Mesh_10), value);
	}

	inline static int32_t get_offset_of_Rotation_11() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Rotation_11)); }
	inline Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8  get_Rotation_11() const { return ___Rotation_11; }
	inline Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8 * get_address_of_Rotation_11() { return &___Rotation_11; }
	inline void set_Rotation_11(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8  value)
	{
		___Rotation_11 = value;
	}

	inline static int32_t get_offset_of_Scale_12() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Scale_12)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_Scale_12() const { return ___Scale_12; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_Scale_12() { return &___Scale_12; }
	inline void set_Scale_12(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___Scale_12 = value;
	}

	inline static int32_t get_offset_of_Translation_13() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Translation_13)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_Translation_13() const { return ___Translation_13; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_Translation_13() { return &___Translation_13; }
	inline void set_Translation_13(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___Translation_13 = value;
	}

	inline static int32_t get_offset_of_Weights_14() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Weights_14)); }
	inline List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * get_Weights_14() const { return ___Weights_14; }
	inline List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E ** get_address_of_Weights_14() { return &___Weights_14; }
	inline void set_Weights_14(List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * value)
	{
		___Weights_14 = value;
		Il2CppCodeGenWriteBarrier((&___Weights_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODE_TA2E234FC7393139A18C5F5641EE372B7F2DFB744_H
#ifndef NORMALTEXTUREINFO_T71DC34684B42EC86879B251FCFEF90183EE1620E_H
#define NORMALTEXTUREINFO_T71DC34684B42EC86879B251FCFEF90183EE1620E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.NormalTextureInfo
struct  NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E  : public TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66
{
public:
	// System.Double GLTF.Schema.NormalTextureInfo::Scale
	double ___Scale_6;

public:
	inline static int32_t get_offset_of_Scale_6() { return static_cast<int32_t>(offsetof(NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E, ___Scale_6)); }
	inline double get_Scale_6() const { return ___Scale_6; }
	inline double* get_address_of_Scale_6() { return &___Scale_6; }
	inline void set_Scale_6(double value)
	{
		___Scale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NORMALTEXTUREINFO_T71DC34684B42EC86879B251FCFEF90183EE1620E_H
#ifndef OCCLUSIONTEXTUREINFO_T4D507E91BFD382D6F653F7FA6C40ABE36279F999_H
#define OCCLUSIONTEXTUREINFO_T4D507E91BFD382D6F653F7FA6C40ABE36279F999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.OcclusionTextureInfo
struct  OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999  : public TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66
{
public:
	// System.Double GLTF.Schema.OcclusionTextureInfo::Strength
	double ___Strength_6;

public:
	inline static int32_t get_offset_of_Strength_6() { return static_cast<int32_t>(offsetof(OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999, ___Strength_6)); }
	inline double get_Strength_6() const { return ___Strength_6; }
	inline double* get_address_of_Strength_6() { return &___Strength_6; }
	inline void set_Strength_6(double value)
	{
		___Strength_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCCLUSIONTEXTUREINFO_T4D507E91BFD382D6F653F7FA6C40ABE36279F999_H
#ifndef PBRMETALLICROUGHNESS_TBD92F38F67817774635FD764F2D23BCE346B27A2_H
#define PBRMETALLICROUGHNESS_TBD92F38F67817774635FD764F2D23BCE346B27A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.PbrMetallicRoughness
struct  PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Math.Color GLTF.Schema.PbrMetallicRoughness::BaseColorFactor
	Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  ___BaseColorFactor_4;
	// GLTF.Schema.TextureInfo GLTF.Schema.PbrMetallicRoughness::BaseColorTexture
	TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * ___BaseColorTexture_5;
	// System.Double GLTF.Schema.PbrMetallicRoughness::MetallicFactor
	double ___MetallicFactor_6;
	// System.Double GLTF.Schema.PbrMetallicRoughness::RoughnessFactor
	double ___RoughnessFactor_7;
	// GLTF.Schema.TextureInfo GLTF.Schema.PbrMetallicRoughness::MetallicRoughnessTexture
	TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * ___MetallicRoughnessTexture_8;

public:
	inline static int32_t get_offset_of_BaseColorFactor_4() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2, ___BaseColorFactor_4)); }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  get_BaseColorFactor_4() const { return ___BaseColorFactor_4; }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 * get_address_of_BaseColorFactor_4() { return &___BaseColorFactor_4; }
	inline void set_BaseColorFactor_4(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  value)
	{
		___BaseColorFactor_4 = value;
	}

	inline static int32_t get_offset_of_BaseColorTexture_5() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2, ___BaseColorTexture_5)); }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * get_BaseColorTexture_5() const { return ___BaseColorTexture_5; }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 ** get_address_of_BaseColorTexture_5() { return &___BaseColorTexture_5; }
	inline void set_BaseColorTexture_5(TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * value)
	{
		___BaseColorTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___BaseColorTexture_5), value);
	}

	inline static int32_t get_offset_of_MetallicFactor_6() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2, ___MetallicFactor_6)); }
	inline double get_MetallicFactor_6() const { return ___MetallicFactor_6; }
	inline double* get_address_of_MetallicFactor_6() { return &___MetallicFactor_6; }
	inline void set_MetallicFactor_6(double value)
	{
		___MetallicFactor_6 = value;
	}

	inline static int32_t get_offset_of_RoughnessFactor_7() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2, ___RoughnessFactor_7)); }
	inline double get_RoughnessFactor_7() const { return ___RoughnessFactor_7; }
	inline double* get_address_of_RoughnessFactor_7() { return &___RoughnessFactor_7; }
	inline void set_RoughnessFactor_7(double value)
	{
		___RoughnessFactor_7 = value;
	}

	inline static int32_t get_offset_of_MetallicRoughnessTexture_8() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2, ___MetallicRoughnessTexture_8)); }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * get_MetallicRoughnessTexture_8() const { return ___MetallicRoughnessTexture_8; }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 ** get_address_of_MetallicRoughnessTexture_8() { return &___MetallicRoughnessTexture_8; }
	inline void set_MetallicRoughnessTexture_8(TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * value)
	{
		___MetallicRoughnessTexture_8 = value;
		Il2CppCodeGenWriteBarrier((&___MetallicRoughnessTexture_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBRMETALLICROUGHNESS_TBD92F38F67817774635FD764F2D23BCE346B27A2_H
#ifndef SKIN_T464B07F248F89DD7228D66EAD836CE5B20BB6F34_H
#define SKIN_T464B07F248F89DD7228D66EAD836CE5B20BB6F34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Skin
struct  Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// GLTF.Schema.AccessorId GLTF.Schema.Skin::InverseBindMatrices
	AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * ___InverseBindMatrices_5;
	// GLTF.Schema.NodeId GLTF.Schema.Skin::Skeleton
	NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * ___Skeleton_6;
	// System.Collections.Generic.List`1<GLTF.Schema.NodeId> GLTF.Schema.Skin::Joints
	List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * ___Joints_7;

public:
	inline static int32_t get_offset_of_InverseBindMatrices_5() { return static_cast<int32_t>(offsetof(Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34, ___InverseBindMatrices_5)); }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * get_InverseBindMatrices_5() const { return ___InverseBindMatrices_5; }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F ** get_address_of_InverseBindMatrices_5() { return &___InverseBindMatrices_5; }
	inline void set_InverseBindMatrices_5(AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * value)
	{
		___InverseBindMatrices_5 = value;
		Il2CppCodeGenWriteBarrier((&___InverseBindMatrices_5), value);
	}

	inline static int32_t get_offset_of_Skeleton_6() { return static_cast<int32_t>(offsetof(Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34, ___Skeleton_6)); }
	inline NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * get_Skeleton_6() const { return ___Skeleton_6; }
	inline NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 ** get_address_of_Skeleton_6() { return &___Skeleton_6; }
	inline void set_Skeleton_6(NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * value)
	{
		___Skeleton_6 = value;
		Il2CppCodeGenWriteBarrier((&___Skeleton_6), value);
	}

	inline static int32_t get_offset_of_Joints_7() { return static_cast<int32_t>(offsetof(Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34, ___Joints_7)); }
	inline List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * get_Joints_7() const { return ___Joints_7; }
	inline List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA ** get_address_of_Joints_7() { return &___Joints_7; }
	inline void set_Joints_7(List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * value)
	{
		___Joints_7 = value;
		Il2CppCodeGenWriteBarrier((&___Joints_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKIN_T464B07F248F89DD7228D66EAD836CE5B20BB6F34_H
#ifndef WRAPMODE_T1EA7797C8FC57C749F98136109CBEAEB4103B7CA_H
#define WRAPMODE_T1EA7797C8FC57C749F98136109CBEAEB4103B7CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.WrapMode
struct  WrapMode_t1EA7797C8FC57C749F98136109CBEAEB4103B7CA 
{
public:
	// System.Int32 GLTF.Schema.WrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WrapMode_t1EA7797C8FC57C749F98136109CBEAEB4103B7CA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPMODE_T1EA7797C8FC57C749F98136109CBEAEB4103B7CA_H
#ifndef DECLARATIONBINDINGTYPE_T1E6D8BC0DB7CA4D626BBAADA28BD76FFBBD73660_H
#define DECLARATIONBINDINGTYPE_T1E6D8BC0DB7CA4D626BBAADA28BD76FFBBD73660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.DeclarationBindingType
struct  DeclarationBindingType_t1E6D8BC0DB7CA4D626BBAADA28BD76FFBBD73660 
{
public:
	// System.Int32 Jint.DeclarationBindingType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeclarationBindingType_t1E6D8BC0DB7CA4D626BBAADA28BD76FFBBD73660, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECLARATIONBINDINGTYPE_T1E6D8BC0DB7CA4D626BBAADA28BD76FFBBD73660_H
#ifndef STEPMODE_TF8A960EEDD1BEF8767A1D32BC546D0BB17792D23_H
#define STEPMODE_TF8A960EEDD1BEF8767A1D32BC546D0BB17792D23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Debugger.StepMode
struct  StepMode_tF8A960EEDD1BEF8767A1D32BC546D0BB17792D23 
{
public:
	// System.Int32 Jint.Runtime.Debugger.StepMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StepMode_tF8A960EEDD1BEF8767A1D32BC546D0BB17792D23, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEPMODE_TF8A960EEDD1BEF8767A1D32BC546D0BB17792D23_H
#ifndef CLRFUNCTIONINSTANCE_T2F555835B5CF10010FA57E6641B4DD2241B2BAF5_H
#define CLRFUNCTIONINSTANCE_T2F555835B5CF10010FA57E6641B4DD2241B2BAF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.ClrFunctionInstance
struct  ClrFunctionInstance_t2F555835B5CF10010FA57E6641B4DD2241B2BAF5  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// System.Func`3<Jint.Native.JsValue,Jint.Native.JsValue[],Jint.Native.JsValue> Jint.Runtime.Interop.ClrFunctionInstance::_func
	Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 * ____func_8;

public:
	inline static int32_t get_offset_of__func_8() { return static_cast<int32_t>(offsetof(ClrFunctionInstance_t2F555835B5CF10010FA57E6641B4DD2241B2BAF5, ____func_8)); }
	inline Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 * get__func_8() const { return ____func_8; }
	inline Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 ** get_address_of__func_8() { return &____func_8; }
	inline void set__func_8(Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 * value)
	{
		____func_8 = value;
		Il2CppCodeGenWriteBarrier((&____func_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLRFUNCTIONINSTANCE_T2F555835B5CF10010FA57E6641B4DD2241B2BAF5_H
#ifndef DELEGATEWRAPPER_TAB370D5DD315553C91B6D945B3A6B3CC6C78F75A_H
#define DELEGATEWRAPPER_TAB370D5DD315553C91B6D945B3A6B3CC6C78F75A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.DelegateWrapper
struct  DelegateWrapper_tAB370D5DD315553C91B6D945B3A6B3CC6C78F75A  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// System.Delegate Jint.Runtime.Interop.DelegateWrapper::_d
	Delegate_t * ____d_8;

public:
	inline static int32_t get_offset_of__d_8() { return static_cast<int32_t>(offsetof(DelegateWrapper_tAB370D5DD315553C91B6D945B3A6B3CC6C78F75A, ____d_8)); }
	inline Delegate_t * get__d_8() const { return ____d_8; }
	inline Delegate_t ** get_address_of__d_8() { return &____d_8; }
	inline void set__d_8(Delegate_t * value)
	{
		____d_8 = value;
		Il2CppCodeGenWriteBarrier((&____d_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEWRAPPER_TAB370D5DD315553C91B6D945B3A6B3CC6C78F75A_H
#ifndef GETTERFUNCTIONINSTANCE_T0BD07D8EC478683CD4C2332C96C91B09F6AF6FE6_H
#define GETTERFUNCTIONINSTANCE_T0BD07D8EC478683CD4C2332C96C91B09F6AF6FE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.GetterFunctionInstance
struct  GetterFunctionInstance_t0BD07D8EC478683CD4C2332C96C91B09F6AF6FE6  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// System.Func`2<Jint.Native.JsValue,Jint.Native.JsValue> Jint.Runtime.Interop.GetterFunctionInstance::_getter
	Func_2_t7AC4704B1010794CE0FF7D63E97681D54F52A3C9 * ____getter_8;

public:
	inline static int32_t get_offset_of__getter_8() { return static_cast<int32_t>(offsetof(GetterFunctionInstance_t0BD07D8EC478683CD4C2332C96C91B09F6AF6FE6, ____getter_8)); }
	inline Func_2_t7AC4704B1010794CE0FF7D63E97681D54F52A3C9 * get__getter_8() const { return ____getter_8; }
	inline Func_2_t7AC4704B1010794CE0FF7D63E97681D54F52A3C9 ** get_address_of__getter_8() { return &____getter_8; }
	inline void set__getter_8(Func_2_t7AC4704B1010794CE0FF7D63E97681D54F52A3C9 * value)
	{
		____getter_8 = value;
		Il2CppCodeGenWriteBarrier((&____getter_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTERFUNCTIONINSTANCE_T0BD07D8EC478683CD4C2332C96C91B09F6AF6FE6_H
#ifndef TYPES_TE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A_H
#define TYPES_TE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Types
struct  Types_tE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A 
{
public:
	// System.Int32 Jint.Runtime.Types::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Types_tE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPES_TE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#define TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_0;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_1;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_5;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_0)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_0() const { return ___Zero_0; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_1)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_1() const { return ___MaxValue_1; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinValue_2() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_2)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_2() const { return ___MinValue_2; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_2() { return &___MinValue_2; }
	inline void set_MinValue_2(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_2 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_4() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_4)); }
	inline bool get__legacyConfigChecked_4() const { return ____legacyConfigChecked_4; }
	inline bool* get_address_of__legacyConfigChecked_4() { return &____legacyConfigChecked_4; }
	inline void set__legacyConfigChecked_4(bool value)
	{
		____legacyConfigChecked_4 = value;
	}

	inline static int32_t get_offset_of__legacyMode_5() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_5)); }
	inline bool get__legacyMode_5() const { return ____legacyMode_5; }
	inline bool* get_address_of__legacyMode_5() { return &____legacyMode_5; }
	inline void set__legacyMode_5(bool value)
	{
		____legacyMode_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifndef ACCESSORSPARSEINDICES_T5D6A82EB5C95370C9030AF6891AB3B020602A8A9_H
#define ACCESSORSPARSEINDICES_T5D6A82EB5C95370C9030AF6891AB3B020602A8A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AccessorSparseIndices
struct  AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Schema.BufferViewId GLTF.Schema.AccessorSparseIndices::BufferView
	BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * ___BufferView_4;
	// System.Int32 GLTF.Schema.AccessorSparseIndices::ByteOffset
	int32_t ___ByteOffset_5;
	// GLTF.Schema.GLTFComponentType GLTF.Schema.AccessorSparseIndices::ComponentType
	int32_t ___ComponentType_6;

public:
	inline static int32_t get_offset_of_BufferView_4() { return static_cast<int32_t>(offsetof(AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9, ___BufferView_4)); }
	inline BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * get_BufferView_4() const { return ___BufferView_4; }
	inline BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 ** get_address_of_BufferView_4() { return &___BufferView_4; }
	inline void set_BufferView_4(BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * value)
	{
		___BufferView_4 = value;
		Il2CppCodeGenWriteBarrier((&___BufferView_4), value);
	}

	inline static int32_t get_offset_of_ByteOffset_5() { return static_cast<int32_t>(offsetof(AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9, ___ByteOffset_5)); }
	inline int32_t get_ByteOffset_5() const { return ___ByteOffset_5; }
	inline int32_t* get_address_of_ByteOffset_5() { return &___ByteOffset_5; }
	inline void set_ByteOffset_5(int32_t value)
	{
		___ByteOffset_5 = value;
	}

	inline static int32_t get_offset_of_ComponentType_6() { return static_cast<int32_t>(offsetof(AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9, ___ComponentType_6)); }
	inline int32_t get_ComponentType_6() const { return ___ComponentType_6; }
	inline int32_t* get_address_of_ComponentType_6() { return &___ComponentType_6; }
	inline void set_ComponentType_6(int32_t value)
	{
		___ComponentType_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSORSPARSEINDICES_T5D6A82EB5C95370C9030AF6891AB3B020602A8A9_H
#ifndef ANIMATIONCHANNELTARGET_TA3FB03729DBDF54F53133C5ECB74E66250643AC3_H
#define ANIMATIONCHANNELTARGET_TA3FB03729DBDF54F53133C5ECB74E66250643AC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AnimationChannelTarget
struct  AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Schema.NodeId GLTF.Schema.AnimationChannelTarget::Node
	NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * ___Node_4;
	// GLTF.Schema.GLTFAnimationChannelPath GLTF.Schema.AnimationChannelTarget::Path
	int32_t ___Path_5;

public:
	inline static int32_t get_offset_of_Node_4() { return static_cast<int32_t>(offsetof(AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3, ___Node_4)); }
	inline NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * get_Node_4() const { return ___Node_4; }
	inline NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 ** get_address_of_Node_4() { return &___Node_4; }
	inline void set_Node_4(NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * value)
	{
		___Node_4 = value;
		Il2CppCodeGenWriteBarrier((&___Node_4), value);
	}

	inline static int32_t get_offset_of_Path_5() { return static_cast<int32_t>(offsetof(AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3, ___Path_5)); }
	inline int32_t get_Path_5() const { return ___Path_5; }
	inline int32_t* get_address_of_Path_5() { return &___Path_5; }
	inline void set_Path_5(int32_t value)
	{
		___Path_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCHANNELTARGET_TA3FB03729DBDF54F53133C5ECB74E66250643AC3_H
#ifndef ANIMATIONSAMPLER_T87DCC41677F9DC3D5E9933F23F31CBB298C43489_H
#define ANIMATIONSAMPLER_T87DCC41677F9DC3D5E9933F23F31CBB298C43489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AnimationSampler
struct  AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Schema.AccessorId GLTF.Schema.AnimationSampler::Input
	AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * ___Input_4;
	// GLTF.Schema.InterpolationType GLTF.Schema.AnimationSampler::Interpolation
	int32_t ___Interpolation_5;
	// GLTF.Schema.AccessorId GLTF.Schema.AnimationSampler::Output
	AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * ___Output_6;

public:
	inline static int32_t get_offset_of_Input_4() { return static_cast<int32_t>(offsetof(AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489, ___Input_4)); }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * get_Input_4() const { return ___Input_4; }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F ** get_address_of_Input_4() { return &___Input_4; }
	inline void set_Input_4(AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * value)
	{
		___Input_4 = value;
		Il2CppCodeGenWriteBarrier((&___Input_4), value);
	}

	inline static int32_t get_offset_of_Interpolation_5() { return static_cast<int32_t>(offsetof(AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489, ___Interpolation_5)); }
	inline int32_t get_Interpolation_5() const { return ___Interpolation_5; }
	inline int32_t* get_address_of_Interpolation_5() { return &___Interpolation_5; }
	inline void set_Interpolation_5(int32_t value)
	{
		___Interpolation_5 = value;
	}

	inline static int32_t get_offset_of_Output_6() { return static_cast<int32_t>(offsetof(AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489, ___Output_6)); }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * get_Output_6() const { return ___Output_6; }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F ** get_address_of_Output_6() { return &___Output_6; }
	inline void set_Output_6(AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * value)
	{
		___Output_6 = value;
		Il2CppCodeGenWriteBarrier((&___Output_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSAMPLER_T87DCC41677F9DC3D5E9933F23F31CBB298C43489_H
#ifndef BUFFERVIEW_TA426A728440976427358CC00CC2DF72A1FBFED7D_H
#define BUFFERVIEW_TA426A728440976427358CC00CC2DF72A1FBFED7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.BufferView
struct  BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// GLTF.Schema.BufferId GLTF.Schema.BufferView::Buffer
	BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4 * ___Buffer_5;
	// System.UInt32 GLTF.Schema.BufferView::ByteOffset
	uint32_t ___ByteOffset_6;
	// System.UInt32 GLTF.Schema.BufferView::ByteLength
	uint32_t ___ByteLength_7;
	// System.UInt32 GLTF.Schema.BufferView::ByteStride
	uint32_t ___ByteStride_8;
	// GLTF.Schema.BufferViewTarget GLTF.Schema.BufferView::Target
	int32_t ___Target_9;

public:
	inline static int32_t get_offset_of_Buffer_5() { return static_cast<int32_t>(offsetof(BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D, ___Buffer_5)); }
	inline BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4 * get_Buffer_5() const { return ___Buffer_5; }
	inline BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4 ** get_address_of_Buffer_5() { return &___Buffer_5; }
	inline void set_Buffer_5(BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4 * value)
	{
		___Buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_5), value);
	}

	inline static int32_t get_offset_of_ByteOffset_6() { return static_cast<int32_t>(offsetof(BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D, ___ByteOffset_6)); }
	inline uint32_t get_ByteOffset_6() const { return ___ByteOffset_6; }
	inline uint32_t* get_address_of_ByteOffset_6() { return &___ByteOffset_6; }
	inline void set_ByteOffset_6(uint32_t value)
	{
		___ByteOffset_6 = value;
	}

	inline static int32_t get_offset_of_ByteLength_7() { return static_cast<int32_t>(offsetof(BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D, ___ByteLength_7)); }
	inline uint32_t get_ByteLength_7() const { return ___ByteLength_7; }
	inline uint32_t* get_address_of_ByteLength_7() { return &___ByteLength_7; }
	inline void set_ByteLength_7(uint32_t value)
	{
		___ByteLength_7 = value;
	}

	inline static int32_t get_offset_of_ByteStride_8() { return static_cast<int32_t>(offsetof(BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D, ___ByteStride_8)); }
	inline uint32_t get_ByteStride_8() const { return ___ByteStride_8; }
	inline uint32_t* get_address_of_ByteStride_8() { return &___ByteStride_8; }
	inline void set_ByteStride_8(uint32_t value)
	{
		___ByteStride_8 = value;
	}

	inline static int32_t get_offset_of_Target_9() { return static_cast<int32_t>(offsetof(BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D, ___Target_9)); }
	inline int32_t get_Target_9() const { return ___Target_9; }
	inline int32_t* get_address_of_Target_9() { return &___Target_9; }
	inline void set_Target_9(int32_t value)
	{
		___Target_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERVIEW_TA426A728440976427358CC00CC2DF72A1FBFED7D_H
#ifndef GLTFCAMERA_T57A1CF701A46BA0A466550871375B8DEC12CDA85_H
#define GLTFCAMERA_T57A1CF701A46BA0A466550871375B8DEC12CDA85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFCamera
struct  GLTFCamera_t57A1CF701A46BA0A466550871375B8DEC12CDA85  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// GLTF.Schema.CameraOrthographic GLTF.Schema.GLTFCamera::Orthographic
	CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A * ___Orthographic_5;
	// GLTF.Schema.CameraPerspective GLTF.Schema.GLTFCamera::Perspective
	CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A * ___Perspective_6;
	// GLTF.Schema.CameraType GLTF.Schema.GLTFCamera::Type
	int32_t ___Type_7;

public:
	inline static int32_t get_offset_of_Orthographic_5() { return static_cast<int32_t>(offsetof(GLTFCamera_t57A1CF701A46BA0A466550871375B8DEC12CDA85, ___Orthographic_5)); }
	inline CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A * get_Orthographic_5() const { return ___Orthographic_5; }
	inline CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A ** get_address_of_Orthographic_5() { return &___Orthographic_5; }
	inline void set_Orthographic_5(CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A * value)
	{
		___Orthographic_5 = value;
		Il2CppCodeGenWriteBarrier((&___Orthographic_5), value);
	}

	inline static int32_t get_offset_of_Perspective_6() { return static_cast<int32_t>(offsetof(GLTFCamera_t57A1CF701A46BA0A466550871375B8DEC12CDA85, ___Perspective_6)); }
	inline CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A * get_Perspective_6() const { return ___Perspective_6; }
	inline CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A ** get_address_of_Perspective_6() { return &___Perspective_6; }
	inline void set_Perspective_6(CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A * value)
	{
		___Perspective_6 = value;
		Il2CppCodeGenWriteBarrier((&___Perspective_6), value);
	}

	inline static int32_t get_offset_of_Type_7() { return static_cast<int32_t>(offsetof(GLTFCamera_t57A1CF701A46BA0A466550871375B8DEC12CDA85, ___Type_7)); }
	inline int32_t get_Type_7() const { return ___Type_7; }
	inline int32_t* get_address_of_Type_7() { return &___Type_7; }
	inline void set_Type_7(int32_t value)
	{
		___Type_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFCAMERA_T57A1CF701A46BA0A466550871375B8DEC12CDA85_H
#ifndef GLTFMATERIAL_T3823C70B2B7CB4235F78C94163E7FA538F482C99_H
#define GLTFMATERIAL_T3823C70B2B7CB4235F78C94163E7FA538F482C99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFMaterial
struct  GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// GLTF.Schema.PbrMetallicRoughness GLTF.Schema.GLTFMaterial::PbrMetallicRoughness
	PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2 * ___PbrMetallicRoughness_5;
	// GLTF.Schema.MaterialCommonConstant GLTF.Schema.GLTFMaterial::CommonConstant
	MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6 * ___CommonConstant_6;
	// GLTF.Schema.NormalTextureInfo GLTF.Schema.GLTFMaterial::NormalTexture
	NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E * ___NormalTexture_7;
	// GLTF.Schema.OcclusionTextureInfo GLTF.Schema.GLTFMaterial::OcclusionTexture
	OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999 * ___OcclusionTexture_8;
	// GLTF.Schema.TextureInfo GLTF.Schema.GLTFMaterial::EmissiveTexture
	TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * ___EmissiveTexture_9;
	// GLTF.Math.Color GLTF.Schema.GLTFMaterial::EmissiveFactor
	Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  ___EmissiveFactor_10;
	// GLTF.Schema.AlphaMode GLTF.Schema.GLTFMaterial::AlphaMode
	int32_t ___AlphaMode_11;
	// System.Double GLTF.Schema.GLTFMaterial::AlphaCutoff
	double ___AlphaCutoff_12;
	// System.Boolean GLTF.Schema.GLTFMaterial::DoubleSided
	bool ___DoubleSided_13;

public:
	inline static int32_t get_offset_of_PbrMetallicRoughness_5() { return static_cast<int32_t>(offsetof(GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99, ___PbrMetallicRoughness_5)); }
	inline PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2 * get_PbrMetallicRoughness_5() const { return ___PbrMetallicRoughness_5; }
	inline PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2 ** get_address_of_PbrMetallicRoughness_5() { return &___PbrMetallicRoughness_5; }
	inline void set_PbrMetallicRoughness_5(PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2 * value)
	{
		___PbrMetallicRoughness_5 = value;
		Il2CppCodeGenWriteBarrier((&___PbrMetallicRoughness_5), value);
	}

	inline static int32_t get_offset_of_CommonConstant_6() { return static_cast<int32_t>(offsetof(GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99, ___CommonConstant_6)); }
	inline MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6 * get_CommonConstant_6() const { return ___CommonConstant_6; }
	inline MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6 ** get_address_of_CommonConstant_6() { return &___CommonConstant_6; }
	inline void set_CommonConstant_6(MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6 * value)
	{
		___CommonConstant_6 = value;
		Il2CppCodeGenWriteBarrier((&___CommonConstant_6), value);
	}

	inline static int32_t get_offset_of_NormalTexture_7() { return static_cast<int32_t>(offsetof(GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99, ___NormalTexture_7)); }
	inline NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E * get_NormalTexture_7() const { return ___NormalTexture_7; }
	inline NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E ** get_address_of_NormalTexture_7() { return &___NormalTexture_7; }
	inline void set_NormalTexture_7(NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E * value)
	{
		___NormalTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___NormalTexture_7), value);
	}

	inline static int32_t get_offset_of_OcclusionTexture_8() { return static_cast<int32_t>(offsetof(GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99, ___OcclusionTexture_8)); }
	inline OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999 * get_OcclusionTexture_8() const { return ___OcclusionTexture_8; }
	inline OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999 ** get_address_of_OcclusionTexture_8() { return &___OcclusionTexture_8; }
	inline void set_OcclusionTexture_8(OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999 * value)
	{
		___OcclusionTexture_8 = value;
		Il2CppCodeGenWriteBarrier((&___OcclusionTexture_8), value);
	}

	inline static int32_t get_offset_of_EmissiveTexture_9() { return static_cast<int32_t>(offsetof(GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99, ___EmissiveTexture_9)); }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * get_EmissiveTexture_9() const { return ___EmissiveTexture_9; }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 ** get_address_of_EmissiveTexture_9() { return &___EmissiveTexture_9; }
	inline void set_EmissiveTexture_9(TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * value)
	{
		___EmissiveTexture_9 = value;
		Il2CppCodeGenWriteBarrier((&___EmissiveTexture_9), value);
	}

	inline static int32_t get_offset_of_EmissiveFactor_10() { return static_cast<int32_t>(offsetof(GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99, ___EmissiveFactor_10)); }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  get_EmissiveFactor_10() const { return ___EmissiveFactor_10; }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 * get_address_of_EmissiveFactor_10() { return &___EmissiveFactor_10; }
	inline void set_EmissiveFactor_10(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  value)
	{
		___EmissiveFactor_10 = value;
	}

	inline static int32_t get_offset_of_AlphaMode_11() { return static_cast<int32_t>(offsetof(GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99, ___AlphaMode_11)); }
	inline int32_t get_AlphaMode_11() const { return ___AlphaMode_11; }
	inline int32_t* get_address_of_AlphaMode_11() { return &___AlphaMode_11; }
	inline void set_AlphaMode_11(int32_t value)
	{
		___AlphaMode_11 = value;
	}

	inline static int32_t get_offset_of_AlphaCutoff_12() { return static_cast<int32_t>(offsetof(GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99, ___AlphaCutoff_12)); }
	inline double get_AlphaCutoff_12() const { return ___AlphaCutoff_12; }
	inline double* get_address_of_AlphaCutoff_12() { return &___AlphaCutoff_12; }
	inline void set_AlphaCutoff_12(double value)
	{
		___AlphaCutoff_12 = value;
	}

	inline static int32_t get_offset_of_DoubleSided_13() { return static_cast<int32_t>(offsetof(GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99, ___DoubleSided_13)); }
	inline bool get_DoubleSided_13() const { return ___DoubleSided_13; }
	inline bool* get_address_of_DoubleSided_13() { return &___DoubleSided_13; }
	inline void set_DoubleSided_13(bool value)
	{
		___DoubleSided_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFMATERIAL_T3823C70B2B7CB4235F78C94163E7FA538F482C99_H
#ifndef MESHPRIMITIVE_T926B4BCAF5026C2DE7602FE9A15CE1E6784BA356_H
#define MESHPRIMITIVE_T926B4BCAF5026C2DE7602FE9A15CE1E6784BA356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MeshPrimitive
struct  MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId> GLTF.Schema.MeshPrimitive::Attributes
	Dictionary_2_tFA101F0B1ABA5C9CC83DEA8915A01ED1E64CB071 * ___Attributes_4;
	// GLTF.Schema.AccessorId GLTF.Schema.MeshPrimitive::Indices
	AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * ___Indices_5;
	// GLTF.Schema.MaterialId GLTF.Schema.MeshPrimitive::Material
	MaterialId_tD6B61586B1AE066F319C2118D3B9452637A36AF2 * ___Material_6;
	// GLTF.Schema.DrawMode GLTF.Schema.MeshPrimitive::Mode
	int32_t ___Mode_7;
	// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>> GLTF.Schema.MeshPrimitive::Targets
	List_1_tE97E4E597C42A24D18E09F33F48B49705CF33B72 * ___Targets_8;

public:
	inline static int32_t get_offset_of_Attributes_4() { return static_cast<int32_t>(offsetof(MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356, ___Attributes_4)); }
	inline Dictionary_2_tFA101F0B1ABA5C9CC83DEA8915A01ED1E64CB071 * get_Attributes_4() const { return ___Attributes_4; }
	inline Dictionary_2_tFA101F0B1ABA5C9CC83DEA8915A01ED1E64CB071 ** get_address_of_Attributes_4() { return &___Attributes_4; }
	inline void set_Attributes_4(Dictionary_2_tFA101F0B1ABA5C9CC83DEA8915A01ED1E64CB071 * value)
	{
		___Attributes_4 = value;
		Il2CppCodeGenWriteBarrier((&___Attributes_4), value);
	}

	inline static int32_t get_offset_of_Indices_5() { return static_cast<int32_t>(offsetof(MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356, ___Indices_5)); }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * get_Indices_5() const { return ___Indices_5; }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F ** get_address_of_Indices_5() { return &___Indices_5; }
	inline void set_Indices_5(AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * value)
	{
		___Indices_5 = value;
		Il2CppCodeGenWriteBarrier((&___Indices_5), value);
	}

	inline static int32_t get_offset_of_Material_6() { return static_cast<int32_t>(offsetof(MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356, ___Material_6)); }
	inline MaterialId_tD6B61586B1AE066F319C2118D3B9452637A36AF2 * get_Material_6() const { return ___Material_6; }
	inline MaterialId_tD6B61586B1AE066F319C2118D3B9452637A36AF2 ** get_address_of_Material_6() { return &___Material_6; }
	inline void set_Material_6(MaterialId_tD6B61586B1AE066F319C2118D3B9452637A36AF2 * value)
	{
		___Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___Material_6), value);
	}

	inline static int32_t get_offset_of_Mode_7() { return static_cast<int32_t>(offsetof(MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356, ___Mode_7)); }
	inline int32_t get_Mode_7() const { return ___Mode_7; }
	inline int32_t* get_address_of_Mode_7() { return &___Mode_7; }
	inline void set_Mode_7(int32_t value)
	{
		___Mode_7 = value;
	}

	inline static int32_t get_offset_of_Targets_8() { return static_cast<int32_t>(offsetof(MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356, ___Targets_8)); }
	inline List_1_tE97E4E597C42A24D18E09F33F48B49705CF33B72 * get_Targets_8() const { return ___Targets_8; }
	inline List_1_tE97E4E597C42A24D18E09F33F48B49705CF33B72 ** get_address_of_Targets_8() { return &___Targets_8; }
	inline void set_Targets_8(List_1_tE97E4E597C42A24D18E09F33F48B49705CF33B72 * value)
	{
		___Targets_8 = value;
		Il2CppCodeGenWriteBarrier((&___Targets_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHPRIMITIVE_T926B4BCAF5026C2DE7602FE9A15CE1E6784BA356_H
#ifndef SAMPLER_T6C8135F817296462CAA463A25BF6444ABB84C54A_H
#define SAMPLER_T6C8135F817296462CAA463A25BF6444ABB84C54A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Sampler
struct  Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// GLTF.Schema.MagFilterMode GLTF.Schema.Sampler::MagFilter
	int32_t ___MagFilter_5;
	// GLTF.Schema.MinFilterMode GLTF.Schema.Sampler::MinFilter
	int32_t ___MinFilter_6;
	// GLTF.Schema.WrapMode GLTF.Schema.Sampler::WrapS
	int32_t ___WrapS_7;
	// GLTF.Schema.WrapMode GLTF.Schema.Sampler::WrapT
	int32_t ___WrapT_8;

public:
	inline static int32_t get_offset_of_MagFilter_5() { return static_cast<int32_t>(offsetof(Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A, ___MagFilter_5)); }
	inline int32_t get_MagFilter_5() const { return ___MagFilter_5; }
	inline int32_t* get_address_of_MagFilter_5() { return &___MagFilter_5; }
	inline void set_MagFilter_5(int32_t value)
	{
		___MagFilter_5 = value;
	}

	inline static int32_t get_offset_of_MinFilter_6() { return static_cast<int32_t>(offsetof(Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A, ___MinFilter_6)); }
	inline int32_t get_MinFilter_6() const { return ___MinFilter_6; }
	inline int32_t* get_address_of_MinFilter_6() { return &___MinFilter_6; }
	inline void set_MinFilter_6(int32_t value)
	{
		___MinFilter_6 = value;
	}

	inline static int32_t get_offset_of_WrapS_7() { return static_cast<int32_t>(offsetof(Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A, ___WrapS_7)); }
	inline int32_t get_WrapS_7() const { return ___WrapS_7; }
	inline int32_t* get_address_of_WrapS_7() { return &___WrapS_7; }
	inline void set_WrapS_7(int32_t value)
	{
		___WrapS_7 = value;
	}

	inline static int32_t get_offset_of_WrapT_8() { return static_cast<int32_t>(offsetof(Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A, ___WrapT_8)); }
	inline int32_t get_WrapT_8() const { return ___WrapT_8; }
	inline int32_t* get_address_of_WrapT_8() { return &___WrapT_8; }
	inline void set_WrapT_8(int32_t value)
	{
		___WrapT_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLER_T6C8135F817296462CAA463A25BF6444ABB84C54A_H
#ifndef OPTIONS_T9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48_H
#define OPTIONS_T9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Options
struct  Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48  : public RuntimeObject
{
public:
	// System.Boolean Jint.Options::_strict
	bool ____strict_0;
	// System.Boolean Jint.Options::_allowDebuggerStatement
	bool ____allowDebuggerStatement_1;
	// System.Boolean Jint.Options::_debugMode
	bool ____debugMode_2;
	// System.Boolean Jint.Options::_allowClr
	bool ____allowClr_3;
	// System.Collections.Generic.List`1<Jint.Runtime.Interop.IObjectConverter> Jint.Options::_objectConverters
	List_1_tC5F29A3852862B17B14A3CDFE61A42230EE4D7D9 * ____objectConverters_4;
	// System.Int32 Jint.Options::_maxStatements
	int32_t ____maxStatements_5;
	// System.Int32 Jint.Options::_maxRecursionDepth
	int32_t ____maxRecursionDepth_6;
	// System.TimeSpan Jint.Options::_timeoutInterval
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ____timeoutInterval_7;
	// System.Globalization.CultureInfo Jint.Options::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_8;
	// System.TimeZoneInfo Jint.Options::_localTimeZone
	TimeZoneInfo_t46EF9BAEAA787846F1A1EC419BE75CFEFAFF6777 * ____localTimeZone_9;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Jint.Options::_lookupAssemblies
	List_1_t0B17DCDEBCE728AA1CAFF9D7D1441755B5949EED * ____lookupAssemblies_10;
	// System.Predicate`1<System.Exception> Jint.Options::_clrExceptionsHandler
	Predicate_1_t26A4B8466526D596ABAD09AEC4CE1E40FB5CC700 * ____clrExceptionsHandler_11;
	// Jint.Runtime.Interop.IReferenceResolver Jint.Options::_referenceResolver
	RuntimeObject* ____referenceResolver_12;

public:
	inline static int32_t get_offset_of__strict_0() { return static_cast<int32_t>(offsetof(Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48, ____strict_0)); }
	inline bool get__strict_0() const { return ____strict_0; }
	inline bool* get_address_of__strict_0() { return &____strict_0; }
	inline void set__strict_0(bool value)
	{
		____strict_0 = value;
	}

	inline static int32_t get_offset_of__allowDebuggerStatement_1() { return static_cast<int32_t>(offsetof(Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48, ____allowDebuggerStatement_1)); }
	inline bool get__allowDebuggerStatement_1() const { return ____allowDebuggerStatement_1; }
	inline bool* get_address_of__allowDebuggerStatement_1() { return &____allowDebuggerStatement_1; }
	inline void set__allowDebuggerStatement_1(bool value)
	{
		____allowDebuggerStatement_1 = value;
	}

	inline static int32_t get_offset_of__debugMode_2() { return static_cast<int32_t>(offsetof(Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48, ____debugMode_2)); }
	inline bool get__debugMode_2() const { return ____debugMode_2; }
	inline bool* get_address_of__debugMode_2() { return &____debugMode_2; }
	inline void set__debugMode_2(bool value)
	{
		____debugMode_2 = value;
	}

	inline static int32_t get_offset_of__allowClr_3() { return static_cast<int32_t>(offsetof(Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48, ____allowClr_3)); }
	inline bool get__allowClr_3() const { return ____allowClr_3; }
	inline bool* get_address_of__allowClr_3() { return &____allowClr_3; }
	inline void set__allowClr_3(bool value)
	{
		____allowClr_3 = value;
	}

	inline static int32_t get_offset_of__objectConverters_4() { return static_cast<int32_t>(offsetof(Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48, ____objectConverters_4)); }
	inline List_1_tC5F29A3852862B17B14A3CDFE61A42230EE4D7D9 * get__objectConverters_4() const { return ____objectConverters_4; }
	inline List_1_tC5F29A3852862B17B14A3CDFE61A42230EE4D7D9 ** get_address_of__objectConverters_4() { return &____objectConverters_4; }
	inline void set__objectConverters_4(List_1_tC5F29A3852862B17B14A3CDFE61A42230EE4D7D9 * value)
	{
		____objectConverters_4 = value;
		Il2CppCodeGenWriteBarrier((&____objectConverters_4), value);
	}

	inline static int32_t get_offset_of__maxStatements_5() { return static_cast<int32_t>(offsetof(Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48, ____maxStatements_5)); }
	inline int32_t get__maxStatements_5() const { return ____maxStatements_5; }
	inline int32_t* get_address_of__maxStatements_5() { return &____maxStatements_5; }
	inline void set__maxStatements_5(int32_t value)
	{
		____maxStatements_5 = value;
	}

	inline static int32_t get_offset_of__maxRecursionDepth_6() { return static_cast<int32_t>(offsetof(Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48, ____maxRecursionDepth_6)); }
	inline int32_t get__maxRecursionDepth_6() const { return ____maxRecursionDepth_6; }
	inline int32_t* get_address_of__maxRecursionDepth_6() { return &____maxRecursionDepth_6; }
	inline void set__maxRecursionDepth_6(int32_t value)
	{
		____maxRecursionDepth_6 = value;
	}

	inline static int32_t get_offset_of__timeoutInterval_7() { return static_cast<int32_t>(offsetof(Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48, ____timeoutInterval_7)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get__timeoutInterval_7() const { return ____timeoutInterval_7; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of__timeoutInterval_7() { return &____timeoutInterval_7; }
	inline void set__timeoutInterval_7(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		____timeoutInterval_7 = value;
	}

	inline static int32_t get_offset_of__culture_8() { return static_cast<int32_t>(offsetof(Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48, ____culture_8)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_8() const { return ____culture_8; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_8() { return &____culture_8; }
	inline void set__culture_8(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_8 = value;
		Il2CppCodeGenWriteBarrier((&____culture_8), value);
	}

	inline static int32_t get_offset_of__localTimeZone_9() { return static_cast<int32_t>(offsetof(Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48, ____localTimeZone_9)); }
	inline TimeZoneInfo_t46EF9BAEAA787846F1A1EC419BE75CFEFAFF6777 * get__localTimeZone_9() const { return ____localTimeZone_9; }
	inline TimeZoneInfo_t46EF9BAEAA787846F1A1EC419BE75CFEFAFF6777 ** get_address_of__localTimeZone_9() { return &____localTimeZone_9; }
	inline void set__localTimeZone_9(TimeZoneInfo_t46EF9BAEAA787846F1A1EC419BE75CFEFAFF6777 * value)
	{
		____localTimeZone_9 = value;
		Il2CppCodeGenWriteBarrier((&____localTimeZone_9), value);
	}

	inline static int32_t get_offset_of__lookupAssemblies_10() { return static_cast<int32_t>(offsetof(Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48, ____lookupAssemblies_10)); }
	inline List_1_t0B17DCDEBCE728AA1CAFF9D7D1441755B5949EED * get__lookupAssemblies_10() const { return ____lookupAssemblies_10; }
	inline List_1_t0B17DCDEBCE728AA1CAFF9D7D1441755B5949EED ** get_address_of__lookupAssemblies_10() { return &____lookupAssemblies_10; }
	inline void set__lookupAssemblies_10(List_1_t0B17DCDEBCE728AA1CAFF9D7D1441755B5949EED * value)
	{
		____lookupAssemblies_10 = value;
		Il2CppCodeGenWriteBarrier((&____lookupAssemblies_10), value);
	}

	inline static int32_t get_offset_of__clrExceptionsHandler_11() { return static_cast<int32_t>(offsetof(Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48, ____clrExceptionsHandler_11)); }
	inline Predicate_1_t26A4B8466526D596ABAD09AEC4CE1E40FB5CC700 * get__clrExceptionsHandler_11() const { return ____clrExceptionsHandler_11; }
	inline Predicate_1_t26A4B8466526D596ABAD09AEC4CE1E40FB5CC700 ** get_address_of__clrExceptionsHandler_11() { return &____clrExceptionsHandler_11; }
	inline void set__clrExceptionsHandler_11(Predicate_1_t26A4B8466526D596ABAD09AEC4CE1E40FB5CC700 * value)
	{
		____clrExceptionsHandler_11 = value;
		Il2CppCodeGenWriteBarrier((&____clrExceptionsHandler_11), value);
	}

	inline static int32_t get_offset_of__referenceResolver_12() { return static_cast<int32_t>(offsetof(Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48, ____referenceResolver_12)); }
	inline RuntimeObject* get__referenceResolver_12() const { return ____referenceResolver_12; }
	inline RuntimeObject** get_address_of__referenceResolver_12() { return &____referenceResolver_12; }
	inline void set__referenceResolver_12(RuntimeObject* value)
	{
		____referenceResolver_12 = value;
		Il2CppCodeGenWriteBarrier((&____referenceResolver_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONS_T9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef BREAKDELEGATE_TCA6935CACA104BE87718D10A6CB84A635B87F5FD_H
#define BREAKDELEGATE_TCA6935CACA104BE87718D10A6CB84A635B87F5FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Engine/BreakDelegate
struct  BreakDelegate_tCA6935CACA104BE87718D10A6CB84A635B87F5FD  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BREAKDELEGATE_TCA6935CACA104BE87718D10A6CB84A635B87F5FD_H
#ifndef DEBUGSTEPDELEGATE_T71D4D1019B035251C57C7527AF515F8D509D1284_H
#define DEBUGSTEPDELEGATE_T71D4D1019B035251C57C7527AF515F8D509D1284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Engine/DebugStepDelegate
struct  DebugStepDelegate_t71D4D1019B035251C57C7527AF515F8D509D1284  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGSTEPDELEGATE_T71D4D1019B035251C57C7527AF515F8D509D1284_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5500 = { sizeof (GLTFAccessorAttributeType_t733A462A97AE55D3A765746932C23C9AF0A9A67E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5500[8] = 
{
	GLTFAccessorAttributeType_t733A462A97AE55D3A765746932C23C9AF0A9A67E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5501 = { sizeof (NumericArray_t4941F537DC57A0602218632AABDF228987E030E9)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5501[12] = 
{
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsUInts_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsFloats_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsVec2s_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsVec3s_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsVec4s_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsMatrix4x4s_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsColors_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsTexcoords_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsVertices_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsNormals_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsTangents_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsTriangles_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5502 = { sizeof (AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5502[3] = 
{
	AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C::get_offset_of_Count_4(),
	AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C::get_offset_of_Indices_5(),
	AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C::get_offset_of_Values_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5503 = { sizeof (AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5503[3] = 
{
	AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9::get_offset_of_BufferView_4(),
	AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9::get_offset_of_ByteOffset_5(),
	AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9::get_offset_of_ComponentType_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5504 = { sizeof (AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5504[2] = 
{
	AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3::get_offset_of_BufferView_4(),
	AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3::get_offset_of_ByteOffset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5505 = { sizeof (AnimationChannel_t904A44258FFDCEBCDE71AFA92ED2514EE891BFC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5505[2] = 
{
	AnimationChannel_t904A44258FFDCEBCDE71AFA92ED2514EE891BFC1::get_offset_of_Sampler_4(),
	AnimationChannel_t904A44258FFDCEBCDE71AFA92ED2514EE891BFC1::get_offset_of_Target_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5506 = { sizeof (AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5506[2] = 
{
	AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3::get_offset_of_Node_4(),
	AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3::get_offset_of_Path_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5507 = { sizeof (GLTFAnimationChannelPath_t6C68975EA59C1B193742B9FBF09EF26192171FE8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5507[5] = 
{
	GLTFAnimationChannelPath_t6C68975EA59C1B193742B9FBF09EF26192171FE8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5508 = { sizeof (InterpolationType_t31C5CDC2D3A825B4A0287213B304384CC34CEB28)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5508[5] = 
{
	InterpolationType_t31C5CDC2D3A825B4A0287213B304384CC34CEB28::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5509 = { sizeof (AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5509[3] = 
{
	AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489::get_offset_of_Input_4(),
	AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489::get_offset_of_Interpolation_5(),
	AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489::get_offset_of_Output_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5510 = { sizeof (Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5510[4] = 
{
	Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B::get_offset_of_Copyright_4(),
	Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B::get_offset_of_Generator_5(),
	Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B::get_offset_of_Version_6(),
	Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B::get_offset_of_MinVersion_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5511 = { sizeof (BufferViewTarget_t56AAAFD9880E8FE1098EE7B4F2F7DC54893FB87E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5511[4] = 
{
	BufferViewTarget_t56AAAFD9880E8FE1098EE7B4F2F7DC54893FB87E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5512 = { sizeof (BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5512[5] = 
{
	BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D::get_offset_of_Buffer_5(),
	BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D::get_offset_of_ByteOffset_6(),
	BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D::get_offset_of_ByteLength_7(),
	BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D::get_offset_of_ByteStride_8(),
	BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D::get_offset_of_Target_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5513 = { sizeof (CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5513[4] = 
{
	CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A::get_offset_of_XMag_4(),
	CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A::get_offset_of_YMag_5(),
	CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A::get_offset_of_ZFar_6(),
	CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A::get_offset_of_ZNear_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5514 = { sizeof (CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5514[4] = 
{
	CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A::get_offset_of_AspectRatio_4(),
	CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A::get_offset_of_YFov_5(),
	CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A::get_offset_of_ZFar_6(),
	CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A::get_offset_of_ZNear_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5515 = { sizeof (GLTFAnimation_tF5162217FBA9DA23C125D87C6335764CED3B84D0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5515[2] = 
{
	GLTFAnimation_tF5162217FBA9DA23C125D87C6335764CED3B84D0::get_offset_of_Channels_5(),
	GLTFAnimation_tF5162217FBA9DA23C125D87C6335764CED3B84D0::get_offset_of_Samplers_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5516 = { sizeof (U3CU3Ec__DisplayClass2_0_tCBC9C5921528275966BC1DC33758B464100BDD92), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5516[4] = 
{
	U3CU3Ec__DisplayClass2_0_tCBC9C5921528275966BC1DC33758B464100BDD92::get_offset_of_root_0(),
	U3CU3Ec__DisplayClass2_0_tCBC9C5921528275966BC1DC33758B464100BDD92::get_offset_of_reader_1(),
	U3CU3Ec__DisplayClass2_0_tCBC9C5921528275966BC1DC33758B464100BDD92::get_offset_of_U3CU3E9__0_2(),
	U3CU3Ec__DisplayClass2_0_tCBC9C5921528275966BC1DC33758B464100BDD92::get_offset_of_U3CU3E9__1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5517 = { sizeof (GLTFBuffer_t093686A8CEC027B3EFCCCC89EF874F3BE6738CA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5517[2] = 
{
	GLTFBuffer_t093686A8CEC027B3EFCCCC89EF874F3BE6738CA1::get_offset_of_Uri_5(),
	GLTFBuffer_t093686A8CEC027B3EFCCCC89EF874F3BE6738CA1::get_offset_of_ByteLength_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5518 = { sizeof (GLTFCamera_t57A1CF701A46BA0A466550871375B8DEC12CDA85), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5518[3] = 
{
	GLTFCamera_t57A1CF701A46BA0A466550871375B8DEC12CDA85::get_offset_of_Orthographic_5(),
	GLTFCamera_t57A1CF701A46BA0A466550871375B8DEC12CDA85::get_offset_of_Perspective_6(),
	GLTFCamera_t57A1CF701A46BA0A466550871375B8DEC12CDA85::get_offset_of_Type_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5519 = { sizeof (CameraType_t1A6E126E159E7471427ACB5C621161AC95024DDE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5519[3] = 
{
	CameraType_t1A6E126E159E7471427ACB5C621161AC95024DDE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5520 = { sizeof (GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5520[1] = 
{
	GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C::get_offset_of_Name_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5521 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5521[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5522 = { sizeof (AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5523 = { sizeof (BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5524 = { sizeof (BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5525 = { sizeof (CameraId_t473F137B518358316FD6D1100DFF1D190E849BAC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5526 = { sizeof (ImageId_t6B75A8719AF05FF5EF157163A485CAB340DDE68B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5527 = { sizeof (MaterialId_tD6B61586B1AE066F319C2118D3B9452637A36AF2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5528 = { sizeof (MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5529 = { sizeof (NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5530 = { sizeof (SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5531 = { sizeof (SceneId_t67CF37B99AF8D98A11C03A45BDD51773EE6D605C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5532 = { sizeof (SkinId_t500172B9076F6784EF4CF0CD5CF6F87516549F02), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5533 = { sizeof (TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5534 = { sizeof (GLTFImage_tA3F1517270CE1DC26739C076F757BFA7B651A4EE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5534[3] = 
{
	GLTFImage_tA3F1517270CE1DC26739C076F757BFA7B651A4EE::get_offset_of_Uri_5(),
	GLTFImage_tA3F1517270CE1DC26739C076F757BFA7B651A4EE::get_offset_of_MimeType_6(),
	GLTFImage_tA3F1517270CE1DC26739C076F757BFA7B651A4EE::get_offset_of_BufferView_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5535 = { sizeof (GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5535[9] = 
{
	GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99::get_offset_of_PbrMetallicRoughness_5(),
	GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99::get_offset_of_CommonConstant_6(),
	GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99::get_offset_of_NormalTexture_7(),
	GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99::get_offset_of_OcclusionTexture_8(),
	GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99::get_offset_of_EmissiveTexture_9(),
	GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99::get_offset_of_EmissiveFactor_10(),
	GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99::get_offset_of_AlphaMode_11(),
	GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99::get_offset_of_AlphaCutoff_12(),
	GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99::get_offset_of_DoubleSided_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5536 = { sizeof (AlphaMode_t41015BEC6E2BB18C0A2688E6CF441F148BA768E6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5536[4] = 
{
	AlphaMode_t41015BEC6E2BB18C0A2688E6CF441F148BA768E6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5537 = { sizeof (GLTFMesh_tD3F424D9F92A52F43406967EA5B985F2735DD1AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5537[2] = 
{
	GLTFMesh_tD3F424D9F92A52F43406967EA5B985F2735DD1AD::get_offset_of_Primitives_5(),
	GLTFMesh_tD3F424D9F92A52F43406967EA5B985F2735DD1AD::get_offset_of_Weights_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5538 = { sizeof (U3CU3Ec__DisplayClass4_0_t079FEDDF0C13DB6738A275D9130A8EE28D7D6082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5538[3] = 
{
	U3CU3Ec__DisplayClass4_0_t079FEDDF0C13DB6738A275D9130A8EE28D7D6082::get_offset_of_root_0(),
	U3CU3Ec__DisplayClass4_0_t079FEDDF0C13DB6738A275D9130A8EE28D7D6082::get_offset_of_reader_1(),
	U3CU3Ec__DisplayClass4_0_t079FEDDF0C13DB6738A275D9130A8EE28D7D6082::get_offset_of_U3CU3E9__0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5539 = { sizeof (GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979), -1, sizeof(GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5539[4] = 
{
	GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields::get_offset_of__extensionRegistry_0(),
	GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields::get_offset_of__defaultExtensionFactory_1(),
	GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979::get_offset_of_Extensions_2(),
	GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979::get_offset_of_Extras_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5540 = { sizeof (GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5540[18] = 
{
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_ExtensionsUsed_4(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_ExtensionsRequired_5(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Accessors_6(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Animations_7(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Asset_8(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Buffers_9(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_BufferViews_10(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Cameras_11(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Images_12(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Materials_13(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Meshes_14(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Nodes_15(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Samplers_16(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Scene_17(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Scenes_18(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Skins_19(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Textures_20(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_IsGLB_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5541 = { sizeof (U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5541[15] = 
{
	U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396::get_offset_of_root_0(),
	U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396::get_offset_of_jsonReader_1(),
	U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396::get_offset_of_U3CU3E9__0_2(),
	U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396::get_offset_of_U3CU3E9__1_3(),
	U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396::get_offset_of_U3CU3E9__2_4(),
	U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396::get_offset_of_U3CU3E9__3_5(),
	U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396::get_offset_of_U3CU3E9__4_6(),
	U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396::get_offset_of_U3CU3E9__5_7(),
	U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396::get_offset_of_U3CU3E9__6_8(),
	U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396::get_offset_of_U3CU3E9__7_9(),
	U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396::get_offset_of_U3CU3E9__8_10(),
	U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396::get_offset_of_U3CU3E9__9_11(),
	U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396::get_offset_of_U3CU3E9__10_12(),
	U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396::get_offset_of_U3CU3E9__11_13(),
	U3CU3Ec__DisplayClass21_0_tA33076FF6567D91AE6765179BE8BE2D7DD746396::get_offset_of_U3CU3E9__12_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5542 = { sizeof (GLTFScene_t668D116E319247178F4641B2F3A4DF33C6343BB6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5542[1] = 
{
	GLTFScene_t668D116E319247178F4641B2F3A4DF33C6343BB6::get_offset_of_Nodes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5543 = { sizeof (GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5543[2] = 
{
	GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65::get_offset_of_Sampler_5(),
	GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65::get_offset_of_Source_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5544 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5545 = { sizeof (ExtensionFactory_t9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5545[1] = 
{
	ExtensionFactory_t9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7::get_offset_of_ExtensionName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5546 = { sizeof (DefaultExtension_tC6AFCFA81A2D287E9664F49E49A9DA580F76FBAD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5546[1] = 
{
	DefaultExtension_tC6AFCFA81A2D287E9664F49E49A9DA580F76FBAD::get_offset_of_U3CExtensionDataU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5547 = { sizeof (DefaultExtensionFactory_t6F7054C0E34948CE71E4DB22B285190D70C9B3E8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5548 = { sizeof (MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5548[3] = 
{
	MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6::get_offset_of_AmbientFactor_4(),
	MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6::get_offset_of_LightmapTexture_5(),
	MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6::get_offset_of_LightmapFactor_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5549 = { sizeof (NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5549[1] = 
{
	NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E::get_offset_of_Scale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5550 = { sizeof (OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5550[1] = 
{
	OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999::get_offset_of_Strength_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5551 = { sizeof (PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5551[5] = 
{
	PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2::get_offset_of_BaseColorFactor_4(),
	PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2::get_offset_of_BaseColorTexture_5(),
	PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2::get_offset_of_MetallicFactor_6(),
	PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2::get_offset_of_RoughnessFactor_7(),
	PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2::get_offset_of_MetallicRoughnessTexture_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5552 = { sizeof (MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5552[5] = 
{
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356::get_offset_of_Attributes_4(),
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356::get_offset_of_Indices_5(),
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356::get_offset_of_Material_6(),
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356::get_offset_of_Mode_7(),
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356::get_offset_of_Targets_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5553 = { sizeof (U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5553[5] = 
{
	U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323::get_offset_of_reader_0(),
	U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323::get_offset_of_root_1(),
	U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323::get_offset_of_U3CU3E9__0_2(),
	U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323::get_offset_of_U3CU3E9__2_3(),
	U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323::get_offset_of_U3CU3E9__1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5554 = { sizeof (SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A), -1, sizeof(SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5554[6] = 
{
	SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields::get_offset_of_POSITION_0(),
	SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields::get_offset_of_NORMAL_1(),
	SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields::get_offset_of_JOINT_2(),
	SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields::get_offset_of_WEIGHT_3(),
	SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields::get_offset_of_TANGENT_4(),
	SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields::get_offset_of_INDICES_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5555 = { sizeof (DrawMode_t2A826FD4C32BB9350CE2130C2DDC5AF6D44A430B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5555[8] = 
{
	DrawMode_t2A826FD4C32BB9350CE2130C2DDC5AF6D44A430B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5556 = { sizeof (Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5556[10] = 
{
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_UseTRS_5(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Camera_6(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Children_7(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Skin_8(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Matrix_9(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Mesh_10(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Rotation_11(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Scale_12(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Translation_13(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Weights_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5557 = { sizeof (Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5557[4] = 
{
	Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A::get_offset_of_MagFilter_5(),
	Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A::get_offset_of_MinFilter_6(),
	Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A::get_offset_of_WrapS_7(),
	Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A::get_offset_of_WrapT_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5558 = { sizeof (MagFilterMode_t41718D0BF404CAE8E6F2DAB45747B04C74EB8F5D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5558[4] = 
{
	MagFilterMode_t41718D0BF404CAE8E6F2DAB45747B04C74EB8F5D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5559 = { sizeof (MinFilterMode_t9C07AF4837EC02E1903D6631D2ED23FB38DC778B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5559[8] = 
{
	MinFilterMode_t9C07AF4837EC02E1903D6631D2ED23FB38DC778B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5560 = { sizeof (WrapMode_t1EA7797C8FC57C749F98136109CBEAEB4103B7CA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5560[5] = 
{
	WrapMode_t1EA7797C8FC57C749F98136109CBEAEB4103B7CA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5561 = { sizeof (Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5561[3] = 
{
	Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34::get_offset_of_InverseBindMatrices_5(),
	Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34::get_offset_of_Skeleton_6(),
	Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34::get_offset_of_Joints_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5562 = { sizeof (TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5562[2] = 
{
	TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66::get_offset_of_Index_4(),
	TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66::get_offset_of_TexCoord_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5563 = { sizeof (U3CPrivateImplementationDetailsU3E_t3408FB995FEF331FE68B64E76A23640867228A02), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5564 = { sizeof (U3CModuleU3E_t74F25D240759C02997A4700D8A698D8EA73AA7B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5565 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5565[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5566 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5566[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5567 = { sizeof (DeclarationBindingType_t1E6D8BC0DB7CA4D626BBAADA28BD76FFBBD73660)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5567[4] = 
{
	DeclarationBindingType_t1E6D8BC0DB7CA4D626BBAADA28BD76FFBBD73660::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5568 = { sizeof (Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D), -1, sizeof(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5568[36] = 
{
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of__expressions_0(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of__statements_1(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of__executionContexts_2(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of__completionValue_3(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of__statementsCount_4(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of__timeoutTicks_5(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of__lastSyntaxNode_6(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_ClrTypeConverter_7(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_TypeCache_8(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D_StaticFields::get_offset_of_TypeMappers_9(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_CallStack_10(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_GlobalEnvironment_11(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CGlobalU3Ek__BackingField_12(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CObjectU3Ek__BackingField_13(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CFunctionU3Ek__BackingField_14(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CArrayU3Ek__BackingField_15(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CStringU3Ek__BackingField_16(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CRegExpU3Ek__BackingField_17(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CBooleanU3Ek__BackingField_18(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CNumberU3Ek__BackingField_19(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CDateU3Ek__BackingField_20(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CMathU3Ek__BackingField_21(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CJsonU3Ek__BackingField_22(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CEvalU3Ek__BackingField_23(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CErrorU3Ek__BackingField_24(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CEvalErrorU3Ek__BackingField_25(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CSyntaxErrorU3Ek__BackingField_26(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CTypeErrorU3Ek__BackingField_27(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CRangeErrorU3Ek__BackingField_28(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CReferenceErrorU3Ek__BackingField_29(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CUriErrorU3Ek__BackingField_30(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3COptionsU3Ek__BackingField_31(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_Step_32(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_Break_33(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CDebugHandlerU3Ek__BackingField_34(),
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D::get_offset_of_U3CBreakPointsU3Ek__BackingField_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5569 = { sizeof (DebugStepDelegate_t71D4D1019B035251C57C7527AF515F8D509D1284), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5570 = { sizeof (BreakDelegate_tCA6935CACA104BE87718D10A6CB84A635B87F5FD), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5571 = { sizeof (U3CU3Ec_tDE3B66DA56D8CBD6E7159670C19F68253FDB9EA3), -1, sizeof(U3CU3Ec_tDE3B66DA56D8CBD6E7159670C19F68253FDB9EA3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5571[2] = 
{
	U3CU3Ec_tDE3B66DA56D8CBD6E7159670C19F68253FDB9EA3_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tDE3B66DA56D8CBD6E7159670C19F68253FDB9EA3_StaticFields::get_offset_of_U3CU3E9__142_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5572 = { sizeof (EvalCodeScope_t0E707EEB9CE345520B6856F1378523ED3EBF02F8), -1, 0, sizeof(EvalCodeScope_t0E707EEB9CE345520B6856F1378523ED3EBF02F8_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable5572[4] = 
{
	EvalCodeScope_t0E707EEB9CE345520B6856F1378523ED3EBF02F8::get_offset_of__eval_0(),
	EvalCodeScope_t0E707EEB9CE345520B6856F1378523ED3EBF02F8::get_offset_of__force_1(),
	EvalCodeScope_t0E707EEB9CE345520B6856F1378523ED3EBF02F8::get_offset_of__forcedRefCount_2(),
	EvalCodeScope_t0E707EEB9CE345520B6856F1378523ED3EBF02F8_ThreadStaticFields::get_offset_of__refCount_3() | THREAD_LOCAL_STATIC_MASK,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5573 = { sizeof (Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5573[13] = 
{
	Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48::get_offset_of__strict_0(),
	Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48::get_offset_of__allowDebuggerStatement_1(),
	Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48::get_offset_of__debugMode_2(),
	Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48::get_offset_of__allowClr_3(),
	Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48::get_offset_of__objectConverters_4(),
	Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48::get_offset_of__maxStatements_5(),
	Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48::get_offset_of__maxRecursionDepth_6(),
	Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48::get_offset_of__timeoutInterval_7(),
	Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48::get_offset_of__culture_8(),
	Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48::get_offset_of__localTimeZone_9(),
	Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48::get_offset_of__lookupAssemblies_10(),
	Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48::get_offset_of__clrExceptionsHandler_11(),
	Options_t9A5FE5CDBE18224CE83CF2DAB5936706BBFAAD48::get_offset_of__referenceResolver_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5574 = { sizeof (ReflectionExtensions_t0A3499E3EBC2F4A91F107F819BE4D72C759AC6D0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5575 = { sizeof (StrictModeScope_t0361C75C19C33F43BBFA4EB68CA32044589A43A4), -1, 0, sizeof(StrictModeScope_t0361C75C19C33F43BBFA4EB68CA32044589A43A4_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable5575[4] = 
{
	StrictModeScope_t0361C75C19C33F43BBFA4EB68CA32044589A43A4::get_offset_of__strict_0(),
	StrictModeScope_t0361C75C19C33F43BBFA4EB68CA32044589A43A4::get_offset_of__force_1(),
	StrictModeScope_t0361C75C19C33F43BBFA4EB68CA32044589A43A4::get_offset_of__forcedRefCount_2(),
	StrictModeScope_t0361C75C19C33F43BBFA4EB68CA32044589A43A4_ThreadStaticFields::get_offset_of__refCount_3() | THREAD_LOCAL_STATIC_MASK,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5576 = { sizeof (Arguments_t99C4B1FF8199EF05F80BBEC1627767ACF3225AAE), -1, sizeof(Arguments_t99C4B1FF8199EF05F80BBEC1627767ACF3225AAE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5576[1] = 
{
	Arguments_t99C4B1FF8199EF05F80BBEC1627767ACF3225AAE_StaticFields::get_offset_of_Empty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5577 = { sizeof (CallStackElement_tCA2B7D32517A783A13494E5F7723DD76A656A762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5577[3] = 
{
	CallStackElement_tCA2B7D32517A783A13494E5F7723DD76A656A762::get_offset_of__shortDescription_0(),
	CallStackElement_tCA2B7D32517A783A13494E5F7723DD76A656A762::get_offset_of_U3CCallExpressionU3Ek__BackingField_1(),
	CallStackElement_tCA2B7D32517A783A13494E5F7723DD76A656A762::get_offset_of_U3CFunctionU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5578 = { sizeof (Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74), -1, sizeof(Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5578[9] = 
{
	Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74_StaticFields::get_offset_of_Normal_0(),
	Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74_StaticFields::get_offset_of_Break_1(),
	Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74_StaticFields::get_offset_of_Continue_2(),
	Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74_StaticFields::get_offset_of_Return_3(),
	Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74_StaticFields::get_offset_of_Throw_4(),
	Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74::get_offset_of_U3CTypeU3Ek__BackingField_5(),
	Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74::get_offset_of_U3CValueU3Ek__BackingField_6(),
	Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74::get_offset_of_U3CIdentifierU3Ek__BackingField_7(),
	Completion_t59B39A7DE285AF3FA5456B76364F3C6B9EC85B74::get_offset_of_U3CLocationU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5579 = { sizeof (ExpressionInterpreter_tD102C31F31458F6E6EDF49CFD7A7684CE208DF1F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5579[1] = 
{
	ExpressionInterpreter_tD102C31F31458F6E6EDF49CFD7A7684CE208DF1F::get_offset_of__engine_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5580 = { sizeof (U3CU3Ec_tB1F01E2AF2CF25F9D136A94A6AD008FC4193D541), -1, sizeof(U3CU3Ec_tB1F01E2AF2CF25F9D136A94A6AD008FC4193D541_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5580[2] = 
{
	U3CU3Ec_tB1F01E2AF2CF25F9D136A94A6AD008FC4193D541_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tB1F01E2AF2CF25F9D136A94A6AD008FC4193D541_StaticFields::get_offset_of_U3CU3E9__17_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5581 = { sizeof (JavaScriptException_tECA7359FC824B4EC27229DB2AEE05BB8C1488990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5581[3] = 
{
	JavaScriptException_tECA7359FC824B4EC27229DB2AEE05BB8C1488990::get_offset_of__errorObject_17(),
	JavaScriptException_tECA7359FC824B4EC27229DB2AEE05BB8C1488990::get_offset_of__callStack_18(),
	JavaScriptException_tECA7359FC824B4EC27229DB2AEE05BB8C1488990::get_offset_of_U3CLocationU3Ek__BackingField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5582 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5582[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5583 = { sizeof (RecursionDepthOverflowException_t4469F4DD10F8CD0B9097762F4BCF7817C36EEAA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5583[2] = 
{
	RecursionDepthOverflowException_t4469F4DD10F8CD0B9097762F4BCF7817C36EEAA1::get_offset_of_U3CCallChainU3Ek__BackingField_17(),
	RecursionDepthOverflowException_t4469F4DD10F8CD0B9097762F4BCF7817C36EEAA1::get_offset_of_U3CCallExpressionReferenceU3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5584 = { sizeof (StatementInterpreter_t09AE267789508AD9BC3E14EAFE8262642B534B44), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5584[1] = 
{
	StatementInterpreter_t09AE267789508AD9BC3E14EAFE8262642B534B44::get_offset_of__engine_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5585 = { sizeof (StatementsCountOverflowException_tB6AB2AAE8C222089DA11059B50CCEBF16760849D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5586 = { sizeof (Types_tE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5586[8] = 
{
	Types_tE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5587 = { sizeof (TypeConverter_tE6FEF1D028AF98204D37D7FED4E148C137AA0621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5588 = { sizeof (U3CU3Ec__DisplayClass12_0_t47AD677AC45EF44A6929008C6F40F9BA8FE33C19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5588[1] = 
{
	U3CU3Ec__DisplayClass12_0_t47AD677AC45EF44A6929008C6F40F9BA8FE33C19::get_offset_of_arguments_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5589 = { sizeof (U3CU3Ec_tCF159A02D6FF7D7C726DAA26C516157A941E0008), -1, sizeof(U3CU3Ec_tCF159A02D6FF7D7C726DAA26C516157A941E0008_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5589[2] = 
{
	U3CU3Ec_tCF159A02D6FF7D7C726DAA26C516157A941E0008_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tCF159A02D6FF7D7C726DAA26C516157A941E0008_StaticFields::get_offset_of_U3CU3E9__12_1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5590 = { sizeof (U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5590[11] = 
{
	U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872::get_offset_of_U3CU3E1__state_0(),
	U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872::get_offset_of_U3CU3E2__current_1(),
	U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872::get_offset_of_arguments_3(),
	U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872::get_offset_of_U3CU3E3__arguments_4(),
	U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872::get_offset_of_methods_5(),
	U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872::get_offset_of_U3CU3E3__methods_6(),
	U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872::get_offset_of_U3CU3E8__1_7(),
	U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872::get_offset_of_U3CobjectArgumentsU3E5__2_8(),
	U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872::get_offset_of_U3CU3E7__wrap1_9(),
	U3CFindBestMatchU3Ed__12_t6DCCBA58159B71022BAC8133FC1BA608E4272872::get_offset_of_U3CU3E7__wrap2_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5591 = { sizeof (Reference_t9C84C563E43DCAACF816A789C6561EE27CD96BA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5591[3] = 
{
	Reference_t9C84C563E43DCAACF816A789C6561EE27CD96BA1::get_offset_of__baseValue_0(),
	Reference_t9C84C563E43DCAACF816A789C6561EE27CD96BA1::get_offset_of__name_1(),
	Reference_t9C84C563E43DCAACF816A789C6561EE27CD96BA1::get_offset_of__strict_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5592 = { sizeof (ClrFunctionInstance_t2F555835B5CF10010FA57E6641B4DD2241B2BAF5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5592[1] = 
{
	ClrFunctionInstance_t2F555835B5CF10010FA57E6641B4DD2241B2BAF5::get_offset_of__func_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5593 = { sizeof (DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087), -1, sizeof(DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5593[6] = 
{
	DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087::get_offset_of__engine_0(),
	DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087_StaticFields::get_offset_of__knownConversions_1(),
	DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087_StaticFields::get_offset_of__lockObject_2(),
	DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087_StaticFields::get_offset_of_convertChangeType_3(),
	DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087_StaticFields::get_offset_of_jsValueFromObject_4(),
	DefaultTypeConverter_tC48984CBA042A943B40BAE5960F493DA33962087_StaticFields::get_offset_of_jsValueToObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5594 = { sizeof (U3CU3Ec__DisplayClass7_0_t17D82877DC876D51F55BE1E79A2B1470A8C356BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5594[1] = 
{
	U3CU3Ec__DisplayClass7_0_t17D82877DC876D51F55BE1E79A2B1470A8C356BB::get_offset_of_function_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5595 = { sizeof (U3CU3Ec__DisplayClass7_1_tB5C451BAEB026731D23627D50F68724297E6577B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5595[2] = 
{
	U3CU3Ec__DisplayClass7_1_tB5C451BAEB026731D23627D50F68724297E6577B::get_offset_of_targetElementType_0(),
	U3CU3Ec__DisplayClass7_1_tB5C451BAEB026731D23627D50F68724297E6577B::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5596 = { sizeof (U3CU3Ec__DisplayClass7_2_t660F155E66573E853DEED2D23186154AD85FA4D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5596[2] = 
{
	U3CU3Ec__DisplayClass7_2_t660F155E66573E853DEED2D23186154AD85FA4D1::get_offset_of_formatProvider_0(),
	U3CU3Ec__DisplayClass7_2_t660F155E66573E853DEED2D23186154AD85FA4D1::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5597 = { sizeof (DelegateWrapper_tAB370D5DD315553C91B6D945B3A6B3CC6C78F75A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5597[1] = 
{
	DelegateWrapper_tAB370D5DD315553C91B6D945B3A6B3CC6C78F75A::get_offset_of__d_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5598 = { sizeof (U3CU3Ec_t75BF8A7C7D2A6B60F55126EA3553F5D68D449F28), -1, sizeof(U3CU3Ec_t75BF8A7C7D2A6B60F55126EA3553F5D68D449F28_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5598[2] = 
{
	U3CU3Ec_t75BF8A7C7D2A6B60F55126EA3553F5D68D449F28_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t75BF8A7C7D2A6B60F55126EA3553F5D68D449F28_StaticFields::get_offset_of_U3CU3E9__2_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5599 = { sizeof (GetterFunctionInstance_t0BD07D8EC478683CD4C2332C96C91B09F6AF6FE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5599[1] = 
{
	GetterFunctionInstance_t0BD07D8EC478683CD4C2332C96C91B09F6AF6FE6::get_offset_of__getter_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
