﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Assets._Project.Scripts.Common.PlaneDetection.PlatformDependentPlaneDetector
struct PlatformDependentPlaneDetector_tA059BB399BF4739A6CA16E50E3B2581C47FB6C1F;
// Assets._Project.Scripts.Common.Scripting.API.ICustomMenu
struct ICustomMenu_tF20A4D033B5A006D930F4CE89B5DC6050A7ED7E2;
// Assets._Project.Scripts.Common.Scripting.ErrorHandling.ScriptErrorHandler
struct ScriptErrorHandler_t86C54D9B0DA7092755777EFF56D5D23039FEA924;
// Assets._Project.Scripts.Common.Scripting.Mix.ScriptEventHandler
struct ScriptEventHandler_t6818D0F175ECC12B599C5FE2A33346CECCD49AC7;
// Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler
struct ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC;
// Assets._Project.Scripts.Common.Scripting.PlatformFeatures.Features
struct Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10;
// BarcodeScanner.Parser.ZXingParser
struct ZXingParser_t7B9AC8160C3A1565FA96F8714D30B9C7E852DE26;
// Jint.Engine
struct Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D;
// MPAR.Common.AssetLoader.AbstractAssetLoader
struct AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2;
// MPAR.Common.AssetLoader.IPlatformDependentAssetLoader
struct IPlatformDependentAssetLoader_t4191A469DA1A9ECC9F8F2F7E37DAF82A3B2DBB2E;
// MPAR.Common.AssetLoader.ImageAsset
struct ImageAsset_t828547FE133CA2715FCF37D6CEFAE756ABA4A7BE;
// MPAR.Common.AssetLoader.ModelAsset
struct ModelAsset_t32D8CDA195AA610C8ADB2DD616971E00BE14155F;
// MPAR.Common.AssetLoader.ScriptAsset
struct ScriptAsset_t5634BE2EB46C68F69487B801410DDAB631B6E994;
// MPAR.Common.AssetLoader.VideoOptions
struct VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662;
// MPAR.Common.Collections.ObjectCollection
struct ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB;
// MPAR.Common.GameObjects.Factories.ExhibitBoxFactory
struct ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13;
// MPAR.Common.GameObjects.Factories.InScriptMenuFactory
struct InScriptMenuFactory_tE28604EF9A7748F0689F833E699CABF93F4DA7B9;
// MPAR.Common.GameObjects.Factories.PortableMenuFactory
struct PortableMenuFactory_tAA97D5D2F11B3C26CBC04242D63940BFFBE0070D;
// MPAR.Common.GameObjects.Factories.PortableMenuItemFactory
struct PortableMenuItemFactory_t1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D;
// MPAR.Common.Input.Utility.GestureManager
struct GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909;
// MPAR.Common.Persistence.CacheManager
struct CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B;
// MPAR.Common.Persistence.ISaveLoadManager
struct ISaveLoadManager_tFCCBBA0EEB7591B57A7AA74ED9EF18A0073086B5;
// MPAR.Common.Scripting.API.Interfaces.Menu/ScriptMenu
struct ScriptMenu_t8D4D728B65EE28F23D1B655B6840216521F33B27;
// MPAR.Common.Scripting.API.Objects.MixiplyObject
struct MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B;
// MPAR.Common.Scripting.Collidable
struct Collidable_t2DD800E21B45929FBAEBDCE305BB8E65236D4854;
// MPAR.Common.Scripting.LoadedAssets
struct LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A;
// MPAR.Common.Scripting.Loader.IProjectLoader
struct IProjectLoader_tE3E1F27AD151FD353C5763F8F397D6619E53A4E3;
// MPAR.Common.Scripting.Loader.ScreenshotQRCodeProjectLoader
struct ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386;
// MPAR.Common.Scripting.MixModel[]
struct MixModelU5BU5D_tE8363090940F064FC46E381BFAC1A05A892EFF86;
// MPAR.Common.Scripting.Script
struct Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC;
// MPAR.Common.Scripting.ScriptLoader
struct ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2;
// MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass38_0
struct U3CU3Ec__DisplayClass38_0_t5F706BF913F1C9975BAA3B83FE5051263066BAFD;
// MPAR.Common.Scripting.ScriptManager
struct ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C;
// MPAR.Common.Scripting.Web.WebRequestBuilder
struct WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7;
// MPAR.Common.Scripting.Web.WebRequestBuilder/<>c__DisplayClass12_0
struct U3CU3Ec__DisplayClass12_0_tEFB9F75E1D5E9FFE7001EB94B891A7FD164DFF1F;
// MPAR.Common.UI.IPlatformDependentScriptMenu
struct IPlatformDependentScriptMenu_tBBB6BBD89701461F2C659808C67378235B3D1AA0;
// MPAR.Common.UI.MenuManager
struct MenuManager_t03B7ED0689D55E85C85D1E0E239C0CF9AA5E93D6;
// MPAR.Common.UI.PortableMenu.PortableMenuItem
struct PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867;
// Player
struct Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873;
// RenderHeads.Media.AVProMovieCapture.CaptureFromScreen
struct CaptureFromScreen_tEB468CAE6962D15FD6A3F718B595BDF64D70007D;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<MPAR.Common.AssetLoader.IAsset>
struct Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22;
// System.Action`1<MPAR.Common.Collections.ObjectCollection>
struct Action_1_t0F44F2F0F46EB58693C1552993EA892B40BEC161;
// System.Action`1<MPAR.Common.Scripting.API.Objects.MixiplyObject>
struct Action_1_tBCC67358887F2268280F2E2041859B030AC4FBF8;
// System.Action`1<MPAR.Common.Scripting.Script>
struct Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`1<UnityEngine.AssetBundle>
struct Action_1_tA7D073EEC8F959478A8165E11882A6BD4906D9B5;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C;
// System.Collections.Generic.Dictionary`2<System.String,MPAR.Common.AssetLoader.IAsset>
struct Dictionary_2_t495588211E81426E685BBD6CC05F4597E607BA76;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AssetBundle>
struct Dictionary_2_t9119D8967869E96A9D001B5D96F65C462E234BEA;
// System.Collections.Generic.List`1<Jint.Native.JsValue>
struct List_1_t893ABB43ACE9473D58A8C90EE4E765A65D92EBFA;
// System.Collections.Generic.List`1<MPAR.Common.Collections.CollectionNode>
struct List_1_t311D38FDCB31B0F0591BB5DD3DEA76A5BC1639F7;
// System.Collections.Generic.List`1<MPAR.Common.Collections.ObjectCollectionDynamic/CollectionNodeDynamic>
struct List_1_t33ED8D1DCBA5E6C17BA48FF44566430BF4C85476;
// System.Collections.Generic.List`1<MPAR.Common.Scripting.GalleryMixModel>
struct List_1_tEBE348C8DBDB33364A789FE6FE5089BA27BC084C;
// System.Collections.Generic.List`1<MPAR.Common.Scripting.Script>
struct List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1;
// System.Collections.Generic.List`1<System.Func`3<Jint.Native.JsValue,Jint.Native.JsValue[],Jint.Native.JsValue>>
struct List_1_t76817557DA51974CB26A8BD0DD371A638D54DA52;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Comparison`1<MPAR.Common.Collections.CollectionNode>
struct Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F;
// System.Converter`2<UnityEngine.Animation,MPAR.Common.Scripting.API.Assets.Animation>
struct Converter_2_t2E69D7C5FB71D7D52D508F977FC06A551BD352CA;
// System.Func`2<System.Char,System.Boolean>
struct Func_2_tD15D8591FF52D18217AAF8FE723938708D4B1B28;
// System.Func`2<System.Reflection.ParameterInfo,System.Type>
struct Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1;
// System.Func`2<System.String,System.Collections.IEnumerator>
struct Func_2_t475CC109419A4B3EE9FAA114D232178BA32C6841;
// System.Func`2<UnityEngine.MonoBehaviour,System.Boolean>
struct Func_2_t5A0E7E85DFC2CF97DFE0079B465DB9214F51084C;
// System.Func`2<UnityEngine.Transform,System.Boolean>
struct Func_2_t0F30AD8B62D43CC2E76F1EB584331B7BF4B28336;
// System.Func`3<Jint.Native.JsValue,Jint.Native.JsValue[],Jint.Native.JsValue>
struct Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039;
// System.Func`3<System.String,System.String,System.Collections.IEnumerator>
struct Func_3_t5D19D2F5D175E4953F2471BB6690A6BF7579C653;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Runtime.CompilerServices.CallSite`1<System.Func`3<System.Runtime.CompilerServices.CallSite,System.Object,System.Boolean>>
struct CallSite_1_tA51C476901067D91F355F48DD7AD56997B266A56;
// System.Runtime.CompilerServices.CallSite`1<System.Func`3<System.Runtime.CompilerServices.CallSite,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>>>
struct CallSite_1_tFE3733C1C85F359D9BFDE52BFC1DF0904A2082BD;
// System.Runtime.CompilerServices.CallSite`1<System.Func`4<System.Runtime.CompilerServices.CallSite,System.Object,System.Object,System.Object>>
struct CallSite_1_tC48D5A99F98099B2A9523EA7D849598158F3099C;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Animation
struct Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Transform[]
struct TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804;
// UnityEngine.WWW
struct WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664;
// UnityGLTF.GLTFComponent
struct GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392;
// Zenject.DiContainer
struct DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CLOADASSETBUNDLEFROMURLCOROUTINEU3ED__21_TB11626F401D47B0E27C21F7131B0EF45BFDA8139_H
#define U3CLOADASSETBUNDLEFROMURLCOROUTINEU3ED__21_TB11626F401D47B0E27C21F7131B0EF45BFDA8139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetBundleFromUrlCoroutine>d__21
struct  U3CLoadAssetBundleFromUrlCoroutineU3Ed__21_tB11626F401D47B0E27C21F7131B0EF45BFDA8139  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetBundleFromUrlCoroutine>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetBundleFromUrlCoroutine>d__21::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetBundleFromUrlCoroutine>d__21::url
	String_t* ___url_2;
	// System.Action`1<UnityEngine.AssetBundle> MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetBundleFromUrlCoroutine>d__21::callback
	Action_1_tA7D073EEC8F959478A8165E11882A6BD4906D9B5 * ___callback_3;
	// System.Int32 MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetBundleFromUrlCoroutine>d__21::<version>5__2
	int32_t ___U3CversionU3E5__2_4;
	// UnityEngine.WWW MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetBundleFromUrlCoroutine>d__21::<www>5__3
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleFromUrlCoroutineU3Ed__21_tB11626F401D47B0E27C21F7131B0EF45BFDA8139, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleFromUrlCoroutineU3Ed__21_tB11626F401D47B0E27C21F7131B0EF45BFDA8139, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleFromUrlCoroutineU3Ed__21_tB11626F401D47B0E27C21F7131B0EF45BFDA8139, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((&___url_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleFromUrlCoroutineU3Ed__21_tB11626F401D47B0E27C21F7131B0EF45BFDA8139, ___callback_3)); }
	inline Action_1_tA7D073EEC8F959478A8165E11882A6BD4906D9B5 * get_callback_3() const { return ___callback_3; }
	inline Action_1_tA7D073EEC8F959478A8165E11882A6BD4906D9B5 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(Action_1_tA7D073EEC8F959478A8165E11882A6BD4906D9B5 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_U3CversionU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleFromUrlCoroutineU3Ed__21_tB11626F401D47B0E27C21F7131B0EF45BFDA8139, ___U3CversionU3E5__2_4)); }
	inline int32_t get_U3CversionU3E5__2_4() const { return ___U3CversionU3E5__2_4; }
	inline int32_t* get_address_of_U3CversionU3E5__2_4() { return &___U3CversionU3E5__2_4; }
	inline void set_U3CversionU3E5__2_4(int32_t value)
	{
		___U3CversionU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleFromUrlCoroutineU3Ed__21_tB11626F401D47B0E27C21F7131B0EF45BFDA8139, ___U3CwwwU3E5__3_5)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__3_5() const { return ___U3CwwwU3E5__3_5; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__3_5() { return &___U3CwwwU3E5__3_5; }
	inline void set_U3CwwwU3E5__3_5(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__3_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASSETBUNDLEFROMURLCOROUTINEU3ED__21_TB11626F401D47B0E27C21F7131B0EF45BFDA8139_H
#ifndef U3CLOADASSETFROMURLCOROUTINEU3ED__16_TD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9_H
#define U3CLOADASSETFROMURLCOROUTINEU3ED__16_TD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetFromUrlCoroutine>d__16
struct  U3CLoadAssetFromUrlCoroutineU3Ed__16_tD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetFromUrlCoroutine>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetFromUrlCoroutine>d__16::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.AssetLoader.AbstractAssetLoader MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetFromUrlCoroutine>d__16::<>4__this
	AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2 * ___U3CU3E4__this_2;
	// System.String MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetFromUrlCoroutine>d__16::url
	String_t* ___url_3;
	// System.String MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetFromUrlCoroutine>d__16::extension
	String_t* ___extension_4;
	// System.Action`1<MPAR.Common.AssetLoader.IAsset> MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetFromUrlCoroutine>d__16::callback
	Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * ___callback_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadAssetFromUrlCoroutineU3Ed__16_tD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadAssetFromUrlCoroutineU3Ed__16_tD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadAssetFromUrlCoroutineU3Ed__16_tD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9, ___U3CU3E4__this_2)); }
	inline AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(U3CLoadAssetFromUrlCoroutineU3Ed__16_tD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((&___url_3), value);
	}

	inline static int32_t get_offset_of_extension_4() { return static_cast<int32_t>(offsetof(U3CLoadAssetFromUrlCoroutineU3Ed__16_tD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9, ___extension_4)); }
	inline String_t* get_extension_4() const { return ___extension_4; }
	inline String_t** get_address_of_extension_4() { return &___extension_4; }
	inline void set_extension_4(String_t* value)
	{
		___extension_4 = value;
		Il2CppCodeGenWriteBarrier((&___extension_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CLoadAssetFromUrlCoroutineU3Ed__16_tD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9, ___callback_5)); }
	inline Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * get_callback_5() const { return ___callback_5; }
	inline Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASSETFROMURLCOROUTINEU3ED__16_TD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9_H
#ifndef U3CLOADASSETFROMURLCOROUTINEU3ED__17_TBE7673059893621530E3AF59005C7C101AB97C74_H
#define U3CLOADASSETFROMURLCOROUTINEU3ED__17_TBE7673059893621530E3AF59005C7C101AB97C74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetFromUrlCoroutine>d__17
struct  U3CLoadAssetFromUrlCoroutineU3Ed__17_tBE7673059893621530E3AF59005C7C101AB97C74  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetFromUrlCoroutine>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetFromUrlCoroutine>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.AssetLoader.AbstractAssetLoader MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetFromUrlCoroutine>d__17::<>4__this
	AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2 * ___U3CU3E4__this_2;
	// System.String MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetFromUrlCoroutine>d__17::url
	String_t* ___url_3;
	// System.Action`1<MPAR.Common.AssetLoader.IAsset> MPAR.Common.AssetLoader.AbstractAssetLoader/<LoadAssetFromUrlCoroutine>d__17::callback
	Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * ___callback_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadAssetFromUrlCoroutineU3Ed__17_tBE7673059893621530E3AF59005C7C101AB97C74, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadAssetFromUrlCoroutineU3Ed__17_tBE7673059893621530E3AF59005C7C101AB97C74, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadAssetFromUrlCoroutineU3Ed__17_tBE7673059893621530E3AF59005C7C101AB97C74, ___U3CU3E4__this_2)); }
	inline AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(U3CLoadAssetFromUrlCoroutineU3Ed__17_tBE7673059893621530E3AF59005C7C101AB97C74, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((&___url_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CLoadAssetFromUrlCoroutineU3Ed__17_tBE7673059893621530E3AF59005C7C101AB97C74, ___callback_4)); }
	inline Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * get_callback_4() const { return ___callback_4; }
	inline Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASSETFROMURLCOROUTINEU3ED__17_TBE7673059893621530E3AF59005C7C101AB97C74_H
#ifndef IMAGEASSET_T828547FE133CA2715FCF37D6CEFAE756ABA4A7BE_H
#define IMAGEASSET_T828547FE133CA2715FCF37D6CEFAE756ABA4A7BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.ImageAsset
struct  ImageAsset_t828547FE133CA2715FCF37D6CEFAE756ABA4A7BE  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D MPAR.Common.AssetLoader.ImageAsset::<Image>k__BackingField
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___U3CImageU3Ek__BackingField_0;
	// System.String MPAR.Common.AssetLoader.ImageAsset::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CImageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ImageAsset_t828547FE133CA2715FCF37D6CEFAE756ABA4A7BE, ___U3CImageU3Ek__BackingField_0)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_U3CImageU3Ek__BackingField_0() const { return ___U3CImageU3Ek__BackingField_0; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_U3CImageU3Ek__BackingField_0() { return &___U3CImageU3Ek__BackingField_0; }
	inline void set_U3CImageU3Ek__BackingField_0(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___U3CImageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ImageAsset_t828547FE133CA2715FCF37D6CEFAE756ABA4A7BE, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEASSET_T828547FE133CA2715FCF37D6CEFAE756ABA4A7BE_H
#ifndef U3CLOADU3ED__8_T4E60211E9CF00CA549D823E3FFBBD86F5F92FA94_H
#define U3CLOADU3ED__8_T4E60211E9CF00CA549D823E3FFBBD86F5F92FA94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.ImageAsset/<Load>d__8
struct  U3CLoadU3Ed__8_t4E60211E9CF00CA549D823E3FFBBD86F5F92FA94  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.AssetLoader.ImageAsset/<Load>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.AssetLoader.ImageAsset/<Load>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.AssetLoader.ImageAsset MPAR.Common.AssetLoader.ImageAsset/<Load>d__8::<>4__this
	ImageAsset_t828547FE133CA2715FCF37D6CEFAE756ABA4A7BE * ___U3CU3E4__this_2;
	// System.Byte[] MPAR.Common.AssetLoader.ImageAsset/<Load>d__8::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_3;
	// System.Action`1<MPAR.Common.AssetLoader.IAsset> MPAR.Common.AssetLoader.ImageAsset/<Load>d__8::callback
	Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * ___callback_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__8_t4E60211E9CF00CA549D823E3FFBBD86F5F92FA94, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__8_t4E60211E9CF00CA549D823E3FFBBD86F5F92FA94, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__8_t4E60211E9CF00CA549D823E3FFBBD86F5F92FA94, ___U3CU3E4__this_2)); }
	inline ImageAsset_t828547FE133CA2715FCF37D6CEFAE756ABA4A7BE * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ImageAsset_t828547FE133CA2715FCF37D6CEFAE756ABA4A7BE ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ImageAsset_t828547FE133CA2715FCF37D6CEFAE756ABA4A7BE * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_bytes_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__8_t4E60211E9CF00CA549D823E3FFBBD86F5F92FA94, ___bytes_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_3() const { return ___bytes_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_3() { return &___bytes_3; }
	inline void set_bytes_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_3 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__8_t4E60211E9CF00CA549D823E3FFBBD86F5F92FA94, ___callback_4)); }
	inline Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * get_callback_4() const { return ___callback_4; }
	inline Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3ED__8_T4E60211E9CF00CA549D823E3FFBBD86F5F92FA94_H
#ifndef MODELASSET_T32D8CDA195AA610C8ADB2DD616971E00BE14155F_H
#define MODELASSET_T32D8CDA195AA610C8ADB2DD616971E00BE14155F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.ModelAsset
struct  ModelAsset_t32D8CDA195AA610C8ADB2DD616971E00BE14155F  : public RuntimeObject
{
public:
	// UnityEngine.GameObject MPAR.Common.AssetLoader.ModelAsset::<Object>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CObjectU3Ek__BackingField_0;
	// System.String MPAR.Common.AssetLoader.ModelAsset::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// System.String MPAR.Common.AssetLoader.ModelAsset::<Url>k__BackingField
	String_t* ___U3CUrlU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CObjectU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ModelAsset_t32D8CDA195AA610C8ADB2DD616971E00BE14155F, ___U3CObjectU3Ek__BackingField_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CObjectU3Ek__BackingField_0() const { return ___U3CObjectU3Ek__BackingField_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CObjectU3Ek__BackingField_0() { return &___U3CObjectU3Ek__BackingField_0; }
	inline void set_U3CObjectU3Ek__BackingField_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CObjectU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ModelAsset_t32D8CDA195AA610C8ADB2DD616971E00BE14155F, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CUrlU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ModelAsset_t32D8CDA195AA610C8ADB2DD616971E00BE14155F, ___U3CUrlU3Ek__BackingField_2)); }
	inline String_t* get_U3CUrlU3Ek__BackingField_2() const { return ___U3CUrlU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CUrlU3Ek__BackingField_2() { return &___U3CUrlU3Ek__BackingField_2; }
	inline void set_U3CUrlU3Ek__BackingField_2(String_t* value)
	{
		___U3CUrlU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUrlU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELASSET_T32D8CDA195AA610C8ADB2DD616971E00BE14155F_H
#ifndef U3CLOADU3ED__13_TDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914_H
#define U3CLOADU3ED__13_TDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.ModelAsset/<Load>d__13
struct  U3CLoadU3Ed__13_tDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.AssetLoader.ModelAsset/<Load>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.AssetLoader.ModelAsset/<Load>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.AssetLoader.ModelAsset MPAR.Common.AssetLoader.ModelAsset/<Load>d__13::<>4__this
	ModelAsset_t32D8CDA195AA610C8ADB2DD616971E00BE14155F * ___U3CU3E4__this_2;
	// System.String MPAR.Common.AssetLoader.ModelAsset/<Load>d__13::originalUrl
	String_t* ___originalUrl_3;
	// System.Action`1<MPAR.Common.AssetLoader.IAsset> MPAR.Common.AssetLoader.ModelAsset/<Load>d__13::callback
	Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * ___callback_4;
	// UnityGLTF.GLTFComponent MPAR.Common.AssetLoader.ModelAsset/<Load>d__13::<gltf>5__2
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 * ___U3CgltfU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__13_tDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__13_tDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__13_tDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914, ___U3CU3E4__this_2)); }
	inline ModelAsset_t32D8CDA195AA610C8ADB2DD616971E00BE14155F * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ModelAsset_t32D8CDA195AA610C8ADB2DD616971E00BE14155F ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ModelAsset_t32D8CDA195AA610C8ADB2DD616971E00BE14155F * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_originalUrl_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__13_tDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914, ___originalUrl_3)); }
	inline String_t* get_originalUrl_3() const { return ___originalUrl_3; }
	inline String_t** get_address_of_originalUrl_3() { return &___originalUrl_3; }
	inline void set_originalUrl_3(String_t* value)
	{
		___originalUrl_3 = value;
		Il2CppCodeGenWriteBarrier((&___originalUrl_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__13_tDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914, ___callback_4)); }
	inline Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * get_callback_4() const { return ___callback_4; }
	inline Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U3CgltfU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__13_tDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914, ___U3CgltfU3E5__2_5)); }
	inline GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 * get_U3CgltfU3E5__2_5() const { return ___U3CgltfU3E5__2_5; }
	inline GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 ** get_address_of_U3CgltfU3E5__2_5() { return &___U3CgltfU3E5__2_5; }
	inline void set_U3CgltfU3E5__2_5(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 * value)
	{
		___U3CgltfU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgltfU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3ED__13_TDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914_H
#ifndef SCRIPTASSET_T5634BE2EB46C68F69487B801410DDAB631B6E994_H
#define SCRIPTASSET_T5634BE2EB46C68F69487B801410DDAB631B6E994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.ScriptAsset
struct  ScriptAsset_t5634BE2EB46C68F69487B801410DDAB631B6E994  : public RuntimeObject
{
public:
	// System.String MPAR.Common.AssetLoader.ScriptAsset::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_0;
	// System.String MPAR.Common.AssetLoader.ScriptAsset::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ScriptAsset_t5634BE2EB46C68F69487B801410DDAB631B6E994, ___U3CTextU3Ek__BackingField_0)); }
	inline String_t* get_U3CTextU3Ek__BackingField_0() const { return ___U3CTextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_0() { return &___U3CTextU3Ek__BackingField_0; }
	inline void set_U3CTextU3Ek__BackingField_0(String_t* value)
	{
		___U3CTextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ScriptAsset_t5634BE2EB46C68F69487B801410DDAB631B6E994, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTASSET_T5634BE2EB46C68F69487B801410DDAB631B6E994_H
#ifndef U3CLOADU3ED__8_T65DD09E32B008CA9BDBAC3A52362119AE6211186_H
#define U3CLOADU3ED__8_T65DD09E32B008CA9BDBAC3A52362119AE6211186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.ScriptAsset/<Load>d__8
struct  U3CLoadU3Ed__8_t65DD09E32B008CA9BDBAC3A52362119AE6211186  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.AssetLoader.ScriptAsset/<Load>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.AssetLoader.ScriptAsset/<Load>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.AssetLoader.ScriptAsset MPAR.Common.AssetLoader.ScriptAsset/<Load>d__8::<>4__this
	ScriptAsset_t5634BE2EB46C68F69487B801410DDAB631B6E994 * ___U3CU3E4__this_2;
	// System.Byte[] MPAR.Common.AssetLoader.ScriptAsset/<Load>d__8::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_3;
	// System.Action`1<MPAR.Common.AssetLoader.IAsset> MPAR.Common.AssetLoader.ScriptAsset/<Load>d__8::callback
	Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * ___callback_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__8_t65DD09E32B008CA9BDBAC3A52362119AE6211186, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__8_t65DD09E32B008CA9BDBAC3A52362119AE6211186, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__8_t65DD09E32B008CA9BDBAC3A52362119AE6211186, ___U3CU3E4__this_2)); }
	inline ScriptAsset_t5634BE2EB46C68F69487B801410DDAB631B6E994 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ScriptAsset_t5634BE2EB46C68F69487B801410DDAB631B6E994 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ScriptAsset_t5634BE2EB46C68F69487B801410DDAB631B6E994 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_bytes_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__8_t65DD09E32B008CA9BDBAC3A52362119AE6211186, ___bytes_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_3() const { return ___bytes_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_3() { return &___bytes_3; }
	inline void set_bytes_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_3 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__8_t65DD09E32B008CA9BDBAC3A52362119AE6211186, ___callback_4)); }
	inline Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * get_callback_4() const { return ___callback_4; }
	inline Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3ED__8_T65DD09E32B008CA9BDBAC3A52362119AE6211186_H
#ifndef U3CU3EC_T277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_H
#define U3CU3EC_T277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Collections.ObjectCollection/<>c
struct  U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_StaticFields
{
public:
	// MPAR.Common.Collections.ObjectCollection/<>c MPAR.Common.Collections.ObjectCollection/<>c::<>9
	U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3 * ___U3CU3E9_0;
	// System.Comparison`1<MPAR.Common.Collections.CollectionNode> MPAR.Common.Collections.ObjectCollection/<>c::<>9__22_0
	Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F * ___U3CU3E9__22_0_1;
	// System.Comparison`1<MPAR.Common.Collections.CollectionNode> MPAR.Common.Collections.ObjectCollection/<>c::<>9__22_1
	Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F * ___U3CU3E9__22_1_2;
	// System.Comparison`1<MPAR.Common.Collections.CollectionNode> MPAR.Common.Collections.ObjectCollection/<>c::<>9__22_2
	Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F * ___U3CU3E9__22_2_3;
	// System.Comparison`1<MPAR.Common.Collections.CollectionNode> MPAR.Common.Collections.ObjectCollection/<>c::<>9__22_3
	Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F * ___U3CU3E9__22_3_4;
	// System.Comparison`1<MPAR.Common.Collections.CollectionNode> MPAR.Common.Collections.ObjectCollection/<>c::<>9__28_0
	Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F * ___U3CU3E9__28_0_5;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_StaticFields, ___U3CU3E9__22_0_1)); }
	inline Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F * get_U3CU3E9__22_0_1() const { return ___U3CU3E9__22_0_1; }
	inline Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F ** get_address_of_U3CU3E9__22_0_1() { return &___U3CU3E9__22_0_1; }
	inline void set_U3CU3E9__22_0_1(Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F * value)
	{
		___U3CU3E9__22_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__22_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_StaticFields, ___U3CU3E9__22_1_2)); }
	inline Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F * get_U3CU3E9__22_1_2() const { return ___U3CU3E9__22_1_2; }
	inline Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F ** get_address_of_U3CU3E9__22_1_2() { return &___U3CU3E9__22_1_2; }
	inline void set_U3CU3E9__22_1_2(Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F * value)
	{
		___U3CU3E9__22_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__22_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_StaticFields, ___U3CU3E9__22_2_3)); }
	inline Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F * get_U3CU3E9__22_2_3() const { return ___U3CU3E9__22_2_3; }
	inline Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F ** get_address_of_U3CU3E9__22_2_3() { return &___U3CU3E9__22_2_3; }
	inline void set_U3CU3E9__22_2_3(Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F * value)
	{
		___U3CU3E9__22_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__22_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_StaticFields, ___U3CU3E9__22_3_4)); }
	inline Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F * get_U3CU3E9__22_3_4() const { return ___U3CU3E9__22_3_4; }
	inline Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F ** get_address_of_U3CU3E9__22_3_4() { return &___U3CU3E9__22_3_4; }
	inline void set_U3CU3E9__22_3_4(Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F * value)
	{
		___U3CU3E9__22_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__22_3_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__28_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_StaticFields, ___U3CU3E9__28_0_5)); }
	inline Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F * get_U3CU3E9__28_0_5() const { return ___U3CU3E9__28_0_5; }
	inline Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F ** get_address_of_U3CU3E9__28_0_5() { return &___U3CU3E9__28_0_5; }
	inline void set_U3CU3E9__28_0_5(Comparison_1_t5F77FCE926DF1C2F2C68C5FF0EA361B6CB2D622F * value)
	{
		___U3CU3E9__28_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__28_0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_H
#ifndef ANIMATION_TEEF97D5C57556A56B1C9CC07939CEC59A4DCB996_H
#define ANIMATION_TEEF97D5C57556A56B1C9CC07939CEC59A4DCB996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Assets.Animation
struct  Animation_tEEF97D5C57556A56B1C9CC07939CEC59A4DCB996  : public RuntimeObject
{
public:
	// UnityEngine.Animation MPAR.Common.Scripting.API.Assets.Animation::animation
	Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * ___animation_0;

public:
	inline static int32_t get_offset_of_animation_0() { return static_cast<int32_t>(offsetof(Animation_tEEF97D5C57556A56B1C9CC07939CEC59A4DCB996, ___animation_0)); }
	inline Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * get_animation_0() const { return ___animation_0; }
	inline Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C ** get_address_of_animation_0() { return &___animation_0; }
	inline void set_animation_0(Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * value)
	{
		___animation_0 = value;
		Il2CppCodeGenWriteBarrier((&___animation_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATION_TEEF97D5C57556A56B1C9CC07939CEC59A4DCB996_H
#ifndef ASSET_T18FA838BF8BBDFED83FE107A932A431A932C80A3_H
#define ASSET_T18FA838BF8BBDFED83FE107A932A431A932C80A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Assets.Asset
struct  Asset_t18FA838BF8BBDFED83FE107A932A431A932C80A3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSET_T18FA838BF8BBDFED83FE107A932A431A932C80A3_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T378B5A52952B048048E8A054ACEF332B52D63D5E_H
#define U3CU3EC__DISPLAYCLASS4_0_T378B5A52952B048048E8A054ACEF332B52D63D5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Assets.Asset/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t378B5A52952B048048E8A054ACEF332B52D63D5E  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Scripting.API.Assets.Asset/<>c__DisplayClass4_0::bundleName
	String_t* ___bundleName_0;

public:
	inline static int32_t get_offset_of_bundleName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t378B5A52952B048048E8A054ACEF332B52D63D5E, ___bundleName_0)); }
	inline String_t* get_bundleName_0() const { return ___bundleName_0; }
	inline String_t** get_address_of_bundleName_0() { return &___bundleName_0; }
	inline void set_bundleName_0(String_t* value)
	{
		___bundleName_0 = value;
		Il2CppCodeGenWriteBarrier((&___bundleName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T378B5A52952B048048E8A054ACEF332B52D63D5E_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T9EDB9BB83387F5A86495955953B05FD0BECFF001_H
#define U3CU3EC__DISPLAYCLASS5_0_T9EDB9BB83387F5A86495955953B05FD0BECFF001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Assets.Asset/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t9EDB9BB83387F5A86495955953B05FD0BECFF001  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Scripting.API.Assets.Asset/<>c__DisplayClass5_0::assetName
	String_t* ___assetName_0;

public:
	inline static int32_t get_offset_of_assetName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t9EDB9BB83387F5A86495955953B05FD0BECFF001, ___assetName_0)); }
	inline String_t* get_assetName_0() const { return ___assetName_0; }
	inline String_t** get_address_of_assetName_0() { return &___assetName_0; }
	inline void set_assetName_0(String_t* value)
	{
		___assetName_0 = value;
		Il2CppCodeGenWriteBarrier((&___assetName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T9EDB9BB83387F5A86495955953B05FD0BECFF001_H
#ifndef AUDIO_T3A5B96BE1E05EB0E3AAA751C0946A2E7D9F92FA8_H
#define AUDIO_T3A5B96BE1E05EB0E3AAA751C0946A2E7D9F92FA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Assets.Audio
struct  Audio_t3A5B96BE1E05EB0E3AAA751C0946A2E7D9F92FA8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIO_T3A5B96BE1E05EB0E3AAA751C0946A2E7D9F92FA8_H
#ifndef EFFECT_TDBD3EEB1CF1190DC08D718AC00D8E6996D6ED7AD_H
#define EFFECT_TDBD3EEB1CF1190DC08D718AC00D8E6996D6ED7AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Assets.Effect
struct  Effect_tDBD3EEB1CF1190DC08D718AC00D8E6996D6ED7AD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECT_TDBD3EEB1CF1190DC08D718AC00D8E6996D6ED7AD_H
#ifndef VIDEO_T13A3701ED2FC30161B825D8EF24C810D9C908AE6_H
#define VIDEO_T13A3701ED2FC30161B825D8EF24C810D9C908AE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Assets.Video
struct  Video_t13A3701ED2FC30161B825D8EF24C810D9C908AE6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEO_T13A3701ED2FC30161B825D8EF24C810D9C908AE6_H
#ifndef EVENT_T5328F3E98A257F496099549407BA78C4DE6F765E_H
#define EVENT_T5328F3E98A257F496099549407BA78C4DE6F765E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Events.Event
struct  Event_t5328F3E98A257F496099549407BA78C4DE6F765E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENT_T5328F3E98A257F496099549407BA78C4DE6F765E_H
#ifndef INPUT_T830A2D3A65441ABC6001D38F28B5A07E57FDAD7B_H
#define INPUT_T830A2D3A65441ABC6001D38F28B5A07E57FDAD7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Input.Input
struct  Input_t830A2D3A65441ABC6001D38F28B5A07E57FDAD7B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUT_T830A2D3A65441ABC6001D38F28B5A07E57FDAD7B_H
#ifndef MENU_TC3C19FD4A9E677DB96FA23D74AEF713E337565A0_H
#define MENU_TC3C19FD4A9E677DB96FA23D74AEF713E337565A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Interfaces.Menu
struct  Menu_tC3C19FD4A9E677DB96FA23D74AEF713E337565A0  : public RuntimeObject
{
public:

public:
};

struct Menu_tC3C19FD4A9E677DB96FA23D74AEF713E337565A0_StaticFields
{
public:
	// MPAR.Common.Scripting.API.Interfaces.Menu/ScriptMenu MPAR.Common.Scripting.API.Interfaces.Menu::_Menu
	ScriptMenu_t8D4D728B65EE28F23D1B655B6840216521F33B27 * ____Menu_0;

public:
	inline static int32_t get_offset_of__Menu_0() { return static_cast<int32_t>(offsetof(Menu_tC3C19FD4A9E677DB96FA23D74AEF713E337565A0_StaticFields, ____Menu_0)); }
	inline ScriptMenu_t8D4D728B65EE28F23D1B655B6840216521F33B27 * get__Menu_0() const { return ____Menu_0; }
	inline ScriptMenu_t8D4D728B65EE28F23D1B655B6840216521F33B27 ** get_address_of__Menu_0() { return &____Menu_0; }
	inline void set__Menu_0(ScriptMenu_t8D4D728B65EE28F23D1B655B6840216521F33B27 * value)
	{
		____Menu_0 = value;
		Il2CppCodeGenWriteBarrier((&____Menu_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENU_TC3C19FD4A9E677DB96FA23D74AEF713E337565A0_H
#ifndef SCRIPTMENU_T8D4D728B65EE28F23D1B655B6840216521F33B27_H
#define SCRIPTMENU_T8D4D728B65EE28F23D1B655B6840216521F33B27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Interfaces.Menu/ScriptMenu
struct  ScriptMenu_t8D4D728B65EE28F23D1B655B6840216521F33B27  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTMENU_T8D4D728B65EE28F23D1B655B6840216521F33B27_H
#ifndef U3CU3EO__0_TFBD97D0E94A7B0C6305B3F9B2B106C9A823D01E5_H
#define U3CU3EO__0_TFBD97D0E94A7B0C6305B3F9B2B106C9A823D01E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Interfaces.Menu/ScriptMenu/<>o__0
struct  U3CU3Eo__0_tFBD97D0E94A7B0C6305B3F9B2B106C9A823D01E5  : public RuntimeObject
{
public:

public:
};

struct U3CU3Eo__0_tFBD97D0E94A7B0C6305B3F9B2B106C9A823D01E5_StaticFields
{
public:
	// System.Runtime.CompilerServices.CallSite`1<System.Func`3<System.Runtime.CompilerServices.CallSite,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>>> MPAR.Common.Scripting.API.Interfaces.Menu/ScriptMenu/<>o__0::<>p__0
	CallSite_1_tFE3733C1C85F359D9BFDE52BFC1DF0904A2082BD * ___U3CU3Ep__0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ep__0_0() { return static_cast<int32_t>(offsetof(U3CU3Eo__0_tFBD97D0E94A7B0C6305B3F9B2B106C9A823D01E5_StaticFields, ___U3CU3Ep__0_0)); }
	inline CallSite_1_tFE3733C1C85F359D9BFDE52BFC1DF0904A2082BD * get_U3CU3Ep__0_0() const { return ___U3CU3Ep__0_0; }
	inline CallSite_1_tFE3733C1C85F359D9BFDE52BFC1DF0904A2082BD ** get_address_of_U3CU3Ep__0_0() { return &___U3CU3Ep__0_0; }
	inline void set_U3CU3Ep__0_0(CallSite_1_tFE3733C1C85F359D9BFDE52BFC1DF0904A2082BD * value)
	{
		___U3CU3Ep__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ep__0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EO__0_TFBD97D0E94A7B0C6305B3F9B2B106C9A823D01E5_H
#ifndef UI_TDFF5D24118D6BE841F050DFF9D6F7174E3995D00_H
#define UI_TDFF5D24118D6BE841F050DFF9D6F7174E3995D00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Interfaces.UI
struct  UI_tDFF5D24118D6BE841F050DFF9D6F7174E3995D00  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UI_TDFF5D24118D6BE841F050DFF9D6F7174E3995D00_H
#ifndef U3CU3EC_T72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0_H
#define U3CU3EC_T72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Interfaces.UI/<>c
struct  U3CU3Ec_t72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0_StaticFields
{
public:
	// MPAR.Common.Scripting.API.Interfaces.UI/<>c MPAR.Common.Scripting.API.Interfaces.UI/<>c::<>9
	U3CU3Ec_t72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0 * ___U3CU3E9_0;
	// System.Action MPAR.Common.Scripting.API.Interfaces.UI/<>c::<>9__5_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__5_0_1;
	// System.Action MPAR.Common.Scripting.API.Interfaces.UI/<>c::<>9__6_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__6_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0_StaticFields, ___U3CU3E9__5_0_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__5_0_1() const { return ___U3CU3E9__5_0_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__5_0_1() { return &___U3CU3E9__5_0_1; }
	inline void set_U3CU3E9__5_0_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__5_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0_StaticFields, ___U3CU3E9__6_0_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__6_0_2() const { return ___U3CU3E9__6_0_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__6_0_2() { return &___U3CU3E9__6_0_2; }
	inline void set_U3CU3E9__6_0_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__6_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T524EB53E9D79D3A125C5CFD70481935999523D4F_H
#define U3CU3EC__DISPLAYCLASS7_0_T524EB53E9D79D3A125C5CFD70481935999523D4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Interfaces.UI/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t524EB53E9D79D3A125C5CFD70481935999523D4F  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Scripting.API.Interfaces.UI/<>c__DisplayClass7_0::callback
	String_t* ___callback_0;
	// MPAR.Common.Scripting.API.Objects.MixiplyObject MPAR.Common.Scripting.API.Interfaces.UI/<>c__DisplayClass7_0::ui
	MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B * ___ui_1;
	// System.Func`2<UnityEngine.Transform,System.Boolean> MPAR.Common.Scripting.API.Interfaces.UI/<>c__DisplayClass7_0::<>9__1
	Func_2_t0F30AD8B62D43CC2E76F1EB584331B7BF4B28336 * ___U3CU3E9__1_2;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t524EB53E9D79D3A125C5CFD70481935999523D4F, ___callback_0)); }
	inline String_t* get_callback_0() const { return ___callback_0; }
	inline String_t** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(String_t* value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_ui_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t524EB53E9D79D3A125C5CFD70481935999523D4F, ___ui_1)); }
	inline MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B * get_ui_1() const { return ___ui_1; }
	inline MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B ** get_address_of_ui_1() { return &___ui_1; }
	inline void set_ui_1(MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B * value)
	{
		___ui_1 = value;
		Il2CppCodeGenWriteBarrier((&___ui_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t524EB53E9D79D3A125C5CFD70481935999523D4F, ___U3CU3E9__1_2)); }
	inline Func_2_t0F30AD8B62D43CC2E76F1EB584331B7BF4B28336 * get_U3CU3E9__1_2() const { return ___U3CU3E9__1_2; }
	inline Func_2_t0F30AD8B62D43CC2E76F1EB584331B7BF4B28336 ** get_address_of_U3CU3E9__1_2() { return &___U3CU3E9__1_2; }
	inline void set_U3CU3E9__1_2(Func_2_t0F30AD8B62D43CC2E76F1EB584331B7BF4B28336 * value)
	{
		___U3CU3E9__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T524EB53E9D79D3A125C5CFD70481935999523D4F_H
#ifndef U3CU3EC_T22EB45990884EAFF2E33E3B36ECE2EF7A8873322_H
#define U3CU3EC_T22EB45990884EAFF2E33E3B36ECE2EF7A8873322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Objects.MixiplyObject/<>c
struct  U3CU3Ec_t22EB45990884EAFF2E33E3B36ECE2EF7A8873322  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t22EB45990884EAFF2E33E3B36ECE2EF7A8873322_StaticFields
{
public:
	// MPAR.Common.Scripting.API.Objects.MixiplyObject/<>c MPAR.Common.Scripting.API.Objects.MixiplyObject/<>c::<>9
	U3CU3Ec_t22EB45990884EAFF2E33E3B36ECE2EF7A8873322 * ___U3CU3E9_0;
	// System.Converter`2<UnityEngine.Animation,MPAR.Common.Scripting.API.Assets.Animation> MPAR.Common.Scripting.API.Objects.MixiplyObject/<>c::<>9__70_0
	Converter_2_t2E69D7C5FB71D7D52D508F977FC06A551BD352CA * ___U3CU3E9__70_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t22EB45990884EAFF2E33E3B36ECE2EF7A8873322_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t22EB45990884EAFF2E33E3B36ECE2EF7A8873322 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t22EB45990884EAFF2E33E3B36ECE2EF7A8873322 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t22EB45990884EAFF2E33E3B36ECE2EF7A8873322 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__70_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t22EB45990884EAFF2E33E3B36ECE2EF7A8873322_StaticFields, ___U3CU3E9__70_0_1)); }
	inline Converter_2_t2E69D7C5FB71D7D52D508F977FC06A551BD352CA * get_U3CU3E9__70_0_1() const { return ___U3CU3E9__70_0_1; }
	inline Converter_2_t2E69D7C5FB71D7D52D508F977FC06A551BD352CA ** get_address_of_U3CU3E9__70_0_1() { return &___U3CU3E9__70_0_1; }
	inline void set_U3CU3E9__70_0_1(Converter_2_t2E69D7C5FB71D7D52D508F977FC06A551BD352CA * value)
	{
		___U3CU3E9__70_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__70_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T22EB45990884EAFF2E33E3B36ECE2EF7A8873322_H
#ifndef PRIMITIVE_TCDD3C68D66228397104F11B36425365735BF0269_H
#define PRIMITIVE_TCDD3C68D66228397104F11B36425365735BF0269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Objects.Primitive
struct  Primitive_tCDD3C68D66228397104F11B36425365735BF0269  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVE_TCDD3C68D66228397104F11B36425365735BF0269_H
#ifndef U3CU3EO__7_T2B0D61D6E37B81328988165DEFD5BBC925A2D584_H
#define U3CU3EO__7_T2B0D61D6E37B81328988165DEFD5BBC925A2D584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Objects.Primitive/<>o__7
struct  U3CU3Eo__7_t2B0D61D6E37B81328988165DEFD5BBC925A2D584  : public RuntimeObject
{
public:

public:
};

struct U3CU3Eo__7_t2B0D61D6E37B81328988165DEFD5BBC925A2D584_StaticFields
{
public:
	// System.Runtime.CompilerServices.CallSite`1<System.Func`4<System.Runtime.CompilerServices.CallSite,System.Object,System.Object,System.Object>> MPAR.Common.Scripting.API.Objects.Primitive/<>o__7::<>p__0
	CallSite_1_tC48D5A99F98099B2A9523EA7D849598158F3099C * ___U3CU3Ep__0_0;
	// System.Runtime.CompilerServices.CallSite`1<System.Func`3<System.Runtime.CompilerServices.CallSite,System.Object,System.Boolean>> MPAR.Common.Scripting.API.Objects.Primitive/<>o__7::<>p__1
	CallSite_1_tA51C476901067D91F355F48DD7AD56997B266A56 * ___U3CU3Ep__1_1;
	// System.Runtime.CompilerServices.CallSite`1<System.Func`3<System.Runtime.CompilerServices.CallSite,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>>> MPAR.Common.Scripting.API.Objects.Primitive/<>o__7::<>p__2
	CallSite_1_tFE3733C1C85F359D9BFDE52BFC1DF0904A2082BD * ___U3CU3Ep__2_2;

public:
	inline static int32_t get_offset_of_U3CU3Ep__0_0() { return static_cast<int32_t>(offsetof(U3CU3Eo__7_t2B0D61D6E37B81328988165DEFD5BBC925A2D584_StaticFields, ___U3CU3Ep__0_0)); }
	inline CallSite_1_tC48D5A99F98099B2A9523EA7D849598158F3099C * get_U3CU3Ep__0_0() const { return ___U3CU3Ep__0_0; }
	inline CallSite_1_tC48D5A99F98099B2A9523EA7D849598158F3099C ** get_address_of_U3CU3Ep__0_0() { return &___U3CU3Ep__0_0; }
	inline void set_U3CU3Ep__0_0(CallSite_1_tC48D5A99F98099B2A9523EA7D849598158F3099C * value)
	{
		___U3CU3Ep__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ep__0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ep__1_1() { return static_cast<int32_t>(offsetof(U3CU3Eo__7_t2B0D61D6E37B81328988165DEFD5BBC925A2D584_StaticFields, ___U3CU3Ep__1_1)); }
	inline CallSite_1_tA51C476901067D91F355F48DD7AD56997B266A56 * get_U3CU3Ep__1_1() const { return ___U3CU3Ep__1_1; }
	inline CallSite_1_tA51C476901067D91F355F48DD7AD56997B266A56 ** get_address_of_U3CU3Ep__1_1() { return &___U3CU3Ep__1_1; }
	inline void set_U3CU3Ep__1_1(CallSite_1_tA51C476901067D91F355F48DD7AD56997B266A56 * value)
	{
		___U3CU3Ep__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ep__1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ep__2_2() { return static_cast<int32_t>(offsetof(U3CU3Eo__7_t2B0D61D6E37B81328988165DEFD5BBC925A2D584_StaticFields, ___U3CU3Ep__2_2)); }
	inline CallSite_1_tFE3733C1C85F359D9BFDE52BFC1DF0904A2082BD * get_U3CU3Ep__2_2() const { return ___U3CU3Ep__2_2; }
	inline CallSite_1_tFE3733C1C85F359D9BFDE52BFC1DF0904A2082BD ** get_address_of_U3CU3Ep__2_2() { return &___U3CU3Ep__2_2; }
	inline void set_U3CU3Ep__2_2(CallSite_1_tFE3733C1C85F359D9BFDE52BFC1DF0904A2082BD * value)
	{
		___U3CU3Ep__2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ep__2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EO__7_T2B0D61D6E37B81328988165DEFD5BBC925A2D584_H
#ifndef PHYSICS_T053201723E39B546745364236F3DDC3138C92042_H
#define PHYSICS_T053201723E39B546745364236F3DDC3138C92042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Physics.Physics
struct  Physics_t053201723E39B546745364236F3DDC3138C92042  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICS_T053201723E39B546745364236F3DDC3138C92042_H
#ifndef SCRIPTAPICONTENT_TAF9606D7B4658C85CC42E2597DB1604C42F5FFC4_H
#define SCRIPTAPICONTENT_TAF9606D7B4658C85CC42E2597DB1604C42F5FFC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.ScriptApiContent
struct  ScriptApiContent_tAF9606D7B4658C85CC42E2597DB1604C42F5FFC4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTAPICONTENT_TAF9606D7B4658C85CC42E2597DB1604C42F5FFC4_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T18B6407483C42D410F07C9501DB3E4A8C06C7CEA_H
#define U3CU3EC__DISPLAYCLASS12_0_T18B6407483C42D410F07C9501DB3E4A8C06C7CEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.ScriptApiContent/<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t18B6407483C42D410F07C9501DB3E4A8C06C7CEA  : public RuntimeObject
{
public:
	// Player MPAR.Common.Scripting.API.ScriptApiContent/<>c__DisplayClass12_0::player
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___player_0;
	// System.Single MPAR.Common.Scripting.API.ScriptApiContent/<>c__DisplayClass12_0::damage
	float ___damage_1;

public:
	inline static int32_t get_offset_of_player_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t18B6407483C42D410F07C9501DB3E4A8C06C7CEA, ___player_0)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_player_0() const { return ___player_0; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_player_0() { return &___player_0; }
	inline void set_player_0(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___player_0 = value;
		Il2CppCodeGenWriteBarrier((&___player_0), value);
	}

	inline static int32_t get_offset_of_damage_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t18B6407483C42D410F07C9501DB3E4A8C06C7CEA, ___damage_1)); }
	inline float get_damage_1() const { return ___damage_1; }
	inline float* get_address_of_damage_1() { return &___damage_1; }
	inline void set_damage_1(float value)
	{
		___damage_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T18B6407483C42D410F07C9501DB3E4A8C06C7CEA_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_TC0167226F2ED0C4E021A48336E8426301692FCF6_H
#define U3CU3EC__DISPLAYCLASS6_0_TC0167226F2ED0C4E021A48336E8426301692FCF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.ScriptApiContent/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_tC0167226F2ED0C4E021A48336E8426301692FCF6  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Scripting.API.ScriptApiContent/<>c__DisplayClass6_0::elementName
	String_t* ___elementName_0;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tC0167226F2ED0C4E021A48336E8426301692FCF6, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_TC0167226F2ED0C4E021A48336E8426301692FCF6_H
#ifndef BEHAVIOUR_TBA8D40C72EE63C7BF267F5EF8730F9B32B58CAE2_H
#define BEHAVIOUR_TBA8D40C72EE63C7BF267F5EF8730F9B32B58CAE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Utility.Behaviour
struct  Behaviour_tBA8D40C72EE63C7BF267F5EF8730F9B32B58CAE2  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBA8D40C72EE63C7BF267F5EF8730F9B32B58CAE2_H
#ifndef DEBUG_TC2BB8147F32C43BEA6B75F2DD8451A77423315FB_H
#define DEBUG_TC2BB8147F32C43BEA6B75F2DD8451A77423315FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Utility.Debug.Debug
struct  Debug_tC2BB8147F32C43BEA6B75F2DD8451A77423315FB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUG_TC2BB8147F32C43BEA6B75F2DD8451A77423315FB_H
#ifndef MIXIPLY_T19CA7AE73AD2F7787F1E92DC8AA79B56AE03E13E_H
#define MIXIPLY_T19CA7AE73AD2F7787F1E92DC8AA79B56AE03E13E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Utility.Mixiply
struct  Mixiply_t19CA7AE73AD2F7787F1E92DC8AA79B56AE03E13E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXIPLY_T19CA7AE73AD2F7787F1E92DC8AA79B56AE03E13E_H
#ifndef PLAYER_T8585448877A2DF9A2B4B8AF312457B3581B3E4D6_H
#define PLAYER_T8585448877A2DF9A2B4B8AF312457B3581B3E4D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Utility.Player
struct  Player_t8585448877A2DF9A2B4B8AF312457B3581B3E4D6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER_T8585448877A2DF9A2B4B8AF312457B3581B3E4D6_H
#ifndef WEB_T94CA446B8B5457E3432A4F3E8E3D99E4EFEA2B44_H
#define WEB_T94CA446B8B5457E3432A4F3E8E3D99E4EFEA2B44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Web.Web
struct  Web_t94CA446B8B5457E3432A4F3E8E3D99E4EFEA2B44  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEB_T94CA446B8B5457E3432A4F3E8E3D99E4EFEA2B44_H
#ifndef WEBREQUESTAPI_T721C4D6D2E73AD4E4FECAE9180264D08834B046A_H
#define WEBREQUESTAPI_T721C4D6D2E73AD4E4FECAE9180264D08834B046A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.WebRequestApi
struct  WebRequestApi_t721C4D6D2E73AD4E4FECAE9180264D08834B046A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTAPI_T721C4D6D2E73AD4E4FECAE9180264D08834B046A_H
#ifndef GALLERYMIXMODEL_T6F1D38D099BA9039A95018862BE9D18ADEB0F43F_H
#define GALLERYMIXMODEL_T6F1D38D099BA9039A95018862BE9D18ADEB0F43F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.GalleryMixModel
struct  GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Scripting.GalleryMixModel::id
	String_t* ___id_0;
	// System.String MPAR.Common.Scripting.GalleryMixModel::name
	String_t* ___name_1;
	// System.String MPAR.Common.Scripting.GalleryMixModel::description
	String_t* ___description_2;
	// System.Boolean MPAR.Common.Scripting.GalleryMixModel::shared
	bool ___shared_3;
	// System.Int32 MPAR.Common.Scripting.GalleryMixModel::revision
	int32_t ___revision_4;
	// System.String MPAR.Common.Scripting.GalleryMixModel::userId
	String_t* ___userId_5;
	// System.String MPAR.Common.Scripting.GalleryMixModel::nickname
	String_t* ___nickname_6;
	// System.String MPAR.Common.Scripting.GalleryMixModel::includes
	String_t* ___includes_7;
	// System.String MPAR.Common.Scripting.GalleryMixModel::thumbnail
	String_t* ___thumbnail_8;
	// System.String MPAR.Common.Scripting.GalleryMixModel::mixScript
	String_t* ___mixScript_9;
	// System.Int32 MPAR.Common.Scripting.GalleryMixModel::moderationStatus
	int32_t ___moderationStatus_10;
	// System.String MPAR.Common.Scripting.GalleryMixModel::moderationMessage
	String_t* ___moderationMessage_11;
	// System.Boolean MPAR.Common.Scripting.GalleryMixModel::blocked
	bool ___blocked_12;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_description_2() { return static_cast<int32_t>(offsetof(GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F, ___description_2)); }
	inline String_t* get_description_2() const { return ___description_2; }
	inline String_t** get_address_of_description_2() { return &___description_2; }
	inline void set_description_2(String_t* value)
	{
		___description_2 = value;
		Il2CppCodeGenWriteBarrier((&___description_2), value);
	}

	inline static int32_t get_offset_of_shared_3() { return static_cast<int32_t>(offsetof(GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F, ___shared_3)); }
	inline bool get_shared_3() const { return ___shared_3; }
	inline bool* get_address_of_shared_3() { return &___shared_3; }
	inline void set_shared_3(bool value)
	{
		___shared_3 = value;
	}

	inline static int32_t get_offset_of_revision_4() { return static_cast<int32_t>(offsetof(GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F, ___revision_4)); }
	inline int32_t get_revision_4() const { return ___revision_4; }
	inline int32_t* get_address_of_revision_4() { return &___revision_4; }
	inline void set_revision_4(int32_t value)
	{
		___revision_4 = value;
	}

	inline static int32_t get_offset_of_userId_5() { return static_cast<int32_t>(offsetof(GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F, ___userId_5)); }
	inline String_t* get_userId_5() const { return ___userId_5; }
	inline String_t** get_address_of_userId_5() { return &___userId_5; }
	inline void set_userId_5(String_t* value)
	{
		___userId_5 = value;
		Il2CppCodeGenWriteBarrier((&___userId_5), value);
	}

	inline static int32_t get_offset_of_nickname_6() { return static_cast<int32_t>(offsetof(GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F, ___nickname_6)); }
	inline String_t* get_nickname_6() const { return ___nickname_6; }
	inline String_t** get_address_of_nickname_6() { return &___nickname_6; }
	inline void set_nickname_6(String_t* value)
	{
		___nickname_6 = value;
		Il2CppCodeGenWriteBarrier((&___nickname_6), value);
	}

	inline static int32_t get_offset_of_includes_7() { return static_cast<int32_t>(offsetof(GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F, ___includes_7)); }
	inline String_t* get_includes_7() const { return ___includes_7; }
	inline String_t** get_address_of_includes_7() { return &___includes_7; }
	inline void set_includes_7(String_t* value)
	{
		___includes_7 = value;
		Il2CppCodeGenWriteBarrier((&___includes_7), value);
	}

	inline static int32_t get_offset_of_thumbnail_8() { return static_cast<int32_t>(offsetof(GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F, ___thumbnail_8)); }
	inline String_t* get_thumbnail_8() const { return ___thumbnail_8; }
	inline String_t** get_address_of_thumbnail_8() { return &___thumbnail_8; }
	inline void set_thumbnail_8(String_t* value)
	{
		___thumbnail_8 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnail_8), value);
	}

	inline static int32_t get_offset_of_mixScript_9() { return static_cast<int32_t>(offsetof(GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F, ___mixScript_9)); }
	inline String_t* get_mixScript_9() const { return ___mixScript_9; }
	inline String_t** get_address_of_mixScript_9() { return &___mixScript_9; }
	inline void set_mixScript_9(String_t* value)
	{
		___mixScript_9 = value;
		Il2CppCodeGenWriteBarrier((&___mixScript_9), value);
	}

	inline static int32_t get_offset_of_moderationStatus_10() { return static_cast<int32_t>(offsetof(GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F, ___moderationStatus_10)); }
	inline int32_t get_moderationStatus_10() const { return ___moderationStatus_10; }
	inline int32_t* get_address_of_moderationStatus_10() { return &___moderationStatus_10; }
	inline void set_moderationStatus_10(int32_t value)
	{
		___moderationStatus_10 = value;
	}

	inline static int32_t get_offset_of_moderationMessage_11() { return static_cast<int32_t>(offsetof(GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F, ___moderationMessage_11)); }
	inline String_t* get_moderationMessage_11() const { return ___moderationMessage_11; }
	inline String_t** get_address_of_moderationMessage_11() { return &___moderationMessage_11; }
	inline void set_moderationMessage_11(String_t* value)
	{
		___moderationMessage_11 = value;
		Il2CppCodeGenWriteBarrier((&___moderationMessage_11), value);
	}

	inline static int32_t get_offset_of_blocked_12() { return static_cast<int32_t>(offsetof(GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F, ___blocked_12)); }
	inline bool get_blocked_12() const { return ___blocked_12; }
	inline bool* get_address_of_blocked_12() { return &___blocked_12; }
	inline void set_blocked_12(bool value)
	{
		___blocked_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GALLERYMIXMODEL_T6F1D38D099BA9039A95018862BE9D18ADEB0F43F_H
#ifndef GALLERYMODEL_TF9E0F01E19CF50C229DBFBD6B8591EF17DFFAC6A_H
#define GALLERYMODEL_TF9E0F01E19CF50C229DBFBD6B8591EF17DFFAC6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.GalleryModel
struct  GalleryModel_tF9E0F01E19CF50C229DBFBD6B8591EF17DFFAC6A  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<MPAR.Common.Scripting.GalleryMixModel> MPAR.Common.Scripting.GalleryModel::mixes
	List_1_tEBE348C8DBDB33364A789FE6FE5089BA27BC084C * ___mixes_0;
	// System.Int32 MPAR.Common.Scripting.GalleryModel::total
	int32_t ___total_1;

public:
	inline static int32_t get_offset_of_mixes_0() { return static_cast<int32_t>(offsetof(GalleryModel_tF9E0F01E19CF50C229DBFBD6B8591EF17DFFAC6A, ___mixes_0)); }
	inline List_1_tEBE348C8DBDB33364A789FE6FE5089BA27BC084C * get_mixes_0() const { return ___mixes_0; }
	inline List_1_tEBE348C8DBDB33364A789FE6FE5089BA27BC084C ** get_address_of_mixes_0() { return &___mixes_0; }
	inline void set_mixes_0(List_1_tEBE348C8DBDB33364A789FE6FE5089BA27BC084C * value)
	{
		___mixes_0 = value;
		Il2CppCodeGenWriteBarrier((&___mixes_0), value);
	}

	inline static int32_t get_offset_of_total_1() { return static_cast<int32_t>(offsetof(GalleryModel_tF9E0F01E19CF50C229DBFBD6B8591EF17DFFAC6A, ___total_1)); }
	inline int32_t get_total_1() const { return ___total_1; }
	inline int32_t* get_address_of_total_1() { return &___total_1; }
	inline void set_total_1(int32_t value)
	{
		___total_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GALLERYMODEL_TF9E0F01E19CF50C229DBFBD6B8591EF17DFFAC6A_H
#ifndef FILEGUIDREF_T8FBE42296BD015960D7CC10A785C8400D09AD49F_H
#define FILEGUIDREF_T8FBE42296BD015960D7CC10A785C8400D09AD49F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.Loader.FileGuidRef
struct  FileGuidRef_t8FBE42296BD015960D7CC10A785C8400D09AD49F  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Scripting.Loader.FileGuidRef::<FileName>k__BackingField
	String_t* ___U3CFileNameU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<System.String> MPAR.Common.Scripting.Loader.FileGuidRef::<ScriptGuidIdsThatReference>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CScriptGuidIdsThatReferenceU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFileNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FileGuidRef_t8FBE42296BD015960D7CC10A785C8400D09AD49F, ___U3CFileNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CFileNameU3Ek__BackingField_0() const { return ___U3CFileNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CFileNameU3Ek__BackingField_0() { return &___U3CFileNameU3Ek__BackingField_0; }
	inline void set_U3CFileNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CFileNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CScriptGuidIdsThatReferenceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FileGuidRef_t8FBE42296BD015960D7CC10A785C8400D09AD49F, ___U3CScriptGuidIdsThatReferenceU3Ek__BackingField_1)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CScriptGuidIdsThatReferenceU3Ek__BackingField_1() const { return ___U3CScriptGuidIdsThatReferenceU3Ek__BackingField_1; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CScriptGuidIdsThatReferenceU3Ek__BackingField_1() { return &___U3CScriptGuidIdsThatReferenceU3Ek__BackingField_1; }
	inline void set_U3CScriptGuidIdsThatReferenceU3Ek__BackingField_1(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CScriptGuidIdsThatReferenceU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScriptGuidIdsThatReferenceU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEGUIDREF_T8FBE42296BD015960D7CC10A785C8400D09AD49F_H
#ifndef U3CRECORDFRAMEU3ED__31_TDA2765346EA399B8041FC91B08CE822B3254BFC3_H
#define U3CRECORDFRAMEU3ED__31_TDA2765346EA399B8041FC91B08CE822B3254BFC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.Loader.ScreenshotQRCodeProjectLoader/<RecordFrame>d__31
struct  U3CRecordFrameU3Ed__31_tDA2765346EA399B8041FC91B08CE822B3254BFC3  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.Scripting.Loader.ScreenshotQRCodeProjectLoader/<RecordFrame>d__31::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.Scripting.Loader.ScreenshotQRCodeProjectLoader/<RecordFrame>d__31::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.Scripting.Loader.ScreenshotQRCodeProjectLoader MPAR.Common.Scripting.Loader.ScreenshotQRCodeProjectLoader/<RecordFrame>d__31::<>4__this
	ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRecordFrameU3Ed__31_tDA2765346EA399B8041FC91B08CE822B3254BFC3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRecordFrameU3Ed__31_tDA2765346EA399B8041FC91B08CE822B3254BFC3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CRecordFrameU3Ed__31_tDA2765346EA399B8041FC91B08CE822B3254BFC3, ___U3CU3E4__this_2)); }
	inline ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECORDFRAMEU3ED__31_TDA2765346EA399B8041FC91B08CE822B3254BFC3_H
#ifndef SCRIPTINDEXOBJECT_T4D3656E71D262DD293EC701B6C19622A2DA6E69E_H
#define SCRIPTINDEXOBJECT_T4D3656E71D262DD293EC701B6C19622A2DA6E69E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.Loader.ScriptIndexObject
struct  ScriptIndexObject_t4D3656E71D262DD293EC701B6C19622A2DA6E69E  : public RuntimeObject
{
public:
	// MPAR.Common.Scripting.MixModel[] MPAR.Common.Scripting.Loader.ScriptIndexObject::menuItems
	MixModelU5BU5D_tE8363090940F064FC46E381BFAC1A05A892EFF86* ___menuItems_0;

public:
	inline static int32_t get_offset_of_menuItems_0() { return static_cast<int32_t>(offsetof(ScriptIndexObject_t4D3656E71D262DD293EC701B6C19622A2DA6E69E, ___menuItems_0)); }
	inline MixModelU5BU5D_tE8363090940F064FC46E381BFAC1A05A892EFF86* get_menuItems_0() const { return ___menuItems_0; }
	inline MixModelU5BU5D_tE8363090940F064FC46E381BFAC1A05A892EFF86** get_address_of_menuItems_0() { return &___menuItems_0; }
	inline void set_menuItems_0(MixModelU5BU5D_tE8363090940F064FC46E381BFAC1A05A892EFF86* value)
	{
		___menuItems_0 = value;
		Il2CppCodeGenWriteBarrier((&___menuItems_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINDEXOBJECT_T4D3656E71D262DD293EC701B6C19622A2DA6E69E_H
#ifndef SCRIPTOBJECT_T03F4E0F2751528088A14749379AAC4C91B8D289B_H
#define SCRIPTOBJECT_T03F4E0F2751528088A14749379AAC4C91B8D289B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.Loader.ScriptObject
struct  ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Scripting.Loader.ScriptObject::title
	String_t* ___title_0;
	// System.String[] MPAR.Common.Scripting.Loader.ScriptObject::includes
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___includes_1;
	// System.String MPAR.Common.Scripting.Loader.ScriptObject::description
	String_t* ___description_2;
	// System.String MPAR.Common.Scripting.Loader.ScriptObject::author
	String_t* ___author_3;
	// System.String MPAR.Common.Scripting.Loader.ScriptObject::version
	String_t* ___version_4;
	// System.String MPAR.Common.Scripting.Loader.ScriptObject::thumbnail
	String_t* ___thumbnail_5;
	// System.String MPAR.Common.Scripting.Loader.ScriptObject::filename
	String_t* ___filename_6;

public:
	inline static int32_t get_offset_of_title_0() { return static_cast<int32_t>(offsetof(ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B, ___title_0)); }
	inline String_t* get_title_0() const { return ___title_0; }
	inline String_t** get_address_of_title_0() { return &___title_0; }
	inline void set_title_0(String_t* value)
	{
		___title_0 = value;
		Il2CppCodeGenWriteBarrier((&___title_0), value);
	}

	inline static int32_t get_offset_of_includes_1() { return static_cast<int32_t>(offsetof(ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B, ___includes_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_includes_1() const { return ___includes_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_includes_1() { return &___includes_1; }
	inline void set_includes_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___includes_1 = value;
		Il2CppCodeGenWriteBarrier((&___includes_1), value);
	}

	inline static int32_t get_offset_of_description_2() { return static_cast<int32_t>(offsetof(ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B, ___description_2)); }
	inline String_t* get_description_2() const { return ___description_2; }
	inline String_t** get_address_of_description_2() { return &___description_2; }
	inline void set_description_2(String_t* value)
	{
		___description_2 = value;
		Il2CppCodeGenWriteBarrier((&___description_2), value);
	}

	inline static int32_t get_offset_of_author_3() { return static_cast<int32_t>(offsetof(ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B, ___author_3)); }
	inline String_t* get_author_3() const { return ___author_3; }
	inline String_t** get_address_of_author_3() { return &___author_3; }
	inline void set_author_3(String_t* value)
	{
		___author_3 = value;
		Il2CppCodeGenWriteBarrier((&___author_3), value);
	}

	inline static int32_t get_offset_of_version_4() { return static_cast<int32_t>(offsetof(ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B, ___version_4)); }
	inline String_t* get_version_4() const { return ___version_4; }
	inline String_t** get_address_of_version_4() { return &___version_4; }
	inline void set_version_4(String_t* value)
	{
		___version_4 = value;
		Il2CppCodeGenWriteBarrier((&___version_4), value);
	}

	inline static int32_t get_offset_of_thumbnail_5() { return static_cast<int32_t>(offsetof(ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B, ___thumbnail_5)); }
	inline String_t* get_thumbnail_5() const { return ___thumbnail_5; }
	inline String_t** get_address_of_thumbnail_5() { return &___thumbnail_5; }
	inline void set_thumbnail_5(String_t* value)
	{
		___thumbnail_5 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnail_5), value);
	}

	inline static int32_t get_offset_of_filename_6() { return static_cast<int32_t>(offsetof(ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B, ___filename_6)); }
	inline String_t* get_filename_6() const { return ___filename_6; }
	inline String_t** get_address_of_filename_6() { return &___filename_6; }
	inline void set_filename_6(String_t* value)
	{
		___filename_6 = value;
		Il2CppCodeGenWriteBarrier((&___filename_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTOBJECT_T03F4E0F2751528088A14749379AAC4C91B8D289B_H
#ifndef METHODINFOEXTENSIONS_T2A5DC847FE17F1C7522D0B86346B9DE8466AB370_H
#define METHODINFOEXTENSIONS_T2A5DC847FE17F1C7522D0B86346B9DE8466AB370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.MethodInfoExtensions
struct  MethodInfoExtensions_t2A5DC847FE17F1C7522D0B86346B9DE8466AB370  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFOEXTENSIONS_T2A5DC847FE17F1C7522D0B86346B9DE8466AB370_H
#ifndef U3CU3EC_TBCA14C63283E5E5324AFB227954799B94C1ACEC8_H
#define U3CU3EC_TBCA14C63283E5E5324AFB227954799B94C1ACEC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.MethodInfoExtensions/<>c
struct  U3CU3Ec_tBCA14C63283E5E5324AFB227954799B94C1ACEC8  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tBCA14C63283E5E5324AFB227954799B94C1ACEC8_StaticFields
{
public:
	// MPAR.Common.Scripting.MethodInfoExtensions/<>c MPAR.Common.Scripting.MethodInfoExtensions/<>c::<>9
	U3CU3Ec_tBCA14C63283E5E5324AFB227954799B94C1ACEC8 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.ParameterInfo,System.Type> MPAR.Common.Scripting.MethodInfoExtensions/<>c::<>9__0_0
	Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBCA14C63283E5E5324AFB227954799B94C1ACEC8_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tBCA14C63283E5E5324AFB227954799B94C1ACEC8 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tBCA14C63283E5E5324AFB227954799B94C1ACEC8 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tBCA14C63283E5E5324AFB227954799B94C1ACEC8 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBCA14C63283E5E5324AFB227954799B94C1ACEC8_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TBCA14C63283E5E5324AFB227954799B94C1ACEC8_H
#ifndef MULTIPLATFORMACTION_T68284A49F3E8F58B98C11A50B7CA6CA7A4F7E705_H
#define MULTIPLATFORMACTION_T68284A49F3E8F58B98C11A50B7CA6CA7A4F7E705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.MultiPlatformActions.MultiPlatformAction
struct  MultiPlatformAction_t68284A49F3E8F58B98C11A50B7CA6CA7A4F7E705  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTIPLATFORMACTION_T68284A49F3E8F58B98C11A50B7CA6CA7A4F7E705_H
#ifndef U3CU3EC_TD159991999ABEA1B82B20A26118C32DA046BB8B0_H
#define U3CU3EC_TD159991999ABEA1B82B20A26118C32DA046BB8B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.MultiPlatformActions.MultiPlatformAction/<>c
struct  U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields
{
public:
	// MPAR.Common.Scripting.MultiPlatformActions.MultiPlatformAction/<>c MPAR.Common.Scripting.MultiPlatformActions.MultiPlatformAction/<>c::<>9
	U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0 * ___U3CU3E9_0;
	// System.Action MPAR.Common.Scripting.MultiPlatformActions.MultiPlatformAction/<>c::<>9__0_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__0_0_1;
	// System.Action MPAR.Common.Scripting.MultiPlatformActions.MultiPlatformAction/<>c::<>9__1_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__1_0_2;
	// System.Action MPAR.Common.Scripting.MultiPlatformActions.MultiPlatformAction/<>c::<>9__2_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__2_0_3;
	// System.Action MPAR.Common.Scripting.MultiPlatformActions.MultiPlatformAction/<>c::<>9__3_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__3_0_4;
	// System.Action MPAR.Common.Scripting.MultiPlatformActions.MultiPlatformAction/<>c::<>9__4_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__4_0_5;
	// System.Action MPAR.Common.Scripting.MultiPlatformActions.MultiPlatformAction/<>c::<>9__5_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__5_0_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields, ___U3CU3E9__1_0_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__1_0_2() const { return ___U3CU3E9__1_0_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__1_0_2() { return &___U3CU3E9__1_0_2; }
	inline void set_U3CU3E9__1_0_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__1_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields, ___U3CU3E9__2_0_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__2_0_3() const { return ___U3CU3E9__2_0_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__2_0_3() { return &___U3CU3E9__2_0_3; }
	inline void set_U3CU3E9__2_0_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__2_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields, ___U3CU3E9__3_0_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__3_0_4() const { return ___U3CU3E9__3_0_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__3_0_4() { return &___U3CU3E9__3_0_4; }
	inline void set_U3CU3E9__3_0_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__3_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields, ___U3CU3E9__4_0_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__4_0_5() const { return ___U3CU3E9__4_0_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__4_0_5() { return &___U3CU3E9__4_0_5; }
	inline void set_U3CU3E9__4_0_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__4_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields, ___U3CU3E9__5_0_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__5_0_6() const { return ___U3CU3E9__5_0_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__5_0_6() { return &___U3CU3E9__5_0_6; }
	inline void set_U3CU3E9__5_0_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__5_0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TD159991999ABEA1B82B20A26118C32DA046BB8B0_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_T34A3152F04773B8FE9D133DC5108F4A4984868D2_H
#define U3CU3EC__DISPLAYCLASS0_0_T34A3152F04773B8FE9D133DC5108F4A4984868D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.MultiPlatformActions.SetMouseFreeAction/<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t34A3152F04773B8FE9D133DC5108F4A4984868D2  : public RuntimeObject
{
public:
	// MPAR.Common.Input.Utility.GestureManager MPAR.Common.Scripting.MultiPlatformActions.SetMouseFreeAction/<>c__DisplayClass0_0::gestures
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * ___gestures_0;
	// System.Boolean MPAR.Common.Scripting.MultiPlatformActions.SetMouseFreeAction/<>c__DisplayClass0_0::free
	bool ___free_1;

public:
	inline static int32_t get_offset_of_gestures_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t34A3152F04773B8FE9D133DC5108F4A4984868D2, ___gestures_0)); }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * get_gestures_0() const { return ___gestures_0; }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 ** get_address_of_gestures_0() { return &___gestures_0; }
	inline void set_gestures_0(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * value)
	{
		___gestures_0 = value;
		Il2CppCodeGenWriteBarrier((&___gestures_0), value);
	}

	inline static int32_t get_offset_of_free_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t34A3152F04773B8FE9D133DC5108F4A4984868D2, ___free_1)); }
	inline bool get_free_1() const { return ___free_1; }
	inline bool* get_address_of_free_1() { return &___free_1; }
	inline void set_free_1(bool value)
	{
		___free_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_T34A3152F04773B8FE9D133DC5108F4A4984868D2_H
#ifndef U3CU3EC__DISPLAYCLASS30_0_TC52D98C12DE76ACD76F91DA1B43E1796F81BC844_H
#define U3CU3EC__DISPLAYCLASS30_0_TC52D98C12DE76ACD76F91DA1B43E1796F81BC844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass30_0
struct  U3CU3Ec__DisplayClass30_0_tC52D98C12DE76ACD76F91DA1B43E1796F81BC844  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass30_0::baseUrl
	String_t* ___baseUrl_0;
	// MPAR.Common.Scripting.ScriptLoader MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass30_0::<>4__this
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_baseUrl_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_tC52D98C12DE76ACD76F91DA1B43E1796F81BC844, ___baseUrl_0)); }
	inline String_t* get_baseUrl_0() const { return ___baseUrl_0; }
	inline String_t** get_address_of_baseUrl_0() { return &___baseUrl_0; }
	inline void set_baseUrl_0(String_t* value)
	{
		___baseUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseUrl_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_tC52D98C12DE76ACD76F91DA1B43E1796F81BC844, ___U3CU3E4__this_1)); }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS30_0_TC52D98C12DE76ACD76F91DA1B43E1796F81BC844_H
#ifndef U3CU3EC__DISPLAYCLASS33_0_T3C00579881D6BDEB719A35102C6779BCEEFF2C11_H
#define U3CU3EC__DISPLAYCLASS33_0_T3C00579881D6BDEB719A35102C6779BCEEFF2C11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass33_0
struct  U3CU3Ec__DisplayClass33_0_t3C00579881D6BDEB719A35102C6779BCEEFF2C11  : public RuntimeObject
{
public:
	// MPAR.Common.Scripting.Script MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass33_0::script
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___script_0;
	// System.Action`1<System.Boolean> MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass33_0::callback
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___callback_1;

public:
	inline static int32_t get_offset_of_script_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t3C00579881D6BDEB719A35102C6779BCEEFF2C11, ___script_0)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_script_0() const { return ___script_0; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_script_0() { return &___script_0; }
	inline void set_script_0(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___script_0 = value;
		Il2CppCodeGenWriteBarrier((&___script_0), value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t3C00579881D6BDEB719A35102C6779BCEEFF2C11, ___callback_1)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_callback_1() const { return ___callback_1; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_0_T3C00579881D6BDEB719A35102C6779BCEEFF2C11_H
#ifndef U3CU3EC__DISPLAYCLASS34_0_T52E9FF2846861C2B43715DFA17D60BBF17FAFBB3_H
#define U3CU3EC__DISPLAYCLASS34_0_T52E9FF2846861C2B43715DFA17D60BBF17FAFBB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass34_0
struct  U3CU3Ec__DisplayClass34_0_t52E9FF2846861C2B43715DFA17D60BBF17FAFBB3  : public RuntimeObject
{
public:
	// MPAR.Common.Scripting.ScriptLoader MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass34_0::<>4__this
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * ___U3CU3E4__this_0;
	// System.Action`1<MPAR.Common.Scripting.Script> MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass34_0::callback
	Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * ___callback_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t52E9FF2846861C2B43715DFA17D60BBF17FAFBB3, ___U3CU3E4__this_0)); }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t52E9FF2846861C2B43715DFA17D60BBF17FAFBB3, ___callback_1)); }
	inline Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * get_callback_1() const { return ___callback_1; }
	inline Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS34_0_T52E9FF2846861C2B43715DFA17D60BBF17FAFBB3_H
#ifndef U3CU3EC__DISPLAYCLASS36_0_T298D8788C6CC89EB351E61B1FEA298848BD4A2BA_H
#define U3CU3EC__DISPLAYCLASS36_0_T298D8788C6CC89EB351E61B1FEA298848BD4A2BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_t298D8788C6CC89EB351E61B1FEA298848BD4A2BA  : public RuntimeObject
{
public:
	// MPAR.Common.Scripting.Script MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass36_0::script
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___script_0;

public:
	inline static int32_t get_offset_of_script_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t298D8788C6CC89EB351E61B1FEA298848BD4A2BA, ___script_0)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_script_0() const { return ___script_0; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_script_0() { return &___script_0; }
	inline void set_script_0(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___script_0 = value;
		Il2CppCodeGenWriteBarrier((&___script_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS36_0_T298D8788C6CC89EB351E61B1FEA298848BD4A2BA_H
#ifndef U3CU3EC__DISPLAYCLASS38_0_T5F706BF913F1C9975BAA3B83FE5051263066BAFD_H
#define U3CU3EC__DISPLAYCLASS38_0_T5F706BF913F1C9975BAA3B83FE5051263066BAFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_t5F706BF913F1C9975BAA3B83FE5051263066BAFD  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass38_0::j
	int32_t ___j_0;
	// System.Collections.Generic.List`1<MPAR.Common.Scripting.Script> MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass38_0::scripts
	List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * ___scripts_1;
	// MPAR.Common.Scripting.ScriptLoader MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass38_0::<>4__this
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_j_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t5F706BF913F1C9975BAA3B83FE5051263066BAFD, ___j_0)); }
	inline int32_t get_j_0() const { return ___j_0; }
	inline int32_t* get_address_of_j_0() { return &___j_0; }
	inline void set_j_0(int32_t value)
	{
		___j_0 = value;
	}

	inline static int32_t get_offset_of_scripts_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t5F706BF913F1C9975BAA3B83FE5051263066BAFD, ___scripts_1)); }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * get_scripts_1() const { return ___scripts_1; }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 ** get_address_of_scripts_1() { return &___scripts_1; }
	inline void set_scripts_1(List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * value)
	{
		___scripts_1 = value;
		Il2CppCodeGenWriteBarrier((&___scripts_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t5F706BF913F1C9975BAA3B83FE5051263066BAFD, ___U3CU3E4__this_2)); }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_0_T5F706BF913F1C9975BAA3B83FE5051263066BAFD_H
#ifndef U3CU3EC__DISPLAYCLASS38_1_T53857E613F79F3A0534EBA2D25FF013D146CEB64_H
#define U3CU3EC__DISPLAYCLASS38_1_T53857E613F79F3A0534EBA2D25FF013D146CEB64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass38_1
struct  U3CU3Ec__DisplayClass38_1_t53857E613F79F3A0534EBA2D25FF013D146CEB64  : public RuntimeObject
{
public:
	// MPAR.Common.Scripting.Script MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass38_1::script
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___script_0;
	// MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass38_0 MPAR.Common.Scripting.ScriptLoader/<>c__DisplayClass38_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass38_0_t5F706BF913F1C9975BAA3B83FE5051263066BAFD * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_script_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_1_t53857E613F79F3A0534EBA2D25FF013D146CEB64, ___script_0)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_script_0() const { return ___script_0; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_script_0() { return &___script_0; }
	inline void set_script_0(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___script_0 = value;
		Il2CppCodeGenWriteBarrier((&___script_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_1_t53857E613F79F3A0534EBA2D25FF013D146CEB64, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass38_0_t5F706BF913F1C9975BAA3B83FE5051263066BAFD * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass38_0_t5F706BF913F1C9975BAA3B83FE5051263066BAFD ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass38_0_t5F706BF913F1C9975BAA3B83FE5051263066BAFD * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_1_T53857E613F79F3A0534EBA2D25FF013D146CEB64_H
#ifndef U3CAPPLYUPDATEU3ED__34_TD44A5483F1F3CC7F2095C0A3D1B0674454AB0211_H
#define U3CAPPLYUPDATEU3ED__34_TD44A5483F1F3CC7F2095C0A3D1B0674454AB0211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.ScriptLoader/<ApplyUpdate>d__34
struct  U3CApplyUpdateU3Ed__34_tD44A5483F1F3CC7F2095C0A3D1B0674454AB0211  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.Scripting.ScriptLoader/<ApplyUpdate>d__34::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.Scripting.ScriptLoader/<ApplyUpdate>d__34::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.Scripting.ScriptLoader MPAR.Common.Scripting.ScriptLoader/<ApplyUpdate>d__34::<>4__this
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * ___U3CU3E4__this_2;
	// System.Action`1<MPAR.Common.Scripting.Script> MPAR.Common.Scripting.ScriptLoader/<ApplyUpdate>d__34::callback
	Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * ___callback_3;
	// MPAR.Common.Scripting.Script MPAR.Common.Scripting.ScriptLoader/<ApplyUpdate>d__34::script
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___script_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CApplyUpdateU3Ed__34_tD44A5483F1F3CC7F2095C0A3D1B0674454AB0211, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CApplyUpdateU3Ed__34_tD44A5483F1F3CC7F2095C0A3D1B0674454AB0211, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CApplyUpdateU3Ed__34_tD44A5483F1F3CC7F2095C0A3D1B0674454AB0211, ___U3CU3E4__this_2)); }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CApplyUpdateU3Ed__34_tD44A5483F1F3CC7F2095C0A3D1B0674454AB0211, ___callback_3)); }
	inline Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * get_callback_3() const { return ___callback_3; }
	inline Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_script_4() { return static_cast<int32_t>(offsetof(U3CApplyUpdateU3Ed__34_tD44A5483F1F3CC7F2095C0A3D1B0674454AB0211, ___script_4)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_script_4() const { return ___script_4; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_script_4() { return &___script_4; }
	inline void set_script_4(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___script_4 = value;
		Il2CppCodeGenWriteBarrier((&___script_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAPPLYUPDATEU3ED__34_TD44A5483F1F3CC7F2095C0A3D1B0674454AB0211_H
#ifndef U3CCHECKFORUPDATESU3ED__33_TE32A498D1F06F706A79404A0F5109E1E62FBFA11_H
#define U3CCHECKFORUPDATESU3ED__33_TE32A498D1F06F706A79404A0F5109E1E62FBFA11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.ScriptLoader/<CheckForUpdates>d__33
struct  U3CCheckForUpdatesU3Ed__33_tE32A498D1F06F706A79404A0F5109E1E62FBFA11  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.Scripting.ScriptLoader/<CheckForUpdates>d__33::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.Scripting.ScriptLoader/<CheckForUpdates>d__33::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.Scripting.Script MPAR.Common.Scripting.ScriptLoader/<CheckForUpdates>d__33::script
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___script_2;
	// System.Action`1<System.Boolean> MPAR.Common.Scripting.ScriptLoader/<CheckForUpdates>d__33::callback
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___callback_3;
	// MPAR.Common.Scripting.ScriptLoader MPAR.Common.Scripting.ScriptLoader/<CheckForUpdates>d__33::<>4__this
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCheckForUpdatesU3Ed__33_tE32A498D1F06F706A79404A0F5109E1E62FBFA11, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCheckForUpdatesU3Ed__33_tE32A498D1F06F706A79404A0F5109E1E62FBFA11, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_script_2() { return static_cast<int32_t>(offsetof(U3CCheckForUpdatesU3Ed__33_tE32A498D1F06F706A79404A0F5109E1E62FBFA11, ___script_2)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_script_2() const { return ___script_2; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_script_2() { return &___script_2; }
	inline void set_script_2(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___script_2 = value;
		Il2CppCodeGenWriteBarrier((&___script_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CCheckForUpdatesU3Ed__33_tE32A498D1F06F706A79404A0F5109E1E62FBFA11, ___callback_3)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_callback_3() const { return ___callback_3; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CCheckForUpdatesU3Ed__33_tE32A498D1F06F706A79404A0F5109E1E62FBFA11, ___U3CU3E4__this_4)); }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKFORUPDATESU3ED__33_TE32A498D1F06F706A79404A0F5109E1E62FBFA11_H
#ifndef U3CGETSCRIPTFROMMANIFESTURLU3ED__31_TCC03994615EB7D1FB58B7C3287A492C5110B8225_H
#define U3CGETSCRIPTFROMMANIFESTURLU3ED__31_TCC03994615EB7D1FB58B7C3287A492C5110B8225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.ScriptLoader/<GetScriptFromManifestUrl>d__31
struct  U3CGetScriptFromManifestUrlU3Ed__31_tCC03994615EB7D1FB58B7C3287A492C5110B8225  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.Scripting.ScriptLoader/<GetScriptFromManifestUrl>d__31::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.Scripting.ScriptLoader/<GetScriptFromManifestUrl>d__31::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String MPAR.Common.Scripting.ScriptLoader/<GetScriptFromManifestUrl>d__31::url
	String_t* ___url_2;
	// MPAR.Common.Scripting.ScriptLoader MPAR.Common.Scripting.ScriptLoader/<GetScriptFromManifestUrl>d__31::<>4__this
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * ___U3CU3E4__this_3;
	// System.Action`1<MPAR.Common.Scripting.Script> MPAR.Common.Scripting.ScriptLoader/<GetScriptFromManifestUrl>d__31::callback
	Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * ___callback_4;
	// UnityEngine.WWW MPAR.Common.Scripting.ScriptLoader/<GetScriptFromManifestUrl>d__31::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetScriptFromManifestUrlU3Ed__31_tCC03994615EB7D1FB58B7C3287A492C5110B8225, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetScriptFromManifestUrlU3Ed__31_tCC03994615EB7D1FB58B7C3287A492C5110B8225, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(U3CGetScriptFromManifestUrlU3Ed__31_tCC03994615EB7D1FB58B7C3287A492C5110B8225, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((&___url_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetScriptFromManifestUrlU3Ed__31_tCC03994615EB7D1FB58B7C3287A492C5110B8225, ___U3CU3E4__this_3)); }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CGetScriptFromManifestUrlU3Ed__31_tCC03994615EB7D1FB58B7C3287A492C5110B8225, ___callback_4)); }
	inline Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * get_callback_4() const { return ___callback_4; }
	inline Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetScriptFromManifestUrlU3Ed__31_tCC03994615EB7D1FB58B7C3287A492C5110B8225, ___U3CwwwU3E5__2_5)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_5() const { return ___U3CwwwU3E5__2_5; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_5() { return &___U3CwwwU3E5__2_5; }
	inline void set_U3CwwwU3E5__2_5(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSCRIPTFROMMANIFESTURLU3ED__31_TCC03994615EB7D1FB58B7C3287A492C5110B8225_H
#ifndef U3CLOADFROMPROJECTURLU3ED__30_TE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC_H
#define U3CLOADFROMPROJECTURLU3ED__30_TE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.ScriptLoader/<LoadFromProjectUrl>d__30
struct  U3CLoadFromProjectUrlU3Ed__30_tE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.Scripting.ScriptLoader/<LoadFromProjectUrl>d__30::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.Scripting.ScriptLoader/<LoadFromProjectUrl>d__30::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String MPAR.Common.Scripting.ScriptLoader/<LoadFromProjectUrl>d__30::baseUrl
	String_t* ___baseUrl_2;
	// MPAR.Common.Scripting.ScriptLoader MPAR.Common.Scripting.ScriptLoader/<LoadFromProjectUrl>d__30::<>4__this
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * ___U3CU3E4__this_3;
	// System.String MPAR.Common.Scripting.ScriptLoader/<LoadFromProjectUrl>d__30::autoRunUrl
	String_t* ___autoRunUrl_4;
	// UnityEngine.WWW MPAR.Common.Scripting.ScriptLoader/<LoadFromProjectUrl>d__30::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadFromProjectUrlU3Ed__30_tE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadFromProjectUrlU3Ed__30_tE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_baseUrl_2() { return static_cast<int32_t>(offsetof(U3CLoadFromProjectUrlU3Ed__30_tE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC, ___baseUrl_2)); }
	inline String_t* get_baseUrl_2() const { return ___baseUrl_2; }
	inline String_t** get_address_of_baseUrl_2() { return &___baseUrl_2; }
	inline void set_baseUrl_2(String_t* value)
	{
		___baseUrl_2 = value;
		Il2CppCodeGenWriteBarrier((&___baseUrl_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadFromProjectUrlU3Ed__30_tE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC, ___U3CU3E4__this_3)); }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_autoRunUrl_4() { return static_cast<int32_t>(offsetof(U3CLoadFromProjectUrlU3Ed__30_tE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC, ___autoRunUrl_4)); }
	inline String_t* get_autoRunUrl_4() const { return ___autoRunUrl_4; }
	inline String_t** get_address_of_autoRunUrl_4() { return &___autoRunUrl_4; }
	inline void set_autoRunUrl_4(String_t* value)
	{
		___autoRunUrl_4 = value;
		Il2CppCodeGenWriteBarrier((&___autoRunUrl_4), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CLoadFromProjectUrlU3Ed__30_tE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC, ___U3CwwwU3E5__2_5)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_5() const { return ___U3CwwwU3E5__2_5; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_5() { return &___U3CwwwU3E5__2_5; }
	inline void set_U3CwwwU3E5__2_5(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADFROMPROJECTURLU3ED__30_TE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC_H
#ifndef U3CCHECKSCRIPTAVAILABILITYU3ED__28_TF994452797D1E4BB179BCFC898871C6D74703EE7_H
#define U3CCHECKSCRIPTAVAILABILITYU3ED__28_TF994452797D1E4BB179BCFC898871C6D74703EE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.ScriptLoader/<checkScriptAvailability>d__28
struct  U3CcheckScriptAvailabilityU3Ed__28_tF994452797D1E4BB179BCFC898871C6D74703EE7  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.Scripting.ScriptLoader/<checkScriptAvailability>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.Scripting.ScriptLoader/<checkScriptAvailability>d__28::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.Scripting.Script MPAR.Common.Scripting.ScriptLoader/<checkScriptAvailability>d__28::script
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___script_2;
	// System.Action`1<System.Boolean> MPAR.Common.Scripting.ScriptLoader/<checkScriptAvailability>d__28::action
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___action_3;
	// UnityEngine.WWW MPAR.Common.Scripting.ScriptLoader/<checkScriptAvailability>d__28::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CcheckScriptAvailabilityU3Ed__28_tF994452797D1E4BB179BCFC898871C6D74703EE7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CcheckScriptAvailabilityU3Ed__28_tF994452797D1E4BB179BCFC898871C6D74703EE7, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_script_2() { return static_cast<int32_t>(offsetof(U3CcheckScriptAvailabilityU3Ed__28_tF994452797D1E4BB179BCFC898871C6D74703EE7, ___script_2)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_script_2() const { return ___script_2; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_script_2() { return &___script_2; }
	inline void set_script_2(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___script_2 = value;
		Il2CppCodeGenWriteBarrier((&___script_2), value);
	}

	inline static int32_t get_offset_of_action_3() { return static_cast<int32_t>(offsetof(U3CcheckScriptAvailabilityU3Ed__28_tF994452797D1E4BB179BCFC898871C6D74703EE7, ___action_3)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_action_3() const { return ___action_3; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_action_3() { return &___action_3; }
	inline void set_action_3(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___action_3 = value;
		Il2CppCodeGenWriteBarrier((&___action_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CcheckScriptAvailabilityU3Ed__28_tF994452797D1E4BB179BCFC898871C6D74703EE7, ___U3CwwwU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKSCRIPTAVAILABILITYU3ED__28_TF994452797D1E4BB179BCFC898871C6D74703EE7_H
#ifndef U3CU3EC_T20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9_H
#define U3CU3EC_T20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.ScriptManager/<>c
struct  U3CU3Ec_t20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9_StaticFields
{
public:
	// MPAR.Common.Scripting.ScriptManager/<>c MPAR.Common.Scripting.ScriptManager/<>c::<>9
	U3CU3Ec_t20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9 * ___U3CU3E9_0;
	// System.Func`2<System.Char,System.Boolean> MPAR.Common.Scripting.ScriptManager/<>c::<>9__89_0
	Func_2_tD15D8591FF52D18217AAF8FE723938708D4B1B28 * ___U3CU3E9__89_0_1;
	// System.Func`2<UnityEngine.MonoBehaviour,System.Boolean> MPAR.Common.Scripting.ScriptManager/<>c::<>9__90_0
	Func_2_t5A0E7E85DFC2CF97DFE0079B465DB9214F51084C * ___U3CU3E9__90_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__89_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9_StaticFields, ___U3CU3E9__89_0_1)); }
	inline Func_2_tD15D8591FF52D18217AAF8FE723938708D4B1B28 * get_U3CU3E9__89_0_1() const { return ___U3CU3E9__89_0_1; }
	inline Func_2_tD15D8591FF52D18217AAF8FE723938708D4B1B28 ** get_address_of_U3CU3E9__89_0_1() { return &___U3CU3E9__89_0_1; }
	inline void set_U3CU3E9__89_0_1(Func_2_tD15D8591FF52D18217AAF8FE723938708D4B1B28 * value)
	{
		___U3CU3E9__89_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__89_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__90_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9_StaticFields, ___U3CU3E9__90_0_2)); }
	inline Func_2_t5A0E7E85DFC2CF97DFE0079B465DB9214F51084C * get_U3CU3E9__90_0_2() const { return ___U3CU3E9__90_0_2; }
	inline Func_2_t5A0E7E85DFC2CF97DFE0079B465DB9214F51084C ** get_address_of_U3CU3E9__90_0_2() { return &___U3CU3E9__90_0_2; }
	inline void set_U3CU3E9__90_0_2(Func_2_t5A0E7E85DFC2CF97DFE0079B465DB9214F51084C * value)
	{
		___U3CU3E9__90_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__90_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9_H
#ifndef U3CDOWNLOADU3ED__89_T47BF6203AA7EE9D54C7FA87CCE039C8390B359FD_H
#define U3CDOWNLOADU3ED__89_T47BF6203AA7EE9D54C7FA87CCE039C8390B359FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.ScriptManager/<Download>d__89
struct  U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.Scripting.ScriptManager/<Download>d__89::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.Scripting.ScriptManager/<Download>d__89::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.Scripting.Script MPAR.Common.Scripting.ScriptManager/<Download>d__89::scr
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___scr_2;
	// System.Action`1<MPAR.Common.Scripting.Script> MPAR.Common.Scripting.ScriptManager/<Download>d__89::callback
	Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * ___callback_3;
	// MPAR.Common.Scripting.Script MPAR.Common.Scripting.ScriptManager/<Download>d__89::<script>5__2
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___U3CscriptU3E5__2_4;
	// UnityEngine.WWW MPAR.Common.Scripting.ScriptManager/<Download>d__89::<www>5__3
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__3_5;
	// System.String MPAR.Common.Scripting.ScriptManager/<Download>d__89::<js>5__4
	String_t* ___U3CjsU3E5__4_6;
	// System.Text.StringBuilder MPAR.Common.Scripting.ScriptManager/<Download>d__89::<includeBuilder>5__5
	StringBuilder_t * ___U3CincludeBuilderU3E5__5_7;
	// System.String[] MPAR.Common.Scripting.ScriptManager/<Download>d__89::<>7__wrap5
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___U3CU3E7__wrap5_8;
	// System.Int32 MPAR.Common.Scripting.ScriptManager/<Download>d__89::<>7__wrap6
	int32_t ___U3CU3E7__wrap6_9;
	// UnityEngine.WWW MPAR.Common.Scripting.ScriptManager/<Download>d__89::<www2>5__8
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3Cwww2U3E5__8_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_scr_2() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD, ___scr_2)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_scr_2() const { return ___scr_2; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_scr_2() { return &___scr_2; }
	inline void set_scr_2(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___scr_2 = value;
		Il2CppCodeGenWriteBarrier((&___scr_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD, ___callback_3)); }
	inline Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * get_callback_3() const { return ___callback_3; }
	inline Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(Action_1_tCBFA23DEDE10273DC35CCD7723C39542C8B9BD4E * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_U3CscriptU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD, ___U3CscriptU3E5__2_4)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_U3CscriptU3E5__2_4() const { return ___U3CscriptU3E5__2_4; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_U3CscriptU3E5__2_4() { return &___U3CscriptU3E5__2_4; }
	inline void set_U3CscriptU3E5__2_4(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___U3CscriptU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscriptU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD, ___U3CwwwU3E5__3_5)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__3_5() const { return ___U3CwwwU3E5__3_5; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__3_5() { return &___U3CwwwU3E5__3_5; }
	inline void set_U3CwwwU3E5__3_5(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__3_5), value);
	}

	inline static int32_t get_offset_of_U3CjsU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD, ___U3CjsU3E5__4_6)); }
	inline String_t* get_U3CjsU3E5__4_6() const { return ___U3CjsU3E5__4_6; }
	inline String_t** get_address_of_U3CjsU3E5__4_6() { return &___U3CjsU3E5__4_6; }
	inline void set_U3CjsU3E5__4_6(String_t* value)
	{
		___U3CjsU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsU3E5__4_6), value);
	}

	inline static int32_t get_offset_of_U3CincludeBuilderU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD, ___U3CincludeBuilderU3E5__5_7)); }
	inline StringBuilder_t * get_U3CincludeBuilderU3E5__5_7() const { return ___U3CincludeBuilderU3E5__5_7; }
	inline StringBuilder_t ** get_address_of_U3CincludeBuilderU3E5__5_7() { return &___U3CincludeBuilderU3E5__5_7; }
	inline void set_U3CincludeBuilderU3E5__5_7(StringBuilder_t * value)
	{
		___U3CincludeBuilderU3E5__5_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CincludeBuilderU3E5__5_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap5_8() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD, ___U3CU3E7__wrap5_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_U3CU3E7__wrap5_8() const { return ___U3CU3E7__wrap5_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_U3CU3E7__wrap5_8() { return &___U3CU3E7__wrap5_8; }
	inline void set_U3CU3E7__wrap5_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___U3CU3E7__wrap5_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap5_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap6_9() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD, ___U3CU3E7__wrap6_9)); }
	inline int32_t get_U3CU3E7__wrap6_9() const { return ___U3CU3E7__wrap6_9; }
	inline int32_t* get_address_of_U3CU3E7__wrap6_9() { return &___U3CU3E7__wrap6_9; }
	inline void set_U3CU3E7__wrap6_9(int32_t value)
	{
		___U3CU3E7__wrap6_9 = value;
	}

	inline static int32_t get_offset_of_U3Cwww2U3E5__8_10() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD, ___U3Cwww2U3E5__8_10)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3Cwww2U3E5__8_10() const { return ___U3Cwww2U3E5__8_10; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3Cwww2U3E5__8_10() { return &___U3Cwww2U3E5__8_10; }
	inline void set_U3Cwww2U3E5__8_10(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3Cwww2U3E5__8_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cwww2U3E5__8_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADU3ED__89_T47BF6203AA7EE9D54C7FA87CCE039C8390B359FD_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_TEFB9F75E1D5E9FFE7001EB94B891A7FD164DFF1F_H
#define U3CU3EC__DISPLAYCLASS12_0_TEFB9F75E1D5E9FFE7001EB94B891A7FD164DFF1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.Web.WebRequestBuilder/<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_tEFB9F75E1D5E9FFE7001EB94B891A7FD164DFF1F  : public RuntimeObject
{
public:
	// System.Boolean MPAR.Common.Scripting.Web.WebRequestBuilder/<>c__DisplayClass12_0::done
	bool ___done_0;
	// System.String MPAR.Common.Scripting.Web.WebRequestBuilder/<>c__DisplayClass12_0::content
	String_t* ___content_1;

public:
	inline static int32_t get_offset_of_done_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_tEFB9F75E1D5E9FFE7001EB94B891A7FD164DFF1F, ___done_0)); }
	inline bool get_done_0() const { return ___done_0; }
	inline bool* get_address_of_done_0() { return &___done_0; }
	inline void set_done_0(bool value)
	{
		___done_0 = value;
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_tEFB9F75E1D5E9FFE7001EB94B891A7FD164DFF1F, ___content_1)); }
	inline String_t* get_content_1() const { return ___content_1; }
	inline String_t** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(String_t* value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_TEFB9F75E1D5E9FFE7001EB94B891A7FD164DFF1F_H
#ifndef U3CEXECCOROUTINEU3ED__12_T87C49FB243D9C9492874983697481B4FA1D13021_H
#define U3CEXECCOROUTINEU3ED__12_T87C49FB243D9C9492874983697481B4FA1D13021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.Web.WebRequestBuilder/<ExecCoroutine>d__12
struct  U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.Scripting.Web.WebRequestBuilder/<ExecCoroutine>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.Scripting.Web.WebRequestBuilder/<ExecCoroutine>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.Scripting.Web.WebRequestBuilder MPAR.Common.Scripting.Web.WebRequestBuilder/<ExecCoroutine>d__12::<>4__this
	WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7 * ___U3CU3E4__this_2;
	// MPAR.Common.Scripting.Web.WebRequestBuilder/<>c__DisplayClass12_0 MPAR.Common.Scripting.Web.WebRequestBuilder/<ExecCoroutine>d__12::<>8__1
	U3CU3Ec__DisplayClass12_0_tEFB9F75E1D5E9FFE7001EB94B891A7FD164DFF1F * ___U3CU3E8__1_3;
	// System.Func`2<System.String,System.Collections.IEnumerator> MPAR.Common.Scripting.Web.WebRequestBuilder/<ExecCoroutine>d__12::rawCallback
	Func_2_t475CC109419A4B3EE9FAA114D232178BA32C6841 * ___rawCallback_4;
	// System.Func`3<Jint.Native.JsValue,Jint.Native.JsValue[],Jint.Native.JsValue> MPAR.Common.Scripting.Web.WebRequestBuilder/<ExecCoroutine>d__12::callback
	Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 * ___callback_5;
	// MPAR.Common.Scripting.ScriptManager MPAR.Common.Scripting.Web.WebRequestBuilder/<ExecCoroutine>d__12::<scriptManager>5__2
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C * ___U3CscriptManagerU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021, ___U3CU3E4__this_2)); }
	inline WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_3() { return static_cast<int32_t>(offsetof(U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021, ___U3CU3E8__1_3)); }
	inline U3CU3Ec__DisplayClass12_0_tEFB9F75E1D5E9FFE7001EB94B891A7FD164DFF1F * get_U3CU3E8__1_3() const { return ___U3CU3E8__1_3; }
	inline U3CU3Ec__DisplayClass12_0_tEFB9F75E1D5E9FFE7001EB94B891A7FD164DFF1F ** get_address_of_U3CU3E8__1_3() { return &___U3CU3E8__1_3; }
	inline void set_U3CU3E8__1_3(U3CU3Ec__DisplayClass12_0_tEFB9F75E1D5E9FFE7001EB94B891A7FD164DFF1F * value)
	{
		___U3CU3E8__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_3), value);
	}

	inline static int32_t get_offset_of_rawCallback_4() { return static_cast<int32_t>(offsetof(U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021, ___rawCallback_4)); }
	inline Func_2_t475CC109419A4B3EE9FAA114D232178BA32C6841 * get_rawCallback_4() const { return ___rawCallback_4; }
	inline Func_2_t475CC109419A4B3EE9FAA114D232178BA32C6841 ** get_address_of_rawCallback_4() { return &___rawCallback_4; }
	inline void set_rawCallback_4(Func_2_t475CC109419A4B3EE9FAA114D232178BA32C6841 * value)
	{
		___rawCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___rawCallback_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021, ___callback_5)); }
	inline Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 * get_callback_5() const { return ___callback_5; }
	inline Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_U3CscriptManagerU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021, ___U3CscriptManagerU3E5__2_6)); }
	inline ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C * get_U3CscriptManagerU3E5__2_6() const { return ___U3CscriptManagerU3E5__2_6; }
	inline ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C ** get_address_of_U3CscriptManagerU3E5__2_6() { return &___U3CscriptManagerU3E5__2_6; }
	inline void set_U3CscriptManagerU3E5__2_6(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C * value)
	{
		___U3CscriptManagerU3E5__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscriptManagerU3E5__2_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECCOROUTINEU3ED__12_T87C49FB243D9C9492874983697481B4FA1D13021_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef SETMOUSEFREEACTION_TF1885192463ACB1AA5B86D4018B3DA184237505B_H
#define SETMOUSEFREEACTION_TF1885192463ACB1AA5B86D4018B3DA184237505B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.MultiPlatformActions.SetMouseFreeAction
struct  SetMouseFreeAction_tF1885192463ACB1AA5B86D4018B3DA184237505B  : public MultiPlatformAction_t68284A49F3E8F58B98C11A50B7CA6CA7A4F7E705
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETMOUSEFREEACTION_TF1885192463ACB1AA5B86D4018B3DA184237505B_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef MODERATIONSTATUS_T792C26BC188A4F75004604C2E67FFDBE186F1158_H
#define MODERATIONSTATUS_T792C26BC188A4F75004604C2E67FFDBE186F1158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Entities.ModerationStatus
struct  ModerationStatus_t792C26BC188A4F75004604C2E67FFDBE186F1158 
{
public:
	// System.Int32 Assets._Project.Scripts.Common.Entities.ModerationStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModerationStatus_t792C26BC188A4F75004604C2E67FFDBE186F1158, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODERATIONSTATUS_T792C26BC188A4F75004604C2E67FFDBE186F1158_H
#ifndef TYPES_TE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A_H
#define TYPES_TE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Types
struct  Types_tE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A 
{
public:
	// System.Int32 Jint.Runtime.Types::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Types_tE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPES_TE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A_H
#ifndef COLLECTIONNODE_T6ED8EDB43863FAF4EF18CB7E156D7620FF4F93DC_H
#define COLLECTIONNODE_T6ED8EDB43863FAF4EF18CB7E156D7620FF4F93DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Collections.CollectionNode
struct  CollectionNode_t6ED8EDB43863FAF4EF18CB7E156D7620FF4F93DC  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Collections.CollectionNode::Name
	String_t* ___Name_0;
	// UnityEngine.Vector2 MPAR.Common.Collections.CollectionNode::Offset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Offset_1;
	// System.Single MPAR.Common.Collections.CollectionNode::Radius
	float ___Radius_2;
	// UnityEngine.Transform MPAR.Common.Collections.CollectionNode::transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform_3;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(CollectionNode_t6ED8EDB43863FAF4EF18CB7E156D7620FF4F93DC, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Offset_1() { return static_cast<int32_t>(offsetof(CollectionNode_t6ED8EDB43863FAF4EF18CB7E156D7620FF4F93DC, ___Offset_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_Offset_1() const { return ___Offset_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_Offset_1() { return &___Offset_1; }
	inline void set_Offset_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___Offset_1 = value;
	}

	inline static int32_t get_offset_of_Radius_2() { return static_cast<int32_t>(offsetof(CollectionNode_t6ED8EDB43863FAF4EF18CB7E156D7620FF4F93DC, ___Radius_2)); }
	inline float get_Radius_2() const { return ___Radius_2; }
	inline float* get_address_of_Radius_2() { return &___Radius_2; }
	inline void set_Radius_2(float value)
	{
		___Radius_2 = value;
	}

	inline static int32_t get_offset_of_transform_3() { return static_cast<int32_t>(offsetof(CollectionNode_t6ED8EDB43863FAF4EF18CB7E156D7620FF4F93DC, ___transform_3)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_transform_3() const { return ___transform_3; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_transform_3() { return &___transform_3; }
	inline void set_transform_3(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___transform_3 = value;
		Il2CppCodeGenWriteBarrier((&___transform_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONNODE_T6ED8EDB43863FAF4EF18CB7E156D7620FF4F93DC_H
#ifndef LAYOUTTYPEENUM_T9D756A67715DCD69C2107BE558831A1FB6DF7021_H
#define LAYOUTTYPEENUM_T9D756A67715DCD69C2107BE558831A1FB6DF7021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Collections.LayoutTypeEnum
struct  LayoutTypeEnum_t9D756A67715DCD69C2107BE558831A1FB6DF7021 
{
public:
	// System.Int32 MPAR.Common.Collections.LayoutTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LayoutTypeEnum_t9D756A67715DCD69C2107BE558831A1FB6DF7021, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTTYPEENUM_T9D756A67715DCD69C2107BE558831A1FB6DF7021_H
#ifndef BEHAVIORENUM_T159F12A07D59CCB912D1E65E3393A29F9032C340_H
#define BEHAVIORENUM_T159F12A07D59CCB912D1E65E3393A29F9032C340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Collections.ObjectCollectionDynamic/BehaviorEnum
struct  BehaviorEnum_t159F12A07D59CCB912D1E65E3393A29F9032C340 
{
public:
	// System.Int32 MPAR.Common.Collections.ObjectCollectionDynamic/BehaviorEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BehaviorEnum_t159F12A07D59CCB912D1E65E3393A29F9032C340, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIORENUM_T159F12A07D59CCB912D1E65E3393A29F9032C340_H
#ifndef ORIENTTYPEENUM_T68335295F5F9990B4603413C5A074BDAFE36EDC1_H
#define ORIENTTYPEENUM_T68335295F5F9990B4603413C5A074BDAFE36EDC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Collections.OrientTypeEnum
struct  OrientTypeEnum_t68335295F5F9990B4603413C5A074BDAFE36EDC1 
{
public:
	// System.Int32 MPAR.Common.Collections.OrientTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OrientTypeEnum_t68335295F5F9990B4603413C5A074BDAFE36EDC1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTTYPEENUM_T68335295F5F9990B4603413C5A074BDAFE36EDC1_H
#ifndef SORTTYPEENUM_T39097EA708FC937E322E139D9144FAFB4FAEC747_H
#define SORTTYPEENUM_T39097EA708FC937E322E139D9144FAFB4FAEC747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Collections.SortTypeEnum
struct  SortTypeEnum_t39097EA708FC937E322E139D9144FAFB4FAEC747 
{
public:
	// System.Int32 MPAR.Common.Collections.SortTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SortTypeEnum_t39097EA708FC937E322E139D9144FAFB4FAEC747, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTTYPEENUM_T39097EA708FC937E322E139D9144FAFB4FAEC747_H
#ifndef SURFACETYPEENUM_T096EE4E9ED02B6B13FBE8735A7E004FEC28B0368_H
#define SURFACETYPEENUM_T096EE4E9ED02B6B13FBE8735A7E004FEC28B0368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Collections.SurfaceTypeEnum
struct  SurfaceTypeEnum_t096EE4E9ED02B6B13FBE8735A7E004FEC28B0368 
{
public:
	// System.Int32 MPAR.Common.Collections.SurfaceTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SurfaceTypeEnum_t096EE4E9ED02B6B13FBE8735A7E004FEC28B0368, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACETYPEENUM_T096EE4E9ED02B6B13FBE8735A7E004FEC28B0368_H
#ifndef COLOR_T100E89EFD2BFD031D5EA8DC95C865E5AEDBE3210_H
#define COLOR_T100E89EFD2BFD031D5EA8DC95C865E5AEDBE3210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Objects.Color
struct  Color_t100E89EFD2BFD031D5EA8DC95C865E5AEDBE3210  : public RuntimeObject
{
public:
	// UnityEngine.Color MPAR.Common.Scripting.API.Objects.Color::color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___color_0;

public:
	inline static int32_t get_offset_of_color_0() { return static_cast<int32_t>(offsetof(Color_t100E89EFD2BFD031D5EA8DC95C865E5AEDBE3210, ___color_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_color_0() const { return ___color_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_color_0() { return &___color_0; }
	inline void set_color_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___color_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T100E89EFD2BFD031D5EA8DC95C865E5AEDBE3210_H
#ifndef VECTOR_T00A95500B27F53ADE6532C18B38E4E68ADBE4207_H
#define VECTOR_T00A95500B27F53ADE6532C18B38E4E68ADBE4207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Objects.Vector
struct  Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 MPAR.Common.Scripting.API.Objects.Vector::vector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vector_0;

public:
	inline static int32_t get_offset_of_vector_0() { return static_cast<int32_t>(offsetof(Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207, ___vector_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vector_0() const { return ___vector_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vector_0() { return &___vector_0; }
	inline void set_vector_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vector_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR_T00A95500B27F53ADE6532C18B38E4E68ADBE4207_H
#ifndef METHOD_T10215807550AF57F7078A1243049F302E442D274_H
#define METHOD_T10215807550AF57F7078A1243049F302E442D274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Method
struct  Method_t10215807550AF57F7078A1243049F302E442D274 
{
public:
	// System.Int32 RestSharp.Method::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Method_t10215807550AF57F7078A1243049F302E442D274, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHOD_T10215807550AF57F7078A1243049F302E442D274_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COLLECTIONNODEDYNAMIC_TBDB73984DE03C86E6E43F322CF24ED2F46B19B78_H
#define COLLECTIONNODEDYNAMIC_TBDB73984DE03C86E6E43F322CF24ED2F46B19B78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Collections.ObjectCollectionDynamic/CollectionNodeDynamic
struct  CollectionNodeDynamic_tBDB73984DE03C86E6E43F322CF24ED2F46B19B78  : public CollectionNode_t6ED8EDB43863FAF4EF18CB7E156D7620FF4F93DC
{
public:
	// UnityEngine.Vector3 MPAR.Common.Collections.ObjectCollectionDynamic/CollectionNodeDynamic::localPositionOnStartup
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___localPositionOnStartup_4;
	// UnityEngine.Vector3 MPAR.Common.Collections.ObjectCollectionDynamic/CollectionNodeDynamic::localEulerAnglesOnStartup
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___localEulerAnglesOnStartup_5;

public:
	inline static int32_t get_offset_of_localPositionOnStartup_4() { return static_cast<int32_t>(offsetof(CollectionNodeDynamic_tBDB73984DE03C86E6E43F322CF24ED2F46B19B78, ___localPositionOnStartup_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_localPositionOnStartup_4() const { return ___localPositionOnStartup_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_localPositionOnStartup_4() { return &___localPositionOnStartup_4; }
	inline void set_localPositionOnStartup_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___localPositionOnStartup_4 = value;
	}

	inline static int32_t get_offset_of_localEulerAnglesOnStartup_5() { return static_cast<int32_t>(offsetof(CollectionNodeDynamic_tBDB73984DE03C86E6E43F322CF24ED2F46B19B78, ___localEulerAnglesOnStartup_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_localEulerAnglesOnStartup_5() const { return ___localEulerAnglesOnStartup_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_localEulerAnglesOnStartup_5() { return &___localEulerAnglesOnStartup_5; }
	inline void set_localEulerAnglesOnStartup_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___localEulerAnglesOnStartup_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONNODEDYNAMIC_TBDB73984DE03C86E6E43F322CF24ED2F46B19B78_H
#ifndef PARAMETERDEFINITION_TE505C70240704BCB0A37A1AE2D962EA6BBCBDB3C_H
#define PARAMETERDEFINITION_TE505C70240704BCB0A37A1AE2D962EA6BBCBDB3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Assets.ParameterDefinition
struct  ParameterDefinition_tE505C70240704BCB0A37A1AE2D962EA6BBCBDB3C  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Scripting.API.Assets.ParameterDefinition::name
	String_t* ___name_0;
	// Jint.Runtime.Types MPAR.Common.Scripting.API.Assets.ParameterDefinition::type
	int32_t ___type_1;
	// System.Boolean MPAR.Common.Scripting.API.Assets.ParameterDefinition::required
	bool ___required_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ParameterDefinition_tE505C70240704BCB0A37A1AE2D962EA6BBCBDB3C, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(ParameterDefinition_tE505C70240704BCB0A37A1AE2D962EA6BBCBDB3C, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_required_2() { return static_cast<int32_t>(offsetof(ParameterDefinition_tE505C70240704BCB0A37A1AE2D962EA6BBCBDB3C, ___required_2)); }
	inline bool get_required_2() const { return ___required_2; }
	inline bool* get_address_of_required_2() { return &___required_2; }
	inline void set_required_2(bool value)
	{
		___required_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERDEFINITION_TE505C70240704BCB0A37A1AE2D962EA6BBCBDB3C_H
#ifndef MIXMODEL_T3D97435B97215AA60BFBD50870C49C217867CC5F_H
#define MIXMODEL_T3D97435B97215AA60BFBD50870C49C217867CC5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.MixModel
struct  MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Scripting.MixModel::id
	String_t* ___id_0;
	// System.String MPAR.Common.Scripting.MixModel::name
	String_t* ___name_1;
	// System.String MPAR.Common.Scripting.MixModel::fileName
	String_t* ___fileName_2;
	// System.String[] MPAR.Common.Scripting.MixModel::includes
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___includes_3;
	// System.String MPAR.Common.Scripting.MixModel::description
	String_t* ___description_4;
	// System.String MPAR.Common.Scripting.MixModel::nickname
	String_t* ___nickname_5;
	// System.Int32 MPAR.Common.Scripting.MixModel::revision
	int32_t ___revision_6;
	// System.String MPAR.Common.Scripting.MixModel::thumbnail
	String_t* ___thumbnail_7;
	// System.Boolean MPAR.Common.Scripting.MixModel::shared
	bool ___shared_8;
	// System.String MPAR.Common.Scripting.MixModel::userId
	String_t* ___userId_9;
	// System.String MPAR.Common.Scripting.MixModel::apiServiceBaseUri
	String_t* ___apiServiceBaseUri_10;
	// System.String[] MPAR.Common.Scripting.MixModel::targets
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___targets_11;
	// System.String MPAR.Common.Scripting.MixModel::manifestUrl
	String_t* ___manifestUrl_12;
	// System.Boolean MPAR.Common.Scripting.MixModel::blocked
	bool ___blocked_13;
	// Assets._Project.Scripts.Common.Entities.ModerationStatus MPAR.Common.Scripting.MixModel::moderationStatus
	int32_t ___moderationStatus_14;
	// System.Int32 MPAR.Common.Scripting.MixModel::lineScriptStartsAt
	int32_t ___lineScriptStartsAt_15;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> MPAR.Common.Scripting.MixModel::lineEachFileStartsAt
	Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * ___lineEachFileStartsAt_16;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_fileName_2() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___fileName_2)); }
	inline String_t* get_fileName_2() const { return ___fileName_2; }
	inline String_t** get_address_of_fileName_2() { return &___fileName_2; }
	inline void set_fileName_2(String_t* value)
	{
		___fileName_2 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_2), value);
	}

	inline static int32_t get_offset_of_includes_3() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___includes_3)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_includes_3() const { return ___includes_3; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_includes_3() { return &___includes_3; }
	inline void set_includes_3(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___includes_3 = value;
		Il2CppCodeGenWriteBarrier((&___includes_3), value);
	}

	inline static int32_t get_offset_of_description_4() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___description_4)); }
	inline String_t* get_description_4() const { return ___description_4; }
	inline String_t** get_address_of_description_4() { return &___description_4; }
	inline void set_description_4(String_t* value)
	{
		___description_4 = value;
		Il2CppCodeGenWriteBarrier((&___description_4), value);
	}

	inline static int32_t get_offset_of_nickname_5() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___nickname_5)); }
	inline String_t* get_nickname_5() const { return ___nickname_5; }
	inline String_t** get_address_of_nickname_5() { return &___nickname_5; }
	inline void set_nickname_5(String_t* value)
	{
		___nickname_5 = value;
		Il2CppCodeGenWriteBarrier((&___nickname_5), value);
	}

	inline static int32_t get_offset_of_revision_6() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___revision_6)); }
	inline int32_t get_revision_6() const { return ___revision_6; }
	inline int32_t* get_address_of_revision_6() { return &___revision_6; }
	inline void set_revision_6(int32_t value)
	{
		___revision_6 = value;
	}

	inline static int32_t get_offset_of_thumbnail_7() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___thumbnail_7)); }
	inline String_t* get_thumbnail_7() const { return ___thumbnail_7; }
	inline String_t** get_address_of_thumbnail_7() { return &___thumbnail_7; }
	inline void set_thumbnail_7(String_t* value)
	{
		___thumbnail_7 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnail_7), value);
	}

	inline static int32_t get_offset_of_shared_8() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___shared_8)); }
	inline bool get_shared_8() const { return ___shared_8; }
	inline bool* get_address_of_shared_8() { return &___shared_8; }
	inline void set_shared_8(bool value)
	{
		___shared_8 = value;
	}

	inline static int32_t get_offset_of_userId_9() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___userId_9)); }
	inline String_t* get_userId_9() const { return ___userId_9; }
	inline String_t** get_address_of_userId_9() { return &___userId_9; }
	inline void set_userId_9(String_t* value)
	{
		___userId_9 = value;
		Il2CppCodeGenWriteBarrier((&___userId_9), value);
	}

	inline static int32_t get_offset_of_apiServiceBaseUri_10() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___apiServiceBaseUri_10)); }
	inline String_t* get_apiServiceBaseUri_10() const { return ___apiServiceBaseUri_10; }
	inline String_t** get_address_of_apiServiceBaseUri_10() { return &___apiServiceBaseUri_10; }
	inline void set_apiServiceBaseUri_10(String_t* value)
	{
		___apiServiceBaseUri_10 = value;
		Il2CppCodeGenWriteBarrier((&___apiServiceBaseUri_10), value);
	}

	inline static int32_t get_offset_of_targets_11() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___targets_11)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_targets_11() const { return ___targets_11; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_targets_11() { return &___targets_11; }
	inline void set_targets_11(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___targets_11 = value;
		Il2CppCodeGenWriteBarrier((&___targets_11), value);
	}

	inline static int32_t get_offset_of_manifestUrl_12() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___manifestUrl_12)); }
	inline String_t* get_manifestUrl_12() const { return ___manifestUrl_12; }
	inline String_t** get_address_of_manifestUrl_12() { return &___manifestUrl_12; }
	inline void set_manifestUrl_12(String_t* value)
	{
		___manifestUrl_12 = value;
		Il2CppCodeGenWriteBarrier((&___manifestUrl_12), value);
	}

	inline static int32_t get_offset_of_blocked_13() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___blocked_13)); }
	inline bool get_blocked_13() const { return ___blocked_13; }
	inline bool* get_address_of_blocked_13() { return &___blocked_13; }
	inline void set_blocked_13(bool value)
	{
		___blocked_13 = value;
	}

	inline static int32_t get_offset_of_moderationStatus_14() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___moderationStatus_14)); }
	inline int32_t get_moderationStatus_14() const { return ___moderationStatus_14; }
	inline int32_t* get_address_of_moderationStatus_14() { return &___moderationStatus_14; }
	inline void set_moderationStatus_14(int32_t value)
	{
		___moderationStatus_14 = value;
	}

	inline static int32_t get_offset_of_lineScriptStartsAt_15() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___lineScriptStartsAt_15)); }
	inline int32_t get_lineScriptStartsAt_15() const { return ___lineScriptStartsAt_15; }
	inline int32_t* get_address_of_lineScriptStartsAt_15() { return &___lineScriptStartsAt_15; }
	inline void set_lineScriptStartsAt_15(int32_t value)
	{
		___lineScriptStartsAt_15 = value;
	}

	inline static int32_t get_offset_of_lineEachFileStartsAt_16() { return static_cast<int32_t>(offsetof(MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F, ___lineEachFileStartsAt_16)); }
	inline Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * get_lineEachFileStartsAt_16() const { return ___lineEachFileStartsAt_16; }
	inline Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C ** get_address_of_lineEachFileStartsAt_16() { return &___lineEachFileStartsAt_16; }
	inline void set_lineEachFileStartsAt_16(Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * value)
	{
		___lineEachFileStartsAt_16 = value;
		Il2CppCodeGenWriteBarrier((&___lineEachFileStartsAt_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXMODEL_T3D97435B97215AA60BFBD50870C49C217867CC5F_H
#ifndef SCRIPT_TD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC_H
#define SCRIPT_TD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.Script
struct  Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Scripting.Script::<Id>k__BackingField
	String_t* ___U3CIdU3Ek__BackingField_0;
	// UnityEngine.Texture2D MPAR.Common.Scripting.Script::<Thumbnail>k__BackingField
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___U3CThumbnailU3Ek__BackingField_1;
	// System.String MPAR.Common.Scripting.Script::<ThumbnailPath>k__BackingField
	String_t* ___U3CThumbnailPathU3Ek__BackingField_2;
	// System.String MPAR.Common.Scripting.Script::<Javascript>k__BackingField
	String_t* ___U3CJavascriptU3Ek__BackingField_3;
	// System.String MPAR.Common.Scripting.Script::<ScriptUrl>k__BackingField
	String_t* ___U3CScriptUrlU3Ek__BackingField_4;
	// System.String MPAR.Common.Scripting.Script::<ManifestUrl>k__BackingField
	String_t* ___U3CManifestUrlU3Ek__BackingField_5;
	// System.String MPAR.Common.Scripting.Script::name
	String_t* ___name_6;
	// System.String MPAR.Common.Scripting.Script::<Description>k__BackingField
	String_t* ___U3CDescriptionU3Ek__BackingField_7;
	// System.String MPAR.Common.Scripting.Script::<Author>k__BackingField
	String_t* ___U3CAuthorU3Ek__BackingField_8;
	// System.Int32 MPAR.Common.Scripting.Script::<Version>k__BackingField
	int32_t ___U3CVersionU3Ek__BackingField_9;
	// System.String[] MPAR.Common.Scripting.Script::<Includes>k__BackingField
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___U3CIncludesU3Ek__BackingField_10;
	// System.Boolean MPAR.Common.Scripting.Script::<Blocked>k__BackingField
	bool ___U3CBlockedU3Ek__BackingField_11;
	// Assets._Project.Scripts.Common.Entities.ModerationStatus MPAR.Common.Scripting.Script::<ModerationStatus>k__BackingField
	int32_t ___U3CModerationStatusU3Ek__BackingField_12;
	// System.DateTime MPAR.Common.Scripting.Script::<LastAccessed>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CLastAccessedU3Ek__BackingField_13;
	// System.Int32 MPAR.Common.Scripting.Script::<LineScriptStartsAt>k__BackingField
	int32_t ___U3CLineScriptStartsAtU3Ek__BackingField_14;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> MPAR.Common.Scripting.Script::<LineEachFileStartsAt>k__BackingField
	Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * ___U3CLineEachFileStartsAtU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___U3CIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CIdU3Ek__BackingField_0() const { return ___U3CIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CIdU3Ek__BackingField_0() { return &___U3CIdU3Ek__BackingField_0; }
	inline void set_U3CIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CThumbnailU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___U3CThumbnailU3Ek__BackingField_1)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_U3CThumbnailU3Ek__BackingField_1() const { return ___U3CThumbnailU3Ek__BackingField_1; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_U3CThumbnailU3Ek__BackingField_1() { return &___U3CThumbnailU3Ek__BackingField_1; }
	inline void set_U3CThumbnailU3Ek__BackingField_1(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___U3CThumbnailU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CThumbnailU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CThumbnailPathU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___U3CThumbnailPathU3Ek__BackingField_2)); }
	inline String_t* get_U3CThumbnailPathU3Ek__BackingField_2() const { return ___U3CThumbnailPathU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CThumbnailPathU3Ek__BackingField_2() { return &___U3CThumbnailPathU3Ek__BackingField_2; }
	inline void set_U3CThumbnailPathU3Ek__BackingField_2(String_t* value)
	{
		___U3CThumbnailPathU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CThumbnailPathU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CJavascriptU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___U3CJavascriptU3Ek__BackingField_3)); }
	inline String_t* get_U3CJavascriptU3Ek__BackingField_3() const { return ___U3CJavascriptU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CJavascriptU3Ek__BackingField_3() { return &___U3CJavascriptU3Ek__BackingField_3; }
	inline void set_U3CJavascriptU3Ek__BackingField_3(String_t* value)
	{
		___U3CJavascriptU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJavascriptU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CScriptUrlU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___U3CScriptUrlU3Ek__BackingField_4)); }
	inline String_t* get_U3CScriptUrlU3Ek__BackingField_4() const { return ___U3CScriptUrlU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CScriptUrlU3Ek__BackingField_4() { return &___U3CScriptUrlU3Ek__BackingField_4; }
	inline void set_U3CScriptUrlU3Ek__BackingField_4(String_t* value)
	{
		___U3CScriptUrlU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScriptUrlU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CManifestUrlU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___U3CManifestUrlU3Ek__BackingField_5)); }
	inline String_t* get_U3CManifestUrlU3Ek__BackingField_5() const { return ___U3CManifestUrlU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CManifestUrlU3Ek__BackingField_5() { return &___U3CManifestUrlU3Ek__BackingField_5; }
	inline void set_U3CManifestUrlU3Ek__BackingField_5(String_t* value)
	{
		___U3CManifestUrlU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CManifestUrlU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_name_6() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___name_6)); }
	inline String_t* get_name_6() const { return ___name_6; }
	inline String_t** get_address_of_name_6() { return &___name_6; }
	inline void set_name_6(String_t* value)
	{
		___name_6 = value;
		Il2CppCodeGenWriteBarrier((&___name_6), value);
	}

	inline static int32_t get_offset_of_U3CDescriptionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___U3CDescriptionU3Ek__BackingField_7)); }
	inline String_t* get_U3CDescriptionU3Ek__BackingField_7() const { return ___U3CDescriptionU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CDescriptionU3Ek__BackingField_7() { return &___U3CDescriptionU3Ek__BackingField_7; }
	inline void set_U3CDescriptionU3Ek__BackingField_7(String_t* value)
	{
		___U3CDescriptionU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDescriptionU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CAuthorU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___U3CAuthorU3Ek__BackingField_8)); }
	inline String_t* get_U3CAuthorU3Ek__BackingField_8() const { return ___U3CAuthorU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CAuthorU3Ek__BackingField_8() { return &___U3CAuthorU3Ek__BackingField_8; }
	inline void set_U3CAuthorU3Ek__BackingField_8(String_t* value)
	{
		___U3CAuthorU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthorU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___U3CVersionU3Ek__BackingField_9)); }
	inline int32_t get_U3CVersionU3Ek__BackingField_9() const { return ___U3CVersionU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CVersionU3Ek__BackingField_9() { return &___U3CVersionU3Ek__BackingField_9; }
	inline void set_U3CVersionU3Ek__BackingField_9(int32_t value)
	{
		___U3CVersionU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CIncludesU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___U3CIncludesU3Ek__BackingField_10)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_U3CIncludesU3Ek__BackingField_10() const { return ___U3CIncludesU3Ek__BackingField_10; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_U3CIncludesU3Ek__BackingField_10() { return &___U3CIncludesU3Ek__BackingField_10; }
	inline void set_U3CIncludesU3Ek__BackingField_10(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___U3CIncludesU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIncludesU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CBlockedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___U3CBlockedU3Ek__BackingField_11)); }
	inline bool get_U3CBlockedU3Ek__BackingField_11() const { return ___U3CBlockedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CBlockedU3Ek__BackingField_11() { return &___U3CBlockedU3Ek__BackingField_11; }
	inline void set_U3CBlockedU3Ek__BackingField_11(bool value)
	{
		___U3CBlockedU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CModerationStatusU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___U3CModerationStatusU3Ek__BackingField_12)); }
	inline int32_t get_U3CModerationStatusU3Ek__BackingField_12() const { return ___U3CModerationStatusU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CModerationStatusU3Ek__BackingField_12() { return &___U3CModerationStatusU3Ek__BackingField_12; }
	inline void set_U3CModerationStatusU3Ek__BackingField_12(int32_t value)
	{
		___U3CModerationStatusU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CLastAccessedU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___U3CLastAccessedU3Ek__BackingField_13)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CLastAccessedU3Ek__BackingField_13() const { return ___U3CLastAccessedU3Ek__BackingField_13; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CLastAccessedU3Ek__BackingField_13() { return &___U3CLastAccessedU3Ek__BackingField_13; }
	inline void set_U3CLastAccessedU3Ek__BackingField_13(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CLastAccessedU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CLineScriptStartsAtU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___U3CLineScriptStartsAtU3Ek__BackingField_14)); }
	inline int32_t get_U3CLineScriptStartsAtU3Ek__BackingField_14() const { return ___U3CLineScriptStartsAtU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CLineScriptStartsAtU3Ek__BackingField_14() { return &___U3CLineScriptStartsAtU3Ek__BackingField_14; }
	inline void set_U3CLineScriptStartsAtU3Ek__BackingField_14(int32_t value)
	{
		___U3CLineScriptStartsAtU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CLineEachFileStartsAtU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC, ___U3CLineEachFileStartsAtU3Ek__BackingField_15)); }
	inline Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * get_U3CLineEachFileStartsAtU3Ek__BackingField_15() const { return ___U3CLineEachFileStartsAtU3Ek__BackingField_15; }
	inline Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C ** get_address_of_U3CLineEachFileStartsAtU3Ek__BackingField_15() { return &___U3CLineEachFileStartsAtU3Ek__BackingField_15; }
	inline void set_U3CLineEachFileStartsAtU3Ek__BackingField_15(Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * value)
	{
		___U3CLineEachFileStartsAtU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLineEachFileStartsAtU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPT_TD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC_H
#ifndef WEBREQUESTBUILDER_TDDA774CA523A4255E9D0A503835562FE63C398C7_H
#define WEBREQUESTBUILDER_TDDA774CA523A4255E9D0A503835562FE63C398C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.Web.WebRequestBuilder
struct  WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Scripting.Web.WebRequestBuilder::baseUrl
	String_t* ___baseUrl_0;
	// System.String MPAR.Common.Scripting.Web.WebRequestBuilder::apiEndpoint
	String_t* ___apiEndpoint_1;
	// RestSharp.Method MPAR.Common.Scripting.Web.WebRequestBuilder::method
	int32_t ___method_2;
	// System.String MPAR.Common.Scripting.Web.WebRequestBuilder::username
	String_t* ___username_3;
	// System.String MPAR.Common.Scripting.Web.WebRequestBuilder::password
	String_t* ___password_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> MPAR.Common.Scripting.Web.WebRequestBuilder::parameters
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___parameters_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> MPAR.Common.Scripting.Web.WebRequestBuilder::urlSegments
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___urlSegments_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> MPAR.Common.Scripting.Web.WebRequestBuilder::headers
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___headers_7;
	// System.Object MPAR.Common.Scripting.Web.WebRequestBuilder::jsonBody
	RuntimeObject * ___jsonBody_8;

public:
	inline static int32_t get_offset_of_baseUrl_0() { return static_cast<int32_t>(offsetof(WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7, ___baseUrl_0)); }
	inline String_t* get_baseUrl_0() const { return ___baseUrl_0; }
	inline String_t** get_address_of_baseUrl_0() { return &___baseUrl_0; }
	inline void set_baseUrl_0(String_t* value)
	{
		___baseUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseUrl_0), value);
	}

	inline static int32_t get_offset_of_apiEndpoint_1() { return static_cast<int32_t>(offsetof(WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7, ___apiEndpoint_1)); }
	inline String_t* get_apiEndpoint_1() const { return ___apiEndpoint_1; }
	inline String_t** get_address_of_apiEndpoint_1() { return &___apiEndpoint_1; }
	inline void set_apiEndpoint_1(String_t* value)
	{
		___apiEndpoint_1 = value;
		Il2CppCodeGenWriteBarrier((&___apiEndpoint_1), value);
	}

	inline static int32_t get_offset_of_method_2() { return static_cast<int32_t>(offsetof(WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7, ___method_2)); }
	inline int32_t get_method_2() const { return ___method_2; }
	inline int32_t* get_address_of_method_2() { return &___method_2; }
	inline void set_method_2(int32_t value)
	{
		___method_2 = value;
	}

	inline static int32_t get_offset_of_username_3() { return static_cast<int32_t>(offsetof(WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7, ___username_3)); }
	inline String_t* get_username_3() const { return ___username_3; }
	inline String_t** get_address_of_username_3() { return &___username_3; }
	inline void set_username_3(String_t* value)
	{
		___username_3 = value;
		Il2CppCodeGenWriteBarrier((&___username_3), value);
	}

	inline static int32_t get_offset_of_password_4() { return static_cast<int32_t>(offsetof(WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7, ___password_4)); }
	inline String_t* get_password_4() const { return ___password_4; }
	inline String_t** get_address_of_password_4() { return &___password_4; }
	inline void set_password_4(String_t* value)
	{
		___password_4 = value;
		Il2CppCodeGenWriteBarrier((&___password_4), value);
	}

	inline static int32_t get_offset_of_parameters_5() { return static_cast<int32_t>(offsetof(WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7, ___parameters_5)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_parameters_5() const { return ___parameters_5; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_parameters_5() { return &___parameters_5; }
	inline void set_parameters_5(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___parameters_5 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_5), value);
	}

	inline static int32_t get_offset_of_urlSegments_6() { return static_cast<int32_t>(offsetof(WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7, ___urlSegments_6)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_urlSegments_6() const { return ___urlSegments_6; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_urlSegments_6() { return &___urlSegments_6; }
	inline void set_urlSegments_6(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___urlSegments_6 = value;
		Il2CppCodeGenWriteBarrier((&___urlSegments_6), value);
	}

	inline static int32_t get_offset_of_headers_7() { return static_cast<int32_t>(offsetof(WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7, ___headers_7)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_headers_7() const { return ___headers_7; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_headers_7() { return &___headers_7; }
	inline void set_headers_7(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___headers_7 = value;
		Il2CppCodeGenWriteBarrier((&___headers_7), value);
	}

	inline static int32_t get_offset_of_jsonBody_8() { return static_cast<int32_t>(offsetof(WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7, ___jsonBody_8)); }
	inline RuntimeObject * get_jsonBody_8() const { return ___jsonBody_8; }
	inline RuntimeObject ** get_address_of_jsonBody_8() { return &___jsonBody_8; }
	inline void set_jsonBody_8(RuntimeObject * value)
	{
		___jsonBody_8 = value;
		Il2CppCodeGenWriteBarrier((&___jsonBody_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTBUILDER_TDDA774CA523A4255E9D0A503835562FE63C398C7_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ABSTRACTASSETLOADER_T8FE1E80CC3316AB57156FE25E056F61190B9F4C2_H
#define ABSTRACTASSETLOADER_T8FE1E80CC3316AB57156FE25E056F61190B9F4C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.AbstractAssetLoader
struct  AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MPAR.Common.Persistence.ISaveLoadManager MPAR.Common.AssetLoader.AbstractAssetLoader::<SaveLoadManager>k__BackingField
	RuntimeObject* ___U3CSaveLoadManagerU3Ek__BackingField_5;
	// MPAR.Common.Persistence.CacheManager MPAR.Common.AssetLoader.AbstractAssetLoader::<CacheManager>k__BackingField
	CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * ___U3CCacheManagerU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CSaveLoadManagerU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2, ___U3CSaveLoadManagerU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CSaveLoadManagerU3Ek__BackingField_5() const { return ___U3CSaveLoadManagerU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CSaveLoadManagerU3Ek__BackingField_5() { return &___U3CSaveLoadManagerU3Ek__BackingField_5; }
	inline void set_U3CSaveLoadManagerU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CSaveLoadManagerU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSaveLoadManagerU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CCacheManagerU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2, ___U3CCacheManagerU3Ek__BackingField_6)); }
	inline CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * get_U3CCacheManagerU3Ek__BackingField_6() const { return ___U3CCacheManagerU3Ek__BackingField_6; }
	inline CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B ** get_address_of_U3CCacheManagerU3Ek__BackingField_6() { return &___U3CCacheManagerU3Ek__BackingField_6; }
	inline void set_U3CCacheManagerU3Ek__BackingField_6(CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * value)
	{
		___U3CCacheManagerU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCacheManagerU3Ek__BackingField_6), value);
	}
};

struct AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> MPAR.Common.AssetLoader.AbstractAssetLoader::<SupportedAssets>k__BackingField
	Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A * ___U3CSupportedAssetsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CSupportedAssetsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2_StaticFields, ___U3CSupportedAssetsU3Ek__BackingField_4)); }
	inline Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A * get_U3CSupportedAssetsU3Ek__BackingField_4() const { return ___U3CSupportedAssetsU3Ek__BackingField_4; }
	inline Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A ** get_address_of_U3CSupportedAssetsU3Ek__BackingField_4() { return &___U3CSupportedAssetsU3Ek__BackingField_4; }
	inline void set_U3CSupportedAssetsU3Ek__BackingField_4(Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A * value)
	{
		___U3CSupportedAssetsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSupportedAssetsU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTASSETLOADER_T8FE1E80CC3316AB57156FE25E056F61190B9F4C2_H
#ifndef VIDEOCONTROLS_T23B5B1B5640362B413000093351D1A6C473A00AE_H
#define VIDEOCONTROLS_T23B5B1B5640362B413000093351D1A6C473A00AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.VideoControls
struct  VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MPAR.Common.AssetLoader.VideoOptions MPAR.Common.AssetLoader.VideoControls::<AttachedVideoOptions>k__BackingField
	VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662 * ___U3CAttachedVideoOptionsU3Ek__BackingField_4;
	// System.Single MPAR.Common.AssetLoader.VideoControls::<Progress>k__BackingField
	float ___U3CProgressU3Ek__BackingField_5;
	// UnityEngine.Transform MPAR.Common.AssetLoader.VideoControls::<ProgressBar>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CProgressBarU3Ek__BackingField_6;
	// UnityEngine.Transform MPAR.Common.AssetLoader.VideoControls::<ProgressDragger>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CProgressDraggerU3Ek__BackingField_7;
	// MPAR.Common.UI.PortableMenu.PortableMenuItem MPAR.Common.AssetLoader.VideoControls::<PlayButton>k__BackingField
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * ___U3CPlayButtonU3Ek__BackingField_8;
	// MPAR.Common.UI.PortableMenu.PortableMenuItem MPAR.Common.AssetLoader.VideoControls::<PauseButton>k__BackingField
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * ___U3CPauseButtonU3Ek__BackingField_9;
	// UnityEngine.Transform MPAR.Common.AssetLoader.VideoControls::minProgressTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___minProgressTransform_10;
	// UnityEngine.Transform MPAR.Common.AssetLoader.VideoControls::maxProgressTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___maxProgressTransform_11;
	// System.Boolean MPAR.Common.AssetLoader.VideoControls::hasDisplayedFirstFrame
	bool ___hasDisplayedFirstFrame_12;
	// System.Boolean MPAR.Common.AssetLoader.VideoControls::updatingProgress
	bool ___updatingProgress_13;
	// System.Single MPAR.Common.AssetLoader.VideoControls::lastProgressChange
	float ___lastProgressChange_14;
	// UnityEngine.Transform[] MPAR.Common.AssetLoader.VideoControls::potentialMenuPositions
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* ___potentialMenuPositions_15;
	// UnityEngine.Vector3 MPAR.Common.AssetLoader.VideoControls::moveVelocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___moveVelocity_16;
	// System.Boolean MPAR.Common.AssetLoader.VideoControls::inExhibitBox
	bool ___inExhibitBox_17;
	// MPAR.Common.Input.Utility.GestureManager MPAR.Common.AssetLoader.VideoControls::Gestures
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * ___Gestures_18;

public:
	inline static int32_t get_offset_of_U3CAttachedVideoOptionsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE, ___U3CAttachedVideoOptionsU3Ek__BackingField_4)); }
	inline VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662 * get_U3CAttachedVideoOptionsU3Ek__BackingField_4() const { return ___U3CAttachedVideoOptionsU3Ek__BackingField_4; }
	inline VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662 ** get_address_of_U3CAttachedVideoOptionsU3Ek__BackingField_4() { return &___U3CAttachedVideoOptionsU3Ek__BackingField_4; }
	inline void set_U3CAttachedVideoOptionsU3Ek__BackingField_4(VideoOptions_t3A1836F9B559300F8A3E125FAC35EFAEE9D35662 * value)
	{
		___U3CAttachedVideoOptionsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAttachedVideoOptionsU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CProgressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE, ___U3CProgressU3Ek__BackingField_5)); }
	inline float get_U3CProgressU3Ek__BackingField_5() const { return ___U3CProgressU3Ek__BackingField_5; }
	inline float* get_address_of_U3CProgressU3Ek__BackingField_5() { return &___U3CProgressU3Ek__BackingField_5; }
	inline void set_U3CProgressU3Ek__BackingField_5(float value)
	{
		___U3CProgressU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CProgressBarU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE, ___U3CProgressBarU3Ek__BackingField_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CProgressBarU3Ek__BackingField_6() const { return ___U3CProgressBarU3Ek__BackingField_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CProgressBarU3Ek__BackingField_6() { return &___U3CProgressBarU3Ek__BackingField_6; }
	inline void set_U3CProgressBarU3Ek__BackingField_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CProgressBarU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProgressBarU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CProgressDraggerU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE, ___U3CProgressDraggerU3Ek__BackingField_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CProgressDraggerU3Ek__BackingField_7() const { return ___U3CProgressDraggerU3Ek__BackingField_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CProgressDraggerU3Ek__BackingField_7() { return &___U3CProgressDraggerU3Ek__BackingField_7; }
	inline void set_U3CProgressDraggerU3Ek__BackingField_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CProgressDraggerU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProgressDraggerU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CPlayButtonU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE, ___U3CPlayButtonU3Ek__BackingField_8)); }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * get_U3CPlayButtonU3Ek__BackingField_8() const { return ___U3CPlayButtonU3Ek__BackingField_8; }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 ** get_address_of_U3CPlayButtonU3Ek__BackingField_8() { return &___U3CPlayButtonU3Ek__BackingField_8; }
	inline void set_U3CPlayButtonU3Ek__BackingField_8(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * value)
	{
		___U3CPlayButtonU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlayButtonU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CPauseButtonU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE, ___U3CPauseButtonU3Ek__BackingField_9)); }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * get_U3CPauseButtonU3Ek__BackingField_9() const { return ___U3CPauseButtonU3Ek__BackingField_9; }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 ** get_address_of_U3CPauseButtonU3Ek__BackingField_9() { return &___U3CPauseButtonU3Ek__BackingField_9; }
	inline void set_U3CPauseButtonU3Ek__BackingField_9(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * value)
	{
		___U3CPauseButtonU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPauseButtonU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_minProgressTransform_10() { return static_cast<int32_t>(offsetof(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE, ___minProgressTransform_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_minProgressTransform_10() const { return ___minProgressTransform_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_minProgressTransform_10() { return &___minProgressTransform_10; }
	inline void set_minProgressTransform_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___minProgressTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___minProgressTransform_10), value);
	}

	inline static int32_t get_offset_of_maxProgressTransform_11() { return static_cast<int32_t>(offsetof(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE, ___maxProgressTransform_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_maxProgressTransform_11() const { return ___maxProgressTransform_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_maxProgressTransform_11() { return &___maxProgressTransform_11; }
	inline void set_maxProgressTransform_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___maxProgressTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___maxProgressTransform_11), value);
	}

	inline static int32_t get_offset_of_hasDisplayedFirstFrame_12() { return static_cast<int32_t>(offsetof(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE, ___hasDisplayedFirstFrame_12)); }
	inline bool get_hasDisplayedFirstFrame_12() const { return ___hasDisplayedFirstFrame_12; }
	inline bool* get_address_of_hasDisplayedFirstFrame_12() { return &___hasDisplayedFirstFrame_12; }
	inline void set_hasDisplayedFirstFrame_12(bool value)
	{
		___hasDisplayedFirstFrame_12 = value;
	}

	inline static int32_t get_offset_of_updatingProgress_13() { return static_cast<int32_t>(offsetof(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE, ___updatingProgress_13)); }
	inline bool get_updatingProgress_13() const { return ___updatingProgress_13; }
	inline bool* get_address_of_updatingProgress_13() { return &___updatingProgress_13; }
	inline void set_updatingProgress_13(bool value)
	{
		___updatingProgress_13 = value;
	}

	inline static int32_t get_offset_of_lastProgressChange_14() { return static_cast<int32_t>(offsetof(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE, ___lastProgressChange_14)); }
	inline float get_lastProgressChange_14() const { return ___lastProgressChange_14; }
	inline float* get_address_of_lastProgressChange_14() { return &___lastProgressChange_14; }
	inline void set_lastProgressChange_14(float value)
	{
		___lastProgressChange_14 = value;
	}

	inline static int32_t get_offset_of_potentialMenuPositions_15() { return static_cast<int32_t>(offsetof(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE, ___potentialMenuPositions_15)); }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* get_potentialMenuPositions_15() const { return ___potentialMenuPositions_15; }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804** get_address_of_potentialMenuPositions_15() { return &___potentialMenuPositions_15; }
	inline void set_potentialMenuPositions_15(TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* value)
	{
		___potentialMenuPositions_15 = value;
		Il2CppCodeGenWriteBarrier((&___potentialMenuPositions_15), value);
	}

	inline static int32_t get_offset_of_moveVelocity_16() { return static_cast<int32_t>(offsetof(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE, ___moveVelocity_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_moveVelocity_16() const { return ___moveVelocity_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_moveVelocity_16() { return &___moveVelocity_16; }
	inline void set_moveVelocity_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___moveVelocity_16 = value;
	}

	inline static int32_t get_offset_of_inExhibitBox_17() { return static_cast<int32_t>(offsetof(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE, ___inExhibitBox_17)); }
	inline bool get_inExhibitBox_17() const { return ___inExhibitBox_17; }
	inline bool* get_address_of_inExhibitBox_17() { return &___inExhibitBox_17; }
	inline void set_inExhibitBox_17(bool value)
	{
		___inExhibitBox_17 = value;
	}

	inline static int32_t get_offset_of_Gestures_18() { return static_cast<int32_t>(offsetof(VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE, ___Gestures_18)); }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * get_Gestures_18() const { return ___Gestures_18; }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 ** get_address_of_Gestures_18() { return &___Gestures_18; }
	inline void set_Gestures_18(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * value)
	{
		___Gestures_18 = value;
		Il2CppCodeGenWriteBarrier((&___Gestures_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCONTROLS_T23B5B1B5640362B413000093351D1A6C473A00AE_H
#ifndef OBJECTCOLLECTION_T6FB07C8809401FD9FEEBD867A1F4044A022417EB_H
#define OBJECTCOLLECTION_T6FB07C8809401FD9FEEBD867A1F4044A022417EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Collections.ObjectCollection
struct  ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action`1<MPAR.Common.Collections.ObjectCollection> MPAR.Common.Collections.ObjectCollection::OnCollectionUpdated
	Action_1_t0F44F2F0F46EB58693C1552993EA892B40BEC161 * ___OnCollectionUpdated_4;
	// System.Collections.Generic.List`1<MPAR.Common.Collections.CollectionNode> MPAR.Common.Collections.ObjectCollection::NodeList
	List_1_t311D38FDCB31B0F0591BB5DD3DEA76A5BC1639F7 * ___NodeList_5;
	// MPAR.Common.Collections.SurfaceTypeEnum MPAR.Common.Collections.ObjectCollection::SurfaceType
	int32_t ___SurfaceType_6;
	// MPAR.Common.Collections.SortTypeEnum MPAR.Common.Collections.ObjectCollection::SortType
	int32_t ___SortType_7;
	// MPAR.Common.Collections.OrientTypeEnum MPAR.Common.Collections.ObjectCollection::OrientType
	int32_t ___OrientType_8;
	// MPAR.Common.Collections.LayoutTypeEnum MPAR.Common.Collections.ObjectCollection::LayoutType
	int32_t ___LayoutType_9;
	// System.Boolean MPAR.Common.Collections.ObjectCollection::IgnoreInactiveTransforms
	bool ___IgnoreInactiveTransforms_10;
	// System.Single MPAR.Common.Collections.ObjectCollection::Radius
	float ___Radius_11;
	// System.Int32 MPAR.Common.Collections.ObjectCollection::Rows
	int32_t ___Rows_12;
	// System.Single MPAR.Common.Collections.ObjectCollection::CellWidth
	float ___CellWidth_13;
	// System.Single MPAR.Common.Collections.ObjectCollection::CellHeight
	float ___CellHeight_14;
	// UnityEngine.Mesh MPAR.Common.Collections.ObjectCollection::SphereMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___SphereMesh_15;
	// UnityEngine.Mesh MPAR.Common.Collections.ObjectCollection::CylinderMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___CylinderMesh_16;
	// System.Int32 MPAR.Common.Collections.ObjectCollection::_columns
	int32_t ____columns_17;
	// System.Single MPAR.Common.Collections.ObjectCollection::_width
	float ____width_18;
	// System.Single MPAR.Common.Collections.ObjectCollection::_height
	float ____height_19;
	// System.Single MPAR.Common.Collections.ObjectCollection::_circumference
	float ____circumference_20;
	// UnityEngine.Vector2 MPAR.Common.Collections.ObjectCollection::_halfCell
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____halfCell_21;

public:
	inline static int32_t get_offset_of_OnCollectionUpdated_4() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ___OnCollectionUpdated_4)); }
	inline Action_1_t0F44F2F0F46EB58693C1552993EA892B40BEC161 * get_OnCollectionUpdated_4() const { return ___OnCollectionUpdated_4; }
	inline Action_1_t0F44F2F0F46EB58693C1552993EA892B40BEC161 ** get_address_of_OnCollectionUpdated_4() { return &___OnCollectionUpdated_4; }
	inline void set_OnCollectionUpdated_4(Action_1_t0F44F2F0F46EB58693C1552993EA892B40BEC161 * value)
	{
		___OnCollectionUpdated_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnCollectionUpdated_4), value);
	}

	inline static int32_t get_offset_of_NodeList_5() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ___NodeList_5)); }
	inline List_1_t311D38FDCB31B0F0591BB5DD3DEA76A5BC1639F7 * get_NodeList_5() const { return ___NodeList_5; }
	inline List_1_t311D38FDCB31B0F0591BB5DD3DEA76A5BC1639F7 ** get_address_of_NodeList_5() { return &___NodeList_5; }
	inline void set_NodeList_5(List_1_t311D38FDCB31B0F0591BB5DD3DEA76A5BC1639F7 * value)
	{
		___NodeList_5 = value;
		Il2CppCodeGenWriteBarrier((&___NodeList_5), value);
	}

	inline static int32_t get_offset_of_SurfaceType_6() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ___SurfaceType_6)); }
	inline int32_t get_SurfaceType_6() const { return ___SurfaceType_6; }
	inline int32_t* get_address_of_SurfaceType_6() { return &___SurfaceType_6; }
	inline void set_SurfaceType_6(int32_t value)
	{
		___SurfaceType_6 = value;
	}

	inline static int32_t get_offset_of_SortType_7() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ___SortType_7)); }
	inline int32_t get_SortType_7() const { return ___SortType_7; }
	inline int32_t* get_address_of_SortType_7() { return &___SortType_7; }
	inline void set_SortType_7(int32_t value)
	{
		___SortType_7 = value;
	}

	inline static int32_t get_offset_of_OrientType_8() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ___OrientType_8)); }
	inline int32_t get_OrientType_8() const { return ___OrientType_8; }
	inline int32_t* get_address_of_OrientType_8() { return &___OrientType_8; }
	inline void set_OrientType_8(int32_t value)
	{
		___OrientType_8 = value;
	}

	inline static int32_t get_offset_of_LayoutType_9() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ___LayoutType_9)); }
	inline int32_t get_LayoutType_9() const { return ___LayoutType_9; }
	inline int32_t* get_address_of_LayoutType_9() { return &___LayoutType_9; }
	inline void set_LayoutType_9(int32_t value)
	{
		___LayoutType_9 = value;
	}

	inline static int32_t get_offset_of_IgnoreInactiveTransforms_10() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ___IgnoreInactiveTransforms_10)); }
	inline bool get_IgnoreInactiveTransforms_10() const { return ___IgnoreInactiveTransforms_10; }
	inline bool* get_address_of_IgnoreInactiveTransforms_10() { return &___IgnoreInactiveTransforms_10; }
	inline void set_IgnoreInactiveTransforms_10(bool value)
	{
		___IgnoreInactiveTransforms_10 = value;
	}

	inline static int32_t get_offset_of_Radius_11() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ___Radius_11)); }
	inline float get_Radius_11() const { return ___Radius_11; }
	inline float* get_address_of_Radius_11() { return &___Radius_11; }
	inline void set_Radius_11(float value)
	{
		___Radius_11 = value;
	}

	inline static int32_t get_offset_of_Rows_12() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ___Rows_12)); }
	inline int32_t get_Rows_12() const { return ___Rows_12; }
	inline int32_t* get_address_of_Rows_12() { return &___Rows_12; }
	inline void set_Rows_12(int32_t value)
	{
		___Rows_12 = value;
	}

	inline static int32_t get_offset_of_CellWidth_13() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ___CellWidth_13)); }
	inline float get_CellWidth_13() const { return ___CellWidth_13; }
	inline float* get_address_of_CellWidth_13() { return &___CellWidth_13; }
	inline void set_CellWidth_13(float value)
	{
		___CellWidth_13 = value;
	}

	inline static int32_t get_offset_of_CellHeight_14() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ___CellHeight_14)); }
	inline float get_CellHeight_14() const { return ___CellHeight_14; }
	inline float* get_address_of_CellHeight_14() { return &___CellHeight_14; }
	inline void set_CellHeight_14(float value)
	{
		___CellHeight_14 = value;
	}

	inline static int32_t get_offset_of_SphereMesh_15() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ___SphereMesh_15)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_SphereMesh_15() const { return ___SphereMesh_15; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_SphereMesh_15() { return &___SphereMesh_15; }
	inline void set_SphereMesh_15(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___SphereMesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___SphereMesh_15), value);
	}

	inline static int32_t get_offset_of_CylinderMesh_16() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ___CylinderMesh_16)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_CylinderMesh_16() const { return ___CylinderMesh_16; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_CylinderMesh_16() { return &___CylinderMesh_16; }
	inline void set_CylinderMesh_16(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___CylinderMesh_16 = value;
		Il2CppCodeGenWriteBarrier((&___CylinderMesh_16), value);
	}

	inline static int32_t get_offset_of__columns_17() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ____columns_17)); }
	inline int32_t get__columns_17() const { return ____columns_17; }
	inline int32_t* get_address_of__columns_17() { return &____columns_17; }
	inline void set__columns_17(int32_t value)
	{
		____columns_17 = value;
	}

	inline static int32_t get_offset_of__width_18() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ____width_18)); }
	inline float get__width_18() const { return ____width_18; }
	inline float* get_address_of__width_18() { return &____width_18; }
	inline void set__width_18(float value)
	{
		____width_18 = value;
	}

	inline static int32_t get_offset_of__height_19() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ____height_19)); }
	inline float get__height_19() const { return ____height_19; }
	inline float* get_address_of__height_19() { return &____height_19; }
	inline void set__height_19(float value)
	{
		____height_19 = value;
	}

	inline static int32_t get_offset_of__circumference_20() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ____circumference_20)); }
	inline float get__circumference_20() const { return ____circumference_20; }
	inline float* get_address_of__circumference_20() { return &____circumference_20; }
	inline void set__circumference_20(float value)
	{
		____circumference_20 = value;
	}

	inline static int32_t get_offset_of__halfCell_21() { return static_cast<int32_t>(offsetof(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB, ____halfCell_21)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__halfCell_21() const { return ____halfCell_21; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__halfCell_21() { return &____halfCell_21; }
	inline void set__halfCell_21(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____halfCell_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCOLLECTION_T6FB07C8809401FD9FEEBD867A1F4044A022417EB_H
#ifndef OBJECTCOLLECTIONDYNAMIC_TBCBA691F0D90197B528822C08D696686A38254F7_H
#define OBJECTCOLLECTIONDYNAMIC_TBCBA691F0D90197B528822C08D696686A38254F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Collections.ObjectCollectionDynamic
struct  ObjectCollectionDynamic_tBCBA691F0D90197B528822C08D696686A38254F7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MPAR.Common.Collections.ObjectCollectionDynamic/BehaviorEnum MPAR.Common.Collections.ObjectCollectionDynamic::Behavior
	int32_t ___Behavior_4;
	// System.Collections.Generic.List`1<MPAR.Common.Collections.ObjectCollectionDynamic/CollectionNodeDynamic> MPAR.Common.Collections.ObjectCollectionDynamic::DynamicNodeList
	List_1_t33ED8D1DCBA5E6C17BA48FF44566430BF4C85476 * ___DynamicNodeList_5;
	// MPAR.Common.Collections.ObjectCollection MPAR.Common.Collections.ObjectCollectionDynamic::collection
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * ___collection_6;

public:
	inline static int32_t get_offset_of_Behavior_4() { return static_cast<int32_t>(offsetof(ObjectCollectionDynamic_tBCBA691F0D90197B528822C08D696686A38254F7, ___Behavior_4)); }
	inline int32_t get_Behavior_4() const { return ___Behavior_4; }
	inline int32_t* get_address_of_Behavior_4() { return &___Behavior_4; }
	inline void set_Behavior_4(int32_t value)
	{
		___Behavior_4 = value;
	}

	inline static int32_t get_offset_of_DynamicNodeList_5() { return static_cast<int32_t>(offsetof(ObjectCollectionDynamic_tBCBA691F0D90197B528822C08D696686A38254F7, ___DynamicNodeList_5)); }
	inline List_1_t33ED8D1DCBA5E6C17BA48FF44566430BF4C85476 * get_DynamicNodeList_5() const { return ___DynamicNodeList_5; }
	inline List_1_t33ED8D1DCBA5E6C17BA48FF44566430BF4C85476 ** get_address_of_DynamicNodeList_5() { return &___DynamicNodeList_5; }
	inline void set_DynamicNodeList_5(List_1_t33ED8D1DCBA5E6C17BA48FF44566430BF4C85476 * value)
	{
		___DynamicNodeList_5 = value;
		Il2CppCodeGenWriteBarrier((&___DynamicNodeList_5), value);
	}

	inline static int32_t get_offset_of_collection_6() { return static_cast<int32_t>(offsetof(ObjectCollectionDynamic_tBCBA691F0D90197B528822C08D696686A38254F7, ___collection_6)); }
	inline ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * get_collection_6() const { return ___collection_6; }
	inline ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB ** get_address_of_collection_6() { return &___collection_6; }
	inline void set_collection_6(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * value)
	{
		___collection_6 = value;
		Il2CppCodeGenWriteBarrier((&___collection_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCOLLECTIONDYNAMIC_TBCBA691F0D90197B528822C08D696686A38254F7_H
#ifndef SINGLETON_1_TA72674132541C5E55463F5DE02560212461CFE0D_H
#define SINGLETON_1_TA72674132541C5E55463F5DE02560212461CFE0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Singleton`1<MPAR.Common.Scripting.ScriptLoader>
struct  Singleton_1_tA72674132541C5E55463F5DE02560212461CFE0D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_tA72674132541C5E55463F5DE02560212461CFE0D_StaticFields
{
public:
	// T MPAR.Common.GameObjects.Singleton`1::instance
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * ___instance_4;
	// System.Boolean MPAR.Common.GameObjects.Singleton`1::searchForInstance
	bool ___searchForInstance_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_tA72674132541C5E55463F5DE02560212461CFE0D_StaticFields, ___instance_4)); }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * get_instance_4() const { return ___instance_4; }
	inline ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_searchForInstance_5() { return static_cast<int32_t>(offsetof(Singleton_1_tA72674132541C5E55463F5DE02560212461CFE0D_StaticFields, ___searchForInstance_5)); }
	inline bool get_searchForInstance_5() const { return ___searchForInstance_5; }
	inline bool* get_address_of_searchForInstance_5() { return &___searchForInstance_5; }
	inline void set_searchForInstance_5(bool value)
	{
		___searchForInstance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_TA72674132541C5E55463F5DE02560212461CFE0D_H
#ifndef SINGLETON_1_T7D6B46F083A5562768796682F79B6B11E347F43F_H
#define SINGLETON_1_T7D6B46F083A5562768796682F79B6B11E347F43F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Singleton`1<MPAR.Common.Scripting.ScriptManager>
struct  Singleton_1_t7D6B46F083A5562768796682F79B6B11E347F43F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t7D6B46F083A5562768796682F79B6B11E347F43F_StaticFields
{
public:
	// T MPAR.Common.GameObjects.Singleton`1::instance
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C * ___instance_4;
	// System.Boolean MPAR.Common.GameObjects.Singleton`1::searchForInstance
	bool ___searchForInstance_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_t7D6B46F083A5562768796682F79B6B11E347F43F_StaticFields, ___instance_4)); }
	inline ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C * get_instance_4() const { return ___instance_4; }
	inline ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_searchForInstance_5() { return static_cast<int32_t>(offsetof(Singleton_1_t7D6B46F083A5562768796682F79B6B11E347F43F_StaticFields, ___searchForInstance_5)); }
	inline bool get_searchForInstance_5() const { return ___searchForInstance_5; }
	inline bool* get_address_of_searchForInstance_5() { return &___searchForInstance_5; }
	inline void set_searchForInstance_5(bool value)
	{
		___searchForInstance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T7D6B46F083A5562768796682F79B6B11E347F43F_H
#ifndef MIXIPLYOBJECT_T13708040370D4F8E9A32AA4F1A17E08D988C7E7B_H
#define MIXIPLYOBJECT_T13708040370D4F8E9A32AA4F1A17E08D988C7E7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.API.Objects.MixiplyObject
struct  MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXIPLYOBJECT_T13708040370D4F8E9A32AA4F1A17E08D988C7E7B_H
#ifndef INSTRUCTIONCOMPONENT_T8CB36B2C94C806D8014D2F77D6D1A8586518021D_H
#define INSTRUCTIONCOMPONENT_T8CB36B2C94C806D8014D2F77D6D1A8586518021D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.Behaviours.InstructionComponent
struct  InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject MPAR.Common.Scripting.Behaviours.InstructionComponent::<GameObject>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CGameObjectU3Ek__BackingField_4;
	// System.Object MPAR.Common.Scripting.Behaviours.InstructionComponent::<state>k__BackingField
	RuntimeObject * ___U3CstateU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<System.Func`3<Jint.Native.JsValue,Jint.Native.JsValue[],Jint.Native.JsValue>> MPAR.Common.Scripting.Behaviours.InstructionComponent::<Behaviours>k__BackingField
	List_1_t76817557DA51974CB26A8BD0DD371A638D54DA52 * ___U3CBehavioursU3Ek__BackingField_6;
	// Jint.Engine MPAR.Common.Scripting.Behaviours.InstructionComponent::<Engine>k__BackingField
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ___U3CEngineU3Ek__BackingField_7;
	// MPAR.Common.Input.Utility.GestureManager MPAR.Common.Scripting.Behaviours.InstructionComponent::<Gestures>k__BackingField
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * ___U3CGesturesU3Ek__BackingField_8;
	// UnityEngine.Camera MPAR.Common.Scripting.Behaviours.InstructionComponent::<MainCamera>k__BackingField
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___U3CMainCameraU3Ek__BackingField_9;
	// MPAR.Common.Scripting.Collidable MPAR.Common.Scripting.Behaviours.InstructionComponent::<Collidable>k__BackingField
	Collidable_t2DD800E21B45929FBAEBDCE305BB8E65236D4854 * ___U3CCollidableU3Ek__BackingField_10;
	// UnityEngine.Renderer MPAR.Common.Scripting.Behaviours.InstructionComponent::<CachedRenderer>k__BackingField
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___U3CCachedRendererU3Ek__BackingField_11;
	// System.Boolean MPAR.Common.Scripting.Behaviours.InstructionComponent::transparentShader
	bool ___transparentShader_12;
	// UnityEngine.Vector3 MPAR.Common.Scripting.Behaviours.InstructionComponent::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_13;
	// UnityEngine.Vector3 MPAR.Common.Scripting.Behaviours.InstructionComponent::rotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rotation_14;
	// UnityEngine.Vector3 MPAR.Common.Scripting.Behaviours.InstructionComponent::scale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___scale_15;
	// System.String MPAR.Common.Scripting.Behaviours.InstructionComponent::uniqueId
	String_t* ___uniqueId_16;

public:
	inline static int32_t get_offset_of_U3CGameObjectU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D, ___U3CGameObjectU3Ek__BackingField_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CGameObjectU3Ek__BackingField_4() const { return ___U3CGameObjectU3Ek__BackingField_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CGameObjectU3Ek__BackingField_4() { return &___U3CGameObjectU3Ek__BackingField_4; }
	inline void set_U3CGameObjectU3Ek__BackingField_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CGameObjectU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CstateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D, ___U3CstateU3Ek__BackingField_5)); }
	inline RuntimeObject * get_U3CstateU3Ek__BackingField_5() const { return ___U3CstateU3Ek__BackingField_5; }
	inline RuntimeObject ** get_address_of_U3CstateU3Ek__BackingField_5() { return &___U3CstateU3Ek__BackingField_5; }
	inline void set_U3CstateU3Ek__BackingField_5(RuntimeObject * value)
	{
		___U3CstateU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstateU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CBehavioursU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D, ___U3CBehavioursU3Ek__BackingField_6)); }
	inline List_1_t76817557DA51974CB26A8BD0DD371A638D54DA52 * get_U3CBehavioursU3Ek__BackingField_6() const { return ___U3CBehavioursU3Ek__BackingField_6; }
	inline List_1_t76817557DA51974CB26A8BD0DD371A638D54DA52 ** get_address_of_U3CBehavioursU3Ek__BackingField_6() { return &___U3CBehavioursU3Ek__BackingField_6; }
	inline void set_U3CBehavioursU3Ek__BackingField_6(List_1_t76817557DA51974CB26A8BD0DD371A638D54DA52 * value)
	{
		___U3CBehavioursU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBehavioursU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CEngineU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D, ___U3CEngineU3Ek__BackingField_7)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get_U3CEngineU3Ek__BackingField_7() const { return ___U3CEngineU3Ek__BackingField_7; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of_U3CEngineU3Ek__BackingField_7() { return &___U3CEngineU3Ek__BackingField_7; }
	inline void set_U3CEngineU3Ek__BackingField_7(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		___U3CEngineU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEngineU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CGesturesU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D, ___U3CGesturesU3Ek__BackingField_8)); }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * get_U3CGesturesU3Ek__BackingField_8() const { return ___U3CGesturesU3Ek__BackingField_8; }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 ** get_address_of_U3CGesturesU3Ek__BackingField_8() { return &___U3CGesturesU3Ek__BackingField_8; }
	inline void set_U3CGesturesU3Ek__BackingField_8(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * value)
	{
		___U3CGesturesU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGesturesU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CMainCameraU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D, ___U3CMainCameraU3Ek__BackingField_9)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_U3CMainCameraU3Ek__BackingField_9() const { return ___U3CMainCameraU3Ek__BackingField_9; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_U3CMainCameraU3Ek__BackingField_9() { return &___U3CMainCameraU3Ek__BackingField_9; }
	inline void set_U3CMainCameraU3Ek__BackingField_9(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___U3CMainCameraU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMainCameraU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CCollidableU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D, ___U3CCollidableU3Ek__BackingField_10)); }
	inline Collidable_t2DD800E21B45929FBAEBDCE305BB8E65236D4854 * get_U3CCollidableU3Ek__BackingField_10() const { return ___U3CCollidableU3Ek__BackingField_10; }
	inline Collidable_t2DD800E21B45929FBAEBDCE305BB8E65236D4854 ** get_address_of_U3CCollidableU3Ek__BackingField_10() { return &___U3CCollidableU3Ek__BackingField_10; }
	inline void set_U3CCollidableU3Ek__BackingField_10(Collidable_t2DD800E21B45929FBAEBDCE305BB8E65236D4854 * value)
	{
		___U3CCollidableU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollidableU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CCachedRendererU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D, ___U3CCachedRendererU3Ek__BackingField_11)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_U3CCachedRendererU3Ek__BackingField_11() const { return ___U3CCachedRendererU3Ek__BackingField_11; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_U3CCachedRendererU3Ek__BackingField_11() { return &___U3CCachedRendererU3Ek__BackingField_11; }
	inline void set_U3CCachedRendererU3Ek__BackingField_11(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___U3CCachedRendererU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCachedRendererU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_transparentShader_12() { return static_cast<int32_t>(offsetof(InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D, ___transparentShader_12)); }
	inline bool get_transparentShader_12() const { return ___transparentShader_12; }
	inline bool* get_address_of_transparentShader_12() { return &___transparentShader_12; }
	inline void set_transparentShader_12(bool value)
	{
		___transparentShader_12 = value;
	}

	inline static int32_t get_offset_of_position_13() { return static_cast<int32_t>(offsetof(InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D, ___position_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_13() const { return ___position_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_13() { return &___position_13; }
	inline void set_position_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_13 = value;
	}

	inline static int32_t get_offset_of_rotation_14() { return static_cast<int32_t>(offsetof(InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D, ___rotation_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rotation_14() const { return ___rotation_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rotation_14() { return &___rotation_14; }
	inline void set_rotation_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rotation_14 = value;
	}

	inline static int32_t get_offset_of_scale_15() { return static_cast<int32_t>(offsetof(InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D, ___scale_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_scale_15() const { return ___scale_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_scale_15() { return &___scale_15; }
	inline void set_scale_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___scale_15 = value;
	}

	inline static int32_t get_offset_of_uniqueId_16() { return static_cast<int32_t>(offsetof(InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D, ___uniqueId_16)); }
	inline String_t* get_uniqueId_16() const { return ___uniqueId_16; }
	inline String_t** get_address_of_uniqueId_16() { return &___uniqueId_16; }
	inline void set_uniqueId_16(String_t* value)
	{
		___uniqueId_16 = value;
		Il2CppCodeGenWriteBarrier((&___uniqueId_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTRUCTIONCOMPONENT_T8CB36B2C94C806D8014D2F77D6D1A8586518021D_H
#ifndef COLLIDABLE_T2DD800E21B45929FBAEBDCE305BB8E65236D4854_H
#define COLLIDABLE_T2DD800E21B45929FBAEBDCE305BB8E65236D4854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.Collidable
struct  Collidable_t2DD800E21B45929FBAEBDCE305BB8E65236D4854  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action`1<MPAR.Common.Scripting.API.Objects.MixiplyObject> MPAR.Common.Scripting.Collidable::<Action>k__BackingField
	Action_1_tBCC67358887F2268280F2E2041859B030AC4FBF8 * ___U3CActionU3Ek__BackingField_4;
	// MPAR.Common.Scripting.API.Objects.MixiplyObject MPAR.Common.Scripting.Collidable::<CollisionToConsume>k__BackingField
	MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B * ___U3CCollisionToConsumeU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CActionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Collidable_t2DD800E21B45929FBAEBDCE305BB8E65236D4854, ___U3CActionU3Ek__BackingField_4)); }
	inline Action_1_tBCC67358887F2268280F2E2041859B030AC4FBF8 * get_U3CActionU3Ek__BackingField_4() const { return ___U3CActionU3Ek__BackingField_4; }
	inline Action_1_tBCC67358887F2268280F2E2041859B030AC4FBF8 ** get_address_of_U3CActionU3Ek__BackingField_4() { return &___U3CActionU3Ek__BackingField_4; }
	inline void set_U3CActionU3Ek__BackingField_4(Action_1_tBCC67358887F2268280F2E2041859B030AC4FBF8 * value)
	{
		___U3CActionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActionU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCollisionToConsumeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Collidable_t2DD800E21B45929FBAEBDCE305BB8E65236D4854, ___U3CCollisionToConsumeU3Ek__BackingField_5)); }
	inline MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B * get_U3CCollisionToConsumeU3Ek__BackingField_5() const { return ___U3CCollisionToConsumeU3Ek__BackingField_5; }
	inline MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B ** get_address_of_U3CCollisionToConsumeU3Ek__BackingField_5() { return &___U3CCollisionToConsumeU3Ek__BackingField_5; }
	inline void set_U3CCollisionToConsumeU3Ek__BackingField_5(MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B * value)
	{
		___U3CCollisionToConsumeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionToConsumeU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDABLE_T2DD800E21B45929FBAEBDCE305BB8E65236D4854_H
#ifndef LOADEDASSETS_T00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A_H
#define LOADEDASSETS_T00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.LoadedAssets
struct  LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AssetBundle> MPAR.Common.Scripting.LoadedAssets::<AssetBundles>k__BackingField
	Dictionary_2_t9119D8967869E96A9D001B5D96F65C462E234BEA * ___U3CAssetBundlesU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,MPAR.Common.AssetLoader.IAsset> MPAR.Common.Scripting.LoadedAssets::<Assets>k__BackingField
	Dictionary_2_t495588211E81426E685BBD6CC05F4597E607BA76 * ___U3CAssetsU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<System.String> MPAR.Common.Scripting.LoadedAssets::<LoadingAssets>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CLoadingAssetsU3Ek__BackingField_6;
	// MPAR.Common.Persistence.CacheManager MPAR.Common.Scripting.LoadedAssets::<CacheManager>k__BackingField
	CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * ___U3CCacheManagerU3Ek__BackingField_7;
	// MPAR.Common.Persistence.ISaveLoadManager MPAR.Common.Scripting.LoadedAssets::<SaveLoadManager>k__BackingField
	RuntimeObject* ___U3CSaveLoadManagerU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CAssetBundlesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A, ___U3CAssetBundlesU3Ek__BackingField_4)); }
	inline Dictionary_2_t9119D8967869E96A9D001B5D96F65C462E234BEA * get_U3CAssetBundlesU3Ek__BackingField_4() const { return ___U3CAssetBundlesU3Ek__BackingField_4; }
	inline Dictionary_2_t9119D8967869E96A9D001B5D96F65C462E234BEA ** get_address_of_U3CAssetBundlesU3Ek__BackingField_4() { return &___U3CAssetBundlesU3Ek__BackingField_4; }
	inline void set_U3CAssetBundlesU3Ek__BackingField_4(Dictionary_2_t9119D8967869E96A9D001B5D96F65C462E234BEA * value)
	{
		___U3CAssetBundlesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAssetBundlesU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CAssetsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A, ___U3CAssetsU3Ek__BackingField_5)); }
	inline Dictionary_2_t495588211E81426E685BBD6CC05F4597E607BA76 * get_U3CAssetsU3Ek__BackingField_5() const { return ___U3CAssetsU3Ek__BackingField_5; }
	inline Dictionary_2_t495588211E81426E685BBD6CC05F4597E607BA76 ** get_address_of_U3CAssetsU3Ek__BackingField_5() { return &___U3CAssetsU3Ek__BackingField_5; }
	inline void set_U3CAssetsU3Ek__BackingField_5(Dictionary_2_t495588211E81426E685BBD6CC05F4597E607BA76 * value)
	{
		___U3CAssetsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAssetsU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CLoadingAssetsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A, ___U3CLoadingAssetsU3Ek__BackingField_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CLoadingAssetsU3Ek__BackingField_6() const { return ___U3CLoadingAssetsU3Ek__BackingField_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CLoadingAssetsU3Ek__BackingField_6() { return &___U3CLoadingAssetsU3Ek__BackingField_6; }
	inline void set_U3CLoadingAssetsU3Ek__BackingField_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CLoadingAssetsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadingAssetsU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CCacheManagerU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A, ___U3CCacheManagerU3Ek__BackingField_7)); }
	inline CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * get_U3CCacheManagerU3Ek__BackingField_7() const { return ___U3CCacheManagerU3Ek__BackingField_7; }
	inline CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B ** get_address_of_U3CCacheManagerU3Ek__BackingField_7() { return &___U3CCacheManagerU3Ek__BackingField_7; }
	inline void set_U3CCacheManagerU3Ek__BackingField_7(CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * value)
	{
		___U3CCacheManagerU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCacheManagerU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CSaveLoadManagerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A, ___U3CSaveLoadManagerU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CSaveLoadManagerU3Ek__BackingField_8() const { return ___U3CSaveLoadManagerU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CSaveLoadManagerU3Ek__BackingField_8() { return &___U3CSaveLoadManagerU3Ek__BackingField_8; }
	inline void set_U3CSaveLoadManagerU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CSaveLoadManagerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSaveLoadManagerU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADEDASSETS_T00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A_H
#ifndef APIKEYPROJECTLOADER_TF51E2E3FEA6D64E61EEFF57B60D394B69E5CCDCC_H
#define APIKEYPROJECTLOADER_TF51E2E3FEA6D64E61EEFF57B60D394B69E5CCDCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.Loader.ApiKeyProjectLoader
struct  ApiKeyProjectLoader_tF51E2E3FEA6D64E61EEFF57B60D394B69E5CCDCC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String MPAR.Common.Scripting.Loader.ApiKeyProjectLoader::baseUrl
	String_t* ___baseUrl_4;

public:
	inline static int32_t get_offset_of_baseUrl_4() { return static_cast<int32_t>(offsetof(ApiKeyProjectLoader_tF51E2E3FEA6D64E61EEFF57B60D394B69E5CCDCC, ___baseUrl_4)); }
	inline String_t* get_baseUrl_4() const { return ___baseUrl_4; }
	inline String_t** get_address_of_baseUrl_4() { return &___baseUrl_4; }
	inline void set_baseUrl_4(String_t* value)
	{
		___baseUrl_4 = value;
		Il2CppCodeGenWriteBarrier((&___baseUrl_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIKEYPROJECTLOADER_TF51E2E3FEA6D64E61EEFF57B60D394B69E5CCDCC_H
#ifndef SCREENSHOTQRCODEPROJECTLOADER_T547BA35CCEF84AB874655BD630F0D1A8CE5CE386_H
#define SCREENSHOTQRCODEPROJECTLOADER_T547BA35CCEF84AB874655BD630F0D1A8CE5CE386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.Loader.ScreenshotQRCodeProjectLoader
struct  ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean MPAR.Common.Scripting.Loader.ScreenshotQRCodeProjectLoader::<Running>k__BackingField
	bool ___U3CRunningU3Ek__BackingField_4;
	// System.Func`3<System.String,System.String,System.Collections.IEnumerator> MPAR.Common.Scripting.Loader.ScreenshotQRCodeProjectLoader::<Callback>k__BackingField
	Func_3_t5D19D2F5D175E4953F2471BB6690A6BF7579C653 * ___U3CCallbackU3Ek__BackingField_5;
	// System.Single MPAR.Common.Scripting.Loader.ScreenshotQRCodeProjectLoader::<LastInvoke>k__BackingField
	float ___U3CLastInvokeU3Ek__BackingField_6;
	// System.Single MPAR.Common.Scripting.Loader.ScreenshotQRCodeProjectLoader::<InvokeInterval>k__BackingField
	float ___U3CInvokeIntervalU3Ek__BackingField_7;
	// UnityEngine.Texture2D MPAR.Common.Scripting.Loader.ScreenshotQRCodeProjectLoader::<Image>k__BackingField
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___U3CImageU3Ek__BackingField_8;
	// BarcodeScanner.Parser.ZXingParser MPAR.Common.Scripting.Loader.ScreenshotQRCodeProjectLoader::<Parser>k__BackingField
	ZXingParser_t7B9AC8160C3A1565FA96F8714D30B9C7E852DE26 * ___U3CParserU3Ek__BackingField_9;
	// System.String MPAR.Common.Scripting.Loader.ScreenshotQRCodeProjectLoader::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_10;
	// System.Boolean MPAR.Common.Scripting.Loader.ScreenshotQRCodeProjectLoader::InProgress
	bool ___InProgress_11;

public:
	inline static int32_t get_offset_of_U3CRunningU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386, ___U3CRunningU3Ek__BackingField_4)); }
	inline bool get_U3CRunningU3Ek__BackingField_4() const { return ___U3CRunningU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CRunningU3Ek__BackingField_4() { return &___U3CRunningU3Ek__BackingField_4; }
	inline void set_U3CRunningU3Ek__BackingField_4(bool value)
	{
		___U3CRunningU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386, ___U3CCallbackU3Ek__BackingField_5)); }
	inline Func_3_t5D19D2F5D175E4953F2471BB6690A6BF7579C653 * get_U3CCallbackU3Ek__BackingField_5() const { return ___U3CCallbackU3Ek__BackingField_5; }
	inline Func_3_t5D19D2F5D175E4953F2471BB6690A6BF7579C653 ** get_address_of_U3CCallbackU3Ek__BackingField_5() { return &___U3CCallbackU3Ek__BackingField_5; }
	inline void set_U3CCallbackU3Ek__BackingField_5(Func_3_t5D19D2F5D175E4953F2471BB6690A6BF7579C653 * value)
	{
		___U3CCallbackU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CLastInvokeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386, ___U3CLastInvokeU3Ek__BackingField_6)); }
	inline float get_U3CLastInvokeU3Ek__BackingField_6() const { return ___U3CLastInvokeU3Ek__BackingField_6; }
	inline float* get_address_of_U3CLastInvokeU3Ek__BackingField_6() { return &___U3CLastInvokeU3Ek__BackingField_6; }
	inline void set_U3CLastInvokeU3Ek__BackingField_6(float value)
	{
		___U3CLastInvokeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CInvokeIntervalU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386, ___U3CInvokeIntervalU3Ek__BackingField_7)); }
	inline float get_U3CInvokeIntervalU3Ek__BackingField_7() const { return ___U3CInvokeIntervalU3Ek__BackingField_7; }
	inline float* get_address_of_U3CInvokeIntervalU3Ek__BackingField_7() { return &___U3CInvokeIntervalU3Ek__BackingField_7; }
	inline void set_U3CInvokeIntervalU3Ek__BackingField_7(float value)
	{
		___U3CInvokeIntervalU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CImageU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386, ___U3CImageU3Ek__BackingField_8)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_U3CImageU3Ek__BackingField_8() const { return ___U3CImageU3Ek__BackingField_8; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_U3CImageU3Ek__BackingField_8() { return &___U3CImageU3Ek__BackingField_8; }
	inline void set_U3CImageU3Ek__BackingField_8(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___U3CImageU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CParserU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386, ___U3CParserU3Ek__BackingField_9)); }
	inline ZXingParser_t7B9AC8160C3A1565FA96F8714D30B9C7E852DE26 * get_U3CParserU3Ek__BackingField_9() const { return ___U3CParserU3Ek__BackingField_9; }
	inline ZXingParser_t7B9AC8160C3A1565FA96F8714D30B9C7E852DE26 ** get_address_of_U3CParserU3Ek__BackingField_9() { return &___U3CParserU3Ek__BackingField_9; }
	inline void set_U3CParserU3Ek__BackingField_9(ZXingParser_t7B9AC8160C3A1565FA96F8714D30B9C7E852DE26 * value)
	{
		___U3CParserU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParserU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386, ___U3CValueU3Ek__BackingField_10)); }
	inline String_t* get_U3CValueU3Ek__BackingField_10() const { return ___U3CValueU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_10() { return &___U3CValueU3Ek__BackingField_10; }
	inline void set_U3CValueU3Ek__BackingField_10(String_t* value)
	{
		___U3CValueU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_InProgress_11() { return static_cast<int32_t>(offsetof(ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386, ___InProgress_11)); }
	inline bool get_InProgress_11() const { return ___InProgress_11; }
	inline bool* get_address_of_InProgress_11() { return &___InProgress_11; }
	inline void set_InProgress_11(bool value)
	{
		___InProgress_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSHOTQRCODEPROJECTLOADER_T547BA35CCEF84AB874655BD630F0D1A8CE5CE386_H
#ifndef STATICPROJECTLOADER_T9B6ADCD33AC4B4D5035D11C21B3E67406CF9C92F_H
#define STATICPROJECTLOADER_T9B6ADCD33AC4B4D5035D11C21B3E67406CF9C92F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.Loader.StaticProjectLoader
struct  StaticProjectLoader_t9B6ADCD33AC4B4D5035D11C21B3E67406CF9C92F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICPROJECTLOADER_T9B6ADCD33AC4B4D5035D11C21B3E67406CF9C92F_H
#ifndef VIDEORECORDER_TAD0FB133532C7B99B421C2DF4B850C14BF815AF6_H
#define VIDEORECORDER_TAD0FB133532C7B99B421C2DF4B850C14BF815AF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.VideoRecorder
struct  VideoRecorder_tAD0FB133532C7B99B421C2DF4B850C14BF815AF6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// RenderHeads.Media.AVProMovieCapture.CaptureFromScreen MPAR.Common.Scripting.VideoRecorder::<Capture>k__BackingField
	CaptureFromScreen_tEB468CAE6962D15FD6A3F718B595BDF64D70007D * ___U3CCaptureU3Ek__BackingField_4;
	// System.Single MPAR.Common.Scripting.VideoRecorder::_captureStartTime
	float ____captureStartTime_5;
	// System.Boolean MPAR.Common.Scripting.VideoRecorder::_recording
	bool ____recording_6;

public:
	inline static int32_t get_offset_of_U3CCaptureU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(VideoRecorder_tAD0FB133532C7B99B421C2DF4B850C14BF815AF6, ___U3CCaptureU3Ek__BackingField_4)); }
	inline CaptureFromScreen_tEB468CAE6962D15FD6A3F718B595BDF64D70007D * get_U3CCaptureU3Ek__BackingField_4() const { return ___U3CCaptureU3Ek__BackingField_4; }
	inline CaptureFromScreen_tEB468CAE6962D15FD6A3F718B595BDF64D70007D ** get_address_of_U3CCaptureU3Ek__BackingField_4() { return &___U3CCaptureU3Ek__BackingField_4; }
	inline void set_U3CCaptureU3Ek__BackingField_4(CaptureFromScreen_tEB468CAE6962D15FD6A3F718B595BDF64D70007D * value)
	{
		___U3CCaptureU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCaptureU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of__captureStartTime_5() { return static_cast<int32_t>(offsetof(VideoRecorder_tAD0FB133532C7B99B421C2DF4B850C14BF815AF6, ____captureStartTime_5)); }
	inline float get__captureStartTime_5() const { return ____captureStartTime_5; }
	inline float* get_address_of__captureStartTime_5() { return &____captureStartTime_5; }
	inline void set__captureStartTime_5(float value)
	{
		____captureStartTime_5 = value;
	}

	inline static int32_t get_offset_of__recording_6() { return static_cast<int32_t>(offsetof(VideoRecorder_tAD0FB133532C7B99B421C2DF4B850C14BF815AF6, ____recording_6)); }
	inline bool get__recording_6() const { return ____recording_6; }
	inline bool* get_address_of__recording_6() { return &____recording_6; }
	inline void set__recording_6(bool value)
	{
		____recording_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEORECORDER_TAD0FB133532C7B99B421C2DF4B850C14BF815AF6_H
#ifndef MONOINSTALLERBASE_TC2BC7AF08360C9F8D2703E955C0D5EC9AF4C62DF_H
#define MONOINSTALLERBASE_TC2BC7AF08360C9F8D2703E955C0D5EC9AF4C62DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoInstallerBase
struct  MonoInstallerBase_tC2BC7AF08360C9F8D2703E955C0D5EC9AF4C62DF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Zenject.DiContainer Zenject.MonoInstallerBase::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_4;

public:
	inline static int32_t get_offset_of__container_4() { return static_cast<int32_t>(offsetof(MonoInstallerBase_tC2BC7AF08360C9F8D2703E955C0D5EC9AF4C62DF, ____container_4)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_4() const { return ____container_4; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_4() { return &____container_4; }
	inline void set__container_4(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_4 = value;
		Il2CppCodeGenWriteBarrier((&____container_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOINSTALLERBASE_TC2BC7AF08360C9F8D2703E955C0D5EC9AF4C62DF_H
#ifndef SCRIPTLOADER_TE0646E44B177F7A0B8DD42619E6909245286F2F2_H
#define SCRIPTLOADER_TE0646E44B177F7A0B8DD42619E6909245286F2F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.ScriptLoader
struct  ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2  : public Singleton_1_tA72674132541C5E55463F5DE02560212461CFE0D
{
public:
	// MPAR.Common.AssetLoader.IPlatformDependentAssetLoader MPAR.Common.Scripting.ScriptLoader::assets
	RuntimeObject* ___assets_6;
	// MPAR.Common.UI.IPlatformDependentScriptMenu MPAR.Common.Scripting.ScriptLoader::scriptMenu
	RuntimeObject* ___scriptMenu_7;
	// MPAR.Common.GameObjects.Factories.InScriptMenuFactory MPAR.Common.Scripting.ScriptLoader::menuFactory
	InScriptMenuFactory_tE28604EF9A7748F0689F833E699CABF93F4DA7B9 * ___menuFactory_8;
	// MPAR.Common.Persistence.ISaveLoadManager MPAR.Common.Scripting.ScriptLoader::saver
	RuntimeObject* ___saver_9;
	// MPAR.Common.Persistence.CacheManager MPAR.Common.Scripting.ScriptLoader::<CacheManager>k__BackingField
	CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * ___U3CCacheManagerU3Ek__BackingField_10;
	// MPAR.Common.Scripting.Loader.IProjectLoader MPAR.Common.Scripting.ScriptLoader::<ProjectLoader>k__BackingField
	RuntimeObject* ___U3CProjectLoaderU3Ek__BackingField_11;
	// MPAR.Common.Persistence.ISaveLoadManager MPAR.Common.Scripting.ScriptLoader::<SaveLoadManager>k__BackingField
	RuntimeObject* ___U3CSaveLoadManagerU3Ek__BackingField_12;
	// MPAR.Common.UI.IPlatformDependentScriptMenu MPAR.Common.Scripting.ScriptLoader::<ScriptMenu>k__BackingField
	RuntimeObject* ___U3CScriptMenuU3Ek__BackingField_13;
	// System.String MPAR.Common.Scripting.ScriptLoader::path
	String_t* ___path_14;
	// System.String MPAR.Common.Scripting.ScriptLoader::autoRunScriptUrl
	String_t* ___autoRunScriptUrl_15;
	// System.String MPAR.Common.Scripting.ScriptLoader::preloadedBaseUrl
	String_t* ___preloadedBaseUrl_16;
	// System.String MPAR.Common.Scripting.ScriptLoader::preloadedAutoRunUrl
	String_t* ___preloadedAutoRunUrl_17;
	// System.Boolean MPAR.Common.Scripting.ScriptLoader::startedLoading
	bool ___startedLoading_18;

public:
	inline static int32_t get_offset_of_assets_6() { return static_cast<int32_t>(offsetof(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2, ___assets_6)); }
	inline RuntimeObject* get_assets_6() const { return ___assets_6; }
	inline RuntimeObject** get_address_of_assets_6() { return &___assets_6; }
	inline void set_assets_6(RuntimeObject* value)
	{
		___assets_6 = value;
		Il2CppCodeGenWriteBarrier((&___assets_6), value);
	}

	inline static int32_t get_offset_of_scriptMenu_7() { return static_cast<int32_t>(offsetof(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2, ___scriptMenu_7)); }
	inline RuntimeObject* get_scriptMenu_7() const { return ___scriptMenu_7; }
	inline RuntimeObject** get_address_of_scriptMenu_7() { return &___scriptMenu_7; }
	inline void set_scriptMenu_7(RuntimeObject* value)
	{
		___scriptMenu_7 = value;
		Il2CppCodeGenWriteBarrier((&___scriptMenu_7), value);
	}

	inline static int32_t get_offset_of_menuFactory_8() { return static_cast<int32_t>(offsetof(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2, ___menuFactory_8)); }
	inline InScriptMenuFactory_tE28604EF9A7748F0689F833E699CABF93F4DA7B9 * get_menuFactory_8() const { return ___menuFactory_8; }
	inline InScriptMenuFactory_tE28604EF9A7748F0689F833E699CABF93F4DA7B9 ** get_address_of_menuFactory_8() { return &___menuFactory_8; }
	inline void set_menuFactory_8(InScriptMenuFactory_tE28604EF9A7748F0689F833E699CABF93F4DA7B9 * value)
	{
		___menuFactory_8 = value;
		Il2CppCodeGenWriteBarrier((&___menuFactory_8), value);
	}

	inline static int32_t get_offset_of_saver_9() { return static_cast<int32_t>(offsetof(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2, ___saver_9)); }
	inline RuntimeObject* get_saver_9() const { return ___saver_9; }
	inline RuntimeObject** get_address_of_saver_9() { return &___saver_9; }
	inline void set_saver_9(RuntimeObject* value)
	{
		___saver_9 = value;
		Il2CppCodeGenWriteBarrier((&___saver_9), value);
	}

	inline static int32_t get_offset_of_U3CCacheManagerU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2, ___U3CCacheManagerU3Ek__BackingField_10)); }
	inline CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * get_U3CCacheManagerU3Ek__BackingField_10() const { return ___U3CCacheManagerU3Ek__BackingField_10; }
	inline CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B ** get_address_of_U3CCacheManagerU3Ek__BackingField_10() { return &___U3CCacheManagerU3Ek__BackingField_10; }
	inline void set_U3CCacheManagerU3Ek__BackingField_10(CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * value)
	{
		___U3CCacheManagerU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCacheManagerU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CProjectLoaderU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2, ___U3CProjectLoaderU3Ek__BackingField_11)); }
	inline RuntimeObject* get_U3CProjectLoaderU3Ek__BackingField_11() const { return ___U3CProjectLoaderU3Ek__BackingField_11; }
	inline RuntimeObject** get_address_of_U3CProjectLoaderU3Ek__BackingField_11() { return &___U3CProjectLoaderU3Ek__BackingField_11; }
	inline void set_U3CProjectLoaderU3Ek__BackingField_11(RuntimeObject* value)
	{
		___U3CProjectLoaderU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProjectLoaderU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CSaveLoadManagerU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2, ___U3CSaveLoadManagerU3Ek__BackingField_12)); }
	inline RuntimeObject* get_U3CSaveLoadManagerU3Ek__BackingField_12() const { return ___U3CSaveLoadManagerU3Ek__BackingField_12; }
	inline RuntimeObject** get_address_of_U3CSaveLoadManagerU3Ek__BackingField_12() { return &___U3CSaveLoadManagerU3Ek__BackingField_12; }
	inline void set_U3CSaveLoadManagerU3Ek__BackingField_12(RuntimeObject* value)
	{
		___U3CSaveLoadManagerU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSaveLoadManagerU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CScriptMenuU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2, ___U3CScriptMenuU3Ek__BackingField_13)); }
	inline RuntimeObject* get_U3CScriptMenuU3Ek__BackingField_13() const { return ___U3CScriptMenuU3Ek__BackingField_13; }
	inline RuntimeObject** get_address_of_U3CScriptMenuU3Ek__BackingField_13() { return &___U3CScriptMenuU3Ek__BackingField_13; }
	inline void set_U3CScriptMenuU3Ek__BackingField_13(RuntimeObject* value)
	{
		___U3CScriptMenuU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScriptMenuU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_path_14() { return static_cast<int32_t>(offsetof(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2, ___path_14)); }
	inline String_t* get_path_14() const { return ___path_14; }
	inline String_t** get_address_of_path_14() { return &___path_14; }
	inline void set_path_14(String_t* value)
	{
		___path_14 = value;
		Il2CppCodeGenWriteBarrier((&___path_14), value);
	}

	inline static int32_t get_offset_of_autoRunScriptUrl_15() { return static_cast<int32_t>(offsetof(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2, ___autoRunScriptUrl_15)); }
	inline String_t* get_autoRunScriptUrl_15() const { return ___autoRunScriptUrl_15; }
	inline String_t** get_address_of_autoRunScriptUrl_15() { return &___autoRunScriptUrl_15; }
	inline void set_autoRunScriptUrl_15(String_t* value)
	{
		___autoRunScriptUrl_15 = value;
		Il2CppCodeGenWriteBarrier((&___autoRunScriptUrl_15), value);
	}

	inline static int32_t get_offset_of_preloadedBaseUrl_16() { return static_cast<int32_t>(offsetof(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2, ___preloadedBaseUrl_16)); }
	inline String_t* get_preloadedBaseUrl_16() const { return ___preloadedBaseUrl_16; }
	inline String_t** get_address_of_preloadedBaseUrl_16() { return &___preloadedBaseUrl_16; }
	inline void set_preloadedBaseUrl_16(String_t* value)
	{
		___preloadedBaseUrl_16 = value;
		Il2CppCodeGenWriteBarrier((&___preloadedBaseUrl_16), value);
	}

	inline static int32_t get_offset_of_preloadedAutoRunUrl_17() { return static_cast<int32_t>(offsetof(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2, ___preloadedAutoRunUrl_17)); }
	inline String_t* get_preloadedAutoRunUrl_17() const { return ___preloadedAutoRunUrl_17; }
	inline String_t** get_address_of_preloadedAutoRunUrl_17() { return &___preloadedAutoRunUrl_17; }
	inline void set_preloadedAutoRunUrl_17(String_t* value)
	{
		___preloadedAutoRunUrl_17 = value;
		Il2CppCodeGenWriteBarrier((&___preloadedAutoRunUrl_17), value);
	}

	inline static int32_t get_offset_of_startedLoading_18() { return static_cast<int32_t>(offsetof(ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2, ___startedLoading_18)); }
	inline bool get_startedLoading_18() const { return ___startedLoading_18; }
	inline bool* get_address_of_startedLoading_18() { return &___startedLoading_18; }
	inline void set_startedLoading_18(bool value)
	{
		___startedLoading_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTLOADER_TE0646E44B177F7A0B8DD42619E6909245286F2F2_H
#ifndef SCRIPTMANAGER_TDDEF980C92189FD070F605511CA2EA9A04188F3C_H
#define SCRIPTMANAGER_TDDEF980C92189FD070F605511CA2EA9A04188F3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Scripting.ScriptManager
struct  ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C  : public Singleton_1_t7D6B46F083A5562768796682F79B6B11E347F43F
{
public:
	// System.Collections.Generic.List`1<Jint.Native.JsValue> MPAR.Common.Scripting.ScriptManager::<WhileLoadingMethods>k__BackingField
	List_1_t893ABB43ACE9473D58A8C90EE4E765A65D92EBFA * ___U3CWhileLoadingMethodsU3Ek__BackingField_6;
	// System.Boolean MPAR.Common.Scripting.ScriptManager::startedScript
	bool ___startedScript_7;
	// System.Boolean MPAR.Common.Scripting.ScriptManager::setupScript
	bool ___setupScript_8;
	// System.String MPAR.Common.Scripting.ScriptManager::scriptAdditions
	String_t* ___scriptAdditions_9;
	// System.Boolean MPAR.Common.Scripting.ScriptManager::<IsScriptRunning>k__BackingField
	bool ___U3CIsScriptRunningU3Ek__BackingField_10;
	// System.String MPAR.Common.Scripting.ScriptManager::<Script>k__BackingField
	String_t* ___U3CScriptU3Ek__BackingField_11;
	// MPAR.Common.Scripting.Script MPAR.Common.Scripting.ScriptManager::<ScriptInfo>k__BackingField
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___U3CScriptInfoU3Ek__BackingField_12;
	// Jint.Engine MPAR.Common.Scripting.ScriptManager::<Engine>k__BackingField
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ___U3CEngineU3Ek__BackingField_13;
	// MPAR.Common.AssetLoader.IPlatformDependentAssetLoader MPAR.Common.Scripting.ScriptManager::<Assets>k__BackingField
	RuntimeObject* ___U3CAssetsU3Ek__BackingField_14;
	// MPAR.Common.Persistence.ISaveLoadManager MPAR.Common.Scripting.ScriptManager::<SaveLoadManager>k__BackingField
	RuntimeObject* ___U3CSaveLoadManagerU3Ek__BackingField_15;
	// MPAR.Common.Scripting.Loader.IProjectLoader MPAR.Common.Scripting.ScriptManager::<ProjectLoader>k__BackingField
	RuntimeObject* ___U3CProjectLoaderU3Ek__BackingField_16;
	// Assets._Project.Scripts.Common.Scripting.API.ICustomMenu MPAR.Common.Scripting.ScriptManager::<InScriptMenu>k__BackingField
	RuntimeObject* ___U3CInScriptMenuU3Ek__BackingField_17;
	// MPAR.Common.Scripting.LoadedAssets MPAR.Common.Scripting.ScriptManager::<LoadedAssets>k__BackingField
	LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A * ___U3CLoadedAssetsU3Ek__BackingField_18;
	// Assets._Project.Scripts.Common.Scripting.PlatformFeatures.Features MPAR.Common.Scripting.ScriptManager::<Features>k__BackingField
	Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10 * ___U3CFeaturesU3Ek__BackingField_19;
	// Assets._Project.Scripts.Common.PlaneDetection.PlatformDependentPlaneDetector MPAR.Common.Scripting.ScriptManager::<PlaneDetector>k__BackingField
	PlatformDependentPlaneDetector_tA059BB399BF4739A6CA16E50E3B2581C47FB6C1F * ___U3CPlaneDetectorU3Ek__BackingField_20;
	// MPAR.Common.UI.MenuManager MPAR.Common.Scripting.ScriptManager::<MenuManager>k__BackingField
	MenuManager_t03B7ED0689D55E85C85D1E0E239C0CF9AA5E93D6 * ___U3CMenuManagerU3Ek__BackingField_21;
	// MPAR.Common.Input.Utility.GestureManager MPAR.Common.Scripting.ScriptManager::<Gestures>k__BackingField
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * ___U3CGesturesU3Ek__BackingField_22;
	// MPAR.Common.GameObjects.Factories.InScriptMenuFactory MPAR.Common.Scripting.ScriptManager::<InScriptMenuFactory>k__BackingField
	InScriptMenuFactory_tE28604EF9A7748F0689F833E699CABF93F4DA7B9 * ___U3CInScriptMenuFactoryU3Ek__BackingField_23;
	// MPAR.Common.GameObjects.Factories.PortableMenuFactory MPAR.Common.Scripting.ScriptManager::<PortableMenuFactory>k__BackingField
	PortableMenuFactory_tAA97D5D2F11B3C26CBC04242D63940BFFBE0070D * ___U3CPortableMenuFactoryU3Ek__BackingField_24;
	// MPAR.Common.GameObjects.Factories.PortableMenuItemFactory MPAR.Common.Scripting.ScriptManager::<PortableMenuItemFactory>k__BackingField
	PortableMenuItemFactory_t1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D * ___U3CPortableMenuItemFactoryU3Ek__BackingField_25;
	// MPAR.Common.GameObjects.Factories.ExhibitBoxFactory MPAR.Common.Scripting.ScriptManager::<ExhibitBoxFactory>k__BackingField
	ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13 * ___U3CExhibitBoxFactoryU3Ek__BackingField_26;
	// Assets._Project.Scripts.Common.Scripting.ErrorHandling.ScriptErrorHandler MPAR.Common.Scripting.ScriptManager::<ScriptErrorHandler>k__BackingField
	ScriptErrorHandler_t86C54D9B0DA7092755777EFF56D5D23039FEA924 * ___U3CScriptErrorHandlerU3Ek__BackingField_27;
	// Assets._Project.Scripts.Common.Scripting.Mix.ScriptEventHandler MPAR.Common.Scripting.ScriptManager::<ScriptEventHandler>k__BackingField
	ScriptEventHandler_t6818D0F175ECC12B599C5FE2A33346CECCD49AC7 * ___U3CScriptEventHandlerU3Ek__BackingField_28;
	// Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler MPAR.Common.Scripting.ScriptManager::<ScriptInputHandler>k__BackingField
	ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC * ___U3CScriptInputHandlerU3Ek__BackingField_29;

public:
	inline static int32_t get_offset_of_U3CWhileLoadingMethodsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CWhileLoadingMethodsU3Ek__BackingField_6)); }
	inline List_1_t893ABB43ACE9473D58A8C90EE4E765A65D92EBFA * get_U3CWhileLoadingMethodsU3Ek__BackingField_6() const { return ___U3CWhileLoadingMethodsU3Ek__BackingField_6; }
	inline List_1_t893ABB43ACE9473D58A8C90EE4E765A65D92EBFA ** get_address_of_U3CWhileLoadingMethodsU3Ek__BackingField_6() { return &___U3CWhileLoadingMethodsU3Ek__BackingField_6; }
	inline void set_U3CWhileLoadingMethodsU3Ek__BackingField_6(List_1_t893ABB43ACE9473D58A8C90EE4E765A65D92EBFA * value)
	{
		___U3CWhileLoadingMethodsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWhileLoadingMethodsU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_startedScript_7() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___startedScript_7)); }
	inline bool get_startedScript_7() const { return ___startedScript_7; }
	inline bool* get_address_of_startedScript_7() { return &___startedScript_7; }
	inline void set_startedScript_7(bool value)
	{
		___startedScript_7 = value;
	}

	inline static int32_t get_offset_of_setupScript_8() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___setupScript_8)); }
	inline bool get_setupScript_8() const { return ___setupScript_8; }
	inline bool* get_address_of_setupScript_8() { return &___setupScript_8; }
	inline void set_setupScript_8(bool value)
	{
		___setupScript_8 = value;
	}

	inline static int32_t get_offset_of_scriptAdditions_9() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___scriptAdditions_9)); }
	inline String_t* get_scriptAdditions_9() const { return ___scriptAdditions_9; }
	inline String_t** get_address_of_scriptAdditions_9() { return &___scriptAdditions_9; }
	inline void set_scriptAdditions_9(String_t* value)
	{
		___scriptAdditions_9 = value;
		Il2CppCodeGenWriteBarrier((&___scriptAdditions_9), value);
	}

	inline static int32_t get_offset_of_U3CIsScriptRunningU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CIsScriptRunningU3Ek__BackingField_10)); }
	inline bool get_U3CIsScriptRunningU3Ek__BackingField_10() const { return ___U3CIsScriptRunningU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CIsScriptRunningU3Ek__BackingField_10() { return &___U3CIsScriptRunningU3Ek__BackingField_10; }
	inline void set_U3CIsScriptRunningU3Ek__BackingField_10(bool value)
	{
		___U3CIsScriptRunningU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CScriptU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CScriptU3Ek__BackingField_11)); }
	inline String_t* get_U3CScriptU3Ek__BackingField_11() const { return ___U3CScriptU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CScriptU3Ek__BackingField_11() { return &___U3CScriptU3Ek__BackingField_11; }
	inline void set_U3CScriptU3Ek__BackingField_11(String_t* value)
	{
		___U3CScriptU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScriptU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CScriptInfoU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CScriptInfoU3Ek__BackingField_12)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_U3CScriptInfoU3Ek__BackingField_12() const { return ___U3CScriptInfoU3Ek__BackingField_12; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_U3CScriptInfoU3Ek__BackingField_12() { return &___U3CScriptInfoU3Ek__BackingField_12; }
	inline void set_U3CScriptInfoU3Ek__BackingField_12(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___U3CScriptInfoU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScriptInfoU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CEngineU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CEngineU3Ek__BackingField_13)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get_U3CEngineU3Ek__BackingField_13() const { return ___U3CEngineU3Ek__BackingField_13; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of_U3CEngineU3Ek__BackingField_13() { return &___U3CEngineU3Ek__BackingField_13; }
	inline void set_U3CEngineU3Ek__BackingField_13(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		___U3CEngineU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEngineU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CAssetsU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CAssetsU3Ek__BackingField_14)); }
	inline RuntimeObject* get_U3CAssetsU3Ek__BackingField_14() const { return ___U3CAssetsU3Ek__BackingField_14; }
	inline RuntimeObject** get_address_of_U3CAssetsU3Ek__BackingField_14() { return &___U3CAssetsU3Ek__BackingField_14; }
	inline void set_U3CAssetsU3Ek__BackingField_14(RuntimeObject* value)
	{
		___U3CAssetsU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAssetsU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CSaveLoadManagerU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CSaveLoadManagerU3Ek__BackingField_15)); }
	inline RuntimeObject* get_U3CSaveLoadManagerU3Ek__BackingField_15() const { return ___U3CSaveLoadManagerU3Ek__BackingField_15; }
	inline RuntimeObject** get_address_of_U3CSaveLoadManagerU3Ek__BackingField_15() { return &___U3CSaveLoadManagerU3Ek__BackingField_15; }
	inline void set_U3CSaveLoadManagerU3Ek__BackingField_15(RuntimeObject* value)
	{
		___U3CSaveLoadManagerU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSaveLoadManagerU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CProjectLoaderU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CProjectLoaderU3Ek__BackingField_16)); }
	inline RuntimeObject* get_U3CProjectLoaderU3Ek__BackingField_16() const { return ___U3CProjectLoaderU3Ek__BackingField_16; }
	inline RuntimeObject** get_address_of_U3CProjectLoaderU3Ek__BackingField_16() { return &___U3CProjectLoaderU3Ek__BackingField_16; }
	inline void set_U3CProjectLoaderU3Ek__BackingField_16(RuntimeObject* value)
	{
		___U3CProjectLoaderU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProjectLoaderU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CInScriptMenuU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CInScriptMenuU3Ek__BackingField_17)); }
	inline RuntimeObject* get_U3CInScriptMenuU3Ek__BackingField_17() const { return ___U3CInScriptMenuU3Ek__BackingField_17; }
	inline RuntimeObject** get_address_of_U3CInScriptMenuU3Ek__BackingField_17() { return &___U3CInScriptMenuU3Ek__BackingField_17; }
	inline void set_U3CInScriptMenuU3Ek__BackingField_17(RuntimeObject* value)
	{
		___U3CInScriptMenuU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInScriptMenuU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CLoadedAssetsU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CLoadedAssetsU3Ek__BackingField_18)); }
	inline LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A * get_U3CLoadedAssetsU3Ek__BackingField_18() const { return ___U3CLoadedAssetsU3Ek__BackingField_18; }
	inline LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A ** get_address_of_U3CLoadedAssetsU3Ek__BackingField_18() { return &___U3CLoadedAssetsU3Ek__BackingField_18; }
	inline void set_U3CLoadedAssetsU3Ek__BackingField_18(LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A * value)
	{
		___U3CLoadedAssetsU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadedAssetsU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CFeaturesU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CFeaturesU3Ek__BackingField_19)); }
	inline Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10 * get_U3CFeaturesU3Ek__BackingField_19() const { return ___U3CFeaturesU3Ek__BackingField_19; }
	inline Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10 ** get_address_of_U3CFeaturesU3Ek__BackingField_19() { return &___U3CFeaturesU3Ek__BackingField_19; }
	inline void set_U3CFeaturesU3Ek__BackingField_19(Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10 * value)
	{
		___U3CFeaturesU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFeaturesU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CPlaneDetectorU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CPlaneDetectorU3Ek__BackingField_20)); }
	inline PlatformDependentPlaneDetector_tA059BB399BF4739A6CA16E50E3B2581C47FB6C1F * get_U3CPlaneDetectorU3Ek__BackingField_20() const { return ___U3CPlaneDetectorU3Ek__BackingField_20; }
	inline PlatformDependentPlaneDetector_tA059BB399BF4739A6CA16E50E3B2581C47FB6C1F ** get_address_of_U3CPlaneDetectorU3Ek__BackingField_20() { return &___U3CPlaneDetectorU3Ek__BackingField_20; }
	inline void set_U3CPlaneDetectorU3Ek__BackingField_20(PlatformDependentPlaneDetector_tA059BB399BF4739A6CA16E50E3B2581C47FB6C1F * value)
	{
		___U3CPlaneDetectorU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlaneDetectorU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CMenuManagerU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CMenuManagerU3Ek__BackingField_21)); }
	inline MenuManager_t03B7ED0689D55E85C85D1E0E239C0CF9AA5E93D6 * get_U3CMenuManagerU3Ek__BackingField_21() const { return ___U3CMenuManagerU3Ek__BackingField_21; }
	inline MenuManager_t03B7ED0689D55E85C85D1E0E239C0CF9AA5E93D6 ** get_address_of_U3CMenuManagerU3Ek__BackingField_21() { return &___U3CMenuManagerU3Ek__BackingField_21; }
	inline void set_U3CMenuManagerU3Ek__BackingField_21(MenuManager_t03B7ED0689D55E85C85D1E0E239C0CF9AA5E93D6 * value)
	{
		___U3CMenuManagerU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMenuManagerU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3CGesturesU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CGesturesU3Ek__BackingField_22)); }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * get_U3CGesturesU3Ek__BackingField_22() const { return ___U3CGesturesU3Ek__BackingField_22; }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 ** get_address_of_U3CGesturesU3Ek__BackingField_22() { return &___U3CGesturesU3Ek__BackingField_22; }
	inline void set_U3CGesturesU3Ek__BackingField_22(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * value)
	{
		___U3CGesturesU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGesturesU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_U3CInScriptMenuFactoryU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CInScriptMenuFactoryU3Ek__BackingField_23)); }
	inline InScriptMenuFactory_tE28604EF9A7748F0689F833E699CABF93F4DA7B9 * get_U3CInScriptMenuFactoryU3Ek__BackingField_23() const { return ___U3CInScriptMenuFactoryU3Ek__BackingField_23; }
	inline InScriptMenuFactory_tE28604EF9A7748F0689F833E699CABF93F4DA7B9 ** get_address_of_U3CInScriptMenuFactoryU3Ek__BackingField_23() { return &___U3CInScriptMenuFactoryU3Ek__BackingField_23; }
	inline void set_U3CInScriptMenuFactoryU3Ek__BackingField_23(InScriptMenuFactory_tE28604EF9A7748F0689F833E699CABF93F4DA7B9 * value)
	{
		___U3CInScriptMenuFactoryU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInScriptMenuFactoryU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CPortableMenuFactoryU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CPortableMenuFactoryU3Ek__BackingField_24)); }
	inline PortableMenuFactory_tAA97D5D2F11B3C26CBC04242D63940BFFBE0070D * get_U3CPortableMenuFactoryU3Ek__BackingField_24() const { return ___U3CPortableMenuFactoryU3Ek__BackingField_24; }
	inline PortableMenuFactory_tAA97D5D2F11B3C26CBC04242D63940BFFBE0070D ** get_address_of_U3CPortableMenuFactoryU3Ek__BackingField_24() { return &___U3CPortableMenuFactoryU3Ek__BackingField_24; }
	inline void set_U3CPortableMenuFactoryU3Ek__BackingField_24(PortableMenuFactory_tAA97D5D2F11B3C26CBC04242D63940BFFBE0070D * value)
	{
		___U3CPortableMenuFactoryU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPortableMenuFactoryU3Ek__BackingField_24), value);
	}

	inline static int32_t get_offset_of_U3CPortableMenuItemFactoryU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CPortableMenuItemFactoryU3Ek__BackingField_25)); }
	inline PortableMenuItemFactory_t1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D * get_U3CPortableMenuItemFactoryU3Ek__BackingField_25() const { return ___U3CPortableMenuItemFactoryU3Ek__BackingField_25; }
	inline PortableMenuItemFactory_t1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D ** get_address_of_U3CPortableMenuItemFactoryU3Ek__BackingField_25() { return &___U3CPortableMenuItemFactoryU3Ek__BackingField_25; }
	inline void set_U3CPortableMenuItemFactoryU3Ek__BackingField_25(PortableMenuItemFactory_t1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D * value)
	{
		___U3CPortableMenuItemFactoryU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPortableMenuItemFactoryU3Ek__BackingField_25), value);
	}

	inline static int32_t get_offset_of_U3CExhibitBoxFactoryU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CExhibitBoxFactoryU3Ek__BackingField_26)); }
	inline ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13 * get_U3CExhibitBoxFactoryU3Ek__BackingField_26() const { return ___U3CExhibitBoxFactoryU3Ek__BackingField_26; }
	inline ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13 ** get_address_of_U3CExhibitBoxFactoryU3Ek__BackingField_26() { return &___U3CExhibitBoxFactoryU3Ek__BackingField_26; }
	inline void set_U3CExhibitBoxFactoryU3Ek__BackingField_26(ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13 * value)
	{
		___U3CExhibitBoxFactoryU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExhibitBoxFactoryU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CScriptErrorHandlerU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CScriptErrorHandlerU3Ek__BackingField_27)); }
	inline ScriptErrorHandler_t86C54D9B0DA7092755777EFF56D5D23039FEA924 * get_U3CScriptErrorHandlerU3Ek__BackingField_27() const { return ___U3CScriptErrorHandlerU3Ek__BackingField_27; }
	inline ScriptErrorHandler_t86C54D9B0DA7092755777EFF56D5D23039FEA924 ** get_address_of_U3CScriptErrorHandlerU3Ek__BackingField_27() { return &___U3CScriptErrorHandlerU3Ek__BackingField_27; }
	inline void set_U3CScriptErrorHandlerU3Ek__BackingField_27(ScriptErrorHandler_t86C54D9B0DA7092755777EFF56D5D23039FEA924 * value)
	{
		___U3CScriptErrorHandlerU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScriptErrorHandlerU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CScriptEventHandlerU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CScriptEventHandlerU3Ek__BackingField_28)); }
	inline ScriptEventHandler_t6818D0F175ECC12B599C5FE2A33346CECCD49AC7 * get_U3CScriptEventHandlerU3Ek__BackingField_28() const { return ___U3CScriptEventHandlerU3Ek__BackingField_28; }
	inline ScriptEventHandler_t6818D0F175ECC12B599C5FE2A33346CECCD49AC7 ** get_address_of_U3CScriptEventHandlerU3Ek__BackingField_28() { return &___U3CScriptEventHandlerU3Ek__BackingField_28; }
	inline void set_U3CScriptEventHandlerU3Ek__BackingField_28(ScriptEventHandler_t6818D0F175ECC12B599C5FE2A33346CECCD49AC7 * value)
	{
		___U3CScriptEventHandlerU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScriptEventHandlerU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CScriptInputHandlerU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C, ___U3CScriptInputHandlerU3Ek__BackingField_29)); }
	inline ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC * get_U3CScriptInputHandlerU3Ek__BackingField_29() const { return ___U3CScriptInputHandlerU3Ek__BackingField_29; }
	inline ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC ** get_address_of_U3CScriptInputHandlerU3Ek__BackingField_29() { return &___U3CScriptInputHandlerU3Ek__BackingField_29; }
	inline void set_U3CScriptInputHandlerU3Ek__BackingField_29(ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC * value)
	{
		___U3CScriptInputHandlerU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScriptInputHandlerU3Ek__BackingField_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTMANAGER_TDDEF980C92189FD070F605511CA2EA9A04188F3C_H
#ifndef MONOINSTALLER_T5D8FF9EC635C3C146F50E3490F8B81803B1688EC_H
#define MONOINSTALLER_T5D8FF9EC635C3C146F50E3490F8B81803B1688EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoInstaller
struct  MonoInstaller_t5D8FF9EC635C3C146F50E3490F8B81803B1688EC  : public MonoInstallerBase_tC2BC7AF08360C9F8D2703E955C0D5EC9AF4C62DF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOINSTALLER_T5D8FF9EC635C3C146F50E3490F8B81803B1688EC_H
#ifndef DEPENDENCYINSTALLER_TCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF_H
#define DEPENDENCYINSTALLER_TCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Controllers.DependencyInstaller
struct  DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF  : public MonoInstaller_t5D8FF9EC635C3C146F50E3490F8B81803B1688EC
{
public:
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::ExhibitBoxPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ExhibitBoxPrefab_5;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::DesktopScriptMenuItemPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DesktopScriptMenuItemPrefab_6;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::DesktopScriptPagePrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DesktopScriptPagePrefab_7;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::PortableMenuPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___PortableMenuPrefab_8;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::PortableMenuItemPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___PortableMenuItemPrefab_9;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::VideoControlsPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___VideoControlsPrefab_10;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::InGameConsolePrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___InGameConsolePrefab_11;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::DesktopInScriptMenuPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DesktopInScriptMenuPrefab_12;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::DesktopInScriptTextMenuItemPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DesktopInScriptTextMenuItemPrefab_13;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::DesktopInScriptImageMenuItemPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DesktopInScriptImageMenuItemPrefab_14;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::DesktopCamera
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DesktopCamera_15;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::MRCamera
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___MRCamera_16;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::HololensCamera
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___HololensCamera_17;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::iOSCamera
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___iOSCamera_18;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::ARCoreDevicePrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ARCoreDevicePrefab_19;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::SpatialMappingManager
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___SpatialMappingManager_20;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::SpatialUnderstandingPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___SpatialUnderstandingPrefab_21;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::InputManagerPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___InputManagerPrefab_22;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::iOSMenuCanvasPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___iOSMenuCanvasPrefab_23;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::iOSScriptMenuItemPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___iOSScriptMenuItemPrefab_24;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::PlaneGeneratorPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___PlaneGeneratorPrefab_25;
	// UnityEngine.GameObject MPAR.Common.Controllers.DependencyInstaller::PointCloudParticlesPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___PointCloudParticlesPrefab_26;

public:
	inline static int32_t get_offset_of_ExhibitBoxPrefab_5() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___ExhibitBoxPrefab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ExhibitBoxPrefab_5() const { return ___ExhibitBoxPrefab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ExhibitBoxPrefab_5() { return &___ExhibitBoxPrefab_5; }
	inline void set_ExhibitBoxPrefab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ExhibitBoxPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___ExhibitBoxPrefab_5), value);
	}

	inline static int32_t get_offset_of_DesktopScriptMenuItemPrefab_6() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___DesktopScriptMenuItemPrefab_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DesktopScriptMenuItemPrefab_6() const { return ___DesktopScriptMenuItemPrefab_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DesktopScriptMenuItemPrefab_6() { return &___DesktopScriptMenuItemPrefab_6; }
	inline void set_DesktopScriptMenuItemPrefab_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DesktopScriptMenuItemPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___DesktopScriptMenuItemPrefab_6), value);
	}

	inline static int32_t get_offset_of_DesktopScriptPagePrefab_7() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___DesktopScriptPagePrefab_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DesktopScriptPagePrefab_7() const { return ___DesktopScriptPagePrefab_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DesktopScriptPagePrefab_7() { return &___DesktopScriptPagePrefab_7; }
	inline void set_DesktopScriptPagePrefab_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DesktopScriptPagePrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___DesktopScriptPagePrefab_7), value);
	}

	inline static int32_t get_offset_of_PortableMenuPrefab_8() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___PortableMenuPrefab_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_PortableMenuPrefab_8() const { return ___PortableMenuPrefab_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_PortableMenuPrefab_8() { return &___PortableMenuPrefab_8; }
	inline void set_PortableMenuPrefab_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___PortableMenuPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___PortableMenuPrefab_8), value);
	}

	inline static int32_t get_offset_of_PortableMenuItemPrefab_9() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___PortableMenuItemPrefab_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_PortableMenuItemPrefab_9() const { return ___PortableMenuItemPrefab_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_PortableMenuItemPrefab_9() { return &___PortableMenuItemPrefab_9; }
	inline void set_PortableMenuItemPrefab_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___PortableMenuItemPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___PortableMenuItemPrefab_9), value);
	}

	inline static int32_t get_offset_of_VideoControlsPrefab_10() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___VideoControlsPrefab_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_VideoControlsPrefab_10() const { return ___VideoControlsPrefab_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_VideoControlsPrefab_10() { return &___VideoControlsPrefab_10; }
	inline void set_VideoControlsPrefab_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___VideoControlsPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((&___VideoControlsPrefab_10), value);
	}

	inline static int32_t get_offset_of_InGameConsolePrefab_11() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___InGameConsolePrefab_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_InGameConsolePrefab_11() const { return ___InGameConsolePrefab_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_InGameConsolePrefab_11() { return &___InGameConsolePrefab_11; }
	inline void set_InGameConsolePrefab_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___InGameConsolePrefab_11 = value;
		Il2CppCodeGenWriteBarrier((&___InGameConsolePrefab_11), value);
	}

	inline static int32_t get_offset_of_DesktopInScriptMenuPrefab_12() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___DesktopInScriptMenuPrefab_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DesktopInScriptMenuPrefab_12() const { return ___DesktopInScriptMenuPrefab_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DesktopInScriptMenuPrefab_12() { return &___DesktopInScriptMenuPrefab_12; }
	inline void set_DesktopInScriptMenuPrefab_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DesktopInScriptMenuPrefab_12 = value;
		Il2CppCodeGenWriteBarrier((&___DesktopInScriptMenuPrefab_12), value);
	}

	inline static int32_t get_offset_of_DesktopInScriptTextMenuItemPrefab_13() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___DesktopInScriptTextMenuItemPrefab_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DesktopInScriptTextMenuItemPrefab_13() const { return ___DesktopInScriptTextMenuItemPrefab_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DesktopInScriptTextMenuItemPrefab_13() { return &___DesktopInScriptTextMenuItemPrefab_13; }
	inline void set_DesktopInScriptTextMenuItemPrefab_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DesktopInScriptTextMenuItemPrefab_13 = value;
		Il2CppCodeGenWriteBarrier((&___DesktopInScriptTextMenuItemPrefab_13), value);
	}

	inline static int32_t get_offset_of_DesktopInScriptImageMenuItemPrefab_14() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___DesktopInScriptImageMenuItemPrefab_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DesktopInScriptImageMenuItemPrefab_14() const { return ___DesktopInScriptImageMenuItemPrefab_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DesktopInScriptImageMenuItemPrefab_14() { return &___DesktopInScriptImageMenuItemPrefab_14; }
	inline void set_DesktopInScriptImageMenuItemPrefab_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DesktopInScriptImageMenuItemPrefab_14 = value;
		Il2CppCodeGenWriteBarrier((&___DesktopInScriptImageMenuItemPrefab_14), value);
	}

	inline static int32_t get_offset_of_DesktopCamera_15() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___DesktopCamera_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DesktopCamera_15() const { return ___DesktopCamera_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DesktopCamera_15() { return &___DesktopCamera_15; }
	inline void set_DesktopCamera_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DesktopCamera_15 = value;
		Il2CppCodeGenWriteBarrier((&___DesktopCamera_15), value);
	}

	inline static int32_t get_offset_of_MRCamera_16() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___MRCamera_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_MRCamera_16() const { return ___MRCamera_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_MRCamera_16() { return &___MRCamera_16; }
	inline void set_MRCamera_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___MRCamera_16 = value;
		Il2CppCodeGenWriteBarrier((&___MRCamera_16), value);
	}

	inline static int32_t get_offset_of_HololensCamera_17() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___HololensCamera_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_HololensCamera_17() const { return ___HololensCamera_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_HololensCamera_17() { return &___HololensCamera_17; }
	inline void set_HololensCamera_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___HololensCamera_17 = value;
		Il2CppCodeGenWriteBarrier((&___HololensCamera_17), value);
	}

	inline static int32_t get_offset_of_iOSCamera_18() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___iOSCamera_18)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_iOSCamera_18() const { return ___iOSCamera_18; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_iOSCamera_18() { return &___iOSCamera_18; }
	inline void set_iOSCamera_18(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___iOSCamera_18 = value;
		Il2CppCodeGenWriteBarrier((&___iOSCamera_18), value);
	}

	inline static int32_t get_offset_of_ARCoreDevicePrefab_19() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___ARCoreDevicePrefab_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ARCoreDevicePrefab_19() const { return ___ARCoreDevicePrefab_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ARCoreDevicePrefab_19() { return &___ARCoreDevicePrefab_19; }
	inline void set_ARCoreDevicePrefab_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ARCoreDevicePrefab_19 = value;
		Il2CppCodeGenWriteBarrier((&___ARCoreDevicePrefab_19), value);
	}

	inline static int32_t get_offset_of_SpatialMappingManager_20() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___SpatialMappingManager_20)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_SpatialMappingManager_20() const { return ___SpatialMappingManager_20; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_SpatialMappingManager_20() { return &___SpatialMappingManager_20; }
	inline void set_SpatialMappingManager_20(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___SpatialMappingManager_20 = value;
		Il2CppCodeGenWriteBarrier((&___SpatialMappingManager_20), value);
	}

	inline static int32_t get_offset_of_SpatialUnderstandingPrefab_21() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___SpatialUnderstandingPrefab_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_SpatialUnderstandingPrefab_21() const { return ___SpatialUnderstandingPrefab_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_SpatialUnderstandingPrefab_21() { return &___SpatialUnderstandingPrefab_21; }
	inline void set_SpatialUnderstandingPrefab_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___SpatialUnderstandingPrefab_21 = value;
		Il2CppCodeGenWriteBarrier((&___SpatialUnderstandingPrefab_21), value);
	}

	inline static int32_t get_offset_of_InputManagerPrefab_22() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___InputManagerPrefab_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_InputManagerPrefab_22() const { return ___InputManagerPrefab_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_InputManagerPrefab_22() { return &___InputManagerPrefab_22; }
	inline void set_InputManagerPrefab_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___InputManagerPrefab_22 = value;
		Il2CppCodeGenWriteBarrier((&___InputManagerPrefab_22), value);
	}

	inline static int32_t get_offset_of_iOSMenuCanvasPrefab_23() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___iOSMenuCanvasPrefab_23)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_iOSMenuCanvasPrefab_23() const { return ___iOSMenuCanvasPrefab_23; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_iOSMenuCanvasPrefab_23() { return &___iOSMenuCanvasPrefab_23; }
	inline void set_iOSMenuCanvasPrefab_23(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___iOSMenuCanvasPrefab_23 = value;
		Il2CppCodeGenWriteBarrier((&___iOSMenuCanvasPrefab_23), value);
	}

	inline static int32_t get_offset_of_iOSScriptMenuItemPrefab_24() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___iOSScriptMenuItemPrefab_24)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_iOSScriptMenuItemPrefab_24() const { return ___iOSScriptMenuItemPrefab_24; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_iOSScriptMenuItemPrefab_24() { return &___iOSScriptMenuItemPrefab_24; }
	inline void set_iOSScriptMenuItemPrefab_24(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___iOSScriptMenuItemPrefab_24 = value;
		Il2CppCodeGenWriteBarrier((&___iOSScriptMenuItemPrefab_24), value);
	}

	inline static int32_t get_offset_of_PlaneGeneratorPrefab_25() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___PlaneGeneratorPrefab_25)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_PlaneGeneratorPrefab_25() const { return ___PlaneGeneratorPrefab_25; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_PlaneGeneratorPrefab_25() { return &___PlaneGeneratorPrefab_25; }
	inline void set_PlaneGeneratorPrefab_25(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___PlaneGeneratorPrefab_25 = value;
		Il2CppCodeGenWriteBarrier((&___PlaneGeneratorPrefab_25), value);
	}

	inline static int32_t get_offset_of_PointCloudParticlesPrefab_26() { return static_cast<int32_t>(offsetof(DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF, ___PointCloudParticlesPrefab_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_PointCloudParticlesPrefab_26() const { return ___PointCloudParticlesPrefab_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_PointCloudParticlesPrefab_26() { return &___PointCloudParticlesPrefab_26; }
	inline void set_PointCloudParticlesPrefab_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___PointCloudParticlesPrefab_26 = value;
		Il2CppCodeGenWriteBarrier((&___PointCloudParticlesPrefab_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPENDENCYINSTALLER_TCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8100 = { sizeof (DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8100[22] = 
{
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_ExhibitBoxPrefab_5(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_DesktopScriptMenuItemPrefab_6(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_DesktopScriptPagePrefab_7(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_PortableMenuPrefab_8(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_PortableMenuItemPrefab_9(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_VideoControlsPrefab_10(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_InGameConsolePrefab_11(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_DesktopInScriptMenuPrefab_12(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_DesktopInScriptTextMenuItemPrefab_13(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_DesktopInScriptImageMenuItemPrefab_14(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_DesktopCamera_15(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_MRCamera_16(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_HololensCamera_17(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_iOSCamera_18(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_ARCoreDevicePrefab_19(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_SpatialMappingManager_20(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_SpatialUnderstandingPrefab_21(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_InputManagerPrefab_22(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_iOSMenuCanvasPrefab_23(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_iOSScriptMenuItemPrefab_24(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_PlaneGeneratorPrefab_25(),
	DependencyInstaller_tCFB843CDE3E3E202DB29055D5ECBBE32A6EE85EF::get_offset_of_PointCloudParticlesPrefab_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8101 = { sizeof (LayoutTypeEnum_t9D756A67715DCD69C2107BE558831A1FB6DF7021)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8101[3] = 
{
	LayoutTypeEnum_t9D756A67715DCD69C2107BE558831A1FB6DF7021::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8102 = { sizeof (OrientTypeEnum_t68335295F5F9990B4603413C5A074BDAFE36EDC1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8102[6] = 
{
	OrientTypeEnum_t68335295F5F9990B4603413C5A074BDAFE36EDC1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8103 = { sizeof (SortTypeEnum_t39097EA708FC937E322E139D9144FAFB4FAEC747)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8103[6] = 
{
	SortTypeEnum_t39097EA708FC937E322E139D9144FAFB4FAEC747::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8104 = { sizeof (SurfaceTypeEnum_t096EE4E9ED02B6B13FBE8735A7E004FEC28B0368)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8104[5] = 
{
	SurfaceTypeEnum_t096EE4E9ED02B6B13FBE8735A7E004FEC28B0368::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8105 = { sizeof (CollectionNode_t6ED8EDB43863FAF4EF18CB7E156D7620FF4F93DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8105[4] = 
{
	CollectionNode_t6ED8EDB43863FAF4EF18CB7E156D7620FF4F93DC::get_offset_of_Name_0(),
	CollectionNode_t6ED8EDB43863FAF4EF18CB7E156D7620FF4F93DC::get_offset_of_Offset_1(),
	CollectionNode_t6ED8EDB43863FAF4EF18CB7E156D7620FF4F93DC::get_offset_of_Radius_2(),
	CollectionNode_t6ED8EDB43863FAF4EF18CB7E156D7620FF4F93DC::get_offset_of_transform_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8106 = { sizeof (ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8106[18] = 
{
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of_OnCollectionUpdated_4(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of_NodeList_5(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of_SurfaceType_6(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of_SortType_7(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of_OrientType_8(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of_LayoutType_9(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of_IgnoreInactiveTransforms_10(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of_Radius_11(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of_Rows_12(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of_CellWidth_13(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of_CellHeight_14(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of_SphereMesh_15(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of_CylinderMesh_16(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of__columns_17(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of__width_18(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of__height_19(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of__circumference_20(),
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB::get_offset_of__halfCell_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8107 = { sizeof (U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3), -1, sizeof(U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8107[6] = 
{
	U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_StaticFields::get_offset_of_U3CU3E9__22_0_1(),
	U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_StaticFields::get_offset_of_U3CU3E9__22_1_2(),
	U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_StaticFields::get_offset_of_U3CU3E9__22_2_3(),
	U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_StaticFields::get_offset_of_U3CU3E9__22_3_4(),
	U3CU3Ec_t277EFC0BFC0080A68C71659FDE59A4160DE6B7D3_StaticFields::get_offset_of_U3CU3E9__28_0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8108 = { sizeof (ObjectCollectionDynamic_tBCBA691F0D90197B528822C08D696686A38254F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8108[3] = 
{
	ObjectCollectionDynamic_tBCBA691F0D90197B528822C08D696686A38254F7::get_offset_of_Behavior_4(),
	ObjectCollectionDynamic_tBCBA691F0D90197B528822C08D696686A38254F7::get_offset_of_DynamicNodeList_5(),
	ObjectCollectionDynamic_tBCBA691F0D90197B528822C08D696686A38254F7::get_offset_of_collection_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8109 = { sizeof (BehaviorEnum_t159F12A07D59CCB912D1E65E3393A29F9032C340)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8109[4] = 
{
	BehaviorEnum_t159F12A07D59CCB912D1E65E3393A29F9032C340::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8110 = { sizeof (CollectionNodeDynamic_tBDB73984DE03C86E6E43F322CF24ED2F46B19B78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8110[2] = 
{
	CollectionNodeDynamic_tBDB73984DE03C86E6E43F322CF24ED2F46B19B78::get_offset_of_localPositionOnStartup_4(),
	CollectionNodeDynamic_tBDB73984DE03C86E6E43F322CF24ED2F46B19B78::get_offset_of_localEulerAnglesOnStartup_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8111 = { sizeof (LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8111[5] = 
{
	LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A::get_offset_of_U3CAssetBundlesU3Ek__BackingField_4(),
	LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A::get_offset_of_U3CAssetsU3Ek__BackingField_5(),
	LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A::get_offset_of_U3CLoadingAssetsU3Ek__BackingField_6(),
	LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A::get_offset_of_U3CCacheManagerU3Ek__BackingField_7(),
	LoadedAssets_t00BEC30DEDFF407C995DAA7F8C6409BBB3913E8A::get_offset_of_U3CSaveLoadManagerU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8112 = { sizeof (Collidable_t2DD800E21B45929FBAEBDCE305BB8E65236D4854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8112[2] = 
{
	Collidable_t2DD800E21B45929FBAEBDCE305BB8E65236D4854::get_offset_of_U3CActionU3Ek__BackingField_4(),
	Collidable_t2DD800E21B45929FBAEBDCE305BB8E65236D4854::get_offset_of_U3CCollisionToConsumeU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8113 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8114 = { sizeof (GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8114[13] = 
{
	GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F::get_offset_of_id_0(),
	GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F::get_offset_of_name_1(),
	GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F::get_offset_of_description_2(),
	GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F::get_offset_of_shared_3(),
	GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F::get_offset_of_revision_4(),
	GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F::get_offset_of_userId_5(),
	GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F::get_offset_of_nickname_6(),
	GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F::get_offset_of_includes_7(),
	GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F::get_offset_of_thumbnail_8(),
	GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F::get_offset_of_mixScript_9(),
	GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F::get_offset_of_moderationStatus_10(),
	GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F::get_offset_of_moderationMessage_11(),
	GalleryMixModel_t6F1D38D099BA9039A95018862BE9D18ADEB0F43F::get_offset_of_blocked_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8115 = { sizeof (GalleryModel_tF9E0F01E19CF50C229DBFBD6B8591EF17DFFAC6A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8115[2] = 
{
	GalleryModel_tF9E0F01E19CF50C229DBFBD6B8591EF17DFFAC6A::get_offset_of_mixes_0(),
	GalleryModel_tF9E0F01E19CF50C229DBFBD6B8591EF17DFFAC6A::get_offset_of_total_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8116 = { sizeof (MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8116[17] = 
{
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_id_0(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_name_1(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_fileName_2(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_includes_3(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_description_4(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_nickname_5(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_revision_6(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_thumbnail_7(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_shared_8(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_userId_9(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_apiServiceBaseUri_10(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_targets_11(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_manifestUrl_12(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_blocked_13(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_moderationStatus_14(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_lineScriptStartsAt_15(),
	MixModel_t3D97435B97215AA60BFBD50870C49C217867CC5F::get_offset_of_lineEachFileStartsAt_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8117 = { sizeof (ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8117[13] = 
{
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2::get_offset_of_assets_6(),
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2::get_offset_of_scriptMenu_7(),
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2::get_offset_of_menuFactory_8(),
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2::get_offset_of_saver_9(),
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2::get_offset_of_U3CCacheManagerU3Ek__BackingField_10(),
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2::get_offset_of_U3CProjectLoaderU3Ek__BackingField_11(),
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2::get_offset_of_U3CSaveLoadManagerU3Ek__BackingField_12(),
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2::get_offset_of_U3CScriptMenuU3Ek__BackingField_13(),
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2::get_offset_of_path_14(),
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2::get_offset_of_autoRunScriptUrl_15(),
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2::get_offset_of_preloadedBaseUrl_16(),
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2::get_offset_of_preloadedAutoRunUrl_17(),
	ScriptLoader_tE0646E44B177F7A0B8DD42619E6909245286F2F2::get_offset_of_startedLoading_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8118 = { sizeof (U3CcheckScriptAvailabilityU3Ed__28_tF994452797D1E4BB179BCFC898871C6D74703EE7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8118[5] = 
{
	U3CcheckScriptAvailabilityU3Ed__28_tF994452797D1E4BB179BCFC898871C6D74703EE7::get_offset_of_U3CU3E1__state_0(),
	U3CcheckScriptAvailabilityU3Ed__28_tF994452797D1E4BB179BCFC898871C6D74703EE7::get_offset_of_U3CU3E2__current_1(),
	U3CcheckScriptAvailabilityU3Ed__28_tF994452797D1E4BB179BCFC898871C6D74703EE7::get_offset_of_script_2(),
	U3CcheckScriptAvailabilityU3Ed__28_tF994452797D1E4BB179BCFC898871C6D74703EE7::get_offset_of_action_3(),
	U3CcheckScriptAvailabilityU3Ed__28_tF994452797D1E4BB179BCFC898871C6D74703EE7::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8119 = { sizeof (U3CU3Ec__DisplayClass30_0_tC52D98C12DE76ACD76F91DA1B43E1796F81BC844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8119[2] = 
{
	U3CU3Ec__DisplayClass30_0_tC52D98C12DE76ACD76F91DA1B43E1796F81BC844::get_offset_of_baseUrl_0(),
	U3CU3Ec__DisplayClass30_0_tC52D98C12DE76ACD76F91DA1B43E1796F81BC844::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8120 = { sizeof (U3CLoadFromProjectUrlU3Ed__30_tE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8120[6] = 
{
	U3CLoadFromProjectUrlU3Ed__30_tE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC::get_offset_of_U3CU3E1__state_0(),
	U3CLoadFromProjectUrlU3Ed__30_tE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC::get_offset_of_U3CU3E2__current_1(),
	U3CLoadFromProjectUrlU3Ed__30_tE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC::get_offset_of_baseUrl_2(),
	U3CLoadFromProjectUrlU3Ed__30_tE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC::get_offset_of_U3CU3E4__this_3(),
	U3CLoadFromProjectUrlU3Ed__30_tE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC::get_offset_of_autoRunUrl_4(),
	U3CLoadFromProjectUrlU3Ed__30_tE050CB036AB9B376EE1E74F72F87BCEA0E4FB7EC::get_offset_of_U3CwwwU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8121 = { sizeof (U3CGetScriptFromManifestUrlU3Ed__31_tCC03994615EB7D1FB58B7C3287A492C5110B8225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8121[6] = 
{
	U3CGetScriptFromManifestUrlU3Ed__31_tCC03994615EB7D1FB58B7C3287A492C5110B8225::get_offset_of_U3CU3E1__state_0(),
	U3CGetScriptFromManifestUrlU3Ed__31_tCC03994615EB7D1FB58B7C3287A492C5110B8225::get_offset_of_U3CU3E2__current_1(),
	U3CGetScriptFromManifestUrlU3Ed__31_tCC03994615EB7D1FB58B7C3287A492C5110B8225::get_offset_of_url_2(),
	U3CGetScriptFromManifestUrlU3Ed__31_tCC03994615EB7D1FB58B7C3287A492C5110B8225::get_offset_of_U3CU3E4__this_3(),
	U3CGetScriptFromManifestUrlU3Ed__31_tCC03994615EB7D1FB58B7C3287A492C5110B8225::get_offset_of_callback_4(),
	U3CGetScriptFromManifestUrlU3Ed__31_tCC03994615EB7D1FB58B7C3287A492C5110B8225::get_offset_of_U3CwwwU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8122 = { sizeof (U3CU3Ec__DisplayClass33_0_t3C00579881D6BDEB719A35102C6779BCEEFF2C11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8122[2] = 
{
	U3CU3Ec__DisplayClass33_0_t3C00579881D6BDEB719A35102C6779BCEEFF2C11::get_offset_of_script_0(),
	U3CU3Ec__DisplayClass33_0_t3C00579881D6BDEB719A35102C6779BCEEFF2C11::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8123 = { sizeof (U3CCheckForUpdatesU3Ed__33_tE32A498D1F06F706A79404A0F5109E1E62FBFA11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8123[5] = 
{
	U3CCheckForUpdatesU3Ed__33_tE32A498D1F06F706A79404A0F5109E1E62FBFA11::get_offset_of_U3CU3E1__state_0(),
	U3CCheckForUpdatesU3Ed__33_tE32A498D1F06F706A79404A0F5109E1E62FBFA11::get_offset_of_U3CU3E2__current_1(),
	U3CCheckForUpdatesU3Ed__33_tE32A498D1F06F706A79404A0F5109E1E62FBFA11::get_offset_of_script_2(),
	U3CCheckForUpdatesU3Ed__33_tE32A498D1F06F706A79404A0F5109E1E62FBFA11::get_offset_of_callback_3(),
	U3CCheckForUpdatesU3Ed__33_tE32A498D1F06F706A79404A0F5109E1E62FBFA11::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8124 = { sizeof (U3CU3Ec__DisplayClass34_0_t52E9FF2846861C2B43715DFA17D60BBF17FAFBB3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8124[2] = 
{
	U3CU3Ec__DisplayClass34_0_t52E9FF2846861C2B43715DFA17D60BBF17FAFBB3::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass34_0_t52E9FF2846861C2B43715DFA17D60BBF17FAFBB3::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8125 = { sizeof (U3CApplyUpdateU3Ed__34_tD44A5483F1F3CC7F2095C0A3D1B0674454AB0211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8125[5] = 
{
	U3CApplyUpdateU3Ed__34_tD44A5483F1F3CC7F2095C0A3D1B0674454AB0211::get_offset_of_U3CU3E1__state_0(),
	U3CApplyUpdateU3Ed__34_tD44A5483F1F3CC7F2095C0A3D1B0674454AB0211::get_offset_of_U3CU3E2__current_1(),
	U3CApplyUpdateU3Ed__34_tD44A5483F1F3CC7F2095C0A3D1B0674454AB0211::get_offset_of_U3CU3E4__this_2(),
	U3CApplyUpdateU3Ed__34_tD44A5483F1F3CC7F2095C0A3D1B0674454AB0211::get_offset_of_callback_3(),
	U3CApplyUpdateU3Ed__34_tD44A5483F1F3CC7F2095C0A3D1B0674454AB0211::get_offset_of_script_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8126 = { sizeof (U3CU3Ec__DisplayClass36_0_t298D8788C6CC89EB351E61B1FEA298848BD4A2BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8126[1] = 
{
	U3CU3Ec__DisplayClass36_0_t298D8788C6CC89EB351E61B1FEA298848BD4A2BA::get_offset_of_script_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8127 = { sizeof (U3CU3Ec__DisplayClass38_0_t5F706BF913F1C9975BAA3B83FE5051263066BAFD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8127[3] = 
{
	U3CU3Ec__DisplayClass38_0_t5F706BF913F1C9975BAA3B83FE5051263066BAFD::get_offset_of_j_0(),
	U3CU3Ec__DisplayClass38_0_t5F706BF913F1C9975BAA3B83FE5051263066BAFD::get_offset_of_scripts_1(),
	U3CU3Ec__DisplayClass38_0_t5F706BF913F1C9975BAA3B83FE5051263066BAFD::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8128 = { sizeof (U3CU3Ec__DisplayClass38_1_t53857E613F79F3A0534EBA2D25FF013D146CEB64), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8128[2] = 
{
	U3CU3Ec__DisplayClass38_1_t53857E613F79F3A0534EBA2D25FF013D146CEB64::get_offset_of_script_0(),
	U3CU3Ec__DisplayClass38_1_t53857E613F79F3A0534EBA2D25FF013D146CEB64::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8129 = { sizeof (Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8129[16] = 
{
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_U3CIdU3Ek__BackingField_0(),
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_U3CThumbnailU3Ek__BackingField_1(),
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_U3CThumbnailPathU3Ek__BackingField_2(),
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_U3CJavascriptU3Ek__BackingField_3(),
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_U3CScriptUrlU3Ek__BackingField_4(),
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_U3CManifestUrlU3Ek__BackingField_5(),
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_name_6(),
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_U3CDescriptionU3Ek__BackingField_7(),
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_U3CAuthorU3Ek__BackingField_8(),
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_U3CVersionU3Ek__BackingField_9(),
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_U3CIncludesU3Ek__BackingField_10(),
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_U3CBlockedU3Ek__BackingField_11(),
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_U3CModerationStatusU3Ek__BackingField_12(),
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_U3CLastAccessedU3Ek__BackingField_13(),
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_U3CLineScriptStartsAtU3Ek__BackingField_14(),
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC::get_offset_of_U3CLineEachFileStartsAtU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8130 = { sizeof (ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8130[24] = 
{
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CWhileLoadingMethodsU3Ek__BackingField_6(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_startedScript_7(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_setupScript_8(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_scriptAdditions_9(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CIsScriptRunningU3Ek__BackingField_10(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CScriptU3Ek__BackingField_11(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CScriptInfoU3Ek__BackingField_12(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CEngineU3Ek__BackingField_13(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CAssetsU3Ek__BackingField_14(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CSaveLoadManagerU3Ek__BackingField_15(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CProjectLoaderU3Ek__BackingField_16(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CInScriptMenuU3Ek__BackingField_17(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CLoadedAssetsU3Ek__BackingField_18(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CFeaturesU3Ek__BackingField_19(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CPlaneDetectorU3Ek__BackingField_20(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CMenuManagerU3Ek__BackingField_21(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CGesturesU3Ek__BackingField_22(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CInScriptMenuFactoryU3Ek__BackingField_23(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CPortableMenuFactoryU3Ek__BackingField_24(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CPortableMenuItemFactoryU3Ek__BackingField_25(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CExhibitBoxFactoryU3Ek__BackingField_26(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CScriptErrorHandlerU3Ek__BackingField_27(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CScriptEventHandlerU3Ek__BackingField_28(),
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C::get_offset_of_U3CScriptInputHandlerU3Ek__BackingField_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8131 = { sizeof (U3CU3Ec_t20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9), -1, sizeof(U3CU3Ec_t20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8131[3] = 
{
	U3CU3Ec_t20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9_StaticFields::get_offset_of_U3CU3E9__89_0_1(),
	U3CU3Ec_t20F6FCFF9DADE2C9C67612A26737A29BCCA2A6F9_StaticFields::get_offset_of_U3CU3E9__90_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8132 = { sizeof (U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8132[11] = 
{
	U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD::get_offset_of_U3CU3E1__state_0(),
	U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD::get_offset_of_U3CU3E2__current_1(),
	U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD::get_offset_of_scr_2(),
	U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD::get_offset_of_callback_3(),
	U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD::get_offset_of_U3CscriptU3E5__2_4(),
	U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD::get_offset_of_U3CwwwU3E5__3_5(),
	U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD::get_offset_of_U3CjsU3E5__4_6(),
	U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD::get_offset_of_U3CincludeBuilderU3E5__5_7(),
	U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD::get_offset_of_U3CU3E7__wrap5_8(),
	U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD::get_offset_of_U3CU3E7__wrap6_9(),
	U3CDownloadU3Ed__89_t47BF6203AA7EE9D54C7FA87CCE039C8390B359FD::get_offset_of_U3Cwww2U3E5__8_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8133 = { sizeof (MethodInfoExtensions_t2A5DC847FE17F1C7522D0B86346B9DE8466AB370), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8134 = { sizeof (U3CU3Ec_tBCA14C63283E5E5324AFB227954799B94C1ACEC8), -1, sizeof(U3CU3Ec_tBCA14C63283E5E5324AFB227954799B94C1ACEC8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8134[2] = 
{
	U3CU3Ec_tBCA14C63283E5E5324AFB227954799B94C1ACEC8_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tBCA14C63283E5E5324AFB227954799B94C1ACEC8_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8135 = { sizeof (VideoRecorder_tAD0FB133532C7B99B421C2DF4B850C14BF815AF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8135[3] = 
{
	VideoRecorder_tAD0FB133532C7B99B421C2DF4B850C14BF815AF6::get_offset_of_U3CCaptureU3Ek__BackingField_4(),
	VideoRecorder_tAD0FB133532C7B99B421C2DF4B850C14BF815AF6::get_offset_of__captureStartTime_5(),
	VideoRecorder_tAD0FB133532C7B99B421C2DF4B850C14BF815AF6::get_offset_of__recording_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8136 = { sizeof (InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8136[13] = 
{
	InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D::get_offset_of_U3CGameObjectU3Ek__BackingField_4(),
	InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D::get_offset_of_U3CstateU3Ek__BackingField_5(),
	InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D::get_offset_of_U3CBehavioursU3Ek__BackingField_6(),
	InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D::get_offset_of_U3CEngineU3Ek__BackingField_7(),
	InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D::get_offset_of_U3CGesturesU3Ek__BackingField_8(),
	InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D::get_offset_of_U3CMainCameraU3Ek__BackingField_9(),
	InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D::get_offset_of_U3CCollidableU3Ek__BackingField_10(),
	InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D::get_offset_of_U3CCachedRendererU3Ek__BackingField_11(),
	InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D::get_offset_of_transparentShader_12(),
	InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D::get_offset_of_position_13(),
	InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D::get_offset_of_rotation_14(),
	InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D::get_offset_of_scale_15(),
	InstructionComponent_t8CB36B2C94C806D8014D2F77D6D1A8586518021D::get_offset_of_uniqueId_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8137 = { sizeof (ApiKeyProjectLoader_tF51E2E3FEA6D64E61EEFF57B60D394B69E5CCDCC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8137[1] = 
{
	ApiKeyProjectLoader_tF51E2E3FEA6D64E61EEFF57B60D394B69E5CCDCC::get_offset_of_baseUrl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8138 = { sizeof (FileGuidRef_t8FBE42296BD015960D7CC10A785C8400D09AD49F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8138[2] = 
{
	FileGuidRef_t8FBE42296BD015960D7CC10A785C8400D09AD49F::get_offset_of_U3CFileNameU3Ek__BackingField_0(),
	FileGuidRef_t8FBE42296BD015960D7CC10A785C8400D09AD49F::get_offset_of_U3CScriptGuidIdsThatReferenceU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8139 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8140 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8141 = { sizeof (ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8141[8] = 
{
	ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386::get_offset_of_U3CRunningU3Ek__BackingField_4(),
	ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386::get_offset_of_U3CCallbackU3Ek__BackingField_5(),
	ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386::get_offset_of_U3CLastInvokeU3Ek__BackingField_6(),
	ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386::get_offset_of_U3CInvokeIntervalU3Ek__BackingField_7(),
	ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386::get_offset_of_U3CImageU3Ek__BackingField_8(),
	ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386::get_offset_of_U3CParserU3Ek__BackingField_9(),
	ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386::get_offset_of_U3CValueU3Ek__BackingField_10(),
	ScreenshotQRCodeProjectLoader_t547BA35CCEF84AB874655BD630F0D1A8CE5CE386::get_offset_of_InProgress_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8142 = { sizeof (U3CRecordFrameU3Ed__31_tDA2765346EA399B8041FC91B08CE822B3254BFC3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8142[3] = 
{
	U3CRecordFrameU3Ed__31_tDA2765346EA399B8041FC91B08CE822B3254BFC3::get_offset_of_U3CU3E1__state_0(),
	U3CRecordFrameU3Ed__31_tDA2765346EA399B8041FC91B08CE822B3254BFC3::get_offset_of_U3CU3E2__current_1(),
	U3CRecordFrameU3Ed__31_tDA2765346EA399B8041FC91B08CE822B3254BFC3::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8143 = { sizeof (ScriptIndexObject_t4D3656E71D262DD293EC701B6C19622A2DA6E69E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8143[1] = 
{
	ScriptIndexObject_t4D3656E71D262DD293EC701B6C19622A2DA6E69E::get_offset_of_menuItems_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8144 = { sizeof (ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8144[7] = 
{
	ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B::get_offset_of_title_0(),
	ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B::get_offset_of_includes_1(),
	ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B::get_offset_of_description_2(),
	ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B::get_offset_of_author_3(),
	ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B::get_offset_of_version_4(),
	ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B::get_offset_of_thumbnail_5(),
	ScriptObject_t03F4E0F2751528088A14749379AAC4C91B8D289B::get_offset_of_filename_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8145 = { sizeof (StaticProjectLoader_t9B6ADCD33AC4B4D5035D11C21B3E67406CF9C92F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8146 = { sizeof (WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8146[9] = 
{
	WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7::get_offset_of_baseUrl_0(),
	WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7::get_offset_of_apiEndpoint_1(),
	WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7::get_offset_of_method_2(),
	WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7::get_offset_of_username_3(),
	WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7::get_offset_of_password_4(),
	WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7::get_offset_of_parameters_5(),
	WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7::get_offset_of_urlSegments_6(),
	WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7::get_offset_of_headers_7(),
	WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7::get_offset_of_jsonBody_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8147 = { sizeof (U3CU3Ec__DisplayClass12_0_tEFB9F75E1D5E9FFE7001EB94B891A7FD164DFF1F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8147[2] = 
{
	U3CU3Ec__DisplayClass12_0_tEFB9F75E1D5E9FFE7001EB94B891A7FD164DFF1F::get_offset_of_done_0(),
	U3CU3Ec__DisplayClass12_0_tEFB9F75E1D5E9FFE7001EB94B891A7FD164DFF1F::get_offset_of_content_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8148 = { sizeof (U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8148[7] = 
{
	U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021::get_offset_of_U3CU3E1__state_0(),
	U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021::get_offset_of_U3CU3E2__current_1(),
	U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021::get_offset_of_U3CU3E4__this_2(),
	U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021::get_offset_of_U3CU3E8__1_3(),
	U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021::get_offset_of_rawCallback_4(),
	U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021::get_offset_of_callback_5(),
	U3CExecCoroutineU3Ed__12_t87C49FB243D9C9492874983697481B4FA1D13021::get_offset_of_U3CscriptManagerU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8149 = { sizeof (ScriptApiContent_tAF9606D7B4658C85CC42E2597DB1604C42F5FFC4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8150 = { sizeof (U3CU3Ec__DisplayClass6_0_tC0167226F2ED0C4E021A48336E8426301692FCF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8150[1] = 
{
	U3CU3Ec__DisplayClass6_0_tC0167226F2ED0C4E021A48336E8426301692FCF6::get_offset_of_elementName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8151 = { sizeof (U3CU3Ec__DisplayClass12_0_t18B6407483C42D410F07C9501DB3E4A8C06C7CEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8151[2] = 
{
	U3CU3Ec__DisplayClass12_0_t18B6407483C42D410F07C9501DB3E4A8C06C7CEA::get_offset_of_player_0(),
	U3CU3Ec__DisplayClass12_0_t18B6407483C42D410F07C9501DB3E4A8C06C7CEA::get_offset_of_damage_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8152 = { sizeof (WebRequestApi_t721C4D6D2E73AD4E4FECAE9180264D08834B046A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8153 = { sizeof (Web_t94CA446B8B5457E3432A4F3E8E3D99E4EFEA2B44), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8154 = { sizeof (Behaviour_tBA8D40C72EE63C7BF267F5EF8730F9B32B58CAE2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8155 = { sizeof (Mixiply_t19CA7AE73AD2F7787F1E92DC8AA79B56AE03E13E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8156 = { sizeof (Player_t8585448877A2DF9A2B4B8AF312457B3581B3E4D6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8157 = { sizeof (Debug_tC2BB8147F32C43BEA6B75F2DD8451A77423315FB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8158 = { sizeof (Physics_t053201723E39B546745364236F3DDC3138C92042), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8159 = { sizeof (Color_t100E89EFD2BFD031D5EA8DC95C865E5AEDBE3210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8159[1] = 
{
	Color_t100E89EFD2BFD031D5EA8DC95C865E5AEDBE3210::get_offset_of_color_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8160 = { sizeof (MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8161 = { sizeof (U3CU3Ec_t22EB45990884EAFF2E33E3B36ECE2EF7A8873322), -1, sizeof(U3CU3Ec_t22EB45990884EAFF2E33E3B36ECE2EF7A8873322_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8161[2] = 
{
	U3CU3Ec_t22EB45990884EAFF2E33E3B36ECE2EF7A8873322_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t22EB45990884EAFF2E33E3B36ECE2EF7A8873322_StaticFields::get_offset_of_U3CU3E9__70_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8162 = { sizeof (Primitive_tCDD3C68D66228397104F11B36425365735BF0269), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8163 = { sizeof (U3CU3Eo__7_t2B0D61D6E37B81328988165DEFD5BBC925A2D584), -1, sizeof(U3CU3Eo__7_t2B0D61D6E37B81328988165DEFD5BBC925A2D584_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8163[3] = 
{
	U3CU3Eo__7_t2B0D61D6E37B81328988165DEFD5BBC925A2D584_StaticFields::get_offset_of_U3CU3Ep__0_0(),
	U3CU3Eo__7_t2B0D61D6E37B81328988165DEFD5BBC925A2D584_StaticFields::get_offset_of_U3CU3Ep__1_1(),
	U3CU3Eo__7_t2B0D61D6E37B81328988165DEFD5BBC925A2D584_StaticFields::get_offset_of_U3CU3Ep__2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8164 = { sizeof (Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8164[1] = 
{
	Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207::get_offset_of_vector_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8165 = { sizeof (Menu_tC3C19FD4A9E677DB96FA23D74AEF713E337565A0), -1, sizeof(Menu_tC3C19FD4A9E677DB96FA23D74AEF713E337565A0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8165[1] = 
{
	Menu_tC3C19FD4A9E677DB96FA23D74AEF713E337565A0_StaticFields::get_offset_of__Menu_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8166 = { sizeof (ScriptMenu_t8D4D728B65EE28F23D1B655B6840216521F33B27), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8167 = { sizeof (U3CU3Eo__0_tFBD97D0E94A7B0C6305B3F9B2B106C9A823D01E5), -1, sizeof(U3CU3Eo__0_tFBD97D0E94A7B0C6305B3F9B2B106C9A823D01E5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8167[1] = 
{
	U3CU3Eo__0_tFBD97D0E94A7B0C6305B3F9B2B106C9A823D01E5_StaticFields::get_offset_of_U3CU3Ep__0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8168 = { sizeof (UI_tDFF5D24118D6BE841F050DFF9D6F7174E3995D00), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8169 = { sizeof (U3CU3Ec_t72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0), -1, sizeof(U3CU3Ec_t72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8169[3] = 
{
	U3CU3Ec_t72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0_StaticFields::get_offset_of_U3CU3E9__5_0_1(),
	U3CU3Ec_t72B3D9C9C4C7F9C8A6D1D0FD0EAF486F03F156B0_StaticFields::get_offset_of_U3CU3E9__6_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8170 = { sizeof (U3CU3Ec__DisplayClass7_0_t524EB53E9D79D3A125C5CFD70481935999523D4F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8170[3] = 
{
	U3CU3Ec__DisplayClass7_0_t524EB53E9D79D3A125C5CFD70481935999523D4F::get_offset_of_callback_0(),
	U3CU3Ec__DisplayClass7_0_t524EB53E9D79D3A125C5CFD70481935999523D4F::get_offset_of_ui_1(),
	U3CU3Ec__DisplayClass7_0_t524EB53E9D79D3A125C5CFD70481935999523D4F::get_offset_of_U3CU3E9__1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8171 = { sizeof (Input_t830A2D3A65441ABC6001D38F28B5A07E57FDAD7B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8172 = { sizeof (Event_t5328F3E98A257F496099549407BA78C4DE6F765E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8173 = { sizeof (Animation_tEEF97D5C57556A56B1C9CC07939CEC59A4DCB996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8173[1] = 
{
	Animation_tEEF97D5C57556A56B1C9CC07939CEC59A4DCB996::get_offset_of_animation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8174 = { sizeof (Asset_t18FA838BF8BBDFED83FE107A932A431A932C80A3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8175 = { sizeof (U3CU3Ec__DisplayClass4_0_t378B5A52952B048048E8A054ACEF332B52D63D5E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8175[1] = 
{
	U3CU3Ec__DisplayClass4_0_t378B5A52952B048048E8A054ACEF332B52D63D5E::get_offset_of_bundleName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8176 = { sizeof (U3CU3Ec__DisplayClass5_0_t9EDB9BB83387F5A86495955953B05FD0BECFF001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8176[1] = 
{
	U3CU3Ec__DisplayClass5_0_t9EDB9BB83387F5A86495955953B05FD0BECFF001::get_offset_of_assetName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8177 = { sizeof (Audio_t3A5B96BE1E05EB0E3AAA751C0946A2E7D9F92FA8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8178 = { sizeof (Effect_tDBD3EEB1CF1190DC08D718AC00D8E6996D6ED7AD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8179 = { sizeof (Video_t13A3701ED2FC30161B825D8EF24C810D9C908AE6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8180 = { sizeof (ParameterDefinition_tE505C70240704BCB0A37A1AE2D962EA6BBCBDB3C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8180[3] = 
{
	ParameterDefinition_tE505C70240704BCB0A37A1AE2D962EA6BBCBDB3C::get_offset_of_name_0(),
	ParameterDefinition_tE505C70240704BCB0A37A1AE2D962EA6BBCBDB3C::get_offset_of_type_1(),
	ParameterDefinition_tE505C70240704BCB0A37A1AE2D962EA6BBCBDB3C::get_offset_of_required_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8181 = { sizeof (MultiPlatformAction_t68284A49F3E8F58B98C11A50B7CA6CA7A4F7E705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8182 = { sizeof (U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0), -1, sizeof(U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8182[7] = 
{
	U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
	U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields::get_offset_of_U3CU3E9__1_0_2(),
	U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields::get_offset_of_U3CU3E9__2_0_3(),
	U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields::get_offset_of_U3CU3E9__3_0_4(),
	U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields::get_offset_of_U3CU3E9__4_0_5(),
	U3CU3Ec_tD159991999ABEA1B82B20A26118C32DA046BB8B0_StaticFields::get_offset_of_U3CU3E9__5_0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8183 = { sizeof (SetMouseFreeAction_tF1885192463ACB1AA5B86D4018B3DA184237505B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8184 = { sizeof (U3CU3Ec__DisplayClass0_0_t34A3152F04773B8FE9D133DC5108F4A4984868D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8184[2] = 
{
	U3CU3Ec__DisplayClass0_0_t34A3152F04773B8FE9D133DC5108F4A4984868D2::get_offset_of_gestures_0(),
	U3CU3Ec__DisplayClass0_0_t34A3152F04773B8FE9D133DC5108F4A4984868D2::get_offset_of_free_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8185 = { sizeof (AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2), -1, sizeof(AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8185[3] = 
{
	AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2_StaticFields::get_offset_of_U3CSupportedAssetsU3Ek__BackingField_4(),
	AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2::get_offset_of_U3CSaveLoadManagerU3Ek__BackingField_5(),
	AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2::get_offset_of_U3CCacheManagerU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8186 = { sizeof (U3CLoadAssetFromUrlCoroutineU3Ed__16_tD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8186[6] = 
{
	U3CLoadAssetFromUrlCoroutineU3Ed__16_tD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9::get_offset_of_U3CU3E1__state_0(),
	U3CLoadAssetFromUrlCoroutineU3Ed__16_tD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9::get_offset_of_U3CU3E2__current_1(),
	U3CLoadAssetFromUrlCoroutineU3Ed__16_tD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9::get_offset_of_U3CU3E4__this_2(),
	U3CLoadAssetFromUrlCoroutineU3Ed__16_tD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9::get_offset_of_url_3(),
	U3CLoadAssetFromUrlCoroutineU3Ed__16_tD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9::get_offset_of_extension_4(),
	U3CLoadAssetFromUrlCoroutineU3Ed__16_tD4A87618960BE6F7FD8B1F1C03C4C5B0C5DFE1F9::get_offset_of_callback_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8187 = { sizeof (U3CLoadAssetFromUrlCoroutineU3Ed__17_tBE7673059893621530E3AF59005C7C101AB97C74), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8187[5] = 
{
	U3CLoadAssetFromUrlCoroutineU3Ed__17_tBE7673059893621530E3AF59005C7C101AB97C74::get_offset_of_U3CU3E1__state_0(),
	U3CLoadAssetFromUrlCoroutineU3Ed__17_tBE7673059893621530E3AF59005C7C101AB97C74::get_offset_of_U3CU3E2__current_1(),
	U3CLoadAssetFromUrlCoroutineU3Ed__17_tBE7673059893621530E3AF59005C7C101AB97C74::get_offset_of_U3CU3E4__this_2(),
	U3CLoadAssetFromUrlCoroutineU3Ed__17_tBE7673059893621530E3AF59005C7C101AB97C74::get_offset_of_url_3(),
	U3CLoadAssetFromUrlCoroutineU3Ed__17_tBE7673059893621530E3AF59005C7C101AB97C74::get_offset_of_callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8188 = { sizeof (U3CLoadAssetBundleFromUrlCoroutineU3Ed__21_tB11626F401D47B0E27C21F7131B0EF45BFDA8139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8188[6] = 
{
	U3CLoadAssetBundleFromUrlCoroutineU3Ed__21_tB11626F401D47B0E27C21F7131B0EF45BFDA8139::get_offset_of_U3CU3E1__state_0(),
	U3CLoadAssetBundleFromUrlCoroutineU3Ed__21_tB11626F401D47B0E27C21F7131B0EF45BFDA8139::get_offset_of_U3CU3E2__current_1(),
	U3CLoadAssetBundleFromUrlCoroutineU3Ed__21_tB11626F401D47B0E27C21F7131B0EF45BFDA8139::get_offset_of_url_2(),
	U3CLoadAssetBundleFromUrlCoroutineU3Ed__21_tB11626F401D47B0E27C21F7131B0EF45BFDA8139::get_offset_of_callback_3(),
	U3CLoadAssetBundleFromUrlCoroutineU3Ed__21_tB11626F401D47B0E27C21F7131B0EF45BFDA8139::get_offset_of_U3CversionU3E5__2_4(),
	U3CLoadAssetBundleFromUrlCoroutineU3Ed__21_tB11626F401D47B0E27C21F7131B0EF45BFDA8139::get_offset_of_U3CwwwU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8189 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable8189[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8190 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable8190[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8191 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8192 = { sizeof (ImageAsset_t828547FE133CA2715FCF37D6CEFAE756ABA4A7BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8192[2] = 
{
	ImageAsset_t828547FE133CA2715FCF37D6CEFAE756ABA4A7BE::get_offset_of_U3CImageU3Ek__BackingField_0(),
	ImageAsset_t828547FE133CA2715FCF37D6CEFAE756ABA4A7BE::get_offset_of_U3CNameU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8193 = { sizeof (U3CLoadU3Ed__8_t4E60211E9CF00CA549D823E3FFBBD86F5F92FA94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8193[5] = 
{
	U3CLoadU3Ed__8_t4E60211E9CF00CA549D823E3FFBBD86F5F92FA94::get_offset_of_U3CU3E1__state_0(),
	U3CLoadU3Ed__8_t4E60211E9CF00CA549D823E3FFBBD86F5F92FA94::get_offset_of_U3CU3E2__current_1(),
	U3CLoadU3Ed__8_t4E60211E9CF00CA549D823E3FFBBD86F5F92FA94::get_offset_of_U3CU3E4__this_2(),
	U3CLoadU3Ed__8_t4E60211E9CF00CA549D823E3FFBBD86F5F92FA94::get_offset_of_bytes_3(),
	U3CLoadU3Ed__8_t4E60211E9CF00CA549D823E3FFBBD86F5F92FA94::get_offset_of_callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8194 = { sizeof (ModelAsset_t32D8CDA195AA610C8ADB2DD616971E00BE14155F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8194[3] = 
{
	ModelAsset_t32D8CDA195AA610C8ADB2DD616971E00BE14155F::get_offset_of_U3CObjectU3Ek__BackingField_0(),
	ModelAsset_t32D8CDA195AA610C8ADB2DD616971E00BE14155F::get_offset_of_U3CNameU3Ek__BackingField_1(),
	ModelAsset_t32D8CDA195AA610C8ADB2DD616971E00BE14155F::get_offset_of_U3CUrlU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8195 = { sizeof (U3CLoadU3Ed__13_tDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8195[6] = 
{
	U3CLoadU3Ed__13_tDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914::get_offset_of_U3CU3E1__state_0(),
	U3CLoadU3Ed__13_tDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914::get_offset_of_U3CU3E2__current_1(),
	U3CLoadU3Ed__13_tDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914::get_offset_of_U3CU3E4__this_2(),
	U3CLoadU3Ed__13_tDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914::get_offset_of_originalUrl_3(),
	U3CLoadU3Ed__13_tDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914::get_offset_of_callback_4(),
	U3CLoadU3Ed__13_tDAB2A6CDA1CC54B74AA35C1EF41CA8FC3AF49914::get_offset_of_U3CgltfU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8196 = { sizeof (ScriptAsset_t5634BE2EB46C68F69487B801410DDAB631B6E994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8196[2] = 
{
	ScriptAsset_t5634BE2EB46C68F69487B801410DDAB631B6E994::get_offset_of_U3CTextU3Ek__BackingField_0(),
	ScriptAsset_t5634BE2EB46C68F69487B801410DDAB631B6E994::get_offset_of_U3CNameU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8197 = { sizeof (U3CLoadU3Ed__8_t65DD09E32B008CA9BDBAC3A52362119AE6211186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8197[5] = 
{
	U3CLoadU3Ed__8_t65DD09E32B008CA9BDBAC3A52362119AE6211186::get_offset_of_U3CU3E1__state_0(),
	U3CLoadU3Ed__8_t65DD09E32B008CA9BDBAC3A52362119AE6211186::get_offset_of_U3CU3E2__current_1(),
	U3CLoadU3Ed__8_t65DD09E32B008CA9BDBAC3A52362119AE6211186::get_offset_of_U3CU3E4__this_2(),
	U3CLoadU3Ed__8_t65DD09E32B008CA9BDBAC3A52362119AE6211186::get_offset_of_bytes_3(),
	U3CLoadU3Ed__8_t65DD09E32B008CA9BDBAC3A52362119AE6211186::get_offset_of_callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8198 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8199 = { sizeof (VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8199[15] = 
{
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE::get_offset_of_U3CAttachedVideoOptionsU3Ek__BackingField_4(),
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE::get_offset_of_U3CProgressU3Ek__BackingField_5(),
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE::get_offset_of_U3CProgressBarU3Ek__BackingField_6(),
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE::get_offset_of_U3CProgressDraggerU3Ek__BackingField_7(),
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE::get_offset_of_U3CPlayButtonU3Ek__BackingField_8(),
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE::get_offset_of_U3CPauseButtonU3Ek__BackingField_9(),
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE::get_offset_of_minProgressTransform_10(),
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE::get_offset_of_maxProgressTransform_11(),
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE::get_offset_of_hasDisplayedFirstFrame_12(),
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE::get_offset_of_updatingProgress_13(),
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE::get_offset_of_lastProgressChange_14(),
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE::get_offset_of_potentialMenuPositions_15(),
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE::get_offset_of_moveVelocity_16(),
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE::get_offset_of_inExhibitBox_17(),
	VideoControls_t23B5B1B5640362B413000093351D1A6C473A00AE::get_offset_of_Gestures_18(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
