﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Jint.Engine
struct Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D;
// Jint.Native.Argument.ArgumentsInstance/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B;
// Jint.Native.Array.ArrayInstance
struct ArrayInstance_t3FAE9584CB9935D861D86E72FE19FE7EB6C5016E;
// Jint.Native.Array.ArrayPrototype
struct ArrayPrototype_t02B45CBA6C396ABAB36D738790A9B466E69C0A80;
// Jint.Native.Boolean.BooleanPrototype
struct BooleanPrototype_tF496E12A7D263301D5E7E383BC0DB245AD8A1C03;
// Jint.Native.Date.DatePrototype
struct DatePrototype_t1A74663765296A18E85477D2A2F0EB7B7962D07E;
// Jint.Native.Error.ErrorPrototype
struct ErrorPrototype_tB21E1B2F5B399BBE416BDC3CA41436C1D77F9B3B;
// Jint.Native.Function.FunctionInstance
struct FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458;
// Jint.Native.Function.FunctionPrototype
struct FunctionPrototype_tF0430BB38A6440E67399DEFFA08B7D87561B4C79;
// Jint.Native.ICallable
struct ICallable_tF83AEB2D5F421A41A2E5A91EEF5B691C2BDBF5B6;
// Jint.Native.JsValue
struct JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19;
// Jint.Native.JsValue[]
struct JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88;
// Jint.Native.Json.JsonParser/Extra
struct Extra_t44DEAC88F69744BDF6E5EA00CED22E44FC94D634;
// Jint.Native.Json.JsonParser/Token
struct Token_t00D4946B1CE66C52F6C421D710DB429762485B01;
// Jint.Native.Number.Dtoa.CachedPowers/CachedPower[]
struct CachedPowerU5BU5D_t9FF2F013F11604303179616A8BC7B99E02CD0978;
// Jint.Native.Number.NumberPrototype
struct NumberPrototype_t9D802198E4B8D3A4C03E4C8E58B0966FB922F876;
// Jint.Native.Object.ObjectInstance
struct ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900;
// Jint.Native.Object.ObjectPrototype
struct ObjectPrototype_tEC07D7C1F708C35E24A7171E19D9A411DD0FECDB;
// Jint.Native.RegExp.RegExpPrototype
struct RegExpPrototype_t766F1B8425C5B57B62666EDDBC896F51AC3FFA1D;
// Jint.Native.String.StringPrototype
struct StringPrototype_t3AE5DF2E522E2F4170AEDACC9CF807F5538FCF2D;
// Jint.Parser.Ast.Expression
struct Expression_tAC599936AD6D021EA6A3A695C803975140919ADB;
// Jint.Parser.Ast.Identifier
struct Identifier_t50FB8837E02860B417C98D78294E352C617900EB;
// Jint.Parser.Ast.Statement
struct Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0;
// Jint.Parser.IFunctionDeclaration
struct IFunctionDeclaration_tD3833E6BAA01817FC32927914B5A1E213530D502;
// Jint.Parser.Location
struct Location_t9639547EECD85868A9B1627343BF1C80DE46B783;
// Jint.Runtime.Descriptors.PropertyDescriptor
struct PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73;
// Jint.Runtime.Environments.EnvironmentRecord
struct EnvironmentRecord_t81E449E16161905BFB3BD8D0596E82E2619FB53B;
// Jint.Runtime.Environments.LexicalEnvironment
struct LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026;
// MHLab.PATCH.PatchApplier
struct PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938;
// MHLab.PATCH.Settings.SettingsOverrider
struct SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C;
// MHLab.PATCH.Version
struct Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976;
// Octodiff.Core.IHashAlgorithm
struct IHashAlgorithm_t1D31AC58CD374A4FE5168648D1895A17D253E10C;
// Octodiff.Diagnostics.IProgressReporter
struct IProgressReporter_t5CC169CF82CFCE2D376EC0A506E1186FEC321E39;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Jint.Native.Argument.ArgumentsInstance>
struct Action_1_tB88E99B016BB97FBDF8031F96C58443230EF7498;
// System.Action`1<Jint.Native.JsValue>
struct Action_1_tD813DBEEC2FAAF8612CCE843E86C5DD44B7EA1AD;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE;
// System.Action`2<System.String,System.String>
struct Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757;
// System.Action`3<System.Int64,System.Int64,System.Int32>
struct Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281;
// System.Action`3<System.String,System.String,System.Exception>
struct Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A;
// System.Collections.Generic.IDictionary`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>
struct IDictionary_2_tA1DF5A6B6E4E121455B7694170DC28D1CBB47AED;
// System.Collections.Generic.IDictionary`2<System.UInt32,Jint.Runtime.Descriptors.PropertyDescriptor>
struct IDictionary_2_tB77AB585E30782408CD279744AE9CEBFFA582A37;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>>
struct IEnumerator_1_t858701A14C232E24122AEE2DD678CFC8213D3910;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,Jint.Runtime.Descriptors.PropertyDescriptor>>
struct IEnumerator_1_t3339F034800CDC4D31FBA569035D1EE15C358CE9;
// System.Collections.Generic.List`1<Jint.Native.Json.JsonParser/Token>
struct List_1_tA5B30B21C63CE4B4670B08A30938650292A9C98C;
// System.Collections.Generic.List`1<MHLab.PATCH.Patch>
struct List_1_tE24C1D4E7B1731EAAFBDFF55DEAEF3A76E38C5CE;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.Stack`1<System.Int32>
struct Stack_1_t09A8EBB6E2F5CCF7E2E67BDECA461795F93D523A;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Comparison`1<MHLab.PATCH.Patch>
struct Comparison_1_tB495CD0B15D943013A6EEF2B915E351640B62754;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`2<Jint.Parser.Ast.Identifier,System.String>
struct Func_2_tB82667B46D0DE3B8E984BE60AB203DC1BD410848;
// System.Func`2<MHLab.PATCH.Patch,System.Boolean>
struct Func_2_tB31DDBA848EDD775F31274A5149C07F6C19DB2A6;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>,Jint.Runtime.Descriptors.PropertyDescriptor>
struct Func_2_t87986FF485CF6F7356DFB534C9CC23ED97BDD96D;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>,System.Boolean>
struct Func_2_t24F919A9CB24363AA52DA57D86B524C1C6E932C0;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>,System.String>
struct Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759;
// System.Func`2<System.String,Jint.Parser.Ast.Identifier>
struct Func_2_t67EE28E631D84CBB5C39518AD0DF71EDF1F5C7F9;
// System.Func`2<System.String,System.String>
struct Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96;
// System.Func`3<Jint.Engine,System.Object,Jint.Native.JsValue>
struct Func_3_t0263D647E14FFD8A6878068A09948B5B31C8110B;
// System.IO.BinaryReader
struct BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Random
struct Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.RegularExpressions.Regex
struct Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T704E3C0969FBB43968385371DD4472AF67DF3706_H
#define U3CMODULEU3E_T704E3C0969FBB43968385371DD4472AF67DF3706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t704E3C0969FBB43968385371DD4472AF67DF3706 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T704E3C0969FBB43968385371DD4472AF67DF3706_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T75B025429FF850EE010C748A7ACBCA234F395E7B_H
#define U3CU3EC__DISPLAYCLASS8_0_T75B025429FF850EE010C748A7ACBCA234F395E7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Argument.ArgumentsInstance/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B  : public RuntimeObject
{
public:
	// Jint.Native.JsValue[] Jint.Native.Argument.ArgumentsInstance/<>c__DisplayClass8_0::args
	JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* ___args_0;
	// Jint.Engine Jint.Native.Argument.ArgumentsInstance/<>c__DisplayClass8_0::engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ___engine_1;
	// System.String[] Jint.Native.Argument.ArgumentsInstance/<>c__DisplayClass8_0::names
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___names_2;
	// System.Boolean Jint.Native.Argument.ArgumentsInstance/<>c__DisplayClass8_0::strict
	bool ___strict_3;
	// Jint.Runtime.Environments.EnvironmentRecord Jint.Native.Argument.ArgumentsInstance/<>c__DisplayClass8_0::env
	EnvironmentRecord_t81E449E16161905BFB3BD8D0596E82E2619FB53B * ___env_4;
	// Jint.Native.Function.FunctionInstance Jint.Native.Argument.ArgumentsInstance/<>c__DisplayClass8_0::func
	FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458 * ___func_5;

public:
	inline static int32_t get_offset_of_args_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B, ___args_0)); }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* get_args_0() const { return ___args_0; }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88** get_address_of_args_0() { return &___args_0; }
	inline void set_args_0(JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* value)
	{
		___args_0 = value;
		Il2CppCodeGenWriteBarrier((&___args_0), value);
	}

	inline static int32_t get_offset_of_engine_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B, ___engine_1)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get_engine_1() const { return ___engine_1; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of_engine_1() { return &___engine_1; }
	inline void set_engine_1(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		___engine_1 = value;
		Il2CppCodeGenWriteBarrier((&___engine_1), value);
	}

	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B, ___names_2)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_names_2() const { return ___names_2; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier((&___names_2), value);
	}

	inline static int32_t get_offset_of_strict_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B, ___strict_3)); }
	inline bool get_strict_3() const { return ___strict_3; }
	inline bool* get_address_of_strict_3() { return &___strict_3; }
	inline void set_strict_3(bool value)
	{
		___strict_3 = value;
	}

	inline static int32_t get_offset_of_env_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B, ___env_4)); }
	inline EnvironmentRecord_t81E449E16161905BFB3BD8D0596E82E2619FB53B * get_env_4() const { return ___env_4; }
	inline EnvironmentRecord_t81E449E16161905BFB3BD8D0596E82E2619FB53B ** get_address_of_env_4() { return &___env_4; }
	inline void set_env_4(EnvironmentRecord_t81E449E16161905BFB3BD8D0596E82E2619FB53B * value)
	{
		___env_4 = value;
		Il2CppCodeGenWriteBarrier((&___env_4), value);
	}

	inline static int32_t get_offset_of_func_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B, ___func_5)); }
	inline FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458 * get_func_5() const { return ___func_5; }
	inline FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458 ** get_address_of_func_5() { return &___func_5; }
	inline void set_func_5(FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458 * value)
	{
		___func_5 = value;
		Il2CppCodeGenWriteBarrier((&___func_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T75B025429FF850EE010C748A7ACBCA234F395E7B_H
#ifndef U3CU3EC__DISPLAYCLASS8_1_T69B02D7552EA92BB50A63A6FC19F7B88218C47CB_H
#define U3CU3EC__DISPLAYCLASS8_1_T69B02D7552EA92BB50A63A6FC19F7B88218C47CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Argument.ArgumentsInstance/<>c__DisplayClass8_1
struct  U3CU3Ec__DisplayClass8_1_t69B02D7552EA92BB50A63A6FC19F7B88218C47CB  : public RuntimeObject
{
public:
	// System.String Jint.Native.Argument.ArgumentsInstance/<>c__DisplayClass8_1::name
	String_t* ___name_0;
	// Jint.Native.Argument.ArgumentsInstance/<>c__DisplayClass8_0 Jint.Native.Argument.ArgumentsInstance/<>c__DisplayClass8_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_1_t69B02D7552EA92BB50A63A6FC19F7B88218C47CB, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_1_t69B02D7552EA92BB50A63A6FC19F7B88218C47CB, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_1_T69B02D7552EA92BB50A63A6FC19F7B88218C47CB_H
#ifndef U3CU3EC_TCAF0295190D68D942B0720F12D542470E433930A_H
#define U3CU3EC_TCAF0295190D68D942B0720F12D542470E433930A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Array.ArrayPrototype/<>c
struct  U3CU3Ec_tCAF0295190D68D942B0720F12D542470E433930A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tCAF0295190D68D942B0720F12D542470E433930A_StaticFields
{
public:
	// Jint.Native.Array.ArrayPrototype/<>c Jint.Native.Array.ArrayPrototype/<>c::<>9
	U3CU3Ec_tCAF0295190D68D942B0720F12D542470E433930A * ___U3CU3E9_0;
	// System.Action`1<Jint.Native.JsValue> Jint.Native.Array.ArrayPrototype/<>c::<>9__20_1
	Action_1_tD813DBEEC2FAAF8612CCE843E86C5DD44B7EA1AD * ___U3CU3E9__20_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCAF0295190D68D942B0720F12D542470E433930A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tCAF0295190D68D942B0720F12D542470E433930A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tCAF0295190D68D942B0720F12D542470E433930A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tCAF0295190D68D942B0720F12D542470E433930A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__20_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCAF0295190D68D942B0720F12D542470E433930A_StaticFields, ___U3CU3E9__20_1_1)); }
	inline Action_1_tD813DBEEC2FAAF8612CCE843E86C5DD44B7EA1AD * get_U3CU3E9__20_1_1() const { return ___U3CU3E9__20_1_1; }
	inline Action_1_tD813DBEEC2FAAF8612CCE843E86C5DD44B7EA1AD ** get_address_of_U3CU3E9__20_1_1() { return &___U3CU3E9__20_1_1; }
	inline void set_U3CU3E9__20_1_1(Action_1_tD813DBEEC2FAAF8612CCE843E86C5DD44B7EA1AD * value)
	{
		___U3CU3E9__20_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__20_1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TCAF0295190D68D942B0720F12D542470E433930A_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T9BE102B85A5047BB6E74A0770C0BEAEAADEA6A3F_H
#define U3CU3EC__DISPLAYCLASS13_0_T9BE102B85A5047BB6E74A0770C0BEAEAADEA6A3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Array.ArrayPrototype/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t9BE102B85A5047BB6E74A0770C0BEAEAADEA6A3F  : public RuntimeObject
{
public:
	// Jint.Native.ICallable Jint.Native.Array.ArrayPrototype/<>c__DisplayClass13_0::compareFn
	RuntimeObject* ___compareFn_0;
	// Jint.Native.Object.ObjectInstance Jint.Native.Array.ArrayPrototype/<>c__DisplayClass13_0::obj
	ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * ___obj_1;

public:
	inline static int32_t get_offset_of_compareFn_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t9BE102B85A5047BB6E74A0770C0BEAEAADEA6A3F, ___compareFn_0)); }
	inline RuntimeObject* get_compareFn_0() const { return ___compareFn_0; }
	inline RuntimeObject** get_address_of_compareFn_0() { return &___compareFn_0; }
	inline void set_compareFn_0(RuntimeObject* value)
	{
		___compareFn_0 = value;
		Il2CppCodeGenWriteBarrier((&___compareFn_0), value);
	}

	inline static int32_t get_offset_of_obj_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t9BE102B85A5047BB6E74A0770C0BEAEAADEA6A3F, ___obj_1)); }
	inline ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * get_obj_1() const { return ___obj_1; }
	inline ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 ** get_address_of_obj_1() { return &___obj_1; }
	inline void set_obj_1(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * value)
	{
		___obj_1 = value;
		Il2CppCodeGenWriteBarrier((&___obj_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T9BE102B85A5047BB6E74A0770C0BEAEAADEA6A3F_H
#ifndef U3CU3EC__DISPLAYCLASS20_0_TC410025BF2B7FB429803AF15A4B4541215C77EFE_H
#define U3CU3EC__DISPLAYCLASS20_0_TC410025BF2B7FB429803AF15A4B4541215C77EFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Array.ArrayPrototype/<>c__DisplayClass20_0
struct  U3CU3Ec__DisplayClass20_0_tC410025BF2B7FB429803AF15A4B4541215C77EFE  : public RuntimeObject
{
public:
	// Jint.Native.ICallable Jint.Native.Array.ArrayPrototype/<>c__DisplayClass20_0::func
	RuntimeObject* ___func_0;
	// Jint.Native.Array.ArrayPrototype Jint.Native.Array.ArrayPrototype/<>c__DisplayClass20_0::<>4__this
	ArrayPrototype_t02B45CBA6C396ABAB36D738790A9B466E69C0A80 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_func_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tC410025BF2B7FB429803AF15A4B4541215C77EFE, ___func_0)); }
	inline RuntimeObject* get_func_0() const { return ___func_0; }
	inline RuntimeObject** get_address_of_func_0() { return &___func_0; }
	inline void set_func_0(RuntimeObject* value)
	{
		___func_0 = value;
		Il2CppCodeGenWriteBarrier((&___func_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tC410025BF2B7FB429803AF15A4B4541215C77EFE, ___U3CU3E4__this_1)); }
	inline ArrayPrototype_t02B45CBA6C396ABAB36D738790A9B466E69C0A80 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ArrayPrototype_t02B45CBA6C396ABAB36D738790A9B466E69C0A80 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ArrayPrototype_t02B45CBA6C396ABAB36D738790A9B466E69C0A80 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS20_0_TC410025BF2B7FB429803AF15A4B4541215C77EFE_H
#ifndef U3CU3EC_T08D26E0D571CE9310A6335B765A54D63B54C9E9B_H
#define U3CU3EC_T08D26E0D571CE9310A6335B765A54D63B54C9E9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Function.FunctionConstructor/<>c
struct  U3CU3Ec_t08D26E0D571CE9310A6335B765A54D63B54C9E9B  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t08D26E0D571CE9310A6335B765A54D63B54C9E9B_StaticFields
{
public:
	// Jint.Native.Function.FunctionConstructor/<>c Jint.Native.Function.FunctionConstructor/<>c::<>9
	U3CU3Ec_t08D26E0D571CE9310A6335B765A54D63B54C9E9B * ___U3CU3E9_0;
	// System.Func`2<System.String,Jint.Parser.Ast.Identifier> Jint.Native.Function.FunctionConstructor/<>c::<>9__9_0
	Func_2_t67EE28E631D84CBB5C39518AD0DF71EDF1F5C7F9 * ___U3CU3E9__9_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t08D26E0D571CE9310A6335B765A54D63B54C9E9B_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t08D26E0D571CE9310A6335B765A54D63B54C9E9B * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t08D26E0D571CE9310A6335B765A54D63B54C9E9B ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t08D26E0D571CE9310A6335B765A54D63B54C9E9B * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t08D26E0D571CE9310A6335B765A54D63B54C9E9B_StaticFields, ___U3CU3E9__9_0_1)); }
	inline Func_2_t67EE28E631D84CBB5C39518AD0DF71EDF1F5C7F9 * get_U3CU3E9__9_0_1() const { return ___U3CU3E9__9_0_1; }
	inline Func_2_t67EE28E631D84CBB5C39518AD0DF71EDF1F5C7F9 ** get_address_of_U3CU3E9__9_0_1() { return &___U3CU3E9__9_0_1; }
	inline void set_U3CU3E9__9_0_1(Func_2_t67EE28E631D84CBB5C39518AD0DF71EDF1F5C7F9 * value)
	{
		___U3CU3E9__9_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__9_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T08D26E0D571CE9310A6335B765A54D63B54C9E9B_H
#ifndef U3CU3EC_TD424B78C5B503E08486D4BB009F1AFB919516C0E_H
#define U3CU3EC_TD424B78C5B503E08486D4BB009F1AFB919516C0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Function.ScriptFunctionInstance/<>c
struct  U3CU3Ec_tD424B78C5B503E08486D4BB009F1AFB919516C0E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tD424B78C5B503E08486D4BB009F1AFB919516C0E_StaticFields
{
public:
	// Jint.Native.Function.ScriptFunctionInstance/<>c Jint.Native.Function.ScriptFunctionInstance/<>c::<>9
	U3CU3Ec_tD424B78C5B503E08486D4BB009F1AFB919516C0E * ___U3CU3E9_0;
	// System.Func`2<Jint.Parser.Ast.Identifier,System.String> Jint.Native.Function.ScriptFunctionInstance/<>c::<>9__1_0
	Func_2_tB82667B46D0DE3B8E984BE60AB203DC1BD410848 * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD424B78C5B503E08486D4BB009F1AFB919516C0E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tD424B78C5B503E08486D4BB009F1AFB919516C0E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tD424B78C5B503E08486D4BB009F1AFB919516C0E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tD424B78C5B503E08486D4BB009F1AFB919516C0E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD424B78C5B503E08486D4BB009F1AFB919516C0E_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_tB82667B46D0DE3B8E984BE60AB203DC1BD410848 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_tB82667B46D0DE3B8E984BE60AB203DC1BD410848 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_tB82667B46D0DE3B8E984BE60AB203DC1BD410848 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TD424B78C5B503E08486D4BB009F1AFB919516C0E_H
#ifndef U3CU3EC_TE628833A5B75D69477BA5C23673B22B219921407_H
#define U3CU3EC_TE628833A5B75D69477BA5C23673B22B219921407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.JsValue/<>c
struct  U3CU3Ec_tE628833A5B75D69477BA5C23673B22B219921407  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tE628833A5B75D69477BA5C23673B22B219921407_StaticFields
{
public:
	// Jint.Native.JsValue/<>c Jint.Native.JsValue/<>c::<>9
	U3CU3Ec_tE628833A5B75D69477BA5C23673B22B219921407 * ___U3CU3E9_0;
	// System.Func`3<Jint.Engine,System.Object,Jint.Native.JsValue> Jint.Native.JsValue/<>c::<>9__35_0
	Func_3_t0263D647E14FFD8A6878068A09948B5B31C8110B * ___U3CU3E9__35_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE628833A5B75D69477BA5C23673B22B219921407_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tE628833A5B75D69477BA5C23673B22B219921407 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tE628833A5B75D69477BA5C23673B22B219921407 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tE628833A5B75D69477BA5C23673B22B219921407 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE628833A5B75D69477BA5C23673B22B219921407_StaticFields, ___U3CU3E9__35_0_1)); }
	inline Func_3_t0263D647E14FFD8A6878068A09948B5B31C8110B * get_U3CU3E9__35_0_1() const { return ___U3CU3E9__35_0_1; }
	inline Func_3_t0263D647E14FFD8A6878068A09948B5B31C8110B ** get_address_of_U3CU3E9__35_0_1() { return &___U3CU3E9__35_0_1; }
	inline void set_U3CU3E9__35_0_1(Func_3_t0263D647E14FFD8A6878068A09948B5B31C8110B * value)
	{
		___U3CU3E9__35_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__35_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TE628833A5B75D69477BA5C23673B22B219921407_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_T2B171BBBBC38F00CD1CF51AE8D112FE05F150826_H
#define U3CU3EC__DISPLAYCLASS35_0_T2B171BBBBC38F00CD1CF51AE8D112FE05F150826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.JsValue/<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_t2B171BBBBC38F00CD1CF51AE8D112FE05F150826  : public RuntimeObject
{
public:
	// Jint.Engine Jint.Native.JsValue/<>c__DisplayClass35_0::engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ___engine_0;

public:
	inline static int32_t get_offset_of_engine_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t2B171BBBBC38F00CD1CF51AE8D112FE05F150826, ___engine_0)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get_engine_0() const { return ___engine_0; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of_engine_0() { return &___engine_0; }
	inline void set_engine_0(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		___engine_0 = value;
		Il2CppCodeGenWriteBarrier((&___engine_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_T2B171BBBBC38F00CD1CF51AE8D112FE05F150826_H
#ifndef JSVALUEDEBUGVIEW_TD22862785DEE056971356FB2EEF6B23328C755F7_H
#define JSVALUEDEBUGVIEW_TD22862785DEE056971356FB2EEF6B23328C755F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.JsValue/JsValueDebugView
struct  JsValueDebugView_tD22862785DEE056971356FB2EEF6B23328C755F7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSVALUEDEBUGVIEW_TD22862785DEE056971356FB2EEF6B23328C755F7_H
#ifndef JSONSERIALIZER_TD12BA78AD82034231EB2236DE3A1F627CAF98ABC_H
#define JSONSERIALIZER_TD12BA78AD82034231EB2236DE3A1F627CAF98ABC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Json.JsonSerializer
struct  JsonSerializer_tD12BA78AD82034231EB2236DE3A1F627CAF98ABC  : public RuntimeObject
{
public:
	// Jint.Engine Jint.Native.Json.JsonSerializer::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_0;
	// System.Collections.Generic.Stack`1<System.Object> Jint.Native.Json.JsonSerializer::_stack
	Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * ____stack_1;
	// System.String Jint.Native.Json.JsonSerializer::_indent
	String_t* ____indent_2;
	// System.String Jint.Native.Json.JsonSerializer::_gap
	String_t* ____gap_3;
	// System.Collections.Generic.List`1<System.String> Jint.Native.Json.JsonSerializer::_propertyList
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____propertyList_4;
	// Jint.Native.JsValue Jint.Native.Json.JsonSerializer::_replacerFunction
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ____replacerFunction_5;

public:
	inline static int32_t get_offset_of__engine_0() { return static_cast<int32_t>(offsetof(JsonSerializer_tD12BA78AD82034231EB2236DE3A1F627CAF98ABC, ____engine_0)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_0() const { return ____engine_0; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_0() { return &____engine_0; }
	inline void set__engine_0(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_0 = value;
		Il2CppCodeGenWriteBarrier((&____engine_0), value);
	}

	inline static int32_t get_offset_of__stack_1() { return static_cast<int32_t>(offsetof(JsonSerializer_tD12BA78AD82034231EB2236DE3A1F627CAF98ABC, ____stack_1)); }
	inline Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * get__stack_1() const { return ____stack_1; }
	inline Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 ** get_address_of__stack_1() { return &____stack_1; }
	inline void set__stack_1(Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * value)
	{
		____stack_1 = value;
		Il2CppCodeGenWriteBarrier((&____stack_1), value);
	}

	inline static int32_t get_offset_of__indent_2() { return static_cast<int32_t>(offsetof(JsonSerializer_tD12BA78AD82034231EB2236DE3A1F627CAF98ABC, ____indent_2)); }
	inline String_t* get__indent_2() const { return ____indent_2; }
	inline String_t** get_address_of__indent_2() { return &____indent_2; }
	inline void set__indent_2(String_t* value)
	{
		____indent_2 = value;
		Il2CppCodeGenWriteBarrier((&____indent_2), value);
	}

	inline static int32_t get_offset_of__gap_3() { return static_cast<int32_t>(offsetof(JsonSerializer_tD12BA78AD82034231EB2236DE3A1F627CAF98ABC, ____gap_3)); }
	inline String_t* get__gap_3() const { return ____gap_3; }
	inline String_t** get_address_of__gap_3() { return &____gap_3; }
	inline void set__gap_3(String_t* value)
	{
		____gap_3 = value;
		Il2CppCodeGenWriteBarrier((&____gap_3), value);
	}

	inline static int32_t get_offset_of__propertyList_4() { return static_cast<int32_t>(offsetof(JsonSerializer_tD12BA78AD82034231EB2236DE3A1F627CAF98ABC, ____propertyList_4)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__propertyList_4() const { return ____propertyList_4; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__propertyList_4() { return &____propertyList_4; }
	inline void set__propertyList_4(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____propertyList_4 = value;
		Il2CppCodeGenWriteBarrier((&____propertyList_4), value);
	}

	inline static int32_t get_offset_of__replacerFunction_5() { return static_cast<int32_t>(offsetof(JsonSerializer_tD12BA78AD82034231EB2236DE3A1F627CAF98ABC, ____replacerFunction_5)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get__replacerFunction_5() const { return ____replacerFunction_5; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of__replacerFunction_5() { return &____replacerFunction_5; }
	inline void set__replacerFunction_5(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		____replacerFunction_5 = value;
		Il2CppCodeGenWriteBarrier((&____replacerFunction_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZER_TD12BA78AD82034231EB2236DE3A1F627CAF98ABC_H
#ifndef U3CU3EC_TA51A29EDAD4C2DC91275E1CB4A17187897F2691D_H
#define U3CU3EC_TA51A29EDAD4C2DC91275E1CB4A17187897F2691D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Json.JsonSerializer/<>c
struct  U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D_StaticFields
{
public:
	// Jint.Native.Json.JsonSerializer/<>c Jint.Native.Json.JsonSerializer/<>c::<>9
	U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D * ___U3CU3E9_0;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>,Jint.Runtime.Descriptors.PropertyDescriptor> Jint.Native.Json.JsonSerializer/<>c::<>9__7_0
	Func_2_t87986FF485CF6F7356DFB534C9CC23ED97BDD96D * ___U3CU3E9__7_0_1;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>,System.Boolean> Jint.Native.Json.JsonSerializer/<>c::<>9__12_0
	Func_2_t24F919A9CB24363AA52DA57D86B524C1C6E932C0 * ___U3CU3E9__12_0_2;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>,System.String> Jint.Native.Json.JsonSerializer/<>c::<>9__12_1
	Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 * ___U3CU3E9__12_1_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D_StaticFields, ___U3CU3E9__7_0_1)); }
	inline Func_2_t87986FF485CF6F7356DFB534C9CC23ED97BDD96D * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline Func_2_t87986FF485CF6F7356DFB534C9CC23ED97BDD96D ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(Func_2_t87986FF485CF6F7356DFB534C9CC23ED97BDD96D * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D_StaticFields, ___U3CU3E9__12_0_2)); }
	inline Func_2_t24F919A9CB24363AA52DA57D86B524C1C6E932C0 * get_U3CU3E9__12_0_2() const { return ___U3CU3E9__12_0_2; }
	inline Func_2_t24F919A9CB24363AA52DA57D86B524C1C6E932C0 ** get_address_of_U3CU3E9__12_0_2() { return &___U3CU3E9__12_0_2; }
	inline void set_U3CU3E9__12_0_2(Func_2_t24F919A9CB24363AA52DA57D86B524C1C6E932C0 * value)
	{
		___U3CU3E9__12_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D_StaticFields, ___U3CU3E9__12_1_3)); }
	inline Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 * get_U3CU3E9__12_1_3() const { return ___U3CU3E9__12_1_3; }
	inline Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 ** get_address_of_U3CU3E9__12_1_3() { return &___U3CU3E9__12_1_3; }
	inline void set_U3CU3E9__12_1_3(Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 * value)
	{
		___U3CU3E9__12_1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TA51A29EDAD4C2DC91275E1CB4A17187897F2691D_H
#ifndef NULL_TAED0901ECC3A323E8142DAD74DFB342D0E3DDB74_H
#define NULL_TAED0901ECC3A323E8142DAD74DFB342D0E3DDB74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Null
struct  Null_tAED0901ECC3A323E8142DAD74DFB342D0E3DDB74  : public RuntimeObject
{
public:

public:
};

struct Null_tAED0901ECC3A323E8142DAD74DFB342D0E3DDB74_StaticFields
{
public:
	// Jint.Native.JsValue Jint.Native.Null::Instance
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___Instance_0;
	// System.String Jint.Native.Null::Text
	String_t* ___Text_1;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(Null_tAED0901ECC3A323E8142DAD74DFB342D0E3DDB74_StaticFields, ___Instance_0)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_Instance_0() const { return ___Instance_0; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}

	inline static int32_t get_offset_of_Text_1() { return static_cast<int32_t>(offsetof(Null_tAED0901ECC3A323E8142DAD74DFB342D0E3DDB74_StaticFields, ___Text_1)); }
	inline String_t* get_Text_1() const { return ___Text_1; }
	inline String_t** get_address_of_Text_1() { return &___Text_1; }
	inline void set_Text_1(String_t* value)
	{
		___Text_1 = value;
		Il2CppCodeGenWriteBarrier((&___Text_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULL_TAED0901ECC3A323E8142DAD74DFB342D0E3DDB74_H
#ifndef CACHEDPOWERS_TABF022BFEFBCC9E0A2CEB7B2C67E994B63BF9380_H
#define CACHEDPOWERS_TABF022BFEFBCC9E0A2CEB7B2C67E994B63BF9380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Number.Dtoa.CachedPowers
struct  CachedPowers_tABF022BFEFBCC9E0A2CEB7B2C67E994B63BF9380  : public RuntimeObject
{
public:

public:
};

struct CachedPowers_tABF022BFEFBCC9E0A2CEB7B2C67E994B63BF9380_StaticFields
{
public:
	// Jint.Native.Number.Dtoa.CachedPowers/CachedPower[] Jint.Native.Number.Dtoa.CachedPowers::CACHED_POWERS
	CachedPowerU5BU5D_t9FF2F013F11604303179616A8BC7B99E02CD0978* ___CACHED_POWERS_0;

public:
	inline static int32_t get_offset_of_CACHED_POWERS_0() { return static_cast<int32_t>(offsetof(CachedPowers_tABF022BFEFBCC9E0A2CEB7B2C67E994B63BF9380_StaticFields, ___CACHED_POWERS_0)); }
	inline CachedPowerU5BU5D_t9FF2F013F11604303179616A8BC7B99E02CD0978* get_CACHED_POWERS_0() const { return ___CACHED_POWERS_0; }
	inline CachedPowerU5BU5D_t9FF2F013F11604303179616A8BC7B99E02CD0978** get_address_of_CACHED_POWERS_0() { return &___CACHED_POWERS_0; }
	inline void set_CACHED_POWERS_0(CachedPowerU5BU5D_t9FF2F013F11604303179616A8BC7B99E02CD0978* value)
	{
		___CACHED_POWERS_0 = value;
		Il2CppCodeGenWriteBarrier((&___CACHED_POWERS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDPOWERS_TABF022BFEFBCC9E0A2CEB7B2C67E994B63BF9380_H
#ifndef CACHEDPOWER_T803C239948A2313EBA931E7E542083187EC419CB_H
#define CACHEDPOWER_T803C239948A2313EBA931E7E542083187EC419CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Number.Dtoa.CachedPowers/CachedPower
struct  CachedPower_t803C239948A2313EBA931E7E542083187EC419CB  : public RuntimeObject
{
public:
	// System.Int64 Jint.Native.Number.Dtoa.CachedPowers/CachedPower::Significand
	int64_t ___Significand_0;
	// System.Int16 Jint.Native.Number.Dtoa.CachedPowers/CachedPower::BinaryExponent
	int16_t ___BinaryExponent_1;
	// System.Int16 Jint.Native.Number.Dtoa.CachedPowers/CachedPower::DecimalExponent
	int16_t ___DecimalExponent_2;

public:
	inline static int32_t get_offset_of_Significand_0() { return static_cast<int32_t>(offsetof(CachedPower_t803C239948A2313EBA931E7E542083187EC419CB, ___Significand_0)); }
	inline int64_t get_Significand_0() const { return ___Significand_0; }
	inline int64_t* get_address_of_Significand_0() { return &___Significand_0; }
	inline void set_Significand_0(int64_t value)
	{
		___Significand_0 = value;
	}

	inline static int32_t get_offset_of_BinaryExponent_1() { return static_cast<int32_t>(offsetof(CachedPower_t803C239948A2313EBA931E7E542083187EC419CB, ___BinaryExponent_1)); }
	inline int16_t get_BinaryExponent_1() const { return ___BinaryExponent_1; }
	inline int16_t* get_address_of_BinaryExponent_1() { return &___BinaryExponent_1; }
	inline void set_BinaryExponent_1(int16_t value)
	{
		___BinaryExponent_1 = value;
	}

	inline static int32_t get_offset_of_DecimalExponent_2() { return static_cast<int32_t>(offsetof(CachedPower_t803C239948A2313EBA931E7E542083187EC419CB, ___DecimalExponent_2)); }
	inline int16_t get_DecimalExponent_2() const { return ___DecimalExponent_2; }
	inline int16_t* get_address_of_DecimalExponent_2() { return &___DecimalExponent_2; }
	inline void set_DecimalExponent_2(int16_t value)
	{
		___DecimalExponent_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDPOWER_T803C239948A2313EBA931E7E542083187EC419CB_H
#ifndef DIYFP_T9FAD9346E18B6744394689F28AE5E28202C3A9D1_H
#define DIYFP_T9FAD9346E18B6744394689F28AE5E28202C3A9D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Number.Dtoa.DiyFp
struct  DiyFp_t9FAD9346E18B6744394689F28AE5E28202C3A9D1  : public RuntimeObject
{
public:
	// System.Int64 Jint.Native.Number.Dtoa.DiyFp::<F>k__BackingField
	int64_t ___U3CFU3Ek__BackingField_0;
	// System.Int32 Jint.Native.Number.Dtoa.DiyFp::<E>k__BackingField
	int32_t ___U3CEU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DiyFp_t9FAD9346E18B6744394689F28AE5E28202C3A9D1, ___U3CFU3Ek__BackingField_0)); }
	inline int64_t get_U3CFU3Ek__BackingField_0() const { return ___U3CFU3Ek__BackingField_0; }
	inline int64_t* get_address_of_U3CFU3Ek__BackingField_0() { return &___U3CFU3Ek__BackingField_0; }
	inline void set_U3CFU3Ek__BackingField_0(int64_t value)
	{
		___U3CFU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CEU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DiyFp_t9FAD9346E18B6744394689F28AE5E28202C3A9D1, ___U3CEU3Ek__BackingField_1)); }
	inline int32_t get_U3CEU3Ek__BackingField_1() const { return ___U3CEU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CEU3Ek__BackingField_1() { return &___U3CEU3Ek__BackingField_1; }
	inline void set_U3CEU3Ek__BackingField_1(int32_t value)
	{
		___U3CEU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIYFP_T9FAD9346E18B6744394689F28AE5E28202C3A9D1_H
#ifndef DOUBLEHELPER_T1B21D4D69B841266281A3005FAE3BDF893758E1A_H
#define DOUBLEHELPER_T1B21D4D69B841266281A3005FAE3BDF893758E1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Number.Dtoa.DoubleHelper
struct  DoubleHelper_t1B21D4D69B841266281A3005FAE3BDF893758E1A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEHELPER_T1B21D4D69B841266281A3005FAE3BDF893758E1A_H
#ifndef FASTDTOA_T72EBB000B83D5C0C423DC8D997542F5AE0166C8E_H
#define FASTDTOA_T72EBB000B83D5C0C423DC8D997542F5AE0166C8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Number.Dtoa.FastDtoa
struct  FastDtoa_t72EBB000B83D5C0C423DC8D997542F5AE0166C8E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTDTOA_T72EBB000B83D5C0C423DC8D997542F5AE0166C8E_H
#ifndef FASTDTOABUILDER_T59B39461C1DC0951965DC8C43CD9A729911C67AE_H
#define FASTDTOABUILDER_T59B39461C1DC0951965DC8C43CD9A729911C67AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Number.Dtoa.FastDtoaBuilder
struct  FastDtoaBuilder_t59B39461C1DC0951965DC8C43CD9A729911C67AE  : public RuntimeObject
{
public:
	// System.Char[] Jint.Native.Number.Dtoa.FastDtoaBuilder::_chars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____chars_0;
	// System.Int32 Jint.Native.Number.Dtoa.FastDtoaBuilder::End
	int32_t ___End_1;
	// System.Int32 Jint.Native.Number.Dtoa.FastDtoaBuilder::Point
	int32_t ___Point_2;
	// System.Boolean Jint.Native.Number.Dtoa.FastDtoaBuilder::_formatted
	bool ____formatted_3;

public:
	inline static int32_t get_offset_of__chars_0() { return static_cast<int32_t>(offsetof(FastDtoaBuilder_t59B39461C1DC0951965DC8C43CD9A729911C67AE, ____chars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__chars_0() const { return ____chars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__chars_0() { return &____chars_0; }
	inline void set__chars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____chars_0 = value;
		Il2CppCodeGenWriteBarrier((&____chars_0), value);
	}

	inline static int32_t get_offset_of_End_1() { return static_cast<int32_t>(offsetof(FastDtoaBuilder_t59B39461C1DC0951965DC8C43CD9A729911C67AE, ___End_1)); }
	inline int32_t get_End_1() const { return ___End_1; }
	inline int32_t* get_address_of_End_1() { return &___End_1; }
	inline void set_End_1(int32_t value)
	{
		___End_1 = value;
	}

	inline static int32_t get_offset_of_Point_2() { return static_cast<int32_t>(offsetof(FastDtoaBuilder_t59B39461C1DC0951965DC8C43CD9A729911C67AE, ___Point_2)); }
	inline int32_t get_Point_2() const { return ___Point_2; }
	inline int32_t* get_address_of_Point_2() { return &___Point_2; }
	inline void set_Point_2(int32_t value)
	{
		___Point_2 = value;
	}

	inline static int32_t get_offset_of__formatted_3() { return static_cast<int32_t>(offsetof(FastDtoaBuilder_t59B39461C1DC0951965DC8C43CD9A729911C67AE, ____formatted_3)); }
	inline bool get__formatted_3() const { return ____formatted_3; }
	inline bool* get_address_of__formatted_3() { return &____formatted_3; }
	inline void set__formatted_3(bool value)
	{
		____formatted_3 = value;
	}
};

struct FastDtoaBuilder_t59B39461C1DC0951965DC8C43CD9A729911C67AE_StaticFields
{
public:
	// System.Char[] Jint.Native.Number.Dtoa.FastDtoaBuilder::Digits
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___Digits_4;

public:
	inline static int32_t get_offset_of_Digits_4() { return static_cast<int32_t>(offsetof(FastDtoaBuilder_t59B39461C1DC0951965DC8C43CD9A729911C67AE_StaticFields, ___Digits_4)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_Digits_4() const { return ___Digits_4; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_Digits_4() { return &___Digits_4; }
	inline void set_Digits_4(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___Digits_4 = value;
		Il2CppCodeGenWriteBarrier((&___Digits_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTDTOABUILDER_T59B39461C1DC0951965DC8C43CD9A729911C67AE_H
#ifndef NUMBEREXTENSIONS_TC6FC0C487A2F745894DFAED9F9213AF4A16E3B5F_H
#define NUMBEREXTENSIONS_TC6FC0C487A2F745894DFAED9F9213AF4A16E3B5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Number.Dtoa.NumberExtensions
struct  NumberExtensions_tC6FC0C487A2F745894DFAED9F9213AF4A16E3B5F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBEREXTENSIONS_TC6FC0C487A2F745894DFAED9F9213AF4A16E3B5F_H
#ifndef U3CU3EC_TC67FAF868A2F654B8A82885758BA467818CFA678_H
#define U3CU3EC_TC67FAF868A2F654B8A82885758BA467818CFA678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Object.ObjectConstructor/<>c
struct  U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678_StaticFields
{
public:
	// Jint.Native.Object.ObjectConstructor/<>c Jint.Native.Object.ObjectConstructor/<>c::<>9
	U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678 * ___U3CU3E9_0;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>,System.String> Jint.Native.Object.ObjectConstructor/<>c::<>9__17_0
	Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 * ___U3CU3E9__17_0_1;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>,System.String> Jint.Native.Object.ObjectConstructor/<>c::<>9__20_0
	Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 * ___U3CU3E9__20_0_2;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>,System.Boolean> Jint.Native.Object.ObjectConstructor/<>c::<>9__22_0
	Func_2_t24F919A9CB24363AA52DA57D86B524C1C6E932C0 * ___U3CU3E9__22_0_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678_StaticFields, ___U3CU3E9__17_0_1)); }
	inline Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 * get_U3CU3E9__17_0_1() const { return ___U3CU3E9__17_0_1; }
	inline Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 ** get_address_of_U3CU3E9__17_0_1() { return &___U3CU3E9__17_0_1; }
	inline void set_U3CU3E9__17_0_1(Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 * value)
	{
		___U3CU3E9__17_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__20_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678_StaticFields, ___U3CU3E9__20_0_2)); }
	inline Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 * get_U3CU3E9__20_0_2() const { return ___U3CU3E9__20_0_2; }
	inline Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 ** get_address_of_U3CU3E9__20_0_2() { return &___U3CU3E9__20_0_2; }
	inline void set_U3CU3E9__20_0_2(Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 * value)
	{
		___U3CU3E9__20_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__20_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678_StaticFields, ___U3CU3E9__22_0_3)); }
	inline Func_2_t24F919A9CB24363AA52DA57D86B524C1C6E932C0 * get_U3CU3E9__22_0_3() const { return ___U3CU3E9__22_0_3; }
	inline Func_2_t24F919A9CB24363AA52DA57D86B524C1C6E932C0 ** get_address_of_U3CU3E9__22_0_3() { return &___U3CU3E9__22_0_3; }
	inline void set_U3CU3E9__22_0_3(Func_2_t24F919A9CB24363AA52DA57D86B524C1C6E932C0 * value)
	{
		___U3CU3E9__22_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__22_0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TC67FAF868A2F654B8A82885758BA467818CFA678_H
#ifndef OBJECTINSTANCE_T9A33413FF3D113AEBA2D280D5498C04222E32900_H
#define OBJECTINSTANCE_T9A33413FF3D113AEBA2D280D5498C04222E32900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Object.ObjectInstance
struct  ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900  : public RuntimeObject
{
public:
	// Jint.Engine Jint.Native.Object.ObjectInstance::<Engine>k__BackingField
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ___U3CEngineU3Ek__BackingField_0;
	// System.Collections.Generic.IDictionary`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor> Jint.Native.Object.ObjectInstance::<Properties>k__BackingField
	RuntimeObject* ___U3CPropertiesU3Ek__BackingField_1;
	// Jint.Native.Object.ObjectInstance Jint.Native.Object.ObjectInstance::<Prototype>k__BackingField
	ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * ___U3CPrototypeU3Ek__BackingField_2;
	// System.Boolean Jint.Native.Object.ObjectInstance::<Extensible>k__BackingField
	bool ___U3CExtensibleU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CEngineU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900, ___U3CEngineU3Ek__BackingField_0)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get_U3CEngineU3Ek__BackingField_0() const { return ___U3CEngineU3Ek__BackingField_0; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of_U3CEngineU3Ek__BackingField_0() { return &___U3CEngineU3Ek__BackingField_0; }
	inline void set_U3CEngineU3Ek__BackingField_0(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		___U3CEngineU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEngineU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900, ___U3CPropertiesU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CPropertiesU3Ek__BackingField_1() const { return ___U3CPropertiesU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CPropertiesU3Ek__BackingField_1() { return &___U3CPropertiesU3Ek__BackingField_1; }
	inline void set_U3CPropertiesU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CPropertiesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertiesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CPrototypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900, ___U3CPrototypeU3Ek__BackingField_2)); }
	inline ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * get_U3CPrototypeU3Ek__BackingField_2() const { return ___U3CPrototypeU3Ek__BackingField_2; }
	inline ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 ** get_address_of_U3CPrototypeU3Ek__BackingField_2() { return &___U3CPrototypeU3Ek__BackingField_2; }
	inline void set_U3CPrototypeU3Ek__BackingField_2(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * value)
	{
		___U3CPrototypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrototypeU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CExtensibleU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900, ___U3CExtensibleU3Ek__BackingField_3)); }
	inline bool get_U3CExtensibleU3Ek__BackingField_3() const { return ___U3CExtensibleU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CExtensibleU3Ek__BackingField_3() { return &___U3CExtensibleU3Ek__BackingField_3; }
	inline void set_U3CExtensibleU3Ek__BackingField_3(bool value)
	{
		___U3CExtensibleU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTINSTANCE_T9A33413FF3D113AEBA2D280D5498C04222E32900_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_T4F3190029952FF4ABAE057EE3C3B704A38430D79_H
#define U3CU3EC__DISPLAYCLASS21_0_T4F3190029952FF4ABAE057EE3C3B704A38430D79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.String.StringPrototype/<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_t4F3190029952FF4ABAE057EE3C3B704A38430D79  : public RuntimeObject
{
public:
	// Jint.Native.JsValue Jint.Native.String.StringPrototype/<>c__DisplayClass21_0::replaceValue
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___replaceValue_0;
	// System.String Jint.Native.String.StringPrototype/<>c__DisplayClass21_0::thisString
	String_t* ___thisString_1;
	// Jint.Native.Function.FunctionInstance Jint.Native.String.StringPrototype/<>c__DisplayClass21_0::replaceFunction
	FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458 * ___replaceFunction_2;

public:
	inline static int32_t get_offset_of_replaceValue_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t4F3190029952FF4ABAE057EE3C3B704A38430D79, ___replaceValue_0)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_replaceValue_0() const { return ___replaceValue_0; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_replaceValue_0() { return &___replaceValue_0; }
	inline void set_replaceValue_0(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___replaceValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___replaceValue_0), value);
	}

	inline static int32_t get_offset_of_thisString_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t4F3190029952FF4ABAE057EE3C3B704A38430D79, ___thisString_1)); }
	inline String_t* get_thisString_1() const { return ___thisString_1; }
	inline String_t** get_address_of_thisString_1() { return &___thisString_1; }
	inline void set_thisString_1(String_t* value)
	{
		___thisString_1 = value;
		Il2CppCodeGenWriteBarrier((&___thisString_1), value);
	}

	inline static int32_t get_offset_of_replaceFunction_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t4F3190029952FF4ABAE057EE3C3B704A38430D79, ___replaceFunction_2)); }
	inline FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458 * get_replaceFunction_2() const { return ___replaceFunction_2; }
	inline FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458 ** get_address_of_replaceFunction_2() { return &___replaceFunction_2; }
	inline void set_replaceFunction_2(FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458 * value)
	{
		___replaceFunction_2 = value;
		Il2CppCodeGenWriteBarrier((&___replaceFunction_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_T4F3190029952FF4ABAE057EE3C3B704A38430D79_H
#ifndef UNDEFINED_T8BB3FFB7F5CAE6F9092ACE5257ABCC60AC3AD7C4_H
#define UNDEFINED_T8BB3FFB7F5CAE6F9092ACE5257ABCC60AC3AD7C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Undefined
struct  Undefined_t8BB3FFB7F5CAE6F9092ACE5257ABCC60AC3AD7C4  : public RuntimeObject
{
public:

public:
};

struct Undefined_t8BB3FFB7F5CAE6F9092ACE5257ABCC60AC3AD7C4_StaticFields
{
public:
	// Jint.Native.JsValue Jint.Native.Undefined::Instance
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___Instance_0;
	// System.String Jint.Native.Undefined::Text
	String_t* ___Text_1;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(Undefined_t8BB3FFB7F5CAE6F9092ACE5257ABCC60AC3AD7C4_StaticFields, ___Instance_0)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_Instance_0() const { return ___Instance_0; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}

	inline static int32_t get_offset_of_Text_1() { return static_cast<int32_t>(offsetof(Undefined_t8BB3FFB7F5CAE6F9092ACE5257ABCC60AC3AD7C4_StaticFields, ___Text_1)); }
	inline String_t* get_Text_1() const { return ___Text_1; }
	inline String_t** get_address_of_Text_1() { return &___Text_1; }
	inline void set_Text_1(String_t* value)
	{
		___Text_1 = value;
		Il2CppCodeGenWriteBarrier((&___Text_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNDEFINED_T8BB3FFB7F5CAE6F9092ACE5257ABCC60AC3AD7C4_H
#ifndef FILEMANAGER_TED5F6A99B61D40EAE6B612DDB9D6448AF26528AE_H
#define FILEMANAGER_TED5F6A99B61D40EAE6B612DDB9D6448AF26528AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.FileManager
struct  FileManager_tED5F6A99B61D40EAE6B612DDB9D6448AF26528AE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEMANAGER_TED5F6A99B61D40EAE6B612DDB9D6448AF26528AE_H
#ifndef LAUNCHERMANAGER_TA5EA158E3347DBF833C1A015EFBAE39A10537D61_H
#define LAUNCHERMANAGER_TA5EA158E3347DBF833C1A015EFBAE39A10537D61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.LauncherManager
struct  LauncherManager_tA5EA158E3347DBF833C1A015EFBAE39A10537D61  : public RuntimeObject
{
public:
	// MHLab.PATCH.PatchApplier MHLab.PATCH.LauncherManager::m_patchApplier
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938 * ___m_patchApplier_0;
	// MHLab.PATCH.Settings.SettingsOverrider MHLab.PATCH.LauncherManager::SETTINGS
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C * ___SETTINGS_1;

public:
	inline static int32_t get_offset_of_m_patchApplier_0() { return static_cast<int32_t>(offsetof(LauncherManager_tA5EA158E3347DBF833C1A015EFBAE39A10537D61, ___m_patchApplier_0)); }
	inline PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938 * get_m_patchApplier_0() const { return ___m_patchApplier_0; }
	inline PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938 ** get_address_of_m_patchApplier_0() { return &___m_patchApplier_0; }
	inline void set_m_patchApplier_0(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938 * value)
	{
		___m_patchApplier_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_patchApplier_0), value);
	}

	inline static int32_t get_offset_of_SETTINGS_1() { return static_cast<int32_t>(offsetof(LauncherManager_tA5EA158E3347DBF833C1A015EFBAE39A10537D61, ___SETTINGS_1)); }
	inline SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C * get_SETTINGS_1() const { return ___SETTINGS_1; }
	inline SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C ** get_address_of_SETTINGS_1() { return &___SETTINGS_1; }
	inline void set_SETTINGS_1(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C * value)
	{
		___SETTINGS_1 = value;
		Il2CppCodeGenWriteBarrier((&___SETTINGS_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAUNCHERMANAGER_TA5EA158E3347DBF833C1A015EFBAE39A10537D61_H
#ifndef PATCHAPPLIER_TF0FD45BF12E56EF771624EC00E3AB69839E51938_H
#define PATCHAPPLIER_TF0FD45BF12E56EF771624EC00E3AB69839E51938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.PatchApplier
struct  PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<MHLab.PATCH.Patch> MHLab.PATCH.PatchApplier::_patches
	List_1_tE24C1D4E7B1731EAAFBDFF55DEAEF3A76E38C5CE * ____patches_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> MHLab.PATCH.PatchApplier::_downloadedArchiveFilesHashes
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ____downloadedArchiveFilesHashes_1;
	// System.Action`1<System.String> MHLab.PATCH.PatchApplier::OnFileProcessed
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___OnFileProcessed_2;
	// System.Action`1<System.String> MHLab.PATCH.PatchApplier::OnFileProcessing
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___OnFileProcessing_3;
	// System.Action`2<System.String,System.String> MHLab.PATCH.PatchApplier::OnLog
	Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * ___OnLog_4;
	// System.Action`3<System.String,System.String,System.Exception> MHLab.PATCH.PatchApplier::OnError
	Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * ___OnError_5;
	// System.Action`3<System.String,System.String,System.Exception> MHLab.PATCH.PatchApplier::OnFatalError
	Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * ___OnFatalError_6;
	// System.Action`1<System.String> MHLab.PATCH.PatchApplier::OnTaskStarted
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___OnTaskStarted_7;
	// System.Action`1<System.String> MHLab.PATCH.PatchApplier::OnTaskCompleted
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___OnTaskCompleted_8;
	// System.Action`2<System.Int32,System.Int32> MHLab.PATCH.PatchApplier::OnSetMainProgressBar
	Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * ___OnSetMainProgressBar_9;
	// System.Action`2<System.Int32,System.Int32> MHLab.PATCH.PatchApplier::OnSetDetailProgressBar
	Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * ___OnSetDetailProgressBar_10;
	// System.Action MHLab.PATCH.PatchApplier::OnIncreaseMainProgressBar
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnIncreaseMainProgressBar_11;
	// System.Action MHLab.PATCH.PatchApplier::OnIncreaseDetailProgressBar
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnIncreaseDetailProgressBar_12;
	// System.Action`3<System.Int64,System.Int64,System.Int32> MHLab.PATCH.PatchApplier::OnDownloadProgress
	Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 * ___OnDownloadProgress_13;
	// System.Action MHLab.PATCH.PatchApplier::OnDownloadCompleted
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnDownloadCompleted_14;
	// System.Boolean MHLab.PATCH.PatchApplier::IsDirtyWorkspace
	bool ___IsDirtyWorkspace_15;

public:
	inline static int32_t get_offset_of__patches_0() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ____patches_0)); }
	inline List_1_tE24C1D4E7B1731EAAFBDFF55DEAEF3A76E38C5CE * get__patches_0() const { return ____patches_0; }
	inline List_1_tE24C1D4E7B1731EAAFBDFF55DEAEF3A76E38C5CE ** get_address_of__patches_0() { return &____patches_0; }
	inline void set__patches_0(List_1_tE24C1D4E7B1731EAAFBDFF55DEAEF3A76E38C5CE * value)
	{
		____patches_0 = value;
		Il2CppCodeGenWriteBarrier((&____patches_0), value);
	}

	inline static int32_t get_offset_of__downloadedArchiveFilesHashes_1() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ____downloadedArchiveFilesHashes_1)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get__downloadedArchiveFilesHashes_1() const { return ____downloadedArchiveFilesHashes_1; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of__downloadedArchiveFilesHashes_1() { return &____downloadedArchiveFilesHashes_1; }
	inline void set__downloadedArchiveFilesHashes_1(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		____downloadedArchiveFilesHashes_1 = value;
		Il2CppCodeGenWriteBarrier((&____downloadedArchiveFilesHashes_1), value);
	}

	inline static int32_t get_offset_of_OnFileProcessed_2() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ___OnFileProcessed_2)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_OnFileProcessed_2() const { return ___OnFileProcessed_2; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_OnFileProcessed_2() { return &___OnFileProcessed_2; }
	inline void set_OnFileProcessed_2(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___OnFileProcessed_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnFileProcessed_2), value);
	}

	inline static int32_t get_offset_of_OnFileProcessing_3() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ___OnFileProcessing_3)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_OnFileProcessing_3() const { return ___OnFileProcessing_3; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_OnFileProcessing_3() { return &___OnFileProcessing_3; }
	inline void set_OnFileProcessing_3(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___OnFileProcessing_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnFileProcessing_3), value);
	}

	inline static int32_t get_offset_of_OnLog_4() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ___OnLog_4)); }
	inline Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * get_OnLog_4() const { return ___OnLog_4; }
	inline Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 ** get_address_of_OnLog_4() { return &___OnLog_4; }
	inline void set_OnLog_4(Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * value)
	{
		___OnLog_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnLog_4), value);
	}

	inline static int32_t get_offset_of_OnError_5() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ___OnError_5)); }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * get_OnError_5() const { return ___OnError_5; }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A ** get_address_of_OnError_5() { return &___OnError_5; }
	inline void set_OnError_5(Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * value)
	{
		___OnError_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnError_5), value);
	}

	inline static int32_t get_offset_of_OnFatalError_6() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ___OnFatalError_6)); }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * get_OnFatalError_6() const { return ___OnFatalError_6; }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A ** get_address_of_OnFatalError_6() { return &___OnFatalError_6; }
	inline void set_OnFatalError_6(Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * value)
	{
		___OnFatalError_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnFatalError_6), value);
	}

	inline static int32_t get_offset_of_OnTaskStarted_7() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ___OnTaskStarted_7)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_OnTaskStarted_7() const { return ___OnTaskStarted_7; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_OnTaskStarted_7() { return &___OnTaskStarted_7; }
	inline void set_OnTaskStarted_7(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___OnTaskStarted_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnTaskStarted_7), value);
	}

	inline static int32_t get_offset_of_OnTaskCompleted_8() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ___OnTaskCompleted_8)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_OnTaskCompleted_8() const { return ___OnTaskCompleted_8; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_OnTaskCompleted_8() { return &___OnTaskCompleted_8; }
	inline void set_OnTaskCompleted_8(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___OnTaskCompleted_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnTaskCompleted_8), value);
	}

	inline static int32_t get_offset_of_OnSetMainProgressBar_9() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ___OnSetMainProgressBar_9)); }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * get_OnSetMainProgressBar_9() const { return ___OnSetMainProgressBar_9; }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE ** get_address_of_OnSetMainProgressBar_9() { return &___OnSetMainProgressBar_9; }
	inline void set_OnSetMainProgressBar_9(Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * value)
	{
		___OnSetMainProgressBar_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnSetMainProgressBar_9), value);
	}

	inline static int32_t get_offset_of_OnSetDetailProgressBar_10() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ___OnSetDetailProgressBar_10)); }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * get_OnSetDetailProgressBar_10() const { return ___OnSetDetailProgressBar_10; }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE ** get_address_of_OnSetDetailProgressBar_10() { return &___OnSetDetailProgressBar_10; }
	inline void set_OnSetDetailProgressBar_10(Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * value)
	{
		___OnSetDetailProgressBar_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnSetDetailProgressBar_10), value);
	}

	inline static int32_t get_offset_of_OnIncreaseMainProgressBar_11() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ___OnIncreaseMainProgressBar_11)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnIncreaseMainProgressBar_11() const { return ___OnIncreaseMainProgressBar_11; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnIncreaseMainProgressBar_11() { return &___OnIncreaseMainProgressBar_11; }
	inline void set_OnIncreaseMainProgressBar_11(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnIncreaseMainProgressBar_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnIncreaseMainProgressBar_11), value);
	}

	inline static int32_t get_offset_of_OnIncreaseDetailProgressBar_12() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ___OnIncreaseDetailProgressBar_12)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnIncreaseDetailProgressBar_12() const { return ___OnIncreaseDetailProgressBar_12; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnIncreaseDetailProgressBar_12() { return &___OnIncreaseDetailProgressBar_12; }
	inline void set_OnIncreaseDetailProgressBar_12(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnIncreaseDetailProgressBar_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnIncreaseDetailProgressBar_12), value);
	}

	inline static int32_t get_offset_of_OnDownloadProgress_13() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ___OnDownloadProgress_13)); }
	inline Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 * get_OnDownloadProgress_13() const { return ___OnDownloadProgress_13; }
	inline Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 ** get_address_of_OnDownloadProgress_13() { return &___OnDownloadProgress_13; }
	inline void set_OnDownloadProgress_13(Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 * value)
	{
		___OnDownloadProgress_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownloadProgress_13), value);
	}

	inline static int32_t get_offset_of_OnDownloadCompleted_14() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ___OnDownloadCompleted_14)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnDownloadCompleted_14() const { return ___OnDownloadCompleted_14; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnDownloadCompleted_14() { return &___OnDownloadCompleted_14; }
	inline void set_OnDownloadCompleted_14(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnDownloadCompleted_14 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownloadCompleted_14), value);
	}

	inline static int32_t get_offset_of_IsDirtyWorkspace_15() { return static_cast<int32_t>(offsetof(PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938, ___IsDirtyWorkspace_15)); }
	inline bool get_IsDirtyWorkspace_15() const { return ___IsDirtyWorkspace_15; }
	inline bool* get_address_of_IsDirtyWorkspace_15() { return &___IsDirtyWorkspace_15; }
	inline void set_IsDirtyWorkspace_15(bool value)
	{
		___IsDirtyWorkspace_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATCHAPPLIER_TF0FD45BF12E56EF771624EC00E3AB69839E51938_H
#ifndef U3CU3EC_T90691F5CC2F74205A916FA45AC0D2DADB4019436_H
#define U3CU3EC_T90691F5CC2F74205A916FA45AC0D2DADB4019436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.PatchApplier/<>c
struct  U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields
{
public:
	// MHLab.PATCH.PatchApplier/<>c MHLab.PATCH.PatchApplier/<>c::<>9
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436 * ___U3CU3E9_0;
	// System.Action`1<System.String> MHLab.PATCH.PatchApplier/<>c::<>9__16_0
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___U3CU3E9__16_0_1;
	// System.Action`1<System.String> MHLab.PATCH.PatchApplier/<>c::<>9__16_1
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___U3CU3E9__16_1_2;
	// System.Action`2<System.String,System.String> MHLab.PATCH.PatchApplier/<>c::<>9__16_2
	Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * ___U3CU3E9__16_2_3;
	// System.Action`3<System.String,System.String,System.Exception> MHLab.PATCH.PatchApplier/<>c::<>9__16_3
	Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * ___U3CU3E9__16_3_4;
	// System.Action`3<System.String,System.String,System.Exception> MHLab.PATCH.PatchApplier/<>c::<>9__16_4
	Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * ___U3CU3E9__16_4_5;
	// System.Action`1<System.String> MHLab.PATCH.PatchApplier/<>c::<>9__16_5
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___U3CU3E9__16_5_6;
	// System.Action`1<System.String> MHLab.PATCH.PatchApplier/<>c::<>9__16_6
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___U3CU3E9__16_6_7;
	// System.Action`2<System.Int32,System.Int32> MHLab.PATCH.PatchApplier/<>c::<>9__16_7
	Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * ___U3CU3E9__16_7_8;
	// System.Action`2<System.Int32,System.Int32> MHLab.PATCH.PatchApplier/<>c::<>9__16_8
	Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * ___U3CU3E9__16_8_9;
	// System.Action MHLab.PATCH.PatchApplier/<>c::<>9__16_9
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__16_9_10;
	// System.Action MHLab.PATCH.PatchApplier/<>c::<>9__16_10
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__16_10_11;
	// System.Action`3<System.Int64,System.Int64,System.Int32> MHLab.PATCH.PatchApplier/<>c::<>9__16_11
	Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 * ___U3CU3E9__16_11_12;
	// System.Action MHLab.PATCH.PatchApplier/<>c::<>9__16_12
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__16_12_13;
	// System.Comparison`1<MHLab.PATCH.Patch> MHLab.PATCH.PatchApplier/<>c::<>9__17_1
	Comparison_1_tB495CD0B15D943013A6EEF2B915E351640B62754 * ___U3CU3E9__17_1_14;
	// System.Comparison`1<MHLab.PATCH.Patch> MHLab.PATCH.PatchApplier/<>c::<>9__17_3
	Comparison_1_tB495CD0B15D943013A6EEF2B915E351640B62754 * ___U3CU3E9__17_3_15;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9__16_0_1)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_U3CU3E9__16_0_1() const { return ___U3CU3E9__16_0_1; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_U3CU3E9__16_0_1() { return &___U3CU3E9__16_0_1; }
	inline void set_U3CU3E9__16_0_1(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___U3CU3E9__16_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9__16_1_2)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_U3CU3E9__16_1_2() const { return ___U3CU3E9__16_1_2; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_U3CU3E9__16_1_2() { return &___U3CU3E9__16_1_2; }
	inline void set_U3CU3E9__16_1_2(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___U3CU3E9__16_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9__16_2_3)); }
	inline Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * get_U3CU3E9__16_2_3() const { return ___U3CU3E9__16_2_3; }
	inline Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 ** get_address_of_U3CU3E9__16_2_3() { return &___U3CU3E9__16_2_3; }
	inline void set_U3CU3E9__16_2_3(Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * value)
	{
		___U3CU3E9__16_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9__16_3_4)); }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * get_U3CU3E9__16_3_4() const { return ___U3CU3E9__16_3_4; }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A ** get_address_of_U3CU3E9__16_3_4() { return &___U3CU3E9__16_3_4; }
	inline void set_U3CU3E9__16_3_4(Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * value)
	{
		___U3CU3E9__16_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_3_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_4_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9__16_4_5)); }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * get_U3CU3E9__16_4_5() const { return ___U3CU3E9__16_4_5; }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A ** get_address_of_U3CU3E9__16_4_5() { return &___U3CU3E9__16_4_5; }
	inline void set_U3CU3E9__16_4_5(Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * value)
	{
		___U3CU3E9__16_4_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_4_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_5_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9__16_5_6)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_U3CU3E9__16_5_6() const { return ___U3CU3E9__16_5_6; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_U3CU3E9__16_5_6() { return &___U3CU3E9__16_5_6; }
	inline void set_U3CU3E9__16_5_6(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___U3CU3E9__16_5_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_5_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_6_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9__16_6_7)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_U3CU3E9__16_6_7() const { return ___U3CU3E9__16_6_7; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_U3CU3E9__16_6_7() { return &___U3CU3E9__16_6_7; }
	inline void set_U3CU3E9__16_6_7(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___U3CU3E9__16_6_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_6_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_7_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9__16_7_8)); }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * get_U3CU3E9__16_7_8() const { return ___U3CU3E9__16_7_8; }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE ** get_address_of_U3CU3E9__16_7_8() { return &___U3CU3E9__16_7_8; }
	inline void set_U3CU3E9__16_7_8(Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * value)
	{
		___U3CU3E9__16_7_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_7_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_8_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9__16_8_9)); }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * get_U3CU3E9__16_8_9() const { return ___U3CU3E9__16_8_9; }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE ** get_address_of_U3CU3E9__16_8_9() { return &___U3CU3E9__16_8_9; }
	inline void set_U3CU3E9__16_8_9(Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * value)
	{
		___U3CU3E9__16_8_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_8_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_9_10() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9__16_9_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__16_9_10() const { return ___U3CU3E9__16_9_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__16_9_10() { return &___U3CU3E9__16_9_10; }
	inline void set_U3CU3E9__16_9_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__16_9_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_9_10), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_10_11() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9__16_10_11)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__16_10_11() const { return ___U3CU3E9__16_10_11; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__16_10_11() { return &___U3CU3E9__16_10_11; }
	inline void set_U3CU3E9__16_10_11(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__16_10_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_10_11), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_11_12() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9__16_11_12)); }
	inline Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 * get_U3CU3E9__16_11_12() const { return ___U3CU3E9__16_11_12; }
	inline Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 ** get_address_of_U3CU3E9__16_11_12() { return &___U3CU3E9__16_11_12; }
	inline void set_U3CU3E9__16_11_12(Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 * value)
	{
		___U3CU3E9__16_11_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_11_12), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_12_13() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9__16_12_13)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__16_12_13() const { return ___U3CU3E9__16_12_13; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__16_12_13() { return &___U3CU3E9__16_12_13; }
	inline void set_U3CU3E9__16_12_13(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__16_12_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_12_13), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_1_14() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9__17_1_14)); }
	inline Comparison_1_tB495CD0B15D943013A6EEF2B915E351640B62754 * get_U3CU3E9__17_1_14() const { return ___U3CU3E9__17_1_14; }
	inline Comparison_1_tB495CD0B15D943013A6EEF2B915E351640B62754 ** get_address_of_U3CU3E9__17_1_14() { return &___U3CU3E9__17_1_14; }
	inline void set_U3CU3E9__17_1_14(Comparison_1_tB495CD0B15D943013A6EEF2B915E351640B62754 * value)
	{
		___U3CU3E9__17_1_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_1_14), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_3_15() { return static_cast<int32_t>(offsetof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields, ___U3CU3E9__17_3_15)); }
	inline Comparison_1_tB495CD0B15D943013A6EEF2B915E351640B62754 * get_U3CU3E9__17_3_15() const { return ___U3CU3E9__17_3_15; }
	inline Comparison_1_tB495CD0B15D943013A6EEF2B915E351640B62754 ** get_address_of_U3CU3E9__17_3_15() { return &___U3CU3E9__17_3_15; }
	inline void set_U3CU3E9__17_3_15(Comparison_1_tB495CD0B15D943013A6EEF2B915E351640B62754 * value)
	{
		___U3CU3E9__17_3_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_3_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T90691F5CC2F74205A916FA45AC0D2DADB4019436_H
#ifndef U3CU3EC__DISPLAYCLASS17_0_T5BB4A897CD7172CD72926C9BC88AC164F62CEB04_H
#define U3CU3EC__DISPLAYCLASS17_0_T5BB4A897CD7172CD72926C9BC88AC164F62CEB04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.PatchApplier/<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_t5BB4A897CD7172CD72926C9BC88AC164F62CEB04  : public RuntimeObject
{
public:
	// MHLab.PATCH.Version MHLab.PATCH.PatchApplier/<>c__DisplayClass17_0::currentVersion
	Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976 * ___currentVersion_0;
	// System.Func`2<MHLab.PATCH.Patch,System.Boolean> MHLab.PATCH.PatchApplier/<>c__DisplayClass17_0::<>9__2
	Func_2_tB31DDBA848EDD775F31274A5149C07F6C19DB2A6 * ___U3CU3E9__2_1;

public:
	inline static int32_t get_offset_of_currentVersion_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t5BB4A897CD7172CD72926C9BC88AC164F62CEB04, ___currentVersion_0)); }
	inline Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976 * get_currentVersion_0() const { return ___currentVersion_0; }
	inline Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976 ** get_address_of_currentVersion_0() { return &___currentVersion_0; }
	inline void set_currentVersion_0(Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976 * value)
	{
		___currentVersion_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentVersion_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t5BB4A897CD7172CD72926C9BC88AC164F62CEB04, ___U3CU3E9__2_1)); }
	inline Func_2_tB31DDBA848EDD775F31274A5149C07F6C19DB2A6 * get_U3CU3E9__2_1() const { return ___U3CU3E9__2_1; }
	inline Func_2_tB31DDBA848EDD775F31274A5149C07F6C19DB2A6 ** get_address_of_U3CU3E9__2_1() { return &___U3CU3E9__2_1; }
	inline void set_U3CU3E9__2_1(Func_2_tB31DDBA848EDD775F31274A5149C07F6C19DB2A6 * value)
	{
		___U3CU3E9__2_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_0_T5BB4A897CD7172CD72926C9BC88AC164F62CEB04_H
#ifndef BINARYCOMPARER_T978B3C973609E9A7EE9830B13EFA66D6EF063983_H
#define BINARYCOMPARER_T978B3C973609E9A7EE9830B13EFA66D6EF063983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Utilities.BinaryComparer
struct  BinaryComparer_t978B3C973609E9A7EE9830B13EFA66D6EF063983  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYCOMPARER_T978B3C973609E9A7EE9830B13EFA66D6EF063983_H
#ifndef HASHING_TD9A6183B77B7B71494AEFD6BA8E8BE6BBDB55FED_H
#define HASHING_TD9A6183B77B7B71494AEFD6BA8E8BE6BBDB55FED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Utilities.Hashing
struct  Hashing_tD9A6183B77B7B71494AEFD6BA8E8BE6BBDB55FED  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHING_TD9A6183B77B7B71494AEFD6BA8E8BE6BBDB55FED_H
#ifndef RIJNDAEL_T3628B330F435769E367ED28C39FDB2D5601C1392_H
#define RIJNDAEL_T3628B330F435769E367ED28C39FDB2D5601C1392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Utilities.Rijndael
struct  Rijndael_t3628B330F435769E367ED28C39FDB2D5601C1392  : public RuntimeObject
{
public:

public:
};

struct Rijndael_t3628B330F435769E367ED28C39FDB2D5601C1392_StaticFields
{
public:
	// System.Byte[] MHLab.PATCH.Utilities.Rijndael::initVectorBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___initVectorBytes_0;

public:
	inline static int32_t get_offset_of_initVectorBytes_0() { return static_cast<int32_t>(offsetof(Rijndael_t3628B330F435769E367ED28C39FDB2D5601C1392_StaticFields, ___initVectorBytes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_initVectorBytes_0() const { return ___initVectorBytes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_initVectorBytes_0() { return &___initVectorBytes_0; }
	inline void set_initVectorBytes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___initVectorBytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___initVectorBytes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIJNDAEL_T3628B330F435769E367ED28C39FDB2D5601C1392_H
#ifndef UTILITY_T6507726DA6EAD3D639EF1B6D4268D0AF79CD83E6_H
#define UTILITY_T6507726DA6EAD3D639EF1B6D4268D0AF79CD83E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Utilities.Utility
struct  Utility_t6507726DA6EAD3D639EF1B6D4268D0AF79CD83E6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITY_T6507726DA6EAD3D639EF1B6D4268D0AF79CD83E6_H
#ifndef VERSION_T9E4808E22CBFFB54540FB10B7CA88195A0F75976_H
#define VERSION_T9E4808E22CBFFB54540FB10B7CA88195A0F75976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Version
struct  Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976  : public RuntimeObject
{
public:
	// System.Int32 MHLab.PATCH.Version::m_majorReleaseNumber
	int32_t ___m_majorReleaseNumber_0;
	// System.Int32 MHLab.PATCH.Version::m_minorReleaseNumber
	int32_t ___m_minorReleaseNumber_1;
	// System.Int32 MHLab.PATCH.Version::m_maintenanceReleaseNumber
	int32_t ___m_maintenanceReleaseNumber_2;
	// System.Int32 MHLab.PATCH.Version::m_buildNumber
	int32_t ___m_buildNumber_3;

public:
	inline static int32_t get_offset_of_m_majorReleaseNumber_0() { return static_cast<int32_t>(offsetof(Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976, ___m_majorReleaseNumber_0)); }
	inline int32_t get_m_majorReleaseNumber_0() const { return ___m_majorReleaseNumber_0; }
	inline int32_t* get_address_of_m_majorReleaseNumber_0() { return &___m_majorReleaseNumber_0; }
	inline void set_m_majorReleaseNumber_0(int32_t value)
	{
		___m_majorReleaseNumber_0 = value;
	}

	inline static int32_t get_offset_of_m_minorReleaseNumber_1() { return static_cast<int32_t>(offsetof(Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976, ___m_minorReleaseNumber_1)); }
	inline int32_t get_m_minorReleaseNumber_1() const { return ___m_minorReleaseNumber_1; }
	inline int32_t* get_address_of_m_minorReleaseNumber_1() { return &___m_minorReleaseNumber_1; }
	inline void set_m_minorReleaseNumber_1(int32_t value)
	{
		___m_minorReleaseNumber_1 = value;
	}

	inline static int32_t get_offset_of_m_maintenanceReleaseNumber_2() { return static_cast<int32_t>(offsetof(Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976, ___m_maintenanceReleaseNumber_2)); }
	inline int32_t get_m_maintenanceReleaseNumber_2() const { return ___m_maintenanceReleaseNumber_2; }
	inline int32_t* get_address_of_m_maintenanceReleaseNumber_2() { return &___m_maintenanceReleaseNumber_2; }
	inline void set_m_maintenanceReleaseNumber_2(int32_t value)
	{
		___m_maintenanceReleaseNumber_2 = value;
	}

	inline static int32_t get_offset_of_m_buildNumber_3() { return static_cast<int32_t>(offsetof(Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976, ___m_buildNumber_3)); }
	inline int32_t get_m_buildNumber_3() const { return ___m_buildNumber_3; }
	inline int32_t* get_address_of_m_buildNumber_3() { return &___m_buildNumber_3; }
	inline void set_m_buildNumber_3(int32_t value)
	{
		___m_buildNumber_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSION_T9E4808E22CBFFB54540FB10B7CA88195A0F75976_H
#ifndef U3CU3EC_T55DA89562498EBEF40EB707B902EAE64CFF65851_H
#define U3CU3EC_T55DA89562498EBEF40EB707B902EAE64CFF65851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Version/<>c
struct  U3CU3Ec_t55DA89562498EBEF40EB707B902EAE64CFF65851  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t55DA89562498EBEF40EB707B902EAE64CFF65851_StaticFields
{
public:
	// MHLab.PATCH.Version/<>c MHLab.PATCH.Version/<>c::<>9
	U3CU3Ec_t55DA89562498EBEF40EB707B902EAE64CFF65851 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.String> MHLab.PATCH.Version/<>c::<>9__15_0
	Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * ___U3CU3E9__15_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t55DA89562498EBEF40EB707B902EAE64CFF65851_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t55DA89562498EBEF40EB707B902EAE64CFF65851 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t55DA89562498EBEF40EB707B902EAE64CFF65851 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t55DA89562498EBEF40EB707B902EAE64CFF65851 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t55DA89562498EBEF40EB707B902EAE64CFF65851_StaticFields, ___U3CU3E9__15_0_1)); }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * get_U3CU3E9__15_0_1() const { return ___U3CU3E9__15_0_1; }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 ** get_address_of_U3CU3E9__15_0_1() { return &___U3CU3E9__15_0_1; }
	inline void set_U3CU3E9__15_0_1(Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * value)
	{
		___U3CU3E9__15_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__15_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T55DA89562498EBEF40EB707B902EAE64CFF65851_H
#ifndef BINARYDELTAREADER_T6D4B02E88361BD2601A687680DAECF9C7356FC44_H
#define BINARYDELTAREADER_T6D4B02E88361BD2601A687680DAECF9C7356FC44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Octodiff.Core.BinaryDeltaReader
struct  BinaryDeltaReader_t6D4B02E88361BD2601A687680DAECF9C7356FC44  : public RuntimeObject
{
public:
	// System.IO.BinaryReader Octodiff.Core.BinaryDeltaReader::reader
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * ___reader_0;
	// Octodiff.Diagnostics.IProgressReporter Octodiff.Core.BinaryDeltaReader::progressReporter
	RuntimeObject* ___progressReporter_1;
	// System.Byte[] Octodiff.Core.BinaryDeltaReader::expectedHash
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___expectedHash_2;
	// Octodiff.Core.IHashAlgorithm Octodiff.Core.BinaryDeltaReader::hashAlgorithm
	RuntimeObject* ___hashAlgorithm_3;
	// System.Boolean Octodiff.Core.BinaryDeltaReader::hasReadMetadata
	bool ___hasReadMetadata_4;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(BinaryDeltaReader_t6D4B02E88361BD2601A687680DAECF9C7356FC44, ___reader_0)); }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * get_reader_0() const { return ___reader_0; }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier((&___reader_0), value);
	}

	inline static int32_t get_offset_of_progressReporter_1() { return static_cast<int32_t>(offsetof(BinaryDeltaReader_t6D4B02E88361BD2601A687680DAECF9C7356FC44, ___progressReporter_1)); }
	inline RuntimeObject* get_progressReporter_1() const { return ___progressReporter_1; }
	inline RuntimeObject** get_address_of_progressReporter_1() { return &___progressReporter_1; }
	inline void set_progressReporter_1(RuntimeObject* value)
	{
		___progressReporter_1 = value;
		Il2CppCodeGenWriteBarrier((&___progressReporter_1), value);
	}

	inline static int32_t get_offset_of_expectedHash_2() { return static_cast<int32_t>(offsetof(BinaryDeltaReader_t6D4B02E88361BD2601A687680DAECF9C7356FC44, ___expectedHash_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_expectedHash_2() const { return ___expectedHash_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_expectedHash_2() { return &___expectedHash_2; }
	inline void set_expectedHash_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___expectedHash_2 = value;
		Il2CppCodeGenWriteBarrier((&___expectedHash_2), value);
	}

	inline static int32_t get_offset_of_hashAlgorithm_3() { return static_cast<int32_t>(offsetof(BinaryDeltaReader_t6D4B02E88361BD2601A687680DAECF9C7356FC44, ___hashAlgorithm_3)); }
	inline RuntimeObject* get_hashAlgorithm_3() const { return ___hashAlgorithm_3; }
	inline RuntimeObject** get_address_of_hashAlgorithm_3() { return &___hashAlgorithm_3; }
	inline void set_hashAlgorithm_3(RuntimeObject* value)
	{
		___hashAlgorithm_3 = value;
		Il2CppCodeGenWriteBarrier((&___hashAlgorithm_3), value);
	}

	inline static int32_t get_offset_of_hasReadMetadata_4() { return static_cast<int32_t>(offsetof(BinaryDeltaReader_t6D4B02E88361BD2601A687680DAECF9C7356FC44, ___hasReadMetadata_4)); }
	inline bool get_hasReadMetadata_4() const { return ___hasReadMetadata_4; }
	inline bool* get_address_of_hasReadMetadata_4() { return &___hasReadMetadata_4; }
	inline void set_hasReadMetadata_4(bool value)
	{
		___hasReadMetadata_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYDELTAREADER_T6D4B02E88361BD2601A687680DAECF9C7356FC44_H
#ifndef BINARYFORMAT_T54D3BBC8CC0E2537B2F3D81427AA08FEDE8ED12F_H
#define BINARYFORMAT_T54D3BBC8CC0E2537B2F3D81427AA08FEDE8ED12F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Octodiff.Core.BinaryFormat
struct  BinaryFormat_t54D3BBC8CC0E2537B2F3D81427AA08FEDE8ED12F  : public RuntimeObject
{
public:

public:
};

struct BinaryFormat_t54D3BBC8CC0E2537B2F3D81427AA08FEDE8ED12F_StaticFields
{
public:
	// System.Byte[] Octodiff.Core.BinaryFormat::SignatureHeader
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___SignatureHeader_0;
	// System.Byte[] Octodiff.Core.BinaryFormat::DeltaHeader
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___DeltaHeader_1;
	// System.Byte[] Octodiff.Core.BinaryFormat::EndOfMetadata
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EndOfMetadata_2;

public:
	inline static int32_t get_offset_of_SignatureHeader_0() { return static_cast<int32_t>(offsetof(BinaryFormat_t54D3BBC8CC0E2537B2F3D81427AA08FEDE8ED12F_StaticFields, ___SignatureHeader_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_SignatureHeader_0() const { return ___SignatureHeader_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_SignatureHeader_0() { return &___SignatureHeader_0; }
	inline void set_SignatureHeader_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___SignatureHeader_0 = value;
		Il2CppCodeGenWriteBarrier((&___SignatureHeader_0), value);
	}

	inline static int32_t get_offset_of_DeltaHeader_1() { return static_cast<int32_t>(offsetof(BinaryFormat_t54D3BBC8CC0E2537B2F3D81427AA08FEDE8ED12F_StaticFields, ___DeltaHeader_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_DeltaHeader_1() const { return ___DeltaHeader_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_DeltaHeader_1() { return &___DeltaHeader_1; }
	inline void set_DeltaHeader_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___DeltaHeader_1 = value;
		Il2CppCodeGenWriteBarrier((&___DeltaHeader_1), value);
	}

	inline static int32_t get_offset_of_EndOfMetadata_2() { return static_cast<int32_t>(offsetof(BinaryFormat_t54D3BBC8CC0E2537B2F3D81427AA08FEDE8ED12F_StaticFields, ___EndOfMetadata_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EndOfMetadata_2() const { return ___EndOfMetadata_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EndOfMetadata_2() { return &___EndOfMetadata_2; }
	inline void set_EndOfMetadata_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EndOfMetadata_2 = value;
		Il2CppCodeGenWriteBarrier((&___EndOfMetadata_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYFORMAT_T54D3BBC8CC0E2537B2F3D81427AA08FEDE8ED12F_H
#ifndef DELTAAPPLIER_TEEA63F189630738E3ED55578959BEA9680176335_H
#define DELTAAPPLIER_TEEA63F189630738E3ED55578959BEA9680176335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Octodiff.Core.DeltaApplier
struct  DeltaApplier_tEEA63F189630738E3ED55578959BEA9680176335  : public RuntimeObject
{
public:
	// System.Boolean Octodiff.Core.DeltaApplier::<SkipHashCheck>k__BackingField
	bool ___U3CSkipHashCheckU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CSkipHashCheckU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DeltaApplier_tEEA63F189630738E3ED55578959BEA9680176335, ___U3CSkipHashCheckU3Ek__BackingField_0)); }
	inline bool get_U3CSkipHashCheckU3Ek__BackingField_0() const { return ___U3CSkipHashCheckU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CSkipHashCheckU3Ek__BackingField_0() { return &___U3CSkipHashCheckU3Ek__BackingField_0; }
	inline void set_U3CSkipHashCheckU3Ek__BackingField_0(bool value)
	{
		___U3CSkipHashCheckU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELTAAPPLIER_TEEA63F189630738E3ED55578959BEA9680176335_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T29B5ACD9AA13C46D839642A2CD7A4078957AE95E_H
#define U3CU3EC__DISPLAYCLASS5_0_T29B5ACD9AA13C46D839642A2CD7A4078957AE95E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Octodiff.Core.DeltaApplier/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t29B5ACD9AA13C46D839642A2CD7A4078957AE95E  : public RuntimeObject
{
public:
	// System.IO.Stream Octodiff.Core.DeltaApplier/<>c__DisplayClass5_0::outputStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___outputStream_0;
	// System.IO.Stream Octodiff.Core.DeltaApplier/<>c__DisplayClass5_0::basisFileStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___basisFileStream_1;

public:
	inline static int32_t get_offset_of_outputStream_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t29B5ACD9AA13C46D839642A2CD7A4078957AE95E, ___outputStream_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_outputStream_0() const { return ___outputStream_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_outputStream_0() { return &___outputStream_0; }
	inline void set_outputStream_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___outputStream_0 = value;
		Il2CppCodeGenWriteBarrier((&___outputStream_0), value);
	}

	inline static int32_t get_offset_of_basisFileStream_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t29B5ACD9AA13C46D839642A2CD7A4078957AE95E, ___basisFileStream_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_basisFileStream_1() const { return ___basisFileStream_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_basisFileStream_1() { return &___basisFileStream_1; }
	inline void set_basisFileStream_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___basisFileStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___basisFileStream_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T29B5ACD9AA13C46D839642A2CD7A4078957AE95E_H
#ifndef HASHALGORITHMWRAPPER_T59B0D3D042D436BEEAB3A6D3F2E5677E25C82686_H
#define HASHALGORITHMWRAPPER_T59B0D3D042D436BEEAB3A6D3F2E5677E25C82686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Octodiff.Core.HashAlgorithmWrapper
struct  HashAlgorithmWrapper_t59B0D3D042D436BEEAB3A6D3F2E5677E25C82686  : public RuntimeObject
{
public:
	// System.Security.Cryptography.HashAlgorithm Octodiff.Core.HashAlgorithmWrapper::algorithm
	HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * ___algorithm_0;
	// System.String Octodiff.Core.HashAlgorithmWrapper::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_algorithm_0() { return static_cast<int32_t>(offsetof(HashAlgorithmWrapper_t59B0D3D042D436BEEAB3A6D3F2E5677E25C82686, ___algorithm_0)); }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * get_algorithm_0() const { return ___algorithm_0; }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA ** get_address_of_algorithm_0() { return &___algorithm_0; }
	inline void set_algorithm_0(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * value)
	{
		___algorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_0), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HashAlgorithmWrapper_t59B0D3D042D436BEEAB3A6D3F2E5677E25C82686, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHMWRAPPER_T59B0D3D042D436BEEAB3A6D3F2E5677E25C82686_H
#ifndef SUPPORTEDALGORITHMS_T4D45387BB8E5BC8B5A0D867FFFCC66988B28322F_H
#define SUPPORTEDALGORITHMS_T4D45387BB8E5BC8B5A0D867FFFCC66988B28322F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Octodiff.Core.SupportedAlgorithms
struct  SupportedAlgorithms_t4D45387BB8E5BC8B5A0D867FFFCC66988B28322F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTEDALGORITHMS_T4D45387BB8E5BC8B5A0D867FFFCC66988B28322F_H
#ifndef HASHING_TE673B84DD49D3ADCD0E15A3E6B8F68D8F135CFCF_H
#define HASHING_TE673B84DD49D3ADCD0E15A3E6B8F68D8F135CFCF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Octodiff.Core.SupportedAlgorithms/Hashing
struct  Hashing_tE673B84DD49D3ADCD0E15A3E6B8F68D8F135CFCF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHING_TE673B84DD49D3ADCD0E15A3E6B8F68D8F135CFCF_H
#ifndef NULLPROGRESSREPORTER_TC3B472D8C44A6FBDE465E4E0D28185E3445F1B38_H
#define NULLPROGRESSREPORTER_TC3B472D8C44A6FBDE465E4E0D28185E3445F1B38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Octodiff.Diagnostics.NullProgressReporter
struct  NullProgressReporter_tC3B472D8C44A6FBDE465E4E0D28185E3445F1B38  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLPROGRESSREPORTER_TC3B472D8C44A6FBDE465E4E0D28185E3445F1B38_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef __STATICARRAYINITTYPESIZEU3D142_T6373C387136FCB64284E833885687923757F585D_H
#define __STATICARRAYINITTYPESIZEU3D142_T6373C387136FCB64284E833885687923757F585D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=142
struct  __StaticArrayInitTypeSizeU3D142_t6373C387136FCB64284E833885687923757F585D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D142_t6373C387136FCB64284E833885687923757F585D__padding[142];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D142_T6373C387136FCB64284E833885687923757F585D_H
#ifndef __STATICARRAYINITTYPESIZEU3D20_T4E67425F223A447FB35934546C68C5AE249274DB_H
#define __STATICARRAYINITTYPESIZEU3D20_T4E67425F223A447FB35934546C68C5AE249274DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20
struct  __StaticArrayInitTypeSizeU3D20_t4E67425F223A447FB35934546C68C5AE249274DB 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_t4E67425F223A447FB35934546C68C5AE249274DB__padding[20];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D20_T4E67425F223A447FB35934546C68C5AE249274DB_H
#ifndef ARGUMENTSINSTANCE_TE7E5E059105C4D56F956C6696AC64F219B328E17_H
#define ARGUMENTSINSTANCE_TE7E5E059105C4D56F956C6696AC64F219B328E17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Argument.ArgumentsInstance
struct  ArgumentsInstance_tE7E5E059105C4D56F956C6696AC64F219B328E17  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:
	// System.Boolean Jint.Native.Argument.ArgumentsInstance::<Strict>k__BackingField
	bool ___U3CStrictU3Ek__BackingField_4;
	// System.Action`1<Jint.Native.Argument.ArgumentsInstance> Jint.Native.Argument.ArgumentsInstance::_initializer
	Action_1_tB88E99B016BB97FBDF8031F96C58443230EF7498 * ____initializer_5;
	// System.Boolean Jint.Native.Argument.ArgumentsInstance::_initialized
	bool ____initialized_6;
	// Jint.Native.Object.ObjectInstance Jint.Native.Argument.ArgumentsInstance::<ParameterMap>k__BackingField
	ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * ___U3CParameterMapU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CStrictU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ArgumentsInstance_tE7E5E059105C4D56F956C6696AC64F219B328E17, ___U3CStrictU3Ek__BackingField_4)); }
	inline bool get_U3CStrictU3Ek__BackingField_4() const { return ___U3CStrictU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CStrictU3Ek__BackingField_4() { return &___U3CStrictU3Ek__BackingField_4; }
	inline void set_U3CStrictU3Ek__BackingField_4(bool value)
	{
		___U3CStrictU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of__initializer_5() { return static_cast<int32_t>(offsetof(ArgumentsInstance_tE7E5E059105C4D56F956C6696AC64F219B328E17, ____initializer_5)); }
	inline Action_1_tB88E99B016BB97FBDF8031F96C58443230EF7498 * get__initializer_5() const { return ____initializer_5; }
	inline Action_1_tB88E99B016BB97FBDF8031F96C58443230EF7498 ** get_address_of__initializer_5() { return &____initializer_5; }
	inline void set__initializer_5(Action_1_tB88E99B016BB97FBDF8031F96C58443230EF7498 * value)
	{
		____initializer_5 = value;
		Il2CppCodeGenWriteBarrier((&____initializer_5), value);
	}

	inline static int32_t get_offset_of__initialized_6() { return static_cast<int32_t>(offsetof(ArgumentsInstance_tE7E5E059105C4D56F956C6696AC64F219B328E17, ____initialized_6)); }
	inline bool get__initialized_6() const { return ____initialized_6; }
	inline bool* get_address_of__initialized_6() { return &____initialized_6; }
	inline void set__initialized_6(bool value)
	{
		____initialized_6 = value;
	}

	inline static int32_t get_offset_of_U3CParameterMapU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ArgumentsInstance_tE7E5E059105C4D56F956C6696AC64F219B328E17, ___U3CParameterMapU3Ek__BackingField_7)); }
	inline ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * get_U3CParameterMapU3Ek__BackingField_7() const { return ___U3CParameterMapU3Ek__BackingField_7; }
	inline ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 ** get_address_of_U3CParameterMapU3Ek__BackingField_7() { return &___U3CParameterMapU3Ek__BackingField_7; }
	inline void set_U3CParameterMapU3Ek__BackingField_7(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * value)
	{
		___U3CParameterMapU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParameterMapU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTSINSTANCE_TE7E5E059105C4D56F956C6696AC64F219B328E17_H
#ifndef ARRAYINSTANCE_T3FAE9584CB9935D861D86E72FE19FE7EB6C5016E_H
#define ARRAYINSTANCE_T3FAE9584CB9935D861D86E72FE19FE7EB6C5016E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Array.ArrayInstance
struct  ArrayInstance_t3FAE9584CB9935D861D86E72FE19FE7EB6C5016E  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:
	// Jint.Engine Jint.Native.Array.ArrayInstance::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_4;
	// System.Collections.Generic.IDictionary`2<System.UInt32,Jint.Runtime.Descriptors.PropertyDescriptor> Jint.Native.Array.ArrayInstance::_array
	RuntimeObject* ____array_5;
	// Jint.Runtime.Descriptors.PropertyDescriptor Jint.Native.Array.ArrayInstance::_length
	PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73 * ____length_6;

public:
	inline static int32_t get_offset_of__engine_4() { return static_cast<int32_t>(offsetof(ArrayInstance_t3FAE9584CB9935D861D86E72FE19FE7EB6C5016E, ____engine_4)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_4() const { return ____engine_4; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_4() { return &____engine_4; }
	inline void set__engine_4(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_4 = value;
		Il2CppCodeGenWriteBarrier((&____engine_4), value);
	}

	inline static int32_t get_offset_of__array_5() { return static_cast<int32_t>(offsetof(ArrayInstance_t3FAE9584CB9935D861D86E72FE19FE7EB6C5016E, ____array_5)); }
	inline RuntimeObject* get__array_5() const { return ____array_5; }
	inline RuntimeObject** get_address_of__array_5() { return &____array_5; }
	inline void set__array_5(RuntimeObject* value)
	{
		____array_5 = value;
		Il2CppCodeGenWriteBarrier((&____array_5), value);
	}

	inline static int32_t get_offset_of__length_6() { return static_cast<int32_t>(offsetof(ArrayInstance_t3FAE9584CB9935D861D86E72FE19FE7EB6C5016E, ____length_6)); }
	inline PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73 * get__length_6() const { return ____length_6; }
	inline PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73 ** get_address_of__length_6() { return &____length_6; }
	inline void set__length_6(PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73 * value)
	{
		____length_6 = value;
		Il2CppCodeGenWriteBarrier((&____length_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYINSTANCE_T3FAE9584CB9935D861D86E72FE19FE7EB6C5016E_H
#ifndef BOOLEANINSTANCE_TD6874F12BD130B6F23EAF7D298FD8546860E749E_H
#define BOOLEANINSTANCE_TD6874F12BD130B6F23EAF7D298FD8546860E749E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Boolean.BooleanInstance
struct  BooleanInstance_tD6874F12BD130B6F23EAF7D298FD8546860E749E  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:
	// Jint.Native.JsValue Jint.Native.Boolean.BooleanInstance::<PrimitiveValue>k__BackingField
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___U3CPrimitiveValueU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CPrimitiveValueU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BooleanInstance_tD6874F12BD130B6F23EAF7D298FD8546860E749E, ___U3CPrimitiveValueU3Ek__BackingField_4)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_U3CPrimitiveValueU3Ek__BackingField_4() const { return ___U3CPrimitiveValueU3Ek__BackingField_4; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_U3CPrimitiveValueU3Ek__BackingField_4() { return &___U3CPrimitiveValueU3Ek__BackingField_4; }
	inline void set_U3CPrimitiveValueU3Ek__BackingField_4(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___U3CPrimitiveValueU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrimitiveValueU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEANINSTANCE_TD6874F12BD130B6F23EAF7D298FD8546860E749E_H
#ifndef DATEINSTANCE_TFEBBE794D12E67B7227A43F06FC2B73BA633972E_H
#define DATEINSTANCE_TFEBBE794D12E67B7227A43F06FC2B73BA633972E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Date.DateInstance
struct  DateInstance_tFEBBE794D12E67B7227A43F06FC2B73BA633972E  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:
	// System.Double Jint.Native.Date.DateInstance::<PrimitiveValue>k__BackingField
	double ___U3CPrimitiveValueU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CPrimitiveValueU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DateInstance_tFEBBE794D12E67B7227A43F06FC2B73BA633972E, ___U3CPrimitiveValueU3Ek__BackingField_6)); }
	inline double get_U3CPrimitiveValueU3Ek__BackingField_6() const { return ___U3CPrimitiveValueU3Ek__BackingField_6; }
	inline double* get_address_of_U3CPrimitiveValueU3Ek__BackingField_6() { return &___U3CPrimitiveValueU3Ek__BackingField_6; }
	inline void set_U3CPrimitiveValueU3Ek__BackingField_6(double value)
	{
		___U3CPrimitiveValueU3Ek__BackingField_6 = value;
	}
};

struct DateInstance_tFEBBE794D12E67B7227A43F06FC2B73BA633972E_StaticFields
{
public:
	// System.Double Jint.Native.Date.DateInstance::Max
	double ___Max_4;
	// System.Double Jint.Native.Date.DateInstance::Min
	double ___Min_5;

public:
	inline static int32_t get_offset_of_Max_4() { return static_cast<int32_t>(offsetof(DateInstance_tFEBBE794D12E67B7227A43F06FC2B73BA633972E_StaticFields, ___Max_4)); }
	inline double get_Max_4() const { return ___Max_4; }
	inline double* get_address_of_Max_4() { return &___Max_4; }
	inline void set_Max_4(double value)
	{
		___Max_4 = value;
	}

	inline static int32_t get_offset_of_Min_5() { return static_cast<int32_t>(offsetof(DateInstance_tFEBBE794D12E67B7227A43F06FC2B73BA633972E_StaticFields, ___Min_5)); }
	inline double get_Min_5() const { return ___Min_5; }
	inline double* get_address_of_Min_5() { return &___Min_5; }
	inline void set_Min_5(double value)
	{
		___Min_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEINSTANCE_TFEBBE794D12E67B7227A43F06FC2B73BA633972E_H
#ifndef ERRORINSTANCE_T2342D0236A711154460069F45C331A2002E1A3E9_H
#define ERRORINSTANCE_T2342D0236A711154460069F45C331A2002E1A3E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Error.ErrorInstance
struct  ErrorInstance_t2342D0236A711154460069F45C331A2002E1A3E9  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORINSTANCE_T2342D0236A711154460069F45C331A2002E1A3E9_H
#ifndef FUNCTIONINSTANCE_TE38D99DCDA862B7930D886A656437288185E5458_H
#define FUNCTIONINSTANCE_TE38D99DCDA862B7930D886A656437288185E5458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Function.FunctionInstance
struct  FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:
	// Jint.Engine Jint.Native.Function.FunctionInstance::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_4;
	// Jint.Runtime.Environments.LexicalEnvironment Jint.Native.Function.FunctionInstance::<Scope>k__BackingField
	LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * ___U3CScopeU3Ek__BackingField_5;
	// System.String[] Jint.Native.Function.FunctionInstance::<FormalParameters>k__BackingField
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___U3CFormalParametersU3Ek__BackingField_6;
	// System.Boolean Jint.Native.Function.FunctionInstance::<Strict>k__BackingField
	bool ___U3CStrictU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of__engine_4() { return static_cast<int32_t>(offsetof(FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458, ____engine_4)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_4() const { return ____engine_4; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_4() { return &____engine_4; }
	inline void set__engine_4(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_4 = value;
		Il2CppCodeGenWriteBarrier((&____engine_4), value);
	}

	inline static int32_t get_offset_of_U3CScopeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458, ___U3CScopeU3Ek__BackingField_5)); }
	inline LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * get_U3CScopeU3Ek__BackingField_5() const { return ___U3CScopeU3Ek__BackingField_5; }
	inline LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 ** get_address_of_U3CScopeU3Ek__BackingField_5() { return &___U3CScopeU3Ek__BackingField_5; }
	inline void set_U3CScopeU3Ek__BackingField_5(LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * value)
	{
		___U3CScopeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScopeU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CFormalParametersU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458, ___U3CFormalParametersU3Ek__BackingField_6)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_U3CFormalParametersU3Ek__BackingField_6() const { return ___U3CFormalParametersU3Ek__BackingField_6; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_U3CFormalParametersU3Ek__BackingField_6() { return &___U3CFormalParametersU3Ek__BackingField_6; }
	inline void set_U3CFormalParametersU3Ek__BackingField_6(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___U3CFormalParametersU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormalParametersU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CStrictU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458, ___U3CStrictU3Ek__BackingField_7)); }
	inline bool get_U3CStrictU3Ek__BackingField_7() const { return ___U3CStrictU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CStrictU3Ek__BackingField_7() { return &___U3CStrictU3Ek__BackingField_7; }
	inline void set_U3CStrictU3Ek__BackingField_7(bool value)
	{
		___U3CStrictU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONINSTANCE_TE38D99DCDA862B7930D886A656437288185E5458_H
#ifndef GLOBALOBJECT_TD5B044775D4BA82AD01F71F4F605F090811A5D27_H
#define GLOBALOBJECT_TD5B044775D4BA82AD01F71F4F605F090811A5D27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Global.GlobalObject
struct  GlobalObject_tD5B044775D4BA82AD01F71F4F605F090811A5D27  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:

public:
};

struct GlobalObject_tD5B044775D4BA82AD01F71F4F605F090811A5D27_StaticFields
{
public:
	// System.Char[] Jint.Native.Global.GlobalObject::UriReserved
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___UriReserved_4;
	// System.Char[] Jint.Native.Global.GlobalObject::UriUnescaped
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___UriUnescaped_5;

public:
	inline static int32_t get_offset_of_UriReserved_4() { return static_cast<int32_t>(offsetof(GlobalObject_tD5B044775D4BA82AD01F71F4F605F090811A5D27_StaticFields, ___UriReserved_4)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_UriReserved_4() const { return ___UriReserved_4; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_UriReserved_4() { return &___UriReserved_4; }
	inline void set_UriReserved_4(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___UriReserved_4 = value;
		Il2CppCodeGenWriteBarrier((&___UriReserved_4), value);
	}

	inline static int32_t get_offset_of_UriUnescaped_5() { return static_cast<int32_t>(offsetof(GlobalObject_tD5B044775D4BA82AD01F71F4F605F090811A5D27_StaticFields, ___UriUnescaped_5)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_UriUnescaped_5() const { return ___UriUnescaped_5; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_UriUnescaped_5() { return &___UriUnescaped_5; }
	inline void set_UriUnescaped_5(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___UriUnescaped_5 = value;
		Il2CppCodeGenWriteBarrier((&___UriUnescaped_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALOBJECT_TD5B044775D4BA82AD01F71F4F605F090811A5D27_H
#ifndef JSONINSTANCE_TAAC3C2498512BF0ADC8391178660107A034B91AE_H
#define JSONINSTANCE_TAAC3C2498512BF0ADC8391178660107A034B91AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Json.JsonInstance
struct  JsonInstance_tAAC3C2498512BF0ADC8391178660107A034B91AE  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:
	// Jint.Engine Jint.Native.Json.JsonInstance::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_4;

public:
	inline static int32_t get_offset_of__engine_4() { return static_cast<int32_t>(offsetof(JsonInstance_tAAC3C2498512BF0ADC8391178660107A034B91AE, ____engine_4)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_4() const { return ____engine_4; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_4() { return &____engine_4; }
	inline void set__engine_4(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_4 = value;
		Il2CppCodeGenWriteBarrier((&____engine_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONINSTANCE_TAAC3C2498512BF0ADC8391178660107A034B91AE_H
#ifndef MATHINSTANCE_T3512482B63B6D54FC3F9105785754D409F54F4C0_H
#define MATHINSTANCE_T3512482B63B6D54FC3F9105785754D409F54F4C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Math.MathInstance
struct  MathInstance_t3512482B63B6D54FC3F9105785754D409F54F4C0  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:

public:
};

struct MathInstance_t3512482B63B6D54FC3F9105785754D409F54F4C0_StaticFields
{
public:
	// System.Random Jint.Native.Math.MathInstance::_random
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ____random_4;

public:
	inline static int32_t get_offset_of__random_4() { return static_cast<int32_t>(offsetof(MathInstance_t3512482B63B6D54FC3F9105785754D409F54F4C0_StaticFields, ____random_4)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get__random_4() const { return ____random_4; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of__random_4() { return &____random_4; }
	inline void set__random_4(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		____random_4 = value;
		Il2CppCodeGenWriteBarrier((&____random_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHINSTANCE_T3512482B63B6D54FC3F9105785754D409F54F4C0_H
#ifndef NUMBERINSTANCE_T063D5094F4A658EB965EFEA8A1E9C087B2E65F34_H
#define NUMBERINSTANCE_T063D5094F4A658EB965EFEA8A1E9C087B2E65F34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Number.NumberInstance
struct  NumberInstance_t063D5094F4A658EB965EFEA8A1E9C087B2E65F34  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:
	// Jint.Native.JsValue Jint.Native.Number.NumberInstance::<PrimitiveValue>k__BackingField
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___U3CPrimitiveValueU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CPrimitiveValueU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(NumberInstance_t063D5094F4A658EB965EFEA8A1E9C087B2E65F34, ___U3CPrimitiveValueU3Ek__BackingField_5)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_U3CPrimitiveValueU3Ek__BackingField_5() const { return ___U3CPrimitiveValueU3Ek__BackingField_5; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_U3CPrimitiveValueU3Ek__BackingField_5() { return &___U3CPrimitiveValueU3Ek__BackingField_5; }
	inline void set_U3CPrimitiveValueU3Ek__BackingField_5(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___U3CPrimitiveValueU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrimitiveValueU3Ek__BackingField_5), value);
	}
};

struct NumberInstance_t063D5094F4A658EB965EFEA8A1E9C087B2E65F34_StaticFields
{
public:
	// System.Int64 Jint.Native.Number.NumberInstance::NegativeZeroBits
	int64_t ___NegativeZeroBits_4;

public:
	inline static int32_t get_offset_of_NegativeZeroBits_4() { return static_cast<int32_t>(offsetof(NumberInstance_t063D5094F4A658EB965EFEA8A1E9C087B2E65F34_StaticFields, ___NegativeZeroBits_4)); }
	inline int64_t get_NegativeZeroBits_4() const { return ___NegativeZeroBits_4; }
	inline int64_t* get_address_of_NegativeZeroBits_4() { return &___NegativeZeroBits_4; }
	inline void set_NegativeZeroBits_4(int64_t value)
	{
		___NegativeZeroBits_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBERINSTANCE_T063D5094F4A658EB965EFEA8A1E9C087B2E65F34_H
#ifndef OBJECTPROTOTYPE_TEC07D7C1F708C35E24A7171E19D9A411DD0FECDB_H
#define OBJECTPROTOTYPE_TEC07D7C1F708C35E24A7171E19D9A411DD0FECDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Object.ObjectPrototype
struct  ObjectPrototype_tEC07D7C1F708C35E24A7171E19D9A411DD0FECDB  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPROTOTYPE_TEC07D7C1F708C35E24A7171E19D9A411DD0FECDB_H
#ifndef REGEXPINSTANCE_TDC893C9F06BFA6B6D0B496F594731135534AE3F3_H
#define REGEXPINSTANCE_TDC893C9F06BFA6B6D0B496F594731135534AE3F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.RegExp.RegExpInstance
struct  RegExpInstance_tDC893C9F06BFA6B6D0B496F594731135534AE3F3  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:
	// System.Text.RegularExpressions.Regex Jint.Native.RegExp.RegExpInstance::<Value>k__BackingField
	Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * ___U3CValueU3Ek__BackingField_4;
	// System.String Jint.Native.RegExp.RegExpInstance::<Source>k__BackingField
	String_t* ___U3CSourceU3Ek__BackingField_5;
	// System.String Jint.Native.RegExp.RegExpInstance::<Flags>k__BackingField
	String_t* ___U3CFlagsU3Ek__BackingField_6;
	// System.Boolean Jint.Native.RegExp.RegExpInstance::<Global>k__BackingField
	bool ___U3CGlobalU3Ek__BackingField_7;
	// System.Boolean Jint.Native.RegExp.RegExpInstance::<IgnoreCase>k__BackingField
	bool ___U3CIgnoreCaseU3Ek__BackingField_8;
	// System.Boolean Jint.Native.RegExp.RegExpInstance::<Multiline>k__BackingField
	bool ___U3CMultilineU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RegExpInstance_tDC893C9F06BFA6B6D0B496F594731135534AE3F3, ___U3CValueU3Ek__BackingField_4)); }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * get_U3CValueU3Ek__BackingField_4() const { return ___U3CValueU3Ek__BackingField_4; }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF ** get_address_of_U3CValueU3Ek__BackingField_4() { return &___U3CValueU3Ek__BackingField_4; }
	inline void set_U3CValueU3Ek__BackingField_4(Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * value)
	{
		___U3CValueU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CSourceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RegExpInstance_tDC893C9F06BFA6B6D0B496F594731135534AE3F3, ___U3CSourceU3Ek__BackingField_5)); }
	inline String_t* get_U3CSourceU3Ek__BackingField_5() const { return ___U3CSourceU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CSourceU3Ek__BackingField_5() { return &___U3CSourceU3Ek__BackingField_5; }
	inline void set_U3CSourceU3Ek__BackingField_5(String_t* value)
	{
		___U3CSourceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSourceU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CFlagsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RegExpInstance_tDC893C9F06BFA6B6D0B496F594731135534AE3F3, ___U3CFlagsU3Ek__BackingField_6)); }
	inline String_t* get_U3CFlagsU3Ek__BackingField_6() const { return ___U3CFlagsU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CFlagsU3Ek__BackingField_6() { return &___U3CFlagsU3Ek__BackingField_6; }
	inline void set_U3CFlagsU3Ek__BackingField_6(String_t* value)
	{
		___U3CFlagsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFlagsU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CGlobalU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RegExpInstance_tDC893C9F06BFA6B6D0B496F594731135534AE3F3, ___U3CGlobalU3Ek__BackingField_7)); }
	inline bool get_U3CGlobalU3Ek__BackingField_7() const { return ___U3CGlobalU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CGlobalU3Ek__BackingField_7() { return &___U3CGlobalU3Ek__BackingField_7; }
	inline void set_U3CGlobalU3Ek__BackingField_7(bool value)
	{
		___U3CGlobalU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreCaseU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RegExpInstance_tDC893C9F06BFA6B6D0B496F594731135534AE3F3, ___U3CIgnoreCaseU3Ek__BackingField_8)); }
	inline bool get_U3CIgnoreCaseU3Ek__BackingField_8() const { return ___U3CIgnoreCaseU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIgnoreCaseU3Ek__BackingField_8() { return &___U3CIgnoreCaseU3Ek__BackingField_8; }
	inline void set_U3CIgnoreCaseU3Ek__BackingField_8(bool value)
	{
		___U3CIgnoreCaseU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CMultilineU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RegExpInstance_tDC893C9F06BFA6B6D0B496F594731135534AE3F3, ___U3CMultilineU3Ek__BackingField_9)); }
	inline bool get_U3CMultilineU3Ek__BackingField_9() const { return ___U3CMultilineU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CMultilineU3Ek__BackingField_9() { return &___U3CMultilineU3Ek__BackingField_9; }
	inline void set_U3CMultilineU3Ek__BackingField_9(bool value)
	{
		___U3CMultilineU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXPINSTANCE_TDC893C9F06BFA6B6D0B496F594731135534AE3F3_H
#ifndef STRINGINSTANCE_TD8ED774792033B8F196F6ABB557DFDBADE0B22B7_H
#define STRINGINSTANCE_TD8ED774792033B8F196F6ABB557DFDBADE0B22B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.String.StringInstance
struct  StringInstance_tD8ED774792033B8F196F6ABB557DFDBADE0B22B7  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:
	// Jint.Native.JsValue Jint.Native.String.StringInstance::<PrimitiveValue>k__BackingField
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___U3CPrimitiveValueU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CPrimitiveValueU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(StringInstance_tD8ED774792033B8F196F6ABB557DFDBADE0B22B7, ___U3CPrimitiveValueU3Ek__BackingField_4)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_U3CPrimitiveValueU3Ek__BackingField_4() const { return ___U3CPrimitiveValueU3Ek__BackingField_4; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_U3CPrimitiveValueU3Ek__BackingField_4() { return &___U3CPrimitiveValueU3Ek__BackingField_4; }
	inline void set_U3CPrimitiveValueU3Ek__BackingField_4(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___U3CPrimitiveValueU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrimitiveValueU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGINSTANCE_TD8ED774792033B8F196F6ABB557DFDBADE0B22B7_H
#ifndef STATE_TE5E4EE87409DF109801A5725B4A75C00E666E86A_H
#define STATE_TE5E4EE87409DF109801A5725B4A75C00E666E86A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.State
struct  State_tE5E4EE87409DF109801A5725B4A75C00E666E86A 
{
public:
	// System.Int32 Jint.Parser.State::LastCommentStart
	int32_t ___LastCommentStart_0;
	// System.Boolean Jint.Parser.State::AllowIn
	bool ___AllowIn_1;
	// System.Collections.Generic.HashSet`1<System.String> Jint.Parser.State::LabelSet
	HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * ___LabelSet_2;
	// System.Boolean Jint.Parser.State::InFunctionBody
	bool ___InFunctionBody_3;
	// System.Boolean Jint.Parser.State::InIteration
	bool ___InIteration_4;
	// System.Boolean Jint.Parser.State::InSwitch
	bool ___InSwitch_5;
	// System.Collections.Generic.Stack`1<System.Int32> Jint.Parser.State::MarkerStack
	Stack_1_t09A8EBB6E2F5CCF7E2E67BDECA461795F93D523A * ___MarkerStack_6;

public:
	inline static int32_t get_offset_of_LastCommentStart_0() { return static_cast<int32_t>(offsetof(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A, ___LastCommentStart_0)); }
	inline int32_t get_LastCommentStart_0() const { return ___LastCommentStart_0; }
	inline int32_t* get_address_of_LastCommentStart_0() { return &___LastCommentStart_0; }
	inline void set_LastCommentStart_0(int32_t value)
	{
		___LastCommentStart_0 = value;
	}

	inline static int32_t get_offset_of_AllowIn_1() { return static_cast<int32_t>(offsetof(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A, ___AllowIn_1)); }
	inline bool get_AllowIn_1() const { return ___AllowIn_1; }
	inline bool* get_address_of_AllowIn_1() { return &___AllowIn_1; }
	inline void set_AllowIn_1(bool value)
	{
		___AllowIn_1 = value;
	}

	inline static int32_t get_offset_of_LabelSet_2() { return static_cast<int32_t>(offsetof(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A, ___LabelSet_2)); }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * get_LabelSet_2() const { return ___LabelSet_2; }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A ** get_address_of_LabelSet_2() { return &___LabelSet_2; }
	inline void set_LabelSet_2(HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * value)
	{
		___LabelSet_2 = value;
		Il2CppCodeGenWriteBarrier((&___LabelSet_2), value);
	}

	inline static int32_t get_offset_of_InFunctionBody_3() { return static_cast<int32_t>(offsetof(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A, ___InFunctionBody_3)); }
	inline bool get_InFunctionBody_3() const { return ___InFunctionBody_3; }
	inline bool* get_address_of_InFunctionBody_3() { return &___InFunctionBody_3; }
	inline void set_InFunctionBody_3(bool value)
	{
		___InFunctionBody_3 = value;
	}

	inline static int32_t get_offset_of_InIteration_4() { return static_cast<int32_t>(offsetof(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A, ___InIteration_4)); }
	inline bool get_InIteration_4() const { return ___InIteration_4; }
	inline bool* get_address_of_InIteration_4() { return &___InIteration_4; }
	inline void set_InIteration_4(bool value)
	{
		___InIteration_4 = value;
	}

	inline static int32_t get_offset_of_InSwitch_5() { return static_cast<int32_t>(offsetof(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A, ___InSwitch_5)); }
	inline bool get_InSwitch_5() const { return ___InSwitch_5; }
	inline bool* get_address_of_InSwitch_5() { return &___InSwitch_5; }
	inline void set_InSwitch_5(bool value)
	{
		___InSwitch_5 = value;
	}

	inline static int32_t get_offset_of_MarkerStack_6() { return static_cast<int32_t>(offsetof(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A, ___MarkerStack_6)); }
	inline Stack_1_t09A8EBB6E2F5CCF7E2E67BDECA461795F93D523A * get_MarkerStack_6() const { return ___MarkerStack_6; }
	inline Stack_1_t09A8EBB6E2F5CCF7E2E67BDECA461795F93D523A ** get_address_of_MarkerStack_6() { return &___MarkerStack_6; }
	inline void set_MarkerStack_6(Stack_1_t09A8EBB6E2F5CCF7E2E67BDECA461795F93D523A * value)
	{
		___MarkerStack_6 = value;
		Il2CppCodeGenWriteBarrier((&___MarkerStack_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Jint.Parser.State
struct State_tE5E4EE87409DF109801A5725B4A75C00E666E86A_marshaled_pinvoke
{
	int32_t ___LastCommentStart_0;
	int32_t ___AllowIn_1;
	HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * ___LabelSet_2;
	int32_t ___InFunctionBody_3;
	int32_t ___InIteration_4;
	int32_t ___InSwitch_5;
	Stack_1_t09A8EBB6E2F5CCF7E2E67BDECA461795F93D523A * ___MarkerStack_6;
};
// Native definition for COM marshalling of Jint.Parser.State
struct State_tE5E4EE87409DF109801A5725B4A75C00E666E86A_marshaled_com
{
	int32_t ___LastCommentStart_0;
	int32_t ___AllowIn_1;
	HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * ___LabelSet_2;
	int32_t ___InFunctionBody_3;
	int32_t ___InIteration_4;
	int32_t ___InSwitch_5;
	Stack_1_t09A8EBB6E2F5CCF7E2E67BDECA461795F93D523A * ___MarkerStack_6;
};
#endif // STATE_TE5E4EE87409DF109801A5725B4A75C00E666E86A_H
#ifndef COMPATIBILITYEXCEPTION_T2744573051489904AFA54781620382D1740CAA67_H
#define COMPATIBILITYEXCEPTION_T2744573051489904AFA54781620382D1740CAA67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Octodiff.Core.CompatibilityException
struct  CompatibilityException_t2744573051489904AFA54781620382D1740CAA67  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPATIBILITYEXCEPTION_T2744573051489904AFA54781620382D1740CAA67_H
#ifndef CORRUPTFILEFORMATEXCEPTION_TFB71F02C812CCFFAE7E61A6EA9679D9F18003220_H
#define CORRUPTFILEFORMATEXCEPTION_TFB71F02C812CCFFAE7E61A6EA9679D9F18003220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Octodiff.Core.CorruptFileFormatException
struct  CorruptFileFormatException_tFB71F02C812CCFFAE7E61A6EA9679D9F18003220  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORRUPTFILEFORMATEXCEPTION_TFB71F02C812CCFFAE7E61A6EA9679D9F18003220_H
#ifndef USAGEEXCEPTION_T956E8ACD6899FD00E72618C8FA87DAC6D6115B4E_H
#define USAGEEXCEPTION_T956E8ACD6899FD00E72618C8FA87DAC6D6115B4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Octodiff.Core.UsageException
struct  UsageException_t956E8ACD6899FD00E72618C8FA87DAC6D6115B4E  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USAGEEXCEPTION_T956E8ACD6899FD00E72618C8FA87DAC6D6115B4E_H
#ifndef KEYVALUEPAIR_2_T911B3BBBBCC41329D0A2EFDB6EB5BDA5D86E52BE_H
#define KEYVALUEPAIR_2_T911B3BBBBCC41329D0A2EFDB6EB5BDA5D86E52BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>
struct  KeyValuePair_2_t911B3BBBBCC41329D0A2EFDB6EB5BDA5D86E52BE 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t911B3BBBBCC41329D0A2EFDB6EB5BDA5D86E52BE, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t911B3BBBBCC41329D0A2EFDB6EB5BDA5D86E52BE, ___value_1)); }
	inline PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73 * get_value_1() const { return ___value_1; }
	inline PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T911B3BBBBCC41329D0A2EFDB6EB5BDA5D86E52BE_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#define NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TBD5F738543D4F244D12D007ECBC0145D4EA910C2_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TBD5F738543D4F244D12D007ECBC0145D4EA910C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tBD5F738543D4F244D12D007ECBC0145D4EA910C2  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tBD5F738543D4F244D12D007ECBC0145D4EA910C2_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=142 <PrivateImplementationDetails>::7B7505A366B96617ACEAD621E1A0916FC82FE252
	__StaticArrayInitTypeSizeU3D142_t6373C387136FCB64284E833885687923757F585D  ___7B7505A366B96617ACEAD621E1A0916FC82FE252_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::84A0343BF19D2274E807E1B6505C382F81D6E3C9
	__StaticArrayInitTypeSizeU3D20_t4E67425F223A447FB35934546C68C5AE249274DB  ___84A0343BF19D2274E807E1B6505C382F81D6E3C9_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::CAD8623838274740D6497489F547CE972C42A942
	__StaticArrayInitTypeSizeU3D20_t4E67425F223A447FB35934546C68C5AE249274DB  ___CAD8623838274740D6497489F547CE972C42A942_2;

public:
	inline static int32_t get_offset_of_U37B7505A366B96617ACEAD621E1A0916FC82FE252_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBD5F738543D4F244D12D007ECBC0145D4EA910C2_StaticFields, ___7B7505A366B96617ACEAD621E1A0916FC82FE252_0)); }
	inline __StaticArrayInitTypeSizeU3D142_t6373C387136FCB64284E833885687923757F585D  get_U37B7505A366B96617ACEAD621E1A0916FC82FE252_0() const { return ___7B7505A366B96617ACEAD621E1A0916FC82FE252_0; }
	inline __StaticArrayInitTypeSizeU3D142_t6373C387136FCB64284E833885687923757F585D * get_address_of_U37B7505A366B96617ACEAD621E1A0916FC82FE252_0() { return &___7B7505A366B96617ACEAD621E1A0916FC82FE252_0; }
	inline void set_U37B7505A366B96617ACEAD621E1A0916FC82FE252_0(__StaticArrayInitTypeSizeU3D142_t6373C387136FCB64284E833885687923757F585D  value)
	{
		___7B7505A366B96617ACEAD621E1A0916FC82FE252_0 = value;
	}

	inline static int32_t get_offset_of_U384A0343BF19D2274E807E1B6505C382F81D6E3C9_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBD5F738543D4F244D12D007ECBC0145D4EA910C2_StaticFields, ___84A0343BF19D2274E807E1B6505C382F81D6E3C9_1)); }
	inline __StaticArrayInitTypeSizeU3D20_t4E67425F223A447FB35934546C68C5AE249274DB  get_U384A0343BF19D2274E807E1B6505C382F81D6E3C9_1() const { return ___84A0343BF19D2274E807E1B6505C382F81D6E3C9_1; }
	inline __StaticArrayInitTypeSizeU3D20_t4E67425F223A447FB35934546C68C5AE249274DB * get_address_of_U384A0343BF19D2274E807E1B6505C382F81D6E3C9_1() { return &___84A0343BF19D2274E807E1B6505C382F81D6E3C9_1; }
	inline void set_U384A0343BF19D2274E807E1B6505C382F81D6E3C9_1(__StaticArrayInitTypeSizeU3D20_t4E67425F223A447FB35934546C68C5AE249274DB  value)
	{
		___84A0343BF19D2274E807E1B6505C382F81D6E3C9_1 = value;
	}

	inline static int32_t get_offset_of_CAD8623838274740D6497489F547CE972C42A942_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBD5F738543D4F244D12D007ECBC0145D4EA910C2_StaticFields, ___CAD8623838274740D6497489F547CE972C42A942_2)); }
	inline __StaticArrayInitTypeSizeU3D20_t4E67425F223A447FB35934546C68C5AE249274DB  get_CAD8623838274740D6497489F547CE972C42A942_2() const { return ___CAD8623838274740D6497489F547CE972C42A942_2; }
	inline __StaticArrayInitTypeSizeU3D20_t4E67425F223A447FB35934546C68C5AE249274DB * get_address_of_CAD8623838274740D6497489F547CE972C42A942_2() { return &___CAD8623838274740D6497489F547CE972C42A942_2; }
	inline void set_CAD8623838274740D6497489F547CE972C42A942_2(__StaticArrayInitTypeSizeU3D20_t4E67425F223A447FB35934546C68C5AE249274DB  value)
	{
		___CAD8623838274740D6497489F547CE972C42A942_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TBD5F738543D4F244D12D007ECBC0145D4EA910C2_H
#ifndef ARRAYCONSTRUCTOR_T3EE97DD1C34DD817D0D8540BBEC734D56BB7D9C8_H
#define ARRAYCONSTRUCTOR_T3EE97DD1C34DD817D0D8540BBEC734D56BB7D9C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Array.ArrayConstructor
struct  ArrayConstructor_t3EE97DD1C34DD817D0D8540BBEC734D56BB7D9C8  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// Jint.Native.Array.ArrayPrototype Jint.Native.Array.ArrayConstructor::<PrototypeObject>k__BackingField
	ArrayPrototype_t02B45CBA6C396ABAB36D738790A9B466E69C0A80 * ___U3CPrototypeObjectU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CPrototypeObjectU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ArrayConstructor_t3EE97DD1C34DD817D0D8540BBEC734D56BB7D9C8, ___U3CPrototypeObjectU3Ek__BackingField_8)); }
	inline ArrayPrototype_t02B45CBA6C396ABAB36D738790A9B466E69C0A80 * get_U3CPrototypeObjectU3Ek__BackingField_8() const { return ___U3CPrototypeObjectU3Ek__BackingField_8; }
	inline ArrayPrototype_t02B45CBA6C396ABAB36D738790A9B466E69C0A80 ** get_address_of_U3CPrototypeObjectU3Ek__BackingField_8() { return &___U3CPrototypeObjectU3Ek__BackingField_8; }
	inline void set_U3CPrototypeObjectU3Ek__BackingField_8(ArrayPrototype_t02B45CBA6C396ABAB36D738790A9B466E69C0A80 * value)
	{
		___U3CPrototypeObjectU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrototypeObjectU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYCONSTRUCTOR_T3EE97DD1C34DD817D0D8540BBEC734D56BB7D9C8_H
#ifndef U3CGETOWNPROPERTIESU3ED__9_T51477DED42F4DA6F170DBD9ADAFAC43472331381_H
#define U3CGETOWNPROPERTIESU3ED__9_T51477DED42F4DA6F170DBD9ADAFAC43472331381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Array.ArrayInstance/<GetOwnProperties>d__9
struct  U3CGetOwnPropertiesU3Ed__9_t51477DED42F4DA6F170DBD9ADAFAC43472331381  : public RuntimeObject
{
public:
	// System.Int32 Jint.Native.Array.ArrayInstance/<GetOwnProperties>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor> Jint.Native.Array.ArrayInstance/<GetOwnProperties>d__9::<>2__current
	KeyValuePair_2_t911B3BBBBCC41329D0A2EFDB6EB5BDA5D86E52BE  ___U3CU3E2__current_1;
	// System.Int32 Jint.Native.Array.ArrayInstance/<GetOwnProperties>d__9::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Jint.Native.Array.ArrayInstance Jint.Native.Array.ArrayInstance/<GetOwnProperties>d__9::<>4__this
	ArrayInstance_t3FAE9584CB9935D861D86E72FE19FE7EB6C5016E * ___U3CU3E4__this_3;
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,Jint.Runtime.Descriptors.PropertyDescriptor>> Jint.Native.Array.ArrayInstance/<GetOwnProperties>d__9::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_4;
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>> Jint.Native.Array.ArrayInstance/<GetOwnProperties>d__9::<>7__wrap2
	RuntimeObject* ___U3CU3E7__wrap2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetOwnPropertiesU3Ed__9_t51477DED42F4DA6F170DBD9ADAFAC43472331381, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetOwnPropertiesU3Ed__9_t51477DED42F4DA6F170DBD9ADAFAC43472331381, ___U3CU3E2__current_1)); }
	inline KeyValuePair_2_t911B3BBBBCC41329D0A2EFDB6EB5BDA5D86E52BE  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline KeyValuePair_2_t911B3BBBBCC41329D0A2EFDB6EB5BDA5D86E52BE * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(KeyValuePair_2_t911B3BBBBCC41329D0A2EFDB6EB5BDA5D86E52BE  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetOwnPropertiesU3Ed__9_t51477DED42F4DA6F170DBD9ADAFAC43472331381, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetOwnPropertiesU3Ed__9_t51477DED42F4DA6F170DBD9ADAFAC43472331381, ___U3CU3E4__this_3)); }
	inline ArrayInstance_t3FAE9584CB9935D861D86E72FE19FE7EB6C5016E * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ArrayInstance_t3FAE9584CB9935D861D86E72FE19FE7EB6C5016E ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ArrayInstance_t3FAE9584CB9935D861D86E72FE19FE7EB6C5016E * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_4() { return static_cast<int32_t>(offsetof(U3CGetOwnPropertiesU3Ed__9_t51477DED42F4DA6F170DBD9ADAFAC43472331381, ___U3CU3E7__wrap1_4)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_4() const { return ___U3CU3E7__wrap1_4; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_4() { return &___U3CU3E7__wrap1_4; }
	inline void set_U3CU3E7__wrap1_4(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_5() { return static_cast<int32_t>(offsetof(U3CGetOwnPropertiesU3Ed__9_t51477DED42F4DA6F170DBD9ADAFAC43472331381, ___U3CU3E7__wrap2_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap2_5() const { return ___U3CU3E7__wrap2_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap2_5() { return &___U3CU3E7__wrap2_5; }
	inline void set_U3CU3E7__wrap2_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETOWNPROPERTIESU3ED__9_T51477DED42F4DA6F170DBD9ADAFAC43472331381_H
#ifndef ARRAYPROTOTYPE_T02B45CBA6C396ABAB36D738790A9B466E69C0A80_H
#define ARRAYPROTOTYPE_T02B45CBA6C396ABAB36D738790A9B466E69C0A80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Array.ArrayPrototype
struct  ArrayPrototype_t02B45CBA6C396ABAB36D738790A9B466E69C0A80  : public ArrayInstance_t3FAE9584CB9935D861D86E72FE19FE7EB6C5016E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYPROTOTYPE_T02B45CBA6C396ABAB36D738790A9B466E69C0A80_H
#ifndef BOOLEANCONSTRUCTOR_T4500B0A4DA339E11D5E6E983A0168B8D98678CDB_H
#define BOOLEANCONSTRUCTOR_T4500B0A4DA339E11D5E6E983A0168B8D98678CDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Boolean.BooleanConstructor
struct  BooleanConstructor_t4500B0A4DA339E11D5E6E983A0168B8D98678CDB  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// Jint.Native.Boolean.BooleanPrototype Jint.Native.Boolean.BooleanConstructor::<PrototypeObject>k__BackingField
	BooleanPrototype_tF496E12A7D263301D5E7E383BC0DB245AD8A1C03 * ___U3CPrototypeObjectU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CPrototypeObjectU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(BooleanConstructor_t4500B0A4DA339E11D5E6E983A0168B8D98678CDB, ___U3CPrototypeObjectU3Ek__BackingField_8)); }
	inline BooleanPrototype_tF496E12A7D263301D5E7E383BC0DB245AD8A1C03 * get_U3CPrototypeObjectU3Ek__BackingField_8() const { return ___U3CPrototypeObjectU3Ek__BackingField_8; }
	inline BooleanPrototype_tF496E12A7D263301D5E7E383BC0DB245AD8A1C03 ** get_address_of_U3CPrototypeObjectU3Ek__BackingField_8() { return &___U3CPrototypeObjectU3Ek__BackingField_8; }
	inline void set_U3CPrototypeObjectU3Ek__BackingField_8(BooleanPrototype_tF496E12A7D263301D5E7E383BC0DB245AD8A1C03 * value)
	{
		___U3CPrototypeObjectU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrototypeObjectU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEANCONSTRUCTOR_T4500B0A4DA339E11D5E6E983A0168B8D98678CDB_H
#ifndef BOOLEANPROTOTYPE_TF496E12A7D263301D5E7E383BC0DB245AD8A1C03_H
#define BOOLEANPROTOTYPE_TF496E12A7D263301D5E7E383BC0DB245AD8A1C03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Boolean.BooleanPrototype
struct  BooleanPrototype_tF496E12A7D263301D5E7E383BC0DB245AD8A1C03  : public BooleanInstance_tD6874F12BD130B6F23EAF7D298FD8546860E749E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEANPROTOTYPE_TF496E12A7D263301D5E7E383BC0DB245AD8A1C03_H
#ifndef DATECONSTRUCTOR_T5098B976636DBEB0847A7217566BD868C59701D0_H
#define DATECONSTRUCTOR_T5098B976636DBEB0847A7217566BD868C59701D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Date.DateConstructor
struct  DateConstructor_t5098B976636DBEB0847A7217566BD868C59701D0  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// Jint.Native.Date.DatePrototype Jint.Native.Date.DateConstructor::<PrototypeObject>k__BackingField
	DatePrototype_t1A74663765296A18E85477D2A2F0EB7B7962D07E * ___U3CPrototypeObjectU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CPrototypeObjectU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(DateConstructor_t5098B976636DBEB0847A7217566BD868C59701D0, ___U3CPrototypeObjectU3Ek__BackingField_9)); }
	inline DatePrototype_t1A74663765296A18E85477D2A2F0EB7B7962D07E * get_U3CPrototypeObjectU3Ek__BackingField_9() const { return ___U3CPrototypeObjectU3Ek__BackingField_9; }
	inline DatePrototype_t1A74663765296A18E85477D2A2F0EB7B7962D07E ** get_address_of_U3CPrototypeObjectU3Ek__BackingField_9() { return &___U3CPrototypeObjectU3Ek__BackingField_9; }
	inline void set_U3CPrototypeObjectU3Ek__BackingField_9(DatePrototype_t1A74663765296A18E85477D2A2F0EB7B7962D07E * value)
	{
		___U3CPrototypeObjectU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrototypeObjectU3Ek__BackingField_9), value);
	}
};

struct DateConstructor_t5098B976636DBEB0847A7217566BD868C59701D0_StaticFields
{
public:
	// System.DateTime Jint.Native.Date.DateConstructor::Epoch
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___Epoch_8;

public:
	inline static int32_t get_offset_of_Epoch_8() { return static_cast<int32_t>(offsetof(DateConstructor_t5098B976636DBEB0847A7217566BD868C59701D0_StaticFields, ___Epoch_8)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_Epoch_8() const { return ___Epoch_8; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_Epoch_8() { return &___Epoch_8; }
	inline void set_Epoch_8(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___Epoch_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATECONSTRUCTOR_T5098B976636DBEB0847A7217566BD868C59701D0_H
#ifndef DATEPROTOTYPE_T1A74663765296A18E85477D2A2F0EB7B7962D07E_H
#define DATEPROTOTYPE_T1A74663765296A18E85477D2A2F0EB7B7962D07E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Date.DatePrototype
struct  DatePrototype_t1A74663765296A18E85477D2A2F0EB7B7962D07E  : public DateInstance_tFEBBE794D12E67B7227A43F06FC2B73BA633972E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEPROTOTYPE_T1A74663765296A18E85477D2A2F0EB7B7962D07E_H
#ifndef ERRORCONSTRUCTOR_T238B22C1C5F1F2624F128AC2C0063646D0376D6E_H
#define ERRORCONSTRUCTOR_T238B22C1C5F1F2624F128AC2C0063646D0376D6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Error.ErrorConstructor
struct  ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// System.String Jint.Native.Error.ErrorConstructor::_name
	String_t* ____name_8;
	// Jint.Native.Error.ErrorPrototype Jint.Native.Error.ErrorConstructor::<PrototypeObject>k__BackingField
	ErrorPrototype_tB21E1B2F5B399BBE416BDC3CA41436C1D77F9B3B * ___U3CPrototypeObjectU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of__name_8() { return static_cast<int32_t>(offsetof(ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E, ____name_8)); }
	inline String_t* get__name_8() const { return ____name_8; }
	inline String_t** get_address_of__name_8() { return &____name_8; }
	inline void set__name_8(String_t* value)
	{
		____name_8 = value;
		Il2CppCodeGenWriteBarrier((&____name_8), value);
	}

	inline static int32_t get_offset_of_U3CPrototypeObjectU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E, ___U3CPrototypeObjectU3Ek__BackingField_9)); }
	inline ErrorPrototype_tB21E1B2F5B399BBE416BDC3CA41436C1D77F9B3B * get_U3CPrototypeObjectU3Ek__BackingField_9() const { return ___U3CPrototypeObjectU3Ek__BackingField_9; }
	inline ErrorPrototype_tB21E1B2F5B399BBE416BDC3CA41436C1D77F9B3B ** get_address_of_U3CPrototypeObjectU3Ek__BackingField_9() { return &___U3CPrototypeObjectU3Ek__BackingField_9; }
	inline void set_U3CPrototypeObjectU3Ek__BackingField_9(ErrorPrototype_tB21E1B2F5B399BBE416BDC3CA41436C1D77F9B3B * value)
	{
		___U3CPrototypeObjectU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrototypeObjectU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORCONSTRUCTOR_T238B22C1C5F1F2624F128AC2C0063646D0376D6E_H
#ifndef ERRORPROTOTYPE_TB21E1B2F5B399BBE416BDC3CA41436C1D77F9B3B_H
#define ERRORPROTOTYPE_TB21E1B2F5B399BBE416BDC3CA41436C1D77F9B3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Error.ErrorPrototype
struct  ErrorPrototype_tB21E1B2F5B399BBE416BDC3CA41436C1D77F9B3B  : public ErrorInstance_t2342D0236A711154460069F45C331A2002E1A3E9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORPROTOTYPE_TB21E1B2F5B399BBE416BDC3CA41436C1D77F9B3B_H
#ifndef BINDFUNCTIONINSTANCE_T6E4E2A76EEF5CD308EA29A5092891E3848880951_H
#define BINDFUNCTIONINSTANCE_T6E4E2A76EEF5CD308EA29A5092891E3848880951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Function.BindFunctionInstance
struct  BindFunctionInstance_t6E4E2A76EEF5CD308EA29A5092891E3848880951  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// Jint.Native.JsValue Jint.Native.Function.BindFunctionInstance::<TargetFunction>k__BackingField
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___U3CTargetFunctionU3Ek__BackingField_8;
	// Jint.Native.JsValue Jint.Native.Function.BindFunctionInstance::<BoundThis>k__BackingField
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___U3CBoundThisU3Ek__BackingField_9;
	// Jint.Native.JsValue[] Jint.Native.Function.BindFunctionInstance::<BoundArgs>k__BackingField
	JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* ___U3CBoundArgsU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CTargetFunctionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(BindFunctionInstance_t6E4E2A76EEF5CD308EA29A5092891E3848880951, ___U3CTargetFunctionU3Ek__BackingField_8)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_U3CTargetFunctionU3Ek__BackingField_8() const { return ___U3CTargetFunctionU3Ek__BackingField_8; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_U3CTargetFunctionU3Ek__BackingField_8() { return &___U3CTargetFunctionU3Ek__BackingField_8; }
	inline void set_U3CTargetFunctionU3Ek__BackingField_8(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___U3CTargetFunctionU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetFunctionU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CBoundThisU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(BindFunctionInstance_t6E4E2A76EEF5CD308EA29A5092891E3848880951, ___U3CBoundThisU3Ek__BackingField_9)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_U3CBoundThisU3Ek__BackingField_9() const { return ___U3CBoundThisU3Ek__BackingField_9; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_U3CBoundThisU3Ek__BackingField_9() { return &___U3CBoundThisU3Ek__BackingField_9; }
	inline void set_U3CBoundThisU3Ek__BackingField_9(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___U3CBoundThisU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBoundThisU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CBoundArgsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(BindFunctionInstance_t6E4E2A76EEF5CD308EA29A5092891E3848880951, ___U3CBoundArgsU3Ek__BackingField_10)); }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* get_U3CBoundArgsU3Ek__BackingField_10() const { return ___U3CBoundArgsU3Ek__BackingField_10; }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88** get_address_of_U3CBoundArgsU3Ek__BackingField_10() { return &___U3CBoundArgsU3Ek__BackingField_10; }
	inline void set_U3CBoundArgsU3Ek__BackingField_10(JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* value)
	{
		___U3CBoundArgsU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBoundArgsU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDFUNCTIONINSTANCE_T6E4E2A76EEF5CD308EA29A5092891E3848880951_H
#ifndef EVALFUNCTIONINSTANCE_T14923E8CAC9C999E8100B15A3D5D07AA4F903268_H
#define EVALFUNCTIONINSTANCE_T14923E8CAC9C999E8100B15A3D5D07AA4F903268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Function.EvalFunctionInstance
struct  EvalFunctionInstance_t14923E8CAC9C999E8100B15A3D5D07AA4F903268  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// Jint.Engine Jint.Native.Function.EvalFunctionInstance::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_8;

public:
	inline static int32_t get_offset_of__engine_8() { return static_cast<int32_t>(offsetof(EvalFunctionInstance_t14923E8CAC9C999E8100B15A3D5D07AA4F903268, ____engine_8)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_8() const { return ____engine_8; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_8() { return &____engine_8; }
	inline void set__engine_8(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_8 = value;
		Il2CppCodeGenWriteBarrier((&____engine_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVALFUNCTIONINSTANCE_T14923E8CAC9C999E8100B15A3D5D07AA4F903268_H
#ifndef FUNCTIONCONSTRUCTOR_TFE3FF73EB5E5CE01E3D44BC0F4680317010CA9F5_H
#define FUNCTIONCONSTRUCTOR_TFE3FF73EB5E5CE01E3D44BC0F4680317010CA9F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Function.FunctionConstructor
struct  FunctionConstructor_tFE3FF73EB5E5CE01E3D44BC0F4680317010CA9F5  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// Jint.Native.Function.FunctionPrototype Jint.Native.Function.FunctionConstructor::<PrototypeObject>k__BackingField
	FunctionPrototype_tF0430BB38A6440E67399DEFFA08B7D87561B4C79 * ___U3CPrototypeObjectU3Ek__BackingField_8;
	// Jint.Native.Function.FunctionInstance Jint.Native.Function.FunctionConstructor::_throwTypeError
	FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458 * ____throwTypeError_9;

public:
	inline static int32_t get_offset_of_U3CPrototypeObjectU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FunctionConstructor_tFE3FF73EB5E5CE01E3D44BC0F4680317010CA9F5, ___U3CPrototypeObjectU3Ek__BackingField_8)); }
	inline FunctionPrototype_tF0430BB38A6440E67399DEFFA08B7D87561B4C79 * get_U3CPrototypeObjectU3Ek__BackingField_8() const { return ___U3CPrototypeObjectU3Ek__BackingField_8; }
	inline FunctionPrototype_tF0430BB38A6440E67399DEFFA08B7D87561B4C79 ** get_address_of_U3CPrototypeObjectU3Ek__BackingField_8() { return &___U3CPrototypeObjectU3Ek__BackingField_8; }
	inline void set_U3CPrototypeObjectU3Ek__BackingField_8(FunctionPrototype_tF0430BB38A6440E67399DEFFA08B7D87561B4C79 * value)
	{
		___U3CPrototypeObjectU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrototypeObjectU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of__throwTypeError_9() { return static_cast<int32_t>(offsetof(FunctionConstructor_tFE3FF73EB5E5CE01E3D44BC0F4680317010CA9F5, ____throwTypeError_9)); }
	inline FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458 * get__throwTypeError_9() const { return ____throwTypeError_9; }
	inline FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458 ** get_address_of__throwTypeError_9() { return &____throwTypeError_9; }
	inline void set__throwTypeError_9(FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458 * value)
	{
		____throwTypeError_9 = value;
		Il2CppCodeGenWriteBarrier((&____throwTypeError_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONCONSTRUCTOR_TFE3FF73EB5E5CE01E3D44BC0F4680317010CA9F5_H
#ifndef FUNCTIONPROTOTYPE_TF0430BB38A6440E67399DEFFA08B7D87561B4C79_H
#define FUNCTIONPROTOTYPE_TF0430BB38A6440E67399DEFFA08B7D87561B4C79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Function.FunctionPrototype
struct  FunctionPrototype_tF0430BB38A6440E67399DEFFA08B7D87561B4C79  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONPROTOTYPE_TF0430BB38A6440E67399DEFFA08B7D87561B4C79_H
#ifndef SCRIPTFUNCTIONINSTANCE_TE4F868CC521A1980CFC94C7A0DE632ECDC1B90D1_H
#define SCRIPTFUNCTIONINSTANCE_TE4F868CC521A1980CFC94C7A0DE632ECDC1B90D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Function.ScriptFunctionInstance
struct  ScriptFunctionInstance_tE4F868CC521A1980CFC94C7A0DE632ECDC1B90D1  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// Jint.Parser.IFunctionDeclaration Jint.Native.Function.ScriptFunctionInstance::_functionDeclaration
	RuntimeObject* ____functionDeclaration_8;

public:
	inline static int32_t get_offset_of__functionDeclaration_8() { return static_cast<int32_t>(offsetof(ScriptFunctionInstance_tE4F868CC521A1980CFC94C7A0DE632ECDC1B90D1, ____functionDeclaration_8)); }
	inline RuntimeObject* get__functionDeclaration_8() const { return ____functionDeclaration_8; }
	inline RuntimeObject** get_address_of__functionDeclaration_8() { return &____functionDeclaration_8; }
	inline void set__functionDeclaration_8(RuntimeObject* value)
	{
		____functionDeclaration_8 = value;
		Il2CppCodeGenWriteBarrier((&____functionDeclaration_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTFUNCTIONINSTANCE_TE4F868CC521A1980CFC94C7A0DE632ECDC1B90D1_H
#ifndef THROWTYPEERROR_T137EE81E953352892F6929D59FCF620EB00384B3_H
#define THROWTYPEERROR_T137EE81E953352892F6929D59FCF620EB00384B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Function.ThrowTypeError
struct  ThrowTypeError_t137EE81E953352892F6929D59FCF620EB00384B3  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// Jint.Engine Jint.Native.Function.ThrowTypeError::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_8;

public:
	inline static int32_t get_offset_of__engine_8() { return static_cast<int32_t>(offsetof(ThrowTypeError_t137EE81E953352892F6929D59FCF620EB00384B3, ____engine_8)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_8() const { return ____engine_8; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_8() { return &____engine_8; }
	inline void set__engine_8(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_8 = value;
		Il2CppCodeGenWriteBarrier((&____engine_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THROWTYPEERROR_T137EE81E953352892F6929D59FCF620EB00384B3_H
#ifndef JSONPARSER_T9363D7184C8DD8C7825A568F1CE439F7D744BF2E_H
#define JSONPARSER_T9363D7184C8DD8C7825A568F1CE439F7D744BF2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Json.JsonParser
struct  JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E  : public RuntimeObject
{
public:
	// Jint.Engine Jint.Native.Json.JsonParser::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_0;
	// Jint.Native.Json.JsonParser/Extra Jint.Native.Json.JsonParser::_extra
	Extra_t44DEAC88F69744BDF6E5EA00CED22E44FC94D634 * ____extra_1;
	// System.Int32 Jint.Native.Json.JsonParser::_index
	int32_t ____index_2;
	// System.Int32 Jint.Native.Json.JsonParser::_length
	int32_t ____length_3;
	// System.Int32 Jint.Native.Json.JsonParser::_lineNumber
	int32_t ____lineNumber_4;
	// System.Int32 Jint.Native.Json.JsonParser::_lineStart
	int32_t ____lineStart_5;
	// Jint.Parser.Location Jint.Native.Json.JsonParser::_location
	Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * ____location_6;
	// Jint.Native.Json.JsonParser/Token Jint.Native.Json.JsonParser::_lookahead
	Token_t00D4946B1CE66C52F6C421D710DB429762485B01 * ____lookahead_7;
	// System.String Jint.Native.Json.JsonParser::_source
	String_t* ____source_8;
	// Jint.Parser.State Jint.Native.Json.JsonParser::_state
	State_tE5E4EE87409DF109801A5725B4A75C00E666E86A  ____state_9;

public:
	inline static int32_t get_offset_of__engine_0() { return static_cast<int32_t>(offsetof(JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E, ____engine_0)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_0() const { return ____engine_0; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_0() { return &____engine_0; }
	inline void set__engine_0(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_0 = value;
		Il2CppCodeGenWriteBarrier((&____engine_0), value);
	}

	inline static int32_t get_offset_of__extra_1() { return static_cast<int32_t>(offsetof(JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E, ____extra_1)); }
	inline Extra_t44DEAC88F69744BDF6E5EA00CED22E44FC94D634 * get__extra_1() const { return ____extra_1; }
	inline Extra_t44DEAC88F69744BDF6E5EA00CED22E44FC94D634 ** get_address_of__extra_1() { return &____extra_1; }
	inline void set__extra_1(Extra_t44DEAC88F69744BDF6E5EA00CED22E44FC94D634 * value)
	{
		____extra_1 = value;
		Il2CppCodeGenWriteBarrier((&____extra_1), value);
	}

	inline static int32_t get_offset_of__index_2() { return static_cast<int32_t>(offsetof(JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E, ____index_2)); }
	inline int32_t get__index_2() const { return ____index_2; }
	inline int32_t* get_address_of__index_2() { return &____index_2; }
	inline void set__index_2(int32_t value)
	{
		____index_2 = value;
	}

	inline static int32_t get_offset_of__length_3() { return static_cast<int32_t>(offsetof(JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E, ____length_3)); }
	inline int32_t get__length_3() const { return ____length_3; }
	inline int32_t* get_address_of__length_3() { return &____length_3; }
	inline void set__length_3(int32_t value)
	{
		____length_3 = value;
	}

	inline static int32_t get_offset_of__lineNumber_4() { return static_cast<int32_t>(offsetof(JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E, ____lineNumber_4)); }
	inline int32_t get__lineNumber_4() const { return ____lineNumber_4; }
	inline int32_t* get_address_of__lineNumber_4() { return &____lineNumber_4; }
	inline void set__lineNumber_4(int32_t value)
	{
		____lineNumber_4 = value;
	}

	inline static int32_t get_offset_of__lineStart_5() { return static_cast<int32_t>(offsetof(JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E, ____lineStart_5)); }
	inline int32_t get__lineStart_5() const { return ____lineStart_5; }
	inline int32_t* get_address_of__lineStart_5() { return &____lineStart_5; }
	inline void set__lineStart_5(int32_t value)
	{
		____lineStart_5 = value;
	}

	inline static int32_t get_offset_of__location_6() { return static_cast<int32_t>(offsetof(JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E, ____location_6)); }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * get__location_6() const { return ____location_6; }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 ** get_address_of__location_6() { return &____location_6; }
	inline void set__location_6(Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * value)
	{
		____location_6 = value;
		Il2CppCodeGenWriteBarrier((&____location_6), value);
	}

	inline static int32_t get_offset_of__lookahead_7() { return static_cast<int32_t>(offsetof(JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E, ____lookahead_7)); }
	inline Token_t00D4946B1CE66C52F6C421D710DB429762485B01 * get__lookahead_7() const { return ____lookahead_7; }
	inline Token_t00D4946B1CE66C52F6C421D710DB429762485B01 ** get_address_of__lookahead_7() { return &____lookahead_7; }
	inline void set__lookahead_7(Token_t00D4946B1CE66C52F6C421D710DB429762485B01 * value)
	{
		____lookahead_7 = value;
		Il2CppCodeGenWriteBarrier((&____lookahead_7), value);
	}

	inline static int32_t get_offset_of__source_8() { return static_cast<int32_t>(offsetof(JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E, ____source_8)); }
	inline String_t* get__source_8() const { return ____source_8; }
	inline String_t** get_address_of__source_8() { return &____source_8; }
	inline void set__source_8(String_t* value)
	{
		____source_8 = value;
		Il2CppCodeGenWriteBarrier((&____source_8), value);
	}

	inline static int32_t get_offset_of__state_9() { return static_cast<int32_t>(offsetof(JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E, ____state_9)); }
	inline State_tE5E4EE87409DF109801A5725B4A75C00E666E86A  get__state_9() const { return ____state_9; }
	inline State_tE5E4EE87409DF109801A5725B4A75C00E666E86A * get_address_of__state_9() { return &____state_9; }
	inline void set__state_9(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A  value)
	{
		____state_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPARSER_T9363D7184C8DD8C7825A568F1CE439F7D744BF2E_H
#ifndef EXTRA_T44DEAC88F69744BDF6E5EA00CED22E44FC94D634_H
#define EXTRA_T44DEAC88F69744BDF6E5EA00CED22E44FC94D634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Json.JsonParser/Extra
struct  Extra_t44DEAC88F69744BDF6E5EA00CED22E44FC94D634  : public RuntimeObject
{
public:
	// System.Nullable`1<System.Int32> Jint.Native.Json.JsonParser/Extra::Loc
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___Loc_0;
	// System.Int32[] Jint.Native.Json.JsonParser/Extra::Range
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Range_1;
	// System.String Jint.Native.Json.JsonParser/Extra::Source
	String_t* ___Source_2;
	// System.Collections.Generic.List`1<Jint.Native.Json.JsonParser/Token> Jint.Native.Json.JsonParser/Extra::Tokens
	List_1_tA5B30B21C63CE4B4670B08A30938650292A9C98C * ___Tokens_3;

public:
	inline static int32_t get_offset_of_Loc_0() { return static_cast<int32_t>(offsetof(Extra_t44DEAC88F69744BDF6E5EA00CED22E44FC94D634, ___Loc_0)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_Loc_0() const { return ___Loc_0; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_Loc_0() { return &___Loc_0; }
	inline void set_Loc_0(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___Loc_0 = value;
	}

	inline static int32_t get_offset_of_Range_1() { return static_cast<int32_t>(offsetof(Extra_t44DEAC88F69744BDF6E5EA00CED22E44FC94D634, ___Range_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Range_1() const { return ___Range_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Range_1() { return &___Range_1; }
	inline void set_Range_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Range_1 = value;
		Il2CppCodeGenWriteBarrier((&___Range_1), value);
	}

	inline static int32_t get_offset_of_Source_2() { return static_cast<int32_t>(offsetof(Extra_t44DEAC88F69744BDF6E5EA00CED22E44FC94D634, ___Source_2)); }
	inline String_t* get_Source_2() const { return ___Source_2; }
	inline String_t** get_address_of_Source_2() { return &___Source_2; }
	inline void set_Source_2(String_t* value)
	{
		___Source_2 = value;
		Il2CppCodeGenWriteBarrier((&___Source_2), value);
	}

	inline static int32_t get_offset_of_Tokens_3() { return static_cast<int32_t>(offsetof(Extra_t44DEAC88F69744BDF6E5EA00CED22E44FC94D634, ___Tokens_3)); }
	inline List_1_tA5B30B21C63CE4B4670B08A30938650292A9C98C * get_Tokens_3() const { return ___Tokens_3; }
	inline List_1_tA5B30B21C63CE4B4670B08A30938650292A9C98C ** get_address_of_Tokens_3() { return &___Tokens_3; }
	inline void set_Tokens_3(List_1_tA5B30B21C63CE4B4670B08A30938650292A9C98C * value)
	{
		___Tokens_3 = value;
		Il2CppCodeGenWriteBarrier((&___Tokens_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTRA_T44DEAC88F69744BDF6E5EA00CED22E44FC94D634_H
#ifndef TOKENS_T69C8EC6A0876F3050E6B45BB8DBBCF3A940F6D2C_H
#define TOKENS_T69C8EC6A0876F3050E6B45BB8DBBCF3A940F6D2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Json.JsonParser/Tokens
struct  Tokens_t69C8EC6A0876F3050E6B45BB8DBBCF3A940F6D2C 
{
public:
	// System.Int32 Jint.Native.Json.JsonParser/Tokens::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Tokens_t69C8EC6A0876F3050E6B45BB8DBBCF3A940F6D2C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENS_T69C8EC6A0876F3050E6B45BB8DBBCF3A940F6D2C_H
#ifndef NUMBERCONSTRUCTOR_TE5FB6BAB098D799610B0161A628216517C7ADADC_H
#define NUMBERCONSTRUCTOR_TE5FB6BAB098D799610B0161A628216517C7ADADC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Number.NumberConstructor
struct  NumberConstructor_tE5FB6BAB098D799610B0161A628216517C7ADADC  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// Jint.Native.Number.NumberPrototype Jint.Native.Number.NumberConstructor::<PrototypeObject>k__BackingField
	NumberPrototype_t9D802198E4B8D3A4C03E4C8E58B0966FB922F876 * ___U3CPrototypeObjectU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CPrototypeObjectU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(NumberConstructor_tE5FB6BAB098D799610B0161A628216517C7ADADC, ___U3CPrototypeObjectU3Ek__BackingField_8)); }
	inline NumberPrototype_t9D802198E4B8D3A4C03E4C8E58B0966FB922F876 * get_U3CPrototypeObjectU3Ek__BackingField_8() const { return ___U3CPrototypeObjectU3Ek__BackingField_8; }
	inline NumberPrototype_t9D802198E4B8D3A4C03E4C8E58B0966FB922F876 ** get_address_of_U3CPrototypeObjectU3Ek__BackingField_8() { return &___U3CPrototypeObjectU3Ek__BackingField_8; }
	inline void set_U3CPrototypeObjectU3Ek__BackingField_8(NumberPrototype_t9D802198E4B8D3A4C03E4C8E58B0966FB922F876 * value)
	{
		___U3CPrototypeObjectU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrototypeObjectU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBERCONSTRUCTOR_TE5FB6BAB098D799610B0161A628216517C7ADADC_H
#ifndef NUMBERPROTOTYPE_T9D802198E4B8D3A4C03E4C8E58B0966FB922F876_H
#define NUMBERPROTOTYPE_T9D802198E4B8D3A4C03E4C8E58B0966FB922F876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Number.NumberPrototype
struct  NumberPrototype_t9D802198E4B8D3A4C03E4C8E58B0966FB922F876  : public NumberInstance_t063D5094F4A658EB965EFEA8A1E9C087B2E65F34
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBERPROTOTYPE_T9D802198E4B8D3A4C03E4C8E58B0966FB922F876_H
#ifndef OBJECTCONSTRUCTOR_TEA6936468E07A954DA98B3D6FA258133E1992BB0_H
#define OBJECTCONSTRUCTOR_TEA6936468E07A954DA98B3D6FA258133E1992BB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Object.ObjectConstructor
struct  ObjectConstructor_tEA6936468E07A954DA98B3D6FA258133E1992BB0  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// Jint.Engine Jint.Native.Object.ObjectConstructor::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_8;
	// Jint.Native.Object.ObjectPrototype Jint.Native.Object.ObjectConstructor::<PrototypeObject>k__BackingField
	ObjectPrototype_tEC07D7C1F708C35E24A7171E19D9A411DD0FECDB * ___U3CPrototypeObjectU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of__engine_8() { return static_cast<int32_t>(offsetof(ObjectConstructor_tEA6936468E07A954DA98B3D6FA258133E1992BB0, ____engine_8)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_8() const { return ____engine_8; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_8() { return &____engine_8; }
	inline void set__engine_8(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_8 = value;
		Il2CppCodeGenWriteBarrier((&____engine_8), value);
	}

	inline static int32_t get_offset_of_U3CPrototypeObjectU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ObjectConstructor_tEA6936468E07A954DA98B3D6FA258133E1992BB0, ___U3CPrototypeObjectU3Ek__BackingField_9)); }
	inline ObjectPrototype_tEC07D7C1F708C35E24A7171E19D9A411DD0FECDB * get_U3CPrototypeObjectU3Ek__BackingField_9() const { return ___U3CPrototypeObjectU3Ek__BackingField_9; }
	inline ObjectPrototype_tEC07D7C1F708C35E24A7171E19D9A411DD0FECDB ** get_address_of_U3CPrototypeObjectU3Ek__BackingField_9() { return &___U3CPrototypeObjectU3Ek__BackingField_9; }
	inline void set_U3CPrototypeObjectU3Ek__BackingField_9(ObjectPrototype_tEC07D7C1F708C35E24A7171E19D9A411DD0FECDB * value)
	{
		___U3CPrototypeObjectU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrototypeObjectU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCONSTRUCTOR_TEA6936468E07A954DA98B3D6FA258133E1992BB0_H
#ifndef REGEXPCONSTRUCTOR_T959535B1C26428C9B2A92C0B914883DE4BEB7508_H
#define REGEXPCONSTRUCTOR_T959535B1C26428C9B2A92C0B914883DE4BEB7508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.RegExp.RegExpConstructor
struct  RegExpConstructor_t959535B1C26428C9B2A92C0B914883DE4BEB7508  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// Jint.Native.RegExp.RegExpPrototype Jint.Native.RegExp.RegExpConstructor::<PrototypeObject>k__BackingField
	RegExpPrototype_t766F1B8425C5B57B62666EDDBC896F51AC3FFA1D * ___U3CPrototypeObjectU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CPrototypeObjectU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RegExpConstructor_t959535B1C26428C9B2A92C0B914883DE4BEB7508, ___U3CPrototypeObjectU3Ek__BackingField_8)); }
	inline RegExpPrototype_t766F1B8425C5B57B62666EDDBC896F51AC3FFA1D * get_U3CPrototypeObjectU3Ek__BackingField_8() const { return ___U3CPrototypeObjectU3Ek__BackingField_8; }
	inline RegExpPrototype_t766F1B8425C5B57B62666EDDBC896F51AC3FFA1D ** get_address_of_U3CPrototypeObjectU3Ek__BackingField_8() { return &___U3CPrototypeObjectU3Ek__BackingField_8; }
	inline void set_U3CPrototypeObjectU3Ek__BackingField_8(RegExpPrototype_t766F1B8425C5B57B62666EDDBC896F51AC3FFA1D * value)
	{
		___U3CPrototypeObjectU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrototypeObjectU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXPCONSTRUCTOR_T959535B1C26428C9B2A92C0B914883DE4BEB7508_H
#ifndef REGEXPPROTOTYPE_T766F1B8425C5B57B62666EDDBC896F51AC3FFA1D_H
#define REGEXPPROTOTYPE_T766F1B8425C5B57B62666EDDBC896F51AC3FFA1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.RegExp.RegExpPrototype
struct  RegExpPrototype_t766F1B8425C5B57B62666EDDBC896F51AC3FFA1D  : public RegExpInstance_tDC893C9F06BFA6B6D0B496F594731135534AE3F3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXPPROTOTYPE_T766F1B8425C5B57B62666EDDBC896F51AC3FFA1D_H
#ifndef STRINGCONSTRUCTOR_TE617B827C95B0979B5C5D21C8F40D48D2D1C0215_H
#define STRINGCONSTRUCTOR_TE617B827C95B0979B5C5D21C8F40D48D2D1C0215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.String.StringConstructor
struct  StringConstructor_tE617B827C95B0979B5C5D21C8F40D48D2D1C0215  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// Jint.Native.String.StringPrototype Jint.Native.String.StringConstructor::<PrototypeObject>k__BackingField
	StringPrototype_t3AE5DF2E522E2F4170AEDACC9CF807F5538FCF2D * ___U3CPrototypeObjectU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CPrototypeObjectU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(StringConstructor_tE617B827C95B0979B5C5D21C8F40D48D2D1C0215, ___U3CPrototypeObjectU3Ek__BackingField_8)); }
	inline StringPrototype_t3AE5DF2E522E2F4170AEDACC9CF807F5538FCF2D * get_U3CPrototypeObjectU3Ek__BackingField_8() const { return ___U3CPrototypeObjectU3Ek__BackingField_8; }
	inline StringPrototype_t3AE5DF2E522E2F4170AEDACC9CF807F5538FCF2D ** get_address_of_U3CPrototypeObjectU3Ek__BackingField_8() { return &___U3CPrototypeObjectU3Ek__BackingField_8; }
	inline void set_U3CPrototypeObjectU3Ek__BackingField_8(StringPrototype_t3AE5DF2E522E2F4170AEDACC9CF807F5538FCF2D * value)
	{
		___U3CPrototypeObjectU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrototypeObjectU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGCONSTRUCTOR_TE617B827C95B0979B5C5D21C8F40D48D2D1C0215_H
#ifndef STRINGPROTOTYPE_T3AE5DF2E522E2F4170AEDACC9CF807F5538FCF2D_H
#define STRINGPROTOTYPE_T3AE5DF2E522E2F4170AEDACC9CF807F5538FCF2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.String.StringPrototype
struct  StringPrototype_t3AE5DF2E522E2F4170AEDACC9CF807F5538FCF2D  : public StringInstance_tD8ED774792033B8F196F6ABB557DFDBADE0B22B7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGPROTOTYPE_T3AE5DF2E522E2F4170AEDACC9CF807F5538FCF2D_H
#ifndef SYNTAXNODES_TE429806CC3C5D818D51936071CF565AA6B61D4C6_H
#define SYNTAXNODES_TE429806CC3C5D818D51936071CF565AA6B61D4C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.SyntaxNodes
struct  SyntaxNodes_tE429806CC3C5D818D51936071CF565AA6B61D4C6 
{
public:
	// System.Int32 Jint.Parser.Ast.SyntaxNodes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SyntaxNodes_tE429806CC3C5D818D51936071CF565AA6B61D4C6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNTAXNODES_TE429806CC3C5D818D51936071CF565AA6B61D4C6_H
#ifndef TYPES_TE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A_H
#define TYPES_TE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Types
struct  Types_tE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A 
{
public:
	// System.Int32 Jint.Runtime.Types::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Types_tE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPES_TE8BD283A24EFB4AC7F8FB7118741889F04CEEE3A_H
#ifndef COMPRESSIONTYPE_TFD4585547C8DA57E6AEE2FCC7CA7BD0471795803_H
#define COMPRESSIONTYPE_TFD4585547C8DA57E6AEE2FCC7CA7BD0471795803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Compression.CompressionType
struct  CompressionType_tFD4585547C8DA57E6AEE2FCC7CA7BD0471795803 
{
public:
	// System.Int32 MHLab.PATCH.Compression.CompressionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompressionType_tFD4585547C8DA57E6AEE2FCC7CA7BD0471795803, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONTYPE_TFD4585547C8DA57E6AEE2FCC7CA7BD0471795803_H
#ifndef JSVALUE_T822B6795737DB1350A3D0DFAB03CFD4371350D19_H
#define JSVALUE_T822B6795737DB1350A3D0DFAB03CFD4371350D19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.JsValue
struct  JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19  : public RuntimeObject
{
public:
	// System.Double Jint.Native.JsValue::_double
	double ____double_4;
	// System.Object Jint.Native.JsValue::_object
	RuntimeObject * ____object_5;
	// Jint.Runtime.Types Jint.Native.JsValue::_type
	int32_t ____type_6;

public:
	inline static int32_t get_offset_of__double_4() { return static_cast<int32_t>(offsetof(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19, ____double_4)); }
	inline double get__double_4() const { return ____double_4; }
	inline double* get_address_of__double_4() { return &____double_4; }
	inline void set__double_4(double value)
	{
		____double_4 = value;
	}

	inline static int32_t get_offset_of__object_5() { return static_cast<int32_t>(offsetof(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19, ____object_5)); }
	inline RuntimeObject * get__object_5() const { return ____object_5; }
	inline RuntimeObject ** get_address_of__object_5() { return &____object_5; }
	inline void set__object_5(RuntimeObject * value)
	{
		____object_5 = value;
		Il2CppCodeGenWriteBarrier((&____object_5), value);
	}

	inline static int32_t get_offset_of__type_6() { return static_cast<int32_t>(offsetof(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19, ____type_6)); }
	inline int32_t get__type_6() const { return ____type_6; }
	inline int32_t* get_address_of__type_6() { return &____type_6; }
	inline void set__type_6(int32_t value)
	{
		____type_6 = value;
	}
};

struct JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19_StaticFields
{
public:
	// Jint.Native.JsValue Jint.Native.JsValue::Undefined
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___Undefined_0;
	// Jint.Native.JsValue Jint.Native.JsValue::Null
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___Null_1;
	// Jint.Native.JsValue Jint.Native.JsValue::False
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___False_2;
	// Jint.Native.JsValue Jint.Native.JsValue::True
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___True_3;

public:
	inline static int32_t get_offset_of_Undefined_0() { return static_cast<int32_t>(offsetof(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19_StaticFields, ___Undefined_0)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_Undefined_0() const { return ___Undefined_0; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_Undefined_0() { return &___Undefined_0; }
	inline void set_Undefined_0(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___Undefined_0 = value;
		Il2CppCodeGenWriteBarrier((&___Undefined_0), value);
	}

	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19_StaticFields, ___Null_1)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_Null_1() const { return ___Null_1; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}

	inline static int32_t get_offset_of_False_2() { return static_cast<int32_t>(offsetof(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19_StaticFields, ___False_2)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_False_2() const { return ___False_2; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_False_2() { return &___False_2; }
	inline void set_False_2(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___False_2 = value;
		Il2CppCodeGenWriteBarrier((&___False_2), value);
	}

	inline static int32_t get_offset_of_True_3() { return static_cast<int32_t>(offsetof(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19_StaticFields, ___True_3)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_True_3() const { return ___True_3; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_True_3() { return &___True_3; }
	inline void set_True_3(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___True_3 = value;
		Il2CppCodeGenWriteBarrier((&___True_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSVALUE_T822B6795737DB1350A3D0DFAB03CFD4371350D19_H
#ifndef TOKEN_T00D4946B1CE66C52F6C421D710DB429762485B01_H
#define TOKEN_T00D4946B1CE66C52F6C421D710DB429762485B01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Json.JsonParser/Token
struct  Token_t00D4946B1CE66C52F6C421D710DB429762485B01  : public RuntimeObject
{
public:
	// Jint.Native.Json.JsonParser/Tokens Jint.Native.Json.JsonParser/Token::Type
	int32_t ___Type_0;
	// System.Object Jint.Native.Json.JsonParser/Token::Value
	RuntimeObject * ___Value_1;
	// System.Int32[] Jint.Native.Json.JsonParser/Token::Range
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Range_2;
	// System.Nullable`1<System.Int32> Jint.Native.Json.JsonParser/Token::LineNumber
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___LineNumber_3;
	// System.Int32 Jint.Native.Json.JsonParser/Token::LineStart
	int32_t ___LineStart_4;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(Token_t00D4946B1CE66C52F6C421D710DB429762485B01, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(Token_t00D4946B1CE66C52F6C421D710DB429762485B01, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}

	inline static int32_t get_offset_of_Range_2() { return static_cast<int32_t>(offsetof(Token_t00D4946B1CE66C52F6C421D710DB429762485B01, ___Range_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Range_2() const { return ___Range_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Range_2() { return &___Range_2; }
	inline void set_Range_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Range_2 = value;
		Il2CppCodeGenWriteBarrier((&___Range_2), value);
	}

	inline static int32_t get_offset_of_LineNumber_3() { return static_cast<int32_t>(offsetof(Token_t00D4946B1CE66C52F6C421D710DB429762485B01, ___LineNumber_3)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_LineNumber_3() const { return ___LineNumber_3; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_LineNumber_3() { return &___LineNumber_3; }
	inline void set_LineNumber_3(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___LineNumber_3 = value;
	}

	inline static int32_t get_offset_of_LineStart_4() { return static_cast<int32_t>(offsetof(Token_t00D4946B1CE66C52F6C421D710DB429762485B01, ___LineStart_4)); }
	inline int32_t get_LineStart_4() const { return ___LineStart_4; }
	inline int32_t* get_address_of_LineStart_4() { return &___LineStart_4; }
	inline void set_LineStart_4(int32_t value)
	{
		___LineStart_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T00D4946B1CE66C52F6C421D710DB429762485B01_H
#ifndef SYNTAXNODE_T7493862A6C53ED276ECECF7246A8DFE2851C880B_H
#define SYNTAXNODE_T7493862A6C53ED276ECECF7246A8DFE2851C880B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.SyntaxNode
struct  SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B  : public RuntimeObject
{
public:
	// Jint.Parser.Ast.SyntaxNodes Jint.Parser.Ast.SyntaxNode::Type
	int32_t ___Type_0;
	// System.Int32[] Jint.Parser.Ast.SyntaxNode::Range
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Range_1;
	// Jint.Parser.Location Jint.Parser.Ast.SyntaxNode::Location
	Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * ___Location_2;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Range_1() { return static_cast<int32_t>(offsetof(SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B, ___Range_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Range_1() const { return ___Range_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Range_1() { return &___Range_1; }
	inline void set_Range_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Range_1 = value;
		Il2CppCodeGenWriteBarrier((&___Range_1), value);
	}

	inline static int32_t get_offset_of_Location_2() { return static_cast<int32_t>(offsetof(SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B, ___Location_2)); }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * get_Location_2() const { return ___Location_2; }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 ** get_address_of_Location_2() { return &___Location_2; }
	inline void set_Location_2(Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * value)
	{
		___Location_2 = value;
		Il2CppCodeGenWriteBarrier((&___Location_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNTAXNODE_T7493862A6C53ED276ECECF7246A8DFE2851C880B_H
#ifndef PATCH_TEA6D72DC3DD84AE96CFC0668FE1A7467F20A0499_H
#define PATCH_TEA6D72DC3DD84AE96CFC0668FE1A7467F20A0499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Patch
struct  Patch_tEA6D72DC3DD84AE96CFC0668FE1A7467F20A0499  : public RuntimeObject
{
public:
	// MHLab.PATCH.Version MHLab.PATCH.Patch::From
	Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976 * ___From_0;
	// MHLab.PATCH.Version MHLab.PATCH.Patch::To
	Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976 * ___To_1;
	// System.String MHLab.PATCH.Patch::Hash
	String_t* ___Hash_2;
	// MHLab.PATCH.Compression.CompressionType MHLab.PATCH.Patch::Type
	int32_t ___Type_3;

public:
	inline static int32_t get_offset_of_From_0() { return static_cast<int32_t>(offsetof(Patch_tEA6D72DC3DD84AE96CFC0668FE1A7467F20A0499, ___From_0)); }
	inline Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976 * get_From_0() const { return ___From_0; }
	inline Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976 ** get_address_of_From_0() { return &___From_0; }
	inline void set_From_0(Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976 * value)
	{
		___From_0 = value;
		Il2CppCodeGenWriteBarrier((&___From_0), value);
	}

	inline static int32_t get_offset_of_To_1() { return static_cast<int32_t>(offsetof(Patch_tEA6D72DC3DD84AE96CFC0668FE1A7467F20A0499, ___To_1)); }
	inline Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976 * get_To_1() const { return ___To_1; }
	inline Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976 ** get_address_of_To_1() { return &___To_1; }
	inline void set_To_1(Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976 * value)
	{
		___To_1 = value;
		Il2CppCodeGenWriteBarrier((&___To_1), value);
	}

	inline static int32_t get_offset_of_Hash_2() { return static_cast<int32_t>(offsetof(Patch_tEA6D72DC3DD84AE96CFC0668FE1A7467F20A0499, ___Hash_2)); }
	inline String_t* get_Hash_2() const { return ___Hash_2; }
	inline String_t** get_address_of_Hash_2() { return &___Hash_2; }
	inline void set_Hash_2(String_t* value)
	{
		___Hash_2 = value;
		Il2CppCodeGenWriteBarrier((&___Hash_2), value);
	}

	inline static int32_t get_offset_of_Type_3() { return static_cast<int32_t>(offsetof(Patch_tEA6D72DC3DD84AE96CFC0668FE1A7467F20A0499, ___Type_3)); }
	inline int32_t get_Type_3() const { return ___Type_3; }
	inline int32_t* get_address_of_Type_3() { return &___Type_3; }
	inline void set_Type_3(int32_t value)
	{
		___Type_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATCH_TEA6D72DC3DD84AE96CFC0668FE1A7467F20A0499_H
#ifndef EXPRESSION_TAC599936AD6D021EA6A3A695C803975140919ADB_H
#define EXPRESSION_TAC599936AD6D021EA6A3A695C803975140919ADB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.Expression
struct  Expression_tAC599936AD6D021EA6A3A695C803975140919ADB  : public SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSION_TAC599936AD6D021EA6A3A695C803975140919ADB_H
#ifndef STATEMENT_TDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0_H
#define STATEMENT_TDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.Statement
struct  Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0  : public SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B
{
public:
	// System.String Jint.Parser.Ast.Statement::LabelSet
	String_t* ___LabelSet_3;

public:
	inline static int32_t get_offset_of_LabelSet_3() { return static_cast<int32_t>(offsetof(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0, ___LabelSet_3)); }
	inline String_t* get_LabelSet_3() const { return ___LabelSet_3; }
	inline String_t** get_address_of_LabelSet_3() { return &___LabelSet_3; }
	inline void set_LabelSet_3(String_t* value)
	{
		___LabelSet_3 = value;
		Il2CppCodeGenWriteBarrier((&___LabelSet_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMENT_TDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0_H
#ifndef VARIABLEDECLARATOR_T303A327DDF511BB7C5455E20BD722F71861B3C7B_H
#define VARIABLEDECLARATOR_T303A327DDF511BB7C5455E20BD722F71861B3C7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.VariableDeclarator
struct  VariableDeclarator_t303A327DDF511BB7C5455E20BD722F71861B3C7B  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// Jint.Parser.Ast.Identifier Jint.Parser.Ast.VariableDeclarator::Id
	Identifier_t50FB8837E02860B417C98D78294E352C617900EB * ___Id_3;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.VariableDeclarator::Init
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Init_4;

public:
	inline static int32_t get_offset_of_Id_3() { return static_cast<int32_t>(offsetof(VariableDeclarator_t303A327DDF511BB7C5455E20BD722F71861B3C7B, ___Id_3)); }
	inline Identifier_t50FB8837E02860B417C98D78294E352C617900EB * get_Id_3() const { return ___Id_3; }
	inline Identifier_t50FB8837E02860B417C98D78294E352C617900EB ** get_address_of_Id_3() { return &___Id_3; }
	inline void set_Id_3(Identifier_t50FB8837E02860B417C98D78294E352C617900EB * value)
	{
		___Id_3 = value;
		Il2CppCodeGenWriteBarrier((&___Id_3), value);
	}

	inline static int32_t get_offset_of_Init_4() { return static_cast<int32_t>(offsetof(VariableDeclarator_t303A327DDF511BB7C5455E20BD722F71861B3C7B, ___Init_4)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Init_4() const { return ___Init_4; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Init_4() { return &___Init_4; }
	inline void set_Init_4(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Init_4 = value;
		Il2CppCodeGenWriteBarrier((&___Init_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLEDECLARATOR_T303A327DDF511BB7C5455E20BD722F71861B3C7B_H
#ifndef WHILESTATEMENT_T883CA973F4D5A4150031BD97CEDCDCF68B8622B4_H
#define WHILESTATEMENT_T883CA973F4D5A4150031BD97CEDCDCF68B8622B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.WhileStatement
struct  WhileStatement_t883CA973F4D5A4150031BD97CEDCDCF68B8622B4  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.WhileStatement::Test
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Test_4;
	// Jint.Parser.Ast.Statement Jint.Parser.Ast.WhileStatement::Body
	Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * ___Body_5;

public:
	inline static int32_t get_offset_of_Test_4() { return static_cast<int32_t>(offsetof(WhileStatement_t883CA973F4D5A4150031BD97CEDCDCF68B8622B4, ___Test_4)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Test_4() const { return ___Test_4; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Test_4() { return &___Test_4; }
	inline void set_Test_4(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Test_4 = value;
		Il2CppCodeGenWriteBarrier((&___Test_4), value);
	}

	inline static int32_t get_offset_of_Body_5() { return static_cast<int32_t>(offsetof(WhileStatement_t883CA973F4D5A4150031BD97CEDCDCF68B8622B4, ___Body_5)); }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * get_Body_5() const { return ___Body_5; }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 ** get_address_of_Body_5() { return &___Body_5; }
	inline void set_Body_5(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * value)
	{
		___Body_5 = value;
		Il2CppCodeGenWriteBarrier((&___Body_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHILESTATEMENT_T883CA973F4D5A4150031BD97CEDCDCF68B8622B4_H
#ifndef WITHSTATEMENT_T99F2439D7420AD32EC2F9B9ED3E924C639202A7C_H
#define WITHSTATEMENT_T99F2439D7420AD32EC2F9B9ED3E924C639202A7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.WithStatement
struct  WithStatement_t99F2439D7420AD32EC2F9B9ED3E924C639202A7C  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.WithStatement::Object
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Object_4;
	// Jint.Parser.Ast.Statement Jint.Parser.Ast.WithStatement::Body
	Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * ___Body_5;

public:
	inline static int32_t get_offset_of_Object_4() { return static_cast<int32_t>(offsetof(WithStatement_t99F2439D7420AD32EC2F9B9ED3E924C639202A7C, ___Object_4)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Object_4() const { return ___Object_4; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Object_4() { return &___Object_4; }
	inline void set_Object_4(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Object_4 = value;
		Il2CppCodeGenWriteBarrier((&___Object_4), value);
	}

	inline static int32_t get_offset_of_Body_5() { return static_cast<int32_t>(offsetof(WithStatement_t99F2439D7420AD32EC2F9B9ED3E924C639202A7C, ___Body_5)); }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * get_Body_5() const { return ___Body_5; }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 ** get_address_of_Body_5() { return &___Body_5; }
	inline void set_Body_5(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * value)
	{
		___Body_5 = value;
		Il2CppCodeGenWriteBarrier((&___Body_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WITHSTATEMENT_T99F2439D7420AD32EC2F9B9ED3E924C639202A7C_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5700 = { sizeof (VariableDeclarator_t303A327DDF511BB7C5455E20BD722F71861B3C7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5700[2] = 
{
	VariableDeclarator_t303A327DDF511BB7C5455E20BD722F71861B3C7B::get_offset_of_Id_3(),
	VariableDeclarator_t303A327DDF511BB7C5455E20BD722F71861B3C7B::get_offset_of_Init_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5701 = { sizeof (WhileStatement_t883CA973F4D5A4150031BD97CEDCDCF68B8622B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5701[2] = 
{
	WhileStatement_t883CA973F4D5A4150031BD97CEDCDCF68B8622B4::get_offset_of_Test_4(),
	WhileStatement_t883CA973F4D5A4150031BD97CEDCDCF68B8622B4::get_offset_of_Body_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5702 = { sizeof (WithStatement_t99F2439D7420AD32EC2F9B9ED3E924C639202A7C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5702[2] = 
{
	WithStatement_t99F2439D7420AD32EC2F9B9ED3E924C639202A7C::get_offset_of_Object_4(),
	WithStatement_t99F2439D7420AD32EC2F9B9ED3E924C639202A7C::get_offset_of_Body_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5703 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5704 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5705 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5706 = { sizeof (JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19), -1, sizeof(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5706[7] = 
{
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19_StaticFields::get_offset_of_Undefined_0(),
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19_StaticFields::get_offset_of_Null_1(),
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19_StaticFields::get_offset_of_False_2(),
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19_StaticFields::get_offset_of_True_3(),
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19::get_offset_of__double_4(),
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19::get_offset_of__object_5(),
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19::get_offset_of__type_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5707 = { sizeof (JsValueDebugView_tD22862785DEE056971356FB2EEF6B23328C755F7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5708 = { sizeof (U3CU3Ec__DisplayClass35_0_t2B171BBBBC38F00CD1CF51AE8D112FE05F150826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5708[1] = 
{
	U3CU3Ec__DisplayClass35_0_t2B171BBBBC38F00CD1CF51AE8D112FE05F150826::get_offset_of_engine_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5709 = { sizeof (U3CU3Ec_tE628833A5B75D69477BA5C23673B22B219921407), -1, sizeof(U3CU3Ec_tE628833A5B75D69477BA5C23673B22B219921407_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5709[2] = 
{
	U3CU3Ec_tE628833A5B75D69477BA5C23673B22B219921407_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tE628833A5B75D69477BA5C23673B22B219921407_StaticFields::get_offset_of_U3CU3E9__35_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5710 = { sizeof (Null_tAED0901ECC3A323E8142DAD74DFB342D0E3DDB74), -1, sizeof(Null_tAED0901ECC3A323E8142DAD74DFB342D0E3DDB74_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5710[2] = 
{
	Null_tAED0901ECC3A323E8142DAD74DFB342D0E3DDB74_StaticFields::get_offset_of_Instance_0(),
	Null_tAED0901ECC3A323E8142DAD74DFB342D0E3DDB74_StaticFields::get_offset_of_Text_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5711 = { sizeof (Undefined_t8BB3FFB7F5CAE6F9092ACE5257ABCC60AC3AD7C4), -1, sizeof(Undefined_t8BB3FFB7F5CAE6F9092ACE5257ABCC60AC3AD7C4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5711[2] = 
{
	Undefined_t8BB3FFB7F5CAE6F9092ACE5257ABCC60AC3AD7C4_StaticFields::get_offset_of_Instance_0(),
	Undefined_t8BB3FFB7F5CAE6F9092ACE5257ABCC60AC3AD7C4_StaticFields::get_offset_of_Text_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5712 = { sizeof (StringConstructor_tE617B827C95B0979B5C5D21C8F40D48D2D1C0215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5712[1] = 
{
	StringConstructor_tE617B827C95B0979B5C5D21C8F40D48D2D1C0215::get_offset_of_U3CPrototypeObjectU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5713 = { sizeof (StringInstance_tD8ED774792033B8F196F6ABB557DFDBADE0B22B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5713[1] = 
{
	StringInstance_tD8ED774792033B8F196F6ABB557DFDBADE0B22B7::get_offset_of_U3CPrimitiveValueU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5714 = { sizeof (StringPrototype_t3AE5DF2E522E2F4170AEDACC9CF807F5538FCF2D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5715 = { sizeof (U3CU3Ec__DisplayClass21_0_t4F3190029952FF4ABAE057EE3C3B704A38430D79), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5715[3] = 
{
	U3CU3Ec__DisplayClass21_0_t4F3190029952FF4ABAE057EE3C3B704A38430D79::get_offset_of_replaceValue_0(),
	U3CU3Ec__DisplayClass21_0_t4F3190029952FF4ABAE057EE3C3B704A38430D79::get_offset_of_thisString_1(),
	U3CU3Ec__DisplayClass21_0_t4F3190029952FF4ABAE057EE3C3B704A38430D79::get_offset_of_replaceFunction_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5716 = { sizeof (RegExpConstructor_t959535B1C26428C9B2A92C0B914883DE4BEB7508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5716[1] = 
{
	RegExpConstructor_t959535B1C26428C9B2A92C0B914883DE4BEB7508::get_offset_of_U3CPrototypeObjectU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5717 = { sizeof (RegExpInstance_tDC893C9F06BFA6B6D0B496F594731135534AE3F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5717[6] = 
{
	RegExpInstance_tDC893C9F06BFA6B6D0B496F594731135534AE3F3::get_offset_of_U3CValueU3Ek__BackingField_4(),
	RegExpInstance_tDC893C9F06BFA6B6D0B496F594731135534AE3F3::get_offset_of_U3CSourceU3Ek__BackingField_5(),
	RegExpInstance_tDC893C9F06BFA6B6D0B496F594731135534AE3F3::get_offset_of_U3CFlagsU3Ek__BackingField_6(),
	RegExpInstance_tDC893C9F06BFA6B6D0B496F594731135534AE3F3::get_offset_of_U3CGlobalU3Ek__BackingField_7(),
	RegExpInstance_tDC893C9F06BFA6B6D0B496F594731135534AE3F3::get_offset_of_U3CIgnoreCaseU3Ek__BackingField_8(),
	RegExpInstance_tDC893C9F06BFA6B6D0B496F594731135534AE3F3::get_offset_of_U3CMultilineU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5718 = { sizeof (RegExpPrototype_t766F1B8425C5B57B62666EDDBC896F51AC3FFA1D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5719 = { sizeof (ObjectConstructor_tEA6936468E07A954DA98B3D6FA258133E1992BB0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5719[2] = 
{
	ObjectConstructor_tEA6936468E07A954DA98B3D6FA258133E1992BB0::get_offset_of__engine_8(),
	ObjectConstructor_tEA6936468E07A954DA98B3D6FA258133E1992BB0::get_offset_of_U3CPrototypeObjectU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5720 = { sizeof (U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678), -1, sizeof(U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5720[4] = 
{
	U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678_StaticFields::get_offset_of_U3CU3E9__17_0_1(),
	U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678_StaticFields::get_offset_of_U3CU3E9__20_0_2(),
	U3CU3Ec_tC67FAF868A2F654B8A82885758BA467818CFA678_StaticFields::get_offset_of_U3CU3E9__22_0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5721 = { sizeof (ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5721[4] = 
{
	ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900::get_offset_of_U3CEngineU3Ek__BackingField_0(),
	ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900::get_offset_of_U3CPropertiesU3Ek__BackingField_1(),
	ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900::get_offset_of_U3CPrototypeU3Ek__BackingField_2(),
	ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900::get_offset_of_U3CExtensibleU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5722 = { sizeof (ObjectPrototype_tEC07D7C1F708C35E24A7171E19D9A411DD0FECDB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5723 = { sizeof (NumberConstructor_tE5FB6BAB098D799610B0161A628216517C7ADADC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5723[1] = 
{
	NumberConstructor_tE5FB6BAB098D799610B0161A628216517C7ADADC::get_offset_of_U3CPrototypeObjectU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5724 = { sizeof (NumberInstance_t063D5094F4A658EB965EFEA8A1E9C087B2E65F34), -1, sizeof(NumberInstance_t063D5094F4A658EB965EFEA8A1E9C087B2E65F34_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5724[2] = 
{
	NumberInstance_t063D5094F4A658EB965EFEA8A1E9C087B2E65F34_StaticFields::get_offset_of_NegativeZeroBits_4(),
	NumberInstance_t063D5094F4A658EB965EFEA8A1E9C087B2E65F34::get_offset_of_U3CPrimitiveValueU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5725 = { sizeof (NumberPrototype_t9D802198E4B8D3A4C03E4C8E58B0966FB922F876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5726 = { sizeof (CachedPowers_tABF022BFEFBCC9E0A2CEB7B2C67E994B63BF9380), -1, sizeof(CachedPowers_tABF022BFEFBCC9E0A2CEB7B2C67E994B63BF9380_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5726[1] = 
{
	CachedPowers_tABF022BFEFBCC9E0A2CEB7B2C67E994B63BF9380_StaticFields::get_offset_of_CACHED_POWERS_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5727 = { sizeof (CachedPower_t803C239948A2313EBA931E7E542083187EC419CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5727[3] = 
{
	CachedPower_t803C239948A2313EBA931E7E542083187EC419CB::get_offset_of_Significand_0(),
	CachedPower_t803C239948A2313EBA931E7E542083187EC419CB::get_offset_of_BinaryExponent_1(),
	CachedPower_t803C239948A2313EBA931E7E542083187EC419CB::get_offset_of_DecimalExponent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5728 = { sizeof (DiyFp_t9FAD9346E18B6744394689F28AE5E28202C3A9D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5728[2] = 
{
	DiyFp_t9FAD9346E18B6744394689F28AE5E28202C3A9D1::get_offset_of_U3CFU3Ek__BackingField_0(),
	DiyFp_t9FAD9346E18B6744394689F28AE5E28202C3A9D1::get_offset_of_U3CEU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5729 = { sizeof (DoubleHelper_t1B21D4D69B841266281A3005FAE3BDF893758E1A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5730 = { sizeof (FastDtoa_t72EBB000B83D5C0C423DC8D997542F5AE0166C8E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5731 = { sizeof (FastDtoaBuilder_t59B39461C1DC0951965DC8C43CD9A729911C67AE), -1, sizeof(FastDtoaBuilder_t59B39461C1DC0951965DC8C43CD9A729911C67AE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5731[5] = 
{
	FastDtoaBuilder_t59B39461C1DC0951965DC8C43CD9A729911C67AE::get_offset_of__chars_0(),
	FastDtoaBuilder_t59B39461C1DC0951965DC8C43CD9A729911C67AE::get_offset_of_End_1(),
	FastDtoaBuilder_t59B39461C1DC0951965DC8C43CD9A729911C67AE::get_offset_of_Point_2(),
	FastDtoaBuilder_t59B39461C1DC0951965DC8C43CD9A729911C67AE::get_offset_of__formatted_3(),
	FastDtoaBuilder_t59B39461C1DC0951965DC8C43CD9A729911C67AE_StaticFields::get_offset_of_Digits_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5732 = { sizeof (NumberExtensions_tC6FC0C487A2F745894DFAED9F9213AF4A16E3B5F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5733 = { sizeof (MathInstance_t3512482B63B6D54FC3F9105785754D409F54F4C0), -1, sizeof(MathInstance_t3512482B63B6D54FC3F9105785754D409F54F4C0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5733[1] = 
{
	MathInstance_t3512482B63B6D54FC3F9105785754D409F54F4C0_StaticFields::get_offset_of__random_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5734 = { sizeof (JsonInstance_tAAC3C2498512BF0ADC8391178660107A034B91AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5734[1] = 
{
	JsonInstance_tAAC3C2498512BF0ADC8391178660107A034B91AE::get_offset_of__engine_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5735 = { sizeof (JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5735[10] = 
{
	JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E::get_offset_of__engine_0(),
	JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E::get_offset_of__extra_1(),
	JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E::get_offset_of__index_2(),
	JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E::get_offset_of__length_3(),
	JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E::get_offset_of__lineNumber_4(),
	JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E::get_offset_of__lineStart_5(),
	JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E::get_offset_of__location_6(),
	JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E::get_offset_of__lookahead_7(),
	JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E::get_offset_of__source_8(),
	JsonParser_t9363D7184C8DD8C7825A568F1CE439F7D744BF2E::get_offset_of__state_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5736 = { sizeof (Extra_t44DEAC88F69744BDF6E5EA00CED22E44FC94D634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5736[4] = 
{
	Extra_t44DEAC88F69744BDF6E5EA00CED22E44FC94D634::get_offset_of_Loc_0(),
	Extra_t44DEAC88F69744BDF6E5EA00CED22E44FC94D634::get_offset_of_Range_1(),
	Extra_t44DEAC88F69744BDF6E5EA00CED22E44FC94D634::get_offset_of_Source_2(),
	Extra_t44DEAC88F69744BDF6E5EA00CED22E44FC94D634::get_offset_of_Tokens_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5737 = { sizeof (Tokens_t69C8EC6A0876F3050E6B45BB8DBBCF3A940F6D2C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5737[7] = 
{
	Tokens_t69C8EC6A0876F3050E6B45BB8DBBCF3A940F6D2C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5738 = { sizeof (Token_t00D4946B1CE66C52F6C421D710DB429762485B01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5738[5] = 
{
	Token_t00D4946B1CE66C52F6C421D710DB429762485B01::get_offset_of_Type_0(),
	Token_t00D4946B1CE66C52F6C421D710DB429762485B01::get_offset_of_Value_1(),
	Token_t00D4946B1CE66C52F6C421D710DB429762485B01::get_offset_of_Range_2(),
	Token_t00D4946B1CE66C52F6C421D710DB429762485B01::get_offset_of_LineNumber_3(),
	Token_t00D4946B1CE66C52F6C421D710DB429762485B01::get_offset_of_LineStart_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5739 = { sizeof (JsonSerializer_tD12BA78AD82034231EB2236DE3A1F627CAF98ABC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5739[6] = 
{
	JsonSerializer_tD12BA78AD82034231EB2236DE3A1F627CAF98ABC::get_offset_of__engine_0(),
	JsonSerializer_tD12BA78AD82034231EB2236DE3A1F627CAF98ABC::get_offset_of__stack_1(),
	JsonSerializer_tD12BA78AD82034231EB2236DE3A1F627CAF98ABC::get_offset_of__indent_2(),
	JsonSerializer_tD12BA78AD82034231EB2236DE3A1F627CAF98ABC::get_offset_of__gap_3(),
	JsonSerializer_tD12BA78AD82034231EB2236DE3A1F627CAF98ABC::get_offset_of__propertyList_4(),
	JsonSerializer_tD12BA78AD82034231EB2236DE3A1F627CAF98ABC::get_offset_of__replacerFunction_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5740 = { sizeof (U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D), -1, sizeof(U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5740[4] = 
{
	U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D_StaticFields::get_offset_of_U3CU3E9__7_0_1(),
	U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D_StaticFields::get_offset_of_U3CU3E9__12_0_2(),
	U3CU3Ec_tA51A29EDAD4C2DC91275E1CB4A17187897F2691D_StaticFields::get_offset_of_U3CU3E9__12_1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5741 = { sizeof (GlobalObject_tD5B044775D4BA82AD01F71F4F605F090811A5D27), -1, sizeof(GlobalObject_tD5B044775D4BA82AD01F71F4F605F090811A5D27_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5741[2] = 
{
	GlobalObject_tD5B044775D4BA82AD01F71F4F605F090811A5D27_StaticFields::get_offset_of_UriReserved_4(),
	GlobalObject_tD5B044775D4BA82AD01F71F4F605F090811A5D27_StaticFields::get_offset_of_UriUnescaped_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5742 = { sizeof (BindFunctionInstance_t6E4E2A76EEF5CD308EA29A5092891E3848880951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5742[3] = 
{
	BindFunctionInstance_t6E4E2A76EEF5CD308EA29A5092891E3848880951::get_offset_of_U3CTargetFunctionU3Ek__BackingField_8(),
	BindFunctionInstance_t6E4E2A76EEF5CD308EA29A5092891E3848880951::get_offset_of_U3CBoundThisU3Ek__BackingField_9(),
	BindFunctionInstance_t6E4E2A76EEF5CD308EA29A5092891E3848880951::get_offset_of_U3CBoundArgsU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5743 = { sizeof (EvalFunctionInstance_t14923E8CAC9C999E8100B15A3D5D07AA4F903268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5743[1] = 
{
	EvalFunctionInstance_t14923E8CAC9C999E8100B15A3D5D07AA4F903268::get_offset_of__engine_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5744 = { sizeof (FunctionConstructor_tFE3FF73EB5E5CE01E3D44BC0F4680317010CA9F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5744[2] = 
{
	FunctionConstructor_tFE3FF73EB5E5CE01E3D44BC0F4680317010CA9F5::get_offset_of_U3CPrototypeObjectU3Ek__BackingField_8(),
	FunctionConstructor_tFE3FF73EB5E5CE01E3D44BC0F4680317010CA9F5::get_offset_of__throwTypeError_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5745 = { sizeof (U3CU3Ec_t08D26E0D571CE9310A6335B765A54D63B54C9E9B), -1, sizeof(U3CU3Ec_t08D26E0D571CE9310A6335B765A54D63B54C9E9B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5745[2] = 
{
	U3CU3Ec_t08D26E0D571CE9310A6335B765A54D63B54C9E9B_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t08D26E0D571CE9310A6335B765A54D63B54C9E9B_StaticFields::get_offset_of_U3CU3E9__9_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5746 = { sizeof (FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5746[4] = 
{
	FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458::get_offset_of__engine_4(),
	FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458::get_offset_of_U3CScopeU3Ek__BackingField_5(),
	FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458::get_offset_of_U3CFormalParametersU3Ek__BackingField_6(),
	FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458::get_offset_of_U3CStrictU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5747 = { sizeof (FunctionPrototype_tF0430BB38A6440E67399DEFFA08B7D87561B4C79), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5748 = { sizeof (ScriptFunctionInstance_tE4F868CC521A1980CFC94C7A0DE632ECDC1B90D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5748[1] = 
{
	ScriptFunctionInstance_tE4F868CC521A1980CFC94C7A0DE632ECDC1B90D1::get_offset_of__functionDeclaration_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5749 = { sizeof (U3CU3Ec_tD424B78C5B503E08486D4BB009F1AFB919516C0E), -1, sizeof(U3CU3Ec_tD424B78C5B503E08486D4BB009F1AFB919516C0E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5749[2] = 
{
	U3CU3Ec_tD424B78C5B503E08486D4BB009F1AFB919516C0E_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tD424B78C5B503E08486D4BB009F1AFB919516C0E_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5750 = { sizeof (ThrowTypeError_t137EE81E953352892F6929D59FCF620EB00384B3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5750[1] = 
{
	ThrowTypeError_t137EE81E953352892F6929D59FCF620EB00384B3::get_offset_of__engine_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5751 = { sizeof (ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5751[2] = 
{
	ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E::get_offset_of__name_8(),
	ErrorConstructor_t238B22C1C5F1F2624F128AC2C0063646D0376D6E::get_offset_of_U3CPrototypeObjectU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5752 = { sizeof (ErrorInstance_t2342D0236A711154460069F45C331A2002E1A3E9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5753 = { sizeof (ErrorPrototype_tB21E1B2F5B399BBE416BDC3CA41436C1D77F9B3B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5754 = { sizeof (DateConstructor_t5098B976636DBEB0847A7217566BD868C59701D0), -1, sizeof(DateConstructor_t5098B976636DBEB0847A7217566BD868C59701D0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5754[2] = 
{
	DateConstructor_t5098B976636DBEB0847A7217566BD868C59701D0_StaticFields::get_offset_of_Epoch_8(),
	DateConstructor_t5098B976636DBEB0847A7217566BD868C59701D0::get_offset_of_U3CPrototypeObjectU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5755 = { sizeof (DateInstance_tFEBBE794D12E67B7227A43F06FC2B73BA633972E), -1, sizeof(DateInstance_tFEBBE794D12E67B7227A43F06FC2B73BA633972E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5755[3] = 
{
	DateInstance_tFEBBE794D12E67B7227A43F06FC2B73BA633972E_StaticFields::get_offset_of_Max_4(),
	DateInstance_tFEBBE794D12E67B7227A43F06FC2B73BA633972E_StaticFields::get_offset_of_Min_5(),
	DateInstance_tFEBBE794D12E67B7227A43F06FC2B73BA633972E::get_offset_of_U3CPrimitiveValueU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5756 = { sizeof (DatePrototype_t1A74663765296A18E85477D2A2F0EB7B7962D07E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5757 = { sizeof (BooleanConstructor_t4500B0A4DA339E11D5E6E983A0168B8D98678CDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5757[1] = 
{
	BooleanConstructor_t4500B0A4DA339E11D5E6E983A0168B8D98678CDB::get_offset_of_U3CPrototypeObjectU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5758 = { sizeof (BooleanInstance_tD6874F12BD130B6F23EAF7D298FD8546860E749E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5758[1] = 
{
	BooleanInstance_tD6874F12BD130B6F23EAF7D298FD8546860E749E::get_offset_of_U3CPrimitiveValueU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5759 = { sizeof (BooleanPrototype_tF496E12A7D263301D5E7E383BC0DB245AD8A1C03), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5760 = { sizeof (ArrayConstructor_t3EE97DD1C34DD817D0D8540BBEC734D56BB7D9C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5760[1] = 
{
	ArrayConstructor_t3EE97DD1C34DD817D0D8540BBEC734D56BB7D9C8::get_offset_of_U3CPrototypeObjectU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5761 = { sizeof (ArrayInstance_t3FAE9584CB9935D861D86E72FE19FE7EB6C5016E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5761[3] = 
{
	ArrayInstance_t3FAE9584CB9935D861D86E72FE19FE7EB6C5016E::get_offset_of__engine_4(),
	ArrayInstance_t3FAE9584CB9935D861D86E72FE19FE7EB6C5016E::get_offset_of__array_5(),
	ArrayInstance_t3FAE9584CB9935D861D86E72FE19FE7EB6C5016E::get_offset_of__length_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5762 = { sizeof (U3CGetOwnPropertiesU3Ed__9_t51477DED42F4DA6F170DBD9ADAFAC43472331381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5762[6] = 
{
	U3CGetOwnPropertiesU3Ed__9_t51477DED42F4DA6F170DBD9ADAFAC43472331381::get_offset_of_U3CU3E1__state_0(),
	U3CGetOwnPropertiesU3Ed__9_t51477DED42F4DA6F170DBD9ADAFAC43472331381::get_offset_of_U3CU3E2__current_1(),
	U3CGetOwnPropertiesU3Ed__9_t51477DED42F4DA6F170DBD9ADAFAC43472331381::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetOwnPropertiesU3Ed__9_t51477DED42F4DA6F170DBD9ADAFAC43472331381::get_offset_of_U3CU3E4__this_3(),
	U3CGetOwnPropertiesU3Ed__9_t51477DED42F4DA6F170DBD9ADAFAC43472331381::get_offset_of_U3CU3E7__wrap1_4(),
	U3CGetOwnPropertiesU3Ed__9_t51477DED42F4DA6F170DBD9ADAFAC43472331381::get_offset_of_U3CU3E7__wrap2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5763 = { sizeof (ArrayPrototype_t02B45CBA6C396ABAB36D738790A9B466E69C0A80), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5764 = { sizeof (U3CU3Ec__DisplayClass13_0_t9BE102B85A5047BB6E74A0770C0BEAEAADEA6A3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5764[2] = 
{
	U3CU3Ec__DisplayClass13_0_t9BE102B85A5047BB6E74A0770C0BEAEAADEA6A3F::get_offset_of_compareFn_0(),
	U3CU3Ec__DisplayClass13_0_t9BE102B85A5047BB6E74A0770C0BEAEAADEA6A3F::get_offset_of_obj_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5765 = { sizeof (U3CU3Ec__DisplayClass20_0_tC410025BF2B7FB429803AF15A4B4541215C77EFE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5765[2] = 
{
	U3CU3Ec__DisplayClass20_0_tC410025BF2B7FB429803AF15A4B4541215C77EFE::get_offset_of_func_0(),
	U3CU3Ec__DisplayClass20_0_tC410025BF2B7FB429803AF15A4B4541215C77EFE::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5766 = { sizeof (U3CU3Ec_tCAF0295190D68D942B0720F12D542470E433930A), -1, sizeof(U3CU3Ec_tCAF0295190D68D942B0720F12D542470E433930A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5766[2] = 
{
	U3CU3Ec_tCAF0295190D68D942B0720F12D542470E433930A_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tCAF0295190D68D942B0720F12D542470E433930A_StaticFields::get_offset_of_U3CU3E9__20_1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5767 = { sizeof (ArgumentsInstance_tE7E5E059105C4D56F956C6696AC64F219B328E17), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5767[4] = 
{
	ArgumentsInstance_tE7E5E059105C4D56F956C6696AC64F219B328E17::get_offset_of_U3CStrictU3Ek__BackingField_4(),
	ArgumentsInstance_tE7E5E059105C4D56F956C6696AC64F219B328E17::get_offset_of__initializer_5(),
	ArgumentsInstance_tE7E5E059105C4D56F956C6696AC64F219B328E17::get_offset_of__initialized_6(),
	ArgumentsInstance_tE7E5E059105C4D56F956C6696AC64F219B328E17::get_offset_of_U3CParameterMapU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5768 = { sizeof (U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5768[6] = 
{
	U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B::get_offset_of_args_0(),
	U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B::get_offset_of_engine_1(),
	U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B::get_offset_of_names_2(),
	U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B::get_offset_of_strict_3(),
	U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B::get_offset_of_env_4(),
	U3CU3Ec__DisplayClass8_0_t75B025429FF850EE010C748A7ACBCA234F395E7B::get_offset_of_func_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5769 = { sizeof (U3CU3Ec__DisplayClass8_1_t69B02D7552EA92BB50A63A6FC19F7B88218C47CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5769[2] = 
{
	U3CU3Ec__DisplayClass8_1_t69B02D7552EA92BB50A63A6FC19F7B88218C47CB::get_offset_of_name_0(),
	U3CU3Ec__DisplayClass8_1_t69B02D7552EA92BB50A63A6FC19F7B88218C47CB::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5770 = { sizeof (U3CPrivateImplementationDetailsU3E_tBD5F738543D4F244D12D007ECBC0145D4EA910C2), -1, sizeof(U3CPrivateImplementationDetailsU3E_tBD5F738543D4F244D12D007ECBC0145D4EA910C2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5770[3] = 
{
	U3CPrivateImplementationDetailsU3E_tBD5F738543D4F244D12D007ECBC0145D4EA910C2_StaticFields::get_offset_of_U37B7505A366B96617ACEAD621E1A0916FC82FE252_0(),
	U3CPrivateImplementationDetailsU3E_tBD5F738543D4F244D12D007ECBC0145D4EA910C2_StaticFields::get_offset_of_U384A0343BF19D2274E807E1B6505C382F81D6E3C9_1(),
	U3CPrivateImplementationDetailsU3E_tBD5F738543D4F244D12D007ECBC0145D4EA910C2_StaticFields::get_offset_of_CAD8623838274740D6497489F547CE972C42A942_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5771 = { sizeof (__StaticArrayInitTypeSizeU3D20_t4E67425F223A447FB35934546C68C5AE249274DB)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D20_t4E67425F223A447FB35934546C68C5AE249274DB ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5772 = { sizeof (__StaticArrayInitTypeSizeU3D142_t6373C387136FCB64284E833885687923757F585D)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D142_t6373C387136FCB64284E833885687923757F585D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5773 = { sizeof (U3CModuleU3E_t704E3C0969FBB43968385371DD4472AF67DF3706), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5774 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5775 = { sizeof (NullProgressReporter_tC3B472D8C44A6FBDE465E4E0D28185E3445F1B38), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5776 = { sizeof (BinaryDeltaReader_t6D4B02E88361BD2601A687680DAECF9C7356FC44), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5776[5] = 
{
	BinaryDeltaReader_t6D4B02E88361BD2601A687680DAECF9C7356FC44::get_offset_of_reader_0(),
	BinaryDeltaReader_t6D4B02E88361BD2601A687680DAECF9C7356FC44::get_offset_of_progressReporter_1(),
	BinaryDeltaReader_t6D4B02E88361BD2601A687680DAECF9C7356FC44::get_offset_of_expectedHash_2(),
	BinaryDeltaReader_t6D4B02E88361BD2601A687680DAECF9C7356FC44::get_offset_of_hashAlgorithm_3(),
	BinaryDeltaReader_t6D4B02E88361BD2601A687680DAECF9C7356FC44::get_offset_of_hasReadMetadata_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5777 = { sizeof (BinaryFormat_t54D3BBC8CC0E2537B2F3D81427AA08FEDE8ED12F), -1, sizeof(BinaryFormat_t54D3BBC8CC0E2537B2F3D81427AA08FEDE8ED12F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5777[3] = 
{
	BinaryFormat_t54D3BBC8CC0E2537B2F3D81427AA08FEDE8ED12F_StaticFields::get_offset_of_SignatureHeader_0(),
	BinaryFormat_t54D3BBC8CC0E2537B2F3D81427AA08FEDE8ED12F_StaticFields::get_offset_of_DeltaHeader_1(),
	BinaryFormat_t54D3BBC8CC0E2537B2F3D81427AA08FEDE8ED12F_StaticFields::get_offset_of_EndOfMetadata_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5778 = { sizeof (CompatibilityException_t2744573051489904AFA54781620382D1740CAA67), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5779 = { sizeof (CorruptFileFormatException_tFB71F02C812CCFFAE7E61A6EA9679D9F18003220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5780 = { sizeof (DeltaApplier_tEEA63F189630738E3ED55578959BEA9680176335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5780[1] = 
{
	DeltaApplier_tEEA63F189630738E3ED55578959BEA9680176335::get_offset_of_U3CSkipHashCheckU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5781 = { sizeof (U3CU3Ec__DisplayClass5_0_t29B5ACD9AA13C46D839642A2CD7A4078957AE95E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5781[2] = 
{
	U3CU3Ec__DisplayClass5_0_t29B5ACD9AA13C46D839642A2CD7A4078957AE95E::get_offset_of_outputStream_0(),
	U3CU3Ec__DisplayClass5_0_t29B5ACD9AA13C46D839642A2CD7A4078957AE95E::get_offset_of_basisFileStream_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5782 = { sizeof (HashAlgorithmWrapper_t59B0D3D042D436BEEAB3A6D3F2E5677E25C82686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5782[2] = 
{
	HashAlgorithmWrapper_t59B0D3D042D436BEEAB3A6D3F2E5677E25C82686::get_offset_of_algorithm_0(),
	HashAlgorithmWrapper_t59B0D3D042D436BEEAB3A6D3F2E5677E25C82686::get_offset_of_U3CNameU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5783 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5784 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5785 = { sizeof (SupportedAlgorithms_t4D45387BB8E5BC8B5A0D867FFFCC66988B28322F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5786 = { sizeof (Hashing_tE673B84DD49D3ADCD0E15A3E6B8F68D8F135CFCF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5787 = { sizeof (UsageException_t956E8ACD6899FD00E72618C8FA87DAC6D6115B4E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5788 = { sizeof (FileManager_tED5F6A99B61D40EAE6B612DDB9D6448AF26528AE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5789 = { sizeof (LauncherManager_tA5EA158E3347DBF833C1A015EFBAE39A10537D61), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5789[2] = 
{
	LauncherManager_tA5EA158E3347DBF833C1A015EFBAE39A10537D61::get_offset_of_m_patchApplier_0(),
	LauncherManager_tA5EA158E3347DBF833C1A015EFBAE39A10537D61::get_offset_of_SETTINGS_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5790 = { sizeof (Patch_tEA6D72DC3DD84AE96CFC0668FE1A7467F20A0499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5790[4] = 
{
	Patch_tEA6D72DC3DD84AE96CFC0668FE1A7467F20A0499::get_offset_of_From_0(),
	Patch_tEA6D72DC3DD84AE96CFC0668FE1A7467F20A0499::get_offset_of_To_1(),
	Patch_tEA6D72DC3DD84AE96CFC0668FE1A7467F20A0499::get_offset_of_Hash_2(),
	Patch_tEA6D72DC3DD84AE96CFC0668FE1A7467F20A0499::get_offset_of_Type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5791 = { sizeof (PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5791[16] = 
{
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of__patches_0(),
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of__downloadedArchiveFilesHashes_1(),
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of_OnFileProcessed_2(),
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of_OnFileProcessing_3(),
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of_OnLog_4(),
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of_OnError_5(),
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of_OnFatalError_6(),
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of_OnTaskStarted_7(),
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of_OnTaskCompleted_8(),
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of_OnSetMainProgressBar_9(),
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of_OnSetDetailProgressBar_10(),
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of_OnIncreaseMainProgressBar_11(),
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of_OnIncreaseDetailProgressBar_12(),
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of_OnDownloadProgress_13(),
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of_OnDownloadCompleted_14(),
	PatchApplier_tF0FD45BF12E56EF771624EC00E3AB69839E51938::get_offset_of_IsDirtyWorkspace_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5792 = { sizeof (U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436), -1, sizeof(U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5792[16] = 
{
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9__16_0_1(),
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9__16_1_2(),
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9__16_2_3(),
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9__16_3_4(),
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9__16_4_5(),
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9__16_5_6(),
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9__16_6_7(),
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9__16_7_8(),
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9__16_8_9(),
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9__16_9_10(),
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9__16_10_11(),
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9__16_11_12(),
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9__16_12_13(),
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9__17_1_14(),
	U3CU3Ec_t90691F5CC2F74205A916FA45AC0D2DADB4019436_StaticFields::get_offset_of_U3CU3E9__17_3_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5793 = { sizeof (U3CU3Ec__DisplayClass17_0_t5BB4A897CD7172CD72926C9BC88AC164F62CEB04), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5793[2] = 
{
	U3CU3Ec__DisplayClass17_0_t5BB4A897CD7172CD72926C9BC88AC164F62CEB04::get_offset_of_currentVersion_0(),
	U3CU3Ec__DisplayClass17_0_t5BB4A897CD7172CD72926C9BC88AC164F62CEB04::get_offset_of_U3CU3E9__2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5794 = { sizeof (Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5794[4] = 
{
	Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976::get_offset_of_m_majorReleaseNumber_0(),
	Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976::get_offset_of_m_minorReleaseNumber_1(),
	Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976::get_offset_of_m_maintenanceReleaseNumber_2(),
	Version_t9E4808E22CBFFB54540FB10B7CA88195A0F75976::get_offset_of_m_buildNumber_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5795 = { sizeof (U3CU3Ec_t55DA89562498EBEF40EB707B902EAE64CFF65851), -1, sizeof(U3CU3Ec_t55DA89562498EBEF40EB707B902EAE64CFF65851_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5795[2] = 
{
	U3CU3Ec_t55DA89562498EBEF40EB707B902EAE64CFF65851_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t55DA89562498EBEF40EB707B902EAE64CFF65851_StaticFields::get_offset_of_U3CU3E9__15_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5796 = { sizeof (BinaryComparer_t978B3C973609E9A7EE9830B13EFA66D6EF063983), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5797 = { sizeof (Hashing_tD9A6183B77B7B71494AEFD6BA8E8BE6BBDB55FED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5798 = { sizeof (Rijndael_t3628B330F435769E367ED28C39FDB2D5601C1392), -1, sizeof(Rijndael_t3628B330F435769E367ED28C39FDB2D5601C1392_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5798[1] = 
{
	Rijndael_t3628B330F435769E367ED28C39FDB2D5601C1392_StaticFields::get_offset_of_initVectorBytes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5799 = { sizeof (Utility_t6507726DA6EAD3D639EF1B6D4268D0AF79CD83E6), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
