﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE;
// System.Action`3<System.Int32,System.Int32,System.Int32>
struct Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Attribute[]
struct AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char
struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.Dictionary`2<System.String,System.Data.DataColumn>
struct Dictionary_2_t097E7AE4AAA0034CC5A6B941AC4C115EA7AFC199;
// System.Collections.Generic.List`1<System.Data.DataColumn>
struct List_1_t70187E1F2F9140ADB155B98F17D5D765F84B9204;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.ComponentModel.AttributeCollection
struct AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE;
// System.ComponentModel.CollectionChangeEventArgs
struct CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0;
// System.ComponentModel.CollectionChangeEventHandler
struct CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4;
// System.ComponentModel.ISite
struct ISite_t6804B48BC23ABB5F4141903F878589BCEF6097A2;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82;
// System.ComponentModel.TypeConverter
struct TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB;
// System.ComponentModel.TypeConverter/StandardValuesCollection
struct StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3;
// System.Data.AutoIncrementValue
struct AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086;
// System.Data.Common.DataStorage
struct DataStorage_t48039E31DBE91E7C82D0DB461ABEDD50C6C017A6;
// System.Data.Constraint
struct Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2;
// System.Data.Constraint[]
struct ConstraintU5BU5D_t7E694ACFAC8AA785DDDB7F40FC22F2A72E970557;
// System.Data.DataColumn
struct DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D;
// System.Data.DataColumnChangeEventArgs
struct DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280;
// System.Data.DataColumnCollection
struct DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A;
// System.Data.DataColumn[]
struct DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC;
// System.Data.DataError
struct DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786;
// System.Data.DataError/ColumnError[]
struct ColumnErrorU5BU5D_t569A5340C9E310B9803262B32FC9D9803AA3DD93;
// System.Data.DataExpression
struct DataExpression_tECCBF728C87CAF0185856F73F7DB54BB94EF094D;
// System.Data.DataRelation
struct DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3;
// System.Data.DataRelation[]
struct DataRelationU5BU5D_t705BDBA68D45143524D5C70D82EA04F0B676C15B;
// System.Data.DataRow
struct DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B;
// System.Data.DataRowChangeEventArgs
struct DataRowChangeEventArgs_tA5961502488B4B5C32F788CB86746087E043A444;
// System.Data.DataRowCollection/DataRowTree
struct DataRowTree_t885C3CBC17060B726BFEE177710D6E9E57FEA230;
// System.Data.DataSet
struct DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8;
// System.Data.DataTable
struct DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863;
// System.Data.ForeignKeyConstraint
struct ForeignKeyConstraint_t3F1E5D8CA4D7F8EE363336DF44B29BC24E8C016A;
// System.Data.Index
struct Index_t0B13AD066A6CAA0045DCA5BB8912F8E599BE9AF7;
// System.Data.PropertyCollection
struct PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4;
// System.Data.RBTree`1/TreePage<System.Data.DataRow>[]
struct TreePageU5BU5D_tBBE7049BB6412C75C6BF3F25774F9E64CA96CFE6;
// System.Data.SimpleType
struct SimpleType_tB77732892B48FAB73D5074136016F9EC03006202;
// System.Data.UniqueConstraint
struct UniqueConstraint_t291F6C173D4820C1ACAE889805C3649A44DC1D22;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IDisposable
struct IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2;
// UnityEngine.Video.VideoPlayer/ErrorEventHandler
struct ErrorEventHandler_tF5863946928B48BE13146ED5FF70AC92678FE222;
// UnityEngine.Video.VideoPlayer/EventHandler
struct EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308;
// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler
struct FrameReadyEventHandler_t518B277D916AB292680CAA186BCDB3D3EF130422;
// UnityEngine.Video.VideoPlayer/TimeEventHandler
struct TimeEventHandler_tDD815DAABFADDD98C8993B2A97A2FCE858266BC1;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T064756C4EE8D64CEFE107E600CEBCB3F77894990_H
#define U3CMODULEU3E_T064756C4EE8D64CEFE107E600CEBCB3F77894990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t064756C4EE8D64CEFE107E600CEBCB3F77894990 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T064756C4EE8D64CEFE107E600CEBCB3F77894990_H
#ifndef U3CMODULEU3E_T256052731F6AF04064254ED502AB58F751060075_H
#define U3CMODULEU3E_T256052731F6AF04064254ED502AB58F751060075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t256052731F6AF04064254ED502AB58F751060075 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T256052731F6AF04064254ED502AB58F751060075_H
#ifndef U3CMODULEU3E_T2DE490318B8122B2500276938943D527361370EF_H
#define U3CMODULEU3E_T2DE490318B8122B2500276938943D527361370EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t2DE490318B8122B2500276938943D527361370EF 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T2DE490318B8122B2500276938943D527361370EF_H
#ifndef U3CMODULEU3E_TF3A658440E2EA6B9E28737BEB10616BB13D75E94_H
#define U3CMODULEU3E_TF3A658440E2EA6B9E28737BEB10616BB13D75E94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tF3A658440E2EA6B9E28737BEB10616BB13D75E94 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TF3A658440E2EA6B9E28737BEB10616BB13D75E94_H
#ifndef U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#define U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#ifndef U3CMODULEU3E_TBE4A2C03D239132B99D8F2C5938A2EC7220D741C_H
#define U3CMODULEU3E_TBE4A2C03D239132B99D8F2C5938A2EC7220D741C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tBE4A2C03D239132B99D8F2C5938A2EC7220D741C 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TBE4A2C03D239132B99D8F2C5938A2EC7220D741C_H
#ifndef U3CMODULEU3E_TB054F17A779AC945E3659AF119A96DB806541AF9_H
#define U3CMODULEU3E_TB054F17A779AC945E3659AF119A96DB806541AF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tB054F17A779AC945E3659AF119A96DB806541AF9 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TB054F17A779AC945E3659AF119A96DB806541AF9_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SR_T0C89764BAB3C8C6F8EA05107995810E932907097_H
#define SR_T0C89764BAB3C8C6F8EA05107995810E932907097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SR
struct  SR_t0C89764BAB3C8C6F8EA05107995810E932907097  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SR_T0C89764BAB3C8C6F8EA05107995810E932907097_H
#ifndef SR_T9F9B78806C8CD422A08572CB2F28D766146CF3AB_H
#define SR_T9F9B78806C8CD422A08572CB2F28D766146CF3AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SR
struct  SR_t9F9B78806C8CD422A08572CB2F28D766146CF3AB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SR_T9F9B78806C8CD422A08572CB2F28D766146CF3AB_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef MARSHALBYVALUECOMPONENT_TADC0E481D4D19F965AB659F9038A1D7D47FA636B_H
#define MARSHALBYVALUECOMPONENT_TADC0E481D4D19F965AB659F9038A1D7D47FA636B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MarshalByValueComponent
struct  MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B  : public RuntimeObject
{
public:
	// System.ComponentModel.ISite System.ComponentModel.MarshalByValueComponent::site
	RuntimeObject* ___site_1;
	// System.ComponentModel.EventHandlerList System.ComponentModel.MarshalByValueComponent::events
	EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * ___events_2;

public:
	inline static int32_t get_offset_of_site_1() { return static_cast<int32_t>(offsetof(MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B, ___site_1)); }
	inline RuntimeObject* get_site_1() const { return ___site_1; }
	inline RuntimeObject** get_address_of_site_1() { return &___site_1; }
	inline void set_site_1(RuntimeObject* value)
	{
		___site_1 = value;
		Il2CppCodeGenWriteBarrier((&___site_1), value);
	}

	inline static int32_t get_offset_of_events_2() { return static_cast<int32_t>(offsetof(MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B, ___events_2)); }
	inline EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * get_events_2() const { return ___events_2; }
	inline EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 ** get_address_of_events_2() { return &___events_2; }
	inline void set_events_2(EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * value)
	{
		___events_2 = value;
		Il2CppCodeGenWriteBarrier((&___events_2), value);
	}
};

struct MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B_StaticFields
{
public:
	// System.Object System.ComponentModel.MarshalByValueComponent::EventDisposed
	RuntimeObject * ___EventDisposed_0;

public:
	inline static int32_t get_offset_of_EventDisposed_0() { return static_cast<int32_t>(offsetof(MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B_StaticFields, ___EventDisposed_0)); }
	inline RuntimeObject * get_EventDisposed_0() const { return ___EventDisposed_0; }
	inline RuntimeObject ** get_address_of_EventDisposed_0() { return &___EventDisposed_0; }
	inline void set_EventDisposed_0(RuntimeObject * value)
	{
		___EventDisposed_0 = value;
		Il2CppCodeGenWriteBarrier((&___EventDisposed_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYVALUECOMPONENT_TADC0E481D4D19F965AB659F9038A1D7D47FA636B_H
#ifndef MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#define MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MemberDescriptor
struct  MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8  : public RuntimeObject
{
public:
	// System.String System.ComponentModel.MemberDescriptor::name
	String_t* ___name_0;
	// System.String System.ComponentModel.MemberDescriptor::displayName
	String_t* ___displayName_1;
	// System.Int32 System.ComponentModel.MemberDescriptor::nameHash
	int32_t ___nameHash_2;
	// System.ComponentModel.AttributeCollection System.ComponentModel.MemberDescriptor::attributeCollection
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * ___attributeCollection_3;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::attributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___attributes_4;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::originalAttributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___originalAttributes_5;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFiltered
	bool ___attributesFiltered_6;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFilled
	bool ___attributesFilled_7;
	// System.Int32 System.ComponentModel.MemberDescriptor::metadataVersion
	int32_t ___metadataVersion_8;
	// System.String System.ComponentModel.MemberDescriptor::category
	String_t* ___category_9;
	// System.String System.ComponentModel.MemberDescriptor::description
	String_t* ___description_10;
	// System.Object System.ComponentModel.MemberDescriptor::lockCookie
	RuntimeObject * ___lockCookie_11;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_displayName_1() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___displayName_1)); }
	inline String_t* get_displayName_1() const { return ___displayName_1; }
	inline String_t** get_address_of_displayName_1() { return &___displayName_1; }
	inline void set_displayName_1(String_t* value)
	{
		___displayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_1), value);
	}

	inline static int32_t get_offset_of_nameHash_2() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___nameHash_2)); }
	inline int32_t get_nameHash_2() const { return ___nameHash_2; }
	inline int32_t* get_address_of_nameHash_2() { return &___nameHash_2; }
	inline void set_nameHash_2(int32_t value)
	{
		___nameHash_2 = value;
	}

	inline static int32_t get_offset_of_attributeCollection_3() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributeCollection_3)); }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * get_attributeCollection_3() const { return ___attributeCollection_3; }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE ** get_address_of_attributeCollection_3() { return &___attributeCollection_3; }
	inline void set_attributeCollection_3(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * value)
	{
		___attributeCollection_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributeCollection_3), value);
	}

	inline static int32_t get_offset_of_attributes_4() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributes_4)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_attributes_4() const { return ___attributes_4; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_attributes_4() { return &___attributes_4; }
	inline void set_attributes_4(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___attributes_4 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_4), value);
	}

	inline static int32_t get_offset_of_originalAttributes_5() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___originalAttributes_5)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_originalAttributes_5() const { return ___originalAttributes_5; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_originalAttributes_5() { return &___originalAttributes_5; }
	inline void set_originalAttributes_5(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___originalAttributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___originalAttributes_5), value);
	}

	inline static int32_t get_offset_of_attributesFiltered_6() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFiltered_6)); }
	inline bool get_attributesFiltered_6() const { return ___attributesFiltered_6; }
	inline bool* get_address_of_attributesFiltered_6() { return &___attributesFiltered_6; }
	inline void set_attributesFiltered_6(bool value)
	{
		___attributesFiltered_6 = value;
	}

	inline static int32_t get_offset_of_attributesFilled_7() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFilled_7)); }
	inline bool get_attributesFilled_7() const { return ___attributesFilled_7; }
	inline bool* get_address_of_attributesFilled_7() { return &___attributesFilled_7; }
	inline void set_attributesFilled_7(bool value)
	{
		___attributesFilled_7 = value;
	}

	inline static int32_t get_offset_of_metadataVersion_8() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___metadataVersion_8)); }
	inline int32_t get_metadataVersion_8() const { return ___metadataVersion_8; }
	inline int32_t* get_address_of_metadataVersion_8() { return &___metadataVersion_8; }
	inline void set_metadataVersion_8(int32_t value)
	{
		___metadataVersion_8 = value;
	}

	inline static int32_t get_offset_of_category_9() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___category_9)); }
	inline String_t* get_category_9() const { return ___category_9; }
	inline String_t** get_address_of_category_9() { return &___category_9; }
	inline void set_category_9(String_t* value)
	{
		___category_9 = value;
		Il2CppCodeGenWriteBarrier((&___category_9), value);
	}

	inline static int32_t get_offset_of_description_10() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___description_10)); }
	inline String_t* get_description_10() const { return ___description_10; }
	inline String_t** get_address_of_description_10() { return &___description_10; }
	inline void set_description_10(String_t* value)
	{
		___description_10 = value;
		Il2CppCodeGenWriteBarrier((&___description_10), value);
	}

	inline static int32_t get_offset_of_lockCookie_11() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___lockCookie_11)); }
	inline RuntimeObject * get_lockCookie_11() const { return ___lockCookie_11; }
	inline RuntimeObject ** get_address_of_lockCookie_11() { return &___lockCookie_11; }
	inline void set_lockCookie_11(RuntimeObject * value)
	{
		___lockCookie_11 = value;
		Il2CppCodeGenWriteBarrier((&___lockCookie_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifndef AUTOINCREMENTVALUE_T11483E6928C70E29355C27AE6FAF1279FA78C086_H
#define AUTOINCREMENTVALUE_T11483E6928C70E29355C27AE6FAF1279FA78C086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.AutoIncrementValue
struct  AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086  : public RuntimeObject
{
public:
	// System.Boolean System.Data.AutoIncrementValue::<Auto>k__BackingField
	bool ___U3CAutoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CAutoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086, ___U3CAutoU3Ek__BackingField_0)); }
	inline bool get_U3CAutoU3Ek__BackingField_0() const { return ___U3CAutoU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CAutoU3Ek__BackingField_0() { return &___U3CAutoU3Ek__BackingField_0; }
	inline void set_U3CAutoU3Ek__BackingField_0(bool value)
	{
		___U3CAutoU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOINCREMENTVALUE_T11483E6928C70E29355C27AE6FAF1279FA78C086_H
#ifndef CONSTRAINT_TB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2_H
#define CONSTRAINT_TB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Constraint
struct  Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2  : public RuntimeObject
{
public:
	// System.String System.Data.Constraint::_schemaName
	String_t* ____schemaName_0;
	// System.Boolean System.Data.Constraint::_inCollection
	bool ____inCollection_1;
	// System.Data.DataSet System.Data.Constraint::_dataSet
	DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * ____dataSet_2;
	// System.String System.Data.Constraint::_name
	String_t* ____name_3;
	// System.Data.PropertyCollection System.Data.Constraint::_extendedProperties
	PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * ____extendedProperties_4;

public:
	inline static int32_t get_offset_of__schemaName_0() { return static_cast<int32_t>(offsetof(Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2, ____schemaName_0)); }
	inline String_t* get__schemaName_0() const { return ____schemaName_0; }
	inline String_t** get_address_of__schemaName_0() { return &____schemaName_0; }
	inline void set__schemaName_0(String_t* value)
	{
		____schemaName_0 = value;
		Il2CppCodeGenWriteBarrier((&____schemaName_0), value);
	}

	inline static int32_t get_offset_of__inCollection_1() { return static_cast<int32_t>(offsetof(Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2, ____inCollection_1)); }
	inline bool get__inCollection_1() const { return ____inCollection_1; }
	inline bool* get_address_of__inCollection_1() { return &____inCollection_1; }
	inline void set__inCollection_1(bool value)
	{
		____inCollection_1 = value;
	}

	inline static int32_t get_offset_of__dataSet_2() { return static_cast<int32_t>(offsetof(Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2, ____dataSet_2)); }
	inline DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * get__dataSet_2() const { return ____dataSet_2; }
	inline DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 ** get_address_of__dataSet_2() { return &____dataSet_2; }
	inline void set__dataSet_2(DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * value)
	{
		____dataSet_2 = value;
		Il2CppCodeGenWriteBarrier((&____dataSet_2), value);
	}

	inline static int32_t get_offset_of__name_3() { return static_cast<int32_t>(offsetof(Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2, ____name_3)); }
	inline String_t* get__name_3() const { return ____name_3; }
	inline String_t** get_address_of__name_3() { return &____name_3; }
	inline void set__name_3(String_t* value)
	{
		____name_3 = value;
		Il2CppCodeGenWriteBarrier((&____name_3), value);
	}

	inline static int32_t get_offset_of__extendedProperties_4() { return static_cast<int32_t>(offsetof(Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2, ____extendedProperties_4)); }
	inline PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * get__extendedProperties_4() const { return ____extendedProperties_4; }
	inline PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 ** get_address_of__extendedProperties_4() { return &____extendedProperties_4; }
	inline void set__extendedProperties_4(PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * value)
	{
		____extendedProperties_4 = value;
		Il2CppCodeGenWriteBarrier((&____extendedProperties_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINT_TB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2_H
#ifndef CONSTRAINTENUMERATOR_T66AB033C0618C3354F9911CE469131D6020CDC36_H
#define CONSTRAINTENUMERATOR_T66AB033C0618C3354F9911CE469131D6020CDC36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ConstraintEnumerator
struct  ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Data.ConstraintEnumerator::_tables
	RuntimeObject* ____tables_0;
	// System.Collections.IEnumerator System.Data.ConstraintEnumerator::_constraints
	RuntimeObject* ____constraints_1;
	// System.Data.Constraint System.Data.ConstraintEnumerator::_currentObject
	Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2 * ____currentObject_2;

public:
	inline static int32_t get_offset_of__tables_0() { return static_cast<int32_t>(offsetof(ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36, ____tables_0)); }
	inline RuntimeObject* get__tables_0() const { return ____tables_0; }
	inline RuntimeObject** get_address_of__tables_0() { return &____tables_0; }
	inline void set__tables_0(RuntimeObject* value)
	{
		____tables_0 = value;
		Il2CppCodeGenWriteBarrier((&____tables_0), value);
	}

	inline static int32_t get_offset_of__constraints_1() { return static_cast<int32_t>(offsetof(ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36, ____constraints_1)); }
	inline RuntimeObject* get__constraints_1() const { return ____constraints_1; }
	inline RuntimeObject** get_address_of__constraints_1() { return &____constraints_1; }
	inline void set__constraints_1(RuntimeObject* value)
	{
		____constraints_1 = value;
		Il2CppCodeGenWriteBarrier((&____constraints_1), value);
	}

	inline static int32_t get_offset_of__currentObject_2() { return static_cast<int32_t>(offsetof(ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36, ____currentObject_2)); }
	inline Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2 * get__currentObject_2() const { return ____currentObject_2; }
	inline Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2 ** get_address_of__currentObject_2() { return &____currentObject_2; }
	inline void set__currentObject_2(Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2 * value)
	{
		____currentObject_2 = value;
		Il2CppCodeGenWriteBarrier((&____currentObject_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTENUMERATOR_T66AB033C0618C3354F9911CE469131D6020CDC36_H
#ifndef DATAERROR_TD52C55EF7C5FABAA58B11DBB0C55BE671F18F786_H
#define DATAERROR_TD52C55EF7C5FABAA58B11DBB0C55BE671F18F786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataError
struct  DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786  : public RuntimeObject
{
public:
	// System.String System.Data.DataError::_rowError
	String_t* ____rowError_0;
	// System.Int32 System.Data.DataError::_count
	int32_t ____count_1;
	// System.Data.DataError/ColumnError[] System.Data.DataError::_errorList
	ColumnErrorU5BU5D_t569A5340C9E310B9803262B32FC9D9803AA3DD93* ____errorList_2;

public:
	inline static int32_t get_offset_of__rowError_0() { return static_cast<int32_t>(offsetof(DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786, ____rowError_0)); }
	inline String_t* get__rowError_0() const { return ____rowError_0; }
	inline String_t** get_address_of__rowError_0() { return &____rowError_0; }
	inline void set__rowError_0(String_t* value)
	{
		____rowError_0 = value;
		Il2CppCodeGenWriteBarrier((&____rowError_0), value);
	}

	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}

	inline static int32_t get_offset_of__errorList_2() { return static_cast<int32_t>(offsetof(DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786, ____errorList_2)); }
	inline ColumnErrorU5BU5D_t569A5340C9E310B9803262B32FC9D9803AA3DD93* get__errorList_2() const { return ____errorList_2; }
	inline ColumnErrorU5BU5D_t569A5340C9E310B9803262B32FC9D9803AA3DD93** get_address_of__errorList_2() { return &____errorList_2; }
	inline void set__errorList_2(ColumnErrorU5BU5D_t569A5340C9E310B9803262B32FC9D9803AA3DD93* value)
	{
		____errorList_2 = value;
		Il2CppCodeGenWriteBarrier((&____errorList_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAERROR_TD52C55EF7C5FABAA58B11DBB0C55BE671F18F786_H
#ifndef DATAROWBUILDER_T1686A02FA53DF491D826A981024C255668E94CC6_H
#define DATAROWBUILDER_T1686A02FA53DF491D826A981024C255668E94CC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRowBuilder
struct  DataRowBuilder_t1686A02FA53DF491D826A981024C255668E94CC6  : public RuntimeObject
{
public:
	// System.Data.DataTable System.Data.DataRowBuilder::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_0;
	// System.Int32 System.Data.DataRowBuilder::_record
	int32_t ____record_1;

public:
	inline static int32_t get_offset_of__table_0() { return static_cast<int32_t>(offsetof(DataRowBuilder_t1686A02FA53DF491D826A981024C255668E94CC6, ____table_0)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_0() const { return ____table_0; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_0() { return &____table_0; }
	inline void set__table_0(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_0 = value;
		Il2CppCodeGenWriteBarrier((&____table_0), value);
	}

	inline static int32_t get_offset_of__record_1() { return static_cast<int32_t>(offsetof(DataRowBuilder_t1686A02FA53DF491D826A981024C255668E94CC6, ____record_1)); }
	inline int32_t get__record_1() const { return ____record_1; }
	inline int32_t* get_address_of__record_1() { return &____record_1; }
	inline void set__record_1(int32_t value)
	{
		____record_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAROWBUILDER_T1686A02FA53DF491D826A981024C255668E94CC6_H
#ifndef EXCEPTIONBUILDER_TE6546B379EB375CF76757E7B0315505DD7929C0C_H
#define EXCEPTIONBUILDER_TE6546B379EB375CF76757E7B0315505DD7929C0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ExceptionBuilder
struct  ExceptionBuilder_tE6546B379EB375CF76757E7B0315505DD7929C0C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONBUILDER_TE6546B379EB375CF76757E7B0315505DD7929C0C_H
#ifndef INTERNALDATACOLLECTIONBASE_T8A94DFD07E59FFED7EE80E5F808509E3B2DA7334_H
#define INTERNALDATACOLLECTIONBASE_T8A94DFD07E59FFED7EE80E5F808509E3B2DA7334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.InternalDataCollectionBase
struct  InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334  : public RuntimeObject
{
public:

public:
};

struct InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334_StaticFields
{
public:
	// System.ComponentModel.CollectionChangeEventArgs System.Data.InternalDataCollectionBase::s_refreshEventArgs
	CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0 * ___s_refreshEventArgs_0;

public:
	inline static int32_t get_offset_of_s_refreshEventArgs_0() { return static_cast<int32_t>(offsetof(InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334_StaticFields, ___s_refreshEventArgs_0)); }
	inline CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0 * get_s_refreshEventArgs_0() const { return ___s_refreshEventArgs_0; }
	inline CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0 ** get_address_of_s_refreshEventArgs_0() { return &___s_refreshEventArgs_0; }
	inline void set_s_refreshEventArgs_0(CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0 * value)
	{
		___s_refreshEventArgs_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_refreshEventArgs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALDATACOLLECTIONBASE_T8A94DFD07E59FFED7EE80E5F808509E3B2DA7334_H
#ifndef EVENTSOURCE_T263F509672F3C6747C5BA393F20E2717B7A981EB_H
#define EVENTSOURCE_T263F509672F3C6747C5BA393F20E2717B7A981EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Tracing.EventSource
struct  EventSource_t263F509672F3C6747C5BA393F20E2717B7A981EB  : public RuntimeObject
{
public:
	// System.String System.Diagnostics.Tracing.EventSource::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EventSource_t263F509672F3C6747C5BA393F20E2717B7A981EB, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSOURCE_T263F509672F3C6747C5BA393F20E2717B7A981EB_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef FORMATPROVIDER_T8B5DFBEF60CF2684D61AE0D41D3291717512CF4C_H
#define FORMATPROVIDER_T8B5DFBEF60CF2684D61AE0D41D3291717512CF4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.FormatProvider
struct  FormatProvider_t8B5DFBEF60CF2684D61AE0D41D3291717512CF4C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATPROVIDER_T8B5DFBEF60CF2684D61AE0D41D3291717512CF4C_H
#ifndef NUMBER_TFD98EE73F24490FF998A76D9569C96182BDB0FC2_H
#define NUMBER_TFD98EE73F24490FF998A76D9569C96182BDB0FC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.FormatProvider/Number
struct  Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2  : public RuntimeObject
{
public:

public:
};

struct Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2_StaticFields
{
public:
	// System.String[] System.Globalization.FormatProvider/Number::s_posCurrencyFormats
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___s_posCurrencyFormats_0;
	// System.String[] System.Globalization.FormatProvider/Number::s_negCurrencyFormats
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___s_negCurrencyFormats_1;
	// System.String[] System.Globalization.FormatProvider/Number::s_posPercentFormats
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___s_posPercentFormats_2;
	// System.String[] System.Globalization.FormatProvider/Number::s_negPercentFormats
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___s_negPercentFormats_3;
	// System.String[] System.Globalization.FormatProvider/Number::s_negNumberFormats
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___s_negNumberFormats_4;
	// System.String System.Globalization.FormatProvider/Number::s_posNumberFormat
	String_t* ___s_posNumberFormat_5;

public:
	inline static int32_t get_offset_of_s_posCurrencyFormats_0() { return static_cast<int32_t>(offsetof(Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2_StaticFields, ___s_posCurrencyFormats_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_s_posCurrencyFormats_0() const { return ___s_posCurrencyFormats_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_s_posCurrencyFormats_0() { return &___s_posCurrencyFormats_0; }
	inline void set_s_posCurrencyFormats_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___s_posCurrencyFormats_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_posCurrencyFormats_0), value);
	}

	inline static int32_t get_offset_of_s_negCurrencyFormats_1() { return static_cast<int32_t>(offsetof(Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2_StaticFields, ___s_negCurrencyFormats_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_s_negCurrencyFormats_1() const { return ___s_negCurrencyFormats_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_s_negCurrencyFormats_1() { return &___s_negCurrencyFormats_1; }
	inline void set_s_negCurrencyFormats_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___s_negCurrencyFormats_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_negCurrencyFormats_1), value);
	}

	inline static int32_t get_offset_of_s_posPercentFormats_2() { return static_cast<int32_t>(offsetof(Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2_StaticFields, ___s_posPercentFormats_2)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_s_posPercentFormats_2() const { return ___s_posPercentFormats_2; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_s_posPercentFormats_2() { return &___s_posPercentFormats_2; }
	inline void set_s_posPercentFormats_2(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___s_posPercentFormats_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_posPercentFormats_2), value);
	}

	inline static int32_t get_offset_of_s_negPercentFormats_3() { return static_cast<int32_t>(offsetof(Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2_StaticFields, ___s_negPercentFormats_3)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_s_negPercentFormats_3() const { return ___s_negPercentFormats_3; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_s_negPercentFormats_3() { return &___s_negPercentFormats_3; }
	inline void set_s_negPercentFormats_3(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___s_negPercentFormats_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_negPercentFormats_3), value);
	}

	inline static int32_t get_offset_of_s_negNumberFormats_4() { return static_cast<int32_t>(offsetof(Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2_StaticFields, ___s_negNumberFormats_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_s_negNumberFormats_4() const { return ___s_negNumberFormats_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_s_negNumberFormats_4() { return &___s_negNumberFormats_4; }
	inline void set_s_negNumberFormats_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___s_negNumberFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_negNumberFormats_4), value);
	}

	inline static int32_t get_offset_of_s_posNumberFormat_5() { return static_cast<int32_t>(offsetof(Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2_StaticFields, ___s_posNumberFormat_5)); }
	inline String_t* get_s_posNumberFormat_5() const { return ___s_posNumberFormat_5; }
	inline String_t** get_address_of_s_posNumberFormat_5() { return &___s_posNumberFormat_5; }
	inline void set_s_posNumberFormat_5(String_t* value)
	{
		___s_posNumberFormat_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_posNumberFormat_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBER_TFD98EE73F24490FF998A76D9569C96182BDB0FC2_H
#ifndef BIGINTEGERCALCULATOR_TFB78069521210FFCDF1D75AF99008213A8763F25_H
#define BIGINTEGERCALCULATOR_TFB78069521210FFCDF1D75AF99008213A8763F25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Numerics.BigIntegerCalculator
struct  BigIntegerCalculator_tFB78069521210FFCDF1D75AF99008213A8763F25  : public RuntimeObject
{
public:

public:
};

struct BigIntegerCalculator_tFB78069521210FFCDF1D75AF99008213A8763F25_StaticFields
{
public:
	// System.Int32 System.Numerics.BigIntegerCalculator::ReducerThreshold
	int32_t ___ReducerThreshold_0;
	// System.Int32 System.Numerics.BigIntegerCalculator::SquareThreshold
	int32_t ___SquareThreshold_1;
	// System.Int32 System.Numerics.BigIntegerCalculator::AllocationThreshold
	int32_t ___AllocationThreshold_2;
	// System.Int32 System.Numerics.BigIntegerCalculator::MultiplyThreshold
	int32_t ___MultiplyThreshold_3;

public:
	inline static int32_t get_offset_of_ReducerThreshold_0() { return static_cast<int32_t>(offsetof(BigIntegerCalculator_tFB78069521210FFCDF1D75AF99008213A8763F25_StaticFields, ___ReducerThreshold_0)); }
	inline int32_t get_ReducerThreshold_0() const { return ___ReducerThreshold_0; }
	inline int32_t* get_address_of_ReducerThreshold_0() { return &___ReducerThreshold_0; }
	inline void set_ReducerThreshold_0(int32_t value)
	{
		___ReducerThreshold_0 = value;
	}

	inline static int32_t get_offset_of_SquareThreshold_1() { return static_cast<int32_t>(offsetof(BigIntegerCalculator_tFB78069521210FFCDF1D75AF99008213A8763F25_StaticFields, ___SquareThreshold_1)); }
	inline int32_t get_SquareThreshold_1() const { return ___SquareThreshold_1; }
	inline int32_t* get_address_of_SquareThreshold_1() { return &___SquareThreshold_1; }
	inline void set_SquareThreshold_1(int32_t value)
	{
		___SquareThreshold_1 = value;
	}

	inline static int32_t get_offset_of_AllocationThreshold_2() { return static_cast<int32_t>(offsetof(BigIntegerCalculator_tFB78069521210FFCDF1D75AF99008213A8763F25_StaticFields, ___AllocationThreshold_2)); }
	inline int32_t get_AllocationThreshold_2() const { return ___AllocationThreshold_2; }
	inline int32_t* get_address_of_AllocationThreshold_2() { return &___AllocationThreshold_2; }
	inline void set_AllocationThreshold_2(int32_t value)
	{
		___AllocationThreshold_2 = value;
	}

	inline static int32_t get_offset_of_MultiplyThreshold_3() { return static_cast<int32_t>(offsetof(BigIntegerCalculator_tFB78069521210FFCDF1D75AF99008213A8763F25_StaticFields, ___MultiplyThreshold_3)); }
	inline int32_t get_MultiplyThreshold_3() const { return ___MultiplyThreshold_3; }
	inline int32_t* get_address_of_MultiplyThreshold_3() { return &___MultiplyThreshold_3; }
	inline void set_MultiplyThreshold_3(int32_t value)
	{
		___MultiplyThreshold_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BIGINTEGERCALCULATOR_TFB78069521210FFCDF1D75AF99008213A8763F25_H
#ifndef BIGNUMBER_T219B8EAECD7E9014F6B25E8E7A3152F1F50783BA_H
#define BIGNUMBER_T219B8EAECD7E9014F6B25E8E7A3152F1F50783BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Numerics.BigNumber
struct  BigNumber_t219B8EAECD7E9014F6B25E8E7A3152F1F50783BA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BIGNUMBER_T219B8EAECD7E9014F6B25E8E7A3152F1F50783BA_H
#ifndef NUMERICSHELPERS_TD5B6EE08BA6FBEA20F1E464B39AD8C21478CD91A_H
#define NUMERICSHELPERS_TD5B6EE08BA6FBEA20F1E464B39AD8C21478CD91A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Numerics.NumericsHelpers
struct  NumericsHelpers_tD5B6EE08BA6FBEA20F1E464B39AD8C21478CD91A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMERICSHELPERS_TD5B6EE08BA6FBEA20F1E464B39AD8C21478CD91A_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ANDROIDNATIVEPOPUPS_TFCDF1E0DDBEC0C449E8C74993CE36894D3862368_H
#define ANDROIDNATIVEPOPUPS_TFCDF1E0DDBEC0C449E8C74993CE36894D3862368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.AndroidNativePopups
struct  AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368  : public RuntimeObject
{
public:

public:
};

struct AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_StaticFields
{
public:
	// System.IDisposable TheNextFlow.UnityPlugins.AndroidNativePopups::androidUtils
	RuntimeObject* ___androidUtils_0;

public:
	inline static int32_t get_offset_of_androidUtils_0() { return static_cast<int32_t>(offsetof(AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_StaticFields, ___androidUtils_0)); }
	inline RuntimeObject* get_androidUtils_0() const { return ___androidUtils_0; }
	inline RuntimeObject** get_address_of_androidUtils_0() { return &___androidUtils_0; }
	inline void set_androidUtils_0(RuntimeObject* value)
	{
		___androidUtils_0 = value;
		Il2CppCodeGenWriteBarrier((&___androidUtils_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDNATIVEPOPUPS_TFCDF1E0DDBEC0C449E8C74993CE36894D3862368_H
#ifndef IOSNATIVEPOPUPS_T6C8DB9578139592203A95BAB1632A0891D2AAA4B_H
#define IOSNATIVEPOPUPS_T6C8DB9578139592203A95BAB1632A0891D2AAA4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.IosNativePopups
struct  IosNativePopups_t6C8DB9578139592203A95BAB1632A0891D2AAA4B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNATIVEPOPUPS_T6C8DB9578139592203A95BAB1632A0891D2AAA4B_H
#ifndef MOBILENATIVEPOPUPS_T4171E4F9ED145F876E9953246D168B2CBA6F00D0_H
#define MOBILENATIVEPOPUPS_T4171E4F9ED145F876E9953246D168B2CBA6F00D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.MobileNativePopups
struct  MobileNativePopups_t4171E4F9ED145F876E9953246D168B2CBA6F00D0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILENATIVEPOPUPS_T4171E4F9ED145F876E9953246D168B2CBA6F00D0_H
#ifndef U3COPENALERTDIALOGU3EC__ANONSTOREY0_T7AA3F28020ACCEAB250731370FC4D05F470B4B18_H
#define U3COPENALERTDIALOGU3EC__ANONSTOREY0_T7AA3F28020ACCEAB250731370FC4D05F470B4B18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey0
struct  U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18  : public RuntimeObject
{
public:
	// System.Action TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey0::onCancel
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onCancel_0;

public:
	inline static int32_t get_offset_of_onCancel_0() { return static_cast<int32_t>(offsetof(U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18, ___onCancel_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onCancel_0() const { return ___onCancel_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onCancel_0() { return &___onCancel_0; }
	inline void set_onCancel_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onCancel_0 = value;
		Il2CppCodeGenWriteBarrier((&___onCancel_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3COPENALERTDIALOGU3EC__ANONSTOREY0_T7AA3F28020ACCEAB250731370FC4D05F470B4B18_H
#ifndef U3COPENALERTDIALOGU3EC__ANONSTOREY1_T464A898B52C251237E9C1DA5250B421534114C72_H
#define U3COPENALERTDIALOGU3EC__ANONSTOREY1_T464A898B52C251237E9C1DA5250B421534114C72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey1
struct  U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72  : public RuntimeObject
{
public:
	// System.Action TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey1::onCancel
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onCancel_0;
	// System.Action TheNextFlow.UnityPlugins.MobileNativePopups/<OpenAlertDialog>c__AnonStorey1::onOk
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onOk_1;

public:
	inline static int32_t get_offset_of_onCancel_0() { return static_cast<int32_t>(offsetof(U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72, ___onCancel_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onCancel_0() const { return ___onCancel_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onCancel_0() { return &___onCancel_0; }
	inline void set_onCancel_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onCancel_0 = value;
		Il2CppCodeGenWriteBarrier((&___onCancel_0), value);
	}

	inline static int32_t get_offset_of_onOk_1() { return static_cast<int32_t>(offsetof(U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72, ___onOk_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onOk_1() const { return ___onOk_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onOk_1() { return &___onOk_1; }
	inline void set_onOk_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onOk_1 = value;
		Il2CppCodeGenWriteBarrier((&___onOk_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3COPENALERTDIALOGU3EC__ANONSTOREY1_T464A898B52C251237E9C1DA5250B421534114C72_H
#ifndef XRDEVICE_T392FCA3D1DCEB95FF500C8F374C88B034C31DF4A_H
#define XRDEVICE_T392FCA3D1DCEB95FF500C8F374C88B034C31DF4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.XRDevice
struct  XRDevice_t392FCA3D1DCEB95FF500C8F374C88B034C31DF4A  : public RuntimeObject
{
public:

public:
};

struct XRDevice_t392FCA3D1DCEB95FF500C8F374C88B034C31DF4A_StaticFields
{
public:
	// System.Action`1<System.String> UnityEngine.XR.XRDevice::deviceLoaded
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___deviceLoaded_0;

public:
	inline static int32_t get_offset_of_deviceLoaded_0() { return static_cast<int32_t>(offsetof(XRDevice_t392FCA3D1DCEB95FF500C8F374C88B034C31DF4A_StaticFields, ___deviceLoaded_0)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_deviceLoaded_0() const { return ___deviceLoaded_0; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_deviceLoaded_0() { return &___deviceLoaded_0; }
	inline void set_deviceLoaded_0(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___deviceLoaded_0 = value;
		Il2CppCodeGenWriteBarrier((&___deviceLoaded_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRDEVICE_T392FCA3D1DCEB95FF500C8F374C88B034C31DF4A_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_TF2C68FFE572BEBC9BD9FFBA4B794164E3A87CBED_H
#define MONOPINVOKECALLBACKATTRIBUTE_TF2C68FFE572BEBC9BD9FFBA4B794164E3A87CBED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_tF2C68FFE572BEBC9BD9FFBA4B794164E3A87CBED  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_TF2C68FFE572BEBC9BD9FFBA4B794164E3A87CBED_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#define PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyDescriptor
struct  PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D  : public MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8
{
public:
	// System.ComponentModel.TypeConverter System.ComponentModel.PropertyDescriptor::converter
	TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * ___converter_12;
	// System.Collections.Hashtable System.ComponentModel.PropertyDescriptor::valueChangedHandlers
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___valueChangedHandlers_13;
	// System.Object[] System.ComponentModel.PropertyDescriptor::editors
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___editors_14;
	// System.Type[] System.ComponentModel.PropertyDescriptor::editorTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___editorTypes_15;
	// System.Int32 System.ComponentModel.PropertyDescriptor::editorCount
	int32_t ___editorCount_16;

public:
	inline static int32_t get_offset_of_converter_12() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___converter_12)); }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * get_converter_12() const { return ___converter_12; }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB ** get_address_of_converter_12() { return &___converter_12; }
	inline void set_converter_12(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * value)
	{
		___converter_12 = value;
		Il2CppCodeGenWriteBarrier((&___converter_12), value);
	}

	inline static int32_t get_offset_of_valueChangedHandlers_13() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___valueChangedHandlers_13)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_valueChangedHandlers_13() const { return ___valueChangedHandlers_13; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_valueChangedHandlers_13() { return &___valueChangedHandlers_13; }
	inline void set_valueChangedHandlers_13(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___valueChangedHandlers_13 = value;
		Il2CppCodeGenWriteBarrier((&___valueChangedHandlers_13), value);
	}

	inline static int32_t get_offset_of_editors_14() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editors_14)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_editors_14() const { return ___editors_14; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_editors_14() { return &___editors_14; }
	inline void set_editors_14(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___editors_14 = value;
		Il2CppCodeGenWriteBarrier((&___editors_14), value);
	}

	inline static int32_t get_offset_of_editorTypes_15() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorTypes_15)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_editorTypes_15() const { return ___editorTypes_15; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_editorTypes_15() { return &___editorTypes_15; }
	inline void set_editorTypes_15(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___editorTypes_15 = value;
		Il2CppCodeGenWriteBarrier((&___editorTypes_15), value);
	}

	inline static int32_t get_offset_of_editorCount_16() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorCount_16)); }
	inline int32_t get_editorCount_16() const { return ___editorCount_16; }
	inline int32_t* get_address_of_editorCount_16() { return &___editorCount_16; }
	inline void set_editorCount_16(int32_t value)
	{
		___editorCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifndef AUTOINCREMENTINT64_TC7974B9725E9D62D6ACC9EEBCEDC88F25063B486_H
#define AUTOINCREMENTINT64_TC7974B9725E9D62D6ACC9EEBCEDC88F25063B486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.AutoIncrementInt64
struct  AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486  : public AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086
{
public:
	// System.Int64 System.Data.AutoIncrementInt64::_current
	int64_t ____current_1;
	// System.Int64 System.Data.AutoIncrementInt64::_seed
	int64_t ____seed_2;
	// System.Int64 System.Data.AutoIncrementInt64::_step
	int64_t ____step_3;

public:
	inline static int32_t get_offset_of__current_1() { return static_cast<int32_t>(offsetof(AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486, ____current_1)); }
	inline int64_t get__current_1() const { return ____current_1; }
	inline int64_t* get_address_of__current_1() { return &____current_1; }
	inline void set__current_1(int64_t value)
	{
		____current_1 = value;
	}

	inline static int32_t get_offset_of__seed_2() { return static_cast<int32_t>(offsetof(AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486, ____seed_2)); }
	inline int64_t get__seed_2() const { return ____seed_2; }
	inline int64_t* get_address_of__seed_2() { return &____seed_2; }
	inline void set__seed_2(int64_t value)
	{
		____seed_2 = value;
	}

	inline static int32_t get_offset_of__step_3() { return static_cast<int32_t>(offsetof(AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486, ____step_3)); }
	inline int64_t get__step_3() const { return ____step_3; }
	inline int64_t* get_address_of__step_3() { return &____step_3; }
	inline void set__step_3(int64_t value)
	{
		____step_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOINCREMENTINT64_TC7974B9725E9D62D6ACC9EEBCEDC88F25063B486_H
#ifndef CONSTRAINTCOLLECTION_T349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62_H
#define CONSTRAINTCOLLECTION_T349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ConstraintCollection
struct  ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62  : public InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334
{
public:
	// System.Data.DataTable System.Data.ConstraintCollection::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_1;
	// System.Collections.ArrayList System.Data.ConstraintCollection::_list
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____list_2;
	// System.Int32 System.Data.ConstraintCollection::_defaultNameIndex
	int32_t ____defaultNameIndex_3;
	// System.ComponentModel.CollectionChangeEventHandler System.Data.ConstraintCollection::_onCollectionChanged
	CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * ____onCollectionChanged_4;
	// System.Data.Constraint[] System.Data.ConstraintCollection::_delayLoadingConstraints
	ConstraintU5BU5D_t7E694ACFAC8AA785DDDB7F40FC22F2A72E970557* ____delayLoadingConstraints_5;
	// System.Boolean System.Data.ConstraintCollection::_fLoadForeignKeyConstraintsOnly
	bool ____fLoadForeignKeyConstraintsOnly_6;

public:
	inline static int32_t get_offset_of__table_1() { return static_cast<int32_t>(offsetof(ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62, ____table_1)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_1() const { return ____table_1; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_1() { return &____table_1; }
	inline void set__table_1(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_1 = value;
		Il2CppCodeGenWriteBarrier((&____table_1), value);
	}

	inline static int32_t get_offset_of__list_2() { return static_cast<int32_t>(offsetof(ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62, ____list_2)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__list_2() const { return ____list_2; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__list_2() { return &____list_2; }
	inline void set__list_2(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____list_2 = value;
		Il2CppCodeGenWriteBarrier((&____list_2), value);
	}

	inline static int32_t get_offset_of__defaultNameIndex_3() { return static_cast<int32_t>(offsetof(ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62, ____defaultNameIndex_3)); }
	inline int32_t get__defaultNameIndex_3() const { return ____defaultNameIndex_3; }
	inline int32_t* get_address_of__defaultNameIndex_3() { return &____defaultNameIndex_3; }
	inline void set__defaultNameIndex_3(int32_t value)
	{
		____defaultNameIndex_3 = value;
	}

	inline static int32_t get_offset_of__onCollectionChanged_4() { return static_cast<int32_t>(offsetof(ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62, ____onCollectionChanged_4)); }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * get__onCollectionChanged_4() const { return ____onCollectionChanged_4; }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 ** get_address_of__onCollectionChanged_4() { return &____onCollectionChanged_4; }
	inline void set__onCollectionChanged_4(CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * value)
	{
		____onCollectionChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&____onCollectionChanged_4), value);
	}

	inline static int32_t get_offset_of__delayLoadingConstraints_5() { return static_cast<int32_t>(offsetof(ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62, ____delayLoadingConstraints_5)); }
	inline ConstraintU5BU5D_t7E694ACFAC8AA785DDDB7F40FC22F2A72E970557* get__delayLoadingConstraints_5() const { return ____delayLoadingConstraints_5; }
	inline ConstraintU5BU5D_t7E694ACFAC8AA785DDDB7F40FC22F2A72E970557** get_address_of__delayLoadingConstraints_5() { return &____delayLoadingConstraints_5; }
	inline void set__delayLoadingConstraints_5(ConstraintU5BU5D_t7E694ACFAC8AA785DDDB7F40FC22F2A72E970557* value)
	{
		____delayLoadingConstraints_5 = value;
		Il2CppCodeGenWriteBarrier((&____delayLoadingConstraints_5), value);
	}

	inline static int32_t get_offset_of__fLoadForeignKeyConstraintsOnly_6() { return static_cast<int32_t>(offsetof(ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62, ____fLoadForeignKeyConstraintsOnly_6)); }
	inline bool get__fLoadForeignKeyConstraintsOnly_6() const { return ____fLoadForeignKeyConstraintsOnly_6; }
	inline bool* get_address_of__fLoadForeignKeyConstraintsOnly_6() { return &____fLoadForeignKeyConstraintsOnly_6; }
	inline void set__fLoadForeignKeyConstraintsOnly_6(bool value)
	{
		____fLoadForeignKeyConstraintsOnly_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTCOLLECTION_T349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62_H
#ifndef DATACOLUMNCHANGEEVENTARGS_T60FA69137EA2CF2B173B3E61C26D423EABD3E280_H
#define DATACOLUMNCHANGEEVENTARGS_T60FA69137EA2CF2B173B3E61C26D423EABD3E280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataColumnChangeEventArgs
struct  DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Data.DataColumn System.Data.DataColumnChangeEventArgs::_column
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * ____column_1;
	// System.Data.DataRow System.Data.DataColumnChangeEventArgs::<Row>k__BackingField
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B * ___U3CRowU3Ek__BackingField_2;
	// System.Object System.Data.DataColumnChangeEventArgs::<ProposedValue>k__BackingField
	RuntimeObject * ___U3CProposedValueU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__column_1() { return static_cast<int32_t>(offsetof(DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280, ____column_1)); }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * get__column_1() const { return ____column_1; }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D ** get_address_of__column_1() { return &____column_1; }
	inline void set__column_1(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * value)
	{
		____column_1 = value;
		Il2CppCodeGenWriteBarrier((&____column_1), value);
	}

	inline static int32_t get_offset_of_U3CRowU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280, ___U3CRowU3Ek__BackingField_2)); }
	inline DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B * get_U3CRowU3Ek__BackingField_2() const { return ___U3CRowU3Ek__BackingField_2; }
	inline DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B ** get_address_of_U3CRowU3Ek__BackingField_2() { return &___U3CRowU3Ek__BackingField_2; }
	inline void set_U3CRowU3Ek__BackingField_2(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B * value)
	{
		___U3CRowU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRowU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CProposedValueU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280, ___U3CProposedValueU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CProposedValueU3Ek__BackingField_3() const { return ___U3CProposedValueU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CProposedValueU3Ek__BackingField_3() { return &___U3CProposedValueU3Ek__BackingField_3; }
	inline void set_U3CProposedValueU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CProposedValueU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProposedValueU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACOLUMNCHANGEEVENTARGS_T60FA69137EA2CF2B173B3E61C26D423EABD3E280_H
#ifndef DATACOLUMNCOLLECTION_T398628201192B6EF9DB23A650DAB1E79CEA1796A_H
#define DATACOLUMNCOLLECTION_T398628201192B6EF9DB23A650DAB1E79CEA1796A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataColumnCollection
struct  DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A  : public InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334
{
public:
	// System.Data.DataTable System.Data.DataColumnCollection::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_1;
	// System.Collections.ArrayList System.Data.DataColumnCollection::_list
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____list_2;
	// System.Int32 System.Data.DataColumnCollection::_defaultNameIndex
	int32_t ____defaultNameIndex_3;
	// System.Data.DataColumn[] System.Data.DataColumnCollection::_delayedAddRangeColumns
	DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* ____delayedAddRangeColumns_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Data.DataColumn> System.Data.DataColumnCollection::_columnFromName
	Dictionary_2_t097E7AE4AAA0034CC5A6B941AC4C115EA7AFC199 * ____columnFromName_5;
	// System.Boolean System.Data.DataColumnCollection::_fInClear
	bool ____fInClear_6;
	// System.Data.DataColumn[] System.Data.DataColumnCollection::_columnsImplementingIChangeTracking
	DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* ____columnsImplementingIChangeTracking_7;
	// System.Int32 System.Data.DataColumnCollection::_nColumnsImplementingIChangeTracking
	int32_t ____nColumnsImplementingIChangeTracking_8;
	// System.Int32 System.Data.DataColumnCollection::_nColumnsImplementingIRevertibleChangeTracking
	int32_t ____nColumnsImplementingIRevertibleChangeTracking_9;
	// System.ComponentModel.CollectionChangeEventHandler System.Data.DataColumnCollection::CollectionChanged
	CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * ___CollectionChanged_10;
	// System.ComponentModel.CollectionChangeEventHandler System.Data.DataColumnCollection::CollectionChanging
	CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * ___CollectionChanging_11;
	// System.ComponentModel.CollectionChangeEventHandler System.Data.DataColumnCollection::ColumnPropertyChanged
	CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * ___ColumnPropertyChanged_12;

public:
	inline static int32_t get_offset_of__table_1() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____table_1)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_1() const { return ____table_1; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_1() { return &____table_1; }
	inline void set__table_1(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_1 = value;
		Il2CppCodeGenWriteBarrier((&____table_1), value);
	}

	inline static int32_t get_offset_of__list_2() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____list_2)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__list_2() const { return ____list_2; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__list_2() { return &____list_2; }
	inline void set__list_2(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____list_2 = value;
		Il2CppCodeGenWriteBarrier((&____list_2), value);
	}

	inline static int32_t get_offset_of__defaultNameIndex_3() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____defaultNameIndex_3)); }
	inline int32_t get__defaultNameIndex_3() const { return ____defaultNameIndex_3; }
	inline int32_t* get_address_of__defaultNameIndex_3() { return &____defaultNameIndex_3; }
	inline void set__defaultNameIndex_3(int32_t value)
	{
		____defaultNameIndex_3 = value;
	}

	inline static int32_t get_offset_of__delayedAddRangeColumns_4() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____delayedAddRangeColumns_4)); }
	inline DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* get__delayedAddRangeColumns_4() const { return ____delayedAddRangeColumns_4; }
	inline DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC** get_address_of__delayedAddRangeColumns_4() { return &____delayedAddRangeColumns_4; }
	inline void set__delayedAddRangeColumns_4(DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* value)
	{
		____delayedAddRangeColumns_4 = value;
		Il2CppCodeGenWriteBarrier((&____delayedAddRangeColumns_4), value);
	}

	inline static int32_t get_offset_of__columnFromName_5() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____columnFromName_5)); }
	inline Dictionary_2_t097E7AE4AAA0034CC5A6B941AC4C115EA7AFC199 * get__columnFromName_5() const { return ____columnFromName_5; }
	inline Dictionary_2_t097E7AE4AAA0034CC5A6B941AC4C115EA7AFC199 ** get_address_of__columnFromName_5() { return &____columnFromName_5; }
	inline void set__columnFromName_5(Dictionary_2_t097E7AE4AAA0034CC5A6B941AC4C115EA7AFC199 * value)
	{
		____columnFromName_5 = value;
		Il2CppCodeGenWriteBarrier((&____columnFromName_5), value);
	}

	inline static int32_t get_offset_of__fInClear_6() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____fInClear_6)); }
	inline bool get__fInClear_6() const { return ____fInClear_6; }
	inline bool* get_address_of__fInClear_6() { return &____fInClear_6; }
	inline void set__fInClear_6(bool value)
	{
		____fInClear_6 = value;
	}

	inline static int32_t get_offset_of__columnsImplementingIChangeTracking_7() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____columnsImplementingIChangeTracking_7)); }
	inline DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* get__columnsImplementingIChangeTracking_7() const { return ____columnsImplementingIChangeTracking_7; }
	inline DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC** get_address_of__columnsImplementingIChangeTracking_7() { return &____columnsImplementingIChangeTracking_7; }
	inline void set__columnsImplementingIChangeTracking_7(DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* value)
	{
		____columnsImplementingIChangeTracking_7 = value;
		Il2CppCodeGenWriteBarrier((&____columnsImplementingIChangeTracking_7), value);
	}

	inline static int32_t get_offset_of__nColumnsImplementingIChangeTracking_8() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____nColumnsImplementingIChangeTracking_8)); }
	inline int32_t get__nColumnsImplementingIChangeTracking_8() const { return ____nColumnsImplementingIChangeTracking_8; }
	inline int32_t* get_address_of__nColumnsImplementingIChangeTracking_8() { return &____nColumnsImplementingIChangeTracking_8; }
	inline void set__nColumnsImplementingIChangeTracking_8(int32_t value)
	{
		____nColumnsImplementingIChangeTracking_8 = value;
	}

	inline static int32_t get_offset_of__nColumnsImplementingIRevertibleChangeTracking_9() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____nColumnsImplementingIRevertibleChangeTracking_9)); }
	inline int32_t get__nColumnsImplementingIRevertibleChangeTracking_9() const { return ____nColumnsImplementingIRevertibleChangeTracking_9; }
	inline int32_t* get_address_of__nColumnsImplementingIRevertibleChangeTracking_9() { return &____nColumnsImplementingIRevertibleChangeTracking_9; }
	inline void set__nColumnsImplementingIRevertibleChangeTracking_9(int32_t value)
	{
		____nColumnsImplementingIRevertibleChangeTracking_9 = value;
	}

	inline static int32_t get_offset_of_CollectionChanged_10() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ___CollectionChanged_10)); }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * get_CollectionChanged_10() const { return ___CollectionChanged_10; }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 ** get_address_of_CollectionChanged_10() { return &___CollectionChanged_10; }
	inline void set_CollectionChanged_10(CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * value)
	{
		___CollectionChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___CollectionChanged_10), value);
	}

	inline static int32_t get_offset_of_CollectionChanging_11() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ___CollectionChanging_11)); }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * get_CollectionChanging_11() const { return ___CollectionChanging_11; }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 ** get_address_of_CollectionChanging_11() { return &___CollectionChanging_11; }
	inline void set_CollectionChanging_11(CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * value)
	{
		___CollectionChanging_11 = value;
		Il2CppCodeGenWriteBarrier((&___CollectionChanging_11), value);
	}

	inline static int32_t get_offset_of_ColumnPropertyChanged_12() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ___ColumnPropertyChanged_12)); }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * get_ColumnPropertyChanged_12() const { return ___ColumnPropertyChanged_12; }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 ** get_address_of_ColumnPropertyChanged_12() { return &___ColumnPropertyChanged_12; }
	inline void set_ColumnPropertyChanged_12(CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * value)
	{
		___ColumnPropertyChanged_12 = value;
		Il2CppCodeGenWriteBarrier((&___ColumnPropertyChanged_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACOLUMNCOLLECTION_T398628201192B6EF9DB23A650DAB1E79CEA1796A_H
#ifndef DATACOMMONEVENTSOURCE_T01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_H
#define DATACOMMONEVENTSOURCE_T01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataCommonEventSource
struct  DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82  : public EventSource_t263F509672F3C6747C5BA393F20E2717B7A981EB
{
public:

public:
};

struct DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_StaticFields
{
public:
	// System.Data.DataCommonEventSource System.Data.DataCommonEventSource::Log
	DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82 * ___Log_1;
	// System.Int64 System.Data.DataCommonEventSource::s_nextScopeId
	int64_t ___s_nextScopeId_2;

public:
	inline static int32_t get_offset_of_Log_1() { return static_cast<int32_t>(offsetof(DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_StaticFields, ___Log_1)); }
	inline DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82 * get_Log_1() const { return ___Log_1; }
	inline DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82 ** get_address_of_Log_1() { return &___Log_1; }
	inline void set_Log_1(DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82 * value)
	{
		___Log_1 = value;
		Il2CppCodeGenWriteBarrier((&___Log_1), value);
	}

	inline static int32_t get_offset_of_s_nextScopeId_2() { return static_cast<int32_t>(offsetof(DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_StaticFields, ___s_nextScopeId_2)); }
	inline int64_t get_s_nextScopeId_2() const { return ___s_nextScopeId_2; }
	inline int64_t* get_address_of_s_nextScopeId_2() { return &___s_nextScopeId_2; }
	inline void set_s_nextScopeId_2(int64_t value)
	{
		___s_nextScopeId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACOMMONEVENTSOURCE_T01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_H
#ifndef COLUMNERROR_T24D74F834948955F6EC5949F7D308787C8AE77C3_H
#define COLUMNERROR_T24D74F834948955F6EC5949F7D308787C8AE77C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataError/ColumnError
struct  ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3 
{
public:
	// System.Data.DataColumn System.Data.DataError/ColumnError::_column
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * ____column_0;
	// System.String System.Data.DataError/ColumnError::_error
	String_t* ____error_1;

public:
	inline static int32_t get_offset_of__column_0() { return static_cast<int32_t>(offsetof(ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3, ____column_0)); }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * get__column_0() const { return ____column_0; }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D ** get_address_of__column_0() { return &____column_0; }
	inline void set__column_0(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * value)
	{
		____column_0 = value;
		Il2CppCodeGenWriteBarrier((&____column_0), value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3, ____error_1)); }
	inline String_t* get__error_1() const { return ____error_1; }
	inline String_t** get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(String_t* value)
	{
		____error_1 = value;
		Il2CppCodeGenWriteBarrier((&____error_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Data.DataError/ColumnError
struct ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3_marshaled_pinvoke
{
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * ____column_0;
	char* ____error_1;
};
// Native definition for COM marshalling of System.Data.DataError/ColumnError
struct ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3_marshaled_com
{
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * ____column_0;
	Il2CppChar* ____error_1;
};
#endif // COLUMNERROR_T24D74F834948955F6EC5949F7D308787C8AE77C3_H
#ifndef DATAKEY_TCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B_H
#define DATAKEY_TCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataKey
struct  DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B 
{
public:
	// System.Data.DataColumn[] System.Data.DataKey::_columns
	DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* ____columns_0;

public:
	inline static int32_t get_offset_of__columns_0() { return static_cast<int32_t>(offsetof(DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B, ____columns_0)); }
	inline DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* get__columns_0() const { return ____columns_0; }
	inline DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC** get_address_of__columns_0() { return &____columns_0; }
	inline void set__columns_0(DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* value)
	{
		____columns_0 = value;
		Il2CppCodeGenWriteBarrier((&____columns_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Data.DataKey
struct DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B_marshaled_pinvoke
{
	DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* ____columns_0;
};
// Native definition for COM marshalling of System.Data.DataKey
struct DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B_marshaled_com
{
	DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* ____columns_0;
};
#endif // DATAKEY_TCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B_H
#ifndef DATARELATIONCOLLECTION_TB592C84F2EE6B60DFB933CC67B8DE1065098269B_H
#define DATARELATIONCOLLECTION_TB592C84F2EE6B60DFB933CC67B8DE1065098269B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRelationCollection
struct  DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B  : public InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334
{
public:
	// System.Data.DataRelation System.Data.DataRelationCollection::_inTransition
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 * ____inTransition_1;
	// System.Int32 System.Data.DataRelationCollection::_defaultNameIndex
	int32_t ____defaultNameIndex_2;
	// System.ComponentModel.CollectionChangeEventHandler System.Data.DataRelationCollection::_onCollectionChangedDelegate
	CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * ____onCollectionChangedDelegate_3;
	// System.ComponentModel.CollectionChangeEventHandler System.Data.DataRelationCollection::_onCollectionChangingDelegate
	CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * ____onCollectionChangingDelegate_4;
	// System.Int32 System.Data.DataRelationCollection::_objectID
	int32_t ____objectID_6;

public:
	inline static int32_t get_offset_of__inTransition_1() { return static_cast<int32_t>(offsetof(DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B, ____inTransition_1)); }
	inline DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 * get__inTransition_1() const { return ____inTransition_1; }
	inline DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 ** get_address_of__inTransition_1() { return &____inTransition_1; }
	inline void set__inTransition_1(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 * value)
	{
		____inTransition_1 = value;
		Il2CppCodeGenWriteBarrier((&____inTransition_1), value);
	}

	inline static int32_t get_offset_of__defaultNameIndex_2() { return static_cast<int32_t>(offsetof(DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B, ____defaultNameIndex_2)); }
	inline int32_t get__defaultNameIndex_2() const { return ____defaultNameIndex_2; }
	inline int32_t* get_address_of__defaultNameIndex_2() { return &____defaultNameIndex_2; }
	inline void set__defaultNameIndex_2(int32_t value)
	{
		____defaultNameIndex_2 = value;
	}

	inline static int32_t get_offset_of__onCollectionChangedDelegate_3() { return static_cast<int32_t>(offsetof(DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B, ____onCollectionChangedDelegate_3)); }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * get__onCollectionChangedDelegate_3() const { return ____onCollectionChangedDelegate_3; }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 ** get_address_of__onCollectionChangedDelegate_3() { return &____onCollectionChangedDelegate_3; }
	inline void set__onCollectionChangedDelegate_3(CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * value)
	{
		____onCollectionChangedDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((&____onCollectionChangedDelegate_3), value);
	}

	inline static int32_t get_offset_of__onCollectionChangingDelegate_4() { return static_cast<int32_t>(offsetof(DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B, ____onCollectionChangingDelegate_4)); }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * get__onCollectionChangingDelegate_4() const { return ____onCollectionChangingDelegate_4; }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 ** get_address_of__onCollectionChangingDelegate_4() { return &____onCollectionChangingDelegate_4; }
	inline void set__onCollectionChangingDelegate_4(CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * value)
	{
		____onCollectionChangingDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((&____onCollectionChangingDelegate_4), value);
	}

	inline static int32_t get_offset_of__objectID_6() { return static_cast<int32_t>(offsetof(DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B, ____objectID_6)); }
	inline int32_t get__objectID_6() const { return ____objectID_6; }
	inline int32_t* get_address_of__objectID_6() { return &____objectID_6; }
	inline void set__objectID_6(int32_t value)
	{
		____objectID_6 = value;
	}
};

struct DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B_StaticFields
{
public:
	// System.Int32 System.Data.DataRelationCollection::s_objectTypeCount
	int32_t ___s_objectTypeCount_5;

public:
	inline static int32_t get_offset_of_s_objectTypeCount_5() { return static_cast<int32_t>(offsetof(DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B_StaticFields, ___s_objectTypeCount_5)); }
	inline int32_t get_s_objectTypeCount_5() const { return ___s_objectTypeCount_5; }
	inline int32_t* get_address_of_s_objectTypeCount_5() { return &___s_objectTypeCount_5; }
	inline void set_s_objectTypeCount_5(int32_t value)
	{
		___s_objectTypeCount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATARELATIONCOLLECTION_TB592C84F2EE6B60DFB933CC67B8DE1065098269B_H
#ifndef DATAROWCOLLECTION_T45B9FDFC3667C7FA9C0F7F14787B5ED70DC673E0_H
#define DATAROWCOLLECTION_T45B9FDFC3667C7FA9C0F7F14787B5ED70DC673E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRowCollection
struct  DataRowCollection_t45B9FDFC3667C7FA9C0F7F14787B5ED70DC673E0  : public InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334
{
public:
	// System.Data.DataTable System.Data.DataRowCollection::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_1;
	// System.Data.DataRowCollection/DataRowTree System.Data.DataRowCollection::_list
	DataRowTree_t885C3CBC17060B726BFEE177710D6E9E57FEA230 * ____list_2;
	// System.Int32 System.Data.DataRowCollection::_nullInList
	int32_t ____nullInList_3;

public:
	inline static int32_t get_offset_of__table_1() { return static_cast<int32_t>(offsetof(DataRowCollection_t45B9FDFC3667C7FA9C0F7F14787B5ED70DC673E0, ____table_1)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_1() const { return ____table_1; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_1() { return &____table_1; }
	inline void set__table_1(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_1 = value;
		Il2CppCodeGenWriteBarrier((&____table_1), value);
	}

	inline static int32_t get_offset_of__list_2() { return static_cast<int32_t>(offsetof(DataRowCollection_t45B9FDFC3667C7FA9C0F7F14787B5ED70DC673E0, ____list_2)); }
	inline DataRowTree_t885C3CBC17060B726BFEE177710D6E9E57FEA230 * get__list_2() const { return ____list_2; }
	inline DataRowTree_t885C3CBC17060B726BFEE177710D6E9E57FEA230 ** get_address_of__list_2() { return &____list_2; }
	inline void set__list_2(DataRowTree_t885C3CBC17060B726BFEE177710D6E9E57FEA230 * value)
	{
		____list_2 = value;
		Il2CppCodeGenWriteBarrier((&____list_2), value);
	}

	inline static int32_t get_offset_of__nullInList_3() { return static_cast<int32_t>(offsetof(DataRowCollection_t45B9FDFC3667C7FA9C0F7F14787B5ED70DC673E0, ____nullInList_3)); }
	inline int32_t get__nullInList_3() const { return ____nullInList_3; }
	inline int32_t* get_address_of__nullInList_3() { return &____nullInList_3; }
	inline void set__nullInList_3(int32_t value)
	{
		____nullInList_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAROWCOLLECTION_T45B9FDFC3667C7FA9C0F7F14787B5ED70DC673E0_H
#ifndef FOREIGNKEYCONSTRAINTENUMERATOR_T555045F1D5AC5CF9D756F3A5955790BB8C7C3C6F_H
#define FOREIGNKEYCONSTRAINTENUMERATOR_T555045F1D5AC5CF9D756F3A5955790BB8C7C3C6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ForeignKeyConstraintEnumerator
struct  ForeignKeyConstraintEnumerator_t555045F1D5AC5CF9D756F3A5955790BB8C7C3C6F  : public ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOREIGNKEYCONSTRAINTENUMERATOR_T555045F1D5AC5CF9D756F3A5955790BB8C7C3C6F_H
#ifndef DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#define DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef NUMBERBUFFER_T523F9CCA824CB72F54893DDB17C47553B942A2F8_H
#define NUMBERBUFFER_T523F9CCA824CB72F54893DDB17C47553B942A2F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.FormatProvider/Number/NumberBuffer
struct  NumberBuffer_t523F9CCA824CB72F54893DDB17C47553B942A2F8 
{
public:
	// System.Int32 System.Globalization.FormatProvider/Number/NumberBuffer::precision
	int32_t ___precision_0;
	// System.Int32 System.Globalization.FormatProvider/Number/NumberBuffer::scale
	int32_t ___scale_1;
	// System.Boolean System.Globalization.FormatProvider/Number/NumberBuffer::sign
	bool ___sign_2;
	// System.Char* System.Globalization.FormatProvider/Number/NumberBuffer::overrideDigits
	Il2CppChar* ___overrideDigits_3;

public:
	inline static int32_t get_offset_of_precision_0() { return static_cast<int32_t>(offsetof(NumberBuffer_t523F9CCA824CB72F54893DDB17C47553B942A2F8, ___precision_0)); }
	inline int32_t get_precision_0() const { return ___precision_0; }
	inline int32_t* get_address_of_precision_0() { return &___precision_0; }
	inline void set_precision_0(int32_t value)
	{
		___precision_0 = value;
	}

	inline static int32_t get_offset_of_scale_1() { return static_cast<int32_t>(offsetof(NumberBuffer_t523F9CCA824CB72F54893DDB17C47553B942A2F8, ___scale_1)); }
	inline int32_t get_scale_1() const { return ___scale_1; }
	inline int32_t* get_address_of_scale_1() { return &___scale_1; }
	inline void set_scale_1(int32_t value)
	{
		___scale_1 = value;
	}

	inline static int32_t get_offset_of_sign_2() { return static_cast<int32_t>(offsetof(NumberBuffer_t523F9CCA824CB72F54893DDB17C47553B942A2F8, ___sign_2)); }
	inline bool get_sign_2() const { return ___sign_2; }
	inline bool* get_address_of_sign_2() { return &___sign_2; }
	inline void set_sign_2(bool value)
	{
		___sign_2 = value;
	}

	inline static int32_t get_offset_of_overrideDigits_3() { return static_cast<int32_t>(offsetof(NumberBuffer_t523F9CCA824CB72F54893DDB17C47553B942A2F8, ___overrideDigits_3)); }
	inline Il2CppChar* get_overrideDigits_3() const { return ___overrideDigits_3; }
	inline Il2CppChar** get_address_of_overrideDigits_3() { return &___overrideDigits_3; }
	inline void set_overrideDigits_3(Il2CppChar* value)
	{
		___overrideDigits_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Globalization.FormatProvider/Number/NumberBuffer
struct NumberBuffer_t523F9CCA824CB72F54893DDB17C47553B942A2F8_marshaled_pinvoke
{
	int32_t ___precision_0;
	int32_t ___scale_1;
	int32_t ___sign_2;
	Il2CppChar* ___overrideDigits_3;
};
// Native definition for COM marshalling of System.Globalization.FormatProvider/Number/NumberBuffer
struct NumberBuffer_t523F9CCA824CB72F54893DDB17C47553B942A2F8_marshaled_com
{
	int32_t ___precision_0;
	int32_t ___scale_1;
	int32_t ___sign_2;
	Il2CppChar* ___overrideDigits_3;
};
#endif // NUMBERBUFFER_T523F9CCA824CB72F54893DDB17C47553B942A2F8_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#define INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BIGINTEGER_T01F3792AFD6865BDF469CC7C7867761F3922BCEC_H
#define BIGINTEGER_T01F3792AFD6865BDF469CC7C7867761F3922BCEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Numerics.BigInteger
struct  BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC 
{
public:
	// System.Int32 System.Numerics.BigInteger::_sign
	int32_t ____sign_0;
	// System.UInt32[] System.Numerics.BigInteger::_bits
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____bits_1;

public:
	inline static int32_t get_offset_of__sign_0() { return static_cast<int32_t>(offsetof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC, ____sign_0)); }
	inline int32_t get__sign_0() const { return ____sign_0; }
	inline int32_t* get_address_of__sign_0() { return &____sign_0; }
	inline void set__sign_0(int32_t value)
	{
		____sign_0 = value;
	}

	inline static int32_t get_offset_of__bits_1() { return static_cast<int32_t>(offsetof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC, ____bits_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__bits_1() const { return ____bits_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__bits_1() { return &____bits_1; }
	inline void set__bits_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____bits_1 = value;
		Il2CppCodeGenWriteBarrier((&____bits_1), value);
	}
};

struct BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields
{
public:
	// System.Numerics.BigInteger System.Numerics.BigInteger::s_bnMinInt
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  ___s_bnMinInt_2;
	// System.Numerics.BigInteger System.Numerics.BigInteger::s_bnOneInt
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  ___s_bnOneInt_3;
	// System.Numerics.BigInteger System.Numerics.BigInteger::s_bnZeroInt
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  ___s_bnZeroInt_4;
	// System.Numerics.BigInteger System.Numerics.BigInteger::s_bnMinusOneInt
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  ___s_bnMinusOneInt_5;
	// System.Byte[] System.Numerics.BigInteger::s_success
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___s_success_6;

public:
	inline static int32_t get_offset_of_s_bnMinInt_2() { return static_cast<int32_t>(offsetof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields, ___s_bnMinInt_2)); }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  get_s_bnMinInt_2() const { return ___s_bnMinInt_2; }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC * get_address_of_s_bnMinInt_2() { return &___s_bnMinInt_2; }
	inline void set_s_bnMinInt_2(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  value)
	{
		___s_bnMinInt_2 = value;
	}

	inline static int32_t get_offset_of_s_bnOneInt_3() { return static_cast<int32_t>(offsetof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields, ___s_bnOneInt_3)); }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  get_s_bnOneInt_3() const { return ___s_bnOneInt_3; }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC * get_address_of_s_bnOneInt_3() { return &___s_bnOneInt_3; }
	inline void set_s_bnOneInt_3(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  value)
	{
		___s_bnOneInt_3 = value;
	}

	inline static int32_t get_offset_of_s_bnZeroInt_4() { return static_cast<int32_t>(offsetof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields, ___s_bnZeroInt_4)); }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  get_s_bnZeroInt_4() const { return ___s_bnZeroInt_4; }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC * get_address_of_s_bnZeroInt_4() { return &___s_bnZeroInt_4; }
	inline void set_s_bnZeroInt_4(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  value)
	{
		___s_bnZeroInt_4 = value;
	}

	inline static int32_t get_offset_of_s_bnMinusOneInt_5() { return static_cast<int32_t>(offsetof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields, ___s_bnMinusOneInt_5)); }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  get_s_bnMinusOneInt_5() const { return ___s_bnMinusOneInt_5; }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC * get_address_of_s_bnMinusOneInt_5() { return &___s_bnMinusOneInt_5; }
	inline void set_s_bnMinusOneInt_5(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  value)
	{
		___s_bnMinusOneInt_5 = value;
	}

	inline static int32_t get_offset_of_s_success_6() { return static_cast<int32_t>(offsetof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields, ___s_success_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_s_success_6() const { return ___s_success_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_s_success_6() { return &___s_success_6; }
	inline void set_s_success_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___s_success_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_success_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Numerics.BigInteger
struct BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_marshaled_pinvoke
{
	int32_t ____sign_0;
	uint32_t* ____bits_1;
};
// Native definition for COM marshalling of System.Numerics.BigInteger
struct BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_marshaled_com
{
	int32_t ____sign_0;
	uint32_t* ____bits_1;
};
#endif // BIGINTEGER_T01F3792AFD6865BDF469CC7C7867761F3922BCEC_H
#ifndef BIGNUMBERBUFFER_T9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE_H
#define BIGNUMBERBUFFER_T9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Numerics.BigNumber/BigNumberBuffer
struct  BigNumberBuffer_t9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE 
{
public:
	// System.Text.StringBuilder System.Numerics.BigNumber/BigNumberBuffer::digits
	StringBuilder_t * ___digits_0;
	// System.Int32 System.Numerics.BigNumber/BigNumberBuffer::precision
	int32_t ___precision_1;
	// System.Int32 System.Numerics.BigNumber/BigNumberBuffer::scale
	int32_t ___scale_2;
	// System.Boolean System.Numerics.BigNumber/BigNumberBuffer::sign
	bool ___sign_3;

public:
	inline static int32_t get_offset_of_digits_0() { return static_cast<int32_t>(offsetof(BigNumberBuffer_t9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE, ___digits_0)); }
	inline StringBuilder_t * get_digits_0() const { return ___digits_0; }
	inline StringBuilder_t ** get_address_of_digits_0() { return &___digits_0; }
	inline void set_digits_0(StringBuilder_t * value)
	{
		___digits_0 = value;
		Il2CppCodeGenWriteBarrier((&___digits_0), value);
	}

	inline static int32_t get_offset_of_precision_1() { return static_cast<int32_t>(offsetof(BigNumberBuffer_t9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE, ___precision_1)); }
	inline int32_t get_precision_1() const { return ___precision_1; }
	inline int32_t* get_address_of_precision_1() { return &___precision_1; }
	inline void set_precision_1(int32_t value)
	{
		___precision_1 = value;
	}

	inline static int32_t get_offset_of_scale_2() { return static_cast<int32_t>(offsetof(BigNumberBuffer_t9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE, ___scale_2)); }
	inline int32_t get_scale_2() const { return ___scale_2; }
	inline int32_t* get_address_of_scale_2() { return &___scale_2; }
	inline void set_scale_2(int32_t value)
	{
		___scale_2 = value;
	}

	inline static int32_t get_offset_of_sign_3() { return static_cast<int32_t>(offsetof(BigNumberBuffer_t9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE, ___sign_3)); }
	inline bool get_sign_3() const { return ___sign_3; }
	inline bool* get_address_of_sign_3() { return &___sign_3; }
	inline void set_sign_3(bool value)
	{
		___sign_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Numerics.BigNumber/BigNumberBuffer
struct BigNumberBuffer_t9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE_marshaled_pinvoke
{
	char* ___digits_0;
	int32_t ___precision_1;
	int32_t ___scale_2;
	int32_t ___sign_3;
};
// Native definition for COM marshalling of System.Numerics.BigNumber/BigNumberBuffer
struct BigNumberBuffer_t9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE_marshaled_com
{
	Il2CppChar* ___digits_0;
	int32_t ___precision_1;
	int32_t ___scale_2;
	int32_t ___sign_3;
};
#endif // BIGNUMBERBUFFER_T9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE_H
#ifndef DOUBLEULONG_T7F954356D3B9DACCD9CCC6C1879F2009F65777C7_H
#define DOUBLEULONG_T7F954356D3B9DACCD9CCC6C1879F2009F65777C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Numerics.DoubleUlong
struct  DoubleUlong_t7F954356D3B9DACCD9CCC6C1879F2009F65777C7 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Double System.Numerics.DoubleUlong::dbl
			double ___dbl_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			double ___dbl_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt64 System.Numerics.DoubleUlong::uu
			uint64_t ___uu_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint64_t ___uu_1_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_dbl_0() { return static_cast<int32_t>(offsetof(DoubleUlong_t7F954356D3B9DACCD9CCC6C1879F2009F65777C7, ___dbl_0)); }
	inline double get_dbl_0() const { return ___dbl_0; }
	inline double* get_address_of_dbl_0() { return &___dbl_0; }
	inline void set_dbl_0(double value)
	{
		___dbl_0 = value;
	}

	inline static int32_t get_offset_of_uu_1() { return static_cast<int32_t>(offsetof(DoubleUlong_t7F954356D3B9DACCD9CCC6C1879F2009F65777C7, ___uu_1)); }
	inline uint64_t get_uu_1() const { return ___uu_1; }
	inline uint64_t* get_address_of_uu_1() { return &___uu_1; }
	inline void set_uu_1(uint64_t value)
	{
		___uu_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEULONG_T7F954356D3B9DACCD9CCC6C1879F2009F65777C7_H
#ifndef DATACONTRACTATTRIBUTE_TAD58D5877BD04EADB56BB4AEDDE342C73F032FC5_H
#define DATACONTRACTATTRIBUTE_TAD58D5877BD04EADB56BB4AEDDE342C73F032FC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.DataContractAttribute
struct  DataContractAttribute_tAD58D5877BD04EADB56BB4AEDDE342C73F032FC5  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.Runtime.Serialization.DataContractAttribute::isReference
	bool ___isReference_0;

public:
	inline static int32_t get_offset_of_isReference_0() { return static_cast<int32_t>(offsetof(DataContractAttribute_tAD58D5877BD04EADB56BB4AEDDE342C73F032FC5, ___isReference_0)); }
	inline bool get_isReference_0() const { return ___isReference_0; }
	inline bool* get_address_of_isReference_0() { return &___isReference_0; }
	inline void set_isReference_0(bool value)
	{
		___isReference_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACONTRACTATTRIBUTE_TAD58D5877BD04EADB56BB4AEDDE342C73F032FC5_H
#ifndef DATAMEMBERATTRIBUTE_TC865433FEC93FFD46D6F3E4BB28F262C9EE40525_H
#define DATAMEMBERATTRIBUTE_TC865433FEC93FFD46D6F3E4BB28F262C9EE40525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.DataMemberAttribute
struct  DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Runtime.Serialization.DataMemberAttribute::name
	String_t* ___name_0;
	// System.Int32 System.Runtime.Serialization.DataMemberAttribute::order
	int32_t ___order_1;
	// System.Boolean System.Runtime.Serialization.DataMemberAttribute::isRequired
	bool ___isRequired_2;
	// System.Boolean System.Runtime.Serialization.DataMemberAttribute::emitDefaultValue
	bool ___emitDefaultValue_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525, ___order_1)); }
	inline int32_t get_order_1() const { return ___order_1; }
	inline int32_t* get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(int32_t value)
	{
		___order_1 = value;
	}

	inline static int32_t get_offset_of_isRequired_2() { return static_cast<int32_t>(offsetof(DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525, ___isRequired_2)); }
	inline bool get_isRequired_2() const { return ___isRequired_2; }
	inline bool* get_address_of_isRequired_2() { return &___isRequired_2; }
	inline void set_isRequired_2(bool value)
	{
		___isRequired_2 = value;
	}

	inline static int32_t get_offset_of_emitDefaultValue_3() { return static_cast<int32_t>(offsetof(DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525, ___emitDefaultValue_3)); }
	inline bool get_emitDefaultValue_3() const { return ___emitDefaultValue_3; }
	inline bool* get_address_of_emitDefaultValue_3() { return &___emitDefaultValue_3; }
	inline void set_emitDefaultValue_3(bool value)
	{
		___emitDefaultValue_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAMEMBERATTRIBUTE_TC865433FEC93FFD46D6F3E4BB28F262C9EE40525_H
#ifndef ENUMMEMBERATTRIBUTE_T115D80337B2C8222158FC46345EA100EEB63B32D_H
#define ENUMMEMBERATTRIBUTE_T115D80337B2C8222158FC46345EA100EEB63B32D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.EnumMemberAttribute
struct  EnumMemberAttribute_t115D80337B2C8222158FC46345EA100EEB63B32D  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Runtime.Serialization.EnumMemberAttribute::value
	String_t* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(EnumMemberAttribute_t115D80337B2C8222158FC46345EA100EEB63B32D, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMMEMBERATTRIBUTE_T115D80337B2C8222158FC46345EA100EEB63B32D_H
#ifndef KNOWNTYPEATTRIBUTE_TFCFB5B9A0AE4BBCBD655E2EC756FABAC3ADF487E_H
#define KNOWNTYPEATTRIBUTE_TFCFB5B9A0AE4BBCBD655E2EC756FABAC3ADF487E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.KnownTypeAttribute
struct  KnownTypeAttribute_tFCFB5B9A0AE4BBCBD655E2EC756FABAC3ADF487E  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type System.Runtime.Serialization.KnownTypeAttribute::type
	Type_t * ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(KnownTypeAttribute_tFCFB5B9A0AE4BBCBD655E2EC756FABAC3ADF487E, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KNOWNTYPEATTRIBUTE_TFCFB5B9A0AE4BBCBD655E2EC756FABAC3ADF487E_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef TYPECONVERTER_T8306AE03734853B551DDF089C1F17836A7764DBB_H
#define TYPECONVERTER_T8306AE03734853B551DDF089C1F17836A7764DBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeConverter
struct  TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB  : public RuntimeObject
{
public:

public:
};

struct TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB_StaticFields
{
public:
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.TypeConverter::useCompatibleTypeConversion
	bool ___useCompatibleTypeConversion_1;

public:
	inline static int32_t get_offset_of_useCompatibleTypeConversion_1() { return static_cast<int32_t>(offsetof(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB_StaticFields, ___useCompatibleTypeConversion_1)); }
	inline bool get_useCompatibleTypeConversion_1() const { return ___useCompatibleTypeConversion_1; }
	inline bool* get_address_of_useCompatibleTypeConversion_1() { return &___useCompatibleTypeConversion_1; }
	inline void set_useCompatibleTypeConversion_1(bool value)
	{
		___useCompatibleTypeConversion_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPECONVERTER_T8306AE03734853B551DDF089C1F17836A7764DBB_H
#ifndef ACCEPTREJECTRULE_T391EEC16C998A618BB6208797DE62FFD8538D5D5_H
#define ACCEPTREJECTRULE_T391EEC16C998A618BB6208797DE62FFD8538D5D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.AcceptRejectRule
struct  AcceptRejectRule_t391EEC16C998A618BB6208797DE62FFD8538D5D5 
{
public:
	// System.Int32 System.Data.AcceptRejectRule::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AcceptRejectRule_t391EEC16C998A618BB6208797DE62FFD8538D5D5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCEPTREJECTRULE_T391EEC16C998A618BB6208797DE62FFD8538D5D5_H
#ifndef AGGREGATETYPE_T5D6ACD9ED88CE8523487CEA56C474358136EF1FE_H
#define AGGREGATETYPE_T5D6ACD9ED88CE8523487CEA56C474358136EF1FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.AggregateType
struct  AggregateType_t5D6ACD9ED88CE8523487CEA56C474358136EF1FE 
{
public:
	// System.Int32 System.Data.AggregateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AggregateType_t5D6ACD9ED88CE8523487CEA56C474358136EF1FE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGREGATETYPE_T5D6ACD9ED88CE8523487CEA56C474358136EF1FE_H
#ifndef AUTOINCREMENTBIGINTEGER_T55278962E348F18E54ACFFC6045EDE3016C8A797_H
#define AUTOINCREMENTBIGINTEGER_T55278962E348F18E54ACFFC6045EDE3016C8A797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.AutoIncrementBigInteger
struct  AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797  : public AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086
{
public:
	// System.Numerics.BigInteger System.Data.AutoIncrementBigInteger::_current
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  ____current_1;
	// System.Int64 System.Data.AutoIncrementBigInteger::_seed
	int64_t ____seed_2;
	// System.Numerics.BigInteger System.Data.AutoIncrementBigInteger::_step
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  ____step_3;

public:
	inline static int32_t get_offset_of__current_1() { return static_cast<int32_t>(offsetof(AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797, ____current_1)); }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  get__current_1() const { return ____current_1; }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC * get_address_of__current_1() { return &____current_1; }
	inline void set__current_1(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  value)
	{
		____current_1 = value;
	}

	inline static int32_t get_offset_of__seed_2() { return static_cast<int32_t>(offsetof(AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797, ____seed_2)); }
	inline int64_t get__seed_2() const { return ____seed_2; }
	inline int64_t* get_address_of__seed_2() { return &____seed_2; }
	inline void set__seed_2(int64_t value)
	{
		____seed_2 = value;
	}

	inline static int32_t get_offset_of__step_3() { return static_cast<int32_t>(offsetof(AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797, ____step_3)); }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  get__step_3() const { return ____step_3; }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC * get_address_of__step_3() { return &____step_3; }
	inline void set__step_3(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  value)
	{
		____step_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOINCREMENTBIGINTEGER_T55278962E348F18E54ACFFC6045EDE3016C8A797_H
#ifndef CHILDFOREIGNKEYCONSTRAINTENUMERATOR_TF57E30068558F2981330827F8B31D700243A827B_H
#define CHILDFOREIGNKEYCONSTRAINTENUMERATOR_TF57E30068558F2981330827F8B31D700243A827B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ChildForeignKeyConstraintEnumerator
struct  ChildForeignKeyConstraintEnumerator_tF57E30068558F2981330827F8B31D700243A827B  : public ForeignKeyConstraintEnumerator_t555045F1D5AC5CF9D756F3A5955790BB8C7C3C6F
{
public:
	// System.Data.DataTable System.Data.ChildForeignKeyConstraintEnumerator::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_3;

public:
	inline static int32_t get_offset_of__table_3() { return static_cast<int32_t>(offsetof(ChildForeignKeyConstraintEnumerator_tF57E30068558F2981330827F8B31D700243A827B, ____table_3)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_3() const { return ____table_3; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_3() { return &____table_3; }
	inline void set__table_3(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_3 = value;
		Il2CppCodeGenWriteBarrier((&____table_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHILDFOREIGNKEYCONSTRAINTENUMERATOR_TF57E30068558F2981330827F8B31D700243A827B_H
#ifndef STORAGETYPE_TD15D471C3D3DF84EF73A02FCAB2E4DC1D964F2FC_H
#define STORAGETYPE_TD15D471C3D3DF84EF73A02FCAB2E4DC1D964F2FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.StorageType
struct  StorageType_tD15D471C3D3DF84EF73A02FCAB2E4DC1D964F2FC 
{
public:
	// System.Int32 System.Data.Common.StorageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StorageType_tD15D471C3D3DF84EF73A02FCAB2E4DC1D964F2FC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORAGETYPE_TD15D471C3D3DF84EF73A02FCAB2E4DC1D964F2FC_H
#ifndef DATACOLUMNPROPERTYDESCRIPTOR_TDFBE9FD48209F7B6754670423AA58122A0BBF356_H
#define DATACOLUMNPROPERTYDESCRIPTOR_TDFBE9FD48209F7B6754670423AA58122A0BBF356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataColumnPropertyDescriptor
struct  DataColumnPropertyDescriptor_tDFBE9FD48209F7B6754670423AA58122A0BBF356  : public PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D
{
public:
	// System.Data.DataColumn System.Data.DataColumnPropertyDescriptor::<Column>k__BackingField
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * ___U3CColumnU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CColumnU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(DataColumnPropertyDescriptor_tDFBE9FD48209F7B6754670423AA58122A0BBF356, ___U3CColumnU3Ek__BackingField_17)); }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * get_U3CColumnU3Ek__BackingField_17() const { return ___U3CColumnU3Ek__BackingField_17; }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D ** get_address_of_U3CColumnU3Ek__BackingField_17() { return &___U3CColumnU3Ek__BackingField_17; }
	inline void set_U3CColumnU3Ek__BackingField_17(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * value)
	{
		___U3CColumnU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CColumnU3Ek__BackingField_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACOLUMNPROPERTYDESCRIPTOR_TDFBE9FD48209F7B6754670423AA58122A0BBF356_H
#ifndef DATAEXCEPTION_TF3EDE6A8A27EF7362516D20F88010F26804AFF1B_H
#define DATAEXCEPTION_TF3EDE6A8A27EF7362516D20F88010F26804AFF1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataException
struct  DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAEXCEPTION_TF3EDE6A8A27EF7362516D20F88010F26804AFF1B_H
#ifndef DATARELATION_TAA881A9E007B471CD0037C2BBF37D5B900BD31B3_H
#define DATARELATION_TAA881A9E007B471CD0037C2BBF37D5B900BD31B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRelation
struct  DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3  : public RuntimeObject
{
public:
	// System.Data.DataSet System.Data.DataRelation::_dataSet
	DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * ____dataSet_0;
	// System.Data.PropertyCollection System.Data.DataRelation::_extendedProperties
	PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * ____extendedProperties_1;
	// System.String System.Data.DataRelation::_relationName
	String_t* ____relationName_2;
	// System.Data.DataKey System.Data.DataRelation::_childKey
	DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B  ____childKey_3;
	// System.Data.DataKey System.Data.DataRelation::_parentKey
	DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B  ____parentKey_4;
	// System.Data.UniqueConstraint System.Data.DataRelation::_parentKeyConstraint
	UniqueConstraint_t291F6C173D4820C1ACAE889805C3649A44DC1D22 * ____parentKeyConstraint_5;
	// System.Data.ForeignKeyConstraint System.Data.DataRelation::_childKeyConstraint
	ForeignKeyConstraint_t3F1E5D8CA4D7F8EE363336DF44B29BC24E8C016A * ____childKeyConstraint_6;
	// System.String[] System.Data.DataRelation::_parentColumnNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____parentColumnNames_7;
	// System.String[] System.Data.DataRelation::_childColumnNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____childColumnNames_8;
	// System.String System.Data.DataRelation::_parentTableName
	String_t* ____parentTableName_9;
	// System.String System.Data.DataRelation::_childTableName
	String_t* ____childTableName_10;
	// System.String System.Data.DataRelation::_parentTableNamespace
	String_t* ____parentTableNamespace_11;
	// System.String System.Data.DataRelation::_childTableNamespace
	String_t* ____childTableNamespace_12;
	// System.Boolean System.Data.DataRelation::_nested
	bool ____nested_13;
	// System.Boolean System.Data.DataRelation::_createConstraints
	bool ____createConstraints_14;
	// System.Boolean System.Data.DataRelation::_checkMultipleNested
	bool ____checkMultipleNested_15;
	// System.Int32 System.Data.DataRelation::_objectID
	int32_t ____objectID_17;
	// System.ComponentModel.PropertyChangedEventHandler System.Data.DataRelation::PropertyChanging
	PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * ___PropertyChanging_18;

public:
	inline static int32_t get_offset_of__dataSet_0() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____dataSet_0)); }
	inline DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * get__dataSet_0() const { return ____dataSet_0; }
	inline DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 ** get_address_of__dataSet_0() { return &____dataSet_0; }
	inline void set__dataSet_0(DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * value)
	{
		____dataSet_0 = value;
		Il2CppCodeGenWriteBarrier((&____dataSet_0), value);
	}

	inline static int32_t get_offset_of__extendedProperties_1() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____extendedProperties_1)); }
	inline PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * get__extendedProperties_1() const { return ____extendedProperties_1; }
	inline PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 ** get_address_of__extendedProperties_1() { return &____extendedProperties_1; }
	inline void set__extendedProperties_1(PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * value)
	{
		____extendedProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&____extendedProperties_1), value);
	}

	inline static int32_t get_offset_of__relationName_2() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____relationName_2)); }
	inline String_t* get__relationName_2() const { return ____relationName_2; }
	inline String_t** get_address_of__relationName_2() { return &____relationName_2; }
	inline void set__relationName_2(String_t* value)
	{
		____relationName_2 = value;
		Il2CppCodeGenWriteBarrier((&____relationName_2), value);
	}

	inline static int32_t get_offset_of__childKey_3() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____childKey_3)); }
	inline DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B  get__childKey_3() const { return ____childKey_3; }
	inline DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B * get_address_of__childKey_3() { return &____childKey_3; }
	inline void set__childKey_3(DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B  value)
	{
		____childKey_3 = value;
	}

	inline static int32_t get_offset_of__parentKey_4() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____parentKey_4)); }
	inline DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B  get__parentKey_4() const { return ____parentKey_4; }
	inline DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B * get_address_of__parentKey_4() { return &____parentKey_4; }
	inline void set__parentKey_4(DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B  value)
	{
		____parentKey_4 = value;
	}

	inline static int32_t get_offset_of__parentKeyConstraint_5() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____parentKeyConstraint_5)); }
	inline UniqueConstraint_t291F6C173D4820C1ACAE889805C3649A44DC1D22 * get__parentKeyConstraint_5() const { return ____parentKeyConstraint_5; }
	inline UniqueConstraint_t291F6C173D4820C1ACAE889805C3649A44DC1D22 ** get_address_of__parentKeyConstraint_5() { return &____parentKeyConstraint_5; }
	inline void set__parentKeyConstraint_5(UniqueConstraint_t291F6C173D4820C1ACAE889805C3649A44DC1D22 * value)
	{
		____parentKeyConstraint_5 = value;
		Il2CppCodeGenWriteBarrier((&____parentKeyConstraint_5), value);
	}

	inline static int32_t get_offset_of__childKeyConstraint_6() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____childKeyConstraint_6)); }
	inline ForeignKeyConstraint_t3F1E5D8CA4D7F8EE363336DF44B29BC24E8C016A * get__childKeyConstraint_6() const { return ____childKeyConstraint_6; }
	inline ForeignKeyConstraint_t3F1E5D8CA4D7F8EE363336DF44B29BC24E8C016A ** get_address_of__childKeyConstraint_6() { return &____childKeyConstraint_6; }
	inline void set__childKeyConstraint_6(ForeignKeyConstraint_t3F1E5D8CA4D7F8EE363336DF44B29BC24E8C016A * value)
	{
		____childKeyConstraint_6 = value;
		Il2CppCodeGenWriteBarrier((&____childKeyConstraint_6), value);
	}

	inline static int32_t get_offset_of__parentColumnNames_7() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____parentColumnNames_7)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__parentColumnNames_7() const { return ____parentColumnNames_7; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__parentColumnNames_7() { return &____parentColumnNames_7; }
	inline void set__parentColumnNames_7(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____parentColumnNames_7 = value;
		Il2CppCodeGenWriteBarrier((&____parentColumnNames_7), value);
	}

	inline static int32_t get_offset_of__childColumnNames_8() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____childColumnNames_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__childColumnNames_8() const { return ____childColumnNames_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__childColumnNames_8() { return &____childColumnNames_8; }
	inline void set__childColumnNames_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____childColumnNames_8 = value;
		Il2CppCodeGenWriteBarrier((&____childColumnNames_8), value);
	}

	inline static int32_t get_offset_of__parentTableName_9() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____parentTableName_9)); }
	inline String_t* get__parentTableName_9() const { return ____parentTableName_9; }
	inline String_t** get_address_of__parentTableName_9() { return &____parentTableName_9; }
	inline void set__parentTableName_9(String_t* value)
	{
		____parentTableName_9 = value;
		Il2CppCodeGenWriteBarrier((&____parentTableName_9), value);
	}

	inline static int32_t get_offset_of__childTableName_10() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____childTableName_10)); }
	inline String_t* get__childTableName_10() const { return ____childTableName_10; }
	inline String_t** get_address_of__childTableName_10() { return &____childTableName_10; }
	inline void set__childTableName_10(String_t* value)
	{
		____childTableName_10 = value;
		Il2CppCodeGenWriteBarrier((&____childTableName_10), value);
	}

	inline static int32_t get_offset_of__parentTableNamespace_11() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____parentTableNamespace_11)); }
	inline String_t* get__parentTableNamespace_11() const { return ____parentTableNamespace_11; }
	inline String_t** get_address_of__parentTableNamespace_11() { return &____parentTableNamespace_11; }
	inline void set__parentTableNamespace_11(String_t* value)
	{
		____parentTableNamespace_11 = value;
		Il2CppCodeGenWriteBarrier((&____parentTableNamespace_11), value);
	}

	inline static int32_t get_offset_of__childTableNamespace_12() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____childTableNamespace_12)); }
	inline String_t* get__childTableNamespace_12() const { return ____childTableNamespace_12; }
	inline String_t** get_address_of__childTableNamespace_12() { return &____childTableNamespace_12; }
	inline void set__childTableNamespace_12(String_t* value)
	{
		____childTableNamespace_12 = value;
		Il2CppCodeGenWriteBarrier((&____childTableNamespace_12), value);
	}

	inline static int32_t get_offset_of__nested_13() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____nested_13)); }
	inline bool get__nested_13() const { return ____nested_13; }
	inline bool* get_address_of__nested_13() { return &____nested_13; }
	inline void set__nested_13(bool value)
	{
		____nested_13 = value;
	}

	inline static int32_t get_offset_of__createConstraints_14() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____createConstraints_14)); }
	inline bool get__createConstraints_14() const { return ____createConstraints_14; }
	inline bool* get_address_of__createConstraints_14() { return &____createConstraints_14; }
	inline void set__createConstraints_14(bool value)
	{
		____createConstraints_14 = value;
	}

	inline static int32_t get_offset_of__checkMultipleNested_15() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____checkMultipleNested_15)); }
	inline bool get__checkMultipleNested_15() const { return ____checkMultipleNested_15; }
	inline bool* get_address_of__checkMultipleNested_15() { return &____checkMultipleNested_15; }
	inline void set__checkMultipleNested_15(bool value)
	{
		____checkMultipleNested_15 = value;
	}

	inline static int32_t get_offset_of__objectID_17() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____objectID_17)); }
	inline int32_t get__objectID_17() const { return ____objectID_17; }
	inline int32_t* get_address_of__objectID_17() { return &____objectID_17; }
	inline void set__objectID_17(int32_t value)
	{
		____objectID_17 = value;
	}

	inline static int32_t get_offset_of_PropertyChanging_18() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ___PropertyChanging_18)); }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * get_PropertyChanging_18() const { return ___PropertyChanging_18; }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 ** get_address_of_PropertyChanging_18() { return &___PropertyChanging_18; }
	inline void set_PropertyChanging_18(PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * value)
	{
		___PropertyChanging_18 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyChanging_18), value);
	}
};

struct DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3_StaticFields
{
public:
	// System.Int32 System.Data.DataRelation::s_objectTypeCount
	int32_t ___s_objectTypeCount_16;

public:
	inline static int32_t get_offset_of_s_objectTypeCount_16() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3_StaticFields, ___s_objectTypeCount_16)); }
	inline int32_t get_s_objectTypeCount_16() const { return ___s_objectTypeCount_16; }
	inline int32_t* get_address_of_s_objectTypeCount_16() { return &___s_objectTypeCount_16; }
	inline void set_s_objectTypeCount_16(int32_t value)
	{
		___s_objectTypeCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATARELATION_TAA881A9E007B471CD0037C2BBF37D5B900BD31B3_H
#ifndef DATASETRELATIONCOLLECTION_T5096447D1E7B322D3316DC212DCAD61701F2760D_H
#define DATASETRELATIONCOLLECTION_T5096447D1E7B322D3316DC212DCAD61701F2760D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRelationCollection/DataSetRelationCollection
struct  DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D  : public DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B
{
public:
	// System.Data.DataSet System.Data.DataRelationCollection/DataSetRelationCollection::_dataSet
	DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * ____dataSet_7;
	// System.Collections.ArrayList System.Data.DataRelationCollection/DataSetRelationCollection::_relations
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____relations_8;
	// System.Data.DataRelation[] System.Data.DataRelationCollection/DataSetRelationCollection::_delayLoadingRelations
	DataRelationU5BU5D_t705BDBA68D45143524D5C70D82EA04F0B676C15B* ____delayLoadingRelations_9;

public:
	inline static int32_t get_offset_of__dataSet_7() { return static_cast<int32_t>(offsetof(DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D, ____dataSet_7)); }
	inline DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * get__dataSet_7() const { return ____dataSet_7; }
	inline DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 ** get_address_of__dataSet_7() { return &____dataSet_7; }
	inline void set__dataSet_7(DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * value)
	{
		____dataSet_7 = value;
		Il2CppCodeGenWriteBarrier((&____dataSet_7), value);
	}

	inline static int32_t get_offset_of__relations_8() { return static_cast<int32_t>(offsetof(DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D, ____relations_8)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__relations_8() const { return ____relations_8; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__relations_8() { return &____relations_8; }
	inline void set__relations_8(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____relations_8 = value;
		Il2CppCodeGenWriteBarrier((&____relations_8), value);
	}

	inline static int32_t get_offset_of__delayLoadingRelations_9() { return static_cast<int32_t>(offsetof(DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D, ____delayLoadingRelations_9)); }
	inline DataRelationU5BU5D_t705BDBA68D45143524D5C70D82EA04F0B676C15B* get__delayLoadingRelations_9() const { return ____delayLoadingRelations_9; }
	inline DataRelationU5BU5D_t705BDBA68D45143524D5C70D82EA04F0B676C15B** get_address_of__delayLoadingRelations_9() { return &____delayLoadingRelations_9; }
	inline void set__delayLoadingRelations_9(DataRelationU5BU5D_t705BDBA68D45143524D5C70D82EA04F0B676C15B* value)
	{
		____delayLoadingRelations_9 = value;
		Il2CppCodeGenWriteBarrier((&____delayLoadingRelations_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETRELATIONCOLLECTION_T5096447D1E7B322D3316DC212DCAD61701F2760D_H
#ifndef DATATABLERELATIONCOLLECTION_T50330D4BF5D9EA756BEAE555D934839EDC4804ED_H
#define DATATABLERELATIONCOLLECTION_T50330D4BF5D9EA756BEAE555D934839EDC4804ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRelationCollection/DataTableRelationCollection
struct  DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED  : public DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B
{
public:
	// System.Data.DataTable System.Data.DataRelationCollection/DataTableRelationCollection::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_7;
	// System.Collections.ArrayList System.Data.DataRelationCollection/DataTableRelationCollection::_relations
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____relations_8;
	// System.Boolean System.Data.DataRelationCollection/DataTableRelationCollection::_fParentCollection
	bool ____fParentCollection_9;
	// System.ComponentModel.CollectionChangeEventHandler System.Data.DataRelationCollection/DataTableRelationCollection::RelationPropertyChanged
	CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * ___RelationPropertyChanged_10;

public:
	inline static int32_t get_offset_of__table_7() { return static_cast<int32_t>(offsetof(DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED, ____table_7)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_7() const { return ____table_7; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_7() { return &____table_7; }
	inline void set__table_7(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_7 = value;
		Il2CppCodeGenWriteBarrier((&____table_7), value);
	}

	inline static int32_t get_offset_of__relations_8() { return static_cast<int32_t>(offsetof(DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED, ____relations_8)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__relations_8() const { return ____relations_8; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__relations_8() { return &____relations_8; }
	inline void set__relations_8(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____relations_8 = value;
		Il2CppCodeGenWriteBarrier((&____relations_8), value);
	}

	inline static int32_t get_offset_of__fParentCollection_9() { return static_cast<int32_t>(offsetof(DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED, ____fParentCollection_9)); }
	inline bool get__fParentCollection_9() const { return ____fParentCollection_9; }
	inline bool* get_address_of__fParentCollection_9() { return &____fParentCollection_9; }
	inline void set__fParentCollection_9(bool value)
	{
		____fParentCollection_9 = value;
	}

	inline static int32_t get_offset_of_RelationPropertyChanged_10() { return static_cast<int32_t>(offsetof(DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED, ___RelationPropertyChanged_10)); }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * get_RelationPropertyChanged_10() const { return ___RelationPropertyChanged_10; }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 ** get_address_of_RelationPropertyChanged_10() { return &___RelationPropertyChanged_10; }
	inline void set_RelationPropertyChanged_10(CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * value)
	{
		___RelationPropertyChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___RelationPropertyChanged_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATABLERELATIONCOLLECTION_T50330D4BF5D9EA756BEAE555D934839EDC4804ED_H
#ifndef DATARELATIONPROPERTYDESCRIPTOR_T0937B241D5C01EB1F18BBF9894A5A9E387E2A856_H
#define DATARELATIONPROPERTYDESCRIPTOR_T0937B241D5C01EB1F18BBF9894A5A9E387E2A856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRelationPropertyDescriptor
struct  DataRelationPropertyDescriptor_t0937B241D5C01EB1F18BBF9894A5A9E387E2A856  : public PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D
{
public:
	// System.Data.DataRelation System.Data.DataRelationPropertyDescriptor::<Relation>k__BackingField
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 * ___U3CRelationU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CRelationU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(DataRelationPropertyDescriptor_t0937B241D5C01EB1F18BBF9894A5A9E387E2A856, ___U3CRelationU3Ek__BackingField_17)); }
	inline DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 * get_U3CRelationU3Ek__BackingField_17() const { return ___U3CRelationU3Ek__BackingField_17; }
	inline DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 ** get_address_of_U3CRelationU3Ek__BackingField_17() { return &___U3CRelationU3Ek__BackingField_17; }
	inline void set_U3CRelationU3Ek__BackingField_17(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 * value)
	{
		___U3CRelationU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRelationU3Ek__BackingField_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATARELATIONPROPERTYDESCRIPTOR_T0937B241D5C01EB1F18BBF9894A5A9E387E2A856_H
#ifndef DATAROWACTION_TDA5E813CDEE8ABF5D37A4A055D75B66DDBEB068F_H
#define DATAROWACTION_TDA5E813CDEE8ABF5D37A4A055D75B66DDBEB068F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRowAction
struct  DataRowAction_tDA5E813CDEE8ABF5D37A4A055D75B66DDBEB068F 
{
public:
	// System.Int32 System.Data.DataRowAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataRowAction_tDA5E813CDEE8ABF5D37A4A055D75B66DDBEB068F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAROWACTION_TDA5E813CDEE8ABF5D37A4A055D75B66DDBEB068F_H
#ifndef DATAROWSTATE_TE4CAA76865C430B04232443A665CEE5C1EF6DEBE_H
#define DATAROWSTATE_TE4CAA76865C430B04232443A665CEE5C1EF6DEBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRowState
struct  DataRowState_tE4CAA76865C430B04232443A665CEE5C1EF6DEBE 
{
public:
	// System.Int32 System.Data.DataRowState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataRowState_tE4CAA76865C430B04232443A665CEE5C1EF6DEBE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAROWSTATE_TE4CAA76865C430B04232443A665CEE5C1EF6DEBE_H
#ifndef DATASETDATETIME_TF3A212AE464B7F93E17EB834312EB23CDBC29CE2_H
#define DATASETDATETIME_TF3A212AE464B7F93E17EB834312EB23CDBC29CE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataSetDateTime
struct  DataSetDateTime_tF3A212AE464B7F93E17EB834312EB23CDBC29CE2 
{
public:
	// System.Int32 System.Data.DataSetDateTime::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataSetDateTime_tF3A212AE464B7F93E17EB834312EB23CDBC29CE2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETDATETIME_TF3A212AE464B7F93E17EB834312EB23CDBC29CE2_H
#ifndef MAPPINGTYPE_T4C46BE07523A8492499624C2950ED8B391FC037C_H
#define MAPPINGTYPE_T4C46BE07523A8492499624C2950ED8B391FC037C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.MappingType
struct  MappingType_t4C46BE07523A8492499624C2950ED8B391FC037C 
{
public:
	// System.Int32 System.Data.MappingType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MappingType_t4C46BE07523A8492499624C2950ED8B391FC037C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGTYPE_T4C46BE07523A8492499624C2950ED8B391FC037C_H
#ifndef PARENTFOREIGNKEYCONSTRAINTENUMERATOR_T442E012770A77A20F68D240C7E9271BB1B1FB9FA_H
#define PARENTFOREIGNKEYCONSTRAINTENUMERATOR_T442E012770A77A20F68D240C7E9271BB1B1FB9FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ParentForeignKeyConstraintEnumerator
struct  ParentForeignKeyConstraintEnumerator_t442E012770A77A20F68D240C7E9271BB1B1FB9FA  : public ForeignKeyConstraintEnumerator_t555045F1D5AC5CF9D756F3A5955790BB8C7C3C6F
{
public:
	// System.Data.DataTable System.Data.ParentForeignKeyConstraintEnumerator::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_3;

public:
	inline static int32_t get_offset_of__table_3() { return static_cast<int32_t>(offsetof(ParentForeignKeyConstraintEnumerator_t442E012770A77A20F68D240C7E9271BB1B1FB9FA, ____table_3)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_3() const { return ____table_3; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_3() { return &____table_3; }
	inline void set__table_3(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_3 = value;
		Il2CppCodeGenWriteBarrier((&____table_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARENTFOREIGNKEYCONSTRAINTENUMERATOR_T442E012770A77A20F68D240C7E9271BB1B1FB9FA_H
#ifndef TREEACCESSMETHOD_TC8B021C190FE4395F52FFCE1F381E99A8618F0A3_H
#define TREEACCESSMETHOD_TC8B021C190FE4395F52FFCE1F381E99A8618F0A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.TreeAccessMethod
struct  TreeAccessMethod_tC8B021C190FE4395F52FFCE1F381E99A8618F0A3 
{
public:
	// System.Int32 System.Data.TreeAccessMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TreeAccessMethod_tC8B021C190FE4395F52FFCE1F381E99A8618F0A3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TREEACCESSMETHOD_TC8B021C190FE4395F52FFCE1F381E99A8618F0A3_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef GETBYTESMODE_T58B43CD1A2F9BB629A2EE28F59E0FF610C79A3B5_H
#define GETBYTESMODE_T58B43CD1A2F9BB629A2EE28F59E0FF610C79A3B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Numerics.BigInteger/GetBytesMode
struct  GetBytesMode_t58B43CD1A2F9BB629A2EE28F59E0FF610C79A3B5 
{
public:
	// System.Int32 System.Numerics.BigInteger/GetBytesMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GetBytesMode_t58B43CD1A2F9BB629A2EE28F59E0FF610C79A3B5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETBYTESMODE_T58B43CD1A2F9BB629A2EE28F59E0FF610C79A3B5_H
#ifndef VFXSPAWNERSTATE_T614A1AADC2EDB20E82A625BE053A22DB31D89AC7_H
#define VFXSPAWNERSTATE_T614A1AADC2EDB20E82A625BE053A22DB31D89AC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.VFX.VFXSpawnerState
struct  VFXSpawnerState_t614A1AADC2EDB20E82A625BE053A22DB31D89AC7  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Experimental.VFX.VFXSpawnerState::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Boolean UnityEngine.Experimental.VFX.VFXSpawnerState::m_Owner
	bool ___m_Owner_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(VFXSpawnerState_t614A1AADC2EDB20E82A625BE053A22DB31D89AC7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_Owner_1() { return static_cast<int32_t>(offsetof(VFXSpawnerState_t614A1AADC2EDB20E82A625BE053A22DB31D89AC7, ___m_Owner_1)); }
	inline bool get_m_Owner_1() const { return ___m_Owner_1; }
	inline bool* get_address_of_m_Owner_1() { return &___m_Owner_1; }
	inline void set_m_Owner_1(bool value)
	{
		___m_Owner_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.VFX.VFXSpawnerState
struct VFXSpawnerState_t614A1AADC2EDB20E82A625BE053A22DB31D89AC7_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	int32_t ___m_Owner_1;
};
// Native definition for COM marshalling of UnityEngine.Experimental.VFX.VFXSpawnerState
struct VFXSpawnerState_t614A1AADC2EDB20E82A625BE053A22DB31D89AC7_marshaled_com
{
	intptr_t ___m_Ptr_0;
	int32_t ___m_Owner_1;
};
#endif // VFXSPAWNERSTATE_T614A1AADC2EDB20E82A625BE053A22DB31D89AC7_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef PLAYABLEHANDLE_T9D3B4E540D4413CED81DDD6A24C5373BEFA1D182_H
#define PLAYABLEHANDLE_T9D3B4E540D4413CED81DDD6A24C5373BEFA1D182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableHandle::m_Version
	uint32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182, ___m_Version_1)); }
	inline uint32_t get_m_Version_1() const { return ___m_Version_1; }
	inline uint32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(uint32_t value)
	{
		___m_Version_1 = value;
	}
};

struct PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182_StaticFields
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::m_Null
	PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  ___m_Null_2;

public:
	inline static int32_t get_offset_of_m_Null_2() { return static_cast<int32_t>(offsetof(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182_StaticFields, ___m_Null_2)); }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  get_m_Null_2() const { return ___m_Null_2; }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182 * get_address_of_m_Null_2() { return &___m_Null_2; }
	inline void set_m_Null_2(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  value)
	{
		___m_Null_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T9D3B4E540D4413CED81DDD6A24C5373BEFA1D182_H
#ifndef VIDEO3DLAYOUT_T5F64D0CE5E9B37C2FCE67F397FA5CFE9C047E4A1_H
#define VIDEO3DLAYOUT_T5F64D0CE5E9B37C2FCE67F397FA5CFE9C047E4A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.Video3DLayout
struct  Video3DLayout_t5F64D0CE5E9B37C2FCE67F397FA5CFE9C047E4A1 
{
public:
	// System.Int32 UnityEngine.Video.Video3DLayout::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Video3DLayout_t5F64D0CE5E9B37C2FCE67F397FA5CFE9C047E4A1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEO3DLAYOUT_T5F64D0CE5E9B37C2FCE67F397FA5CFE9C047E4A1_H
#ifndef VIDEOASPECTRATIO_T5739968D28C4F8F802B085E293F22110205B8379_H
#define VIDEOASPECTRATIO_T5739968D28C4F8F802B085E293F22110205B8379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoAspectRatio
struct  VideoAspectRatio_t5739968D28C4F8F802B085E293F22110205B8379 
{
public:
	// System.Int32 UnityEngine.Video.VideoAspectRatio::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoAspectRatio_t5739968D28C4F8F802B085E293F22110205B8379, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOASPECTRATIO_T5739968D28C4F8F802B085E293F22110205B8379_H
#ifndef VIDEOAUDIOOUTPUTMODE_T8CDE10B382F3C321345EC57C9164A9177139DC6F_H
#define VIDEOAUDIOOUTPUTMODE_T8CDE10B382F3C321345EC57C9164A9177139DC6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoAudioOutputMode
struct  VideoAudioOutputMode_t8CDE10B382F3C321345EC57C9164A9177139DC6F 
{
public:
	// System.Int32 UnityEngine.Video.VideoAudioOutputMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoAudioOutputMode_t8CDE10B382F3C321345EC57C9164A9177139DC6F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOAUDIOOUTPUTMODE_T8CDE10B382F3C321345EC57C9164A9177139DC6F_H
#ifndef VIDEORENDERMODE_T0DBAABB576FDA890C49C6AD3EE641623F93E9161_H
#define VIDEORENDERMODE_T0DBAABB576FDA890C49C6AD3EE641623F93E9161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoRenderMode
struct  VideoRenderMode_t0DBAABB576FDA890C49C6AD3EE641623F93E9161 
{
public:
	// System.Int32 UnityEngine.Video.VideoRenderMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoRenderMode_t0DBAABB576FDA890C49C6AD3EE641623F93E9161, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEORENDERMODE_T0DBAABB576FDA890C49C6AD3EE641623F93E9161_H
#ifndef VIDEOSOURCE_T32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A_H
#define VIDEOSOURCE_T32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoSource
struct  VideoSource_t32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A 
{
public:
	// System.Int32 UnityEngine.Video.VideoSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoSource_t32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOSOURCE_T32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A_H
#ifndef VIDEOTIMEREFERENCE_T9EAEBD354AE5E56F0D0F36E73A428BB2A0B8B31B_H
#define VIDEOTIMEREFERENCE_T9EAEBD354AE5E56F0D0F36E73A428BB2A0B8B31B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoTimeReference
struct  VideoTimeReference_t9EAEBD354AE5E56F0D0F36E73A428BB2A0B8B31B 
{
public:
	// System.Int32 UnityEngine.Video.VideoTimeReference::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoTimeReference_t9EAEBD354AE5E56F0D0F36E73A428BB2A0B8B31B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTIMEREFERENCE_T9EAEBD354AE5E56F0D0F36E73A428BB2A0B8B31B_H
#ifndef VIDEOTIMESOURCE_T15F04FD6B3D75A8D98480E8B77117C0FF691BB77_H
#define VIDEOTIMESOURCE_T15F04FD6B3D75A8D98480E8B77117C0FF691BB77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoTimeSource
struct  VideoTimeSource_t15F04FD6B3D75A8D98480E8B77117C0FF691BB77 
{
public:
	// System.Int32 UnityEngine.Video.VideoTimeSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoTimeSource_t15F04FD6B3D75A8D98480E8B77117C0FF691BB77, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTIMESOURCE_T15F04FD6B3D75A8D98480E8B77117C0FF691BB77_H
#ifndef EXPANDABLEOBJECTCONVERTER_TC19580E01F630034FD5140CFA7453E1125E13F99_H
#define EXPANDABLEOBJECTCONVERTER_TC19580E01F630034FD5140CFA7453E1125E13F99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ExpandableObjectConverter
struct  ExpandableObjectConverter_tC19580E01F630034FD5140CFA7453E1125E13F99  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPANDABLEOBJECTCONVERTER_TC19580E01F630034FD5140CFA7453E1125E13F99_H
#ifndef COLUMNTYPECONVERTER_TC0CFD16E38C032C2274AF18C195363BC4047AB8C_H
#define COLUMNTYPECONVERTER_TC0CFD16E38C032C2274AF18C195363BC4047AB8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ColumnTypeConverter
struct  ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:
	// System.ComponentModel.TypeConverter/StandardValuesCollection System.Data.ColumnTypeConverter::_values
	StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * ____values_3;

public:
	inline static int32_t get_offset_of__values_3() { return static_cast<int32_t>(offsetof(ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C, ____values_3)); }
	inline StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * get__values_3() const { return ____values_3; }
	inline StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 ** get_address_of__values_3() { return &____values_3; }
	inline void set__values_3(StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * value)
	{
		____values_3 = value;
		Il2CppCodeGenWriteBarrier((&____values_3), value);
	}
};

struct ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C_StaticFields
{
public:
	// System.Type[] System.Data.ColumnTypeConverter::s_types
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___s_types_2;

public:
	inline static int32_t get_offset_of_s_types_2() { return static_cast<int32_t>(offsetof(ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C_StaticFields, ___s_types_2)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_s_types_2() const { return ___s_types_2; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_s_types_2() { return &___s_types_2; }
	inline void set_s_types_2(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___s_types_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_types_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLUMNTYPECONVERTER_TC0CFD16E38C032C2274AF18C195363BC4047AB8C_H
#ifndef CONSTRAINTEXCEPTION_T38F5592AB8DF6A31990C7D0D787C5E585A25E662_H
#define CONSTRAINTEXCEPTION_T38F5592AB8DF6A31990C7D0D787C5E585A25E662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ConstraintException
struct  ConstraintException_t38F5592AB8DF6A31990C7D0D787C5E585A25E662  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTEXCEPTION_T38F5592AB8DF6A31990C7D0D787C5E585A25E662_H
#ifndef DATACOLUMN_T397593FCD95F7B10FA2D2E706EFDA54B05E5835D_H
#define DATACOLUMN_T397593FCD95F7B10FA2D2E706EFDA54B05E5835D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataColumn
struct  DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D  : public MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B
{
public:
	// System.Boolean System.Data.DataColumn::_allowNull
	bool ____allowNull_3;
	// System.String System.Data.DataColumn::_caption
	String_t* ____caption_4;
	// System.String System.Data.DataColumn::_columnName
	String_t* ____columnName_5;
	// System.Type System.Data.DataColumn::_dataType
	Type_t * ____dataType_6;
	// System.Data.Common.StorageType System.Data.DataColumn::_storageType
	int32_t ____storageType_7;
	// System.Object System.Data.DataColumn::_defaultValue
	RuntimeObject * ____defaultValue_8;
	// System.Data.DataSetDateTime System.Data.DataColumn::_dateTimeMode
	int32_t ____dateTimeMode_9;
	// System.Data.DataExpression System.Data.DataColumn::_expression
	DataExpression_tECCBF728C87CAF0185856F73F7DB54BB94EF094D * ____expression_10;
	// System.Int32 System.Data.DataColumn::_maxLength
	int32_t ____maxLength_11;
	// System.Int32 System.Data.DataColumn::_ordinal
	int32_t ____ordinal_12;
	// System.Boolean System.Data.DataColumn::_readOnly
	bool ____readOnly_13;
	// System.Data.Index System.Data.DataColumn::_sortIndex
	Index_t0B13AD066A6CAA0045DCA5BB8912F8E599BE9AF7 * ____sortIndex_14;
	// System.Data.DataTable System.Data.DataColumn::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_15;
	// System.Boolean System.Data.DataColumn::_unique
	bool ____unique_16;
	// System.Data.MappingType System.Data.DataColumn::_columnMapping
	int32_t ____columnMapping_17;
	// System.Int32 System.Data.DataColumn::_hashCode
	int32_t ____hashCode_18;
	// System.Int32 System.Data.DataColumn::_errors
	int32_t ____errors_19;
	// System.Boolean System.Data.DataColumn::_isSqlType
	bool ____isSqlType_20;
	// System.Boolean System.Data.DataColumn::_implementsINullable
	bool ____implementsINullable_21;
	// System.Boolean System.Data.DataColumn::_implementsIChangeTracking
	bool ____implementsIChangeTracking_22;
	// System.Boolean System.Data.DataColumn::_implementsIRevertibleChangeTracking
	bool ____implementsIRevertibleChangeTracking_23;
	// System.Boolean System.Data.DataColumn::_implementsIXMLSerializable
	bool ____implementsIXMLSerializable_24;
	// System.Boolean System.Data.DataColumn::_defaultValueIsNull
	bool ____defaultValueIsNull_25;
	// System.Collections.Generic.List`1<System.Data.DataColumn> System.Data.DataColumn::_dependentColumns
	List_1_t70187E1F2F9140ADB155B98F17D5D765F84B9204 * ____dependentColumns_26;
	// System.Data.PropertyCollection System.Data.DataColumn::_extendedProperties
	PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * ____extendedProperties_27;
	// System.Data.Common.DataStorage System.Data.DataColumn::_storage
	DataStorage_t48039E31DBE91E7C82D0DB461ABEDD50C6C017A6 * ____storage_28;
	// System.Data.AutoIncrementValue System.Data.DataColumn::_autoInc
	AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086 * ____autoInc_29;
	// System.String System.Data.DataColumn::_columnUri
	String_t* ____columnUri_30;
	// System.String System.Data.DataColumn::_columnPrefix
	String_t* ____columnPrefix_31;
	// System.String System.Data.DataColumn::_encodedColumnName
	String_t* ____encodedColumnName_32;
	// System.Data.SimpleType System.Data.DataColumn::_simpleType
	SimpleType_tB77732892B48FAB73D5074136016F9EC03006202 * ____simpleType_33;
	// System.Int32 System.Data.DataColumn::_objectID
	int32_t ____objectID_35;
	// System.String System.Data.DataColumn::<XmlDataType>k__BackingField
	String_t* ___U3CXmlDataTypeU3Ek__BackingField_36;
	// System.ComponentModel.PropertyChangedEventHandler System.Data.DataColumn::PropertyChanging
	PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * ___PropertyChanging_37;

public:
	inline static int32_t get_offset_of__allowNull_3() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____allowNull_3)); }
	inline bool get__allowNull_3() const { return ____allowNull_3; }
	inline bool* get_address_of__allowNull_3() { return &____allowNull_3; }
	inline void set__allowNull_3(bool value)
	{
		____allowNull_3 = value;
	}

	inline static int32_t get_offset_of__caption_4() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____caption_4)); }
	inline String_t* get__caption_4() const { return ____caption_4; }
	inline String_t** get_address_of__caption_4() { return &____caption_4; }
	inline void set__caption_4(String_t* value)
	{
		____caption_4 = value;
		Il2CppCodeGenWriteBarrier((&____caption_4), value);
	}

	inline static int32_t get_offset_of__columnName_5() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____columnName_5)); }
	inline String_t* get__columnName_5() const { return ____columnName_5; }
	inline String_t** get_address_of__columnName_5() { return &____columnName_5; }
	inline void set__columnName_5(String_t* value)
	{
		____columnName_5 = value;
		Il2CppCodeGenWriteBarrier((&____columnName_5), value);
	}

	inline static int32_t get_offset_of__dataType_6() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____dataType_6)); }
	inline Type_t * get__dataType_6() const { return ____dataType_6; }
	inline Type_t ** get_address_of__dataType_6() { return &____dataType_6; }
	inline void set__dataType_6(Type_t * value)
	{
		____dataType_6 = value;
		Il2CppCodeGenWriteBarrier((&____dataType_6), value);
	}

	inline static int32_t get_offset_of__storageType_7() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____storageType_7)); }
	inline int32_t get__storageType_7() const { return ____storageType_7; }
	inline int32_t* get_address_of__storageType_7() { return &____storageType_7; }
	inline void set__storageType_7(int32_t value)
	{
		____storageType_7 = value;
	}

	inline static int32_t get_offset_of__defaultValue_8() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____defaultValue_8)); }
	inline RuntimeObject * get__defaultValue_8() const { return ____defaultValue_8; }
	inline RuntimeObject ** get_address_of__defaultValue_8() { return &____defaultValue_8; }
	inline void set__defaultValue_8(RuntimeObject * value)
	{
		____defaultValue_8 = value;
		Il2CppCodeGenWriteBarrier((&____defaultValue_8), value);
	}

	inline static int32_t get_offset_of__dateTimeMode_9() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____dateTimeMode_9)); }
	inline int32_t get__dateTimeMode_9() const { return ____dateTimeMode_9; }
	inline int32_t* get_address_of__dateTimeMode_9() { return &____dateTimeMode_9; }
	inline void set__dateTimeMode_9(int32_t value)
	{
		____dateTimeMode_9 = value;
	}

	inline static int32_t get_offset_of__expression_10() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____expression_10)); }
	inline DataExpression_tECCBF728C87CAF0185856F73F7DB54BB94EF094D * get__expression_10() const { return ____expression_10; }
	inline DataExpression_tECCBF728C87CAF0185856F73F7DB54BB94EF094D ** get_address_of__expression_10() { return &____expression_10; }
	inline void set__expression_10(DataExpression_tECCBF728C87CAF0185856F73F7DB54BB94EF094D * value)
	{
		____expression_10 = value;
		Il2CppCodeGenWriteBarrier((&____expression_10), value);
	}

	inline static int32_t get_offset_of__maxLength_11() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____maxLength_11)); }
	inline int32_t get__maxLength_11() const { return ____maxLength_11; }
	inline int32_t* get_address_of__maxLength_11() { return &____maxLength_11; }
	inline void set__maxLength_11(int32_t value)
	{
		____maxLength_11 = value;
	}

	inline static int32_t get_offset_of__ordinal_12() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____ordinal_12)); }
	inline int32_t get__ordinal_12() const { return ____ordinal_12; }
	inline int32_t* get_address_of__ordinal_12() { return &____ordinal_12; }
	inline void set__ordinal_12(int32_t value)
	{
		____ordinal_12 = value;
	}

	inline static int32_t get_offset_of__readOnly_13() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____readOnly_13)); }
	inline bool get__readOnly_13() const { return ____readOnly_13; }
	inline bool* get_address_of__readOnly_13() { return &____readOnly_13; }
	inline void set__readOnly_13(bool value)
	{
		____readOnly_13 = value;
	}

	inline static int32_t get_offset_of__sortIndex_14() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____sortIndex_14)); }
	inline Index_t0B13AD066A6CAA0045DCA5BB8912F8E599BE9AF7 * get__sortIndex_14() const { return ____sortIndex_14; }
	inline Index_t0B13AD066A6CAA0045DCA5BB8912F8E599BE9AF7 ** get_address_of__sortIndex_14() { return &____sortIndex_14; }
	inline void set__sortIndex_14(Index_t0B13AD066A6CAA0045DCA5BB8912F8E599BE9AF7 * value)
	{
		____sortIndex_14 = value;
		Il2CppCodeGenWriteBarrier((&____sortIndex_14), value);
	}

	inline static int32_t get_offset_of__table_15() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____table_15)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_15() const { return ____table_15; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_15() { return &____table_15; }
	inline void set__table_15(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_15 = value;
		Il2CppCodeGenWriteBarrier((&____table_15), value);
	}

	inline static int32_t get_offset_of__unique_16() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____unique_16)); }
	inline bool get__unique_16() const { return ____unique_16; }
	inline bool* get_address_of__unique_16() { return &____unique_16; }
	inline void set__unique_16(bool value)
	{
		____unique_16 = value;
	}

	inline static int32_t get_offset_of__columnMapping_17() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____columnMapping_17)); }
	inline int32_t get__columnMapping_17() const { return ____columnMapping_17; }
	inline int32_t* get_address_of__columnMapping_17() { return &____columnMapping_17; }
	inline void set__columnMapping_17(int32_t value)
	{
		____columnMapping_17 = value;
	}

	inline static int32_t get_offset_of__hashCode_18() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____hashCode_18)); }
	inline int32_t get__hashCode_18() const { return ____hashCode_18; }
	inline int32_t* get_address_of__hashCode_18() { return &____hashCode_18; }
	inline void set__hashCode_18(int32_t value)
	{
		____hashCode_18 = value;
	}

	inline static int32_t get_offset_of__errors_19() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____errors_19)); }
	inline int32_t get__errors_19() const { return ____errors_19; }
	inline int32_t* get_address_of__errors_19() { return &____errors_19; }
	inline void set__errors_19(int32_t value)
	{
		____errors_19 = value;
	}

	inline static int32_t get_offset_of__isSqlType_20() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____isSqlType_20)); }
	inline bool get__isSqlType_20() const { return ____isSqlType_20; }
	inline bool* get_address_of__isSqlType_20() { return &____isSqlType_20; }
	inline void set__isSqlType_20(bool value)
	{
		____isSqlType_20 = value;
	}

	inline static int32_t get_offset_of__implementsINullable_21() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____implementsINullable_21)); }
	inline bool get__implementsINullable_21() const { return ____implementsINullable_21; }
	inline bool* get_address_of__implementsINullable_21() { return &____implementsINullable_21; }
	inline void set__implementsINullable_21(bool value)
	{
		____implementsINullable_21 = value;
	}

	inline static int32_t get_offset_of__implementsIChangeTracking_22() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____implementsIChangeTracking_22)); }
	inline bool get__implementsIChangeTracking_22() const { return ____implementsIChangeTracking_22; }
	inline bool* get_address_of__implementsIChangeTracking_22() { return &____implementsIChangeTracking_22; }
	inline void set__implementsIChangeTracking_22(bool value)
	{
		____implementsIChangeTracking_22 = value;
	}

	inline static int32_t get_offset_of__implementsIRevertibleChangeTracking_23() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____implementsIRevertibleChangeTracking_23)); }
	inline bool get__implementsIRevertibleChangeTracking_23() const { return ____implementsIRevertibleChangeTracking_23; }
	inline bool* get_address_of__implementsIRevertibleChangeTracking_23() { return &____implementsIRevertibleChangeTracking_23; }
	inline void set__implementsIRevertibleChangeTracking_23(bool value)
	{
		____implementsIRevertibleChangeTracking_23 = value;
	}

	inline static int32_t get_offset_of__implementsIXMLSerializable_24() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____implementsIXMLSerializable_24)); }
	inline bool get__implementsIXMLSerializable_24() const { return ____implementsIXMLSerializable_24; }
	inline bool* get_address_of__implementsIXMLSerializable_24() { return &____implementsIXMLSerializable_24; }
	inline void set__implementsIXMLSerializable_24(bool value)
	{
		____implementsIXMLSerializable_24 = value;
	}

	inline static int32_t get_offset_of__defaultValueIsNull_25() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____defaultValueIsNull_25)); }
	inline bool get__defaultValueIsNull_25() const { return ____defaultValueIsNull_25; }
	inline bool* get_address_of__defaultValueIsNull_25() { return &____defaultValueIsNull_25; }
	inline void set__defaultValueIsNull_25(bool value)
	{
		____defaultValueIsNull_25 = value;
	}

	inline static int32_t get_offset_of__dependentColumns_26() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____dependentColumns_26)); }
	inline List_1_t70187E1F2F9140ADB155B98F17D5D765F84B9204 * get__dependentColumns_26() const { return ____dependentColumns_26; }
	inline List_1_t70187E1F2F9140ADB155B98F17D5D765F84B9204 ** get_address_of__dependentColumns_26() { return &____dependentColumns_26; }
	inline void set__dependentColumns_26(List_1_t70187E1F2F9140ADB155B98F17D5D765F84B9204 * value)
	{
		____dependentColumns_26 = value;
		Il2CppCodeGenWriteBarrier((&____dependentColumns_26), value);
	}

	inline static int32_t get_offset_of__extendedProperties_27() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____extendedProperties_27)); }
	inline PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * get__extendedProperties_27() const { return ____extendedProperties_27; }
	inline PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 ** get_address_of__extendedProperties_27() { return &____extendedProperties_27; }
	inline void set__extendedProperties_27(PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * value)
	{
		____extendedProperties_27 = value;
		Il2CppCodeGenWriteBarrier((&____extendedProperties_27), value);
	}

	inline static int32_t get_offset_of__storage_28() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____storage_28)); }
	inline DataStorage_t48039E31DBE91E7C82D0DB461ABEDD50C6C017A6 * get__storage_28() const { return ____storage_28; }
	inline DataStorage_t48039E31DBE91E7C82D0DB461ABEDD50C6C017A6 ** get_address_of__storage_28() { return &____storage_28; }
	inline void set__storage_28(DataStorage_t48039E31DBE91E7C82D0DB461ABEDD50C6C017A6 * value)
	{
		____storage_28 = value;
		Il2CppCodeGenWriteBarrier((&____storage_28), value);
	}

	inline static int32_t get_offset_of__autoInc_29() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____autoInc_29)); }
	inline AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086 * get__autoInc_29() const { return ____autoInc_29; }
	inline AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086 ** get_address_of__autoInc_29() { return &____autoInc_29; }
	inline void set__autoInc_29(AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086 * value)
	{
		____autoInc_29 = value;
		Il2CppCodeGenWriteBarrier((&____autoInc_29), value);
	}

	inline static int32_t get_offset_of__columnUri_30() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____columnUri_30)); }
	inline String_t* get__columnUri_30() const { return ____columnUri_30; }
	inline String_t** get_address_of__columnUri_30() { return &____columnUri_30; }
	inline void set__columnUri_30(String_t* value)
	{
		____columnUri_30 = value;
		Il2CppCodeGenWriteBarrier((&____columnUri_30), value);
	}

	inline static int32_t get_offset_of__columnPrefix_31() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____columnPrefix_31)); }
	inline String_t* get__columnPrefix_31() const { return ____columnPrefix_31; }
	inline String_t** get_address_of__columnPrefix_31() { return &____columnPrefix_31; }
	inline void set__columnPrefix_31(String_t* value)
	{
		____columnPrefix_31 = value;
		Il2CppCodeGenWriteBarrier((&____columnPrefix_31), value);
	}

	inline static int32_t get_offset_of__encodedColumnName_32() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____encodedColumnName_32)); }
	inline String_t* get__encodedColumnName_32() const { return ____encodedColumnName_32; }
	inline String_t** get_address_of__encodedColumnName_32() { return &____encodedColumnName_32; }
	inline void set__encodedColumnName_32(String_t* value)
	{
		____encodedColumnName_32 = value;
		Il2CppCodeGenWriteBarrier((&____encodedColumnName_32), value);
	}

	inline static int32_t get_offset_of__simpleType_33() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____simpleType_33)); }
	inline SimpleType_tB77732892B48FAB73D5074136016F9EC03006202 * get__simpleType_33() const { return ____simpleType_33; }
	inline SimpleType_tB77732892B48FAB73D5074136016F9EC03006202 ** get_address_of__simpleType_33() { return &____simpleType_33; }
	inline void set__simpleType_33(SimpleType_tB77732892B48FAB73D5074136016F9EC03006202 * value)
	{
		____simpleType_33 = value;
		Il2CppCodeGenWriteBarrier((&____simpleType_33), value);
	}

	inline static int32_t get_offset_of__objectID_35() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____objectID_35)); }
	inline int32_t get__objectID_35() const { return ____objectID_35; }
	inline int32_t* get_address_of__objectID_35() { return &____objectID_35; }
	inline void set__objectID_35(int32_t value)
	{
		____objectID_35 = value;
	}

	inline static int32_t get_offset_of_U3CXmlDataTypeU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ___U3CXmlDataTypeU3Ek__BackingField_36)); }
	inline String_t* get_U3CXmlDataTypeU3Ek__BackingField_36() const { return ___U3CXmlDataTypeU3Ek__BackingField_36; }
	inline String_t** get_address_of_U3CXmlDataTypeU3Ek__BackingField_36() { return &___U3CXmlDataTypeU3Ek__BackingField_36; }
	inline void set_U3CXmlDataTypeU3Ek__BackingField_36(String_t* value)
	{
		___U3CXmlDataTypeU3Ek__BackingField_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CXmlDataTypeU3Ek__BackingField_36), value);
	}

	inline static int32_t get_offset_of_PropertyChanging_37() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ___PropertyChanging_37)); }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * get_PropertyChanging_37() const { return ___PropertyChanging_37; }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 ** get_address_of_PropertyChanging_37() { return &___PropertyChanging_37; }
	inline void set_PropertyChanging_37(PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * value)
	{
		___PropertyChanging_37 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyChanging_37), value);
	}
};

struct DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D_StaticFields
{
public:
	// System.Int32 System.Data.DataColumn::s_objectTypeCount
	int32_t ___s_objectTypeCount_34;

public:
	inline static int32_t get_offset_of_s_objectTypeCount_34() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D_StaticFields, ___s_objectTypeCount_34)); }
	inline int32_t get_s_objectTypeCount_34() const { return ___s_objectTypeCount_34; }
	inline int32_t* get_address_of_s_objectTypeCount_34() { return &___s_objectTypeCount_34; }
	inline void set_s_objectTypeCount_34(int32_t value)
	{
		___s_objectTypeCount_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACOLUMN_T397593FCD95F7B10FA2D2E706EFDA54B05E5835D_H
#ifndef DATAROW_TA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B_H
#define DATAROW_TA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRow
struct  DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B  : public RuntimeObject
{
public:
	// System.Data.DataTable System.Data.DataRow::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_0;
	// System.Data.DataColumnCollection System.Data.DataRow::_columns
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A * ____columns_1;
	// System.Int32 System.Data.DataRow::_oldRecord
	int32_t ____oldRecord_2;
	// System.Int32 System.Data.DataRow::_newRecord
	int32_t ____newRecord_3;
	// System.Int32 System.Data.DataRow::_tempRecord
	int32_t ____tempRecord_4;
	// System.Int64 System.Data.DataRow::_rowID
	int64_t ____rowID_5;
	// System.Data.DataRowAction System.Data.DataRow::_action
	int32_t ____action_6;
	// System.Boolean System.Data.DataRow::_inChangingEvent
	bool ____inChangingEvent_7;
	// System.Boolean System.Data.DataRow::_inDeletingEvent
	bool ____inDeletingEvent_8;
	// System.Boolean System.Data.DataRow::_inCascade
	bool ____inCascade_9;
	// System.Data.DataColumn System.Data.DataRow::_lastChangedColumn
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * ____lastChangedColumn_10;
	// System.Int32 System.Data.DataRow::_countColumnChange
	int32_t ____countColumnChange_11;
	// System.Data.DataError System.Data.DataRow::_error
	DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786 * ____error_12;
	// System.Int32 System.Data.DataRow::_rbTreeNodeId
	int32_t ____rbTreeNodeId_13;
	// System.Int32 System.Data.DataRow::_objectID
	int32_t ____objectID_15;

public:
	inline static int32_t get_offset_of__table_0() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____table_0)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_0() const { return ____table_0; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_0() { return &____table_0; }
	inline void set__table_0(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_0 = value;
		Il2CppCodeGenWriteBarrier((&____table_0), value);
	}

	inline static int32_t get_offset_of__columns_1() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____columns_1)); }
	inline DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A * get__columns_1() const { return ____columns_1; }
	inline DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A ** get_address_of__columns_1() { return &____columns_1; }
	inline void set__columns_1(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A * value)
	{
		____columns_1 = value;
		Il2CppCodeGenWriteBarrier((&____columns_1), value);
	}

	inline static int32_t get_offset_of__oldRecord_2() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____oldRecord_2)); }
	inline int32_t get__oldRecord_2() const { return ____oldRecord_2; }
	inline int32_t* get_address_of__oldRecord_2() { return &____oldRecord_2; }
	inline void set__oldRecord_2(int32_t value)
	{
		____oldRecord_2 = value;
	}

	inline static int32_t get_offset_of__newRecord_3() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____newRecord_3)); }
	inline int32_t get__newRecord_3() const { return ____newRecord_3; }
	inline int32_t* get_address_of__newRecord_3() { return &____newRecord_3; }
	inline void set__newRecord_3(int32_t value)
	{
		____newRecord_3 = value;
	}

	inline static int32_t get_offset_of__tempRecord_4() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____tempRecord_4)); }
	inline int32_t get__tempRecord_4() const { return ____tempRecord_4; }
	inline int32_t* get_address_of__tempRecord_4() { return &____tempRecord_4; }
	inline void set__tempRecord_4(int32_t value)
	{
		____tempRecord_4 = value;
	}

	inline static int32_t get_offset_of__rowID_5() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____rowID_5)); }
	inline int64_t get__rowID_5() const { return ____rowID_5; }
	inline int64_t* get_address_of__rowID_5() { return &____rowID_5; }
	inline void set__rowID_5(int64_t value)
	{
		____rowID_5 = value;
	}

	inline static int32_t get_offset_of__action_6() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____action_6)); }
	inline int32_t get__action_6() const { return ____action_6; }
	inline int32_t* get_address_of__action_6() { return &____action_6; }
	inline void set__action_6(int32_t value)
	{
		____action_6 = value;
	}

	inline static int32_t get_offset_of__inChangingEvent_7() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____inChangingEvent_7)); }
	inline bool get__inChangingEvent_7() const { return ____inChangingEvent_7; }
	inline bool* get_address_of__inChangingEvent_7() { return &____inChangingEvent_7; }
	inline void set__inChangingEvent_7(bool value)
	{
		____inChangingEvent_7 = value;
	}

	inline static int32_t get_offset_of__inDeletingEvent_8() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____inDeletingEvent_8)); }
	inline bool get__inDeletingEvent_8() const { return ____inDeletingEvent_8; }
	inline bool* get_address_of__inDeletingEvent_8() { return &____inDeletingEvent_8; }
	inline void set__inDeletingEvent_8(bool value)
	{
		____inDeletingEvent_8 = value;
	}

	inline static int32_t get_offset_of__inCascade_9() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____inCascade_9)); }
	inline bool get__inCascade_9() const { return ____inCascade_9; }
	inline bool* get_address_of__inCascade_9() { return &____inCascade_9; }
	inline void set__inCascade_9(bool value)
	{
		____inCascade_9 = value;
	}

	inline static int32_t get_offset_of__lastChangedColumn_10() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____lastChangedColumn_10)); }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * get__lastChangedColumn_10() const { return ____lastChangedColumn_10; }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D ** get_address_of__lastChangedColumn_10() { return &____lastChangedColumn_10; }
	inline void set__lastChangedColumn_10(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * value)
	{
		____lastChangedColumn_10 = value;
		Il2CppCodeGenWriteBarrier((&____lastChangedColumn_10), value);
	}

	inline static int32_t get_offset_of__countColumnChange_11() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____countColumnChange_11)); }
	inline int32_t get__countColumnChange_11() const { return ____countColumnChange_11; }
	inline int32_t* get_address_of__countColumnChange_11() { return &____countColumnChange_11; }
	inline void set__countColumnChange_11(int32_t value)
	{
		____countColumnChange_11 = value;
	}

	inline static int32_t get_offset_of__error_12() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____error_12)); }
	inline DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786 * get__error_12() const { return ____error_12; }
	inline DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786 ** get_address_of__error_12() { return &____error_12; }
	inline void set__error_12(DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786 * value)
	{
		____error_12 = value;
		Il2CppCodeGenWriteBarrier((&____error_12), value);
	}

	inline static int32_t get_offset_of__rbTreeNodeId_13() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____rbTreeNodeId_13)); }
	inline int32_t get__rbTreeNodeId_13() const { return ____rbTreeNodeId_13; }
	inline int32_t* get_address_of__rbTreeNodeId_13() { return &____rbTreeNodeId_13; }
	inline void set__rbTreeNodeId_13(int32_t value)
	{
		____rbTreeNodeId_13 = value;
	}

	inline static int32_t get_offset_of__objectID_15() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____objectID_15)); }
	inline int32_t get__objectID_15() const { return ____objectID_15; }
	inline int32_t* get_address_of__objectID_15() { return &____objectID_15; }
	inline void set__objectID_15(int32_t value)
	{
		____objectID_15 = value;
	}
};

struct DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B_StaticFields
{
public:
	// System.Int32 System.Data.DataRow::s_objectTypeCount
	int32_t ___s_objectTypeCount_14;

public:
	inline static int32_t get_offset_of_s_objectTypeCount_14() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B_StaticFields, ___s_objectTypeCount_14)); }
	inline int32_t get_s_objectTypeCount_14() const { return ___s_objectTypeCount_14; }
	inline int32_t* get_address_of_s_objectTypeCount_14() { return &___s_objectTypeCount_14; }
	inline void set_s_objectTypeCount_14(int32_t value)
	{
		___s_objectTypeCount_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAROW_TA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B_H
#ifndef DATAROWCHANGEEVENTARGS_TA5961502488B4B5C32F788CB86746087E043A444_H
#define DATAROWCHANGEEVENTARGS_TA5961502488B4B5C32F788CB86746087E043A444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRowChangeEventArgs
struct  DataRowChangeEventArgs_tA5961502488B4B5C32F788CB86746087E043A444  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Data.DataRow System.Data.DataRowChangeEventArgs::<Row>k__BackingField
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B * ___U3CRowU3Ek__BackingField_1;
	// System.Data.DataRowAction System.Data.DataRowChangeEventArgs::<Action>k__BackingField
	int32_t ___U3CActionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRowU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DataRowChangeEventArgs_tA5961502488B4B5C32F788CB86746087E043A444, ___U3CRowU3Ek__BackingField_1)); }
	inline DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B * get_U3CRowU3Ek__BackingField_1() const { return ___U3CRowU3Ek__BackingField_1; }
	inline DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B ** get_address_of_U3CRowU3Ek__BackingField_1() { return &___U3CRowU3Ek__BackingField_1; }
	inline void set_U3CRowU3Ek__BackingField_1(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B * value)
	{
		___U3CRowU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRowU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CActionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DataRowChangeEventArgs_tA5961502488B4B5C32F788CB86746087E043A444, ___U3CActionU3Ek__BackingField_2)); }
	inline int32_t get_U3CActionU3Ek__BackingField_2() const { return ___U3CActionU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CActionU3Ek__BackingField_2() { return &___U3CActionU3Ek__BackingField_2; }
	inline void set_U3CActionU3Ek__BackingField_2(int32_t value)
	{
		___U3CActionU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAROWCHANGEEVENTARGS_TA5961502488B4B5C32F788CB86746087E043A444_H
#ifndef DELETEDROWINACCESSIBLEEXCEPTION_TB043F9422B3AB955DCC1D5465B61C1BEE78A588A_H
#define DELETEDROWINACCESSIBLEEXCEPTION_TB043F9422B3AB955DCC1D5465B61C1BEE78A588A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DeletedRowInaccessibleException
struct  DeletedRowInaccessibleException_tB043F9422B3AB955DCC1D5465B61C1BEE78A588A  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETEDROWINACCESSIBLEEXCEPTION_TB043F9422B3AB955DCC1D5465B61C1BEE78A588A_H
#ifndef DUPLICATENAMEEXCEPTION_T5D57DF3B9D0B944ECD83B9F2672400E377F80448_H
#define DUPLICATENAMEEXCEPTION_T5D57DF3B9D0B944ECD83B9F2672400E377F80448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DuplicateNameException
struct  DuplicateNameException_t5D57DF3B9D0B944ECD83B9F2672400E377F80448  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUPLICATENAMEEXCEPTION_T5D57DF3B9D0B944ECD83B9F2672400E377F80448_H
#ifndef INROWCHANGINGEVENTEXCEPTION_T4CEE672B6EC85565D9A4EF5A5103BF2DCCA4CB9E_H
#define INROWCHANGINGEVENTEXCEPTION_T4CEE672B6EC85565D9A4EF5A5103BF2DCCA4CB9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.InRowChangingEventException
struct  InRowChangingEventException_t4CEE672B6EC85565D9A4EF5A5103BF2DCCA4CB9E  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INROWCHANGINGEVENTEXCEPTION_T4CEE672B6EC85565D9A4EF5A5103BF2DCCA4CB9E_H
#ifndef INVALIDCONSTRAINTEXCEPTION_TD2E64652BEA365247A6012CC0DA5C4B3F201E7DF_H
#define INVALIDCONSTRAINTEXCEPTION_TD2E64652BEA365247A6012CC0DA5C4B3F201E7DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.InvalidConstraintException
struct  InvalidConstraintException_tD2E64652BEA365247A6012CC0DA5C4B3F201E7DF  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDCONSTRAINTEXCEPTION_TD2E64652BEA365247A6012CC0DA5C4B3F201E7DF_H
#ifndef NONULLALLOWEDEXCEPTION_T2FF3B7ADEEE81C689279686D6BFF76F195B06D1D_H
#define NONULLALLOWEDEXCEPTION_T2FF3B7ADEEE81C689279686D6BFF76F195B06D1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.NoNullAllowedException
struct  NoNullAllowedException_t2FF3B7ADEEE81C689279686D6BFF76F195B06D1D  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONULLALLOWEDEXCEPTION_T2FF3B7ADEEE81C689279686D6BFF76F195B06D1D_H
#ifndef RBTREE_1_T52494974B2E56D078473058BA9061372549CE5B2_H
#define RBTREE_1_T52494974B2E56D078473058BA9061372549CE5B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.RBTree`1<System.Data.DataRow>
struct  RBTree_1_t52494974B2E56D078473058BA9061372549CE5B2  : public RuntimeObject
{
public:
	// System.Data.RBTree`1/TreePage<K>[] System.Data.RBTree`1::_pageTable
	TreePageU5BU5D_tBBE7049BB6412C75C6BF3F25774F9E64CA96CFE6* ____pageTable_0;
	// System.Int32[] System.Data.RBTree`1::_pageTableMap
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____pageTableMap_1;
	// System.Int32 System.Data.RBTree`1::_inUsePageCount
	int32_t ____inUsePageCount_2;
	// System.Int32 System.Data.RBTree`1::_nextFreePageLine
	int32_t ____nextFreePageLine_3;
	// System.Int32 System.Data.RBTree`1::root
	int32_t ___root_4;
	// System.Int32 System.Data.RBTree`1::_version
	int32_t ____version_5;
	// System.Int32 System.Data.RBTree`1::_inUseNodeCount
	int32_t ____inUseNodeCount_6;
	// System.Int32 System.Data.RBTree`1::_inUseSatelliteTreeCount
	int32_t ____inUseSatelliteTreeCount_7;
	// System.Data.TreeAccessMethod System.Data.RBTree`1::_accessMethod
	int32_t ____accessMethod_8;

public:
	inline static int32_t get_offset_of__pageTable_0() { return static_cast<int32_t>(offsetof(RBTree_1_t52494974B2E56D078473058BA9061372549CE5B2, ____pageTable_0)); }
	inline TreePageU5BU5D_tBBE7049BB6412C75C6BF3F25774F9E64CA96CFE6* get__pageTable_0() const { return ____pageTable_0; }
	inline TreePageU5BU5D_tBBE7049BB6412C75C6BF3F25774F9E64CA96CFE6** get_address_of__pageTable_0() { return &____pageTable_0; }
	inline void set__pageTable_0(TreePageU5BU5D_tBBE7049BB6412C75C6BF3F25774F9E64CA96CFE6* value)
	{
		____pageTable_0 = value;
		Il2CppCodeGenWriteBarrier((&____pageTable_0), value);
	}

	inline static int32_t get_offset_of__pageTableMap_1() { return static_cast<int32_t>(offsetof(RBTree_1_t52494974B2E56D078473058BA9061372549CE5B2, ____pageTableMap_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__pageTableMap_1() const { return ____pageTableMap_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__pageTableMap_1() { return &____pageTableMap_1; }
	inline void set__pageTableMap_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____pageTableMap_1 = value;
		Il2CppCodeGenWriteBarrier((&____pageTableMap_1), value);
	}

	inline static int32_t get_offset_of__inUsePageCount_2() { return static_cast<int32_t>(offsetof(RBTree_1_t52494974B2E56D078473058BA9061372549CE5B2, ____inUsePageCount_2)); }
	inline int32_t get__inUsePageCount_2() const { return ____inUsePageCount_2; }
	inline int32_t* get_address_of__inUsePageCount_2() { return &____inUsePageCount_2; }
	inline void set__inUsePageCount_2(int32_t value)
	{
		____inUsePageCount_2 = value;
	}

	inline static int32_t get_offset_of__nextFreePageLine_3() { return static_cast<int32_t>(offsetof(RBTree_1_t52494974B2E56D078473058BA9061372549CE5B2, ____nextFreePageLine_3)); }
	inline int32_t get__nextFreePageLine_3() const { return ____nextFreePageLine_3; }
	inline int32_t* get_address_of__nextFreePageLine_3() { return &____nextFreePageLine_3; }
	inline void set__nextFreePageLine_3(int32_t value)
	{
		____nextFreePageLine_3 = value;
	}

	inline static int32_t get_offset_of_root_4() { return static_cast<int32_t>(offsetof(RBTree_1_t52494974B2E56D078473058BA9061372549CE5B2, ___root_4)); }
	inline int32_t get_root_4() const { return ___root_4; }
	inline int32_t* get_address_of_root_4() { return &___root_4; }
	inline void set_root_4(int32_t value)
	{
		___root_4 = value;
	}

	inline static int32_t get_offset_of__version_5() { return static_cast<int32_t>(offsetof(RBTree_1_t52494974B2E56D078473058BA9061372549CE5B2, ____version_5)); }
	inline int32_t get__version_5() const { return ____version_5; }
	inline int32_t* get_address_of__version_5() { return &____version_5; }
	inline void set__version_5(int32_t value)
	{
		____version_5 = value;
	}

	inline static int32_t get_offset_of__inUseNodeCount_6() { return static_cast<int32_t>(offsetof(RBTree_1_t52494974B2E56D078473058BA9061372549CE5B2, ____inUseNodeCount_6)); }
	inline int32_t get__inUseNodeCount_6() const { return ____inUseNodeCount_6; }
	inline int32_t* get_address_of__inUseNodeCount_6() { return &____inUseNodeCount_6; }
	inline void set__inUseNodeCount_6(int32_t value)
	{
		____inUseNodeCount_6 = value;
	}

	inline static int32_t get_offset_of__inUseSatelliteTreeCount_7() { return static_cast<int32_t>(offsetof(RBTree_1_t52494974B2E56D078473058BA9061372549CE5B2, ____inUseSatelliteTreeCount_7)); }
	inline int32_t get__inUseSatelliteTreeCount_7() const { return ____inUseSatelliteTreeCount_7; }
	inline int32_t* get_address_of__inUseSatelliteTreeCount_7() { return &____inUseSatelliteTreeCount_7; }
	inline void set__inUseSatelliteTreeCount_7(int32_t value)
	{
		____inUseSatelliteTreeCount_7 = value;
	}

	inline static int32_t get_offset_of__accessMethod_8() { return static_cast<int32_t>(offsetof(RBTree_1_t52494974B2E56D078473058BA9061372549CE5B2, ____accessMethod_8)); }
	inline int32_t get__accessMethod_8() const { return ____accessMethod_8; }
	inline int32_t* get_address_of__accessMethod_8() { return &____accessMethod_8; }
	inline void set__accessMethod_8(int32_t value)
	{
		____accessMethod_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RBTREE_1_T52494974B2E56D078473058BA9061372549CE5B2_H
#ifndef READONLYEXCEPTION_T6FEDA3BE846CBEF2C4AE11165DC094BFCFF2F7D6_H
#define READONLYEXCEPTION_T6FEDA3BE846CBEF2C4AE11165DC094BFCFF2F7D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ReadOnlyException
struct  ReadOnlyException_t6FEDA3BE846CBEF2C4AE11165DC094BFCFF2F7D6  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYEXCEPTION_T6FEDA3BE846CBEF2C4AE11165DC094BFCFF2F7D6_H
#ifndef ROWNOTINTABLEEXCEPTION_T85FF30B2DC1D594CADD7AA0803E4A32CDFE2000F_H
#define ROWNOTINTABLEEXCEPTION_T85FF30B2DC1D594CADD7AA0803E4A32CDFE2000F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.RowNotInTableException
struct  RowNotInTableException_t85FF30B2DC1D594CADD7AA0803E4A32CDFE2000F  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROWNOTINTABLEEXCEPTION_T85FF30B2DC1D594CADD7AA0803E4A32CDFE2000F_H
#ifndef VERSIONNOTFOUNDEXCEPTION_T7A0CF1C23F03BD674086DBB8170914DDBA23C181_H
#define VERSIONNOTFOUNDEXCEPTION_T7A0CF1C23F03BD674086DBB8170914DDBA23C181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.VersionNotFoundException
struct  VersionNotFoundException_t7A0CF1C23F03BD674086DBB8170914DDBA23C181  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSIONNOTFOUNDEXCEPTION_T7A0CF1C23F03BD674086DBB8170914DDBA23C181_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef VIDEOCLIPPLAYABLE_T4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12_H
#define VIDEOCLIPPLAYABLE_T4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Video.VideoClipPlayable
struct  VideoClipPlayable_t4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Video.VideoClipPlayable::m_Handle
	PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(VideoClipPlayable_t4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12, ___m_Handle_0)); }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCLIPPLAYABLE_T4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12_H
#ifndef VIDEOCLIP_TA4039CBBC6F9C3AD62B067964A6C20C6FB7376D5_H
#define VIDEOCLIP_TA4039CBBC6F9C3AD62B067964A6C20C6FB7376D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoClip
struct  VideoClip_tA4039CBBC6F9C3AD62B067964A6C20C6FB7376D5  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCLIP_TA4039CBBC6F9C3AD62B067964A6C20C6FB7376D5_H
#ifndef CONSTRAINTCONVERTER_T7AFB06FC75603F71A099A48A36ACC0EFCF178D93_H
#define CONSTRAINTCONVERTER_T7AFB06FC75603F71A099A48A36ACC0EFCF178D93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ConstraintConverter
struct  ConstraintConverter_t7AFB06FC75603F71A099A48A36ACC0EFCF178D93  : public ExpandableObjectConverter_tC19580E01F630034FD5140CFA7453E1125E13F99
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTCONVERTER_T7AFB06FC75603F71A099A48A36ACC0EFCF178D93_H
#ifndef DATACOLUMNCHANGEEVENTHANDLER_TC860540A9091FE1BB1DF718AB44874A54585FE07_H
#define DATACOLUMNCHANGEEVENTHANDLER_TC860540A9091FE1BB1DF718AB44874A54585FE07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataColumnChangeEventHandler
struct  DataColumnChangeEventHandler_tC860540A9091FE1BB1DF718AB44874A54585FE07  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACOLUMNCHANGEEVENTHANDLER_TC860540A9091FE1BB1DF718AB44874A54585FE07_H
#ifndef DATAROWCHANGEEVENTHANDLER_T528DC5369A320B2397E1E5CF4E27CC518C0C72A0_H
#define DATAROWCHANGEEVENTHANDLER_T528DC5369A320B2397E1E5CF4E27CC518C0C72A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRowChangeEventHandler
struct  DataRowChangeEventHandler_t528DC5369A320B2397E1E5CF4E27CC518C0C72A0  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAROWCHANGEEVENTHANDLER_T528DC5369A320B2397E1E5CF4E27CC518C0C72A0_H
#ifndef DATAROWTREE_T885C3CBC17060B726BFEE177710D6E9E57FEA230_H
#define DATAROWTREE_T885C3CBC17060B726BFEE177710D6E9E57FEA230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRowCollection/DataRowTree
struct  DataRowTree_t885C3CBC17060B726BFEE177710D6E9E57FEA230  : public RBTree_1_t52494974B2E56D078473058BA9061372549CE5B2
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAROWTREE_T885C3CBC17060B726BFEE177710D6E9E57FEA230_H
#ifndef DATAROWCREATEDEVENTHANDLER_T03CF53F5DF69A9FC021AC0092080CCDC74916F82_H
#define DATAROWCREATEDEVENTHANDLER_T03CF53F5DF69A9FC021AC0092080CCDC74916F82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRowCreatedEventHandler
struct  DataRowCreatedEventHandler_t03CF53F5DF69A9FC021AC0092080CCDC74916F82  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAROWCREATEDEVENTHANDLER_T03CF53F5DF69A9FC021AC0092080CCDC74916F82_H
#ifndef DATASETCLEAREVENTHANDLER_T301F54C2A702D53B71D57E255D2B6B28D7420833_H
#define DATASETCLEAREVENTHANDLER_T301F54C2A702D53B71D57E255D2B6B28D7420833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataSetClearEventhandler
struct  DataSetClearEventhandler_t301F54C2A702D53B71D57E255D2B6B28D7420833  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETCLEAREVENTHANDLER_T301F54C2A702D53B71D57E255D2B6B28D7420833_H
#ifndef COMPLETIONHANDLER_T57827C46E0121AC0285B25FF50B80A23FFE2A7CE_H
#define COMPLETIONHANDLER_T57827C46E0121AC0285B25FF50B80A23FFE2A7CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.IosNativePopups/CompletionHandler
struct  CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPLETIONHANDLER_T57827C46E0121AC0285B25FF50B80A23FFE2A7CE_H
#ifndef INTERNALHANDLER_T99221D275AB4B7194ED4F74F9193CA944BD83FD5_H
#define INTERNALHANDLER_T99221D275AB4B7194ED4F74F9193CA944BD83FD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.IosNativePopups/InternalHandler
struct  InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALHANDLER_T99221D275AB4B7194ED4F74F9193CA944BD83FD5_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef ERROREVENTHANDLER_TF5863946928B48BE13146ED5FF70AC92678FE222_H
#define ERROREVENTHANDLER_TF5863946928B48BE13146ED5FF70AC92678FE222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/ErrorEventHandler
struct  ErrorEventHandler_tF5863946928B48BE13146ED5FF70AC92678FE222  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTHANDLER_TF5863946928B48BE13146ED5FF70AC92678FE222_H
#ifndef EVENTHANDLER_T5069D72E1ED46BD04F19D8D4534811B95A8E2308_H
#define EVENTHANDLER_T5069D72E1ED46BD04F19D8D4534811B95A8E2308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/EventHandler
struct  EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_T5069D72E1ED46BD04F19D8D4534811B95A8E2308_H
#ifndef FRAMEREADYEVENTHANDLER_T518B277D916AB292680CAA186BCDB3D3EF130422_H
#define FRAMEREADYEVENTHANDLER_T518B277D916AB292680CAA186BCDB3D3EF130422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler
struct  FrameReadyEventHandler_t518B277D916AB292680CAA186BCDB3D3EF130422  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEREADYEVENTHANDLER_T518B277D916AB292680CAA186BCDB3D3EF130422_H
#ifndef TIMEEVENTHANDLER_TDD815DAABFADDD98C8993B2A97A2FCE858266BC1_H
#define TIMEEVENTHANDLER_TDD815DAABFADDD98C8993B2A97A2FCE858266BC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/TimeEventHandler
struct  TimeEventHandler_tDD815DAABFADDD98C8993B2A97A2FCE858266BC1  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEEVENTHANDLER_TDD815DAABFADDD98C8993B2A97A2FCE858266BC1_H
#ifndef VISUALEFFECT_TBB62FCDE4826BA7FE8BEB728EC616C8E7ED5A506_H
#define VISUALEFFECT_TBB62FCDE4826BA7FE8BEB728EC616C8E7ED5A506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.VFX.VisualEffect
struct  VisualEffect_tBB62FCDE4826BA7FE8BEB728EC616C8E7ED5A506  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISUALEFFECT_TBB62FCDE4826BA7FE8BEB728EC616C8E7ED5A506_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef VIDEOPLAYER_TFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2_H
#define VIDEOPLAYER_TFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer
struct  VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::prepareCompleted
	EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * ___prepareCompleted_4;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::loopPointReached
	EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * ___loopPointReached_5;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::started
	EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * ___started_6;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::frameDropped
	EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * ___frameDropped_7;
	// UnityEngine.Video.VideoPlayer/ErrorEventHandler UnityEngine.Video.VideoPlayer::errorReceived
	ErrorEventHandler_tF5863946928B48BE13146ED5FF70AC92678FE222 * ___errorReceived_8;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::seekCompleted
	EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * ___seekCompleted_9;
	// UnityEngine.Video.VideoPlayer/TimeEventHandler UnityEngine.Video.VideoPlayer::clockResyncOccurred
	TimeEventHandler_tDD815DAABFADDD98C8993B2A97A2FCE858266BC1 * ___clockResyncOccurred_10;
	// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler UnityEngine.Video.VideoPlayer::frameReady
	FrameReadyEventHandler_t518B277D916AB292680CAA186BCDB3D3EF130422 * ___frameReady_11;

public:
	inline static int32_t get_offset_of_prepareCompleted_4() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___prepareCompleted_4)); }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * get_prepareCompleted_4() const { return ___prepareCompleted_4; }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 ** get_address_of_prepareCompleted_4() { return &___prepareCompleted_4; }
	inline void set_prepareCompleted_4(EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * value)
	{
		___prepareCompleted_4 = value;
		Il2CppCodeGenWriteBarrier((&___prepareCompleted_4), value);
	}

	inline static int32_t get_offset_of_loopPointReached_5() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___loopPointReached_5)); }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * get_loopPointReached_5() const { return ___loopPointReached_5; }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 ** get_address_of_loopPointReached_5() { return &___loopPointReached_5; }
	inline void set_loopPointReached_5(EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * value)
	{
		___loopPointReached_5 = value;
		Il2CppCodeGenWriteBarrier((&___loopPointReached_5), value);
	}

	inline static int32_t get_offset_of_started_6() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___started_6)); }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * get_started_6() const { return ___started_6; }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 ** get_address_of_started_6() { return &___started_6; }
	inline void set_started_6(EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * value)
	{
		___started_6 = value;
		Il2CppCodeGenWriteBarrier((&___started_6), value);
	}

	inline static int32_t get_offset_of_frameDropped_7() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___frameDropped_7)); }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * get_frameDropped_7() const { return ___frameDropped_7; }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 ** get_address_of_frameDropped_7() { return &___frameDropped_7; }
	inline void set_frameDropped_7(EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * value)
	{
		___frameDropped_7 = value;
		Il2CppCodeGenWriteBarrier((&___frameDropped_7), value);
	}

	inline static int32_t get_offset_of_errorReceived_8() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___errorReceived_8)); }
	inline ErrorEventHandler_tF5863946928B48BE13146ED5FF70AC92678FE222 * get_errorReceived_8() const { return ___errorReceived_8; }
	inline ErrorEventHandler_tF5863946928B48BE13146ED5FF70AC92678FE222 ** get_address_of_errorReceived_8() { return &___errorReceived_8; }
	inline void set_errorReceived_8(ErrorEventHandler_tF5863946928B48BE13146ED5FF70AC92678FE222 * value)
	{
		___errorReceived_8 = value;
		Il2CppCodeGenWriteBarrier((&___errorReceived_8), value);
	}

	inline static int32_t get_offset_of_seekCompleted_9() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___seekCompleted_9)); }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * get_seekCompleted_9() const { return ___seekCompleted_9; }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 ** get_address_of_seekCompleted_9() { return &___seekCompleted_9; }
	inline void set_seekCompleted_9(EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * value)
	{
		___seekCompleted_9 = value;
		Il2CppCodeGenWriteBarrier((&___seekCompleted_9), value);
	}

	inline static int32_t get_offset_of_clockResyncOccurred_10() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___clockResyncOccurred_10)); }
	inline TimeEventHandler_tDD815DAABFADDD98C8993B2A97A2FCE858266BC1 * get_clockResyncOccurred_10() const { return ___clockResyncOccurred_10; }
	inline TimeEventHandler_tDD815DAABFADDD98C8993B2A97A2FCE858266BC1 ** get_address_of_clockResyncOccurred_10() { return &___clockResyncOccurred_10; }
	inline void set_clockResyncOccurred_10(TimeEventHandler_tDD815DAABFADDD98C8993B2A97A2FCE858266BC1 * value)
	{
		___clockResyncOccurred_10 = value;
		Il2CppCodeGenWriteBarrier((&___clockResyncOccurred_10), value);
	}

	inline static int32_t get_offset_of_frameReady_11() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___frameReady_11)); }
	inline FrameReadyEventHandler_t518B277D916AB292680CAA186BCDB3D3EF130422 * get_frameReady_11() const { return ___frameReady_11; }
	inline FrameReadyEventHandler_t518B277D916AB292680CAA186BCDB3D3EF130422 ** get_address_of_frameReady_11() { return &___frameReady_11; }
	inline void set_frameReady_11(FrameReadyEventHandler_t518B277D916AB292680CAA186BCDB3D3EF130422 * value)
	{
		___frameReady_11 = value;
		Il2CppCodeGenWriteBarrier((&___frameReady_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOPLAYER_TFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2_H
#ifndef ALERTDIALOGPROXY_T55B5EC27E25A527CCB4EE248A488E8B702897241_H
#define ALERTDIALOGPROXY_T55B5EC27E25A527CCB4EE248A488E8B702897241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.AlertDialogProxy
struct  AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action TheNextFlow.UnityPlugins.AlertDialogProxy::onOk
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onOk_4;
	// System.Action TheNextFlow.UnityPlugins.AlertDialogProxy::onCancel
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onCancel_5;
	// System.Action TheNextFlow.UnityPlugins.AlertDialogProxy::onNeutral
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onNeutral_6;

public:
	inline static int32_t get_offset_of_onOk_4() { return static_cast<int32_t>(offsetof(AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241, ___onOk_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onOk_4() const { return ___onOk_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onOk_4() { return &___onOk_4; }
	inline void set_onOk_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onOk_4 = value;
		Il2CppCodeGenWriteBarrier((&___onOk_4), value);
	}

	inline static int32_t get_offset_of_onCancel_5() { return static_cast<int32_t>(offsetof(AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241, ___onCancel_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onCancel_5() const { return ___onCancel_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onCancel_5() { return &___onCancel_5; }
	inline void set_onCancel_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onCancel_5 = value;
		Il2CppCodeGenWriteBarrier((&___onCancel_5), value);
	}

	inline static int32_t get_offset_of_onNeutral_6() { return static_cast<int32_t>(offsetof(AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241, ___onNeutral_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onNeutral_6() const { return ___onNeutral_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onNeutral_6() { return &___onNeutral_6; }
	inline void set_onNeutral_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onNeutral_6 = value;
		Il2CppCodeGenWriteBarrier((&___onNeutral_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTDIALOGPROXY_T55B5EC27E25A527CCB4EE248A488E8B702897241_H
#ifndef DATEPICKERDIALOGPROXY_T3DC13C8B3EFB9AB6890BF88F75AD27564AEB0F0E_H
#define DATEPICKERDIALOGPROXY_T3DC13C8B3EFB9AB6890BF88F75AD27564AEB0F0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.DatePickerDialogProxy
struct  DatePickerDialogProxy_t3DC13C8B3EFB9AB6890BF88F75AD27564AEB0F0E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action`3<System.Int32,System.Int32,System.Int32> TheNextFlow.UnityPlugins.DatePickerDialogProxy::onDateSet
	Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB * ___onDateSet_4;

public:
	inline static int32_t get_offset_of_onDateSet_4() { return static_cast<int32_t>(offsetof(DatePickerDialogProxy_t3DC13C8B3EFB9AB6890BF88F75AD27564AEB0F0E, ___onDateSet_4)); }
	inline Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB * get_onDateSet_4() const { return ___onDateSet_4; }
	inline Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB ** get_address_of_onDateSet_4() { return &___onDateSet_4; }
	inline void set_onDateSet_4(Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB * value)
	{
		___onDateSet_4 = value;
		Il2CppCodeGenWriteBarrier((&___onDateSet_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEPICKERDIALOGPROXY_T3DC13C8B3EFB9AB6890BF88F75AD27564AEB0F0E_H
#ifndef TIMEPICKERDIALOGPROXY_TEA3E774F2ACE744CCC2DBBF4A78BD2049B76E266_H
#define TIMEPICKERDIALOGPROXY_TEA3E774F2ACE744CCC2DBBF4A78BD2049B76E266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheNextFlow.UnityPlugins.TimePickerDialogProxy
struct  TimePickerDialogProxy_tEA3E774F2ACE744CCC2DBBF4A78BD2049B76E266  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action`2<System.Int32,System.Int32> TheNextFlow.UnityPlugins.TimePickerDialogProxy::onTimeSet
	Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * ___onTimeSet_4;

public:
	inline static int32_t get_offset_of_onTimeSet_4() { return static_cast<int32_t>(offsetof(TimePickerDialogProxy_tEA3E774F2ACE744CCC2DBBF4A78BD2049B76E266, ___onTimeSet_4)); }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * get_onTimeSet_4() const { return ___onTimeSet_4; }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE ** get_address_of_onTimeSet_4() { return &___onTimeSet_4; }
	inline void set_onTimeSet_4(Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * value)
	{
		___onTimeSet_4 = value;
		Il2CppCodeGenWriteBarrier((&___onTimeSet_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEPICKERDIALOGPROXY_TEA3E774F2ACE744CCC2DBBF4A78BD2049B76E266_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4600 = { sizeof (VFXSpawnerState_t614A1AADC2EDB20E82A625BE053A22DB31D89AC7), sizeof(VFXSpawnerState_t614A1AADC2EDB20E82A625BE053A22DB31D89AC7_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4600[2] = 
{
	VFXSpawnerState_t614A1AADC2EDB20E82A625BE053A22DB31D89AC7::get_offset_of_m_Ptr_0(),
	VFXSpawnerState_t614A1AADC2EDB20E82A625BE053A22DB31D89AC7::get_offset_of_m_Owner_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4601 = { sizeof (VisualEffect_tBB62FCDE4826BA7FE8BEB728EC616C8E7ED5A506), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4602 = { sizeof (U3CModuleU3E_tB054F17A779AC945E3659AF119A96DB806541AF9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4603 = { sizeof (XRDevice_t392FCA3D1DCEB95FF500C8F374C88B034C31DF4A), -1, sizeof(XRDevice_t392FCA3D1DCEB95FF500C8F374C88B034C31DF4A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4603[1] = 
{
	XRDevice_t392FCA3D1DCEB95FF500C8F374C88B034C31DF4A_StaticFields::get_offset_of_deviceLoaded_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4604 = { sizeof (U3CModuleU3E_t064756C4EE8D64CEFE107E600CEBCB3F77894990), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4605 = { sizeof (VideoClip_tA4039CBBC6F9C3AD62B067964A6C20C6FB7376D5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4606 = { sizeof (VideoClipPlayable_t4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12)+ sizeof (RuntimeObject), sizeof(VideoClipPlayable_t4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4606[1] = 
{
	VideoClipPlayable_t4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4607 = { sizeof (VideoRenderMode_t0DBAABB576FDA890C49C6AD3EE641623F93E9161)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4607[6] = 
{
	VideoRenderMode_t0DBAABB576FDA890C49C6AD3EE641623F93E9161::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4608 = { sizeof (Video3DLayout_t5F64D0CE5E9B37C2FCE67F397FA5CFE9C047E4A1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4608[4] = 
{
	Video3DLayout_t5F64D0CE5E9B37C2FCE67F397FA5CFE9C047E4A1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4609 = { sizeof (VideoAspectRatio_t5739968D28C4F8F802B085E293F22110205B8379)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4609[7] = 
{
	VideoAspectRatio_t5739968D28C4F8F802B085E293F22110205B8379::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4610 = { sizeof (VideoTimeSource_t15F04FD6B3D75A8D98480E8B77117C0FF691BB77)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4610[3] = 
{
	VideoTimeSource_t15F04FD6B3D75A8D98480E8B77117C0FF691BB77::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4611 = { sizeof (VideoTimeReference_t9EAEBD354AE5E56F0D0F36E73A428BB2A0B8B31B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4611[4] = 
{
	VideoTimeReference_t9EAEBD354AE5E56F0D0F36E73A428BB2A0B8B31B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4612 = { sizeof (VideoSource_t32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4612[3] = 
{
	VideoSource_t32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4613 = { sizeof (VideoAudioOutputMode_t8CDE10B382F3C321345EC57C9164A9177139DC6F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4613[5] = 
{
	VideoAudioOutputMode_t8CDE10B382F3C321345EC57C9164A9177139DC6F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4614 = { sizeof (VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4614[8] = 
{
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_prepareCompleted_4(),
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_loopPointReached_5(),
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_started_6(),
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_frameDropped_7(),
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_errorReceived_8(),
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_seekCompleted_9(),
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_clockResyncOccurred_10(),
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_frameReady_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4615 = { sizeof (EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4616 = { sizeof (ErrorEventHandler_tF5863946928B48BE13146ED5FF70AC92678FE222), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4617 = { sizeof (FrameReadyEventHandler_t518B277D916AB292680CAA186BCDB3D3EF130422), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4618 = { sizeof (TimeEventHandler_tDD815DAABFADDD98C8993B2A97A2FCE858266BC1), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4619 = { sizeof (U3CModuleU3E_t256052731F6AF04064254ED502AB58F751060075), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4620 = { sizeof (SR_t9F9B78806C8CD422A08572CB2F28D766146CF3AB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4621 = { sizeof (BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC)+ sizeof (RuntimeObject), sizeof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_marshaled_pinvoke), sizeof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4621[7] = 
{
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC::get_offset_of__sign_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC::get_offset_of__bits_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields::get_offset_of_s_bnMinInt_2(),
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields::get_offset_of_s_bnOneInt_3(),
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields::get_offset_of_s_bnZeroInt_4(),
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields::get_offset_of_s_bnMinusOneInt_5(),
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields::get_offset_of_s_success_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4622 = { sizeof (GetBytesMode_t58B43CD1A2F9BB629A2EE28F59E0FF610C79A3B5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4622[4] = 
{
	GetBytesMode_t58B43CD1A2F9BB629A2EE28F59E0FF610C79A3B5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4623 = { sizeof (BigIntegerCalculator_tFB78069521210FFCDF1D75AF99008213A8763F25), -1, sizeof(BigIntegerCalculator_tFB78069521210FFCDF1D75AF99008213A8763F25_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4623[4] = 
{
	BigIntegerCalculator_tFB78069521210FFCDF1D75AF99008213A8763F25_StaticFields::get_offset_of_ReducerThreshold_0(),
	BigIntegerCalculator_tFB78069521210FFCDF1D75AF99008213A8763F25_StaticFields::get_offset_of_SquareThreshold_1(),
	BigIntegerCalculator_tFB78069521210FFCDF1D75AF99008213A8763F25_StaticFields::get_offset_of_AllocationThreshold_2(),
	BigIntegerCalculator_tFB78069521210FFCDF1D75AF99008213A8763F25_StaticFields::get_offset_of_MultiplyThreshold_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4624 = { sizeof (BigNumber_t219B8EAECD7E9014F6B25E8E7A3152F1F50783BA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4625 = { sizeof (BigNumberBuffer_t9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE)+ sizeof (RuntimeObject), sizeof(BigNumberBuffer_t9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4625[4] = 
{
	BigNumberBuffer_t9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE::get_offset_of_digits_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BigNumberBuffer_t9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE::get_offset_of_precision_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BigNumberBuffer_t9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE::get_offset_of_scale_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BigNumberBuffer_t9F40EEF6DD8AB26815DE5A7B877478BA0EB7E1BE::get_offset_of_sign_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4626 = { sizeof (DoubleUlong_t7F954356D3B9DACCD9CCC6C1879F2009F65777C7)+ sizeof (RuntimeObject), sizeof(DoubleUlong_t7F954356D3B9DACCD9CCC6C1879F2009F65777C7 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4626[2] = 
{
	DoubleUlong_t7F954356D3B9DACCD9CCC6C1879F2009F65777C7::get_offset_of_dbl_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoubleUlong_t7F954356D3B9DACCD9CCC6C1879F2009F65777C7::get_offset_of_uu_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4627 = { sizeof (NumericsHelpers_tD5B6EE08BA6FBEA20F1E464B39AD8C21478CD91A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4628 = { sizeof (FormatProvider_t8B5DFBEF60CF2684D61AE0D41D3291717512CF4C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4629 = { sizeof (Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2), -1, sizeof(Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4629[6] = 
{
	Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2_StaticFields::get_offset_of_s_posCurrencyFormats_0(),
	Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2_StaticFields::get_offset_of_s_negCurrencyFormats_1(),
	Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2_StaticFields::get_offset_of_s_posPercentFormats_2(),
	Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2_StaticFields::get_offset_of_s_negPercentFormats_3(),
	Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2_StaticFields::get_offset_of_s_negNumberFormats_4(),
	Number_tFD98EE73F24490FF998A76D9569C96182BDB0FC2_StaticFields::get_offset_of_s_posNumberFormat_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4630 = { sizeof (NumberBuffer_t523F9CCA824CB72F54893DDB17C47553B942A2F8)+ sizeof (RuntimeObject), sizeof(NumberBuffer_t523F9CCA824CB72F54893DDB17C47553B942A2F8_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4630[4] = 
{
	NumberBuffer_t523F9CCA824CB72F54893DDB17C47553B942A2F8::get_offset_of_precision_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumberBuffer_t523F9CCA824CB72F54893DDB17C47553B942A2F8::get_offset_of_scale_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumberBuffer_t523F9CCA824CB72F54893DDB17C47553B942A2F8::get_offset_of_sign_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumberBuffer_t523F9CCA824CB72F54893DDB17C47553B942A2F8::get_offset_of_overrideDigits_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4631 = { sizeof (U3CModuleU3E_tBE4A2C03D239132B99D8F2C5938A2EC7220D741C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4632 = { sizeof (DataContractAttribute_tAD58D5877BD04EADB56BB4AEDDE342C73F032FC5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4632[1] = 
{
	DataContractAttribute_tAD58D5877BD04EADB56BB4AEDDE342C73F032FC5::get_offset_of_isReference_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4633 = { sizeof (DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4633[4] = 
{
	DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525::get_offset_of_name_0(),
	DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525::get_offset_of_order_1(),
	DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525::get_offset_of_isRequired_2(),
	DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525::get_offset_of_emitDefaultValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4634 = { sizeof (EnumMemberAttribute_t115D80337B2C8222158FC46345EA100EEB63B32D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4634[1] = 
{
	EnumMemberAttribute_t115D80337B2C8222158FC46345EA100EEB63B32D::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4635 = { sizeof (KnownTypeAttribute_tFCFB5B9A0AE4BBCBD655E2EC756FABAC3ADF487E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4635[1] = 
{
	KnownTypeAttribute_tFCFB5B9A0AE4BBCBD655E2EC756FABAC3ADF487E::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4636 = { sizeof (U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4637 = { sizeof (U3CModuleU3E_tF3A658440E2EA6B9E28737BEB10616BB13D75E94), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4638 = { sizeof (MobileNativePopups_t4171E4F9ED145F876E9953246D168B2CBA6F00D0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4639 = { sizeof (U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4639[1] = 
{
	U3COpenAlertDialogU3Ec__AnonStorey0_t7AA3F28020ACCEAB250731370FC4D05F470B4B18::get_offset_of_onCancel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4640 = { sizeof (U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4640[2] = 
{
	U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72::get_offset_of_onCancel_0(),
	U3COpenAlertDialogU3Ec__AnonStorey1_t464A898B52C251237E9C1DA5250B421534114C72::get_offset_of_onOk_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4641 = { sizeof (IosNativePopups_t6C8DB9578139592203A95BAB1632A0891D2AAA4B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4642 = { sizeof (CompletionHandler_t57827C46E0121AC0285B25FF50B80A23FFE2A7CE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4643 = { sizeof (InternalHandler_t99221D275AB4B7194ED4F74F9193CA944BD83FD5), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4644 = { sizeof (MonoPInvokeCallbackAttribute_tF2C68FFE572BEBC9BD9FFBA4B794164E3A87CBED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4645 = { sizeof (AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368), -1, sizeof(AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4645[1] = 
{
	AndroidNativePopups_tFCDF1E0DDBEC0C449E8C74993CE36894D3862368_StaticFields::get_offset_of_androidUtils_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4646 = { sizeof (AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4646[3] = 
{
	AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241::get_offset_of_onOk_4(),
	AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241::get_offset_of_onCancel_5(),
	AlertDialogProxy_t55B5EC27E25A527CCB4EE248A488E8B702897241::get_offset_of_onNeutral_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4647 = { sizeof (DatePickerDialogProxy_t3DC13C8B3EFB9AB6890BF88F75AD27564AEB0F0E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4647[1] = 
{
	DatePickerDialogProxy_t3DC13C8B3EFB9AB6890BF88F75AD27564AEB0F0E::get_offset_of_onDateSet_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4648 = { sizeof (TimePickerDialogProxy_tEA3E774F2ACE744CCC2DBBF4A78BD2049B76E266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4648[1] = 
{
	TimePickerDialogProxy_tEA3E774F2ACE744CCC2DBBF4A78BD2049B76E266::get_offset_of_onTimeSet_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4649 = { sizeof (U3CModuleU3E_t2DE490318B8122B2500276938943D527361370EF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4650 = { sizeof (SR_t0C89764BAB3C8C6F8EA05107995810E932907097), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4651 = { sizeof (AcceptRejectRule_t391EEC16C998A618BB6208797DE62FFD8538D5D5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4651[3] = 
{
	AcceptRejectRule_t391EEC16C998A618BB6208797DE62FFD8538D5D5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4652 = { sizeof (AggregateType_t5D6ACD9ED88CE8523487CEA56C474358136EF1FE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4652[10] = 
{
	AggregateType_t5D6ACD9ED88CE8523487CEA56C474358136EF1FE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4653 = { sizeof (InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334), -1, sizeof(InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4653[1] = 
{
	InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334_StaticFields::get_offset_of_s_refreshEventArgs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4654 = { sizeof (ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C), -1, sizeof(ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4654[2] = 
{
	ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C_StaticFields::get_offset_of_s_types_2(),
	ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4655 = { sizeof (DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82), -1, sizeof(DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4655[2] = 
{
	DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_StaticFields::get_offset_of_Log_1(),
	DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_StaticFields::get_offset_of_s_nextScopeId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4656 = { sizeof (Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4656[5] = 
{
	Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2::get_offset_of__schemaName_0(),
	Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2::get_offset_of__inCollection_1(),
	Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2::get_offset_of__dataSet_2(),
	Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2::get_offset_of__name_3(),
	Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2::get_offset_of__extendedProperties_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4657 = { sizeof (ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4657[6] = 
{
	ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62::get_offset_of__table_1(),
	ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62::get_offset_of__list_2(),
	ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62::get_offset_of__defaultNameIndex_3(),
	ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62::get_offset_of__onCollectionChanged_4(),
	ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62::get_offset_of__delayLoadingConstraints_5(),
	ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62::get_offset_of__fLoadForeignKeyConstraintsOnly_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4658 = { sizeof (ConstraintConverter_t7AFB06FC75603F71A099A48A36ACC0EFCF178D93), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4659 = { sizeof (ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4659[3] = 
{
	ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36::get_offset_of__tables_0(),
	ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36::get_offset_of__constraints_1(),
	ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36::get_offset_of__currentObject_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4660 = { sizeof (ForeignKeyConstraintEnumerator_t555045F1D5AC5CF9D756F3A5955790BB8C7C3C6F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4661 = { sizeof (ChildForeignKeyConstraintEnumerator_tF57E30068558F2981330827F8B31D700243A827B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4661[1] = 
{
	ChildForeignKeyConstraintEnumerator_tF57E30068558F2981330827F8B31D700243A827B::get_offset_of__table_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4662 = { sizeof (ParentForeignKeyConstraintEnumerator_t442E012770A77A20F68D240C7E9271BB1B1FB9FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4662[1] = 
{
	ParentForeignKeyConstraintEnumerator_t442E012770A77A20F68D240C7E9271BB1B1FB9FA::get_offset_of__table_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4663 = { sizeof (DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D), -1, sizeof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4663[35] = 
{
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__allowNull_3(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__caption_4(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__columnName_5(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__dataType_6(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__storageType_7(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__defaultValue_8(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__dateTimeMode_9(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__expression_10(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__maxLength_11(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__ordinal_12(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__readOnly_13(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__sortIndex_14(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__table_15(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__unique_16(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__columnMapping_17(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__hashCode_18(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__errors_19(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__isSqlType_20(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__implementsINullable_21(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__implementsIChangeTracking_22(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__implementsIRevertibleChangeTracking_23(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__implementsIXMLSerializable_24(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__defaultValueIsNull_25(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__dependentColumns_26(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__extendedProperties_27(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__storage_28(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__autoInc_29(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__columnUri_30(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__columnPrefix_31(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__encodedColumnName_32(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__simpleType_33(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D_StaticFields::get_offset_of_s_objectTypeCount_34(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__objectID_35(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of_U3CXmlDataTypeU3Ek__BackingField_36(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of_PropertyChanging_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4664 = { sizeof (AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4664[1] = 
{
	AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086::get_offset_of_U3CAutoU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4665 = { sizeof (AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4665[3] = 
{
	AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486::get_offset_of__current_1(),
	AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486::get_offset_of__seed_2(),
	AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486::get_offset_of__step_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4666 = { sizeof (AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4666[3] = 
{
	AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797::get_offset_of__current_1(),
	AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797::get_offset_of__seed_2(),
	AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797::get_offset_of__step_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4667 = { sizeof (DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4667[3] = 
{
	DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280::get_offset_of__column_1(),
	DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280::get_offset_of_U3CRowU3Ek__BackingField_2(),
	DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280::get_offset_of_U3CProposedValueU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4668 = { sizeof (DataColumnChangeEventHandler_tC860540A9091FE1BB1DF718AB44874A54585FE07), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4669 = { sizeof (DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4669[12] = 
{
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__table_1(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__list_2(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__defaultNameIndex_3(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__delayedAddRangeColumns_4(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__columnFromName_5(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__fInClear_6(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__columnsImplementingIChangeTracking_7(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__nColumnsImplementingIChangeTracking_8(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__nColumnsImplementingIRevertibleChangeTracking_9(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of_CollectionChanged_10(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of_CollectionChanging_11(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of_ColumnPropertyChanged_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4670 = { sizeof (DataColumnPropertyDescriptor_tDFBE9FD48209F7B6754670423AA58122A0BBF356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4670[1] = 
{
	DataColumnPropertyDescriptor_tDFBE9FD48209F7B6754670423AA58122A0BBF356::get_offset_of_U3CColumnU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4671 = { sizeof (DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4671[3] = 
{
	DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786::get_offset_of__rowError_0(),
	DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786::get_offset_of__count_1(),
	DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786::get_offset_of__errorList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4672 = { sizeof (ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4672[2] = 
{
	ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3::get_offset_of__column_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3::get_offset_of__error_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4673 = { sizeof (DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4674 = { sizeof (ConstraintException_t38F5592AB8DF6A31990C7D0D787C5E585A25E662), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4675 = { sizeof (DeletedRowInaccessibleException_tB043F9422B3AB955DCC1D5465B61C1BEE78A588A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4676 = { sizeof (DuplicateNameException_t5D57DF3B9D0B944ECD83B9F2672400E377F80448), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4677 = { sizeof (InRowChangingEventException_t4CEE672B6EC85565D9A4EF5A5103BF2DCCA4CB9E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4678 = { sizeof (InvalidConstraintException_tD2E64652BEA365247A6012CC0DA5C4B3F201E7DF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4679 = { sizeof (NoNullAllowedException_t2FF3B7ADEEE81C689279686D6BFF76F195B06D1D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4680 = { sizeof (ReadOnlyException_t6FEDA3BE846CBEF2C4AE11165DC094BFCFF2F7D6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4681 = { sizeof (RowNotInTableException_t85FF30B2DC1D594CADD7AA0803E4A32CDFE2000F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4682 = { sizeof (VersionNotFoundException_t7A0CF1C23F03BD674086DBB8170914DDBA23C181), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4683 = { sizeof (ExceptionBuilder_tE6546B379EB375CF76757E7B0315505DD7929C0C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4684 = { sizeof (DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4684[1] = 
{
	DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B::get_offset_of__columns_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4685 = { sizeof (DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3), -1, sizeof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4685[19] = 
{
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__dataSet_0(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__extendedProperties_1(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__relationName_2(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__childKey_3(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__parentKey_4(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__parentKeyConstraint_5(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__childKeyConstraint_6(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__parentColumnNames_7(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__childColumnNames_8(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__parentTableName_9(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__childTableName_10(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__parentTableNamespace_11(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__childTableNamespace_12(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__nested_13(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__createConstraints_14(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__checkMultipleNested_15(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3_StaticFields::get_offset_of_s_objectTypeCount_16(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__objectID_17(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of_PropertyChanging_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4686 = { sizeof (DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B), -1, sizeof(DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4686[6] = 
{
	DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B::get_offset_of__inTransition_1(),
	DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B::get_offset_of__defaultNameIndex_2(),
	DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B::get_offset_of__onCollectionChangedDelegate_3(),
	DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B::get_offset_of__onCollectionChangingDelegate_4(),
	DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B_StaticFields::get_offset_of_s_objectTypeCount_5(),
	DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B::get_offset_of__objectID_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4687 = { sizeof (DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4687[4] = 
{
	DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED::get_offset_of__table_7(),
	DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED::get_offset_of__relations_8(),
	DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED::get_offset_of__fParentCollection_9(),
	DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED::get_offset_of_RelationPropertyChanged_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4688 = { sizeof (DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4688[3] = 
{
	DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D::get_offset_of__dataSet_7(),
	DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D::get_offset_of__relations_8(),
	DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D::get_offset_of__delayLoadingRelations_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4689 = { sizeof (DataRelationPropertyDescriptor_t0937B241D5C01EB1F18BBF9894A5A9E387E2A856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4689[1] = 
{
	DataRelationPropertyDescriptor_t0937B241D5C01EB1F18BBF9894A5A9E387E2A856::get_offset_of_U3CRelationU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4690 = { sizeof (DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B), -1, sizeof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4690[16] = 
{
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__table_0(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__columns_1(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__oldRecord_2(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__newRecord_3(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__tempRecord_4(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__rowID_5(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__action_6(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__inChangingEvent_7(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__inDeletingEvent_8(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__inCascade_9(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__lastChangedColumn_10(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__countColumnChange_11(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__error_12(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__rbTreeNodeId_13(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B_StaticFields::get_offset_of_s_objectTypeCount_14(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__objectID_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4691 = { sizeof (DataRowBuilder_t1686A02FA53DF491D826A981024C255668E94CC6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4691[2] = 
{
	DataRowBuilder_t1686A02FA53DF491D826A981024C255668E94CC6::get_offset_of__table_0(),
	DataRowBuilder_t1686A02FA53DF491D826A981024C255668E94CC6::get_offset_of__record_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4692 = { sizeof (DataRowAction_tDA5E813CDEE8ABF5D37A4A055D75B66DDBEB068F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4692[9] = 
{
	DataRowAction_tDA5E813CDEE8ABF5D37A4A055D75B66DDBEB068F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4693 = { sizeof (DataRowChangeEventArgs_tA5961502488B4B5C32F788CB86746087E043A444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4693[2] = 
{
	DataRowChangeEventArgs_tA5961502488B4B5C32F788CB86746087E043A444::get_offset_of_U3CRowU3Ek__BackingField_1(),
	DataRowChangeEventArgs_tA5961502488B4B5C32F788CB86746087E043A444::get_offset_of_U3CActionU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4694 = { sizeof (DataRowChangeEventHandler_t528DC5369A320B2397E1E5CF4E27CC518C0C72A0), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4695 = { sizeof (DataRowCollection_t45B9FDFC3667C7FA9C0F7F14787B5ED70DC673E0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4695[3] = 
{
	DataRowCollection_t45B9FDFC3667C7FA9C0F7F14787B5ED70DC673E0::get_offset_of__table_1(),
	DataRowCollection_t45B9FDFC3667C7FA9C0F7F14787B5ED70DC673E0::get_offset_of__list_2(),
	DataRowCollection_t45B9FDFC3667C7FA9C0F7F14787B5ED70DC673E0::get_offset_of__nullInList_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4696 = { sizeof (DataRowTree_t885C3CBC17060B726BFEE177710D6E9E57FEA230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4697 = { sizeof (DataRowCreatedEventHandler_t03CF53F5DF69A9FC021AC0092080CCDC74916F82), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4698 = { sizeof (DataSetClearEventhandler_t301F54C2A702D53B71D57E255D2B6B28D7420833), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4699 = { sizeof (DataRowState_tE4CAA76865C430B04232443A665CEE5C1EF6DEBE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4699[6] = 
{
	DataRowState_tE4CAA76865C430B04232443A665CEE5C1EF6DEBE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
