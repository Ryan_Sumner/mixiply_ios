﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// MS.Internal.Xml.Cache.XPathNodeInfoAtom
struct XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7;
// MS.Internal.Xml.Cache.XPathNodePageInfo
struct XPathNodePageInfo_t116B772208F6DB8CCD46DE1F15A18344F0AFC05B;
// MS.Internal.Xml.Cache.XPathNode[]
struct XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B;
// MS.Internal.Xml.XPath.AstNode
struct AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C;
// MS.Internal.Xml.XPath.Operator/Op[]
struct OpU5BU5D_t6F527BF40D983BF4877B310798EFD23BD46F4BFA;
// MS.Internal.Xml.XPath.XPathScanner
struct XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char
struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Xml.XmlSqlBinaryReader/NamespaceDecl>
struct Dictionary_2_tBEF366884DEC0B0D63DF018EECE2F0E08A8F910E;
// System.Collections.Generic.Dictionary`2<System.Xml.IDtdEntityInfo,System.Xml.IDtdEntityInfo>
struct Dictionary_2_tD00930653EC02E03C14ADE0FF8737B6193A5FE1B;
// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Int32>
struct Dictionary_2_tD3EC27D7F99969DB9B3CBEC094139F683A730F43;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<System.Xml.XmlEventCache/XmlEvent[]>
struct List_1_t08F12CB35B6D2C6405C7227FBB70FC5939090022;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.TextReader
struct TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A;
// System.IO.TextWriter
struct TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.Decoder
struct Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26;
// System.Text.Encoder
struct Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464;
// System.Text.EncoderFallback
struct EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63;
// System.Text.EncoderNLS
struct EncoderNLS_t47EFB0F5A59F41C429930063F966A68F8D394DF8;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.Tasks.Task
struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2;
// System.Threading.Tasks.Task`1<System.Boolean>
struct Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87;
// System.Threading.Tasks.Task`1<System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>>
struct Task_1_tFB42A7666202CD6CD81E6BF0FF63815EE40E621D;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.Xml.AttributePSVIInfo
struct AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94;
// System.Xml.BitStack
struct BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7;
// System.Xml.ByteStack
struct ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75;
// System.Xml.CharEntityEncoderFallback
struct CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE;
// System.Xml.CharEntityEncoderFallbackBuffer
struct CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8;
// System.Xml.IDtdEntityInfo
struct IDtdEntityInfo_t4477A2221D64D9E3DB7F89E82E963BB4858A38D2;
// System.Xml.IDtdInfo
struct IDtdInfo_t5971A8C09914EDB816FE7A86A38288FDC4B6F80C;
// System.Xml.IValidationEventHandling
struct IValidationEventHandling_tABD39B6B973C0A0DC259D55D8C4179A43ACAB41B;
// System.Xml.IXmlLineInfo
struct IXmlLineInfo_tD6D8818DFB22D29FC2397C76BA6BFFF54604284A;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t252EBD93E225063727450B6A8B4BE94F5F2E8427;
// System.Xml.IncrementalReadDecoder
struct IncrementalReadDecoder_t787BFB5889B01B88DDA030C503A0C2E0525CA723;
// System.Xml.OnRemoveWriter
struct OnRemoveWriter_t40CE623324C30930692639D6172B422C25AA6370;
// System.Xml.ReadState[]
struct ReadStateU5BU5D_t807C186401CFAF2EE005FBEF4EC70DC421B9896D;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t2FE768B806BA73C644AEE436491F2C3E04039CF1;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F;
// System.Xml.SecureStringHasher
struct SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3;
// System.Xml.SecureStringHasher/HashCodeOfStringDelegate
struct HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E;
// System.Xml.TernaryTreeReadOnly
struct TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2;
// System.Xml.XPath.XPathDocument
struct XPathDocument_tD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321;
// System.Xml.XPath.XPathNavigatorKeyComparer
struct XPathNavigatorKeyComparer_t6A0E82BEC0BE42351DDB26EAA86333C11E0A9378;
// System.Xml.XPath.XPathResultType[]
struct XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF;
// System.Xml.XmlEventCache
struct XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4;
// System.Xml.XmlEventCache/XmlEvent[]
struct XmlEventU5BU5D_tAE240C67B5AAEA739D76EA9A649BEF1153584236;
// System.Xml.XmlNameTable
struct XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F;
// System.Xml.XmlParserContext
struct XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD;
// System.Xml.XmlRawWriter
struct XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2;
// System.Xml.XmlRawWriterBase64Encoder
struct XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678;
// System.Xml.XmlReader
struct XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB;
// System.Xml.XmlResolver
struct XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280;
// System.Xml.XmlSqlBinaryReader/AttrInfo[]
struct AttrInfoU5BU5D_tB6030FFAA6B95DE14B235E8A0533248A19EF12AF;
// System.Xml.XmlSqlBinaryReader/ElemInfo[]
struct ElemInfoU5BU5D_t13B605E8ECDE26BBA310B5E8597770E0449F67BD;
// System.Xml.XmlSqlBinaryReader/NamespaceDecl
struct NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B;
// System.Xml.XmlSqlBinaryReader/NestedBinXml
struct NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175;
// System.Xml.XmlSqlBinaryReader/QName[]
struct QNameU5BU5D_t4006F29C3E0B63E7B8B049A5F88FFDE77D9B2AB5;
// System.Xml.XmlTextEncoder
struct XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475;
// System.Xml.XmlTextReaderImpl
struct XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61;
// System.Xml.XmlTextReaderImpl/LaterInitParam
struct LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF;
// System.Xml.XmlTextReaderImpl/NodeData
struct NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF;
// System.Xml.XmlTextReaderImpl/NodeData[]
struct NodeDataU5BU5D_tD5EBFFECEF67B4D0D7D521A99182D3B2814E07BE;
// System.Xml.XmlTextReaderImpl/OnDefaultAttributeUseDelegate
struct OnDefaultAttributeUseDelegate_tE83A97BD37E08D3C5AA377CEA6388782D99EA34D;
// System.Xml.XmlTextReaderImpl/ParsingState[]
struct ParsingStateU5BU5D_t7A5096046F9BF1BA70E9696B2A2479272FC65739;
// System.Xml.XmlTextReaderImpl/XmlContext
struct XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52;
// System.Xml.XmlWriter
struct XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869;
// System.Xml.XmlWriterSettings
struct XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3;

struct QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F_marshaled_com;
struct QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F_marshaled_pinvoke;
struct XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0_marshaled_com;
struct XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef XPATHNODEHELPER_TDCBBEFE1E40E333E12319E06BD2ACC2685C1EB6A_H
#define XPATHNODEHELPER_TDCBBEFE1E40E333E12319E06BD2ACC2685C1EB6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodeHelper
struct  XPathNodeHelper_tDCBBEFE1E40E333E12319E06BD2ACC2685C1EB6A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODEHELPER_TDCBBEFE1E40E333E12319E06BD2ACC2685C1EB6A_H
#ifndef XPATHNODEINFOATOM_T6FF2C2B2096901C0BB3988616FBA285A67947AC7_H
#define XPATHNODEINFOATOM_T6FF2C2B2096901C0BB3988616FBA285A67947AC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodeInfoAtom
struct  XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7  : public RuntimeObject
{
public:
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::localName
	String_t* ___localName_0;
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::namespaceUri
	String_t* ___namespaceUri_1;
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::prefix
	String_t* ___prefix_2;
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::baseUri
	String_t* ___baseUri_3;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageParent
	XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* ___pageParent_4;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageSibling
	XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* ___pageSibling_5;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageSimilar
	XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* ___pageSimilar_6;
	// System.Xml.XPath.XPathDocument MS.Internal.Xml.Cache.XPathNodeInfoAtom::doc
	XPathDocument_tD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321 * ___doc_7;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::lineNumBase
	int32_t ___lineNumBase_8;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::linePosBase
	int32_t ___linePosBase_9;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::hashCode
	int32_t ___hashCode_10;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::localNameHash
	int32_t ___localNameHash_11;
	// MS.Internal.Xml.Cache.XPathNodePageInfo MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageInfo
	XPathNodePageInfo_t116B772208F6DB8CCD46DE1F15A18344F0AFC05B * ___pageInfo_12;

public:
	inline static int32_t get_offset_of_localName_0() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7, ___localName_0)); }
	inline String_t* get_localName_0() const { return ___localName_0; }
	inline String_t** get_address_of_localName_0() { return &___localName_0; }
	inline void set_localName_0(String_t* value)
	{
		___localName_0 = value;
		Il2CppCodeGenWriteBarrier((&___localName_0), value);
	}

	inline static int32_t get_offset_of_namespaceUri_1() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7, ___namespaceUri_1)); }
	inline String_t* get_namespaceUri_1() const { return ___namespaceUri_1; }
	inline String_t** get_address_of_namespaceUri_1() { return &___namespaceUri_1; }
	inline void set_namespaceUri_1(String_t* value)
	{
		___namespaceUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_1), value);
	}

	inline static int32_t get_offset_of_prefix_2() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7, ___prefix_2)); }
	inline String_t* get_prefix_2() const { return ___prefix_2; }
	inline String_t** get_address_of_prefix_2() { return &___prefix_2; }
	inline void set_prefix_2(String_t* value)
	{
		___prefix_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_2), value);
	}

	inline static int32_t get_offset_of_baseUri_3() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7, ___baseUri_3)); }
	inline String_t* get_baseUri_3() const { return ___baseUri_3; }
	inline String_t** get_address_of_baseUri_3() { return &___baseUri_3; }
	inline void set_baseUri_3(String_t* value)
	{
		___baseUri_3 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_3), value);
	}

	inline static int32_t get_offset_of_pageParent_4() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7, ___pageParent_4)); }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* get_pageParent_4() const { return ___pageParent_4; }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B** get_address_of_pageParent_4() { return &___pageParent_4; }
	inline void set_pageParent_4(XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* value)
	{
		___pageParent_4 = value;
		Il2CppCodeGenWriteBarrier((&___pageParent_4), value);
	}

	inline static int32_t get_offset_of_pageSibling_5() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7, ___pageSibling_5)); }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* get_pageSibling_5() const { return ___pageSibling_5; }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B** get_address_of_pageSibling_5() { return &___pageSibling_5; }
	inline void set_pageSibling_5(XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* value)
	{
		___pageSibling_5 = value;
		Il2CppCodeGenWriteBarrier((&___pageSibling_5), value);
	}

	inline static int32_t get_offset_of_pageSimilar_6() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7, ___pageSimilar_6)); }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* get_pageSimilar_6() const { return ___pageSimilar_6; }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B** get_address_of_pageSimilar_6() { return &___pageSimilar_6; }
	inline void set_pageSimilar_6(XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* value)
	{
		___pageSimilar_6 = value;
		Il2CppCodeGenWriteBarrier((&___pageSimilar_6), value);
	}

	inline static int32_t get_offset_of_doc_7() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7, ___doc_7)); }
	inline XPathDocument_tD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321 * get_doc_7() const { return ___doc_7; }
	inline XPathDocument_tD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321 ** get_address_of_doc_7() { return &___doc_7; }
	inline void set_doc_7(XPathDocument_tD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321 * value)
	{
		___doc_7 = value;
		Il2CppCodeGenWriteBarrier((&___doc_7), value);
	}

	inline static int32_t get_offset_of_lineNumBase_8() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7, ___lineNumBase_8)); }
	inline int32_t get_lineNumBase_8() const { return ___lineNumBase_8; }
	inline int32_t* get_address_of_lineNumBase_8() { return &___lineNumBase_8; }
	inline void set_lineNumBase_8(int32_t value)
	{
		___lineNumBase_8 = value;
	}

	inline static int32_t get_offset_of_linePosBase_9() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7, ___linePosBase_9)); }
	inline int32_t get_linePosBase_9() const { return ___linePosBase_9; }
	inline int32_t* get_address_of_linePosBase_9() { return &___linePosBase_9; }
	inline void set_linePosBase_9(int32_t value)
	{
		___linePosBase_9 = value;
	}

	inline static int32_t get_offset_of_hashCode_10() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7, ___hashCode_10)); }
	inline int32_t get_hashCode_10() const { return ___hashCode_10; }
	inline int32_t* get_address_of_hashCode_10() { return &___hashCode_10; }
	inline void set_hashCode_10(int32_t value)
	{
		___hashCode_10 = value;
	}

	inline static int32_t get_offset_of_localNameHash_11() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7, ___localNameHash_11)); }
	inline int32_t get_localNameHash_11() const { return ___localNameHash_11; }
	inline int32_t* get_address_of_localNameHash_11() { return &___localNameHash_11; }
	inline void set_localNameHash_11(int32_t value)
	{
		___localNameHash_11 = value;
	}

	inline static int32_t get_offset_of_pageInfo_12() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7, ___pageInfo_12)); }
	inline XPathNodePageInfo_t116B772208F6DB8CCD46DE1F15A18344F0AFC05B * get_pageInfo_12() const { return ___pageInfo_12; }
	inline XPathNodePageInfo_t116B772208F6DB8CCD46DE1F15A18344F0AFC05B ** get_address_of_pageInfo_12() { return &___pageInfo_12; }
	inline void set_pageInfo_12(XPathNodePageInfo_t116B772208F6DB8CCD46DE1F15A18344F0AFC05B * value)
	{
		___pageInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___pageInfo_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODEINFOATOM_T6FF2C2B2096901C0BB3988616FBA285A67947AC7_H
#ifndef XPATHNODEPAGEINFO_T116B772208F6DB8CCD46DE1F15A18344F0AFC05B_H
#define XPATHNODEPAGEINFO_T116B772208F6DB8CCD46DE1F15A18344F0AFC05B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodePageInfo
struct  XPathNodePageInfo_t116B772208F6DB8CCD46DE1F15A18344F0AFC05B  : public RuntimeObject
{
public:
	// System.Int32 MS.Internal.Xml.Cache.XPathNodePageInfo::pageNum
	int32_t ___pageNum_0;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodePageInfo::nodeCount
	int32_t ___nodeCount_1;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodePageInfo::pageNext
	XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* ___pageNext_2;

public:
	inline static int32_t get_offset_of_pageNum_0() { return static_cast<int32_t>(offsetof(XPathNodePageInfo_t116B772208F6DB8CCD46DE1F15A18344F0AFC05B, ___pageNum_0)); }
	inline int32_t get_pageNum_0() const { return ___pageNum_0; }
	inline int32_t* get_address_of_pageNum_0() { return &___pageNum_0; }
	inline void set_pageNum_0(int32_t value)
	{
		___pageNum_0 = value;
	}

	inline static int32_t get_offset_of_nodeCount_1() { return static_cast<int32_t>(offsetof(XPathNodePageInfo_t116B772208F6DB8CCD46DE1F15A18344F0AFC05B, ___nodeCount_1)); }
	inline int32_t get_nodeCount_1() const { return ___nodeCount_1; }
	inline int32_t* get_address_of_nodeCount_1() { return &___nodeCount_1; }
	inline void set_nodeCount_1(int32_t value)
	{
		___nodeCount_1 = value;
	}

	inline static int32_t get_offset_of_pageNext_2() { return static_cast<int32_t>(offsetof(XPathNodePageInfo_t116B772208F6DB8CCD46DE1F15A18344F0AFC05B, ___pageNext_2)); }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* get_pageNext_2() const { return ___pageNext_2; }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B** get_address_of_pageNext_2() { return &___pageNext_2; }
	inline void set_pageNext_2(XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* value)
	{
		___pageNext_2 = value;
		Il2CppCodeGenWriteBarrier((&___pageNext_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODEPAGEINFO_T116B772208F6DB8CCD46DE1F15A18344F0AFC05B_H
#ifndef ASTNODE_TEB8196548F948D593E8705AE90AECD4B02D2D93C_H
#define ASTNODE_TEB8196548F948D593E8705AE90AECD4B02D2D93C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.AstNode
struct  AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTNODE_TEB8196548F948D593E8705AE90AECD4B02D2D93C_H
#ifndef XPATHPARSER_T4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_H
#define XPATHPARSER_T4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.XPathParser
struct  XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE  : public RuntimeObject
{
public:
	// MS.Internal.Xml.XPath.XPathScanner MS.Internal.Xml.XPath.XPathParser::scanner
	XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481 * ___scanner_0;
	// System.Int32 MS.Internal.Xml.XPath.XPathParser::parseDepth
	int32_t ___parseDepth_1;

public:
	inline static int32_t get_offset_of_scanner_0() { return static_cast<int32_t>(offsetof(XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE, ___scanner_0)); }
	inline XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481 * get_scanner_0() const { return ___scanner_0; }
	inline XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481 ** get_address_of_scanner_0() { return &___scanner_0; }
	inline void set_scanner_0(XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481 * value)
	{
		___scanner_0 = value;
		Il2CppCodeGenWriteBarrier((&___scanner_0), value);
	}

	inline static int32_t get_offset_of_parseDepth_1() { return static_cast<int32_t>(offsetof(XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE, ___parseDepth_1)); }
	inline int32_t get_parseDepth_1() const { return ___parseDepth_1; }
	inline int32_t* get_address_of_parseDepth_1() { return &___parseDepth_1; }
	inline void set_parseDepth_1(int32_t value)
	{
		___parseDepth_1 = value;
	}
};

struct XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields
{
public:
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray1
	XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* ___temparray1_2;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray2
	XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* ___temparray2_3;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray3
	XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* ___temparray3_4;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray4
	XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* ___temparray4_5;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray5
	XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* ___temparray5_6;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray6
	XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* ___temparray6_7;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray7
	XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* ___temparray7_8;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray8
	XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* ___temparray8_9;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray9
	XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* ___temparray9_10;
	// System.Collections.Hashtable MS.Internal.Xml.XPath.XPathParser::functionTable
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___functionTable_11;
	// System.Collections.Hashtable MS.Internal.Xml.XPath.XPathParser::AxesTable
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___AxesTable_12;

public:
	inline static int32_t get_offset_of_temparray1_2() { return static_cast<int32_t>(offsetof(XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields, ___temparray1_2)); }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* get_temparray1_2() const { return ___temparray1_2; }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF** get_address_of_temparray1_2() { return &___temparray1_2; }
	inline void set_temparray1_2(XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* value)
	{
		___temparray1_2 = value;
		Il2CppCodeGenWriteBarrier((&___temparray1_2), value);
	}

	inline static int32_t get_offset_of_temparray2_3() { return static_cast<int32_t>(offsetof(XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields, ___temparray2_3)); }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* get_temparray2_3() const { return ___temparray2_3; }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF** get_address_of_temparray2_3() { return &___temparray2_3; }
	inline void set_temparray2_3(XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* value)
	{
		___temparray2_3 = value;
		Il2CppCodeGenWriteBarrier((&___temparray2_3), value);
	}

	inline static int32_t get_offset_of_temparray3_4() { return static_cast<int32_t>(offsetof(XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields, ___temparray3_4)); }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* get_temparray3_4() const { return ___temparray3_4; }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF** get_address_of_temparray3_4() { return &___temparray3_4; }
	inline void set_temparray3_4(XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* value)
	{
		___temparray3_4 = value;
		Il2CppCodeGenWriteBarrier((&___temparray3_4), value);
	}

	inline static int32_t get_offset_of_temparray4_5() { return static_cast<int32_t>(offsetof(XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields, ___temparray4_5)); }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* get_temparray4_5() const { return ___temparray4_5; }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF** get_address_of_temparray4_5() { return &___temparray4_5; }
	inline void set_temparray4_5(XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* value)
	{
		___temparray4_5 = value;
		Il2CppCodeGenWriteBarrier((&___temparray4_5), value);
	}

	inline static int32_t get_offset_of_temparray5_6() { return static_cast<int32_t>(offsetof(XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields, ___temparray5_6)); }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* get_temparray5_6() const { return ___temparray5_6; }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF** get_address_of_temparray5_6() { return &___temparray5_6; }
	inline void set_temparray5_6(XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* value)
	{
		___temparray5_6 = value;
		Il2CppCodeGenWriteBarrier((&___temparray5_6), value);
	}

	inline static int32_t get_offset_of_temparray6_7() { return static_cast<int32_t>(offsetof(XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields, ___temparray6_7)); }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* get_temparray6_7() const { return ___temparray6_7; }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF** get_address_of_temparray6_7() { return &___temparray6_7; }
	inline void set_temparray6_7(XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* value)
	{
		___temparray6_7 = value;
		Il2CppCodeGenWriteBarrier((&___temparray6_7), value);
	}

	inline static int32_t get_offset_of_temparray7_8() { return static_cast<int32_t>(offsetof(XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields, ___temparray7_8)); }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* get_temparray7_8() const { return ___temparray7_8; }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF** get_address_of_temparray7_8() { return &___temparray7_8; }
	inline void set_temparray7_8(XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* value)
	{
		___temparray7_8 = value;
		Il2CppCodeGenWriteBarrier((&___temparray7_8), value);
	}

	inline static int32_t get_offset_of_temparray8_9() { return static_cast<int32_t>(offsetof(XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields, ___temparray8_9)); }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* get_temparray8_9() const { return ___temparray8_9; }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF** get_address_of_temparray8_9() { return &___temparray8_9; }
	inline void set_temparray8_9(XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* value)
	{
		___temparray8_9 = value;
		Il2CppCodeGenWriteBarrier((&___temparray8_9), value);
	}

	inline static int32_t get_offset_of_temparray9_10() { return static_cast<int32_t>(offsetof(XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields, ___temparray9_10)); }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* get_temparray9_10() const { return ___temparray9_10; }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF** get_address_of_temparray9_10() { return &___temparray9_10; }
	inline void set_temparray9_10(XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* value)
	{
		___temparray9_10 = value;
		Il2CppCodeGenWriteBarrier((&___temparray9_10), value);
	}

	inline static int32_t get_offset_of_functionTable_11() { return static_cast<int32_t>(offsetof(XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields, ___functionTable_11)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_functionTable_11() const { return ___functionTable_11; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_functionTable_11() { return &___functionTable_11; }
	inline void set_functionTable_11(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___functionTable_11 = value;
		Il2CppCodeGenWriteBarrier((&___functionTable_11), value);
	}

	inline static int32_t get_offset_of_AxesTable_12() { return static_cast<int32_t>(offsetof(XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields, ___AxesTable_12)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_AxesTable_12() const { return ___AxesTable_12; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_AxesTable_12() { return &___AxesTable_12; }
	inline void set_AxesTable_12(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___AxesTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___AxesTable_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHPARSER_T4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef LOCALAPPCONTEXTSWITCHES_TC0069108E3FEC5EEA8E7E80D817C1A80F9009674_H
#define LOCALAPPCONTEXTSWITCHES_TC0069108E3FEC5EEA8E7E80D817C1A80F9009674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.LocalAppContextSwitches
struct  LocalAppContextSwitches_tC0069108E3FEC5EEA8E7E80D817C1A80F9009674  : public RuntimeObject
{
public:

public:
};

struct LocalAppContextSwitches_tC0069108E3FEC5EEA8E7E80D817C1A80F9009674_StaticFields
{
public:
	// System.Boolean System.LocalAppContextSwitches::IgnoreEmptyKeySequences
	bool ___IgnoreEmptyKeySequences_0;
	// System.Boolean System.LocalAppContextSwitches::DontThrowOnInvalidSurrogatePairs
	bool ___DontThrowOnInvalidSurrogatePairs_1;

public:
	inline static int32_t get_offset_of_IgnoreEmptyKeySequences_0() { return static_cast<int32_t>(offsetof(LocalAppContextSwitches_tC0069108E3FEC5EEA8E7E80D817C1A80F9009674_StaticFields, ___IgnoreEmptyKeySequences_0)); }
	inline bool get_IgnoreEmptyKeySequences_0() const { return ___IgnoreEmptyKeySequences_0; }
	inline bool* get_address_of_IgnoreEmptyKeySequences_0() { return &___IgnoreEmptyKeySequences_0; }
	inline void set_IgnoreEmptyKeySequences_0(bool value)
	{
		___IgnoreEmptyKeySequences_0 = value;
	}

	inline static int32_t get_offset_of_DontThrowOnInvalidSurrogatePairs_1() { return static_cast<int32_t>(offsetof(LocalAppContextSwitches_tC0069108E3FEC5EEA8E7E80D817C1A80F9009674_StaticFields, ___DontThrowOnInvalidSurrogatePairs_1)); }
	inline bool get_DontThrowOnInvalidSurrogatePairs_1() const { return ___DontThrowOnInvalidSurrogatePairs_1; }
	inline bool* get_address_of_DontThrowOnInvalidSurrogatePairs_1() { return &___DontThrowOnInvalidSurrogatePairs_1; }
	inline void set_DontThrowOnInvalidSurrogatePairs_1(bool value)
	{
		___DontThrowOnInvalidSurrogatePairs_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALAPPCONTEXTSWITCHES_TC0069108E3FEC5EEA8E7E80D817C1A80F9009674_H
#ifndef ENCODERFALLBACK_TDE342346D01608628F1BCEBB652D31009852CF63_H
#define ENCODERFALLBACK_TDE342346D01608628F1BCEBB652D31009852CF63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.EncoderFallback
struct  EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63  : public RuntimeObject
{
public:
	// System.Boolean System.Text.EncoderFallback::bIsMicrosoftBestFitFallback
	bool ___bIsMicrosoftBestFitFallback_0;

public:
	inline static int32_t get_offset_of_bIsMicrosoftBestFitFallback_0() { return static_cast<int32_t>(offsetof(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63, ___bIsMicrosoftBestFitFallback_0)); }
	inline bool get_bIsMicrosoftBestFitFallback_0() const { return ___bIsMicrosoftBestFitFallback_0; }
	inline bool* get_address_of_bIsMicrosoftBestFitFallback_0() { return &___bIsMicrosoftBestFitFallback_0; }
	inline void set_bIsMicrosoftBestFitFallback_0(bool value)
	{
		___bIsMicrosoftBestFitFallback_0 = value;
	}
};

struct EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63_StaticFields
{
public:
	// System.Text.EncoderFallback modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.EncoderFallback::replacementFallback
	EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * ___replacementFallback_1;
	// System.Text.EncoderFallback modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.EncoderFallback::exceptionFallback
	EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * ___exceptionFallback_2;
	// System.Object System.Text.EncoderFallback::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_3;

public:
	inline static int32_t get_offset_of_replacementFallback_1() { return static_cast<int32_t>(offsetof(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63_StaticFields, ___replacementFallback_1)); }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * get_replacementFallback_1() const { return ___replacementFallback_1; }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 ** get_address_of_replacementFallback_1() { return &___replacementFallback_1; }
	inline void set_replacementFallback_1(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * value)
	{
		___replacementFallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___replacementFallback_1), value);
	}

	inline static int32_t get_offset_of_exceptionFallback_2() { return static_cast<int32_t>(offsetof(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63_StaticFields, ___exceptionFallback_2)); }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * get_exceptionFallback_2() const { return ___exceptionFallback_2; }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 ** get_address_of_exceptionFallback_2() { return &___exceptionFallback_2; }
	inline void set_exceptionFallback_2(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * value)
	{
		___exceptionFallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___exceptionFallback_2), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_3() { return static_cast<int32_t>(offsetof(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63_StaticFields, ___s_InternalSyncObject_3)); }
	inline RuntimeObject * get_s_InternalSyncObject_3() const { return ___s_InternalSyncObject_3; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_3() { return &___s_InternalSyncObject_3; }
	inline void set_s_InternalSyncObject_3(RuntimeObject * value)
	{
		___s_InternalSyncObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODERFALLBACK_TDE342346D01608628F1BCEBB652D31009852CF63_H
#ifndef ENCODERFALLBACKBUFFER_TE878BFB956A0F4A1D630C08CA42B170534A3FD5C_H
#define ENCODERFALLBACKBUFFER_TE878BFB956A0F4A1D630C08CA42B170534A3FD5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.EncoderFallbackBuffer
struct  EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C  : public RuntimeObject
{
public:
	// System.Char* System.Text.EncoderFallbackBuffer::charStart
	Il2CppChar* ___charStart_0;
	// System.Char* System.Text.EncoderFallbackBuffer::charEnd
	Il2CppChar* ___charEnd_1;
	// System.Text.EncoderNLS System.Text.EncoderFallbackBuffer::encoder
	EncoderNLS_t47EFB0F5A59F41C429930063F966A68F8D394DF8 * ___encoder_2;
	// System.Boolean System.Text.EncoderFallbackBuffer::setEncoder
	bool ___setEncoder_3;
	// System.Boolean System.Text.EncoderFallbackBuffer::bUsedEncoder
	bool ___bUsedEncoder_4;
	// System.Boolean System.Text.EncoderFallbackBuffer::bFallingBack
	bool ___bFallingBack_5;
	// System.Int32 System.Text.EncoderFallbackBuffer::iRecursionCount
	int32_t ___iRecursionCount_6;

public:
	inline static int32_t get_offset_of_charStart_0() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C, ___charStart_0)); }
	inline Il2CppChar* get_charStart_0() const { return ___charStart_0; }
	inline Il2CppChar** get_address_of_charStart_0() { return &___charStart_0; }
	inline void set_charStart_0(Il2CppChar* value)
	{
		___charStart_0 = value;
	}

	inline static int32_t get_offset_of_charEnd_1() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C, ___charEnd_1)); }
	inline Il2CppChar* get_charEnd_1() const { return ___charEnd_1; }
	inline Il2CppChar** get_address_of_charEnd_1() { return &___charEnd_1; }
	inline void set_charEnd_1(Il2CppChar* value)
	{
		___charEnd_1 = value;
	}

	inline static int32_t get_offset_of_encoder_2() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C, ___encoder_2)); }
	inline EncoderNLS_t47EFB0F5A59F41C429930063F966A68F8D394DF8 * get_encoder_2() const { return ___encoder_2; }
	inline EncoderNLS_t47EFB0F5A59F41C429930063F966A68F8D394DF8 ** get_address_of_encoder_2() { return &___encoder_2; }
	inline void set_encoder_2(EncoderNLS_t47EFB0F5A59F41C429930063F966A68F8D394DF8 * value)
	{
		___encoder_2 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_2), value);
	}

	inline static int32_t get_offset_of_setEncoder_3() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C, ___setEncoder_3)); }
	inline bool get_setEncoder_3() const { return ___setEncoder_3; }
	inline bool* get_address_of_setEncoder_3() { return &___setEncoder_3; }
	inline void set_setEncoder_3(bool value)
	{
		___setEncoder_3 = value;
	}

	inline static int32_t get_offset_of_bUsedEncoder_4() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C, ___bUsedEncoder_4)); }
	inline bool get_bUsedEncoder_4() const { return ___bUsedEncoder_4; }
	inline bool* get_address_of_bUsedEncoder_4() { return &___bUsedEncoder_4; }
	inline void set_bUsedEncoder_4(bool value)
	{
		___bUsedEncoder_4 = value;
	}

	inline static int32_t get_offset_of_bFallingBack_5() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C, ___bFallingBack_5)); }
	inline bool get_bFallingBack_5() const { return ___bFallingBack_5; }
	inline bool* get_address_of_bFallingBack_5() { return &___bFallingBack_5; }
	inline void set_bFallingBack_5(bool value)
	{
		___bFallingBack_5 = value;
	}

	inline static int32_t get_offset_of_iRecursionCount_6() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C, ___iRecursionCount_6)); }
	inline int32_t get_iRecursionCount_6() const { return ___iRecursionCount_6; }
	inline int32_t* get_address_of_iRecursionCount_6() { return &___iRecursionCount_6; }
	inline void set_iRecursionCount_6(int32_t value)
	{
		___iRecursionCount_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODERFALLBACKBUFFER_TE878BFB956A0F4A1D630C08CA42B170534A3FD5C_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ASYNCHELPER_T9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_H
#define ASYNCHELPER_T9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.AsyncHelper
struct  AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2  : public RuntimeObject
{
public:

public:
};

struct AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields
{
public:
	// System.Threading.Tasks.Task System.Xml.AsyncHelper::DoneTask
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___DoneTask_0;
	// System.Threading.Tasks.Task`1<System.Boolean> System.Xml.AsyncHelper::DoneTaskTrue
	Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 * ___DoneTaskTrue_1;
	// System.Threading.Tasks.Task`1<System.Boolean> System.Xml.AsyncHelper::DoneTaskFalse
	Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 * ___DoneTaskFalse_2;
	// System.Threading.Tasks.Task`1<System.Int32> System.Xml.AsyncHelper::DoneTaskZero
	Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * ___DoneTaskZero_3;

public:
	inline static int32_t get_offset_of_DoneTask_0() { return static_cast<int32_t>(offsetof(AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields, ___DoneTask_0)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_DoneTask_0() const { return ___DoneTask_0; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_DoneTask_0() { return &___DoneTask_0; }
	inline void set_DoneTask_0(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___DoneTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___DoneTask_0), value);
	}

	inline static int32_t get_offset_of_DoneTaskTrue_1() { return static_cast<int32_t>(offsetof(AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields, ___DoneTaskTrue_1)); }
	inline Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 * get_DoneTaskTrue_1() const { return ___DoneTaskTrue_1; }
	inline Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 ** get_address_of_DoneTaskTrue_1() { return &___DoneTaskTrue_1; }
	inline void set_DoneTaskTrue_1(Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 * value)
	{
		___DoneTaskTrue_1 = value;
		Il2CppCodeGenWriteBarrier((&___DoneTaskTrue_1), value);
	}

	inline static int32_t get_offset_of_DoneTaskFalse_2() { return static_cast<int32_t>(offsetof(AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields, ___DoneTaskFalse_2)); }
	inline Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 * get_DoneTaskFalse_2() const { return ___DoneTaskFalse_2; }
	inline Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 ** get_address_of_DoneTaskFalse_2() { return &___DoneTaskFalse_2; }
	inline void set_DoneTaskFalse_2(Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 * value)
	{
		___DoneTaskFalse_2 = value;
		Il2CppCodeGenWriteBarrier((&___DoneTaskFalse_2), value);
	}

	inline static int32_t get_offset_of_DoneTaskZero_3() { return static_cast<int32_t>(offsetof(AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields, ___DoneTaskZero_3)); }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * get_DoneTaskZero_3() const { return ___DoneTaskZero_3; }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 ** get_address_of_DoneTaskZero_3() { return &___DoneTaskZero_3; }
	inline void set_DoneTaskZero_3(Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * value)
	{
		___DoneTaskZero_3 = value;
		Il2CppCodeGenWriteBarrier((&___DoneTaskZero_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCHELPER_T9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_H
#ifndef BASE64ENCODER_TF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922_H
#define BASE64ENCODER_TF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Base64Encoder
struct  Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922  : public RuntimeObject
{
public:
	// System.Byte[] System.Xml.Base64Encoder::leftOverBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___leftOverBytes_0;
	// System.Int32 System.Xml.Base64Encoder::leftOverBytesCount
	int32_t ___leftOverBytesCount_1;
	// System.Char[] System.Xml.Base64Encoder::charsLine
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___charsLine_2;

public:
	inline static int32_t get_offset_of_leftOverBytes_0() { return static_cast<int32_t>(offsetof(Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922, ___leftOverBytes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_leftOverBytes_0() const { return ___leftOverBytes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_leftOverBytes_0() { return &___leftOverBytes_0; }
	inline void set_leftOverBytes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___leftOverBytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___leftOverBytes_0), value);
	}

	inline static int32_t get_offset_of_leftOverBytesCount_1() { return static_cast<int32_t>(offsetof(Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922, ___leftOverBytesCount_1)); }
	inline int32_t get_leftOverBytesCount_1() const { return ___leftOverBytesCount_1; }
	inline int32_t* get_address_of_leftOverBytesCount_1() { return &___leftOverBytesCount_1; }
	inline void set_leftOverBytesCount_1(int32_t value)
	{
		___leftOverBytesCount_1 = value;
	}

	inline static int32_t get_offset_of_charsLine_2() { return static_cast<int32_t>(offsetof(Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922, ___charsLine_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_charsLine_2() const { return ___charsLine_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_charsLine_2() { return &___charsLine_2; }
	inline void set_charsLine_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___charsLine_2 = value;
		Il2CppCodeGenWriteBarrier((&___charsLine_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE64ENCODER_TF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922_H
#ifndef BINHEXENCODER_T1D70914F68F07D8480A2946DA87C8A41AD386DBA_H
#define BINHEXENCODER_T1D70914F68F07D8480A2946DA87C8A41AD386DBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinHexEncoder
struct  BinHexEncoder_t1D70914F68F07D8480A2946DA87C8A41AD386DBA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINHEXENCODER_T1D70914F68F07D8480A2946DA87C8A41AD386DBA_H
#ifndef BINXMLDATETIME_TD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_H
#define BINXMLDATETIME_TD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinXmlDateTime
struct  BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53  : public RuntimeObject
{
public:

public:
};

struct BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_StaticFields
{
public:
	// System.Int32[] System.Xml.BinXmlDateTime::KatmaiTimeScaleMultiplicator
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___KatmaiTimeScaleMultiplicator_0;
	// System.Double System.Xml.BinXmlDateTime::SQLTicksPerMillisecond
	double ___SQLTicksPerMillisecond_1;
	// System.Int32 System.Xml.BinXmlDateTime::SQLTicksPerSecond
	int32_t ___SQLTicksPerSecond_2;
	// System.Int32 System.Xml.BinXmlDateTime::SQLTicksPerMinute
	int32_t ___SQLTicksPerMinute_3;
	// System.Int32 System.Xml.BinXmlDateTime::SQLTicksPerHour
	int32_t ___SQLTicksPerHour_4;
	// System.Int32 System.Xml.BinXmlDateTime::SQLTicksPerDay
	int32_t ___SQLTicksPerDay_5;

public:
	inline static int32_t get_offset_of_KatmaiTimeScaleMultiplicator_0() { return static_cast<int32_t>(offsetof(BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_StaticFields, ___KatmaiTimeScaleMultiplicator_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_KatmaiTimeScaleMultiplicator_0() const { return ___KatmaiTimeScaleMultiplicator_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_KatmaiTimeScaleMultiplicator_0() { return &___KatmaiTimeScaleMultiplicator_0; }
	inline void set_KatmaiTimeScaleMultiplicator_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___KatmaiTimeScaleMultiplicator_0 = value;
		Il2CppCodeGenWriteBarrier((&___KatmaiTimeScaleMultiplicator_0), value);
	}

	inline static int32_t get_offset_of_SQLTicksPerMillisecond_1() { return static_cast<int32_t>(offsetof(BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_StaticFields, ___SQLTicksPerMillisecond_1)); }
	inline double get_SQLTicksPerMillisecond_1() const { return ___SQLTicksPerMillisecond_1; }
	inline double* get_address_of_SQLTicksPerMillisecond_1() { return &___SQLTicksPerMillisecond_1; }
	inline void set_SQLTicksPerMillisecond_1(double value)
	{
		___SQLTicksPerMillisecond_1 = value;
	}

	inline static int32_t get_offset_of_SQLTicksPerSecond_2() { return static_cast<int32_t>(offsetof(BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_StaticFields, ___SQLTicksPerSecond_2)); }
	inline int32_t get_SQLTicksPerSecond_2() const { return ___SQLTicksPerSecond_2; }
	inline int32_t* get_address_of_SQLTicksPerSecond_2() { return &___SQLTicksPerSecond_2; }
	inline void set_SQLTicksPerSecond_2(int32_t value)
	{
		___SQLTicksPerSecond_2 = value;
	}

	inline static int32_t get_offset_of_SQLTicksPerMinute_3() { return static_cast<int32_t>(offsetof(BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_StaticFields, ___SQLTicksPerMinute_3)); }
	inline int32_t get_SQLTicksPerMinute_3() const { return ___SQLTicksPerMinute_3; }
	inline int32_t* get_address_of_SQLTicksPerMinute_3() { return &___SQLTicksPerMinute_3; }
	inline void set_SQLTicksPerMinute_3(int32_t value)
	{
		___SQLTicksPerMinute_3 = value;
	}

	inline static int32_t get_offset_of_SQLTicksPerHour_4() { return static_cast<int32_t>(offsetof(BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_StaticFields, ___SQLTicksPerHour_4)); }
	inline int32_t get_SQLTicksPerHour_4() const { return ___SQLTicksPerHour_4; }
	inline int32_t* get_address_of_SQLTicksPerHour_4() { return &___SQLTicksPerHour_4; }
	inline void set_SQLTicksPerHour_4(int32_t value)
	{
		___SQLTicksPerHour_4 = value;
	}

	inline static int32_t get_offset_of_SQLTicksPerDay_5() { return static_cast<int32_t>(offsetof(BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_StaticFields, ___SQLTicksPerDay_5)); }
	inline int32_t get_SQLTicksPerDay_5() const { return ___SQLTicksPerDay_5; }
	inline int32_t* get_address_of_SQLTicksPerDay_5() { return &___SQLTicksPerDay_5; }
	inline void set_SQLTicksPerDay_5(int32_t value)
	{
		___SQLTicksPerDay_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINXMLDATETIME_TD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_H
#ifndef BINARYCOMPATIBILITY_T12E05D85EDA182BE84FFFDDBE58545DC7B43AD72_H
#define BINARYCOMPATIBILITY_T12E05D85EDA182BE84FFFDDBE58545DC7B43AD72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinaryCompatibility
struct  BinaryCompatibility_t12E05D85EDA182BE84FFFDDBE58545DC7B43AD72  : public RuntimeObject
{
public:

public:
};

struct BinaryCompatibility_t12E05D85EDA182BE84FFFDDBE58545DC7B43AD72_StaticFields
{
public:
	// System.Boolean System.Xml.BinaryCompatibility::_targetsAtLeast_Desktop_V4_5_2
	bool ____targetsAtLeast_Desktop_V4_5_2_0;

public:
	inline static int32_t get_offset_of__targetsAtLeast_Desktop_V4_5_2_0() { return static_cast<int32_t>(offsetof(BinaryCompatibility_t12E05D85EDA182BE84FFFDDBE58545DC7B43AD72_StaticFields, ____targetsAtLeast_Desktop_V4_5_2_0)); }
	inline bool get__targetsAtLeast_Desktop_V4_5_2_0() const { return ____targetsAtLeast_Desktop_V4_5_2_0; }
	inline bool* get_address_of__targetsAtLeast_Desktop_V4_5_2_0() { return &____targetsAtLeast_Desktop_V4_5_2_0; }
	inline void set__targetsAtLeast_Desktop_V4_5_2_0(bool value)
	{
		____targetsAtLeast_Desktop_V4_5_2_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYCOMPATIBILITY_T12E05D85EDA182BE84FFFDDBE58545DC7B43AD72_H
#ifndef BITSTACK_TA699E16D899D050049FF82EC15EC8B8F36CFB2E7_H
#define BITSTACK_TA699E16D899D050049FF82EC15EC8B8F36CFB2E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BitStack
struct  BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7  : public RuntimeObject
{
public:
	// System.UInt32[] System.Xml.BitStack::bitStack
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___bitStack_0;
	// System.Int32 System.Xml.BitStack::stackPos
	int32_t ___stackPos_1;
	// System.UInt32 System.Xml.BitStack::curr
	uint32_t ___curr_2;

public:
	inline static int32_t get_offset_of_bitStack_0() { return static_cast<int32_t>(offsetof(BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7, ___bitStack_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_bitStack_0() const { return ___bitStack_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_bitStack_0() { return &___bitStack_0; }
	inline void set_bitStack_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___bitStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___bitStack_0), value);
	}

	inline static int32_t get_offset_of_stackPos_1() { return static_cast<int32_t>(offsetof(BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7, ___stackPos_1)); }
	inline int32_t get_stackPos_1() const { return ___stackPos_1; }
	inline int32_t* get_address_of_stackPos_1() { return &___stackPos_1; }
	inline void set_stackPos_1(int32_t value)
	{
		___stackPos_1 = value;
	}

	inline static int32_t get_offset_of_curr_2() { return static_cast<int32_t>(offsetof(BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7, ___curr_2)); }
	inline uint32_t get_curr_2() const { return ___curr_2; }
	inline uint32_t* get_address_of_curr_2() { return &___curr_2; }
	inline void set_curr_2(uint32_t value)
	{
		___curr_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITSTACK_TA699E16D899D050049FF82EC15EC8B8F36CFB2E7_H
#ifndef BITS_T6CCF20605799DC40E70BCBC72EF872899C211658_H
#define BITS_T6CCF20605799DC40E70BCBC72EF872899C211658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Bits
struct  Bits_t6CCF20605799DC40E70BCBC72EF872899C211658  : public RuntimeObject
{
public:

public:
};

struct Bits_t6CCF20605799DC40E70BCBC72EF872899C211658_StaticFields
{
public:
	// System.UInt32 System.Xml.Bits::MASK_0101010101010101
	uint32_t ___MASK_0101010101010101_0;
	// System.UInt32 System.Xml.Bits::MASK_0011001100110011
	uint32_t ___MASK_0011001100110011_1;
	// System.UInt32 System.Xml.Bits::MASK_0000111100001111
	uint32_t ___MASK_0000111100001111_2;
	// System.UInt32 System.Xml.Bits::MASK_0000000011111111
	uint32_t ___MASK_0000000011111111_3;
	// System.UInt32 System.Xml.Bits::MASK_1111111111111111
	uint32_t ___MASK_1111111111111111_4;

public:
	inline static int32_t get_offset_of_MASK_0101010101010101_0() { return static_cast<int32_t>(offsetof(Bits_t6CCF20605799DC40E70BCBC72EF872899C211658_StaticFields, ___MASK_0101010101010101_0)); }
	inline uint32_t get_MASK_0101010101010101_0() const { return ___MASK_0101010101010101_0; }
	inline uint32_t* get_address_of_MASK_0101010101010101_0() { return &___MASK_0101010101010101_0; }
	inline void set_MASK_0101010101010101_0(uint32_t value)
	{
		___MASK_0101010101010101_0 = value;
	}

	inline static int32_t get_offset_of_MASK_0011001100110011_1() { return static_cast<int32_t>(offsetof(Bits_t6CCF20605799DC40E70BCBC72EF872899C211658_StaticFields, ___MASK_0011001100110011_1)); }
	inline uint32_t get_MASK_0011001100110011_1() const { return ___MASK_0011001100110011_1; }
	inline uint32_t* get_address_of_MASK_0011001100110011_1() { return &___MASK_0011001100110011_1; }
	inline void set_MASK_0011001100110011_1(uint32_t value)
	{
		___MASK_0011001100110011_1 = value;
	}

	inline static int32_t get_offset_of_MASK_0000111100001111_2() { return static_cast<int32_t>(offsetof(Bits_t6CCF20605799DC40E70BCBC72EF872899C211658_StaticFields, ___MASK_0000111100001111_2)); }
	inline uint32_t get_MASK_0000111100001111_2() const { return ___MASK_0000111100001111_2; }
	inline uint32_t* get_address_of_MASK_0000111100001111_2() { return &___MASK_0000111100001111_2; }
	inline void set_MASK_0000111100001111_2(uint32_t value)
	{
		___MASK_0000111100001111_2 = value;
	}

	inline static int32_t get_offset_of_MASK_0000000011111111_3() { return static_cast<int32_t>(offsetof(Bits_t6CCF20605799DC40E70BCBC72EF872899C211658_StaticFields, ___MASK_0000000011111111_3)); }
	inline uint32_t get_MASK_0000000011111111_3() const { return ___MASK_0000000011111111_3; }
	inline uint32_t* get_address_of_MASK_0000000011111111_3() { return &___MASK_0000000011111111_3; }
	inline void set_MASK_0000000011111111_3(uint32_t value)
	{
		___MASK_0000000011111111_3 = value;
	}

	inline static int32_t get_offset_of_MASK_1111111111111111_4() { return static_cast<int32_t>(offsetof(Bits_t6CCF20605799DC40E70BCBC72EF872899C211658_StaticFields, ___MASK_1111111111111111_4)); }
	inline uint32_t get_MASK_1111111111111111_4() const { return ___MASK_1111111111111111_4; }
	inline uint32_t* get_address_of_MASK_1111111111111111_4() { return &___MASK_1111111111111111_4; }
	inline void set_MASK_1111111111111111_4(uint32_t value)
	{
		___MASK_1111111111111111_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITS_T6CCF20605799DC40E70BCBC72EF872899C211658_H
#ifndef BYTESTACK_T5D4D0EB77957524FC89C77E3BB037A0F25C23D75_H
#define BYTESTACK_T5D4D0EB77957524FC89C77E3BB037A0F25C23D75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ByteStack
struct  ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75  : public RuntimeObject
{
public:
	// System.Byte[] System.Xml.ByteStack::stack
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___stack_0;
	// System.Int32 System.Xml.ByteStack::growthRate
	int32_t ___growthRate_1;
	// System.Int32 System.Xml.ByteStack::top
	int32_t ___top_2;
	// System.Int32 System.Xml.ByteStack::size
	int32_t ___size_3;

public:
	inline static int32_t get_offset_of_stack_0() { return static_cast<int32_t>(offsetof(ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75, ___stack_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_stack_0() const { return ___stack_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_stack_0() { return &___stack_0; }
	inline void set_stack_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___stack_0), value);
	}

	inline static int32_t get_offset_of_growthRate_1() { return static_cast<int32_t>(offsetof(ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75, ___growthRate_1)); }
	inline int32_t get_growthRate_1() const { return ___growthRate_1; }
	inline int32_t* get_address_of_growthRate_1() { return &___growthRate_1; }
	inline void set_growthRate_1(int32_t value)
	{
		___growthRate_1 = value;
	}

	inline static int32_t get_offset_of_top_2() { return static_cast<int32_t>(offsetof(ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75, ___top_2)); }
	inline int32_t get_top_2() const { return ___top_2; }
	inline int32_t* get_address_of_top_2() { return &___top_2; }
	inline void set_top_2(int32_t value)
	{
		___top_2 = value;
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75, ___size_3)); }
	inline int32_t get_size_3() const { return ___size_3; }
	inline int32_t* get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(int32_t value)
	{
		___size_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTESTACK_T5D4D0EB77957524FC89C77E3BB037A0F25C23D75_H
#ifndef HTMLTERNARYTREE_T75391F99BF6805F09C00996C1A7F329CD74113D4_H
#define HTMLTERNARYTREE_T75391F99BF6805F09C00996C1A7F329CD74113D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HtmlTernaryTree
struct  HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4  : public RuntimeObject
{
public:

public:
};

struct HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4_StaticFields
{
public:
	// System.Byte[] System.Xml.HtmlTernaryTree::htmlElements
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___htmlElements_0;
	// System.Byte[] System.Xml.HtmlTernaryTree::htmlAttributes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___htmlAttributes_1;

public:
	inline static int32_t get_offset_of_htmlElements_0() { return static_cast<int32_t>(offsetof(HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4_StaticFields, ___htmlElements_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_htmlElements_0() const { return ___htmlElements_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_htmlElements_0() { return &___htmlElements_0; }
	inline void set_htmlElements_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___htmlElements_0 = value;
		Il2CppCodeGenWriteBarrier((&___htmlElements_0), value);
	}

	inline static int32_t get_offset_of_htmlAttributes_1() { return static_cast<int32_t>(offsetof(HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4_StaticFields, ___htmlAttributes_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_htmlAttributes_1() const { return ___htmlAttributes_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_htmlAttributes_1() { return &___htmlAttributes_1; }
	inline void set_htmlAttributes_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___htmlAttributes_1 = value;
		Il2CppCodeGenWriteBarrier((&___htmlAttributes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLTERNARYTREE_T75391F99BF6805F09C00996C1A7F329CD74113D4_H
#ifndef INCREMENTALREADDECODER_T787BFB5889B01B88DDA030C503A0C2E0525CA723_H
#define INCREMENTALREADDECODER_T787BFB5889B01B88DDA030C503A0C2E0525CA723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.IncrementalReadDecoder
struct  IncrementalReadDecoder_t787BFB5889B01B88DDA030C503A0C2E0525CA723  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INCREMENTALREADDECODER_T787BFB5889B01B88DDA030C503A0C2E0525CA723_H
#ifndef SECURESTRINGHASHER_TC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_H
#define SECURESTRINGHASHER_TC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.SecureStringHasher
struct  SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.SecureStringHasher::hashCodeRandomizer
	int32_t ___hashCodeRandomizer_1;

public:
	inline static int32_t get_offset_of_hashCodeRandomizer_1() { return static_cast<int32_t>(offsetof(SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3, ___hashCodeRandomizer_1)); }
	inline int32_t get_hashCodeRandomizer_1() const { return ___hashCodeRandomizer_1; }
	inline int32_t* get_address_of_hashCodeRandomizer_1() { return &___hashCodeRandomizer_1; }
	inline void set_hashCodeRandomizer_1(int32_t value)
	{
		___hashCodeRandomizer_1 = value;
	}
};

struct SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_StaticFields
{
public:
	// System.Xml.SecureStringHasher/HashCodeOfStringDelegate System.Xml.SecureStringHasher::hashCodeDelegate
	HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * ___hashCodeDelegate_0;

public:
	inline static int32_t get_offset_of_hashCodeDelegate_0() { return static_cast<int32_t>(offsetof(SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_StaticFields, ___hashCodeDelegate_0)); }
	inline HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * get_hashCodeDelegate_0() const { return ___hashCodeDelegate_0; }
	inline HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E ** get_address_of_hashCodeDelegate_0() { return &___hashCodeDelegate_0; }
	inline void set_hashCodeDelegate_0(HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * value)
	{
		___hashCodeDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___hashCodeDelegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURESTRINGHASHER_TC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_H
#ifndef TERNARYTREEREADONLY_T2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2_H
#define TERNARYTREEREADONLY_T2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.TernaryTreeReadOnly
struct  TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2  : public RuntimeObject
{
public:
	// System.Byte[] System.Xml.TernaryTreeReadOnly::nodeBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___nodeBuffer_0;

public:
	inline static int32_t get_offset_of_nodeBuffer_0() { return static_cast<int32_t>(offsetof(TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2, ___nodeBuffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_nodeBuffer_0() const { return ___nodeBuffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_nodeBuffer_0() { return &___nodeBuffer_0; }
	inline void set_nodeBuffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___nodeBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___nodeBuffer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERNARYTREEREADONLY_T2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2_H
#ifndef XPATHITEM_TCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98_H
#define XPATHITEM_TCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathItem
struct  XPathItem_tCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHITEM_TCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98_H
#ifndef XMLREADER_T13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_H
#define XMLREADER_T13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB  : public RuntimeObject
{
public:

public:
};

struct XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields
{
public:
	// System.UInt32 System.Xml.XmlReader::IsTextualNodeBitmap
	uint32_t ___IsTextualNodeBitmap_0;
	// System.UInt32 System.Xml.XmlReader::CanReadContentAsBitmap
	uint32_t ___CanReadContentAsBitmap_1;
	// System.UInt32 System.Xml.XmlReader::HasValueBitmap
	uint32_t ___HasValueBitmap_2;

public:
	inline static int32_t get_offset_of_IsTextualNodeBitmap_0() { return static_cast<int32_t>(offsetof(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields, ___IsTextualNodeBitmap_0)); }
	inline uint32_t get_IsTextualNodeBitmap_0() const { return ___IsTextualNodeBitmap_0; }
	inline uint32_t* get_address_of_IsTextualNodeBitmap_0() { return &___IsTextualNodeBitmap_0; }
	inline void set_IsTextualNodeBitmap_0(uint32_t value)
	{
		___IsTextualNodeBitmap_0 = value;
	}

	inline static int32_t get_offset_of_CanReadContentAsBitmap_1() { return static_cast<int32_t>(offsetof(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields, ___CanReadContentAsBitmap_1)); }
	inline uint32_t get_CanReadContentAsBitmap_1() const { return ___CanReadContentAsBitmap_1; }
	inline uint32_t* get_address_of_CanReadContentAsBitmap_1() { return &___CanReadContentAsBitmap_1; }
	inline void set_CanReadContentAsBitmap_1(uint32_t value)
	{
		___CanReadContentAsBitmap_1 = value;
	}

	inline static int32_t get_offset_of_HasValueBitmap_2() { return static_cast<int32_t>(offsetof(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields, ___HasValueBitmap_2)); }
	inline uint32_t get_HasValueBitmap_2() const { return ___HasValueBitmap_2; }
	inline uint32_t* get_address_of_HasValueBitmap_2() { return &___HasValueBitmap_2; }
	inline void set_HasValueBitmap_2(uint32_t value)
	{
		___HasValueBitmap_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_T13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_H
#ifndef NAMESPACEDECL_T9E6E447DBA857B63A848C5CAACBB5C176493473B_H
#define NAMESPACEDECL_T9E6E447DBA857B63A848C5CAACBB5C176493473B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSqlBinaryReader/NamespaceDecl
struct  NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlSqlBinaryReader/NamespaceDecl::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlSqlBinaryReader/NamespaceDecl::uri
	String_t* ___uri_1;
	// System.Xml.XmlSqlBinaryReader/NamespaceDecl System.Xml.XmlSqlBinaryReader/NamespaceDecl::scopeLink
	NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B * ___scopeLink_2;
	// System.Xml.XmlSqlBinaryReader/NamespaceDecl System.Xml.XmlSqlBinaryReader/NamespaceDecl::prevLink
	NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B * ___prevLink_3;
	// System.Int32 System.Xml.XmlSqlBinaryReader/NamespaceDecl::scope
	int32_t ___scope_4;
	// System.Boolean System.Xml.XmlSqlBinaryReader/NamespaceDecl::implied
	bool ___implied_5;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_uri_1() { return static_cast<int32_t>(offsetof(NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B, ___uri_1)); }
	inline String_t* get_uri_1() const { return ___uri_1; }
	inline String_t** get_address_of_uri_1() { return &___uri_1; }
	inline void set_uri_1(String_t* value)
	{
		___uri_1 = value;
		Il2CppCodeGenWriteBarrier((&___uri_1), value);
	}

	inline static int32_t get_offset_of_scopeLink_2() { return static_cast<int32_t>(offsetof(NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B, ___scopeLink_2)); }
	inline NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B * get_scopeLink_2() const { return ___scopeLink_2; }
	inline NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B ** get_address_of_scopeLink_2() { return &___scopeLink_2; }
	inline void set_scopeLink_2(NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B * value)
	{
		___scopeLink_2 = value;
		Il2CppCodeGenWriteBarrier((&___scopeLink_2), value);
	}

	inline static int32_t get_offset_of_prevLink_3() { return static_cast<int32_t>(offsetof(NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B, ___prevLink_3)); }
	inline NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B * get_prevLink_3() const { return ___prevLink_3; }
	inline NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B ** get_address_of_prevLink_3() { return &___prevLink_3; }
	inline void set_prevLink_3(NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B * value)
	{
		___prevLink_3 = value;
		Il2CppCodeGenWriteBarrier((&___prevLink_3), value);
	}

	inline static int32_t get_offset_of_scope_4() { return static_cast<int32_t>(offsetof(NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B, ___scope_4)); }
	inline int32_t get_scope_4() const { return ___scope_4; }
	inline int32_t* get_address_of_scope_4() { return &___scope_4; }
	inline void set_scope_4(int32_t value)
	{
		___scope_4 = value;
	}

	inline static int32_t get_offset_of_implied_5() { return static_cast<int32_t>(offsetof(NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B, ___implied_5)); }
	inline bool get_implied_5() const { return ___implied_5; }
	inline bool* get_address_of_implied_5() { return &___implied_5; }
	inline void set_implied_5(bool value)
	{
		___implied_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEDECL_T9E6E447DBA857B63A848C5CAACBB5C176493473B_H
#ifndef XMLWRITER_T4FAF83E5244FC8F339B19D481C348ACA1510E869_H
#define XMLWRITER_T4FAF83E5244FC8F339B19D481C348ACA1510E869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWriter
struct  XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869  : public RuntimeObject
{
public:
	// System.Char[] System.Xml.XmlWriter::writeNodeBuffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___writeNodeBuffer_0;

public:
	inline static int32_t get_offset_of_writeNodeBuffer_0() { return static_cast<int32_t>(offsetof(XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869, ___writeNodeBuffer_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_writeNodeBuffer_0() const { return ___writeNodeBuffer_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_writeNodeBuffer_0() { return &___writeNodeBuffer_0; }
	inline void set_writeNodeBuffer_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___writeNodeBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___writeNodeBuffer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWRITER_T4FAF83E5244FC8F339B19D481C348ACA1510E869_H
#ifndef XPATHNODE_TC207ED6C653E80055FE6C5ECD3E6137A326659A0_H
#define XPATHNODE_TC207ED6C653E80055FE6C5ECD3E6137A326659A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNode
struct  XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0 
{
public:
	// MS.Internal.Xml.Cache.XPathNodeInfoAtom MS.Internal.Xml.Cache.XPathNode::info
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7 * ___info_0;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::idxSibling
	uint16_t ___idxSibling_1;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::idxParent
	uint16_t ___idxParent_2;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::idxSimilar
	uint16_t ___idxSimilar_3;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::posOffset
	uint16_t ___posOffset_4;
	// System.UInt32 MS.Internal.Xml.Cache.XPathNode::props
	uint32_t ___props_5;
	// System.String MS.Internal.Xml.Cache.XPathNode::value
	String_t* ___value_6;

public:
	inline static int32_t get_offset_of_info_0() { return static_cast<int32_t>(offsetof(XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0, ___info_0)); }
	inline XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7 * get_info_0() const { return ___info_0; }
	inline XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7 ** get_address_of_info_0() { return &___info_0; }
	inline void set_info_0(XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7 * value)
	{
		___info_0 = value;
		Il2CppCodeGenWriteBarrier((&___info_0), value);
	}

	inline static int32_t get_offset_of_idxSibling_1() { return static_cast<int32_t>(offsetof(XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0, ___idxSibling_1)); }
	inline uint16_t get_idxSibling_1() const { return ___idxSibling_1; }
	inline uint16_t* get_address_of_idxSibling_1() { return &___idxSibling_1; }
	inline void set_idxSibling_1(uint16_t value)
	{
		___idxSibling_1 = value;
	}

	inline static int32_t get_offset_of_idxParent_2() { return static_cast<int32_t>(offsetof(XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0, ___idxParent_2)); }
	inline uint16_t get_idxParent_2() const { return ___idxParent_2; }
	inline uint16_t* get_address_of_idxParent_2() { return &___idxParent_2; }
	inline void set_idxParent_2(uint16_t value)
	{
		___idxParent_2 = value;
	}

	inline static int32_t get_offset_of_idxSimilar_3() { return static_cast<int32_t>(offsetof(XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0, ___idxSimilar_3)); }
	inline uint16_t get_idxSimilar_3() const { return ___idxSimilar_3; }
	inline uint16_t* get_address_of_idxSimilar_3() { return &___idxSimilar_3; }
	inline void set_idxSimilar_3(uint16_t value)
	{
		___idxSimilar_3 = value;
	}

	inline static int32_t get_offset_of_posOffset_4() { return static_cast<int32_t>(offsetof(XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0, ___posOffset_4)); }
	inline uint16_t get_posOffset_4() const { return ___posOffset_4; }
	inline uint16_t* get_address_of_posOffset_4() { return &___posOffset_4; }
	inline void set_posOffset_4(uint16_t value)
	{
		___posOffset_4 = value;
	}

	inline static int32_t get_offset_of_props_5() { return static_cast<int32_t>(offsetof(XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0, ___props_5)); }
	inline uint32_t get_props_5() const { return ___props_5; }
	inline uint32_t* get_address_of_props_5() { return &___props_5; }
	inline void set_props_5(uint32_t value)
	{
		___props_5 = value;
	}

	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0, ___value_6)); }
	inline String_t* get_value_6() const { return ___value_6; }
	inline String_t** get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(String_t* value)
	{
		___value_6 = value;
		Il2CppCodeGenWriteBarrier((&___value_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MS.Internal.Xml.Cache.XPathNode
struct XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0_marshaled_pinvoke
{
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7 * ___info_0;
	uint16_t ___idxSibling_1;
	uint16_t ___idxParent_2;
	uint16_t ___idxSimilar_3;
	uint16_t ___posOffset_4;
	uint32_t ___props_5;
	char* ___value_6;
};
// Native definition for COM marshalling of MS.Internal.Xml.Cache.XPathNode
struct XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0_marshaled_com
{
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7 * ___info_0;
	uint16_t ___idxSibling_1;
	uint16_t ___idxParent_2;
	uint16_t ___idxSimilar_3;
	uint16_t ___posOffset_4;
	uint32_t ___props_5;
	Il2CppChar* ___value_6;
};
#endif // XPATHNODE_TC207ED6C653E80055FE6C5ECD3E6137A326659A0_H
#ifndef XPATHNODEREF_T6F631244BF7B58CE7DB9239662B4EE745CD54E14_H
#define XPATHNODEREF_T6F631244BF7B58CE7DB9239662B4EE745CD54E14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodeRef
struct  XPathNodeRef_t6F631244BF7B58CE7DB9239662B4EE745CD54E14 
{
public:
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeRef::page
	XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* ___page_0;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeRef::idx
	int32_t ___idx_1;

public:
	inline static int32_t get_offset_of_page_0() { return static_cast<int32_t>(offsetof(XPathNodeRef_t6F631244BF7B58CE7DB9239662B4EE745CD54E14, ___page_0)); }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* get_page_0() const { return ___page_0; }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B** get_address_of_page_0() { return &___page_0; }
	inline void set_page_0(XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* value)
	{
		___page_0 = value;
		Il2CppCodeGenWriteBarrier((&___page_0), value);
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(XPathNodeRef_t6F631244BF7B58CE7DB9239662B4EE745CD54E14, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MS.Internal.Xml.Cache.XPathNodeRef
struct XPathNodeRef_t6F631244BF7B58CE7DB9239662B4EE745CD54E14_marshaled_pinvoke
{
	XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0_marshaled_pinvoke* ___page_0;
	int32_t ___idx_1;
};
// Native definition for COM marshalling of MS.Internal.Xml.Cache.XPathNodeRef
struct XPathNodeRef_t6F631244BF7B58CE7DB9239662B4EE745CD54E14_marshaled_com
{
	XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0_marshaled_com* ___page_0;
	int32_t ___idx_1;
};
#endif // XPATHNODEREF_T6F631244BF7B58CE7DB9239662B4EE745CD54E14_H
#ifndef ROOT_TE82700519CE1726D106D4076E62825DA615058BA_H
#define ROOT_TE82700519CE1726D106D4076E62825DA615058BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Root
struct  Root_tE82700519CE1726D106D4076E62825DA615058BA  : public AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOT_TE82700519CE1726D106D4076E62825DA615058BA_H
#ifndef VARIABLE_T18C81156491F857437BADE92E874B26983F85E25_H
#define VARIABLE_T18C81156491F857437BADE92E874B26983F85E25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Variable
struct  Variable_t18C81156491F857437BADE92E874B26983F85E25  : public AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C
{
public:
	// System.String MS.Internal.Xml.XPath.Variable::localname
	String_t* ___localname_0;
	// System.String MS.Internal.Xml.XPath.Variable::prefix
	String_t* ___prefix_1;

public:
	inline static int32_t get_offset_of_localname_0() { return static_cast<int32_t>(offsetof(Variable_t18C81156491F857437BADE92E874B26983F85E25, ___localname_0)); }
	inline String_t* get_localname_0() const { return ___localname_0; }
	inline String_t** get_address_of_localname_0() { return &___localname_0; }
	inline void set_localname_0(String_t* value)
	{
		___localname_0 = value;
		Il2CppCodeGenWriteBarrier((&___localname_0), value);
	}

	inline static int32_t get_offset_of_prefix_1() { return static_cast<int32_t>(offsetof(Variable_t18C81156491F857437BADE92E874B26983F85E25, ___prefix_1)); }
	inline String_t* get_prefix_1() const { return ___prefix_1; }
	inline String_t** get_address_of_prefix_1() { return &___prefix_1; }
	inline void set_prefix_1(String_t* value)
	{
		___prefix_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLE_T18C81156491F857437BADE92E874B26983F85E25_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#define INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MONOTODOATTRIBUTE_T93F6D8CCDF532E8867F424F9A3B05BA28A7152C3_H
#define MONOTODOATTRIBUTE_T93F6D8CCDF532E8867F424F9A3B05BA28A7152C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoTODOAttribute
struct  MonoTODOAttribute_t93F6D8CCDF532E8867F424F9A3B05BA28A7152C3  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTODOATTRIBUTE_T93F6D8CCDF532E8867F424F9A3B05BA28A7152C3_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef BINHEXDECODER_T2CCB202E9FC5A9055FB0B5945E193EA98E6C3EBB_H
#define BINHEXDECODER_T2CCB202E9FC5A9055FB0B5945E193EA98E6C3EBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinHexDecoder
struct  BinHexDecoder_t2CCB202E9FC5A9055FB0B5945E193EA98E6C3EBB  : public IncrementalReadDecoder_t787BFB5889B01B88DDA030C503A0C2E0525CA723
{
public:
	// System.Byte[] System.Xml.BinHexDecoder::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_0;
	// System.Int32 System.Xml.BinHexDecoder::curIndex
	int32_t ___curIndex_1;
	// System.Int32 System.Xml.BinHexDecoder::endIndex
	int32_t ___endIndex_2;
	// System.Boolean System.Xml.BinHexDecoder::hasHalfByteCached
	bool ___hasHalfByteCached_3;
	// System.Byte System.Xml.BinHexDecoder::cachedHalfByte
	uint8_t ___cachedHalfByte_4;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(BinHexDecoder_t2CCB202E9FC5A9055FB0B5945E193EA98E6C3EBB, ___buffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_0() const { return ___buffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_curIndex_1() { return static_cast<int32_t>(offsetof(BinHexDecoder_t2CCB202E9FC5A9055FB0B5945E193EA98E6C3EBB, ___curIndex_1)); }
	inline int32_t get_curIndex_1() const { return ___curIndex_1; }
	inline int32_t* get_address_of_curIndex_1() { return &___curIndex_1; }
	inline void set_curIndex_1(int32_t value)
	{
		___curIndex_1 = value;
	}

	inline static int32_t get_offset_of_endIndex_2() { return static_cast<int32_t>(offsetof(BinHexDecoder_t2CCB202E9FC5A9055FB0B5945E193EA98E6C3EBB, ___endIndex_2)); }
	inline int32_t get_endIndex_2() const { return ___endIndex_2; }
	inline int32_t* get_address_of_endIndex_2() { return &___endIndex_2; }
	inline void set_endIndex_2(int32_t value)
	{
		___endIndex_2 = value;
	}

	inline static int32_t get_offset_of_hasHalfByteCached_3() { return static_cast<int32_t>(offsetof(BinHexDecoder_t2CCB202E9FC5A9055FB0B5945E193EA98E6C3EBB, ___hasHalfByteCached_3)); }
	inline bool get_hasHalfByteCached_3() const { return ___hasHalfByteCached_3; }
	inline bool* get_address_of_hasHalfByteCached_3() { return &___hasHalfByteCached_3; }
	inline void set_hasHalfByteCached_3(bool value)
	{
		___hasHalfByteCached_3 = value;
	}

	inline static int32_t get_offset_of_cachedHalfByte_4() { return static_cast<int32_t>(offsetof(BinHexDecoder_t2CCB202E9FC5A9055FB0B5945E193EA98E6C3EBB, ___cachedHalfByte_4)); }
	inline uint8_t get_cachedHalfByte_4() const { return ___cachedHalfByte_4; }
	inline uint8_t* get_address_of_cachedHalfByte_4() { return &___cachedHalfByte_4; }
	inline void set_cachedHalfByte_4(uint8_t value)
	{
		___cachedHalfByte_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINHEXDECODER_T2CCB202E9FC5A9055FB0B5945E193EA98E6C3EBB_H
#ifndef BINXMLSQLDECIMAL_T09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_H
#define BINXMLSQLDECIMAL_T09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinXmlSqlDecimal
struct  BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A 
{
public:
	// System.Byte System.Xml.BinXmlSqlDecimal::m_bLen
	uint8_t ___m_bLen_0;
	// System.Byte System.Xml.BinXmlSqlDecimal::m_bPrec
	uint8_t ___m_bPrec_1;
	// System.Byte System.Xml.BinXmlSqlDecimal::m_bScale
	uint8_t ___m_bScale_2;
	// System.Byte System.Xml.BinXmlSqlDecimal::m_bSign
	uint8_t ___m_bSign_3;
	// System.UInt32 System.Xml.BinXmlSqlDecimal::m_data1
	uint32_t ___m_data1_4;
	// System.UInt32 System.Xml.BinXmlSqlDecimal::m_data2
	uint32_t ___m_data2_5;
	// System.UInt32 System.Xml.BinXmlSqlDecimal::m_data3
	uint32_t ___m_data3_6;
	// System.UInt32 System.Xml.BinXmlSqlDecimal::m_data4
	uint32_t ___m_data4_7;

public:
	inline static int32_t get_offset_of_m_bLen_0() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A, ___m_bLen_0)); }
	inline uint8_t get_m_bLen_0() const { return ___m_bLen_0; }
	inline uint8_t* get_address_of_m_bLen_0() { return &___m_bLen_0; }
	inline void set_m_bLen_0(uint8_t value)
	{
		___m_bLen_0 = value;
	}

	inline static int32_t get_offset_of_m_bPrec_1() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A, ___m_bPrec_1)); }
	inline uint8_t get_m_bPrec_1() const { return ___m_bPrec_1; }
	inline uint8_t* get_address_of_m_bPrec_1() { return &___m_bPrec_1; }
	inline void set_m_bPrec_1(uint8_t value)
	{
		___m_bPrec_1 = value;
	}

	inline static int32_t get_offset_of_m_bScale_2() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A, ___m_bScale_2)); }
	inline uint8_t get_m_bScale_2() const { return ___m_bScale_2; }
	inline uint8_t* get_address_of_m_bScale_2() { return &___m_bScale_2; }
	inline void set_m_bScale_2(uint8_t value)
	{
		___m_bScale_2 = value;
	}

	inline static int32_t get_offset_of_m_bSign_3() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A, ___m_bSign_3)); }
	inline uint8_t get_m_bSign_3() const { return ___m_bSign_3; }
	inline uint8_t* get_address_of_m_bSign_3() { return &___m_bSign_3; }
	inline void set_m_bSign_3(uint8_t value)
	{
		___m_bSign_3 = value;
	}

	inline static int32_t get_offset_of_m_data1_4() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A, ___m_data1_4)); }
	inline uint32_t get_m_data1_4() const { return ___m_data1_4; }
	inline uint32_t* get_address_of_m_data1_4() { return &___m_data1_4; }
	inline void set_m_data1_4(uint32_t value)
	{
		___m_data1_4 = value;
	}

	inline static int32_t get_offset_of_m_data2_5() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A, ___m_data2_5)); }
	inline uint32_t get_m_data2_5() const { return ___m_data2_5; }
	inline uint32_t* get_address_of_m_data2_5() { return &___m_data2_5; }
	inline void set_m_data2_5(uint32_t value)
	{
		___m_data2_5 = value;
	}

	inline static int32_t get_offset_of_m_data3_6() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A, ___m_data3_6)); }
	inline uint32_t get_m_data3_6() const { return ___m_data3_6; }
	inline uint32_t* get_address_of_m_data3_6() { return &___m_data3_6; }
	inline void set_m_data3_6(uint32_t value)
	{
		___m_data3_6 = value;
	}

	inline static int32_t get_offset_of_m_data4_7() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A, ___m_data4_7)); }
	inline uint32_t get_m_data4_7() const { return ___m_data4_7; }
	inline uint32_t* get_address_of_m_data4_7() { return &___m_data4_7; }
	inline void set_m_data4_7(uint32_t value)
	{
		___m_data4_7 = value;
	}
};

struct BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields
{
public:
	// System.Byte System.Xml.BinXmlSqlDecimal::NUMERIC_MAX_PRECISION
	uint8_t ___NUMERIC_MAX_PRECISION_8;
	// System.Byte System.Xml.BinXmlSqlDecimal::MaxPrecision
	uint8_t ___MaxPrecision_9;
	// System.Byte System.Xml.BinXmlSqlDecimal::MaxScale
	uint8_t ___MaxScale_10;
	// System.Int32 System.Xml.BinXmlSqlDecimal::x_cNumeMax
	int32_t ___x_cNumeMax_11;
	// System.Int64 System.Xml.BinXmlSqlDecimal::x_lInt32Base
	int64_t ___x_lInt32Base_12;
	// System.UInt64 System.Xml.BinXmlSqlDecimal::x_ulInt32Base
	uint64_t ___x_ulInt32Base_13;
	// System.UInt64 System.Xml.BinXmlSqlDecimal::x_ulInt32BaseForMod
	uint64_t ___x_ulInt32BaseForMod_14;
	// System.UInt64 System.Xml.BinXmlSqlDecimal::x_llMax
	uint64_t ___x_llMax_15;
	// System.Double System.Xml.BinXmlSqlDecimal::DUINT_BASE
	double ___DUINT_BASE_16;
	// System.Double System.Xml.BinXmlSqlDecimal::DUINT_BASE2
	double ___DUINT_BASE2_17;
	// System.Double System.Xml.BinXmlSqlDecimal::DUINT_BASE3
	double ___DUINT_BASE3_18;
	// System.UInt32[] System.Xml.BinXmlSqlDecimal::x_rgulShiftBase
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_rgulShiftBase_19;
	// System.Byte[] System.Xml.BinXmlSqlDecimal::rgCLenFromPrec
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___rgCLenFromPrec_20;

public:
	inline static int32_t get_offset_of_NUMERIC_MAX_PRECISION_8() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields, ___NUMERIC_MAX_PRECISION_8)); }
	inline uint8_t get_NUMERIC_MAX_PRECISION_8() const { return ___NUMERIC_MAX_PRECISION_8; }
	inline uint8_t* get_address_of_NUMERIC_MAX_PRECISION_8() { return &___NUMERIC_MAX_PRECISION_8; }
	inline void set_NUMERIC_MAX_PRECISION_8(uint8_t value)
	{
		___NUMERIC_MAX_PRECISION_8 = value;
	}

	inline static int32_t get_offset_of_MaxPrecision_9() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields, ___MaxPrecision_9)); }
	inline uint8_t get_MaxPrecision_9() const { return ___MaxPrecision_9; }
	inline uint8_t* get_address_of_MaxPrecision_9() { return &___MaxPrecision_9; }
	inline void set_MaxPrecision_9(uint8_t value)
	{
		___MaxPrecision_9 = value;
	}

	inline static int32_t get_offset_of_MaxScale_10() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields, ___MaxScale_10)); }
	inline uint8_t get_MaxScale_10() const { return ___MaxScale_10; }
	inline uint8_t* get_address_of_MaxScale_10() { return &___MaxScale_10; }
	inline void set_MaxScale_10(uint8_t value)
	{
		___MaxScale_10 = value;
	}

	inline static int32_t get_offset_of_x_cNumeMax_11() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields, ___x_cNumeMax_11)); }
	inline int32_t get_x_cNumeMax_11() const { return ___x_cNumeMax_11; }
	inline int32_t* get_address_of_x_cNumeMax_11() { return &___x_cNumeMax_11; }
	inline void set_x_cNumeMax_11(int32_t value)
	{
		___x_cNumeMax_11 = value;
	}

	inline static int32_t get_offset_of_x_lInt32Base_12() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields, ___x_lInt32Base_12)); }
	inline int64_t get_x_lInt32Base_12() const { return ___x_lInt32Base_12; }
	inline int64_t* get_address_of_x_lInt32Base_12() { return &___x_lInt32Base_12; }
	inline void set_x_lInt32Base_12(int64_t value)
	{
		___x_lInt32Base_12 = value;
	}

	inline static int32_t get_offset_of_x_ulInt32Base_13() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields, ___x_ulInt32Base_13)); }
	inline uint64_t get_x_ulInt32Base_13() const { return ___x_ulInt32Base_13; }
	inline uint64_t* get_address_of_x_ulInt32Base_13() { return &___x_ulInt32Base_13; }
	inline void set_x_ulInt32Base_13(uint64_t value)
	{
		___x_ulInt32Base_13 = value;
	}

	inline static int32_t get_offset_of_x_ulInt32BaseForMod_14() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields, ___x_ulInt32BaseForMod_14)); }
	inline uint64_t get_x_ulInt32BaseForMod_14() const { return ___x_ulInt32BaseForMod_14; }
	inline uint64_t* get_address_of_x_ulInt32BaseForMod_14() { return &___x_ulInt32BaseForMod_14; }
	inline void set_x_ulInt32BaseForMod_14(uint64_t value)
	{
		___x_ulInt32BaseForMod_14 = value;
	}

	inline static int32_t get_offset_of_x_llMax_15() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields, ___x_llMax_15)); }
	inline uint64_t get_x_llMax_15() const { return ___x_llMax_15; }
	inline uint64_t* get_address_of_x_llMax_15() { return &___x_llMax_15; }
	inline void set_x_llMax_15(uint64_t value)
	{
		___x_llMax_15 = value;
	}

	inline static int32_t get_offset_of_DUINT_BASE_16() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields, ___DUINT_BASE_16)); }
	inline double get_DUINT_BASE_16() const { return ___DUINT_BASE_16; }
	inline double* get_address_of_DUINT_BASE_16() { return &___DUINT_BASE_16; }
	inline void set_DUINT_BASE_16(double value)
	{
		___DUINT_BASE_16 = value;
	}

	inline static int32_t get_offset_of_DUINT_BASE2_17() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields, ___DUINT_BASE2_17)); }
	inline double get_DUINT_BASE2_17() const { return ___DUINT_BASE2_17; }
	inline double* get_address_of_DUINT_BASE2_17() { return &___DUINT_BASE2_17; }
	inline void set_DUINT_BASE2_17(double value)
	{
		___DUINT_BASE2_17 = value;
	}

	inline static int32_t get_offset_of_DUINT_BASE3_18() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields, ___DUINT_BASE3_18)); }
	inline double get_DUINT_BASE3_18() const { return ___DUINT_BASE3_18; }
	inline double* get_address_of_DUINT_BASE3_18() { return &___DUINT_BASE3_18; }
	inline void set_DUINT_BASE3_18(double value)
	{
		___DUINT_BASE3_18 = value;
	}

	inline static int32_t get_offset_of_x_rgulShiftBase_19() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields, ___x_rgulShiftBase_19)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_rgulShiftBase_19() const { return ___x_rgulShiftBase_19; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_rgulShiftBase_19() { return &___x_rgulShiftBase_19; }
	inline void set_x_rgulShiftBase_19(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_rgulShiftBase_19 = value;
		Il2CppCodeGenWriteBarrier((&___x_rgulShiftBase_19), value);
	}

	inline static int32_t get_offset_of_rgCLenFromPrec_20() { return static_cast<int32_t>(offsetof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields, ___rgCLenFromPrec_20)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_rgCLenFromPrec_20() const { return ___rgCLenFromPrec_20; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_rgCLenFromPrec_20() { return &___rgCLenFromPrec_20; }
	inline void set_rgCLenFromPrec_20(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___rgCLenFromPrec_20 = value;
		Il2CppCodeGenWriteBarrier((&___rgCLenFromPrec_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINXMLSQLDECIMAL_T09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_H
#ifndef BINXMLSQLMONEY_TAD1ACE807DEA1505FC669E2DD513D272E2E66EEC_H
#define BINXMLSQLMONEY_TAD1ACE807DEA1505FC669E2DD513D272E2E66EEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinXmlSqlMoney
struct  BinXmlSqlMoney_tAD1ACE807DEA1505FC669E2DD513D272E2E66EEC 
{
public:
	// System.Int64 System.Xml.BinXmlSqlMoney::data
	int64_t ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(BinXmlSqlMoney_tAD1ACE807DEA1505FC669E2DD513D272E2E66EEC, ___data_0)); }
	inline int64_t get_data_0() const { return ___data_0; }
	inline int64_t* get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(int64_t value)
	{
		___data_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINXMLSQLMONEY_TAD1ACE807DEA1505FC669E2DD513D272E2E66EEC_H
#ifndef CHARENTITYENCODERFALLBACK_TC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE_H
#define CHARENTITYENCODERFALLBACK_TC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.CharEntityEncoderFallback
struct  CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE  : public EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63
{
public:
	// System.Xml.CharEntityEncoderFallbackBuffer System.Xml.CharEntityEncoderFallback::fallbackBuffer
	CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8 * ___fallbackBuffer_4;
	// System.Int32[] System.Xml.CharEntityEncoderFallback::textContentMarks
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___textContentMarks_5;
	// System.Int32 System.Xml.CharEntityEncoderFallback::endMarkPos
	int32_t ___endMarkPos_6;
	// System.Int32 System.Xml.CharEntityEncoderFallback::curMarkPos
	int32_t ___curMarkPos_7;
	// System.Int32 System.Xml.CharEntityEncoderFallback::startOffset
	int32_t ___startOffset_8;

public:
	inline static int32_t get_offset_of_fallbackBuffer_4() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE, ___fallbackBuffer_4)); }
	inline CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8 * get_fallbackBuffer_4() const { return ___fallbackBuffer_4; }
	inline CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8 ** get_address_of_fallbackBuffer_4() { return &___fallbackBuffer_4; }
	inline void set_fallbackBuffer_4(CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8 * value)
	{
		___fallbackBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackBuffer_4), value);
	}

	inline static int32_t get_offset_of_textContentMarks_5() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE, ___textContentMarks_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_textContentMarks_5() const { return ___textContentMarks_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_textContentMarks_5() { return &___textContentMarks_5; }
	inline void set_textContentMarks_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___textContentMarks_5 = value;
		Il2CppCodeGenWriteBarrier((&___textContentMarks_5), value);
	}

	inline static int32_t get_offset_of_endMarkPos_6() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE, ___endMarkPos_6)); }
	inline int32_t get_endMarkPos_6() const { return ___endMarkPos_6; }
	inline int32_t* get_address_of_endMarkPos_6() { return &___endMarkPos_6; }
	inline void set_endMarkPos_6(int32_t value)
	{
		___endMarkPos_6 = value;
	}

	inline static int32_t get_offset_of_curMarkPos_7() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE, ___curMarkPos_7)); }
	inline int32_t get_curMarkPos_7() const { return ___curMarkPos_7; }
	inline int32_t* get_address_of_curMarkPos_7() { return &___curMarkPos_7; }
	inline void set_curMarkPos_7(int32_t value)
	{
		___curMarkPos_7 = value;
	}

	inline static int32_t get_offset_of_startOffset_8() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE, ___startOffset_8)); }
	inline int32_t get_startOffset_8() const { return ___startOffset_8; }
	inline int32_t* get_address_of_startOffset_8() { return &___startOffset_8; }
	inline void set_startOffset_8(int32_t value)
	{
		___startOffset_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARENTITYENCODERFALLBACK_TC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE_H
#ifndef CHARENTITYENCODERFALLBACKBUFFER_TD634B3F88428F39172AB3EBDF8759B1B24D3C2D8_H
#define CHARENTITYENCODERFALLBACKBUFFER_TD634B3F88428F39172AB3EBDF8759B1B24D3C2D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.CharEntityEncoderFallbackBuffer
struct  CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8  : public EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C
{
public:
	// System.Xml.CharEntityEncoderFallback System.Xml.CharEntityEncoderFallbackBuffer::parent
	CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE * ___parent_7;
	// System.String System.Xml.CharEntityEncoderFallbackBuffer::charEntity
	String_t* ___charEntity_8;
	// System.Int32 System.Xml.CharEntityEncoderFallbackBuffer::charEntityIndex
	int32_t ___charEntityIndex_9;

public:
	inline static int32_t get_offset_of_parent_7() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8, ___parent_7)); }
	inline CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE * get_parent_7() const { return ___parent_7; }
	inline CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE ** get_address_of_parent_7() { return &___parent_7; }
	inline void set_parent_7(CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE * value)
	{
		___parent_7 = value;
		Il2CppCodeGenWriteBarrier((&___parent_7), value);
	}

	inline static int32_t get_offset_of_charEntity_8() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8, ___charEntity_8)); }
	inline String_t* get_charEntity_8() const { return ___charEntity_8; }
	inline String_t** get_address_of_charEntity_8() { return &___charEntity_8; }
	inline void set_charEntity_8(String_t* value)
	{
		___charEntity_8 = value;
		Il2CppCodeGenWriteBarrier((&___charEntity_8), value);
	}

	inline static int32_t get_offset_of_charEntityIndex_9() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8, ___charEntityIndex_9)); }
	inline int32_t get_charEntityIndex_9() const { return ___charEntityIndex_9; }
	inline int32_t* get_address_of_charEntityIndex_9() { return &___charEntityIndex_9; }
	inline void set_charEntityIndex_9(int32_t value)
	{
		___charEntityIndex_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARENTITYENCODERFALLBACKBUFFER_TD634B3F88428F39172AB3EBDF8759B1B24D3C2D8_H
#ifndef INCREMENTALREADDUMMYDECODER_T64BDE5CCA28D8D8979B7683C594FC2A85DC70663_H
#define INCREMENTALREADDUMMYDECODER_T64BDE5CCA28D8D8979B7683C594FC2A85DC70663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.IncrementalReadDummyDecoder
struct  IncrementalReadDummyDecoder_t64BDE5CCA28D8D8979B7683C594FC2A85DC70663  : public IncrementalReadDecoder_t787BFB5889B01B88DDA030C503A0C2E0525CA723
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INCREMENTALREADDUMMYDECODER_T64BDE5CCA28D8D8979B7683C594FC2A85DC70663_H
#ifndef LINEINFO_T7E3D50496C7BA51B84D485D0A30B9006943544E5_H
#define LINEINFO_T7E3D50496C7BA51B84D485D0A30B9006943544E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.LineInfo
struct  LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5 
{
public:
	// System.Int32 System.Xml.LineInfo::lineNo
	int32_t ___lineNo_0;
	// System.Int32 System.Xml.LineInfo::linePos
	int32_t ___linePos_1;

public:
	inline static int32_t get_offset_of_lineNo_0() { return static_cast<int32_t>(offsetof(LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5, ___lineNo_0)); }
	inline int32_t get_lineNo_0() const { return ___lineNo_0; }
	inline int32_t* get_address_of_lineNo_0() { return &___lineNo_0; }
	inline void set_lineNo_0(int32_t value)
	{
		___lineNo_0 = value;
	}

	inline static int32_t get_offset_of_linePos_1() { return static_cast<int32_t>(offsetof(LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5, ___linePos_1)); }
	inline int32_t get_linePos_1() const { return ___linePos_1; }
	inline int32_t* get_address_of_linePos_1() { return &___linePos_1; }
	inline void set_linePos_1(int32_t value)
	{
		___linePos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEINFO_T7E3D50496C7BA51B84D485D0A30B9006943544E5_H
#ifndef XPATHNAVIGATOR_T4E711E4382A896C152FA99D0D39A8C6CEC238EC3_H
#define XPATHNAVIGATOR_T4E711E4382A896C152FA99D0D39A8C6CEC238EC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigator
struct  XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3  : public XPathItem_tCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98
{
public:

public:
};

struct XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3_StaticFields
{
public:
	// System.Xml.XPath.XPathNavigatorKeyComparer System.Xml.XPath.XPathNavigator::comparer
	XPathNavigatorKeyComparer_t6A0E82BEC0BE42351DDB26EAA86333C11E0A9378 * ___comparer_0;
	// System.Char[] System.Xml.XPath.XPathNavigator::NodeTypeLetter
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___NodeTypeLetter_1;
	// System.Char[] System.Xml.XPath.XPathNavigator::UniqueIdTbl
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___UniqueIdTbl_2;
	// System.Int32[] System.Xml.XPath.XPathNavigator::ContentKindMasks
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ContentKindMasks_3;

public:
	inline static int32_t get_offset_of_comparer_0() { return static_cast<int32_t>(offsetof(XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3_StaticFields, ___comparer_0)); }
	inline XPathNavigatorKeyComparer_t6A0E82BEC0BE42351DDB26EAA86333C11E0A9378 * get_comparer_0() const { return ___comparer_0; }
	inline XPathNavigatorKeyComparer_t6A0E82BEC0BE42351DDB26EAA86333C11E0A9378 ** get_address_of_comparer_0() { return &___comparer_0; }
	inline void set_comparer_0(XPathNavigatorKeyComparer_t6A0E82BEC0BE42351DDB26EAA86333C11E0A9378 * value)
	{
		___comparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_0), value);
	}

	inline static int32_t get_offset_of_NodeTypeLetter_1() { return static_cast<int32_t>(offsetof(XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3_StaticFields, ___NodeTypeLetter_1)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_NodeTypeLetter_1() const { return ___NodeTypeLetter_1; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_NodeTypeLetter_1() { return &___NodeTypeLetter_1; }
	inline void set_NodeTypeLetter_1(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___NodeTypeLetter_1 = value;
		Il2CppCodeGenWriteBarrier((&___NodeTypeLetter_1), value);
	}

	inline static int32_t get_offset_of_UniqueIdTbl_2() { return static_cast<int32_t>(offsetof(XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3_StaticFields, ___UniqueIdTbl_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_UniqueIdTbl_2() const { return ___UniqueIdTbl_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_UniqueIdTbl_2() { return &___UniqueIdTbl_2; }
	inline void set_UniqueIdTbl_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___UniqueIdTbl_2 = value;
		Il2CppCodeGenWriteBarrier((&___UniqueIdTbl_2), value);
	}

	inline static int32_t get_offset_of_ContentKindMasks_3() { return static_cast<int32_t>(offsetof(XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3_StaticFields, ___ContentKindMasks_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ContentKindMasks_3() const { return ___ContentKindMasks_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ContentKindMasks_3() { return &___ContentKindMasks_3; }
	inline void set_ContentKindMasks_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ContentKindMasks_3 = value;
		Il2CppCodeGenWriteBarrier((&___ContentKindMasks_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAVIGATOR_T4E711E4382A896C152FA99D0D39A8C6CEC238EC3_H
#ifndef XMLASYNCCHECKREADER_T2D4FE06C976E98C24DDD496E1E2323ECAB8E6968_H
#define XMLASYNCCHECKREADER_T2D4FE06C976E98C24DDD496E1E2323ECAB8E6968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAsyncCheckReader
struct  XmlAsyncCheckReader_t2D4FE06C976E98C24DDD496E1E2323ECAB8E6968  : public XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB
{
public:
	// System.Xml.XmlReader System.Xml.XmlAsyncCheckReader::coreReader
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * ___coreReader_3;
	// System.Threading.Tasks.Task System.Xml.XmlAsyncCheckReader::lastTask
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___lastTask_4;

public:
	inline static int32_t get_offset_of_coreReader_3() { return static_cast<int32_t>(offsetof(XmlAsyncCheckReader_t2D4FE06C976E98C24DDD496E1E2323ECAB8E6968, ___coreReader_3)); }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * get_coreReader_3() const { return ___coreReader_3; }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB ** get_address_of_coreReader_3() { return &___coreReader_3; }
	inline void set_coreReader_3(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * value)
	{
		___coreReader_3 = value;
		Il2CppCodeGenWriteBarrier((&___coreReader_3), value);
	}

	inline static int32_t get_offset_of_lastTask_4() { return static_cast<int32_t>(offsetof(XmlAsyncCheckReader_t2D4FE06C976E98C24DDD496E1E2323ECAB8E6968, ___lastTask_4)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_lastTask_4() const { return ___lastTask_4; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_lastTask_4() { return &___lastTask_4; }
	inline void set_lastTask_4(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___lastTask_4 = value;
		Il2CppCodeGenWriteBarrier((&___lastTask_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLASYNCCHECKREADER_T2D4FE06C976E98C24DDD496E1E2323ECAB8E6968_H
#ifndef XMLASYNCCHECKWRITER_T33C862CF7ABB0B088B0476A97B43E1D97119DC23_H
#define XMLASYNCCHECKWRITER_T33C862CF7ABB0B088B0476A97B43E1D97119DC23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAsyncCheckWriter
struct  XmlAsyncCheckWriter_t33C862CF7ABB0B088B0476A97B43E1D97119DC23  : public XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869
{
public:
	// System.Xml.XmlWriter System.Xml.XmlAsyncCheckWriter::coreWriter
	XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * ___coreWriter_1;
	// System.Threading.Tasks.Task System.Xml.XmlAsyncCheckWriter::lastTask
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___lastTask_2;

public:
	inline static int32_t get_offset_of_coreWriter_1() { return static_cast<int32_t>(offsetof(XmlAsyncCheckWriter_t33C862CF7ABB0B088B0476A97B43E1D97119DC23, ___coreWriter_1)); }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * get_coreWriter_1() const { return ___coreWriter_1; }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 ** get_address_of_coreWriter_1() { return &___coreWriter_1; }
	inline void set_coreWriter_1(XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * value)
	{
		___coreWriter_1 = value;
		Il2CppCodeGenWriteBarrier((&___coreWriter_1), value);
	}

	inline static int32_t get_offset_of_lastTask_2() { return static_cast<int32_t>(offsetof(XmlAsyncCheckWriter_t33C862CF7ABB0B088B0476A97B43E1D97119DC23, ___lastTask_2)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_lastTask_2() const { return ___lastTask_2; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_lastTask_2() { return &___lastTask_2; }
	inline void set_lastTask_2(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___lastTask_2 = value;
		Il2CppCodeGenWriteBarrier((&___lastTask_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLASYNCCHECKWRITER_T33C862CF7ABB0B088B0476A97B43E1D97119DC23_H
#ifndef XMLCHARTYPE_T7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_H
#define XMLCHARTYPE_T7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharType
struct  XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 
{
public:
	// System.Byte[] System.Xml.XmlCharType::charProperties
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___charProperties_2;

public:
	inline static int32_t get_offset_of_charProperties_2() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9, ___charProperties_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_charProperties_2() const { return ___charProperties_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_charProperties_2() { return &___charProperties_2; }
	inline void set_charProperties_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___charProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___charProperties_2), value);
	}
};

struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields
{
public:
	// System.Object System.Xml.XmlCharType::s_Lock
	RuntimeObject * ___s_Lock_0;
	// System.Byte[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlCharType::s_CharProperties
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___s_CharProperties_1;

public:
	inline static int32_t get_offset_of_s_Lock_0() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields, ___s_Lock_0)); }
	inline RuntimeObject * get_s_Lock_0() const { return ___s_Lock_0; }
	inline RuntimeObject ** get_address_of_s_Lock_0() { return &___s_Lock_0; }
	inline void set_s_Lock_0(RuntimeObject * value)
	{
		___s_Lock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Lock_0), value);
	}

	inline static int32_t get_offset_of_s_CharProperties_1() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields, ___s_CharProperties_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_s_CharProperties_1() const { return ___s_CharProperties_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_s_CharProperties_1() { return &___s_CharProperties_1; }
	inline void set_s_CharProperties_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___s_CharProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CharProperties_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlCharType
struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_pinvoke
{
	uint8_t* ___charProperties_2;
};
// Native definition for COM marshalling of System.Xml.XmlCharType
struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_com
{
	uint8_t* ___charProperties_2;
};
#endif // XMLCHARTYPE_T7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_H
#ifndef XMLRAWWRITER_T96129C955F861EC1DDA80EEABE73BB794D617FF2_H
#define XMLRAWWRITER_T96129C955F861EC1DDA80EEABE73BB794D617FF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlRawWriter
struct  XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2  : public XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869
{
public:
	// System.Xml.XmlRawWriterBase64Encoder System.Xml.XmlRawWriter::base64Encoder
	XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678 * ___base64Encoder_1;
	// System.Xml.IXmlNamespaceResolver System.Xml.XmlRawWriter::resolver
	RuntimeObject* ___resolver_2;

public:
	inline static int32_t get_offset_of_base64Encoder_1() { return static_cast<int32_t>(offsetof(XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2, ___base64Encoder_1)); }
	inline XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678 * get_base64Encoder_1() const { return ___base64Encoder_1; }
	inline XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678 ** get_address_of_base64Encoder_1() { return &___base64Encoder_1; }
	inline void set_base64Encoder_1(XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678 * value)
	{
		___base64Encoder_1 = value;
		Il2CppCodeGenWriteBarrier((&___base64Encoder_1), value);
	}

	inline static int32_t get_offset_of_resolver_2() { return static_cast<int32_t>(offsetof(XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2, ___resolver_2)); }
	inline RuntimeObject* get_resolver_2() const { return ___resolver_2; }
	inline RuntimeObject** get_address_of_resolver_2() { return &___resolver_2; }
	inline void set_resolver_2(RuntimeObject* value)
	{
		___resolver_2 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLRAWWRITER_T96129C955F861EC1DDA80EEABE73BB794D617FF2_H
#ifndef XMLRAWWRITERBASE64ENCODER_TD74EC123571C47ABE46E0B6CCE61964E30695678_H
#define XMLRAWWRITERBASE64ENCODER_TD74EC123571C47ABE46E0B6CCE61964E30695678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlRawWriterBase64Encoder
struct  XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678  : public Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922
{
public:
	// System.Xml.XmlRawWriter System.Xml.XmlRawWriterBase64Encoder::rawWriter
	XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * ___rawWriter_3;

public:
	inline static int32_t get_offset_of_rawWriter_3() { return static_cast<int32_t>(offsetof(XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678, ___rawWriter_3)); }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * get_rawWriter_3() const { return ___rawWriter_3; }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 ** get_address_of_rawWriter_3() { return &___rawWriter_3; }
	inline void set_rawWriter_3(XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * value)
	{
		___rawWriter_3 = value;
		Il2CppCodeGenWriteBarrier((&___rawWriter_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLRAWWRITERBASE64ENCODER_TD74EC123571C47ABE46E0B6CCE61964E30695678_H
#ifndef QNAME_T49332A07486EFE325DF0D3F34BE798ED3497C42F_H
#define QNAME_T49332A07486EFE325DF0D3F34BE798ED3497C42F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSqlBinaryReader/QName
struct  QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F 
{
public:
	// System.String System.Xml.XmlSqlBinaryReader/QName::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlSqlBinaryReader/QName::localname
	String_t* ___localname_1;
	// System.String System.Xml.XmlSqlBinaryReader/QName::namespaceUri
	String_t* ___namespaceUri_2;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_localname_1() { return static_cast<int32_t>(offsetof(QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F, ___localname_1)); }
	inline String_t* get_localname_1() const { return ___localname_1; }
	inline String_t** get_address_of_localname_1() { return &___localname_1; }
	inline void set_localname_1(String_t* value)
	{
		___localname_1 = value;
		Il2CppCodeGenWriteBarrier((&___localname_1), value);
	}

	inline static int32_t get_offset_of_namespaceUri_2() { return static_cast<int32_t>(offsetof(QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F, ___namespaceUri_2)); }
	inline String_t* get_namespaceUri_2() const { return ___namespaceUri_2; }
	inline String_t** get_address_of_namespaceUri_2() { return &___namespaceUri_2; }
	inline void set_namespaceUri_2(String_t* value)
	{
		___namespaceUri_2 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlSqlBinaryReader/QName
struct QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F_marshaled_pinvoke
{
	char* ___prefix_0;
	char* ___localname_1;
	char* ___namespaceUri_2;
};
// Native definition for COM marshalling of System.Xml.XmlSqlBinaryReader/QName
struct QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F_marshaled_com
{
	Il2CppChar* ___prefix_0;
	Il2CppChar* ___localname_1;
	Il2CppChar* ___namespaceUri_2;
};
#endif // QNAME_T49332A07486EFE325DF0D3F34BE798ED3497C42F_H
#ifndef SYMBOLTABLES_T8F49FC5423EF06561EBAF65E6130FA03C025325E_H
#define SYMBOLTABLES_T8F49FC5423EF06561EBAF65E6130FA03C025325E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSqlBinaryReader/SymbolTables
struct  SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E 
{
public:
	// System.String[] System.Xml.XmlSqlBinaryReader/SymbolTables::symtable
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___symtable_0;
	// System.Int32 System.Xml.XmlSqlBinaryReader/SymbolTables::symCount
	int32_t ___symCount_1;
	// System.Xml.XmlSqlBinaryReader/QName[] System.Xml.XmlSqlBinaryReader/SymbolTables::qnametable
	QNameU5BU5D_t4006F29C3E0B63E7B8B049A5F88FFDE77D9B2AB5* ___qnametable_2;
	// System.Int32 System.Xml.XmlSqlBinaryReader/SymbolTables::qnameCount
	int32_t ___qnameCount_3;

public:
	inline static int32_t get_offset_of_symtable_0() { return static_cast<int32_t>(offsetof(SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E, ___symtable_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_symtable_0() const { return ___symtable_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_symtable_0() { return &___symtable_0; }
	inline void set_symtable_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___symtable_0 = value;
		Il2CppCodeGenWriteBarrier((&___symtable_0), value);
	}

	inline static int32_t get_offset_of_symCount_1() { return static_cast<int32_t>(offsetof(SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E, ___symCount_1)); }
	inline int32_t get_symCount_1() const { return ___symCount_1; }
	inline int32_t* get_address_of_symCount_1() { return &___symCount_1; }
	inline void set_symCount_1(int32_t value)
	{
		___symCount_1 = value;
	}

	inline static int32_t get_offset_of_qnametable_2() { return static_cast<int32_t>(offsetof(SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E, ___qnametable_2)); }
	inline QNameU5BU5D_t4006F29C3E0B63E7B8B049A5F88FFDE77D9B2AB5* get_qnametable_2() const { return ___qnametable_2; }
	inline QNameU5BU5D_t4006F29C3E0B63E7B8B049A5F88FFDE77D9B2AB5** get_address_of_qnametable_2() { return &___qnametable_2; }
	inline void set_qnametable_2(QNameU5BU5D_t4006F29C3E0B63E7B8B049A5F88FFDE77D9B2AB5* value)
	{
		___qnametable_2 = value;
		Il2CppCodeGenWriteBarrier((&___qnametable_2), value);
	}

	inline static int32_t get_offset_of_qnameCount_3() { return static_cast<int32_t>(offsetof(SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E, ___qnameCount_3)); }
	inline int32_t get_qnameCount_3() const { return ___qnameCount_3; }
	inline int32_t* get_address_of_qnameCount_3() { return &___qnameCount_3; }
	inline void set_qnameCount_3(int32_t value)
	{
		___qnameCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlSqlBinaryReader/SymbolTables
struct SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E_marshaled_pinvoke
{
	char** ___symtable_0;
	int32_t ___symCount_1;
	QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F_marshaled_pinvoke* ___qnametable_2;
	int32_t ___qnameCount_3;
};
// Native definition for COM marshalling of System.Xml.XmlSqlBinaryReader/SymbolTables
struct SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E_marshaled_com
{
	Il2CppChar** ___symtable_0;
	int32_t ___symCount_1;
	QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F_marshaled_com* ___qnametable_2;
	int32_t ___qnameCount_3;
};
#endif // SYMBOLTABLES_T8F49FC5423EF06561EBAF65E6130FA03C025325E_H
#ifndef XMLTEXTREADER_TAF28DD94DDC4A59EE85A627A606C9347C8149A8C_H
#define XMLTEXTREADER_TAF28DD94DDC4A59EE85A627A606C9347C8149A8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReader
struct  XmlTextReader_tAF28DD94DDC4A59EE85A627A606C9347C8149A8C  : public XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB
{
public:
	// System.Xml.XmlTextReaderImpl System.Xml.XmlTextReader::impl
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61 * ___impl_3;

public:
	inline static int32_t get_offset_of_impl_3() { return static_cast<int32_t>(offsetof(XmlTextReader_tAF28DD94DDC4A59EE85A627A606C9347C8149A8C, ___impl_3)); }
	inline XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61 * get_impl_3() const { return ___impl_3; }
	inline XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61 ** get_address_of_impl_3() { return &___impl_3; }
	inline void set_impl_3(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61 * value)
	{
		___impl_3 = value;
		Il2CppCodeGenWriteBarrier((&___impl_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTREADER_TAF28DD94DDC4A59EE85A627A606C9347C8149A8C_H
#ifndef PARSINGSTATE_TE4A8E7F14B2068AE43ECF99F81F55B0301A551A2_H
#define PARSINGSTATE_TE4A8E7F14B2068AE43ECF99F81F55B0301A551A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/ParsingState
struct  ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2 
{
public:
	// System.Char[] System.Xml.XmlTextReaderImpl/ParsingState::chars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___chars_0;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::charPos
	int32_t ___charPos_1;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::charsUsed
	int32_t ___charsUsed_2;
	// System.Text.Encoding System.Xml.XmlTextReaderImpl/ParsingState::encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_3;
	// System.Boolean System.Xml.XmlTextReaderImpl/ParsingState::appendMode
	bool ___appendMode_4;
	// System.IO.Stream System.Xml.XmlTextReaderImpl/ParsingState::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	// System.Text.Decoder System.Xml.XmlTextReaderImpl/ParsingState::decoder
	Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * ___decoder_6;
	// System.Byte[] System.Xml.XmlTextReaderImpl/ParsingState::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_7;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::bytePos
	int32_t ___bytePos_8;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::bytesUsed
	int32_t ___bytesUsed_9;
	// System.IO.TextReader System.Xml.XmlTextReaderImpl/ParsingState::textReader
	TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * ___textReader_10;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::lineNo
	int32_t ___lineNo_11;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::lineStartPos
	int32_t ___lineStartPos_12;
	// System.String System.Xml.XmlTextReaderImpl/ParsingState::baseUriStr
	String_t* ___baseUriStr_13;
	// System.Uri System.Xml.XmlTextReaderImpl/ParsingState::baseUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___baseUri_14;
	// System.Boolean System.Xml.XmlTextReaderImpl/ParsingState::isEof
	bool ___isEof_15;
	// System.Boolean System.Xml.XmlTextReaderImpl/ParsingState::isStreamEof
	bool ___isStreamEof_16;
	// System.Xml.IDtdEntityInfo System.Xml.XmlTextReaderImpl/ParsingState::entity
	RuntimeObject* ___entity_17;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::entityId
	int32_t ___entityId_18;
	// System.Boolean System.Xml.XmlTextReaderImpl/ParsingState::eolNormalized
	bool ___eolNormalized_19;
	// System.Boolean System.Xml.XmlTextReaderImpl/ParsingState::entityResolvedManually
	bool ___entityResolvedManually_20;

public:
	inline static int32_t get_offset_of_chars_0() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___chars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_chars_0() const { return ___chars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_chars_0() { return &___chars_0; }
	inline void set_chars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___chars_0 = value;
		Il2CppCodeGenWriteBarrier((&___chars_0), value);
	}

	inline static int32_t get_offset_of_charPos_1() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___charPos_1)); }
	inline int32_t get_charPos_1() const { return ___charPos_1; }
	inline int32_t* get_address_of_charPos_1() { return &___charPos_1; }
	inline void set_charPos_1(int32_t value)
	{
		___charPos_1 = value;
	}

	inline static int32_t get_offset_of_charsUsed_2() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___charsUsed_2)); }
	inline int32_t get_charsUsed_2() const { return ___charsUsed_2; }
	inline int32_t* get_address_of_charsUsed_2() { return &___charsUsed_2; }
	inline void set_charsUsed_2(int32_t value)
	{
		___charsUsed_2 = value;
	}

	inline static int32_t get_offset_of_encoding_3() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___encoding_3)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_encoding_3() const { return ___encoding_3; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_encoding_3() { return &___encoding_3; }
	inline void set_encoding_3(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_3), value);
	}

	inline static int32_t get_offset_of_appendMode_4() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___appendMode_4)); }
	inline bool get_appendMode_4() const { return ___appendMode_4; }
	inline bool* get_address_of_appendMode_4() { return &___appendMode_4; }
	inline void set_appendMode_4(bool value)
	{
		___appendMode_4 = value;
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___stream_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_5() const { return ___stream_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_decoder_6() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___decoder_6)); }
	inline Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * get_decoder_6() const { return ___decoder_6; }
	inline Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 ** get_address_of_decoder_6() { return &___decoder_6; }
	inline void set_decoder_6(Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * value)
	{
		___decoder_6 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_6), value);
	}

	inline static int32_t get_offset_of_bytes_7() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___bytes_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_7() const { return ___bytes_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_7() { return &___bytes_7; }
	inline void set_bytes_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_7 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_7), value);
	}

	inline static int32_t get_offset_of_bytePos_8() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___bytePos_8)); }
	inline int32_t get_bytePos_8() const { return ___bytePos_8; }
	inline int32_t* get_address_of_bytePos_8() { return &___bytePos_8; }
	inline void set_bytePos_8(int32_t value)
	{
		___bytePos_8 = value;
	}

	inline static int32_t get_offset_of_bytesUsed_9() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___bytesUsed_9)); }
	inline int32_t get_bytesUsed_9() const { return ___bytesUsed_9; }
	inline int32_t* get_address_of_bytesUsed_9() { return &___bytesUsed_9; }
	inline void set_bytesUsed_9(int32_t value)
	{
		___bytesUsed_9 = value;
	}

	inline static int32_t get_offset_of_textReader_10() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___textReader_10)); }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * get_textReader_10() const { return ___textReader_10; }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A ** get_address_of_textReader_10() { return &___textReader_10; }
	inline void set_textReader_10(TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * value)
	{
		___textReader_10 = value;
		Il2CppCodeGenWriteBarrier((&___textReader_10), value);
	}

	inline static int32_t get_offset_of_lineNo_11() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___lineNo_11)); }
	inline int32_t get_lineNo_11() const { return ___lineNo_11; }
	inline int32_t* get_address_of_lineNo_11() { return &___lineNo_11; }
	inline void set_lineNo_11(int32_t value)
	{
		___lineNo_11 = value;
	}

	inline static int32_t get_offset_of_lineStartPos_12() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___lineStartPos_12)); }
	inline int32_t get_lineStartPos_12() const { return ___lineStartPos_12; }
	inline int32_t* get_address_of_lineStartPos_12() { return &___lineStartPos_12; }
	inline void set_lineStartPos_12(int32_t value)
	{
		___lineStartPos_12 = value;
	}

	inline static int32_t get_offset_of_baseUriStr_13() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___baseUriStr_13)); }
	inline String_t* get_baseUriStr_13() const { return ___baseUriStr_13; }
	inline String_t** get_address_of_baseUriStr_13() { return &___baseUriStr_13; }
	inline void set_baseUriStr_13(String_t* value)
	{
		___baseUriStr_13 = value;
		Il2CppCodeGenWriteBarrier((&___baseUriStr_13), value);
	}

	inline static int32_t get_offset_of_baseUri_14() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___baseUri_14)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_baseUri_14() const { return ___baseUri_14; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_baseUri_14() { return &___baseUri_14; }
	inline void set_baseUri_14(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___baseUri_14 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_14), value);
	}

	inline static int32_t get_offset_of_isEof_15() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___isEof_15)); }
	inline bool get_isEof_15() const { return ___isEof_15; }
	inline bool* get_address_of_isEof_15() { return &___isEof_15; }
	inline void set_isEof_15(bool value)
	{
		___isEof_15 = value;
	}

	inline static int32_t get_offset_of_isStreamEof_16() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___isStreamEof_16)); }
	inline bool get_isStreamEof_16() const { return ___isStreamEof_16; }
	inline bool* get_address_of_isStreamEof_16() { return &___isStreamEof_16; }
	inline void set_isStreamEof_16(bool value)
	{
		___isStreamEof_16 = value;
	}

	inline static int32_t get_offset_of_entity_17() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___entity_17)); }
	inline RuntimeObject* get_entity_17() const { return ___entity_17; }
	inline RuntimeObject** get_address_of_entity_17() { return &___entity_17; }
	inline void set_entity_17(RuntimeObject* value)
	{
		___entity_17 = value;
		Il2CppCodeGenWriteBarrier((&___entity_17), value);
	}

	inline static int32_t get_offset_of_entityId_18() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___entityId_18)); }
	inline int32_t get_entityId_18() const { return ___entityId_18; }
	inline int32_t* get_address_of_entityId_18() { return &___entityId_18; }
	inline void set_entityId_18(int32_t value)
	{
		___entityId_18 = value;
	}

	inline static int32_t get_offset_of_eolNormalized_19() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___eolNormalized_19)); }
	inline bool get_eolNormalized_19() const { return ___eolNormalized_19; }
	inline bool* get_address_of_eolNormalized_19() { return &___eolNormalized_19; }
	inline void set_eolNormalized_19(bool value)
	{
		___eolNormalized_19 = value;
	}

	inline static int32_t get_offset_of_entityResolvedManually_20() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___entityResolvedManually_20)); }
	inline bool get_entityResolvedManually_20() const { return ___entityResolvedManually_20; }
	inline bool* get_address_of_entityResolvedManually_20() { return &___entityResolvedManually_20; }
	inline void set_entityResolvedManually_20(bool value)
	{
		___entityResolvedManually_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlTextReaderImpl/ParsingState
struct ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2_marshaled_pinvoke
{
	uint8_t* ___chars_0;
	int32_t ___charPos_1;
	int32_t ___charsUsed_2;
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_3;
	int32_t ___appendMode_4;
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * ___decoder_6;
	uint8_t* ___bytes_7;
	int32_t ___bytePos_8;
	int32_t ___bytesUsed_9;
	TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * ___textReader_10;
	int32_t ___lineNo_11;
	int32_t ___lineStartPos_12;
	char* ___baseUriStr_13;
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___baseUri_14;
	int32_t ___isEof_15;
	int32_t ___isStreamEof_16;
	RuntimeObject* ___entity_17;
	int32_t ___entityId_18;
	int32_t ___eolNormalized_19;
	int32_t ___entityResolvedManually_20;
};
// Native definition for COM marshalling of System.Xml.XmlTextReaderImpl/ParsingState
struct ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2_marshaled_com
{
	uint8_t* ___chars_0;
	int32_t ___charPos_1;
	int32_t ___charsUsed_2;
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_3;
	int32_t ___appendMode_4;
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * ___decoder_6;
	uint8_t* ___bytes_7;
	int32_t ___bytePos_8;
	int32_t ___bytesUsed_9;
	TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * ___textReader_10;
	int32_t ___lineNo_11;
	int32_t ___lineStartPos_12;
	Il2CppChar* ___baseUriStr_13;
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___baseUri_14;
	int32_t ___isEof_15;
	int32_t ___isStreamEof_16;
	RuntimeObject* ___entity_17;
	int32_t ___entityId_18;
	int32_t ___eolNormalized_19;
	int32_t ___entityResolvedManually_20;
};
#endif // PARSINGSTATE_TE4A8E7F14B2068AE43ECF99F81F55B0301A551A2_H
#ifndef XMLTEXTWRITERBASE64ENCODER_T1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C_H
#define XMLTEXTWRITERBASE64ENCODER_T1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriterBase64Encoder
struct  XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C  : public Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922
{
public:
	// System.Xml.XmlTextEncoder System.Xml.XmlTextWriterBase64Encoder::xmlTextEncoder
	XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * ___xmlTextEncoder_3;

public:
	inline static int32_t get_offset_of_xmlTextEncoder_3() { return static_cast<int32_t>(offsetof(XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C, ___xmlTextEncoder_3)); }
	inline XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * get_xmlTextEncoder_3() const { return ___xmlTextEncoder_3; }
	inline XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 ** get_address_of_xmlTextEncoder_3() { return &___xmlTextEncoder_3; }
	inline void set_xmlTextEncoder_3(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * value)
	{
		___xmlTextEncoder_3 = value;
		Il2CppCodeGenWriteBarrier((&___xmlTextEncoder_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTWRITERBASE64ENCODER_T1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C_H
#ifndef STRINGCONCAT_T506391CC6FC326F2114A013043BA36C3066B5C43_H
#define STRINGCONCAT_T506391CC6FC326F2114A013043BA36C3066B5C43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Xsl.Runtime.StringConcat
struct  StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43 
{
public:
	// System.String System.Xml.Xsl.Runtime.StringConcat::s1
	String_t* ___s1_0;
	// System.String System.Xml.Xsl.Runtime.StringConcat::s2
	String_t* ___s2_1;
	// System.String System.Xml.Xsl.Runtime.StringConcat::s3
	String_t* ___s3_2;
	// System.String System.Xml.Xsl.Runtime.StringConcat::s4
	String_t* ___s4_3;
	// System.String System.Xml.Xsl.Runtime.StringConcat::delimiter
	String_t* ___delimiter_4;
	// System.Collections.Generic.List`1<System.String> System.Xml.Xsl.Runtime.StringConcat::strList
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___strList_5;
	// System.Int32 System.Xml.Xsl.Runtime.StringConcat::idxStr
	int32_t ___idxStr_6;

public:
	inline static int32_t get_offset_of_s1_0() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___s1_0)); }
	inline String_t* get_s1_0() const { return ___s1_0; }
	inline String_t** get_address_of_s1_0() { return &___s1_0; }
	inline void set_s1_0(String_t* value)
	{
		___s1_0 = value;
		Il2CppCodeGenWriteBarrier((&___s1_0), value);
	}

	inline static int32_t get_offset_of_s2_1() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___s2_1)); }
	inline String_t* get_s2_1() const { return ___s2_1; }
	inline String_t** get_address_of_s2_1() { return &___s2_1; }
	inline void set_s2_1(String_t* value)
	{
		___s2_1 = value;
		Il2CppCodeGenWriteBarrier((&___s2_1), value);
	}

	inline static int32_t get_offset_of_s3_2() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___s3_2)); }
	inline String_t* get_s3_2() const { return ___s3_2; }
	inline String_t** get_address_of_s3_2() { return &___s3_2; }
	inline void set_s3_2(String_t* value)
	{
		___s3_2 = value;
		Il2CppCodeGenWriteBarrier((&___s3_2), value);
	}

	inline static int32_t get_offset_of_s4_3() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___s4_3)); }
	inline String_t* get_s4_3() const { return ___s4_3; }
	inline String_t** get_address_of_s4_3() { return &___s4_3; }
	inline void set_s4_3(String_t* value)
	{
		___s4_3 = value;
		Il2CppCodeGenWriteBarrier((&___s4_3), value);
	}

	inline static int32_t get_offset_of_delimiter_4() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___delimiter_4)); }
	inline String_t* get_delimiter_4() const { return ___delimiter_4; }
	inline String_t** get_address_of_delimiter_4() { return &___delimiter_4; }
	inline void set_delimiter_4(String_t* value)
	{
		___delimiter_4 = value;
		Il2CppCodeGenWriteBarrier((&___delimiter_4), value);
	}

	inline static int32_t get_offset_of_strList_5() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___strList_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_strList_5() const { return ___strList_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_strList_5() { return &___strList_5; }
	inline void set_strList_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___strList_5 = value;
		Il2CppCodeGenWriteBarrier((&___strList_5), value);
	}

	inline static int32_t get_offset_of_idxStr_6() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___idxStr_6)); }
	inline int32_t get_idxStr_6() const { return ___idxStr_6; }
	inline int32_t* get_address_of_idxStr_6() { return &___idxStr_6; }
	inline void set_idxStr_6(int32_t value)
	{
		___idxStr_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Xsl.Runtime.StringConcat
struct StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43_marshaled_pinvoke
{
	char* ___s1_0;
	char* ___s2_1;
	char* ___s3_2;
	char* ___s4_3;
	char* ___delimiter_4;
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___strList_5;
	int32_t ___idxStr_6;
};
// Native definition for COM marshalling of System.Xml.Xsl.Runtime.StringConcat
struct StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43_marshaled_com
{
	Il2CppChar* ___s1_0;
	Il2CppChar* ___s2_1;
	Il2CppChar* ___s3_2;
	Il2CppChar* ___s4_3;
	Il2CppChar* ___delimiter_4;
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___strList_5;
	int32_t ___idxStr_6;
};
#endif // STRINGCONCAT_T506391CC6FC326F2114A013043BA36C3066B5C43_H
#ifndef XPATHDOCUMENTNAVIGATOR_T4EA718BDD563CB09DE0E93644B2E3FECC8B6B498_H
#define XPATHDOCUMENTNAVIGATOR_T4EA718BDD563CB09DE0E93644B2E3FECC8B6B498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathDocumentNavigator
struct  XPathDocumentNavigator_t4EA718BDD563CB09DE0E93644B2E3FECC8B6B498  : public XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3
{
public:
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathDocumentNavigator::pageCurrent
	XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* ___pageCurrent_4;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathDocumentNavigator::pageParent
	XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* ___pageParent_5;
	// System.Int32 MS.Internal.Xml.Cache.XPathDocumentNavigator::idxCurrent
	int32_t ___idxCurrent_6;
	// System.Int32 MS.Internal.Xml.Cache.XPathDocumentNavigator::idxParent
	int32_t ___idxParent_7;

public:
	inline static int32_t get_offset_of_pageCurrent_4() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t4EA718BDD563CB09DE0E93644B2E3FECC8B6B498, ___pageCurrent_4)); }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* get_pageCurrent_4() const { return ___pageCurrent_4; }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B** get_address_of_pageCurrent_4() { return &___pageCurrent_4; }
	inline void set_pageCurrent_4(XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* value)
	{
		___pageCurrent_4 = value;
		Il2CppCodeGenWriteBarrier((&___pageCurrent_4), value);
	}

	inline static int32_t get_offset_of_pageParent_5() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t4EA718BDD563CB09DE0E93644B2E3FECC8B6B498, ___pageParent_5)); }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* get_pageParent_5() const { return ___pageParent_5; }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B** get_address_of_pageParent_5() { return &___pageParent_5; }
	inline void set_pageParent_5(XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* value)
	{
		___pageParent_5 = value;
		Il2CppCodeGenWriteBarrier((&___pageParent_5), value);
	}

	inline static int32_t get_offset_of_idxCurrent_6() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t4EA718BDD563CB09DE0E93644B2E3FECC8B6B498, ___idxCurrent_6)); }
	inline int32_t get_idxCurrent_6() const { return ___idxCurrent_6; }
	inline int32_t* get_address_of_idxCurrent_6() { return &___idxCurrent_6; }
	inline void set_idxCurrent_6(int32_t value)
	{
		___idxCurrent_6 = value;
	}

	inline static int32_t get_offset_of_idxParent_7() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t4EA718BDD563CB09DE0E93644B2E3FECC8B6B498, ___idxParent_7)); }
	inline int32_t get_idxParent_7() const { return ___idxParent_7; }
	inline int32_t* get_address_of_idxParent_7() { return &___idxParent_7; }
	inline void set_idxParent_7(int32_t value)
	{
		___idxParent_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHDOCUMENTNAVIGATOR_T4EA718BDD563CB09DE0E93644B2E3FECC8B6B498_H
#ifndef FUNCTIONTYPE_T68B7C1F2B2C83325C06D04D0542F334304BA7A9D_H
#define FUNCTIONTYPE_T68B7C1F2B2C83325C06D04D0542F334304BA7A9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Function/FunctionType
struct  FunctionType_t68B7C1F2B2C83325C06D04D0542F334304BA7A9D 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.Function/FunctionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FunctionType_t68B7C1F2B2C83325C06D04D0542F334304BA7A9D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONTYPE_T68B7C1F2B2C83325C06D04D0542F334304BA7A9D_H
#ifndef OP_TCCFB3D07967B00ECC946757A2AFE63466C34F976_H
#define OP_TCCFB3D07967B00ECC946757A2AFE63466C34F976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Operator/Op
struct  Op_tCCFB3D07967B00ECC946757A2AFE63466C34F976 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.Operator/Op::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Op_tCCFB3D07967B00ECC946757A2AFE63466C34F976, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OP_TCCFB3D07967B00ECC946757A2AFE63466C34F976_H
#ifndef LEXKIND_T12FE968EFB884A0346676DCECA65929ED89C10E4_H
#define LEXKIND_T12FE968EFB884A0346676DCECA65929ED89C10E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.XPathScanner/LexKind
struct  LexKind_t12FE968EFB884A0346676DCECA65929ED89C10E4 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.XPathScanner/LexKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LexKind_t12FE968EFB884A0346676DCECA65929ED89C10E4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEXKIND_T12FE968EFB884A0346676DCECA65929ED89C10E4_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef ATTRIBUTEPROPERTIES_TFB77E07917EF07DD10F6FBC47F7FB0E07F0A0C2F_H
#define ATTRIBUTEPROPERTIES_TFB77E07917EF07DD10F6FBC47F7FB0E07F0A0C2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.AttributeProperties
struct  AttributeProperties_tFB77E07917EF07DD10F6FBC47F7FB0E07F0A0C2F 
{
public:
	// System.UInt32 System.Xml.AttributeProperties::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeProperties_tFB77E07917EF07DD10F6FBC47F7FB0E07F0A0C2F, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEPROPERTIES_TFB77E07917EF07DD10F6FBC47F7FB0E07F0A0C2F_H
#ifndef BINXMLTOKEN_T592F736A62786D804D147AA2E468F02DB3FCCF39_H
#define BINXMLTOKEN_T592F736A62786D804D147AA2E468F02DB3FCCF39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinXmlToken
struct  BinXmlToken_t592F736A62786D804D147AA2E468F02DB3FCCF39 
{
public:
	// System.Int32 System.Xml.BinXmlToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BinXmlToken_t592F736A62786D804D147AA2E468F02DB3FCCF39, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINXMLTOKEN_T592F736A62786D804D147AA2E468F02DB3FCCF39_H
#ifndef CONFORMANCELEVEL_T42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A_H
#define CONFORMANCELEVEL_T42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ConformanceLevel
struct  ConformanceLevel_t42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A 
{
public:
	// System.Int32 System.Xml.ConformanceLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConformanceLevel_t42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFORMANCELEVEL_T42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A_H
#ifndef DTDPROCESSING_TAB3B800A5365ED9C5841D71F40E5A38840D32DB3_H
#define DTDPROCESSING_TAB3B800A5365ED9C5841D71F40E5A38840D32DB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdProcessing
struct  DtdProcessing_tAB3B800A5365ED9C5841D71F40E5A38840D32DB3 
{
public:
	// System.Int32 System.Xml.DtdProcessing::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DtdProcessing_tAB3B800A5365ED9C5841D71F40E5A38840D32DB3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPROCESSING_TAB3B800A5365ED9C5841D71F40E5A38840D32DB3_H
#ifndef ELEMENTPROPERTIES_TFC06E5E2552285434B447CACE58013EA7EE5AF8E_H
#define ELEMENTPROPERTIES_TFC06E5E2552285434B447CACE58013EA7EE5AF8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ElementProperties
struct  ElementProperties_tFC06E5E2552285434B447CACE58013EA7EE5AF8E 
{
public:
	// System.UInt32 System.Xml.ElementProperties::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ElementProperties_tFC06E5E2552285434B447CACE58013EA7EE5AF8E, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTPROPERTIES_TFC06E5E2552285434B447CACE58013EA7EE5AF8E_H
#ifndef ENTITYHANDLING_T15C89E916C1AC46126DCF896A6317CE364B8F89B_H
#define ENTITYHANDLING_T15C89E916C1AC46126DCF896A6317CE364B8F89B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.EntityHandling
struct  EntityHandling_t15C89E916C1AC46126DCF896A6317CE364B8F89B 
{
public:
	// System.Int32 System.Xml.EntityHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EntityHandling_t15C89E916C1AC46126DCF896A6317CE364B8F89B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYHANDLING_T15C89E916C1AC46126DCF896A6317CE364B8F89B_H
#ifndef NAMESPACEHANDLING_TECE89F8A3E0CF300C5A18E41FBC65494C7EB3618_H
#define NAMESPACEHANDLING_TECE89F8A3E0CF300C5A18E41FBC65494C7EB3618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NamespaceHandling
struct  NamespaceHandling_tECE89F8A3E0CF300C5A18E41FBC65494C7EB3618 
{
public:
	// System.Int32 System.Xml.NamespaceHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NamespaceHandling_tECE89F8A3E0CF300C5A18E41FBC65494C7EB3618, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEHANDLING_TECE89F8A3E0CF300C5A18E41FBC65494C7EB3618_H
#ifndef NEWLINEHANDLING_T35189DCBC088E592125C4F2FF4EDD3DB9F594543_H
#define NEWLINEHANDLING_T35189DCBC088E592125C4F2FF4EDD3DB9F594543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NewLineHandling
struct  NewLineHandling_t35189DCBC088E592125C4F2FF4EDD3DB9F594543 
{
public:
	// System.Int32 System.Xml.NewLineHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NewLineHandling_t35189DCBC088E592125C4F2FF4EDD3DB9F594543, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWLINEHANDLING_T35189DCBC088E592125C4F2FF4EDD3DB9F594543_H
#ifndef QUERYOUTPUTWRITER_TACE01C408AB8433A13AAD333AF266EF4563FA009_H
#define QUERYOUTPUTWRITER_TACE01C408AB8433A13AAD333AF266EF4563FA009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.QueryOutputWriter
struct  QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009  : public XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2
{
public:
	// System.Xml.XmlRawWriter System.Xml.QueryOutputWriter::wrapped
	XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * ___wrapped_3;
	// System.Boolean System.Xml.QueryOutputWriter::inCDataSection
	bool ___inCDataSection_4;
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Int32> System.Xml.QueryOutputWriter::lookupCDataElems
	Dictionary_2_tD3EC27D7F99969DB9B3CBEC094139F683A730F43 * ___lookupCDataElems_5;
	// System.Xml.BitStack System.Xml.QueryOutputWriter::bitsCData
	BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * ___bitsCData_6;
	// System.Xml.XmlQualifiedName System.Xml.QueryOutputWriter::qnameCData
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___qnameCData_7;
	// System.Boolean System.Xml.QueryOutputWriter::outputDocType
	bool ___outputDocType_8;
	// System.Boolean System.Xml.QueryOutputWriter::checkWellFormedDoc
	bool ___checkWellFormedDoc_9;
	// System.Boolean System.Xml.QueryOutputWriter::hasDocElem
	bool ___hasDocElem_10;
	// System.Boolean System.Xml.QueryOutputWriter::inAttr
	bool ___inAttr_11;
	// System.String System.Xml.QueryOutputWriter::systemId
	String_t* ___systemId_12;
	// System.String System.Xml.QueryOutputWriter::publicId
	String_t* ___publicId_13;
	// System.Int32 System.Xml.QueryOutputWriter::depth
	int32_t ___depth_14;

public:
	inline static int32_t get_offset_of_wrapped_3() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___wrapped_3)); }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * get_wrapped_3() const { return ___wrapped_3; }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 ** get_address_of_wrapped_3() { return &___wrapped_3; }
	inline void set_wrapped_3(XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * value)
	{
		___wrapped_3 = value;
		Il2CppCodeGenWriteBarrier((&___wrapped_3), value);
	}

	inline static int32_t get_offset_of_inCDataSection_4() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___inCDataSection_4)); }
	inline bool get_inCDataSection_4() const { return ___inCDataSection_4; }
	inline bool* get_address_of_inCDataSection_4() { return &___inCDataSection_4; }
	inline void set_inCDataSection_4(bool value)
	{
		___inCDataSection_4 = value;
	}

	inline static int32_t get_offset_of_lookupCDataElems_5() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___lookupCDataElems_5)); }
	inline Dictionary_2_tD3EC27D7F99969DB9B3CBEC094139F683A730F43 * get_lookupCDataElems_5() const { return ___lookupCDataElems_5; }
	inline Dictionary_2_tD3EC27D7F99969DB9B3CBEC094139F683A730F43 ** get_address_of_lookupCDataElems_5() { return &___lookupCDataElems_5; }
	inline void set_lookupCDataElems_5(Dictionary_2_tD3EC27D7F99969DB9B3CBEC094139F683A730F43 * value)
	{
		___lookupCDataElems_5 = value;
		Il2CppCodeGenWriteBarrier((&___lookupCDataElems_5), value);
	}

	inline static int32_t get_offset_of_bitsCData_6() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___bitsCData_6)); }
	inline BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * get_bitsCData_6() const { return ___bitsCData_6; }
	inline BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 ** get_address_of_bitsCData_6() { return &___bitsCData_6; }
	inline void set_bitsCData_6(BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * value)
	{
		___bitsCData_6 = value;
		Il2CppCodeGenWriteBarrier((&___bitsCData_6), value);
	}

	inline static int32_t get_offset_of_qnameCData_7() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___qnameCData_7)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_qnameCData_7() const { return ___qnameCData_7; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_qnameCData_7() { return &___qnameCData_7; }
	inline void set_qnameCData_7(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___qnameCData_7 = value;
		Il2CppCodeGenWriteBarrier((&___qnameCData_7), value);
	}

	inline static int32_t get_offset_of_outputDocType_8() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___outputDocType_8)); }
	inline bool get_outputDocType_8() const { return ___outputDocType_8; }
	inline bool* get_address_of_outputDocType_8() { return &___outputDocType_8; }
	inline void set_outputDocType_8(bool value)
	{
		___outputDocType_8 = value;
	}

	inline static int32_t get_offset_of_checkWellFormedDoc_9() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___checkWellFormedDoc_9)); }
	inline bool get_checkWellFormedDoc_9() const { return ___checkWellFormedDoc_9; }
	inline bool* get_address_of_checkWellFormedDoc_9() { return &___checkWellFormedDoc_9; }
	inline void set_checkWellFormedDoc_9(bool value)
	{
		___checkWellFormedDoc_9 = value;
	}

	inline static int32_t get_offset_of_hasDocElem_10() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___hasDocElem_10)); }
	inline bool get_hasDocElem_10() const { return ___hasDocElem_10; }
	inline bool* get_address_of_hasDocElem_10() { return &___hasDocElem_10; }
	inline void set_hasDocElem_10(bool value)
	{
		___hasDocElem_10 = value;
	}

	inline static int32_t get_offset_of_inAttr_11() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___inAttr_11)); }
	inline bool get_inAttr_11() const { return ___inAttr_11; }
	inline bool* get_address_of_inAttr_11() { return &___inAttr_11; }
	inline void set_inAttr_11(bool value)
	{
		___inAttr_11 = value;
	}

	inline static int32_t get_offset_of_systemId_12() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___systemId_12)); }
	inline String_t* get_systemId_12() const { return ___systemId_12; }
	inline String_t** get_address_of_systemId_12() { return &___systemId_12; }
	inline void set_systemId_12(String_t* value)
	{
		___systemId_12 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_12), value);
	}

	inline static int32_t get_offset_of_publicId_13() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___publicId_13)); }
	inline String_t* get_publicId_13() const { return ___publicId_13; }
	inline String_t** get_address_of_publicId_13() { return &___publicId_13; }
	inline void set_publicId_13(String_t* value)
	{
		___publicId_13 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_13), value);
	}

	inline static int32_t get_offset_of_depth_14() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___depth_14)); }
	inline int32_t get_depth_14() const { return ___depth_14; }
	inline int32_t* get_address_of_depth_14() { return &___depth_14; }
	inline void set_depth_14(int32_t value)
	{
		___depth_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYOUTPUTWRITER_TACE01C408AB8433A13AAD333AF266EF4563FA009_H
#ifndef STATE_T0C92915E8C4E902724D043F981062725DE46FEAA_H
#define STATE_T0C92915E8C4E902724D043F981062725DE46FEAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReadContentAsBinaryHelper/State
struct  State_t0C92915E8C4E902724D043F981062725DE46FEAA 
{
public:
	// System.Int32 System.Xml.ReadContentAsBinaryHelper/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t0C92915E8C4E902724D043F981062725DE46FEAA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T0C92915E8C4E902724D043F981062725DE46FEAA_H
#ifndef READSTATE_TAAF15D8DE96B6A22379D2B6FEF22764640D05DD0_H
#define READSTATE_TAAF15D8DE96B6A22379D2B6FEF22764640D05DD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReadState
struct  ReadState_tAAF15D8DE96B6A22379D2B6FEF22764640D05DD0 
{
public:
	// System.Int32 System.Xml.ReadState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReadState_tAAF15D8DE96B6A22379D2B6FEF22764640D05DD0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTATE_TAAF15D8DE96B6A22379D2B6FEF22764640D05DD0_H
#ifndef XMLSCHEMAVALIDATIONFLAGS_T866C695263C623EE4F1746A541248AE12D311A73_H
#define XMLSCHEMAVALIDATIONFLAGS_T866C695263C623EE4F1746A541248AE12D311A73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidationFlags
struct  XmlSchemaValidationFlags_t866C695263C623EE4F1746A541248AE12D311A73 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidationFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaValidationFlags_t866C695263C623EE4F1746A541248AE12D311A73, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATIONFLAGS_T866C695263C623EE4F1746A541248AE12D311A73_H
#ifndef VALIDATIONTYPE_TFA1BE5AAA395D6B8033755CD823243B8FAC2673E_H
#define VALIDATIONTYPE_TFA1BE5AAA395D6B8033755CD823243B8FAC2673E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ValidationType
struct  ValidationType_tFA1BE5AAA395D6B8033755CD823243B8FAC2673E 
{
public:
	// System.Int32 System.Xml.ValidationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ValidationType_tFA1BE5AAA395D6B8033755CD823243B8FAC2673E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONTYPE_TFA1BE5AAA395D6B8033755CD823243B8FAC2673E_H
#ifndef WHITESPACEHANDLING_T46A500512D84C27CBFB4580E40C5D583F4B149DC_H
#define WHITESPACEHANDLING_T46A500512D84C27CBFB4580E40C5D583F4B149DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WhitespaceHandling
struct  WhitespaceHandling_t46A500512D84C27CBFB4580E40C5D583F4B149DC 
{
public:
	// System.Int32 System.Xml.WhitespaceHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WhitespaceHandling_t46A500512D84C27CBFB4580E40C5D583F4B149DC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHITESPACEHANDLING_T46A500512D84C27CBFB4580E40C5D583F4B149DC_H
#ifndef XMLASYNCCHECKREADERWITHLINEINFO_TFF91FA10CC8CF8DD006D69CFB36E0DE795F1533B_H
#define XMLASYNCCHECKREADERWITHLINEINFO_TFF91FA10CC8CF8DD006D69CFB36E0DE795F1533B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAsyncCheckReaderWithLineInfo
struct  XmlAsyncCheckReaderWithLineInfo_tFF91FA10CC8CF8DD006D69CFB36E0DE795F1533B  : public XmlAsyncCheckReader_t2D4FE06C976E98C24DDD496E1E2323ECAB8E6968
{
public:
	// System.Xml.IXmlLineInfo System.Xml.XmlAsyncCheckReaderWithLineInfo::readerAsIXmlLineInfo
	RuntimeObject* ___readerAsIXmlLineInfo_5;

public:
	inline static int32_t get_offset_of_readerAsIXmlLineInfo_5() { return static_cast<int32_t>(offsetof(XmlAsyncCheckReaderWithLineInfo_tFF91FA10CC8CF8DD006D69CFB36E0DE795F1533B, ___readerAsIXmlLineInfo_5)); }
	inline RuntimeObject* get_readerAsIXmlLineInfo_5() const { return ___readerAsIXmlLineInfo_5; }
	inline RuntimeObject** get_address_of_readerAsIXmlLineInfo_5() { return &___readerAsIXmlLineInfo_5; }
	inline void set_readerAsIXmlLineInfo_5(RuntimeObject* value)
	{
		___readerAsIXmlLineInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___readerAsIXmlLineInfo_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLASYNCCHECKREADERWITHLINEINFO_TFF91FA10CC8CF8DD006D69CFB36E0DE795F1533B_H
#ifndef XMLASYNCCHECKREADERWITHNS_T52C90781655051B73C07890BE6B1C7327AE8593C_H
#define XMLASYNCCHECKREADERWITHNS_T52C90781655051B73C07890BE6B1C7327AE8593C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAsyncCheckReaderWithNS
struct  XmlAsyncCheckReaderWithNS_t52C90781655051B73C07890BE6B1C7327AE8593C  : public XmlAsyncCheckReader_t2D4FE06C976E98C24DDD496E1E2323ECAB8E6968
{
public:
	// System.Xml.IXmlNamespaceResolver System.Xml.XmlAsyncCheckReaderWithNS::readerAsIXmlNamespaceResolver
	RuntimeObject* ___readerAsIXmlNamespaceResolver_5;

public:
	inline static int32_t get_offset_of_readerAsIXmlNamespaceResolver_5() { return static_cast<int32_t>(offsetof(XmlAsyncCheckReaderWithNS_t52C90781655051B73C07890BE6B1C7327AE8593C, ___readerAsIXmlNamespaceResolver_5)); }
	inline RuntimeObject* get_readerAsIXmlNamespaceResolver_5() const { return ___readerAsIXmlNamespaceResolver_5; }
	inline RuntimeObject** get_address_of_readerAsIXmlNamespaceResolver_5() { return &___readerAsIXmlNamespaceResolver_5; }
	inline void set_readerAsIXmlNamespaceResolver_5(RuntimeObject* value)
	{
		___readerAsIXmlNamespaceResolver_5 = value;
		Il2CppCodeGenWriteBarrier((&___readerAsIXmlNamespaceResolver_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLASYNCCHECKREADERWITHNS_T52C90781655051B73C07890BE6B1C7327AE8593C_H
#ifndef XMLAUTODETECTWRITER_TA120BE82B4500DCB84FB5362B92DB88DA4555C71_H
#define XMLAUTODETECTWRITER_TA120BE82B4500DCB84FB5362B92DB88DA4555C71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAutoDetectWriter
struct  XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71  : public XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2
{
public:
	// System.Xml.XmlRawWriter System.Xml.XmlAutoDetectWriter::wrapped
	XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * ___wrapped_3;
	// System.Xml.OnRemoveWriter System.Xml.XmlAutoDetectWriter::onRemove
	OnRemoveWriter_t40CE623324C30930692639D6172B422C25AA6370 * ___onRemove_4;
	// System.Xml.XmlWriterSettings System.Xml.XmlAutoDetectWriter::writerSettings
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * ___writerSettings_5;
	// System.Xml.XmlEventCache System.Xml.XmlAutoDetectWriter::eventCache
	XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4 * ___eventCache_6;
	// System.IO.TextWriter System.Xml.XmlAutoDetectWriter::textWriter
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___textWriter_7;
	// System.IO.Stream System.Xml.XmlAutoDetectWriter::strm
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___strm_8;

public:
	inline static int32_t get_offset_of_wrapped_3() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71, ___wrapped_3)); }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * get_wrapped_3() const { return ___wrapped_3; }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 ** get_address_of_wrapped_3() { return &___wrapped_3; }
	inline void set_wrapped_3(XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * value)
	{
		___wrapped_3 = value;
		Il2CppCodeGenWriteBarrier((&___wrapped_3), value);
	}

	inline static int32_t get_offset_of_onRemove_4() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71, ___onRemove_4)); }
	inline OnRemoveWriter_t40CE623324C30930692639D6172B422C25AA6370 * get_onRemove_4() const { return ___onRemove_4; }
	inline OnRemoveWriter_t40CE623324C30930692639D6172B422C25AA6370 ** get_address_of_onRemove_4() { return &___onRemove_4; }
	inline void set_onRemove_4(OnRemoveWriter_t40CE623324C30930692639D6172B422C25AA6370 * value)
	{
		___onRemove_4 = value;
		Il2CppCodeGenWriteBarrier((&___onRemove_4), value);
	}

	inline static int32_t get_offset_of_writerSettings_5() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71, ___writerSettings_5)); }
	inline XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * get_writerSettings_5() const { return ___writerSettings_5; }
	inline XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 ** get_address_of_writerSettings_5() { return &___writerSettings_5; }
	inline void set_writerSettings_5(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * value)
	{
		___writerSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&___writerSettings_5), value);
	}

	inline static int32_t get_offset_of_eventCache_6() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71, ___eventCache_6)); }
	inline XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4 * get_eventCache_6() const { return ___eventCache_6; }
	inline XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4 ** get_address_of_eventCache_6() { return &___eventCache_6; }
	inline void set_eventCache_6(XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4 * value)
	{
		___eventCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___eventCache_6), value);
	}

	inline static int32_t get_offset_of_textWriter_7() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71, ___textWriter_7)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get_textWriter_7() const { return ___textWriter_7; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of_textWriter_7() { return &___textWriter_7; }
	inline void set_textWriter_7(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		___textWriter_7 = value;
		Il2CppCodeGenWriteBarrier((&___textWriter_7), value);
	}

	inline static int32_t get_offset_of_strm_8() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71, ___strm_8)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_strm_8() const { return ___strm_8; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_strm_8() { return &___strm_8; }
	inline void set_strm_8(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___strm_8 = value;
		Il2CppCodeGenWriteBarrier((&___strm_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLAUTODETECTWRITER_TA120BE82B4500DCB84FB5362B92DB88DA4555C71_H
#ifndef XMLEVENTCACHE_T4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4_H
#define XMLEVENTCACHE_T4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEventCache
struct  XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4  : public XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2
{
public:
	// System.Collections.Generic.List`1<System.Xml.XmlEventCache/XmlEvent[]> System.Xml.XmlEventCache::pages
	List_1_t08F12CB35B6D2C6405C7227FBB70FC5939090022 * ___pages_3;
	// System.Xml.XmlEventCache/XmlEvent[] System.Xml.XmlEventCache::pageCurr
	XmlEventU5BU5D_tAE240C67B5AAEA739D76EA9A649BEF1153584236* ___pageCurr_4;
	// System.Int32 System.Xml.XmlEventCache::pageSize
	int32_t ___pageSize_5;
	// System.Boolean System.Xml.XmlEventCache::hasRootNode
	bool ___hasRootNode_6;
	// System.Xml.Xsl.Runtime.StringConcat System.Xml.XmlEventCache::singleText
	StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43  ___singleText_7;
	// System.String System.Xml.XmlEventCache::baseUri
	String_t* ___baseUri_8;

public:
	inline static int32_t get_offset_of_pages_3() { return static_cast<int32_t>(offsetof(XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4, ___pages_3)); }
	inline List_1_t08F12CB35B6D2C6405C7227FBB70FC5939090022 * get_pages_3() const { return ___pages_3; }
	inline List_1_t08F12CB35B6D2C6405C7227FBB70FC5939090022 ** get_address_of_pages_3() { return &___pages_3; }
	inline void set_pages_3(List_1_t08F12CB35B6D2C6405C7227FBB70FC5939090022 * value)
	{
		___pages_3 = value;
		Il2CppCodeGenWriteBarrier((&___pages_3), value);
	}

	inline static int32_t get_offset_of_pageCurr_4() { return static_cast<int32_t>(offsetof(XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4, ___pageCurr_4)); }
	inline XmlEventU5BU5D_tAE240C67B5AAEA739D76EA9A649BEF1153584236* get_pageCurr_4() const { return ___pageCurr_4; }
	inline XmlEventU5BU5D_tAE240C67B5AAEA739D76EA9A649BEF1153584236** get_address_of_pageCurr_4() { return &___pageCurr_4; }
	inline void set_pageCurr_4(XmlEventU5BU5D_tAE240C67B5AAEA739D76EA9A649BEF1153584236* value)
	{
		___pageCurr_4 = value;
		Il2CppCodeGenWriteBarrier((&___pageCurr_4), value);
	}

	inline static int32_t get_offset_of_pageSize_5() { return static_cast<int32_t>(offsetof(XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4, ___pageSize_5)); }
	inline int32_t get_pageSize_5() const { return ___pageSize_5; }
	inline int32_t* get_address_of_pageSize_5() { return &___pageSize_5; }
	inline void set_pageSize_5(int32_t value)
	{
		___pageSize_5 = value;
	}

	inline static int32_t get_offset_of_hasRootNode_6() { return static_cast<int32_t>(offsetof(XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4, ___hasRootNode_6)); }
	inline bool get_hasRootNode_6() const { return ___hasRootNode_6; }
	inline bool* get_address_of_hasRootNode_6() { return &___hasRootNode_6; }
	inline void set_hasRootNode_6(bool value)
	{
		___hasRootNode_6 = value;
	}

	inline static int32_t get_offset_of_singleText_7() { return static_cast<int32_t>(offsetof(XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4, ___singleText_7)); }
	inline StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43  get_singleText_7() const { return ___singleText_7; }
	inline StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43 * get_address_of_singleText_7() { return &___singleText_7; }
	inline void set_singleText_7(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43  value)
	{
		___singleText_7 = value;
	}

	inline static int32_t get_offset_of_baseUri_8() { return static_cast<int32_t>(offsetof(XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4, ___baseUri_8)); }
	inline String_t* get_baseUri_8() const { return ___baseUri_8; }
	inline String_t** get_address_of_baseUri_8() { return &___baseUri_8; }
	inline void set_baseUri_8(String_t* value)
	{
		___baseUri_8 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLEVENTCACHE_T4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4_H
#ifndef XMLEVENTTYPE_TE042EAA577D37C5BAD142325B493273F6B8CC559_H
#define XMLEVENTTYPE_TE042EAA577D37C5BAD142325B493273F6B8CC559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEventCache/XmlEventType
struct  XmlEventType_tE042EAA577D37C5BAD142325B493273F6B8CC559 
{
public:
	// System.Int32 System.Xml.XmlEventCache/XmlEventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlEventType_tE042EAA577D37C5BAD142325B493273F6B8CC559, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLEVENTTYPE_TE042EAA577D37C5BAD142325B493273F6B8CC559_H
#ifndef XMLNODETYPE_TEE56AC4F9EC36B979516EA5836C4DA730B0A21E1_H
#define XMLNODETYPE_TEE56AC4F9EC36B979516EA5836C4DA730B0A21E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeType
struct  XmlNodeType_tEE56AC4F9EC36B979516EA5836C4DA730B0A21E1 
{
public:
	// System.Int32 System.Xml.XmlNodeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlNodeType_tEE56AC4F9EC36B979516EA5836C4DA730B0A21E1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODETYPE_TEE56AC4F9EC36B979516EA5836C4DA730B0A21E1_H
#ifndef XMLOUTPUTMETHOD_TF47F7732ECF902DFC04BAB9F9B7025F94DA862D4_H
#define XMLOUTPUTMETHOD_TF47F7732ECF902DFC04BAB9F9B7025F94DA862D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlOutputMethod
struct  XmlOutputMethod_tF47F7732ECF902DFC04BAB9F9B7025F94DA862D4 
{
public:
	// System.Int32 System.Xml.XmlOutputMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlOutputMethod_tF47F7732ECF902DFC04BAB9F9B7025F94DA862D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLOUTPUTMETHOD_TF47F7732ECF902DFC04BAB9F9B7025F94DA862D4_H
#ifndef XMLSPACE_TC1A644D65B6BE72CF02BA2687B5AE169713271AB_H
#define XMLSPACE_TC1A644D65B6BE72CF02BA2687B5AE169713271AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSpace
struct  XmlSpace_tC1A644D65B6BE72CF02BA2687B5AE169713271AB 
{
public:
	// System.Int32 System.Xml.XmlSpace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSpace_tC1A644D65B6BE72CF02BA2687B5AE169713271AB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSPACE_TC1A644D65B6BE72CF02BA2687B5AE169713271AB_H
#ifndef ATTRINFO_TC56788540A1F53E77E0A21E93000A66846FAFC0B_H
#define ATTRINFO_TC56788540A1F53E77E0A21E93000A66846FAFC0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSqlBinaryReader/AttrInfo
struct  AttrInfo_tC56788540A1F53E77E0A21E93000A66846FAFC0B 
{
public:
	// System.Xml.XmlSqlBinaryReader/QName System.Xml.XmlSqlBinaryReader/AttrInfo::name
	QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F  ___name_0;
	// System.String System.Xml.XmlSqlBinaryReader/AttrInfo::val
	String_t* ___val_1;
	// System.Int32 System.Xml.XmlSqlBinaryReader/AttrInfo::contentPos
	int32_t ___contentPos_2;
	// System.Int32 System.Xml.XmlSqlBinaryReader/AttrInfo::hashCode
	int32_t ___hashCode_3;
	// System.Int32 System.Xml.XmlSqlBinaryReader/AttrInfo::prevHash
	int32_t ___prevHash_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AttrInfo_tC56788540A1F53E77E0A21E93000A66846FAFC0B, ___name_0)); }
	inline QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F  get_name_0() const { return ___name_0; }
	inline QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F * get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F  value)
	{
		___name_0 = value;
	}

	inline static int32_t get_offset_of_val_1() { return static_cast<int32_t>(offsetof(AttrInfo_tC56788540A1F53E77E0A21E93000A66846FAFC0B, ___val_1)); }
	inline String_t* get_val_1() const { return ___val_1; }
	inline String_t** get_address_of_val_1() { return &___val_1; }
	inline void set_val_1(String_t* value)
	{
		___val_1 = value;
		Il2CppCodeGenWriteBarrier((&___val_1), value);
	}

	inline static int32_t get_offset_of_contentPos_2() { return static_cast<int32_t>(offsetof(AttrInfo_tC56788540A1F53E77E0A21E93000A66846FAFC0B, ___contentPos_2)); }
	inline int32_t get_contentPos_2() const { return ___contentPos_2; }
	inline int32_t* get_address_of_contentPos_2() { return &___contentPos_2; }
	inline void set_contentPos_2(int32_t value)
	{
		___contentPos_2 = value;
	}

	inline static int32_t get_offset_of_hashCode_3() { return static_cast<int32_t>(offsetof(AttrInfo_tC56788540A1F53E77E0A21E93000A66846FAFC0B, ___hashCode_3)); }
	inline int32_t get_hashCode_3() const { return ___hashCode_3; }
	inline int32_t* get_address_of_hashCode_3() { return &___hashCode_3; }
	inline void set_hashCode_3(int32_t value)
	{
		___hashCode_3 = value;
	}

	inline static int32_t get_offset_of_prevHash_4() { return static_cast<int32_t>(offsetof(AttrInfo_tC56788540A1F53E77E0A21E93000A66846FAFC0B, ___prevHash_4)); }
	inline int32_t get_prevHash_4() const { return ___prevHash_4; }
	inline int32_t* get_address_of_prevHash_4() { return &___prevHash_4; }
	inline void set_prevHash_4(int32_t value)
	{
		___prevHash_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlSqlBinaryReader/AttrInfo
struct AttrInfo_tC56788540A1F53E77E0A21E93000A66846FAFC0B_marshaled_pinvoke
{
	QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F_marshaled_pinvoke ___name_0;
	char* ___val_1;
	int32_t ___contentPos_2;
	int32_t ___hashCode_3;
	int32_t ___prevHash_4;
};
// Native definition for COM marshalling of System.Xml.XmlSqlBinaryReader/AttrInfo
struct AttrInfo_tC56788540A1F53E77E0A21E93000A66846FAFC0B_marshaled_com
{
	QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F_marshaled_com ___name_0;
	Il2CppChar* ___val_1;
	int32_t ___contentPos_2;
	int32_t ___hashCode_3;
	int32_t ___prevHash_4;
};
#endif // ATTRINFO_TC56788540A1F53E77E0A21E93000A66846FAFC0B_H
#ifndef NESTEDBINXML_T5E9B989BEC527EFE0C0907B63FCF17C3AC25D175_H
#define NESTEDBINXML_T5E9B989BEC527EFE0C0907B63FCF17C3AC25D175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSqlBinaryReader/NestedBinXml
struct  NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175  : public RuntimeObject
{
public:
	// System.Xml.XmlSqlBinaryReader/SymbolTables System.Xml.XmlSqlBinaryReader/NestedBinXml::symbolTables
	SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E  ___symbolTables_0;
	// System.Int32 System.Xml.XmlSqlBinaryReader/NestedBinXml::docState
	int32_t ___docState_1;
	// System.Xml.XmlSqlBinaryReader/NestedBinXml System.Xml.XmlSqlBinaryReader/NestedBinXml::next
	NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175 * ___next_2;

public:
	inline static int32_t get_offset_of_symbolTables_0() { return static_cast<int32_t>(offsetof(NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175, ___symbolTables_0)); }
	inline SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E  get_symbolTables_0() const { return ___symbolTables_0; }
	inline SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E * get_address_of_symbolTables_0() { return &___symbolTables_0; }
	inline void set_symbolTables_0(SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E  value)
	{
		___symbolTables_0 = value;
	}

	inline static int32_t get_offset_of_docState_1() { return static_cast<int32_t>(offsetof(NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175, ___docState_1)); }
	inline int32_t get_docState_1() const { return ___docState_1; }
	inline int32_t* get_address_of_docState_1() { return &___docState_1; }
	inline void set_docState_1(int32_t value)
	{
		___docState_1 = value;
	}

	inline static int32_t get_offset_of_next_2() { return static_cast<int32_t>(offsetof(NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175, ___next_2)); }
	inline NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175 * get_next_2() const { return ___next_2; }
	inline NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175 ** get_address_of_next_2() { return &___next_2; }
	inline void set_next_2(NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175 * value)
	{
		___next_2 = value;
		Il2CppCodeGenWriteBarrier((&___next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NESTEDBINXML_T5E9B989BEC527EFE0C0907B63FCF17C3AC25D175_H
#ifndef SCANSTATE_T7FFA2F3C0ACF51BC1C8E551490732B29A6323475_H
#define SCANSTATE_T7FFA2F3C0ACF51BC1C8E551490732B29A6323475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSqlBinaryReader/ScanState
struct  ScanState_t7FFA2F3C0ACF51BC1C8E551490732B29A6323475 
{
public:
	// System.Int32 System.Xml.XmlSqlBinaryReader/ScanState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScanState_t7FFA2F3C0ACF51BC1C8E551490732B29A6323475, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANSTATE_T7FFA2F3C0ACF51BC1C8E551490732B29A6323475_H
#ifndef XMLSTANDALONE_TD77DD6044609EA966459D821754EEE0BED37B39B_H
#define XMLSTANDALONE_TD77DD6044609EA966459D821754EEE0BED37B39B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlStandalone
struct  XmlStandalone_tD77DD6044609EA966459D821754EEE0BED37B39B 
{
public:
	// System.Int32 System.Xml.XmlStandalone::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlStandalone_tD77DD6044609EA966459D821754EEE0BED37B39B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSTANDALONE_TD77DD6044609EA966459D821754EEE0BED37B39B_H
#ifndef XMLTEXTENCODER_T444AD6ADF242669731301011B84C2A007B08A475_H
#define XMLTEXTENCODER_T444AD6ADF242669731301011B84C2A007B08A475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextEncoder
struct  XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475  : public RuntimeObject
{
public:
	// System.IO.TextWriter System.Xml.XmlTextEncoder::textWriter
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___textWriter_0;
	// System.Boolean System.Xml.XmlTextEncoder::inAttribute
	bool ___inAttribute_1;
	// System.Char System.Xml.XmlTextEncoder::quoteChar
	Il2CppChar ___quoteChar_2;
	// System.Text.StringBuilder System.Xml.XmlTextEncoder::attrValue
	StringBuilder_t * ___attrValue_3;
	// System.Boolean System.Xml.XmlTextEncoder::cacheAttrValue
	bool ___cacheAttrValue_4;
	// System.Xml.XmlCharType System.Xml.XmlTextEncoder::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_5;

public:
	inline static int32_t get_offset_of_textWriter_0() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475, ___textWriter_0)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get_textWriter_0() const { return ___textWriter_0; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of_textWriter_0() { return &___textWriter_0; }
	inline void set_textWriter_0(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		___textWriter_0 = value;
		Il2CppCodeGenWriteBarrier((&___textWriter_0), value);
	}

	inline static int32_t get_offset_of_inAttribute_1() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475, ___inAttribute_1)); }
	inline bool get_inAttribute_1() const { return ___inAttribute_1; }
	inline bool* get_address_of_inAttribute_1() { return &___inAttribute_1; }
	inline void set_inAttribute_1(bool value)
	{
		___inAttribute_1 = value;
	}

	inline static int32_t get_offset_of_quoteChar_2() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475, ___quoteChar_2)); }
	inline Il2CppChar get_quoteChar_2() const { return ___quoteChar_2; }
	inline Il2CppChar* get_address_of_quoteChar_2() { return &___quoteChar_2; }
	inline void set_quoteChar_2(Il2CppChar value)
	{
		___quoteChar_2 = value;
	}

	inline static int32_t get_offset_of_attrValue_3() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475, ___attrValue_3)); }
	inline StringBuilder_t * get_attrValue_3() const { return ___attrValue_3; }
	inline StringBuilder_t ** get_address_of_attrValue_3() { return &___attrValue_3; }
	inline void set_attrValue_3(StringBuilder_t * value)
	{
		___attrValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___attrValue_3), value);
	}

	inline static int32_t get_offset_of_cacheAttrValue_4() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475, ___cacheAttrValue_4)); }
	inline bool get_cacheAttrValue_4() const { return ___cacheAttrValue_4; }
	inline bool* get_address_of_cacheAttrValue_4() { return &___cacheAttrValue_4; }
	inline void set_cacheAttrValue_4(bool value)
	{
		___cacheAttrValue_4 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_5() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475, ___xmlCharType_5)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_5() const { return ___xmlCharType_5; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_5() { return &___xmlCharType_5; }
	inline void set_xmlCharType_5(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTENCODER_T444AD6ADF242669731301011B84C2A007B08A475_H
#ifndef INCREMENTALREADSTATE_T9D04D5EB17C9FE284AD3C7D5F3BB74EAA765653C_H
#define INCREMENTALREADSTATE_T9D04D5EB17C9FE284AD3C7D5F3BB74EAA765653C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/IncrementalReadState
struct  IncrementalReadState_t9D04D5EB17C9FE284AD3C7D5F3BB74EAA765653C 
{
public:
	// System.Int32 System.Xml.XmlTextReaderImpl/IncrementalReadState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IncrementalReadState_t9D04D5EB17C9FE284AD3C7D5F3BB74EAA765653C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INCREMENTALREADSTATE_T9D04D5EB17C9FE284AD3C7D5F3BB74EAA765653C_H
#ifndef PARSINGFUNCTION_T257574D1598B3E416A38FAB3204B4298FB2672B7_H
#define PARSINGFUNCTION_T257574D1598B3E416A38FAB3204B4298FB2672B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/ParsingFunction
struct  ParsingFunction_t257574D1598B3E416A38FAB3204B4298FB2672B7 
{
public:
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingFunction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParsingFunction_t257574D1598B3E416A38FAB3204B4298FB2672B7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSINGFUNCTION_T257574D1598B3E416A38FAB3204B4298FB2672B7_H
#ifndef PARSINGMODE_T16C9A13D4CD287424B850D09ABF6377BAA2A32EB_H
#define PARSINGMODE_T16C9A13D4CD287424B850D09ABF6377BAA2A32EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/ParsingMode
struct  ParsingMode_t16C9A13D4CD287424B850D09ABF6377BAA2A32EB 
{
public:
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParsingMode_t16C9A13D4CD287424B850D09ABF6377BAA2A32EB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSINGMODE_T16C9A13D4CD287424B850D09ABF6377BAA2A32EB_H
#ifndef OPERATOR_T5D5C7086CDF441136B62FC9AF3B7C4E5F24AA458_H
#define OPERATOR_T5D5C7086CDF441136B62FC9AF3B7C4E5F24AA458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Operator
struct  Operator_t5D5C7086CDF441136B62FC9AF3B7C4E5F24AA458  : public AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C
{
public:
	// MS.Internal.Xml.XPath.Operator/Op MS.Internal.Xml.XPath.Operator::opType
	int32_t ___opType_1;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Operator::opnd1
	AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C * ___opnd1_2;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Operator::opnd2
	AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C * ___opnd2_3;

public:
	inline static int32_t get_offset_of_opType_1() { return static_cast<int32_t>(offsetof(Operator_t5D5C7086CDF441136B62FC9AF3B7C4E5F24AA458, ___opType_1)); }
	inline int32_t get_opType_1() const { return ___opType_1; }
	inline int32_t* get_address_of_opType_1() { return &___opType_1; }
	inline void set_opType_1(int32_t value)
	{
		___opType_1 = value;
	}

	inline static int32_t get_offset_of_opnd1_2() { return static_cast<int32_t>(offsetof(Operator_t5D5C7086CDF441136B62FC9AF3B7C4E5F24AA458, ___opnd1_2)); }
	inline AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C * get_opnd1_2() const { return ___opnd1_2; }
	inline AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C ** get_address_of_opnd1_2() { return &___opnd1_2; }
	inline void set_opnd1_2(AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C * value)
	{
		___opnd1_2 = value;
		Il2CppCodeGenWriteBarrier((&___opnd1_2), value);
	}

	inline static int32_t get_offset_of_opnd2_3() { return static_cast<int32_t>(offsetof(Operator_t5D5C7086CDF441136B62FC9AF3B7C4E5F24AA458, ___opnd2_3)); }
	inline AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C * get_opnd2_3() const { return ___opnd2_3; }
	inline AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C ** get_address_of_opnd2_3() { return &___opnd2_3; }
	inline void set_opnd2_3(AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C * value)
	{
		___opnd2_3 = value;
		Il2CppCodeGenWriteBarrier((&___opnd2_3), value);
	}
};

struct Operator_t5D5C7086CDF441136B62FC9AF3B7C4E5F24AA458_StaticFields
{
public:
	// MS.Internal.Xml.XPath.Operator/Op[] MS.Internal.Xml.XPath.Operator::invertOp
	OpU5BU5D_t6F527BF40D983BF4877B310798EFD23BD46F4BFA* ___invertOp_0;

public:
	inline static int32_t get_offset_of_invertOp_0() { return static_cast<int32_t>(offsetof(Operator_t5D5C7086CDF441136B62FC9AF3B7C4E5F24AA458_StaticFields, ___invertOp_0)); }
	inline OpU5BU5D_t6F527BF40D983BF4877B310798EFD23BD46F4BFA* get_invertOp_0() const { return ___invertOp_0; }
	inline OpU5BU5D_t6F527BF40D983BF4877B310798EFD23BD46F4BFA** get_address_of_invertOp_0() { return &___invertOp_0; }
	inline void set_invertOp_0(OpU5BU5D_t6F527BF40D983BF4877B310798EFD23BD46F4BFA* value)
	{
		___invertOp_0 = value;
		Il2CppCodeGenWriteBarrier((&___invertOp_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOR_T5D5C7086CDF441136B62FC9AF3B7C4E5F24AA458_H
#ifndef PARAMINFO_T7A2C2C6976B72B72E5DDE9D83295FA5C3A83DE5C_H
#define PARAMINFO_T7A2C2C6976B72B72E5DDE9D83295FA5C3A83DE5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.XPathParser/ParamInfo
struct  ParamInfo_t7A2C2C6976B72B72E5DDE9D83295FA5C3A83DE5C  : public RuntimeObject
{
public:
	// MS.Internal.Xml.XPath.Function/FunctionType MS.Internal.Xml.XPath.XPathParser/ParamInfo::ftype
	int32_t ___ftype_0;
	// System.Int32 MS.Internal.Xml.XPath.XPathParser/ParamInfo::minargs
	int32_t ___minargs_1;
	// System.Int32 MS.Internal.Xml.XPath.XPathParser/ParamInfo::maxargs
	int32_t ___maxargs_2;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser/ParamInfo::argTypes
	XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* ___argTypes_3;

public:
	inline static int32_t get_offset_of_ftype_0() { return static_cast<int32_t>(offsetof(ParamInfo_t7A2C2C6976B72B72E5DDE9D83295FA5C3A83DE5C, ___ftype_0)); }
	inline int32_t get_ftype_0() const { return ___ftype_0; }
	inline int32_t* get_address_of_ftype_0() { return &___ftype_0; }
	inline void set_ftype_0(int32_t value)
	{
		___ftype_0 = value;
	}

	inline static int32_t get_offset_of_minargs_1() { return static_cast<int32_t>(offsetof(ParamInfo_t7A2C2C6976B72B72E5DDE9D83295FA5C3A83DE5C, ___minargs_1)); }
	inline int32_t get_minargs_1() const { return ___minargs_1; }
	inline int32_t* get_address_of_minargs_1() { return &___minargs_1; }
	inline void set_minargs_1(int32_t value)
	{
		___minargs_1 = value;
	}

	inline static int32_t get_offset_of_maxargs_2() { return static_cast<int32_t>(offsetof(ParamInfo_t7A2C2C6976B72B72E5DDE9D83295FA5C3A83DE5C, ___maxargs_2)); }
	inline int32_t get_maxargs_2() const { return ___maxargs_2; }
	inline int32_t* get_address_of_maxargs_2() { return &___maxargs_2; }
	inline void set_maxargs_2(int32_t value)
	{
		___maxargs_2 = value;
	}

	inline static int32_t get_offset_of_argTypes_3() { return static_cast<int32_t>(offsetof(ParamInfo_t7A2C2C6976B72B72E5DDE9D83295FA5C3A83DE5C, ___argTypes_3)); }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* get_argTypes_3() const { return ___argTypes_3; }
	inline XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF** get_address_of_argTypes_3() { return &___argTypes_3; }
	inline void set_argTypes_3(XPathResultTypeU5BU5D_tD43B42E34AE5CE27B990DCECA46F8F5D784747BF* value)
	{
		___argTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___argTypes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMINFO_T7A2C2C6976B72B72E5DDE9D83295FA5C3A83DE5C_H
#ifndef XPATHSCANNER_T0A7B6C3C5EB6670560039D459C772F71D5C08481_H
#define XPATHSCANNER_T0A7B6C3C5EB6670560039D459C772F71D5C08481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.XPathScanner
struct  XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481  : public RuntimeObject
{
public:
	// System.String MS.Internal.Xml.XPath.XPathScanner::xpathExpr
	String_t* ___xpathExpr_0;
	// System.Int32 MS.Internal.Xml.XPath.XPathScanner::xpathExprIndex
	int32_t ___xpathExprIndex_1;
	// MS.Internal.Xml.XPath.XPathScanner/LexKind MS.Internal.Xml.XPath.XPathScanner::kind
	int32_t ___kind_2;
	// System.Char MS.Internal.Xml.XPath.XPathScanner::currentChar
	Il2CppChar ___currentChar_3;
	// System.String MS.Internal.Xml.XPath.XPathScanner::name
	String_t* ___name_4;
	// System.String MS.Internal.Xml.XPath.XPathScanner::prefix
	String_t* ___prefix_5;
	// System.String MS.Internal.Xml.XPath.XPathScanner::stringValue
	String_t* ___stringValue_6;
	// System.Double MS.Internal.Xml.XPath.XPathScanner::numberValue
	double ___numberValue_7;
	// System.Boolean MS.Internal.Xml.XPath.XPathScanner::canBeFunction
	bool ___canBeFunction_8;
	// System.Xml.XmlCharType MS.Internal.Xml.XPath.XPathScanner::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_9;

public:
	inline static int32_t get_offset_of_xpathExpr_0() { return static_cast<int32_t>(offsetof(XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481, ___xpathExpr_0)); }
	inline String_t* get_xpathExpr_0() const { return ___xpathExpr_0; }
	inline String_t** get_address_of_xpathExpr_0() { return &___xpathExpr_0; }
	inline void set_xpathExpr_0(String_t* value)
	{
		___xpathExpr_0 = value;
		Il2CppCodeGenWriteBarrier((&___xpathExpr_0), value);
	}

	inline static int32_t get_offset_of_xpathExprIndex_1() { return static_cast<int32_t>(offsetof(XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481, ___xpathExprIndex_1)); }
	inline int32_t get_xpathExprIndex_1() const { return ___xpathExprIndex_1; }
	inline int32_t* get_address_of_xpathExprIndex_1() { return &___xpathExprIndex_1; }
	inline void set_xpathExprIndex_1(int32_t value)
	{
		___xpathExprIndex_1 = value;
	}

	inline static int32_t get_offset_of_kind_2() { return static_cast<int32_t>(offsetof(XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481, ___kind_2)); }
	inline int32_t get_kind_2() const { return ___kind_2; }
	inline int32_t* get_address_of_kind_2() { return &___kind_2; }
	inline void set_kind_2(int32_t value)
	{
		___kind_2 = value;
	}

	inline static int32_t get_offset_of_currentChar_3() { return static_cast<int32_t>(offsetof(XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481, ___currentChar_3)); }
	inline Il2CppChar get_currentChar_3() const { return ___currentChar_3; }
	inline Il2CppChar* get_address_of_currentChar_3() { return &___currentChar_3; }
	inline void set_currentChar_3(Il2CppChar value)
	{
		___currentChar_3 = value;
	}

	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier((&___name_4), value);
	}

	inline static int32_t get_offset_of_prefix_5() { return static_cast<int32_t>(offsetof(XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481, ___prefix_5)); }
	inline String_t* get_prefix_5() const { return ___prefix_5; }
	inline String_t** get_address_of_prefix_5() { return &___prefix_5; }
	inline void set_prefix_5(String_t* value)
	{
		___prefix_5 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_5), value);
	}

	inline static int32_t get_offset_of_stringValue_6() { return static_cast<int32_t>(offsetof(XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481, ___stringValue_6)); }
	inline String_t* get_stringValue_6() const { return ___stringValue_6; }
	inline String_t** get_address_of_stringValue_6() { return &___stringValue_6; }
	inline void set_stringValue_6(String_t* value)
	{
		___stringValue_6 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_6), value);
	}

	inline static int32_t get_offset_of_numberValue_7() { return static_cast<int32_t>(offsetof(XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481, ___numberValue_7)); }
	inline double get_numberValue_7() const { return ___numberValue_7; }
	inline double* get_address_of_numberValue_7() { return &___numberValue_7; }
	inline void set_numberValue_7(double value)
	{
		___numberValue_7 = value;
	}

	inline static int32_t get_offset_of_canBeFunction_8() { return static_cast<int32_t>(offsetof(XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481, ___canBeFunction_8)); }
	inline bool get_canBeFunction_8() const { return ___canBeFunction_8; }
	inline bool* get_address_of_canBeFunction_8() { return &___canBeFunction_8; }
	inline void set_canBeFunction_8(bool value)
	{
		___canBeFunction_8 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_9() { return static_cast<int32_t>(offsetof(XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481, ___xmlCharType_9)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_9() const { return ___xmlCharType_9; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_9() { return &___xmlCharType_9; }
	inline void set_xmlCharType_9(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHSCANNER_T0A7B6C3C5EB6670560039D459C772F71D5C08481_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef READCONTENTASBINARYHELPER_T3CD837B0BD1C1C1D2E4EEF057C0C22742A766797_H
#define READCONTENTASBINARYHELPER_T3CD837B0BD1C1C1D2E4EEF057C0C22742A766797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReadContentAsBinaryHelper
struct  ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797  : public RuntimeObject
{
public:
	// System.Xml.XmlReader System.Xml.ReadContentAsBinaryHelper::reader
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * ___reader_0;
	// System.Xml.ReadContentAsBinaryHelper/State System.Xml.ReadContentAsBinaryHelper::state
	int32_t ___state_1;
	// System.Int32 System.Xml.ReadContentAsBinaryHelper::valueOffset
	int32_t ___valueOffset_2;
	// System.Boolean System.Xml.ReadContentAsBinaryHelper::isEnd
	bool ___isEnd_3;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797, ___reader_0)); }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * get_reader_0() const { return ___reader_0; }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier((&___reader_0), value);
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_valueOffset_2() { return static_cast<int32_t>(offsetof(ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797, ___valueOffset_2)); }
	inline int32_t get_valueOffset_2() const { return ___valueOffset_2; }
	inline int32_t* get_address_of_valueOffset_2() { return &___valueOffset_2; }
	inline void set_valueOffset_2(int32_t value)
	{
		___valueOffset_2 = value;
	}

	inline static int32_t get_offset_of_isEnd_3() { return static_cast<int32_t>(offsetof(ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797, ___isEnd_3)); }
	inline bool get_isEnd_3() const { return ___isEnd_3; }
	inline bool* get_address_of_isEnd_3() { return &___isEnd_3; }
	inline void set_isEnd_3(bool value)
	{
		___isEnd_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READCONTENTASBINARYHELPER_T3CD837B0BD1C1C1D2E4EEF057C0C22742A766797_H
#ifndef VALIDATINGREADERNODEDATA_TB7D1981AED7CEDF168218AA0F59586DBB3502E3F_H
#define VALIDATINGREADERNODEDATA_TB7D1981AED7CEDF168218AA0F59586DBB3502E3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ValidatingReaderNodeData
struct  ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F  : public RuntimeObject
{
public:
	// System.String System.Xml.ValidatingReaderNodeData::localName
	String_t* ___localName_0;
	// System.String System.Xml.ValidatingReaderNodeData::namespaceUri
	String_t* ___namespaceUri_1;
	// System.String System.Xml.ValidatingReaderNodeData::prefix
	String_t* ___prefix_2;
	// System.String System.Xml.ValidatingReaderNodeData::nameWPrefix
	String_t* ___nameWPrefix_3;
	// System.String System.Xml.ValidatingReaderNodeData::rawValue
	String_t* ___rawValue_4;
	// System.String System.Xml.ValidatingReaderNodeData::originalStringValue
	String_t* ___originalStringValue_5;
	// System.Int32 System.Xml.ValidatingReaderNodeData::depth
	int32_t ___depth_6;
	// System.Xml.AttributePSVIInfo System.Xml.ValidatingReaderNodeData::attributePSVIInfo
	AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94 * ___attributePSVIInfo_7;
	// System.Xml.XmlNodeType System.Xml.ValidatingReaderNodeData::nodeType
	int32_t ___nodeType_8;
	// System.Int32 System.Xml.ValidatingReaderNodeData::lineNo
	int32_t ___lineNo_9;
	// System.Int32 System.Xml.ValidatingReaderNodeData::linePos
	int32_t ___linePos_10;

public:
	inline static int32_t get_offset_of_localName_0() { return static_cast<int32_t>(offsetof(ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F, ___localName_0)); }
	inline String_t* get_localName_0() const { return ___localName_0; }
	inline String_t** get_address_of_localName_0() { return &___localName_0; }
	inline void set_localName_0(String_t* value)
	{
		___localName_0 = value;
		Il2CppCodeGenWriteBarrier((&___localName_0), value);
	}

	inline static int32_t get_offset_of_namespaceUri_1() { return static_cast<int32_t>(offsetof(ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F, ___namespaceUri_1)); }
	inline String_t* get_namespaceUri_1() const { return ___namespaceUri_1; }
	inline String_t** get_address_of_namespaceUri_1() { return &___namespaceUri_1; }
	inline void set_namespaceUri_1(String_t* value)
	{
		___namespaceUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_1), value);
	}

	inline static int32_t get_offset_of_prefix_2() { return static_cast<int32_t>(offsetof(ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F, ___prefix_2)); }
	inline String_t* get_prefix_2() const { return ___prefix_2; }
	inline String_t** get_address_of_prefix_2() { return &___prefix_2; }
	inline void set_prefix_2(String_t* value)
	{
		___prefix_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_2), value);
	}

	inline static int32_t get_offset_of_nameWPrefix_3() { return static_cast<int32_t>(offsetof(ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F, ___nameWPrefix_3)); }
	inline String_t* get_nameWPrefix_3() const { return ___nameWPrefix_3; }
	inline String_t** get_address_of_nameWPrefix_3() { return &___nameWPrefix_3; }
	inline void set_nameWPrefix_3(String_t* value)
	{
		___nameWPrefix_3 = value;
		Il2CppCodeGenWriteBarrier((&___nameWPrefix_3), value);
	}

	inline static int32_t get_offset_of_rawValue_4() { return static_cast<int32_t>(offsetof(ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F, ___rawValue_4)); }
	inline String_t* get_rawValue_4() const { return ___rawValue_4; }
	inline String_t** get_address_of_rawValue_4() { return &___rawValue_4; }
	inline void set_rawValue_4(String_t* value)
	{
		___rawValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___rawValue_4), value);
	}

	inline static int32_t get_offset_of_originalStringValue_5() { return static_cast<int32_t>(offsetof(ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F, ___originalStringValue_5)); }
	inline String_t* get_originalStringValue_5() const { return ___originalStringValue_5; }
	inline String_t** get_address_of_originalStringValue_5() { return &___originalStringValue_5; }
	inline void set_originalStringValue_5(String_t* value)
	{
		___originalStringValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___originalStringValue_5), value);
	}

	inline static int32_t get_offset_of_depth_6() { return static_cast<int32_t>(offsetof(ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F, ___depth_6)); }
	inline int32_t get_depth_6() const { return ___depth_6; }
	inline int32_t* get_address_of_depth_6() { return &___depth_6; }
	inline void set_depth_6(int32_t value)
	{
		___depth_6 = value;
	}

	inline static int32_t get_offset_of_attributePSVIInfo_7() { return static_cast<int32_t>(offsetof(ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F, ___attributePSVIInfo_7)); }
	inline AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94 * get_attributePSVIInfo_7() const { return ___attributePSVIInfo_7; }
	inline AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94 ** get_address_of_attributePSVIInfo_7() { return &___attributePSVIInfo_7; }
	inline void set_attributePSVIInfo_7(AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94 * value)
	{
		___attributePSVIInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___attributePSVIInfo_7), value);
	}

	inline static int32_t get_offset_of_nodeType_8() { return static_cast<int32_t>(offsetof(ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F, ___nodeType_8)); }
	inline int32_t get_nodeType_8() const { return ___nodeType_8; }
	inline int32_t* get_address_of_nodeType_8() { return &___nodeType_8; }
	inline void set_nodeType_8(int32_t value)
	{
		___nodeType_8 = value;
	}

	inline static int32_t get_offset_of_lineNo_9() { return static_cast<int32_t>(offsetof(ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F, ___lineNo_9)); }
	inline int32_t get_lineNo_9() const { return ___lineNo_9; }
	inline int32_t* get_address_of_lineNo_9() { return &___lineNo_9; }
	inline void set_lineNo_9(int32_t value)
	{
		___lineNo_9 = value;
	}

	inline static int32_t get_offset_of_linePos_10() { return static_cast<int32_t>(offsetof(ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F, ___linePos_10)); }
	inline int32_t get_linePos_10() const { return ___linePos_10; }
	inline int32_t* get_address_of_linePos_10() { return &___linePos_10; }
	inline void set_linePos_10(int32_t value)
	{
		___linePos_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATINGREADERNODEDATA_TB7D1981AED7CEDF168218AA0F59586DBB3502E3F_H
#ifndef XMLASYNCCHECKREADERWITHLINEINFONS_TDA344AA17D0B0512BDC02C145605922DC1875327_H
#define XMLASYNCCHECKREADERWITHLINEINFONS_TDA344AA17D0B0512BDC02C145605922DC1875327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAsyncCheckReaderWithLineInfoNS
struct  XmlAsyncCheckReaderWithLineInfoNS_tDA344AA17D0B0512BDC02C145605922DC1875327  : public XmlAsyncCheckReaderWithLineInfo_tFF91FA10CC8CF8DD006D69CFB36E0DE795F1533B
{
public:
	// System.Xml.IXmlNamespaceResolver System.Xml.XmlAsyncCheckReaderWithLineInfoNS::readerAsIXmlNamespaceResolver
	RuntimeObject* ___readerAsIXmlNamespaceResolver_6;

public:
	inline static int32_t get_offset_of_readerAsIXmlNamespaceResolver_6() { return static_cast<int32_t>(offsetof(XmlAsyncCheckReaderWithLineInfoNS_tDA344AA17D0B0512BDC02C145605922DC1875327, ___readerAsIXmlNamespaceResolver_6)); }
	inline RuntimeObject* get_readerAsIXmlNamespaceResolver_6() const { return ___readerAsIXmlNamespaceResolver_6; }
	inline RuntimeObject** get_address_of_readerAsIXmlNamespaceResolver_6() { return &___readerAsIXmlNamespaceResolver_6; }
	inline void set_readerAsIXmlNamespaceResolver_6(RuntimeObject* value)
	{
		___readerAsIXmlNamespaceResolver_6 = value;
		Il2CppCodeGenWriteBarrier((&___readerAsIXmlNamespaceResolver_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLASYNCCHECKREADERWITHLINEINFONS_TDA344AA17D0B0512BDC02C145605922DC1875327_H
#ifndef XMLENCODEDRAWTEXTWRITER_TF05AF9DF325D6AFDA8990E210600DDB42865EE4B_H
#define XMLENCODEDRAWTEXTWRITER_TF05AF9DF325D6AFDA8990E210600DDB42865EE4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEncodedRawTextWriter
struct  XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B  : public XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2
{
public:
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::useAsync
	bool ___useAsync_3;
	// System.Byte[] System.Xml.XmlEncodedRawTextWriter::bufBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bufBytes_4;
	// System.IO.Stream System.Xml.XmlEncodedRawTextWriter::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	// System.Text.Encoding System.Xml.XmlEncodedRawTextWriter::encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_6;
	// System.Xml.XmlCharType System.Xml.XmlEncodedRawTextWriter::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_7;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::bufPos
	int32_t ___bufPos_8;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::textPos
	int32_t ___textPos_9;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::contentPos
	int32_t ___contentPos_10;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::cdataPos
	int32_t ___cdataPos_11;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::attrEndPos
	int32_t ___attrEndPos_12;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::bufLen
	int32_t ___bufLen_13;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::writeToNull
	bool ___writeToNull_14;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::hadDoubleBracket
	bool ___hadDoubleBracket_15;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::inAttributeValue
	bool ___inAttributeValue_16;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::bufBytesUsed
	int32_t ___bufBytesUsed_17;
	// System.Char[] System.Xml.XmlEncodedRawTextWriter::bufChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___bufChars_18;
	// System.Text.Encoder System.Xml.XmlEncodedRawTextWriter::encoder
	Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464 * ___encoder_19;
	// System.IO.TextWriter System.Xml.XmlEncodedRawTextWriter::writer
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___writer_20;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::trackTextContent
	bool ___trackTextContent_21;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::inTextContent
	bool ___inTextContent_22;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::lastMarkPos
	int32_t ___lastMarkPos_23;
	// System.Int32[] System.Xml.XmlEncodedRawTextWriter::textContentMarks
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___textContentMarks_24;
	// System.Xml.CharEntityEncoderFallback System.Xml.XmlEncodedRawTextWriter::charEntityFallback
	CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE * ___charEntityFallback_25;
	// System.Xml.NewLineHandling System.Xml.XmlEncodedRawTextWriter::newLineHandling
	int32_t ___newLineHandling_26;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::closeOutput
	bool ___closeOutput_27;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::omitXmlDeclaration
	bool ___omitXmlDeclaration_28;
	// System.String System.Xml.XmlEncodedRawTextWriter::newLineChars
	String_t* ___newLineChars_29;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::checkCharacters
	bool ___checkCharacters_30;
	// System.Xml.XmlStandalone System.Xml.XmlEncodedRawTextWriter::standalone
	int32_t ___standalone_31;
	// System.Xml.XmlOutputMethod System.Xml.XmlEncodedRawTextWriter::outputMethod
	int32_t ___outputMethod_32;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::autoXmlDeclaration
	bool ___autoXmlDeclaration_33;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::mergeCDataSections
	bool ___mergeCDataSections_34;

public:
	inline static int32_t get_offset_of_useAsync_3() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___useAsync_3)); }
	inline bool get_useAsync_3() const { return ___useAsync_3; }
	inline bool* get_address_of_useAsync_3() { return &___useAsync_3; }
	inline void set_useAsync_3(bool value)
	{
		___useAsync_3 = value;
	}

	inline static int32_t get_offset_of_bufBytes_4() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___bufBytes_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bufBytes_4() const { return ___bufBytes_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bufBytes_4() { return &___bufBytes_4; }
	inline void set_bufBytes_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bufBytes_4 = value;
		Il2CppCodeGenWriteBarrier((&___bufBytes_4), value);
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___stream_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_5() const { return ___stream_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___encoding_6)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_encoding_6() const { return ___encoding_6; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_6), value);
	}

	inline static int32_t get_offset_of_xmlCharType_7() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___xmlCharType_7)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_7() const { return ___xmlCharType_7; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_7() { return &___xmlCharType_7; }
	inline void set_xmlCharType_7(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_7 = value;
	}

	inline static int32_t get_offset_of_bufPos_8() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___bufPos_8)); }
	inline int32_t get_bufPos_8() const { return ___bufPos_8; }
	inline int32_t* get_address_of_bufPos_8() { return &___bufPos_8; }
	inline void set_bufPos_8(int32_t value)
	{
		___bufPos_8 = value;
	}

	inline static int32_t get_offset_of_textPos_9() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___textPos_9)); }
	inline int32_t get_textPos_9() const { return ___textPos_9; }
	inline int32_t* get_address_of_textPos_9() { return &___textPos_9; }
	inline void set_textPos_9(int32_t value)
	{
		___textPos_9 = value;
	}

	inline static int32_t get_offset_of_contentPos_10() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___contentPos_10)); }
	inline int32_t get_contentPos_10() const { return ___contentPos_10; }
	inline int32_t* get_address_of_contentPos_10() { return &___contentPos_10; }
	inline void set_contentPos_10(int32_t value)
	{
		___contentPos_10 = value;
	}

	inline static int32_t get_offset_of_cdataPos_11() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___cdataPos_11)); }
	inline int32_t get_cdataPos_11() const { return ___cdataPos_11; }
	inline int32_t* get_address_of_cdataPos_11() { return &___cdataPos_11; }
	inline void set_cdataPos_11(int32_t value)
	{
		___cdataPos_11 = value;
	}

	inline static int32_t get_offset_of_attrEndPos_12() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___attrEndPos_12)); }
	inline int32_t get_attrEndPos_12() const { return ___attrEndPos_12; }
	inline int32_t* get_address_of_attrEndPos_12() { return &___attrEndPos_12; }
	inline void set_attrEndPos_12(int32_t value)
	{
		___attrEndPos_12 = value;
	}

	inline static int32_t get_offset_of_bufLen_13() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___bufLen_13)); }
	inline int32_t get_bufLen_13() const { return ___bufLen_13; }
	inline int32_t* get_address_of_bufLen_13() { return &___bufLen_13; }
	inline void set_bufLen_13(int32_t value)
	{
		___bufLen_13 = value;
	}

	inline static int32_t get_offset_of_writeToNull_14() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___writeToNull_14)); }
	inline bool get_writeToNull_14() const { return ___writeToNull_14; }
	inline bool* get_address_of_writeToNull_14() { return &___writeToNull_14; }
	inline void set_writeToNull_14(bool value)
	{
		___writeToNull_14 = value;
	}

	inline static int32_t get_offset_of_hadDoubleBracket_15() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___hadDoubleBracket_15)); }
	inline bool get_hadDoubleBracket_15() const { return ___hadDoubleBracket_15; }
	inline bool* get_address_of_hadDoubleBracket_15() { return &___hadDoubleBracket_15; }
	inline void set_hadDoubleBracket_15(bool value)
	{
		___hadDoubleBracket_15 = value;
	}

	inline static int32_t get_offset_of_inAttributeValue_16() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___inAttributeValue_16)); }
	inline bool get_inAttributeValue_16() const { return ___inAttributeValue_16; }
	inline bool* get_address_of_inAttributeValue_16() { return &___inAttributeValue_16; }
	inline void set_inAttributeValue_16(bool value)
	{
		___inAttributeValue_16 = value;
	}

	inline static int32_t get_offset_of_bufBytesUsed_17() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___bufBytesUsed_17)); }
	inline int32_t get_bufBytesUsed_17() const { return ___bufBytesUsed_17; }
	inline int32_t* get_address_of_bufBytesUsed_17() { return &___bufBytesUsed_17; }
	inline void set_bufBytesUsed_17(int32_t value)
	{
		___bufBytesUsed_17 = value;
	}

	inline static int32_t get_offset_of_bufChars_18() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___bufChars_18)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_bufChars_18() const { return ___bufChars_18; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_bufChars_18() { return &___bufChars_18; }
	inline void set_bufChars_18(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___bufChars_18 = value;
		Il2CppCodeGenWriteBarrier((&___bufChars_18), value);
	}

	inline static int32_t get_offset_of_encoder_19() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___encoder_19)); }
	inline Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464 * get_encoder_19() const { return ___encoder_19; }
	inline Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464 ** get_address_of_encoder_19() { return &___encoder_19; }
	inline void set_encoder_19(Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464 * value)
	{
		___encoder_19 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_19), value);
	}

	inline static int32_t get_offset_of_writer_20() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___writer_20)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get_writer_20() const { return ___writer_20; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of_writer_20() { return &___writer_20; }
	inline void set_writer_20(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		___writer_20 = value;
		Il2CppCodeGenWriteBarrier((&___writer_20), value);
	}

	inline static int32_t get_offset_of_trackTextContent_21() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___trackTextContent_21)); }
	inline bool get_trackTextContent_21() const { return ___trackTextContent_21; }
	inline bool* get_address_of_trackTextContent_21() { return &___trackTextContent_21; }
	inline void set_trackTextContent_21(bool value)
	{
		___trackTextContent_21 = value;
	}

	inline static int32_t get_offset_of_inTextContent_22() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___inTextContent_22)); }
	inline bool get_inTextContent_22() const { return ___inTextContent_22; }
	inline bool* get_address_of_inTextContent_22() { return &___inTextContent_22; }
	inline void set_inTextContent_22(bool value)
	{
		___inTextContent_22 = value;
	}

	inline static int32_t get_offset_of_lastMarkPos_23() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___lastMarkPos_23)); }
	inline int32_t get_lastMarkPos_23() const { return ___lastMarkPos_23; }
	inline int32_t* get_address_of_lastMarkPos_23() { return &___lastMarkPos_23; }
	inline void set_lastMarkPos_23(int32_t value)
	{
		___lastMarkPos_23 = value;
	}

	inline static int32_t get_offset_of_textContentMarks_24() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___textContentMarks_24)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_textContentMarks_24() const { return ___textContentMarks_24; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_textContentMarks_24() { return &___textContentMarks_24; }
	inline void set_textContentMarks_24(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___textContentMarks_24 = value;
		Il2CppCodeGenWriteBarrier((&___textContentMarks_24), value);
	}

	inline static int32_t get_offset_of_charEntityFallback_25() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___charEntityFallback_25)); }
	inline CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE * get_charEntityFallback_25() const { return ___charEntityFallback_25; }
	inline CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE ** get_address_of_charEntityFallback_25() { return &___charEntityFallback_25; }
	inline void set_charEntityFallback_25(CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE * value)
	{
		___charEntityFallback_25 = value;
		Il2CppCodeGenWriteBarrier((&___charEntityFallback_25), value);
	}

	inline static int32_t get_offset_of_newLineHandling_26() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___newLineHandling_26)); }
	inline int32_t get_newLineHandling_26() const { return ___newLineHandling_26; }
	inline int32_t* get_address_of_newLineHandling_26() { return &___newLineHandling_26; }
	inline void set_newLineHandling_26(int32_t value)
	{
		___newLineHandling_26 = value;
	}

	inline static int32_t get_offset_of_closeOutput_27() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___closeOutput_27)); }
	inline bool get_closeOutput_27() const { return ___closeOutput_27; }
	inline bool* get_address_of_closeOutput_27() { return &___closeOutput_27; }
	inline void set_closeOutput_27(bool value)
	{
		___closeOutput_27 = value;
	}

	inline static int32_t get_offset_of_omitXmlDeclaration_28() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___omitXmlDeclaration_28)); }
	inline bool get_omitXmlDeclaration_28() const { return ___omitXmlDeclaration_28; }
	inline bool* get_address_of_omitXmlDeclaration_28() { return &___omitXmlDeclaration_28; }
	inline void set_omitXmlDeclaration_28(bool value)
	{
		___omitXmlDeclaration_28 = value;
	}

	inline static int32_t get_offset_of_newLineChars_29() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___newLineChars_29)); }
	inline String_t* get_newLineChars_29() const { return ___newLineChars_29; }
	inline String_t** get_address_of_newLineChars_29() { return &___newLineChars_29; }
	inline void set_newLineChars_29(String_t* value)
	{
		___newLineChars_29 = value;
		Il2CppCodeGenWriteBarrier((&___newLineChars_29), value);
	}

	inline static int32_t get_offset_of_checkCharacters_30() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___checkCharacters_30)); }
	inline bool get_checkCharacters_30() const { return ___checkCharacters_30; }
	inline bool* get_address_of_checkCharacters_30() { return &___checkCharacters_30; }
	inline void set_checkCharacters_30(bool value)
	{
		___checkCharacters_30 = value;
	}

	inline static int32_t get_offset_of_standalone_31() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___standalone_31)); }
	inline int32_t get_standalone_31() const { return ___standalone_31; }
	inline int32_t* get_address_of_standalone_31() { return &___standalone_31; }
	inline void set_standalone_31(int32_t value)
	{
		___standalone_31 = value;
	}

	inline static int32_t get_offset_of_outputMethod_32() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___outputMethod_32)); }
	inline int32_t get_outputMethod_32() const { return ___outputMethod_32; }
	inline int32_t* get_address_of_outputMethod_32() { return &___outputMethod_32; }
	inline void set_outputMethod_32(int32_t value)
	{
		___outputMethod_32 = value;
	}

	inline static int32_t get_offset_of_autoXmlDeclaration_33() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___autoXmlDeclaration_33)); }
	inline bool get_autoXmlDeclaration_33() const { return ___autoXmlDeclaration_33; }
	inline bool* get_address_of_autoXmlDeclaration_33() { return &___autoXmlDeclaration_33; }
	inline void set_autoXmlDeclaration_33(bool value)
	{
		___autoXmlDeclaration_33 = value;
	}

	inline static int32_t get_offset_of_mergeCDataSections_34() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___mergeCDataSections_34)); }
	inline bool get_mergeCDataSections_34() const { return ___mergeCDataSections_34; }
	inline bool* get_address_of_mergeCDataSections_34() { return &___mergeCDataSections_34; }
	inline void set_mergeCDataSections_34(bool value)
	{
		___mergeCDataSections_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENCODEDRAWTEXTWRITER_TF05AF9DF325D6AFDA8990E210600DDB42865EE4B_H
#ifndef XMLEVENT_T173A9D51CFBE692A6FBE277FFF0C746C9DA292E4_H
#define XMLEVENT_T173A9D51CFBE692A6FBE277FFF0C746C9DA292E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEventCache/XmlEvent
struct  XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4 
{
public:
	// System.Xml.XmlEventCache/XmlEventType System.Xml.XmlEventCache/XmlEvent::eventType
	int32_t ___eventType_0;
	// System.String System.Xml.XmlEventCache/XmlEvent::s1
	String_t* ___s1_1;
	// System.String System.Xml.XmlEventCache/XmlEvent::s2
	String_t* ___s2_2;
	// System.String System.Xml.XmlEventCache/XmlEvent::s3
	String_t* ___s3_3;
	// System.Object System.Xml.XmlEventCache/XmlEvent::o
	RuntimeObject * ___o_4;

public:
	inline static int32_t get_offset_of_eventType_0() { return static_cast<int32_t>(offsetof(XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4, ___eventType_0)); }
	inline int32_t get_eventType_0() const { return ___eventType_0; }
	inline int32_t* get_address_of_eventType_0() { return &___eventType_0; }
	inline void set_eventType_0(int32_t value)
	{
		___eventType_0 = value;
	}

	inline static int32_t get_offset_of_s1_1() { return static_cast<int32_t>(offsetof(XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4, ___s1_1)); }
	inline String_t* get_s1_1() const { return ___s1_1; }
	inline String_t** get_address_of_s1_1() { return &___s1_1; }
	inline void set_s1_1(String_t* value)
	{
		___s1_1 = value;
		Il2CppCodeGenWriteBarrier((&___s1_1), value);
	}

	inline static int32_t get_offset_of_s2_2() { return static_cast<int32_t>(offsetof(XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4, ___s2_2)); }
	inline String_t* get_s2_2() const { return ___s2_2; }
	inline String_t** get_address_of_s2_2() { return &___s2_2; }
	inline void set_s2_2(String_t* value)
	{
		___s2_2 = value;
		Il2CppCodeGenWriteBarrier((&___s2_2), value);
	}

	inline static int32_t get_offset_of_s3_3() { return static_cast<int32_t>(offsetof(XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4, ___s3_3)); }
	inline String_t* get_s3_3() const { return ___s3_3; }
	inline String_t** get_address_of_s3_3() { return &___s3_3; }
	inline void set_s3_3(String_t* value)
	{
		___s3_3 = value;
		Il2CppCodeGenWriteBarrier((&___s3_3), value);
	}

	inline static int32_t get_offset_of_o_4() { return static_cast<int32_t>(offsetof(XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4, ___o_4)); }
	inline RuntimeObject * get_o_4() const { return ___o_4; }
	inline RuntimeObject ** get_address_of_o_4() { return &___o_4; }
	inline void set_o_4(RuntimeObject * value)
	{
		___o_4 = value;
		Il2CppCodeGenWriteBarrier((&___o_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlEventCache/XmlEvent
struct XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4_marshaled_pinvoke
{
	int32_t ___eventType_0;
	char* ___s1_1;
	char* ___s2_2;
	char* ___s3_3;
	Il2CppIUnknown* ___o_4;
};
// Native definition for COM marshalling of System.Xml.XmlEventCache/XmlEvent
struct XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4_marshaled_com
{
	int32_t ___eventType_0;
	Il2CppChar* ___s1_1;
	Il2CppChar* ___s2_2;
	Il2CppChar* ___s3_3;
	Il2CppIUnknown* ___o_4;
};
#endif // XMLEVENT_T173A9D51CFBE692A6FBE277FFF0C746C9DA292E4_H
#ifndef XMLPARSERCONTEXT_TDC3A95452BC683F0D64EC3C4AF9934D3FC732279_H
#define XMLPARSERCONTEXT_TDC3A95452BC683F0D64EC3C4AF9934D3FC732279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserContext
struct  XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.XmlParserContext::_nt
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ____nt_0;
	// System.Xml.XmlNamespaceManager System.Xml.XmlParserContext::_nsMgr
	XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * ____nsMgr_1;
	// System.String System.Xml.XmlParserContext::_docTypeName
	String_t* ____docTypeName_2;
	// System.String System.Xml.XmlParserContext::_pubId
	String_t* ____pubId_3;
	// System.String System.Xml.XmlParserContext::_sysId
	String_t* ____sysId_4;
	// System.String System.Xml.XmlParserContext::_internalSubset
	String_t* ____internalSubset_5;
	// System.String System.Xml.XmlParserContext::_xmlLang
	String_t* ____xmlLang_6;
	// System.Xml.XmlSpace System.Xml.XmlParserContext::_xmlSpace
	int32_t ____xmlSpace_7;
	// System.String System.Xml.XmlParserContext::_baseURI
	String_t* ____baseURI_8;
	// System.Text.Encoding System.Xml.XmlParserContext::_encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ____encoding_9;

public:
	inline static int32_t get_offset_of__nt_0() { return static_cast<int32_t>(offsetof(XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279, ____nt_0)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get__nt_0() const { return ____nt_0; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of__nt_0() { return &____nt_0; }
	inline void set__nt_0(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		____nt_0 = value;
		Il2CppCodeGenWriteBarrier((&____nt_0), value);
	}

	inline static int32_t get_offset_of__nsMgr_1() { return static_cast<int32_t>(offsetof(XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279, ____nsMgr_1)); }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * get__nsMgr_1() const { return ____nsMgr_1; }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F ** get_address_of__nsMgr_1() { return &____nsMgr_1; }
	inline void set__nsMgr_1(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * value)
	{
		____nsMgr_1 = value;
		Il2CppCodeGenWriteBarrier((&____nsMgr_1), value);
	}

	inline static int32_t get_offset_of__docTypeName_2() { return static_cast<int32_t>(offsetof(XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279, ____docTypeName_2)); }
	inline String_t* get__docTypeName_2() const { return ____docTypeName_2; }
	inline String_t** get_address_of__docTypeName_2() { return &____docTypeName_2; }
	inline void set__docTypeName_2(String_t* value)
	{
		____docTypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&____docTypeName_2), value);
	}

	inline static int32_t get_offset_of__pubId_3() { return static_cast<int32_t>(offsetof(XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279, ____pubId_3)); }
	inline String_t* get__pubId_3() const { return ____pubId_3; }
	inline String_t** get_address_of__pubId_3() { return &____pubId_3; }
	inline void set__pubId_3(String_t* value)
	{
		____pubId_3 = value;
		Il2CppCodeGenWriteBarrier((&____pubId_3), value);
	}

	inline static int32_t get_offset_of__sysId_4() { return static_cast<int32_t>(offsetof(XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279, ____sysId_4)); }
	inline String_t* get__sysId_4() const { return ____sysId_4; }
	inline String_t** get_address_of__sysId_4() { return &____sysId_4; }
	inline void set__sysId_4(String_t* value)
	{
		____sysId_4 = value;
		Il2CppCodeGenWriteBarrier((&____sysId_4), value);
	}

	inline static int32_t get_offset_of__internalSubset_5() { return static_cast<int32_t>(offsetof(XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279, ____internalSubset_5)); }
	inline String_t* get__internalSubset_5() const { return ____internalSubset_5; }
	inline String_t** get_address_of__internalSubset_5() { return &____internalSubset_5; }
	inline void set__internalSubset_5(String_t* value)
	{
		____internalSubset_5 = value;
		Il2CppCodeGenWriteBarrier((&____internalSubset_5), value);
	}

	inline static int32_t get_offset_of__xmlLang_6() { return static_cast<int32_t>(offsetof(XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279, ____xmlLang_6)); }
	inline String_t* get__xmlLang_6() const { return ____xmlLang_6; }
	inline String_t** get_address_of__xmlLang_6() { return &____xmlLang_6; }
	inline void set__xmlLang_6(String_t* value)
	{
		____xmlLang_6 = value;
		Il2CppCodeGenWriteBarrier((&____xmlLang_6), value);
	}

	inline static int32_t get_offset_of__xmlSpace_7() { return static_cast<int32_t>(offsetof(XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279, ____xmlSpace_7)); }
	inline int32_t get__xmlSpace_7() const { return ____xmlSpace_7; }
	inline int32_t* get_address_of__xmlSpace_7() { return &____xmlSpace_7; }
	inline void set__xmlSpace_7(int32_t value)
	{
		____xmlSpace_7 = value;
	}

	inline static int32_t get_offset_of__baseURI_8() { return static_cast<int32_t>(offsetof(XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279, ____baseURI_8)); }
	inline String_t* get__baseURI_8() const { return ____baseURI_8; }
	inline String_t** get_address_of__baseURI_8() { return &____baseURI_8; }
	inline void set__baseURI_8(String_t* value)
	{
		____baseURI_8 = value;
		Il2CppCodeGenWriteBarrier((&____baseURI_8), value);
	}

	inline static int32_t get_offset_of__encoding_9() { return static_cast<int32_t>(offsetof(XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279, ____encoding_9)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get__encoding_9() const { return ____encoding_9; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of__encoding_9() { return &____encoding_9; }
	inline void set__encoding_9(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		____encoding_9 = value;
		Il2CppCodeGenWriteBarrier((&____encoding_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPARSERCONTEXT_TDC3A95452BC683F0D64EC3C4AF9934D3FC732279_H
#ifndef XMLREADERSETTINGS_T33E632C6BB215A5F2B4EAA81B289E027AF617F65_H
#define XMLREADERSETTINGS_T33E632C6BB215A5F2B4EAA81B289E027AF617F65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderSettings
struct  XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.XmlReaderSettings::useAsync
	bool ___useAsync_0;
	// System.Xml.XmlNameTable System.Xml.XmlReaderSettings::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_1;
	// System.Xml.XmlResolver System.Xml.XmlReaderSettings::xmlResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___xmlResolver_2;
	// System.Int32 System.Xml.XmlReaderSettings::lineNumberOffset
	int32_t ___lineNumberOffset_3;
	// System.Int32 System.Xml.XmlReaderSettings::linePositionOffset
	int32_t ___linePositionOffset_4;
	// System.Xml.ConformanceLevel System.Xml.XmlReaderSettings::conformanceLevel
	int32_t ___conformanceLevel_5;
	// System.Boolean System.Xml.XmlReaderSettings::checkCharacters
	bool ___checkCharacters_6;
	// System.Int64 System.Xml.XmlReaderSettings::maxCharactersInDocument
	int64_t ___maxCharactersInDocument_7;
	// System.Int64 System.Xml.XmlReaderSettings::maxCharactersFromEntities
	int64_t ___maxCharactersFromEntities_8;
	// System.Boolean System.Xml.XmlReaderSettings::ignoreWhitespace
	bool ___ignoreWhitespace_9;
	// System.Boolean System.Xml.XmlReaderSettings::ignorePIs
	bool ___ignorePIs_10;
	// System.Boolean System.Xml.XmlReaderSettings::ignoreComments
	bool ___ignoreComments_11;
	// System.Xml.DtdProcessing System.Xml.XmlReaderSettings::dtdProcessing
	int32_t ___dtdProcessing_12;
	// System.Xml.ValidationType System.Xml.XmlReaderSettings::validationType
	int32_t ___validationType_13;
	// System.Xml.Schema.XmlSchemaValidationFlags System.Xml.XmlReaderSettings::validationFlags
	int32_t ___validationFlags_14;
	// System.Xml.Schema.XmlSchemaSet System.Xml.XmlReaderSettings::schemas
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F * ___schemas_15;
	// System.Xml.Schema.ValidationEventHandler System.Xml.XmlReaderSettings::valEventHandler
	ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * ___valEventHandler_16;
	// System.Boolean System.Xml.XmlReaderSettings::closeInput
	bool ___closeInput_17;
	// System.Boolean System.Xml.XmlReaderSettings::isReadOnly
	bool ___isReadOnly_18;
	// System.Boolean System.Xml.XmlReaderSettings::<IsXmlResolverSet>k__BackingField
	bool ___U3CIsXmlResolverSetU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_useAsync_0() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___useAsync_0)); }
	inline bool get_useAsync_0() const { return ___useAsync_0; }
	inline bool* get_address_of_useAsync_0() { return &___useAsync_0; }
	inline void set_useAsync_0(bool value)
	{
		___useAsync_0 = value;
	}

	inline static int32_t get_offset_of_nameTable_1() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___nameTable_1)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_1() const { return ___nameTable_1; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_1() { return &___nameTable_1; }
	inline void set_nameTable_1(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_1), value);
	}

	inline static int32_t get_offset_of_xmlResolver_2() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___xmlResolver_2)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_xmlResolver_2() const { return ___xmlResolver_2; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_xmlResolver_2() { return &___xmlResolver_2; }
	inline void set_xmlResolver_2(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___xmlResolver_2 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_2), value);
	}

	inline static int32_t get_offset_of_lineNumberOffset_3() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___lineNumberOffset_3)); }
	inline int32_t get_lineNumberOffset_3() const { return ___lineNumberOffset_3; }
	inline int32_t* get_address_of_lineNumberOffset_3() { return &___lineNumberOffset_3; }
	inline void set_lineNumberOffset_3(int32_t value)
	{
		___lineNumberOffset_3 = value;
	}

	inline static int32_t get_offset_of_linePositionOffset_4() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___linePositionOffset_4)); }
	inline int32_t get_linePositionOffset_4() const { return ___linePositionOffset_4; }
	inline int32_t* get_address_of_linePositionOffset_4() { return &___linePositionOffset_4; }
	inline void set_linePositionOffset_4(int32_t value)
	{
		___linePositionOffset_4 = value;
	}

	inline static int32_t get_offset_of_conformanceLevel_5() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___conformanceLevel_5)); }
	inline int32_t get_conformanceLevel_5() const { return ___conformanceLevel_5; }
	inline int32_t* get_address_of_conformanceLevel_5() { return &___conformanceLevel_5; }
	inline void set_conformanceLevel_5(int32_t value)
	{
		___conformanceLevel_5 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_6() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___checkCharacters_6)); }
	inline bool get_checkCharacters_6() const { return ___checkCharacters_6; }
	inline bool* get_address_of_checkCharacters_6() { return &___checkCharacters_6; }
	inline void set_checkCharacters_6(bool value)
	{
		___checkCharacters_6 = value;
	}

	inline static int32_t get_offset_of_maxCharactersInDocument_7() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___maxCharactersInDocument_7)); }
	inline int64_t get_maxCharactersInDocument_7() const { return ___maxCharactersInDocument_7; }
	inline int64_t* get_address_of_maxCharactersInDocument_7() { return &___maxCharactersInDocument_7; }
	inline void set_maxCharactersInDocument_7(int64_t value)
	{
		___maxCharactersInDocument_7 = value;
	}

	inline static int32_t get_offset_of_maxCharactersFromEntities_8() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___maxCharactersFromEntities_8)); }
	inline int64_t get_maxCharactersFromEntities_8() const { return ___maxCharactersFromEntities_8; }
	inline int64_t* get_address_of_maxCharactersFromEntities_8() { return &___maxCharactersFromEntities_8; }
	inline void set_maxCharactersFromEntities_8(int64_t value)
	{
		___maxCharactersFromEntities_8 = value;
	}

	inline static int32_t get_offset_of_ignoreWhitespace_9() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___ignoreWhitespace_9)); }
	inline bool get_ignoreWhitespace_9() const { return ___ignoreWhitespace_9; }
	inline bool* get_address_of_ignoreWhitespace_9() { return &___ignoreWhitespace_9; }
	inline void set_ignoreWhitespace_9(bool value)
	{
		___ignoreWhitespace_9 = value;
	}

	inline static int32_t get_offset_of_ignorePIs_10() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___ignorePIs_10)); }
	inline bool get_ignorePIs_10() const { return ___ignorePIs_10; }
	inline bool* get_address_of_ignorePIs_10() { return &___ignorePIs_10; }
	inline void set_ignorePIs_10(bool value)
	{
		___ignorePIs_10 = value;
	}

	inline static int32_t get_offset_of_ignoreComments_11() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___ignoreComments_11)); }
	inline bool get_ignoreComments_11() const { return ___ignoreComments_11; }
	inline bool* get_address_of_ignoreComments_11() { return &___ignoreComments_11; }
	inline void set_ignoreComments_11(bool value)
	{
		___ignoreComments_11 = value;
	}

	inline static int32_t get_offset_of_dtdProcessing_12() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___dtdProcessing_12)); }
	inline int32_t get_dtdProcessing_12() const { return ___dtdProcessing_12; }
	inline int32_t* get_address_of_dtdProcessing_12() { return &___dtdProcessing_12; }
	inline void set_dtdProcessing_12(int32_t value)
	{
		___dtdProcessing_12 = value;
	}

	inline static int32_t get_offset_of_validationType_13() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___validationType_13)); }
	inline int32_t get_validationType_13() const { return ___validationType_13; }
	inline int32_t* get_address_of_validationType_13() { return &___validationType_13; }
	inline void set_validationType_13(int32_t value)
	{
		___validationType_13 = value;
	}

	inline static int32_t get_offset_of_validationFlags_14() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___validationFlags_14)); }
	inline int32_t get_validationFlags_14() const { return ___validationFlags_14; }
	inline int32_t* get_address_of_validationFlags_14() { return &___validationFlags_14; }
	inline void set_validationFlags_14(int32_t value)
	{
		___validationFlags_14 = value;
	}

	inline static int32_t get_offset_of_schemas_15() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___schemas_15)); }
	inline XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F * get_schemas_15() const { return ___schemas_15; }
	inline XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F ** get_address_of_schemas_15() { return &___schemas_15; }
	inline void set_schemas_15(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F * value)
	{
		___schemas_15 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_15), value);
	}

	inline static int32_t get_offset_of_valEventHandler_16() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___valEventHandler_16)); }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * get_valEventHandler_16() const { return ___valEventHandler_16; }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF ** get_address_of_valEventHandler_16() { return &___valEventHandler_16; }
	inline void set_valEventHandler_16(ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * value)
	{
		___valEventHandler_16 = value;
		Il2CppCodeGenWriteBarrier((&___valEventHandler_16), value);
	}

	inline static int32_t get_offset_of_closeInput_17() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___closeInput_17)); }
	inline bool get_closeInput_17() const { return ___closeInput_17; }
	inline bool* get_address_of_closeInput_17() { return &___closeInput_17; }
	inline void set_closeInput_17(bool value)
	{
		___closeInput_17 = value;
	}

	inline static int32_t get_offset_of_isReadOnly_18() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___isReadOnly_18)); }
	inline bool get_isReadOnly_18() const { return ___isReadOnly_18; }
	inline bool* get_address_of_isReadOnly_18() { return &___isReadOnly_18; }
	inline void set_isReadOnly_18(bool value)
	{
		___isReadOnly_18 = value;
	}

	inline static int32_t get_offset_of_U3CIsXmlResolverSetU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65, ___U3CIsXmlResolverSetU3Ek__BackingField_19)); }
	inline bool get_U3CIsXmlResolverSetU3Ek__BackingField_19() const { return ___U3CIsXmlResolverSetU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CIsXmlResolverSetU3Ek__BackingField_19() { return &___U3CIsXmlResolverSetU3Ek__BackingField_19; }
	inline void set_U3CIsXmlResolverSetU3Ek__BackingField_19(bool value)
	{
		___U3CIsXmlResolverSetU3Ek__BackingField_19 = value;
	}
};

struct XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65_StaticFields
{
public:
	// System.Nullable`1<System.Boolean> System.Xml.XmlReaderSettings::s_enableLegacyXmlSettings
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___s_enableLegacyXmlSettings_20;

public:
	inline static int32_t get_offset_of_s_enableLegacyXmlSettings_20() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65_StaticFields, ___s_enableLegacyXmlSettings_20)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_s_enableLegacyXmlSettings_20() const { return ___s_enableLegacyXmlSettings_20; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_s_enableLegacyXmlSettings_20() { return &___s_enableLegacyXmlSettings_20; }
	inline void set_s_enableLegacyXmlSettings_20(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___s_enableLegacyXmlSettings_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADERSETTINGS_T33E632C6BB215A5F2B4EAA81B289E027AF617F65_H
#ifndef XMLSQLBINARYREADER_T725CF548EC3FE5A2D7A7946F1A3E762C71A291DB_H
#define XMLSQLBINARYREADER_T725CF548EC3FE5A2D7A7946F1A3E762C71A291DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSqlBinaryReader
struct  XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB  : public XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB
{
public:
	// System.IO.Stream System.Xml.XmlSqlBinaryReader::inStrm
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___inStrm_8;
	// System.Byte[] System.Xml.XmlSqlBinaryReader::data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data_9;
	// System.Int32 System.Xml.XmlSqlBinaryReader::pos
	int32_t ___pos_10;
	// System.Int32 System.Xml.XmlSqlBinaryReader::mark
	int32_t ___mark_11;
	// System.Int32 System.Xml.XmlSqlBinaryReader::end
	int32_t ___end_12;
	// System.Int64 System.Xml.XmlSqlBinaryReader::offset
	int64_t ___offset_13;
	// System.Boolean System.Xml.XmlSqlBinaryReader::eof
	bool ___eof_14;
	// System.Boolean System.Xml.XmlSqlBinaryReader::sniffed
	bool ___sniffed_15;
	// System.Boolean System.Xml.XmlSqlBinaryReader::isEmpty
	bool ___isEmpty_16;
	// System.Int32 System.Xml.XmlSqlBinaryReader::docState
	int32_t ___docState_17;
	// System.Xml.XmlSqlBinaryReader/SymbolTables System.Xml.XmlSqlBinaryReader::symbolTables
	SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E  ___symbolTables_18;
	// System.Xml.XmlNameTable System.Xml.XmlSqlBinaryReader::xnt
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___xnt_19;
	// System.Boolean System.Xml.XmlSqlBinaryReader::xntFromSettings
	bool ___xntFromSettings_20;
	// System.String System.Xml.XmlSqlBinaryReader::xml
	String_t* ___xml_21;
	// System.String System.Xml.XmlSqlBinaryReader::xmlns
	String_t* ___xmlns_22;
	// System.String System.Xml.XmlSqlBinaryReader::nsxmlns
	String_t* ___nsxmlns_23;
	// System.String System.Xml.XmlSqlBinaryReader::baseUri
	String_t* ___baseUri_24;
	// System.Xml.XmlSqlBinaryReader/ScanState System.Xml.XmlSqlBinaryReader::state
	int32_t ___state_25;
	// System.Xml.XmlNodeType System.Xml.XmlSqlBinaryReader::nodetype
	int32_t ___nodetype_26;
	// System.Xml.BinXmlToken System.Xml.XmlSqlBinaryReader::token
	int32_t ___token_27;
	// System.Int32 System.Xml.XmlSqlBinaryReader::attrIndex
	int32_t ___attrIndex_28;
	// System.Xml.XmlSqlBinaryReader/QName System.Xml.XmlSqlBinaryReader::qnameOther
	QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F  ___qnameOther_29;
	// System.Xml.XmlSqlBinaryReader/QName System.Xml.XmlSqlBinaryReader::qnameElement
	QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F  ___qnameElement_30;
	// System.Xml.XmlNodeType System.Xml.XmlSqlBinaryReader::parentNodeType
	int32_t ___parentNodeType_31;
	// System.Xml.XmlSqlBinaryReader/ElemInfo[] System.Xml.XmlSqlBinaryReader::elementStack
	ElemInfoU5BU5D_t13B605E8ECDE26BBA310B5E8597770E0449F67BD* ___elementStack_32;
	// System.Int32 System.Xml.XmlSqlBinaryReader::elemDepth
	int32_t ___elemDepth_33;
	// System.Xml.XmlSqlBinaryReader/AttrInfo[] System.Xml.XmlSqlBinaryReader::attributes
	AttrInfoU5BU5D_tB6030FFAA6B95DE14B235E8A0533248A19EF12AF* ___attributes_34;
	// System.Int32[] System.Xml.XmlSqlBinaryReader::attrHashTbl
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___attrHashTbl_35;
	// System.Int32 System.Xml.XmlSqlBinaryReader::attrCount
	int32_t ___attrCount_36;
	// System.Int32 System.Xml.XmlSqlBinaryReader::posAfterAttrs
	int32_t ___posAfterAttrs_37;
	// System.Boolean System.Xml.XmlSqlBinaryReader::xmlspacePreserve
	bool ___xmlspacePreserve_38;
	// System.Int32 System.Xml.XmlSqlBinaryReader::tokLen
	int32_t ___tokLen_39;
	// System.Int32 System.Xml.XmlSqlBinaryReader::tokDataPos
	int32_t ___tokDataPos_40;
	// System.Boolean System.Xml.XmlSqlBinaryReader::hasTypedValue
	bool ___hasTypedValue_41;
	// System.Type System.Xml.XmlSqlBinaryReader::valueType
	Type_t * ___valueType_42;
	// System.String System.Xml.XmlSqlBinaryReader::stringValue
	String_t* ___stringValue_43;
	// System.Collections.Generic.Dictionary`2<System.String,System.Xml.XmlSqlBinaryReader/NamespaceDecl> System.Xml.XmlSqlBinaryReader::namespaces
	Dictionary_2_tBEF366884DEC0B0D63DF018EECE2F0E08A8F910E * ___namespaces_44;
	// System.Xml.XmlSqlBinaryReader/NestedBinXml System.Xml.XmlSqlBinaryReader::prevNameInfo
	NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175 * ___prevNameInfo_45;
	// System.Xml.XmlReader System.Xml.XmlSqlBinaryReader::textXmlReader
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * ___textXmlReader_46;
	// System.Boolean System.Xml.XmlSqlBinaryReader::closeInput
	bool ___closeInput_47;
	// System.Boolean System.Xml.XmlSqlBinaryReader::checkCharacters
	bool ___checkCharacters_48;
	// System.Boolean System.Xml.XmlSqlBinaryReader::ignoreWhitespace
	bool ___ignoreWhitespace_49;
	// System.Boolean System.Xml.XmlSqlBinaryReader::ignorePIs
	bool ___ignorePIs_50;
	// System.Boolean System.Xml.XmlSqlBinaryReader::ignoreComments
	bool ___ignoreComments_51;
	// System.Xml.DtdProcessing System.Xml.XmlSqlBinaryReader::dtdProcessing
	int32_t ___dtdProcessing_52;
	// System.Xml.SecureStringHasher System.Xml.XmlSqlBinaryReader::hasher
	SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 * ___hasher_53;
	// System.Xml.XmlCharType System.Xml.XmlSqlBinaryReader::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_54;
	// System.Text.Encoding System.Xml.XmlSqlBinaryReader::unicode
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___unicode_55;
	// System.Byte System.Xml.XmlSqlBinaryReader::version
	uint8_t ___version_56;

public:
	inline static int32_t get_offset_of_inStrm_8() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___inStrm_8)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_inStrm_8() const { return ___inStrm_8; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_inStrm_8() { return &___inStrm_8; }
	inline void set_inStrm_8(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___inStrm_8 = value;
		Il2CppCodeGenWriteBarrier((&___inStrm_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___data_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_data_9() const { return ___data_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_pos_10() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___pos_10)); }
	inline int32_t get_pos_10() const { return ___pos_10; }
	inline int32_t* get_address_of_pos_10() { return &___pos_10; }
	inline void set_pos_10(int32_t value)
	{
		___pos_10 = value;
	}

	inline static int32_t get_offset_of_mark_11() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___mark_11)); }
	inline int32_t get_mark_11() const { return ___mark_11; }
	inline int32_t* get_address_of_mark_11() { return &___mark_11; }
	inline void set_mark_11(int32_t value)
	{
		___mark_11 = value;
	}

	inline static int32_t get_offset_of_end_12() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___end_12)); }
	inline int32_t get_end_12() const { return ___end_12; }
	inline int32_t* get_address_of_end_12() { return &___end_12; }
	inline void set_end_12(int32_t value)
	{
		___end_12 = value;
	}

	inline static int32_t get_offset_of_offset_13() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___offset_13)); }
	inline int64_t get_offset_13() const { return ___offset_13; }
	inline int64_t* get_address_of_offset_13() { return &___offset_13; }
	inline void set_offset_13(int64_t value)
	{
		___offset_13 = value;
	}

	inline static int32_t get_offset_of_eof_14() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___eof_14)); }
	inline bool get_eof_14() const { return ___eof_14; }
	inline bool* get_address_of_eof_14() { return &___eof_14; }
	inline void set_eof_14(bool value)
	{
		___eof_14 = value;
	}

	inline static int32_t get_offset_of_sniffed_15() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___sniffed_15)); }
	inline bool get_sniffed_15() const { return ___sniffed_15; }
	inline bool* get_address_of_sniffed_15() { return &___sniffed_15; }
	inline void set_sniffed_15(bool value)
	{
		___sniffed_15 = value;
	}

	inline static int32_t get_offset_of_isEmpty_16() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___isEmpty_16)); }
	inline bool get_isEmpty_16() const { return ___isEmpty_16; }
	inline bool* get_address_of_isEmpty_16() { return &___isEmpty_16; }
	inline void set_isEmpty_16(bool value)
	{
		___isEmpty_16 = value;
	}

	inline static int32_t get_offset_of_docState_17() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___docState_17)); }
	inline int32_t get_docState_17() const { return ___docState_17; }
	inline int32_t* get_address_of_docState_17() { return &___docState_17; }
	inline void set_docState_17(int32_t value)
	{
		___docState_17 = value;
	}

	inline static int32_t get_offset_of_symbolTables_18() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___symbolTables_18)); }
	inline SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E  get_symbolTables_18() const { return ___symbolTables_18; }
	inline SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E * get_address_of_symbolTables_18() { return &___symbolTables_18; }
	inline void set_symbolTables_18(SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E  value)
	{
		___symbolTables_18 = value;
	}

	inline static int32_t get_offset_of_xnt_19() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___xnt_19)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_xnt_19() const { return ___xnt_19; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_xnt_19() { return &___xnt_19; }
	inline void set_xnt_19(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___xnt_19 = value;
		Il2CppCodeGenWriteBarrier((&___xnt_19), value);
	}

	inline static int32_t get_offset_of_xntFromSettings_20() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___xntFromSettings_20)); }
	inline bool get_xntFromSettings_20() const { return ___xntFromSettings_20; }
	inline bool* get_address_of_xntFromSettings_20() { return &___xntFromSettings_20; }
	inline void set_xntFromSettings_20(bool value)
	{
		___xntFromSettings_20 = value;
	}

	inline static int32_t get_offset_of_xml_21() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___xml_21)); }
	inline String_t* get_xml_21() const { return ___xml_21; }
	inline String_t** get_address_of_xml_21() { return &___xml_21; }
	inline void set_xml_21(String_t* value)
	{
		___xml_21 = value;
		Il2CppCodeGenWriteBarrier((&___xml_21), value);
	}

	inline static int32_t get_offset_of_xmlns_22() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___xmlns_22)); }
	inline String_t* get_xmlns_22() const { return ___xmlns_22; }
	inline String_t** get_address_of_xmlns_22() { return &___xmlns_22; }
	inline void set_xmlns_22(String_t* value)
	{
		___xmlns_22 = value;
		Il2CppCodeGenWriteBarrier((&___xmlns_22), value);
	}

	inline static int32_t get_offset_of_nsxmlns_23() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___nsxmlns_23)); }
	inline String_t* get_nsxmlns_23() const { return ___nsxmlns_23; }
	inline String_t** get_address_of_nsxmlns_23() { return &___nsxmlns_23; }
	inline void set_nsxmlns_23(String_t* value)
	{
		___nsxmlns_23 = value;
		Il2CppCodeGenWriteBarrier((&___nsxmlns_23), value);
	}

	inline static int32_t get_offset_of_baseUri_24() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___baseUri_24)); }
	inline String_t* get_baseUri_24() const { return ___baseUri_24; }
	inline String_t** get_address_of_baseUri_24() { return &___baseUri_24; }
	inline void set_baseUri_24(String_t* value)
	{
		___baseUri_24 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_24), value);
	}

	inline static int32_t get_offset_of_state_25() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___state_25)); }
	inline int32_t get_state_25() const { return ___state_25; }
	inline int32_t* get_address_of_state_25() { return &___state_25; }
	inline void set_state_25(int32_t value)
	{
		___state_25 = value;
	}

	inline static int32_t get_offset_of_nodetype_26() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___nodetype_26)); }
	inline int32_t get_nodetype_26() const { return ___nodetype_26; }
	inline int32_t* get_address_of_nodetype_26() { return &___nodetype_26; }
	inline void set_nodetype_26(int32_t value)
	{
		___nodetype_26 = value;
	}

	inline static int32_t get_offset_of_token_27() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___token_27)); }
	inline int32_t get_token_27() const { return ___token_27; }
	inline int32_t* get_address_of_token_27() { return &___token_27; }
	inline void set_token_27(int32_t value)
	{
		___token_27 = value;
	}

	inline static int32_t get_offset_of_attrIndex_28() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___attrIndex_28)); }
	inline int32_t get_attrIndex_28() const { return ___attrIndex_28; }
	inline int32_t* get_address_of_attrIndex_28() { return &___attrIndex_28; }
	inline void set_attrIndex_28(int32_t value)
	{
		___attrIndex_28 = value;
	}

	inline static int32_t get_offset_of_qnameOther_29() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___qnameOther_29)); }
	inline QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F  get_qnameOther_29() const { return ___qnameOther_29; }
	inline QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F * get_address_of_qnameOther_29() { return &___qnameOther_29; }
	inline void set_qnameOther_29(QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F  value)
	{
		___qnameOther_29 = value;
	}

	inline static int32_t get_offset_of_qnameElement_30() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___qnameElement_30)); }
	inline QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F  get_qnameElement_30() const { return ___qnameElement_30; }
	inline QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F * get_address_of_qnameElement_30() { return &___qnameElement_30; }
	inline void set_qnameElement_30(QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F  value)
	{
		___qnameElement_30 = value;
	}

	inline static int32_t get_offset_of_parentNodeType_31() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___parentNodeType_31)); }
	inline int32_t get_parentNodeType_31() const { return ___parentNodeType_31; }
	inline int32_t* get_address_of_parentNodeType_31() { return &___parentNodeType_31; }
	inline void set_parentNodeType_31(int32_t value)
	{
		___parentNodeType_31 = value;
	}

	inline static int32_t get_offset_of_elementStack_32() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___elementStack_32)); }
	inline ElemInfoU5BU5D_t13B605E8ECDE26BBA310B5E8597770E0449F67BD* get_elementStack_32() const { return ___elementStack_32; }
	inline ElemInfoU5BU5D_t13B605E8ECDE26BBA310B5E8597770E0449F67BD** get_address_of_elementStack_32() { return &___elementStack_32; }
	inline void set_elementStack_32(ElemInfoU5BU5D_t13B605E8ECDE26BBA310B5E8597770E0449F67BD* value)
	{
		___elementStack_32 = value;
		Il2CppCodeGenWriteBarrier((&___elementStack_32), value);
	}

	inline static int32_t get_offset_of_elemDepth_33() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___elemDepth_33)); }
	inline int32_t get_elemDepth_33() const { return ___elemDepth_33; }
	inline int32_t* get_address_of_elemDepth_33() { return &___elemDepth_33; }
	inline void set_elemDepth_33(int32_t value)
	{
		___elemDepth_33 = value;
	}

	inline static int32_t get_offset_of_attributes_34() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___attributes_34)); }
	inline AttrInfoU5BU5D_tB6030FFAA6B95DE14B235E8A0533248A19EF12AF* get_attributes_34() const { return ___attributes_34; }
	inline AttrInfoU5BU5D_tB6030FFAA6B95DE14B235E8A0533248A19EF12AF** get_address_of_attributes_34() { return &___attributes_34; }
	inline void set_attributes_34(AttrInfoU5BU5D_tB6030FFAA6B95DE14B235E8A0533248A19EF12AF* value)
	{
		___attributes_34 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_34), value);
	}

	inline static int32_t get_offset_of_attrHashTbl_35() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___attrHashTbl_35)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_attrHashTbl_35() const { return ___attrHashTbl_35; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_attrHashTbl_35() { return &___attrHashTbl_35; }
	inline void set_attrHashTbl_35(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___attrHashTbl_35 = value;
		Il2CppCodeGenWriteBarrier((&___attrHashTbl_35), value);
	}

	inline static int32_t get_offset_of_attrCount_36() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___attrCount_36)); }
	inline int32_t get_attrCount_36() const { return ___attrCount_36; }
	inline int32_t* get_address_of_attrCount_36() { return &___attrCount_36; }
	inline void set_attrCount_36(int32_t value)
	{
		___attrCount_36 = value;
	}

	inline static int32_t get_offset_of_posAfterAttrs_37() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___posAfterAttrs_37)); }
	inline int32_t get_posAfterAttrs_37() const { return ___posAfterAttrs_37; }
	inline int32_t* get_address_of_posAfterAttrs_37() { return &___posAfterAttrs_37; }
	inline void set_posAfterAttrs_37(int32_t value)
	{
		___posAfterAttrs_37 = value;
	}

	inline static int32_t get_offset_of_xmlspacePreserve_38() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___xmlspacePreserve_38)); }
	inline bool get_xmlspacePreserve_38() const { return ___xmlspacePreserve_38; }
	inline bool* get_address_of_xmlspacePreserve_38() { return &___xmlspacePreserve_38; }
	inline void set_xmlspacePreserve_38(bool value)
	{
		___xmlspacePreserve_38 = value;
	}

	inline static int32_t get_offset_of_tokLen_39() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___tokLen_39)); }
	inline int32_t get_tokLen_39() const { return ___tokLen_39; }
	inline int32_t* get_address_of_tokLen_39() { return &___tokLen_39; }
	inline void set_tokLen_39(int32_t value)
	{
		___tokLen_39 = value;
	}

	inline static int32_t get_offset_of_tokDataPos_40() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___tokDataPos_40)); }
	inline int32_t get_tokDataPos_40() const { return ___tokDataPos_40; }
	inline int32_t* get_address_of_tokDataPos_40() { return &___tokDataPos_40; }
	inline void set_tokDataPos_40(int32_t value)
	{
		___tokDataPos_40 = value;
	}

	inline static int32_t get_offset_of_hasTypedValue_41() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___hasTypedValue_41)); }
	inline bool get_hasTypedValue_41() const { return ___hasTypedValue_41; }
	inline bool* get_address_of_hasTypedValue_41() { return &___hasTypedValue_41; }
	inline void set_hasTypedValue_41(bool value)
	{
		___hasTypedValue_41 = value;
	}

	inline static int32_t get_offset_of_valueType_42() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___valueType_42)); }
	inline Type_t * get_valueType_42() const { return ___valueType_42; }
	inline Type_t ** get_address_of_valueType_42() { return &___valueType_42; }
	inline void set_valueType_42(Type_t * value)
	{
		___valueType_42 = value;
		Il2CppCodeGenWriteBarrier((&___valueType_42), value);
	}

	inline static int32_t get_offset_of_stringValue_43() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___stringValue_43)); }
	inline String_t* get_stringValue_43() const { return ___stringValue_43; }
	inline String_t** get_address_of_stringValue_43() { return &___stringValue_43; }
	inline void set_stringValue_43(String_t* value)
	{
		___stringValue_43 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_43), value);
	}

	inline static int32_t get_offset_of_namespaces_44() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___namespaces_44)); }
	inline Dictionary_2_tBEF366884DEC0B0D63DF018EECE2F0E08A8F910E * get_namespaces_44() const { return ___namespaces_44; }
	inline Dictionary_2_tBEF366884DEC0B0D63DF018EECE2F0E08A8F910E ** get_address_of_namespaces_44() { return &___namespaces_44; }
	inline void set_namespaces_44(Dictionary_2_tBEF366884DEC0B0D63DF018EECE2F0E08A8F910E * value)
	{
		___namespaces_44 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_44), value);
	}

	inline static int32_t get_offset_of_prevNameInfo_45() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___prevNameInfo_45)); }
	inline NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175 * get_prevNameInfo_45() const { return ___prevNameInfo_45; }
	inline NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175 ** get_address_of_prevNameInfo_45() { return &___prevNameInfo_45; }
	inline void set_prevNameInfo_45(NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175 * value)
	{
		___prevNameInfo_45 = value;
		Il2CppCodeGenWriteBarrier((&___prevNameInfo_45), value);
	}

	inline static int32_t get_offset_of_textXmlReader_46() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___textXmlReader_46)); }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * get_textXmlReader_46() const { return ___textXmlReader_46; }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB ** get_address_of_textXmlReader_46() { return &___textXmlReader_46; }
	inline void set_textXmlReader_46(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * value)
	{
		___textXmlReader_46 = value;
		Il2CppCodeGenWriteBarrier((&___textXmlReader_46), value);
	}

	inline static int32_t get_offset_of_closeInput_47() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___closeInput_47)); }
	inline bool get_closeInput_47() const { return ___closeInput_47; }
	inline bool* get_address_of_closeInput_47() { return &___closeInput_47; }
	inline void set_closeInput_47(bool value)
	{
		___closeInput_47 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_48() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___checkCharacters_48)); }
	inline bool get_checkCharacters_48() const { return ___checkCharacters_48; }
	inline bool* get_address_of_checkCharacters_48() { return &___checkCharacters_48; }
	inline void set_checkCharacters_48(bool value)
	{
		___checkCharacters_48 = value;
	}

	inline static int32_t get_offset_of_ignoreWhitespace_49() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___ignoreWhitespace_49)); }
	inline bool get_ignoreWhitespace_49() const { return ___ignoreWhitespace_49; }
	inline bool* get_address_of_ignoreWhitespace_49() { return &___ignoreWhitespace_49; }
	inline void set_ignoreWhitespace_49(bool value)
	{
		___ignoreWhitespace_49 = value;
	}

	inline static int32_t get_offset_of_ignorePIs_50() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___ignorePIs_50)); }
	inline bool get_ignorePIs_50() const { return ___ignorePIs_50; }
	inline bool* get_address_of_ignorePIs_50() { return &___ignorePIs_50; }
	inline void set_ignorePIs_50(bool value)
	{
		___ignorePIs_50 = value;
	}

	inline static int32_t get_offset_of_ignoreComments_51() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___ignoreComments_51)); }
	inline bool get_ignoreComments_51() const { return ___ignoreComments_51; }
	inline bool* get_address_of_ignoreComments_51() { return &___ignoreComments_51; }
	inline void set_ignoreComments_51(bool value)
	{
		___ignoreComments_51 = value;
	}

	inline static int32_t get_offset_of_dtdProcessing_52() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___dtdProcessing_52)); }
	inline int32_t get_dtdProcessing_52() const { return ___dtdProcessing_52; }
	inline int32_t* get_address_of_dtdProcessing_52() { return &___dtdProcessing_52; }
	inline void set_dtdProcessing_52(int32_t value)
	{
		___dtdProcessing_52 = value;
	}

	inline static int32_t get_offset_of_hasher_53() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___hasher_53)); }
	inline SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 * get_hasher_53() const { return ___hasher_53; }
	inline SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 ** get_address_of_hasher_53() { return &___hasher_53; }
	inline void set_hasher_53(SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 * value)
	{
		___hasher_53 = value;
		Il2CppCodeGenWriteBarrier((&___hasher_53), value);
	}

	inline static int32_t get_offset_of_xmlCharType_54() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___xmlCharType_54)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_54() const { return ___xmlCharType_54; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_54() { return &___xmlCharType_54; }
	inline void set_xmlCharType_54(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_54 = value;
	}

	inline static int32_t get_offset_of_unicode_55() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___unicode_55)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_unicode_55() const { return ___unicode_55; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_unicode_55() { return &___unicode_55; }
	inline void set_unicode_55(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___unicode_55 = value;
		Il2CppCodeGenWriteBarrier((&___unicode_55), value);
	}

	inline static int32_t get_offset_of_version_56() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB, ___version_56)); }
	inline uint8_t get_version_56() const { return ___version_56; }
	inline uint8_t* get_address_of_version_56() { return &___version_56; }
	inline void set_version_56(uint8_t value)
	{
		___version_56 = value;
	}
};

struct XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB_StaticFields
{
public:
	// System.Type System.Xml.XmlSqlBinaryReader::TypeOfObject
	Type_t * ___TypeOfObject_3;
	// System.Type System.Xml.XmlSqlBinaryReader::TypeOfString
	Type_t * ___TypeOfString_4;
	// System.Type[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlSqlBinaryReader::TokenTypeMap
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___TokenTypeMap_5;
	// System.Byte[] System.Xml.XmlSqlBinaryReader::XsdKatmaiTimeScaleToValueLengthMap
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___XsdKatmaiTimeScaleToValueLengthMap_6;
	// System.Xml.ReadState[] System.Xml.XmlSqlBinaryReader::ScanState2ReadState
	ReadStateU5BU5D_t807C186401CFAF2EE005FBEF4EC70DC421B9896D* ___ScanState2ReadState_7;

public:
	inline static int32_t get_offset_of_TypeOfObject_3() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB_StaticFields, ___TypeOfObject_3)); }
	inline Type_t * get_TypeOfObject_3() const { return ___TypeOfObject_3; }
	inline Type_t ** get_address_of_TypeOfObject_3() { return &___TypeOfObject_3; }
	inline void set_TypeOfObject_3(Type_t * value)
	{
		___TypeOfObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfObject_3), value);
	}

	inline static int32_t get_offset_of_TypeOfString_4() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB_StaticFields, ___TypeOfString_4)); }
	inline Type_t * get_TypeOfString_4() const { return ___TypeOfString_4; }
	inline Type_t ** get_address_of_TypeOfString_4() { return &___TypeOfString_4; }
	inline void set_TypeOfString_4(Type_t * value)
	{
		___TypeOfString_4 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfString_4), value);
	}

	inline static int32_t get_offset_of_TokenTypeMap_5() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB_StaticFields, ___TokenTypeMap_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_TokenTypeMap_5() const { return ___TokenTypeMap_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_TokenTypeMap_5() { return &___TokenTypeMap_5; }
	inline void set_TokenTypeMap_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___TokenTypeMap_5 = value;
		Il2CppCodeGenWriteBarrier((&___TokenTypeMap_5), value);
	}

	inline static int32_t get_offset_of_XsdKatmaiTimeScaleToValueLengthMap_6() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB_StaticFields, ___XsdKatmaiTimeScaleToValueLengthMap_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_XsdKatmaiTimeScaleToValueLengthMap_6() const { return ___XsdKatmaiTimeScaleToValueLengthMap_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_XsdKatmaiTimeScaleToValueLengthMap_6() { return &___XsdKatmaiTimeScaleToValueLengthMap_6; }
	inline void set_XsdKatmaiTimeScaleToValueLengthMap_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___XsdKatmaiTimeScaleToValueLengthMap_6 = value;
		Il2CppCodeGenWriteBarrier((&___XsdKatmaiTimeScaleToValueLengthMap_6), value);
	}

	inline static int32_t get_offset_of_ScanState2ReadState_7() { return static_cast<int32_t>(offsetof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB_StaticFields, ___ScanState2ReadState_7)); }
	inline ReadStateU5BU5D_t807C186401CFAF2EE005FBEF4EC70DC421B9896D* get_ScanState2ReadState_7() const { return ___ScanState2ReadState_7; }
	inline ReadStateU5BU5D_t807C186401CFAF2EE005FBEF4EC70DC421B9896D** get_address_of_ScanState2ReadState_7() { return &___ScanState2ReadState_7; }
	inline void set_ScanState2ReadState_7(ReadStateU5BU5D_t807C186401CFAF2EE005FBEF4EC70DC421B9896D* value)
	{
		___ScanState2ReadState_7 = value;
		Il2CppCodeGenWriteBarrier((&___ScanState2ReadState_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSQLBINARYREADER_T725CF548EC3FE5A2D7A7946F1A3E762C71A291DB_H
#ifndef ELEMINFO_T930901F4C4D70BD460913E85E8B87AB5C5A8571F_H
#define ELEMINFO_T930901F4C4D70BD460913E85E8B87AB5C5A8571F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSqlBinaryReader/ElemInfo
struct  ElemInfo_t930901F4C4D70BD460913E85E8B87AB5C5A8571F 
{
public:
	// System.Xml.XmlSqlBinaryReader/QName System.Xml.XmlSqlBinaryReader/ElemInfo::name
	QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F  ___name_0;
	// System.String System.Xml.XmlSqlBinaryReader/ElemInfo::xmlLang
	String_t* ___xmlLang_1;
	// System.Xml.XmlSpace System.Xml.XmlSqlBinaryReader/ElemInfo::xmlSpace
	int32_t ___xmlSpace_2;
	// System.Boolean System.Xml.XmlSqlBinaryReader/ElemInfo::xmlspacePreserve
	bool ___xmlspacePreserve_3;
	// System.Xml.XmlSqlBinaryReader/NamespaceDecl System.Xml.XmlSqlBinaryReader/ElemInfo::nsdecls
	NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B * ___nsdecls_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ElemInfo_t930901F4C4D70BD460913E85E8B87AB5C5A8571F, ___name_0)); }
	inline QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F  get_name_0() const { return ___name_0; }
	inline QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F * get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F  value)
	{
		___name_0 = value;
	}

	inline static int32_t get_offset_of_xmlLang_1() { return static_cast<int32_t>(offsetof(ElemInfo_t930901F4C4D70BD460913E85E8B87AB5C5A8571F, ___xmlLang_1)); }
	inline String_t* get_xmlLang_1() const { return ___xmlLang_1; }
	inline String_t** get_address_of_xmlLang_1() { return &___xmlLang_1; }
	inline void set_xmlLang_1(String_t* value)
	{
		___xmlLang_1 = value;
		Il2CppCodeGenWriteBarrier((&___xmlLang_1), value);
	}

	inline static int32_t get_offset_of_xmlSpace_2() { return static_cast<int32_t>(offsetof(ElemInfo_t930901F4C4D70BD460913E85E8B87AB5C5A8571F, ___xmlSpace_2)); }
	inline int32_t get_xmlSpace_2() const { return ___xmlSpace_2; }
	inline int32_t* get_address_of_xmlSpace_2() { return &___xmlSpace_2; }
	inline void set_xmlSpace_2(int32_t value)
	{
		___xmlSpace_2 = value;
	}

	inline static int32_t get_offset_of_xmlspacePreserve_3() { return static_cast<int32_t>(offsetof(ElemInfo_t930901F4C4D70BD460913E85E8B87AB5C5A8571F, ___xmlspacePreserve_3)); }
	inline bool get_xmlspacePreserve_3() const { return ___xmlspacePreserve_3; }
	inline bool* get_address_of_xmlspacePreserve_3() { return &___xmlspacePreserve_3; }
	inline void set_xmlspacePreserve_3(bool value)
	{
		___xmlspacePreserve_3 = value;
	}

	inline static int32_t get_offset_of_nsdecls_4() { return static_cast<int32_t>(offsetof(ElemInfo_t930901F4C4D70BD460913E85E8B87AB5C5A8571F, ___nsdecls_4)); }
	inline NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B * get_nsdecls_4() const { return ___nsdecls_4; }
	inline NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B ** get_address_of_nsdecls_4() { return &___nsdecls_4; }
	inline void set_nsdecls_4(NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B * value)
	{
		___nsdecls_4 = value;
		Il2CppCodeGenWriteBarrier((&___nsdecls_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlSqlBinaryReader/ElemInfo
struct ElemInfo_t930901F4C4D70BD460913E85E8B87AB5C5A8571F_marshaled_pinvoke
{
	QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F_marshaled_pinvoke ___name_0;
	char* ___xmlLang_1;
	int32_t ___xmlSpace_2;
	int32_t ___xmlspacePreserve_3;
	NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B * ___nsdecls_4;
};
// Native definition for COM marshalling of System.Xml.XmlSqlBinaryReader/ElemInfo
struct ElemInfo_t930901F4C4D70BD460913E85E8B87AB5C5A8571F_marshaled_com
{
	QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F_marshaled_com ___name_0;
	Il2CppChar* ___xmlLang_1;
	int32_t ___xmlSpace_2;
	int32_t ___xmlspacePreserve_3;
	NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B * ___nsdecls_4;
};
#endif // ELEMINFO_T930901F4C4D70BD460913E85E8B87AB5C5A8571F_H
#ifndef XMLTEXTREADERIMPL_T393737BE3F9168D966F164C2FD840C3494DEDE61_H
#define XMLTEXTREADERIMPL_T393737BE3F9168D966F164C2FD840C3494DEDE61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl
struct  XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61  : public XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB
{
public:
	// System.Boolean System.Xml.XmlTextReaderImpl::useAsync
	bool ___useAsync_3;
	// System.Xml.XmlTextReaderImpl/LaterInitParam System.Xml.XmlTextReaderImpl::laterInitParam
	LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF * ___laterInitParam_4;
	// System.Xml.XmlCharType System.Xml.XmlTextReaderImpl::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_5;
	// System.Xml.XmlTextReaderImpl/ParsingState System.Xml.XmlTextReaderImpl::ps
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2  ___ps_6;
	// System.Xml.XmlTextReaderImpl/ParsingFunction System.Xml.XmlTextReaderImpl::parsingFunction
	int32_t ___parsingFunction_7;
	// System.Xml.XmlTextReaderImpl/ParsingFunction System.Xml.XmlTextReaderImpl::nextParsingFunction
	int32_t ___nextParsingFunction_8;
	// System.Xml.XmlTextReaderImpl/ParsingFunction System.Xml.XmlTextReaderImpl::nextNextParsingFunction
	int32_t ___nextNextParsingFunction_9;
	// System.Xml.XmlTextReaderImpl/NodeData[] System.Xml.XmlTextReaderImpl::nodes
	NodeDataU5BU5D_tD5EBFFECEF67B4D0D7D521A99182D3B2814E07BE* ___nodes_10;
	// System.Xml.XmlTextReaderImpl/NodeData System.Xml.XmlTextReaderImpl::curNode
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF * ___curNode_11;
	// System.Int32 System.Xml.XmlTextReaderImpl::index
	int32_t ___index_12;
	// System.Int32 System.Xml.XmlTextReaderImpl::curAttrIndex
	int32_t ___curAttrIndex_13;
	// System.Int32 System.Xml.XmlTextReaderImpl::attrCount
	int32_t ___attrCount_14;
	// System.Int32 System.Xml.XmlTextReaderImpl::attrHashtable
	int32_t ___attrHashtable_15;
	// System.Int32 System.Xml.XmlTextReaderImpl::attrDuplWalkCount
	int32_t ___attrDuplWalkCount_16;
	// System.Boolean System.Xml.XmlTextReaderImpl::attrNeedNamespaceLookup
	bool ___attrNeedNamespaceLookup_17;
	// System.Boolean System.Xml.XmlTextReaderImpl::fullAttrCleanup
	bool ___fullAttrCleanup_18;
	// System.Xml.XmlTextReaderImpl/NodeData[] System.Xml.XmlTextReaderImpl::attrDuplSortingArray
	NodeDataU5BU5D_tD5EBFFECEF67B4D0D7D521A99182D3B2814E07BE* ___attrDuplSortingArray_19;
	// System.Xml.XmlNameTable System.Xml.XmlTextReaderImpl::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_20;
	// System.Boolean System.Xml.XmlTextReaderImpl::nameTableFromSettings
	bool ___nameTableFromSettings_21;
	// System.Xml.XmlResolver System.Xml.XmlTextReaderImpl::xmlResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___xmlResolver_22;
	// System.String System.Xml.XmlTextReaderImpl::url
	String_t* ___url_23;
	// System.Boolean System.Xml.XmlTextReaderImpl::normalize
	bool ___normalize_24;
	// System.Boolean System.Xml.XmlTextReaderImpl::supportNamespaces
	bool ___supportNamespaces_25;
	// System.Xml.WhitespaceHandling System.Xml.XmlTextReaderImpl::whitespaceHandling
	int32_t ___whitespaceHandling_26;
	// System.Xml.DtdProcessing System.Xml.XmlTextReaderImpl::dtdProcessing
	int32_t ___dtdProcessing_27;
	// System.Xml.EntityHandling System.Xml.XmlTextReaderImpl::entityHandling
	int32_t ___entityHandling_28;
	// System.Boolean System.Xml.XmlTextReaderImpl::ignorePIs
	bool ___ignorePIs_29;
	// System.Boolean System.Xml.XmlTextReaderImpl::ignoreComments
	bool ___ignoreComments_30;
	// System.Boolean System.Xml.XmlTextReaderImpl::checkCharacters
	bool ___checkCharacters_31;
	// System.Int32 System.Xml.XmlTextReaderImpl::lineNumberOffset
	int32_t ___lineNumberOffset_32;
	// System.Int32 System.Xml.XmlTextReaderImpl::linePositionOffset
	int32_t ___linePositionOffset_33;
	// System.Boolean System.Xml.XmlTextReaderImpl::closeInput
	bool ___closeInput_34;
	// System.Int64 System.Xml.XmlTextReaderImpl::maxCharactersInDocument
	int64_t ___maxCharactersInDocument_35;
	// System.Int64 System.Xml.XmlTextReaderImpl::maxCharactersFromEntities
	int64_t ___maxCharactersFromEntities_36;
	// System.Boolean System.Xml.XmlTextReaderImpl::v1Compat
	bool ___v1Compat_37;
	// System.Xml.XmlNamespaceManager System.Xml.XmlTextReaderImpl::namespaceManager
	XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * ___namespaceManager_38;
	// System.String System.Xml.XmlTextReaderImpl::lastPrefix
	String_t* ___lastPrefix_39;
	// System.Xml.XmlTextReaderImpl/XmlContext System.Xml.XmlTextReaderImpl::xmlContext
	XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52 * ___xmlContext_40;
	// System.Xml.XmlTextReaderImpl/ParsingState[] System.Xml.XmlTextReaderImpl::parsingStatesStack
	ParsingStateU5BU5D_t7A5096046F9BF1BA70E9696B2A2479272FC65739* ___parsingStatesStack_41;
	// System.Int32 System.Xml.XmlTextReaderImpl::parsingStatesStackTop
	int32_t ___parsingStatesStackTop_42;
	// System.String System.Xml.XmlTextReaderImpl::reportedBaseUri
	String_t* ___reportedBaseUri_43;
	// System.Text.Encoding System.Xml.XmlTextReaderImpl::reportedEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___reportedEncoding_44;
	// System.Xml.IDtdInfo System.Xml.XmlTextReaderImpl::dtdInfo
	RuntimeObject* ___dtdInfo_45;
	// System.Xml.XmlNodeType System.Xml.XmlTextReaderImpl::fragmentType
	int32_t ___fragmentType_46;
	// System.Xml.XmlParserContext System.Xml.XmlTextReaderImpl::fragmentParserContext
	XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279 * ___fragmentParserContext_47;
	// System.Boolean System.Xml.XmlTextReaderImpl::fragment
	bool ___fragment_48;
	// System.Xml.IncrementalReadDecoder System.Xml.XmlTextReaderImpl::incReadDecoder
	IncrementalReadDecoder_t787BFB5889B01B88DDA030C503A0C2E0525CA723 * ___incReadDecoder_49;
	// System.Xml.XmlTextReaderImpl/IncrementalReadState System.Xml.XmlTextReaderImpl::incReadState
	int32_t ___incReadState_50;
	// System.Xml.LineInfo System.Xml.XmlTextReaderImpl::incReadLineInfo
	LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5  ___incReadLineInfo_51;
	// System.Int32 System.Xml.XmlTextReaderImpl::incReadDepth
	int32_t ___incReadDepth_52;
	// System.Int32 System.Xml.XmlTextReaderImpl::incReadLeftStartPos
	int32_t ___incReadLeftStartPos_53;
	// System.Int32 System.Xml.XmlTextReaderImpl::incReadLeftEndPos
	int32_t ___incReadLeftEndPos_54;
	// System.Int32 System.Xml.XmlTextReaderImpl::attributeValueBaseEntityId
	int32_t ___attributeValueBaseEntityId_55;
	// System.Boolean System.Xml.XmlTextReaderImpl::emptyEntityInAttributeResolved
	bool ___emptyEntityInAttributeResolved_56;
	// System.Xml.IValidationEventHandling System.Xml.XmlTextReaderImpl::validationEventHandling
	RuntimeObject* ___validationEventHandling_57;
	// System.Xml.XmlTextReaderImpl/OnDefaultAttributeUseDelegate System.Xml.XmlTextReaderImpl::onDefaultAttributeUse
	OnDefaultAttributeUseDelegate_tE83A97BD37E08D3C5AA377CEA6388782D99EA34D * ___onDefaultAttributeUse_58;
	// System.Boolean System.Xml.XmlTextReaderImpl::validatingReaderCompatFlag
	bool ___validatingReaderCompatFlag_59;
	// System.Boolean System.Xml.XmlTextReaderImpl::addDefaultAttributesAndNormalize
	bool ___addDefaultAttributesAndNormalize_60;
	// System.Text.StringBuilder System.Xml.XmlTextReaderImpl::stringBuilder
	StringBuilder_t * ___stringBuilder_61;
	// System.Boolean System.Xml.XmlTextReaderImpl::rootElementParsed
	bool ___rootElementParsed_62;
	// System.Boolean System.Xml.XmlTextReaderImpl::standalone
	bool ___standalone_63;
	// System.Int32 System.Xml.XmlTextReaderImpl::nextEntityId
	int32_t ___nextEntityId_64;
	// System.Xml.XmlTextReaderImpl/ParsingMode System.Xml.XmlTextReaderImpl::parsingMode
	int32_t ___parsingMode_65;
	// System.Xml.ReadState System.Xml.XmlTextReaderImpl::readState
	int32_t ___readState_66;
	// System.Xml.IDtdEntityInfo System.Xml.XmlTextReaderImpl::lastEntity
	RuntimeObject* ___lastEntity_67;
	// System.Boolean System.Xml.XmlTextReaderImpl::afterResetState
	bool ___afterResetState_68;
	// System.Int32 System.Xml.XmlTextReaderImpl::documentStartBytePos
	int32_t ___documentStartBytePos_69;
	// System.Int32 System.Xml.XmlTextReaderImpl::readValueOffset
	int32_t ___readValueOffset_70;
	// System.Int64 System.Xml.XmlTextReaderImpl::charactersInDocument
	int64_t ___charactersInDocument_71;
	// System.Int64 System.Xml.XmlTextReaderImpl::charactersFromEntities
	int64_t ___charactersFromEntities_72;
	// System.Collections.Generic.Dictionary`2<System.Xml.IDtdEntityInfo,System.Xml.IDtdEntityInfo> System.Xml.XmlTextReaderImpl::currentEntities
	Dictionary_2_tD00930653EC02E03C14ADE0FF8737B6193A5FE1B * ___currentEntities_73;
	// System.Boolean System.Xml.XmlTextReaderImpl::disableUndeclaredEntityCheck
	bool ___disableUndeclaredEntityCheck_74;
	// System.Xml.XmlReader System.Xml.XmlTextReaderImpl::outerReader
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * ___outerReader_75;
	// System.Boolean System.Xml.XmlTextReaderImpl::xmlResolverIsSet
	bool ___xmlResolverIsSet_76;
	// System.String System.Xml.XmlTextReaderImpl::Xml
	String_t* ___Xml_77;
	// System.String System.Xml.XmlTextReaderImpl::XmlNs
	String_t* ___XmlNs_78;
	// System.Threading.Tasks.Task`1<System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>> System.Xml.XmlTextReaderImpl::parseText_dummyTask
	Task_1_tFB42A7666202CD6CD81E6BF0FF63815EE40E621D * ___parseText_dummyTask_79;

public:
	inline static int32_t get_offset_of_useAsync_3() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___useAsync_3)); }
	inline bool get_useAsync_3() const { return ___useAsync_3; }
	inline bool* get_address_of_useAsync_3() { return &___useAsync_3; }
	inline void set_useAsync_3(bool value)
	{
		___useAsync_3 = value;
	}

	inline static int32_t get_offset_of_laterInitParam_4() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___laterInitParam_4)); }
	inline LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF * get_laterInitParam_4() const { return ___laterInitParam_4; }
	inline LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF ** get_address_of_laterInitParam_4() { return &___laterInitParam_4; }
	inline void set_laterInitParam_4(LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF * value)
	{
		___laterInitParam_4 = value;
		Il2CppCodeGenWriteBarrier((&___laterInitParam_4), value);
	}

	inline static int32_t get_offset_of_xmlCharType_5() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___xmlCharType_5)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_5() const { return ___xmlCharType_5; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_5() { return &___xmlCharType_5; }
	inline void set_xmlCharType_5(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_5 = value;
	}

	inline static int32_t get_offset_of_ps_6() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___ps_6)); }
	inline ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2  get_ps_6() const { return ___ps_6; }
	inline ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2 * get_address_of_ps_6() { return &___ps_6; }
	inline void set_ps_6(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2  value)
	{
		___ps_6 = value;
	}

	inline static int32_t get_offset_of_parsingFunction_7() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___parsingFunction_7)); }
	inline int32_t get_parsingFunction_7() const { return ___parsingFunction_7; }
	inline int32_t* get_address_of_parsingFunction_7() { return &___parsingFunction_7; }
	inline void set_parsingFunction_7(int32_t value)
	{
		___parsingFunction_7 = value;
	}

	inline static int32_t get_offset_of_nextParsingFunction_8() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___nextParsingFunction_8)); }
	inline int32_t get_nextParsingFunction_8() const { return ___nextParsingFunction_8; }
	inline int32_t* get_address_of_nextParsingFunction_8() { return &___nextParsingFunction_8; }
	inline void set_nextParsingFunction_8(int32_t value)
	{
		___nextParsingFunction_8 = value;
	}

	inline static int32_t get_offset_of_nextNextParsingFunction_9() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___nextNextParsingFunction_9)); }
	inline int32_t get_nextNextParsingFunction_9() const { return ___nextNextParsingFunction_9; }
	inline int32_t* get_address_of_nextNextParsingFunction_9() { return &___nextNextParsingFunction_9; }
	inline void set_nextNextParsingFunction_9(int32_t value)
	{
		___nextNextParsingFunction_9 = value;
	}

	inline static int32_t get_offset_of_nodes_10() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___nodes_10)); }
	inline NodeDataU5BU5D_tD5EBFFECEF67B4D0D7D521A99182D3B2814E07BE* get_nodes_10() const { return ___nodes_10; }
	inline NodeDataU5BU5D_tD5EBFFECEF67B4D0D7D521A99182D3B2814E07BE** get_address_of_nodes_10() { return &___nodes_10; }
	inline void set_nodes_10(NodeDataU5BU5D_tD5EBFFECEF67B4D0D7D521A99182D3B2814E07BE* value)
	{
		___nodes_10 = value;
		Il2CppCodeGenWriteBarrier((&___nodes_10), value);
	}

	inline static int32_t get_offset_of_curNode_11() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___curNode_11)); }
	inline NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF * get_curNode_11() const { return ___curNode_11; }
	inline NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF ** get_address_of_curNode_11() { return &___curNode_11; }
	inline void set_curNode_11(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF * value)
	{
		___curNode_11 = value;
		Il2CppCodeGenWriteBarrier((&___curNode_11), value);
	}

	inline static int32_t get_offset_of_index_12() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___index_12)); }
	inline int32_t get_index_12() const { return ___index_12; }
	inline int32_t* get_address_of_index_12() { return &___index_12; }
	inline void set_index_12(int32_t value)
	{
		___index_12 = value;
	}

	inline static int32_t get_offset_of_curAttrIndex_13() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___curAttrIndex_13)); }
	inline int32_t get_curAttrIndex_13() const { return ___curAttrIndex_13; }
	inline int32_t* get_address_of_curAttrIndex_13() { return &___curAttrIndex_13; }
	inline void set_curAttrIndex_13(int32_t value)
	{
		___curAttrIndex_13 = value;
	}

	inline static int32_t get_offset_of_attrCount_14() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___attrCount_14)); }
	inline int32_t get_attrCount_14() const { return ___attrCount_14; }
	inline int32_t* get_address_of_attrCount_14() { return &___attrCount_14; }
	inline void set_attrCount_14(int32_t value)
	{
		___attrCount_14 = value;
	}

	inline static int32_t get_offset_of_attrHashtable_15() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___attrHashtable_15)); }
	inline int32_t get_attrHashtable_15() const { return ___attrHashtable_15; }
	inline int32_t* get_address_of_attrHashtable_15() { return &___attrHashtable_15; }
	inline void set_attrHashtable_15(int32_t value)
	{
		___attrHashtable_15 = value;
	}

	inline static int32_t get_offset_of_attrDuplWalkCount_16() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___attrDuplWalkCount_16)); }
	inline int32_t get_attrDuplWalkCount_16() const { return ___attrDuplWalkCount_16; }
	inline int32_t* get_address_of_attrDuplWalkCount_16() { return &___attrDuplWalkCount_16; }
	inline void set_attrDuplWalkCount_16(int32_t value)
	{
		___attrDuplWalkCount_16 = value;
	}

	inline static int32_t get_offset_of_attrNeedNamespaceLookup_17() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___attrNeedNamespaceLookup_17)); }
	inline bool get_attrNeedNamespaceLookup_17() const { return ___attrNeedNamespaceLookup_17; }
	inline bool* get_address_of_attrNeedNamespaceLookup_17() { return &___attrNeedNamespaceLookup_17; }
	inline void set_attrNeedNamespaceLookup_17(bool value)
	{
		___attrNeedNamespaceLookup_17 = value;
	}

	inline static int32_t get_offset_of_fullAttrCleanup_18() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___fullAttrCleanup_18)); }
	inline bool get_fullAttrCleanup_18() const { return ___fullAttrCleanup_18; }
	inline bool* get_address_of_fullAttrCleanup_18() { return &___fullAttrCleanup_18; }
	inline void set_fullAttrCleanup_18(bool value)
	{
		___fullAttrCleanup_18 = value;
	}

	inline static int32_t get_offset_of_attrDuplSortingArray_19() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___attrDuplSortingArray_19)); }
	inline NodeDataU5BU5D_tD5EBFFECEF67B4D0D7D521A99182D3B2814E07BE* get_attrDuplSortingArray_19() const { return ___attrDuplSortingArray_19; }
	inline NodeDataU5BU5D_tD5EBFFECEF67B4D0D7D521A99182D3B2814E07BE** get_address_of_attrDuplSortingArray_19() { return &___attrDuplSortingArray_19; }
	inline void set_attrDuplSortingArray_19(NodeDataU5BU5D_tD5EBFFECEF67B4D0D7D521A99182D3B2814E07BE* value)
	{
		___attrDuplSortingArray_19 = value;
		Il2CppCodeGenWriteBarrier((&___attrDuplSortingArray_19), value);
	}

	inline static int32_t get_offset_of_nameTable_20() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___nameTable_20)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_20() const { return ___nameTable_20; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_20() { return &___nameTable_20; }
	inline void set_nameTable_20(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_20 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_20), value);
	}

	inline static int32_t get_offset_of_nameTableFromSettings_21() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___nameTableFromSettings_21)); }
	inline bool get_nameTableFromSettings_21() const { return ___nameTableFromSettings_21; }
	inline bool* get_address_of_nameTableFromSettings_21() { return &___nameTableFromSettings_21; }
	inline void set_nameTableFromSettings_21(bool value)
	{
		___nameTableFromSettings_21 = value;
	}

	inline static int32_t get_offset_of_xmlResolver_22() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___xmlResolver_22)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_xmlResolver_22() const { return ___xmlResolver_22; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_xmlResolver_22() { return &___xmlResolver_22; }
	inline void set_xmlResolver_22(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___xmlResolver_22 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_22), value);
	}

	inline static int32_t get_offset_of_url_23() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___url_23)); }
	inline String_t* get_url_23() const { return ___url_23; }
	inline String_t** get_address_of_url_23() { return &___url_23; }
	inline void set_url_23(String_t* value)
	{
		___url_23 = value;
		Il2CppCodeGenWriteBarrier((&___url_23), value);
	}

	inline static int32_t get_offset_of_normalize_24() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___normalize_24)); }
	inline bool get_normalize_24() const { return ___normalize_24; }
	inline bool* get_address_of_normalize_24() { return &___normalize_24; }
	inline void set_normalize_24(bool value)
	{
		___normalize_24 = value;
	}

	inline static int32_t get_offset_of_supportNamespaces_25() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___supportNamespaces_25)); }
	inline bool get_supportNamespaces_25() const { return ___supportNamespaces_25; }
	inline bool* get_address_of_supportNamespaces_25() { return &___supportNamespaces_25; }
	inline void set_supportNamespaces_25(bool value)
	{
		___supportNamespaces_25 = value;
	}

	inline static int32_t get_offset_of_whitespaceHandling_26() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___whitespaceHandling_26)); }
	inline int32_t get_whitespaceHandling_26() const { return ___whitespaceHandling_26; }
	inline int32_t* get_address_of_whitespaceHandling_26() { return &___whitespaceHandling_26; }
	inline void set_whitespaceHandling_26(int32_t value)
	{
		___whitespaceHandling_26 = value;
	}

	inline static int32_t get_offset_of_dtdProcessing_27() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___dtdProcessing_27)); }
	inline int32_t get_dtdProcessing_27() const { return ___dtdProcessing_27; }
	inline int32_t* get_address_of_dtdProcessing_27() { return &___dtdProcessing_27; }
	inline void set_dtdProcessing_27(int32_t value)
	{
		___dtdProcessing_27 = value;
	}

	inline static int32_t get_offset_of_entityHandling_28() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___entityHandling_28)); }
	inline int32_t get_entityHandling_28() const { return ___entityHandling_28; }
	inline int32_t* get_address_of_entityHandling_28() { return &___entityHandling_28; }
	inline void set_entityHandling_28(int32_t value)
	{
		___entityHandling_28 = value;
	}

	inline static int32_t get_offset_of_ignorePIs_29() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___ignorePIs_29)); }
	inline bool get_ignorePIs_29() const { return ___ignorePIs_29; }
	inline bool* get_address_of_ignorePIs_29() { return &___ignorePIs_29; }
	inline void set_ignorePIs_29(bool value)
	{
		___ignorePIs_29 = value;
	}

	inline static int32_t get_offset_of_ignoreComments_30() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___ignoreComments_30)); }
	inline bool get_ignoreComments_30() const { return ___ignoreComments_30; }
	inline bool* get_address_of_ignoreComments_30() { return &___ignoreComments_30; }
	inline void set_ignoreComments_30(bool value)
	{
		___ignoreComments_30 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_31() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___checkCharacters_31)); }
	inline bool get_checkCharacters_31() const { return ___checkCharacters_31; }
	inline bool* get_address_of_checkCharacters_31() { return &___checkCharacters_31; }
	inline void set_checkCharacters_31(bool value)
	{
		___checkCharacters_31 = value;
	}

	inline static int32_t get_offset_of_lineNumberOffset_32() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___lineNumberOffset_32)); }
	inline int32_t get_lineNumberOffset_32() const { return ___lineNumberOffset_32; }
	inline int32_t* get_address_of_lineNumberOffset_32() { return &___lineNumberOffset_32; }
	inline void set_lineNumberOffset_32(int32_t value)
	{
		___lineNumberOffset_32 = value;
	}

	inline static int32_t get_offset_of_linePositionOffset_33() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___linePositionOffset_33)); }
	inline int32_t get_linePositionOffset_33() const { return ___linePositionOffset_33; }
	inline int32_t* get_address_of_linePositionOffset_33() { return &___linePositionOffset_33; }
	inline void set_linePositionOffset_33(int32_t value)
	{
		___linePositionOffset_33 = value;
	}

	inline static int32_t get_offset_of_closeInput_34() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___closeInput_34)); }
	inline bool get_closeInput_34() const { return ___closeInput_34; }
	inline bool* get_address_of_closeInput_34() { return &___closeInput_34; }
	inline void set_closeInput_34(bool value)
	{
		___closeInput_34 = value;
	}

	inline static int32_t get_offset_of_maxCharactersInDocument_35() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___maxCharactersInDocument_35)); }
	inline int64_t get_maxCharactersInDocument_35() const { return ___maxCharactersInDocument_35; }
	inline int64_t* get_address_of_maxCharactersInDocument_35() { return &___maxCharactersInDocument_35; }
	inline void set_maxCharactersInDocument_35(int64_t value)
	{
		___maxCharactersInDocument_35 = value;
	}

	inline static int32_t get_offset_of_maxCharactersFromEntities_36() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___maxCharactersFromEntities_36)); }
	inline int64_t get_maxCharactersFromEntities_36() const { return ___maxCharactersFromEntities_36; }
	inline int64_t* get_address_of_maxCharactersFromEntities_36() { return &___maxCharactersFromEntities_36; }
	inline void set_maxCharactersFromEntities_36(int64_t value)
	{
		___maxCharactersFromEntities_36 = value;
	}

	inline static int32_t get_offset_of_v1Compat_37() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___v1Compat_37)); }
	inline bool get_v1Compat_37() const { return ___v1Compat_37; }
	inline bool* get_address_of_v1Compat_37() { return &___v1Compat_37; }
	inline void set_v1Compat_37(bool value)
	{
		___v1Compat_37 = value;
	}

	inline static int32_t get_offset_of_namespaceManager_38() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___namespaceManager_38)); }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * get_namespaceManager_38() const { return ___namespaceManager_38; }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F ** get_address_of_namespaceManager_38() { return &___namespaceManager_38; }
	inline void set_namespaceManager_38(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * value)
	{
		___namespaceManager_38 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceManager_38), value);
	}

	inline static int32_t get_offset_of_lastPrefix_39() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___lastPrefix_39)); }
	inline String_t* get_lastPrefix_39() const { return ___lastPrefix_39; }
	inline String_t** get_address_of_lastPrefix_39() { return &___lastPrefix_39; }
	inline void set_lastPrefix_39(String_t* value)
	{
		___lastPrefix_39 = value;
		Il2CppCodeGenWriteBarrier((&___lastPrefix_39), value);
	}

	inline static int32_t get_offset_of_xmlContext_40() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___xmlContext_40)); }
	inline XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52 * get_xmlContext_40() const { return ___xmlContext_40; }
	inline XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52 ** get_address_of_xmlContext_40() { return &___xmlContext_40; }
	inline void set_xmlContext_40(XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52 * value)
	{
		___xmlContext_40 = value;
		Il2CppCodeGenWriteBarrier((&___xmlContext_40), value);
	}

	inline static int32_t get_offset_of_parsingStatesStack_41() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___parsingStatesStack_41)); }
	inline ParsingStateU5BU5D_t7A5096046F9BF1BA70E9696B2A2479272FC65739* get_parsingStatesStack_41() const { return ___parsingStatesStack_41; }
	inline ParsingStateU5BU5D_t7A5096046F9BF1BA70E9696B2A2479272FC65739** get_address_of_parsingStatesStack_41() { return &___parsingStatesStack_41; }
	inline void set_parsingStatesStack_41(ParsingStateU5BU5D_t7A5096046F9BF1BA70E9696B2A2479272FC65739* value)
	{
		___parsingStatesStack_41 = value;
		Il2CppCodeGenWriteBarrier((&___parsingStatesStack_41), value);
	}

	inline static int32_t get_offset_of_parsingStatesStackTop_42() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___parsingStatesStackTop_42)); }
	inline int32_t get_parsingStatesStackTop_42() const { return ___parsingStatesStackTop_42; }
	inline int32_t* get_address_of_parsingStatesStackTop_42() { return &___parsingStatesStackTop_42; }
	inline void set_parsingStatesStackTop_42(int32_t value)
	{
		___parsingStatesStackTop_42 = value;
	}

	inline static int32_t get_offset_of_reportedBaseUri_43() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___reportedBaseUri_43)); }
	inline String_t* get_reportedBaseUri_43() const { return ___reportedBaseUri_43; }
	inline String_t** get_address_of_reportedBaseUri_43() { return &___reportedBaseUri_43; }
	inline void set_reportedBaseUri_43(String_t* value)
	{
		___reportedBaseUri_43 = value;
		Il2CppCodeGenWriteBarrier((&___reportedBaseUri_43), value);
	}

	inline static int32_t get_offset_of_reportedEncoding_44() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___reportedEncoding_44)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_reportedEncoding_44() const { return ___reportedEncoding_44; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_reportedEncoding_44() { return &___reportedEncoding_44; }
	inline void set_reportedEncoding_44(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___reportedEncoding_44 = value;
		Il2CppCodeGenWriteBarrier((&___reportedEncoding_44), value);
	}

	inline static int32_t get_offset_of_dtdInfo_45() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___dtdInfo_45)); }
	inline RuntimeObject* get_dtdInfo_45() const { return ___dtdInfo_45; }
	inline RuntimeObject** get_address_of_dtdInfo_45() { return &___dtdInfo_45; }
	inline void set_dtdInfo_45(RuntimeObject* value)
	{
		___dtdInfo_45 = value;
		Il2CppCodeGenWriteBarrier((&___dtdInfo_45), value);
	}

	inline static int32_t get_offset_of_fragmentType_46() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___fragmentType_46)); }
	inline int32_t get_fragmentType_46() const { return ___fragmentType_46; }
	inline int32_t* get_address_of_fragmentType_46() { return &___fragmentType_46; }
	inline void set_fragmentType_46(int32_t value)
	{
		___fragmentType_46 = value;
	}

	inline static int32_t get_offset_of_fragmentParserContext_47() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___fragmentParserContext_47)); }
	inline XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279 * get_fragmentParserContext_47() const { return ___fragmentParserContext_47; }
	inline XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279 ** get_address_of_fragmentParserContext_47() { return &___fragmentParserContext_47; }
	inline void set_fragmentParserContext_47(XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279 * value)
	{
		___fragmentParserContext_47 = value;
		Il2CppCodeGenWriteBarrier((&___fragmentParserContext_47), value);
	}

	inline static int32_t get_offset_of_fragment_48() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___fragment_48)); }
	inline bool get_fragment_48() const { return ___fragment_48; }
	inline bool* get_address_of_fragment_48() { return &___fragment_48; }
	inline void set_fragment_48(bool value)
	{
		___fragment_48 = value;
	}

	inline static int32_t get_offset_of_incReadDecoder_49() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___incReadDecoder_49)); }
	inline IncrementalReadDecoder_t787BFB5889B01B88DDA030C503A0C2E0525CA723 * get_incReadDecoder_49() const { return ___incReadDecoder_49; }
	inline IncrementalReadDecoder_t787BFB5889B01B88DDA030C503A0C2E0525CA723 ** get_address_of_incReadDecoder_49() { return &___incReadDecoder_49; }
	inline void set_incReadDecoder_49(IncrementalReadDecoder_t787BFB5889B01B88DDA030C503A0C2E0525CA723 * value)
	{
		___incReadDecoder_49 = value;
		Il2CppCodeGenWriteBarrier((&___incReadDecoder_49), value);
	}

	inline static int32_t get_offset_of_incReadState_50() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___incReadState_50)); }
	inline int32_t get_incReadState_50() const { return ___incReadState_50; }
	inline int32_t* get_address_of_incReadState_50() { return &___incReadState_50; }
	inline void set_incReadState_50(int32_t value)
	{
		___incReadState_50 = value;
	}

	inline static int32_t get_offset_of_incReadLineInfo_51() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___incReadLineInfo_51)); }
	inline LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5  get_incReadLineInfo_51() const { return ___incReadLineInfo_51; }
	inline LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5 * get_address_of_incReadLineInfo_51() { return &___incReadLineInfo_51; }
	inline void set_incReadLineInfo_51(LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5  value)
	{
		___incReadLineInfo_51 = value;
	}

	inline static int32_t get_offset_of_incReadDepth_52() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___incReadDepth_52)); }
	inline int32_t get_incReadDepth_52() const { return ___incReadDepth_52; }
	inline int32_t* get_address_of_incReadDepth_52() { return &___incReadDepth_52; }
	inline void set_incReadDepth_52(int32_t value)
	{
		___incReadDepth_52 = value;
	}

	inline static int32_t get_offset_of_incReadLeftStartPos_53() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___incReadLeftStartPos_53)); }
	inline int32_t get_incReadLeftStartPos_53() const { return ___incReadLeftStartPos_53; }
	inline int32_t* get_address_of_incReadLeftStartPos_53() { return &___incReadLeftStartPos_53; }
	inline void set_incReadLeftStartPos_53(int32_t value)
	{
		___incReadLeftStartPos_53 = value;
	}

	inline static int32_t get_offset_of_incReadLeftEndPos_54() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___incReadLeftEndPos_54)); }
	inline int32_t get_incReadLeftEndPos_54() const { return ___incReadLeftEndPos_54; }
	inline int32_t* get_address_of_incReadLeftEndPos_54() { return &___incReadLeftEndPos_54; }
	inline void set_incReadLeftEndPos_54(int32_t value)
	{
		___incReadLeftEndPos_54 = value;
	}

	inline static int32_t get_offset_of_attributeValueBaseEntityId_55() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___attributeValueBaseEntityId_55)); }
	inline int32_t get_attributeValueBaseEntityId_55() const { return ___attributeValueBaseEntityId_55; }
	inline int32_t* get_address_of_attributeValueBaseEntityId_55() { return &___attributeValueBaseEntityId_55; }
	inline void set_attributeValueBaseEntityId_55(int32_t value)
	{
		___attributeValueBaseEntityId_55 = value;
	}

	inline static int32_t get_offset_of_emptyEntityInAttributeResolved_56() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___emptyEntityInAttributeResolved_56)); }
	inline bool get_emptyEntityInAttributeResolved_56() const { return ___emptyEntityInAttributeResolved_56; }
	inline bool* get_address_of_emptyEntityInAttributeResolved_56() { return &___emptyEntityInAttributeResolved_56; }
	inline void set_emptyEntityInAttributeResolved_56(bool value)
	{
		___emptyEntityInAttributeResolved_56 = value;
	}

	inline static int32_t get_offset_of_validationEventHandling_57() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___validationEventHandling_57)); }
	inline RuntimeObject* get_validationEventHandling_57() const { return ___validationEventHandling_57; }
	inline RuntimeObject** get_address_of_validationEventHandling_57() { return &___validationEventHandling_57; }
	inline void set_validationEventHandling_57(RuntimeObject* value)
	{
		___validationEventHandling_57 = value;
		Il2CppCodeGenWriteBarrier((&___validationEventHandling_57), value);
	}

	inline static int32_t get_offset_of_onDefaultAttributeUse_58() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___onDefaultAttributeUse_58)); }
	inline OnDefaultAttributeUseDelegate_tE83A97BD37E08D3C5AA377CEA6388782D99EA34D * get_onDefaultAttributeUse_58() const { return ___onDefaultAttributeUse_58; }
	inline OnDefaultAttributeUseDelegate_tE83A97BD37E08D3C5AA377CEA6388782D99EA34D ** get_address_of_onDefaultAttributeUse_58() { return &___onDefaultAttributeUse_58; }
	inline void set_onDefaultAttributeUse_58(OnDefaultAttributeUseDelegate_tE83A97BD37E08D3C5AA377CEA6388782D99EA34D * value)
	{
		___onDefaultAttributeUse_58 = value;
		Il2CppCodeGenWriteBarrier((&___onDefaultAttributeUse_58), value);
	}

	inline static int32_t get_offset_of_validatingReaderCompatFlag_59() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___validatingReaderCompatFlag_59)); }
	inline bool get_validatingReaderCompatFlag_59() const { return ___validatingReaderCompatFlag_59; }
	inline bool* get_address_of_validatingReaderCompatFlag_59() { return &___validatingReaderCompatFlag_59; }
	inline void set_validatingReaderCompatFlag_59(bool value)
	{
		___validatingReaderCompatFlag_59 = value;
	}

	inline static int32_t get_offset_of_addDefaultAttributesAndNormalize_60() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___addDefaultAttributesAndNormalize_60)); }
	inline bool get_addDefaultAttributesAndNormalize_60() const { return ___addDefaultAttributesAndNormalize_60; }
	inline bool* get_address_of_addDefaultAttributesAndNormalize_60() { return &___addDefaultAttributesAndNormalize_60; }
	inline void set_addDefaultAttributesAndNormalize_60(bool value)
	{
		___addDefaultAttributesAndNormalize_60 = value;
	}

	inline static int32_t get_offset_of_stringBuilder_61() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___stringBuilder_61)); }
	inline StringBuilder_t * get_stringBuilder_61() const { return ___stringBuilder_61; }
	inline StringBuilder_t ** get_address_of_stringBuilder_61() { return &___stringBuilder_61; }
	inline void set_stringBuilder_61(StringBuilder_t * value)
	{
		___stringBuilder_61 = value;
		Il2CppCodeGenWriteBarrier((&___stringBuilder_61), value);
	}

	inline static int32_t get_offset_of_rootElementParsed_62() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___rootElementParsed_62)); }
	inline bool get_rootElementParsed_62() const { return ___rootElementParsed_62; }
	inline bool* get_address_of_rootElementParsed_62() { return &___rootElementParsed_62; }
	inline void set_rootElementParsed_62(bool value)
	{
		___rootElementParsed_62 = value;
	}

	inline static int32_t get_offset_of_standalone_63() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___standalone_63)); }
	inline bool get_standalone_63() const { return ___standalone_63; }
	inline bool* get_address_of_standalone_63() { return &___standalone_63; }
	inline void set_standalone_63(bool value)
	{
		___standalone_63 = value;
	}

	inline static int32_t get_offset_of_nextEntityId_64() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___nextEntityId_64)); }
	inline int32_t get_nextEntityId_64() const { return ___nextEntityId_64; }
	inline int32_t* get_address_of_nextEntityId_64() { return &___nextEntityId_64; }
	inline void set_nextEntityId_64(int32_t value)
	{
		___nextEntityId_64 = value;
	}

	inline static int32_t get_offset_of_parsingMode_65() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___parsingMode_65)); }
	inline int32_t get_parsingMode_65() const { return ___parsingMode_65; }
	inline int32_t* get_address_of_parsingMode_65() { return &___parsingMode_65; }
	inline void set_parsingMode_65(int32_t value)
	{
		___parsingMode_65 = value;
	}

	inline static int32_t get_offset_of_readState_66() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___readState_66)); }
	inline int32_t get_readState_66() const { return ___readState_66; }
	inline int32_t* get_address_of_readState_66() { return &___readState_66; }
	inline void set_readState_66(int32_t value)
	{
		___readState_66 = value;
	}

	inline static int32_t get_offset_of_lastEntity_67() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___lastEntity_67)); }
	inline RuntimeObject* get_lastEntity_67() const { return ___lastEntity_67; }
	inline RuntimeObject** get_address_of_lastEntity_67() { return &___lastEntity_67; }
	inline void set_lastEntity_67(RuntimeObject* value)
	{
		___lastEntity_67 = value;
		Il2CppCodeGenWriteBarrier((&___lastEntity_67), value);
	}

	inline static int32_t get_offset_of_afterResetState_68() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___afterResetState_68)); }
	inline bool get_afterResetState_68() const { return ___afterResetState_68; }
	inline bool* get_address_of_afterResetState_68() { return &___afterResetState_68; }
	inline void set_afterResetState_68(bool value)
	{
		___afterResetState_68 = value;
	}

	inline static int32_t get_offset_of_documentStartBytePos_69() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___documentStartBytePos_69)); }
	inline int32_t get_documentStartBytePos_69() const { return ___documentStartBytePos_69; }
	inline int32_t* get_address_of_documentStartBytePos_69() { return &___documentStartBytePos_69; }
	inline void set_documentStartBytePos_69(int32_t value)
	{
		___documentStartBytePos_69 = value;
	}

	inline static int32_t get_offset_of_readValueOffset_70() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___readValueOffset_70)); }
	inline int32_t get_readValueOffset_70() const { return ___readValueOffset_70; }
	inline int32_t* get_address_of_readValueOffset_70() { return &___readValueOffset_70; }
	inline void set_readValueOffset_70(int32_t value)
	{
		___readValueOffset_70 = value;
	}

	inline static int32_t get_offset_of_charactersInDocument_71() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___charactersInDocument_71)); }
	inline int64_t get_charactersInDocument_71() const { return ___charactersInDocument_71; }
	inline int64_t* get_address_of_charactersInDocument_71() { return &___charactersInDocument_71; }
	inline void set_charactersInDocument_71(int64_t value)
	{
		___charactersInDocument_71 = value;
	}

	inline static int32_t get_offset_of_charactersFromEntities_72() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___charactersFromEntities_72)); }
	inline int64_t get_charactersFromEntities_72() const { return ___charactersFromEntities_72; }
	inline int64_t* get_address_of_charactersFromEntities_72() { return &___charactersFromEntities_72; }
	inline void set_charactersFromEntities_72(int64_t value)
	{
		___charactersFromEntities_72 = value;
	}

	inline static int32_t get_offset_of_currentEntities_73() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___currentEntities_73)); }
	inline Dictionary_2_tD00930653EC02E03C14ADE0FF8737B6193A5FE1B * get_currentEntities_73() const { return ___currentEntities_73; }
	inline Dictionary_2_tD00930653EC02E03C14ADE0FF8737B6193A5FE1B ** get_address_of_currentEntities_73() { return &___currentEntities_73; }
	inline void set_currentEntities_73(Dictionary_2_tD00930653EC02E03C14ADE0FF8737B6193A5FE1B * value)
	{
		___currentEntities_73 = value;
		Il2CppCodeGenWriteBarrier((&___currentEntities_73), value);
	}

	inline static int32_t get_offset_of_disableUndeclaredEntityCheck_74() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___disableUndeclaredEntityCheck_74)); }
	inline bool get_disableUndeclaredEntityCheck_74() const { return ___disableUndeclaredEntityCheck_74; }
	inline bool* get_address_of_disableUndeclaredEntityCheck_74() { return &___disableUndeclaredEntityCheck_74; }
	inline void set_disableUndeclaredEntityCheck_74(bool value)
	{
		___disableUndeclaredEntityCheck_74 = value;
	}

	inline static int32_t get_offset_of_outerReader_75() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___outerReader_75)); }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * get_outerReader_75() const { return ___outerReader_75; }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB ** get_address_of_outerReader_75() { return &___outerReader_75; }
	inline void set_outerReader_75(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * value)
	{
		___outerReader_75 = value;
		Il2CppCodeGenWriteBarrier((&___outerReader_75), value);
	}

	inline static int32_t get_offset_of_xmlResolverIsSet_76() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___xmlResolverIsSet_76)); }
	inline bool get_xmlResolverIsSet_76() const { return ___xmlResolverIsSet_76; }
	inline bool* get_address_of_xmlResolverIsSet_76() { return &___xmlResolverIsSet_76; }
	inline void set_xmlResolverIsSet_76(bool value)
	{
		___xmlResolverIsSet_76 = value;
	}

	inline static int32_t get_offset_of_Xml_77() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___Xml_77)); }
	inline String_t* get_Xml_77() const { return ___Xml_77; }
	inline String_t** get_address_of_Xml_77() { return &___Xml_77; }
	inline void set_Xml_77(String_t* value)
	{
		___Xml_77 = value;
		Il2CppCodeGenWriteBarrier((&___Xml_77), value);
	}

	inline static int32_t get_offset_of_XmlNs_78() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___XmlNs_78)); }
	inline String_t* get_XmlNs_78() const { return ___XmlNs_78; }
	inline String_t** get_address_of_XmlNs_78() { return &___XmlNs_78; }
	inline void set_XmlNs_78(String_t* value)
	{
		___XmlNs_78 = value;
		Il2CppCodeGenWriteBarrier((&___XmlNs_78), value);
	}

	inline static int32_t get_offset_of_parseText_dummyTask_79() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61, ___parseText_dummyTask_79)); }
	inline Task_1_tFB42A7666202CD6CD81E6BF0FF63815EE40E621D * get_parseText_dummyTask_79() const { return ___parseText_dummyTask_79; }
	inline Task_1_tFB42A7666202CD6CD81E6BF0FF63815EE40E621D ** get_address_of_parseText_dummyTask_79() { return &___parseText_dummyTask_79; }
	inline void set_parseText_dummyTask_79(Task_1_tFB42A7666202CD6CD81E6BF0FF63815EE40E621D * value)
	{
		___parseText_dummyTask_79 = value;
		Il2CppCodeGenWriteBarrier((&___parseText_dummyTask_79), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTREADERIMPL_T393737BE3F9168D966F164C2FD840C3494DEDE61_H
#ifndef XMLUTF8RAWTEXTWRITER_TD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E_H
#define XMLUTF8RAWTEXTWRITER_TD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUtf8RawTextWriter
struct  XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E  : public XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2
{
public:
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::useAsync
	bool ___useAsync_3;
	// System.Byte[] System.Xml.XmlUtf8RawTextWriter::bufBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bufBytes_4;
	// System.IO.Stream System.Xml.XmlUtf8RawTextWriter::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	// System.Text.Encoding System.Xml.XmlUtf8RawTextWriter::encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_6;
	// System.Xml.XmlCharType System.Xml.XmlUtf8RawTextWriter::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_7;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::bufPos
	int32_t ___bufPos_8;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::textPos
	int32_t ___textPos_9;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::contentPos
	int32_t ___contentPos_10;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::cdataPos
	int32_t ___cdataPos_11;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::attrEndPos
	int32_t ___attrEndPos_12;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::bufLen
	int32_t ___bufLen_13;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::writeToNull
	bool ___writeToNull_14;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::hadDoubleBracket
	bool ___hadDoubleBracket_15;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::inAttributeValue
	bool ___inAttributeValue_16;
	// System.Xml.NewLineHandling System.Xml.XmlUtf8RawTextWriter::newLineHandling
	int32_t ___newLineHandling_17;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::closeOutput
	bool ___closeOutput_18;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::omitXmlDeclaration
	bool ___omitXmlDeclaration_19;
	// System.String System.Xml.XmlUtf8RawTextWriter::newLineChars
	String_t* ___newLineChars_20;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::checkCharacters
	bool ___checkCharacters_21;
	// System.Xml.XmlStandalone System.Xml.XmlUtf8RawTextWriter::standalone
	int32_t ___standalone_22;
	// System.Xml.XmlOutputMethod System.Xml.XmlUtf8RawTextWriter::outputMethod
	int32_t ___outputMethod_23;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::autoXmlDeclaration
	bool ___autoXmlDeclaration_24;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::mergeCDataSections
	bool ___mergeCDataSections_25;

public:
	inline static int32_t get_offset_of_useAsync_3() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___useAsync_3)); }
	inline bool get_useAsync_3() const { return ___useAsync_3; }
	inline bool* get_address_of_useAsync_3() { return &___useAsync_3; }
	inline void set_useAsync_3(bool value)
	{
		___useAsync_3 = value;
	}

	inline static int32_t get_offset_of_bufBytes_4() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___bufBytes_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bufBytes_4() const { return ___bufBytes_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bufBytes_4() { return &___bufBytes_4; }
	inline void set_bufBytes_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bufBytes_4 = value;
		Il2CppCodeGenWriteBarrier((&___bufBytes_4), value);
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___stream_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_5() const { return ___stream_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___encoding_6)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_encoding_6() const { return ___encoding_6; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_6), value);
	}

	inline static int32_t get_offset_of_xmlCharType_7() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___xmlCharType_7)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_7() const { return ___xmlCharType_7; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_7() { return &___xmlCharType_7; }
	inline void set_xmlCharType_7(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_7 = value;
	}

	inline static int32_t get_offset_of_bufPos_8() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___bufPos_8)); }
	inline int32_t get_bufPos_8() const { return ___bufPos_8; }
	inline int32_t* get_address_of_bufPos_8() { return &___bufPos_8; }
	inline void set_bufPos_8(int32_t value)
	{
		___bufPos_8 = value;
	}

	inline static int32_t get_offset_of_textPos_9() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___textPos_9)); }
	inline int32_t get_textPos_9() const { return ___textPos_9; }
	inline int32_t* get_address_of_textPos_9() { return &___textPos_9; }
	inline void set_textPos_9(int32_t value)
	{
		___textPos_9 = value;
	}

	inline static int32_t get_offset_of_contentPos_10() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___contentPos_10)); }
	inline int32_t get_contentPos_10() const { return ___contentPos_10; }
	inline int32_t* get_address_of_contentPos_10() { return &___contentPos_10; }
	inline void set_contentPos_10(int32_t value)
	{
		___contentPos_10 = value;
	}

	inline static int32_t get_offset_of_cdataPos_11() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___cdataPos_11)); }
	inline int32_t get_cdataPos_11() const { return ___cdataPos_11; }
	inline int32_t* get_address_of_cdataPos_11() { return &___cdataPos_11; }
	inline void set_cdataPos_11(int32_t value)
	{
		___cdataPos_11 = value;
	}

	inline static int32_t get_offset_of_attrEndPos_12() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___attrEndPos_12)); }
	inline int32_t get_attrEndPos_12() const { return ___attrEndPos_12; }
	inline int32_t* get_address_of_attrEndPos_12() { return &___attrEndPos_12; }
	inline void set_attrEndPos_12(int32_t value)
	{
		___attrEndPos_12 = value;
	}

	inline static int32_t get_offset_of_bufLen_13() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___bufLen_13)); }
	inline int32_t get_bufLen_13() const { return ___bufLen_13; }
	inline int32_t* get_address_of_bufLen_13() { return &___bufLen_13; }
	inline void set_bufLen_13(int32_t value)
	{
		___bufLen_13 = value;
	}

	inline static int32_t get_offset_of_writeToNull_14() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___writeToNull_14)); }
	inline bool get_writeToNull_14() const { return ___writeToNull_14; }
	inline bool* get_address_of_writeToNull_14() { return &___writeToNull_14; }
	inline void set_writeToNull_14(bool value)
	{
		___writeToNull_14 = value;
	}

	inline static int32_t get_offset_of_hadDoubleBracket_15() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___hadDoubleBracket_15)); }
	inline bool get_hadDoubleBracket_15() const { return ___hadDoubleBracket_15; }
	inline bool* get_address_of_hadDoubleBracket_15() { return &___hadDoubleBracket_15; }
	inline void set_hadDoubleBracket_15(bool value)
	{
		___hadDoubleBracket_15 = value;
	}

	inline static int32_t get_offset_of_inAttributeValue_16() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___inAttributeValue_16)); }
	inline bool get_inAttributeValue_16() const { return ___inAttributeValue_16; }
	inline bool* get_address_of_inAttributeValue_16() { return &___inAttributeValue_16; }
	inline void set_inAttributeValue_16(bool value)
	{
		___inAttributeValue_16 = value;
	}

	inline static int32_t get_offset_of_newLineHandling_17() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___newLineHandling_17)); }
	inline int32_t get_newLineHandling_17() const { return ___newLineHandling_17; }
	inline int32_t* get_address_of_newLineHandling_17() { return &___newLineHandling_17; }
	inline void set_newLineHandling_17(int32_t value)
	{
		___newLineHandling_17 = value;
	}

	inline static int32_t get_offset_of_closeOutput_18() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___closeOutput_18)); }
	inline bool get_closeOutput_18() const { return ___closeOutput_18; }
	inline bool* get_address_of_closeOutput_18() { return &___closeOutput_18; }
	inline void set_closeOutput_18(bool value)
	{
		___closeOutput_18 = value;
	}

	inline static int32_t get_offset_of_omitXmlDeclaration_19() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___omitXmlDeclaration_19)); }
	inline bool get_omitXmlDeclaration_19() const { return ___omitXmlDeclaration_19; }
	inline bool* get_address_of_omitXmlDeclaration_19() { return &___omitXmlDeclaration_19; }
	inline void set_omitXmlDeclaration_19(bool value)
	{
		___omitXmlDeclaration_19 = value;
	}

	inline static int32_t get_offset_of_newLineChars_20() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___newLineChars_20)); }
	inline String_t* get_newLineChars_20() const { return ___newLineChars_20; }
	inline String_t** get_address_of_newLineChars_20() { return &___newLineChars_20; }
	inline void set_newLineChars_20(String_t* value)
	{
		___newLineChars_20 = value;
		Il2CppCodeGenWriteBarrier((&___newLineChars_20), value);
	}

	inline static int32_t get_offset_of_checkCharacters_21() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___checkCharacters_21)); }
	inline bool get_checkCharacters_21() const { return ___checkCharacters_21; }
	inline bool* get_address_of_checkCharacters_21() { return &___checkCharacters_21; }
	inline void set_checkCharacters_21(bool value)
	{
		___checkCharacters_21 = value;
	}

	inline static int32_t get_offset_of_standalone_22() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___standalone_22)); }
	inline int32_t get_standalone_22() const { return ___standalone_22; }
	inline int32_t* get_address_of_standalone_22() { return &___standalone_22; }
	inline void set_standalone_22(int32_t value)
	{
		___standalone_22 = value;
	}

	inline static int32_t get_offset_of_outputMethod_23() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___outputMethod_23)); }
	inline int32_t get_outputMethod_23() const { return ___outputMethod_23; }
	inline int32_t* get_address_of_outputMethod_23() { return &___outputMethod_23; }
	inline void set_outputMethod_23(int32_t value)
	{
		___outputMethod_23 = value;
	}

	inline static int32_t get_offset_of_autoXmlDeclaration_24() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___autoXmlDeclaration_24)); }
	inline bool get_autoXmlDeclaration_24() const { return ___autoXmlDeclaration_24; }
	inline bool* get_address_of_autoXmlDeclaration_24() { return &___autoXmlDeclaration_24; }
	inline void set_autoXmlDeclaration_24(bool value)
	{
		___autoXmlDeclaration_24 = value;
	}

	inline static int32_t get_offset_of_mergeCDataSections_25() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___mergeCDataSections_25)); }
	inline bool get_mergeCDataSections_25() const { return ___mergeCDataSections_25; }
	inline bool* get_address_of_mergeCDataSections_25() { return &___mergeCDataSections_25; }
	inline void set_mergeCDataSections_25(bool value)
	{
		___mergeCDataSections_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLUTF8RAWTEXTWRITER_TD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E_H
#ifndef HTMLENCODEDRAWTEXTWRITER_TEEBA50D2415AAABDA0747F7710BEFB04F645DD50_H
#define HTMLENCODEDRAWTEXTWRITER_TEEBA50D2415AAABDA0747F7710BEFB04F645DD50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HtmlEncodedRawTextWriter
struct  HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50  : public XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B
{
public:
	// System.Xml.ByteStack System.Xml.HtmlEncodedRawTextWriter::elementScope
	ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 * ___elementScope_35;
	// System.Xml.ElementProperties System.Xml.HtmlEncodedRawTextWriter::currentElementProperties
	uint32_t ___currentElementProperties_36;
	// System.Xml.AttributeProperties System.Xml.HtmlEncodedRawTextWriter::currentAttributeProperties
	uint32_t ___currentAttributeProperties_37;
	// System.Boolean System.Xml.HtmlEncodedRawTextWriter::endsWithAmpersand
	bool ___endsWithAmpersand_38;
	// System.Byte[] System.Xml.HtmlEncodedRawTextWriter::uriEscapingBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___uriEscapingBuffer_39;
	// System.String System.Xml.HtmlEncodedRawTextWriter::mediaType
	String_t* ___mediaType_40;
	// System.Boolean System.Xml.HtmlEncodedRawTextWriter::doNotEscapeUriAttributes
	bool ___doNotEscapeUriAttributes_41;

public:
	inline static int32_t get_offset_of_elementScope_35() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50, ___elementScope_35)); }
	inline ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 * get_elementScope_35() const { return ___elementScope_35; }
	inline ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 ** get_address_of_elementScope_35() { return &___elementScope_35; }
	inline void set_elementScope_35(ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 * value)
	{
		___elementScope_35 = value;
		Il2CppCodeGenWriteBarrier((&___elementScope_35), value);
	}

	inline static int32_t get_offset_of_currentElementProperties_36() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50, ___currentElementProperties_36)); }
	inline uint32_t get_currentElementProperties_36() const { return ___currentElementProperties_36; }
	inline uint32_t* get_address_of_currentElementProperties_36() { return &___currentElementProperties_36; }
	inline void set_currentElementProperties_36(uint32_t value)
	{
		___currentElementProperties_36 = value;
	}

	inline static int32_t get_offset_of_currentAttributeProperties_37() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50, ___currentAttributeProperties_37)); }
	inline uint32_t get_currentAttributeProperties_37() const { return ___currentAttributeProperties_37; }
	inline uint32_t* get_address_of_currentAttributeProperties_37() { return &___currentAttributeProperties_37; }
	inline void set_currentAttributeProperties_37(uint32_t value)
	{
		___currentAttributeProperties_37 = value;
	}

	inline static int32_t get_offset_of_endsWithAmpersand_38() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50, ___endsWithAmpersand_38)); }
	inline bool get_endsWithAmpersand_38() const { return ___endsWithAmpersand_38; }
	inline bool* get_address_of_endsWithAmpersand_38() { return &___endsWithAmpersand_38; }
	inline void set_endsWithAmpersand_38(bool value)
	{
		___endsWithAmpersand_38 = value;
	}

	inline static int32_t get_offset_of_uriEscapingBuffer_39() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50, ___uriEscapingBuffer_39)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_uriEscapingBuffer_39() const { return ___uriEscapingBuffer_39; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_uriEscapingBuffer_39() { return &___uriEscapingBuffer_39; }
	inline void set_uriEscapingBuffer_39(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___uriEscapingBuffer_39 = value;
		Il2CppCodeGenWriteBarrier((&___uriEscapingBuffer_39), value);
	}

	inline static int32_t get_offset_of_mediaType_40() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50, ___mediaType_40)); }
	inline String_t* get_mediaType_40() const { return ___mediaType_40; }
	inline String_t** get_address_of_mediaType_40() { return &___mediaType_40; }
	inline void set_mediaType_40(String_t* value)
	{
		___mediaType_40 = value;
		Il2CppCodeGenWriteBarrier((&___mediaType_40), value);
	}

	inline static int32_t get_offset_of_doNotEscapeUriAttributes_41() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50, ___doNotEscapeUriAttributes_41)); }
	inline bool get_doNotEscapeUriAttributes_41() const { return ___doNotEscapeUriAttributes_41; }
	inline bool* get_address_of_doNotEscapeUriAttributes_41() { return &___doNotEscapeUriAttributes_41; }
	inline void set_doNotEscapeUriAttributes_41(bool value)
	{
		___doNotEscapeUriAttributes_41 = value;
	}
};

struct HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50_StaticFields
{
public:
	// System.Xml.TernaryTreeReadOnly System.Xml.HtmlEncodedRawTextWriter::elementPropertySearch
	TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * ___elementPropertySearch_42;
	// System.Xml.TernaryTreeReadOnly System.Xml.HtmlEncodedRawTextWriter::attributePropertySearch
	TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * ___attributePropertySearch_43;

public:
	inline static int32_t get_offset_of_elementPropertySearch_42() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50_StaticFields, ___elementPropertySearch_42)); }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * get_elementPropertySearch_42() const { return ___elementPropertySearch_42; }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 ** get_address_of_elementPropertySearch_42() { return &___elementPropertySearch_42; }
	inline void set_elementPropertySearch_42(TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * value)
	{
		___elementPropertySearch_42 = value;
		Il2CppCodeGenWriteBarrier((&___elementPropertySearch_42), value);
	}

	inline static int32_t get_offset_of_attributePropertySearch_43() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50_StaticFields, ___attributePropertySearch_43)); }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * get_attributePropertySearch_43() const { return ___attributePropertySearch_43; }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 ** get_address_of_attributePropertySearch_43() { return &___attributePropertySearch_43; }
	inline void set_attributePropertySearch_43(TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * value)
	{
		___attributePropertySearch_43 = value;
		Il2CppCodeGenWriteBarrier((&___attributePropertySearch_43), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLENCODEDRAWTEXTWRITER_TEEBA50D2415AAABDA0747F7710BEFB04F645DD50_H
#ifndef HTMLUTF8RAWTEXTWRITER_T921E985ECE89264C6CA3BA42D2556BA893052825_H
#define HTMLUTF8RAWTEXTWRITER_T921E985ECE89264C6CA3BA42D2556BA893052825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HtmlUtf8RawTextWriter
struct  HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825  : public XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E
{
public:
	// System.Xml.ByteStack System.Xml.HtmlUtf8RawTextWriter::elementScope
	ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 * ___elementScope_26;
	// System.Xml.ElementProperties System.Xml.HtmlUtf8RawTextWriter::currentElementProperties
	uint32_t ___currentElementProperties_27;
	// System.Xml.AttributeProperties System.Xml.HtmlUtf8RawTextWriter::currentAttributeProperties
	uint32_t ___currentAttributeProperties_28;
	// System.Boolean System.Xml.HtmlUtf8RawTextWriter::endsWithAmpersand
	bool ___endsWithAmpersand_29;
	// System.Byte[] System.Xml.HtmlUtf8RawTextWriter::uriEscapingBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___uriEscapingBuffer_30;
	// System.String System.Xml.HtmlUtf8RawTextWriter::mediaType
	String_t* ___mediaType_31;
	// System.Boolean System.Xml.HtmlUtf8RawTextWriter::doNotEscapeUriAttributes
	bool ___doNotEscapeUriAttributes_32;

public:
	inline static int32_t get_offset_of_elementScope_26() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825, ___elementScope_26)); }
	inline ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 * get_elementScope_26() const { return ___elementScope_26; }
	inline ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 ** get_address_of_elementScope_26() { return &___elementScope_26; }
	inline void set_elementScope_26(ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 * value)
	{
		___elementScope_26 = value;
		Il2CppCodeGenWriteBarrier((&___elementScope_26), value);
	}

	inline static int32_t get_offset_of_currentElementProperties_27() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825, ___currentElementProperties_27)); }
	inline uint32_t get_currentElementProperties_27() const { return ___currentElementProperties_27; }
	inline uint32_t* get_address_of_currentElementProperties_27() { return &___currentElementProperties_27; }
	inline void set_currentElementProperties_27(uint32_t value)
	{
		___currentElementProperties_27 = value;
	}

	inline static int32_t get_offset_of_currentAttributeProperties_28() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825, ___currentAttributeProperties_28)); }
	inline uint32_t get_currentAttributeProperties_28() const { return ___currentAttributeProperties_28; }
	inline uint32_t* get_address_of_currentAttributeProperties_28() { return &___currentAttributeProperties_28; }
	inline void set_currentAttributeProperties_28(uint32_t value)
	{
		___currentAttributeProperties_28 = value;
	}

	inline static int32_t get_offset_of_endsWithAmpersand_29() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825, ___endsWithAmpersand_29)); }
	inline bool get_endsWithAmpersand_29() const { return ___endsWithAmpersand_29; }
	inline bool* get_address_of_endsWithAmpersand_29() { return &___endsWithAmpersand_29; }
	inline void set_endsWithAmpersand_29(bool value)
	{
		___endsWithAmpersand_29 = value;
	}

	inline static int32_t get_offset_of_uriEscapingBuffer_30() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825, ___uriEscapingBuffer_30)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_uriEscapingBuffer_30() const { return ___uriEscapingBuffer_30; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_uriEscapingBuffer_30() { return &___uriEscapingBuffer_30; }
	inline void set_uriEscapingBuffer_30(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___uriEscapingBuffer_30 = value;
		Il2CppCodeGenWriteBarrier((&___uriEscapingBuffer_30), value);
	}

	inline static int32_t get_offset_of_mediaType_31() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825, ___mediaType_31)); }
	inline String_t* get_mediaType_31() const { return ___mediaType_31; }
	inline String_t** get_address_of_mediaType_31() { return &___mediaType_31; }
	inline void set_mediaType_31(String_t* value)
	{
		___mediaType_31 = value;
		Il2CppCodeGenWriteBarrier((&___mediaType_31), value);
	}

	inline static int32_t get_offset_of_doNotEscapeUriAttributes_32() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825, ___doNotEscapeUriAttributes_32)); }
	inline bool get_doNotEscapeUriAttributes_32() const { return ___doNotEscapeUriAttributes_32; }
	inline bool* get_address_of_doNotEscapeUriAttributes_32() { return &___doNotEscapeUriAttributes_32; }
	inline void set_doNotEscapeUriAttributes_32(bool value)
	{
		___doNotEscapeUriAttributes_32 = value;
	}
};

struct HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825_StaticFields
{
public:
	// System.Xml.TernaryTreeReadOnly System.Xml.HtmlUtf8RawTextWriter::elementPropertySearch
	TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * ___elementPropertySearch_33;
	// System.Xml.TernaryTreeReadOnly System.Xml.HtmlUtf8RawTextWriter::attributePropertySearch
	TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * ___attributePropertySearch_34;

public:
	inline static int32_t get_offset_of_elementPropertySearch_33() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825_StaticFields, ___elementPropertySearch_33)); }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * get_elementPropertySearch_33() const { return ___elementPropertySearch_33; }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 ** get_address_of_elementPropertySearch_33() { return &___elementPropertySearch_33; }
	inline void set_elementPropertySearch_33(TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * value)
	{
		___elementPropertySearch_33 = value;
		Il2CppCodeGenWriteBarrier((&___elementPropertySearch_33), value);
	}

	inline static int32_t get_offset_of_attributePropertySearch_34() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825_StaticFields, ___attributePropertySearch_34)); }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * get_attributePropertySearch_34() const { return ___attributePropertySearch_34; }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 ** get_address_of_attributePropertySearch_34() { return &___attributePropertySearch_34; }
	inline void set_attributePropertySearch_34(TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * value)
	{
		___attributePropertySearch_34 = value;
		Il2CppCodeGenWriteBarrier((&___attributePropertySearch_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLUTF8RAWTEXTWRITER_T921E985ECE89264C6CA3BA42D2556BA893052825_H
#ifndef ONREMOVEWRITER_T40CE623324C30930692639D6172B422C25AA6370_H
#define ONREMOVEWRITER_T40CE623324C30930692639D6172B422C25AA6370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.OnRemoveWriter
struct  OnRemoveWriter_t40CE623324C30930692639D6172B422C25AA6370  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONREMOVEWRITER_T40CE623324C30930692639D6172B422C25AA6370_H
#ifndef HASHCODEOFSTRINGDELEGATE_TC8B9E43DCB47789C0CCA2921BE18838AB95B323E_H
#define HASHCODEOFSTRINGDELEGATE_TC8B9E43DCB47789C0CCA2921BE18838AB95B323E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.SecureStringHasher/HashCodeOfStringDelegate
struct  HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHCODEOFSTRINGDELEGATE_TC8B9E43DCB47789C0CCA2921BE18838AB95B323E_H
#ifndef TEXTENCODEDRAWTEXTWRITER_T828BEA9C3C114D3F2445708667B5DA7160806CA6_H
#define TEXTENCODEDRAWTEXTWRITER_T828BEA9C3C114D3F2445708667B5DA7160806CA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.TextEncodedRawTextWriter
struct  TextEncodedRawTextWriter_t828BEA9C3C114D3F2445708667B5DA7160806CA6  : public XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTENCODEDRAWTEXTWRITER_T828BEA9C3C114D3F2445708667B5DA7160806CA6_H
#ifndef TEXTUTF8RAWTEXTWRITER_TF6F774D5F5995D09C8D65F10F74BCE6DD7089BA6_H
#define TEXTUTF8RAWTEXTWRITER_TF6F774D5F5995D09C8D65F10F74BCE6DD7089BA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.TextUtf8RawTextWriter
struct  TextUtf8RawTextWriter_tF6F774D5F5995D09C8D65F10F74BCE6DD7089BA6  : public XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUTF8RAWTEXTWRITER_TF6F774D5F5995D09C8D65F10F74BCE6DD7089BA6_H
#ifndef XMLASYNCCHECKREADERWITHLINEINFONSSCHEMA_T3716760293E48E0CCF156F9BBA437D1ACE28F047_H
#define XMLASYNCCHECKREADERWITHLINEINFONSSCHEMA_T3716760293E48E0CCF156F9BBA437D1ACE28F047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAsyncCheckReaderWithLineInfoNSSchema
struct  XmlAsyncCheckReaderWithLineInfoNSSchema_t3716760293E48E0CCF156F9BBA437D1ACE28F047  : public XmlAsyncCheckReaderWithLineInfoNS_tDA344AA17D0B0512BDC02C145605922DC1875327
{
public:
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlAsyncCheckReaderWithLineInfoNSSchema::readerAsIXmlSchemaInfo
	RuntimeObject* ___readerAsIXmlSchemaInfo_7;

public:
	inline static int32_t get_offset_of_readerAsIXmlSchemaInfo_7() { return static_cast<int32_t>(offsetof(XmlAsyncCheckReaderWithLineInfoNSSchema_t3716760293E48E0CCF156F9BBA437D1ACE28F047, ___readerAsIXmlSchemaInfo_7)); }
	inline RuntimeObject* get_readerAsIXmlSchemaInfo_7() const { return ___readerAsIXmlSchemaInfo_7; }
	inline RuntimeObject** get_address_of_readerAsIXmlSchemaInfo_7() { return &___readerAsIXmlSchemaInfo_7; }
	inline void set_readerAsIXmlSchemaInfo_7(RuntimeObject* value)
	{
		___readerAsIXmlSchemaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___readerAsIXmlSchemaInfo_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLASYNCCHECKREADERWITHLINEINFONSSCHEMA_T3716760293E48E0CCF156F9BBA437D1ACE28F047_H
#ifndef XMLENCODEDRAWTEXTWRITERINDENT_TB4179458DD5909031F0C93CC0C34CEFC04AB19B0_H
#define XMLENCODEDRAWTEXTWRITERINDENT_TB4179458DD5909031F0C93CC0C34CEFC04AB19B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEncodedRawTextWriterIndent
struct  XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0  : public XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B
{
public:
	// System.Int32 System.Xml.XmlEncodedRawTextWriterIndent::indentLevel
	int32_t ___indentLevel_35;
	// System.Boolean System.Xml.XmlEncodedRawTextWriterIndent::newLineOnAttributes
	bool ___newLineOnAttributes_36;
	// System.String System.Xml.XmlEncodedRawTextWriterIndent::indentChars
	String_t* ___indentChars_37;
	// System.Boolean System.Xml.XmlEncodedRawTextWriterIndent::mixedContent
	bool ___mixedContent_38;
	// System.Xml.BitStack System.Xml.XmlEncodedRawTextWriterIndent::mixedContentStack
	BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * ___mixedContentStack_39;
	// System.Xml.ConformanceLevel System.Xml.XmlEncodedRawTextWriterIndent::conformanceLevel
	int32_t ___conformanceLevel_40;

public:
	inline static int32_t get_offset_of_indentLevel_35() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0, ___indentLevel_35)); }
	inline int32_t get_indentLevel_35() const { return ___indentLevel_35; }
	inline int32_t* get_address_of_indentLevel_35() { return &___indentLevel_35; }
	inline void set_indentLevel_35(int32_t value)
	{
		___indentLevel_35 = value;
	}

	inline static int32_t get_offset_of_newLineOnAttributes_36() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0, ___newLineOnAttributes_36)); }
	inline bool get_newLineOnAttributes_36() const { return ___newLineOnAttributes_36; }
	inline bool* get_address_of_newLineOnAttributes_36() { return &___newLineOnAttributes_36; }
	inline void set_newLineOnAttributes_36(bool value)
	{
		___newLineOnAttributes_36 = value;
	}

	inline static int32_t get_offset_of_indentChars_37() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0, ___indentChars_37)); }
	inline String_t* get_indentChars_37() const { return ___indentChars_37; }
	inline String_t** get_address_of_indentChars_37() { return &___indentChars_37; }
	inline void set_indentChars_37(String_t* value)
	{
		___indentChars_37 = value;
		Il2CppCodeGenWriteBarrier((&___indentChars_37), value);
	}

	inline static int32_t get_offset_of_mixedContent_38() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0, ___mixedContent_38)); }
	inline bool get_mixedContent_38() const { return ___mixedContent_38; }
	inline bool* get_address_of_mixedContent_38() { return &___mixedContent_38; }
	inline void set_mixedContent_38(bool value)
	{
		___mixedContent_38 = value;
	}

	inline static int32_t get_offset_of_mixedContentStack_39() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0, ___mixedContentStack_39)); }
	inline BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * get_mixedContentStack_39() const { return ___mixedContentStack_39; }
	inline BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 ** get_address_of_mixedContentStack_39() { return &___mixedContentStack_39; }
	inline void set_mixedContentStack_39(BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * value)
	{
		___mixedContentStack_39 = value;
		Il2CppCodeGenWriteBarrier((&___mixedContentStack_39), value);
	}

	inline static int32_t get_offset_of_conformanceLevel_40() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0, ___conformanceLevel_40)); }
	inline int32_t get_conformanceLevel_40() const { return ___conformanceLevel_40; }
	inline int32_t* get_address_of_conformanceLevel_40() { return &___conformanceLevel_40; }
	inline void set_conformanceLevel_40(int32_t value)
	{
		___conformanceLevel_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENCODEDRAWTEXTWRITERINDENT_TB4179458DD5909031F0C93CC0C34CEFC04AB19B0_H
#ifndef HTMLENCODEDRAWTEXTWRITERINDENT_TF20FFFC997445698A8B55A4A358D91C6D8F8BCD2_H
#define HTMLENCODEDRAWTEXTWRITERINDENT_TF20FFFC997445698A8B55A4A358D91C6D8F8BCD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HtmlEncodedRawTextWriterIndent
struct  HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2  : public HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50
{
public:
	// System.Int32 System.Xml.HtmlEncodedRawTextWriterIndent::indentLevel
	int32_t ___indentLevel_44;
	// System.Int32 System.Xml.HtmlEncodedRawTextWriterIndent::endBlockPos
	int32_t ___endBlockPos_45;
	// System.String System.Xml.HtmlEncodedRawTextWriterIndent::indentChars
	String_t* ___indentChars_46;
	// System.Boolean System.Xml.HtmlEncodedRawTextWriterIndent::newLineOnAttributes
	bool ___newLineOnAttributes_47;

public:
	inline static int32_t get_offset_of_indentLevel_44() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2, ___indentLevel_44)); }
	inline int32_t get_indentLevel_44() const { return ___indentLevel_44; }
	inline int32_t* get_address_of_indentLevel_44() { return &___indentLevel_44; }
	inline void set_indentLevel_44(int32_t value)
	{
		___indentLevel_44 = value;
	}

	inline static int32_t get_offset_of_endBlockPos_45() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2, ___endBlockPos_45)); }
	inline int32_t get_endBlockPos_45() const { return ___endBlockPos_45; }
	inline int32_t* get_address_of_endBlockPos_45() { return &___endBlockPos_45; }
	inline void set_endBlockPos_45(int32_t value)
	{
		___endBlockPos_45 = value;
	}

	inline static int32_t get_offset_of_indentChars_46() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2, ___indentChars_46)); }
	inline String_t* get_indentChars_46() const { return ___indentChars_46; }
	inline String_t** get_address_of_indentChars_46() { return &___indentChars_46; }
	inline void set_indentChars_46(String_t* value)
	{
		___indentChars_46 = value;
		Il2CppCodeGenWriteBarrier((&___indentChars_46), value);
	}

	inline static int32_t get_offset_of_newLineOnAttributes_47() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2, ___newLineOnAttributes_47)); }
	inline bool get_newLineOnAttributes_47() const { return ___newLineOnAttributes_47; }
	inline bool* get_address_of_newLineOnAttributes_47() { return &___newLineOnAttributes_47; }
	inline void set_newLineOnAttributes_47(bool value)
	{
		___newLineOnAttributes_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLENCODEDRAWTEXTWRITERINDENT_TF20FFFC997445698A8B55A4A358D91C6D8F8BCD2_H
#ifndef HTMLUTF8RAWTEXTWRITERINDENT_T715DDDD3B0F3FF982DBC89CAED471D95D8A81A55_H
#define HTMLUTF8RAWTEXTWRITERINDENT_T715DDDD3B0F3FF982DBC89CAED471D95D8A81A55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HtmlUtf8RawTextWriterIndent
struct  HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55  : public HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825
{
public:
	// System.Int32 System.Xml.HtmlUtf8RawTextWriterIndent::indentLevel
	int32_t ___indentLevel_35;
	// System.Int32 System.Xml.HtmlUtf8RawTextWriterIndent::endBlockPos
	int32_t ___endBlockPos_36;
	// System.String System.Xml.HtmlUtf8RawTextWriterIndent::indentChars
	String_t* ___indentChars_37;
	// System.Boolean System.Xml.HtmlUtf8RawTextWriterIndent::newLineOnAttributes
	bool ___newLineOnAttributes_38;

public:
	inline static int32_t get_offset_of_indentLevel_35() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55, ___indentLevel_35)); }
	inline int32_t get_indentLevel_35() const { return ___indentLevel_35; }
	inline int32_t* get_address_of_indentLevel_35() { return &___indentLevel_35; }
	inline void set_indentLevel_35(int32_t value)
	{
		___indentLevel_35 = value;
	}

	inline static int32_t get_offset_of_endBlockPos_36() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55, ___endBlockPos_36)); }
	inline int32_t get_endBlockPos_36() const { return ___endBlockPos_36; }
	inline int32_t* get_address_of_endBlockPos_36() { return &___endBlockPos_36; }
	inline void set_endBlockPos_36(int32_t value)
	{
		___endBlockPos_36 = value;
	}

	inline static int32_t get_offset_of_indentChars_37() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55, ___indentChars_37)); }
	inline String_t* get_indentChars_37() const { return ___indentChars_37; }
	inline String_t** get_address_of_indentChars_37() { return &___indentChars_37; }
	inline void set_indentChars_37(String_t* value)
	{
		___indentChars_37 = value;
		Il2CppCodeGenWriteBarrier((&___indentChars_37), value);
	}

	inline static int32_t get_offset_of_newLineOnAttributes_38() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55, ___newLineOnAttributes_38)); }
	inline bool get_newLineOnAttributes_38() const { return ___newLineOnAttributes_38; }
	inline bool* get_address_of_newLineOnAttributes_38() { return &___newLineOnAttributes_38; }
	inline void set_newLineOnAttributes_38(bool value)
	{
		___newLineOnAttributes_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLUTF8RAWTEXTWRITERINDENT_T715DDDD3B0F3FF982DBC89CAED471D95D8A81A55_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (Operator_t5D5C7086CDF441136B62FC9AF3B7C4E5F24AA458), -1, sizeof(Operator_t5D5C7086CDF441136B62FC9AF3B7C4E5F24AA458_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1600[4] = 
{
	Operator_t5D5C7086CDF441136B62FC9AF3B7C4E5F24AA458_StaticFields::get_offset_of_invertOp_0(),
	Operator_t5D5C7086CDF441136B62FC9AF3B7C4E5F24AA458::get_offset_of_opType_1(),
	Operator_t5D5C7086CDF441136B62FC9AF3B7C4E5F24AA458::get_offset_of_opnd1_2(),
	Operator_t5D5C7086CDF441136B62FC9AF3B7C4E5F24AA458::get_offset_of_opnd2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (Op_tCCFB3D07967B00ECC946757A2AFE63466C34F976)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1601[16] = 
{
	Op_tCCFB3D07967B00ECC946757A2AFE63466C34F976::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (Root_tE82700519CE1726D106D4076E62825DA615058BA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (Variable_t18C81156491F857437BADE92E874B26983F85E25), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1603[2] = 
{
	Variable_t18C81156491F857437BADE92E874B26983F85E25::get_offset_of_localname_0(),
	Variable_t18C81156491F857437BADE92E874B26983F85E25::get_offset_of_prefix_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE), -1, sizeof(XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1604[13] = 
{
	XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE::get_offset_of_scanner_0(),
	XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE::get_offset_of_parseDepth_1(),
	XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields::get_offset_of_temparray1_2(),
	XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields::get_offset_of_temparray2_3(),
	XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields::get_offset_of_temparray3_4(),
	XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields::get_offset_of_temparray4_5(),
	XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields::get_offset_of_temparray5_6(),
	XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields::get_offset_of_temparray6_7(),
	XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields::get_offset_of_temparray7_8(),
	XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields::get_offset_of_temparray8_9(),
	XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields::get_offset_of_temparray9_10(),
	XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields::get_offset_of_functionTable_11(),
	XPathParser_t4352BEB74756E9EEA3D09AA0EACA2E25D33244AE_StaticFields::get_offset_of_AxesTable_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (ParamInfo_t7A2C2C6976B72B72E5DDE9D83295FA5C3A83DE5C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1605[4] = 
{
	ParamInfo_t7A2C2C6976B72B72E5DDE9D83295FA5C3A83DE5C::get_offset_of_ftype_0(),
	ParamInfo_t7A2C2C6976B72B72E5DDE9D83295FA5C3A83DE5C::get_offset_of_minargs_1(),
	ParamInfo_t7A2C2C6976B72B72E5DDE9D83295FA5C3A83DE5C::get_offset_of_maxargs_2(),
	ParamInfo_t7A2C2C6976B72B72E5DDE9D83295FA5C3A83DE5C::get_offset_of_argTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1606[10] = 
{
	XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481::get_offset_of_xpathExpr_0(),
	XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481::get_offset_of_xpathExprIndex_1(),
	XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481::get_offset_of_kind_2(),
	XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481::get_offset_of_currentChar_3(),
	XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481::get_offset_of_name_4(),
	XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481::get_offset_of_prefix_5(),
	XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481::get_offset_of_stringValue_6(),
	XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481::get_offset_of_numberValue_7(),
	XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481::get_offset_of_canBeFunction_8(),
	XPathScanner_t0A7B6C3C5EB6670560039D459C772F71D5C08481::get_offset_of_xmlCharType_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (LexKind_t12FE968EFB884A0346676DCECA65929ED89C10E4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1607[32] = 
{
	LexKind_t12FE968EFB884A0346676DCECA65929ED89C10E4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (XPathDocumentNavigator_t4EA718BDD563CB09DE0E93644B2E3FECC8B6B498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1608[4] = 
{
	XPathDocumentNavigator_t4EA718BDD563CB09DE0E93644B2E3FECC8B6B498::get_offset_of_pageCurrent_4(),
	XPathDocumentNavigator_t4EA718BDD563CB09DE0E93644B2E3FECC8B6B498::get_offset_of_pageParent_5(),
	XPathDocumentNavigator_t4EA718BDD563CB09DE0E93644B2E3FECC8B6B498::get_offset_of_idxCurrent_6(),
	XPathDocumentNavigator_t4EA718BDD563CB09DE0E93644B2E3FECC8B6B498::get_offset_of_idxParent_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1609[7] = 
{
	XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0::get_offset_of_info_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0::get_offset_of_idxSibling_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0::get_offset_of_idxParent_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0::get_offset_of_idxSimilar_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0::get_offset_of_posOffset_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0::get_offset_of_props_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_tC207ED6C653E80055FE6C5ECD3E6137A326659A0::get_offset_of_value_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (XPathNodeRef_t6F631244BF7B58CE7DB9239662B4EE745CD54E14)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1610[2] = 
{
	XPathNodeRef_t6F631244BF7B58CE7DB9239662B4EE745CD54E14::get_offset_of_page_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNodeRef_t6F631244BF7B58CE7DB9239662B4EE745CD54E14::get_offset_of_idx_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (XPathNodeHelper_tDCBBEFE1E40E333E12319E06BD2ACC2685C1EB6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (XPathNodePageInfo_t116B772208F6DB8CCD46DE1F15A18344F0AFC05B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1612[3] = 
{
	XPathNodePageInfo_t116B772208F6DB8CCD46DE1F15A18344F0AFC05B::get_offset_of_pageNum_0(),
	XPathNodePageInfo_t116B772208F6DB8CCD46DE1F15A18344F0AFC05B::get_offset_of_nodeCount_1(),
	XPathNodePageInfo_t116B772208F6DB8CCD46DE1F15A18344F0AFC05B::get_offset_of_pageNext_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1613[13] = 
{
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7::get_offset_of_localName_0(),
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7::get_offset_of_namespaceUri_1(),
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7::get_offset_of_prefix_2(),
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7::get_offset_of_baseUri_3(),
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7::get_offset_of_pageParent_4(),
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7::get_offset_of_pageSibling_5(),
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7::get_offset_of_pageSimilar_6(),
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7::get_offset_of_doc_7(),
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7::get_offset_of_lineNumBase_8(),
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7::get_offset_of_linePosBase_9(),
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7::get_offset_of_hashCode_10(),
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7::get_offset_of_localNameHash_11(),
	XPathNodeInfoAtom_t6FF2C2B2096901C0BB3988616FBA285A67947AC7::get_offset_of_pageInfo_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (MonoTODOAttribute_t93F6D8CCDF532E8867F424F9A3B05BA28A7152C3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (LocalAppContextSwitches_tC0069108E3FEC5EEA8E7E80D817C1A80F9009674), -1, sizeof(LocalAppContextSwitches_tC0069108E3FEC5EEA8E7E80D817C1A80F9009674_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1615[2] = 
{
	LocalAppContextSwitches_tC0069108E3FEC5EEA8E7E80D817C1A80F9009674_StaticFields::get_offset_of_IgnoreEmptyKeySequences_0(),
	LocalAppContextSwitches_tC0069108E3FEC5EEA8E7E80D817C1A80F9009674_StaticFields::get_offset_of_DontThrowOnInvalidSurrogatePairs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2), -1, sizeof(AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1616[4] = 
{
	AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields::get_offset_of_DoneTask_0(),
	AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields::get_offset_of_DoneTaskTrue_1(),
	AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields::get_offset_of_DoneTaskFalse_2(),
	AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields::get_offset_of_DoneTaskZero_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1617[3] = 
{
	Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922::get_offset_of_leftOverBytes_0(),
	Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922::get_offset_of_leftOverBytesCount_1(),
	Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922::get_offset_of_charsLine_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1618[1] = 
{
	XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678::get_offset_of_rawWriter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1619[1] = 
{
	XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C::get_offset_of_xmlTextEncoder_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (BinHexDecoder_t2CCB202E9FC5A9055FB0B5945E193EA98E6C3EBB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1620[5] = 
{
	BinHexDecoder_t2CCB202E9FC5A9055FB0B5945E193EA98E6C3EBB::get_offset_of_buffer_0(),
	BinHexDecoder_t2CCB202E9FC5A9055FB0B5945E193EA98E6C3EBB::get_offset_of_curIndex_1(),
	BinHexDecoder_t2CCB202E9FC5A9055FB0B5945E193EA98E6C3EBB::get_offset_of_endIndex_2(),
	BinHexDecoder_t2CCB202E9FC5A9055FB0B5945E193EA98E6C3EBB::get_offset_of_hasHalfByteCached_3(),
	BinHexDecoder_t2CCB202E9FC5A9055FB0B5945E193EA98E6C3EBB::get_offset_of_cachedHalfByte_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (BinHexEncoder_t1D70914F68F07D8480A2946DA87C8A41AD386DBA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (BinXmlToken_t592F736A62786D804D147AA2E468F02DB3FCCF39)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1622[67] = 
{
	BinXmlToken_t592F736A62786D804D147AA2E468F02DB3FCCF39::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A)+ sizeof (RuntimeObject), sizeof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A ), sizeof(BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1623[21] = 
{
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A::get_offset_of_m_bLen_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A::get_offset_of_m_bPrec_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A::get_offset_of_m_bScale_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A::get_offset_of_m_bSign_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A::get_offset_of_m_data1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A::get_offset_of_m_data2_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A::get_offset_of_m_data3_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A::get_offset_of_m_data4_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields::get_offset_of_NUMERIC_MAX_PRECISION_8(),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields::get_offset_of_MaxPrecision_9(),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields::get_offset_of_MaxScale_10(),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields::get_offset_of_x_cNumeMax_11(),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields::get_offset_of_x_lInt32Base_12(),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields::get_offset_of_x_ulInt32Base_13(),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields::get_offset_of_x_ulInt32BaseForMod_14(),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields::get_offset_of_x_llMax_15(),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields::get_offset_of_DUINT_BASE_16(),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields::get_offset_of_DUINT_BASE2_17(),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields::get_offset_of_DUINT_BASE3_18(),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields::get_offset_of_x_rgulShiftBase_19(),
	BinXmlSqlDecimal_t09AD9ABF3AF562886D406D9F0BDE5407E15EC87A_StaticFields::get_offset_of_rgCLenFromPrec_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (BinXmlSqlMoney_tAD1ACE807DEA1505FC669E2DD513D272E2E66EEC)+ sizeof (RuntimeObject), sizeof(BinXmlSqlMoney_tAD1ACE807DEA1505FC669E2DD513D272E2E66EEC ), 0, 0 };
extern const int32_t g_FieldOffsetTable1624[1] = 
{
	BinXmlSqlMoney_tAD1ACE807DEA1505FC669E2DD513D272E2E66EEC::get_offset_of_data_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53), -1, sizeof(BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1625[6] = 
{
	BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_StaticFields::get_offset_of_KatmaiTimeScaleMultiplicator_0(),
	BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_StaticFields::get_offset_of_SQLTicksPerMillisecond_1(),
	BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_StaticFields::get_offset_of_SQLTicksPerSecond_2(),
	BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_StaticFields::get_offset_of_SQLTicksPerMinute_3(),
	BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_StaticFields::get_offset_of_SQLTicksPerHour_4(),
	BinXmlDateTime_tD70CEAFAC7BB4C010DC6A7C809B81A8D28A63B53_StaticFields::get_offset_of_SQLTicksPerDay_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB), -1, sizeof(XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1626[54] = 
{
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB_StaticFields::get_offset_of_TypeOfObject_3(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB_StaticFields::get_offset_of_TypeOfString_4(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB_StaticFields::get_offset_of_TokenTypeMap_5(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB_StaticFields::get_offset_of_XsdKatmaiTimeScaleToValueLengthMap_6(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB_StaticFields::get_offset_of_ScanState2ReadState_7(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_inStrm_8(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_data_9(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_pos_10(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_mark_11(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_end_12(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_offset_13(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_eof_14(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_sniffed_15(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_isEmpty_16(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_docState_17(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_symbolTables_18(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_xnt_19(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_xntFromSettings_20(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_xml_21(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_xmlns_22(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_nsxmlns_23(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_baseUri_24(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_state_25(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_nodetype_26(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_token_27(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_attrIndex_28(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_qnameOther_29(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_qnameElement_30(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_parentNodeType_31(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_elementStack_32(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_elemDepth_33(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_attributes_34(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_attrHashTbl_35(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_attrCount_36(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_posAfterAttrs_37(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_xmlspacePreserve_38(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_tokLen_39(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_tokDataPos_40(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_hasTypedValue_41(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_valueType_42(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_stringValue_43(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_namespaces_44(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_prevNameInfo_45(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_textXmlReader_46(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_closeInput_47(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_checkCharacters_48(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_ignoreWhitespace_49(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_ignorePIs_50(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_ignoreComments_51(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_dtdProcessing_52(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_hasher_53(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_xmlCharType_54(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_unicode_55(),
	XmlSqlBinaryReader_t725CF548EC3FE5A2D7A7946F1A3E762C71A291DB::get_offset_of_version_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (ScanState_t7FFA2F3C0ACF51BC1C8E551490732B29A6323475)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1627[10] = 
{
	ScanState_t7FFA2F3C0ACF51BC1C8E551490732B29A6323475::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F)+ sizeof (RuntimeObject), sizeof(QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1628[3] = 
{
	QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F::get_offset_of_prefix_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F::get_offset_of_localname_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QName_t49332A07486EFE325DF0D3F34BE798ED3497C42F::get_offset_of_namespaceUri_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (ElemInfo_t930901F4C4D70BD460913E85E8B87AB5C5A8571F)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1629[5] = 
{
	ElemInfo_t930901F4C4D70BD460913E85E8B87AB5C5A8571F::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElemInfo_t930901F4C4D70BD460913E85E8B87AB5C5A8571F::get_offset_of_xmlLang_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElemInfo_t930901F4C4D70BD460913E85E8B87AB5C5A8571F::get_offset_of_xmlSpace_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElemInfo_t930901F4C4D70BD460913E85E8B87AB5C5A8571F::get_offset_of_xmlspacePreserve_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElemInfo_t930901F4C4D70BD460913E85E8B87AB5C5A8571F::get_offset_of_nsdecls_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (AttrInfo_tC56788540A1F53E77E0A21E93000A66846FAFC0B)+ sizeof (RuntimeObject), sizeof(AttrInfo_tC56788540A1F53E77E0A21E93000A66846FAFC0B_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1630[5] = 
{
	AttrInfo_tC56788540A1F53E77E0A21E93000A66846FAFC0B::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttrInfo_tC56788540A1F53E77E0A21E93000A66846FAFC0B::get_offset_of_val_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttrInfo_tC56788540A1F53E77E0A21E93000A66846FAFC0B::get_offset_of_contentPos_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttrInfo_tC56788540A1F53E77E0A21E93000A66846FAFC0B::get_offset_of_hashCode_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttrInfo_tC56788540A1F53E77E0A21E93000A66846FAFC0B::get_offset_of_prevHash_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1631[6] = 
{
	NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B::get_offset_of_prefix_0(),
	NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B::get_offset_of_uri_1(),
	NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B::get_offset_of_scopeLink_2(),
	NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B::get_offset_of_prevLink_3(),
	NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B::get_offset_of_scope_4(),
	NamespaceDecl_t9E6E447DBA857B63A848C5CAACBB5C176493473B::get_offset_of_implied_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1632[4] = 
{
	SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E::get_offset_of_symtable_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E::get_offset_of_symCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E::get_offset_of_qnametable_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SymbolTables_t8F49FC5423EF06561EBAF65E6130FA03C025325E::get_offset_of_qnameCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1633[3] = 
{
	NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175::get_offset_of_symbolTables_0(),
	NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175::get_offset_of_docState_1(),
	NestedBinXml_t5E9B989BEC527EFE0C0907B63FCF17C3AC25D175::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1634[3] = 
{
	BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7::get_offset_of_bitStack_0(),
	BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7::get_offset_of_stackPos_1(),
	BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7::get_offset_of_curr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (Bits_t6CCF20605799DC40E70BCBC72EF872899C211658), -1, sizeof(Bits_t6CCF20605799DC40E70BCBC72EF872899C211658_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1635[5] = 
{
	Bits_t6CCF20605799DC40E70BCBC72EF872899C211658_StaticFields::get_offset_of_MASK_0101010101010101_0(),
	Bits_t6CCF20605799DC40E70BCBC72EF872899C211658_StaticFields::get_offset_of_MASK_0011001100110011_1(),
	Bits_t6CCF20605799DC40E70BCBC72EF872899C211658_StaticFields::get_offset_of_MASK_0000111100001111_2(),
	Bits_t6CCF20605799DC40E70BCBC72EF872899C211658_StaticFields::get_offset_of_MASK_0000000011111111_3(),
	Bits_t6CCF20605799DC40E70BCBC72EF872899C211658_StaticFields::get_offset_of_MASK_1111111111111111_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1636[4] = 
{
	ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75::get_offset_of_stack_0(),
	ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75::get_offset_of_growthRate_1(),
	ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75::get_offset_of_top_2(),
	ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75::get_offset_of_size_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (BinaryCompatibility_t12E05D85EDA182BE84FFFDDBE58545DC7B43AD72), -1, sizeof(BinaryCompatibility_t12E05D85EDA182BE84FFFDDBE58545DC7B43AD72_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1637[1] = 
{
	BinaryCompatibility_t12E05D85EDA182BE84FFFDDBE58545DC7B43AD72_StaticFields::get_offset_of__targetsAtLeast_Desktop_V4_5_2_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1638[5] = 
{
	CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE::get_offset_of_fallbackBuffer_4(),
	CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE::get_offset_of_textContentMarks_5(),
	CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE::get_offset_of_endMarkPos_6(),
	CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE::get_offset_of_curMarkPos_7(),
	CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE::get_offset_of_startOffset_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1639[3] = 
{
	CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8::get_offset_of_parent_7(),
	CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8::get_offset_of_charEntity_8(),
	CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8::get_offset_of_charEntityIndex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (ConformanceLevel_t42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1640[4] = 
{
	ConformanceLevel_t42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (DtdProcessing_tAB3B800A5365ED9C5841D71F40E5A38840D32DB3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1641[4] = 
{
	DtdProcessing_tAB3B800A5365ED9C5841D71F40E5A38840D32DB3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (EntityHandling_t15C89E916C1AC46126DCF896A6317CE364B8F89B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1642[3] = 
{
	EntityHandling_t15C89E916C1AC46126DCF896A6317CE364B8F89B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50), -1, sizeof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1643[9] = 
{
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50::get_offset_of_elementScope_35(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50::get_offset_of_currentElementProperties_36(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50::get_offset_of_currentAttributeProperties_37(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50::get_offset_of_endsWithAmpersand_38(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50::get_offset_of_uriEscapingBuffer_39(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50::get_offset_of_mediaType_40(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50::get_offset_of_doNotEscapeUriAttributes_41(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50_StaticFields::get_offset_of_elementPropertySearch_42(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50_StaticFields::get_offset_of_attributePropertySearch_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1644[4] = 
{
	HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2::get_offset_of_indentLevel_44(),
	HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2::get_offset_of_endBlockPos_45(),
	HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2::get_offset_of_indentChars_46(),
	HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2::get_offset_of_newLineOnAttributes_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4), -1, sizeof(HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1645[2] = 
{
	HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4_StaticFields::get_offset_of_htmlElements_0(),
	HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4_StaticFields::get_offset_of_htmlAttributes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825), -1, sizeof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1646[9] = 
{
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825::get_offset_of_elementScope_26(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825::get_offset_of_currentElementProperties_27(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825::get_offset_of_currentAttributeProperties_28(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825::get_offset_of_endsWithAmpersand_29(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825::get_offset_of_uriEscapingBuffer_30(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825::get_offset_of_mediaType_31(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825::get_offset_of_doNotEscapeUriAttributes_32(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825_StaticFields::get_offset_of_elementPropertySearch_33(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825_StaticFields::get_offset_of_attributePropertySearch_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1647[4] = 
{
	HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55::get_offset_of_indentLevel_35(),
	HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55::get_offset_of_endBlockPos_36(),
	HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55::get_offset_of_indentChars_37(),
	HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55::get_offset_of_newLineOnAttributes_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (OnRemoveWriter_t40CE623324C30930692639D6172B422C25AA6370), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (IncrementalReadDecoder_t787BFB5889B01B88DDA030C503A0C2E0525CA723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (IncrementalReadDummyDecoder_t64BDE5CCA28D8D8979B7683C594FC2A85DC70663), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (NamespaceHandling_tECE89F8A3E0CF300C5A18E41FBC65494C7EB3618)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1662[3] = 
{
	NamespaceHandling_tECE89F8A3E0CF300C5A18E41FBC65494C7EB3618::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (NewLineHandling_t35189DCBC088E592125C4F2FF4EDD3DB9F594543)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1663[4] = 
{
	NewLineHandling_t35189DCBC088E592125C4F2FF4EDD3DB9F594543::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1664[12] = 
{
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_wrapped_3(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_inCDataSection_4(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_lookupCDataElems_5(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_bitsCData_6(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_qnameCData_7(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_outputDocType_8(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_checkWellFormedDoc_9(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_hasDocElem_10(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_inAttr_11(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_systemId_12(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_publicId_13(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_depth_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1665[4] = 
{
	ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797::get_offset_of_reader_0(),
	ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797::get_offset_of_state_1(),
	ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797::get_offset_of_valueOffset_2(),
	ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797::get_offset_of_isEnd_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (State_t0C92915E8C4E902724D043F981062725DE46FEAA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1666[4] = 
{
	State_t0C92915E8C4E902724D043F981062725DE46FEAA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (ElementProperties_tFC06E5E2552285434B447CACE58013EA7EE5AF8E)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1667[10] = 
{
	ElementProperties_tFC06E5E2552285434B447CACE58013EA7EE5AF8E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (AttributeProperties_tFB77E07917EF07DD10F6FBC47F7FB0E07F0A0C2F)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1668[5] = 
{
	AttributeProperties_tFB77E07917EF07DD10F6FBC47F7FB0E07F0A0C2F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1669[1] = 
{
	TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2::get_offset_of_nodeBuffer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (ReadState_tAAF15D8DE96B6A22379D2B6FEF22764640D05DD0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1670[6] = 
{
	ReadState_tAAF15D8DE96B6A22379D2B6FEF22764640D05DD0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3), -1, sizeof(SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1671[2] = 
{
	SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_StaticFields::get_offset_of_hashCodeDelegate_0(),
	SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3::get_offset_of_hashCodeRandomizer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (TextEncodedRawTextWriter_t828BEA9C3C114D3F2445708667B5DA7160806CA6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (TextUtf8RawTextWriter_tF6F774D5F5995D09C8D65F10F74BCE6DD7089BA6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1675[11] = 
{
	ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F::get_offset_of_localName_0(),
	ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F::get_offset_of_namespaceUri_1(),
	ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F::get_offset_of_prefix_2(),
	ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F::get_offset_of_nameWPrefix_3(),
	ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F::get_offset_of_rawValue_4(),
	ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F::get_offset_of_originalStringValue_5(),
	ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F::get_offset_of_depth_6(),
	ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F::get_offset_of_attributePSVIInfo_7(),
	ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F::get_offset_of_nodeType_8(),
	ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F::get_offset_of_lineNo_9(),
	ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F::get_offset_of_linePos_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (ValidationType_tFA1BE5AAA395D6B8033755CD823243B8FAC2673E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1676[6] = 
{
	ValidationType_tFA1BE5AAA395D6B8033755CD823243B8FAC2673E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (WhitespaceHandling_t46A500512D84C27CBFB4580E40C5D583F4B149DC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1677[4] = 
{
	WhitespaceHandling_t46A500512D84C27CBFB4580E40C5D583F4B149DC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (XmlAsyncCheckReader_t2D4FE06C976E98C24DDD496E1E2323ECAB8E6968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1678[2] = 
{
	XmlAsyncCheckReader_t2D4FE06C976E98C24DDD496E1E2323ECAB8E6968::get_offset_of_coreReader_3(),
	XmlAsyncCheckReader_t2D4FE06C976E98C24DDD496E1E2323ECAB8E6968::get_offset_of_lastTask_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (XmlAsyncCheckReaderWithNS_t52C90781655051B73C07890BE6B1C7327AE8593C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1679[1] = 
{
	XmlAsyncCheckReaderWithNS_t52C90781655051B73C07890BE6B1C7327AE8593C::get_offset_of_readerAsIXmlNamespaceResolver_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (XmlAsyncCheckReaderWithLineInfo_tFF91FA10CC8CF8DD006D69CFB36E0DE795F1533B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1680[1] = 
{
	XmlAsyncCheckReaderWithLineInfo_tFF91FA10CC8CF8DD006D69CFB36E0DE795F1533B::get_offset_of_readerAsIXmlLineInfo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (XmlAsyncCheckReaderWithLineInfoNS_tDA344AA17D0B0512BDC02C145605922DC1875327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1681[1] = 
{
	XmlAsyncCheckReaderWithLineInfoNS_tDA344AA17D0B0512BDC02C145605922DC1875327::get_offset_of_readerAsIXmlNamespaceResolver_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (XmlAsyncCheckReaderWithLineInfoNSSchema_t3716760293E48E0CCF156F9BBA437D1ACE28F047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1682[1] = 
{
	XmlAsyncCheckReaderWithLineInfoNSSchema_t3716760293E48E0CCF156F9BBA437D1ACE28F047::get_offset_of_readerAsIXmlSchemaInfo_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (XmlAsyncCheckWriter_t33C862CF7ABB0B088B0476A97B43E1D97119DC23), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1683[2] = 
{
	XmlAsyncCheckWriter_t33C862CF7ABB0B088B0476A97B43E1D97119DC23::get_offset_of_coreWriter_1(),
	XmlAsyncCheckWriter_t33C862CF7ABB0B088B0476A97B43E1D97119DC23::get_offset_of_lastTask_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1684[6] = 
{
	XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71::get_offset_of_wrapped_3(),
	XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71::get_offset_of_onRemove_4(),
	XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71::get_offset_of_writerSettings_5(),
	XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71::get_offset_of_eventCache_6(),
	XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71::get_offset_of_textWriter_7(),
	XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71::get_offset_of_strm_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1685[32] = 
{
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_useAsync_3(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_bufBytes_4(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_stream_5(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_encoding_6(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_xmlCharType_7(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_bufPos_8(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_textPos_9(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_contentPos_10(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_cdataPos_11(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_attrEndPos_12(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_bufLen_13(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_writeToNull_14(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_hadDoubleBracket_15(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_inAttributeValue_16(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_bufBytesUsed_17(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_bufChars_18(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_encoder_19(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_writer_20(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_trackTextContent_21(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_inTextContent_22(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_lastMarkPos_23(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_textContentMarks_24(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_charEntityFallback_25(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_newLineHandling_26(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_closeOutput_27(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_omitXmlDeclaration_28(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_newLineChars_29(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_checkCharacters_30(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_standalone_31(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_outputMethod_32(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_autoXmlDeclaration_33(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_mergeCDataSections_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1686[6] = 
{
	XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0::get_offset_of_indentLevel_35(),
	XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0::get_offset_of_newLineOnAttributes_36(),
	XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0::get_offset_of_indentChars_37(),
	XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0::get_offset_of_mixedContent_38(),
	XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0::get_offset_of_mixedContentStack_39(),
	XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0::get_offset_of_conformanceLevel_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1687[6] = 
{
	XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4::get_offset_of_pages_3(),
	XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4::get_offset_of_pageCurr_4(),
	XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4::get_offset_of_pageSize_5(),
	XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4::get_offset_of_hasRootNode_6(),
	XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4::get_offset_of_singleText_7(),
	XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4::get_offset_of_baseUri_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (XmlEventType_tE042EAA577D37C5BAD142325B493273F6B8CC559)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1688[27] = 
{
	XmlEventType_tE042EAA577D37C5BAD142325B493273F6B8CC559::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4)+ sizeof (RuntimeObject), sizeof(XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1689[5] = 
{
	XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4::get_offset_of_eventType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4::get_offset_of_s1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4::get_offset_of_s2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4::get_offset_of_s3_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4::get_offset_of_o_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1690[10] = 
{
	XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279::get_offset_of__nt_0(),
	XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279::get_offset_of__nsMgr_1(),
	XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279::get_offset_of__docTypeName_2(),
	XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279::get_offset_of__pubId_3(),
	XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279::get_offset_of__sysId_4(),
	XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279::get_offset_of__internalSubset_5(),
	XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279::get_offset_of__xmlLang_6(),
	XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279::get_offset_of__xmlSpace_7(),
	XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279::get_offset_of__baseURI_8(),
	XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279::get_offset_of__encoding_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1691[2] = 
{
	XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2::get_offset_of_base64Encoder_1(),
	XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2::get_offset_of_resolver_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB), -1, sizeof(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1692[3] = 
{
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields::get_offset_of_IsTextualNodeBitmap_0(),
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields::get_offset_of_CanReadContentAsBitmap_1(),
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields::get_offset_of_HasValueBitmap_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65), -1, sizeof(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1693[21] = 
{
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_useAsync_0(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_nameTable_1(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_xmlResolver_2(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_lineNumberOffset_3(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_linePositionOffset_4(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_conformanceLevel_5(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_checkCharacters_6(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_maxCharactersInDocument_7(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_maxCharactersFromEntities_8(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_ignoreWhitespace_9(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_ignorePIs_10(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_ignoreComments_11(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_dtdProcessing_12(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_validationType_13(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_validationFlags_14(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_schemas_15(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_valEventHandler_16(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_closeInput_17(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_isReadOnly_18(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65::get_offset_of_U3CIsXmlResolverSetU3Ek__BackingField_19(),
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65_StaticFields::get_offset_of_s_enableLegacyXmlSettings_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (XmlSpace_tC1A644D65B6BE72CF02BA2687B5AE169713271AB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1694[4] = 
{
	XmlSpace_tC1A644D65B6BE72CF02BA2687B5AE169713271AB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1695[6] = 
{
	XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475::get_offset_of_textWriter_0(),
	XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475::get_offset_of_inAttribute_1(),
	XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475::get_offset_of_quoteChar_2(),
	XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475::get_offset_of_attrValue_3(),
	XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475::get_offset_of_cacheAttrValue_4(),
	XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475::get_offset_of_xmlCharType_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (XmlTextReader_tAF28DD94DDC4A59EE85A627A606C9347C8149A8C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1696[1] = 
{
	XmlTextReader_tAF28DD94DDC4A59EE85A627A606C9347C8149A8C::get_offset_of_impl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1697[77] = 
{
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_useAsync_3(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_laterInitParam_4(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_xmlCharType_5(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_ps_6(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_parsingFunction_7(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_nextParsingFunction_8(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_nextNextParsingFunction_9(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_nodes_10(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_curNode_11(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_index_12(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_curAttrIndex_13(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_attrCount_14(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_attrHashtable_15(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_attrDuplWalkCount_16(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_attrNeedNamespaceLookup_17(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_fullAttrCleanup_18(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_attrDuplSortingArray_19(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_nameTable_20(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_nameTableFromSettings_21(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_xmlResolver_22(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_url_23(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_normalize_24(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_supportNamespaces_25(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_whitespaceHandling_26(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_dtdProcessing_27(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_entityHandling_28(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_ignorePIs_29(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_ignoreComments_30(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_checkCharacters_31(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_lineNumberOffset_32(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_linePositionOffset_33(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_closeInput_34(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_maxCharactersInDocument_35(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_maxCharactersFromEntities_36(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_v1Compat_37(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_namespaceManager_38(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_lastPrefix_39(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_xmlContext_40(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_parsingStatesStack_41(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_parsingStatesStackTop_42(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_reportedBaseUri_43(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_reportedEncoding_44(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_dtdInfo_45(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_fragmentType_46(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_fragmentParserContext_47(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_fragment_48(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_incReadDecoder_49(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_incReadState_50(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_incReadLineInfo_51(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_incReadDepth_52(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_incReadLeftStartPos_53(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_incReadLeftEndPos_54(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_attributeValueBaseEntityId_55(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_emptyEntityInAttributeResolved_56(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_validationEventHandling_57(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_onDefaultAttributeUse_58(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_validatingReaderCompatFlag_59(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_addDefaultAttributesAndNormalize_60(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_stringBuilder_61(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_rootElementParsed_62(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_standalone_63(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_nextEntityId_64(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_parsingMode_65(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_readState_66(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_lastEntity_67(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_afterResetState_68(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_documentStartBytePos_69(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_readValueOffset_70(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_charactersInDocument_71(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_charactersFromEntities_72(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_currentEntities_73(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_disableUndeclaredEntityCheck_74(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_outerReader_75(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_xmlResolverIsSet_76(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_Xml_77(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_XmlNs_78(),
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61::get_offset_of_parseText_dummyTask_79(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (ParsingFunction_t257574D1598B3E416A38FAB3204B4298FB2672B7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1698[27] = 
{
	ParsingFunction_t257574D1598B3E416A38FAB3204B4298FB2672B7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (ParsingMode_t16C9A13D4CD287424B850D09ABF6377BAA2A32EB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1699[4] = 
{
	ParsingMode_t16C9A13D4CD287424B850D09ABF6377BAA2A32EB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
