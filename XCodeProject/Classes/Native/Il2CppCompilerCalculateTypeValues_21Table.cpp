﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.XmlSchemaObject>
struct Dictionary_2_tBCC3C44F7B144318C5907155287F49A0A66F9BBA;
// System.Collections.Generic.List`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>
struct List_1_t7C6AA94EA9520975CB29B2B4CA6C9EAED364ECD3;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t456EB67407D2045A257B66A3A25A825E883FD027;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Collections.SortedList
struct SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.ReaderWriterLock
struct ReaderWriterLock_t7EEE8E1E96D63FD5668920B8274B13D184983349;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.Xml.HWStack
struct HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE;
// System.Xml.IValidationEventHandling
struct IValidationEventHandling_tABD39B6B973C0A0DC259D55D8C4179A43ACAB41B;
// System.Xml.PositionInfo
struct PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684;
// System.Xml.Schema.CompiledIdentityConstraint
struct CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E;
// System.Xml.Schema.IdRefNode
struct IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2;
// System.Xml.Schema.NamespaceList
struct NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C;
// System.Xml.Schema.Parser
struct Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920;
// System.Xml.Schema.SchemaAttDef
struct SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4;
// System.Xml.Schema.SchemaElementDecl
struct SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41;
// System.Xml.Schema.SchemaNames
struct SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF;
// System.Xml.Schema.ValidationState
struct ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B;
// System.Xml.Schema.XdrBuilder
struct XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2;
// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[]
struct XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B;
// System.Xml.Schema.XdrBuilder/XdrBeginChildFunction
struct XdrBeginChildFunction_t0A5E4A12B86755E9EA9D9E723493EBBE9FFBF472;
// System.Xml.Schema.XdrBuilder/XdrBuildFunction
struct XdrBuildFunction_tB7A90FF323AFC1DBE190CA3721EB16B45E48B8A7;
// System.Xml.Schema.XdrBuilder/XdrEndChildFunction
struct XdrEndChildFunction_t9A1AE00DFBA27B69E9A156C48AAFEF0425F0601F;
// System.Xml.Schema.XdrBuilder/XdrInitFunction
struct XdrInitFunction_tE54658948C45C11D13B781182403CAA3ADBC658D;
// System.Xml.Schema.XmlAtomicValue/NamespacePrefixForQName
struct NamespacePrefixForQName_t80F0083B9C2151D487D41CCDD89B668C21902D8F;
// System.Xml.Schema.XmlSchema
struct XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F;
// System.Xml.Schema.XmlSchemaAnnotation
struct XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725;
// System.Xml.Schema.XmlSchemaAnyAttribute
struct XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B;
// System.Xml.Schema.XmlSchemaChoice
struct XmlSchemaChoice_tEC175BFF045FDA4DECF5A938922D6CDC4216653B;
// System.Xml.Schema.XmlSchemaCollection
struct XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5;
// System.Xml.Schema.XmlSchemaCompilationSettings
struct XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC;
// System.Xml.Schema.XmlSchemaContent
struct XmlSchemaContent_tE0E45FE215D74AE629A316B2CDA06480C07278D3;
// System.Xml.Schema.XmlSchemaContentModel
struct XmlSchemaContentModel_tB0209C69ADC1F671FA4ADDE88CC88C8959E84E45;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D;
// System.Xml.Schema.XmlSchemaGroup
struct XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2;
// System.Xml.Schema.XmlSchemaGroupBase
struct XmlSchemaGroupBase_tC18B7AAFF241664404A1FB5D936508AB2F4D208F;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B;
// System.Xml.Schema.XmlSchemaObjectCollection
struct XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280;
// System.Xml.Schema.XmlSchemaObjectTable
struct XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833;
// System.Xml.Schema.XmlSchemaParticle
struct XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A;
// System.Xml.Schema.XmlSchemaSimpleTypeContent
struct XmlSchemaSimpleTypeContent_t87EF2A2F4B2F3D7827EA18B0FCB8393EA0201A3E;
// System.Xml.Schema.XmlSchemaSimpleType[]
struct XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9;
// System.Xml.Schema.XmlSchemaXPath
struct XmlSchemaXPath_t6D090E4243F2A15A6A5E4EF5B17B0740711C6C34;
// System.Xml.Serialization.XmlSerializerNamespaces
struct XmlSerializerNamespaces_t31E17AE7CF53591901A576C48D1E446BB985676F;
// System.Xml.XmlAttribute[]
struct XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908;
// System.Xml.XmlDocument
struct XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97;
// System.Xml.XmlNameTable
struct XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F;
// System.Xml.XmlNode[]
struct XmlNodeU5BU5D_tBF402FAC28AE903AC3FF845775405A2274B701AF;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD;
// System.Xml.XmlQualifiedName[]
struct XmlQualifiedNameU5BU5D_t375EB02A213AC919E02336B57A412BB761AFA18B;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65;
// System.Xml.XmlResolver
struct XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280;
// System.Xml.XmlValidatingReaderImpl
struct XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef COLLECTIONBASE_TF5D4583FF325726066A9803839A04E9C0084ED01_H
#define COLLECTIONBASE_TF5D4583FF325726066A9803839A04E9C0084ED01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_tF5D4583FF325726066A9803839A04E9C0084ED01  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_tF5D4583FF325726066A9803839A04E9C0084ED01, ___list_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_list_0() const { return ___list_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_TF5D4583FF325726066A9803839A04E9C0084ED01_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BASEVALIDATOR_TB0AD5FCB23C61EC8841C153CEB7C77E569246B43_H
#define BASEVALIDATOR_TB0AD5FCB23C61EC8841C153CEB7C77E569246B43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BaseValidator
struct  BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaCollection System.Xml.Schema.BaseValidator::schemaCollection
	XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * ___schemaCollection_0;
	// System.Xml.IValidationEventHandling System.Xml.Schema.BaseValidator::eventHandling
	RuntimeObject* ___eventHandling_1;
	// System.Xml.XmlNameTable System.Xml.Schema.BaseValidator::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_2;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.BaseValidator::schemaNames
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * ___schemaNames_3;
	// System.Xml.PositionInfo System.Xml.Schema.BaseValidator::positionInfo
	PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * ___positionInfo_4;
	// System.Xml.XmlResolver System.Xml.Schema.BaseValidator::xmlResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___xmlResolver_5;
	// System.Uri System.Xml.Schema.BaseValidator::baseUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___baseUri_6;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.BaseValidator::schemaInfo
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * ___schemaInfo_7;
	// System.Xml.XmlValidatingReaderImpl System.Xml.Schema.BaseValidator::reader
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * ___reader_8;
	// System.Xml.XmlQualifiedName System.Xml.Schema.BaseValidator::elementName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___elementName_9;
	// System.Xml.Schema.ValidationState System.Xml.Schema.BaseValidator::context
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * ___context_10;
	// System.Text.StringBuilder System.Xml.Schema.BaseValidator::textValue
	StringBuilder_t * ___textValue_11;
	// System.String System.Xml.Schema.BaseValidator::textString
	String_t* ___textString_12;
	// System.Boolean System.Xml.Schema.BaseValidator::hasSibling
	bool ___hasSibling_13;
	// System.Boolean System.Xml.Schema.BaseValidator::checkDatatype
	bool ___checkDatatype_14;

public:
	inline static int32_t get_offset_of_schemaCollection_0() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___schemaCollection_0)); }
	inline XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * get_schemaCollection_0() const { return ___schemaCollection_0; }
	inline XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 ** get_address_of_schemaCollection_0() { return &___schemaCollection_0; }
	inline void set_schemaCollection_0(XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * value)
	{
		___schemaCollection_0 = value;
		Il2CppCodeGenWriteBarrier((&___schemaCollection_0), value);
	}

	inline static int32_t get_offset_of_eventHandling_1() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___eventHandling_1)); }
	inline RuntimeObject* get_eventHandling_1() const { return ___eventHandling_1; }
	inline RuntimeObject** get_address_of_eventHandling_1() { return &___eventHandling_1; }
	inline void set_eventHandling_1(RuntimeObject* value)
	{
		___eventHandling_1 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandling_1), value);
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___nameTable_2)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_schemaNames_3() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___schemaNames_3)); }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * get_schemaNames_3() const { return ___schemaNames_3; }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 ** get_address_of_schemaNames_3() { return &___schemaNames_3; }
	inline void set_schemaNames_3(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * value)
	{
		___schemaNames_3 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_3), value);
	}

	inline static int32_t get_offset_of_positionInfo_4() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___positionInfo_4)); }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * get_positionInfo_4() const { return ___positionInfo_4; }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 ** get_address_of_positionInfo_4() { return &___positionInfo_4; }
	inline void set_positionInfo_4(PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * value)
	{
		___positionInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___positionInfo_4), value);
	}

	inline static int32_t get_offset_of_xmlResolver_5() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___xmlResolver_5)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_xmlResolver_5() const { return ___xmlResolver_5; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_xmlResolver_5() { return &___xmlResolver_5; }
	inline void set_xmlResolver_5(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___xmlResolver_5 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_5), value);
	}

	inline static int32_t get_offset_of_baseUri_6() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___baseUri_6)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_baseUri_6() const { return ___baseUri_6; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_baseUri_6() { return &___baseUri_6; }
	inline void set_baseUri_6(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___baseUri_6 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_6), value);
	}

	inline static int32_t get_offset_of_schemaInfo_7() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___schemaInfo_7)); }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * get_schemaInfo_7() const { return ___schemaInfo_7; }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 ** get_address_of_schemaInfo_7() { return &___schemaInfo_7; }
	inline void set_schemaInfo_7(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * value)
	{
		___schemaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_7), value);
	}

	inline static int32_t get_offset_of_reader_8() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___reader_8)); }
	inline XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * get_reader_8() const { return ___reader_8; }
	inline XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E ** get_address_of_reader_8() { return &___reader_8; }
	inline void set_reader_8(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * value)
	{
		___reader_8 = value;
		Il2CppCodeGenWriteBarrier((&___reader_8), value);
	}

	inline static int32_t get_offset_of_elementName_9() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___elementName_9)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_elementName_9() const { return ___elementName_9; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_elementName_9() { return &___elementName_9; }
	inline void set_elementName_9(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___elementName_9 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_9), value);
	}

	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___context_10)); }
	inline ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * get_context_10() const { return ___context_10; }
	inline ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier((&___context_10), value);
	}

	inline static int32_t get_offset_of_textValue_11() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___textValue_11)); }
	inline StringBuilder_t * get_textValue_11() const { return ___textValue_11; }
	inline StringBuilder_t ** get_address_of_textValue_11() { return &___textValue_11; }
	inline void set_textValue_11(StringBuilder_t * value)
	{
		___textValue_11 = value;
		Il2CppCodeGenWriteBarrier((&___textValue_11), value);
	}

	inline static int32_t get_offset_of_textString_12() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___textString_12)); }
	inline String_t* get_textString_12() const { return ___textString_12; }
	inline String_t** get_address_of_textString_12() { return &___textString_12; }
	inline void set_textString_12(String_t* value)
	{
		___textString_12 = value;
		Il2CppCodeGenWriteBarrier((&___textString_12), value);
	}

	inline static int32_t get_offset_of_hasSibling_13() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___hasSibling_13)); }
	inline bool get_hasSibling_13() const { return ___hasSibling_13; }
	inline bool* get_address_of_hasSibling_13() { return &___hasSibling_13; }
	inline void set_hasSibling_13(bool value)
	{
		___hasSibling_13 = value;
	}

	inline static int32_t get_offset_of_checkDatatype_14() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___checkDatatype_14)); }
	inline bool get_checkDatatype_14() const { return ___checkDatatype_14; }
	inline bool* get_address_of_checkDatatype_14() { return &___checkDatatype_14; }
	inline void set_checkDatatype_14(bool value)
	{
		___checkDatatype_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVALIDATOR_TB0AD5FCB23C61EC8841C153CEB7C77E569246B43_H
#ifndef ATTRIBUTECONTENT_T3C46FE57445E1995CC5754D024077DFD26C0C290_H
#define ATTRIBUTECONTENT_T3C46FE57445E1995CC5754D024077DFD26C0C290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/AttributeContent
struct  AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290  : public RuntimeObject
{
public:
	// System.Xml.Schema.SchemaAttDef System.Xml.Schema.XdrBuilder/AttributeContent::_AttDef
	SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 * ____AttDef_0;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XdrBuilder/AttributeContent::_Name
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ____Name_1;
	// System.String System.Xml.Schema.XdrBuilder/AttributeContent::_Prefix
	String_t* ____Prefix_2;
	// System.Boolean System.Xml.Schema.XdrBuilder/AttributeContent::_Required
	bool ____Required_3;
	// System.UInt32 System.Xml.Schema.XdrBuilder/AttributeContent::_MinVal
	uint32_t ____MinVal_4;
	// System.UInt32 System.Xml.Schema.XdrBuilder/AttributeContent::_MaxVal
	uint32_t ____MaxVal_5;
	// System.UInt32 System.Xml.Schema.XdrBuilder/AttributeContent::_MaxLength
	uint32_t ____MaxLength_6;
	// System.UInt32 System.Xml.Schema.XdrBuilder/AttributeContent::_MinLength
	uint32_t ____MinLength_7;
	// System.Boolean System.Xml.Schema.XdrBuilder/AttributeContent::_EnumerationRequired
	bool ____EnumerationRequired_8;
	// System.Boolean System.Xml.Schema.XdrBuilder/AttributeContent::_HasDataType
	bool ____HasDataType_9;
	// System.Boolean System.Xml.Schema.XdrBuilder/AttributeContent::_Global
	bool ____Global_10;
	// System.Object System.Xml.Schema.XdrBuilder/AttributeContent::_Default
	RuntimeObject * ____Default_11;

public:
	inline static int32_t get_offset_of__AttDef_0() { return static_cast<int32_t>(offsetof(AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290, ____AttDef_0)); }
	inline SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 * get__AttDef_0() const { return ____AttDef_0; }
	inline SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 ** get_address_of__AttDef_0() { return &____AttDef_0; }
	inline void set__AttDef_0(SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 * value)
	{
		____AttDef_0 = value;
		Il2CppCodeGenWriteBarrier((&____AttDef_0), value);
	}

	inline static int32_t get_offset_of__Name_1() { return static_cast<int32_t>(offsetof(AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290, ____Name_1)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get__Name_1() const { return ____Name_1; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of__Name_1() { return &____Name_1; }
	inline void set__Name_1(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		____Name_1 = value;
		Il2CppCodeGenWriteBarrier((&____Name_1), value);
	}

	inline static int32_t get_offset_of__Prefix_2() { return static_cast<int32_t>(offsetof(AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290, ____Prefix_2)); }
	inline String_t* get__Prefix_2() const { return ____Prefix_2; }
	inline String_t** get_address_of__Prefix_2() { return &____Prefix_2; }
	inline void set__Prefix_2(String_t* value)
	{
		____Prefix_2 = value;
		Il2CppCodeGenWriteBarrier((&____Prefix_2), value);
	}

	inline static int32_t get_offset_of__Required_3() { return static_cast<int32_t>(offsetof(AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290, ____Required_3)); }
	inline bool get__Required_3() const { return ____Required_3; }
	inline bool* get_address_of__Required_3() { return &____Required_3; }
	inline void set__Required_3(bool value)
	{
		____Required_3 = value;
	}

	inline static int32_t get_offset_of__MinVal_4() { return static_cast<int32_t>(offsetof(AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290, ____MinVal_4)); }
	inline uint32_t get__MinVal_4() const { return ____MinVal_4; }
	inline uint32_t* get_address_of__MinVal_4() { return &____MinVal_4; }
	inline void set__MinVal_4(uint32_t value)
	{
		____MinVal_4 = value;
	}

	inline static int32_t get_offset_of__MaxVal_5() { return static_cast<int32_t>(offsetof(AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290, ____MaxVal_5)); }
	inline uint32_t get__MaxVal_5() const { return ____MaxVal_5; }
	inline uint32_t* get_address_of__MaxVal_5() { return &____MaxVal_5; }
	inline void set__MaxVal_5(uint32_t value)
	{
		____MaxVal_5 = value;
	}

	inline static int32_t get_offset_of__MaxLength_6() { return static_cast<int32_t>(offsetof(AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290, ____MaxLength_6)); }
	inline uint32_t get__MaxLength_6() const { return ____MaxLength_6; }
	inline uint32_t* get_address_of__MaxLength_6() { return &____MaxLength_6; }
	inline void set__MaxLength_6(uint32_t value)
	{
		____MaxLength_6 = value;
	}

	inline static int32_t get_offset_of__MinLength_7() { return static_cast<int32_t>(offsetof(AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290, ____MinLength_7)); }
	inline uint32_t get__MinLength_7() const { return ____MinLength_7; }
	inline uint32_t* get_address_of__MinLength_7() { return &____MinLength_7; }
	inline void set__MinLength_7(uint32_t value)
	{
		____MinLength_7 = value;
	}

	inline static int32_t get_offset_of__EnumerationRequired_8() { return static_cast<int32_t>(offsetof(AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290, ____EnumerationRequired_8)); }
	inline bool get__EnumerationRequired_8() const { return ____EnumerationRequired_8; }
	inline bool* get_address_of__EnumerationRequired_8() { return &____EnumerationRequired_8; }
	inline void set__EnumerationRequired_8(bool value)
	{
		____EnumerationRequired_8 = value;
	}

	inline static int32_t get_offset_of__HasDataType_9() { return static_cast<int32_t>(offsetof(AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290, ____HasDataType_9)); }
	inline bool get__HasDataType_9() const { return ____HasDataType_9; }
	inline bool* get_address_of__HasDataType_9() { return &____HasDataType_9; }
	inline void set__HasDataType_9(bool value)
	{
		____HasDataType_9 = value;
	}

	inline static int32_t get_offset_of__Global_10() { return static_cast<int32_t>(offsetof(AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290, ____Global_10)); }
	inline bool get__Global_10() const { return ____Global_10; }
	inline bool* get_address_of__Global_10() { return &____Global_10; }
	inline void set__Global_10(bool value)
	{
		____Global_10 = value;
	}

	inline static int32_t get_offset_of__Default_11() { return static_cast<int32_t>(offsetof(AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290, ____Default_11)); }
	inline RuntimeObject * get__Default_11() const { return ____Default_11; }
	inline RuntimeObject ** get_address_of__Default_11() { return &____Default_11; }
	inline void set__Default_11(RuntimeObject * value)
	{
		____Default_11 = value;
		Il2CppCodeGenWriteBarrier((&____Default_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTECONTENT_T3C46FE57445E1995CC5754D024077DFD26C0C290_H
#ifndef ELEMENTCONTENT_TB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E_H
#define ELEMENTCONTENT_TB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/ElementContent
struct  ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E  : public RuntimeObject
{
public:
	// System.Xml.Schema.SchemaElementDecl System.Xml.Schema.XdrBuilder/ElementContent::_ElementDecl
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * ____ElementDecl_0;
	// System.Int32 System.Xml.Schema.XdrBuilder/ElementContent::_ContentAttr
	int32_t ____ContentAttr_1;
	// System.Int32 System.Xml.Schema.XdrBuilder/ElementContent::_OrderAttr
	int32_t ____OrderAttr_2;
	// System.Boolean System.Xml.Schema.XdrBuilder/ElementContent::_MasterGroupRequired
	bool ____MasterGroupRequired_3;
	// System.Boolean System.Xml.Schema.XdrBuilder/ElementContent::_ExistTerminal
	bool ____ExistTerminal_4;
	// System.Boolean System.Xml.Schema.XdrBuilder/ElementContent::_AllowDataType
	bool ____AllowDataType_5;
	// System.Boolean System.Xml.Schema.XdrBuilder/ElementContent::_HasDataType
	bool ____HasDataType_6;
	// System.Boolean System.Xml.Schema.XdrBuilder/ElementContent::_HasType
	bool ____HasType_7;
	// System.Boolean System.Xml.Schema.XdrBuilder/ElementContent::_EnumerationRequired
	bool ____EnumerationRequired_8;
	// System.UInt32 System.Xml.Schema.XdrBuilder/ElementContent::_MinVal
	uint32_t ____MinVal_9;
	// System.UInt32 System.Xml.Schema.XdrBuilder/ElementContent::_MaxVal
	uint32_t ____MaxVal_10;
	// System.UInt32 System.Xml.Schema.XdrBuilder/ElementContent::_MaxLength
	uint32_t ____MaxLength_11;
	// System.UInt32 System.Xml.Schema.XdrBuilder/ElementContent::_MinLength
	uint32_t ____MinLength_12;
	// System.Collections.Hashtable System.Xml.Schema.XdrBuilder/ElementContent::_AttDefList
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____AttDefList_13;

public:
	inline static int32_t get_offset_of__ElementDecl_0() { return static_cast<int32_t>(offsetof(ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E, ____ElementDecl_0)); }
	inline SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * get__ElementDecl_0() const { return ____ElementDecl_0; }
	inline SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 ** get_address_of__ElementDecl_0() { return &____ElementDecl_0; }
	inline void set__ElementDecl_0(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * value)
	{
		____ElementDecl_0 = value;
		Il2CppCodeGenWriteBarrier((&____ElementDecl_0), value);
	}

	inline static int32_t get_offset_of__ContentAttr_1() { return static_cast<int32_t>(offsetof(ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E, ____ContentAttr_1)); }
	inline int32_t get__ContentAttr_1() const { return ____ContentAttr_1; }
	inline int32_t* get_address_of__ContentAttr_1() { return &____ContentAttr_1; }
	inline void set__ContentAttr_1(int32_t value)
	{
		____ContentAttr_1 = value;
	}

	inline static int32_t get_offset_of__OrderAttr_2() { return static_cast<int32_t>(offsetof(ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E, ____OrderAttr_2)); }
	inline int32_t get__OrderAttr_2() const { return ____OrderAttr_2; }
	inline int32_t* get_address_of__OrderAttr_2() { return &____OrderAttr_2; }
	inline void set__OrderAttr_2(int32_t value)
	{
		____OrderAttr_2 = value;
	}

	inline static int32_t get_offset_of__MasterGroupRequired_3() { return static_cast<int32_t>(offsetof(ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E, ____MasterGroupRequired_3)); }
	inline bool get__MasterGroupRequired_3() const { return ____MasterGroupRequired_3; }
	inline bool* get_address_of__MasterGroupRequired_3() { return &____MasterGroupRequired_3; }
	inline void set__MasterGroupRequired_3(bool value)
	{
		____MasterGroupRequired_3 = value;
	}

	inline static int32_t get_offset_of__ExistTerminal_4() { return static_cast<int32_t>(offsetof(ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E, ____ExistTerminal_4)); }
	inline bool get__ExistTerminal_4() const { return ____ExistTerminal_4; }
	inline bool* get_address_of__ExistTerminal_4() { return &____ExistTerminal_4; }
	inline void set__ExistTerminal_4(bool value)
	{
		____ExistTerminal_4 = value;
	}

	inline static int32_t get_offset_of__AllowDataType_5() { return static_cast<int32_t>(offsetof(ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E, ____AllowDataType_5)); }
	inline bool get__AllowDataType_5() const { return ____AllowDataType_5; }
	inline bool* get_address_of__AllowDataType_5() { return &____AllowDataType_5; }
	inline void set__AllowDataType_5(bool value)
	{
		____AllowDataType_5 = value;
	}

	inline static int32_t get_offset_of__HasDataType_6() { return static_cast<int32_t>(offsetof(ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E, ____HasDataType_6)); }
	inline bool get__HasDataType_6() const { return ____HasDataType_6; }
	inline bool* get_address_of__HasDataType_6() { return &____HasDataType_6; }
	inline void set__HasDataType_6(bool value)
	{
		____HasDataType_6 = value;
	}

	inline static int32_t get_offset_of__HasType_7() { return static_cast<int32_t>(offsetof(ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E, ____HasType_7)); }
	inline bool get__HasType_7() const { return ____HasType_7; }
	inline bool* get_address_of__HasType_7() { return &____HasType_7; }
	inline void set__HasType_7(bool value)
	{
		____HasType_7 = value;
	}

	inline static int32_t get_offset_of__EnumerationRequired_8() { return static_cast<int32_t>(offsetof(ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E, ____EnumerationRequired_8)); }
	inline bool get__EnumerationRequired_8() const { return ____EnumerationRequired_8; }
	inline bool* get_address_of__EnumerationRequired_8() { return &____EnumerationRequired_8; }
	inline void set__EnumerationRequired_8(bool value)
	{
		____EnumerationRequired_8 = value;
	}

	inline static int32_t get_offset_of__MinVal_9() { return static_cast<int32_t>(offsetof(ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E, ____MinVal_9)); }
	inline uint32_t get__MinVal_9() const { return ____MinVal_9; }
	inline uint32_t* get_address_of__MinVal_9() { return &____MinVal_9; }
	inline void set__MinVal_9(uint32_t value)
	{
		____MinVal_9 = value;
	}

	inline static int32_t get_offset_of__MaxVal_10() { return static_cast<int32_t>(offsetof(ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E, ____MaxVal_10)); }
	inline uint32_t get__MaxVal_10() const { return ____MaxVal_10; }
	inline uint32_t* get_address_of__MaxVal_10() { return &____MaxVal_10; }
	inline void set__MaxVal_10(uint32_t value)
	{
		____MaxVal_10 = value;
	}

	inline static int32_t get_offset_of__MaxLength_11() { return static_cast<int32_t>(offsetof(ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E, ____MaxLength_11)); }
	inline uint32_t get__MaxLength_11() const { return ____MaxLength_11; }
	inline uint32_t* get_address_of__MaxLength_11() { return &____MaxLength_11; }
	inline void set__MaxLength_11(uint32_t value)
	{
		____MaxLength_11 = value;
	}

	inline static int32_t get_offset_of__MinLength_12() { return static_cast<int32_t>(offsetof(ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E, ____MinLength_12)); }
	inline uint32_t get__MinLength_12() const { return ____MinLength_12; }
	inline uint32_t* get_address_of__MinLength_12() { return &____MinLength_12; }
	inline void set__MinLength_12(uint32_t value)
	{
		____MinLength_12 = value;
	}

	inline static int32_t get_offset_of__AttDefList_13() { return static_cast<int32_t>(offsetof(ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E, ____AttDefList_13)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__AttDefList_13() const { return ____AttDefList_13; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__AttDefList_13() { return &____AttDefList_13; }
	inline void set__AttDefList_13(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____AttDefList_13 = value;
		Il2CppCodeGenWriteBarrier((&____AttDefList_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTCONTENT_TB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E_H
#ifndef NAMESPACEPREFIXFORQNAME_T80F0083B9C2151D487D41CCDD89B668C21902D8F_H
#define NAMESPACEPREFIXFORQNAME_T80F0083B9C2151D487D41CCDD89B668C21902D8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlAtomicValue/NamespacePrefixForQName
struct  NamespacePrefixForQName_t80F0083B9C2151D487D41CCDD89B668C21902D8F  : public RuntimeObject
{
public:
	// System.String System.Xml.Schema.XmlAtomicValue/NamespacePrefixForQName::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.Schema.XmlAtomicValue/NamespacePrefixForQName::ns
	String_t* ___ns_1;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(NamespacePrefixForQName_t80F0083B9C2151D487D41CCDD89B668C21902D8F, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(NamespacePrefixForQName_t80F0083B9C2151D487D41CCDD89B668C21902D8F, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier((&___ns_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEPREFIXFORQNAME_T80F0083B9C2151D487D41CCDD89B668C21902D8F_H
#ifndef XMLSCHEMACOLLECTION_TE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5_H
#define XMLSCHEMACOLLECTION_TE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaCollection
struct  XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Xml.Schema.XmlSchemaCollection::collection
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___collection_0;
	// System.Xml.XmlNameTable System.Xml.Schema.XmlSchemaCollection::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_1;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.XmlSchemaCollection::schemaNames
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * ___schemaNames_2;
	// System.Threading.ReaderWriterLock System.Xml.Schema.XmlSchemaCollection::wLock
	ReaderWriterLock_t7EEE8E1E96D63FD5668920B8274B13D184983349 * ___wLock_3;
	// System.Int32 System.Xml.Schema.XmlSchemaCollection::timeout
	int32_t ___timeout_4;
	// System.Boolean System.Xml.Schema.XmlSchemaCollection::isThreadSafe
	bool ___isThreadSafe_5;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XmlSchemaCollection::validationEventHandler
	ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * ___validationEventHandler_6;
	// System.Xml.XmlResolver System.Xml.Schema.XmlSchemaCollection::xmlResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___xmlResolver_7;

public:
	inline static int32_t get_offset_of_collection_0() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5, ___collection_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_collection_0() const { return ___collection_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_collection_0() { return &___collection_0; }
	inline void set_collection_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___collection_0 = value;
		Il2CppCodeGenWriteBarrier((&___collection_0), value);
	}

	inline static int32_t get_offset_of_nameTable_1() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5, ___nameTable_1)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_1() const { return ___nameTable_1; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_1() { return &___nameTable_1; }
	inline void set_nameTable_1(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_1), value);
	}

	inline static int32_t get_offset_of_schemaNames_2() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5, ___schemaNames_2)); }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * get_schemaNames_2() const { return ___schemaNames_2; }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 ** get_address_of_schemaNames_2() { return &___schemaNames_2; }
	inline void set_schemaNames_2(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * value)
	{
		___schemaNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_2), value);
	}

	inline static int32_t get_offset_of_wLock_3() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5, ___wLock_3)); }
	inline ReaderWriterLock_t7EEE8E1E96D63FD5668920B8274B13D184983349 * get_wLock_3() const { return ___wLock_3; }
	inline ReaderWriterLock_t7EEE8E1E96D63FD5668920B8274B13D184983349 ** get_address_of_wLock_3() { return &___wLock_3; }
	inline void set_wLock_3(ReaderWriterLock_t7EEE8E1E96D63FD5668920B8274B13D184983349 * value)
	{
		___wLock_3 = value;
		Il2CppCodeGenWriteBarrier((&___wLock_3), value);
	}

	inline static int32_t get_offset_of_timeout_4() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5, ___timeout_4)); }
	inline int32_t get_timeout_4() const { return ___timeout_4; }
	inline int32_t* get_address_of_timeout_4() { return &___timeout_4; }
	inline void set_timeout_4(int32_t value)
	{
		___timeout_4 = value;
	}

	inline static int32_t get_offset_of_isThreadSafe_5() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5, ___isThreadSafe_5)); }
	inline bool get_isThreadSafe_5() const { return ___isThreadSafe_5; }
	inline bool* get_address_of_isThreadSafe_5() { return &___isThreadSafe_5; }
	inline void set_isThreadSafe_5(bool value)
	{
		___isThreadSafe_5 = value;
	}

	inline static int32_t get_offset_of_validationEventHandler_6() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5, ___validationEventHandler_6)); }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * get_validationEventHandler_6() const { return ___validationEventHandler_6; }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF ** get_address_of_validationEventHandler_6() { return &___validationEventHandler_6; }
	inline void set_validationEventHandler_6(ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * value)
	{
		___validationEventHandler_6 = value;
		Il2CppCodeGenWriteBarrier((&___validationEventHandler_6), value);
	}

	inline static int32_t get_offset_of_xmlResolver_7() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5, ___xmlResolver_7)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_xmlResolver_7() const { return ___xmlResolver_7; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_xmlResolver_7() { return &___xmlResolver_7; }
	inline void set_xmlResolver_7(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___xmlResolver_7 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOLLECTION_TE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5_H
#ifndef XMLSCHEMACOLLECTIONENUMERATOR_TB04D6CC4C7F2F39E23A2EACF9BF4926977CA0114_H
#define XMLSCHEMACOLLECTIONENUMERATOR_TB04D6CC4C7F2F39E23A2EACF9BF4926977CA0114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaCollectionEnumerator
struct  XmlSchemaCollectionEnumerator_tB04D6CC4C7F2F39E23A2EACF9BF4926977CA0114  : public RuntimeObject
{
public:
	// System.Collections.IDictionaryEnumerator System.Xml.Schema.XmlSchemaCollectionEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(XmlSchemaCollectionEnumerator_tB04D6CC4C7F2F39E23A2EACF9BF4926977CA0114, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOLLECTIONENUMERATOR_TB04D6CC4C7F2F39E23A2EACF9BF4926977CA0114_H
#ifndef XMLSCHEMACOLLECTIONNODE_TDAA903FE8874B90782D728CBB00BAD49A9823558_H
#define XMLSCHEMACOLLECTIONNODE_TDAA903FE8874B90782D728CBB00BAD49A9823558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaCollectionNode
struct  XmlSchemaCollectionNode_tDAA903FE8874B90782D728CBB00BAD49A9823558  : public RuntimeObject
{
public:
	// System.String System.Xml.Schema.XmlSchemaCollectionNode::namespaceUri
	String_t* ___namespaceUri_0;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.XmlSchemaCollectionNode::schemaInfo
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * ___schemaInfo_1;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaCollectionNode::schema
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___schema_2;

public:
	inline static int32_t get_offset_of_namespaceUri_0() { return static_cast<int32_t>(offsetof(XmlSchemaCollectionNode_tDAA903FE8874B90782D728CBB00BAD49A9823558, ___namespaceUri_0)); }
	inline String_t* get_namespaceUri_0() const { return ___namespaceUri_0; }
	inline String_t** get_address_of_namespaceUri_0() { return &___namespaceUri_0; }
	inline void set_namespaceUri_0(String_t* value)
	{
		___namespaceUri_0 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_0), value);
	}

	inline static int32_t get_offset_of_schemaInfo_1() { return static_cast<int32_t>(offsetof(XmlSchemaCollectionNode_tDAA903FE8874B90782D728CBB00BAD49A9823558, ___schemaInfo_1)); }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * get_schemaInfo_1() const { return ___schemaInfo_1; }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 ** get_address_of_schemaInfo_1() { return &___schemaInfo_1; }
	inline void set_schemaInfo_1(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * value)
	{
		___schemaInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_1), value);
	}

	inline static int32_t get_offset_of_schema_2() { return static_cast<int32_t>(offsetof(XmlSchemaCollectionNode_tDAA903FE8874B90782D728CBB00BAD49A9823558, ___schema_2)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_schema_2() const { return ___schema_2; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_schema_2() { return &___schema_2; }
	inline void set_schema_2(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___schema_2 = value;
		Il2CppCodeGenWriteBarrier((&___schema_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOLLECTIONNODE_TDAA903FE8874B90782D728CBB00BAD49A9823558_H
#ifndef XMLSCHEMACOMPILATIONSETTINGS_T33655A7BA800689EC37601FEFD33291F42B8ABBC_H
#define XMLSCHEMACOMPILATIONSETTINGS_T33655A7BA800689EC37601FEFD33291F42B8ABBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaCompilationSettings
struct  XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaCompilationSettings::enableUpaCheck
	bool ___enableUpaCheck_0;

public:
	inline static int32_t get_offset_of_enableUpaCheck_0() { return static_cast<int32_t>(offsetof(XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC, ___enableUpaCheck_0)); }
	inline bool get_enableUpaCheck_0() const { return ___enableUpaCheck_0; }
	inline bool* get_address_of_enableUpaCheck_0() { return &___enableUpaCheck_0; }
	inline void set_enableUpaCheck_0(bool value)
	{
		___enableUpaCheck_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPILATIONSETTINGS_T33655A7BA800689EC37601FEFD33291F42B8ABBC_H
#ifndef XMLSCHEMADATATYPE_T6D9535C4B3780086DF21646303E2350D40A5A550_H
#define XMLSCHEMADATATYPE_T6D9535C4B3780086DF21646303E2350D40A5A550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDatatype
struct  XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADATATYPE_T6D9535C4B3780086DF21646303E2350D40A5A550_H
#ifndef XMLSCHEMAOBJECT_TB5695348FF2B08149CAE95CD10F39F21EDB1F57B_H
#define XMLSCHEMAOBJECT_TB5695348FF2B08149CAE95CD10F39F21EDB1F57B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObject
struct  XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaObject::lineNum
	int32_t ___lineNum_0;
	// System.Int32 System.Xml.Schema.XmlSchemaObject::linePos
	int32_t ___linePos_1;
	// System.String System.Xml.Schema.XmlSchemaObject::sourceUri
	String_t* ___sourceUri_2;
	// System.Xml.Serialization.XmlSerializerNamespaces System.Xml.Schema.XmlSchemaObject::namespaces
	XmlSerializerNamespaces_t31E17AE7CF53591901A576C48D1E446BB985676F * ___namespaces_3;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObject::parent
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * ___parent_4;
	// System.Boolean System.Xml.Schema.XmlSchemaObject::isProcessing
	bool ___isProcessing_5;

public:
	inline static int32_t get_offset_of_lineNum_0() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B, ___lineNum_0)); }
	inline int32_t get_lineNum_0() const { return ___lineNum_0; }
	inline int32_t* get_address_of_lineNum_0() { return &___lineNum_0; }
	inline void set_lineNum_0(int32_t value)
	{
		___lineNum_0 = value;
	}

	inline static int32_t get_offset_of_linePos_1() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B, ___linePos_1)); }
	inline int32_t get_linePos_1() const { return ___linePos_1; }
	inline int32_t* get_address_of_linePos_1() { return &___linePos_1; }
	inline void set_linePos_1(int32_t value)
	{
		___linePos_1 = value;
	}

	inline static int32_t get_offset_of_sourceUri_2() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B, ___sourceUri_2)); }
	inline String_t* get_sourceUri_2() const { return ___sourceUri_2; }
	inline String_t** get_address_of_sourceUri_2() { return &___sourceUri_2; }
	inline void set_sourceUri_2(String_t* value)
	{
		___sourceUri_2 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_2), value);
	}

	inline static int32_t get_offset_of_namespaces_3() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B, ___namespaces_3)); }
	inline XmlSerializerNamespaces_t31E17AE7CF53591901A576C48D1E446BB985676F * get_namespaces_3() const { return ___namespaces_3; }
	inline XmlSerializerNamespaces_t31E17AE7CF53591901A576C48D1E446BB985676F ** get_address_of_namespaces_3() { return &___namespaces_3; }
	inline void set_namespaces_3(XmlSerializerNamespaces_t31E17AE7CF53591901A576C48D1E446BB985676F * value)
	{
		___namespaces_3 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_3), value);
	}

	inline static int32_t get_offset_of_parent_4() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B, ___parent_4)); }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * get_parent_4() const { return ___parent_4; }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B ** get_address_of_parent_4() { return &___parent_4; }
	inline void set_parent_4(XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * value)
	{
		___parent_4 = value;
		Il2CppCodeGenWriteBarrier((&___parent_4), value);
	}

	inline static int32_t get_offset_of_isProcessing_5() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B, ___isProcessing_5)); }
	inline bool get_isProcessing_5() const { return ___isProcessing_5; }
	inline bool* get_address_of_isProcessing_5() { return &___isProcessing_5; }
	inline void set_isProcessing_5(bool value)
	{
		___isProcessing_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECT_TB5695348FF2B08149CAE95CD10F39F21EDB1F57B_H
#ifndef XMLSCHEMAOBJECTENUMERATOR_TE349F7DC1E12665FA0F97D180BF02FA949D909C7_H
#define XMLSCHEMAOBJECTENUMERATOR_TE349F7DC1E12665FA0F97D180BF02FA949D909C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectEnumerator
struct  XmlSchemaObjectEnumerator_tE349F7DC1E12665FA0F97D180BF02FA949D909C7  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Xml.Schema.XmlSchemaObjectEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(XmlSchemaObjectEnumerator_tE349F7DC1E12665FA0F97D180BF02FA949D909C7, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECTENUMERATOR_TE349F7DC1E12665FA0F97D180BF02FA949D909C7_H
#ifndef XMLSCHEMAOBJECTTABLE_T8052FBDE5AB8FDD05FA48092BB0D511C496D3833_H
#define XMLSCHEMAOBJECTTABLE_T8052FBDE5AB8FDD05FA48092BB0D511C496D3833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectTable
struct  XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.XmlSchemaObject> System.Xml.Schema.XmlSchemaObjectTable::table
	Dictionary_2_tBCC3C44F7B144318C5907155287F49A0A66F9BBA * ___table_0;
	// System.Collections.Generic.List`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry> System.Xml.Schema.XmlSchemaObjectTable::entries
	List_1_t7C6AA94EA9520975CB29B2B4CA6C9EAED364ECD3 * ___entries_1;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833, ___table_0)); }
	inline Dictionary_2_tBCC3C44F7B144318C5907155287F49A0A66F9BBA * get_table_0() const { return ___table_0; }
	inline Dictionary_2_tBCC3C44F7B144318C5907155287F49A0A66F9BBA ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(Dictionary_2_tBCC3C44F7B144318C5907155287F49A0A66F9BBA * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833, ___entries_1)); }
	inline List_1_t7C6AA94EA9520975CB29B2B4CA6C9EAED364ECD3 * get_entries_1() const { return ___entries_1; }
	inline List_1_t7C6AA94EA9520975CB29B2B4CA6C9EAED364ECD3 ** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(List_1_t7C6AA94EA9520975CB29B2B4CA6C9EAED364ECD3 * value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECTTABLE_T8052FBDE5AB8FDD05FA48092BB0D511C496D3833_H
#ifndef VALUESCOLLECTION_TE5CD9602867B56FB89416007CBBFF4C6950771C5_H
#define VALUESCOLLECTION_TE5CD9602867B56FB89416007CBBFF4C6950771C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectTable/ValuesCollection
struct  ValuesCollection_tE5CD9602867B56FB89416007CBBFF4C6950771C5  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry> System.Xml.Schema.XmlSchemaObjectTable/ValuesCollection::entries
	List_1_t7C6AA94EA9520975CB29B2B4CA6C9EAED364ECD3 * ___entries_0;
	// System.Int32 System.Xml.Schema.XmlSchemaObjectTable/ValuesCollection::size
	int32_t ___size_1;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(ValuesCollection_tE5CD9602867B56FB89416007CBBFF4C6950771C5, ___entries_0)); }
	inline List_1_t7C6AA94EA9520975CB29B2B4CA6C9EAED364ECD3 * get_entries_0() const { return ___entries_0; }
	inline List_1_t7C6AA94EA9520975CB29B2B4CA6C9EAED364ECD3 ** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(List_1_t7C6AA94EA9520975CB29B2B4CA6C9EAED364ECD3 * value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier((&___entries_0), value);
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(ValuesCollection_tE5CD9602867B56FB89416007CBBFF4C6950771C5, ___size_1)); }
	inline int32_t get_size_1() const { return ___size_1; }
	inline int32_t* get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(int32_t value)
	{
		___size_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUESCOLLECTION_TE5CD9602867B56FB89416007CBBFF4C6950771C5_H
#ifndef XMLSCHEMASET_TD92B4BF5F65FBF5B106399A36284FDC64E602F7F_H
#define XMLSCHEMASET_TD92B4BF5F65FBF5B106399A36284FDC64E602F7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSet
struct  XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.Schema.XmlSchemaSet::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_0;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.XmlSchemaSet::schemaNames
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * ___schemaNames_1;
	// System.Collections.SortedList System.Xml.Schema.XmlSchemaSet::schemas
	SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E * ___schemas_2;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XmlSchemaSet::internalEventHandler
	ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * ___internalEventHandler_3;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XmlSchemaSet::eventHandler
	ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * ___eventHandler_4;
	// System.Boolean System.Xml.Schema.XmlSchemaSet::isCompiled
	bool ___isCompiled_5;
	// System.Collections.Hashtable System.Xml.Schema.XmlSchemaSet::schemaLocations
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___schemaLocations_6;
	// System.Collections.Hashtable System.Xml.Schema.XmlSchemaSet::chameleonSchemas
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___chameleonSchemas_7;
	// System.Collections.Hashtable System.Xml.Schema.XmlSchemaSet::targetNamespaces
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___targetNamespaces_8;
	// System.Boolean System.Xml.Schema.XmlSchemaSet::compileAll
	bool ___compileAll_9;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.XmlSchemaSet::cachedCompiledInfo
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * ___cachedCompiledInfo_10;
	// System.Xml.XmlReaderSettings System.Xml.Schema.XmlSchemaSet::readerSettings
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65 * ___readerSettings_11;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaSet::schemaForSchema
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___schemaForSchema_12;
	// System.Xml.Schema.XmlSchemaCompilationSettings System.Xml.Schema.XmlSchemaSet::compilationSettings
	XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC * ___compilationSettings_13;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaSet::elements
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___elements_14;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaSet::attributes
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___attributes_15;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaSet::schemaTypes
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___schemaTypes_16;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaSet::substitutionGroups
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___substitutionGroups_17;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaSet::typeExtensions
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___typeExtensions_18;
	// System.Object System.Xml.Schema.XmlSchemaSet::internalSyncObject
	RuntimeObject * ___internalSyncObject_19;

public:
	inline static int32_t get_offset_of_nameTable_0() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___nameTable_0)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_0() const { return ___nameTable_0; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_0() { return &___nameTable_0; }
	inline void set_nameTable_0(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_0), value);
	}

	inline static int32_t get_offset_of_schemaNames_1() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___schemaNames_1)); }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * get_schemaNames_1() const { return ___schemaNames_1; }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 ** get_address_of_schemaNames_1() { return &___schemaNames_1; }
	inline void set_schemaNames_1(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * value)
	{
		___schemaNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_1), value);
	}

	inline static int32_t get_offset_of_schemas_2() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___schemas_2)); }
	inline SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E * get_schemas_2() const { return ___schemas_2; }
	inline SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E ** get_address_of_schemas_2() { return &___schemas_2; }
	inline void set_schemas_2(SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E * value)
	{
		___schemas_2 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_2), value);
	}

	inline static int32_t get_offset_of_internalEventHandler_3() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___internalEventHandler_3)); }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * get_internalEventHandler_3() const { return ___internalEventHandler_3; }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF ** get_address_of_internalEventHandler_3() { return &___internalEventHandler_3; }
	inline void set_internalEventHandler_3(ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * value)
	{
		___internalEventHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___internalEventHandler_3), value);
	}

	inline static int32_t get_offset_of_eventHandler_4() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___eventHandler_4)); }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * get_eventHandler_4() const { return ___eventHandler_4; }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF ** get_address_of_eventHandler_4() { return &___eventHandler_4; }
	inline void set_eventHandler_4(ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * value)
	{
		___eventHandler_4 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandler_4), value);
	}

	inline static int32_t get_offset_of_isCompiled_5() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___isCompiled_5)); }
	inline bool get_isCompiled_5() const { return ___isCompiled_5; }
	inline bool* get_address_of_isCompiled_5() { return &___isCompiled_5; }
	inline void set_isCompiled_5(bool value)
	{
		___isCompiled_5 = value;
	}

	inline static int32_t get_offset_of_schemaLocations_6() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___schemaLocations_6)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_schemaLocations_6() const { return ___schemaLocations_6; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_schemaLocations_6() { return &___schemaLocations_6; }
	inline void set_schemaLocations_6(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___schemaLocations_6 = value;
		Il2CppCodeGenWriteBarrier((&___schemaLocations_6), value);
	}

	inline static int32_t get_offset_of_chameleonSchemas_7() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___chameleonSchemas_7)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_chameleonSchemas_7() const { return ___chameleonSchemas_7; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_chameleonSchemas_7() { return &___chameleonSchemas_7; }
	inline void set_chameleonSchemas_7(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___chameleonSchemas_7 = value;
		Il2CppCodeGenWriteBarrier((&___chameleonSchemas_7), value);
	}

	inline static int32_t get_offset_of_targetNamespaces_8() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___targetNamespaces_8)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_targetNamespaces_8() const { return ___targetNamespaces_8; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_targetNamespaces_8() { return &___targetNamespaces_8; }
	inline void set_targetNamespaces_8(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___targetNamespaces_8 = value;
		Il2CppCodeGenWriteBarrier((&___targetNamespaces_8), value);
	}

	inline static int32_t get_offset_of_compileAll_9() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___compileAll_9)); }
	inline bool get_compileAll_9() const { return ___compileAll_9; }
	inline bool* get_address_of_compileAll_9() { return &___compileAll_9; }
	inline void set_compileAll_9(bool value)
	{
		___compileAll_9 = value;
	}

	inline static int32_t get_offset_of_cachedCompiledInfo_10() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___cachedCompiledInfo_10)); }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * get_cachedCompiledInfo_10() const { return ___cachedCompiledInfo_10; }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 ** get_address_of_cachedCompiledInfo_10() { return &___cachedCompiledInfo_10; }
	inline void set_cachedCompiledInfo_10(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * value)
	{
		___cachedCompiledInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCompiledInfo_10), value);
	}

	inline static int32_t get_offset_of_readerSettings_11() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___readerSettings_11)); }
	inline XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65 * get_readerSettings_11() const { return ___readerSettings_11; }
	inline XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65 ** get_address_of_readerSettings_11() { return &___readerSettings_11; }
	inline void set_readerSettings_11(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65 * value)
	{
		___readerSettings_11 = value;
		Il2CppCodeGenWriteBarrier((&___readerSettings_11), value);
	}

	inline static int32_t get_offset_of_schemaForSchema_12() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___schemaForSchema_12)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_schemaForSchema_12() const { return ___schemaForSchema_12; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_schemaForSchema_12() { return &___schemaForSchema_12; }
	inline void set_schemaForSchema_12(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___schemaForSchema_12 = value;
		Il2CppCodeGenWriteBarrier((&___schemaForSchema_12), value);
	}

	inline static int32_t get_offset_of_compilationSettings_13() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___compilationSettings_13)); }
	inline XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC * get_compilationSettings_13() const { return ___compilationSettings_13; }
	inline XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC ** get_address_of_compilationSettings_13() { return &___compilationSettings_13; }
	inline void set_compilationSettings_13(XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC * value)
	{
		___compilationSettings_13 = value;
		Il2CppCodeGenWriteBarrier((&___compilationSettings_13), value);
	}

	inline static int32_t get_offset_of_elements_14() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___elements_14)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_elements_14() const { return ___elements_14; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_elements_14() { return &___elements_14; }
	inline void set_elements_14(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___elements_14 = value;
		Il2CppCodeGenWriteBarrier((&___elements_14), value);
	}

	inline static int32_t get_offset_of_attributes_15() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___attributes_15)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_attributes_15() const { return ___attributes_15; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_attributes_15() { return &___attributes_15; }
	inline void set_attributes_15(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___attributes_15 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_15), value);
	}

	inline static int32_t get_offset_of_schemaTypes_16() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___schemaTypes_16)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_schemaTypes_16() const { return ___schemaTypes_16; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_schemaTypes_16() { return &___schemaTypes_16; }
	inline void set_schemaTypes_16(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___schemaTypes_16 = value;
		Il2CppCodeGenWriteBarrier((&___schemaTypes_16), value);
	}

	inline static int32_t get_offset_of_substitutionGroups_17() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___substitutionGroups_17)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_substitutionGroups_17() const { return ___substitutionGroups_17; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_substitutionGroups_17() { return &___substitutionGroups_17; }
	inline void set_substitutionGroups_17(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___substitutionGroups_17 = value;
		Il2CppCodeGenWriteBarrier((&___substitutionGroups_17), value);
	}

	inline static int32_t get_offset_of_typeExtensions_18() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___typeExtensions_18)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_typeExtensions_18() const { return ___typeExtensions_18; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_typeExtensions_18() { return &___typeExtensions_18; }
	inline void set_typeExtensions_18(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___typeExtensions_18 = value;
		Il2CppCodeGenWriteBarrier((&___typeExtensions_18), value);
	}

	inline static int32_t get_offset_of_internalSyncObject_19() { return static_cast<int32_t>(offsetof(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F, ___internalSyncObject_19)); }
	inline RuntimeObject * get_internalSyncObject_19() const { return ___internalSyncObject_19; }
	inline RuntimeObject ** get_address_of_internalSyncObject_19() { return &___internalSyncObject_19; }
	inline void set_internalSyncObject_19(RuntimeObject * value)
	{
		___internalSyncObject_19 = value;
		Il2CppCodeGenWriteBarrier((&___internalSyncObject_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASET_TD92B4BF5F65FBF5B106399A36284FDC64E602F7F_H
#ifndef XPATHITEM_TCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98_H
#define XPATHITEM_TCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathItem
struct  XPathItem_tCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHITEM_TCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#define DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier((&___Powers10_6), value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Zero_7)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___One_8)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_One_8() const { return ___One_8; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinusOne_9)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MaxValue_10)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinValue_11)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearPositiveZero_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef XDRVALIDATOR_T58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491_H
#define XDRVALIDATOR_T58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrValidator
struct  XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491  : public BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43
{
public:
	// System.Xml.HWStack System.Xml.Schema.XdrValidator::validationStack
	HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * ___validationStack_15;
	// System.Collections.Hashtable System.Xml.Schema.XdrValidator::attPresence
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___attPresence_16;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XdrValidator::name
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___name_17;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.XdrValidator::nsManager
	XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * ___nsManager_18;
	// System.Boolean System.Xml.Schema.XdrValidator::isProcessContents
	bool ___isProcessContents_19;
	// System.Collections.Hashtable System.Xml.Schema.XdrValidator::IDs
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___IDs_20;
	// System.Xml.Schema.IdRefNode System.Xml.Schema.XdrValidator::idRefListHead
	IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 * ___idRefListHead_21;
	// System.Xml.Schema.Parser System.Xml.Schema.XdrValidator::inlineSchemaParser
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920 * ___inlineSchemaParser_22;

public:
	inline static int32_t get_offset_of_validationStack_15() { return static_cast<int32_t>(offsetof(XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491, ___validationStack_15)); }
	inline HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * get_validationStack_15() const { return ___validationStack_15; }
	inline HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE ** get_address_of_validationStack_15() { return &___validationStack_15; }
	inline void set_validationStack_15(HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * value)
	{
		___validationStack_15 = value;
		Il2CppCodeGenWriteBarrier((&___validationStack_15), value);
	}

	inline static int32_t get_offset_of_attPresence_16() { return static_cast<int32_t>(offsetof(XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491, ___attPresence_16)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_attPresence_16() const { return ___attPresence_16; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_attPresence_16() { return &___attPresence_16; }
	inline void set_attPresence_16(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___attPresence_16 = value;
		Il2CppCodeGenWriteBarrier((&___attPresence_16), value);
	}

	inline static int32_t get_offset_of_name_17() { return static_cast<int32_t>(offsetof(XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491, ___name_17)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_name_17() const { return ___name_17; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_name_17() { return &___name_17; }
	inline void set_name_17(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___name_17 = value;
		Il2CppCodeGenWriteBarrier((&___name_17), value);
	}

	inline static int32_t get_offset_of_nsManager_18() { return static_cast<int32_t>(offsetof(XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491, ___nsManager_18)); }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * get_nsManager_18() const { return ___nsManager_18; }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F ** get_address_of_nsManager_18() { return &___nsManager_18; }
	inline void set_nsManager_18(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * value)
	{
		___nsManager_18 = value;
		Il2CppCodeGenWriteBarrier((&___nsManager_18), value);
	}

	inline static int32_t get_offset_of_isProcessContents_19() { return static_cast<int32_t>(offsetof(XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491, ___isProcessContents_19)); }
	inline bool get_isProcessContents_19() const { return ___isProcessContents_19; }
	inline bool* get_address_of_isProcessContents_19() { return &___isProcessContents_19; }
	inline void set_isProcessContents_19(bool value)
	{
		___isProcessContents_19 = value;
	}

	inline static int32_t get_offset_of_IDs_20() { return static_cast<int32_t>(offsetof(XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491, ___IDs_20)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_IDs_20() const { return ___IDs_20; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_IDs_20() { return &___IDs_20; }
	inline void set_IDs_20(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___IDs_20 = value;
		Il2CppCodeGenWriteBarrier((&___IDs_20), value);
	}

	inline static int32_t get_offset_of_idRefListHead_21() { return static_cast<int32_t>(offsetof(XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491, ___idRefListHead_21)); }
	inline IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 * get_idRefListHead_21() const { return ___idRefListHead_21; }
	inline IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 ** get_address_of_idRefListHead_21() { return &___idRefListHead_21; }
	inline void set_idRefListHead_21(IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 * value)
	{
		___idRefListHead_21 = value;
		Il2CppCodeGenWriteBarrier((&___idRefListHead_21), value);
	}

	inline static int32_t get_offset_of_inlineSchemaParser_22() { return static_cast<int32_t>(offsetof(XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491, ___inlineSchemaParser_22)); }
	inline Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920 * get_inlineSchemaParser_22() const { return ___inlineSchemaParser_22; }
	inline Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920 ** get_address_of_inlineSchemaParser_22() { return &___inlineSchemaParser_22; }
	inline void set_inlineSchemaParser_22(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920 * value)
	{
		___inlineSchemaParser_22 = value;
		Il2CppCodeGenWriteBarrier((&___inlineSchemaParser_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDRVALIDATOR_T58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491_H
#ifndef XMLSCHEMAANNOTATED_T94F7899A20B495CD62FA0976A4F23FD4A0847B7B_H
#define XMLSCHEMAANNOTATED_T94F7899A20B495CD62FA0976A4F23FD4A0847B7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAnnotated
struct  XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B  : public XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B
{
public:
	// System.String System.Xml.Schema.XmlSchemaAnnotated::id
	String_t* ___id_6;
	// System.Xml.Schema.XmlSchemaAnnotation System.Xml.Schema.XmlSchemaAnnotated::annotation
	XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 * ___annotation_7;
	// System.Xml.XmlAttribute[] System.Xml.Schema.XmlSchemaAnnotated::moreAttributes
	XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908* ___moreAttributes_8;

public:
	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B, ___id_6)); }
	inline String_t* get_id_6() const { return ___id_6; }
	inline String_t** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(String_t* value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier((&___id_6), value);
	}

	inline static int32_t get_offset_of_annotation_7() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B, ___annotation_7)); }
	inline XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 * get_annotation_7() const { return ___annotation_7; }
	inline XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 ** get_address_of_annotation_7() { return &___annotation_7; }
	inline void set_annotation_7(XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 * value)
	{
		___annotation_7 = value;
		Il2CppCodeGenWriteBarrier((&___annotation_7), value);
	}

	inline static int32_t get_offset_of_moreAttributes_8() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B, ___moreAttributes_8)); }
	inline XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908* get_moreAttributes_8() const { return ___moreAttributes_8; }
	inline XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908** get_address_of_moreAttributes_8() { return &___moreAttributes_8; }
	inline void set_moreAttributes_8(XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908* value)
	{
		___moreAttributes_8 = value;
		Il2CppCodeGenWriteBarrier((&___moreAttributes_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAANNOTATED_T94F7899A20B495CD62FA0976A4F23FD4A0847B7B_H
#ifndef XMLSCHEMAANNOTATION_TC8DD8E9A26CD3A12B16C25F0341600FF69C3B725_H
#define XMLSCHEMAANNOTATION_TC8DD8E9A26CD3A12B16C25F0341600FF69C3B725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAnnotation
struct  XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725  : public XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B
{
public:
	// System.String System.Xml.Schema.XmlSchemaAnnotation::id
	String_t* ___id_6;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaAnnotation::items
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___items_7;
	// System.Xml.XmlAttribute[] System.Xml.Schema.XmlSchemaAnnotation::moreAttributes
	XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908* ___moreAttributes_8;

public:
	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725, ___id_6)); }
	inline String_t* get_id_6() const { return ___id_6; }
	inline String_t** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(String_t* value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier((&___id_6), value);
	}

	inline static int32_t get_offset_of_items_7() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725, ___items_7)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_items_7() const { return ___items_7; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_items_7() { return &___items_7; }
	inline void set_items_7(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___items_7 = value;
		Il2CppCodeGenWriteBarrier((&___items_7), value);
	}

	inline static int32_t get_offset_of_moreAttributes_8() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725, ___moreAttributes_8)); }
	inline XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908* get_moreAttributes_8() const { return ___moreAttributes_8; }
	inline XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908** get_address_of_moreAttributes_8() { return &___moreAttributes_8; }
	inline void set_moreAttributes_8(XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908* value)
	{
		___moreAttributes_8 = value;
		Il2CppCodeGenWriteBarrier((&___moreAttributes_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAANNOTATION_TC8DD8E9A26CD3A12B16C25F0341600FF69C3B725_H
#ifndef XMLSCHEMAAPPINFO_T5E2FEE4B31F35CD076CC2F47DA5CB82DA139A860_H
#define XMLSCHEMAAPPINFO_T5E2FEE4B31F35CD076CC2F47DA5CB82DA139A860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAppInfo
struct  XmlSchemaAppInfo_t5E2FEE4B31F35CD076CC2F47DA5CB82DA139A860  : public XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B
{
public:
	// System.String System.Xml.Schema.XmlSchemaAppInfo::source
	String_t* ___source_6;
	// System.Xml.XmlNode[] System.Xml.Schema.XmlSchemaAppInfo::markup
	XmlNodeU5BU5D_tBF402FAC28AE903AC3FF845775405A2274B701AF* ___markup_7;

public:
	inline static int32_t get_offset_of_source_6() { return static_cast<int32_t>(offsetof(XmlSchemaAppInfo_t5E2FEE4B31F35CD076CC2F47DA5CB82DA139A860, ___source_6)); }
	inline String_t* get_source_6() const { return ___source_6; }
	inline String_t** get_address_of_source_6() { return &___source_6; }
	inline void set_source_6(String_t* value)
	{
		___source_6 = value;
		Il2CppCodeGenWriteBarrier((&___source_6), value);
	}

	inline static int32_t get_offset_of_markup_7() { return static_cast<int32_t>(offsetof(XmlSchemaAppInfo_t5E2FEE4B31F35CD076CC2F47DA5CB82DA139A860, ___markup_7)); }
	inline XmlNodeU5BU5D_tBF402FAC28AE903AC3FF845775405A2274B701AF* get_markup_7() const { return ___markup_7; }
	inline XmlNodeU5BU5D_tBF402FAC28AE903AC3FF845775405A2274B701AF** get_address_of_markup_7() { return &___markup_7; }
	inline void set_markup_7(XmlNodeU5BU5D_tBF402FAC28AE903AC3FF845775405A2274B701AF* value)
	{
		___markup_7 = value;
		Il2CppCodeGenWriteBarrier((&___markup_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAAPPINFO_T5E2FEE4B31F35CD076CC2F47DA5CB82DA139A860_H
#ifndef XMLSCHEMADOCUMENTATION_T95A86FC828CDFF9FCC86D8F7774C6496591E67EE_H
#define XMLSCHEMADOCUMENTATION_T95A86FC828CDFF9FCC86D8F7774C6496591E67EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDocumentation
struct  XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE  : public XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B
{
public:
	// System.String System.Xml.Schema.XmlSchemaDocumentation::source
	String_t* ___source_6;
	// System.String System.Xml.Schema.XmlSchemaDocumentation::language
	String_t* ___language_7;
	// System.Xml.XmlNode[] System.Xml.Schema.XmlSchemaDocumentation::markup
	XmlNodeU5BU5D_tBF402FAC28AE903AC3FF845775405A2274B701AF* ___markup_8;

public:
	inline static int32_t get_offset_of_source_6() { return static_cast<int32_t>(offsetof(XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE, ___source_6)); }
	inline String_t* get_source_6() const { return ___source_6; }
	inline String_t** get_address_of_source_6() { return &___source_6; }
	inline void set_source_6(String_t* value)
	{
		___source_6 = value;
		Il2CppCodeGenWriteBarrier((&___source_6), value);
	}

	inline static int32_t get_offset_of_language_7() { return static_cast<int32_t>(offsetof(XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE, ___language_7)); }
	inline String_t* get_language_7() const { return ___language_7; }
	inline String_t** get_address_of_language_7() { return &___language_7; }
	inline void set_language_7(String_t* value)
	{
		___language_7 = value;
		Il2CppCodeGenWriteBarrier((&___language_7), value);
	}

	inline static int32_t get_offset_of_markup_8() { return static_cast<int32_t>(offsetof(XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE, ___markup_8)); }
	inline XmlNodeU5BU5D_tBF402FAC28AE903AC3FF845775405A2274B701AF* get_markup_8() const { return ___markup_8; }
	inline XmlNodeU5BU5D_tBF402FAC28AE903AC3FF845775405A2274B701AF** get_address_of_markup_8() { return &___markup_8; }
	inline void set_markup_8(XmlNodeU5BU5D_tBF402FAC28AE903AC3FF845775405A2274B701AF* value)
	{
		___markup_8 = value;
		Il2CppCodeGenWriteBarrier((&___markup_8), value);
	}
};

struct XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaDocumentation::languageType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___languageType_9;

public:
	inline static int32_t get_offset_of_languageType_9() { return static_cast<int32_t>(offsetof(XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE_StaticFields, ___languageType_9)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_languageType_9() const { return ___languageType_9; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_languageType_9() { return &___languageType_9; }
	inline void set_languageType_9(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___languageType_9 = value;
		Il2CppCodeGenWriteBarrier((&___languageType_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADOCUMENTATION_T95A86FC828CDFF9FCC86D8F7774C6496591E67EE_H
#ifndef XMLSCHEMAOBJECTCOLLECTION_T6DCC2B614786CE467B1572B384A163DEE72F1280_H
#define XMLSCHEMAOBJECTCOLLECTION_T6DCC2B614786CE467B1572B384A163DEE72F1280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectCollection
struct  XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280  : public CollectionBase_tF5D4583FF325726066A9803839A04E9C0084ED01
{
public:
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObjectCollection::parent
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * ___parent_1;

public:
	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280, ___parent_1)); }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * get_parent_1() const { return ___parent_1; }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier((&___parent_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECTCOLLECTION_T6DCC2B614786CE467B1572B384A163DEE72F1280_H
#ifndef XMLSCHEMAOBJECTENTRY_TD7A5D31C794A4D04759882DDAD01103D2C19D63B_H
#define XMLSCHEMAOBJECTENTRY_TD7A5D31C794A4D04759882DDAD01103D2C19D63B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry
struct  XmlSchemaObjectEntry_tD7A5D31C794A4D04759882DDAD01103D2C19D63B 
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry::qname
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___qname_0;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry::xso
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * ___xso_1;

public:
	inline static int32_t get_offset_of_qname_0() { return static_cast<int32_t>(offsetof(XmlSchemaObjectEntry_tD7A5D31C794A4D04759882DDAD01103D2C19D63B, ___qname_0)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_qname_0() const { return ___qname_0; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_qname_0() { return &___qname_0; }
	inline void set_qname_0(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___qname_0 = value;
		Il2CppCodeGenWriteBarrier((&___qname_0), value);
	}

	inline static int32_t get_offset_of_xso_1() { return static_cast<int32_t>(offsetof(XmlSchemaObjectEntry_tD7A5D31C794A4D04759882DDAD01103D2C19D63B, ___xso_1)); }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * get_xso_1() const { return ___xso_1; }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B ** get_address_of_xso_1() { return &___xso_1; }
	inline void set_xso_1(XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * value)
	{
		___xso_1 = value;
		Il2CppCodeGenWriteBarrier((&___xso_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry
struct XmlSchemaObjectEntry_tD7A5D31C794A4D04759882DDAD01103D2C19D63B_marshaled_pinvoke
{
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___qname_0;
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * ___xso_1;
};
// Native definition for COM marshalling of System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry
struct XmlSchemaObjectEntry_tD7A5D31C794A4D04759882DDAD01103D2C19D63B_marshaled_com
{
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___qname_0;
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * ___xso_1;
};
#endif // XMLSCHEMAOBJECTENTRY_TD7A5D31C794A4D04759882DDAD01103D2C19D63B_H
#ifndef XMLSCHEMASUBSTITUTIONGROUP_TA063228E2E8787AD4326048660AF77CEA08419E2_H
#define XMLSCHEMASUBSTITUTIONGROUP_TA063228E2E8787AD4326048660AF77CEA08419E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSubstitutionGroup
struct  XmlSchemaSubstitutionGroup_tA063228E2E8787AD4326048660AF77CEA08419E2  : public XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B
{
public:
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaSubstitutionGroup::membersList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___membersList_6;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSubstitutionGroup::examplar
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___examplar_7;

public:
	inline static int32_t get_offset_of_membersList_6() { return static_cast<int32_t>(offsetof(XmlSchemaSubstitutionGroup_tA063228E2E8787AD4326048660AF77CEA08419E2, ___membersList_6)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_membersList_6() const { return ___membersList_6; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_membersList_6() { return &___membersList_6; }
	inline void set_membersList_6(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___membersList_6 = value;
		Il2CppCodeGenWriteBarrier((&___membersList_6), value);
	}

	inline static int32_t get_offset_of_examplar_7() { return static_cast<int32_t>(offsetof(XmlSchemaSubstitutionGroup_tA063228E2E8787AD4326048660AF77CEA08419E2, ___examplar_7)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_examplar_7() const { return ___examplar_7; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_examplar_7() { return &___examplar_7; }
	inline void set_examplar_7(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___examplar_7 = value;
		Il2CppCodeGenWriteBarrier((&___examplar_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASUBSTITUTIONGROUP_TA063228E2E8787AD4326048660AF77CEA08419E2_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef TYPECODE_T03ED52F888000DAF40C550C434F29F39A23D61C6_H
#define TYPECODE_T03ED52F888000DAF40C550C434F29F39A23D61C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TypeCode
struct  TypeCode_t03ED52F888000DAF40C550C434F29F39A23D61C6 
{
public:
	// System.Int32 System.TypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeCode_t03ED52F888000DAF40C550C434F29F39A23D61C6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPECODE_T03ED52F888000DAF40C550C434F29F39A23D61C6_H
#ifndef COMPOSITOR_TE5AE71A32402A0265385AABCE88457F1295A65E4_H
#define COMPOSITOR_TE5AE71A32402A0265385AABCE88457F1295A65E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Compositor
struct  Compositor_tE5AE71A32402A0265385AABCE88457F1295A65E4 
{
public:
	// System.Int32 System.Xml.Schema.Compositor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Compositor_tE5AE71A32402A0265385AABCE88457F1295A65E4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOSITOR_TE5AE71A32402A0265385AABCE88457F1295A65E4_H
#ifndef FACETTYPE_TBB711036F122A1F3BB7E820E16214966B55A2BD9_H
#define FACETTYPE_TBB711036F122A1F3BB7E820E16214966B55A2BD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.FacetType
struct  FacetType_tBB711036F122A1F3BB7E820E16214966B55A2BD9 
{
public:
	// System.Int32 System.Xml.Schema.FacetType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FacetType_tBB711036F122A1F3BB7E820E16214966B55A2BD9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACETTYPE_TBB711036F122A1F3BB7E820E16214966B55A2BD9_H
#ifndef TOKEN_T29EB684F3183FA0B300324C6C6BA75403A81FC9A_H
#define TOKEN_T29EB684F3183FA0B300324C6C6BA75403A81FC9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaNames/Token
struct  Token_t29EB684F3183FA0B300324C6C6BA75403A81FC9A 
{
public:
	// System.Int32 System.Xml.Schema.SchemaNames/Token::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Token_t29EB684F3183FA0B300324C6C6BA75403A81FC9A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T29EB684F3183FA0B300324C6C6BA75403A81FC9A_H
#ifndef VALIDATORSTATE_TBFB3D6A7BF982DA0A31422C4EAAA1631AF81EE55_H
#define VALIDATORSTATE_TBFB3D6A7BF982DA0A31422C4EAAA1631AF81EE55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ValidatorState
struct  ValidatorState_tBFB3D6A7BF982DA0A31422C4EAAA1631AF81EE55 
{
public:
	// System.Int32 System.Xml.Schema.ValidatorState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ValidatorState_tBFB3D6A7BF982DA0A31422C4EAAA1631AF81EE55, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATORSTATE_TBFB3D6A7BF982DA0A31422C4EAAA1631AF81EE55_H
#ifndef UNION_T75FE76D5ECF7F32BF3656D21BD446F4E42996391_H
#define UNION_T75FE76D5ECF7F32BF3656D21BD446F4E42996391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlAtomicValue/Union
struct  Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391 
{
public:
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					// System.Boolean System.Xml.Schema.XmlAtomicValue/Union::boolVal
					bool ___boolVal_0;
				};
				#pragma pack(pop, tp)
				struct
				{
					bool ___boolVal_0_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					// System.Double System.Xml.Schema.XmlAtomicValue/Union::dblVal
					double ___dblVal_1;
				};
				#pragma pack(pop, tp)
				struct
				{
					double ___dblVal_1_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					// System.Int64 System.Xml.Schema.XmlAtomicValue/Union::i64Val
					int64_t ___i64Val_2;
				};
				#pragma pack(pop, tp)
				struct
				{
					int64_t ___i64Val_2_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					// System.Int32 System.Xml.Schema.XmlAtomicValue/Union::i32Val
					int32_t ___i32Val_3;
				};
				#pragma pack(pop, tp)
				struct
				{
					int32_t ___i32Val_3_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					// System.DateTime System.Xml.Schema.XmlAtomicValue/Union::dtVal
					DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___dtVal_4;
				};
				#pragma pack(pop, tp)
				struct
				{
					DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___dtVal_4_forAlignmentOnly;
				};
			};
		};
		uint8_t Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391__padding[8];
	};

public:
	inline static int32_t get_offset_of_boolVal_0() { return static_cast<int32_t>(offsetof(Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391, ___boolVal_0)); }
	inline bool get_boolVal_0() const { return ___boolVal_0; }
	inline bool* get_address_of_boolVal_0() { return &___boolVal_0; }
	inline void set_boolVal_0(bool value)
	{
		___boolVal_0 = value;
	}

	inline static int32_t get_offset_of_dblVal_1() { return static_cast<int32_t>(offsetof(Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391, ___dblVal_1)); }
	inline double get_dblVal_1() const { return ___dblVal_1; }
	inline double* get_address_of_dblVal_1() { return &___dblVal_1; }
	inline void set_dblVal_1(double value)
	{
		___dblVal_1 = value;
	}

	inline static int32_t get_offset_of_i64Val_2() { return static_cast<int32_t>(offsetof(Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391, ___i64Val_2)); }
	inline int64_t get_i64Val_2() const { return ___i64Val_2; }
	inline int64_t* get_address_of_i64Val_2() { return &___i64Val_2; }
	inline void set_i64Val_2(int64_t value)
	{
		___i64Val_2 = value;
	}

	inline static int32_t get_offset_of_i32Val_3() { return static_cast<int32_t>(offsetof(Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391, ___i32Val_3)); }
	inline int32_t get_i32Val_3() const { return ___i32Val_3; }
	inline int32_t* get_address_of_i32Val_3() { return &___i32Val_3; }
	inline void set_i32Val_3(int32_t value)
	{
		___i32Val_3 = value;
	}

	inline static int32_t get_offset_of_dtVal_4() { return static_cast<int32_t>(offsetof(Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391, ___dtVal_4)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_dtVal_4() const { return ___dtVal_4; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_dtVal_4() { return &___dtVal_4; }
	inline void set_dtVal_4(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___dtVal_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.XmlAtomicValue/Union
struct Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391_marshaled_pinvoke
{
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					int32_t ___boolVal_0;
				};
				#pragma pack(pop, tp)
				struct
				{
					int32_t ___boolVal_0_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					double ___dblVal_1;
				};
				#pragma pack(pop, tp)
				struct
				{
					double ___dblVal_1_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					int64_t ___i64Val_2;
				};
				#pragma pack(pop, tp)
				struct
				{
					int64_t ___i64Val_2_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					int32_t ___i32Val_3;
				};
				#pragma pack(pop, tp)
				struct
				{
					int32_t ___i32Val_3_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___dtVal_4;
				};
				#pragma pack(pop, tp)
				struct
				{
					DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___dtVal_4_forAlignmentOnly;
				};
			};
		};
		uint8_t Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391__padding[8];
	};
};
// Native definition for COM marshalling of System.Xml.Schema.XmlAtomicValue/Union
struct Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391_marshaled_com
{
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					int32_t ___boolVal_0;
				};
				#pragma pack(pop, tp)
				struct
				{
					int32_t ___boolVal_0_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					double ___dblVal_1;
				};
				#pragma pack(pop, tp)
				struct
				{
					double ___dblVal_1_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					int64_t ___i64Val_2;
				};
				#pragma pack(pop, tp)
				struct
				{
					int64_t ___i64Val_2_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					int32_t ___i32Val_3;
				};
				#pragma pack(pop, tp)
				struct
				{
					int32_t ___i32Val_3_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___dtVal_4;
				};
				#pragma pack(pop, tp)
				struct
				{
					DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___dtVal_4_forAlignmentOnly;
				};
			};
		};
		uint8_t Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391__padding[8];
	};
};
#endif // UNION_T75FE76D5ECF7F32BF3656D21BD446F4E42996391_H
#ifndef XMLSCHEMAATTRIBUTEGROUP_TE80117D629E6793A9E1DA428A472324C4243509E_H
#define XMLSCHEMAATTRIBUTEGROUP_TE80117D629E6793A9E1DA428A472324C4243509E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAttributeGroup
struct  XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E  : public XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B
{
public:
	// System.String System.Xml.Schema.XmlSchemaAttributeGroup::name
	String_t* ___name_9;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaAttributeGroup::attributes
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___attributes_10;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaAttributeGroup::anyAttribute
	XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * ___anyAttribute_11;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttributeGroup::qname
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___qname_12;
	// System.Xml.Schema.XmlSchemaAttributeGroup System.Xml.Schema.XmlSchemaAttributeGroup::redefined
	XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E * ___redefined_13;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaAttributeGroup::attributeUses
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___attributeUses_14;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaAttributeGroup::attributeWildcard
	XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * ___attributeWildcard_15;
	// System.Int32 System.Xml.Schema.XmlSchemaAttributeGroup::selfReferenceCount
	int32_t ___selfReferenceCount_16;

public:
	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((&___name_9), value);
	}

	inline static int32_t get_offset_of_attributes_10() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E, ___attributes_10)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_attributes_10() const { return ___attributes_10; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_attributes_10() { return &___attributes_10; }
	inline void set_attributes_10(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___attributes_10 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_10), value);
	}

	inline static int32_t get_offset_of_anyAttribute_11() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E, ___anyAttribute_11)); }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * get_anyAttribute_11() const { return ___anyAttribute_11; }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 ** get_address_of_anyAttribute_11() { return &___anyAttribute_11; }
	inline void set_anyAttribute_11(XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * value)
	{
		___anyAttribute_11 = value;
		Il2CppCodeGenWriteBarrier((&___anyAttribute_11), value);
	}

	inline static int32_t get_offset_of_qname_12() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E, ___qname_12)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_qname_12() const { return ___qname_12; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_qname_12() { return &___qname_12; }
	inline void set_qname_12(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___qname_12 = value;
		Il2CppCodeGenWriteBarrier((&___qname_12), value);
	}

	inline static int32_t get_offset_of_redefined_13() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E, ___redefined_13)); }
	inline XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E * get_redefined_13() const { return ___redefined_13; }
	inline XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E ** get_address_of_redefined_13() { return &___redefined_13; }
	inline void set_redefined_13(XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E * value)
	{
		___redefined_13 = value;
		Il2CppCodeGenWriteBarrier((&___redefined_13), value);
	}

	inline static int32_t get_offset_of_attributeUses_14() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E, ___attributeUses_14)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_attributeUses_14() const { return ___attributeUses_14; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_attributeUses_14() { return &___attributeUses_14; }
	inline void set_attributeUses_14(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___attributeUses_14 = value;
		Il2CppCodeGenWriteBarrier((&___attributeUses_14), value);
	}

	inline static int32_t get_offset_of_attributeWildcard_15() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E, ___attributeWildcard_15)); }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * get_attributeWildcard_15() const { return ___attributeWildcard_15; }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 ** get_address_of_attributeWildcard_15() { return &___attributeWildcard_15; }
	inline void set_attributeWildcard_15(XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * value)
	{
		___attributeWildcard_15 = value;
		Il2CppCodeGenWriteBarrier((&___attributeWildcard_15), value);
	}

	inline static int32_t get_offset_of_selfReferenceCount_16() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E, ___selfReferenceCount_16)); }
	inline int32_t get_selfReferenceCount_16() const { return ___selfReferenceCount_16; }
	inline int32_t* get_address_of_selfReferenceCount_16() { return &___selfReferenceCount_16; }
	inline void set_selfReferenceCount_16(int32_t value)
	{
		___selfReferenceCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAATTRIBUTEGROUP_TE80117D629E6793A9E1DA428A472324C4243509E_H
#ifndef XMLSCHEMAATTRIBUTEGROUPREF_T5DA5E994E5C8A3E844A6B88B5E9AEA8195B09BD4_H
#define XMLSCHEMAATTRIBUTEGROUPREF_T5DA5E994E5C8A3E844A6B88B5E9AEA8195B09BD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAttributeGroupRef
struct  XmlSchemaAttributeGroupRef_t5DA5E994E5C8A3E844A6B88B5E9AEA8195B09BD4  : public XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttributeGroupRef::refName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___refName_9;

public:
	inline static int32_t get_offset_of_refName_9() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroupRef_t5DA5E994E5C8A3E844A6B88B5E9AEA8195B09BD4, ___refName_9)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_refName_9() const { return ___refName_9; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_refName_9() { return &___refName_9; }
	inline void set_refName_9(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___refName_9 = value;
		Il2CppCodeGenWriteBarrier((&___refName_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAATTRIBUTEGROUPREF_T5DA5E994E5C8A3E844A6B88B5E9AEA8195B09BD4_H
#ifndef XMLSCHEMACONTENT_TE0E45FE215D74AE629A316B2CDA06480C07278D3_H
#define XMLSCHEMACONTENT_TE0E45FE215D74AE629A316B2CDA06480C07278D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContent
struct  XmlSchemaContent_tE0E45FE215D74AE629A316B2CDA06480C07278D3  : public XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENT_TE0E45FE215D74AE629A316B2CDA06480C07278D3_H
#ifndef XMLSCHEMACONTENTMODEL_TB0209C69ADC1F671FA4ADDE88CC88C8959E84E45_H
#define XMLSCHEMACONTENTMODEL_TB0209C69ADC1F671FA4ADDE88CC88C8959E84E45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentModel
struct  XmlSchemaContentModel_tB0209C69ADC1F671FA4ADDE88CC88C8959E84E45  : public XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTMODEL_TB0209C69ADC1F671FA4ADDE88CC88C8959E84E45_H
#ifndef XMLSCHEMACONTENTPROCESSING_TD85295E4C6FA245DAE6F9E4E1C927E76678B197A_H
#define XMLSCHEMACONTENTPROCESSING_TD85295E4C6FA245DAE6F9E4E1C927E76678B197A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentProcessing
struct  XmlSchemaContentProcessing_tD85295E4C6FA245DAE6F9E4E1C927E76678B197A 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentProcessing::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaContentProcessing_tD85295E4C6FA245DAE6F9E4E1C927E76678B197A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTPROCESSING_TD85295E4C6FA245DAE6F9E4E1C927E76678B197A_H
#ifndef XMLSCHEMACONTENTTYPE_TAAF4C8374963BFF5F4C3F71FAA1AE9E8944E03E5_H
#define XMLSCHEMACONTENTTYPE_TAAF4C8374963BFF5F4C3F71FAA1AE9E8944E03E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentType
struct  XmlSchemaContentType_tAAF4C8374963BFF5F4C3F71FAA1AE9E8944E03E5 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaContentType_tAAF4C8374963BFF5F4C3F71FAA1AE9E8944E03E5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTTYPE_TAAF4C8374963BFF5F4C3F71FAA1AE9E8944E03E5_H
#ifndef XMLSCHEMADERIVATIONMETHOD_T9C964CFFC430E62A1FF62E70B9FE2C976A3A5F58_H
#define XMLSCHEMADERIVATIONMETHOD_T9C964CFFC430E62A1FF62E70B9FE2C976A3A5F58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDerivationMethod
struct  XmlSchemaDerivationMethod_t9C964CFFC430E62A1FF62E70B9FE2C976A3A5F58 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaDerivationMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaDerivationMethod_t9C964CFFC430E62A1FF62E70B9FE2C976A3A5F58, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADERIVATIONMETHOD_T9C964CFFC430E62A1FF62E70B9FE2C976A3A5F58_H
#ifndef XMLSCHEMAEXCEPTION_T6E118FD214784A2E7DE004B99148C2C4CCC1EE65_H
#define XMLSCHEMAEXCEPTION_T6E118FD214784A2E7DE004B99148C2C4CCC1EE65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaException
struct  XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.Xml.Schema.XmlSchemaException::res
	String_t* ___res_17;
	// System.String[] System.Xml.Schema.XmlSchemaException::args
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___args_18;
	// System.String System.Xml.Schema.XmlSchemaException::sourceUri
	String_t* ___sourceUri_19;
	// System.Int32 System.Xml.Schema.XmlSchemaException::lineNumber
	int32_t ___lineNumber_20;
	// System.Int32 System.Xml.Schema.XmlSchemaException::linePosition
	int32_t ___linePosition_21;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaException::sourceSchemaObject
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * ___sourceSchemaObject_22;
	// System.String System.Xml.Schema.XmlSchemaException::message
	String_t* ___message_23;

public:
	inline static int32_t get_offset_of_res_17() { return static_cast<int32_t>(offsetof(XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65, ___res_17)); }
	inline String_t* get_res_17() const { return ___res_17; }
	inline String_t** get_address_of_res_17() { return &___res_17; }
	inline void set_res_17(String_t* value)
	{
		___res_17 = value;
		Il2CppCodeGenWriteBarrier((&___res_17), value);
	}

	inline static int32_t get_offset_of_args_18() { return static_cast<int32_t>(offsetof(XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65, ___args_18)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_args_18() const { return ___args_18; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_args_18() { return &___args_18; }
	inline void set_args_18(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___args_18 = value;
		Il2CppCodeGenWriteBarrier((&___args_18), value);
	}

	inline static int32_t get_offset_of_sourceUri_19() { return static_cast<int32_t>(offsetof(XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65, ___sourceUri_19)); }
	inline String_t* get_sourceUri_19() const { return ___sourceUri_19; }
	inline String_t** get_address_of_sourceUri_19() { return &___sourceUri_19; }
	inline void set_sourceUri_19(String_t* value)
	{
		___sourceUri_19 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_19), value);
	}

	inline static int32_t get_offset_of_lineNumber_20() { return static_cast<int32_t>(offsetof(XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65, ___lineNumber_20)); }
	inline int32_t get_lineNumber_20() const { return ___lineNumber_20; }
	inline int32_t* get_address_of_lineNumber_20() { return &___lineNumber_20; }
	inline void set_lineNumber_20(int32_t value)
	{
		___lineNumber_20 = value;
	}

	inline static int32_t get_offset_of_linePosition_21() { return static_cast<int32_t>(offsetof(XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65, ___linePosition_21)); }
	inline int32_t get_linePosition_21() const { return ___linePosition_21; }
	inline int32_t* get_address_of_linePosition_21() { return &___linePosition_21; }
	inline void set_linePosition_21(int32_t value)
	{
		___linePosition_21 = value;
	}

	inline static int32_t get_offset_of_sourceSchemaObject_22() { return static_cast<int32_t>(offsetof(XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65, ___sourceSchemaObject_22)); }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * get_sourceSchemaObject_22() const { return ___sourceSchemaObject_22; }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B ** get_address_of_sourceSchemaObject_22() { return &___sourceSchemaObject_22; }
	inline void set_sourceSchemaObject_22(XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * value)
	{
		___sourceSchemaObject_22 = value;
		Il2CppCodeGenWriteBarrier((&___sourceSchemaObject_22), value);
	}

	inline static int32_t get_offset_of_message_23() { return static_cast<int32_t>(offsetof(XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65, ___message_23)); }
	inline String_t* get_message_23() const { return ___message_23; }
	inline String_t** get_address_of_message_23() { return &___message_23; }
	inline void set_message_23(String_t* value)
	{
		___message_23 = value;
		Il2CppCodeGenWriteBarrier((&___message_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAEXCEPTION_T6E118FD214784A2E7DE004B99148C2C4CCC1EE65_H
#ifndef XMLSCHEMAFORM_T86E3CD541907A35AC5B505EF118C3E20CC760276_H
#define XMLSCHEMAFORM_T86E3CD541907A35AC5B505EF118C3E20CC760276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaForm
struct  XmlSchemaForm_t86E3CD541907A35AC5B505EF118C3E20CC760276 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaForm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaForm_t86E3CD541907A35AC5B505EF118C3E20CC760276, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFORM_T86E3CD541907A35AC5B505EF118C3E20CC760276_H
#ifndef XMLSCHEMAGROUP_T574051591397CB6582B04CC50578E18BB78B5EB2_H
#define XMLSCHEMAGROUP_T574051591397CB6582B04CC50578E18BB78B5EB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaGroup
struct  XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2  : public XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B
{
public:
	// System.String System.Xml.Schema.XmlSchemaGroup::name
	String_t* ___name_9;
	// System.Xml.Schema.XmlSchemaGroupBase System.Xml.Schema.XmlSchemaGroup::particle
	XmlSchemaGroupBase_tC18B7AAFF241664404A1FB5D936508AB2F4D208F * ___particle_10;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaGroup::canonicalParticle
	XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * ___canonicalParticle_11;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaGroup::qname
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___qname_12;
	// System.Xml.Schema.XmlSchemaGroup System.Xml.Schema.XmlSchemaGroup::redefined
	XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2 * ___redefined_13;
	// System.Int32 System.Xml.Schema.XmlSchemaGroup::selfReferenceCount
	int32_t ___selfReferenceCount_14;

public:
	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((&___name_9), value);
	}

	inline static int32_t get_offset_of_particle_10() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2, ___particle_10)); }
	inline XmlSchemaGroupBase_tC18B7AAFF241664404A1FB5D936508AB2F4D208F * get_particle_10() const { return ___particle_10; }
	inline XmlSchemaGroupBase_tC18B7AAFF241664404A1FB5D936508AB2F4D208F ** get_address_of_particle_10() { return &___particle_10; }
	inline void set_particle_10(XmlSchemaGroupBase_tC18B7AAFF241664404A1FB5D936508AB2F4D208F * value)
	{
		___particle_10 = value;
		Il2CppCodeGenWriteBarrier((&___particle_10), value);
	}

	inline static int32_t get_offset_of_canonicalParticle_11() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2, ___canonicalParticle_11)); }
	inline XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * get_canonicalParticle_11() const { return ___canonicalParticle_11; }
	inline XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 ** get_address_of_canonicalParticle_11() { return &___canonicalParticle_11; }
	inline void set_canonicalParticle_11(XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * value)
	{
		___canonicalParticle_11 = value;
		Il2CppCodeGenWriteBarrier((&___canonicalParticle_11), value);
	}

	inline static int32_t get_offset_of_qname_12() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2, ___qname_12)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_qname_12() const { return ___qname_12; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_qname_12() { return &___qname_12; }
	inline void set_qname_12(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___qname_12 = value;
		Il2CppCodeGenWriteBarrier((&___qname_12), value);
	}

	inline static int32_t get_offset_of_redefined_13() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2, ___redefined_13)); }
	inline XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2 * get_redefined_13() const { return ___redefined_13; }
	inline XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2 ** get_address_of_redefined_13() { return &___redefined_13; }
	inline void set_redefined_13(XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2 * value)
	{
		___redefined_13 = value;
		Il2CppCodeGenWriteBarrier((&___redefined_13), value);
	}

	inline static int32_t get_offset_of_selfReferenceCount_14() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2, ___selfReferenceCount_14)); }
	inline int32_t get_selfReferenceCount_14() const { return ___selfReferenceCount_14; }
	inline int32_t* get_address_of_selfReferenceCount_14() { return &___selfReferenceCount_14; }
	inline void set_selfReferenceCount_14(int32_t value)
	{
		___selfReferenceCount_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAGROUP_T574051591397CB6582B04CC50578E18BB78B5EB2_H
#ifndef XMLSCHEMAIDENTITYCONSTRAINT_T9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95_H
#define XMLSCHEMAIDENTITYCONSTRAINT_T9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaIdentityConstraint
struct  XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95  : public XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B
{
public:
	// System.String System.Xml.Schema.XmlSchemaIdentityConstraint::name
	String_t* ___name_9;
	// System.Xml.Schema.XmlSchemaXPath System.Xml.Schema.XmlSchemaIdentityConstraint::selector
	XmlSchemaXPath_t6D090E4243F2A15A6A5E4EF5B17B0740711C6C34 * ___selector_10;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaIdentityConstraint::fields
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___fields_11;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaIdentityConstraint::qualifiedName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___qualifiedName_12;
	// System.Xml.Schema.CompiledIdentityConstraint System.Xml.Schema.XmlSchemaIdentityConstraint::compiledConstraint
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E * ___compiledConstraint_13;

public:
	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((&___name_9), value);
	}

	inline static int32_t get_offset_of_selector_10() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95, ___selector_10)); }
	inline XmlSchemaXPath_t6D090E4243F2A15A6A5E4EF5B17B0740711C6C34 * get_selector_10() const { return ___selector_10; }
	inline XmlSchemaXPath_t6D090E4243F2A15A6A5E4EF5B17B0740711C6C34 ** get_address_of_selector_10() { return &___selector_10; }
	inline void set_selector_10(XmlSchemaXPath_t6D090E4243F2A15A6A5E4EF5B17B0740711C6C34 * value)
	{
		___selector_10 = value;
		Il2CppCodeGenWriteBarrier((&___selector_10), value);
	}

	inline static int32_t get_offset_of_fields_11() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95, ___fields_11)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_fields_11() const { return ___fields_11; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_fields_11() { return &___fields_11; }
	inline void set_fields_11(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___fields_11 = value;
		Il2CppCodeGenWriteBarrier((&___fields_11), value);
	}

	inline static int32_t get_offset_of_qualifiedName_12() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95, ___qualifiedName_12)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_qualifiedName_12() const { return ___qualifiedName_12; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_qualifiedName_12() { return &___qualifiedName_12; }
	inline void set_qualifiedName_12(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___qualifiedName_12 = value;
		Il2CppCodeGenWriteBarrier((&___qualifiedName_12), value);
	}

	inline static int32_t get_offset_of_compiledConstraint_13() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95, ___compiledConstraint_13)); }
	inline CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E * get_compiledConstraint_13() const { return ___compiledConstraint_13; }
	inline CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E ** get_address_of_compiledConstraint_13() { return &___compiledConstraint_13; }
	inline void set_compiledConstraint_13(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E * value)
	{
		___compiledConstraint_13 = value;
		Il2CppCodeGenWriteBarrier((&___compiledConstraint_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAIDENTITYCONSTRAINT_T9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95_H
#ifndef XMLSCHEMANOTATION_TA67925FC817C85C8CB753EBAFBB7EA0AE414D41D_H
#define XMLSCHEMANOTATION_TA67925FC817C85C8CB753EBAFBB7EA0AE414D41D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaNotation
struct  XmlSchemaNotation_tA67925FC817C85C8CB753EBAFBB7EA0AE414D41D  : public XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B
{
public:
	// System.String System.Xml.Schema.XmlSchemaNotation::name
	String_t* ___name_9;
	// System.String System.Xml.Schema.XmlSchemaNotation::publicId
	String_t* ___publicId_10;
	// System.String System.Xml.Schema.XmlSchemaNotation::systemId
	String_t* ___systemId_11;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaNotation::qname
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___qname_12;

public:
	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_tA67925FC817C85C8CB753EBAFBB7EA0AE414D41D, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((&___name_9), value);
	}

	inline static int32_t get_offset_of_publicId_10() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_tA67925FC817C85C8CB753EBAFBB7EA0AE414D41D, ___publicId_10)); }
	inline String_t* get_publicId_10() const { return ___publicId_10; }
	inline String_t** get_address_of_publicId_10() { return &___publicId_10; }
	inline void set_publicId_10(String_t* value)
	{
		___publicId_10 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_10), value);
	}

	inline static int32_t get_offset_of_systemId_11() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_tA67925FC817C85C8CB753EBAFBB7EA0AE414D41D, ___systemId_11)); }
	inline String_t* get_systemId_11() const { return ___systemId_11; }
	inline String_t** get_address_of_systemId_11() { return &___systemId_11; }
	inline void set_systemId_11(String_t* value)
	{
		___systemId_11 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_11), value);
	}

	inline static int32_t get_offset_of_qname_12() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_tA67925FC817C85C8CB753EBAFBB7EA0AE414D41D, ___qname_12)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_qname_12() const { return ___qname_12; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_qname_12() { return &___qname_12; }
	inline void set_qname_12(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___qname_12 = value;
		Il2CppCodeGenWriteBarrier((&___qname_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMANOTATION_TA67925FC817C85C8CB753EBAFBB7EA0AE414D41D_H
#ifndef ENUMERATORTYPE_T123506A6BEEC590B086DA6B4EBBD1CEA328A30A0_H
#define ENUMERATORTYPE_T123506A6BEEC590B086DA6B4EBBD1CEA328A30A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectTable/EnumeratorType
struct  EnumeratorType_t123506A6BEEC590B086DA6B4EBBD1CEA328A30A0 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaObjectTable/EnumeratorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EnumeratorType_t123506A6BEEC590B086DA6B4EBBD1CEA328A30A0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATORTYPE_T123506A6BEEC590B086DA6B4EBBD1CEA328A30A0_H
#ifndef OCCURS_T6B3513E522B1DF0DB690037276296BB18F81BCE0_H
#define OCCURS_T6B3513E522B1DF0DB690037276296BB18F81BCE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaParticle/Occurs
struct  Occurs_t6B3513E522B1DF0DB690037276296BB18F81BCE0 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaParticle/Occurs::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Occurs_t6B3513E522B1DF0DB690037276296BB18F81BCE0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCCURS_T6B3513E522B1DF0DB690037276296BB18F81BCE0_H
#ifndef XMLSCHEMASIMPLETYPECONTENT_T87EF2A2F4B2F3D7827EA18B0FCB8393EA0201A3E_H
#define XMLSCHEMASIMPLETYPECONTENT_T87EF2A2F4B2F3D7827EA18B0FCB8393EA0201A3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeContent
struct  XmlSchemaSimpleTypeContent_t87EF2A2F4B2F3D7827EA18B0FCB8393EA0201A3E  : public XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPECONTENT_T87EF2A2F4B2F3D7827EA18B0FCB8393EA0201A3E_H
#ifndef XMLSCHEMASUBSTITUTIONGROUPV1COMPAT_TAE2B7C8AB4A0DA4E3FEBC99A018C344733A1FC54_H
#define XMLSCHEMASUBSTITUTIONGROUPV1COMPAT_TAE2B7C8AB4A0DA4E3FEBC99A018C344733A1FC54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSubstitutionGroupV1Compat
struct  XmlSchemaSubstitutionGroupV1Compat_tAE2B7C8AB4A0DA4E3FEBC99A018C344733A1FC54  : public XmlSchemaSubstitutionGroup_tA063228E2E8787AD4326048660AF77CEA08419E2
{
public:
	// System.Xml.Schema.XmlSchemaChoice System.Xml.Schema.XmlSchemaSubstitutionGroupV1Compat::choice
	XmlSchemaChoice_tEC175BFF045FDA4DECF5A938922D6CDC4216653B * ___choice_8;

public:
	inline static int32_t get_offset_of_choice_8() { return static_cast<int32_t>(offsetof(XmlSchemaSubstitutionGroupV1Compat_tAE2B7C8AB4A0DA4E3FEBC99A018C344733A1FC54, ___choice_8)); }
	inline XmlSchemaChoice_tEC175BFF045FDA4DECF5A938922D6CDC4216653B * get_choice_8() const { return ___choice_8; }
	inline XmlSchemaChoice_tEC175BFF045FDA4DECF5A938922D6CDC4216653B ** get_address_of_choice_8() { return &___choice_8; }
	inline void set_choice_8(XmlSchemaChoice_tEC175BFF045FDA4DECF5A938922D6CDC4216653B * value)
	{
		___choice_8 = value;
		Il2CppCodeGenWriteBarrier((&___choice_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASUBSTITUTIONGROUPV1COMPAT_TAE2B7C8AB4A0DA4E3FEBC99A018C344733A1FC54_H
#ifndef XMLSCHEMAUSE_T69A028A548128D3041B789FCCCD1BEDCE2365BF3_H
#define XMLSCHEMAUSE_T69A028A548128D3041B789FCCCD1BEDCE2365BF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaUse
struct  XmlSchemaUse_t69A028A548128D3041B789FCCCD1BEDCE2365BF3 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaUse::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaUse_t69A028A548128D3041B789FCCCD1BEDCE2365BF3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAUSE_T69A028A548128D3041B789FCCCD1BEDCE2365BF3_H
#ifndef XMLSCHEMAVALIDATIONFLAGS_T866C695263C623EE4F1746A541248AE12D311A73_H
#define XMLSCHEMAVALIDATIONFLAGS_T866C695263C623EE4F1746A541248AE12D311A73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidationFlags
struct  XmlSchemaValidationFlags_t866C695263C623EE4F1746A541248AE12D311A73 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidationFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaValidationFlags_t866C695263C623EE4F1746A541248AE12D311A73, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATIONFLAGS_T866C695263C623EE4F1746A541248AE12D311A73_H
#ifndef XMLSCHEMAVALIDITY_TFB9AEE7708C4FF08DC1564482F46991CF8FD0621_H
#define XMLSCHEMAVALIDITY_TFB9AEE7708C4FF08DC1564482F46991CF8FD0621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidity
struct  XmlSchemaValidity_tFB9AEE7708C4FF08DC1564482F46991CF8FD0621 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidity::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaValidity_tFB9AEE7708C4FF08DC1564482F46991CF8FD0621, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDITY_TFB9AEE7708C4FF08DC1564482F46991CF8FD0621_H
#ifndef XMLSCHEMAXPATH_T6D090E4243F2A15A6A5E4EF5B17B0740711C6C34_H
#define XMLSCHEMAXPATH_T6D090E4243F2A15A6A5E4EF5B17B0740711C6C34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaXPath
struct  XmlSchemaXPath_t6D090E4243F2A15A6A5E4EF5B17B0740711C6C34  : public XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B
{
public:
	// System.String System.Xml.Schema.XmlSchemaXPath::xpath
	String_t* ___xpath_9;

public:
	inline static int32_t get_offset_of_xpath_9() { return static_cast<int32_t>(offsetof(XmlSchemaXPath_t6D090E4243F2A15A6A5E4EF5B17B0740711C6C34, ___xpath_9)); }
	inline String_t* get_xpath_9() const { return ___xpath_9; }
	inline String_t** get_address_of_xpath_9() { return &___xpath_9; }
	inline void set_xpath_9(String_t* value)
	{
		___xpath_9 = value;
		Il2CppCodeGenWriteBarrier((&___xpath_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAXPATH_T6D090E4243F2A15A6A5E4EF5B17B0740711C6C34_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef XDRATTRIBUTEENTRY_T35EED30A44B91178F3443D4688FB6481C36DA3AD_H
#define XDRATTRIBUTEENTRY_T35EED30A44B91178F3443D4688FB6481C36DA3AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/XdrAttributeEntry
struct  XdrAttributeEntry_t35EED30A44B91178F3443D4688FB6481C36DA3AD  : public RuntimeObject
{
public:
	// System.Xml.Schema.SchemaNames/Token System.Xml.Schema.XdrBuilder/XdrAttributeEntry::_Attribute
	int32_t ____Attribute_0;
	// System.Int32 System.Xml.Schema.XdrBuilder/XdrAttributeEntry::_SchemaFlags
	int32_t ____SchemaFlags_1;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XdrBuilder/XdrAttributeEntry::_Datatype
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ____Datatype_2;
	// System.Xml.Schema.XdrBuilder/XdrBuildFunction System.Xml.Schema.XdrBuilder/XdrAttributeEntry::_BuildFunc
	XdrBuildFunction_tB7A90FF323AFC1DBE190CA3721EB16B45E48B8A7 * ____BuildFunc_3;

public:
	inline static int32_t get_offset_of__Attribute_0() { return static_cast<int32_t>(offsetof(XdrAttributeEntry_t35EED30A44B91178F3443D4688FB6481C36DA3AD, ____Attribute_0)); }
	inline int32_t get__Attribute_0() const { return ____Attribute_0; }
	inline int32_t* get_address_of__Attribute_0() { return &____Attribute_0; }
	inline void set__Attribute_0(int32_t value)
	{
		____Attribute_0 = value;
	}

	inline static int32_t get_offset_of__SchemaFlags_1() { return static_cast<int32_t>(offsetof(XdrAttributeEntry_t35EED30A44B91178F3443D4688FB6481C36DA3AD, ____SchemaFlags_1)); }
	inline int32_t get__SchemaFlags_1() const { return ____SchemaFlags_1; }
	inline int32_t* get_address_of__SchemaFlags_1() { return &____SchemaFlags_1; }
	inline void set__SchemaFlags_1(int32_t value)
	{
		____SchemaFlags_1 = value;
	}

	inline static int32_t get_offset_of__Datatype_2() { return static_cast<int32_t>(offsetof(XdrAttributeEntry_t35EED30A44B91178F3443D4688FB6481C36DA3AD, ____Datatype_2)); }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * get__Datatype_2() const { return ____Datatype_2; }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 ** get_address_of__Datatype_2() { return &____Datatype_2; }
	inline void set__Datatype_2(XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * value)
	{
		____Datatype_2 = value;
		Il2CppCodeGenWriteBarrier((&____Datatype_2), value);
	}

	inline static int32_t get_offset_of__BuildFunc_3() { return static_cast<int32_t>(offsetof(XdrAttributeEntry_t35EED30A44B91178F3443D4688FB6481C36DA3AD, ____BuildFunc_3)); }
	inline XdrBuildFunction_tB7A90FF323AFC1DBE190CA3721EB16B45E48B8A7 * get__BuildFunc_3() const { return ____BuildFunc_3; }
	inline XdrBuildFunction_tB7A90FF323AFC1DBE190CA3721EB16B45E48B8A7 ** get_address_of__BuildFunc_3() { return &____BuildFunc_3; }
	inline void set__BuildFunc_3(XdrBuildFunction_tB7A90FF323AFC1DBE190CA3721EB16B45E48B8A7 * value)
	{
		____BuildFunc_3 = value;
		Il2CppCodeGenWriteBarrier((&____BuildFunc_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDRATTRIBUTEENTRY_T35EED30A44B91178F3443D4688FB6481C36DA3AD_H
#ifndef XDRENTRY_T3C189DCB0821841C390429A96D87E51B157480C6_H
#define XDRENTRY_T3C189DCB0821841C390429A96D87E51B157480C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/XdrEntry
struct  XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6  : public RuntimeObject
{
public:
	// System.Xml.Schema.SchemaNames/Token System.Xml.Schema.XdrBuilder/XdrEntry::_Name
	int32_t ____Name_0;
	// System.Int32[] System.Xml.Schema.XdrBuilder/XdrEntry::_NextStates
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____NextStates_1;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder/XdrEntry::_Attributes
	XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* ____Attributes_2;
	// System.Xml.Schema.XdrBuilder/XdrInitFunction System.Xml.Schema.XdrBuilder/XdrEntry::_InitFunc
	XdrInitFunction_tE54658948C45C11D13B781182403CAA3ADBC658D * ____InitFunc_3;
	// System.Xml.Schema.XdrBuilder/XdrBeginChildFunction System.Xml.Schema.XdrBuilder/XdrEntry::_BeginChildFunc
	XdrBeginChildFunction_t0A5E4A12B86755E9EA9D9E723493EBBE9FFBF472 * ____BeginChildFunc_4;
	// System.Xml.Schema.XdrBuilder/XdrEndChildFunction System.Xml.Schema.XdrBuilder/XdrEntry::_EndChildFunc
	XdrEndChildFunction_t9A1AE00DFBA27B69E9A156C48AAFEF0425F0601F * ____EndChildFunc_5;
	// System.Boolean System.Xml.Schema.XdrBuilder/XdrEntry::_AllowText
	bool ____AllowText_6;

public:
	inline static int32_t get_offset_of__Name_0() { return static_cast<int32_t>(offsetof(XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6, ____Name_0)); }
	inline int32_t get__Name_0() const { return ____Name_0; }
	inline int32_t* get_address_of__Name_0() { return &____Name_0; }
	inline void set__Name_0(int32_t value)
	{
		____Name_0 = value;
	}

	inline static int32_t get_offset_of__NextStates_1() { return static_cast<int32_t>(offsetof(XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6, ____NextStates_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__NextStates_1() const { return ____NextStates_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__NextStates_1() { return &____NextStates_1; }
	inline void set__NextStates_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____NextStates_1 = value;
		Il2CppCodeGenWriteBarrier((&____NextStates_1), value);
	}

	inline static int32_t get_offset_of__Attributes_2() { return static_cast<int32_t>(offsetof(XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6, ____Attributes_2)); }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* get__Attributes_2() const { return ____Attributes_2; }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B** get_address_of__Attributes_2() { return &____Attributes_2; }
	inline void set__Attributes_2(XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* value)
	{
		____Attributes_2 = value;
		Il2CppCodeGenWriteBarrier((&____Attributes_2), value);
	}

	inline static int32_t get_offset_of__InitFunc_3() { return static_cast<int32_t>(offsetof(XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6, ____InitFunc_3)); }
	inline XdrInitFunction_tE54658948C45C11D13B781182403CAA3ADBC658D * get__InitFunc_3() const { return ____InitFunc_3; }
	inline XdrInitFunction_tE54658948C45C11D13B781182403CAA3ADBC658D ** get_address_of__InitFunc_3() { return &____InitFunc_3; }
	inline void set__InitFunc_3(XdrInitFunction_tE54658948C45C11D13B781182403CAA3ADBC658D * value)
	{
		____InitFunc_3 = value;
		Il2CppCodeGenWriteBarrier((&____InitFunc_3), value);
	}

	inline static int32_t get_offset_of__BeginChildFunc_4() { return static_cast<int32_t>(offsetof(XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6, ____BeginChildFunc_4)); }
	inline XdrBeginChildFunction_t0A5E4A12B86755E9EA9D9E723493EBBE9FFBF472 * get__BeginChildFunc_4() const { return ____BeginChildFunc_4; }
	inline XdrBeginChildFunction_t0A5E4A12B86755E9EA9D9E723493EBBE9FFBF472 ** get_address_of__BeginChildFunc_4() { return &____BeginChildFunc_4; }
	inline void set__BeginChildFunc_4(XdrBeginChildFunction_t0A5E4A12B86755E9EA9D9E723493EBBE9FFBF472 * value)
	{
		____BeginChildFunc_4 = value;
		Il2CppCodeGenWriteBarrier((&____BeginChildFunc_4), value);
	}

	inline static int32_t get_offset_of__EndChildFunc_5() { return static_cast<int32_t>(offsetof(XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6, ____EndChildFunc_5)); }
	inline XdrEndChildFunction_t9A1AE00DFBA27B69E9A156C48AAFEF0425F0601F * get__EndChildFunc_5() const { return ____EndChildFunc_5; }
	inline XdrEndChildFunction_t9A1AE00DFBA27B69E9A156C48AAFEF0425F0601F ** get_address_of__EndChildFunc_5() { return &____EndChildFunc_5; }
	inline void set__EndChildFunc_5(XdrEndChildFunction_t9A1AE00DFBA27B69E9A156C48AAFEF0425F0601F * value)
	{
		____EndChildFunc_5 = value;
		Il2CppCodeGenWriteBarrier((&____EndChildFunc_5), value);
	}

	inline static int32_t get_offset_of__AllowText_6() { return static_cast<int32_t>(offsetof(XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6, ____AllowText_6)); }
	inline bool get__AllowText_6() const { return ____AllowText_6; }
	inline bool* get_address_of__AllowText_6() { return &____AllowText_6; }
	inline void set__AllowText_6(bool value)
	{
		____AllowText_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDRENTRY_T3C189DCB0821841C390429A96D87E51B157480C6_H
#ifndef XMLATOMICVALUE_TD2DE4444E12CFCA939E01F07BE367EEEFD7C38C0_H
#define XMLATOMICVALUE_TD2DE4444E12CFCA939E01F07BE367EEEFD7C38C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlAtomicValue
struct  XmlAtomicValue_tD2DE4444E12CFCA939E01F07BE367EEEFD7C38C0  : public XPathItem_tCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98
{
public:
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlAtomicValue::xmlType
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * ___xmlType_0;
	// System.Object System.Xml.Schema.XmlAtomicValue::objVal
	RuntimeObject * ___objVal_1;
	// System.TypeCode System.Xml.Schema.XmlAtomicValue::clrType
	int32_t ___clrType_2;
	// System.Xml.Schema.XmlAtomicValue/Union System.Xml.Schema.XmlAtomicValue::unionVal
	Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391  ___unionVal_3;
	// System.Xml.Schema.XmlAtomicValue/NamespacePrefixForQName System.Xml.Schema.XmlAtomicValue::nsPrefix
	NamespacePrefixForQName_t80F0083B9C2151D487D41CCDD89B668C21902D8F * ___nsPrefix_4;

public:
	inline static int32_t get_offset_of_xmlType_0() { return static_cast<int32_t>(offsetof(XmlAtomicValue_tD2DE4444E12CFCA939E01F07BE367EEEFD7C38C0, ___xmlType_0)); }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * get_xmlType_0() const { return ___xmlType_0; }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 ** get_address_of_xmlType_0() { return &___xmlType_0; }
	inline void set_xmlType_0(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * value)
	{
		___xmlType_0 = value;
		Il2CppCodeGenWriteBarrier((&___xmlType_0), value);
	}

	inline static int32_t get_offset_of_objVal_1() { return static_cast<int32_t>(offsetof(XmlAtomicValue_tD2DE4444E12CFCA939E01F07BE367EEEFD7C38C0, ___objVal_1)); }
	inline RuntimeObject * get_objVal_1() const { return ___objVal_1; }
	inline RuntimeObject ** get_address_of_objVal_1() { return &___objVal_1; }
	inline void set_objVal_1(RuntimeObject * value)
	{
		___objVal_1 = value;
		Il2CppCodeGenWriteBarrier((&___objVal_1), value);
	}

	inline static int32_t get_offset_of_clrType_2() { return static_cast<int32_t>(offsetof(XmlAtomicValue_tD2DE4444E12CFCA939E01F07BE367EEEFD7C38C0, ___clrType_2)); }
	inline int32_t get_clrType_2() const { return ___clrType_2; }
	inline int32_t* get_address_of_clrType_2() { return &___clrType_2; }
	inline void set_clrType_2(int32_t value)
	{
		___clrType_2 = value;
	}

	inline static int32_t get_offset_of_unionVal_3() { return static_cast<int32_t>(offsetof(XmlAtomicValue_tD2DE4444E12CFCA939E01F07BE367EEEFD7C38C0, ___unionVal_3)); }
	inline Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391  get_unionVal_3() const { return ___unionVal_3; }
	inline Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391 * get_address_of_unionVal_3() { return &___unionVal_3; }
	inline void set_unionVal_3(Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391  value)
	{
		___unionVal_3 = value;
	}

	inline static int32_t get_offset_of_nsPrefix_4() { return static_cast<int32_t>(offsetof(XmlAtomicValue_tD2DE4444E12CFCA939E01F07BE367EEEFD7C38C0, ___nsPrefix_4)); }
	inline NamespacePrefixForQName_t80F0083B9C2151D487D41CCDD89B668C21902D8F * get_nsPrefix_4() const { return ___nsPrefix_4; }
	inline NamespacePrefixForQName_t80F0083B9C2151D487D41CCDD89B668C21902D8F ** get_address_of_nsPrefix_4() { return &___nsPrefix_4; }
	inline void set_nsPrefix_4(NamespacePrefixForQName_t80F0083B9C2151D487D41CCDD89B668C21902D8F * value)
	{
		___nsPrefix_4 = value;
		Il2CppCodeGenWriteBarrier((&___nsPrefix_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATOMICVALUE_TD2DE4444E12CFCA939E01F07BE367EEEFD7C38C0_H
#ifndef XMLSCHEMA_TBCBDA8467097049177257E2A0493EDF5C8FD477F_H
#define XMLSCHEMA_TBCBDA8467097049177257E2A0493EDF5C8FD477F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchema
struct  XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F  : public XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B
{
public:
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchema::attributeFormDefault
	int32_t ___attributeFormDefault_6;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchema::elementFormDefault
	int32_t ___elementFormDefault_7;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchema::blockDefault
	int32_t ___blockDefault_8;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchema::finalDefault
	int32_t ___finalDefault_9;
	// System.String System.Xml.Schema.XmlSchema::targetNs
	String_t* ___targetNs_10;
	// System.String System.Xml.Schema.XmlSchema::version
	String_t* ___version_11;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchema::includes
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___includes_12;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchema::items
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___items_13;
	// System.String System.Xml.Schema.XmlSchema::id
	String_t* ___id_14;
	// System.Xml.XmlAttribute[] System.Xml.Schema.XmlSchema::moreAttributes
	XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908* ___moreAttributes_15;
	// System.Boolean System.Xml.Schema.XmlSchema::isCompiled
	bool ___isCompiled_16;
	// System.Boolean System.Xml.Schema.XmlSchema::isCompiledBySet
	bool ___isCompiledBySet_17;
	// System.Boolean System.Xml.Schema.XmlSchema::isPreprocessed
	bool ___isPreprocessed_18;
	// System.Boolean System.Xml.Schema.XmlSchema::isRedefined
	bool ___isRedefined_19;
	// System.Int32 System.Xml.Schema.XmlSchema::errorCount
	int32_t ___errorCount_20;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::attributes
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___attributes_21;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::attributeGroups
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___attributeGroups_22;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::elements
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___elements_23;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::types
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___types_24;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::groups
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___groups_25;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::notations
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___notations_26;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::identityConstraints
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___identityConstraints_27;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchema::importedSchemas
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___importedSchemas_29;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchema::importedNamespaces
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___importedNamespaces_30;
	// System.Int32 System.Xml.Schema.XmlSchema::schemaId
	int32_t ___schemaId_31;
	// System.Uri System.Xml.Schema.XmlSchema::baseUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___baseUri_32;
	// System.Boolean System.Xml.Schema.XmlSchema::isChameleon
	bool ___isChameleon_33;
	// System.Collections.Hashtable System.Xml.Schema.XmlSchema::ids
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___ids_34;
	// System.Xml.XmlDocument System.Xml.Schema.XmlSchema::document
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ___document_35;

public:
	inline static int32_t get_offset_of_attributeFormDefault_6() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___attributeFormDefault_6)); }
	inline int32_t get_attributeFormDefault_6() const { return ___attributeFormDefault_6; }
	inline int32_t* get_address_of_attributeFormDefault_6() { return &___attributeFormDefault_6; }
	inline void set_attributeFormDefault_6(int32_t value)
	{
		___attributeFormDefault_6 = value;
	}

	inline static int32_t get_offset_of_elementFormDefault_7() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___elementFormDefault_7)); }
	inline int32_t get_elementFormDefault_7() const { return ___elementFormDefault_7; }
	inline int32_t* get_address_of_elementFormDefault_7() { return &___elementFormDefault_7; }
	inline void set_elementFormDefault_7(int32_t value)
	{
		___elementFormDefault_7 = value;
	}

	inline static int32_t get_offset_of_blockDefault_8() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___blockDefault_8)); }
	inline int32_t get_blockDefault_8() const { return ___blockDefault_8; }
	inline int32_t* get_address_of_blockDefault_8() { return &___blockDefault_8; }
	inline void set_blockDefault_8(int32_t value)
	{
		___blockDefault_8 = value;
	}

	inline static int32_t get_offset_of_finalDefault_9() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___finalDefault_9)); }
	inline int32_t get_finalDefault_9() const { return ___finalDefault_9; }
	inline int32_t* get_address_of_finalDefault_9() { return &___finalDefault_9; }
	inline void set_finalDefault_9(int32_t value)
	{
		___finalDefault_9 = value;
	}

	inline static int32_t get_offset_of_targetNs_10() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___targetNs_10)); }
	inline String_t* get_targetNs_10() const { return ___targetNs_10; }
	inline String_t** get_address_of_targetNs_10() { return &___targetNs_10; }
	inline void set_targetNs_10(String_t* value)
	{
		___targetNs_10 = value;
		Il2CppCodeGenWriteBarrier((&___targetNs_10), value);
	}

	inline static int32_t get_offset_of_version_11() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___version_11)); }
	inline String_t* get_version_11() const { return ___version_11; }
	inline String_t** get_address_of_version_11() { return &___version_11; }
	inline void set_version_11(String_t* value)
	{
		___version_11 = value;
		Il2CppCodeGenWriteBarrier((&___version_11), value);
	}

	inline static int32_t get_offset_of_includes_12() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___includes_12)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_includes_12() const { return ___includes_12; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_includes_12() { return &___includes_12; }
	inline void set_includes_12(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___includes_12 = value;
		Il2CppCodeGenWriteBarrier((&___includes_12), value);
	}

	inline static int32_t get_offset_of_items_13() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___items_13)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_items_13() const { return ___items_13; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_items_13() { return &___items_13; }
	inline void set_items_13(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___items_13 = value;
		Il2CppCodeGenWriteBarrier((&___items_13), value);
	}

	inline static int32_t get_offset_of_id_14() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___id_14)); }
	inline String_t* get_id_14() const { return ___id_14; }
	inline String_t** get_address_of_id_14() { return &___id_14; }
	inline void set_id_14(String_t* value)
	{
		___id_14 = value;
		Il2CppCodeGenWriteBarrier((&___id_14), value);
	}

	inline static int32_t get_offset_of_moreAttributes_15() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___moreAttributes_15)); }
	inline XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908* get_moreAttributes_15() const { return ___moreAttributes_15; }
	inline XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908** get_address_of_moreAttributes_15() { return &___moreAttributes_15; }
	inline void set_moreAttributes_15(XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908* value)
	{
		___moreAttributes_15 = value;
		Il2CppCodeGenWriteBarrier((&___moreAttributes_15), value);
	}

	inline static int32_t get_offset_of_isCompiled_16() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___isCompiled_16)); }
	inline bool get_isCompiled_16() const { return ___isCompiled_16; }
	inline bool* get_address_of_isCompiled_16() { return &___isCompiled_16; }
	inline void set_isCompiled_16(bool value)
	{
		___isCompiled_16 = value;
	}

	inline static int32_t get_offset_of_isCompiledBySet_17() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___isCompiledBySet_17)); }
	inline bool get_isCompiledBySet_17() const { return ___isCompiledBySet_17; }
	inline bool* get_address_of_isCompiledBySet_17() { return &___isCompiledBySet_17; }
	inline void set_isCompiledBySet_17(bool value)
	{
		___isCompiledBySet_17 = value;
	}

	inline static int32_t get_offset_of_isPreprocessed_18() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___isPreprocessed_18)); }
	inline bool get_isPreprocessed_18() const { return ___isPreprocessed_18; }
	inline bool* get_address_of_isPreprocessed_18() { return &___isPreprocessed_18; }
	inline void set_isPreprocessed_18(bool value)
	{
		___isPreprocessed_18 = value;
	}

	inline static int32_t get_offset_of_isRedefined_19() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___isRedefined_19)); }
	inline bool get_isRedefined_19() const { return ___isRedefined_19; }
	inline bool* get_address_of_isRedefined_19() { return &___isRedefined_19; }
	inline void set_isRedefined_19(bool value)
	{
		___isRedefined_19 = value;
	}

	inline static int32_t get_offset_of_errorCount_20() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___errorCount_20)); }
	inline int32_t get_errorCount_20() const { return ___errorCount_20; }
	inline int32_t* get_address_of_errorCount_20() { return &___errorCount_20; }
	inline void set_errorCount_20(int32_t value)
	{
		___errorCount_20 = value;
	}

	inline static int32_t get_offset_of_attributes_21() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___attributes_21)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_attributes_21() const { return ___attributes_21; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_attributes_21() { return &___attributes_21; }
	inline void set_attributes_21(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___attributes_21 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_21), value);
	}

	inline static int32_t get_offset_of_attributeGroups_22() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___attributeGroups_22)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_attributeGroups_22() const { return ___attributeGroups_22; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_attributeGroups_22() { return &___attributeGroups_22; }
	inline void set_attributeGroups_22(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___attributeGroups_22 = value;
		Il2CppCodeGenWriteBarrier((&___attributeGroups_22), value);
	}

	inline static int32_t get_offset_of_elements_23() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___elements_23)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_elements_23() const { return ___elements_23; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_elements_23() { return &___elements_23; }
	inline void set_elements_23(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___elements_23 = value;
		Il2CppCodeGenWriteBarrier((&___elements_23), value);
	}

	inline static int32_t get_offset_of_types_24() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___types_24)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_types_24() const { return ___types_24; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_types_24() { return &___types_24; }
	inline void set_types_24(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___types_24 = value;
		Il2CppCodeGenWriteBarrier((&___types_24), value);
	}

	inline static int32_t get_offset_of_groups_25() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___groups_25)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_groups_25() const { return ___groups_25; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_groups_25() { return &___groups_25; }
	inline void set_groups_25(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___groups_25 = value;
		Il2CppCodeGenWriteBarrier((&___groups_25), value);
	}

	inline static int32_t get_offset_of_notations_26() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___notations_26)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_notations_26() const { return ___notations_26; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_notations_26() { return &___notations_26; }
	inline void set_notations_26(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___notations_26 = value;
		Il2CppCodeGenWriteBarrier((&___notations_26), value);
	}

	inline static int32_t get_offset_of_identityConstraints_27() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___identityConstraints_27)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_identityConstraints_27() const { return ___identityConstraints_27; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_identityConstraints_27() { return &___identityConstraints_27; }
	inline void set_identityConstraints_27(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___identityConstraints_27 = value;
		Il2CppCodeGenWriteBarrier((&___identityConstraints_27), value);
	}

	inline static int32_t get_offset_of_importedSchemas_29() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___importedSchemas_29)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_importedSchemas_29() const { return ___importedSchemas_29; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_importedSchemas_29() { return &___importedSchemas_29; }
	inline void set_importedSchemas_29(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___importedSchemas_29 = value;
		Il2CppCodeGenWriteBarrier((&___importedSchemas_29), value);
	}

	inline static int32_t get_offset_of_importedNamespaces_30() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___importedNamespaces_30)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_importedNamespaces_30() const { return ___importedNamespaces_30; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_importedNamespaces_30() { return &___importedNamespaces_30; }
	inline void set_importedNamespaces_30(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___importedNamespaces_30 = value;
		Il2CppCodeGenWriteBarrier((&___importedNamespaces_30), value);
	}

	inline static int32_t get_offset_of_schemaId_31() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___schemaId_31)); }
	inline int32_t get_schemaId_31() const { return ___schemaId_31; }
	inline int32_t* get_address_of_schemaId_31() { return &___schemaId_31; }
	inline void set_schemaId_31(int32_t value)
	{
		___schemaId_31 = value;
	}

	inline static int32_t get_offset_of_baseUri_32() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___baseUri_32)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_baseUri_32() const { return ___baseUri_32; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_baseUri_32() { return &___baseUri_32; }
	inline void set_baseUri_32(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___baseUri_32 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_32), value);
	}

	inline static int32_t get_offset_of_isChameleon_33() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___isChameleon_33)); }
	inline bool get_isChameleon_33() const { return ___isChameleon_33; }
	inline bool* get_address_of_isChameleon_33() { return &___isChameleon_33; }
	inline void set_isChameleon_33(bool value)
	{
		___isChameleon_33 = value;
	}

	inline static int32_t get_offset_of_ids_34() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___ids_34)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_ids_34() const { return ___ids_34; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_ids_34() { return &___ids_34; }
	inline void set_ids_34(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___ids_34 = value;
		Il2CppCodeGenWriteBarrier((&___ids_34), value);
	}

	inline static int32_t get_offset_of_document_35() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F, ___document_35)); }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * get_document_35() const { return ___document_35; }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 ** get_address_of_document_35() { return &___document_35; }
	inline void set_document_35(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * value)
	{
		___document_35 = value;
		Il2CppCodeGenWriteBarrier((&___document_35), value);
	}
};

struct XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F_StaticFields
{
public:
	// System.Int32 System.Xml.Schema.XmlSchema::globalIdCounter
	int32_t ___globalIdCounter_28;

public:
	inline static int32_t get_offset_of_globalIdCounter_28() { return static_cast<int32_t>(offsetof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F_StaticFields, ___globalIdCounter_28)); }
	inline int32_t get_globalIdCounter_28() const { return ___globalIdCounter_28; }
	inline int32_t* get_address_of_globalIdCounter_28() { return &___globalIdCounter_28; }
	inline void set_globalIdCounter_28(int32_t value)
	{
		___globalIdCounter_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMA_TBCBDA8467097049177257E2A0493EDF5C8FD477F_H
#ifndef XMLSCHEMAANYATTRIBUTE_T09BC4DE0DCA6086FA21F134EAE49241B32FAD078_H
#define XMLSCHEMAANYATTRIBUTE_T09BC4DE0DCA6086FA21F134EAE49241B32FAD078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAnyAttribute
struct  XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078  : public XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B
{
public:
	// System.String System.Xml.Schema.XmlSchemaAnyAttribute::ns
	String_t* ___ns_9;
	// System.Xml.Schema.XmlSchemaContentProcessing System.Xml.Schema.XmlSchemaAnyAttribute::processContents
	int32_t ___processContents_10;
	// System.Xml.Schema.NamespaceList System.Xml.Schema.XmlSchemaAnyAttribute::namespaceList
	NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C * ___namespaceList_11;

public:
	inline static int32_t get_offset_of_ns_9() { return static_cast<int32_t>(offsetof(XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078, ___ns_9)); }
	inline String_t* get_ns_9() const { return ___ns_9; }
	inline String_t** get_address_of_ns_9() { return &___ns_9; }
	inline void set_ns_9(String_t* value)
	{
		___ns_9 = value;
		Il2CppCodeGenWriteBarrier((&___ns_9), value);
	}

	inline static int32_t get_offset_of_processContents_10() { return static_cast<int32_t>(offsetof(XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078, ___processContents_10)); }
	inline int32_t get_processContents_10() const { return ___processContents_10; }
	inline int32_t* get_address_of_processContents_10() { return &___processContents_10; }
	inline void set_processContents_10(int32_t value)
	{
		___processContents_10 = value;
	}

	inline static int32_t get_offset_of_namespaceList_11() { return static_cast<int32_t>(offsetof(XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078, ___namespaceList_11)); }
	inline NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C * get_namespaceList_11() const { return ___namespaceList_11; }
	inline NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C ** get_address_of_namespaceList_11() { return &___namespaceList_11; }
	inline void set_namespaceList_11(NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C * value)
	{
		___namespaceList_11 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceList_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAANYATTRIBUTE_T09BC4DE0DCA6086FA21F134EAE49241B32FAD078_H
#ifndef XMLSCHEMAATTRIBUTE_TC31F76D28F8D593EFB409CD27FABFC32AF27E99B_H
#define XMLSCHEMAATTRIBUTE_TC31F76D28F8D593EFB409CD27FABFC32AF27E99B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAttribute
struct  XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B  : public XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B
{
public:
	// System.String System.Xml.Schema.XmlSchemaAttribute::defaultValue
	String_t* ___defaultValue_9;
	// System.String System.Xml.Schema.XmlSchemaAttribute::fixedValue
	String_t* ___fixedValue_10;
	// System.String System.Xml.Schema.XmlSchemaAttribute::name
	String_t* ___name_11;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchemaAttribute::form
	int32_t ___form_12;
	// System.Xml.Schema.XmlSchemaUse System.Xml.Schema.XmlSchemaAttribute::use
	int32_t ___use_13;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttribute::refName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___refName_14;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttribute::typeName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___typeName_15;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttribute::qualifiedName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___qualifiedName_16;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaAttribute::type
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___type_17;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaAttribute::attributeType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___attributeType_18;
	// System.Xml.Schema.SchemaAttDef System.Xml.Schema.XmlSchemaAttribute::attDef
	SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 * ___attDef_19;

public:
	inline static int32_t get_offset_of_defaultValue_9() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B, ___defaultValue_9)); }
	inline String_t* get_defaultValue_9() const { return ___defaultValue_9; }
	inline String_t** get_address_of_defaultValue_9() { return &___defaultValue_9; }
	inline void set_defaultValue_9(String_t* value)
	{
		___defaultValue_9 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValue_9), value);
	}

	inline static int32_t get_offset_of_fixedValue_10() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B, ___fixedValue_10)); }
	inline String_t* get_fixedValue_10() const { return ___fixedValue_10; }
	inline String_t** get_address_of_fixedValue_10() { return &___fixedValue_10; }
	inline void set_fixedValue_10(String_t* value)
	{
		___fixedValue_10 = value;
		Il2CppCodeGenWriteBarrier((&___fixedValue_10), value);
	}

	inline static int32_t get_offset_of_name_11() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B, ___name_11)); }
	inline String_t* get_name_11() const { return ___name_11; }
	inline String_t** get_address_of_name_11() { return &___name_11; }
	inline void set_name_11(String_t* value)
	{
		___name_11 = value;
		Il2CppCodeGenWriteBarrier((&___name_11), value);
	}

	inline static int32_t get_offset_of_form_12() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B, ___form_12)); }
	inline int32_t get_form_12() const { return ___form_12; }
	inline int32_t* get_address_of_form_12() { return &___form_12; }
	inline void set_form_12(int32_t value)
	{
		___form_12 = value;
	}

	inline static int32_t get_offset_of_use_13() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B, ___use_13)); }
	inline int32_t get_use_13() const { return ___use_13; }
	inline int32_t* get_address_of_use_13() { return &___use_13; }
	inline void set_use_13(int32_t value)
	{
		___use_13 = value;
	}

	inline static int32_t get_offset_of_refName_14() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B, ___refName_14)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_refName_14() const { return ___refName_14; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_refName_14() { return &___refName_14; }
	inline void set_refName_14(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___refName_14 = value;
		Il2CppCodeGenWriteBarrier((&___refName_14), value);
	}

	inline static int32_t get_offset_of_typeName_15() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B, ___typeName_15)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_typeName_15() const { return ___typeName_15; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_typeName_15() { return &___typeName_15; }
	inline void set_typeName_15(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___typeName_15 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_15), value);
	}

	inline static int32_t get_offset_of_qualifiedName_16() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B, ___qualifiedName_16)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_qualifiedName_16() const { return ___qualifiedName_16; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_qualifiedName_16() { return &___qualifiedName_16; }
	inline void set_qualifiedName_16(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___qualifiedName_16 = value;
		Il2CppCodeGenWriteBarrier((&___qualifiedName_16), value);
	}

	inline static int32_t get_offset_of_type_17() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B, ___type_17)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_type_17() const { return ___type_17; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_type_17() { return &___type_17; }
	inline void set_type_17(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___type_17 = value;
		Il2CppCodeGenWriteBarrier((&___type_17), value);
	}

	inline static int32_t get_offset_of_attributeType_18() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B, ___attributeType_18)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_attributeType_18() const { return ___attributeType_18; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_attributeType_18() { return &___attributeType_18; }
	inline void set_attributeType_18(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___attributeType_18 = value;
		Il2CppCodeGenWriteBarrier((&___attributeType_18), value);
	}

	inline static int32_t get_offset_of_attDef_19() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B, ___attDef_19)); }
	inline SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 * get_attDef_19() const { return ___attDef_19; }
	inline SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 ** get_address_of_attDef_19() { return &___attDef_19; }
	inline void set_attDef_19(SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 * value)
	{
		___attDef_19 = value;
		Il2CppCodeGenWriteBarrier((&___attDef_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAATTRIBUTE_TC31F76D28F8D593EFB409CD27FABFC32AF27E99B_H
#ifndef XMLSCHEMACOMPLEXCONTENT_T0C5B5B9FA891EAAD3F277E4C494956082E127CF3_H
#define XMLSCHEMACOMPLEXCONTENT_T0C5B5B9FA891EAAD3F277E4C494956082E127CF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaComplexContent
struct  XmlSchemaComplexContent_t0C5B5B9FA891EAAD3F277E4C494956082E127CF3  : public XmlSchemaContentModel_tB0209C69ADC1F671FA4ADDE88CC88C8959E84E45
{
public:
	// System.Xml.Schema.XmlSchemaContent System.Xml.Schema.XmlSchemaComplexContent::content
	XmlSchemaContent_tE0E45FE215D74AE629A316B2CDA06480C07278D3 * ___content_9;
	// System.Boolean System.Xml.Schema.XmlSchemaComplexContent::isMixed
	bool ___isMixed_10;
	// System.Boolean System.Xml.Schema.XmlSchemaComplexContent::hasMixedAttribute
	bool ___hasMixedAttribute_11;

public:
	inline static int32_t get_offset_of_content_9() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContent_t0C5B5B9FA891EAAD3F277E4C494956082E127CF3, ___content_9)); }
	inline XmlSchemaContent_tE0E45FE215D74AE629A316B2CDA06480C07278D3 * get_content_9() const { return ___content_9; }
	inline XmlSchemaContent_tE0E45FE215D74AE629A316B2CDA06480C07278D3 ** get_address_of_content_9() { return &___content_9; }
	inline void set_content_9(XmlSchemaContent_tE0E45FE215D74AE629A316B2CDA06480C07278D3 * value)
	{
		___content_9 = value;
		Il2CppCodeGenWriteBarrier((&___content_9), value);
	}

	inline static int32_t get_offset_of_isMixed_10() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContent_t0C5B5B9FA891EAAD3F277E4C494956082E127CF3, ___isMixed_10)); }
	inline bool get_isMixed_10() const { return ___isMixed_10; }
	inline bool* get_address_of_isMixed_10() { return &___isMixed_10; }
	inline void set_isMixed_10(bool value)
	{
		___isMixed_10 = value;
	}

	inline static int32_t get_offset_of_hasMixedAttribute_11() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContent_t0C5B5B9FA891EAAD3F277E4C494956082E127CF3, ___hasMixedAttribute_11)); }
	inline bool get_hasMixedAttribute_11() const { return ___hasMixedAttribute_11; }
	inline bool* get_address_of_hasMixedAttribute_11() { return &___hasMixedAttribute_11; }
	inline void set_hasMixedAttribute_11(bool value)
	{
		___hasMixedAttribute_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPLEXCONTENT_T0C5B5B9FA891EAAD3F277E4C494956082E127CF3_H
#ifndef XMLSCHEMACOMPLEXCONTENTEXTENSION_T3B6C614515825C627D8DE0D040CD97753829004B_H
#define XMLSCHEMACOMPLEXCONTENTEXTENSION_T3B6C614515825C627D8DE0D040CD97753829004B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaComplexContentExtension
struct  XmlSchemaComplexContentExtension_t3B6C614515825C627D8DE0D040CD97753829004B  : public XmlSchemaContent_tE0E45FE215D74AE629A316B2CDA06480C07278D3
{
public:
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexContentExtension::particle
	XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * ___particle_9;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaComplexContentExtension::attributes
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___attributes_10;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaComplexContentExtension::anyAttribute
	XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * ___anyAttribute_11;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaComplexContentExtension::baseTypeName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___baseTypeName_12;

public:
	inline static int32_t get_offset_of_particle_9() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentExtension_t3B6C614515825C627D8DE0D040CD97753829004B, ___particle_9)); }
	inline XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * get_particle_9() const { return ___particle_9; }
	inline XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 ** get_address_of_particle_9() { return &___particle_9; }
	inline void set_particle_9(XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * value)
	{
		___particle_9 = value;
		Il2CppCodeGenWriteBarrier((&___particle_9), value);
	}

	inline static int32_t get_offset_of_attributes_10() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentExtension_t3B6C614515825C627D8DE0D040CD97753829004B, ___attributes_10)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_attributes_10() const { return ___attributes_10; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_attributes_10() { return &___attributes_10; }
	inline void set_attributes_10(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___attributes_10 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_10), value);
	}

	inline static int32_t get_offset_of_anyAttribute_11() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentExtension_t3B6C614515825C627D8DE0D040CD97753829004B, ___anyAttribute_11)); }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * get_anyAttribute_11() const { return ___anyAttribute_11; }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 ** get_address_of_anyAttribute_11() { return &___anyAttribute_11; }
	inline void set_anyAttribute_11(XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * value)
	{
		___anyAttribute_11 = value;
		Il2CppCodeGenWriteBarrier((&___anyAttribute_11), value);
	}

	inline static int32_t get_offset_of_baseTypeName_12() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentExtension_t3B6C614515825C627D8DE0D040CD97753829004B, ___baseTypeName_12)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_baseTypeName_12() const { return ___baseTypeName_12; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_baseTypeName_12() { return &___baseTypeName_12; }
	inline void set_baseTypeName_12(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___baseTypeName_12 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPLEXCONTENTEXTENSION_T3B6C614515825C627D8DE0D040CD97753829004B_H
#ifndef XMLSCHEMACOMPLEXCONTENTRESTRICTION_T05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0_H
#define XMLSCHEMACOMPLEXCONTENTRESTRICTION_T05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaComplexContentRestriction
struct  XmlSchemaComplexContentRestriction_t05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0  : public XmlSchemaContent_tE0E45FE215D74AE629A316B2CDA06480C07278D3
{
public:
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexContentRestriction::particle
	XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * ___particle_9;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaComplexContentRestriction::attributes
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___attributes_10;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaComplexContentRestriction::anyAttribute
	XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * ___anyAttribute_11;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaComplexContentRestriction::baseTypeName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___baseTypeName_12;

public:
	inline static int32_t get_offset_of_particle_9() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_t05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0, ___particle_9)); }
	inline XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * get_particle_9() const { return ___particle_9; }
	inline XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 ** get_address_of_particle_9() { return &___particle_9; }
	inline void set_particle_9(XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * value)
	{
		___particle_9 = value;
		Il2CppCodeGenWriteBarrier((&___particle_9), value);
	}

	inline static int32_t get_offset_of_attributes_10() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_t05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0, ___attributes_10)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_attributes_10() const { return ___attributes_10; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_attributes_10() { return &___attributes_10; }
	inline void set_attributes_10(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___attributes_10 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_10), value);
	}

	inline static int32_t get_offset_of_anyAttribute_11() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_t05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0, ___anyAttribute_11)); }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * get_anyAttribute_11() const { return ___anyAttribute_11; }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 ** get_address_of_anyAttribute_11() { return &___anyAttribute_11; }
	inline void set_anyAttribute_11(XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * value)
	{
		___anyAttribute_11 = value;
		Il2CppCodeGenWriteBarrier((&___anyAttribute_11), value);
	}

	inline static int32_t get_offset_of_baseTypeName_12() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_t05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0, ___baseTypeName_12)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_baseTypeName_12() const { return ___baseTypeName_12; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_baseTypeName_12() { return &___baseTypeName_12; }
	inline void set_baseTypeName_12(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___baseTypeName_12 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPLEXCONTENTRESTRICTION_T05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0_H
#ifndef XMLSCHEMAEXTERNAL_TE975A15EAF7FEB0CD500DB274FF6FD106DC517B8_H
#define XMLSCHEMAEXTERNAL_TE975A15EAF7FEB0CD500DB274FF6FD106DC517B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaExternal
struct  XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8  : public XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B
{
public:
	// System.String System.Xml.Schema.XmlSchemaExternal::location
	String_t* ___location_6;
	// System.Uri System.Xml.Schema.XmlSchemaExternal::baseUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___baseUri_7;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaExternal::schema
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___schema_8;
	// System.String System.Xml.Schema.XmlSchemaExternal::id
	String_t* ___id_9;
	// System.Xml.XmlAttribute[] System.Xml.Schema.XmlSchemaExternal::moreAttributes
	XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908* ___moreAttributes_10;
	// System.Xml.Schema.Compositor System.Xml.Schema.XmlSchemaExternal::compositor
	int32_t ___compositor_11;

public:
	inline static int32_t get_offset_of_location_6() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8, ___location_6)); }
	inline String_t* get_location_6() const { return ___location_6; }
	inline String_t** get_address_of_location_6() { return &___location_6; }
	inline void set_location_6(String_t* value)
	{
		___location_6 = value;
		Il2CppCodeGenWriteBarrier((&___location_6), value);
	}

	inline static int32_t get_offset_of_baseUri_7() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8, ___baseUri_7)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_baseUri_7() const { return ___baseUri_7; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_baseUri_7() { return &___baseUri_7; }
	inline void set_baseUri_7(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___baseUri_7 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_7), value);
	}

	inline static int32_t get_offset_of_schema_8() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8, ___schema_8)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_schema_8() const { return ___schema_8; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_schema_8() { return &___schema_8; }
	inline void set_schema_8(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___schema_8 = value;
		Il2CppCodeGenWriteBarrier((&___schema_8), value);
	}

	inline static int32_t get_offset_of_id_9() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8, ___id_9)); }
	inline String_t* get_id_9() const { return ___id_9; }
	inline String_t** get_address_of_id_9() { return &___id_9; }
	inline void set_id_9(String_t* value)
	{
		___id_9 = value;
		Il2CppCodeGenWriteBarrier((&___id_9), value);
	}

	inline static int32_t get_offset_of_moreAttributes_10() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8, ___moreAttributes_10)); }
	inline XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908* get_moreAttributes_10() const { return ___moreAttributes_10; }
	inline XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908** get_address_of_moreAttributes_10() { return &___moreAttributes_10; }
	inline void set_moreAttributes_10(XmlAttributeU5BU5D_t02E3E190564D2A07467AFE803FAB6C1CE82F7908* value)
	{
		___moreAttributes_10 = value;
		Il2CppCodeGenWriteBarrier((&___moreAttributes_10), value);
	}

	inline static int32_t get_offset_of_compositor_11() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8, ___compositor_11)); }
	inline int32_t get_compositor_11() const { return ___compositor_11; }
	inline int32_t* get_address_of_compositor_11() { return &___compositor_11; }
	inline void set_compositor_11(int32_t value)
	{
		___compositor_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAEXTERNAL_TE975A15EAF7FEB0CD500DB274FF6FD106DC517B8_H
#ifndef XMLSCHEMAFACET_TB6316660F75E032055B12043237A0132A1919250_H
#define XMLSCHEMAFACET_TB6316660F75E032055B12043237A0132A1919250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFacet
struct  XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250  : public XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B
{
public:
	// System.String System.Xml.Schema.XmlSchemaFacet::value
	String_t* ___value_9;
	// System.Boolean System.Xml.Schema.XmlSchemaFacet::isFixed
	bool ___isFixed_10;
	// System.Xml.Schema.FacetType System.Xml.Schema.XmlSchemaFacet::facetType
	int32_t ___facetType_11;

public:
	inline static int32_t get_offset_of_value_9() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250, ___value_9)); }
	inline String_t* get_value_9() const { return ___value_9; }
	inline String_t** get_address_of_value_9() { return &___value_9; }
	inline void set_value_9(String_t* value)
	{
		___value_9 = value;
		Il2CppCodeGenWriteBarrier((&___value_9), value);
	}

	inline static int32_t get_offset_of_isFixed_10() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250, ___isFixed_10)); }
	inline bool get_isFixed_10() const { return ___isFixed_10; }
	inline bool* get_address_of_isFixed_10() { return &___isFixed_10; }
	inline void set_isFixed_10(bool value)
	{
		___isFixed_10 = value;
	}

	inline static int32_t get_offset_of_facetType_11() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250, ___facetType_11)); }
	inline int32_t get_facetType_11() const { return ___facetType_11; }
	inline int32_t* get_address_of_facetType_11() { return &___facetType_11; }
	inline void set_facetType_11(int32_t value)
	{
		___facetType_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFACET_TB6316660F75E032055B12043237A0132A1919250_H
#ifndef XMLSCHEMAINFO_TEC7C8E9E6FC5CF178F83789C9B3336C5A9145035_H
#define XMLSCHEMAINFO_TEC7C8E9E6FC5CF178F83789C9B3336C5A9145035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaInfo
struct  XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaInfo::isDefault
	bool ___isDefault_0;
	// System.Boolean System.Xml.Schema.XmlSchemaInfo::isNil
	bool ___isNil_1;
	// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.XmlSchemaInfo::schemaElement
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D * ___schemaElement_2;
	// System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.XmlSchemaInfo::schemaAttribute
	XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B * ___schemaAttribute_3;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaInfo::schemaType
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * ___schemaType_4;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaInfo::memberType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___memberType_5;
	// System.Xml.Schema.XmlSchemaValidity System.Xml.Schema.XmlSchemaInfo::validity
	int32_t ___validity_6;
	// System.Xml.Schema.XmlSchemaContentType System.Xml.Schema.XmlSchemaInfo::contentType
	int32_t ___contentType_7;

public:
	inline static int32_t get_offset_of_isDefault_0() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035, ___isDefault_0)); }
	inline bool get_isDefault_0() const { return ___isDefault_0; }
	inline bool* get_address_of_isDefault_0() { return &___isDefault_0; }
	inline void set_isDefault_0(bool value)
	{
		___isDefault_0 = value;
	}

	inline static int32_t get_offset_of_isNil_1() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035, ___isNil_1)); }
	inline bool get_isNil_1() const { return ___isNil_1; }
	inline bool* get_address_of_isNil_1() { return &___isNil_1; }
	inline void set_isNil_1(bool value)
	{
		___isNil_1 = value;
	}

	inline static int32_t get_offset_of_schemaElement_2() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035, ___schemaElement_2)); }
	inline XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D * get_schemaElement_2() const { return ___schemaElement_2; }
	inline XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D ** get_address_of_schemaElement_2() { return &___schemaElement_2; }
	inline void set_schemaElement_2(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D * value)
	{
		___schemaElement_2 = value;
		Il2CppCodeGenWriteBarrier((&___schemaElement_2), value);
	}

	inline static int32_t get_offset_of_schemaAttribute_3() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035, ___schemaAttribute_3)); }
	inline XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B * get_schemaAttribute_3() const { return ___schemaAttribute_3; }
	inline XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B ** get_address_of_schemaAttribute_3() { return &___schemaAttribute_3; }
	inline void set_schemaAttribute_3(XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B * value)
	{
		___schemaAttribute_3 = value;
		Il2CppCodeGenWriteBarrier((&___schemaAttribute_3), value);
	}

	inline static int32_t get_offset_of_schemaType_4() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035, ___schemaType_4)); }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * get_schemaType_4() const { return ___schemaType_4; }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 ** get_address_of_schemaType_4() { return &___schemaType_4; }
	inline void set_schemaType_4(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * value)
	{
		___schemaType_4 = value;
		Il2CppCodeGenWriteBarrier((&___schemaType_4), value);
	}

	inline static int32_t get_offset_of_memberType_5() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035, ___memberType_5)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_memberType_5() const { return ___memberType_5; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_memberType_5() { return &___memberType_5; }
	inline void set_memberType_5(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___memberType_5 = value;
		Il2CppCodeGenWriteBarrier((&___memberType_5), value);
	}

	inline static int32_t get_offset_of_validity_6() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035, ___validity_6)); }
	inline int32_t get_validity_6() const { return ___validity_6; }
	inline int32_t* get_address_of_validity_6() { return &___validity_6; }
	inline void set_validity_6(int32_t value)
	{
		___validity_6 = value;
	}

	inline static int32_t get_offset_of_contentType_7() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035, ___contentType_7)); }
	inline int32_t get_contentType_7() const { return ___contentType_7; }
	inline int32_t* get_address_of_contentType_7() { return &___contentType_7; }
	inline void set_contentType_7(int32_t value)
	{
		___contentType_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAINFO_TEC7C8E9E6FC5CF178F83789C9B3336C5A9145035_H
#ifndef XMLSCHEMAKEY_TF6EC3BA3F09B23BE769F5B5EB815B71F2A6AD672_H
#define XMLSCHEMAKEY_TF6EC3BA3F09B23BE769F5B5EB815B71F2A6AD672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaKey
struct  XmlSchemaKey_tF6EC3BA3F09B23BE769F5B5EB815B71F2A6AD672  : public XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAKEY_TF6EC3BA3F09B23BE769F5B5EB815B71F2A6AD672_H
#ifndef XMLSCHEMAKEYREF_TA6761A34F81934FE376DCA84EDC7094325D0FB82_H
#define XMLSCHEMAKEYREF_TA6761A34F81934FE376DCA84EDC7094325D0FB82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaKeyref
struct  XmlSchemaKeyref_tA6761A34F81934FE376DCA84EDC7094325D0FB82  : public XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaKeyref::refer
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___refer_14;

public:
	inline static int32_t get_offset_of_refer_14() { return static_cast<int32_t>(offsetof(XmlSchemaKeyref_tA6761A34F81934FE376DCA84EDC7094325D0FB82, ___refer_14)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_refer_14() const { return ___refer_14; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_refer_14() { return &___refer_14; }
	inline void set_refer_14(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___refer_14 = value;
		Il2CppCodeGenWriteBarrier((&___refer_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAKEYREF_TA6761A34F81934FE376DCA84EDC7094325D0FB82_H
#ifndef XSOENUMERATOR_T0BDA5BB56D78F4B2F028B2831622DC3336011D8E_H
#define XSOENUMERATOR_T0BDA5BB56D78F4B2F028B2831622DC3336011D8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectTable/XSOEnumerator
struct  XSOEnumerator_t0BDA5BB56D78F4B2F028B2831622DC3336011D8E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry> System.Xml.Schema.XmlSchemaObjectTable/XSOEnumerator::entries
	List_1_t7C6AA94EA9520975CB29B2B4CA6C9EAED364ECD3 * ___entries_0;
	// System.Xml.Schema.XmlSchemaObjectTable/EnumeratorType System.Xml.Schema.XmlSchemaObjectTable/XSOEnumerator::enumType
	int32_t ___enumType_1;
	// System.Int32 System.Xml.Schema.XmlSchemaObjectTable/XSOEnumerator::currentIndex
	int32_t ___currentIndex_2;
	// System.Int32 System.Xml.Schema.XmlSchemaObjectTable/XSOEnumerator::size
	int32_t ___size_3;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaObjectTable/XSOEnumerator::currentKey
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___currentKey_4;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObjectTable/XSOEnumerator::currentValue
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * ___currentValue_5;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(XSOEnumerator_t0BDA5BB56D78F4B2F028B2831622DC3336011D8E, ___entries_0)); }
	inline List_1_t7C6AA94EA9520975CB29B2B4CA6C9EAED364ECD3 * get_entries_0() const { return ___entries_0; }
	inline List_1_t7C6AA94EA9520975CB29B2B4CA6C9EAED364ECD3 ** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(List_1_t7C6AA94EA9520975CB29B2B4CA6C9EAED364ECD3 * value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier((&___entries_0), value);
	}

	inline static int32_t get_offset_of_enumType_1() { return static_cast<int32_t>(offsetof(XSOEnumerator_t0BDA5BB56D78F4B2F028B2831622DC3336011D8E, ___enumType_1)); }
	inline int32_t get_enumType_1() const { return ___enumType_1; }
	inline int32_t* get_address_of_enumType_1() { return &___enumType_1; }
	inline void set_enumType_1(int32_t value)
	{
		___enumType_1 = value;
	}

	inline static int32_t get_offset_of_currentIndex_2() { return static_cast<int32_t>(offsetof(XSOEnumerator_t0BDA5BB56D78F4B2F028B2831622DC3336011D8E, ___currentIndex_2)); }
	inline int32_t get_currentIndex_2() const { return ___currentIndex_2; }
	inline int32_t* get_address_of_currentIndex_2() { return &___currentIndex_2; }
	inline void set_currentIndex_2(int32_t value)
	{
		___currentIndex_2 = value;
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(XSOEnumerator_t0BDA5BB56D78F4B2F028B2831622DC3336011D8E, ___size_3)); }
	inline int32_t get_size_3() const { return ___size_3; }
	inline int32_t* get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(int32_t value)
	{
		___size_3 = value;
	}

	inline static int32_t get_offset_of_currentKey_4() { return static_cast<int32_t>(offsetof(XSOEnumerator_t0BDA5BB56D78F4B2F028B2831622DC3336011D8E, ___currentKey_4)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_currentKey_4() const { return ___currentKey_4; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_currentKey_4() { return &___currentKey_4; }
	inline void set_currentKey_4(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___currentKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentKey_4), value);
	}

	inline static int32_t get_offset_of_currentValue_5() { return static_cast<int32_t>(offsetof(XSOEnumerator_t0BDA5BB56D78F4B2F028B2831622DC3336011D8E, ___currentValue_5)); }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * get_currentValue_5() const { return ___currentValue_5; }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B ** get_address_of_currentValue_5() { return &___currentValue_5; }
	inline void set_currentValue_5(XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * value)
	{
		___currentValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentValue_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSOENUMERATOR_T0BDA5BB56D78F4B2F028B2831622DC3336011D8E_H
#ifndef XMLSCHEMAPARTICLE_TB790C2363C7041009BE94F64F031BDDA1E389671_H
#define XMLSCHEMAPARTICLE_TB790C2363C7041009BE94F64F031BDDA1E389671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaParticle
struct  XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671  : public XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B
{
public:
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::minOccurs
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___minOccurs_9;
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::maxOccurs
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___maxOccurs_10;
	// System.Xml.Schema.XmlSchemaParticle/Occurs System.Xml.Schema.XmlSchemaParticle::flags
	int32_t ___flags_11;

public:
	inline static int32_t get_offset_of_minOccurs_9() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671, ___minOccurs_9)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_minOccurs_9() const { return ___minOccurs_9; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_minOccurs_9() { return &___minOccurs_9; }
	inline void set_minOccurs_9(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___minOccurs_9 = value;
	}

	inline static int32_t get_offset_of_maxOccurs_10() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671, ___maxOccurs_10)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_maxOccurs_10() const { return ___maxOccurs_10; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_maxOccurs_10() { return &___maxOccurs_10; }
	inline void set_maxOccurs_10(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___maxOccurs_10 = value;
	}

	inline static int32_t get_offset_of_flags_11() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671, ___flags_11)); }
	inline int32_t get_flags_11() const { return ___flags_11; }
	inline int32_t* get_address_of_flags_11() { return &___flags_11; }
	inline void set_flags_11(int32_t value)
	{
		___flags_11 = value;
	}
};

struct XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaParticle::Empty
	XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * ___Empty_12;

public:
	inline static int32_t get_offset_of_Empty_12() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671_StaticFields, ___Empty_12)); }
	inline XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * get_Empty_12() const { return ___Empty_12; }
	inline XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 ** get_address_of_Empty_12() { return &___Empty_12; }
	inline void set_Empty_12(XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * value)
	{
		___Empty_12 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAPARTICLE_TB790C2363C7041009BE94F64F031BDDA1E389671_H
#ifndef XMLSCHEMASIMPLECONTENT_T47CF5CDB950EC1B3CB066BA2F9E3DAE993AD3550_H
#define XMLSCHEMASIMPLECONTENT_T47CF5CDB950EC1B3CB066BA2F9E3DAE993AD3550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleContent
struct  XmlSchemaSimpleContent_t47CF5CDB950EC1B3CB066BA2F9E3DAE993AD3550  : public XmlSchemaContentModel_tB0209C69ADC1F671FA4ADDE88CC88C8959E84E45
{
public:
	// System.Xml.Schema.XmlSchemaContent System.Xml.Schema.XmlSchemaSimpleContent::content
	XmlSchemaContent_tE0E45FE215D74AE629A316B2CDA06480C07278D3 * ___content_9;

public:
	inline static int32_t get_offset_of_content_9() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContent_t47CF5CDB950EC1B3CB066BA2F9E3DAE993AD3550, ___content_9)); }
	inline XmlSchemaContent_tE0E45FE215D74AE629A316B2CDA06480C07278D3 * get_content_9() const { return ___content_9; }
	inline XmlSchemaContent_tE0E45FE215D74AE629A316B2CDA06480C07278D3 ** get_address_of_content_9() { return &___content_9; }
	inline void set_content_9(XmlSchemaContent_tE0E45FE215D74AE629A316B2CDA06480C07278D3 * value)
	{
		___content_9 = value;
		Il2CppCodeGenWriteBarrier((&___content_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLECONTENT_T47CF5CDB950EC1B3CB066BA2F9E3DAE993AD3550_H
#ifndef XMLSCHEMASIMPLECONTENTEXTENSION_T235FC077FDE8C8796FA6A961658C42D60F4CB812_H
#define XMLSCHEMASIMPLECONTENTEXTENSION_T235FC077FDE8C8796FA6A961658C42D60F4CB812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleContentExtension
struct  XmlSchemaSimpleContentExtension_t235FC077FDE8C8796FA6A961658C42D60F4CB812  : public XmlSchemaContent_tE0E45FE215D74AE629A316B2CDA06480C07278D3
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleContentExtension::attributes
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___attributes_9;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaSimpleContentExtension::anyAttribute
	XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * ___anyAttribute_10;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleContentExtension::baseTypeName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___baseTypeName_11;

public:
	inline static int32_t get_offset_of_attributes_9() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentExtension_t235FC077FDE8C8796FA6A961658C42D60F4CB812, ___attributes_9)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_attributes_9() const { return ___attributes_9; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_attributes_9() { return &___attributes_9; }
	inline void set_attributes_9(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___attributes_9 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_9), value);
	}

	inline static int32_t get_offset_of_anyAttribute_10() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentExtension_t235FC077FDE8C8796FA6A961658C42D60F4CB812, ___anyAttribute_10)); }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * get_anyAttribute_10() const { return ___anyAttribute_10; }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 ** get_address_of_anyAttribute_10() { return &___anyAttribute_10; }
	inline void set_anyAttribute_10(XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * value)
	{
		___anyAttribute_10 = value;
		Il2CppCodeGenWriteBarrier((&___anyAttribute_10), value);
	}

	inline static int32_t get_offset_of_baseTypeName_11() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentExtension_t235FC077FDE8C8796FA6A961658C42D60F4CB812, ___baseTypeName_11)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_baseTypeName_11() const { return ___baseTypeName_11; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_baseTypeName_11() { return &___baseTypeName_11; }
	inline void set_baseTypeName_11(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___baseTypeName_11 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLECONTENTEXTENSION_T235FC077FDE8C8796FA6A961658C42D60F4CB812_H
#ifndef XMLSCHEMASIMPLECONTENTRESTRICTION_T6B9375A26517CBAA49F980C44A3A378A36087621_H
#define XMLSCHEMASIMPLECONTENTRESTRICTION_T6B9375A26517CBAA49F980C44A3A378A36087621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleContentRestriction
struct  XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621  : public XmlSchemaContent_tE0E45FE215D74AE629A316B2CDA06480C07278D3
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleContentRestriction::baseTypeName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___baseTypeName_9;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleContentRestriction::baseType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___baseType_10;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleContentRestriction::facets
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___facets_11;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleContentRestriction::attributes
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___attributes_12;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaSimpleContentRestriction::anyAttribute
	XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * ___anyAttribute_13;

public:
	inline static int32_t get_offset_of_baseTypeName_9() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621, ___baseTypeName_9)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_baseTypeName_9() const { return ___baseTypeName_9; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_baseTypeName_9() { return &___baseTypeName_9; }
	inline void set_baseTypeName_9(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___baseTypeName_9 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_9), value);
	}

	inline static int32_t get_offset_of_baseType_10() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621, ___baseType_10)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_baseType_10() const { return ___baseType_10; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_baseType_10() { return &___baseType_10; }
	inline void set_baseType_10(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___baseType_10 = value;
		Il2CppCodeGenWriteBarrier((&___baseType_10), value);
	}

	inline static int32_t get_offset_of_facets_11() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621, ___facets_11)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_facets_11() const { return ___facets_11; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_facets_11() { return &___facets_11; }
	inline void set_facets_11(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___facets_11 = value;
		Il2CppCodeGenWriteBarrier((&___facets_11), value);
	}

	inline static int32_t get_offset_of_attributes_12() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621, ___attributes_12)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_attributes_12() const { return ___attributes_12; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_attributes_12() { return &___attributes_12; }
	inline void set_attributes_12(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___attributes_12 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_12), value);
	}

	inline static int32_t get_offset_of_anyAttribute_13() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621, ___anyAttribute_13)); }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * get_anyAttribute_13() const { return ___anyAttribute_13; }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 ** get_address_of_anyAttribute_13() { return &___anyAttribute_13; }
	inline void set_anyAttribute_13(XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * value)
	{
		___anyAttribute_13 = value;
		Il2CppCodeGenWriteBarrier((&___anyAttribute_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLECONTENTRESTRICTION_T6B9375A26517CBAA49F980C44A3A378A36087621_H
#ifndef XMLSCHEMASIMPLETYPELIST_TE5804144CB4035DE27AA3C43233579A075469403_H
#define XMLSCHEMASIMPLETYPELIST_TE5804144CB4035DE27AA3C43233579A075469403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeList
struct  XmlSchemaSimpleTypeList_tE5804144CB4035DE27AA3C43233579A075469403  : public XmlSchemaSimpleTypeContent_t87EF2A2F4B2F3D7827EA18B0FCB8393EA0201A3E
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleTypeList::itemTypeName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___itemTypeName_9;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleTypeList::itemType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___itemType_10;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleTypeList::baseItemType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___baseItemType_11;

public:
	inline static int32_t get_offset_of_itemTypeName_9() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_tE5804144CB4035DE27AA3C43233579A075469403, ___itemTypeName_9)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_itemTypeName_9() const { return ___itemTypeName_9; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_itemTypeName_9() { return &___itemTypeName_9; }
	inline void set_itemTypeName_9(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___itemTypeName_9 = value;
		Il2CppCodeGenWriteBarrier((&___itemTypeName_9), value);
	}

	inline static int32_t get_offset_of_itemType_10() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_tE5804144CB4035DE27AA3C43233579A075469403, ___itemType_10)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_itemType_10() const { return ___itemType_10; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_itemType_10() { return &___itemType_10; }
	inline void set_itemType_10(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___itemType_10 = value;
		Il2CppCodeGenWriteBarrier((&___itemType_10), value);
	}

	inline static int32_t get_offset_of_baseItemType_11() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_tE5804144CB4035DE27AA3C43233579A075469403, ___baseItemType_11)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_baseItemType_11() const { return ___baseItemType_11; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_baseItemType_11() { return &___baseItemType_11; }
	inline void set_baseItemType_11(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___baseItemType_11 = value;
		Il2CppCodeGenWriteBarrier((&___baseItemType_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPELIST_TE5804144CB4035DE27AA3C43233579A075469403_H
#ifndef XMLSCHEMASIMPLETYPERESTRICTION_T948C7680F4D262DA9DA7590CCA8E2F96078E490C_H
#define XMLSCHEMASIMPLETYPERESTRICTION_T948C7680F4D262DA9DA7590CCA8E2F96078E490C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeRestriction
struct  XmlSchemaSimpleTypeRestriction_t948C7680F4D262DA9DA7590CCA8E2F96078E490C  : public XmlSchemaSimpleTypeContent_t87EF2A2F4B2F3D7827EA18B0FCB8393EA0201A3E
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleTypeRestriction::baseTypeName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___baseTypeName_9;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleTypeRestriction::baseType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___baseType_10;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleTypeRestriction::facets
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___facets_11;

public:
	inline static int32_t get_offset_of_baseTypeName_9() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t948C7680F4D262DA9DA7590CCA8E2F96078E490C, ___baseTypeName_9)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_baseTypeName_9() const { return ___baseTypeName_9; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_baseTypeName_9() { return &___baseTypeName_9; }
	inline void set_baseTypeName_9(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___baseTypeName_9 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_9), value);
	}

	inline static int32_t get_offset_of_baseType_10() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t948C7680F4D262DA9DA7590CCA8E2F96078E490C, ___baseType_10)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_baseType_10() const { return ___baseType_10; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_baseType_10() { return &___baseType_10; }
	inline void set_baseType_10(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___baseType_10 = value;
		Il2CppCodeGenWriteBarrier((&___baseType_10), value);
	}

	inline static int32_t get_offset_of_facets_11() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t948C7680F4D262DA9DA7590CCA8E2F96078E490C, ___facets_11)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_facets_11() const { return ___facets_11; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_facets_11() { return &___facets_11; }
	inline void set_facets_11(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___facets_11 = value;
		Il2CppCodeGenWriteBarrier((&___facets_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPERESTRICTION_T948C7680F4D262DA9DA7590CCA8E2F96078E490C_H
#ifndef XMLSCHEMASIMPLETYPEUNION_T3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A_H
#define XMLSCHEMASIMPLETYPEUNION_T3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeUnion
struct  XmlSchemaSimpleTypeUnion_t3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A  : public XmlSchemaSimpleTypeContent_t87EF2A2F4B2F3D7827EA18B0FCB8393EA0201A3E
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleTypeUnion::baseTypes
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___baseTypes_9;
	// System.Xml.XmlQualifiedName[] System.Xml.Schema.XmlSchemaSimpleTypeUnion::memberTypes
	XmlQualifiedNameU5BU5D_t375EB02A213AC919E02336B57A412BB761AFA18B* ___memberTypes_10;
	// System.Xml.Schema.XmlSchemaSimpleType[] System.Xml.Schema.XmlSchemaSimpleTypeUnion::baseMemberTypes
	XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B* ___baseMemberTypes_11;

public:
	inline static int32_t get_offset_of_baseTypes_9() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeUnion_t3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A, ___baseTypes_9)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_baseTypes_9() const { return ___baseTypes_9; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_baseTypes_9() { return &___baseTypes_9; }
	inline void set_baseTypes_9(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___baseTypes_9 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypes_9), value);
	}

	inline static int32_t get_offset_of_memberTypes_10() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeUnion_t3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A, ___memberTypes_10)); }
	inline XmlQualifiedNameU5BU5D_t375EB02A213AC919E02336B57A412BB761AFA18B* get_memberTypes_10() const { return ___memberTypes_10; }
	inline XmlQualifiedNameU5BU5D_t375EB02A213AC919E02336B57A412BB761AFA18B** get_address_of_memberTypes_10() { return &___memberTypes_10; }
	inline void set_memberTypes_10(XmlQualifiedNameU5BU5D_t375EB02A213AC919E02336B57A412BB761AFA18B* value)
	{
		___memberTypes_10 = value;
		Il2CppCodeGenWriteBarrier((&___memberTypes_10), value);
	}

	inline static int32_t get_offset_of_baseMemberTypes_11() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeUnion_t3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A, ___baseMemberTypes_11)); }
	inline XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B* get_baseMemberTypes_11() const { return ___baseMemberTypes_11; }
	inline XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B** get_address_of_baseMemberTypes_11() { return &___baseMemberTypes_11; }
	inline void set_baseMemberTypes_11(XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B* value)
	{
		___baseMemberTypes_11 = value;
		Il2CppCodeGenWriteBarrier((&___baseMemberTypes_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPEUNION_T3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A_H
#ifndef XMLSCHEMATYPE_TE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9_H
#define XMLSCHEMATYPE_TE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaType
struct  XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9  : public XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B
{
public:
	// System.String System.Xml.Schema.XmlSchemaType::name
	String_t* ___name_9;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::final
	int32_t ___final_10;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::derivedBy
	int32_t ___derivedBy_11;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaType::baseSchemaType
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * ___baseSchemaType_12;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaType::datatype
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___datatype_13;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::finalResolved
	int32_t ___finalResolved_14;
	// System.Xml.Schema.SchemaElementDecl modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.Schema.XmlSchemaType::elementDecl
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * ___elementDecl_15;
	// System.Xml.XmlQualifiedName modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.Schema.XmlSchemaType::qname
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___qname_16;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaType::redefined
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * ___redefined_17;
	// System.Xml.Schema.XmlSchemaContentType System.Xml.Schema.XmlSchemaType::contentType
	int32_t ___contentType_18;

public:
	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((&___name_9), value);
	}

	inline static int32_t get_offset_of_final_10() { return static_cast<int32_t>(offsetof(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9, ___final_10)); }
	inline int32_t get_final_10() const { return ___final_10; }
	inline int32_t* get_address_of_final_10() { return &___final_10; }
	inline void set_final_10(int32_t value)
	{
		___final_10 = value;
	}

	inline static int32_t get_offset_of_derivedBy_11() { return static_cast<int32_t>(offsetof(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9, ___derivedBy_11)); }
	inline int32_t get_derivedBy_11() const { return ___derivedBy_11; }
	inline int32_t* get_address_of_derivedBy_11() { return &___derivedBy_11; }
	inline void set_derivedBy_11(int32_t value)
	{
		___derivedBy_11 = value;
	}

	inline static int32_t get_offset_of_baseSchemaType_12() { return static_cast<int32_t>(offsetof(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9, ___baseSchemaType_12)); }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * get_baseSchemaType_12() const { return ___baseSchemaType_12; }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 ** get_address_of_baseSchemaType_12() { return &___baseSchemaType_12; }
	inline void set_baseSchemaType_12(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * value)
	{
		___baseSchemaType_12 = value;
		Il2CppCodeGenWriteBarrier((&___baseSchemaType_12), value);
	}

	inline static int32_t get_offset_of_datatype_13() { return static_cast<int32_t>(offsetof(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9, ___datatype_13)); }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * get_datatype_13() const { return ___datatype_13; }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 ** get_address_of_datatype_13() { return &___datatype_13; }
	inline void set_datatype_13(XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * value)
	{
		___datatype_13 = value;
		Il2CppCodeGenWriteBarrier((&___datatype_13), value);
	}

	inline static int32_t get_offset_of_finalResolved_14() { return static_cast<int32_t>(offsetof(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9, ___finalResolved_14)); }
	inline int32_t get_finalResolved_14() const { return ___finalResolved_14; }
	inline int32_t* get_address_of_finalResolved_14() { return &___finalResolved_14; }
	inline void set_finalResolved_14(int32_t value)
	{
		___finalResolved_14 = value;
	}

	inline static int32_t get_offset_of_elementDecl_15() { return static_cast<int32_t>(offsetof(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9, ___elementDecl_15)); }
	inline SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * get_elementDecl_15() const { return ___elementDecl_15; }
	inline SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 ** get_address_of_elementDecl_15() { return &___elementDecl_15; }
	inline void set_elementDecl_15(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * value)
	{
		___elementDecl_15 = value;
		Il2CppCodeGenWriteBarrier((&___elementDecl_15), value);
	}

	inline static int32_t get_offset_of_qname_16() { return static_cast<int32_t>(offsetof(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9, ___qname_16)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_qname_16() const { return ___qname_16; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_qname_16() { return &___qname_16; }
	inline void set_qname_16(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___qname_16 = value;
		Il2CppCodeGenWriteBarrier((&___qname_16), value);
	}

	inline static int32_t get_offset_of_redefined_17() { return static_cast<int32_t>(offsetof(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9, ___redefined_17)); }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * get_redefined_17() const { return ___redefined_17; }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 ** get_address_of_redefined_17() { return &___redefined_17; }
	inline void set_redefined_17(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * value)
	{
		___redefined_17 = value;
		Il2CppCodeGenWriteBarrier((&___redefined_17), value);
	}

	inline static int32_t get_offset_of_contentType_18() { return static_cast<int32_t>(offsetof(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9, ___contentType_18)); }
	inline int32_t get_contentType_18() const { return ___contentType_18; }
	inline int32_t* get_address_of_contentType_18() { return &___contentType_18; }
	inline void set_contentType_18(int32_t value)
	{
		___contentType_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMATYPE_TE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9_H
#ifndef XMLSCHEMAUNIQUE_T23A00CD203938B26F6C292A874981930DD989609_H
#define XMLSCHEMAUNIQUE_T23A00CD203938B26F6C292A874981930DD989609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaUnique
struct  XmlSchemaUnique_t23A00CD203938B26F6C292A874981930DD989609  : public XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAUNIQUE_T23A00CD203938B26F6C292A874981930DD989609_H
#ifndef XMLSCHEMAVALIDATIONEXCEPTION_T5620ED68BF1780277A4C7E1CA022874903501A9F_H
#define XMLSCHEMAVALIDATIONEXCEPTION_T5620ED68BF1780277A4C7E1CA022874903501A9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidationException
struct  XmlSchemaValidationException_t5620ED68BF1780277A4C7E1CA022874903501A9F  : public XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATIONEXCEPTION_T5620ED68BF1780277A4C7E1CA022874903501A9F_H
#ifndef XDRBEGINCHILDFUNCTION_T0A5E4A12B86755E9EA9D9E723493EBBE9FFBF472_H
#define XDRBEGINCHILDFUNCTION_T0A5E4A12B86755E9EA9D9E723493EBBE9FFBF472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/XdrBeginChildFunction
struct  XdrBeginChildFunction_t0A5E4A12B86755E9EA9D9E723493EBBE9FFBF472  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDRBEGINCHILDFUNCTION_T0A5E4A12B86755E9EA9D9E723493EBBE9FFBF472_H
#ifndef XDRBUILDFUNCTION_TB7A90FF323AFC1DBE190CA3721EB16B45E48B8A7_H
#define XDRBUILDFUNCTION_TB7A90FF323AFC1DBE190CA3721EB16B45E48B8A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/XdrBuildFunction
struct  XdrBuildFunction_tB7A90FF323AFC1DBE190CA3721EB16B45E48B8A7  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDRBUILDFUNCTION_TB7A90FF323AFC1DBE190CA3721EB16B45E48B8A7_H
#ifndef XDRENDCHILDFUNCTION_T9A1AE00DFBA27B69E9A156C48AAFEF0425F0601F_H
#define XDRENDCHILDFUNCTION_T9A1AE00DFBA27B69E9A156C48AAFEF0425F0601F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/XdrEndChildFunction
struct  XdrEndChildFunction_t9A1AE00DFBA27B69E9A156C48AAFEF0425F0601F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDRENDCHILDFUNCTION_T9A1AE00DFBA27B69E9A156C48AAFEF0425F0601F_H
#ifndef XDRINITFUNCTION_TE54658948C45C11D13B781182403CAA3ADBC658D_H
#define XDRINITFUNCTION_TE54658948C45C11D13B781182403CAA3ADBC658D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/XdrInitFunction
struct  XdrInitFunction_tE54658948C45C11D13B781182403CAA3ADBC658D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDRINITFUNCTION_TE54658948C45C11D13B781182403CAA3ADBC658D_H
#ifndef XMLSCHEMAANY_TDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2_H
#define XMLSCHEMAANY_TDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAny
struct  XmlSchemaAny_tDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2  : public XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671
{
public:
	// System.String System.Xml.Schema.XmlSchemaAny::ns
	String_t* ___ns_13;
	// System.Xml.Schema.XmlSchemaContentProcessing System.Xml.Schema.XmlSchemaAny::processContents
	int32_t ___processContents_14;
	// System.Xml.Schema.NamespaceList System.Xml.Schema.XmlSchemaAny::namespaceList
	NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C * ___namespaceList_15;

public:
	inline static int32_t get_offset_of_ns_13() { return static_cast<int32_t>(offsetof(XmlSchemaAny_tDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2, ___ns_13)); }
	inline String_t* get_ns_13() const { return ___ns_13; }
	inline String_t** get_address_of_ns_13() { return &___ns_13; }
	inline void set_ns_13(String_t* value)
	{
		___ns_13 = value;
		Il2CppCodeGenWriteBarrier((&___ns_13), value);
	}

	inline static int32_t get_offset_of_processContents_14() { return static_cast<int32_t>(offsetof(XmlSchemaAny_tDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2, ___processContents_14)); }
	inline int32_t get_processContents_14() const { return ___processContents_14; }
	inline int32_t* get_address_of_processContents_14() { return &___processContents_14; }
	inline void set_processContents_14(int32_t value)
	{
		___processContents_14 = value;
	}

	inline static int32_t get_offset_of_namespaceList_15() { return static_cast<int32_t>(offsetof(XmlSchemaAny_tDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2, ___namespaceList_15)); }
	inline NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C * get_namespaceList_15() const { return ___namespaceList_15; }
	inline NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C ** get_address_of_namespaceList_15() { return &___namespaceList_15; }
	inline void set_namespaceList_15(NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C * value)
	{
		___namespaceList_15 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceList_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAANY_TDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2_H
#ifndef XMLSCHEMACOMPLEXTYPE_TFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9_H
#define XMLSCHEMACOMPLEXTYPE_TFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaComplexType
struct  XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9  : public XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9
{
public:
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaComplexType::block
	int32_t ___block_19;
	// System.Xml.Schema.XmlSchemaContentModel System.Xml.Schema.XmlSchemaComplexType::contentModel
	XmlSchemaContentModel_tB0209C69ADC1F671FA4ADDE88CC88C8959E84E45 * ___contentModel_20;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexType::particle
	XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * ___particle_21;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaComplexType::attributes
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___attributes_22;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaComplexType::anyAttribute
	XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * ___anyAttribute_23;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexType::contentTypeParticle
	XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * ___contentTypeParticle_24;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaComplexType::blockResolved
	int32_t ___blockResolved_25;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaComplexType::localElements
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___localElements_26;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaComplexType::attributeUses
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___attributeUses_27;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaComplexType::attributeWildcard
	XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * ___attributeWildcard_28;
	// System.Byte System.Xml.Schema.XmlSchemaComplexType::pvFlags
	uint8_t ___pvFlags_32;

public:
	inline static int32_t get_offset_of_block_19() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9, ___block_19)); }
	inline int32_t get_block_19() const { return ___block_19; }
	inline int32_t* get_address_of_block_19() { return &___block_19; }
	inline void set_block_19(int32_t value)
	{
		___block_19 = value;
	}

	inline static int32_t get_offset_of_contentModel_20() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9, ___contentModel_20)); }
	inline XmlSchemaContentModel_tB0209C69ADC1F671FA4ADDE88CC88C8959E84E45 * get_contentModel_20() const { return ___contentModel_20; }
	inline XmlSchemaContentModel_tB0209C69ADC1F671FA4ADDE88CC88C8959E84E45 ** get_address_of_contentModel_20() { return &___contentModel_20; }
	inline void set_contentModel_20(XmlSchemaContentModel_tB0209C69ADC1F671FA4ADDE88CC88C8959E84E45 * value)
	{
		___contentModel_20 = value;
		Il2CppCodeGenWriteBarrier((&___contentModel_20), value);
	}

	inline static int32_t get_offset_of_particle_21() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9, ___particle_21)); }
	inline XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * get_particle_21() const { return ___particle_21; }
	inline XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 ** get_address_of_particle_21() { return &___particle_21; }
	inline void set_particle_21(XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * value)
	{
		___particle_21 = value;
		Il2CppCodeGenWriteBarrier((&___particle_21), value);
	}

	inline static int32_t get_offset_of_attributes_22() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9, ___attributes_22)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_attributes_22() const { return ___attributes_22; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_attributes_22() { return &___attributes_22; }
	inline void set_attributes_22(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___attributes_22 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_22), value);
	}

	inline static int32_t get_offset_of_anyAttribute_23() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9, ___anyAttribute_23)); }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * get_anyAttribute_23() const { return ___anyAttribute_23; }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 ** get_address_of_anyAttribute_23() { return &___anyAttribute_23; }
	inline void set_anyAttribute_23(XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * value)
	{
		___anyAttribute_23 = value;
		Il2CppCodeGenWriteBarrier((&___anyAttribute_23), value);
	}

	inline static int32_t get_offset_of_contentTypeParticle_24() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9, ___contentTypeParticle_24)); }
	inline XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * get_contentTypeParticle_24() const { return ___contentTypeParticle_24; }
	inline XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 ** get_address_of_contentTypeParticle_24() { return &___contentTypeParticle_24; }
	inline void set_contentTypeParticle_24(XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * value)
	{
		___contentTypeParticle_24 = value;
		Il2CppCodeGenWriteBarrier((&___contentTypeParticle_24), value);
	}

	inline static int32_t get_offset_of_blockResolved_25() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9, ___blockResolved_25)); }
	inline int32_t get_blockResolved_25() const { return ___blockResolved_25; }
	inline int32_t* get_address_of_blockResolved_25() { return &___blockResolved_25; }
	inline void set_blockResolved_25(int32_t value)
	{
		___blockResolved_25 = value;
	}

	inline static int32_t get_offset_of_localElements_26() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9, ___localElements_26)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_localElements_26() const { return ___localElements_26; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_localElements_26() { return &___localElements_26; }
	inline void set_localElements_26(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___localElements_26 = value;
		Il2CppCodeGenWriteBarrier((&___localElements_26), value);
	}

	inline static int32_t get_offset_of_attributeUses_27() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9, ___attributeUses_27)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_attributeUses_27() const { return ___attributeUses_27; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_attributeUses_27() { return &___attributeUses_27; }
	inline void set_attributeUses_27(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___attributeUses_27 = value;
		Il2CppCodeGenWriteBarrier((&___attributeUses_27), value);
	}

	inline static int32_t get_offset_of_attributeWildcard_28() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9, ___attributeWildcard_28)); }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * get_attributeWildcard_28() const { return ___attributeWildcard_28; }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 ** get_address_of_attributeWildcard_28() { return &___attributeWildcard_28; }
	inline void set_attributeWildcard_28(XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * value)
	{
		___attributeWildcard_28 = value;
		Il2CppCodeGenWriteBarrier((&___attributeWildcard_28), value);
	}

	inline static int32_t get_offset_of_pvFlags_32() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9, ___pvFlags_32)); }
	inline uint8_t get_pvFlags_32() const { return ___pvFlags_32; }
	inline uint8_t* get_address_of_pvFlags_32() { return &___pvFlags_32; }
	inline void set_pvFlags_32(uint8_t value)
	{
		___pvFlags_32 = value;
	}
};

struct XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaComplexType System.Xml.Schema.XmlSchemaComplexType::anyTypeLax
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * ___anyTypeLax_29;
	// System.Xml.Schema.XmlSchemaComplexType System.Xml.Schema.XmlSchemaComplexType::anyTypeSkip
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * ___anyTypeSkip_30;
	// System.Xml.Schema.XmlSchemaComplexType System.Xml.Schema.XmlSchemaComplexType::untypedAnyType
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * ___untypedAnyType_31;

public:
	inline static int32_t get_offset_of_anyTypeLax_29() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9_StaticFields, ___anyTypeLax_29)); }
	inline XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * get_anyTypeLax_29() const { return ___anyTypeLax_29; }
	inline XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 ** get_address_of_anyTypeLax_29() { return &___anyTypeLax_29; }
	inline void set_anyTypeLax_29(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * value)
	{
		___anyTypeLax_29 = value;
		Il2CppCodeGenWriteBarrier((&___anyTypeLax_29), value);
	}

	inline static int32_t get_offset_of_anyTypeSkip_30() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9_StaticFields, ___anyTypeSkip_30)); }
	inline XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * get_anyTypeSkip_30() const { return ___anyTypeSkip_30; }
	inline XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 ** get_address_of_anyTypeSkip_30() { return &___anyTypeSkip_30; }
	inline void set_anyTypeSkip_30(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * value)
	{
		___anyTypeSkip_30 = value;
		Il2CppCodeGenWriteBarrier((&___anyTypeSkip_30), value);
	}

	inline static int32_t get_offset_of_untypedAnyType_31() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9_StaticFields, ___untypedAnyType_31)); }
	inline XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * get_untypedAnyType_31() const { return ___untypedAnyType_31; }
	inline XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 ** get_address_of_untypedAnyType_31() { return &___untypedAnyType_31; }
	inline void set_untypedAnyType_31(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * value)
	{
		___untypedAnyType_31 = value;
		Il2CppCodeGenWriteBarrier((&___untypedAnyType_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPLEXTYPE_TFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9_H
#ifndef XMLSCHEMAELEMENT_TF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D_H
#define XMLSCHEMAELEMENT_TF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaElement
struct  XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D  : public XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaElement::isAbstract
	bool ___isAbstract_13;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::hasAbstractAttribute
	bool ___hasAbstractAttribute_14;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::isNillable
	bool ___isNillable_15;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::hasNillableAttribute
	bool ___hasNillableAttribute_16;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::isLocalTypeDerivationChecked
	bool ___isLocalTypeDerivationChecked_17;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::block
	int32_t ___block_18;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::final
	int32_t ___final_19;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchemaElement::form
	int32_t ___form_20;
	// System.String System.Xml.Schema.XmlSchemaElement::defaultValue
	String_t* ___defaultValue_21;
	// System.String System.Xml.Schema.XmlSchemaElement::fixedValue
	String_t* ___fixedValue_22;
	// System.String System.Xml.Schema.XmlSchemaElement::name
	String_t* ___name_23;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::refName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___refName_24;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::substitutionGroup
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___substitutionGroup_25;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::typeName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___typeName_26;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaElement::type
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * ___type_27;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::qualifiedName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___qualifiedName_28;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaElement::elementType
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * ___elementType_29;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::blockResolved
	int32_t ___blockResolved_30;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::finalResolved
	int32_t ___finalResolved_31;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaElement::constraints
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___constraints_32;
	// System.Xml.Schema.SchemaElementDecl System.Xml.Schema.XmlSchemaElement::elementDecl
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * ___elementDecl_33;

public:
	inline static int32_t get_offset_of_isAbstract_13() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___isAbstract_13)); }
	inline bool get_isAbstract_13() const { return ___isAbstract_13; }
	inline bool* get_address_of_isAbstract_13() { return &___isAbstract_13; }
	inline void set_isAbstract_13(bool value)
	{
		___isAbstract_13 = value;
	}

	inline static int32_t get_offset_of_hasAbstractAttribute_14() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___hasAbstractAttribute_14)); }
	inline bool get_hasAbstractAttribute_14() const { return ___hasAbstractAttribute_14; }
	inline bool* get_address_of_hasAbstractAttribute_14() { return &___hasAbstractAttribute_14; }
	inline void set_hasAbstractAttribute_14(bool value)
	{
		___hasAbstractAttribute_14 = value;
	}

	inline static int32_t get_offset_of_isNillable_15() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___isNillable_15)); }
	inline bool get_isNillable_15() const { return ___isNillable_15; }
	inline bool* get_address_of_isNillable_15() { return &___isNillable_15; }
	inline void set_isNillable_15(bool value)
	{
		___isNillable_15 = value;
	}

	inline static int32_t get_offset_of_hasNillableAttribute_16() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___hasNillableAttribute_16)); }
	inline bool get_hasNillableAttribute_16() const { return ___hasNillableAttribute_16; }
	inline bool* get_address_of_hasNillableAttribute_16() { return &___hasNillableAttribute_16; }
	inline void set_hasNillableAttribute_16(bool value)
	{
		___hasNillableAttribute_16 = value;
	}

	inline static int32_t get_offset_of_isLocalTypeDerivationChecked_17() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___isLocalTypeDerivationChecked_17)); }
	inline bool get_isLocalTypeDerivationChecked_17() const { return ___isLocalTypeDerivationChecked_17; }
	inline bool* get_address_of_isLocalTypeDerivationChecked_17() { return &___isLocalTypeDerivationChecked_17; }
	inline void set_isLocalTypeDerivationChecked_17(bool value)
	{
		___isLocalTypeDerivationChecked_17 = value;
	}

	inline static int32_t get_offset_of_block_18() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___block_18)); }
	inline int32_t get_block_18() const { return ___block_18; }
	inline int32_t* get_address_of_block_18() { return &___block_18; }
	inline void set_block_18(int32_t value)
	{
		___block_18 = value;
	}

	inline static int32_t get_offset_of_final_19() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___final_19)); }
	inline int32_t get_final_19() const { return ___final_19; }
	inline int32_t* get_address_of_final_19() { return &___final_19; }
	inline void set_final_19(int32_t value)
	{
		___final_19 = value;
	}

	inline static int32_t get_offset_of_form_20() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___form_20)); }
	inline int32_t get_form_20() const { return ___form_20; }
	inline int32_t* get_address_of_form_20() { return &___form_20; }
	inline void set_form_20(int32_t value)
	{
		___form_20 = value;
	}

	inline static int32_t get_offset_of_defaultValue_21() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___defaultValue_21)); }
	inline String_t* get_defaultValue_21() const { return ___defaultValue_21; }
	inline String_t** get_address_of_defaultValue_21() { return &___defaultValue_21; }
	inline void set_defaultValue_21(String_t* value)
	{
		___defaultValue_21 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValue_21), value);
	}

	inline static int32_t get_offset_of_fixedValue_22() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___fixedValue_22)); }
	inline String_t* get_fixedValue_22() const { return ___fixedValue_22; }
	inline String_t** get_address_of_fixedValue_22() { return &___fixedValue_22; }
	inline void set_fixedValue_22(String_t* value)
	{
		___fixedValue_22 = value;
		Il2CppCodeGenWriteBarrier((&___fixedValue_22), value);
	}

	inline static int32_t get_offset_of_name_23() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___name_23)); }
	inline String_t* get_name_23() const { return ___name_23; }
	inline String_t** get_address_of_name_23() { return &___name_23; }
	inline void set_name_23(String_t* value)
	{
		___name_23 = value;
		Il2CppCodeGenWriteBarrier((&___name_23), value);
	}

	inline static int32_t get_offset_of_refName_24() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___refName_24)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_refName_24() const { return ___refName_24; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_refName_24() { return &___refName_24; }
	inline void set_refName_24(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___refName_24 = value;
		Il2CppCodeGenWriteBarrier((&___refName_24), value);
	}

	inline static int32_t get_offset_of_substitutionGroup_25() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___substitutionGroup_25)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_substitutionGroup_25() const { return ___substitutionGroup_25; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_substitutionGroup_25() { return &___substitutionGroup_25; }
	inline void set_substitutionGroup_25(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___substitutionGroup_25 = value;
		Il2CppCodeGenWriteBarrier((&___substitutionGroup_25), value);
	}

	inline static int32_t get_offset_of_typeName_26() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___typeName_26)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_typeName_26() const { return ___typeName_26; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_typeName_26() { return &___typeName_26; }
	inline void set_typeName_26(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___typeName_26 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_26), value);
	}

	inline static int32_t get_offset_of_type_27() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___type_27)); }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * get_type_27() const { return ___type_27; }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 ** get_address_of_type_27() { return &___type_27; }
	inline void set_type_27(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * value)
	{
		___type_27 = value;
		Il2CppCodeGenWriteBarrier((&___type_27), value);
	}

	inline static int32_t get_offset_of_qualifiedName_28() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___qualifiedName_28)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_qualifiedName_28() const { return ___qualifiedName_28; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_qualifiedName_28() { return &___qualifiedName_28; }
	inline void set_qualifiedName_28(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___qualifiedName_28 = value;
		Il2CppCodeGenWriteBarrier((&___qualifiedName_28), value);
	}

	inline static int32_t get_offset_of_elementType_29() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___elementType_29)); }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * get_elementType_29() const { return ___elementType_29; }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 ** get_address_of_elementType_29() { return &___elementType_29; }
	inline void set_elementType_29(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * value)
	{
		___elementType_29 = value;
		Il2CppCodeGenWriteBarrier((&___elementType_29), value);
	}

	inline static int32_t get_offset_of_blockResolved_30() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___blockResolved_30)); }
	inline int32_t get_blockResolved_30() const { return ___blockResolved_30; }
	inline int32_t* get_address_of_blockResolved_30() { return &___blockResolved_30; }
	inline void set_blockResolved_30(int32_t value)
	{
		___blockResolved_30 = value;
	}

	inline static int32_t get_offset_of_finalResolved_31() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___finalResolved_31)); }
	inline int32_t get_finalResolved_31() const { return ___finalResolved_31; }
	inline int32_t* get_address_of_finalResolved_31() { return &___finalResolved_31; }
	inline void set_finalResolved_31(int32_t value)
	{
		___finalResolved_31 = value;
	}

	inline static int32_t get_offset_of_constraints_32() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___constraints_32)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_constraints_32() const { return ___constraints_32; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_constraints_32() { return &___constraints_32; }
	inline void set_constraints_32(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___constraints_32 = value;
		Il2CppCodeGenWriteBarrier((&___constraints_32), value);
	}

	inline static int32_t get_offset_of_elementDecl_33() { return static_cast<int32_t>(offsetof(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D, ___elementDecl_33)); }
	inline SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * get_elementDecl_33() const { return ___elementDecl_33; }
	inline SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 ** get_address_of_elementDecl_33() { return &___elementDecl_33; }
	inline void set_elementDecl_33(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * value)
	{
		___elementDecl_33 = value;
		Il2CppCodeGenWriteBarrier((&___elementDecl_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAELEMENT_TF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D_H
#ifndef XMLSCHEMAENUMERATIONFACET_T9C81E3524A074C89B09B47FF1406E593F4222414_H
#define XMLSCHEMAENUMERATIONFACET_T9C81E3524A074C89B09B47FF1406E593F4222414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaEnumerationFacet
struct  XmlSchemaEnumerationFacet_t9C81E3524A074C89B09B47FF1406E593F4222414  : public XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAENUMERATIONFACET_T9C81E3524A074C89B09B47FF1406E593F4222414_H
#ifndef XMLSCHEMAGROUPBASE_TC18B7AAFF241664404A1FB5D936508AB2F4D208F_H
#define XMLSCHEMAGROUPBASE_TC18B7AAFF241664404A1FB5D936508AB2F4D208F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaGroupBase
struct  XmlSchemaGroupBase_tC18B7AAFF241664404A1FB5D936508AB2F4D208F  : public XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAGROUPBASE_TC18B7AAFF241664404A1FB5D936508AB2F4D208F_H
#ifndef XMLSCHEMAGROUPREF_TEC541A27220ABE0F02B3B1FD72730E536FAEF6F3_H
#define XMLSCHEMAGROUPREF_TEC541A27220ABE0F02B3B1FD72730E536FAEF6F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaGroupRef
struct  XmlSchemaGroupRef_tEC541A27220ABE0F02B3B1FD72730E536FAEF6F3  : public XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaGroupRef::refName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___refName_13;
	// System.Xml.Schema.XmlSchemaGroupBase System.Xml.Schema.XmlSchemaGroupRef::particle
	XmlSchemaGroupBase_tC18B7AAFF241664404A1FB5D936508AB2F4D208F * ___particle_14;
	// System.Xml.Schema.XmlSchemaGroup System.Xml.Schema.XmlSchemaGroupRef::refined
	XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2 * ___refined_15;

public:
	inline static int32_t get_offset_of_refName_13() { return static_cast<int32_t>(offsetof(XmlSchemaGroupRef_tEC541A27220ABE0F02B3B1FD72730E536FAEF6F3, ___refName_13)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_refName_13() const { return ___refName_13; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_refName_13() { return &___refName_13; }
	inline void set_refName_13(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___refName_13 = value;
		Il2CppCodeGenWriteBarrier((&___refName_13), value);
	}

	inline static int32_t get_offset_of_particle_14() { return static_cast<int32_t>(offsetof(XmlSchemaGroupRef_tEC541A27220ABE0F02B3B1FD72730E536FAEF6F3, ___particle_14)); }
	inline XmlSchemaGroupBase_tC18B7AAFF241664404A1FB5D936508AB2F4D208F * get_particle_14() const { return ___particle_14; }
	inline XmlSchemaGroupBase_tC18B7AAFF241664404A1FB5D936508AB2F4D208F ** get_address_of_particle_14() { return &___particle_14; }
	inline void set_particle_14(XmlSchemaGroupBase_tC18B7AAFF241664404A1FB5D936508AB2F4D208F * value)
	{
		___particle_14 = value;
		Il2CppCodeGenWriteBarrier((&___particle_14), value);
	}

	inline static int32_t get_offset_of_refined_15() { return static_cast<int32_t>(offsetof(XmlSchemaGroupRef_tEC541A27220ABE0F02B3B1FD72730E536FAEF6F3, ___refined_15)); }
	inline XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2 * get_refined_15() const { return ___refined_15; }
	inline XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2 ** get_address_of_refined_15() { return &___refined_15; }
	inline void set_refined_15(XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2 * value)
	{
		___refined_15 = value;
		Il2CppCodeGenWriteBarrier((&___refined_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAGROUPREF_TEC541A27220ABE0F02B3B1FD72730E536FAEF6F3_H
#ifndef XMLSCHEMAIMPORT_T4FC11FF359600C711AFFEF2DC569810456D8CC93_H
#define XMLSCHEMAIMPORT_T4FC11FF359600C711AFFEF2DC569810456D8CC93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaImport
struct  XmlSchemaImport_t4FC11FF359600C711AFFEF2DC569810456D8CC93  : public XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8
{
public:
	// System.String System.Xml.Schema.XmlSchemaImport::ns
	String_t* ___ns_12;
	// System.Xml.Schema.XmlSchemaAnnotation System.Xml.Schema.XmlSchemaImport::annotation
	XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 * ___annotation_13;

public:
	inline static int32_t get_offset_of_ns_12() { return static_cast<int32_t>(offsetof(XmlSchemaImport_t4FC11FF359600C711AFFEF2DC569810456D8CC93, ___ns_12)); }
	inline String_t* get_ns_12() const { return ___ns_12; }
	inline String_t** get_address_of_ns_12() { return &___ns_12; }
	inline void set_ns_12(String_t* value)
	{
		___ns_12 = value;
		Il2CppCodeGenWriteBarrier((&___ns_12), value);
	}

	inline static int32_t get_offset_of_annotation_13() { return static_cast<int32_t>(offsetof(XmlSchemaImport_t4FC11FF359600C711AFFEF2DC569810456D8CC93, ___annotation_13)); }
	inline XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 * get_annotation_13() const { return ___annotation_13; }
	inline XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 ** get_address_of_annotation_13() { return &___annotation_13; }
	inline void set_annotation_13(XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 * value)
	{
		___annotation_13 = value;
		Il2CppCodeGenWriteBarrier((&___annotation_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAIMPORT_T4FC11FF359600C711AFFEF2DC569810456D8CC93_H
#ifndef XMLSCHEMAINCLUDE_TD337A52B31C9B3195CE4272AF057E286CDD34001_H
#define XMLSCHEMAINCLUDE_TD337A52B31C9B3195CE4272AF057E286CDD34001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaInclude
struct  XmlSchemaInclude_tD337A52B31C9B3195CE4272AF057E286CDD34001  : public XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8
{
public:
	// System.Xml.Schema.XmlSchemaAnnotation System.Xml.Schema.XmlSchemaInclude::annotation
	XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 * ___annotation_12;

public:
	inline static int32_t get_offset_of_annotation_12() { return static_cast<int32_t>(offsetof(XmlSchemaInclude_tD337A52B31C9B3195CE4272AF057E286CDD34001, ___annotation_12)); }
	inline XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 * get_annotation_12() const { return ___annotation_12; }
	inline XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 ** get_address_of_annotation_12() { return &___annotation_12; }
	inline void set_annotation_12(XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 * value)
	{
		___annotation_12 = value;
		Il2CppCodeGenWriteBarrier((&___annotation_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAINCLUDE_TD337A52B31C9B3195CE4272AF057E286CDD34001_H
#ifndef XMLSCHEMAMAXEXCLUSIVEFACET_T68766F0FFC4EE44E09AA2B9EE90C31DA1A795F70_H
#define XMLSCHEMAMAXEXCLUSIVEFACET_T68766F0FFC4EE44E09AA2B9EE90C31DA1A795F70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMaxExclusiveFacet
struct  XmlSchemaMaxExclusiveFacet_t68766F0FFC4EE44E09AA2B9EE90C31DA1A795F70  : public XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMAXEXCLUSIVEFACET_T68766F0FFC4EE44E09AA2B9EE90C31DA1A795F70_H
#ifndef XMLSCHEMAMAXINCLUSIVEFACET_T90C4E58720B588EA452F6305E8DF7C06F2EFD209_H
#define XMLSCHEMAMAXINCLUSIVEFACET_T90C4E58720B588EA452F6305E8DF7C06F2EFD209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMaxInclusiveFacet
struct  XmlSchemaMaxInclusiveFacet_t90C4E58720B588EA452F6305E8DF7C06F2EFD209  : public XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMAXINCLUSIVEFACET_T90C4E58720B588EA452F6305E8DF7C06F2EFD209_H
#ifndef XMLSCHEMAMINEXCLUSIVEFACET_T55185193083B2ED3C1DD747C19C211C86868C155_H
#define XMLSCHEMAMINEXCLUSIVEFACET_T55185193083B2ED3C1DD747C19C211C86868C155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMinExclusiveFacet
struct  XmlSchemaMinExclusiveFacet_t55185193083B2ED3C1DD747C19C211C86868C155  : public XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMINEXCLUSIVEFACET_T55185193083B2ED3C1DD747C19C211C86868C155_H
#ifndef XMLSCHEMAMININCLUSIVEFACET_T5E3430DB39E45DA9261CA83D762560C0FFECF02F_H
#define XMLSCHEMAMININCLUSIVEFACET_T5E3430DB39E45DA9261CA83D762560C0FFECF02F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMinInclusiveFacet
struct  XmlSchemaMinInclusiveFacet_t5E3430DB39E45DA9261CA83D762560C0FFECF02F  : public XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMININCLUSIVEFACET_T5E3430DB39E45DA9261CA83D762560C0FFECF02F_H
#ifndef XMLSCHEMANUMERICFACET_T42FE2DAD05E74AB5BABE14667C3182A2DAFC91B0_H
#define XMLSCHEMANUMERICFACET_T42FE2DAD05E74AB5BABE14667C3182A2DAFC91B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaNumericFacet
struct  XmlSchemaNumericFacet_t42FE2DAD05E74AB5BABE14667C3182A2DAFC91B0  : public XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMANUMERICFACET_T42FE2DAD05E74AB5BABE14667C3182A2DAFC91B0_H
#ifndef XSODICTIONARYENUMERATOR_T3241C9CB5FE5DFBC1D82B0CF43CED73E699AF92B_H
#define XSODICTIONARYENUMERATOR_T3241C9CB5FE5DFBC1D82B0CF43CED73E699AF92B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectTable/XSODictionaryEnumerator
struct  XSODictionaryEnumerator_t3241C9CB5FE5DFBC1D82B0CF43CED73E699AF92B  : public XSOEnumerator_t0BDA5BB56D78F4B2F028B2831622DC3336011D8E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSODICTIONARYENUMERATOR_T3241C9CB5FE5DFBC1D82B0CF43CED73E699AF92B_H
#ifndef EMPTYPARTICLE_T4D9B96A627867C8839845CBD83183C9CD3ECE202_H
#define EMPTYPARTICLE_T4D9B96A627867C8839845CBD83183C9CD3ECE202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaParticle/EmptyParticle
struct  EmptyParticle_t4D9B96A627867C8839845CBD83183C9CD3ECE202  : public XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYPARTICLE_T4D9B96A627867C8839845CBD83183C9CD3ECE202_H
#ifndef XMLSCHEMAPATTERNFACET_T0FE0B38A1CCE96CD76792E0662B892BA630FE322_H
#define XMLSCHEMAPATTERNFACET_T0FE0B38A1CCE96CD76792E0662B892BA630FE322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaPatternFacet
struct  XmlSchemaPatternFacet_t0FE0B38A1CCE96CD76792E0662B892BA630FE322  : public XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAPATTERNFACET_T0FE0B38A1CCE96CD76792E0662B892BA630FE322_H
#ifndef XMLSCHEMAREDEFINE_T20EC66AD3FB69E2D3269753B97850EA9377EFD6B_H
#define XMLSCHEMAREDEFINE_T20EC66AD3FB69E2D3269753B97850EA9377EFD6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaRedefine
struct  XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B  : public XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaRedefine::items
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___items_12;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaRedefine::attributeGroups
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___attributeGroups_13;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaRedefine::types
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___types_14;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaRedefine::groups
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___groups_15;

public:
	inline static int32_t get_offset_of_items_12() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B, ___items_12)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_items_12() const { return ___items_12; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_items_12() { return &___items_12; }
	inline void set_items_12(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___items_12 = value;
		Il2CppCodeGenWriteBarrier((&___items_12), value);
	}

	inline static int32_t get_offset_of_attributeGroups_13() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B, ___attributeGroups_13)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_attributeGroups_13() const { return ___attributeGroups_13; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_attributeGroups_13() { return &___attributeGroups_13; }
	inline void set_attributeGroups_13(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___attributeGroups_13 = value;
		Il2CppCodeGenWriteBarrier((&___attributeGroups_13), value);
	}

	inline static int32_t get_offset_of_types_14() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B, ___types_14)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_types_14() const { return ___types_14; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_types_14() { return &___types_14; }
	inline void set_types_14(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___types_14 = value;
		Il2CppCodeGenWriteBarrier((&___types_14), value);
	}

	inline static int32_t get_offset_of_groups_15() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B, ___groups_15)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_groups_15() const { return ___groups_15; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_groups_15() { return &___groups_15; }
	inline void set_groups_15(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___groups_15 = value;
		Il2CppCodeGenWriteBarrier((&___groups_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAREDEFINE_T20EC66AD3FB69E2D3269753B97850EA9377EFD6B_H
#ifndef XMLSCHEMASIMPLETYPE_T3E090F6F088E02B69D984EED6C2A209ACB42A68A_H
#define XMLSCHEMASIMPLETYPE_T3E090F6F088E02B69D984EED6C2A209ACB42A68A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleType
struct  XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A  : public XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9
{
public:
	// System.Xml.Schema.XmlSchemaSimpleTypeContent System.Xml.Schema.XmlSchemaSimpleType::content
	XmlSchemaSimpleTypeContent_t87EF2A2F4B2F3D7827EA18B0FCB8393EA0201A3E * ___content_19;

public:
	inline static int32_t get_offset_of_content_19() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A, ___content_19)); }
	inline XmlSchemaSimpleTypeContent_t87EF2A2F4B2F3D7827EA18B0FCB8393EA0201A3E * get_content_19() const { return ___content_19; }
	inline XmlSchemaSimpleTypeContent_t87EF2A2F4B2F3D7827EA18B0FCB8393EA0201A3E ** get_address_of_content_19() { return &___content_19; }
	inline void set_content_19(XmlSchemaSimpleTypeContent_t87EF2A2F4B2F3D7827EA18B0FCB8393EA0201A3E * value)
	{
		___content_19 = value;
		Il2CppCodeGenWriteBarrier((&___content_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPE_T3E090F6F088E02B69D984EED6C2A209ACB42A68A_H
#ifndef XMLSCHEMAWHITESPACEFACET_TD0BAD46B4C0A014ADC9A796049E24E55F27F4ADF_H
#define XMLSCHEMAWHITESPACEFACET_TD0BAD46B4C0A014ADC9A796049E24E55F27F4ADF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaWhiteSpaceFacet
struct  XmlSchemaWhiteSpaceFacet_tD0BAD46B4C0A014ADC9A796049E24E55F27F4ADF  : public XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAWHITESPACEFACET_TD0BAD46B4C0A014ADC9A796049E24E55F27F4ADF_H
#ifndef XMLVALUEGETTER_TE51E83B894C27BA5CD229FD9E8C5FC9C851555DF_H
#define XMLVALUEGETTER_TE51E83B894C27BA5CD229FD9E8C5FC9C851555DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlValueGetter
struct  XmlValueGetter_tE51E83B894C27BA5CD229FD9E8C5FC9C851555DF  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLVALUEGETTER_TE51E83B894C27BA5CD229FD9E8C5FC9C851555DF_H
#ifndef XMLSCHEMAALL_T395D3C374B5E11F2B2338F0C33E9F267D88CBA3B_H
#define XMLSCHEMAALL_T395D3C374B5E11F2B2338F0C33E9F267D88CBA3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAll
struct  XmlSchemaAll_t395D3C374B5E11F2B2338F0C33E9F267D88CBA3B  : public XmlSchemaGroupBase_tC18B7AAFF241664404A1FB5D936508AB2F4D208F
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaAll::items
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___items_13;

public:
	inline static int32_t get_offset_of_items_13() { return static_cast<int32_t>(offsetof(XmlSchemaAll_t395D3C374B5E11F2B2338F0C33E9F267D88CBA3B, ___items_13)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_items_13() const { return ___items_13; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_items_13() { return &___items_13; }
	inline void set_items_13(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___items_13 = value;
		Il2CppCodeGenWriteBarrier((&___items_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAALL_T395D3C374B5E11F2B2338F0C33E9F267D88CBA3B_H
#ifndef XMLSCHEMACHOICE_TEC175BFF045FDA4DECF5A938922D6CDC4216653B_H
#define XMLSCHEMACHOICE_TEC175BFF045FDA4DECF5A938922D6CDC4216653B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaChoice
struct  XmlSchemaChoice_tEC175BFF045FDA4DECF5A938922D6CDC4216653B  : public XmlSchemaGroupBase_tC18B7AAFF241664404A1FB5D936508AB2F4D208F
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaChoice::items
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___items_13;

public:
	inline static int32_t get_offset_of_items_13() { return static_cast<int32_t>(offsetof(XmlSchemaChoice_tEC175BFF045FDA4DECF5A938922D6CDC4216653B, ___items_13)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_items_13() const { return ___items_13; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_items_13() { return &___items_13; }
	inline void set_items_13(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___items_13 = value;
		Il2CppCodeGenWriteBarrier((&___items_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACHOICE_TEC175BFF045FDA4DECF5A938922D6CDC4216653B_H
#ifndef XMLSCHEMAFRACTIONDIGITSFACET_T4AA8D1C0E9B2296F740AD61B9621F6C743A06D5B_H
#define XMLSCHEMAFRACTIONDIGITSFACET_T4AA8D1C0E9B2296F740AD61B9621F6C743A06D5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFractionDigitsFacet
struct  XmlSchemaFractionDigitsFacet_t4AA8D1C0E9B2296F740AD61B9621F6C743A06D5B  : public XmlSchemaNumericFacet_t42FE2DAD05E74AB5BABE14667C3182A2DAFC91B0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFRACTIONDIGITSFACET_T4AA8D1C0E9B2296F740AD61B9621F6C743A06D5B_H
#ifndef XMLSCHEMALENGTHFACET_T0477816E8D01018C0C88C46CFCB8460D565A62DC_H
#define XMLSCHEMALENGTHFACET_T0477816E8D01018C0C88C46CFCB8460D565A62DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaLengthFacet
struct  XmlSchemaLengthFacet_t0477816E8D01018C0C88C46CFCB8460D565A62DC  : public XmlSchemaNumericFacet_t42FE2DAD05E74AB5BABE14667C3182A2DAFC91B0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMALENGTHFACET_T0477816E8D01018C0C88C46CFCB8460D565A62DC_H
#ifndef XMLSCHEMAMAXLENGTHFACET_TBE5D5CE2A057904C1CB23D397238D6987D2642BD_H
#define XMLSCHEMAMAXLENGTHFACET_TBE5D5CE2A057904C1CB23D397238D6987D2642BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMaxLengthFacet
struct  XmlSchemaMaxLengthFacet_tBE5D5CE2A057904C1CB23D397238D6987D2642BD  : public XmlSchemaNumericFacet_t42FE2DAD05E74AB5BABE14667C3182A2DAFC91B0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMAXLENGTHFACET_TBE5D5CE2A057904C1CB23D397238D6987D2642BD_H
#ifndef XMLSCHEMAMINLENGTHFACET_T19B86AE9CF33EFEB8E9E8ECBB01893D05D041ACA_H
#define XMLSCHEMAMINLENGTHFACET_T19B86AE9CF33EFEB8E9E8ECBB01893D05D041ACA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMinLengthFacet
struct  XmlSchemaMinLengthFacet_t19B86AE9CF33EFEB8E9E8ECBB01893D05D041ACA  : public XmlSchemaNumericFacet_t42FE2DAD05E74AB5BABE14667C3182A2DAFC91B0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMINLENGTHFACET_T19B86AE9CF33EFEB8E9E8ECBB01893D05D041ACA_H
#ifndef XMLSCHEMASEQUENCE_TBF8498445C0FD553EF64CC240B1236CEB83FB215_H
#define XMLSCHEMASEQUENCE_TBF8498445C0FD553EF64CC240B1236CEB83FB215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSequence
struct  XmlSchemaSequence_tBF8498445C0FD553EF64CC240B1236CEB83FB215  : public XmlSchemaGroupBase_tC18B7AAFF241664404A1FB5D936508AB2F4D208F
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSequence::items
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * ___items_13;

public:
	inline static int32_t get_offset_of_items_13() { return static_cast<int32_t>(offsetof(XmlSchemaSequence_tBF8498445C0FD553EF64CC240B1236CEB83FB215, ___items_13)); }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * get_items_13() const { return ___items_13; }
	inline XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 ** get_address_of_items_13() { return &___items_13; }
	inline void set_items_13(XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280 * value)
	{
		___items_13 = value;
		Il2CppCodeGenWriteBarrier((&___items_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASEQUENCE_TBF8498445C0FD553EF64CC240B1236CEB83FB215_H
#ifndef XMLSCHEMATOTALDIGITSFACET_TE1E389B72A9CFC03156A4024ACDDEFF2B1D03AB7_H
#define XMLSCHEMATOTALDIGITSFACET_TE1E389B72A9CFC03156A4024ACDDEFF2B1D03AB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaTotalDigitsFacet
struct  XmlSchemaTotalDigitsFacet_tE1E389B72A9CFC03156A4024ACDDEFF2B1D03AB7  : public XmlSchemaNumericFacet_t42FE2DAD05E74AB5BABE14667C3182A2DAFC91B0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMATOTALDIGITSFACET_TE1E389B72A9CFC03156A4024ACDDEFF2B1D03AB7_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[14] = 
{
	ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E::get_offset_of__ElementDecl_0(),
	ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E::get_offset_of__ContentAttr_1(),
	ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E::get_offset_of__OrderAttr_2(),
	ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E::get_offset_of__MasterGroupRequired_3(),
	ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E::get_offset_of__ExistTerminal_4(),
	ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E::get_offset_of__AllowDataType_5(),
	ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E::get_offset_of__HasDataType_6(),
	ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E::get_offset_of__HasType_7(),
	ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E::get_offset_of__EnumerationRequired_8(),
	ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E::get_offset_of__MinVal_9(),
	ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E::get_offset_of__MaxVal_10(),
	ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E::get_offset_of__MaxLength_11(),
	ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E::get_offset_of__MinLength_12(),
	ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E::get_offset_of__AttDefList_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[12] = 
{
	AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290::get_offset_of__AttDef_0(),
	AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290::get_offset_of__Name_1(),
	AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290::get_offset_of__Prefix_2(),
	AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290::get_offset_of__Required_3(),
	AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290::get_offset_of__MinVal_4(),
	AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290::get_offset_of__MaxVal_5(),
	AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290::get_offset_of__MaxLength_6(),
	AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290::get_offset_of__MinLength_7(),
	AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290::get_offset_of__EnumerationRequired_8(),
	AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290::get_offset_of__HasDataType_9(),
	AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290::get_offset_of__Global_10(),
	AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290::get_offset_of__Default_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (XdrBuildFunction_tB7A90FF323AFC1DBE190CA3721EB16B45E48B8A7), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (XdrInitFunction_tE54658948C45C11D13B781182403CAA3ADBC658D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (XdrBeginChildFunction_t0A5E4A12B86755E9EA9D9E723493EBBE9FFBF472), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (XdrEndChildFunction_t9A1AE00DFBA27B69E9A156C48AAFEF0425F0601F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (XdrAttributeEntry_t35EED30A44B91178F3443D4688FB6481C36DA3AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[4] = 
{
	XdrAttributeEntry_t35EED30A44B91178F3443D4688FB6481C36DA3AD::get_offset_of__Attribute_0(),
	XdrAttributeEntry_t35EED30A44B91178F3443D4688FB6481C36DA3AD::get_offset_of__SchemaFlags_1(),
	XdrAttributeEntry_t35EED30A44B91178F3443D4688FB6481C36DA3AD::get_offset_of__Datatype_2(),
	XdrAttributeEntry_t35EED30A44B91178F3443D4688FB6481C36DA3AD::get_offset_of__BuildFunc_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[7] = 
{
	XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6::get_offset_of__Name_0(),
	XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6::get_offset_of__NextStates_1(),
	XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6::get_offset_of__Attributes_2(),
	XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6::get_offset_of__InitFunc_3(),
	XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6::get_offset_of__BeginChildFunc_4(),
	XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6::get_offset_of__EndChildFunc_5(),
	XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6::get_offset_of__AllowText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[8] = 
{
	XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491::get_offset_of_validationStack_15(),
	XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491::get_offset_of_attPresence_16(),
	XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491::get_offset_of_name_17(),
	XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491::get_offset_of_nsManager_18(),
	XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491::get_offset_of_isProcessContents_19(),
	XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491::get_offset_of_IDs_20(),
	XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491::get_offset_of_idRefListHead_21(),
	XdrValidator_t58D17B0DA19B9AA5E4C77414FCBD12FC40CB3491::get_offset_of_inlineSchemaParser_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (XmlAtomicValue_tD2DE4444E12CFCA939E01F07BE367EEEFD7C38C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[5] = 
{
	XmlAtomicValue_tD2DE4444E12CFCA939E01F07BE367EEEFD7C38C0::get_offset_of_xmlType_0(),
	XmlAtomicValue_tD2DE4444E12CFCA939E01F07BE367EEEFD7C38C0::get_offset_of_objVal_1(),
	XmlAtomicValue_tD2DE4444E12CFCA939E01F07BE367EEEFD7C38C0::get_offset_of_clrType_2(),
	XmlAtomicValue_tD2DE4444E12CFCA939E01F07BE367EEEFD7C38C0::get_offset_of_unionVal_3(),
	XmlAtomicValue_tD2DE4444E12CFCA939E01F07BE367EEEFD7C38C0::get_offset_of_nsPrefix_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2110[5] = 
{
	Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391::get_offset_of_boolVal_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391::get_offset_of_dblVal_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391::get_offset_of_i64Val_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391::get_offset_of_i32Val_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Union_t75FE76D5ECF7F32BF3656D21BD446F4E42996391::get_offset_of_dtVal_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (NamespacePrefixForQName_t80F0083B9C2151D487D41CCDD89B668C21902D8F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2111[2] = 
{
	NamespacePrefixForQName_t80F0083B9C2151D487D41CCDD89B668C21902D8F::get_offset_of_prefix_0(),
	NamespacePrefixForQName_t80F0083B9C2151D487D41CCDD89B668C21902D8F::get_offset_of_ns_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F), -1, sizeof(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2112[30] = 
{
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_attributeFormDefault_6(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_elementFormDefault_7(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_blockDefault_8(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_finalDefault_9(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_targetNs_10(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_version_11(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_includes_12(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_items_13(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_id_14(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_moreAttributes_15(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_isCompiled_16(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_isCompiledBySet_17(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_isPreprocessed_18(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_isRedefined_19(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_errorCount_20(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_attributes_21(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_attributeGroups_22(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_elements_23(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_types_24(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_groups_25(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_notations_26(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_identityConstraints_27(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F_StaticFields::get_offset_of_globalIdCounter_28(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_importedSchemas_29(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_importedNamespaces_30(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_schemaId_31(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_baseUri_32(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_isChameleon_33(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_ids_34(),
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F::get_offset_of_document_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (XmlSchemaAll_t395D3C374B5E11F2B2338F0C33E9F267D88CBA3B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[1] = 
{
	XmlSchemaAll_t395D3C374B5E11F2B2338F0C33E9F267D88CBA3B::get_offset_of_items_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[3] = 
{
	XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B::get_offset_of_id_6(),
	XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B::get_offset_of_annotation_7(),
	XmlSchemaAnnotated_t94F7899A20B495CD62FA0976A4F23FD4A0847B7B::get_offset_of_moreAttributes_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[3] = 
{
	XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725::get_offset_of_id_6(),
	XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725::get_offset_of_items_7(),
	XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725::get_offset_of_moreAttributes_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (XmlSchemaAny_tDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2116[3] = 
{
	XmlSchemaAny_tDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2::get_offset_of_ns_13(),
	XmlSchemaAny_tDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2::get_offset_of_processContents_14(),
	XmlSchemaAny_tDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2::get_offset_of_namespaceList_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[3] = 
{
	XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078::get_offset_of_ns_9(),
	XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078::get_offset_of_processContents_10(),
	XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078::get_offset_of_namespaceList_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (XmlSchemaAppInfo_t5E2FEE4B31F35CD076CC2F47DA5CB82DA139A860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[2] = 
{
	XmlSchemaAppInfo_t5E2FEE4B31F35CD076CC2F47DA5CB82DA139A860::get_offset_of_source_6(),
	XmlSchemaAppInfo_t5E2FEE4B31F35CD076CC2F47DA5CB82DA139A860::get_offset_of_markup_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2119[11] = 
{
	XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B::get_offset_of_defaultValue_9(),
	XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B::get_offset_of_fixedValue_10(),
	XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B::get_offset_of_name_11(),
	XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B::get_offset_of_form_12(),
	XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B::get_offset_of_use_13(),
	XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B::get_offset_of_refName_14(),
	XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B::get_offset_of_typeName_15(),
	XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B::get_offset_of_qualifiedName_16(),
	XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B::get_offset_of_type_17(),
	XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B::get_offset_of_attributeType_18(),
	XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B::get_offset_of_attDef_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[8] = 
{
	XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E::get_offset_of_name_9(),
	XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E::get_offset_of_attributes_10(),
	XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E::get_offset_of_anyAttribute_11(),
	XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E::get_offset_of_qname_12(),
	XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E::get_offset_of_redefined_13(),
	XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E::get_offset_of_attributeUses_14(),
	XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E::get_offset_of_attributeWildcard_15(),
	XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E::get_offset_of_selfReferenceCount_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (XmlSchemaAttributeGroupRef_t5DA5E994E5C8A3E844A6B88B5E9AEA8195B09BD4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[1] = 
{
	XmlSchemaAttributeGroupRef_t5DA5E994E5C8A3E844A6B88B5E9AEA8195B09BD4::get_offset_of_refName_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (XmlSchemaChoice_tEC175BFF045FDA4DECF5A938922D6CDC4216653B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[1] = 
{
	XmlSchemaChoice_tEC175BFF045FDA4DECF5A938922D6CDC4216653B::get_offset_of_items_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[8] = 
{
	XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5::get_offset_of_collection_0(),
	XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5::get_offset_of_nameTable_1(),
	XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5::get_offset_of_schemaNames_2(),
	XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5::get_offset_of_wLock_3(),
	XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5::get_offset_of_timeout_4(),
	XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5::get_offset_of_isThreadSafe_5(),
	XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5::get_offset_of_validationEventHandler_6(),
	XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5::get_offset_of_xmlResolver_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (XmlSchemaCollectionNode_tDAA903FE8874B90782D728CBB00BAD49A9823558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[3] = 
{
	XmlSchemaCollectionNode_tDAA903FE8874B90782D728CBB00BAD49A9823558::get_offset_of_namespaceUri_0(),
	XmlSchemaCollectionNode_tDAA903FE8874B90782D728CBB00BAD49A9823558::get_offset_of_schemaInfo_1(),
	XmlSchemaCollectionNode_tDAA903FE8874B90782D728CBB00BAD49A9823558::get_offset_of_schema_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (XmlSchemaCollectionEnumerator_tB04D6CC4C7F2F39E23A2EACF9BF4926977CA0114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[1] = 
{
	XmlSchemaCollectionEnumerator_tB04D6CC4C7F2F39E23A2EACF9BF4926977CA0114::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[1] = 
{
	XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC::get_offset_of_enableUpaCheck_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (XmlSchemaComplexContent_t0C5B5B9FA891EAAD3F277E4C494956082E127CF3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[3] = 
{
	XmlSchemaComplexContent_t0C5B5B9FA891EAAD3F277E4C494956082E127CF3::get_offset_of_content_9(),
	XmlSchemaComplexContent_t0C5B5B9FA891EAAD3F277E4C494956082E127CF3::get_offset_of_isMixed_10(),
	XmlSchemaComplexContent_t0C5B5B9FA891EAAD3F277E4C494956082E127CF3::get_offset_of_hasMixedAttribute_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (XmlSchemaComplexContentExtension_t3B6C614515825C627D8DE0D040CD97753829004B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[4] = 
{
	XmlSchemaComplexContentExtension_t3B6C614515825C627D8DE0D040CD97753829004B::get_offset_of_particle_9(),
	XmlSchemaComplexContentExtension_t3B6C614515825C627D8DE0D040CD97753829004B::get_offset_of_attributes_10(),
	XmlSchemaComplexContentExtension_t3B6C614515825C627D8DE0D040CD97753829004B::get_offset_of_anyAttribute_11(),
	XmlSchemaComplexContentExtension_t3B6C614515825C627D8DE0D040CD97753829004B::get_offset_of_baseTypeName_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (XmlSchemaComplexContentRestriction_t05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[4] = 
{
	XmlSchemaComplexContentRestriction_t05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0::get_offset_of_particle_9(),
	XmlSchemaComplexContentRestriction_t05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0::get_offset_of_attributes_10(),
	XmlSchemaComplexContentRestriction_t05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0::get_offset_of_anyAttribute_11(),
	XmlSchemaComplexContentRestriction_t05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0::get_offset_of_baseTypeName_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9), -1, sizeof(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2130[14] = 
{
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9::get_offset_of_block_19(),
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9::get_offset_of_contentModel_20(),
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9::get_offset_of_particle_21(),
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9::get_offset_of_attributes_22(),
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9::get_offset_of_anyAttribute_23(),
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9::get_offset_of_contentTypeParticle_24(),
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9::get_offset_of_blockResolved_25(),
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9::get_offset_of_localElements_26(),
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9::get_offset_of_attributeUses_27(),
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9::get_offset_of_attributeWildcard_28(),
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9_StaticFields::get_offset_of_anyTypeLax_29(),
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9_StaticFields::get_offset_of_anyTypeSkip_30(),
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9_StaticFields::get_offset_of_untypedAnyType_31(),
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9::get_offset_of_pvFlags_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (XmlSchemaContent_tE0E45FE215D74AE629A316B2CDA06480C07278D3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (XmlSchemaContentModel_tB0209C69ADC1F671FA4ADDE88CC88C8959E84E45), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (XmlSchemaContentProcessing_tD85295E4C6FA245DAE6F9E4E1C927E76678B197A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2133[5] = 
{
	XmlSchemaContentProcessing_tD85295E4C6FA245DAE6F9E4E1C927E76678B197A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (XmlSchemaContentType_tAAF4C8374963BFF5F4C3F71FAA1AE9E8944E03E5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2134[5] = 
{
	XmlSchemaContentType_tAAF4C8374963BFF5F4C3F71FAA1AE9E8944E03E5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (XmlSchemaDerivationMethod_t9C964CFFC430E62A1FF62E70B9FE2C976A3A5F58)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2136[9] = 
{
	XmlSchemaDerivationMethod_t9C964CFFC430E62A1FF62E70B9FE2C976A3A5F58::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE), -1, sizeof(XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2137[4] = 
{
	XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE::get_offset_of_source_6(),
	XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE::get_offset_of_language_7(),
	XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE::get_offset_of_markup_8(),
	XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE_StaticFields::get_offset_of_languageType_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[21] = 
{
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_isAbstract_13(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_hasAbstractAttribute_14(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_isNillable_15(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_hasNillableAttribute_16(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_isLocalTypeDerivationChecked_17(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_block_18(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_final_19(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_form_20(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_defaultValue_21(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_fixedValue_22(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_name_23(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_refName_24(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_substitutionGroup_25(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_typeName_26(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_type_27(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_qualifiedName_28(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_elementType_29(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_blockResolved_30(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_finalResolved_31(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_constraints_32(),
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D::get_offset_of_elementDecl_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[7] = 
{
	XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65::get_offset_of_res_17(),
	XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65::get_offset_of_args_18(),
	XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65::get_offset_of_sourceUri_19(),
	XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65::get_offset_of_lineNumber_20(),
	XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65::get_offset_of_linePosition_21(),
	XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65::get_offset_of_sourceSchemaObject_22(),
	XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65::get_offset_of_message_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[6] = 
{
	XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8::get_offset_of_location_6(),
	XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8::get_offset_of_baseUri_7(),
	XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8::get_offset_of_schema_8(),
	XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8::get_offset_of_id_9(),
	XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8::get_offset_of_moreAttributes_10(),
	XmlSchemaExternal_tE975A15EAF7FEB0CD500DB274FF6FD106DC517B8::get_offset_of_compositor_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (FacetType_tBB711036F122A1F3BB7E820E16214966B55A2BD9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2141[14] = 
{
	FacetType_tBB711036F122A1F3BB7E820E16214966B55A2BD9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[3] = 
{
	XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250::get_offset_of_value_9(),
	XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250::get_offset_of_isFixed_10(),
	XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250::get_offset_of_facetType_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (XmlSchemaNumericFacet_t42FE2DAD05E74AB5BABE14667C3182A2DAFC91B0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (XmlSchemaLengthFacet_t0477816E8D01018C0C88C46CFCB8460D565A62DC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (XmlSchemaMinLengthFacet_t19B86AE9CF33EFEB8E9E8ECBB01893D05D041ACA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (XmlSchemaMaxLengthFacet_tBE5D5CE2A057904C1CB23D397238D6987D2642BD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (XmlSchemaPatternFacet_t0FE0B38A1CCE96CD76792E0662B892BA630FE322), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (XmlSchemaEnumerationFacet_t9C81E3524A074C89B09B47FF1406E593F4222414), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (XmlSchemaMinExclusiveFacet_t55185193083B2ED3C1DD747C19C211C86868C155), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (XmlSchemaMinInclusiveFacet_t5E3430DB39E45DA9261CA83D762560C0FFECF02F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (XmlSchemaMaxExclusiveFacet_t68766F0FFC4EE44E09AA2B9EE90C31DA1A795F70), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (XmlSchemaMaxInclusiveFacet_t90C4E58720B588EA452F6305E8DF7C06F2EFD209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (XmlSchemaTotalDigitsFacet_tE1E389B72A9CFC03156A4024ACDDEFF2B1D03AB7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (XmlSchemaFractionDigitsFacet_t4AA8D1C0E9B2296F740AD61B9621F6C743A06D5B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (XmlSchemaWhiteSpaceFacet_tD0BAD46B4C0A014ADC9A796049E24E55F27F4ADF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (XmlSchemaForm_t86E3CD541907A35AC5B505EF118C3E20CC760276)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2156[4] = 
{
	XmlSchemaForm_t86E3CD541907A35AC5B505EF118C3E20CC760276::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2157[6] = 
{
	XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2::get_offset_of_name_9(),
	XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2::get_offset_of_particle_10(),
	XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2::get_offset_of_canonicalParticle_11(),
	XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2::get_offset_of_qname_12(),
	XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2::get_offset_of_redefined_13(),
	XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2::get_offset_of_selfReferenceCount_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (XmlSchemaGroupBase_tC18B7AAFF241664404A1FB5D936508AB2F4D208F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (XmlSchemaGroupRef_tEC541A27220ABE0F02B3B1FD72730E536FAEF6F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2159[3] = 
{
	XmlSchemaGroupRef_tEC541A27220ABE0F02B3B1FD72730E536FAEF6F3::get_offset_of_refName_13(),
	XmlSchemaGroupRef_tEC541A27220ABE0F02B3B1FD72730E536FAEF6F3::get_offset_of_particle_14(),
	XmlSchemaGroupRef_tEC541A27220ABE0F02B3B1FD72730E536FAEF6F3::get_offset_of_refined_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2160[5] = 
{
	XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95::get_offset_of_name_9(),
	XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95::get_offset_of_selector_10(),
	XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95::get_offset_of_fields_11(),
	XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95::get_offset_of_qualifiedName_12(),
	XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95::get_offset_of_compiledConstraint_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (XmlSchemaXPath_t6D090E4243F2A15A6A5E4EF5B17B0740711C6C34), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2161[1] = 
{
	XmlSchemaXPath_t6D090E4243F2A15A6A5E4EF5B17B0740711C6C34::get_offset_of_xpath_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (XmlSchemaUnique_t23A00CD203938B26F6C292A874981930DD989609), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (XmlSchemaKey_tF6EC3BA3F09B23BE769F5B5EB815B71F2A6AD672), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (XmlSchemaKeyref_tA6761A34F81934FE376DCA84EDC7094325D0FB82), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[1] = 
{
	XmlSchemaKeyref_tA6761A34F81934FE376DCA84EDC7094325D0FB82::get_offset_of_refer_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (XmlSchemaImport_t4FC11FF359600C711AFFEF2DC569810456D8CC93), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[2] = 
{
	XmlSchemaImport_t4FC11FF359600C711AFFEF2DC569810456D8CC93::get_offset_of_ns_12(),
	XmlSchemaImport_t4FC11FF359600C711AFFEF2DC569810456D8CC93::get_offset_of_annotation_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (XmlSchemaInclude_tD337A52B31C9B3195CE4272AF057E286CDD34001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[1] = 
{
	XmlSchemaInclude_tD337A52B31C9B3195CE4272AF057E286CDD34001::get_offset_of_annotation_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[8] = 
{
	XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035::get_offset_of_isDefault_0(),
	XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035::get_offset_of_isNil_1(),
	XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035::get_offset_of_schemaElement_2(),
	XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035::get_offset_of_schemaAttribute_3(),
	XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035::get_offset_of_schemaType_4(),
	XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035::get_offset_of_memberType_5(),
	XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035::get_offset_of_validity_6(),
	XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035::get_offset_of_contentType_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (XmlSchemaNotation_tA67925FC817C85C8CB753EBAFBB7EA0AE414D41D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[4] = 
{
	XmlSchemaNotation_tA67925FC817C85C8CB753EBAFBB7EA0AE414D41D::get_offset_of_name_9(),
	XmlSchemaNotation_tA67925FC817C85C8CB753EBAFBB7EA0AE414D41D::get_offset_of_publicId_10(),
	XmlSchemaNotation_tA67925FC817C85C8CB753EBAFBB7EA0AE414D41D::get_offset_of_systemId_11(),
	XmlSchemaNotation_tA67925FC817C85C8CB753EBAFBB7EA0AE414D41D::get_offset_of_qname_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[6] = 
{
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B::get_offset_of_lineNum_0(),
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B::get_offset_of_linePos_1(),
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B::get_offset_of_sourceUri_2(),
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B::get_offset_of_namespaces_3(),
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B::get_offset_of_parent_4(),
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B::get_offset_of_isProcessing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[1] = 
{
	XmlSchemaObjectCollection_t6DCC2B614786CE467B1572B384A163DEE72F1280::get_offset_of_parent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (XmlSchemaObjectEnumerator_tE349F7DC1E12665FA0F97D180BF02FA949D909C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[1] = 
{
	XmlSchemaObjectEnumerator_tE349F7DC1E12665FA0F97D180BF02FA949D909C7::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[2] = 
{
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833::get_offset_of_table_0(),
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833::get_offset_of_entries_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (EnumeratorType_t123506A6BEEC590B086DA6B4EBBD1CEA328A30A0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2173[4] = 
{
	EnumeratorType_t123506A6BEEC590B086DA6B4EBBD1CEA328A30A0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (XmlSchemaObjectEntry_tD7A5D31C794A4D04759882DDAD01103D2C19D63B)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2174[2] = 
{
	XmlSchemaObjectEntry_tD7A5D31C794A4D04759882DDAD01103D2C19D63B::get_offset_of_qname_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XmlSchemaObjectEntry_tD7A5D31C794A4D04759882DDAD01103D2C19D63B::get_offset_of_xso_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (ValuesCollection_tE5CD9602867B56FB89416007CBBFF4C6950771C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2175[2] = 
{
	ValuesCollection_tE5CD9602867B56FB89416007CBBFF4C6950771C5::get_offset_of_entries_0(),
	ValuesCollection_tE5CD9602867B56FB89416007CBBFF4C6950771C5::get_offset_of_size_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (XSOEnumerator_t0BDA5BB56D78F4B2F028B2831622DC3336011D8E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[6] = 
{
	XSOEnumerator_t0BDA5BB56D78F4B2F028B2831622DC3336011D8E::get_offset_of_entries_0(),
	XSOEnumerator_t0BDA5BB56D78F4B2F028B2831622DC3336011D8E::get_offset_of_enumType_1(),
	XSOEnumerator_t0BDA5BB56D78F4B2F028B2831622DC3336011D8E::get_offset_of_currentIndex_2(),
	XSOEnumerator_t0BDA5BB56D78F4B2F028B2831622DC3336011D8E::get_offset_of_size_3(),
	XSOEnumerator_t0BDA5BB56D78F4B2F028B2831622DC3336011D8E::get_offset_of_currentKey_4(),
	XSOEnumerator_t0BDA5BB56D78F4B2F028B2831622DC3336011D8E::get_offset_of_currentValue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (XSODictionaryEnumerator_t3241C9CB5FE5DFBC1D82B0CF43CED73E699AF92B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671), -1, sizeof(XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2178[4] = 
{
	XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671::get_offset_of_minOccurs_9(),
	XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671::get_offset_of_maxOccurs_10(),
	XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671::get_offset_of_flags_11(),
	XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671_StaticFields::get_offset_of_Empty_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (Occurs_t6B3513E522B1DF0DB690037276296BB18F81BCE0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2179[4] = 
{
	Occurs_t6B3513E522B1DF0DB690037276296BB18F81BCE0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (EmptyParticle_t4D9B96A627867C8839845CBD83183C9CD3ECE202), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[4] = 
{
	XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B::get_offset_of_items_12(),
	XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B::get_offset_of_attributeGroups_13(),
	XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B::get_offset_of_types_14(),
	XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B::get_offset_of_groups_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (XmlSchemaSequence_tBF8498445C0FD553EF64CC240B1236CEB83FB215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[1] = 
{
	XmlSchemaSequence_tBF8498445C0FD553EF64CC240B1236CEB83FB215::get_offset_of_items_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2183[20] = 
{
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_nameTable_0(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_schemaNames_1(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_schemas_2(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_internalEventHandler_3(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_eventHandler_4(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_isCompiled_5(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_schemaLocations_6(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_chameleonSchemas_7(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_targetNamespaces_8(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_compileAll_9(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_cachedCompiledInfo_10(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_readerSettings_11(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_schemaForSchema_12(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_compilationSettings_13(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_elements_14(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_attributes_15(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_schemaTypes_16(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_substitutionGroups_17(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_typeExtensions_18(),
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F::get_offset_of_internalSyncObject_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (XmlSchemaSimpleContent_t47CF5CDB950EC1B3CB066BA2F9E3DAE993AD3550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2184[1] = 
{
	XmlSchemaSimpleContent_t47CF5CDB950EC1B3CB066BA2F9E3DAE993AD3550::get_offset_of_content_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (XmlSchemaSimpleContentExtension_t235FC077FDE8C8796FA6A961658C42D60F4CB812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2185[3] = 
{
	XmlSchemaSimpleContentExtension_t235FC077FDE8C8796FA6A961658C42D60F4CB812::get_offset_of_attributes_9(),
	XmlSchemaSimpleContentExtension_t235FC077FDE8C8796FA6A961658C42D60F4CB812::get_offset_of_anyAttribute_10(),
	XmlSchemaSimpleContentExtension_t235FC077FDE8C8796FA6A961658C42D60F4CB812::get_offset_of_baseTypeName_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[5] = 
{
	XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621::get_offset_of_baseTypeName_9(),
	XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621::get_offset_of_baseType_10(),
	XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621::get_offset_of_facets_11(),
	XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621::get_offset_of_attributes_12(),
	XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621::get_offset_of_anyAttribute_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[1] = 
{
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A::get_offset_of_content_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (XmlSchemaSimpleTypeContent_t87EF2A2F4B2F3D7827EA18B0FCB8393EA0201A3E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (XmlSchemaSimpleTypeList_tE5804144CB4035DE27AA3C43233579A075469403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[3] = 
{
	XmlSchemaSimpleTypeList_tE5804144CB4035DE27AA3C43233579A075469403::get_offset_of_itemTypeName_9(),
	XmlSchemaSimpleTypeList_tE5804144CB4035DE27AA3C43233579A075469403::get_offset_of_itemType_10(),
	XmlSchemaSimpleTypeList_tE5804144CB4035DE27AA3C43233579A075469403::get_offset_of_baseItemType_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (XmlSchemaSimpleTypeRestriction_t948C7680F4D262DA9DA7590CCA8E2F96078E490C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[3] = 
{
	XmlSchemaSimpleTypeRestriction_t948C7680F4D262DA9DA7590CCA8E2F96078E490C::get_offset_of_baseTypeName_9(),
	XmlSchemaSimpleTypeRestriction_t948C7680F4D262DA9DA7590CCA8E2F96078E490C::get_offset_of_baseType_10(),
	XmlSchemaSimpleTypeRestriction_t948C7680F4D262DA9DA7590CCA8E2F96078E490C::get_offset_of_facets_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (XmlSchemaSimpleTypeUnion_t3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[3] = 
{
	XmlSchemaSimpleTypeUnion_t3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A::get_offset_of_baseTypes_9(),
	XmlSchemaSimpleTypeUnion_t3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A::get_offset_of_memberTypes_10(),
	XmlSchemaSimpleTypeUnion_t3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A::get_offset_of_baseMemberTypes_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (XmlSchemaSubstitutionGroup_tA063228E2E8787AD4326048660AF77CEA08419E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[2] = 
{
	XmlSchemaSubstitutionGroup_tA063228E2E8787AD4326048660AF77CEA08419E2::get_offset_of_membersList_6(),
	XmlSchemaSubstitutionGroup_tA063228E2E8787AD4326048660AF77CEA08419E2::get_offset_of_examplar_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (XmlSchemaSubstitutionGroupV1Compat_tAE2B7C8AB4A0DA4E3FEBC99A018C344733A1FC54), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[1] = 
{
	XmlSchemaSubstitutionGroupV1Compat_tAE2B7C8AB4A0DA4E3FEBC99A018C344733A1FC54::get_offset_of_choice_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2194[10] = 
{
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9::get_offset_of_name_9(),
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9::get_offset_of_final_10(),
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9::get_offset_of_derivedBy_11(),
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9::get_offset_of_baseSchemaType_12(),
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9::get_offset_of_datatype_13(),
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9::get_offset_of_finalResolved_14(),
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9::get_offset_of_elementDecl_15(),
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9::get_offset_of_qname_16(),
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9::get_offset_of_redefined_17(),
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9::get_offset_of_contentType_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (XmlSchemaUse_t69A028A548128D3041B789FCCCD1BEDCE2365BF3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2195[5] = 
{
	XmlSchemaUse_t69A028A548128D3041B789FCCCD1BEDCE2365BF3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (XmlSchemaValidationException_t5620ED68BF1780277A4C7E1CA022874903501A9F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (XmlValueGetter_tE51E83B894C27BA5CD229FD9E8C5FC9C851555DF), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (XmlSchemaValidationFlags_t866C695263C623EE4F1746A541248AE12D311A73)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2198[7] = 
{
	XmlSchemaValidationFlags_t866C695263C623EE4F1746A541248AE12D311A73::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (ValidatorState_tBFB3D6A7BF982DA0A31422C4EAAA1631AF81EE55)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2199[13] = 
{
	ValidatorState_tBFB3D6A7BF982DA0A31422C4EAAA1631AF81EE55::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
