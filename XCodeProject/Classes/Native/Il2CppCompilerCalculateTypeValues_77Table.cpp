﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// GLTF.Math.Matrix4x4[]
struct Matrix4x4U5BU5D_tE3A13CB78E1B2CBDCD537180E7C6D83E18B0D9F0;
// GLTF.Schema.AccessorId
struct AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F;
// GLTF.Schema.BufferId
struct BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4;
// GLTF.Schema.GLTFBuffer
struct GLTFBuffer_t093686A8CEC027B3EFCCCC89EF874F3BE6738CA1;
// GLTF.Schema.GLTFImage
struct GLTFImage_tA3F1517270CE1DC26739C076F757BFA7B651A4EE;
// GLTF.Schema.GLTFMaterial
struct GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99;
// GLTF.Schema.GLTFMesh
struct GLTFMesh_tD3F424D9F92A52F43406967EA5B985F2735DD1AD;
// GLTF.Schema.GLTFRoot
struct GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B;
// GLTF.Schema.GLTFScene
struct GLTFScene_t668D116E319247178F4641B2F3A4DF33C6343BB6;
// GLTF.Schema.GLTFTexture
struct GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65;
// GLTF.Schema.KHR_materials_pbrSpecularGlossinessExtension
struct KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0;
// GLTF.Schema.MSFT_LODExtension
struct MSFT_LODExtension_tA2000797F265C91522ED3917FEB2078117995805;
// GLTF.Schema.MeshId
struct MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231;
// GLTF.Schema.MeshPrimitive
struct MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356;
// GLTF.Schema.Node
struct Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744;
// GLTF.Schema.NodeId
struct NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0;
// GLTF.Schema.PbrMetallicRoughness
struct PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2;
// GLTF.Schema.Skin
struct Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34;
// GLTF.Schema.TextureId
struct TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91;
// MHLab.PATCH.Settings.Settings
struct Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Collections.Generic.IEnumerable`1<UnityThreading.Task>>
struct Action_1_tB6756C6173D7091207B2AA045DC646FB963CF826;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.Action`1<UnityThreading.ActionThread>
struct Action_1_tE3217F0296A761BE816CF81030F2F771090C1B77;
// System.Action`1<UnityThreading.Task>
struct Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F;
// System.Action`2<UnityEngine.GameObject,System.Runtime.ExceptionServices.ExceptionDispatchInfo>
struct Action_2_tA8345456806B324BA9E2C0C4E51F157CE28878B6;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,GLTF.AttributeAccessor>
struct Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C;
// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>
struct Dictionary_2_tFA101F0B1ABA5C9CC83DEA8915A01ED1E64CB071;
// System.Collections.Generic.Dictionary`2<UnityEngine.Mesh,GLTF.Schema.MeshPrimitive[]>
struct Dictionary_2_t78D4C371AE4A9140F690CC6F2DAD6EF1F9112E6E;
// System.Collections.Generic.Dictionary`2<UnityGLTF.GLTFSceneExporter/PrimKey,GLTF.Schema.MeshId>
struct Dictionary_2_t2E8AF592CF42256B846A321987E87A9C492F77DF;
// System.Collections.Generic.List`1<GLTF.Schema.NodeId>
struct List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA;
// System.Collections.Generic.List`1<System.Double>
struct List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t6A61046573B0BC4E12950B90305C189DD041D786;
// System.Collections.Generic.List`1<UnityEngine.Texture>
struct List_1_t8D53F1C83706ED0744EE7BFB70B1BE2766E7A3F8;
// System.Collections.Generic.List`1<UnityGLTF.GLTFSceneExporter/ImageInfo>
struct List_1_tAEAEA85CE01C1D254EA310AD318B239C3A1064AE;
// System.Collections.Generic.List`1<UnityThreading.Task>
struct List_1_t2ABFD0F79D77AAB47F6C9E0EB34B82B905850368;
// System.Collections.Generic.Queue`1<UnityGLTF.AsyncCoroutineHelper/CoroutineInfo>
struct Queue_1_tBD9DB1D533A027BD4D96A9F03A0DCC1D51A45497;
// System.Collections.Generic.Queue`1<UnityThreading.Task>
struct Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`2<UnityThreading.TaskWorker,System.Boolean>
struct Func_2_t1974F937036431FDCBE8C6785949DE4EB35D8153;
// System.Func`2<UnityThreading.ThreadBase,System.Collections.IEnumerator>
struct Func_2_t8ADD2DE3DB0FFA6F3FB7AAD84397C6D7A1132827;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.BinaryWriter
struct BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_tEFDFBE18E061A6065AB2FF735F1425FB59F919BC;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Threading.ManualResetEvent
struct ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7;
// System.Threading.Tasks.Task
struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2;
// System.Threading.Tasks.TaskCompletionSource`1<System.Boolean>
struct TaskCompletionSource_1_t99FE296FC744F35118789383C0C0F3156E38E240;
// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>
struct Task_1_t1359D75350E9D976BFA28AD96E417450DE277673;
// System.Threading.Tasks.Task`1<UnityEngine.Material>
struct Task_1_t74879D22F3CC6429D4D6FCC94A7597CE737CB1DA;
// System.Threading.Tasks.Task`1<UnityGLTF.UnityMeshData>
struct Task_1_t44C0E338925FFB530BE82127BDA04A6421348967;
// System.Threading.Thread
struct Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.BoneWeight[]
struct BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.LODGroup
struct LODGroup_t2B5F4CADAA263141AC8EEABB56E6C73B057C6892;
// UnityEngine.LOD[]
struct LODU5BU5D_tF08898FC223F367F3F9154401FC6913759C62D28;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Transform[]
struct TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;
// UnityEngine.WaitUntil
struct WaitUntil_t012561515C0E1D3DEA19DB3A05444B020C68E13F;
// UnityGLTF.AsyncCoroutineHelper
struct AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1;
// UnityGLTF.Cache.AssetCache
struct AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350;
// UnityGLTF.Cache.MaterialCacheData
struct MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1;
// UnityGLTF.GLTFComponent
struct GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392;
// UnityGLTF.GLTFSceneExporter/RetrieveTexturePathDelegate
struct RetrieveTexturePathDelegate_tFFE02629D2BCA6A3D7674D8797915C30126F3A6B;
// UnityGLTF.GLTFSceneImporter
struct GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854;
// UnityGLTF.GLTFSceneImporter/<>c__DisplayClass50_0
struct U3CU3Ec__DisplayClass50_0_tDCB32992B37184B2BAAD02F93BC74DB03178998E;
// UnityGLTF.GLTFSceneImporter/<>c__DisplayClass52_0
struct U3CU3Ec__DisplayClass52_0_tD36628E62AE9ACC2EE3B3744FB9FAFEFC2CFFCCA;
// UnityGLTF.GLTFSceneImporter/<>c__DisplayClass73_0
struct U3CU3Ec__DisplayClass73_0_tD494223E41D1BFA7571AC3568E9A99A8B3FED886;
// UnityGLTF.IMetalRoughUniformMap
struct IMetalRoughUniformMap_t101D9CAFA327C77D6B00B168C874CCBEBC1F3CD2;
// UnityGLTF.ISpecGlossUniformMap
struct ISpecGlossUniformMap_t916F97887BAE2FE2702F63F9200C2C26B055BE12;
// UnityGLTF.IUniformMap
struct IUniformMap_tED6F1676BB0EC82597108000C52EAD2DAA0C596E;
// UnityGLTF.Loader.ILoader
struct ILoader_tADAD26ECAE6A56C30083D1ECF482BB9B59F55371;
// UnityGLTF.RootMergeComponent
struct RootMergeComponent_tB6DE16CB123C2A75D3C0D4818469BF644203A29B;
// UnityGLTF.UnityMeshData
struct UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D;
// UnityThreading.Dispatcher
struct Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297;
// UnityThreading.DispatcherBase
struct DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578;
// UnityThreading.Task
struct Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F;
// UnityThreading.TaskDistributor
struct TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D;
// UnityThreading.TaskExtension/<>c__DisplayClass17_0
struct U3CU3Ec__DisplayClass17_0_t07C559EC67E78265374C7C87DB093CB50CD821DE;
// UnityThreading.TaskExtension/<>c__DisplayClass38_0
struct U3CU3Ec__DisplayClass38_0_t955423008B58DF24EFBD1C23E7B318A22B15107F;
// UnityThreading.TaskWorker[]
struct TaskWorkerU5BU5D_tF86436D11289B2B862F108DC08B7EC27F6280F5A;
// UnityThreading.ThreadBase
struct ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef GLTFSCENEEXPORTER_T76602B52640298A65167CB64B36993C9E0B5AE21_H
#define GLTFSCENEEXPORTER_T76602B52640298A65167CB64B36993C9E0B5AE21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneExporter
struct  GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21  : public RuntimeObject
{
public:
	// UnityEngine.Transform[] UnityGLTF.GLTFSceneExporter::_rootTransforms
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* ____rootTransforms_0;
	// GLTF.Schema.GLTFRoot UnityGLTF.GLTFSceneExporter::_root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ____root_1;
	// GLTF.Schema.BufferId UnityGLTF.GLTFSceneExporter::_bufferId
	BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4 * ____bufferId_2;
	// GLTF.Schema.GLTFBuffer UnityGLTF.GLTFSceneExporter::_buffer
	GLTFBuffer_t093686A8CEC027B3EFCCCC89EF874F3BE6738CA1 * ____buffer_3;
	// System.IO.BinaryWriter UnityGLTF.GLTFSceneExporter::_bufferWriter
	BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * ____bufferWriter_4;
	// System.Collections.Generic.List`1<UnityGLTF.GLTFSceneExporter/ImageInfo> UnityGLTF.GLTFSceneExporter::_imageInfos
	List_1_tAEAEA85CE01C1D254EA310AD318B239C3A1064AE * ____imageInfos_5;
	// System.Collections.Generic.List`1<UnityEngine.Texture> UnityGLTF.GLTFSceneExporter::_textures
	List_1_t8D53F1C83706ED0744EE7BFB70B1BE2766E7A3F8 * ____textures_6;
	// System.Collections.Generic.List`1<UnityEngine.Material> UnityGLTF.GLTFSceneExporter::_materials
	List_1_t6A61046573B0BC4E12950B90305C189DD041D786 * ____materials_7;
	// System.Boolean UnityGLTF.GLTFSceneExporter::_shouldUseInternalBufferForImages
	bool ____shouldUseInternalBufferForImages_8;
	// UnityGLTF.GLTFSceneExporter/RetrieveTexturePathDelegate UnityGLTF.GLTFSceneExporter::_retrieveTexturePathDelegate
	RetrieveTexturePathDelegate_tFFE02629D2BCA6A3D7674D8797915C30126F3A6B * ____retrieveTexturePathDelegate_9;
	// UnityEngine.Material UnityGLTF.GLTFSceneExporter::_metalGlossChannelSwapMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____metalGlossChannelSwapMaterial_10;
	// UnityEngine.Material UnityGLTF.GLTFSceneExporter::_normalChannelMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____normalChannelMaterial_11;
	// System.Collections.Generic.Dictionary`2<UnityGLTF.GLTFSceneExporter/PrimKey,GLTF.Schema.MeshId> UnityGLTF.GLTFSceneExporter::_primOwner
	Dictionary_2_t2E8AF592CF42256B846A321987E87A9C492F77DF * ____primOwner_18;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Mesh,GLTF.Schema.MeshPrimitive[]> UnityGLTF.GLTFSceneExporter::_meshToPrims
	Dictionary_2_t78D4C371AE4A9140F690CC6F2DAD6EF1F9112E6E * ____meshToPrims_19;

public:
	inline static int32_t get_offset_of__rootTransforms_0() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21, ____rootTransforms_0)); }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* get__rootTransforms_0() const { return ____rootTransforms_0; }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804** get_address_of__rootTransforms_0() { return &____rootTransforms_0; }
	inline void set__rootTransforms_0(TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* value)
	{
		____rootTransforms_0 = value;
		Il2CppCodeGenWriteBarrier((&____rootTransforms_0), value);
	}

	inline static int32_t get_offset_of__root_1() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21, ____root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get__root_1() const { return ____root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of__root_1() { return &____root_1; }
	inline void set__root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		____root_1 = value;
		Il2CppCodeGenWriteBarrier((&____root_1), value);
	}

	inline static int32_t get_offset_of__bufferId_2() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21, ____bufferId_2)); }
	inline BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4 * get__bufferId_2() const { return ____bufferId_2; }
	inline BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4 ** get_address_of__bufferId_2() { return &____bufferId_2; }
	inline void set__bufferId_2(BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4 * value)
	{
		____bufferId_2 = value;
		Il2CppCodeGenWriteBarrier((&____bufferId_2), value);
	}

	inline static int32_t get_offset_of__buffer_3() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21, ____buffer_3)); }
	inline GLTFBuffer_t093686A8CEC027B3EFCCCC89EF874F3BE6738CA1 * get__buffer_3() const { return ____buffer_3; }
	inline GLTFBuffer_t093686A8CEC027B3EFCCCC89EF874F3BE6738CA1 ** get_address_of__buffer_3() { return &____buffer_3; }
	inline void set__buffer_3(GLTFBuffer_t093686A8CEC027B3EFCCCC89EF874F3BE6738CA1 * value)
	{
		____buffer_3 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_3), value);
	}

	inline static int32_t get_offset_of__bufferWriter_4() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21, ____bufferWriter_4)); }
	inline BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * get__bufferWriter_4() const { return ____bufferWriter_4; }
	inline BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 ** get_address_of__bufferWriter_4() { return &____bufferWriter_4; }
	inline void set__bufferWriter_4(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * value)
	{
		____bufferWriter_4 = value;
		Il2CppCodeGenWriteBarrier((&____bufferWriter_4), value);
	}

	inline static int32_t get_offset_of__imageInfos_5() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21, ____imageInfos_5)); }
	inline List_1_tAEAEA85CE01C1D254EA310AD318B239C3A1064AE * get__imageInfos_5() const { return ____imageInfos_5; }
	inline List_1_tAEAEA85CE01C1D254EA310AD318B239C3A1064AE ** get_address_of__imageInfos_5() { return &____imageInfos_5; }
	inline void set__imageInfos_5(List_1_tAEAEA85CE01C1D254EA310AD318B239C3A1064AE * value)
	{
		____imageInfos_5 = value;
		Il2CppCodeGenWriteBarrier((&____imageInfos_5), value);
	}

	inline static int32_t get_offset_of__textures_6() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21, ____textures_6)); }
	inline List_1_t8D53F1C83706ED0744EE7BFB70B1BE2766E7A3F8 * get__textures_6() const { return ____textures_6; }
	inline List_1_t8D53F1C83706ED0744EE7BFB70B1BE2766E7A3F8 ** get_address_of__textures_6() { return &____textures_6; }
	inline void set__textures_6(List_1_t8D53F1C83706ED0744EE7BFB70B1BE2766E7A3F8 * value)
	{
		____textures_6 = value;
		Il2CppCodeGenWriteBarrier((&____textures_6), value);
	}

	inline static int32_t get_offset_of__materials_7() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21, ____materials_7)); }
	inline List_1_t6A61046573B0BC4E12950B90305C189DD041D786 * get__materials_7() const { return ____materials_7; }
	inline List_1_t6A61046573B0BC4E12950B90305C189DD041D786 ** get_address_of__materials_7() { return &____materials_7; }
	inline void set__materials_7(List_1_t6A61046573B0BC4E12950B90305C189DD041D786 * value)
	{
		____materials_7 = value;
		Il2CppCodeGenWriteBarrier((&____materials_7), value);
	}

	inline static int32_t get_offset_of__shouldUseInternalBufferForImages_8() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21, ____shouldUseInternalBufferForImages_8)); }
	inline bool get__shouldUseInternalBufferForImages_8() const { return ____shouldUseInternalBufferForImages_8; }
	inline bool* get_address_of__shouldUseInternalBufferForImages_8() { return &____shouldUseInternalBufferForImages_8; }
	inline void set__shouldUseInternalBufferForImages_8(bool value)
	{
		____shouldUseInternalBufferForImages_8 = value;
	}

	inline static int32_t get_offset_of__retrieveTexturePathDelegate_9() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21, ____retrieveTexturePathDelegate_9)); }
	inline RetrieveTexturePathDelegate_tFFE02629D2BCA6A3D7674D8797915C30126F3A6B * get__retrieveTexturePathDelegate_9() const { return ____retrieveTexturePathDelegate_9; }
	inline RetrieveTexturePathDelegate_tFFE02629D2BCA6A3D7674D8797915C30126F3A6B ** get_address_of__retrieveTexturePathDelegate_9() { return &____retrieveTexturePathDelegate_9; }
	inline void set__retrieveTexturePathDelegate_9(RetrieveTexturePathDelegate_tFFE02629D2BCA6A3D7674D8797915C30126F3A6B * value)
	{
		____retrieveTexturePathDelegate_9 = value;
		Il2CppCodeGenWriteBarrier((&____retrieveTexturePathDelegate_9), value);
	}

	inline static int32_t get_offset_of__metalGlossChannelSwapMaterial_10() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21, ____metalGlossChannelSwapMaterial_10)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__metalGlossChannelSwapMaterial_10() const { return ____metalGlossChannelSwapMaterial_10; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__metalGlossChannelSwapMaterial_10() { return &____metalGlossChannelSwapMaterial_10; }
	inline void set__metalGlossChannelSwapMaterial_10(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____metalGlossChannelSwapMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&____metalGlossChannelSwapMaterial_10), value);
	}

	inline static int32_t get_offset_of__normalChannelMaterial_11() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21, ____normalChannelMaterial_11)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__normalChannelMaterial_11() const { return ____normalChannelMaterial_11; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__normalChannelMaterial_11() { return &____normalChannelMaterial_11; }
	inline void set__normalChannelMaterial_11(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____normalChannelMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((&____normalChannelMaterial_11), value);
	}

	inline static int32_t get_offset_of__primOwner_18() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21, ____primOwner_18)); }
	inline Dictionary_2_t2E8AF592CF42256B846A321987E87A9C492F77DF * get__primOwner_18() const { return ____primOwner_18; }
	inline Dictionary_2_t2E8AF592CF42256B846A321987E87A9C492F77DF ** get_address_of__primOwner_18() { return &____primOwner_18; }
	inline void set__primOwner_18(Dictionary_2_t2E8AF592CF42256B846A321987E87A9C492F77DF * value)
	{
		____primOwner_18 = value;
		Il2CppCodeGenWriteBarrier((&____primOwner_18), value);
	}

	inline static int32_t get_offset_of__meshToPrims_19() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21, ____meshToPrims_19)); }
	inline Dictionary_2_t78D4C371AE4A9140F690CC6F2DAD6EF1F9112E6E * get__meshToPrims_19() const { return ____meshToPrims_19; }
	inline Dictionary_2_t78D4C371AE4A9140F690CC6F2DAD6EF1F9112E6E ** get_address_of__meshToPrims_19() { return &____meshToPrims_19; }
	inline void set__meshToPrims_19(Dictionary_2_t78D4C371AE4A9140F690CC6F2DAD6EF1F9112E6E * value)
	{
		____meshToPrims_19 = value;
		Il2CppCodeGenWriteBarrier((&____meshToPrims_19), value);
	}
};

struct GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21_StaticFields
{
public:
	// System.Boolean UnityGLTF.GLTFSceneExporter::ExportNames
	bool ___ExportNames_20;
	// System.Boolean UnityGLTF.GLTFSceneExporter::ExportFullPath
	bool ___ExportFullPath_21;
	// System.Boolean UnityGLTF.GLTFSceneExporter::RequireExtensions
	bool ___RequireExtensions_22;

public:
	inline static int32_t get_offset_of_ExportNames_20() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21_StaticFields, ___ExportNames_20)); }
	inline bool get_ExportNames_20() const { return ___ExportNames_20; }
	inline bool* get_address_of_ExportNames_20() { return &___ExportNames_20; }
	inline void set_ExportNames_20(bool value)
	{
		___ExportNames_20 = value;
	}

	inline static int32_t get_offset_of_ExportFullPath_21() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21_StaticFields, ___ExportFullPath_21)); }
	inline bool get_ExportFullPath_21() const { return ___ExportFullPath_21; }
	inline bool* get_address_of_ExportFullPath_21() { return &___ExportFullPath_21; }
	inline void set_ExportFullPath_21(bool value)
	{
		___ExportFullPath_21 = value;
	}

	inline static int32_t get_offset_of_RequireExtensions_22() { return static_cast<int32_t>(offsetof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21_StaticFields, ___RequireExtensions_22)); }
	inline bool get_RequireExtensions_22() const { return ___RequireExtensions_22; }
	inline bool* get_address_of_RequireExtensions_22() { return &___RequireExtensions_22; }
	inline void set_RequireExtensions_22(bool value)
	{
		___RequireExtensions_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFSCENEEXPORTER_T76602B52640298A65167CB64B36993C9E0B5AE21_H
#ifndef U3CU3EC__DISPLAYCLASS50_0_TDCB32992B37184B2BAAD02F93BC74DB03178998E_H
#define U3CU3EC__DISPLAYCLASS50_0_TDCB32992B37184B2BAAD02F93BC74DB03178998E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<>c__DisplayClass50_0
struct  U3CU3Ec__DisplayClass50_0_tDCB32992B37184B2BAAD02F93BC74DB03178998E  : public RuntimeObject
{
public:
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<>c__DisplayClass50_0::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_0;
	// System.String UnityGLTF.GLTFSceneImporter/<>c__DisplayClass50_0::jsonFilePath
	String_t* ___jsonFilePath_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass50_0_tDCB32992B37184B2BAAD02F93BC74DB03178998E, ___U3CU3E4__this_0)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_jsonFilePath_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass50_0_tDCB32992B37184B2BAAD02F93BC74DB03178998E, ___jsonFilePath_1)); }
	inline String_t* get_jsonFilePath_1() const { return ___jsonFilePath_1; }
	inline String_t** get_address_of_jsonFilePath_1() { return &___jsonFilePath_1; }
	inline void set_jsonFilePath_1(String_t* value)
	{
		___jsonFilePath_1 = value;
		Il2CppCodeGenWriteBarrier((&___jsonFilePath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS50_0_TDCB32992B37184B2BAAD02F93BC74DB03178998E_H
#ifndef U3CU3EC__DISPLAYCLASS50_1_TD902C284B85AC38CBAFD4438F41781C6F529D180_H
#define U3CU3EC__DISPLAYCLASS50_1_TD902C284B85AC38CBAFD4438F41781C6F529D180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<>c__DisplayClass50_1
struct  U3CU3Ec__DisplayClass50_1_tD902C284B85AC38CBAFD4438F41781C6F529D180  : public RuntimeObject
{
public:
	// System.Threading.Thread UnityGLTF.GLTFSceneImporter/<>c__DisplayClass50_1::loadThread
	Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * ___loadThread_0;

public:
	inline static int32_t get_offset_of_loadThread_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass50_1_tD902C284B85AC38CBAFD4438F41781C6F529D180, ___loadThread_0)); }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * get_loadThread_0() const { return ___loadThread_0; }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 ** get_address_of_loadThread_0() { return &___loadThread_0; }
	inline void set_loadThread_0(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * value)
	{
		___loadThread_0 = value;
		Il2CppCodeGenWriteBarrier((&___loadThread_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS50_1_TD902C284B85AC38CBAFD4438F41781C6F529D180_H
#ifndef U3CU3EC__DISPLAYCLASS50_2_T94962D31C36BB849562C26FA6523CE2F18F5C5C4_H
#define U3CU3EC__DISPLAYCLASS50_2_T94962D31C36BB849562C26FA6523CE2F18F5C5C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<>c__DisplayClass50_2
struct  U3CU3Ec__DisplayClass50_2_t94962D31C36BB849562C26FA6523CE2F18F5C5C4  : public RuntimeObject
{
public:
	// System.Threading.Thread UnityGLTF.GLTFSceneImporter/<>c__DisplayClass50_2::parseJsonThread
	Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * ___parseJsonThread_0;

public:
	inline static int32_t get_offset_of_parseJsonThread_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass50_2_t94962D31C36BB849562C26FA6523CE2F18F5C5C4, ___parseJsonThread_0)); }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * get_parseJsonThread_0() const { return ___parseJsonThread_0; }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 ** get_address_of_parseJsonThread_0() { return &___parseJsonThread_0; }
	inline void set_parseJsonThread_0(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * value)
	{
		___parseJsonThread_0 = value;
		Il2CppCodeGenWriteBarrier((&___parseJsonThread_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS50_2_T94962D31C36BB849562C26FA6523CE2F18F5C5C4_H
#ifndef U3CU3EC__DISPLAYCLASS52_0_TD36628E62AE9ACC2EE3B3744FB9FAFEFC2CFFCCA_H
#define U3CU3EC__DISPLAYCLASS52_0_TD36628E62AE9ACC2EE3B3744FB9FAFEFC2CFFCCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<>c__DisplayClass52_0
struct  U3CU3Ec__DisplayClass52_0_tD36628E62AE9ACC2EE3B3744FB9FAFEFC2CFFCCA  : public RuntimeObject
{
public:
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<>c__DisplayClass52_0::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_0;
	// GLTF.Schema.Node UnityGLTF.GLTFSceneImporter/<>c__DisplayClass52_0::nodeToLoad
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744 * ___nodeToLoad_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass52_0_tD36628E62AE9ACC2EE3B3744FB9FAFEFC2CFFCCA, ___U3CU3E4__this_0)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_nodeToLoad_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass52_0_tD36628E62AE9ACC2EE3B3744FB9FAFEFC2CFFCCA, ___nodeToLoad_1)); }
	inline Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744 * get_nodeToLoad_1() const { return ___nodeToLoad_1; }
	inline Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744 ** get_address_of_nodeToLoad_1() { return &___nodeToLoad_1; }
	inline void set_nodeToLoad_1(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744 * value)
	{
		___nodeToLoad_1 = value;
		Il2CppCodeGenWriteBarrier((&___nodeToLoad_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS52_0_TD36628E62AE9ACC2EE3B3744FB9FAFEFC2CFFCCA_H
#ifndef U3CEMPTYYIELDENUMU3ED__49_T22C2121ABBAE0537A0163637FBB8166C0FCF882B_H
#define U3CEMPTYYIELDENUMU3ED__49_T22C2121ABBAE0537A0163637FBB8166C0FCF882B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<EmptyYieldEnum>d__49
struct  U3CEmptyYieldEnumU3Ed__49_t22C2121ABBAE0537A0163637FBB8166C0FCF882B  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<EmptyYieldEnum>d__49::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.GLTFSceneImporter/<EmptyYieldEnum>d__49::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CEmptyYieldEnumU3Ed__49_t22C2121ABBAE0537A0163637FBB8166C0FCF882B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CEmptyYieldEnumU3Ed__49_t22C2121ABBAE0537A0163637FBB8166C0FCF882B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEMPTYYIELDENUMU3ED__49_T22C2121ABBAE0537A0163637FBB8166C0FCF882B_H
#ifndef U3CWAITUNTILENUMU3ED__48_T04AEC5A1823CF0E6EA76CFF45ED60C9C2BB01921_H
#define U3CWAITUNTILENUMU3ED__48_T04AEC5A1823CF0E6EA76CFF45ED60C9C2BB01921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<WaitUntilEnum>d__48
struct  U3CWaitUntilEnumU3Ed__48_t04AEC5A1823CF0E6EA76CFF45ED60C9C2BB01921  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<WaitUntilEnum>d__48::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.GLTFSceneImporter/<WaitUntilEnum>d__48::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.WaitUntil UnityGLTF.GLTFSceneImporter/<WaitUntilEnum>d__48::waitUntil
	WaitUntil_t012561515C0E1D3DEA19DB3A05444B020C68E13F * ___waitUntil_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitUntilEnumU3Ed__48_t04AEC5A1823CF0E6EA76CFF45ED60C9C2BB01921, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitUntilEnumU3Ed__48_t04AEC5A1823CF0E6EA76CFF45ED60C9C2BB01921, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_waitUntil_2() { return static_cast<int32_t>(offsetof(U3CWaitUntilEnumU3Ed__48_t04AEC5A1823CF0E6EA76CFF45ED60C9C2BB01921, ___waitUntil_2)); }
	inline WaitUntil_t012561515C0E1D3DEA19DB3A05444B020C68E13F * get_waitUntil_2() const { return ___waitUntil_2; }
	inline WaitUntil_t012561515C0E1D3DEA19DB3A05444B020C68E13F ** get_address_of_waitUntil_2() { return &___waitUntil_2; }
	inline void set_waitUntil_2(WaitUntil_t012561515C0E1D3DEA19DB3A05444B020C68E13F * value)
	{
		___waitUntil_2 = value;
		Il2CppCodeGenWriteBarrier((&___waitUntil_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITUNTILENUMU3ED__48_T04AEC5A1823CF0E6EA76CFF45ED60C9C2BB01921_H
#ifndef TASKEXTENSIONS_T9CB30AA030C26D3A036B22CFB3B9CFA5B136D882_H
#define TASKEXTENSIONS_T9CB30AA030C26D3A036B22CFB3B9CFA5B136D882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.TaskExtensions
struct  TaskExtensions_t9CB30AA030C26D3A036B22CFB3B9CFA5B136D882  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKEXTENSIONS_T9CB30AA030C26D3A036B22CFB3B9CFA5B136D882_H
#ifndef U3CASCOROUTINEU3ED__0_T523ECBDE8E55C4CC5FC6CE2EFCD1057B234FE883_H
#define U3CASCOROUTINEU3ED__0_T523ECBDE8E55C4CC5FC6CE2EFCD1057B234FE883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.TaskExtensions/<AsCoroutine>d__0
struct  U3CAsCoroutineU3Ed__0_t523ECBDE8E55C4CC5FC6CE2EFCD1057B234FE883  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.TaskExtensions/<AsCoroutine>d__0::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.TaskExtensions/<AsCoroutine>d__0::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Threading.Tasks.Task UnityGLTF.TaskExtensions/<AsCoroutine>d__0::task
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___task_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAsCoroutineU3Ed__0_t523ECBDE8E55C4CC5FC6CE2EFCD1057B234FE883, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAsCoroutineU3Ed__0_t523ECBDE8E55C4CC5FC6CE2EFCD1057B234FE883, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_task_2() { return static_cast<int32_t>(offsetof(U3CAsCoroutineU3Ed__0_t523ECBDE8E55C4CC5FC6CE2EFCD1057B234FE883, ___task_2)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_task_2() const { return ___task_2; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_task_2() { return &___task_2; }
	inline void set_task_2(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___task_2 = value;
		Il2CppCodeGenWriteBarrier((&___task_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CASCOROUTINEU3ED__0_T523ECBDE8E55C4CC5FC6CE2EFCD1057B234FE883_H
#ifndef UNITYMESHDATA_TD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D_H
#define UNITYMESHDATA_TD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.UnityMeshData
struct  UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] UnityGLTF.UnityMeshData::Vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Vertices_0;
	// UnityEngine.Vector3[] UnityGLTF.UnityMeshData::Normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Normals_1;
	// UnityEngine.Vector2[] UnityGLTF.UnityMeshData::Uv1
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv1_2;
	// UnityEngine.Vector2[] UnityGLTF.UnityMeshData::Uv2
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv2_3;
	// UnityEngine.Vector2[] UnityGLTF.UnityMeshData::Uv3
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv3_4;
	// UnityEngine.Vector2[] UnityGLTF.UnityMeshData::Uv4
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv4_5;
	// UnityEngine.Color[] UnityGLTF.UnityMeshData::Colors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___Colors_6;
	// System.Int32[] UnityGLTF.UnityMeshData::Triangles
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Triangles_7;
	// UnityEngine.Vector4[] UnityGLTF.UnityMeshData::Tangents
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___Tangents_8;
	// UnityEngine.BoneWeight[] UnityGLTF.UnityMeshData::BoneWeights
	BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D* ___BoneWeights_9;

public:
	inline static int32_t get_offset_of_Vertices_0() { return static_cast<int32_t>(offsetof(UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D, ___Vertices_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Vertices_0() const { return ___Vertices_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Vertices_0() { return &___Vertices_0; }
	inline void set_Vertices_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Vertices_0 = value;
		Il2CppCodeGenWriteBarrier((&___Vertices_0), value);
	}

	inline static int32_t get_offset_of_Normals_1() { return static_cast<int32_t>(offsetof(UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D, ___Normals_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Normals_1() const { return ___Normals_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Normals_1() { return &___Normals_1; }
	inline void set_Normals_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Normals_1 = value;
		Il2CppCodeGenWriteBarrier((&___Normals_1), value);
	}

	inline static int32_t get_offset_of_Uv1_2() { return static_cast<int32_t>(offsetof(UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D, ___Uv1_2)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv1_2() const { return ___Uv1_2; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv1_2() { return &___Uv1_2; }
	inline void set_Uv1_2(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv1_2 = value;
		Il2CppCodeGenWriteBarrier((&___Uv1_2), value);
	}

	inline static int32_t get_offset_of_Uv2_3() { return static_cast<int32_t>(offsetof(UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D, ___Uv2_3)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv2_3() const { return ___Uv2_3; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv2_3() { return &___Uv2_3; }
	inline void set_Uv2_3(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv2_3 = value;
		Il2CppCodeGenWriteBarrier((&___Uv2_3), value);
	}

	inline static int32_t get_offset_of_Uv3_4() { return static_cast<int32_t>(offsetof(UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D, ___Uv3_4)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv3_4() const { return ___Uv3_4; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv3_4() { return &___Uv3_4; }
	inline void set_Uv3_4(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv3_4 = value;
		Il2CppCodeGenWriteBarrier((&___Uv3_4), value);
	}

	inline static int32_t get_offset_of_Uv4_5() { return static_cast<int32_t>(offsetof(UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D, ___Uv4_5)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv4_5() const { return ___Uv4_5; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv4_5() { return &___Uv4_5; }
	inline void set_Uv4_5(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv4_5 = value;
		Il2CppCodeGenWriteBarrier((&___Uv4_5), value);
	}

	inline static int32_t get_offset_of_Colors_6() { return static_cast<int32_t>(offsetof(UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D, ___Colors_6)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_Colors_6() const { return ___Colors_6; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_Colors_6() { return &___Colors_6; }
	inline void set_Colors_6(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___Colors_6 = value;
		Il2CppCodeGenWriteBarrier((&___Colors_6), value);
	}

	inline static int32_t get_offset_of_Triangles_7() { return static_cast<int32_t>(offsetof(UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D, ___Triangles_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Triangles_7() const { return ___Triangles_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Triangles_7() { return &___Triangles_7; }
	inline void set_Triangles_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Triangles_7 = value;
		Il2CppCodeGenWriteBarrier((&___Triangles_7), value);
	}

	inline static int32_t get_offset_of_Tangents_8() { return static_cast<int32_t>(offsetof(UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D, ___Tangents_8)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_Tangents_8() const { return ___Tangents_8; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_Tangents_8() { return &___Tangents_8; }
	inline void set_Tangents_8(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___Tangents_8 = value;
		Il2CppCodeGenWriteBarrier((&___Tangents_8), value);
	}

	inline static int32_t get_offset_of_BoneWeights_9() { return static_cast<int32_t>(offsetof(UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D, ___BoneWeights_9)); }
	inline BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D* get_BoneWeights_9() const { return ___BoneWeights_9; }
	inline BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D** get_address_of_BoneWeights_9() { return &___BoneWeights_9; }
	inline void set_BoneWeights_9(BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D* value)
	{
		___BoneWeights_9 = value;
		Il2CppCodeGenWriteBarrier((&___BoneWeights_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYMESHDATA_TD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D_H
#ifndef U3CU3EC_TB31F53CFFD1216D13357FD67DA8DA5C2416B9474_H
#define U3CU3EC_TB31F53CFFD1216D13357FD67DA8DA5C2416B9474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskDistributor/<>c
struct  U3CU3Ec_tB31F53CFFD1216D13357FD67DA8DA5C2416B9474  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB31F53CFFD1216D13357FD67DA8DA5C2416B9474_StaticFields
{
public:
	// UnityThreading.TaskDistributor/<>c UnityThreading.TaskDistributor/<>c::<>9
	U3CU3Ec_tB31F53CFFD1216D13357FD67DA8DA5C2416B9474 * ___U3CU3E9_0;
	// System.Func`2<UnityThreading.TaskWorker,System.Boolean> UnityThreading.TaskDistributor/<>c::<>9__19_0
	Func_2_t1974F937036431FDCBE8C6785949DE4EB35D8153 * ___U3CU3E9__19_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB31F53CFFD1216D13357FD67DA8DA5C2416B9474_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB31F53CFFD1216D13357FD67DA8DA5C2416B9474 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB31F53CFFD1216D13357FD67DA8DA5C2416B9474 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB31F53CFFD1216D13357FD67DA8DA5C2416B9474 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__19_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB31F53CFFD1216D13357FD67DA8DA5C2416B9474_StaticFields, ___U3CU3E9__19_0_1)); }
	inline Func_2_t1974F937036431FDCBE8C6785949DE4EB35D8153 * get_U3CU3E9__19_0_1() const { return ___U3CU3E9__19_0_1; }
	inline Func_2_t1974F937036431FDCBE8C6785949DE4EB35D8153 ** get_address_of_U3CU3E9__19_0_1() { return &___U3CU3E9__19_0_1; }
	inline void set_U3CU3E9__19_0_1(Func_2_t1974F937036431FDCBE8C6785949DE4EB35D8153 * value)
	{
		___U3CU3E9__19_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__19_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TB31F53CFFD1216D13357FD67DA8DA5C2416B9474_H
#ifndef TASKEXTENSION_T7EBF7CA29E1A67538AA20BE1D66BC1606BA8CFAD_H
#define TASKEXTENSION_T7EBF7CA29E1A67538AA20BE1D66BC1606BA8CFAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension
struct  TaskExtension_t7EBF7CA29E1A67538AA20BE1D66BC1606BA8CFAD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKEXTENSION_T7EBF7CA29E1A67538AA20BE1D66BC1606BA8CFAD_H
#ifndef U3CU3EC__DISPLAYCLASS15_0_T8FDC5845942E67DB6886C426627DE013628CE024_H
#define U3CU3EC__DISPLAYCLASS15_0_T8FDC5845942E67DB6886C426627DE013628CE024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_t8FDC5845942E67DB6886C426627DE013628CE024  : public RuntimeObject
{
public:
	// System.Action UnityThreading.TaskExtension/<>c__DisplayClass15_0::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_t8FDC5845942E67DB6886C426627DE013628CE024, ___action_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_0() const { return ___action_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_0_T8FDC5845942E67DB6886C426627DE013628CE024_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_TADCA51721C9B0D421A8CAF0ED66340096A9AC3B7_H
#define U3CU3EC__DISPLAYCLASS16_0_TADCA51721C9B0D421A8CAF0ED66340096A9AC3B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_tADCA51721C9B0D421A8CAF0ED66340096A9AC3B7  : public RuntimeObject
{
public:
	// System.Action`1<UnityThreading.Task> UnityThreading.TaskExtension/<>c__DisplayClass16_0::action
	Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_tADCA51721C9B0D421A8CAF0ED66340096A9AC3B7, ___action_0)); }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * get_action_0() const { return ___action_0; }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_TADCA51721C9B0D421A8CAF0ED66340096A9AC3B7_H
#ifndef U3CU3EC__DISPLAYCLASS17_0_T07C559EC67E78265374C7C87DB093CB50CD821DE_H
#define U3CU3EC__DISPLAYCLASS17_0_T07C559EC67E78265374C7C87DB093CB50CD821DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_t07C559EC67E78265374C7C87DB093CB50CD821DE  : public RuntimeObject
{
public:
	// UnityThreading.DispatcherBase UnityThreading.TaskExtension/<>c__DisplayClass17_0::actiontargetTarget
	DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * ___actiontargetTarget_0;
	// System.Action`1<UnityThreading.Task> UnityThreading.TaskExtension/<>c__DisplayClass17_0::action
	Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * ___action_1;
	// System.Action`1<UnityThreading.Task> UnityThreading.TaskExtension/<>c__DisplayClass17_0::perform
	Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * ___perform_2;

public:
	inline static int32_t get_offset_of_actiontargetTarget_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t07C559EC67E78265374C7C87DB093CB50CD821DE, ___actiontargetTarget_0)); }
	inline DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * get_actiontargetTarget_0() const { return ___actiontargetTarget_0; }
	inline DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 ** get_address_of_actiontargetTarget_0() { return &___actiontargetTarget_0; }
	inline void set_actiontargetTarget_0(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * value)
	{
		___actiontargetTarget_0 = value;
		Il2CppCodeGenWriteBarrier((&___actiontargetTarget_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t07C559EC67E78265374C7C87DB093CB50CD821DE, ___action_1)); }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * get_action_1() const { return ___action_1; }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}

	inline static int32_t get_offset_of_perform_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t07C559EC67E78265374C7C87DB093CB50CD821DE, ___perform_2)); }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * get_perform_2() const { return ___perform_2; }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F ** get_address_of_perform_2() { return &___perform_2; }
	inline void set_perform_2(Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * value)
	{
		___perform_2 = value;
		Il2CppCodeGenWriteBarrier((&___perform_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_0_T07C559EC67E78265374C7C87DB093CB50CD821DE_H
#ifndef U3CU3EC__DISPLAYCLASS17_1_T088DEDE8920DA139F08C4A05C21D327D0514363B_H
#define U3CU3EC__DISPLAYCLASS17_1_T088DEDE8920DA139F08C4A05C21D327D0514363B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass17_1
struct  U3CU3Ec__DisplayClass17_1_t088DEDE8920DA139F08C4A05C21D327D0514363B  : public RuntimeObject
{
public:
	// UnityThreading.Task UnityThreading.TaskExtension/<>c__DisplayClass17_1::t
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * ___t_0;
	// UnityThreading.TaskExtension/<>c__DisplayClass17_0 UnityThreading.TaskExtension/<>c__DisplayClass17_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass17_0_t07C559EC67E78265374C7C87DB093CB50CD821DE * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_1_t088DEDE8920DA139F08C4A05C21D327D0514363B, ___t_0)); }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * get_t_0() const { return ___t_0; }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_1_t088DEDE8920DA139F08C4A05C21D327D0514363B, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass17_0_t07C559EC67E78265374C7C87DB093CB50CD821DE * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass17_0_t07C559EC67E78265374C7C87DB093CB50CD821DE ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass17_0_t07C559EC67E78265374C7C87DB093CB50CD821DE * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_1_T088DEDE8920DA139F08C4A05C21D327D0514363B_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_TECDE733BFE4D0A45FCDF55A3A8C7F7DB7CD4D03B_H
#define U3CU3EC__DISPLAYCLASS21_0_TECDE733BFE4D0A45FCDF55A3A8C7F7DB7CD4D03B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_tECDE733BFE4D0A45FCDF55A3A8C7F7DB7CD4D03B  : public RuntimeObject
{
public:
	// System.Action UnityThreading.TaskExtension/<>c__DisplayClass21_0::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tECDE733BFE4D0A45FCDF55A3A8C7F7DB7CD4D03B, ___action_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_0() const { return ___action_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_TECDE733BFE4D0A45FCDF55A3A8C7F7DB7CD4D03B_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T89D194F62ACA4EF1F59F25C6326F094B023772EC_H
#define U3CU3EC__DISPLAYCLASS22_0_T89D194F62ACA4EF1F59F25C6326F094B023772EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t89D194F62ACA4EF1F59F25C6326F094B023772EC  : public RuntimeObject
{
public:
	// System.Action`1<UnityThreading.Task> UnityThreading.TaskExtension/<>c__DisplayClass22_0::action
	Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t89D194F62ACA4EF1F59F25C6326F094B023772EC, ___action_0)); }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * get_action_0() const { return ___action_0; }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T89D194F62ACA4EF1F59F25C6326F094B023772EC_H
#ifndef U3CU3EC__DISPLAYCLASS23_0_T3C160A91601A49C38F9476CD26D91D7FEFD88A09_H
#define U3CU3EC__DISPLAYCLASS23_0_T3C160A91601A49C38F9476CD26D91D7FEFD88A09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass23_0
struct  U3CU3Ec__DisplayClass23_0_t3C160A91601A49C38F9476CD26D91D7FEFD88A09  : public RuntimeObject
{
public:
	// System.Action`1<UnityThreading.Task> UnityThreading.TaskExtension/<>c__DisplayClass23_0::action
	Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_0_t3C160A91601A49C38F9476CD26D91D7FEFD88A09, ___action_0)); }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * get_action_0() const { return ___action_0; }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS23_0_T3C160A91601A49C38F9476CD26D91D7FEFD88A09_H
#ifndef U3CU3EC__DISPLAYCLASS27_0_T07C50968469A6F475DFBD94E8BF2F6C8B6E66617_H
#define U3CU3EC__DISPLAYCLASS27_0_T07C50968469A6F475DFBD94E8BF2F6C8B6E66617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass27_0
struct  U3CU3Ec__DisplayClass27_0_t07C50968469A6F475DFBD94E8BF2F6C8B6E66617  : public RuntimeObject
{
public:
	// System.Action UnityThreading.TaskExtension/<>c__DisplayClass27_0::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t07C50968469A6F475DFBD94E8BF2F6C8B6E66617, ___action_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_0() const { return ___action_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS27_0_T07C50968469A6F475DFBD94E8BF2F6C8B6E66617_H
#ifndef U3CU3EC__DISPLAYCLASS28_0_T972C520BDE10FAC30F0728ACE7893B80057937D2_H
#define U3CU3EC__DISPLAYCLASS28_0_T972C520BDE10FAC30F0728ACE7893B80057937D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass28_0
struct  U3CU3Ec__DisplayClass28_0_t972C520BDE10FAC30F0728ACE7893B80057937D2  : public RuntimeObject
{
public:
	// System.Action`1<UnityThreading.Task> UnityThreading.TaskExtension/<>c__DisplayClass28_0::action
	Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t972C520BDE10FAC30F0728ACE7893B80057937D2, ___action_0)); }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * get_action_0() const { return ___action_0; }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS28_0_T972C520BDE10FAC30F0728ACE7893B80057937D2_H
#ifndef U3CU3EC__DISPLAYCLASS29_0_T1CC6B9E2B18FBD0E6FC710C23638BEDF73A1AF7A_H
#define U3CU3EC__DISPLAYCLASS29_0_T1CC6B9E2B18FBD0E6FC710C23638BEDF73A1AF7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass29_0
struct  U3CU3Ec__DisplayClass29_0_t1CC6B9E2B18FBD0E6FC710C23638BEDF73A1AF7A  : public RuntimeObject
{
public:
	// UnityThreading.DispatcherBase UnityThreading.TaskExtension/<>c__DisplayClass29_0::target
	DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * ___target_0;
	// System.Action`1<UnityThreading.Task> UnityThreading.TaskExtension/<>c__DisplayClass29_0::action
	Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * ___action_1;
	// UnityThreading.Task UnityThreading.TaskExtension/<>c__DisplayClass29_0::task
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * ___task_2;
	// System.Action UnityThreading.TaskExtension/<>c__DisplayClass29_0::<>9__1
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__1_3;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t1CC6B9E2B18FBD0E6FC710C23638BEDF73A1AF7A, ___target_0)); }
	inline DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * get_target_0() const { return ___target_0; }
	inline DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t1CC6B9E2B18FBD0E6FC710C23638BEDF73A1AF7A, ___action_1)); }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * get_action_1() const { return ___action_1; }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}

	inline static int32_t get_offset_of_task_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t1CC6B9E2B18FBD0E6FC710C23638BEDF73A1AF7A, ___task_2)); }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * get_task_2() const { return ___task_2; }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F ** get_address_of_task_2() { return &___task_2; }
	inline void set_task_2(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * value)
	{
		___task_2 = value;
		Il2CppCodeGenWriteBarrier((&___task_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t1CC6B9E2B18FBD0E6FC710C23638BEDF73A1AF7A, ___U3CU3E9__1_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__1_3() const { return ___U3CU3E9__1_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__1_3() { return &___U3CU3E9__1_3; }
	inline void set_U3CU3E9__1_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS29_0_T1CC6B9E2B18FBD0E6FC710C23638BEDF73A1AF7A_H
#ifndef U3CU3EC__DISPLAYCLASS31_0_T5864AE0B7C851F948BA6606C73A66949413F9885_H
#define U3CU3EC__DISPLAYCLASS31_0_T5864AE0B7C851F948BA6606C73A66949413F9885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass31_0
struct  U3CU3Ec__DisplayClass31_0_t5864AE0B7C851F948BA6606C73A66949413F9885  : public RuntimeObject
{
public:
	// UnityThreading.Task UnityThreading.TaskExtension/<>c__DisplayClass31_0::followingTask
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * ___followingTask_0;
	// UnityThreading.DispatcherBase UnityThreading.TaskExtension/<>c__DisplayClass31_0::target
	DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * ___target_1;

public:
	inline static int32_t get_offset_of_followingTask_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_t5864AE0B7C851F948BA6606C73A66949413F9885, ___followingTask_0)); }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * get_followingTask_0() const { return ___followingTask_0; }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F ** get_address_of_followingTask_0() { return &___followingTask_0; }
	inline void set_followingTask_0(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * value)
	{
		___followingTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___followingTask_0), value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_t5864AE0B7C851F948BA6606C73A66949413F9885, ___target_1)); }
	inline DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * get_target_1() const { return ___target_1; }
	inline DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS31_0_T5864AE0B7C851F948BA6606C73A66949413F9885_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_TB5F2CA1C9C8C0675900287461BB59C003D230D7F_H
#define U3CU3EC__DISPLAYCLASS35_0_TB5F2CA1C9C8C0675900287461BB59C003D230D7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_tB5F2CA1C9C8C0675900287461BB59C003D230D7F  : public RuntimeObject
{
public:
	// System.Action UnityThreading.TaskExtension/<>c__DisplayClass35_0::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_tB5F2CA1C9C8C0675900287461BB59C003D230D7F, ___action_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_0() const { return ___action_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_TB5F2CA1C9C8C0675900287461BB59C003D230D7F_H
#ifndef U3CU3EC__DISPLAYCLASS36_0_TB5A08878EA856CCFB351300F02D1068BCBBBC2E5_H
#define U3CU3EC__DISPLAYCLASS36_0_TB5A08878EA856CCFB351300F02D1068BCBBBC2E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_tB5A08878EA856CCFB351300F02D1068BCBBBC2E5  : public RuntimeObject
{
public:
	// System.Object UnityThreading.TaskExtension/<>c__DisplayClass36_0::syncRoot
	RuntimeObject * ___syncRoot_0;
	// System.Boolean UnityThreading.TaskExtension/<>c__DisplayClass36_0::done
	bool ___done_1;
	// System.Action`1<UnityThreading.Task> UnityThreading.TaskExtension/<>c__DisplayClass36_0::action
	Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * ___action_2;
	// System.Action`1<UnityThreading.Task> UnityThreading.TaskExtension/<>c__DisplayClass36_0::<>9__0
	Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * ___U3CU3E9__0_3;

public:
	inline static int32_t get_offset_of_syncRoot_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_tB5A08878EA856CCFB351300F02D1068BCBBBC2E5, ___syncRoot_0)); }
	inline RuntimeObject * get_syncRoot_0() const { return ___syncRoot_0; }
	inline RuntimeObject ** get_address_of_syncRoot_0() { return &___syncRoot_0; }
	inline void set_syncRoot_0(RuntimeObject * value)
	{
		___syncRoot_0 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_0), value);
	}

	inline static int32_t get_offset_of_done_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_tB5A08878EA856CCFB351300F02D1068BCBBBC2E5, ___done_1)); }
	inline bool get_done_1() const { return ___done_1; }
	inline bool* get_address_of_done_1() { return &___done_1; }
	inline void set_done_1(bool value)
	{
		___done_1 = value;
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_tB5A08878EA856CCFB351300F02D1068BCBBBC2E5, ___action_2)); }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * get_action_2() const { return ___action_2; }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier((&___action_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_tB5A08878EA856CCFB351300F02D1068BCBBBC2E5, ___U3CU3E9__0_3)); }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * get_U3CU3E9__0_3() const { return ___U3CU3E9__0_3; }
	inline Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F ** get_address_of_U3CU3E9__0_3() { return &___U3CU3E9__0_3; }
	inline void set_U3CU3E9__0_3(Action_1_t1A3C2A0A72B43EF7AA9441BEB966573DD27E483F * value)
	{
		___U3CU3E9__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS36_0_TB5A08878EA856CCFB351300F02D1068BCBBBC2E5_H
#ifndef U3CU3EC__DISPLAYCLASS37_0_T86797DACC849C07493E8CE30EB565FDDD6178B63_H
#define U3CU3EC__DISPLAYCLASS37_0_T86797DACC849C07493E8CE30EB565FDDD6178B63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass37_0
struct  U3CU3Ec__DisplayClass37_0_t86797DACC849C07493E8CE30EB565FDDD6178B63  : public RuntimeObject
{
public:
	// System.Action UnityThreading.TaskExtension/<>c__DisplayClass37_0::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t86797DACC849C07493E8CE30EB565FDDD6178B63, ___action_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_0() const { return ___action_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS37_0_T86797DACC849C07493E8CE30EB565FDDD6178B63_H
#ifndef U3CU3EC__DISPLAYCLASS38_0_T955423008B58DF24EFBD1C23E7B318A22B15107F_H
#define U3CU3EC__DISPLAYCLASS38_0_T955423008B58DF24EFBD1C23E7B318A22B15107F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_t955423008B58DF24EFBD1C23E7B318A22B15107F  : public RuntimeObject
{
public:
	// System.Object UnityThreading.TaskExtension/<>c__DisplayClass38_0::syncRoot
	RuntimeObject * ___syncRoot_0;
	// System.Collections.Generic.List`1<UnityThreading.Task> UnityThreading.TaskExtension/<>c__DisplayClass38_0::finishedTasks
	List_1_t2ABFD0F79D77AAB47F6C9E0EB34B82B905850368 * ___finishedTasks_1;
	// System.Int32 UnityThreading.TaskExtension/<>c__DisplayClass38_0::count
	int32_t ___count_2;
	// System.Action`1<System.Collections.Generic.IEnumerable`1<UnityThreading.Task>> UnityThreading.TaskExtension/<>c__DisplayClass38_0::action
	Action_1_tB6756C6173D7091207B2AA045DC646FB963CF826 * ___action_3;

public:
	inline static int32_t get_offset_of_syncRoot_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t955423008B58DF24EFBD1C23E7B318A22B15107F, ___syncRoot_0)); }
	inline RuntimeObject * get_syncRoot_0() const { return ___syncRoot_0; }
	inline RuntimeObject ** get_address_of_syncRoot_0() { return &___syncRoot_0; }
	inline void set_syncRoot_0(RuntimeObject * value)
	{
		___syncRoot_0 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_0), value);
	}

	inline static int32_t get_offset_of_finishedTasks_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t955423008B58DF24EFBD1C23E7B318A22B15107F, ___finishedTasks_1)); }
	inline List_1_t2ABFD0F79D77AAB47F6C9E0EB34B82B905850368 * get_finishedTasks_1() const { return ___finishedTasks_1; }
	inline List_1_t2ABFD0F79D77AAB47F6C9E0EB34B82B905850368 ** get_address_of_finishedTasks_1() { return &___finishedTasks_1; }
	inline void set_finishedTasks_1(List_1_t2ABFD0F79D77AAB47F6C9E0EB34B82B905850368 * value)
	{
		___finishedTasks_1 = value;
		Il2CppCodeGenWriteBarrier((&___finishedTasks_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t955423008B58DF24EFBD1C23E7B318A22B15107F, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_action_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t955423008B58DF24EFBD1C23E7B318A22B15107F, ___action_3)); }
	inline Action_1_tB6756C6173D7091207B2AA045DC646FB963CF826 * get_action_3() const { return ___action_3; }
	inline Action_1_tB6756C6173D7091207B2AA045DC646FB963CF826 ** get_address_of_action_3() { return &___action_3; }
	inline void set_action_3(Action_1_tB6756C6173D7091207B2AA045DC646FB963CF826 * value)
	{
		___action_3 = value;
		Il2CppCodeGenWriteBarrier((&___action_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_0_T955423008B58DF24EFBD1C23E7B318A22B15107F_H
#ifndef U3CU3EC__DISPLAYCLASS38_1_T87B6536086105EBCAE0D6908E1771234B1A95087_H
#define U3CU3EC__DISPLAYCLASS38_1_T87B6536086105EBCAE0D6908E1771234B1A95087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass38_1
struct  U3CU3Ec__DisplayClass38_1_t87B6536086105EBCAE0D6908E1771234B1A95087  : public RuntimeObject
{
public:
	// UnityThreading.Task UnityThreading.TaskExtension/<>c__DisplayClass38_1::task
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * ___task_0;
	// UnityThreading.TaskExtension/<>c__DisplayClass38_0 UnityThreading.TaskExtension/<>c__DisplayClass38_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass38_0_t955423008B58DF24EFBD1C23E7B318A22B15107F * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_task_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_1_t87B6536086105EBCAE0D6908E1771234B1A95087, ___task_0)); }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * get_task_0() const { return ___task_0; }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F ** get_address_of_task_0() { return &___task_0; }
	inline void set_task_0(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * value)
	{
		___task_0 = value;
		Il2CppCodeGenWriteBarrier((&___task_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_1_t87B6536086105EBCAE0D6908E1771234B1A95087, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass38_0_t955423008B58DF24EFBD1C23E7B318A22B15107F * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass38_0_t955423008B58DF24EFBD1C23E7B318A22B15107F ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass38_0_t955423008B58DF24EFBD1C23E7B318A22B15107F * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_1_T87B6536086105EBCAE0D6908E1771234B1A95087_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F_H
#define U3CU3EC__DISPLAYCLASS3_0_T12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F  : public RuntimeObject
{
public:
	// UnityThreading.Task UnityThreading.TaskExtension/<>c__DisplayClass3_0::followingTask
	Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * ___followingTask_0;
	// System.Object UnityThreading.TaskExtension/<>c__DisplayClass3_0::syncRoot
	RuntimeObject * ___syncRoot_1;
	// System.Int32 UnityThreading.TaskExtension/<>c__DisplayClass3_0::remaining
	int32_t ___remaining_2;
	// UnityThreading.DispatcherBase UnityThreading.TaskExtension/<>c__DisplayClass3_0::target
	DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * ___target_3;
	// System.Action UnityThreading.TaskExtension/<>c__DisplayClass3_0::<>9__0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__0_4;
	// System.Action UnityThreading.TaskExtension/<>c__DisplayClass3_0::<>9__1
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__1_5;

public:
	inline static int32_t get_offset_of_followingTask_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F, ___followingTask_0)); }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * get_followingTask_0() const { return ___followingTask_0; }
	inline Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F ** get_address_of_followingTask_0() { return &___followingTask_0; }
	inline void set_followingTask_0(Task_t5886E986429BB4D23327D2F8D6B94A4DB454B15F * value)
	{
		___followingTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___followingTask_0), value);
	}

	inline static int32_t get_offset_of_syncRoot_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F, ___syncRoot_1)); }
	inline RuntimeObject * get_syncRoot_1() const { return ___syncRoot_1; }
	inline RuntimeObject ** get_address_of_syncRoot_1() { return &___syncRoot_1; }
	inline void set_syncRoot_1(RuntimeObject * value)
	{
		___syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_1), value);
	}

	inline static int32_t get_offset_of_remaining_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F, ___remaining_2)); }
	inline int32_t get_remaining_2() const { return ___remaining_2; }
	inline int32_t* get_address_of_remaining_2() { return &___remaining_2; }
	inline void set_remaining_2(int32_t value)
	{
		___remaining_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F, ___target_3)); }
	inline DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * get_target_3() const { return ___target_3; }
	inline DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F, ___U3CU3E9__0_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__0_4() const { return ___U3CU3E9__0_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__0_4() { return &___U3CU3E9__0_4; }
	inline void set_U3CU3E9__0_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F, ___U3CU3E9__1_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__1_5() const { return ___U3CU3E9__1_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__1_5() { return &___U3CU3E9__1_5; }
	inline void set_U3CU3E9__1_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T12AADA9F3D9F3CAE598D79656D801DC78A4D5820_H
#define U3CU3EC__DISPLAYCLASS4_0_T12AADA9F3D9F3CAE598D79656D801DC78A4D5820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t12AADA9F3D9F3CAE598D79656D801DC78A4D5820  : public RuntimeObject
{
public:
	// System.Object UnityThreading.TaskExtension/<>c__DisplayClass4_0::syncRoot
	RuntimeObject * ___syncRoot_0;
	// System.Int32 UnityThreading.TaskExtension/<>c__DisplayClass4_0::remaining
	int32_t ___remaining_1;
	// UnityThreading.DispatcherBase UnityThreading.TaskExtension/<>c__DisplayClass4_0::target
	DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * ___target_2;
	// System.Action UnityThreading.TaskExtension/<>c__DisplayClass4_0::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_3;
	// System.Action UnityThreading.TaskExtension/<>c__DisplayClass4_0::<>9__1
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__1_4;
	// System.Action UnityThreading.TaskExtension/<>c__DisplayClass4_0::<>9__0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__0_5;

public:
	inline static int32_t get_offset_of_syncRoot_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t12AADA9F3D9F3CAE598D79656D801DC78A4D5820, ___syncRoot_0)); }
	inline RuntimeObject * get_syncRoot_0() const { return ___syncRoot_0; }
	inline RuntimeObject ** get_address_of_syncRoot_0() { return &___syncRoot_0; }
	inline void set_syncRoot_0(RuntimeObject * value)
	{
		___syncRoot_0 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_0), value);
	}

	inline static int32_t get_offset_of_remaining_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t12AADA9F3D9F3CAE598D79656D801DC78A4D5820, ___remaining_1)); }
	inline int32_t get_remaining_1() const { return ___remaining_1; }
	inline int32_t* get_address_of_remaining_1() { return &___remaining_1; }
	inline void set_remaining_1(int32_t value)
	{
		___remaining_1 = value;
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t12AADA9F3D9F3CAE598D79656D801DC78A4D5820, ___target_2)); }
	inline DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * get_target_2() const { return ___target_2; }
	inline DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_action_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t12AADA9F3D9F3CAE598D79656D801DC78A4D5820, ___action_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_3() const { return ___action_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_3() { return &___action_3; }
	inline void set_action_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_3 = value;
		Il2CppCodeGenWriteBarrier((&___action_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t12AADA9F3D9F3CAE598D79656D801DC78A4D5820, ___U3CU3E9__1_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__1_4() const { return ___U3CU3E9__1_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__1_4() { return &___U3CU3E9__1_4; }
	inline void set_U3CU3E9__1_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t12AADA9F3D9F3CAE598D79656D801DC78A4D5820, ___U3CU3E9__0_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__0_5() const { return ___U3CU3E9__0_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__0_5() { return &___U3CU3E9__0_5; }
	inline void set_U3CU3E9__0_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T12AADA9F3D9F3CAE598D79656D801DC78A4D5820_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T110C47148758A50F9343802B5C390A7F4EB42FAF_H
#define U3CU3EC__DISPLAYCLASS5_0_T110C47148758A50F9343802B5C390A7F4EB42FAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t110C47148758A50F9343802B5C390A7F4EB42FAF  : public RuntimeObject
{
public:
	// System.Object UnityThreading.TaskExtension/<>c__DisplayClass5_0::syncRoot
	RuntimeObject * ___syncRoot_0;
	// System.Boolean UnityThreading.TaskExtension/<>c__DisplayClass5_0::hasFailed
	bool ___hasFailed_1;
	// UnityThreading.DispatcherBase UnityThreading.TaskExtension/<>c__DisplayClass5_0::target
	DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * ___target_2;
	// System.Action UnityThreading.TaskExtension/<>c__DisplayClass5_0::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_3;
	// System.Action UnityThreading.TaskExtension/<>c__DisplayClass5_0::<>9__1
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__1_4;
	// System.Action UnityThreading.TaskExtension/<>c__DisplayClass5_0::<>9__0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__0_5;

public:
	inline static int32_t get_offset_of_syncRoot_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t110C47148758A50F9343802B5C390A7F4EB42FAF, ___syncRoot_0)); }
	inline RuntimeObject * get_syncRoot_0() const { return ___syncRoot_0; }
	inline RuntimeObject ** get_address_of_syncRoot_0() { return &___syncRoot_0; }
	inline void set_syncRoot_0(RuntimeObject * value)
	{
		___syncRoot_0 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_0), value);
	}

	inline static int32_t get_offset_of_hasFailed_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t110C47148758A50F9343802B5C390A7F4EB42FAF, ___hasFailed_1)); }
	inline bool get_hasFailed_1() const { return ___hasFailed_1; }
	inline bool* get_address_of_hasFailed_1() { return &___hasFailed_1; }
	inline void set_hasFailed_1(bool value)
	{
		___hasFailed_1 = value;
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t110C47148758A50F9343802B5C390A7F4EB42FAF, ___target_2)); }
	inline DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * get_target_2() const { return ___target_2; }
	inline DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_action_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t110C47148758A50F9343802B5C390A7F4EB42FAF, ___action_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_3() const { return ___action_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_3() { return &___action_3; }
	inline void set_action_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_3 = value;
		Il2CppCodeGenWriteBarrier((&___action_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t110C47148758A50F9343802B5C390A7F4EB42FAF, ___U3CU3E9__1_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__1_4() const { return ___U3CU3E9__1_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__1_4() { return &___U3CU3E9__1_4; }
	inline void set_U3CU3E9__1_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t110C47148758A50F9343802B5C390A7F4EB42FAF, ___U3CU3E9__0_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__0_5() const { return ___U3CU3E9__0_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__0_5() { return &___U3CU3E9__0_5; }
	inline void set_U3CU3E9__0_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T110C47148758A50F9343802B5C390A7F4EB42FAF_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_TFDDEA51B12645C88D91ABC3998F4105672629C7F_H
#define U3CU3EC__DISPLAYCLASS7_0_TFDDEA51B12645C88D91ABC3998F4105672629C7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskExtension/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_tFDDEA51B12645C88D91ABC3998F4105672629C7F  : public RuntimeObject
{
public:
	// System.Action`1<System.Object> UnityThreading.TaskExtension/<>c__DisplayClass7_0::action
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tFDDEA51B12645C88D91ABC3998F4105672629C7F, ___action_0)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get_action_0() const { return ___action_0; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_TFDDEA51B12645C88D91ABC3998F4105672629C7F_H
#ifndef U3CU3EC__DISPLAYCLASS31_0_T7D658482E791302EA4C64C8B8FDC8581469477D8_H
#define U3CU3EC__DISPLAYCLASS31_0_T7D658482E791302EA4C64C8B8FDC8581469477D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.ThreadBase/<>c__DisplayClass31_0
struct  U3CU3Ec__DisplayClass31_0_t7D658482E791302EA4C64C8B8FDC8581469477D8  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator UnityThreading.ThreadBase/<>c__DisplayClass31_0::enumerator
	RuntimeObject* ___enumerator_0;
	// UnityThreading.ThreadBase UnityThreading.ThreadBase/<>c__DisplayClass31_0::<>4__this
	ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C * ___U3CU3E4__this_1;
	// System.Action UnityThreading.ThreadBase/<>c__DisplayClass31_0::<>9__0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__0_2;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_t7D658482E791302EA4C64C8B8FDC8581469477D8, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_t7D658482E791302EA4C64C8B8FDC8581469477D8, ___U3CU3E4__this_1)); }
	inline ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_t7D658482E791302EA4C64C8B8FDC8581469477D8, ___U3CU3E9__0_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS31_0_T7D658482E791302EA4C64C8B8FDC8581469477D8_H
#ifndef WAITONEEXTENSION_T3CD316F6F519DB727390A1339BE54412A2E57961_H
#define WAITONEEXTENSION_T3CD316F6F519DB727390A1339BE54412A2E57961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.WaitOneExtension
struct  WaitOneExtension_t3CD316F6F519DB727390A1339BE54412A2E57961  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITONEEXTENSION_T3CD316F6F519DB727390A1339BE54412A2E57961_H
#ifndef KEYVALUEPAIR_2_TA9A5287EAB0CB41B20FD057EA72334780CDC4A9A_H
#define KEYVALUEPAIR_2_TA9A5287EAB0CB41B20FD057EA72334780CDC4A9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,GLTF.Schema.AccessorId>
struct  KeyValuePair_2_tA9A5287EAB0CB41B20FD057EA72334780CDC4A9A 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tA9A5287EAB0CB41B20FD057EA72334780CDC4A9A, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tA9A5287EAB0CB41B20FD057EA72334780CDC4A9A, ___value_1)); }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * get_value_1() const { return ___value_1; }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_TA9A5287EAB0CB41B20FD057EA72334780CDC4A9A_H
#ifndef ENUMERATOR_TEF9CAAB01B45A327284703F74A8B453B79473A75_H
#define ENUMERATOR_TEF9CAAB01B45A327284703F74A8B453B79473A75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<GLTF.Schema.NodeId>
struct  Enumerator_tEF9CAAB01B45A327284703F74A8B453B79473A75 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tEF9CAAB01B45A327284703F74A8B453B79473A75, ___list_0)); }
	inline List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * get_list_0() const { return ___list_0; }
	inline List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tEF9CAAB01B45A327284703F74A8B453B79473A75, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tEF9CAAB01B45A327284703F74A8B453B79473A75, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tEF9CAAB01B45A327284703F74A8B453B79473A75, ___current_3)); }
	inline NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * get_current_3() const { return ___current_3; }
	inline NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_TEF9CAAB01B45A327284703F74A8B453B79473A75_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#define ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_stateMachine_0), value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_defaultContextAction_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultContextAction_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
#endif // ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#ifndef TASKAWAITER_T0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_H
#define TASKAWAITER_T0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter
struct  TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F 
{
public:
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.TaskAwaiter::m_task
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F, ___m_task_0)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_m_task_0() const { return ___m_task_0; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_marshaled_pinvoke
{
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_marshaled_com
{
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;
};
#endif // TASKAWAITER_T0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_H
#ifndef TASKAWAITER_1_T9DC47086C7D78F5DE15200A7B9DCB4571F58E733_H
#define TASKAWAITER_1_T9DC47086C7D78F5DE15200A7B9DCB4571F58E733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter`1<UnityEngine.Material>
struct  TaskAwaiter_1_t9DC47086C7D78F5DE15200A7B9DCB4571F58E733 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.TaskAwaiter`1::m_task
	Task_1_t74879D22F3CC6429D4D6FCC94A7597CE737CB1DA * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_1_t9DC47086C7D78F5DE15200A7B9DCB4571F58E733, ___m_task_0)); }
	inline Task_1_t74879D22F3CC6429D4D6FCC94A7597CE737CB1DA * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t74879D22F3CC6429D4D6FCC94A7597CE737CB1DA ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t74879D22F3CC6429D4D6FCC94A7597CE737CB1DA * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKAWAITER_1_T9DC47086C7D78F5DE15200A7B9DCB4571F58E733_H
#ifndef TASKAWAITER_1_TAF81E0A0509152C9EFD9A2A752D472023C3FD550_H
#define TASKAWAITER_1_TAF81E0A0509152C9EFD9A2A752D472023C3FD550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter`1<UnityGLTF.UnityMeshData>
struct  TaskAwaiter_1_tAF81E0A0509152C9EFD9A2A752D472023C3FD550 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.TaskAwaiter`1::m_task
	Task_1_t44C0E338925FFB530BE82127BDA04A6421348967 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_1_tAF81E0A0509152C9EFD9A2A752D472023C3FD550, ___m_task_0)); }
	inline Task_1_t44C0E338925FFB530BE82127BDA04A6421348967 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t44C0E338925FFB530BE82127BDA04A6421348967 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t44C0E338925FFB530BE82127BDA04A6421348967 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKAWAITER_1_TAF81E0A0509152C9EFD9A2A752D472023C3FD550_H
#ifndef COROUTINEINFO_TE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE_H
#define COROUTINEINFO_TE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.AsyncCoroutineHelper/CoroutineInfo
struct  CoroutineInfo_tE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE 
{
public:
	// System.Collections.IEnumerator UnityGLTF.AsyncCoroutineHelper/CoroutineInfo::Coroutine
	RuntimeObject* ___Coroutine_0;
	// System.Threading.Tasks.TaskCompletionSource`1<System.Boolean> UnityGLTF.AsyncCoroutineHelper/CoroutineInfo::Tcs
	TaskCompletionSource_1_t99FE296FC744F35118789383C0C0F3156E38E240 * ___Tcs_1;
	// System.String UnityGLTF.AsyncCoroutineHelper/CoroutineInfo::Name
	String_t* ___Name_2;

public:
	inline static int32_t get_offset_of_Coroutine_0() { return static_cast<int32_t>(offsetof(CoroutineInfo_tE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE, ___Coroutine_0)); }
	inline RuntimeObject* get_Coroutine_0() const { return ___Coroutine_0; }
	inline RuntimeObject** get_address_of_Coroutine_0() { return &___Coroutine_0; }
	inline void set_Coroutine_0(RuntimeObject* value)
	{
		___Coroutine_0 = value;
		Il2CppCodeGenWriteBarrier((&___Coroutine_0), value);
	}

	inline static int32_t get_offset_of_Tcs_1() { return static_cast<int32_t>(offsetof(CoroutineInfo_tE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE, ___Tcs_1)); }
	inline TaskCompletionSource_1_t99FE296FC744F35118789383C0C0F3156E38E240 * get_Tcs_1() const { return ___Tcs_1; }
	inline TaskCompletionSource_1_t99FE296FC744F35118789383C0C0F3156E38E240 ** get_address_of_Tcs_1() { return &___Tcs_1; }
	inline void set_Tcs_1(TaskCompletionSource_1_t99FE296FC744F35118789383C0C0F3156E38E240 * value)
	{
		___Tcs_1 = value;
		Il2CppCodeGenWriteBarrier((&___Tcs_1), value);
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(CoroutineInfo_tE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityGLTF.AsyncCoroutineHelper/CoroutineInfo
struct CoroutineInfo_tE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE_marshaled_pinvoke
{
	RuntimeObject* ___Coroutine_0;
	TaskCompletionSource_1_t99FE296FC744F35118789383C0C0F3156E38E240 * ___Tcs_1;
	char* ___Name_2;
};
// Native definition for COM marshalling of UnityGLTF.AsyncCoroutineHelper/CoroutineInfo
struct CoroutineInfo_tE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE_marshaled_com
{
	RuntimeObject* ___Coroutine_0;
	TaskCompletionSource_1_t99FE296FC744F35118789383C0C0F3156E38E240 * ___Tcs_1;
	Il2CppChar* ___Name_2;
};
#endif // COROUTINEINFO_TE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE_H
#ifndef GLTFLOADEXCEPTION_T349EF913EF0D9ABB54A482B113C96B44F996DC31_H
#define GLTFLOADEXCEPTION_T349EF913EF0D9ABB54A482B113C96B44F996DC31_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFLoadException
struct  GLTFLoadException_t349EF913EF0D9ABB54A482B113C96B44F996DC31  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFLOADEXCEPTION_T349EF913EF0D9ABB54A482B113C96B44F996DC31_H
#ifndef PRIMKEY_T1517CCEE386BDB5C1D2D1BBB71AE633BE6DBA196_H
#define PRIMKEY_T1517CCEE386BDB5C1D2D1BBB71AE633BE6DBA196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneExporter/PrimKey
struct  PrimKey_t1517CCEE386BDB5C1D2D1BBB71AE633BE6DBA196 
{
public:
	// UnityEngine.Mesh UnityGLTF.GLTFSceneExporter/PrimKey::Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___Mesh_0;
	// UnityEngine.Material UnityGLTF.GLTFSceneExporter/PrimKey::Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___Material_1;

public:
	inline static int32_t get_offset_of_Mesh_0() { return static_cast<int32_t>(offsetof(PrimKey_t1517CCEE386BDB5C1D2D1BBB71AE633BE6DBA196, ___Mesh_0)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_Mesh_0() const { return ___Mesh_0; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_Mesh_0() { return &___Mesh_0; }
	inline void set_Mesh_0(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___Mesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___Mesh_0), value);
	}

	inline static int32_t get_offset_of_Material_1() { return static_cast<int32_t>(offsetof(PrimKey_t1517CCEE386BDB5C1D2D1BBB71AE633BE6DBA196, ___Material_1)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_Material_1() const { return ___Material_1; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_Material_1() { return &___Material_1; }
	inline void set_Material_1(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___Material_1 = value;
		Il2CppCodeGenWriteBarrier((&___Material_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityGLTF.GLTFSceneExporter/PrimKey
struct PrimKey_t1517CCEE386BDB5C1D2D1BBB71AE633BE6DBA196_marshaled_pinvoke
{
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___Mesh_0;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___Material_1;
};
// Native definition for COM marshalling of UnityGLTF.GLTFSceneExporter/PrimKey
struct PrimKey_t1517CCEE386BDB5C1D2D1BBB71AE633BE6DBA196_marshaled_com
{
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___Mesh_0;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___Material_1;
};
#endif // PRIMKEY_T1517CCEE386BDB5C1D2D1BBB71AE633BE6DBA196_H
#ifndef GLBSTREAM_T261F875F33704F47106C7188FBAB3CDE1145505A_H
#define GLBSTREAM_T261F875F33704F47106C7188FBAB3CDE1145505A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/GLBStream
struct  GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A 
{
public:
	// System.IO.Stream UnityGLTF.GLTFSceneImporter/GLBStream::Stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Stream_0;
	// System.Int64 UnityGLTF.GLTFSceneImporter/GLBStream::StartPosition
	int64_t ___StartPosition_1;

public:
	inline static int32_t get_offset_of_Stream_0() { return static_cast<int32_t>(offsetof(GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A, ___Stream_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Stream_0() const { return ___Stream_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Stream_0() { return &___Stream_0; }
	inline void set_Stream_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___Stream_0), value);
	}

	inline static int32_t get_offset_of_StartPosition_1() { return static_cast<int32_t>(offsetof(GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A, ___StartPosition_1)); }
	inline int64_t get_StartPosition_1() const { return ___StartPosition_1; }
	inline int64_t* get_address_of_StartPosition_1() { return &___StartPosition_1; }
	inline void set_StartPosition_1(int64_t value)
	{
		___StartPosition_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityGLTF.GLTFSceneImporter/GLBStream
struct GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A_marshaled_pinvoke
{
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Stream_0;
	int64_t ___StartPosition_1;
};
// Native definition for COM marshalling of UnityGLTF.GLTFSceneImporter/GLBStream
struct GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A_marshaled_com
{
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Stream_0;
	int64_t ___StartPosition_1;
};
#endif // GLBSTREAM_T261F875F33704F47106C7188FBAB3CDE1145505A_H
#ifndef MESHCONSTRUCTIONDATA_TC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419_H
#define MESHCONSTRUCTIONDATA_TC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.MeshConstructionData
struct  MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419 
{
public:
	// GLTF.Schema.MeshPrimitive UnityGLTF.MeshConstructionData::<Primitive>k__BackingField
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * ___U3CPrimitiveU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,GLTF.AttributeAccessor> UnityGLTF.MeshConstructionData::<MeshAttributes>k__BackingField
	Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C * ___U3CMeshAttributesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CPrimitiveU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419, ___U3CPrimitiveU3Ek__BackingField_0)); }
	inline MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * get_U3CPrimitiveU3Ek__BackingField_0() const { return ___U3CPrimitiveU3Ek__BackingField_0; }
	inline MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 ** get_address_of_U3CPrimitiveU3Ek__BackingField_0() { return &___U3CPrimitiveU3Ek__BackingField_0; }
	inline void set_U3CPrimitiveU3Ek__BackingField_0(MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * value)
	{
		___U3CPrimitiveU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrimitiveU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CMeshAttributesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419, ___U3CMeshAttributesU3Ek__BackingField_1)); }
	inline Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C * get_U3CMeshAttributesU3Ek__BackingField_1() const { return ___U3CMeshAttributesU3Ek__BackingField_1; }
	inline Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C ** get_address_of_U3CMeshAttributesU3Ek__BackingField_1() { return &___U3CMeshAttributesU3Ek__BackingField_1; }
	inline void set_U3CMeshAttributesU3Ek__BackingField_1(Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C * value)
	{
		___U3CMeshAttributesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMeshAttributesU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityGLTF.MeshConstructionData
struct MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419_marshaled_pinvoke
{
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * ___U3CPrimitiveU3Ek__BackingField_0;
	Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C * ___U3CMeshAttributesU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityGLTF.MeshConstructionData
struct MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419_marshaled_com
{
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * ___U3CPrimitiveU3Ek__BackingField_0;
	Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C * ___U3CMeshAttributesU3Ek__BackingField_1;
};
#endif // MESHCONSTRUCTIONDATA_TC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419_H
#ifndef SHADERNOTFOUNDEXCEPTION_TBEB66BDDF37AA66E1B27463A3F2013784E302180_H
#define SHADERNOTFOUNDEXCEPTION_TBEB66BDDF37AA66E1B27463A3F2013784E302180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.ShaderNotFoundException
struct  ShaderNotFoundException_tBEB66BDDF37AA66E1B27463A3F2013784E302180  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERNOTFOUNDEXCEPTION_TBEB66BDDF37AA66E1B27463A3F2013784E302180_H
#ifndef WEBREQUESTEXCEPTION_TFB2B03AD112281C424703E9FAC8D0B69520E7887_H
#define WEBREQUESTEXCEPTION_TFB2B03AD112281C424703E9FAC8D0B69520E7887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.WebRequestException
struct  WebRequestException_tFB2B03AD112281C424703E9FAC8D0B69520E7887  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTEXCEPTION_TFB2B03AD112281C424703E9FAC8D0B69520E7887_H
#ifndef ENUMERATOR_T77DA7CAFDC0A844FD705B49C2E730DE0B8D08B5E_H
#define ENUMERATOR_T77DA7CAFDC0A844FD705B49C2E730DE0B8D08B5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,GLTF.Schema.AccessorId>
struct  Enumerator_t77DA7CAFDC0A844FD705B49C2E730DE0B8D08B5E 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_tFA101F0B1ABA5C9CC83DEA8915A01ED1E64CB071 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::version
	int32_t ___version_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::index
	int32_t ___index_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_tA9A5287EAB0CB41B20FD057EA72334780CDC4A9A  ___current_3;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::getEnumeratorRetType
	int32_t ___getEnumeratorRetType_4;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t77DA7CAFDC0A844FD705B49C2E730DE0B8D08B5E, ___dictionary_0)); }
	inline Dictionary_2_tFA101F0B1ABA5C9CC83DEA8915A01ED1E64CB071 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_tFA101F0B1ABA5C9CC83DEA8915A01ED1E64CB071 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_tFA101F0B1ABA5C9CC83DEA8915A01ED1E64CB071 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(Enumerator_t77DA7CAFDC0A844FD705B49C2E730DE0B8D08B5E, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Enumerator_t77DA7CAFDC0A844FD705B49C2E730DE0B8D08B5E, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t77DA7CAFDC0A844FD705B49C2E730DE0B8D08B5E, ___current_3)); }
	inline KeyValuePair_2_tA9A5287EAB0CB41B20FD057EA72334780CDC4A9A  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_tA9A5287EAB0CB41B20FD057EA72334780CDC4A9A * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_tA9A5287EAB0CB41B20FD057EA72334780CDC4A9A  value)
	{
		___current_3 = value;
	}

	inline static int32_t get_offset_of_getEnumeratorRetType_4() { return static_cast<int32_t>(offsetof(Enumerator_t77DA7CAFDC0A844FD705B49C2E730DE0B8D08B5E, ___getEnumeratorRetType_4)); }
	inline int32_t get_getEnumeratorRetType_4() const { return ___getEnumeratorRetType_4; }
	inline int32_t* get_address_of_getEnumeratorRetType_4() { return &___getEnumeratorRetType_4; }
	inline void set_getEnumeratorRetType_4(int32_t value)
	{
		___getEnumeratorRetType_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T77DA7CAFDC0A844FD705B49C2E730DE0B8D08B5E_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef ASYNCTASKMETHODBUILDER_1_T66ED1808B26B8081A2804D6A750D13386E360BD9_H
#define ASYNCTASKMETHODBUILDER_1_T66ED1808B26B8081A2804D6A750D13386E360BD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult>
struct  AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9, ___m_task_2)); }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T66ED1808B26B8081A2804D6A750D13386E360BD9_H
#ifndef ASYNCTASKMETHODBUILDER_1_TC7219A08BD3DD7AA457D88CB3FC2B69ABBED2A73_H
#define ASYNCTASKMETHODBUILDER_1_TC7219A08BD3DD7AA457D88CB3FC2B69ABBED2A73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<UnityEngine.Material>
struct  AsyncTaskMethodBuilder_1_tC7219A08BD3DD7AA457D88CB3FC2B69ABBED2A73 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t74879D22F3CC6429D4D6FCC94A7597CE737CB1DA * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_tC7219A08BD3DD7AA457D88CB3FC2B69ABBED2A73, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_tC7219A08BD3DD7AA457D88CB3FC2B69ABBED2A73, ___m_task_2)); }
	inline Task_1_t74879D22F3CC6429D4D6FCC94A7597CE737CB1DA * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t74879D22F3CC6429D4D6FCC94A7597CE737CB1DA ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t74879D22F3CC6429D4D6FCC94A7597CE737CB1DA * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_tC7219A08BD3DD7AA457D88CB3FC2B69ABBED2A73_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t74879D22F3CC6429D4D6FCC94A7597CE737CB1DA * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_tC7219A08BD3DD7AA457D88CB3FC2B69ABBED2A73_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t74879D22F3CC6429D4D6FCC94A7597CE737CB1DA * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t74879D22F3CC6429D4D6FCC94A7597CE737CB1DA ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t74879D22F3CC6429D4D6FCC94A7597CE737CB1DA * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_TC7219A08BD3DD7AA457D88CB3FC2B69ABBED2A73_H
#ifndef ASYNCVOIDMETHODBUILDER_T44E3C9B52B019BB5BDCC0E1BB83188B536161CFF_H
#define ASYNCVOIDMETHODBUILDER_T44E3C9B52B019BB5BDCC0E1BB83188B536161CFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct  AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF 
{
public:
	// System.Threading.SynchronizationContext System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_synchronizationContext
	SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * ___m_synchronizationContext_0;
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_coreState
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  ___m_coreState_1;
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_task
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_synchronizationContext_0() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF, ___m_synchronizationContext_0)); }
	inline SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * get_m_synchronizationContext_0() const { return ___m_synchronizationContext_0; }
	inline SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 ** get_address_of_m_synchronizationContext_0() { return &___m_synchronizationContext_0; }
	inline void set_m_synchronizationContext_0(SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * value)
	{
		___m_synchronizationContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_synchronizationContext_0), value);
	}

	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF, ___m_task_2)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_m_task_2() const { return ___m_task_2; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF_marshaled_pinvoke
{
	SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_pinvoke ___m_coreState_1;
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_2;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF_marshaled_com
{
	SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_com ___m_coreState_1;
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_2;
};
#endif // ASYNCVOIDMETHODBUILDER_T44E3C9B52B019BB5BDCC0E1BB83188B536161CFF_H
#ifndef THREADPRIORITY_TA18DA7C04EFC2F3A9C97A1F52B9AC531692B2762_H
#define THREADPRIORITY_TA18DA7C04EFC2F3A9C97A1F52B9AC531692B2762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadPriority
struct  ThreadPriority_tA18DA7C04EFC2F3A9C97A1F52B9AC531692B2762 
{
public:
	// System.Int32 System.Threading.ThreadPriority::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ThreadPriority_tA18DA7C04EFC2F3A9C97A1F52B9AC531692B2762, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADPRIORITY_TA18DA7C04EFC2F3A9C97A1F52B9AC531692B2762_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef U3CCALLMETHODONMAINTHREADU3ED__2_T3A63AC9BEB4E68967F37A617F435D3F9FE40F06C_H
#define U3CCALLMETHODONMAINTHREADU3ED__2_T3A63AC9BEB4E68967F37A617F435D3F9FE40F06C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.AsyncCoroutineHelper/<CallMethodOnMainThread>d__2
struct  U3CCallMethodOnMainThreadU3Ed__2_t3A63AC9BEB4E68967F37A617F435D3F9FE40F06C  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.AsyncCoroutineHelper/<CallMethodOnMainThread>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.AsyncCoroutineHelper/<CallMethodOnMainThread>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityGLTF.AsyncCoroutineHelper/CoroutineInfo UnityGLTF.AsyncCoroutineHelper/<CallMethodOnMainThread>d__2::coroutineInfo
	CoroutineInfo_tE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE  ___coroutineInfo_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCallMethodOnMainThreadU3Ed__2_t3A63AC9BEB4E68967F37A617F435D3F9FE40F06C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCallMethodOnMainThreadU3Ed__2_t3A63AC9BEB4E68967F37A617F435D3F9FE40F06C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_coroutineInfo_2() { return static_cast<int32_t>(offsetof(U3CCallMethodOnMainThreadU3Ed__2_t3A63AC9BEB4E68967F37A617F435D3F9FE40F06C, ___coroutineInfo_2)); }
	inline CoroutineInfo_tE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE  get_coroutineInfo_2() const { return ___coroutineInfo_2; }
	inline CoroutineInfo_tE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE * get_address_of_coroutineInfo_2() { return &___coroutineInfo_2; }
	inline void set_coroutineInfo_2(CoroutineInfo_tE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE  value)
	{
		___coroutineInfo_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCALLMETHODONMAINTHREADU3ED__2_T3A63AC9BEB4E68967F37A617F435D3F9FE40F06C_H
#ifndef IMAGETYPE_T08208151488743B3EA18DC95B5732968BA125066_H
#define IMAGETYPE_T08208151488743B3EA18DC95B5732968BA125066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneExporter/IMAGETYPE
struct  IMAGETYPE_t08208151488743B3EA18DC95B5732968BA125066 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneExporter/IMAGETYPE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IMAGETYPE_t08208151488743B3EA18DC95B5732968BA125066, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETYPE_T08208151488743B3EA18DC95B5732968BA125066_H
#ifndef TEXTUREMAPTYPE_TEF0E81720CC8AB381953FB9A49FCEADD143BFFC9_H
#define TEXTUREMAPTYPE_TEF0E81720CC8AB381953FB9A49FCEADD143BFFC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneExporter/TextureMapType
struct  TextureMapType_tEF0E81720CC8AB381953FB9A49FCEADD143BFFC9 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneExporter/TextureMapType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureMapType_tEF0E81720CC8AB381953FB9A49FCEADD143BFFC9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMAPTYPE_TEF0E81720CC8AB381953FB9A49FCEADD143BFFC9_H
#ifndef U3CU3EC__DISPLAYCLASS73_0_TD494223E41D1BFA7571AC3568E9A99A8B3FED886_H
#define U3CU3EC__DISPLAYCLASS73_0_TD494223E41D1BFA7571AC3568E9A99A8B3FED886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<>c__DisplayClass73_0
struct  U3CU3Ec__DisplayClass73_0_tD494223E41D1BFA7571AC3568E9A99A8B3FED886  : public RuntimeObject
{
public:
	// UnityGLTF.UnityMeshData UnityGLTF.GLTFSceneImporter/<>c__DisplayClass73_0::unityMeshData
	UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D * ___unityMeshData_0;
	// UnityGLTF.MeshConstructionData UnityGLTF.GLTFSceneImporter/<>c__DisplayClass73_0::meshConstructionData
	MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419  ___meshConstructionData_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<>c__DisplayClass73_0::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_unityMeshData_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass73_0_tD494223E41D1BFA7571AC3568E9A99A8B3FED886, ___unityMeshData_0)); }
	inline UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D * get_unityMeshData_0() const { return ___unityMeshData_0; }
	inline UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D ** get_address_of_unityMeshData_0() { return &___unityMeshData_0; }
	inline void set_unityMeshData_0(UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D * value)
	{
		___unityMeshData_0 = value;
		Il2CppCodeGenWriteBarrier((&___unityMeshData_0), value);
	}

	inline static int32_t get_offset_of_meshConstructionData_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass73_0_tD494223E41D1BFA7571AC3568E9A99A8B3FED886, ___meshConstructionData_1)); }
	inline MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419  get_meshConstructionData_1() const { return ___meshConstructionData_1; }
	inline MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419 * get_address_of_meshConstructionData_1() { return &___meshConstructionData_1; }
	inline void set_meshConstructionData_1(MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419  value)
	{
		___meshConstructionData_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass73_0_tD494223E41D1BFA7571AC3568E9A99A8B3FED886, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS73_0_TD494223E41D1BFA7571AC3568E9A99A8B3FED886_H
#ifndef COLLIDERTYPE_TF7E70C605402A47138095F21308771417EE85FFA_H
#define COLLIDERTYPE_TF7E70C605402A47138095F21308771417EE85FFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/ColliderType
struct  ColliderType_tF7E70C605402A47138095F21308771417EE85FFA 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/ColliderType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColliderType_tF7E70C605402A47138095F21308771417EE85FFA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDERTYPE_TF7E70C605402A47138095F21308771417EE85FFA_H
#ifndef TASKSORTINGSYSTEM_T69AB27974F2B4DEF923567D25F773D47B3DD0F22_H
#define TASKSORTINGSYSTEM_T69AB27974F2B4DEF923567D25F773D47B3DD0F22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskSortingSystem
struct  TaskSortingSystem_t69AB27974F2B4DEF923567D25F773D47B3DD0F22 
{
public:
	// System.Int32 UnityThreading.TaskSortingSystem::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TaskSortingSystem_t69AB27974F2B4DEF923567D25F773D47B3DD0F22, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKSORTINGSYSTEM_T69AB27974F2B4DEF923567D25F773D47B3DD0F22_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef ASYNCTASKMETHODBUILDER_T0CD1893D670405BED201BE8CA6F2E811F2C0F487_H
#define ASYNCTASKMETHODBUILDER_T0CD1893D670405BED201BE8CA6F2E811F2C0F487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct  AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 
{
public:
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::m_builder
	AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  ___m_builder_1;

public:
	inline static int32_t get_offset_of_m_builder_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487, ___m_builder_1)); }
	inline AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  get_m_builder_1() const { return ___m_builder_1; }
	inline AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9 * get_address_of_m_builder_1() { return &___m_builder_1; }
	inline void set_m_builder_1(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  value)
	{
		___m_builder_1 = value;
	}
};

struct AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::s_cachedCompleted
	Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * ___s_cachedCompleted_0;

public:
	inline static int32_t get_offset_of_s_cachedCompleted_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_StaticFields, ___s_cachedCompleted_0)); }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * get_s_cachedCompleted_0() const { return ___s_cachedCompleted_0; }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 ** get_address_of_s_cachedCompleted_0() { return &___s_cachedCompleted_0; }
	inline void set_s_cachedCompleted_0(Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * value)
	{
		___s_cachedCompleted_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_cachedCompleted_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_marshaled_pinvoke
{
	AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  ___m_builder_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_marshaled_com
{
	AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  ___m_builder_1;
};
#endif // ASYNCTASKMETHODBUILDER_T0CD1893D670405BED201BE8CA6F2E811F2C0F487_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef U3CSTARTU3ED__19_T8DAB112441359EA42B501246B5BF30F664AD6856_H
#define U3CSTARTU3ED__19_T8DAB112441359EA42B501246B5BF30F664AD6856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFComponent/<Start>d__19
struct  U3CStartU3Ed__19_t8DAB112441359EA42B501246B5BF30F664AD6856 
{
public:
	// System.Int32 UnityGLTF.GLTFComponent/<Start>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncVoidMethodBuilder UnityGLTF.GLTFComponent/<Start>d__19::<>t__builder
	AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFComponent UnityGLTF.GLTFComponent/<Start>d__19::<>4__this
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 * ___U3CU3E4__this_2;
	// System.Object UnityGLTF.GLTFComponent/<Start>d__19::<>7__wrap1
	RuntimeObject * ___U3CU3E7__wrap1_3;
	// System.Int32 UnityGLTF.GLTFComponent/<Start>d__19::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_4;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFComponent/<Start>d__19::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__19_t8DAB112441359EA42B501246B5BF30F664AD6856, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__19_t8DAB112441359EA42B501246B5BF30F664AD6856, ___U3CU3Et__builder_1)); }
	inline AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__19_t8DAB112441359EA42B501246B5BF30F664AD6856, ___U3CU3E4__this_2)); }
	inline GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__19_t8DAB112441359EA42B501246B5BF30F664AD6856, ___U3CU3E7__wrap1_3)); }
	inline RuntimeObject * get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline RuntimeObject ** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(RuntimeObject * value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__19_t8DAB112441359EA42B501246B5BF30F664AD6856, ___U3CU3E7__wrap2_4)); }
	inline int32_t get_U3CU3E7__wrap2_4() const { return ___U3CU3E7__wrap2_4; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_4() { return &___U3CU3E7__wrap2_4; }
	inline void set_U3CU3E7__wrap2_4(int32_t value)
	{
		___U3CU3E7__wrap2_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__19_t8DAB112441359EA42B501246B5BF30F664AD6856, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__19_T8DAB112441359EA42B501246B5BF30F664AD6856_H
#ifndef IMAGEINFO_TA735B62CE32CD32BA3FD2C3E124C0E97509E0AC6_H
#define IMAGEINFO_TA735B62CE32CD32BA3FD2C3E124C0E97509E0AC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneExporter/ImageInfo
struct  ImageInfo_tA735B62CE32CD32BA3FD2C3E124C0E97509E0AC6 
{
public:
	// UnityEngine.Texture2D UnityGLTF.GLTFSceneExporter/ImageInfo::texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture_0;
	// UnityGLTF.GLTFSceneExporter/TextureMapType UnityGLTF.GLTFSceneExporter/ImageInfo::textureMapType
	int32_t ___textureMapType_1;

public:
	inline static int32_t get_offset_of_texture_0() { return static_cast<int32_t>(offsetof(ImageInfo_tA735B62CE32CD32BA3FD2C3E124C0E97509E0AC6, ___texture_0)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_texture_0() const { return ___texture_0; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_texture_0() { return &___texture_0; }
	inline void set_texture_0(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___texture_0 = value;
		Il2CppCodeGenWriteBarrier((&___texture_0), value);
	}

	inline static int32_t get_offset_of_textureMapType_1() { return static_cast<int32_t>(offsetof(ImageInfo_tA735B62CE32CD32BA3FD2C3E124C0E97509E0AC6, ___textureMapType_1)); }
	inline int32_t get_textureMapType_1() const { return ___textureMapType_1; }
	inline int32_t* get_address_of_textureMapType_1() { return &___textureMapType_1; }
	inline void set_textureMapType_1(int32_t value)
	{
		___textureMapType_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityGLTF.GLTFSceneExporter/ImageInfo
struct ImageInfo_tA735B62CE32CD32BA3FD2C3E124C0E97509E0AC6_marshaled_pinvoke
{
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture_0;
	int32_t ___textureMapType_1;
};
// Native definition for COM marshalling of UnityGLTF.GLTFSceneExporter/ImageInfo
struct ImageInfo_tA735B62CE32CD32BA3FD2C3E124C0E97509E0AC6_marshaled_com
{
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture_0;
	int32_t ___textureMapType_1;
};
#endif // IMAGEINFO_TA735B62CE32CD32BA3FD2C3E124C0E97509E0AC6_H
#ifndef GLTFSCENEIMPORTER_TB010C9E6BC9B9F07ECE0434D11FC3BE062276854_H
#define GLTFSCENEIMPORTER_TB010C9E6BC9B9F07ECE0434D11FC3BE062276854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter
struct  GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter::MaximumLod
	int32_t ___MaximumLod_0;
	// System.Int32 UnityGLTF.GLTFSceneImporter::Timeout
	int32_t ___Timeout_1;
	// System.Boolean UnityGLTF.GLTFSceneImporter::isMultithreaded
	bool ___isMultithreaded_2;
	// UnityEngine.Transform UnityGLTF.GLTFSceneImporter::<SceneParent>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CSceneParentU3Ek__BackingField_3;
	// UnityEngine.GameObject UnityGLTF.GLTFSceneImporter::<CreatedObject>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CCreatedObjectU3Ek__BackingField_4;
	// UnityGLTF.GLTFSceneImporter/ColliderType UnityGLTF.GLTFSceneImporter::<Collider>k__BackingField
	int32_t ___U3CColliderU3Ek__BackingField_5;
	// System.String UnityGLTF.GLTFSceneImporter::<CustomShaderName>k__BackingField
	String_t* ___U3CCustomShaderNameU3Ek__BackingField_6;
	// System.Single UnityGLTF.GLTFSceneImporter::BudgetPerFrameInMilliseconds
	float ___BudgetPerFrameInMilliseconds_7;
	// System.Boolean UnityGLTF.GLTFSceneImporter::KeepCPUCopyOfMesh
	bool ___KeepCPUCopyOfMesh_8;
	// System.Single UnityGLTF.GLTFSceneImporter::_timeAtLastYield
	float ____timeAtLastYield_9;
	// UnityGLTF.AsyncCoroutineHelper UnityGLTF.GLTFSceneImporter::_asyncCoroutineHelper
	AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1 * ____asyncCoroutineHelper_10;
	// UnityEngine.GameObject UnityGLTF.GLTFSceneImporter::_lastLoadedScene
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____lastLoadedScene_11;
	// GLTF.Schema.GLTFMaterial UnityGLTF.GLTFSceneImporter::DefaultMaterial
	GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 * ___DefaultMaterial_12;
	// UnityGLTF.Cache.MaterialCacheData UnityGLTF.GLTFSceneImporter::_defaultLoadedMaterial
	MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1 * ____defaultLoadedMaterial_13;
	// System.String UnityGLTF.GLTFSceneImporter::_gltfFileName
	String_t* ____gltfFileName_14;
	// UnityGLTF.GLTFSceneImporter/GLBStream UnityGLTF.GLTFSceneImporter::_gltfStream
	GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A  ____gltfStream_15;
	// GLTF.Schema.GLTFRoot UnityGLTF.GLTFSceneImporter::_gltfRoot
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ____gltfRoot_16;
	// UnityGLTF.Cache.AssetCache UnityGLTF.GLTFSceneImporter::_assetCache
	AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350 * ____assetCache_17;
	// UnityGLTF.Loader.ILoader UnityGLTF.GLTFSceneImporter::_loader
	RuntimeObject* ____loader_18;
	// System.Boolean UnityGLTF.GLTFSceneImporter::_isRunning
	bool ____isRunning_19;

public:
	inline static int32_t get_offset_of_MaximumLod_0() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ___MaximumLod_0)); }
	inline int32_t get_MaximumLod_0() const { return ___MaximumLod_0; }
	inline int32_t* get_address_of_MaximumLod_0() { return &___MaximumLod_0; }
	inline void set_MaximumLod_0(int32_t value)
	{
		___MaximumLod_0 = value;
	}

	inline static int32_t get_offset_of_Timeout_1() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ___Timeout_1)); }
	inline int32_t get_Timeout_1() const { return ___Timeout_1; }
	inline int32_t* get_address_of_Timeout_1() { return &___Timeout_1; }
	inline void set_Timeout_1(int32_t value)
	{
		___Timeout_1 = value;
	}

	inline static int32_t get_offset_of_isMultithreaded_2() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ___isMultithreaded_2)); }
	inline bool get_isMultithreaded_2() const { return ___isMultithreaded_2; }
	inline bool* get_address_of_isMultithreaded_2() { return &___isMultithreaded_2; }
	inline void set_isMultithreaded_2(bool value)
	{
		___isMultithreaded_2 = value;
	}

	inline static int32_t get_offset_of_U3CSceneParentU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ___U3CSceneParentU3Ek__BackingField_3)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CSceneParentU3Ek__BackingField_3() const { return ___U3CSceneParentU3Ek__BackingField_3; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CSceneParentU3Ek__BackingField_3() { return &___U3CSceneParentU3Ek__BackingField_3; }
	inline void set_U3CSceneParentU3Ek__BackingField_3(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CSceneParentU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSceneParentU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CCreatedObjectU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ___U3CCreatedObjectU3Ek__BackingField_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CCreatedObjectU3Ek__BackingField_4() const { return ___U3CCreatedObjectU3Ek__BackingField_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CCreatedObjectU3Ek__BackingField_4() { return &___U3CCreatedObjectU3Ek__BackingField_4; }
	inline void set_U3CCreatedObjectU3Ek__BackingField_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CCreatedObjectU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCreatedObjectU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CColliderU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ___U3CColliderU3Ek__BackingField_5)); }
	inline int32_t get_U3CColliderU3Ek__BackingField_5() const { return ___U3CColliderU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CColliderU3Ek__BackingField_5() { return &___U3CColliderU3Ek__BackingField_5; }
	inline void set_U3CColliderU3Ek__BackingField_5(int32_t value)
	{
		___U3CColliderU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CCustomShaderNameU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ___U3CCustomShaderNameU3Ek__BackingField_6)); }
	inline String_t* get_U3CCustomShaderNameU3Ek__BackingField_6() const { return ___U3CCustomShaderNameU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CCustomShaderNameU3Ek__BackingField_6() { return &___U3CCustomShaderNameU3Ek__BackingField_6; }
	inline void set_U3CCustomShaderNameU3Ek__BackingField_6(String_t* value)
	{
		___U3CCustomShaderNameU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomShaderNameU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_BudgetPerFrameInMilliseconds_7() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ___BudgetPerFrameInMilliseconds_7)); }
	inline float get_BudgetPerFrameInMilliseconds_7() const { return ___BudgetPerFrameInMilliseconds_7; }
	inline float* get_address_of_BudgetPerFrameInMilliseconds_7() { return &___BudgetPerFrameInMilliseconds_7; }
	inline void set_BudgetPerFrameInMilliseconds_7(float value)
	{
		___BudgetPerFrameInMilliseconds_7 = value;
	}

	inline static int32_t get_offset_of_KeepCPUCopyOfMesh_8() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ___KeepCPUCopyOfMesh_8)); }
	inline bool get_KeepCPUCopyOfMesh_8() const { return ___KeepCPUCopyOfMesh_8; }
	inline bool* get_address_of_KeepCPUCopyOfMesh_8() { return &___KeepCPUCopyOfMesh_8; }
	inline void set_KeepCPUCopyOfMesh_8(bool value)
	{
		___KeepCPUCopyOfMesh_8 = value;
	}

	inline static int32_t get_offset_of__timeAtLastYield_9() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____timeAtLastYield_9)); }
	inline float get__timeAtLastYield_9() const { return ____timeAtLastYield_9; }
	inline float* get_address_of__timeAtLastYield_9() { return &____timeAtLastYield_9; }
	inline void set__timeAtLastYield_9(float value)
	{
		____timeAtLastYield_9 = value;
	}

	inline static int32_t get_offset_of__asyncCoroutineHelper_10() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____asyncCoroutineHelper_10)); }
	inline AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1 * get__asyncCoroutineHelper_10() const { return ____asyncCoroutineHelper_10; }
	inline AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1 ** get_address_of__asyncCoroutineHelper_10() { return &____asyncCoroutineHelper_10; }
	inline void set__asyncCoroutineHelper_10(AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1 * value)
	{
		____asyncCoroutineHelper_10 = value;
		Il2CppCodeGenWriteBarrier((&____asyncCoroutineHelper_10), value);
	}

	inline static int32_t get_offset_of__lastLoadedScene_11() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____lastLoadedScene_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__lastLoadedScene_11() const { return ____lastLoadedScene_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__lastLoadedScene_11() { return &____lastLoadedScene_11; }
	inline void set__lastLoadedScene_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____lastLoadedScene_11 = value;
		Il2CppCodeGenWriteBarrier((&____lastLoadedScene_11), value);
	}

	inline static int32_t get_offset_of_DefaultMaterial_12() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ___DefaultMaterial_12)); }
	inline GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 * get_DefaultMaterial_12() const { return ___DefaultMaterial_12; }
	inline GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 ** get_address_of_DefaultMaterial_12() { return &___DefaultMaterial_12; }
	inline void set_DefaultMaterial_12(GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 * value)
	{
		___DefaultMaterial_12 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultMaterial_12), value);
	}

	inline static int32_t get_offset_of__defaultLoadedMaterial_13() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____defaultLoadedMaterial_13)); }
	inline MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1 * get__defaultLoadedMaterial_13() const { return ____defaultLoadedMaterial_13; }
	inline MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1 ** get_address_of__defaultLoadedMaterial_13() { return &____defaultLoadedMaterial_13; }
	inline void set__defaultLoadedMaterial_13(MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1 * value)
	{
		____defaultLoadedMaterial_13 = value;
		Il2CppCodeGenWriteBarrier((&____defaultLoadedMaterial_13), value);
	}

	inline static int32_t get_offset_of__gltfFileName_14() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____gltfFileName_14)); }
	inline String_t* get__gltfFileName_14() const { return ____gltfFileName_14; }
	inline String_t** get_address_of__gltfFileName_14() { return &____gltfFileName_14; }
	inline void set__gltfFileName_14(String_t* value)
	{
		____gltfFileName_14 = value;
		Il2CppCodeGenWriteBarrier((&____gltfFileName_14), value);
	}

	inline static int32_t get_offset_of__gltfStream_15() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____gltfStream_15)); }
	inline GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A  get__gltfStream_15() const { return ____gltfStream_15; }
	inline GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A * get_address_of__gltfStream_15() { return &____gltfStream_15; }
	inline void set__gltfStream_15(GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A  value)
	{
		____gltfStream_15 = value;
	}

	inline static int32_t get_offset_of__gltfRoot_16() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____gltfRoot_16)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get__gltfRoot_16() const { return ____gltfRoot_16; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of__gltfRoot_16() { return &____gltfRoot_16; }
	inline void set__gltfRoot_16(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		____gltfRoot_16 = value;
		Il2CppCodeGenWriteBarrier((&____gltfRoot_16), value);
	}

	inline static int32_t get_offset_of__assetCache_17() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____assetCache_17)); }
	inline AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350 * get__assetCache_17() const { return ____assetCache_17; }
	inline AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350 ** get_address_of__assetCache_17() { return &____assetCache_17; }
	inline void set__assetCache_17(AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350 * value)
	{
		____assetCache_17 = value;
		Il2CppCodeGenWriteBarrier((&____assetCache_17), value);
	}

	inline static int32_t get_offset_of__loader_18() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____loader_18)); }
	inline RuntimeObject* get__loader_18() const { return ____loader_18; }
	inline RuntimeObject** get_address_of__loader_18() { return &____loader_18; }
	inline void set__loader_18(RuntimeObject* value)
	{
		____loader_18 = value;
		Il2CppCodeGenWriteBarrier((&____loader_18), value);
	}

	inline static int32_t get_offset_of__isRunning_19() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____isRunning_19)); }
	inline bool get__isRunning_19() const { return ____isRunning_19; }
	inline bool* get_address_of__isRunning_19() { return &____isRunning_19; }
	inline void set__isRunning_19(bool value)
	{
		____isRunning_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFSCENEIMPORTER_TB010C9E6BC9B9F07ECE0434D11FC3BE062276854_H
#ifndef U3CLOADMATERIALASYNCU3ED__43_T8CB980C4845985ECEBC51F97822270F1CD99C95F_H
#define U3CLOADMATERIALASYNCU3ED__43_T8CB980C4845985ECEBC51F97822270F1CD99C95F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<LoadMaterialAsync>d__43
struct  U3CLoadMaterialAsyncU3Ed__43_t8CB980C4845985ECEBC51F97822270F1CD99C95F 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<LoadMaterialAsync>d__43::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<UnityEngine.Material> UnityGLTF.GLTFSceneImporter/<LoadMaterialAsync>d__43::<>t__builder
	AsyncTaskMethodBuilder_1_tC7219A08BD3DD7AA457D88CB3FC2B69ABBED2A73  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<LoadMaterialAsync>d__43::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<LoadMaterialAsync>d__43::materialIndex
	int32_t ___materialIndex_3;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<LoadMaterialAsync>d__43::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_4;
	// GLTF.Schema.GLTFMaterial UnityGLTF.GLTFSceneImporter/<LoadMaterialAsync>d__43::<def>5__2
	GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 * ___U3CdefU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadMaterialAsyncU3Ed__43_t8CB980C4845985ECEBC51F97822270F1CD99C95F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CLoadMaterialAsyncU3Ed__43_t8CB980C4845985ECEBC51F97822270F1CD99C95F, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_tC7219A08BD3DD7AA457D88CB3FC2B69ABBED2A73  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_tC7219A08BD3DD7AA457D88CB3FC2B69ABBED2A73 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_tC7219A08BD3DD7AA457D88CB3FC2B69ABBED2A73  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadMaterialAsyncU3Ed__43_t8CB980C4845985ECEBC51F97822270F1CD99C95F, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_materialIndex_3() { return static_cast<int32_t>(offsetof(U3CLoadMaterialAsyncU3Ed__43_t8CB980C4845985ECEBC51F97822270F1CD99C95F, ___materialIndex_3)); }
	inline int32_t get_materialIndex_3() const { return ___materialIndex_3; }
	inline int32_t* get_address_of_materialIndex_3() { return &___materialIndex_3; }
	inline void set_materialIndex_3(int32_t value)
	{
		___materialIndex_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_4() { return static_cast<int32_t>(offsetof(U3CLoadMaterialAsyncU3Ed__43_t8CB980C4845985ECEBC51F97822270F1CD99C95F, ___U3CU3Eu__1_4)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_4() const { return ___U3CU3Eu__1_4; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_4() { return &___U3CU3Eu__1_4; }
	inline void set_U3CU3Eu__1_4(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CdefU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CLoadMaterialAsyncU3Ed__43_t8CB980C4845985ECEBC51F97822270F1CD99C95F, ___U3CdefU3E5__2_5)); }
	inline GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 * get_U3CdefU3E5__2_5() const { return ___U3CdefU3E5__2_5; }
	inline GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 ** get_address_of_U3CdefU3E5__2_5() { return &___U3CdefU3E5__2_5; }
	inline void set_U3CdefU3E5__2_5(GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 * value)
	{
		___U3CdefU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdefU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADMATERIALASYNCU3ED__43_T8CB980C4845985ECEBC51F97822270F1CD99C95F_H
#ifndef DISPATCHERBASE_TB856AF22CD8042F30BC3EA0B35779BBF104A4578_H
#define DISPATCHERBASE_TB856AF22CD8042F30BC3EA0B35779BBF104A4578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.DispatcherBase
struct  DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578  : public RuntimeObject
{
public:
	// System.Int32 UnityThreading.DispatcherBase::lockCount
	int32_t ___lockCount_0;
	// System.Object UnityThreading.DispatcherBase::taskListSyncRoot
	RuntimeObject * ___taskListSyncRoot_1;
	// System.Collections.Generic.Queue`1<UnityThreading.Task> UnityThreading.DispatcherBase::taskList
	Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 * ___taskList_2;
	// System.Collections.Generic.Queue`1<UnityThreading.Task> UnityThreading.DispatcherBase::delayedTaskList
	Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 * ___delayedTaskList_3;
	// System.Threading.ManualResetEvent UnityThreading.DispatcherBase::dataEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___dataEvent_4;
	// System.Boolean UnityThreading.DispatcherBase::AllowAccessLimitationChecks
	bool ___AllowAccessLimitationChecks_5;
	// UnityThreading.TaskSortingSystem UnityThreading.DispatcherBase::TaskSortingSystem
	int32_t ___TaskSortingSystem_6;

public:
	inline static int32_t get_offset_of_lockCount_0() { return static_cast<int32_t>(offsetof(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578, ___lockCount_0)); }
	inline int32_t get_lockCount_0() const { return ___lockCount_0; }
	inline int32_t* get_address_of_lockCount_0() { return &___lockCount_0; }
	inline void set_lockCount_0(int32_t value)
	{
		___lockCount_0 = value;
	}

	inline static int32_t get_offset_of_taskListSyncRoot_1() { return static_cast<int32_t>(offsetof(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578, ___taskListSyncRoot_1)); }
	inline RuntimeObject * get_taskListSyncRoot_1() const { return ___taskListSyncRoot_1; }
	inline RuntimeObject ** get_address_of_taskListSyncRoot_1() { return &___taskListSyncRoot_1; }
	inline void set_taskListSyncRoot_1(RuntimeObject * value)
	{
		___taskListSyncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&___taskListSyncRoot_1), value);
	}

	inline static int32_t get_offset_of_taskList_2() { return static_cast<int32_t>(offsetof(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578, ___taskList_2)); }
	inline Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 * get_taskList_2() const { return ___taskList_2; }
	inline Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 ** get_address_of_taskList_2() { return &___taskList_2; }
	inline void set_taskList_2(Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 * value)
	{
		___taskList_2 = value;
		Il2CppCodeGenWriteBarrier((&___taskList_2), value);
	}

	inline static int32_t get_offset_of_delayedTaskList_3() { return static_cast<int32_t>(offsetof(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578, ___delayedTaskList_3)); }
	inline Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 * get_delayedTaskList_3() const { return ___delayedTaskList_3; }
	inline Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 ** get_address_of_delayedTaskList_3() { return &___delayedTaskList_3; }
	inline void set_delayedTaskList_3(Queue_1_tEC56579DC33094E531139DD47CE9D21327E28512 * value)
	{
		___delayedTaskList_3 = value;
		Il2CppCodeGenWriteBarrier((&___delayedTaskList_3), value);
	}

	inline static int32_t get_offset_of_dataEvent_4() { return static_cast<int32_t>(offsetof(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578, ___dataEvent_4)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_dataEvent_4() const { return ___dataEvent_4; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_dataEvent_4() { return &___dataEvent_4; }
	inline void set_dataEvent_4(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___dataEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___dataEvent_4), value);
	}

	inline static int32_t get_offset_of_AllowAccessLimitationChecks_5() { return static_cast<int32_t>(offsetof(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578, ___AllowAccessLimitationChecks_5)); }
	inline bool get_AllowAccessLimitationChecks_5() const { return ___AllowAccessLimitationChecks_5; }
	inline bool* get_address_of_AllowAccessLimitationChecks_5() { return &___AllowAccessLimitationChecks_5; }
	inline void set_AllowAccessLimitationChecks_5(bool value)
	{
		___AllowAccessLimitationChecks_5 = value;
	}

	inline static int32_t get_offset_of_TaskSortingSystem_6() { return static_cast<int32_t>(offsetof(DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578, ___TaskSortingSystem_6)); }
	inline int32_t get_TaskSortingSystem_6() const { return ___TaskSortingSystem_6; }
	inline int32_t* get_address_of_TaskSortingSystem_6() { return &___TaskSortingSystem_6; }
	inline void set_TaskSortingSystem_6(int32_t value)
	{
		___TaskSortingSystem_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHERBASE_TB856AF22CD8042F30BC3EA0B35779BBF104A4578_H
#ifndef THREADBASE_T1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C_H
#define THREADBASE_T1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.ThreadBase
struct  ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C  : public RuntimeObject
{
public:
	// UnityThreading.Dispatcher UnityThreading.ThreadBase::targetDispatcher
	Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 * ___targetDispatcher_0;
	// System.Threading.Thread UnityThreading.ThreadBase::thread
	Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * ___thread_1;
	// System.Threading.ManualResetEvent UnityThreading.ThreadBase::exitEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___exitEvent_2;
	// System.String UnityThreading.ThreadBase::threadName
	String_t* ___threadName_4;
	// System.Threading.ThreadPriority UnityThreading.ThreadBase::priority
	int32_t ___priority_5;

public:
	inline static int32_t get_offset_of_targetDispatcher_0() { return static_cast<int32_t>(offsetof(ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C, ___targetDispatcher_0)); }
	inline Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 * get_targetDispatcher_0() const { return ___targetDispatcher_0; }
	inline Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 ** get_address_of_targetDispatcher_0() { return &___targetDispatcher_0; }
	inline void set_targetDispatcher_0(Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 * value)
	{
		___targetDispatcher_0 = value;
		Il2CppCodeGenWriteBarrier((&___targetDispatcher_0), value);
	}

	inline static int32_t get_offset_of_thread_1() { return static_cast<int32_t>(offsetof(ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C, ___thread_1)); }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * get_thread_1() const { return ___thread_1; }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 ** get_address_of_thread_1() { return &___thread_1; }
	inline void set_thread_1(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * value)
	{
		___thread_1 = value;
		Il2CppCodeGenWriteBarrier((&___thread_1), value);
	}

	inline static int32_t get_offset_of_exitEvent_2() { return static_cast<int32_t>(offsetof(ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C, ___exitEvent_2)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_exitEvent_2() const { return ___exitEvent_2; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_exitEvent_2() { return &___exitEvent_2; }
	inline void set_exitEvent_2(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___exitEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___exitEvent_2), value);
	}

	inline static int32_t get_offset_of_threadName_4() { return static_cast<int32_t>(offsetof(ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C, ___threadName_4)); }
	inline String_t* get_threadName_4() const { return ___threadName_4; }
	inline String_t** get_address_of_threadName_4() { return &___threadName_4; }
	inline void set_threadName_4(String_t* value)
	{
		___threadName_4 = value;
		Il2CppCodeGenWriteBarrier((&___threadName_4), value);
	}

	inline static int32_t get_offset_of_priority_5() { return static_cast<int32_t>(offsetof(ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C, ___priority_5)); }
	inline int32_t get_priority_5() const { return ___priority_5; }
	inline int32_t* get_address_of_priority_5() { return &___priority_5; }
	inline void set_priority_5(int32_t value)
	{
		___priority_5 = value;
	}
};

struct ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C_ThreadStaticFields
{
public:
	// UnityThreading.ThreadBase UnityThreading.ThreadBase::currentThread
	ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C * ___currentThread_3;

public:
	inline static int32_t get_offset_of_currentThread_3() { return static_cast<int32_t>(offsetof(ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C_ThreadStaticFields, ___currentThread_3)); }
	inline ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C * get_currentThread_3() const { return ___currentThread_3; }
	inline ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C ** get_address_of_currentThread_3() { return &___currentThread_3; }
	inline void set_currentThread_3(ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C * value)
	{
		___currentThread_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentThread_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADBASE_T1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef U3CLOADU3ED__20_TD8E0DC5A8DE18F109372251361C3435EE537DE43_H
#define U3CLOADU3ED__20_TD8E0DC5A8DE18F109372251361C3435EE537DE43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFComponent/<Load>d__20
struct  U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43 
{
public:
	// System.Int32 UnityGLTF.GLTFComponent/<Load>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFComponent/<Load>d__20::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFComponent UnityGLTF.GLTFComponent/<Load>d__20::<>4__this
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 * ___U3CU3E4__this_2;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFComponent/<Load>d__20::<sceneImporter>5__2
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CsceneImporterU3E5__2_3;
	// UnityGLTF.Loader.ILoader UnityGLTF.GLTFComponent/<Load>d__20::<loader>5__3
	RuntimeObject* ___U3CloaderU3E5__3_4;
	// System.Runtime.CompilerServices.TaskAwaiter`1<UnityEngine.Material> UnityGLTF.GLTFComponent/<Load>d__20::<>u__1
	TaskAwaiter_1_t9DC47086C7D78F5DE15200A7B9DCB4571F58E733  ___U3CU3Eu__1_5;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFComponent/<Load>d__20::<>u__2
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43, ___U3CU3E4__this_2)); }
	inline GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CsceneImporterU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43, ___U3CsceneImporterU3E5__2_3)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CsceneImporterU3E5__2_3() const { return ___U3CsceneImporterU3E5__2_3; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CsceneImporterU3E5__2_3() { return &___U3CsceneImporterU3E5__2_3; }
	inline void set_U3CsceneImporterU3E5__2_3(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CsceneImporterU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsceneImporterU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CloaderU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43, ___U3CloaderU3E5__3_4)); }
	inline RuntimeObject* get_U3CloaderU3E5__3_4() const { return ___U3CloaderU3E5__3_4; }
	inline RuntimeObject** get_address_of_U3CloaderU3E5__3_4() { return &___U3CloaderU3E5__3_4; }
	inline void set_U3CloaderU3E5__3_4(RuntimeObject* value)
	{
		___U3CloaderU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloaderU3E5__3_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_1_t9DC47086C7D78F5DE15200A7B9DCB4571F58E733  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_1_t9DC47086C7D78F5DE15200A7B9DCB4571F58E733 * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_1_t9DC47086C7D78F5DE15200A7B9DCB4571F58E733  value)
	{
		___U3CU3Eu__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__2_6() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43, ___U3CU3Eu__2_6)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__2_6() const { return ___U3CU3Eu__2_6; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__2_6() { return &___U3CU3Eu__2_6; }
	inline void set_U3CU3Eu__2_6(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__2_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3ED__20_TD8E0DC5A8DE18F109372251361C3435EE537DE43_H
#ifndef RETRIEVETEXTUREPATHDELEGATE_TFFE02629D2BCA6A3D7674D8797915C30126F3A6B_H
#define RETRIEVETEXTUREPATHDELEGATE_TFFE02629D2BCA6A3D7674D8797915C30126F3A6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneExporter/RetrieveTexturePathDelegate
struct  RetrieveTexturePathDelegate_tFFE02629D2BCA6A3D7674D8797915C30126F3A6B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETRIEVETEXTUREPATHDELEGATE_TFFE02629D2BCA6A3D7674D8797915C30126F3A6B_H
#ifndef U3CCONSTRUCTBUFFERU3ED__54_T4F5030F1E43E234598709AD4840BEEE7A9B1509C_H
#define U3CCONSTRUCTBUFFERU3ED__54_T4F5030F1E43E234598709AD4840BEEE7A9B1509C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<ConstructBuffer>d__54
struct  U3CConstructBufferU3Ed__54_t4F5030F1E43E234598709AD4840BEEE7A9B1509C 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructBuffer>d__54::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<ConstructBuffer>d__54::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// GLTF.Schema.GLTFBuffer UnityGLTF.GLTFSceneImporter/<ConstructBuffer>d__54::buffer
	GLTFBuffer_t093686A8CEC027B3EFCCCC89EF874F3BE6738CA1 * ___buffer_2;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<ConstructBuffer>d__54::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_3;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructBuffer>d__54::bufferIndex
	int32_t ___bufferIndex_4;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<ConstructBuffer>d__54::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CConstructBufferU3Ed__54_t4F5030F1E43E234598709AD4840BEEE7A9B1509C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CConstructBufferU3Ed__54_t4F5030F1E43E234598709AD4840BEEE7A9B1509C, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_buffer_2() { return static_cast<int32_t>(offsetof(U3CConstructBufferU3Ed__54_t4F5030F1E43E234598709AD4840BEEE7A9B1509C, ___buffer_2)); }
	inline GLTFBuffer_t093686A8CEC027B3EFCCCC89EF874F3BE6738CA1 * get_buffer_2() const { return ___buffer_2; }
	inline GLTFBuffer_t093686A8CEC027B3EFCCCC89EF874F3BE6738CA1 ** get_address_of_buffer_2() { return &___buffer_2; }
	inline void set_buffer_2(GLTFBuffer_t093686A8CEC027B3EFCCCC89EF874F3BE6738CA1 * value)
	{
		___buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CConstructBufferU3Ed__54_t4F5030F1E43E234598709AD4840BEEE7A9B1509C, ___U3CU3E4__this_3)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_bufferIndex_4() { return static_cast<int32_t>(offsetof(U3CConstructBufferU3Ed__54_t4F5030F1E43E234598709AD4840BEEE7A9B1509C, ___bufferIndex_4)); }
	inline int32_t get_bufferIndex_4() const { return ___bufferIndex_4; }
	inline int32_t* get_address_of_bufferIndex_4() { return &___bufferIndex_4; }
	inline void set_bufferIndex_4(int32_t value)
	{
		___bufferIndex_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CConstructBufferU3Ed__54_t4F5030F1E43E234598709AD4840BEEE7A9B1509C, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONSTRUCTBUFFERU3ED__54_T4F5030F1E43E234598709AD4840BEEE7A9B1509C_H
#ifndef U3CCONSTRUCTBUFFERDATAU3ED__45_T9F1660E7623137ED5EB0EFBD40AE0A3B6241870E_H
#define U3CCONSTRUCTBUFFERDATAU3ED__45_T9F1660E7623137ED5EB0EFBD40AE0A3B6241870E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<ConstructBufferData>d__45
struct  U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructBufferData>d__45::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<ConstructBufferData>d__45::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// GLTF.Schema.Node UnityGLTF.GLTFSceneImporter/<ConstructBufferData>d__45::node
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744 * ___node_2;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<ConstructBufferData>d__45::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_3;
	// GLTF.Schema.MSFT_LODExtension UnityGLTF.GLTFSceneImporter/<ConstructBufferData>d__45::<lodsextension>5__2
	MSFT_LODExtension_tA2000797F265C91522ED3917FEB2078117995805 * ___U3ClodsextensionU3E5__2_4;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<ConstructBufferData>d__45::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_5;
	// System.Collections.Generic.List`1/Enumerator<GLTF.Schema.NodeId> UnityGLTF.GLTFSceneImporter/<ConstructBufferData>d__45::<>7__wrap2
	Enumerator_tEF9CAAB01B45A327284703F74A8B453B79473A75  ___U3CU3E7__wrap2_6;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructBufferData>d__45::<i>5__4
	int32_t ___U3CiU3E5__4_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_node_2() { return static_cast<int32_t>(offsetof(U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E, ___node_2)); }
	inline Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744 * get_node_2() const { return ___node_2; }
	inline Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744 ** get_address_of_node_2() { return &___node_2; }
	inline void set_node_2(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744 * value)
	{
		___node_2 = value;
		Il2CppCodeGenWriteBarrier((&___node_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E, ___U3CU3E4__this_3)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3ClodsextensionU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E, ___U3ClodsextensionU3E5__2_4)); }
	inline MSFT_LODExtension_tA2000797F265C91522ED3917FEB2078117995805 * get_U3ClodsextensionU3E5__2_4() const { return ___U3ClodsextensionU3E5__2_4; }
	inline MSFT_LODExtension_tA2000797F265C91522ED3917FEB2078117995805 ** get_address_of_U3ClodsextensionU3E5__2_4() { return &___U3ClodsextensionU3E5__2_4; }
	inline void set_U3ClodsextensionU3E5__2_4(MSFT_LODExtension_tA2000797F265C91522ED3917FEB2078117995805 * value)
	{
		___U3ClodsextensionU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClodsextensionU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_6() { return static_cast<int32_t>(offsetof(U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E, ___U3CU3E7__wrap2_6)); }
	inline Enumerator_tEF9CAAB01B45A327284703F74A8B453B79473A75  get_U3CU3E7__wrap2_6() const { return ___U3CU3E7__wrap2_6; }
	inline Enumerator_tEF9CAAB01B45A327284703F74A8B453B79473A75 * get_address_of_U3CU3E7__wrap2_6() { return &___U3CU3E7__wrap2_6; }
	inline void set_U3CU3E7__wrap2_6(Enumerator_tEF9CAAB01B45A327284703F74A8B453B79473A75  value)
	{
		___U3CU3E7__wrap2_6 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E, ___U3CiU3E5__4_7)); }
	inline int32_t get_U3CiU3E5__4_7() const { return ___U3CiU3E5__4_7; }
	inline int32_t* get_address_of_U3CiU3E5__4_7() { return &___U3CiU3E5__4_7; }
	inline void set_U3CiU3E5__4_7(int32_t value)
	{
		___U3CiU3E5__4_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONSTRUCTBUFFERDATAU3ED__45_T9F1660E7623137ED5EB0EFBD40AE0A3B6241870E_H
#ifndef U3CCONSTRUCTIMAGEU3ED__55_T535ABFE1389C26A16257F498B5A134047A6157C6_H
#define U3CCONSTRUCTIMAGEU3ED__55_T535ABFE1389C26A16257F498B5A134047A6157C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<ConstructImage>d__55
struct  U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructImage>d__55::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<ConstructImage>d__55::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<ConstructImage>d__55::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructImage>d__55::imageCacheIndex
	int32_t ___imageCacheIndex_3;
	// GLTF.Schema.GLTFImage UnityGLTF.GLTFSceneImporter/<ConstructImage>d__55::image
	GLTFImage_tA3F1517270CE1DC26739C076F757BFA7B651A4EE * ___image_4;
	// System.Boolean UnityGLTF.GLTFSceneImporter/<ConstructImage>d__55::markGpuOnly
	bool ___markGpuOnly_5;
	// System.Boolean UnityGLTF.GLTFSceneImporter/<ConstructImage>d__55::linear
	bool ___linear_6;
	// System.IO.Stream UnityGLTF.GLTFSceneImporter/<ConstructImage>d__55::<stream>5__2
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___U3CstreamU3E5__2_7;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<ConstructImage>d__55::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_imageCacheIndex_3() { return static_cast<int32_t>(offsetof(U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6, ___imageCacheIndex_3)); }
	inline int32_t get_imageCacheIndex_3() const { return ___imageCacheIndex_3; }
	inline int32_t* get_address_of_imageCacheIndex_3() { return &___imageCacheIndex_3; }
	inline void set_imageCacheIndex_3(int32_t value)
	{
		___imageCacheIndex_3 = value;
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6, ___image_4)); }
	inline GLTFImage_tA3F1517270CE1DC26739C076F757BFA7B651A4EE * get_image_4() const { return ___image_4; }
	inline GLTFImage_tA3F1517270CE1DC26739C076F757BFA7B651A4EE ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(GLTFImage_tA3F1517270CE1DC26739C076F757BFA7B651A4EE * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier((&___image_4), value);
	}

	inline static int32_t get_offset_of_markGpuOnly_5() { return static_cast<int32_t>(offsetof(U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6, ___markGpuOnly_5)); }
	inline bool get_markGpuOnly_5() const { return ___markGpuOnly_5; }
	inline bool* get_address_of_markGpuOnly_5() { return &___markGpuOnly_5; }
	inline void set_markGpuOnly_5(bool value)
	{
		___markGpuOnly_5 = value;
	}

	inline static int32_t get_offset_of_linear_6() { return static_cast<int32_t>(offsetof(U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6, ___linear_6)); }
	inline bool get_linear_6() const { return ___linear_6; }
	inline bool* get_address_of_linear_6() { return &___linear_6; }
	inline void set_linear_6(bool value)
	{
		___linear_6 = value;
	}

	inline static int32_t get_offset_of_U3CstreamU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6, ___U3CstreamU3E5__2_7)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_U3CstreamU3E5__2_7() const { return ___U3CstreamU3E5__2_7; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_U3CstreamU3E5__2_7() { return &___U3CstreamU3E5__2_7; }
	inline void set_U3CstreamU3E5__2_7(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___U3CstreamU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstreamU3E5__2_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_8() { return static_cast<int32_t>(offsetof(U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6, ___U3CU3Eu__1_8)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_8() const { return ___U3CU3Eu__1_8; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_8() { return &___U3CU3Eu__1_8; }
	inline void set_U3CU3Eu__1_8(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONSTRUCTIMAGEU3ED__55_T535ABFE1389C26A16257F498B5A134047A6157C6_H
#ifndef U3CCONSTRUCTIMAGEBUFFERU3ED__47_T24501153C7B53D1792EDEDBF6789C1D4044AF860_H
#define U3CCONSTRUCTIMAGEBUFFERU3ED__47_T24501153C7B53D1792EDEDBF6789C1D4044AF860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<ConstructImageBuffer>d__47
struct  U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructImageBuffer>d__47::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<ConstructImageBuffer>d__47::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<ConstructImageBuffer>d__47::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// GLTF.Schema.GLTFTexture UnityGLTF.GLTFSceneImporter/<ConstructImageBuffer>d__47::texture
	GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 * ___texture_3;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructImageBuffer>d__47::textureIndex
	int32_t ___textureIndex_4;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructImageBuffer>d__47::<sourceId>5__2
	int32_t ___U3CsourceIdU3E5__2_5;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<ConstructImageBuffer>d__47::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_texture_3() { return static_cast<int32_t>(offsetof(U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860, ___texture_3)); }
	inline GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 * get_texture_3() const { return ___texture_3; }
	inline GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 ** get_address_of_texture_3() { return &___texture_3; }
	inline void set_texture_3(GLTFTexture_t2D12B18E729EBA0BBA4A5B51A991297BDFF7CD65 * value)
	{
		___texture_3 = value;
		Il2CppCodeGenWriteBarrier((&___texture_3), value);
	}

	inline static int32_t get_offset_of_textureIndex_4() { return static_cast<int32_t>(offsetof(U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860, ___textureIndex_4)); }
	inline int32_t get_textureIndex_4() const { return ___textureIndex_4; }
	inline int32_t* get_address_of_textureIndex_4() { return &___textureIndex_4; }
	inline void set_textureIndex_4(int32_t value)
	{
		___textureIndex_4 = value;
	}

	inline static int32_t get_offset_of_U3CsourceIdU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860, ___U3CsourceIdU3E5__2_5)); }
	inline int32_t get_U3CsourceIdU3E5__2_5() const { return ___U3CsourceIdU3E5__2_5; }
	inline int32_t* get_address_of_U3CsourceIdU3E5__2_5() { return &___U3CsourceIdU3E5__2_5; }
	inline void set_U3CsourceIdU3E5__2_5(int32_t value)
	{
		___U3CsourceIdU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_6() { return static_cast<int32_t>(offsetof(U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860, ___U3CU3Eu__1_6)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_6() const { return ___U3CU3Eu__1_6; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_6() { return &___U3CU3Eu__1_6; }
	inline void set_U3CU3Eu__1_6(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONSTRUCTIMAGEBUFFERU3ED__47_T24501153C7B53D1792EDEDBF6789C1D4044AF860_H
#ifndef U3CCONSTRUCTMATERIALU3ED__78_TD1EAE9F22085AC88D49279FA14D6900EDD524357_H
#define U3CCONSTRUCTMATERIALU3ED__78_TD1EAE9F22085AC88D49279FA14D6900EDD524357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<ConstructMaterial>d__78
struct  U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMaterial>d__78::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<ConstructMaterial>d__78::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<ConstructMaterial>d__78::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// GLTF.Schema.GLTFMaterial UnityGLTF.GLTFSceneImporter/<ConstructMaterial>d__78::def
	GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 * ___def_3;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMaterial>d__78::materialIndex
	int32_t ___materialIndex_4;
	// UnityGLTF.IUniformMap UnityGLTF.GLTFSceneImporter/<ConstructMaterial>d__78::<mapper>5__2
	RuntimeObject* ___U3CmapperU3E5__2_5;
	// UnityGLTF.IMetalRoughUniformMap UnityGLTF.GLTFSceneImporter/<ConstructMaterial>d__78::<mrMapper>5__3
	RuntimeObject* ___U3CmrMapperU3E5__3_6;
	// UnityGLTF.ISpecGlossUniformMap UnityGLTF.GLTFSceneImporter/<ConstructMaterial>d__78::<sgMapper>5__4
	RuntimeObject* ___U3CsgMapperU3E5__4_7;
	// GLTF.Schema.PbrMetallicRoughness UnityGLTF.GLTFSceneImporter/<ConstructMaterial>d__78::<pbr>5__5
	PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2 * ___U3CpbrU3E5__5_8;
	// GLTF.Schema.TextureId UnityGLTF.GLTFSceneImporter/<ConstructMaterial>d__78::<textureId>5__6
	TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91 * ___U3CtextureIdU3E5__6_9;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<ConstructMaterial>d__78::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_10;
	// GLTF.Schema.KHR_materials_pbrSpecularGlossinessExtension UnityGLTF.GLTFSceneImporter/<ConstructMaterial>d__78::<specGloss>5__7
	KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0 * ___U3CspecGlossU3E5__7_11;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_def_3() { return static_cast<int32_t>(offsetof(U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357, ___def_3)); }
	inline GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 * get_def_3() const { return ___def_3; }
	inline GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 ** get_address_of_def_3() { return &___def_3; }
	inline void set_def_3(GLTFMaterial_t3823C70B2B7CB4235F78C94163E7FA538F482C99 * value)
	{
		___def_3 = value;
		Il2CppCodeGenWriteBarrier((&___def_3), value);
	}

	inline static int32_t get_offset_of_materialIndex_4() { return static_cast<int32_t>(offsetof(U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357, ___materialIndex_4)); }
	inline int32_t get_materialIndex_4() const { return ___materialIndex_4; }
	inline int32_t* get_address_of_materialIndex_4() { return &___materialIndex_4; }
	inline void set_materialIndex_4(int32_t value)
	{
		___materialIndex_4 = value;
	}

	inline static int32_t get_offset_of_U3CmapperU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357, ___U3CmapperU3E5__2_5)); }
	inline RuntimeObject* get_U3CmapperU3E5__2_5() const { return ___U3CmapperU3E5__2_5; }
	inline RuntimeObject** get_address_of_U3CmapperU3E5__2_5() { return &___U3CmapperU3E5__2_5; }
	inline void set_U3CmapperU3E5__2_5(RuntimeObject* value)
	{
		___U3CmapperU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmapperU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3CmrMapperU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357, ___U3CmrMapperU3E5__3_6)); }
	inline RuntimeObject* get_U3CmrMapperU3E5__3_6() const { return ___U3CmrMapperU3E5__3_6; }
	inline RuntimeObject** get_address_of_U3CmrMapperU3E5__3_6() { return &___U3CmrMapperU3E5__3_6; }
	inline void set_U3CmrMapperU3E5__3_6(RuntimeObject* value)
	{
		___U3CmrMapperU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmrMapperU3E5__3_6), value);
	}

	inline static int32_t get_offset_of_U3CsgMapperU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357, ___U3CsgMapperU3E5__4_7)); }
	inline RuntimeObject* get_U3CsgMapperU3E5__4_7() const { return ___U3CsgMapperU3E5__4_7; }
	inline RuntimeObject** get_address_of_U3CsgMapperU3E5__4_7() { return &___U3CsgMapperU3E5__4_7; }
	inline void set_U3CsgMapperU3E5__4_7(RuntimeObject* value)
	{
		___U3CsgMapperU3E5__4_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsgMapperU3E5__4_7), value);
	}

	inline static int32_t get_offset_of_U3CpbrU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357, ___U3CpbrU3E5__5_8)); }
	inline PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2 * get_U3CpbrU3E5__5_8() const { return ___U3CpbrU3E5__5_8; }
	inline PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2 ** get_address_of_U3CpbrU3E5__5_8() { return &___U3CpbrU3E5__5_8; }
	inline void set_U3CpbrU3E5__5_8(PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2 * value)
	{
		___U3CpbrU3E5__5_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpbrU3E5__5_8), value);
	}

	inline static int32_t get_offset_of_U3CtextureIdU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357, ___U3CtextureIdU3E5__6_9)); }
	inline TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91 * get_U3CtextureIdU3E5__6_9() const { return ___U3CtextureIdU3E5__6_9; }
	inline TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91 ** get_address_of_U3CtextureIdU3E5__6_9() { return &___U3CtextureIdU3E5__6_9; }
	inline void set_U3CtextureIdU3E5__6_9(TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91 * value)
	{
		___U3CtextureIdU3E5__6_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextureIdU3E5__6_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_10() { return static_cast<int32_t>(offsetof(U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357, ___U3CU3Eu__1_10)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_10() const { return ___U3CU3Eu__1_10; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_10() { return &___U3CU3Eu__1_10; }
	inline void set_U3CU3Eu__1_10(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_10 = value;
	}

	inline static int32_t get_offset_of_U3CspecGlossU3E5__7_11() { return static_cast<int32_t>(offsetof(U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357, ___U3CspecGlossU3E5__7_11)); }
	inline KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0 * get_U3CspecGlossU3E5__7_11() const { return ___U3CspecGlossU3E5__7_11; }
	inline KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0 ** get_address_of_U3CspecGlossU3E5__7_11() { return &___U3CspecGlossU3E5__7_11; }
	inline void set_U3CspecGlossU3E5__7_11(KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0 * value)
	{
		___U3CspecGlossU3E5__7_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CspecGlossU3E5__7_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONSTRUCTMATERIALU3ED__78_TD1EAE9F22085AC88D49279FA14D6900EDD524357_H
#ifndef U3CCONSTRUCTMESHU3ED__72_T7C23442B158C254E2BC7939894FB15EFCF8B3A42_H
#define U3CCONSTRUCTMESHU3ED__72_T7C23442B158C254E2BC7939894FB15EFCF8B3A42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<ConstructMesh>d__72
struct  U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMesh>d__72::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<ConstructMesh>d__72::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<ConstructMesh>d__72::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMesh>d__72::meshId
	int32_t ___meshId_3;
	// GLTF.Schema.GLTFMesh UnityGLTF.GLTFSceneImporter/<ConstructMesh>d__72::mesh
	GLTFMesh_tD3F424D9F92A52F43406967EA5B985F2735DD1AD * ___mesh_4;
	// GLTF.Schema.Skin UnityGLTF.GLTFSceneImporter/<ConstructMesh>d__72::skin
	Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34 * ___skin_5;
	// UnityEngine.Transform UnityGLTF.GLTFSceneImporter/<ConstructMesh>d__72::parent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___parent_6;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMesh>d__72::<i>5__2
	int32_t ___U3CiU3E5__2_7;
	// GLTF.Schema.MeshPrimitive UnityGLTF.GLTFSceneImporter/<ConstructMesh>d__72::<primitive>5__3
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * ___U3CprimitiveU3E5__3_8;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMesh>d__72::<materialIndex>5__4
	int32_t ___U3CmaterialIndexU3E5__4_9;
	// UnityEngine.GameObject UnityGLTF.GLTFSceneImporter/<ConstructMesh>d__72::<primitiveObj>5__5
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CprimitiveObjU3E5__5_10;
	// UnityEngine.Mesh UnityGLTF.GLTFSceneImporter/<ConstructMesh>d__72::<curMesh>5__6
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___U3CcurMeshU3E5__6_11;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<ConstructMesh>d__72::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_12;
	// UnityEngine.SkinnedMeshRenderer UnityGLTF.GLTFSceneImporter/<ConstructMesh>d__72::<skinnedMeshRenderer>5__7
	SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * ___U3CskinnedMeshRendererU3E5__7_13;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_meshId_3() { return static_cast<int32_t>(offsetof(U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42, ___meshId_3)); }
	inline int32_t get_meshId_3() const { return ___meshId_3; }
	inline int32_t* get_address_of_meshId_3() { return &___meshId_3; }
	inline void set_meshId_3(int32_t value)
	{
		___meshId_3 = value;
	}

	inline static int32_t get_offset_of_mesh_4() { return static_cast<int32_t>(offsetof(U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42, ___mesh_4)); }
	inline GLTFMesh_tD3F424D9F92A52F43406967EA5B985F2735DD1AD * get_mesh_4() const { return ___mesh_4; }
	inline GLTFMesh_tD3F424D9F92A52F43406967EA5B985F2735DD1AD ** get_address_of_mesh_4() { return &___mesh_4; }
	inline void set_mesh_4(GLTFMesh_tD3F424D9F92A52F43406967EA5B985F2735DD1AD * value)
	{
		___mesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_4), value);
	}

	inline static int32_t get_offset_of_skin_5() { return static_cast<int32_t>(offsetof(U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42, ___skin_5)); }
	inline Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34 * get_skin_5() const { return ___skin_5; }
	inline Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34 ** get_address_of_skin_5() { return &___skin_5; }
	inline void set_skin_5(Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34 * value)
	{
		___skin_5 = value;
		Il2CppCodeGenWriteBarrier((&___skin_5), value);
	}

	inline static int32_t get_offset_of_parent_6() { return static_cast<int32_t>(offsetof(U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42, ___parent_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_parent_6() const { return ___parent_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_parent_6() { return &___parent_6; }
	inline void set_parent_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___parent_6 = value;
		Il2CppCodeGenWriteBarrier((&___parent_6), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42, ___U3CiU3E5__2_7)); }
	inline int32_t get_U3CiU3E5__2_7() const { return ___U3CiU3E5__2_7; }
	inline int32_t* get_address_of_U3CiU3E5__2_7() { return &___U3CiU3E5__2_7; }
	inline void set_U3CiU3E5__2_7(int32_t value)
	{
		___U3CiU3E5__2_7 = value;
	}

	inline static int32_t get_offset_of_U3CprimitiveU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42, ___U3CprimitiveU3E5__3_8)); }
	inline MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * get_U3CprimitiveU3E5__3_8() const { return ___U3CprimitiveU3E5__3_8; }
	inline MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 ** get_address_of_U3CprimitiveU3E5__3_8() { return &___U3CprimitiveU3E5__3_8; }
	inline void set_U3CprimitiveU3E5__3_8(MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * value)
	{
		___U3CprimitiveU3E5__3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprimitiveU3E5__3_8), value);
	}

	inline static int32_t get_offset_of_U3CmaterialIndexU3E5__4_9() { return static_cast<int32_t>(offsetof(U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42, ___U3CmaterialIndexU3E5__4_9)); }
	inline int32_t get_U3CmaterialIndexU3E5__4_9() const { return ___U3CmaterialIndexU3E5__4_9; }
	inline int32_t* get_address_of_U3CmaterialIndexU3E5__4_9() { return &___U3CmaterialIndexU3E5__4_9; }
	inline void set_U3CmaterialIndexU3E5__4_9(int32_t value)
	{
		___U3CmaterialIndexU3E5__4_9 = value;
	}

	inline static int32_t get_offset_of_U3CprimitiveObjU3E5__5_10() { return static_cast<int32_t>(offsetof(U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42, ___U3CprimitiveObjU3E5__5_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CprimitiveObjU3E5__5_10() const { return ___U3CprimitiveObjU3E5__5_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CprimitiveObjU3E5__5_10() { return &___U3CprimitiveObjU3E5__5_10; }
	inline void set_U3CprimitiveObjU3E5__5_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CprimitiveObjU3E5__5_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprimitiveObjU3E5__5_10), value);
	}

	inline static int32_t get_offset_of_U3CcurMeshU3E5__6_11() { return static_cast<int32_t>(offsetof(U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42, ___U3CcurMeshU3E5__6_11)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_U3CcurMeshU3E5__6_11() const { return ___U3CcurMeshU3E5__6_11; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_U3CcurMeshU3E5__6_11() { return &___U3CcurMeshU3E5__6_11; }
	inline void set_U3CcurMeshU3E5__6_11(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___U3CcurMeshU3E5__6_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurMeshU3E5__6_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_12() { return static_cast<int32_t>(offsetof(U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42, ___U3CU3Eu__1_12)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_12() const { return ___U3CU3Eu__1_12; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_12() { return &___U3CU3Eu__1_12; }
	inline void set_U3CU3Eu__1_12(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_12 = value;
	}

	inline static int32_t get_offset_of_U3CskinnedMeshRendererU3E5__7_13() { return static_cast<int32_t>(offsetof(U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42, ___U3CskinnedMeshRendererU3E5__7_13)); }
	inline SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * get_U3CskinnedMeshRendererU3E5__7_13() const { return ___U3CskinnedMeshRendererU3E5__7_13; }
	inline SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 ** get_address_of_U3CskinnedMeshRendererU3E5__7_13() { return &___U3CskinnedMeshRendererU3E5__7_13; }
	inline void set_U3CskinnedMeshRendererU3E5__7_13(SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * value)
	{
		___U3CskinnedMeshRendererU3E5__7_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CskinnedMeshRendererU3E5__7_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONSTRUCTMESHU3ED__72_T7C23442B158C254E2BC7939894FB15EFCF8B3A42_H
#ifndef U3CCONSTRUCTMESHATTRIBUTESU3ED__46_TFA596F06EAC89B47AF44E40B8944068EB387E949_H
#define U3CCONSTRUCTMESHATTRIBUTESU3ED__46_TFA596F06EAC89B47AF44E40B8944068EB387E949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__46
struct  U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__46::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__46::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// GLTF.Schema.MeshId UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__46::meshId
	MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231 * ___meshId_2;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__46::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_3;
	// GLTF.Schema.GLTFMesh UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__46::mesh
	GLTFMesh_tD3F424D9F92A52F43406967EA5B985F2735DD1AD * ___mesh_4;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__46::<meshIdIndex>5__2
	int32_t ___U3CmeshIdIndexU3E5__2_5;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__46::<i>5__3
	int32_t ___U3CiU3E5__3_6;
	// GLTF.Schema.MeshPrimitive UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__46::<primitive>5__4
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * ___U3CprimitiveU3E5__4_7;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__46::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_meshId_2() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949, ___meshId_2)); }
	inline MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231 * get_meshId_2() const { return ___meshId_2; }
	inline MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231 ** get_address_of_meshId_2() { return &___meshId_2; }
	inline void set_meshId_2(MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231 * value)
	{
		___meshId_2 = value;
		Il2CppCodeGenWriteBarrier((&___meshId_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949, ___U3CU3E4__this_3)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_mesh_4() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949, ___mesh_4)); }
	inline GLTFMesh_tD3F424D9F92A52F43406967EA5B985F2735DD1AD * get_mesh_4() const { return ___mesh_4; }
	inline GLTFMesh_tD3F424D9F92A52F43406967EA5B985F2735DD1AD ** get_address_of_mesh_4() { return &___mesh_4; }
	inline void set_mesh_4(GLTFMesh_tD3F424D9F92A52F43406967EA5B985F2735DD1AD * value)
	{
		___mesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_4), value);
	}

	inline static int32_t get_offset_of_U3CmeshIdIndexU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949, ___U3CmeshIdIndexU3E5__2_5)); }
	inline int32_t get_U3CmeshIdIndexU3E5__2_5() const { return ___U3CmeshIdIndexU3E5__2_5; }
	inline int32_t* get_address_of_U3CmeshIdIndexU3E5__2_5() { return &___U3CmeshIdIndexU3E5__2_5; }
	inline void set_U3CmeshIdIndexU3E5__2_5(int32_t value)
	{
		___U3CmeshIdIndexU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949, ___U3CiU3E5__3_6)); }
	inline int32_t get_U3CiU3E5__3_6() const { return ___U3CiU3E5__3_6; }
	inline int32_t* get_address_of_U3CiU3E5__3_6() { return &___U3CiU3E5__3_6; }
	inline void set_U3CiU3E5__3_6(int32_t value)
	{
		___U3CiU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CprimitiveU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949, ___U3CprimitiveU3E5__4_7)); }
	inline MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * get_U3CprimitiveU3E5__4_7() const { return ___U3CprimitiveU3E5__4_7; }
	inline MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 ** get_address_of_U3CprimitiveU3E5__4_7() { return &___U3CprimitiveU3E5__4_7; }
	inline void set_U3CprimitiveU3E5__4_7(MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * value)
	{
		___U3CprimitiveU3E5__4_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprimitiveU3E5__4_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_8() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949, ___U3CU3Eu__1_8)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_8() const { return ___U3CU3Eu__1_8; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_8() { return &___U3CU3Eu__1_8; }
	inline void set_U3CU3Eu__1_8(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONSTRUCTMESHATTRIBUTESU3ED__46_TFA596F06EAC89B47AF44E40B8944068EB387E949_H
#ifndef U3CCONSTRUCTMESHATTRIBUTESU3ED__57_T92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA_H
#define U3CCONSTRUCTMESHATTRIBUTESU3ED__57_T92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__57
struct  U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__57::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__57::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__57::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__57::meshID
	int32_t ___meshID_3;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__57::primitiveIndex
	int32_t ___primitiveIndex_4;
	// GLTF.Schema.MeshPrimitive UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__57::primitive
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * ___primitive_5;
	// System.Collections.Generic.Dictionary`2<System.String,GLTF.AttributeAccessor> UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__57::<attributeAccessors>5__2
	Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C * ___U3CattributeAccessorsU3E5__2_6;
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,GLTF.Schema.AccessorId> UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__57::<>7__wrap2
	Enumerator_t77DA7CAFDC0A844FD705B49C2E730DE0B8D08B5E  ___U3CU3E7__wrap2_7;
	// System.Collections.Generic.KeyValuePair`2<System.String,GLTF.Schema.AccessorId> UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__57::<attributePair>5__4
	KeyValuePair_2_tA9A5287EAB0CB41B20FD057EA72334780CDC4A9A  ___U3CattributePairU3E5__4_8;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__57::<bufferId>5__5
	int32_t ___U3CbufferIdU3E5__5_9;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<ConstructMeshAttributes>d__57::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_meshID_3() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA, ___meshID_3)); }
	inline int32_t get_meshID_3() const { return ___meshID_3; }
	inline int32_t* get_address_of_meshID_3() { return &___meshID_3; }
	inline void set_meshID_3(int32_t value)
	{
		___meshID_3 = value;
	}

	inline static int32_t get_offset_of_primitiveIndex_4() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA, ___primitiveIndex_4)); }
	inline int32_t get_primitiveIndex_4() const { return ___primitiveIndex_4; }
	inline int32_t* get_address_of_primitiveIndex_4() { return &___primitiveIndex_4; }
	inline void set_primitiveIndex_4(int32_t value)
	{
		___primitiveIndex_4 = value;
	}

	inline static int32_t get_offset_of_primitive_5() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA, ___primitive_5)); }
	inline MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * get_primitive_5() const { return ___primitive_5; }
	inline MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 ** get_address_of_primitive_5() { return &___primitive_5; }
	inline void set_primitive_5(MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * value)
	{
		___primitive_5 = value;
		Il2CppCodeGenWriteBarrier((&___primitive_5), value);
	}

	inline static int32_t get_offset_of_U3CattributeAccessorsU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA, ___U3CattributeAccessorsU3E5__2_6)); }
	inline Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C * get_U3CattributeAccessorsU3E5__2_6() const { return ___U3CattributeAccessorsU3E5__2_6; }
	inline Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C ** get_address_of_U3CattributeAccessorsU3E5__2_6() { return &___U3CattributeAccessorsU3E5__2_6; }
	inline void set_U3CattributeAccessorsU3E5__2_6(Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C * value)
	{
		___U3CattributeAccessorsU3E5__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CattributeAccessorsU3E5__2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_7() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA, ___U3CU3E7__wrap2_7)); }
	inline Enumerator_t77DA7CAFDC0A844FD705B49C2E730DE0B8D08B5E  get_U3CU3E7__wrap2_7() const { return ___U3CU3E7__wrap2_7; }
	inline Enumerator_t77DA7CAFDC0A844FD705B49C2E730DE0B8D08B5E * get_address_of_U3CU3E7__wrap2_7() { return &___U3CU3E7__wrap2_7; }
	inline void set_U3CU3E7__wrap2_7(Enumerator_t77DA7CAFDC0A844FD705B49C2E730DE0B8D08B5E  value)
	{
		___U3CU3E7__wrap2_7 = value;
	}

	inline static int32_t get_offset_of_U3CattributePairU3E5__4_8() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA, ___U3CattributePairU3E5__4_8)); }
	inline KeyValuePair_2_tA9A5287EAB0CB41B20FD057EA72334780CDC4A9A  get_U3CattributePairU3E5__4_8() const { return ___U3CattributePairU3E5__4_8; }
	inline KeyValuePair_2_tA9A5287EAB0CB41B20FD057EA72334780CDC4A9A * get_address_of_U3CattributePairU3E5__4_8() { return &___U3CattributePairU3E5__4_8; }
	inline void set_U3CattributePairU3E5__4_8(KeyValuePair_2_tA9A5287EAB0CB41B20FD057EA72334780CDC4A9A  value)
	{
		___U3CattributePairU3E5__4_8 = value;
	}

	inline static int32_t get_offset_of_U3CbufferIdU3E5__5_9() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA, ___U3CbufferIdU3E5__5_9)); }
	inline int32_t get_U3CbufferIdU3E5__5_9() const { return ___U3CbufferIdU3E5__5_9; }
	inline int32_t* get_address_of_U3CbufferIdU3E5__5_9() { return &___U3CbufferIdU3E5__5_9; }
	inline void set_U3CbufferIdU3E5__5_9(int32_t value)
	{
		___U3CbufferIdU3E5__5_9 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_10() { return static_cast<int32_t>(offsetof(U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA, ___U3CU3Eu__1_10)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_10() const { return ___U3CU3Eu__1_10; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_10() { return &___U3CU3Eu__1_10; }
	inline void set_U3CU3Eu__1_10(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONSTRUCTMESHATTRIBUTESU3ED__57_T92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA_H
#ifndef U3CCONSTRUCTMESHPRIMITIVEU3ED__73_T1EC85122DC787A6C42488ADF833B4B7A4367EBC9_H
#define U3CCONSTRUCTMESHPRIMITIVEU3ED__73_T1EC85122DC787A6C42488ADF833B4B7A4367EBC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<ConstructMeshPrimitive>d__73
struct  U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMeshPrimitive>d__73::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<ConstructMeshPrimitive>d__73::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<ConstructMeshPrimitive>d__73::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMeshPrimitive>d__73::meshID
	int32_t ___meshID_3;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMeshPrimitive>d__73::primitiveIndex
	int32_t ___primitiveIndex_4;
	// GLTF.Schema.MeshPrimitive UnityGLTF.GLTFSceneImporter/<ConstructMeshPrimitive>d__73::primitive
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * ___primitive_5;
	// UnityGLTF.GLTFSceneImporter/<>c__DisplayClass73_0 UnityGLTF.GLTFSceneImporter/<ConstructMeshPrimitive>d__73::<>8__1
	U3CU3Ec__DisplayClass73_0_tD494223E41D1BFA7571AC3568E9A99A8B3FED886 * ___U3CU3E8__1_6;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructMeshPrimitive>d__73::materialIndex
	int32_t ___materialIndex_7;
	// System.Runtime.CompilerServices.TaskAwaiter`1<UnityGLTF.UnityMeshData> UnityGLTF.GLTFSceneImporter/<ConstructMeshPrimitive>d__73::<>u__1
	TaskAwaiter_1_tAF81E0A0509152C9EFD9A2A752D472023C3FD550  ___U3CU3Eu__1_8;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<ConstructMeshPrimitive>d__73::<>u__2
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__2_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_meshID_3() { return static_cast<int32_t>(offsetof(U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9, ___meshID_3)); }
	inline int32_t get_meshID_3() const { return ___meshID_3; }
	inline int32_t* get_address_of_meshID_3() { return &___meshID_3; }
	inline void set_meshID_3(int32_t value)
	{
		___meshID_3 = value;
	}

	inline static int32_t get_offset_of_primitiveIndex_4() { return static_cast<int32_t>(offsetof(U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9, ___primitiveIndex_4)); }
	inline int32_t get_primitiveIndex_4() const { return ___primitiveIndex_4; }
	inline int32_t* get_address_of_primitiveIndex_4() { return &___primitiveIndex_4; }
	inline void set_primitiveIndex_4(int32_t value)
	{
		___primitiveIndex_4 = value;
	}

	inline static int32_t get_offset_of_primitive_5() { return static_cast<int32_t>(offsetof(U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9, ___primitive_5)); }
	inline MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * get_primitive_5() const { return ___primitive_5; }
	inline MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 ** get_address_of_primitive_5() { return &___primitive_5; }
	inline void set_primitive_5(MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356 * value)
	{
		___primitive_5 = value;
		Il2CppCodeGenWriteBarrier((&___primitive_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_6() { return static_cast<int32_t>(offsetof(U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9, ___U3CU3E8__1_6)); }
	inline U3CU3Ec__DisplayClass73_0_tD494223E41D1BFA7571AC3568E9A99A8B3FED886 * get_U3CU3E8__1_6() const { return ___U3CU3E8__1_6; }
	inline U3CU3Ec__DisplayClass73_0_tD494223E41D1BFA7571AC3568E9A99A8B3FED886 ** get_address_of_U3CU3E8__1_6() { return &___U3CU3E8__1_6; }
	inline void set_U3CU3E8__1_6(U3CU3Ec__DisplayClass73_0_tD494223E41D1BFA7571AC3568E9A99A8B3FED886 * value)
	{
		___U3CU3E8__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_6), value);
	}

	inline static int32_t get_offset_of_materialIndex_7() { return static_cast<int32_t>(offsetof(U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9, ___materialIndex_7)); }
	inline int32_t get_materialIndex_7() const { return ___materialIndex_7; }
	inline int32_t* get_address_of_materialIndex_7() { return &___materialIndex_7; }
	inline void set_materialIndex_7(int32_t value)
	{
		___materialIndex_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_8() { return static_cast<int32_t>(offsetof(U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9, ___U3CU3Eu__1_8)); }
	inline TaskAwaiter_1_tAF81E0A0509152C9EFD9A2A752D472023C3FD550  get_U3CU3Eu__1_8() const { return ___U3CU3Eu__1_8; }
	inline TaskAwaiter_1_tAF81E0A0509152C9EFD9A2A752D472023C3FD550 * get_address_of_U3CU3Eu__1_8() { return &___U3CU3Eu__1_8; }
	inline void set_U3CU3Eu__1_8(TaskAwaiter_1_tAF81E0A0509152C9EFD9A2A752D472023C3FD550  value)
	{
		___U3CU3Eu__1_8 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__2_9() { return static_cast<int32_t>(offsetof(U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9, ___U3CU3Eu__2_9)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__2_9() const { return ___U3CU3Eu__2_9; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__2_9() { return &___U3CU3Eu__2_9; }
	inline void set_U3CU3Eu__2_9(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__2_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONSTRUCTMESHPRIMITIVEU3ED__73_T1EC85122DC787A6C42488ADF833B4B7A4367EBC9_H
#ifndef U3CCONSTRUCTNODEU3ED__64_TA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B_H
#define U3CCONSTRUCTNODEU3ED__64_TA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64
struct  U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::nodeIndex
	int32_t ___nodeIndex_3;
	// GLTF.Schema.Node UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::node
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744 * ___node_4;
	// UnityEngine.GameObject UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::<nodeObj>5__2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CnodeObjU3E5__2_5;
	// GLTF.Schema.MSFT_LODExtension UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::<lodsextension>5__3
	MSFT_LODExtension_tA2000797F265C91522ED3917FEB2078117995805 * ___U3ClodsextensionU3E5__3_6;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_7;
	// System.Collections.Generic.List`1/Enumerator<GLTF.Schema.NodeId> UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::<>7__wrap3
	Enumerator_tEF9CAAB01B45A327284703F74A8B453B79473A75  ___U3CU3E7__wrap3_8;
	// GLTF.Schema.NodeId UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::<child>5__5
	NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * ___U3CchildU3E5__5_9;
	// UnityEngine.LOD[] UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::<lods>5__6
	LODU5BU5D_tF08898FC223F367F3F9154401FC6913759C62D28* ___U3ClodsU3E5__6_10;
	// System.Collections.Generic.List`1<System.Double> UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::<lodCoverage>5__7
	List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * ___U3ClodCoverageU3E5__7_11;
	// UnityEngine.GameObject UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::<lodGroupNodeObj>5__8
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3ClodGroupNodeObjU3E5__8_12;
	// UnityEngine.LODGroup UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::<lodGroup>5__9
	LODGroup_t2B5F4CADAA263141AC8EEABB56E6C73B057C6892 * ___U3ClodGroupU3E5__9_13;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::<i>5__10
	int32_t ___U3CiU3E5__10_14;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructNode>d__64::<lodNodeId>5__11
	int32_t ___U3ClodNodeIdU3E5__11_15;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_nodeIndex_3() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___nodeIndex_3)); }
	inline int32_t get_nodeIndex_3() const { return ___nodeIndex_3; }
	inline int32_t* get_address_of_nodeIndex_3() { return &___nodeIndex_3; }
	inline void set_nodeIndex_3(int32_t value)
	{
		___nodeIndex_3 = value;
	}

	inline static int32_t get_offset_of_node_4() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___node_4)); }
	inline Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744 * get_node_4() const { return ___node_4; }
	inline Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744 ** get_address_of_node_4() { return &___node_4; }
	inline void set_node_4(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744 * value)
	{
		___node_4 = value;
		Il2CppCodeGenWriteBarrier((&___node_4), value);
	}

	inline static int32_t get_offset_of_U3CnodeObjU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___U3CnodeObjU3E5__2_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CnodeObjU3E5__2_5() const { return ___U3CnodeObjU3E5__2_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CnodeObjU3E5__2_5() { return &___U3CnodeObjU3E5__2_5; }
	inline void set_U3CnodeObjU3E5__2_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CnodeObjU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeObjU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3ClodsextensionU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___U3ClodsextensionU3E5__3_6)); }
	inline MSFT_LODExtension_tA2000797F265C91522ED3917FEB2078117995805 * get_U3ClodsextensionU3E5__3_6() const { return ___U3ClodsextensionU3E5__3_6; }
	inline MSFT_LODExtension_tA2000797F265C91522ED3917FEB2078117995805 ** get_address_of_U3ClodsextensionU3E5__3_6() { return &___U3ClodsextensionU3E5__3_6; }
	inline void set_U3ClodsextensionU3E5__3_6(MSFT_LODExtension_tA2000797F265C91522ED3917FEB2078117995805 * value)
	{
		___U3ClodsextensionU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClodsextensionU3E5__3_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_7() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___U3CU3Eu__1_7)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_7() const { return ___U3CU3Eu__1_7; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_7() { return &___U3CU3Eu__1_7; }
	inline void set_U3CU3Eu__1_7(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap3_8() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___U3CU3E7__wrap3_8)); }
	inline Enumerator_tEF9CAAB01B45A327284703F74A8B453B79473A75  get_U3CU3E7__wrap3_8() const { return ___U3CU3E7__wrap3_8; }
	inline Enumerator_tEF9CAAB01B45A327284703F74A8B453B79473A75 * get_address_of_U3CU3E7__wrap3_8() { return &___U3CU3E7__wrap3_8; }
	inline void set_U3CU3E7__wrap3_8(Enumerator_tEF9CAAB01B45A327284703F74A8B453B79473A75  value)
	{
		___U3CU3E7__wrap3_8 = value;
	}

	inline static int32_t get_offset_of_U3CchildU3E5__5_9() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___U3CchildU3E5__5_9)); }
	inline NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * get_U3CchildU3E5__5_9() const { return ___U3CchildU3E5__5_9; }
	inline NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 ** get_address_of_U3CchildU3E5__5_9() { return &___U3CchildU3E5__5_9; }
	inline void set_U3CchildU3E5__5_9(NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * value)
	{
		___U3CchildU3E5__5_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CchildU3E5__5_9), value);
	}

	inline static int32_t get_offset_of_U3ClodsU3E5__6_10() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___U3ClodsU3E5__6_10)); }
	inline LODU5BU5D_tF08898FC223F367F3F9154401FC6913759C62D28* get_U3ClodsU3E5__6_10() const { return ___U3ClodsU3E5__6_10; }
	inline LODU5BU5D_tF08898FC223F367F3F9154401FC6913759C62D28** get_address_of_U3ClodsU3E5__6_10() { return &___U3ClodsU3E5__6_10; }
	inline void set_U3ClodsU3E5__6_10(LODU5BU5D_tF08898FC223F367F3F9154401FC6913759C62D28* value)
	{
		___U3ClodsU3E5__6_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClodsU3E5__6_10), value);
	}

	inline static int32_t get_offset_of_U3ClodCoverageU3E5__7_11() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___U3ClodCoverageU3E5__7_11)); }
	inline List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * get_U3ClodCoverageU3E5__7_11() const { return ___U3ClodCoverageU3E5__7_11; }
	inline List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E ** get_address_of_U3ClodCoverageU3E5__7_11() { return &___U3ClodCoverageU3E5__7_11; }
	inline void set_U3ClodCoverageU3E5__7_11(List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * value)
	{
		___U3ClodCoverageU3E5__7_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClodCoverageU3E5__7_11), value);
	}

	inline static int32_t get_offset_of_U3ClodGroupNodeObjU3E5__8_12() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___U3ClodGroupNodeObjU3E5__8_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3ClodGroupNodeObjU3E5__8_12() const { return ___U3ClodGroupNodeObjU3E5__8_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3ClodGroupNodeObjU3E5__8_12() { return &___U3ClodGroupNodeObjU3E5__8_12; }
	inline void set_U3ClodGroupNodeObjU3E5__8_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3ClodGroupNodeObjU3E5__8_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClodGroupNodeObjU3E5__8_12), value);
	}

	inline static int32_t get_offset_of_U3ClodGroupU3E5__9_13() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___U3ClodGroupU3E5__9_13)); }
	inline LODGroup_t2B5F4CADAA263141AC8EEABB56E6C73B057C6892 * get_U3ClodGroupU3E5__9_13() const { return ___U3ClodGroupU3E5__9_13; }
	inline LODGroup_t2B5F4CADAA263141AC8EEABB56E6C73B057C6892 ** get_address_of_U3ClodGroupU3E5__9_13() { return &___U3ClodGroupU3E5__9_13; }
	inline void set_U3ClodGroupU3E5__9_13(LODGroup_t2B5F4CADAA263141AC8EEABB56E6C73B057C6892 * value)
	{
		___U3ClodGroupU3E5__9_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClodGroupU3E5__9_13), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__10_14() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___U3CiU3E5__10_14)); }
	inline int32_t get_U3CiU3E5__10_14() const { return ___U3CiU3E5__10_14; }
	inline int32_t* get_address_of_U3CiU3E5__10_14() { return &___U3CiU3E5__10_14; }
	inline void set_U3CiU3E5__10_14(int32_t value)
	{
		___U3CiU3E5__10_14 = value;
	}

	inline static int32_t get_offset_of_U3ClodNodeIdU3E5__11_15() { return static_cast<int32_t>(offsetof(U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B, ___U3ClodNodeIdU3E5__11_15)); }
	inline int32_t get_U3ClodNodeIdU3E5__11_15() const { return ___U3ClodNodeIdU3E5__11_15; }
	inline int32_t* get_address_of_U3ClodNodeIdU3E5__11_15() { return &___U3ClodNodeIdU3E5__11_15; }
	inline void set_U3ClodNodeIdU3E5__11_15(int32_t value)
	{
		___U3ClodNodeIdU3E5__11_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONSTRUCTNODEU3ED__64_TA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B_H
#ifndef U3CCONSTRUCTSCENEU3ED__63_TB41D9E6916623C7CB011C6B5044B54DDC2D32F1F_H
#define U3CCONSTRUCTSCENEU3ED__63_TB41D9E6916623C7CB011C6B5044B54DDC2D32F1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<ConstructScene>d__63
struct  U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructScene>d__63::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<ConstructScene>d__63::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// GLTF.Schema.GLTFScene UnityGLTF.GLTFSceneImporter/<ConstructScene>d__63::scene
	GLTFScene_t668D116E319247178F4641B2F3A4DF33C6343BB6 * ___scene_2;
	// System.Boolean UnityGLTF.GLTFSceneImporter/<ConstructScene>d__63::showSceneObj
	bool ___showSceneObj_3;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<ConstructScene>d__63::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_4;
	// UnityEngine.GameObject UnityGLTF.GLTFSceneImporter/<ConstructScene>d__63::<sceneObj>5__2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CsceneObjU3E5__2_5;
	// UnityEngine.Transform[] UnityGLTF.GLTFSceneImporter/<ConstructScene>d__63::<nodeTransforms>5__3
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* ___U3CnodeTransformsU3E5__3_6;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructScene>d__63::<i>5__4
	int32_t ___U3CiU3E5__4_7;
	// GLTF.Schema.NodeId UnityGLTF.GLTFSceneImporter/<ConstructScene>d__63::<node>5__5
	NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * ___U3CnodeU3E5__5_8;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<ConstructScene>d__63::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_scene_2() { return static_cast<int32_t>(offsetof(U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F, ___scene_2)); }
	inline GLTFScene_t668D116E319247178F4641B2F3A4DF33C6343BB6 * get_scene_2() const { return ___scene_2; }
	inline GLTFScene_t668D116E319247178F4641B2F3A4DF33C6343BB6 ** get_address_of_scene_2() { return &___scene_2; }
	inline void set_scene_2(GLTFScene_t668D116E319247178F4641B2F3A4DF33C6343BB6 * value)
	{
		___scene_2 = value;
		Il2CppCodeGenWriteBarrier((&___scene_2), value);
	}

	inline static int32_t get_offset_of_showSceneObj_3() { return static_cast<int32_t>(offsetof(U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F, ___showSceneObj_3)); }
	inline bool get_showSceneObj_3() const { return ___showSceneObj_3; }
	inline bool* get_address_of_showSceneObj_3() { return &___showSceneObj_3; }
	inline void set_showSceneObj_3(bool value)
	{
		___showSceneObj_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F, ___U3CU3E4__this_4)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_U3CsceneObjU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F, ___U3CsceneObjU3E5__2_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CsceneObjU3E5__2_5() const { return ___U3CsceneObjU3E5__2_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CsceneObjU3E5__2_5() { return &___U3CsceneObjU3E5__2_5; }
	inline void set_U3CsceneObjU3E5__2_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CsceneObjU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsceneObjU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3CnodeTransformsU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F, ___U3CnodeTransformsU3E5__3_6)); }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* get_U3CnodeTransformsU3E5__3_6() const { return ___U3CnodeTransformsU3E5__3_6; }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804** get_address_of_U3CnodeTransformsU3E5__3_6() { return &___U3CnodeTransformsU3E5__3_6; }
	inline void set_U3CnodeTransformsU3E5__3_6(TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* value)
	{
		___U3CnodeTransformsU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeTransformsU3E5__3_6), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F, ___U3CiU3E5__4_7)); }
	inline int32_t get_U3CiU3E5__4_7() const { return ___U3CiU3E5__4_7; }
	inline int32_t* get_address_of_U3CiU3E5__4_7() { return &___U3CiU3E5__4_7; }
	inline void set_U3CiU3E5__4_7(int32_t value)
	{
		___U3CiU3E5__4_7 = value;
	}

	inline static int32_t get_offset_of_U3CnodeU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F, ___U3CnodeU3E5__5_8)); }
	inline NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * get_U3CnodeU3E5__5_8() const { return ___U3CnodeU3E5__5_8; }
	inline NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 ** get_address_of_U3CnodeU3E5__5_8() { return &___U3CnodeU3E5__5_8; }
	inline void set_U3CnodeU3E5__5_8(NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * value)
	{
		___U3CnodeU3E5__5_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeU3E5__5_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_9() { return static_cast<int32_t>(offsetof(U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F, ___U3CU3Eu__1_9)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_9() const { return ___U3CU3Eu__1_9; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_9() { return &___U3CU3Eu__1_9; }
	inline void set_U3CU3Eu__1_9(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONSTRUCTSCENEU3ED__63_TB41D9E6916623C7CB011C6B5044B54DDC2D32F1F_H
#ifndef U3CCONSTRUCTUNITYMESHU3ED__77_T85B4F6C284934F074E708D8E2AAB58643BB334B5_H
#define U3CCONSTRUCTUNITYMESHU3ED__77_T85B4F6C284934F074E708D8E2AAB58643BB334B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<ConstructUnityMesh>d__77
struct  U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructUnityMesh>d__77::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<ConstructUnityMesh>d__77::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.MeshConstructionData UnityGLTF.GLTFSceneImporter/<ConstructUnityMesh>d__77::meshConstructionData
	MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419  ___meshConstructionData_2;
	// UnityGLTF.UnityMeshData UnityGLTF.GLTFSceneImporter/<ConstructUnityMesh>d__77::unityMeshData
	UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D * ___unityMeshData_3;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<ConstructUnityMesh>d__77::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_4;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructUnityMesh>d__77::meshId
	int32_t ___meshId_5;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructUnityMesh>d__77::primitiveIndex
	int32_t ___primitiveIndex_6;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructUnityMesh>d__77::<vertexCount>5__2
	int32_t ___U3CvertexCountU3E5__2_7;
	// System.Boolean UnityGLTF.GLTFSceneImporter/<ConstructUnityMesh>d__77::<hasNormals>5__3
	bool ___U3ChasNormalsU3E5__3_8;
	// UnityEngine.Mesh UnityGLTF.GLTFSceneImporter/<ConstructUnityMesh>d__77::<mesh>5__4
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___U3CmeshU3E5__4_9;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<ConstructUnityMesh>d__77::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_meshConstructionData_2() { return static_cast<int32_t>(offsetof(U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5, ___meshConstructionData_2)); }
	inline MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419  get_meshConstructionData_2() const { return ___meshConstructionData_2; }
	inline MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419 * get_address_of_meshConstructionData_2() { return &___meshConstructionData_2; }
	inline void set_meshConstructionData_2(MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419  value)
	{
		___meshConstructionData_2 = value;
	}

	inline static int32_t get_offset_of_unityMeshData_3() { return static_cast<int32_t>(offsetof(U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5, ___unityMeshData_3)); }
	inline UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D * get_unityMeshData_3() const { return ___unityMeshData_3; }
	inline UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D ** get_address_of_unityMeshData_3() { return &___unityMeshData_3; }
	inline void set_unityMeshData_3(UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D * value)
	{
		___unityMeshData_3 = value;
		Il2CppCodeGenWriteBarrier((&___unityMeshData_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5, ___U3CU3E4__this_4)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_meshId_5() { return static_cast<int32_t>(offsetof(U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5, ___meshId_5)); }
	inline int32_t get_meshId_5() const { return ___meshId_5; }
	inline int32_t* get_address_of_meshId_5() { return &___meshId_5; }
	inline void set_meshId_5(int32_t value)
	{
		___meshId_5 = value;
	}

	inline static int32_t get_offset_of_primitiveIndex_6() { return static_cast<int32_t>(offsetof(U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5, ___primitiveIndex_6)); }
	inline int32_t get_primitiveIndex_6() const { return ___primitiveIndex_6; }
	inline int32_t* get_address_of_primitiveIndex_6() { return &___primitiveIndex_6; }
	inline void set_primitiveIndex_6(int32_t value)
	{
		___primitiveIndex_6 = value;
	}

	inline static int32_t get_offset_of_U3CvertexCountU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5, ___U3CvertexCountU3E5__2_7)); }
	inline int32_t get_U3CvertexCountU3E5__2_7() const { return ___U3CvertexCountU3E5__2_7; }
	inline int32_t* get_address_of_U3CvertexCountU3E5__2_7() { return &___U3CvertexCountU3E5__2_7; }
	inline void set_U3CvertexCountU3E5__2_7(int32_t value)
	{
		___U3CvertexCountU3E5__2_7 = value;
	}

	inline static int32_t get_offset_of_U3ChasNormalsU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5, ___U3ChasNormalsU3E5__3_8)); }
	inline bool get_U3ChasNormalsU3E5__3_8() const { return ___U3ChasNormalsU3E5__3_8; }
	inline bool* get_address_of_U3ChasNormalsU3E5__3_8() { return &___U3ChasNormalsU3E5__3_8; }
	inline void set_U3ChasNormalsU3E5__3_8(bool value)
	{
		___U3ChasNormalsU3E5__3_8 = value;
	}

	inline static int32_t get_offset_of_U3CmeshU3E5__4_9() { return static_cast<int32_t>(offsetof(U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5, ___U3CmeshU3E5__4_9)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_U3CmeshU3E5__4_9() const { return ___U3CmeshU3E5__4_9; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_U3CmeshU3E5__4_9() { return &___U3CmeshU3E5__4_9; }
	inline void set_U3CmeshU3E5__4_9(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___U3CmeshU3E5__4_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmeshU3E5__4_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_10() { return static_cast<int32_t>(offsetof(U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5, ___U3CU3Eu__1_10)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_10() const { return ___U3CU3Eu__1_10; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_10() { return &___U3CU3Eu__1_10; }
	inline void set_U3CU3Eu__1_10(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONSTRUCTUNITYMESHU3ED__77_T85B4F6C284934F074E708D8E2AAB58643BB334B5_H
#ifndef U3CCONSTRUCTUNITYTEXTUREU3ED__56_TBA6303F731167733DB4DEB17898073F86FE1EA25_H
#define U3CCONSTRUCTUNITYTEXTUREU3ED__56_TBA6303F731167733DB4DEB17898073F86FE1EA25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<ConstructUnityTexture>d__56
struct  U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructUnityTexture>d__56::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<ConstructUnityTexture>d__56::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// System.Boolean UnityGLTF.GLTFSceneImporter/<ConstructUnityTexture>d__56::linear
	bool ___linear_2;
	// System.IO.Stream UnityGLTF.GLTFSceneImporter/<ConstructUnityTexture>d__56::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_3;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<ConstructUnityTexture>d__56::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_4;
	// System.Boolean UnityGLTF.GLTFSceneImporter/<ConstructUnityTexture>d__56::markGpuOnly
	bool ___markGpuOnly_5;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ConstructUnityTexture>d__56::imageCacheIndex
	int32_t ___imageCacheIndex_6;
	// UnityEngine.Texture2D UnityGLTF.GLTFSceneImporter/<ConstructUnityTexture>d__56::<texture>5__2
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___U3CtextureU3E5__2_7;
	// System.Byte[] UnityGLTF.GLTFSceneImporter/<ConstructUnityTexture>d__56::<buffer>5__3
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CbufferU3E5__3_8;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<ConstructUnityTexture>d__56::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_linear_2() { return static_cast<int32_t>(offsetof(U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25, ___linear_2)); }
	inline bool get_linear_2() const { return ___linear_2; }
	inline bool* get_address_of_linear_2() { return &___linear_2; }
	inline void set_linear_2(bool value)
	{
		___linear_2 = value;
	}

	inline static int32_t get_offset_of_stream_3() { return static_cast<int32_t>(offsetof(U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25, ___stream_3)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_3() const { return ___stream_3; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_3() { return &___stream_3; }
	inline void set_stream_3(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_3 = value;
		Il2CppCodeGenWriteBarrier((&___stream_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25, ___U3CU3E4__this_4)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_markGpuOnly_5() { return static_cast<int32_t>(offsetof(U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25, ___markGpuOnly_5)); }
	inline bool get_markGpuOnly_5() const { return ___markGpuOnly_5; }
	inline bool* get_address_of_markGpuOnly_5() { return &___markGpuOnly_5; }
	inline void set_markGpuOnly_5(bool value)
	{
		___markGpuOnly_5 = value;
	}

	inline static int32_t get_offset_of_imageCacheIndex_6() { return static_cast<int32_t>(offsetof(U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25, ___imageCacheIndex_6)); }
	inline int32_t get_imageCacheIndex_6() const { return ___imageCacheIndex_6; }
	inline int32_t* get_address_of_imageCacheIndex_6() { return &___imageCacheIndex_6; }
	inline void set_imageCacheIndex_6(int32_t value)
	{
		___imageCacheIndex_6 = value;
	}

	inline static int32_t get_offset_of_U3CtextureU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25, ___U3CtextureU3E5__2_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_U3CtextureU3E5__2_7() const { return ___U3CtextureU3E5__2_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_U3CtextureU3E5__2_7() { return &___U3CtextureU3E5__2_7; }
	inline void set_U3CtextureU3E5__2_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___U3CtextureU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextureU3E5__2_7), value);
	}

	inline static int32_t get_offset_of_U3CbufferU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25, ___U3CbufferU3E5__3_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CbufferU3E5__3_8() const { return ___U3CbufferU3E5__3_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CbufferU3E5__3_8() { return &___U3CbufferU3E5__3_8; }
	inline void set_U3CbufferU3E5__3_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CbufferU3E5__3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbufferU3E5__3_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_9() { return static_cast<int32_t>(offsetof(U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25, ___U3CU3Eu__1_9)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_9() const { return ___U3CU3Eu__1_9; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_9() { return &___U3CU3Eu__1_9; }
	inline void set_U3CU3Eu__1_9(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONSTRUCTUNITYTEXTUREU3ED__56_TBA6303F731167733DB4DEB17898073F86FE1EA25_H
#ifndef U3CLOADJSONU3ED__50_T683B35F04BE5EED5474504834C5367319DFD940A_H
#define U3CLOADJSONU3ED__50_T683B35F04BE5EED5474504834C5367319DFD940A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<LoadJson>d__50
struct  U3CLoadJsonU3Ed__50_t683B35F04BE5EED5474504834C5367319DFD940A 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<LoadJson>d__50::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<LoadJson>d__50::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<LoadJson>d__50::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// System.String UnityGLTF.GLTFSceneImporter/<LoadJson>d__50::jsonFilePath
	String_t* ___jsonFilePath_3;
	// UnityGLTF.GLTFSceneImporter/<>c__DisplayClass50_0 UnityGLTF.GLTFSceneImporter/<LoadJson>d__50::<>8__1
	U3CU3Ec__DisplayClass50_0_tDCB32992B37184B2BAAD02F93BC74DB03178998E * ___U3CU3E8__1_4;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<LoadJson>d__50::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadJsonU3Ed__50_t683B35F04BE5EED5474504834C5367319DFD940A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CLoadJsonU3Ed__50_t683B35F04BE5EED5474504834C5367319DFD940A, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadJsonU3Ed__50_t683B35F04BE5EED5474504834C5367319DFD940A, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_jsonFilePath_3() { return static_cast<int32_t>(offsetof(U3CLoadJsonU3Ed__50_t683B35F04BE5EED5474504834C5367319DFD940A, ___jsonFilePath_3)); }
	inline String_t* get_jsonFilePath_3() const { return ___jsonFilePath_3; }
	inline String_t** get_address_of_jsonFilePath_3() { return &___jsonFilePath_3; }
	inline void set_jsonFilePath_3(String_t* value)
	{
		___jsonFilePath_3 = value;
		Il2CppCodeGenWriteBarrier((&___jsonFilePath_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_4() { return static_cast<int32_t>(offsetof(U3CLoadJsonU3Ed__50_t683B35F04BE5EED5474504834C5367319DFD940A, ___U3CU3E8__1_4)); }
	inline U3CU3Ec__DisplayClass50_0_tDCB32992B37184B2BAAD02F93BC74DB03178998E * get_U3CU3E8__1_4() const { return ___U3CU3E8__1_4; }
	inline U3CU3Ec__DisplayClass50_0_tDCB32992B37184B2BAAD02F93BC74DB03178998E ** get_address_of_U3CU3E8__1_4() { return &___U3CU3E8__1_4; }
	inline void set_U3CU3E8__1_4(U3CU3Ec__DisplayClass50_0_tDCB32992B37184B2BAAD02F93BC74DB03178998E * value)
	{
		___U3CU3E8__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CLoadJsonU3Ed__50_t683B35F04BE5EED5474504834C5367319DFD940A, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADJSONU3ED__50_T683B35F04BE5EED5474504834C5367319DFD940A_H
#ifndef U3CLOADNODEASYNCU3ED__42_TCE807A7F6B49C9E8FCD562667E86E99BD38F6DF7_H
#define U3CLOADNODEASYNCU3ED__42_TCE807A7F6B49C9E8FCD562667E86E99BD38F6DF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<LoadNodeAsync>d__42
struct  U3CLoadNodeAsyncU3Ed__42_tCE807A7F6B49C9E8FCD562667E86E99BD38F6DF7 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<LoadNodeAsync>d__42::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<LoadNodeAsync>d__42::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<LoadNodeAsync>d__42::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<LoadNodeAsync>d__42::nodeIndex
	int32_t ___nodeIndex_3;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<LoadNodeAsync>d__42::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadNodeAsyncU3Ed__42_tCE807A7F6B49C9E8FCD562667E86E99BD38F6DF7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CLoadNodeAsyncU3Ed__42_tCE807A7F6B49C9E8FCD562667E86E99BD38F6DF7, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadNodeAsyncU3Ed__42_tCE807A7F6B49C9E8FCD562667E86E99BD38F6DF7, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_nodeIndex_3() { return static_cast<int32_t>(offsetof(U3CLoadNodeAsyncU3Ed__42_tCE807A7F6B49C9E8FCD562667E86E99BD38F6DF7, ___nodeIndex_3)); }
	inline int32_t get_nodeIndex_3() const { return ___nodeIndex_3; }
	inline int32_t* get_address_of_nodeIndex_3() { return &___nodeIndex_3; }
	inline void set_nodeIndex_3(int32_t value)
	{
		___nodeIndex_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_4() { return static_cast<int32_t>(offsetof(U3CLoadNodeAsyncU3Ed__42_tCE807A7F6B49C9E8FCD562667E86E99BD38F6DF7, ___U3CU3Eu__1_4)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_4() const { return ___U3CU3Eu__1_4; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_4() { return &___U3CU3Eu__1_4; }
	inline void set_U3CU3Eu__1_4(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADNODEASYNCU3ED__42_TCE807A7F6B49C9E8FCD562667E86E99BD38F6DF7_H
#ifndef U3CLOADSCENEASYNCU3ED__40_TD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F_H
#define U3CLOADSCENEASYNCU3ED__40_TD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<LoadSceneAsync>d__40
struct  U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<LoadSceneAsync>d__40::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<LoadSceneAsync>d__40::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<LoadSceneAsync>d__40::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<LoadSceneAsync>d__40::sceneIndex
	int32_t ___sceneIndex_3;
	// System.Boolean UnityGLTF.GLTFSceneImporter/<LoadSceneAsync>d__40::showSceneObj
	bool ___showSceneObj_4;
	// System.Action`2<UnityEngine.GameObject,System.Runtime.ExceptionServices.ExceptionDispatchInfo> UnityGLTF.GLTFSceneImporter/<LoadSceneAsync>d__40::onLoadComplete
	Action_2_tA8345456806B324BA9E2C0C4E51F157CE28878B6 * ___onLoadComplete_5;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<LoadSceneAsync>d__40::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_sceneIndex_3() { return static_cast<int32_t>(offsetof(U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F, ___sceneIndex_3)); }
	inline int32_t get_sceneIndex_3() const { return ___sceneIndex_3; }
	inline int32_t* get_address_of_sceneIndex_3() { return &___sceneIndex_3; }
	inline void set_sceneIndex_3(int32_t value)
	{
		___sceneIndex_3 = value;
	}

	inline static int32_t get_offset_of_showSceneObj_4() { return static_cast<int32_t>(offsetof(U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F, ___showSceneObj_4)); }
	inline bool get_showSceneObj_4() const { return ___showSceneObj_4; }
	inline bool* get_address_of_showSceneObj_4() { return &___showSceneObj_4; }
	inline void set_showSceneObj_4(bool value)
	{
		___showSceneObj_4 = value;
	}

	inline static int32_t get_offset_of_onLoadComplete_5() { return static_cast<int32_t>(offsetof(U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F, ___onLoadComplete_5)); }
	inline Action_2_tA8345456806B324BA9E2C0C4E51F157CE28878B6 * get_onLoadComplete_5() const { return ___onLoadComplete_5; }
	inline Action_2_tA8345456806B324BA9E2C0C4E51F157CE28878B6 ** get_address_of_onLoadComplete_5() { return &___onLoadComplete_5; }
	inline void set_onLoadComplete_5(Action_2_tA8345456806B324BA9E2C0C4E51F157CE28878B6 * value)
	{
		___onLoadComplete_5 = value;
		Il2CppCodeGenWriteBarrier((&___onLoadComplete_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_6() { return static_cast<int32_t>(offsetof(U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F, ___U3CU3Eu__1_6)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_6() const { return ___U3CU3Eu__1_6; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_6() { return &___U3CU3Eu__1_6; }
	inline void set_U3CU3Eu__1_6(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCENEASYNCU3ED__40_TD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F_H
#ifndef U3CSETUPBONESU3ED__69_T819E455A4E83C57661BDEBACB331350123B6ACF4_H
#define U3CSETUPBONESU3ED__69_T819E455A4E83C57661BDEBACB331350123B6ACF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<SetupBones>d__69
struct  U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<SetupBones>d__69::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<SetupBones>d__69::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// GLTF.Schema.Skin UnityGLTF.GLTFSceneImporter/<SetupBones>d__69::skin
	Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34 * ___skin_2;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<SetupBones>d__69::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_3;
	// UnityEngine.SkinnedMeshRenderer UnityGLTF.GLTFSceneImporter/<SetupBones>d__69::renderer
	SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * ___renderer_4;
	// UnityEngine.Mesh UnityGLTF.GLTFSceneImporter/<SetupBones>d__69::curMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___curMesh_5;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<SetupBones>d__69::<boneCount>5__2
	int32_t ___U3CboneCountU3E5__2_6;
	// UnityEngine.Transform[] UnityGLTF.GLTFSceneImporter/<SetupBones>d__69::<bones>5__3
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* ___U3CbonesU3E5__3_7;
	// GLTF.Math.Matrix4x4[] UnityGLTF.GLTFSceneImporter/<SetupBones>d__69::<gltfBindPoses>5__4
	Matrix4x4U5BU5D_tE3A13CB78E1B2CBDCD537180E7C6D83E18B0D9F0* ___U3CgltfBindPosesU3E5__4_8;
	// UnityEngine.Matrix4x4[] UnityGLTF.GLTFSceneImporter/<SetupBones>d__69::<bindPoses>5__5
	Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9* ___U3CbindPosesU3E5__5_9;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<SetupBones>d__69::<i>5__6
	int32_t ___U3CiU3E5__6_10;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<SetupBones>d__69::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_11;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_skin_2() { return static_cast<int32_t>(offsetof(U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4, ___skin_2)); }
	inline Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34 * get_skin_2() const { return ___skin_2; }
	inline Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34 ** get_address_of_skin_2() { return &___skin_2; }
	inline void set_skin_2(Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34 * value)
	{
		___skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___skin_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4, ___U3CU3E4__this_3)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_renderer_4() { return static_cast<int32_t>(offsetof(U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4, ___renderer_4)); }
	inline SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * get_renderer_4() const { return ___renderer_4; }
	inline SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 ** get_address_of_renderer_4() { return &___renderer_4; }
	inline void set_renderer_4(SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * value)
	{
		___renderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___renderer_4), value);
	}

	inline static int32_t get_offset_of_curMesh_5() { return static_cast<int32_t>(offsetof(U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4, ___curMesh_5)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_curMesh_5() const { return ___curMesh_5; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_curMesh_5() { return &___curMesh_5; }
	inline void set_curMesh_5(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___curMesh_5 = value;
		Il2CppCodeGenWriteBarrier((&___curMesh_5), value);
	}

	inline static int32_t get_offset_of_U3CboneCountU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4, ___U3CboneCountU3E5__2_6)); }
	inline int32_t get_U3CboneCountU3E5__2_6() const { return ___U3CboneCountU3E5__2_6; }
	inline int32_t* get_address_of_U3CboneCountU3E5__2_6() { return &___U3CboneCountU3E5__2_6; }
	inline void set_U3CboneCountU3E5__2_6(int32_t value)
	{
		___U3CboneCountU3E5__2_6 = value;
	}

	inline static int32_t get_offset_of_U3CbonesU3E5__3_7() { return static_cast<int32_t>(offsetof(U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4, ___U3CbonesU3E5__3_7)); }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* get_U3CbonesU3E5__3_7() const { return ___U3CbonesU3E5__3_7; }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804** get_address_of_U3CbonesU3E5__3_7() { return &___U3CbonesU3E5__3_7; }
	inline void set_U3CbonesU3E5__3_7(TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* value)
	{
		___U3CbonesU3E5__3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbonesU3E5__3_7), value);
	}

	inline static int32_t get_offset_of_U3CgltfBindPosesU3E5__4_8() { return static_cast<int32_t>(offsetof(U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4, ___U3CgltfBindPosesU3E5__4_8)); }
	inline Matrix4x4U5BU5D_tE3A13CB78E1B2CBDCD537180E7C6D83E18B0D9F0* get_U3CgltfBindPosesU3E5__4_8() const { return ___U3CgltfBindPosesU3E5__4_8; }
	inline Matrix4x4U5BU5D_tE3A13CB78E1B2CBDCD537180E7C6D83E18B0D9F0** get_address_of_U3CgltfBindPosesU3E5__4_8() { return &___U3CgltfBindPosesU3E5__4_8; }
	inline void set_U3CgltfBindPosesU3E5__4_8(Matrix4x4U5BU5D_tE3A13CB78E1B2CBDCD537180E7C6D83E18B0D9F0* value)
	{
		___U3CgltfBindPosesU3E5__4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgltfBindPosesU3E5__4_8), value);
	}

	inline static int32_t get_offset_of_U3CbindPosesU3E5__5_9() { return static_cast<int32_t>(offsetof(U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4, ___U3CbindPosesU3E5__5_9)); }
	inline Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9* get_U3CbindPosesU3E5__5_9() const { return ___U3CbindPosesU3E5__5_9; }
	inline Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9** get_address_of_U3CbindPosesU3E5__5_9() { return &___U3CbindPosesU3E5__5_9; }
	inline void set_U3CbindPosesU3E5__5_9(Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9* value)
	{
		___U3CbindPosesU3E5__5_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbindPosesU3E5__5_9), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__6_10() { return static_cast<int32_t>(offsetof(U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4, ___U3CiU3E5__6_10)); }
	inline int32_t get_U3CiU3E5__6_10() const { return ___U3CiU3E5__6_10; }
	inline int32_t* get_address_of_U3CiU3E5__6_10() { return &___U3CiU3E5__6_10; }
	inline void set_U3CiU3E5__6_10(int32_t value)
	{
		___U3CiU3E5__6_10 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_11() { return static_cast<int32_t>(offsetof(U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4, ___U3CU3Eu__1_11)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_11() const { return ___U3CU3Eu__1_11; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_11() { return &___U3CU3Eu__1_11; }
	inline void set_U3CU3Eu__1_11(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETUPBONESU3ED__69_T819E455A4E83C57661BDEBACB331350123B6ACF4_H
#ifndef U3CTRYYIELDONTIMEOUTU3ED__74_TF2900FF5C648912CE5DAA0C36BD396DA386AF5C6_H
#define U3CTRYYIELDONTIMEOUTU3ED__74_TF2900FF5C648912CE5DAA0C36BD396DA386AF5C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<TryYieldOnTimeout>d__74
struct  U3CTryYieldOnTimeoutU3Ed__74_tF2900FF5C648912CE5DAA0C36BD396DA386AF5C6 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<TryYieldOnTimeout>d__74::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<TryYieldOnTimeout>d__74::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<TryYieldOnTimeout>d__74::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<TryYieldOnTimeout>d__74::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTryYieldOnTimeoutU3Ed__74_tF2900FF5C648912CE5DAA0C36BD396DA386AF5C6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CTryYieldOnTimeoutU3Ed__74_tF2900FF5C648912CE5DAA0C36BD396DA386AF5C6, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTryYieldOnTimeoutU3Ed__74_tF2900FF5C648912CE5DAA0C36BD396DA386AF5C6, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_3() { return static_cast<int32_t>(offsetof(U3CTryYieldOnTimeoutU3Ed__74_tF2900FF5C648912CE5DAA0C36BD396DA386AF5C6, ___U3CU3Eu__1_3)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_3() const { return ___U3CU3Eu__1_3; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_3() { return &___U3CU3Eu__1_3; }
	inline void set_U3CU3Eu__1_3(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRYYIELDONTIMEOUTU3ED__74_TF2900FF5C648912CE5DAA0C36BD396DA386AF5C6_H
#ifndef U3C_LOADNODEU3ED__52_T4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7_H
#define U3C_LOADNODEU3ED__52_T4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<_LoadNode>d__52
struct  U3C_LoadNodeU3Ed__52_t4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<_LoadNode>d__52::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<_LoadNode>d__52::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<_LoadNode>d__52::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<_LoadNode>d__52::nodeIndex
	int32_t ___nodeIndex_3;
	// UnityGLTF.GLTFSceneImporter/<>c__DisplayClass52_0 UnityGLTF.GLTFSceneImporter/<_LoadNode>d__52::<>8__1
	U3CU3Ec__DisplayClass52_0_tD36628E62AE9ACC2EE3B3744FB9FAFEFC2CFFCCA * ___U3CU3E8__1_4;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<_LoadNode>d__52::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3C_LoadNodeU3Ed__52_t4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3C_LoadNodeU3Ed__52_t4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3C_LoadNodeU3Ed__52_t4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_nodeIndex_3() { return static_cast<int32_t>(offsetof(U3C_LoadNodeU3Ed__52_t4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7, ___nodeIndex_3)); }
	inline int32_t get_nodeIndex_3() const { return ___nodeIndex_3; }
	inline int32_t* get_address_of_nodeIndex_3() { return &___nodeIndex_3; }
	inline void set_nodeIndex_3(int32_t value)
	{
		___nodeIndex_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E8__1_4() { return static_cast<int32_t>(offsetof(U3C_LoadNodeU3Ed__52_t4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7, ___U3CU3E8__1_4)); }
	inline U3CU3Ec__DisplayClass52_0_tD36628E62AE9ACC2EE3B3744FB9FAFEFC2CFFCCA * get_U3CU3E8__1_4() const { return ___U3CU3E8__1_4; }
	inline U3CU3Ec__DisplayClass52_0_tD36628E62AE9ACC2EE3B3744FB9FAFEFC2CFFCCA ** get_address_of_U3CU3E8__1_4() { return &___U3CU3E8__1_4; }
	inline void set_U3CU3E8__1_4(U3CU3Ec__DisplayClass52_0_tD36628E62AE9ACC2EE3B3744FB9FAFEFC2CFFCCA * value)
	{
		___U3CU3E8__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3C_LoadNodeU3Ed__52_t4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_LOADNODEU3ED__52_T4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7_H
#ifndef U3C_LOADSCENEU3ED__53_T8BAE33712802D267371AF1C769DC15F2496BAB38_H
#define U3C_LOADSCENEU3ED__53_T8BAE33712802D267371AF1C769DC15F2496BAB38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<_LoadScene>d__53
struct  U3C_LoadSceneU3Ed__53_t8BAE33712802D267371AF1C769DC15F2496BAB38 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<_LoadScene>d__53::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.GLTFSceneImporter/<_LoadScene>d__53::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<_LoadScene>d__53::sceneIndex
	int32_t ___sceneIndex_2;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<_LoadScene>d__53::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_3;
	// System.Boolean UnityGLTF.GLTFSceneImporter/<_LoadScene>d__53::showSceneObj
	bool ___showSceneObj_4;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.GLTFSceneImporter/<_LoadScene>d__53::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3C_LoadSceneU3Ed__53_t8BAE33712802D267371AF1C769DC15F2496BAB38, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3C_LoadSceneU3Ed__53_t8BAE33712802D267371AF1C769DC15F2496BAB38, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_sceneIndex_2() { return static_cast<int32_t>(offsetof(U3C_LoadSceneU3Ed__53_t8BAE33712802D267371AF1C769DC15F2496BAB38, ___sceneIndex_2)); }
	inline int32_t get_sceneIndex_2() const { return ___sceneIndex_2; }
	inline int32_t* get_address_of_sceneIndex_2() { return &___sceneIndex_2; }
	inline void set_sceneIndex_2(int32_t value)
	{
		___sceneIndex_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3C_LoadSceneU3Ed__53_t8BAE33712802D267371AF1C769DC15F2496BAB38, ___U3CU3E4__this_3)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_showSceneObj_4() { return static_cast<int32_t>(offsetof(U3C_LoadSceneU3Ed__53_t8BAE33712802D267371AF1C769DC15F2496BAB38, ___showSceneObj_4)); }
	inline bool get_showSceneObj_4() const { return ___showSceneObj_4; }
	inline bool* get_address_of_showSceneObj_4() { return &___showSceneObj_4; }
	inline void set_showSceneObj_4(bool value)
	{
		___showSceneObj_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3C_LoadSceneU3Ed__53_t8BAE33712802D267371AF1C769DC15F2496BAB38, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_LOADSCENEU3ED__53_T8BAE33712802D267371AF1C769DC15F2496BAB38_H
#ifndef U3CSTARTU3ED__4_T5E6BB381169DA4E190828DD890D257D535728DFF_H
#define U3CSTARTU3ED__4_T5E6BB381169DA4E190828DD890D257D535728DFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.RootMergeComponent/<Start>d__4
struct  U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF 
{
public:
	// System.Int32 UnityGLTF.RootMergeComponent/<Start>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder UnityGLTF.RootMergeComponent/<Start>d__4::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityGLTF.RootMergeComponent UnityGLTF.RootMergeComponent/<Start>d__4::<>4__this
	RootMergeComponent_tB6DE16CB123C2A75D3C0D4818469BF644203A29B * ___U3CU3E4__this_2;
	// UnityGLTF.Loader.ILoader UnityGLTF.RootMergeComponent/<Start>d__4::<loader0>5__2
	RuntimeObject* ___U3Cloader0U3E5__2_3;
	// UnityGLTF.Loader.ILoader UnityGLTF.RootMergeComponent/<Start>d__4::<loader1>5__3
	RuntimeObject* ___U3Cloader1U3E5__3_4;
	// GLTF.Schema.GLTFRoot UnityGLTF.RootMergeComponent/<Start>d__4::<asset0Root>5__4
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___U3Casset0RootU3E5__4_5;
	// System.Runtime.CompilerServices.TaskAwaiter UnityGLTF.RootMergeComponent/<Start>d__4::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF, ___U3CU3E4__this_2)); }
	inline RootMergeComponent_tB6DE16CB123C2A75D3C0D4818469BF644203A29B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RootMergeComponent_tB6DE16CB123C2A75D3C0D4818469BF644203A29B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RootMergeComponent_tB6DE16CB123C2A75D3C0D4818469BF644203A29B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3Cloader0U3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF, ___U3Cloader0U3E5__2_3)); }
	inline RuntimeObject* get_U3Cloader0U3E5__2_3() const { return ___U3Cloader0U3E5__2_3; }
	inline RuntimeObject** get_address_of_U3Cloader0U3E5__2_3() { return &___U3Cloader0U3E5__2_3; }
	inline void set_U3Cloader0U3E5__2_3(RuntimeObject* value)
	{
		___U3Cloader0U3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cloader0U3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3Cloader1U3E5__3_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF, ___U3Cloader1U3E5__3_4)); }
	inline RuntimeObject* get_U3Cloader1U3E5__3_4() const { return ___U3Cloader1U3E5__3_4; }
	inline RuntimeObject** get_address_of_U3Cloader1U3E5__3_4() { return &___U3Cloader1U3E5__3_4; }
	inline void set_U3Cloader1U3E5__3_4(RuntimeObject* value)
	{
		___U3Cloader1U3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cloader1U3E5__3_4), value);
	}

	inline static int32_t get_offset_of_U3Casset0RootU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF, ___U3Casset0RootU3E5__4_5)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_U3Casset0RootU3E5__4_5() const { return ___U3Casset0RootU3E5__4_5; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_U3Casset0RootU3E5__4_5() { return &___U3Casset0RootU3E5__4_5; }
	inline void set_U3Casset0RootU3E5__4_5(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___U3Casset0RootU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3Casset0RootU3E5__4_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF, ___U3CU3Eu__1_6)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_6() const { return ___U3CU3Eu__1_6; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_6() { return &___U3CU3Eu__1_6; }
	inline void set_U3CU3Eu__1_6(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__4_T5E6BB381169DA4E190828DD890D257D535728DFF_H
#ifndef ACTIONTHREAD_T3DD80C24E69D827BB2DE777B7928D4A7385E3EDB_H
#define ACTIONTHREAD_T3DD80C24E69D827BB2DE777B7928D4A7385E3EDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.ActionThread
struct  ActionThread_t3DD80C24E69D827BB2DE777B7928D4A7385E3EDB  : public ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C
{
public:
	// System.Action`1<UnityThreading.ActionThread> UnityThreading.ActionThread::action
	Action_1_tE3217F0296A761BE816CF81030F2F771090C1B77 * ___action_6;

public:
	inline static int32_t get_offset_of_action_6() { return static_cast<int32_t>(offsetof(ActionThread_t3DD80C24E69D827BB2DE777B7928D4A7385E3EDB, ___action_6)); }
	inline Action_1_tE3217F0296A761BE816CF81030F2F771090C1B77 * get_action_6() const { return ___action_6; }
	inline Action_1_tE3217F0296A761BE816CF81030F2F771090C1B77 ** get_address_of_action_6() { return &___action_6; }
	inline void set_action_6(Action_1_tE3217F0296A761BE816CF81030F2F771090C1B77 * value)
	{
		___action_6 = value;
		Il2CppCodeGenWriteBarrier((&___action_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTHREAD_T3DD80C24E69D827BB2DE777B7928D4A7385E3EDB_H
#ifndef ENUMERATABLEACTIONTHREAD_T4FB977900F29D21D8467DD98A870D71DC3349624_H
#define ENUMERATABLEACTIONTHREAD_T4FB977900F29D21D8467DD98A870D71DC3349624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.EnumeratableActionThread
struct  EnumeratableActionThread_t4FB977900F29D21D8467DD98A870D71DC3349624  : public ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C
{
public:
	// System.Func`2<UnityThreading.ThreadBase,System.Collections.IEnumerator> UnityThreading.EnumeratableActionThread::enumeratableAction
	Func_2_t8ADD2DE3DB0FFA6F3FB7AAD84397C6D7A1132827 * ___enumeratableAction_6;

public:
	inline static int32_t get_offset_of_enumeratableAction_6() { return static_cast<int32_t>(offsetof(EnumeratableActionThread_t4FB977900F29D21D8467DD98A870D71DC3349624, ___enumeratableAction_6)); }
	inline Func_2_t8ADD2DE3DB0FFA6F3FB7AAD84397C6D7A1132827 * get_enumeratableAction_6() const { return ___enumeratableAction_6; }
	inline Func_2_t8ADD2DE3DB0FFA6F3FB7AAD84397C6D7A1132827 ** get_address_of_enumeratableAction_6() { return &___enumeratableAction_6; }
	inline void set_enumeratableAction_6(Func_2_t8ADD2DE3DB0FFA6F3FB7AAD84397C6D7A1132827 * value)
	{
		___enumeratableAction_6 = value;
		Il2CppCodeGenWriteBarrier((&___enumeratableAction_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATABLEACTIONTHREAD_T4FB977900F29D21D8467DD98A870D71DC3349624_H
#ifndef TASKDISTRIBUTOR_T9256280BACD12FBA2EA31607C5A7D68B84C8972D_H
#define TASKDISTRIBUTOR_T9256280BACD12FBA2EA31607C5A7D68B84C8972D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskDistributor
struct  TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D  : public DispatcherBase_tB856AF22CD8042F30BC3EA0B35779BBF104A4578
{
public:
	// UnityThreading.TaskWorker[] UnityThreading.TaskDistributor::workerThreads
	TaskWorkerU5BU5D_tF86436D11289B2B862F108DC08B7EC27F6280F5A* ___workerThreads_7;
	// System.Int32 UnityThreading.TaskDistributor::MaxAdditionalWorkerThreads
	int32_t ___MaxAdditionalWorkerThreads_9;
	// System.String UnityThreading.TaskDistributor::name
	String_t* ___name_10;
	// System.Boolean UnityThreading.TaskDistributor::isDisposed
	bool ___isDisposed_11;
	// System.Threading.ThreadPriority UnityThreading.TaskDistributor::priority
	int32_t ___priority_12;

public:
	inline static int32_t get_offset_of_workerThreads_7() { return static_cast<int32_t>(offsetof(TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D, ___workerThreads_7)); }
	inline TaskWorkerU5BU5D_tF86436D11289B2B862F108DC08B7EC27F6280F5A* get_workerThreads_7() const { return ___workerThreads_7; }
	inline TaskWorkerU5BU5D_tF86436D11289B2B862F108DC08B7EC27F6280F5A** get_address_of_workerThreads_7() { return &___workerThreads_7; }
	inline void set_workerThreads_7(TaskWorkerU5BU5D_tF86436D11289B2B862F108DC08B7EC27F6280F5A* value)
	{
		___workerThreads_7 = value;
		Il2CppCodeGenWriteBarrier((&___workerThreads_7), value);
	}

	inline static int32_t get_offset_of_MaxAdditionalWorkerThreads_9() { return static_cast<int32_t>(offsetof(TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D, ___MaxAdditionalWorkerThreads_9)); }
	inline int32_t get_MaxAdditionalWorkerThreads_9() const { return ___MaxAdditionalWorkerThreads_9; }
	inline int32_t* get_address_of_MaxAdditionalWorkerThreads_9() { return &___MaxAdditionalWorkerThreads_9; }
	inline void set_MaxAdditionalWorkerThreads_9(int32_t value)
	{
		___MaxAdditionalWorkerThreads_9 = value;
	}

	inline static int32_t get_offset_of_name_10() { return static_cast<int32_t>(offsetof(TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D, ___name_10)); }
	inline String_t* get_name_10() const { return ___name_10; }
	inline String_t** get_address_of_name_10() { return &___name_10; }
	inline void set_name_10(String_t* value)
	{
		___name_10 = value;
		Il2CppCodeGenWriteBarrier((&___name_10), value);
	}

	inline static int32_t get_offset_of_isDisposed_11() { return static_cast<int32_t>(offsetof(TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D, ___isDisposed_11)); }
	inline bool get_isDisposed_11() const { return ___isDisposed_11; }
	inline bool* get_address_of_isDisposed_11() { return &___isDisposed_11; }
	inline void set_isDisposed_11(bool value)
	{
		___isDisposed_11 = value;
	}

	inline static int32_t get_offset_of_priority_12() { return static_cast<int32_t>(offsetof(TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D, ___priority_12)); }
	inline int32_t get_priority_12() const { return ___priority_12; }
	inline int32_t* get_address_of_priority_12() { return &___priority_12; }
	inline void set_priority_12(int32_t value)
	{
		___priority_12 = value;
	}
};

struct TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D_StaticFields
{
public:
	// UnityThreading.TaskDistributor UnityThreading.TaskDistributor::mainTaskDistributor
	TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D * ___mainTaskDistributor_8;

public:
	inline static int32_t get_offset_of_mainTaskDistributor_8() { return static_cast<int32_t>(offsetof(TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D_StaticFields, ___mainTaskDistributor_8)); }
	inline TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D * get_mainTaskDistributor_8() const { return ___mainTaskDistributor_8; }
	inline TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D ** get_address_of_mainTaskDistributor_8() { return &___mainTaskDistributor_8; }
	inline void set_mainTaskDistributor_8(TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D * value)
	{
		___mainTaskDistributor_8 = value;
		Il2CppCodeGenWriteBarrier((&___mainTaskDistributor_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKDISTRIBUTOR_T9256280BACD12FBA2EA31607C5A7D68B84C8972D_H
#ifndef TASKWORKER_TCAB7E178AEA41331FEE65B59CEFF533051BBCD17_H
#define TASKWORKER_TCAB7E178AEA41331FEE65B59CEFF533051BBCD17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TaskWorker
struct  TaskWorker_tCAB7E178AEA41331FEE65B59CEFF533051BBCD17  : public ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C
{
public:
	// UnityThreading.Dispatcher UnityThreading.TaskWorker::Dispatcher
	Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 * ___Dispatcher_6;
	// UnityThreading.TaskDistributor UnityThreading.TaskWorker::<TaskDistributor>k__BackingField
	TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D * ___U3CTaskDistributorU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_Dispatcher_6() { return static_cast<int32_t>(offsetof(TaskWorker_tCAB7E178AEA41331FEE65B59CEFF533051BBCD17, ___Dispatcher_6)); }
	inline Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 * get_Dispatcher_6() const { return ___Dispatcher_6; }
	inline Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 ** get_address_of_Dispatcher_6() { return &___Dispatcher_6; }
	inline void set_Dispatcher_6(Dispatcher_tD5EE49BC8AEFD030FF77F95C8A029588E07F0297 * value)
	{
		___Dispatcher_6 = value;
		Il2CppCodeGenWriteBarrier((&___Dispatcher_6), value);
	}

	inline static int32_t get_offset_of_U3CTaskDistributorU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TaskWorker_tCAB7E178AEA41331FEE65B59CEFF533051BBCD17, ___U3CTaskDistributorU3Ek__BackingField_7)); }
	inline TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D * get_U3CTaskDistributorU3Ek__BackingField_7() const { return ___U3CTaskDistributorU3Ek__BackingField_7; }
	inline TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D ** get_address_of_U3CTaskDistributorU3Ek__BackingField_7() { return &___U3CTaskDistributorU3Ek__BackingField_7; }
	inline void set_U3CTaskDistributorU3Ek__BackingField_7(TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D * value)
	{
		___U3CTaskDistributorU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTaskDistributorU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKWORKER_TCAB7E178AEA41331FEE65B59CEFF533051BBCD17_H
#ifndef TICKTHREAD_T26C02952D001DAE8298765320926F3DC5C3C7432_H
#define TICKTHREAD_T26C02952D001DAE8298765320926F3DC5C3C7432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityThreading.TickThread
struct  TickThread_t26C02952D001DAE8298765320926F3DC5C3C7432  : public ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C
{
public:
	// System.Action UnityThreading.TickThread::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_6;
	// System.Int32 UnityThreading.TickThread::tickLengthInMilliseconds
	int32_t ___tickLengthInMilliseconds_7;
	// System.Threading.ManualResetEvent UnityThreading.TickThread::tickEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___tickEvent_8;

public:
	inline static int32_t get_offset_of_action_6() { return static_cast<int32_t>(offsetof(TickThread_t26C02952D001DAE8298765320926F3DC5C3C7432, ___action_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_6() const { return ___action_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_6() { return &___action_6; }
	inline void set_action_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_6 = value;
		Il2CppCodeGenWriteBarrier((&___action_6), value);
	}

	inline static int32_t get_offset_of_tickLengthInMilliseconds_7() { return static_cast<int32_t>(offsetof(TickThread_t26C02952D001DAE8298765320926F3DC5C3C7432, ___tickLengthInMilliseconds_7)); }
	inline int32_t get_tickLengthInMilliseconds_7() const { return ___tickLengthInMilliseconds_7; }
	inline int32_t* get_address_of_tickLengthInMilliseconds_7() { return &___tickLengthInMilliseconds_7; }
	inline void set_tickLengthInMilliseconds_7(int32_t value)
	{
		___tickLengthInMilliseconds_7 = value;
	}

	inline static int32_t get_offset_of_tickEvent_8() { return static_cast<int32_t>(offsetof(TickThread_t26C02952D001DAE8298765320926F3DC5C3C7432, ___tickEvent_8)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_tickEvent_8() const { return ___tickEvent_8; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_tickEvent_8() { return &___tickEvent_8; }
	inline void set_tickEvent_8(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___tickEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&___tickEvent_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TICKTHREAD_T26C02952D001DAE8298765320926F3DC5C3C7432_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef SINGLETON_1_T0538B159A5071E794F4630AB2BF4A43415710E13_H
#define SINGLETON_1_T0538B159A5071E794F4630AB2BF4A43415710E13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Patch.Singleton`1<MHLab.PATCH.Settings.Settings>
struct  Singleton_1_t0538B159A5071E794F4630AB2BF4A43415710E13  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t0538B159A5071E794F4630AB2BF4A43415710E13_StaticFields
{
public:
	// T Patch.Singleton`1::_instance
	Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1 * ____instance_4;
	// System.Object Patch.Singleton`1::_lock
	RuntimeObject * ____lock_5;
	// System.Boolean Patch.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_6;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_t0538B159A5071E794F4630AB2BF4A43415710E13_StaticFields, ____instance_4)); }
	inline Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1 * get__instance_4() const { return ____instance_4; }
	inline Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((&____instance_4), value);
	}

	inline static int32_t get_offset_of__lock_5() { return static_cast<int32_t>(offsetof(Singleton_1_t0538B159A5071E794F4630AB2BF4A43415710E13_StaticFields, ____lock_5)); }
	inline RuntimeObject * get__lock_5() const { return ____lock_5; }
	inline RuntimeObject ** get_address_of__lock_5() { return &____lock_5; }
	inline void set__lock_5(RuntimeObject * value)
	{
		____lock_5 = value;
		Il2CppCodeGenWriteBarrier((&____lock_5), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_6() { return static_cast<int32_t>(offsetof(Singleton_1_t0538B159A5071E794F4630AB2BF4A43415710E13_StaticFields, ___applicationIsQuitting_6)); }
	inline bool get_applicationIsQuitting_6() const { return ___applicationIsQuitting_6; }
	inline bool* get_address_of_applicationIsQuitting_6() { return &___applicationIsQuitting_6; }
	inline void set_applicationIsQuitting_6(bool value)
	{
		___applicationIsQuitting_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T0538B159A5071E794F4630AB2BF4A43415710E13_H
#ifndef ASYNCCOROUTINEHELPER_TC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1_H
#define ASYNCCOROUTINEHELPER_TC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.AsyncCoroutineHelper
struct  AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.Queue`1<UnityGLTF.AsyncCoroutineHelper/CoroutineInfo> UnityGLTF.AsyncCoroutineHelper::_actions
	Queue_1_tBD9DB1D533A027BD4D96A9F03A0DCC1D51A45497 * ____actions_4;

public:
	inline static int32_t get_offset_of__actions_4() { return static_cast<int32_t>(offsetof(AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1, ____actions_4)); }
	inline Queue_1_tBD9DB1D533A027BD4D96A9F03A0DCC1D51A45497 * get__actions_4() const { return ____actions_4; }
	inline Queue_1_tBD9DB1D533A027BD4D96A9F03A0DCC1D51A45497 ** get_address_of__actions_4() { return &____actions_4; }
	inline void set__actions_4(Queue_1_tBD9DB1D533A027BD4D96A9F03A0DCC1D51A45497 * value)
	{
		____actions_4 = value;
		Il2CppCodeGenWriteBarrier((&____actions_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCOROUTINEHELPER_TC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1_H
#ifndef GLTFCOMPONENT_T6CC2776C793501FA3D7ED2191E9C5F1CD7B36392_H
#define GLTFCOMPONENT_T6CC2776C793501FA3D7ED2191E9C5F1CD7B36392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFComponent
struct  GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String UnityGLTF.GLTFComponent::GLTFUri
	String_t* ___GLTFUri_4;
	// System.Boolean UnityGLTF.GLTFComponent::Multithreaded
	bool ___Multithreaded_5;
	// System.Boolean UnityGLTF.GLTFComponent::UseStream
	bool ___UseStream_6;
	// System.Boolean UnityGLTF.GLTFComponent::AppendStreamingAssets
	bool ___AppendStreamingAssets_7;
	// System.Boolean UnityGLTF.GLTFComponent::PlayAnimationOnLoad
	bool ___PlayAnimationOnLoad_8;
	// System.Boolean UnityGLTF.GLTFComponent::<Loaded>k__BackingField
	bool ___U3CLoadedU3Ek__BackingField_9;
	// System.Boolean UnityGLTF.GLTFComponent::loadOnStart
	bool ___loadOnStart_10;
	// System.Boolean UnityGLTF.GLTFComponent::MaterialsOnly
	bool ___MaterialsOnly_11;
	// System.Int32 UnityGLTF.GLTFComponent::RetryCount
	int32_t ___RetryCount_12;
	// System.Single UnityGLTF.GLTFComponent::RetryTimeout
	float ___RetryTimeout_13;
	// System.Int32 UnityGLTF.GLTFComponent::numRetries
	int32_t ___numRetries_14;
	// System.Int32 UnityGLTF.GLTFComponent::MaximumLod
	int32_t ___MaximumLod_15;
	// System.Int32 UnityGLTF.GLTFComponent::Timeout
	int32_t ___Timeout_16;
	// UnityGLTF.GLTFSceneImporter/ColliderType UnityGLTF.GLTFComponent::Collider
	int32_t ___Collider_17;
	// UnityGLTF.AsyncCoroutineHelper UnityGLTF.GLTFComponent::asyncCoroutineHelper
	AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1 * ___asyncCoroutineHelper_18;
	// UnityEngine.Shader UnityGLTF.GLTFComponent::shaderOverride
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___shaderOverride_19;

public:
	inline static int32_t get_offset_of_GLTFUri_4() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___GLTFUri_4)); }
	inline String_t* get_GLTFUri_4() const { return ___GLTFUri_4; }
	inline String_t** get_address_of_GLTFUri_4() { return &___GLTFUri_4; }
	inline void set_GLTFUri_4(String_t* value)
	{
		___GLTFUri_4 = value;
		Il2CppCodeGenWriteBarrier((&___GLTFUri_4), value);
	}

	inline static int32_t get_offset_of_Multithreaded_5() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___Multithreaded_5)); }
	inline bool get_Multithreaded_5() const { return ___Multithreaded_5; }
	inline bool* get_address_of_Multithreaded_5() { return &___Multithreaded_5; }
	inline void set_Multithreaded_5(bool value)
	{
		___Multithreaded_5 = value;
	}

	inline static int32_t get_offset_of_UseStream_6() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___UseStream_6)); }
	inline bool get_UseStream_6() const { return ___UseStream_6; }
	inline bool* get_address_of_UseStream_6() { return &___UseStream_6; }
	inline void set_UseStream_6(bool value)
	{
		___UseStream_6 = value;
	}

	inline static int32_t get_offset_of_AppendStreamingAssets_7() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___AppendStreamingAssets_7)); }
	inline bool get_AppendStreamingAssets_7() const { return ___AppendStreamingAssets_7; }
	inline bool* get_address_of_AppendStreamingAssets_7() { return &___AppendStreamingAssets_7; }
	inline void set_AppendStreamingAssets_7(bool value)
	{
		___AppendStreamingAssets_7 = value;
	}

	inline static int32_t get_offset_of_PlayAnimationOnLoad_8() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___PlayAnimationOnLoad_8)); }
	inline bool get_PlayAnimationOnLoad_8() const { return ___PlayAnimationOnLoad_8; }
	inline bool* get_address_of_PlayAnimationOnLoad_8() { return &___PlayAnimationOnLoad_8; }
	inline void set_PlayAnimationOnLoad_8(bool value)
	{
		___PlayAnimationOnLoad_8 = value;
	}

	inline static int32_t get_offset_of_U3CLoadedU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___U3CLoadedU3Ek__BackingField_9)); }
	inline bool get_U3CLoadedU3Ek__BackingField_9() const { return ___U3CLoadedU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CLoadedU3Ek__BackingField_9() { return &___U3CLoadedU3Ek__BackingField_9; }
	inline void set_U3CLoadedU3Ek__BackingField_9(bool value)
	{
		___U3CLoadedU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_loadOnStart_10() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___loadOnStart_10)); }
	inline bool get_loadOnStart_10() const { return ___loadOnStart_10; }
	inline bool* get_address_of_loadOnStart_10() { return &___loadOnStart_10; }
	inline void set_loadOnStart_10(bool value)
	{
		___loadOnStart_10 = value;
	}

	inline static int32_t get_offset_of_MaterialsOnly_11() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___MaterialsOnly_11)); }
	inline bool get_MaterialsOnly_11() const { return ___MaterialsOnly_11; }
	inline bool* get_address_of_MaterialsOnly_11() { return &___MaterialsOnly_11; }
	inline void set_MaterialsOnly_11(bool value)
	{
		___MaterialsOnly_11 = value;
	}

	inline static int32_t get_offset_of_RetryCount_12() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___RetryCount_12)); }
	inline int32_t get_RetryCount_12() const { return ___RetryCount_12; }
	inline int32_t* get_address_of_RetryCount_12() { return &___RetryCount_12; }
	inline void set_RetryCount_12(int32_t value)
	{
		___RetryCount_12 = value;
	}

	inline static int32_t get_offset_of_RetryTimeout_13() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___RetryTimeout_13)); }
	inline float get_RetryTimeout_13() const { return ___RetryTimeout_13; }
	inline float* get_address_of_RetryTimeout_13() { return &___RetryTimeout_13; }
	inline void set_RetryTimeout_13(float value)
	{
		___RetryTimeout_13 = value;
	}

	inline static int32_t get_offset_of_numRetries_14() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___numRetries_14)); }
	inline int32_t get_numRetries_14() const { return ___numRetries_14; }
	inline int32_t* get_address_of_numRetries_14() { return &___numRetries_14; }
	inline void set_numRetries_14(int32_t value)
	{
		___numRetries_14 = value;
	}

	inline static int32_t get_offset_of_MaximumLod_15() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___MaximumLod_15)); }
	inline int32_t get_MaximumLod_15() const { return ___MaximumLod_15; }
	inline int32_t* get_address_of_MaximumLod_15() { return &___MaximumLod_15; }
	inline void set_MaximumLod_15(int32_t value)
	{
		___MaximumLod_15 = value;
	}

	inline static int32_t get_offset_of_Timeout_16() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___Timeout_16)); }
	inline int32_t get_Timeout_16() const { return ___Timeout_16; }
	inline int32_t* get_address_of_Timeout_16() { return &___Timeout_16; }
	inline void set_Timeout_16(int32_t value)
	{
		___Timeout_16 = value;
	}

	inline static int32_t get_offset_of_Collider_17() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___Collider_17)); }
	inline int32_t get_Collider_17() const { return ___Collider_17; }
	inline int32_t* get_address_of_Collider_17() { return &___Collider_17; }
	inline void set_Collider_17(int32_t value)
	{
		___Collider_17 = value;
	}

	inline static int32_t get_offset_of_asyncCoroutineHelper_18() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___asyncCoroutineHelper_18)); }
	inline AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1 * get_asyncCoroutineHelper_18() const { return ___asyncCoroutineHelper_18; }
	inline AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1 ** get_address_of_asyncCoroutineHelper_18() { return &___asyncCoroutineHelper_18; }
	inline void set_asyncCoroutineHelper_18(AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1 * value)
	{
		___asyncCoroutineHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___asyncCoroutineHelper_18), value);
	}

	inline static int32_t get_offset_of_shaderOverride_19() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___shaderOverride_19)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_shaderOverride_19() const { return ___shaderOverride_19; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_shaderOverride_19() { return &___shaderOverride_19; }
	inline void set_shaderOverride_19(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___shaderOverride_19 = value;
		Il2CppCodeGenWriteBarrier((&___shaderOverride_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFCOMPONENT_T6CC2776C793501FA3D7ED2191E9C5F1CD7B36392_H
#ifndef ROOTMERGECOMPONENT_TB6DE16CB123C2A75D3C0D4818469BF644203A29B_H
#define ROOTMERGECOMPONENT_TB6DE16CB123C2A75D3C0D4818469BF644203A29B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.RootMergeComponent
struct  RootMergeComponent_tB6DE16CB123C2A75D3C0D4818469BF644203A29B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String UnityGLTF.RootMergeComponent::asset0Path
	String_t* ___asset0Path_4;
	// System.String UnityGLTF.RootMergeComponent::asset1Path
	String_t* ___asset1Path_5;
	// System.Boolean UnityGLTF.RootMergeComponent::Multithreaded
	bool ___Multithreaded_6;
	// System.Int32 UnityGLTF.RootMergeComponent::MaximumLod
	int32_t ___MaximumLod_7;

public:
	inline static int32_t get_offset_of_asset0Path_4() { return static_cast<int32_t>(offsetof(RootMergeComponent_tB6DE16CB123C2A75D3C0D4818469BF644203A29B, ___asset0Path_4)); }
	inline String_t* get_asset0Path_4() const { return ___asset0Path_4; }
	inline String_t** get_address_of_asset0Path_4() { return &___asset0Path_4; }
	inline void set_asset0Path_4(String_t* value)
	{
		___asset0Path_4 = value;
		Il2CppCodeGenWriteBarrier((&___asset0Path_4), value);
	}

	inline static int32_t get_offset_of_asset1Path_5() { return static_cast<int32_t>(offsetof(RootMergeComponent_tB6DE16CB123C2A75D3C0D4818469BF644203A29B, ___asset1Path_5)); }
	inline String_t* get_asset1Path_5() const { return ___asset1Path_5; }
	inline String_t** get_address_of_asset1Path_5() { return &___asset1Path_5; }
	inline void set_asset1Path_5(String_t* value)
	{
		___asset1Path_5 = value;
		Il2CppCodeGenWriteBarrier((&___asset1Path_5), value);
	}

	inline static int32_t get_offset_of_Multithreaded_6() { return static_cast<int32_t>(offsetof(RootMergeComponent_tB6DE16CB123C2A75D3C0D4818469BF644203A29B, ___Multithreaded_6)); }
	inline bool get_Multithreaded_6() const { return ___Multithreaded_6; }
	inline bool* get_address_of_Multithreaded_6() { return &___Multithreaded_6; }
	inline void set_Multithreaded_6(bool value)
	{
		___Multithreaded_6 = value;
	}

	inline static int32_t get_offset_of_MaximumLod_7() { return static_cast<int32_t>(offsetof(RootMergeComponent_tB6DE16CB123C2A75D3C0D4818469BF644203A29B, ___MaximumLod_7)); }
	inline int32_t get_MaximumLod_7() const { return ___MaximumLod_7; }
	inline int32_t* get_address_of_MaximumLod_7() { return &___MaximumLod_7; }
	inline void set_MaximumLod_7(int32_t value)
	{
		___MaximumLod_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOTMERGECOMPONENT_TB6DE16CB123C2A75D3C0D4818469BF644203A29B_H
#ifndef SETTINGS_T2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_H
#define SETTINGS_T2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Settings.Settings
struct  Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1  : public Singleton_1_t0538B159A5071E794F4630AB2BF4A43415710E13
{
public:

public:
};

struct Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_StaticFields
{
public:
	// System.String MHLab.PATCH.Settings.Settings::APP_PATH
	String_t* ___APP_PATH_7;
	// System.String MHLab.PATCH.Settings.Settings::ASSETS_PATH
	String_t* ___ASSETS_PATH_8;
	// System.String MHLab.PATCH.Settings.Settings::LANGUAGE_PATH
	String_t* ___LANGUAGE_PATH_9;
	// System.String MHLab.PATCH.Settings.Settings::SAVING_LANGUAGE_PATH
	String_t* ___SAVING_LANGUAGE_PATH_10;
	// System.String MHLab.PATCH.Settings.Settings::LANGUAGE_EXTENSION
	String_t* ___LANGUAGE_EXTENSION_11;
	// System.String MHLab.PATCH.Settings.Settings::LANGUAGE_DEFAULT
	String_t* ___LANGUAGE_DEFAULT_12;

public:
	inline static int32_t get_offset_of_APP_PATH_7() { return static_cast<int32_t>(offsetof(Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_StaticFields, ___APP_PATH_7)); }
	inline String_t* get_APP_PATH_7() const { return ___APP_PATH_7; }
	inline String_t** get_address_of_APP_PATH_7() { return &___APP_PATH_7; }
	inline void set_APP_PATH_7(String_t* value)
	{
		___APP_PATH_7 = value;
		Il2CppCodeGenWriteBarrier((&___APP_PATH_7), value);
	}

	inline static int32_t get_offset_of_ASSETS_PATH_8() { return static_cast<int32_t>(offsetof(Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_StaticFields, ___ASSETS_PATH_8)); }
	inline String_t* get_ASSETS_PATH_8() const { return ___ASSETS_PATH_8; }
	inline String_t** get_address_of_ASSETS_PATH_8() { return &___ASSETS_PATH_8; }
	inline void set_ASSETS_PATH_8(String_t* value)
	{
		___ASSETS_PATH_8 = value;
		Il2CppCodeGenWriteBarrier((&___ASSETS_PATH_8), value);
	}

	inline static int32_t get_offset_of_LANGUAGE_PATH_9() { return static_cast<int32_t>(offsetof(Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_StaticFields, ___LANGUAGE_PATH_9)); }
	inline String_t* get_LANGUAGE_PATH_9() const { return ___LANGUAGE_PATH_9; }
	inline String_t** get_address_of_LANGUAGE_PATH_9() { return &___LANGUAGE_PATH_9; }
	inline void set_LANGUAGE_PATH_9(String_t* value)
	{
		___LANGUAGE_PATH_9 = value;
		Il2CppCodeGenWriteBarrier((&___LANGUAGE_PATH_9), value);
	}

	inline static int32_t get_offset_of_SAVING_LANGUAGE_PATH_10() { return static_cast<int32_t>(offsetof(Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_StaticFields, ___SAVING_LANGUAGE_PATH_10)); }
	inline String_t* get_SAVING_LANGUAGE_PATH_10() const { return ___SAVING_LANGUAGE_PATH_10; }
	inline String_t** get_address_of_SAVING_LANGUAGE_PATH_10() { return &___SAVING_LANGUAGE_PATH_10; }
	inline void set_SAVING_LANGUAGE_PATH_10(String_t* value)
	{
		___SAVING_LANGUAGE_PATH_10 = value;
		Il2CppCodeGenWriteBarrier((&___SAVING_LANGUAGE_PATH_10), value);
	}

	inline static int32_t get_offset_of_LANGUAGE_EXTENSION_11() { return static_cast<int32_t>(offsetof(Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_StaticFields, ___LANGUAGE_EXTENSION_11)); }
	inline String_t* get_LANGUAGE_EXTENSION_11() const { return ___LANGUAGE_EXTENSION_11; }
	inline String_t** get_address_of_LANGUAGE_EXTENSION_11() { return &___LANGUAGE_EXTENSION_11; }
	inline void set_LANGUAGE_EXTENSION_11(String_t* value)
	{
		___LANGUAGE_EXTENSION_11 = value;
		Il2CppCodeGenWriteBarrier((&___LANGUAGE_EXTENSION_11), value);
	}

	inline static int32_t get_offset_of_LANGUAGE_DEFAULT_12() { return static_cast<int32_t>(offsetof(Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_StaticFields, ___LANGUAGE_DEFAULT_12)); }
	inline String_t* get_LANGUAGE_DEFAULT_12() const { return ___LANGUAGE_DEFAULT_12; }
	inline String_t** get_address_of_LANGUAGE_DEFAULT_12() { return &___LANGUAGE_DEFAULT_12; }
	inline void set_LANGUAGE_DEFAULT_12(String_t* value)
	{
		___LANGUAGE_DEFAULT_12 = value;
		Il2CppCodeGenWriteBarrier((&___LANGUAGE_DEFAULT_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7700 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7700[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7701 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7701[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7702 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7702[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7703 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7703[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7704 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7704[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7705 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7705[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7706 = { sizeof (TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D), -1, sizeof(TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7706[6] = 
{
	TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D::get_offset_of_workerThreads_7(),
	TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D_StaticFields::get_offset_of_mainTaskDistributor_8(),
	TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D::get_offset_of_MaxAdditionalWorkerThreads_9(),
	TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D::get_offset_of_name_10(),
	TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D::get_offset_of_isDisposed_11(),
	TaskDistributor_t9256280BACD12FBA2EA31607C5A7D68B84C8972D::get_offset_of_priority_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7707 = { sizeof (U3CU3Ec_tB31F53CFFD1216D13357FD67DA8DA5C2416B9474), -1, sizeof(U3CU3Ec_tB31F53CFFD1216D13357FD67DA8DA5C2416B9474_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7707[2] = 
{
	U3CU3Ec_tB31F53CFFD1216D13357FD67DA8DA5C2416B9474_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tB31F53CFFD1216D13357FD67DA8DA5C2416B9474_StaticFields::get_offset_of_U3CU3E9__19_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7708 = { sizeof (TaskWorker_tCAB7E178AEA41331FEE65B59CEFF533051BBCD17), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7708[2] = 
{
	TaskWorker_tCAB7E178AEA41331FEE65B59CEFF533051BBCD17::get_offset_of_Dispatcher_6(),
	TaskWorker_tCAB7E178AEA41331FEE65B59CEFF533051BBCD17::get_offset_of_U3CTaskDistributorU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7709 = { sizeof (TaskExtension_t7EBF7CA29E1A67538AA20BE1D66BC1606BA8CFAD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7710 = { sizeof (U3CU3Ec__DisplayClass3_0_t12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7710[6] = 
{
	U3CU3Ec__DisplayClass3_0_t12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F::get_offset_of_followingTask_0(),
	U3CU3Ec__DisplayClass3_0_t12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F::get_offset_of_syncRoot_1(),
	U3CU3Ec__DisplayClass3_0_t12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F::get_offset_of_remaining_2(),
	U3CU3Ec__DisplayClass3_0_t12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F::get_offset_of_target_3(),
	U3CU3Ec__DisplayClass3_0_t12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F::get_offset_of_U3CU3E9__0_4(),
	U3CU3Ec__DisplayClass3_0_t12419C9A3BA7906AE0FAFB3D657EE5A06F5DA09F::get_offset_of_U3CU3E9__1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7711 = { sizeof (U3CU3Ec__DisplayClass4_0_t12AADA9F3D9F3CAE598D79656D801DC78A4D5820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7711[6] = 
{
	U3CU3Ec__DisplayClass4_0_t12AADA9F3D9F3CAE598D79656D801DC78A4D5820::get_offset_of_syncRoot_0(),
	U3CU3Ec__DisplayClass4_0_t12AADA9F3D9F3CAE598D79656D801DC78A4D5820::get_offset_of_remaining_1(),
	U3CU3Ec__DisplayClass4_0_t12AADA9F3D9F3CAE598D79656D801DC78A4D5820::get_offset_of_target_2(),
	U3CU3Ec__DisplayClass4_0_t12AADA9F3D9F3CAE598D79656D801DC78A4D5820::get_offset_of_action_3(),
	U3CU3Ec__DisplayClass4_0_t12AADA9F3D9F3CAE598D79656D801DC78A4D5820::get_offset_of_U3CU3E9__1_4(),
	U3CU3Ec__DisplayClass4_0_t12AADA9F3D9F3CAE598D79656D801DC78A4D5820::get_offset_of_U3CU3E9__0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7712 = { sizeof (U3CU3Ec__DisplayClass5_0_t110C47148758A50F9343802B5C390A7F4EB42FAF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7712[6] = 
{
	U3CU3Ec__DisplayClass5_0_t110C47148758A50F9343802B5C390A7F4EB42FAF::get_offset_of_syncRoot_0(),
	U3CU3Ec__DisplayClass5_0_t110C47148758A50F9343802B5C390A7F4EB42FAF::get_offset_of_hasFailed_1(),
	U3CU3Ec__DisplayClass5_0_t110C47148758A50F9343802B5C390A7F4EB42FAF::get_offset_of_target_2(),
	U3CU3Ec__DisplayClass5_0_t110C47148758A50F9343802B5C390A7F4EB42FAF::get_offset_of_action_3(),
	U3CU3Ec__DisplayClass5_0_t110C47148758A50F9343802B5C390A7F4EB42FAF::get_offset_of_U3CU3E9__1_4(),
	U3CU3Ec__DisplayClass5_0_t110C47148758A50F9343802B5C390A7F4EB42FAF::get_offset_of_U3CU3E9__0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7713 = { sizeof (U3CU3Ec__DisplayClass7_0_tFDDEA51B12645C88D91ABC3998F4105672629C7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7713[1] = 
{
	U3CU3Ec__DisplayClass7_0_tFDDEA51B12645C88D91ABC3998F4105672629C7F::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7714 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7714[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7715 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7715[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7716 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7716[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7717 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7717[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7718 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7718[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7719 = { sizeof (U3CU3Ec__DisplayClass15_0_t8FDC5845942E67DB6886C426627DE013628CE024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7719[1] = 
{
	U3CU3Ec__DisplayClass15_0_t8FDC5845942E67DB6886C426627DE013628CE024::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7720 = { sizeof (U3CU3Ec__DisplayClass16_0_tADCA51721C9B0D421A8CAF0ED66340096A9AC3B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7720[1] = 
{
	U3CU3Ec__DisplayClass16_0_tADCA51721C9B0D421A8CAF0ED66340096A9AC3B7::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7721 = { sizeof (U3CU3Ec__DisplayClass17_0_t07C559EC67E78265374C7C87DB093CB50CD821DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7721[3] = 
{
	U3CU3Ec__DisplayClass17_0_t07C559EC67E78265374C7C87DB093CB50CD821DE::get_offset_of_actiontargetTarget_0(),
	U3CU3Ec__DisplayClass17_0_t07C559EC67E78265374C7C87DB093CB50CD821DE::get_offset_of_action_1(),
	U3CU3Ec__DisplayClass17_0_t07C559EC67E78265374C7C87DB093CB50CD821DE::get_offset_of_perform_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7722 = { sizeof (U3CU3Ec__DisplayClass17_1_t088DEDE8920DA139F08C4A05C21D327D0514363B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7722[2] = 
{
	U3CU3Ec__DisplayClass17_1_t088DEDE8920DA139F08C4A05C21D327D0514363B::get_offset_of_t_0(),
	U3CU3Ec__DisplayClass17_1_t088DEDE8920DA139F08C4A05C21D327D0514363B::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7723 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7723[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7724 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7724[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7725 = { sizeof (U3CU3Ec__DisplayClass21_0_tECDE733BFE4D0A45FCDF55A3A8C7F7DB7CD4D03B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7725[1] = 
{
	U3CU3Ec__DisplayClass21_0_tECDE733BFE4D0A45FCDF55A3A8C7F7DB7CD4D03B::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7726 = { sizeof (U3CU3Ec__DisplayClass22_0_t89D194F62ACA4EF1F59F25C6326F094B023772EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7726[1] = 
{
	U3CU3Ec__DisplayClass22_0_t89D194F62ACA4EF1F59F25C6326F094B023772EC::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7727 = { sizeof (U3CU3Ec__DisplayClass23_0_t3C160A91601A49C38F9476CD26D91D7FEFD88A09), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7727[1] = 
{
	U3CU3Ec__DisplayClass23_0_t3C160A91601A49C38F9476CD26D91D7FEFD88A09::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7728 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7728[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7729 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7729[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7730 = { sizeof (U3CU3Ec__DisplayClass27_0_t07C50968469A6F475DFBD94E8BF2F6C8B6E66617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7730[1] = 
{
	U3CU3Ec__DisplayClass27_0_t07C50968469A6F475DFBD94E8BF2F6C8B6E66617::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7731 = { sizeof (U3CU3Ec__DisplayClass28_0_t972C520BDE10FAC30F0728ACE7893B80057937D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7731[1] = 
{
	U3CU3Ec__DisplayClass28_0_t972C520BDE10FAC30F0728ACE7893B80057937D2::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7732 = { sizeof (U3CU3Ec__DisplayClass29_0_t1CC6B9E2B18FBD0E6FC710C23638BEDF73A1AF7A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7732[4] = 
{
	U3CU3Ec__DisplayClass29_0_t1CC6B9E2B18FBD0E6FC710C23638BEDF73A1AF7A::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass29_0_t1CC6B9E2B18FBD0E6FC710C23638BEDF73A1AF7A::get_offset_of_action_1(),
	U3CU3Ec__DisplayClass29_0_t1CC6B9E2B18FBD0E6FC710C23638BEDF73A1AF7A::get_offset_of_task_2(),
	U3CU3Ec__DisplayClass29_0_t1CC6B9E2B18FBD0E6FC710C23638BEDF73A1AF7A::get_offset_of_U3CU3E9__1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7733 = { sizeof (U3CU3Ec__DisplayClass31_0_t5864AE0B7C851F948BA6606C73A66949413F9885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7733[2] = 
{
	U3CU3Ec__DisplayClass31_0_t5864AE0B7C851F948BA6606C73A66949413F9885::get_offset_of_followingTask_0(),
	U3CU3Ec__DisplayClass31_0_t5864AE0B7C851F948BA6606C73A66949413F9885::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7734 = { sizeof (U3CU3Ec__DisplayClass35_0_tB5F2CA1C9C8C0675900287461BB59C003D230D7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7734[1] = 
{
	U3CU3Ec__DisplayClass35_0_tB5F2CA1C9C8C0675900287461BB59C003D230D7F::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7735 = { sizeof (U3CU3Ec__DisplayClass36_0_tB5A08878EA856CCFB351300F02D1068BCBBBC2E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7735[4] = 
{
	U3CU3Ec__DisplayClass36_0_tB5A08878EA856CCFB351300F02D1068BCBBBC2E5::get_offset_of_syncRoot_0(),
	U3CU3Ec__DisplayClass36_0_tB5A08878EA856CCFB351300F02D1068BCBBBC2E5::get_offset_of_done_1(),
	U3CU3Ec__DisplayClass36_0_tB5A08878EA856CCFB351300F02D1068BCBBBC2E5::get_offset_of_action_2(),
	U3CU3Ec__DisplayClass36_0_tB5A08878EA856CCFB351300F02D1068BCBBBC2E5::get_offset_of_U3CU3E9__0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7736 = { sizeof (U3CU3Ec__DisplayClass37_0_t86797DACC849C07493E8CE30EB565FDDD6178B63), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7736[1] = 
{
	U3CU3Ec__DisplayClass37_0_t86797DACC849C07493E8CE30EB565FDDD6178B63::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7737 = { sizeof (U3CU3Ec__DisplayClass38_0_t955423008B58DF24EFBD1C23E7B318A22B15107F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7737[4] = 
{
	U3CU3Ec__DisplayClass38_0_t955423008B58DF24EFBD1C23E7B318A22B15107F::get_offset_of_syncRoot_0(),
	U3CU3Ec__DisplayClass38_0_t955423008B58DF24EFBD1C23E7B318A22B15107F::get_offset_of_finishedTasks_1(),
	U3CU3Ec__DisplayClass38_0_t955423008B58DF24EFBD1C23E7B318A22B15107F::get_offset_of_count_2(),
	U3CU3Ec__DisplayClass38_0_t955423008B58DF24EFBD1C23E7B318A22B15107F::get_offset_of_action_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7738 = { sizeof (U3CU3Ec__DisplayClass38_1_t87B6536086105EBCAE0D6908E1771234B1A95087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7738[2] = 
{
	U3CU3Ec__DisplayClass38_1_t87B6536086105EBCAE0D6908E1771234B1A95087::get_offset_of_task_0(),
	U3CU3Ec__DisplayClass38_1_t87B6536086105EBCAE0D6908E1771234B1A95087::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7739 = { sizeof (ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C), -1, 0, sizeof(ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable7739[6] = 
{
	ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C::get_offset_of_targetDispatcher_0(),
	ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C::get_offset_of_thread_1(),
	ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C::get_offset_of_exitEvent_2(),
	ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C_ThreadStaticFields::get_offset_of_currentThread_3() | THREAD_LOCAL_STATIC_MASK,
	ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C::get_offset_of_threadName_4(),
	ThreadBase_t1E3BCFCF2AF91355F6FE1E8CE70CFFD1CBC7EF1C::get_offset_of_priority_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7740 = { sizeof (U3CU3Ec__DisplayClass31_0_t7D658482E791302EA4C64C8B8FDC8581469477D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7740[3] = 
{
	U3CU3Ec__DisplayClass31_0_t7D658482E791302EA4C64C8B8FDC8581469477D8::get_offset_of_enumerator_0(),
	U3CU3Ec__DisplayClass31_0_t7D658482E791302EA4C64C8B8FDC8581469477D8::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass31_0_t7D658482E791302EA4C64C8B8FDC8581469477D8::get_offset_of_U3CU3E9__0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7741 = { sizeof (ActionThread_t3DD80C24E69D827BB2DE777B7928D4A7385E3EDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7741[1] = 
{
	ActionThread_t3DD80C24E69D827BB2DE777B7928D4A7385E3EDB::get_offset_of_action_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7742 = { sizeof (EnumeratableActionThread_t4FB977900F29D21D8467DD98A870D71DC3349624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7742[1] = 
{
	EnumeratableActionThread_t4FB977900F29D21D8467DD98A870D71DC3349624::get_offset_of_enumeratableAction_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7743 = { sizeof (TickThread_t26C02952D001DAE8298765320926F3DC5C3C7432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7743[3] = 
{
	TickThread_t26C02952D001DAE8298765320926F3DC5C3C7432::get_offset_of_action_6(),
	TickThread_t26C02952D001DAE8298765320926F3DC5C3C7432::get_offset_of_tickLengthInMilliseconds_7(),
	TickThread_t26C02952D001DAE8298765320926F3DC5C3C7432::get_offset_of_tickEvent_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7744 = { sizeof (WaitOneExtension_t3CD316F6F519DB727390A1339BE54412A2E57961), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7745 = { sizeof (Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1), -1, sizeof(Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7745[6] = 
{
	Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_StaticFields::get_offset_of_APP_PATH_7(),
	Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_StaticFields::get_offset_of_ASSETS_PATH_8(),
	Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_StaticFields::get_offset_of_LANGUAGE_PATH_9(),
	Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_StaticFields::get_offset_of_SAVING_LANGUAGE_PATH_10(),
	Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_StaticFields::get_offset_of_LANGUAGE_EXTENSION_11(),
	Settings_t2638DD0A7F54DFF8DAD5B65354E9CA3F3E3C2DC1_StaticFields::get_offset_of_LANGUAGE_DEFAULT_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7746 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7746[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7747 = { sizeof (RootMergeComponent_tB6DE16CB123C2A75D3C0D4818469BF644203A29B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7747[4] = 
{
	RootMergeComponent_tB6DE16CB123C2A75D3C0D4818469BF644203A29B::get_offset_of_asset0Path_4(),
	RootMergeComponent_tB6DE16CB123C2A75D3C0D4818469BF644203A29B::get_offset_of_asset1Path_5(),
	RootMergeComponent_tB6DE16CB123C2A75D3C0D4818469BF644203A29B::get_offset_of_Multithreaded_6(),
	RootMergeComponent_tB6DE16CB123C2A75D3C0D4818469BF644203A29B::get_offset_of_MaximumLod_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7748 = { sizeof (U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7748[7] = 
{
	U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF::get_offset_of_U3Cloader0U3E5__2_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF::get_offset_of_U3Cloader1U3E5__3_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF::get_offset_of_U3Casset0RootU3E5__4_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartU3Ed__4_t5E6BB381169DA4E190828DD890D257D535728DFF::get_offset_of_U3CU3Eu__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7749 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7750 = { sizeof (AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7750[1] = 
{
	AsyncCoroutineHelper_tC44564CF83A72D7E0B403BDB05C373F6CBB6D2F1::get_offset_of__actions_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7751 = { sizeof (CoroutineInfo_tE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7751[3] = 
{
	CoroutineInfo_tE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE::get_offset_of_Coroutine_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CoroutineInfo_tE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE::get_offset_of_Tcs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CoroutineInfo_tE8A0F721C7177981AAFF76D313EA9B7C3E0F5CFE::get_offset_of_Name_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7752 = { sizeof (U3CCallMethodOnMainThreadU3Ed__2_t3A63AC9BEB4E68967F37A617F435D3F9FE40F06C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7752[3] = 
{
	U3CCallMethodOnMainThreadU3Ed__2_t3A63AC9BEB4E68967F37A617F435D3F9FE40F06C::get_offset_of_U3CU3E1__state_0(),
	U3CCallMethodOnMainThreadU3Ed__2_t3A63AC9BEB4E68967F37A617F435D3F9FE40F06C::get_offset_of_U3CU3E2__current_1(),
	U3CCallMethodOnMainThreadU3Ed__2_t3A63AC9BEB4E68967F37A617F435D3F9FE40F06C::get_offset_of_coroutineInfo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7753 = { sizeof (TaskExtensions_t9CB30AA030C26D3A036B22CFB3B9CFA5B136D882), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7754 = { sizeof (U3CAsCoroutineU3Ed__0_t523ECBDE8E55C4CC5FC6CE2EFCD1057B234FE883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7754[3] = 
{
	U3CAsCoroutineU3Ed__0_t523ECBDE8E55C4CC5FC6CE2EFCD1057B234FE883::get_offset_of_U3CU3E1__state_0(),
	U3CAsCoroutineU3Ed__0_t523ECBDE8E55C4CC5FC6CE2EFCD1057B234FE883::get_offset_of_U3CU3E2__current_1(),
	U3CAsCoroutineU3Ed__0_t523ECBDE8E55C4CC5FC6CE2EFCD1057B234FE883::get_offset_of_task_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7755 = { sizeof (WebRequestException_tFB2B03AD112281C424703E9FAC8D0B69520E7887), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7756 = { sizeof (ShaderNotFoundException_tBEB66BDDF37AA66E1B27463A3F2013784E302180), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7757 = { sizeof (GLTFLoadException_t349EF913EF0D9ABB54A482B113C96B44F996DC31), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7758 = { sizeof (GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7758[16] = 
{
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_GLTFUri_4(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_Multithreaded_5(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_UseStream_6(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_AppendStreamingAssets_7(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_PlayAnimationOnLoad_8(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_U3CLoadedU3Ek__BackingField_9(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_loadOnStart_10(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_MaterialsOnly_11(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_RetryCount_12(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_RetryTimeout_13(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_numRetries_14(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_MaximumLod_15(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_Timeout_16(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_Collider_17(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_asyncCoroutineHelper_18(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_shaderOverride_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7759 = { sizeof (U3CStartU3Ed__19_t8DAB112441359EA42B501246B5BF30F664AD6856)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7759[6] = 
{
	U3CStartU3Ed__19_t8DAB112441359EA42B501246B5BF30F664AD6856::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartU3Ed__19_t8DAB112441359EA42B501246B5BF30F664AD6856::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartU3Ed__19_t8DAB112441359EA42B501246B5BF30F664AD6856::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartU3Ed__19_t8DAB112441359EA42B501246B5BF30F664AD6856::get_offset_of_U3CU3E7__wrap1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartU3Ed__19_t8DAB112441359EA42B501246B5BF30F664AD6856::get_offset_of_U3CU3E7__wrap2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartU3Ed__19_t8DAB112441359EA42B501246B5BF30F664AD6856::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7760 = { sizeof (U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7760[7] = 
{
	U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43::get_offset_of_U3CsceneImporterU3E5__2_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43::get_offset_of_U3CloaderU3E5__3_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadU3Ed__20_tD8E0DC5A8DE18F109372251361C3435EE537DE43::get_offset_of_U3CU3Eu__2_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7761 = { sizeof (GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21), -1, sizeof(GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7761[23] = 
{
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21::get_offset_of__rootTransforms_0(),
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21::get_offset_of__root_1(),
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21::get_offset_of__bufferId_2(),
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21::get_offset_of__buffer_3(),
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21::get_offset_of__bufferWriter_4(),
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21::get_offset_of__imageInfos_5(),
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21::get_offset_of__textures_6(),
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21::get_offset_of__materials_7(),
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21::get_offset_of__shouldUseInternalBufferForImages_8(),
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21::get_offset_of__retrieveTexturePathDelegate_9(),
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21::get_offset_of__metalGlossChannelSwapMaterial_10(),
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21::get_offset_of__normalChannelMaterial_11(),
	0,
	0,
	0,
	0,
	0,
	0,
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21::get_offset_of__primOwner_18(),
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21::get_offset_of__meshToPrims_19(),
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21_StaticFields::get_offset_of_ExportNames_20(),
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21_StaticFields::get_offset_of_ExportFullPath_21(),
	GLTFSceneExporter_t76602B52640298A65167CB64B36993C9E0B5AE21_StaticFields::get_offset_of_RequireExtensions_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7762 = { sizeof (RetrieveTexturePathDelegate_tFFE02629D2BCA6A3D7674D8797915C30126F3A6B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7763 = { sizeof (IMAGETYPE_t08208151488743B3EA18DC95B5732968BA125066)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7763[8] = 
{
	IMAGETYPE_t08208151488743B3EA18DC95B5732968BA125066::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7764 = { sizeof (TextureMapType_tEF0E81720CC8AB381953FB9A49FCEADD143BFFC9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7764[8] = 
{
	TextureMapType_tEF0E81720CC8AB381953FB9A49FCEADD143BFFC9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7765 = { sizeof (ImageInfo_tA735B62CE32CD32BA3FD2C3E124C0E97509E0AC6)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7765[2] = 
{
	ImageInfo_tA735B62CE32CD32BA3FD2C3E124C0E97509E0AC6::get_offset_of_texture_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageInfo_tA735B62CE32CD32BA3FD2C3E124C0E97509E0AC6::get_offset_of_textureMapType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7766 = { sizeof (PrimKey_t1517CCEE386BDB5C1D2D1BBB71AE633BE6DBA196)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7766[2] = 
{
	PrimKey_t1517CCEE386BDB5C1D2D1BBB71AE633BE6DBA196::get_offset_of_Mesh_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PrimKey_t1517CCEE386BDB5C1D2D1BBB71AE633BE6DBA196::get_offset_of_Material_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7767 = { sizeof (MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7767[2] = 
{
	MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419::get_offset_of_U3CPrimitiveU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshConstructionData_tC2D7CC182BCE40B34DFA30AD5F7CB2EF16115419::get_offset_of_U3CMeshAttributesU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7768 = { sizeof (UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7768[10] = 
{
	UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D::get_offset_of_Vertices_0(),
	UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D::get_offset_of_Normals_1(),
	UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D::get_offset_of_Uv1_2(),
	UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D::get_offset_of_Uv2_3(),
	UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D::get_offset_of_Uv3_4(),
	UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D::get_offset_of_Uv4_5(),
	UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D::get_offset_of_Colors_6(),
	UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D::get_offset_of_Triangles_7(),
	UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D::get_offset_of_Tangents_8(),
	UnityMeshData_tD2E7EE9BF2746EDABC265ECEDC9BFD848294D28D::get_offset_of_BoneWeights_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7769 = { sizeof (GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7769[20] = 
{
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of_MaximumLod_0(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of_Timeout_1(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of_isMultithreaded_2(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of_U3CSceneParentU3Ek__BackingField_3(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of_U3CCreatedObjectU3Ek__BackingField_4(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of_U3CColliderU3Ek__BackingField_5(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of_U3CCustomShaderNameU3Ek__BackingField_6(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of_BudgetPerFrameInMilliseconds_7(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of_KeepCPUCopyOfMesh_8(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__timeAtLastYield_9(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__asyncCoroutineHelper_10(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__lastLoadedScene_11(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of_DefaultMaterial_12(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__defaultLoadedMaterial_13(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__gltfFileName_14(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__gltfStream_15(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__gltfRoot_16(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__assetCache_17(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__loader_18(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__isRunning_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7770 = { sizeof (ColliderType_tF7E70C605402A47138095F21308771417EE85FFA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7770[5] = 
{
	ColliderType_tF7E70C605402A47138095F21308771417EE85FFA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7771 = { sizeof (GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7771[2] = 
{
	GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A::get_offset_of_Stream_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A::get_offset_of_StartPosition_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7772 = { sizeof (U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7772[7] = 
{
	U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F::get_offset_of_sceneIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F::get_offset_of_showSceneObj_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F::get_offset_of_onLoadComplete_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadSceneAsyncU3Ed__40_tD2F1C69CD5CD6F4AD6653A9F46B93E0A6C810E9F::get_offset_of_U3CU3Eu__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7773 = { sizeof (U3CLoadNodeAsyncU3Ed__42_tCE807A7F6B49C9E8FCD562667E86E99BD38F6DF7)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7773[5] = 
{
	U3CLoadNodeAsyncU3Ed__42_tCE807A7F6B49C9E8FCD562667E86E99BD38F6DF7::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadNodeAsyncU3Ed__42_tCE807A7F6B49C9E8FCD562667E86E99BD38F6DF7::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadNodeAsyncU3Ed__42_tCE807A7F6B49C9E8FCD562667E86E99BD38F6DF7::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadNodeAsyncU3Ed__42_tCE807A7F6B49C9E8FCD562667E86E99BD38F6DF7::get_offset_of_nodeIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadNodeAsyncU3Ed__42_tCE807A7F6B49C9E8FCD562667E86E99BD38F6DF7::get_offset_of_U3CU3Eu__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7774 = { sizeof (U3CLoadMaterialAsyncU3Ed__43_t8CB980C4845985ECEBC51F97822270F1CD99C95F)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7774[6] = 
{
	U3CLoadMaterialAsyncU3Ed__43_t8CB980C4845985ECEBC51F97822270F1CD99C95F::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadMaterialAsyncU3Ed__43_t8CB980C4845985ECEBC51F97822270F1CD99C95F::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadMaterialAsyncU3Ed__43_t8CB980C4845985ECEBC51F97822270F1CD99C95F::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadMaterialAsyncU3Ed__43_t8CB980C4845985ECEBC51F97822270F1CD99C95F::get_offset_of_materialIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadMaterialAsyncU3Ed__43_t8CB980C4845985ECEBC51F97822270F1CD99C95F::get_offset_of_U3CU3Eu__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadMaterialAsyncU3Ed__43_t8CB980C4845985ECEBC51F97822270F1CD99C95F::get_offset_of_U3CdefU3E5__2_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7775 = { sizeof (U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7775[8] = 
{
	U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E::get_offset_of_node_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E::get_offset_of_U3CU3E4__this_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E::get_offset_of_U3ClodsextensionU3E5__2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E::get_offset_of_U3CU3E7__wrap2_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructBufferDataU3Ed__45_t9F1660E7623137ED5EB0EFBD40AE0A3B6241870E::get_offset_of_U3CiU3E5__4_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7776 = { sizeof (U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7776[9] = 
{
	U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949::get_offset_of_meshId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949::get_offset_of_U3CU3E4__this_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949::get_offset_of_mesh_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949::get_offset_of_U3CmeshIdIndexU3E5__2_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949::get_offset_of_U3CiU3E5__3_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949::get_offset_of_U3CprimitiveU3E5__4_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__46_tFA596F06EAC89B47AF44E40B8944068EB387E949::get_offset_of_U3CU3Eu__1_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7777 = { sizeof (U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7777[7] = 
{
	U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860::get_offset_of_texture_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860::get_offset_of_textureIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860::get_offset_of_U3CsourceIdU3E5__2_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructImageBufferU3Ed__47_t24501153C7B53D1792EDEDBF6789C1D4044AF860::get_offset_of_U3CU3Eu__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7778 = { sizeof (U3CWaitUntilEnumU3Ed__48_t04AEC5A1823CF0E6EA76CFF45ED60C9C2BB01921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7778[3] = 
{
	U3CWaitUntilEnumU3Ed__48_t04AEC5A1823CF0E6EA76CFF45ED60C9C2BB01921::get_offset_of_U3CU3E1__state_0(),
	U3CWaitUntilEnumU3Ed__48_t04AEC5A1823CF0E6EA76CFF45ED60C9C2BB01921::get_offset_of_U3CU3E2__current_1(),
	U3CWaitUntilEnumU3Ed__48_t04AEC5A1823CF0E6EA76CFF45ED60C9C2BB01921::get_offset_of_waitUntil_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7779 = { sizeof (U3CEmptyYieldEnumU3Ed__49_t22C2121ABBAE0537A0163637FBB8166C0FCF882B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7779[2] = 
{
	U3CEmptyYieldEnumU3Ed__49_t22C2121ABBAE0537A0163637FBB8166C0FCF882B::get_offset_of_U3CU3E1__state_0(),
	U3CEmptyYieldEnumU3Ed__49_t22C2121ABBAE0537A0163637FBB8166C0FCF882B::get_offset_of_U3CU3E2__current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7780 = { sizeof (U3CU3Ec__DisplayClass50_0_tDCB32992B37184B2BAAD02F93BC74DB03178998E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7780[2] = 
{
	U3CU3Ec__DisplayClass50_0_tDCB32992B37184B2BAAD02F93BC74DB03178998E::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass50_0_tDCB32992B37184B2BAAD02F93BC74DB03178998E::get_offset_of_jsonFilePath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7781 = { sizeof (U3CU3Ec__DisplayClass50_1_tD902C284B85AC38CBAFD4438F41781C6F529D180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7781[1] = 
{
	U3CU3Ec__DisplayClass50_1_tD902C284B85AC38CBAFD4438F41781C6F529D180::get_offset_of_loadThread_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7782 = { sizeof (U3CU3Ec__DisplayClass50_2_t94962D31C36BB849562C26FA6523CE2F18F5C5C4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7782[1] = 
{
	U3CU3Ec__DisplayClass50_2_t94962D31C36BB849562C26FA6523CE2F18F5C5C4::get_offset_of_parseJsonThread_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7783 = { sizeof (U3CLoadJsonU3Ed__50_t683B35F04BE5EED5474504834C5367319DFD940A)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7783[6] = 
{
	U3CLoadJsonU3Ed__50_t683B35F04BE5EED5474504834C5367319DFD940A::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadJsonU3Ed__50_t683B35F04BE5EED5474504834C5367319DFD940A::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadJsonU3Ed__50_t683B35F04BE5EED5474504834C5367319DFD940A::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadJsonU3Ed__50_t683B35F04BE5EED5474504834C5367319DFD940A::get_offset_of_jsonFilePath_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadJsonU3Ed__50_t683B35F04BE5EED5474504834C5367319DFD940A::get_offset_of_U3CU3E8__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadJsonU3Ed__50_t683B35F04BE5EED5474504834C5367319DFD940A::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7784 = { sizeof (U3CU3Ec__DisplayClass52_0_tD36628E62AE9ACC2EE3B3744FB9FAFEFC2CFFCCA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7784[2] = 
{
	U3CU3Ec__DisplayClass52_0_tD36628E62AE9ACC2EE3B3744FB9FAFEFC2CFFCCA::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass52_0_tD36628E62AE9ACC2EE3B3744FB9FAFEFC2CFFCCA::get_offset_of_nodeToLoad_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7785 = { sizeof (U3C_LoadNodeU3Ed__52_t4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7785[6] = 
{
	U3C_LoadNodeU3Ed__52_t4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3C_LoadNodeU3Ed__52_t4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3C_LoadNodeU3Ed__52_t4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3C_LoadNodeU3Ed__52_t4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7::get_offset_of_nodeIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3C_LoadNodeU3Ed__52_t4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7::get_offset_of_U3CU3E8__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3C_LoadNodeU3Ed__52_t4EEA01874D7F2853EF400AAAA023BA2AE0C88AC7::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7786 = { sizeof (U3C_LoadSceneU3Ed__53_t8BAE33712802D267371AF1C769DC15F2496BAB38)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7786[6] = 
{
	U3C_LoadSceneU3Ed__53_t8BAE33712802D267371AF1C769DC15F2496BAB38::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3C_LoadSceneU3Ed__53_t8BAE33712802D267371AF1C769DC15F2496BAB38::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3C_LoadSceneU3Ed__53_t8BAE33712802D267371AF1C769DC15F2496BAB38::get_offset_of_sceneIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3C_LoadSceneU3Ed__53_t8BAE33712802D267371AF1C769DC15F2496BAB38::get_offset_of_U3CU3E4__this_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3C_LoadSceneU3Ed__53_t8BAE33712802D267371AF1C769DC15F2496BAB38::get_offset_of_showSceneObj_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3C_LoadSceneU3Ed__53_t8BAE33712802D267371AF1C769DC15F2496BAB38::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7787 = { sizeof (U3CConstructBufferU3Ed__54_t4F5030F1E43E234598709AD4840BEEE7A9B1509C)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7787[6] = 
{
	U3CConstructBufferU3Ed__54_t4F5030F1E43E234598709AD4840BEEE7A9B1509C::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructBufferU3Ed__54_t4F5030F1E43E234598709AD4840BEEE7A9B1509C::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructBufferU3Ed__54_t4F5030F1E43E234598709AD4840BEEE7A9B1509C::get_offset_of_buffer_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructBufferU3Ed__54_t4F5030F1E43E234598709AD4840BEEE7A9B1509C::get_offset_of_U3CU3E4__this_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructBufferU3Ed__54_t4F5030F1E43E234598709AD4840BEEE7A9B1509C::get_offset_of_bufferIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructBufferU3Ed__54_t4F5030F1E43E234598709AD4840BEEE7A9B1509C::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7788 = { sizeof (U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7788[9] = 
{
	U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6::get_offset_of_imageCacheIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6::get_offset_of_image_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6::get_offset_of_markGpuOnly_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6::get_offset_of_linear_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6::get_offset_of_U3CstreamU3E5__2_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructImageU3Ed__55_t535ABFE1389C26A16257F498B5A134047A6157C6::get_offset_of_U3CU3Eu__1_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7789 = { sizeof (U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7789[10] = 
{
	U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25::get_offset_of_linear_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25::get_offset_of_stream_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25::get_offset_of_U3CU3E4__this_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25::get_offset_of_markGpuOnly_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25::get_offset_of_imageCacheIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25::get_offset_of_U3CtextureU3E5__2_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25::get_offset_of_U3CbufferU3E5__3_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityTextureU3Ed__56_tBA6303F731167733DB4DEB17898073F86FE1EA25::get_offset_of_U3CU3Eu__1_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7790 = { sizeof (U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7790[11] = 
{
	U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA::get_offset_of_meshID_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA::get_offset_of_primitiveIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA::get_offset_of_primitive_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA::get_offset_of_U3CattributeAccessorsU3E5__2_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA::get_offset_of_U3CU3E7__wrap2_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA::get_offset_of_U3CattributePairU3E5__4_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA::get_offset_of_U3CbufferIdU3E5__5_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshAttributesU3Ed__57_t92D9A7E8CA4AA7E0E914C40EB0E32ABE9C8EB1CA::get_offset_of_U3CU3Eu__1_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7791 = { sizeof (U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7791[10] = 
{
	U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F::get_offset_of_scene_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F::get_offset_of_showSceneObj_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F::get_offset_of_U3CU3E4__this_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F::get_offset_of_U3CsceneObjU3E5__2_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F::get_offset_of_U3CnodeTransformsU3E5__3_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F::get_offset_of_U3CiU3E5__4_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F::get_offset_of_U3CnodeU3E5__5_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructSceneU3Ed__63_tB41D9E6916623C7CB011C6B5044B54DDC2D32F1F::get_offset_of_U3CU3Eu__1_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7792 = { sizeof (U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7792[16] = 
{
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_nodeIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_node_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_U3CnodeObjU3E5__2_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_U3ClodsextensionU3E5__3_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_U3CU3Eu__1_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_U3CU3E7__wrap3_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_U3CchildU3E5__5_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_U3ClodsU3E5__6_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_U3ClodCoverageU3E5__7_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_U3ClodGroupNodeObjU3E5__8_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_U3ClodGroupU3E5__9_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_U3CiU3E5__10_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructNodeU3Ed__64_tA9DD0B3BE45F1C3BDC29CE633555AC2A75339E0B::get_offset_of_U3ClodNodeIdU3E5__11_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7793 = { sizeof (U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7793[12] = 
{
	U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4::get_offset_of_skin_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4::get_offset_of_U3CU3E4__this_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4::get_offset_of_renderer_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4::get_offset_of_curMesh_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4::get_offset_of_U3CboneCountU3E5__2_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4::get_offset_of_U3CbonesU3E5__3_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4::get_offset_of_U3CgltfBindPosesU3E5__4_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4::get_offset_of_U3CbindPosesU3E5__5_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4::get_offset_of_U3CiU3E5__6_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSetupBonesU3Ed__69_t819E455A4E83C57661BDEBACB331350123B6ACF4::get_offset_of_U3CU3Eu__1_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7794 = { sizeof (U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7794[14] = 
{
	U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42::get_offset_of_meshId_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42::get_offset_of_mesh_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42::get_offset_of_skin_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42::get_offset_of_parent_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42::get_offset_of_U3CiU3E5__2_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42::get_offset_of_U3CprimitiveU3E5__3_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42::get_offset_of_U3CmaterialIndexU3E5__4_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42::get_offset_of_U3CprimitiveObjU3E5__5_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42::get_offset_of_U3CcurMeshU3E5__6_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42::get_offset_of_U3CU3Eu__1_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshU3Ed__72_t7C23442B158C254E2BC7939894FB15EFCF8B3A42::get_offset_of_U3CskinnedMeshRendererU3E5__7_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7795 = { sizeof (U3CU3Ec__DisplayClass73_0_tD494223E41D1BFA7571AC3568E9A99A8B3FED886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7795[3] = 
{
	U3CU3Ec__DisplayClass73_0_tD494223E41D1BFA7571AC3568E9A99A8B3FED886::get_offset_of_unityMeshData_0(),
	U3CU3Ec__DisplayClass73_0_tD494223E41D1BFA7571AC3568E9A99A8B3FED886::get_offset_of_meshConstructionData_1(),
	U3CU3Ec__DisplayClass73_0_tD494223E41D1BFA7571AC3568E9A99A8B3FED886::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7796 = { sizeof (U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7796[10] = 
{
	U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9::get_offset_of_meshID_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9::get_offset_of_primitiveIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9::get_offset_of_primitive_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9::get_offset_of_U3CU3E8__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9::get_offset_of_materialIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9::get_offset_of_U3CU3Eu__1_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMeshPrimitiveU3Ed__73_t1EC85122DC787A6C42488ADF833B4B7A4367EBC9::get_offset_of_U3CU3Eu__2_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7797 = { sizeof (U3CTryYieldOnTimeoutU3Ed__74_tF2900FF5C648912CE5DAA0C36BD396DA386AF5C6)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7797[4] = 
{
	U3CTryYieldOnTimeoutU3Ed__74_tF2900FF5C648912CE5DAA0C36BD396DA386AF5C6::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CTryYieldOnTimeoutU3Ed__74_tF2900FF5C648912CE5DAA0C36BD396DA386AF5C6::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CTryYieldOnTimeoutU3Ed__74_tF2900FF5C648912CE5DAA0C36BD396DA386AF5C6::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CTryYieldOnTimeoutU3Ed__74_tF2900FF5C648912CE5DAA0C36BD396DA386AF5C6::get_offset_of_U3CU3Eu__1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7798 = { sizeof (U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7798[11] = 
{
	U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5::get_offset_of_meshConstructionData_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5::get_offset_of_unityMeshData_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5::get_offset_of_U3CU3E4__this_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5::get_offset_of_meshId_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5::get_offset_of_primitiveIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5::get_offset_of_U3CvertexCountU3E5__2_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5::get_offset_of_U3ChasNormalsU3E5__3_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5::get_offset_of_U3CmeshU3E5__4_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructUnityMeshU3Ed__77_t85B4F6C284934F074E708D8E2AAB58643BB334B5::get_offset_of_U3CU3Eu__1_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7799 = { sizeof (U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7799[12] = 
{
	U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357::get_offset_of_def_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357::get_offset_of_materialIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357::get_offset_of_U3CmapperU3E5__2_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357::get_offset_of_U3CmrMapperU3E5__3_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357::get_offset_of_U3CsgMapperU3E5__4_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357::get_offset_of_U3CpbrU3E5__5_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357::get_offset_of_U3CtextureIdU3E5__6_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357::get_offset_of_U3CU3Eu__1_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CConstructMaterialU3Ed__78_tD1EAE9F22085AC88D49279FA14D6900EDD524357::get_offset_of_U3CspecGlossU3E5__7_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
