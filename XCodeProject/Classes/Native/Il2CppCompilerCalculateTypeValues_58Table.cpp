﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// MHLab.PATCH.Downloader.DownloadEventArgs
struct DownloadEventArgs_tE8931C7F6C9F12BB6E657D9F11491CD22D94BC6A;
// MHLab.PATCH.Downloader.DownloadProgressHandler
struct DownloadProgressHandler_t9F6C1F118EC7695FC511C7DD4B25EEFBDA7C1B9F;
// MHLab.PATCH.Downloader.FileDownloader
struct FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8;
// MHLab.PATCH.Install.Installer
struct Installer_t4AA6D9961392D2013E285491910531239D5A2932;
// MHLab.PATCH.Settings.SettingsOverrider
struct SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C;
// Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo
struct CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4;
// Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo[]
struct CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103;
// Microsoft.CSharp.RuntimeBinder.Errors.ErrorCode[]
struct ErrorCodeU5BU5D_tB94DF500819EF4A6CF9F0B42001C0DD3A96CEDF7;
// Microsoft.CSharp.RuntimeBinder.RuntimeBinder
struct RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03;
// Microsoft.CSharp.RuntimeBinder.Semantics.AggregateDeclaration
struct AggregateDeclaration_t1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5;
// Microsoft.CSharp.RuntimeBinder.Semantics.AggregateType
struct AggregateType_t8F8CDB34BC1F11AD3004AFE3BD62A0C6221CB3BB;
// Microsoft.CSharp.RuntimeBinder.Semantics.ArgInfos
struct ArgInfos_t8DDC052214D1A27ABDAEAB029CCF83CE18A6AE6D;
// Microsoft.CSharp.RuntimeBinder.Semantics.BSYMMGR
struct BSYMMGR_tBA5223E26DB1C60FC714AA4B84D365380236B957;
// Microsoft.CSharp.RuntimeBinder.Semantics.BindingContext
struct BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073;
// Microsoft.CSharp.RuntimeBinder.Semantics.CNullable
struct CNullable_t7AE2FD6161F70658549CB2814F27E766A2C3085C;
// Microsoft.CSharp.RuntimeBinder.Semantics.CSemanticChecker
struct CSemanticChecker_t392A4B642F3784AA63593EAEAA5D6E36F89E2A8A;
// Microsoft.CSharp.RuntimeBinder.Semantics.CType
struct CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA;
// Microsoft.CSharp.RuntimeBinder.Semantics.Expr
struct Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5;
// Microsoft.CSharp.RuntimeBinder.Semantics.ExprClass
struct ExprClass_t65292AB1E432F6C18D890D791F1A736AC8233278;
// Microsoft.CSharp.RuntimeBinder.Semantics.ExprFactory
struct ExprFactory_tFD6AA538BD7633AE2E5585D41577AB6B996811DA;
// Microsoft.CSharp.RuntimeBinder.Semantics.ExprList
struct ExprList_tA3E91238529996C97D96CB0008512C01CBC28132;
// Microsoft.CSharp.RuntimeBinder.Semantics.ExprMemberGroup
struct ExprMemberGroup_tDD0D29E0D6B46D5922D603F22F5D67F78AE35AA4;
// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder
struct ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E;
// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpSig[]
struct BinOpSigU5BU5D_t5FAB057C77FA31908C3C5D928EBFBDE82EB1260B;
// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinderResult
struct GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0;
// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/PfnBindBinOp
struct PfnBindBinOp_t376819D18187F697A9950060805897A8BF537F68;
// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/PfnBindUnaOp
struct PfnBindUnaOp_tF887D64C6A968258E18C38389610C6E1E70E6944;
// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/UnaOpSig[]
struct UnaOpSigU5BU5D_tD8A410FEA2A14E00294B50610C9DFDF03317B702;
// Microsoft.CSharp.RuntimeBinder.Semantics.GlobalSymbolContext
struct GlobalSymbolContext_t9C2C782AF29EBF932B5BB01E8A6065E80C5E3DC2;
// Microsoft.CSharp.RuntimeBinder.Semantics.MethPropWithInst
struct MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C;
// Microsoft.CSharp.RuntimeBinder.Semantics.MethWithType
struct MethWithType_t4CCE9B8DC13DE224B4CE2FCAA33226ACA7380616;
// Microsoft.CSharp.RuntimeBinder.Semantics.MethodOrPropertySymbol
struct MethodOrPropertySymbol_tFCA1AC659653934F87CB627CB32229A52A0270B0;
// Microsoft.CSharp.RuntimeBinder.Semantics.NamespaceOrAggregateSymbol
struct NamespaceOrAggregateSymbol_t1B1F54E58D34F283E61D89E7703F9E56A1248357;
// Microsoft.CSharp.RuntimeBinder.Semantics.NamespaceSymbol
struct NamespaceSymbol_t09CBB22705730203A7D1B5F35B62D46C7EA94253;
// Microsoft.CSharp.RuntimeBinder.Semantics.Operators/OperatorInfo[]
struct OperatorInfoU5BU5D_t3F7DFFEA45E406A04BCC6D2B9BFE7DB54FB0FFF9;
// Microsoft.CSharp.RuntimeBinder.Semantics.ParentSymbol
struct ParentSymbol_t5CA9C0921CCD816CF7FCF2D10B913499758EA593;
// Microsoft.CSharp.RuntimeBinder.Semantics.SYMTBL
struct SYMTBL_tD9CD6C1E5BF2477F2C025858E574A9F0624ACCB8;
// Microsoft.CSharp.RuntimeBinder.Semantics.SymFactory
struct SymFactory_t132D3AF45C44031D0EA63987998943D16C46D1F1;
// Microsoft.CSharp.RuntimeBinder.Semantics.Symbol
struct Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA;
// Microsoft.CSharp.RuntimeBinder.Semantics.SymbolLoader
struct SymbolLoader_t44A1A145E4324210903F3FA34033B41C9A80A69F;
// Microsoft.CSharp.RuntimeBinder.Semantics.TypeArray
struct TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2;
// Microsoft.CSharp.RuntimeBinder.Semantics.TypeManager
struct TypeManager_t67F89DC00FB47A6EBA5060FEF0CFF62B74F073B6;
// Microsoft.CSharp.RuntimeBinder.SymbolTable
struct SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C;
// Microsoft.CSharp.RuntimeBinder.SymbolTable/<>c__DisplayClass15_0
struct U3CU3Ec__DisplayClass15_0_t8EF4067FAF6A75CB6BF5EFF004BE26788651C6A8;
// Microsoft.CSharp.RuntimeBinder.Syntax.Name
struct Name_t95244514D054D80B6614F5E2275EC034F1527725;
// Microsoft.CSharp.RuntimeBinder.Syntax.NameManager
struct NameManager_t965C30E738C964824296FF875F6E7B4828AEDD31;
// Microsoft.CSharp.RuntimeBinder.Syntax.NameTable
struct NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0;
// Microsoft.CSharp.RuntimeBinder.Syntax.NameTable/Entry[]
struct EntryU5BU5D_t8CE801A724A0E7C021F32553EDDFA0745502D2EE;
// Microsoft.CSharp.RuntimeBinder.Syntax.Name[]
struct NameU5BU5D_tAF4AAC512553ABE0CA97252D968C3910789AFAD3;
// Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedName[]
struct PredefinedNameU5BU5D_t80E1FD2DF873DA212FE771FEF08C91DA7D13B11C;
// Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedType[]
struct PredefinedTypeU5BU5D_t7EA0AADE7719AD582C448A220A5447D60C2177C0;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE;
// System.Action`2<System.String,System.String>
struct Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757;
// System.Action`3<System.Int64,System.Int64,System.Int32>
struct Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281;
// System.Action`3<System.String,System.String,System.Exception>
struct Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Microsoft.CSharp.RuntimeBinder.Semantics.ExprCall,System.Linq.Expressions.Expression>
struct Dictionary_2_t092C659B9D5D00294F7E3BDFF7284CAA8847E08E;
// System.Collections.Generic.Dictionary`2<Microsoft.CSharp.RuntimeBinder.Syntax.Name,System.String>
struct Dictionary_2_t7E9D603770E286A4E73D896EBCF744FC1B256C3C;
// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
struct Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7;
// System.Collections.Generic.HashSet`1<Microsoft.CSharp.RuntimeBinder.SymbolTable/NameHashKey>
struct HashSet_1_t10E1E5C7A746A1156C4CD5A0928A5D4F6CFD684F;
// System.Collections.Generic.HashSet`1<System.Type>
struct HashSet_1_t4539A1D24467B4BCB004FE3DFB4D15C90372E5E7;
// System.Collections.Generic.List`1<MHLab.PATCH.Version>
struct List_1_t92A1CC18FB9FE5949C69296A0D8D3698C5727DF2;
// System.Collections.Generic.List`1<Microsoft.CSharp.RuntimeBinder.Semantics.CType>
struct List_1_t8C111A83B900F2EF6541780D77CFDD38873EF3B1;
// System.Collections.Generic.List`1<Microsoft.CSharp.RuntimeBinder.Semantics.CandidateFunctionMember>
struct List_1_t9226D64AE50FE47D8B11AE1370678622D6D69428;
// System.Collections.Generic.List`1<Microsoft.CSharp.RuntimeBinder.Semantics.MethPropWithInst>
struct List_1_t92334295FD181400CD37F835093DA258221B337D;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Comparison`1<MHLab.PATCH.Version>
struct Comparison_1_tB62C7DFC32565B8EC7275A3E0AA32D03DEE6C38B;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Dynamic.CallInfo
struct CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935;
// System.EventHandler
struct EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C;
// System.Func`2<System.Boolean,System.Boolean>
struct Func_2_t992C4FF3CDF2A4BD79586600575703C9315961A1;
// System.Func`2<System.Reflection.MethodInfo,System.Boolean>
struct Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438;
// System.Func`3<System.Reflection.MemberInfo,System.Reflection.MemberInfo,System.Boolean>
struct Func_3_tD31145957D9862FA32A98EED47776298D76B975C;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Lazy`1<Microsoft.CSharp.RuntimeBinder.RuntimeBinder>
struct Lazy_1_t3AB76E562D17A7F29EA76A6400E4400BC8E0A2A6;
// System.Linq.Expressions.Expression
struct Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F;
// System.Linq.Expressions.Expression[]
struct ExpressionU5BU5D_t69D0BBE3C10949764DC20D5769D2469085774758;
// System.Linq.Expressions.LabelTarget
struct LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7;
// System.Net.IWebProxy
struct IWebProxy_tA24C0862A1ACA35D20FD079E2672CA5786C1A67E;
// System.Net.WebResponse
struct WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodBase
struct MethodBase_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_TC6BFFD591588D685E886F92456B1C92F17411C71_H
#define U3CMODULEU3E_TC6BFFD591588D685E886F92456B1C92F17411C71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tC6BFFD591588D685E886F92456B1C92F17411C71 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TC6BFFD591588D685E886F92456B1C92F17411C71_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef COMPRESSOR_T40F613A221FDE851F69B704F2E9BF0D600C799F3_H
#define COMPRESSOR_T40F613A221FDE851F69B704F2E9BF0D600C799F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Compression.Compressor
struct  Compressor_t40F613A221FDE851F69B704F2E9BF0D600C799F3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSOR_T40F613A221FDE851F69B704F2E9BF0D600C799F3_H
#ifndef TARGZCOMPRESSOR_T89910D0FCADAC20E2E9C4F8C6D24063F8DD1FA7C_H
#define TARGZCOMPRESSOR_T89910D0FCADAC20E2E9C4F8C6D24063F8DD1FA7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Compression.TAR.TARGZCompressor
struct  TARGZCompressor_t89910D0FCADAC20E2E9C4F8C6D24063F8DD1FA7C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGZCOMPRESSOR_T89910D0FCADAC20E2E9C4F8C6D24063F8DD1FA7C_H
#ifndef ZIPCOMPRESSOR_TC0AD4EADD989CDA689C2686C91CE1385B3873CC5_H
#define ZIPCOMPRESSOR_TC0AD4EADD989CDA689C2686C91CE1385B3873CC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Compression.ZIP.ZIPCompressor
struct  ZIPCompressor_tC0AD4EADD989CDA689C2686C91CE1385B3873CC5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPCOMPRESSOR_TC0AD4EADD989CDA689C2686C91CE1385B3873CC5_H
#ifndef DEBUGGER_T1E0CE4A97B210FE0CCBA830D22EA6D0769519A70_H
#define DEBUGGER_T1E0CE4A97B210FE0CCBA830D22EA6D0769519A70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Debugging.Debugger
struct  Debugger_t1E0CE4A97B210FE0CCBA830D22EA6D0769519A70  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGER_T1E0CE4A97B210FE0CCBA830D22EA6D0769519A70_H
#ifndef DOWNLOADDATA_TEB2C132BB12298D726F8840BDE898573130365C0_H
#define DOWNLOADDATA_TEB2C132BB12298D726F8840BDE898573130365C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Downloader.DownloadData
struct  DownloadData_tEB2C132BB12298D726F8840BDE898573130365C0  : public RuntimeObject
{
public:
	// System.Net.WebResponse MHLab.PATCH.Downloader.DownloadData::response
	WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * ___response_0;
	// System.IO.Stream MHLab.PATCH.Downloader.DownloadData::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_1;
	// System.Int64 MHLab.PATCH.Downloader.DownloadData::size
	int64_t ___size_2;
	// System.Int64 MHLab.PATCH.Downloader.DownloadData::start
	int64_t ___start_3;
	// System.Net.IWebProxy MHLab.PATCH.Downloader.DownloadData::proxy
	RuntimeObject* ___proxy_4;

public:
	inline static int32_t get_offset_of_response_0() { return static_cast<int32_t>(offsetof(DownloadData_tEB2C132BB12298D726F8840BDE898573130365C0, ___response_0)); }
	inline WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * get_response_0() const { return ___response_0; }
	inline WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD ** get_address_of_response_0() { return &___response_0; }
	inline void set_response_0(WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * value)
	{
		___response_0 = value;
		Il2CppCodeGenWriteBarrier((&___response_0), value);
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(DownloadData_tEB2C132BB12298D726F8840BDE898573130365C0, ___stream_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_1() const { return ___stream_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___stream_1), value);
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(DownloadData_tEB2C132BB12298D726F8840BDE898573130365C0, ___size_2)); }
	inline int64_t get_size_2() const { return ___size_2; }
	inline int64_t* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(int64_t value)
	{
		___size_2 = value;
	}

	inline static int32_t get_offset_of_start_3() { return static_cast<int32_t>(offsetof(DownloadData_tEB2C132BB12298D726F8840BDE898573130365C0, ___start_3)); }
	inline int64_t get_start_3() const { return ___start_3; }
	inline int64_t* get_address_of_start_3() { return &___start_3; }
	inline void set_start_3(int64_t value)
	{
		___start_3 = value;
	}

	inline static int32_t get_offset_of_proxy_4() { return static_cast<int32_t>(offsetof(DownloadData_tEB2C132BB12298D726F8840BDE898573130365C0, ___proxy_4)); }
	inline RuntimeObject* get_proxy_4() const { return ___proxy_4; }
	inline RuntimeObject** get_address_of_proxy_4() { return &___proxy_4; }
	inline void set_proxy_4(RuntimeObject* value)
	{
		___proxy_4 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADDATA_TEB2C132BB12298D726F8840BDE898573130365C0_H
#ifndef FILEDOWNLOADER_TEEDF59D489CB432A2B34B5C7765CFF6EF91444D8_H
#define FILEDOWNLOADER_TEEDF59D489CB432A2B34B5C7765CFF6EF91444D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Downloader.FileDownloader
struct  FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8  : public RuntimeObject
{
public:
	// System.Int32 MHLab.PATCH.Downloader.FileDownloader::downloadBlockSize
	int32_t ___downloadBlockSize_0;
	// System.Boolean MHLab.PATCH.Downloader.FileDownloader::canceled
	bool ___canceled_1;
	// System.String MHLab.PATCH.Downloader.FileDownloader::downloadingTo
	String_t* ___downloadingTo_2;
	// MHLab.PATCH.Downloader.DownloadProgressHandler MHLab.PATCH.Downloader.FileDownloader::ProgressChanged
	DownloadProgressHandler_t9F6C1F118EC7695FC511C7DD4B25EEFBDA7C1B9F * ___ProgressChanged_3;
	// System.Net.IWebProxy MHLab.PATCH.Downloader.FileDownloader::proxy
	RuntimeObject* ___proxy_4;
	// System.EventHandler MHLab.PATCH.Downloader.FileDownloader::DownloadComplete
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___DownloadComplete_5;

public:
	inline static int32_t get_offset_of_downloadBlockSize_0() { return static_cast<int32_t>(offsetof(FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8, ___downloadBlockSize_0)); }
	inline int32_t get_downloadBlockSize_0() const { return ___downloadBlockSize_0; }
	inline int32_t* get_address_of_downloadBlockSize_0() { return &___downloadBlockSize_0; }
	inline void set_downloadBlockSize_0(int32_t value)
	{
		___downloadBlockSize_0 = value;
	}

	inline static int32_t get_offset_of_canceled_1() { return static_cast<int32_t>(offsetof(FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8, ___canceled_1)); }
	inline bool get_canceled_1() const { return ___canceled_1; }
	inline bool* get_address_of_canceled_1() { return &___canceled_1; }
	inline void set_canceled_1(bool value)
	{
		___canceled_1 = value;
	}

	inline static int32_t get_offset_of_downloadingTo_2() { return static_cast<int32_t>(offsetof(FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8, ___downloadingTo_2)); }
	inline String_t* get_downloadingTo_2() const { return ___downloadingTo_2; }
	inline String_t** get_address_of_downloadingTo_2() { return &___downloadingTo_2; }
	inline void set_downloadingTo_2(String_t* value)
	{
		___downloadingTo_2 = value;
		Il2CppCodeGenWriteBarrier((&___downloadingTo_2), value);
	}

	inline static int32_t get_offset_of_ProgressChanged_3() { return static_cast<int32_t>(offsetof(FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8, ___ProgressChanged_3)); }
	inline DownloadProgressHandler_t9F6C1F118EC7695FC511C7DD4B25EEFBDA7C1B9F * get_ProgressChanged_3() const { return ___ProgressChanged_3; }
	inline DownloadProgressHandler_t9F6C1F118EC7695FC511C7DD4B25EEFBDA7C1B9F ** get_address_of_ProgressChanged_3() { return &___ProgressChanged_3; }
	inline void set_ProgressChanged_3(DownloadProgressHandler_t9F6C1F118EC7695FC511C7DD4B25EEFBDA7C1B9F * value)
	{
		___ProgressChanged_3 = value;
		Il2CppCodeGenWriteBarrier((&___ProgressChanged_3), value);
	}

	inline static int32_t get_offset_of_proxy_4() { return static_cast<int32_t>(offsetof(FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8, ___proxy_4)); }
	inline RuntimeObject* get_proxy_4() const { return ___proxy_4; }
	inline RuntimeObject** get_address_of_proxy_4() { return &___proxy_4; }
	inline void set_proxy_4(RuntimeObject* value)
	{
		___proxy_4 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_4), value);
	}

	inline static int32_t get_offset_of_DownloadComplete_5() { return static_cast<int32_t>(offsetof(FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8, ___DownloadComplete_5)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_DownloadComplete_5() const { return ___DownloadComplete_5; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_DownloadComplete_5() { return &___DownloadComplete_5; }
	inline void set_DownloadComplete_5(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___DownloadComplete_5 = value;
		Il2CppCodeGenWriteBarrier((&___DownloadComplete_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEDOWNLOADER_TEEDF59D489CB432A2B34B5C7765CFF6EF91444D8_H
#ifndef INSTALLMANAGER_T6A16071C2DDC3B23EF861B128B2E2EC152A30990_H
#define INSTALLMANAGER_T6A16071C2DDC3B23EF861B128B2E2EC152A30990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Install.InstallManager
struct  InstallManager_t6A16071C2DDC3B23EF861B128B2E2EC152A30990  : public RuntimeObject
{
public:
	// MHLab.PATCH.Install.Installer MHLab.PATCH.Install.InstallManager::m_installer
	Installer_t4AA6D9961392D2013E285491910531239D5A2932 * ___m_installer_0;
	// MHLab.PATCH.Settings.SettingsOverrider MHLab.PATCH.Install.InstallManager::SETTINGS
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C * ___SETTINGS_1;

public:
	inline static int32_t get_offset_of_m_installer_0() { return static_cast<int32_t>(offsetof(InstallManager_t6A16071C2DDC3B23EF861B128B2E2EC152A30990, ___m_installer_0)); }
	inline Installer_t4AA6D9961392D2013E285491910531239D5A2932 * get_m_installer_0() const { return ___m_installer_0; }
	inline Installer_t4AA6D9961392D2013E285491910531239D5A2932 ** get_address_of_m_installer_0() { return &___m_installer_0; }
	inline void set_m_installer_0(Installer_t4AA6D9961392D2013E285491910531239D5A2932 * value)
	{
		___m_installer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_installer_0), value);
	}

	inline static int32_t get_offset_of_SETTINGS_1() { return static_cast<int32_t>(offsetof(InstallManager_t6A16071C2DDC3B23EF861B128B2E2EC152A30990, ___SETTINGS_1)); }
	inline SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C * get_SETTINGS_1() const { return ___SETTINGS_1; }
	inline SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C ** get_address_of_SETTINGS_1() { return &___SETTINGS_1; }
	inline void set_SETTINGS_1(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C * value)
	{
		___SETTINGS_1 = value;
		Il2CppCodeGenWriteBarrier((&___SETTINGS_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLMANAGER_T6A16071C2DDC3B23EF861B128B2E2EC152A30990_H
#ifndef INSTALLER_T4AA6D9961392D2013E285491910531239D5A2932_H
#define INSTALLER_T4AA6D9961392D2013E285491910531239D5A2932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Install.Installer
struct  Installer_t4AA6D9961392D2013E285491910531239D5A2932  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<MHLab.PATCH.Version> MHLab.PATCH.Install.Installer::availableBuilds
	List_1_t92A1CC18FB9FE5949C69296A0D8D3698C5727DF2 * ___availableBuilds_0;
	// System.Action`1<System.String> MHLab.PATCH.Install.Installer::OnFileProcessed
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___OnFileProcessed_1;
	// System.Action`1<System.String> MHLab.PATCH.Install.Installer::OnFileProcessing
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___OnFileProcessing_2;
	// System.Action`2<System.String,System.String> MHLab.PATCH.Install.Installer::OnLog
	Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * ___OnLog_3;
	// System.Action`3<System.String,System.String,System.Exception> MHLab.PATCH.Install.Installer::OnError
	Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * ___OnError_4;
	// System.Action`3<System.String,System.String,System.Exception> MHLab.PATCH.Install.Installer::OnFatalError
	Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * ___OnFatalError_5;
	// System.Action`1<System.String> MHLab.PATCH.Install.Installer::OnTaskStarted
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___OnTaskStarted_6;
	// System.Action`1<System.String> MHLab.PATCH.Install.Installer::OnTaskCompleted
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___OnTaskCompleted_7;
	// System.Action`2<System.Int32,System.Int32> MHLab.PATCH.Install.Installer::OnSetMainProgressBar
	Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * ___OnSetMainProgressBar_8;
	// System.Action`2<System.Int32,System.Int32> MHLab.PATCH.Install.Installer::OnSetDetailProgressBar
	Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * ___OnSetDetailProgressBar_9;
	// System.Action MHLab.PATCH.Install.Installer::OnIncreaseMainProgressBar
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnIncreaseMainProgressBar_10;
	// System.Action MHLab.PATCH.Install.Installer::OnIncreaseDetailProgressBar
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnIncreaseDetailProgressBar_11;
	// System.Action`3<System.Int64,System.Int64,System.Int32> MHLab.PATCH.Install.Installer::OnDownloadProgress
	Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 * ___OnDownloadProgress_12;
	// System.Action MHLab.PATCH.Install.Installer::OnDownloadCompleted
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnDownloadCompleted_13;
	// MHLab.PATCH.Downloader.FileDownloader MHLab.PATCH.Install.Installer::downloader
	FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8 * ___downloader_14;

public:
	inline static int32_t get_offset_of_availableBuilds_0() { return static_cast<int32_t>(offsetof(Installer_t4AA6D9961392D2013E285491910531239D5A2932, ___availableBuilds_0)); }
	inline List_1_t92A1CC18FB9FE5949C69296A0D8D3698C5727DF2 * get_availableBuilds_0() const { return ___availableBuilds_0; }
	inline List_1_t92A1CC18FB9FE5949C69296A0D8D3698C5727DF2 ** get_address_of_availableBuilds_0() { return &___availableBuilds_0; }
	inline void set_availableBuilds_0(List_1_t92A1CC18FB9FE5949C69296A0D8D3698C5727DF2 * value)
	{
		___availableBuilds_0 = value;
		Il2CppCodeGenWriteBarrier((&___availableBuilds_0), value);
	}

	inline static int32_t get_offset_of_OnFileProcessed_1() { return static_cast<int32_t>(offsetof(Installer_t4AA6D9961392D2013E285491910531239D5A2932, ___OnFileProcessed_1)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_OnFileProcessed_1() const { return ___OnFileProcessed_1; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_OnFileProcessed_1() { return &___OnFileProcessed_1; }
	inline void set_OnFileProcessed_1(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___OnFileProcessed_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnFileProcessed_1), value);
	}

	inline static int32_t get_offset_of_OnFileProcessing_2() { return static_cast<int32_t>(offsetof(Installer_t4AA6D9961392D2013E285491910531239D5A2932, ___OnFileProcessing_2)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_OnFileProcessing_2() const { return ___OnFileProcessing_2; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_OnFileProcessing_2() { return &___OnFileProcessing_2; }
	inline void set_OnFileProcessing_2(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___OnFileProcessing_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnFileProcessing_2), value);
	}

	inline static int32_t get_offset_of_OnLog_3() { return static_cast<int32_t>(offsetof(Installer_t4AA6D9961392D2013E285491910531239D5A2932, ___OnLog_3)); }
	inline Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * get_OnLog_3() const { return ___OnLog_3; }
	inline Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 ** get_address_of_OnLog_3() { return &___OnLog_3; }
	inline void set_OnLog_3(Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * value)
	{
		___OnLog_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnLog_3), value);
	}

	inline static int32_t get_offset_of_OnError_4() { return static_cast<int32_t>(offsetof(Installer_t4AA6D9961392D2013E285491910531239D5A2932, ___OnError_4)); }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * get_OnError_4() const { return ___OnError_4; }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A ** get_address_of_OnError_4() { return &___OnError_4; }
	inline void set_OnError_4(Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * value)
	{
		___OnError_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnError_4), value);
	}

	inline static int32_t get_offset_of_OnFatalError_5() { return static_cast<int32_t>(offsetof(Installer_t4AA6D9961392D2013E285491910531239D5A2932, ___OnFatalError_5)); }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * get_OnFatalError_5() const { return ___OnFatalError_5; }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A ** get_address_of_OnFatalError_5() { return &___OnFatalError_5; }
	inline void set_OnFatalError_5(Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * value)
	{
		___OnFatalError_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnFatalError_5), value);
	}

	inline static int32_t get_offset_of_OnTaskStarted_6() { return static_cast<int32_t>(offsetof(Installer_t4AA6D9961392D2013E285491910531239D5A2932, ___OnTaskStarted_6)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_OnTaskStarted_6() const { return ___OnTaskStarted_6; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_OnTaskStarted_6() { return &___OnTaskStarted_6; }
	inline void set_OnTaskStarted_6(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___OnTaskStarted_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnTaskStarted_6), value);
	}

	inline static int32_t get_offset_of_OnTaskCompleted_7() { return static_cast<int32_t>(offsetof(Installer_t4AA6D9961392D2013E285491910531239D5A2932, ___OnTaskCompleted_7)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_OnTaskCompleted_7() const { return ___OnTaskCompleted_7; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_OnTaskCompleted_7() { return &___OnTaskCompleted_7; }
	inline void set_OnTaskCompleted_7(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___OnTaskCompleted_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnTaskCompleted_7), value);
	}

	inline static int32_t get_offset_of_OnSetMainProgressBar_8() { return static_cast<int32_t>(offsetof(Installer_t4AA6D9961392D2013E285491910531239D5A2932, ___OnSetMainProgressBar_8)); }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * get_OnSetMainProgressBar_8() const { return ___OnSetMainProgressBar_8; }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE ** get_address_of_OnSetMainProgressBar_8() { return &___OnSetMainProgressBar_8; }
	inline void set_OnSetMainProgressBar_8(Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * value)
	{
		___OnSetMainProgressBar_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnSetMainProgressBar_8), value);
	}

	inline static int32_t get_offset_of_OnSetDetailProgressBar_9() { return static_cast<int32_t>(offsetof(Installer_t4AA6D9961392D2013E285491910531239D5A2932, ___OnSetDetailProgressBar_9)); }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * get_OnSetDetailProgressBar_9() const { return ___OnSetDetailProgressBar_9; }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE ** get_address_of_OnSetDetailProgressBar_9() { return &___OnSetDetailProgressBar_9; }
	inline void set_OnSetDetailProgressBar_9(Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * value)
	{
		___OnSetDetailProgressBar_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnSetDetailProgressBar_9), value);
	}

	inline static int32_t get_offset_of_OnIncreaseMainProgressBar_10() { return static_cast<int32_t>(offsetof(Installer_t4AA6D9961392D2013E285491910531239D5A2932, ___OnIncreaseMainProgressBar_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnIncreaseMainProgressBar_10() const { return ___OnIncreaseMainProgressBar_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnIncreaseMainProgressBar_10() { return &___OnIncreaseMainProgressBar_10; }
	inline void set_OnIncreaseMainProgressBar_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnIncreaseMainProgressBar_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnIncreaseMainProgressBar_10), value);
	}

	inline static int32_t get_offset_of_OnIncreaseDetailProgressBar_11() { return static_cast<int32_t>(offsetof(Installer_t4AA6D9961392D2013E285491910531239D5A2932, ___OnIncreaseDetailProgressBar_11)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnIncreaseDetailProgressBar_11() const { return ___OnIncreaseDetailProgressBar_11; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnIncreaseDetailProgressBar_11() { return &___OnIncreaseDetailProgressBar_11; }
	inline void set_OnIncreaseDetailProgressBar_11(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnIncreaseDetailProgressBar_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnIncreaseDetailProgressBar_11), value);
	}

	inline static int32_t get_offset_of_OnDownloadProgress_12() { return static_cast<int32_t>(offsetof(Installer_t4AA6D9961392D2013E285491910531239D5A2932, ___OnDownloadProgress_12)); }
	inline Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 * get_OnDownloadProgress_12() const { return ___OnDownloadProgress_12; }
	inline Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 ** get_address_of_OnDownloadProgress_12() { return &___OnDownloadProgress_12; }
	inline void set_OnDownloadProgress_12(Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 * value)
	{
		___OnDownloadProgress_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownloadProgress_12), value);
	}

	inline static int32_t get_offset_of_OnDownloadCompleted_13() { return static_cast<int32_t>(offsetof(Installer_t4AA6D9961392D2013E285491910531239D5A2932, ___OnDownloadCompleted_13)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnDownloadCompleted_13() const { return ___OnDownloadCompleted_13; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnDownloadCompleted_13() { return &___OnDownloadCompleted_13; }
	inline void set_OnDownloadCompleted_13(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnDownloadCompleted_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownloadCompleted_13), value);
	}

	inline static int32_t get_offset_of_downloader_14() { return static_cast<int32_t>(offsetof(Installer_t4AA6D9961392D2013E285491910531239D5A2932, ___downloader_14)); }
	inline FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8 * get_downloader_14() const { return ___downloader_14; }
	inline FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8 ** get_address_of_downloader_14() { return &___downloader_14; }
	inline void set_downloader_14(FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8 * value)
	{
		___downloader_14 = value;
		Il2CppCodeGenWriteBarrier((&___downloader_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLER_T4AA6D9961392D2013E285491910531239D5A2932_H
#ifndef U3CU3EC_TE05096997AC3C296BA928EE56F3E5365ADB8B8DC_H
#define U3CU3EC_TE05096997AC3C296BA928EE56F3E5365ADB8B8DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Install.Installer/<>c
struct  U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields
{
public:
	// MHLab.PATCH.Install.Installer/<>c MHLab.PATCH.Install.Installer/<>c::<>9
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC * ___U3CU3E9_0;
	// System.Action`1<System.String> MHLab.PATCH.Install.Installer/<>c::<>9__14_0
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___U3CU3E9__14_0_1;
	// System.Action`1<System.String> MHLab.PATCH.Install.Installer/<>c::<>9__14_1
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___U3CU3E9__14_1_2;
	// System.Action`2<System.String,System.String> MHLab.PATCH.Install.Installer/<>c::<>9__14_2
	Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * ___U3CU3E9__14_2_3;
	// System.Action`3<System.String,System.String,System.Exception> MHLab.PATCH.Install.Installer/<>c::<>9__14_3
	Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * ___U3CU3E9__14_3_4;
	// System.Action`3<System.String,System.String,System.Exception> MHLab.PATCH.Install.Installer/<>c::<>9__14_4
	Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * ___U3CU3E9__14_4_5;
	// System.Action`1<System.String> MHLab.PATCH.Install.Installer/<>c::<>9__14_5
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___U3CU3E9__14_5_6;
	// System.Action`1<System.String> MHLab.PATCH.Install.Installer/<>c::<>9__14_6
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___U3CU3E9__14_6_7;
	// System.Action`2<System.Int32,System.Int32> MHLab.PATCH.Install.Installer/<>c::<>9__14_7
	Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * ___U3CU3E9__14_7_8;
	// System.Action`2<System.Int32,System.Int32> MHLab.PATCH.Install.Installer/<>c::<>9__14_8
	Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * ___U3CU3E9__14_8_9;
	// System.Action MHLab.PATCH.Install.Installer/<>c::<>9__14_9
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__14_9_10;
	// System.Action MHLab.PATCH.Install.Installer/<>c::<>9__14_10
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__14_10_11;
	// System.Action`3<System.Int64,System.Int64,System.Int32> MHLab.PATCH.Install.Installer/<>c::<>9__14_11
	Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 * ___U3CU3E9__14_11_12;
	// System.Action MHLab.PATCH.Install.Installer/<>c::<>9__14_12
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__14_12_13;
	// System.Comparison`1<MHLab.PATCH.Version> MHLab.PATCH.Install.Installer/<>c::<>9__24_0
	Comparison_1_tB62C7DFC32565B8EC7275A3E0AA32D03DEE6C38B * ___U3CU3E9__24_0_14;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields, ___U3CU3E9__14_0_1)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_U3CU3E9__14_0_1() const { return ___U3CU3E9__14_0_1; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_U3CU3E9__14_0_1() { return &___U3CU3E9__14_0_1; }
	inline void set_U3CU3E9__14_0_1(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___U3CU3E9__14_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields, ___U3CU3E9__14_1_2)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_U3CU3E9__14_1_2() const { return ___U3CU3E9__14_1_2; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_U3CU3E9__14_1_2() { return &___U3CU3E9__14_1_2; }
	inline void set_U3CU3E9__14_1_2(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___U3CU3E9__14_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields, ___U3CU3E9__14_2_3)); }
	inline Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * get_U3CU3E9__14_2_3() const { return ___U3CU3E9__14_2_3; }
	inline Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 ** get_address_of_U3CU3E9__14_2_3() { return &___U3CU3E9__14_2_3; }
	inline void set_U3CU3E9__14_2_3(Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * value)
	{
		___U3CU3E9__14_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields, ___U3CU3E9__14_3_4)); }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * get_U3CU3E9__14_3_4() const { return ___U3CU3E9__14_3_4; }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A ** get_address_of_U3CU3E9__14_3_4() { return &___U3CU3E9__14_3_4; }
	inline void set_U3CU3E9__14_3_4(Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * value)
	{
		___U3CU3E9__14_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_3_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_4_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields, ___U3CU3E9__14_4_5)); }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * get_U3CU3E9__14_4_5() const { return ___U3CU3E9__14_4_5; }
	inline Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A ** get_address_of_U3CU3E9__14_4_5() { return &___U3CU3E9__14_4_5; }
	inline void set_U3CU3E9__14_4_5(Action_3_tF26718CAF8D37903C04586AF0FB9C7F1211D283A * value)
	{
		___U3CU3E9__14_4_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_4_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_5_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields, ___U3CU3E9__14_5_6)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_U3CU3E9__14_5_6() const { return ___U3CU3E9__14_5_6; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_U3CU3E9__14_5_6() { return &___U3CU3E9__14_5_6; }
	inline void set_U3CU3E9__14_5_6(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___U3CU3E9__14_5_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_5_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_6_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields, ___U3CU3E9__14_6_7)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_U3CU3E9__14_6_7() const { return ___U3CU3E9__14_6_7; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_U3CU3E9__14_6_7() { return &___U3CU3E9__14_6_7; }
	inline void set_U3CU3E9__14_6_7(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___U3CU3E9__14_6_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_6_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_7_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields, ___U3CU3E9__14_7_8)); }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * get_U3CU3E9__14_7_8() const { return ___U3CU3E9__14_7_8; }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE ** get_address_of_U3CU3E9__14_7_8() { return &___U3CU3E9__14_7_8; }
	inline void set_U3CU3E9__14_7_8(Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * value)
	{
		___U3CU3E9__14_7_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_7_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_8_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields, ___U3CU3E9__14_8_9)); }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * get_U3CU3E9__14_8_9() const { return ___U3CU3E9__14_8_9; }
	inline Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE ** get_address_of_U3CU3E9__14_8_9() { return &___U3CU3E9__14_8_9; }
	inline void set_U3CU3E9__14_8_9(Action_2_t7F48DB8D71AB14B1331B4BB8EE28580F28191ACE * value)
	{
		___U3CU3E9__14_8_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_8_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_9_10() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields, ___U3CU3E9__14_9_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__14_9_10() const { return ___U3CU3E9__14_9_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__14_9_10() { return &___U3CU3E9__14_9_10; }
	inline void set_U3CU3E9__14_9_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__14_9_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_9_10), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_10_11() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields, ___U3CU3E9__14_10_11)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__14_10_11() const { return ___U3CU3E9__14_10_11; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__14_10_11() { return &___U3CU3E9__14_10_11; }
	inline void set_U3CU3E9__14_10_11(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__14_10_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_10_11), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_11_12() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields, ___U3CU3E9__14_11_12)); }
	inline Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 * get_U3CU3E9__14_11_12() const { return ___U3CU3E9__14_11_12; }
	inline Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 ** get_address_of_U3CU3E9__14_11_12() { return &___U3CU3E9__14_11_12; }
	inline void set_U3CU3E9__14_11_12(Action_3_tE236A2C025696B2B42E591086BC292FFB1B0E281 * value)
	{
		___U3CU3E9__14_11_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_11_12), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_12_13() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields, ___U3CU3E9__14_12_13)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__14_12_13() const { return ___U3CU3E9__14_12_13; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__14_12_13() { return &___U3CU3E9__14_12_13; }
	inline void set_U3CU3E9__14_12_13(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__14_12_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_12_13), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_0_14() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields, ___U3CU3E9__24_0_14)); }
	inline Comparison_1_tB62C7DFC32565B8EC7275A3E0AA32D03DEE6C38B * get_U3CU3E9__24_0_14() const { return ___U3CU3E9__24_0_14; }
	inline Comparison_1_tB62C7DFC32565B8EC7275A3E0AA32D03DEE6C38B ** get_address_of_U3CU3E9__24_0_14() { return &___U3CU3E9__24_0_14; }
	inline void set_U3CU3E9__24_0_14(Comparison_1_tB62C7DFC32565B8EC7275A3E0AA32D03DEE6C38B * value)
	{
		___U3CU3E9__24_0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_0_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TE05096997AC3C296BA928EE56F3E5365ADB8B8DC_H
#ifndef SETTINGSMANAGER_T617C17387413F4365C1732CCD3F763A86B9230B8_H
#define SETTINGSMANAGER_T617C17387413F4365C1732CCD3F763A86B9230B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Settings.SettingsManager
struct  SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8  : public RuntimeObject
{
public:

public:
};

struct SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields
{
public:
	// System.String MHLab.PATCH.Settings.SettingsManager::APP_PATH
	String_t* ___APP_PATH_0;
	// System.String MHLab.PATCH.Settings.SettingsManager::CURRENT_BUILD_PATH
	String_t* ___CURRENT_BUILD_PATH_1;
	// System.String MHLab.PATCH.Settings.SettingsManager::BUILDS_PATH
	String_t* ___BUILDS_PATH_2;
	// System.String MHLab.PATCH.Settings.SettingsManager::PATCHES_PATH
	String_t* ___PATCHES_PATH_3;
	// System.String MHLab.PATCH.Settings.SettingsManager::SIGNATURES_PATH
	String_t* ___SIGNATURES_PATH_4;
	// System.String MHLab.PATCH.Settings.SettingsManager::FINAL_PATCHES_PATH
	String_t* ___FINAL_PATCHES_PATH_5;
	// System.String MHLab.PATCH.Settings.SettingsManager::DEPLOY_PATH
	String_t* ___DEPLOY_PATH_6;
	// System.String MHLab.PATCH.Settings.SettingsManager::PATCHER_FILES_PATH
	String_t* ___PATCHER_FILES_PATH_7;
	// System.String MHLab.PATCH.Settings.SettingsManager::LOGS_ERROR_PATH
	String_t* ___LOGS_ERROR_PATH_8;
	// System.String MHLab.PATCH.Settings.SettingsManager::PATCHES_TMP_FOLDER
	String_t* ___PATCHES_TMP_FOLDER_9;
	// System.String MHLab.PATCH.Settings.SettingsManager::LAUNCHER_CONFIG_GENERATION_PATH
	String_t* ___LAUNCHER_CONFIG_GENERATION_PATH_10;
	// System.Char MHLab.PATCH.Settings.SettingsManager::PATCHES_SYMBOL_SEPARATOR
	Il2CppChar ___PATCHES_SYMBOL_SEPARATOR_11;
	// System.String MHLab.PATCH.Settings.SettingsManager::PATCH_VERSION_ENCRYPTION_PASSWORD
	String_t* ___PATCH_VERSION_ENCRYPTION_PASSWORD_12;
	// System.String MHLab.PATCH.Settings.SettingsManager::PATCH_EXTENSION
	String_t* ___PATCH_EXTENSION_13;
	// System.String MHLab.PATCH.Settings.SettingsManager::PATCH_DELETE_FILE_EXTENSION
	String_t* ___PATCH_DELETE_FILE_EXTENSION_14;
	// System.String MHLab.PATCH.Settings.SettingsManager::PATCH_VERSION_PATH
	String_t* ___PATCH_VERSION_PATH_15;
	// System.String MHLab.PATCH.Settings.SettingsManager::VERSIONS_FILE_DOWNLOAD_URL
	String_t* ___VERSIONS_FILE_DOWNLOAD_URL_16;
	// System.String MHLab.PATCH.Settings.SettingsManager::PATCHES_DOWNLOAD_URL
	String_t* ___PATCHES_DOWNLOAD_URL_17;
	// System.String MHLab.PATCH.Settings.SettingsManager::BUILDS_DOWNLOAD_URL
	String_t* ___BUILDS_DOWNLOAD_URL_18;
	// System.String MHLab.PATCH.Settings.SettingsManager::PATCHER_DOWNLOAD_URL
	String_t* ___PATCHER_DOWNLOAD_URL_19;
	// System.UInt16 MHLab.PATCH.Settings.SettingsManager::PATCH_DOWNLOAD_RETRY_ATTEMPTS
	uint16_t ___PATCH_DOWNLOAD_RETRY_ATTEMPTS_20;
	// System.UInt16 MHLab.PATCH.Settings.SettingsManager::FILE_DELETE_RETRY_ATTEMPTS
	uint16_t ___FILE_DELETE_RETRY_ATTEMPTS_21;
	// System.String MHLab.PATCH.Settings.SettingsManager::LAUNCH_APP
	String_t* ___LAUNCH_APP_22;
	// System.String MHLab.PATCH.Settings.SettingsManager::LAUNCHER_NAME
	String_t* ___LAUNCHER_NAME_23;
	// System.String MHLab.PATCH.Settings.SettingsManager::LAUNCH_ARG
	String_t* ___LAUNCH_ARG_24;
	// System.String MHLab.PATCH.Settings.SettingsManager::LAUNCH_COMMAND
	String_t* ___LAUNCH_COMMAND_25;
	// System.Boolean MHLab.PATCH.Settings.SettingsManager::USE_RAW_LAUNCH_ARG
	bool ___USE_RAW_LAUNCH_ARG_26;
	// System.Boolean MHLab.PATCH.Settings.SettingsManager::INSTALL_IN_LOCAL_PATH
	bool ___INSTALL_IN_LOCAL_PATH_27;
	// System.String MHLab.PATCH.Settings.SettingsManager::PROGRAM_FILES_DIRECTORY_TO_INSTALL
	String_t* ___PROGRAM_FILES_DIRECTORY_TO_INSTALL_28;
	// System.String MHLab.PATCH.Settings.SettingsManager::PROGRAM_FILES_DIRECTORY_TO_INSTALL_ABS_PATH
	String_t* ___PROGRAM_FILES_DIRECTORY_TO_INSTALL_ABS_PATH_29;
	// System.String MHLab.PATCH.Settings.SettingsManager::LAUNCHER_CONFIG_PATH
	String_t* ___LAUNCHER_CONFIG_PATH_30;
	// System.String MHLab.PATCH.Settings.SettingsManager::PATCH_SAFE_BACKUP
	String_t* ___PATCH_SAFE_BACKUP_31;
	// System.String MHLab.PATCH.Settings.SettingsManager::VERSION_FILE_LOCAL_PATH
	String_t* ___VERSION_FILE_LOCAL_PATH_32;
	// System.Boolean MHLab.PATCH.Settings.SettingsManager::ENABLE_FTP
	bool ___ENABLE_FTP_33;
	// System.String MHLab.PATCH.Settings.SettingsManager::FTP_USERNAME
	String_t* ___FTP_USERNAME_34;
	// System.String MHLab.PATCH.Settings.SettingsManager::FTP_PASSWORD
	String_t* ___FTP_PASSWORD_35;
	// System.Boolean MHLab.PATCH.Settings.SettingsManager::ENABLE_PATCHER
	bool ___ENABLE_PATCHER_36;
	// System.Boolean MHLab.PATCH.Settings.SettingsManager::ENABLE_INSTALLER
	bool ___ENABLE_INSTALLER_37;
	// System.Boolean MHLab.PATCH.Settings.SettingsManager::ENABLE_REPAIRER
	bool ___ENABLE_REPAIRER_38;
	// System.Boolean MHLab.PATCH.Settings.SettingsManager::INSTALL_PATCHER
	bool ___INSTALL_PATCHER_39;
	// System.Boolean MHLab.PATCH.Settings.SettingsManager::CREATE_DESKTOP_SHORTCUT
	bool ___CREATE_DESKTOP_SHORTCUT_40;
	// System.String MHLab.PATCH.Settings.SettingsManager::PATCHNOTES_URL
	String_t* ___PATCHNOTES_URL_41;
	// System.String MHLab.PATCH.Settings.SettingsManager::NEWS_URL
	String_t* ___NEWS_URL_42;
	// System.Int32 MHLab.PATCH.Settings.SettingsManager::DOWNLOAD_BUFFER_SIZE
	int32_t ___DOWNLOAD_BUFFER_SIZE_43;

public:
	inline static int32_t get_offset_of_APP_PATH_0() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___APP_PATH_0)); }
	inline String_t* get_APP_PATH_0() const { return ___APP_PATH_0; }
	inline String_t** get_address_of_APP_PATH_0() { return &___APP_PATH_0; }
	inline void set_APP_PATH_0(String_t* value)
	{
		___APP_PATH_0 = value;
		Il2CppCodeGenWriteBarrier((&___APP_PATH_0), value);
	}

	inline static int32_t get_offset_of_CURRENT_BUILD_PATH_1() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___CURRENT_BUILD_PATH_1)); }
	inline String_t* get_CURRENT_BUILD_PATH_1() const { return ___CURRENT_BUILD_PATH_1; }
	inline String_t** get_address_of_CURRENT_BUILD_PATH_1() { return &___CURRENT_BUILD_PATH_1; }
	inline void set_CURRENT_BUILD_PATH_1(String_t* value)
	{
		___CURRENT_BUILD_PATH_1 = value;
		Il2CppCodeGenWriteBarrier((&___CURRENT_BUILD_PATH_1), value);
	}

	inline static int32_t get_offset_of_BUILDS_PATH_2() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___BUILDS_PATH_2)); }
	inline String_t* get_BUILDS_PATH_2() const { return ___BUILDS_PATH_2; }
	inline String_t** get_address_of_BUILDS_PATH_2() { return &___BUILDS_PATH_2; }
	inline void set_BUILDS_PATH_2(String_t* value)
	{
		___BUILDS_PATH_2 = value;
		Il2CppCodeGenWriteBarrier((&___BUILDS_PATH_2), value);
	}

	inline static int32_t get_offset_of_PATCHES_PATH_3() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___PATCHES_PATH_3)); }
	inline String_t* get_PATCHES_PATH_3() const { return ___PATCHES_PATH_3; }
	inline String_t** get_address_of_PATCHES_PATH_3() { return &___PATCHES_PATH_3; }
	inline void set_PATCHES_PATH_3(String_t* value)
	{
		___PATCHES_PATH_3 = value;
		Il2CppCodeGenWriteBarrier((&___PATCHES_PATH_3), value);
	}

	inline static int32_t get_offset_of_SIGNATURES_PATH_4() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___SIGNATURES_PATH_4)); }
	inline String_t* get_SIGNATURES_PATH_4() const { return ___SIGNATURES_PATH_4; }
	inline String_t** get_address_of_SIGNATURES_PATH_4() { return &___SIGNATURES_PATH_4; }
	inline void set_SIGNATURES_PATH_4(String_t* value)
	{
		___SIGNATURES_PATH_4 = value;
		Il2CppCodeGenWriteBarrier((&___SIGNATURES_PATH_4), value);
	}

	inline static int32_t get_offset_of_FINAL_PATCHES_PATH_5() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___FINAL_PATCHES_PATH_5)); }
	inline String_t* get_FINAL_PATCHES_PATH_5() const { return ___FINAL_PATCHES_PATH_5; }
	inline String_t** get_address_of_FINAL_PATCHES_PATH_5() { return &___FINAL_PATCHES_PATH_5; }
	inline void set_FINAL_PATCHES_PATH_5(String_t* value)
	{
		___FINAL_PATCHES_PATH_5 = value;
		Il2CppCodeGenWriteBarrier((&___FINAL_PATCHES_PATH_5), value);
	}

	inline static int32_t get_offset_of_DEPLOY_PATH_6() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___DEPLOY_PATH_6)); }
	inline String_t* get_DEPLOY_PATH_6() const { return ___DEPLOY_PATH_6; }
	inline String_t** get_address_of_DEPLOY_PATH_6() { return &___DEPLOY_PATH_6; }
	inline void set_DEPLOY_PATH_6(String_t* value)
	{
		___DEPLOY_PATH_6 = value;
		Il2CppCodeGenWriteBarrier((&___DEPLOY_PATH_6), value);
	}

	inline static int32_t get_offset_of_PATCHER_FILES_PATH_7() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___PATCHER_FILES_PATH_7)); }
	inline String_t* get_PATCHER_FILES_PATH_7() const { return ___PATCHER_FILES_PATH_7; }
	inline String_t** get_address_of_PATCHER_FILES_PATH_7() { return &___PATCHER_FILES_PATH_7; }
	inline void set_PATCHER_FILES_PATH_7(String_t* value)
	{
		___PATCHER_FILES_PATH_7 = value;
		Il2CppCodeGenWriteBarrier((&___PATCHER_FILES_PATH_7), value);
	}

	inline static int32_t get_offset_of_LOGS_ERROR_PATH_8() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___LOGS_ERROR_PATH_8)); }
	inline String_t* get_LOGS_ERROR_PATH_8() const { return ___LOGS_ERROR_PATH_8; }
	inline String_t** get_address_of_LOGS_ERROR_PATH_8() { return &___LOGS_ERROR_PATH_8; }
	inline void set_LOGS_ERROR_PATH_8(String_t* value)
	{
		___LOGS_ERROR_PATH_8 = value;
		Il2CppCodeGenWriteBarrier((&___LOGS_ERROR_PATH_8), value);
	}

	inline static int32_t get_offset_of_PATCHES_TMP_FOLDER_9() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___PATCHES_TMP_FOLDER_9)); }
	inline String_t* get_PATCHES_TMP_FOLDER_9() const { return ___PATCHES_TMP_FOLDER_9; }
	inline String_t** get_address_of_PATCHES_TMP_FOLDER_9() { return &___PATCHES_TMP_FOLDER_9; }
	inline void set_PATCHES_TMP_FOLDER_9(String_t* value)
	{
		___PATCHES_TMP_FOLDER_9 = value;
		Il2CppCodeGenWriteBarrier((&___PATCHES_TMP_FOLDER_9), value);
	}

	inline static int32_t get_offset_of_LAUNCHER_CONFIG_GENERATION_PATH_10() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___LAUNCHER_CONFIG_GENERATION_PATH_10)); }
	inline String_t* get_LAUNCHER_CONFIG_GENERATION_PATH_10() const { return ___LAUNCHER_CONFIG_GENERATION_PATH_10; }
	inline String_t** get_address_of_LAUNCHER_CONFIG_GENERATION_PATH_10() { return &___LAUNCHER_CONFIG_GENERATION_PATH_10; }
	inline void set_LAUNCHER_CONFIG_GENERATION_PATH_10(String_t* value)
	{
		___LAUNCHER_CONFIG_GENERATION_PATH_10 = value;
		Il2CppCodeGenWriteBarrier((&___LAUNCHER_CONFIG_GENERATION_PATH_10), value);
	}

	inline static int32_t get_offset_of_PATCHES_SYMBOL_SEPARATOR_11() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___PATCHES_SYMBOL_SEPARATOR_11)); }
	inline Il2CppChar get_PATCHES_SYMBOL_SEPARATOR_11() const { return ___PATCHES_SYMBOL_SEPARATOR_11; }
	inline Il2CppChar* get_address_of_PATCHES_SYMBOL_SEPARATOR_11() { return &___PATCHES_SYMBOL_SEPARATOR_11; }
	inline void set_PATCHES_SYMBOL_SEPARATOR_11(Il2CppChar value)
	{
		___PATCHES_SYMBOL_SEPARATOR_11 = value;
	}

	inline static int32_t get_offset_of_PATCH_VERSION_ENCRYPTION_PASSWORD_12() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___PATCH_VERSION_ENCRYPTION_PASSWORD_12)); }
	inline String_t* get_PATCH_VERSION_ENCRYPTION_PASSWORD_12() const { return ___PATCH_VERSION_ENCRYPTION_PASSWORD_12; }
	inline String_t** get_address_of_PATCH_VERSION_ENCRYPTION_PASSWORD_12() { return &___PATCH_VERSION_ENCRYPTION_PASSWORD_12; }
	inline void set_PATCH_VERSION_ENCRYPTION_PASSWORD_12(String_t* value)
	{
		___PATCH_VERSION_ENCRYPTION_PASSWORD_12 = value;
		Il2CppCodeGenWriteBarrier((&___PATCH_VERSION_ENCRYPTION_PASSWORD_12), value);
	}

	inline static int32_t get_offset_of_PATCH_EXTENSION_13() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___PATCH_EXTENSION_13)); }
	inline String_t* get_PATCH_EXTENSION_13() const { return ___PATCH_EXTENSION_13; }
	inline String_t** get_address_of_PATCH_EXTENSION_13() { return &___PATCH_EXTENSION_13; }
	inline void set_PATCH_EXTENSION_13(String_t* value)
	{
		___PATCH_EXTENSION_13 = value;
		Il2CppCodeGenWriteBarrier((&___PATCH_EXTENSION_13), value);
	}

	inline static int32_t get_offset_of_PATCH_DELETE_FILE_EXTENSION_14() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___PATCH_DELETE_FILE_EXTENSION_14)); }
	inline String_t* get_PATCH_DELETE_FILE_EXTENSION_14() const { return ___PATCH_DELETE_FILE_EXTENSION_14; }
	inline String_t** get_address_of_PATCH_DELETE_FILE_EXTENSION_14() { return &___PATCH_DELETE_FILE_EXTENSION_14; }
	inline void set_PATCH_DELETE_FILE_EXTENSION_14(String_t* value)
	{
		___PATCH_DELETE_FILE_EXTENSION_14 = value;
		Il2CppCodeGenWriteBarrier((&___PATCH_DELETE_FILE_EXTENSION_14), value);
	}

	inline static int32_t get_offset_of_PATCH_VERSION_PATH_15() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___PATCH_VERSION_PATH_15)); }
	inline String_t* get_PATCH_VERSION_PATH_15() const { return ___PATCH_VERSION_PATH_15; }
	inline String_t** get_address_of_PATCH_VERSION_PATH_15() { return &___PATCH_VERSION_PATH_15; }
	inline void set_PATCH_VERSION_PATH_15(String_t* value)
	{
		___PATCH_VERSION_PATH_15 = value;
		Il2CppCodeGenWriteBarrier((&___PATCH_VERSION_PATH_15), value);
	}

	inline static int32_t get_offset_of_VERSIONS_FILE_DOWNLOAD_URL_16() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___VERSIONS_FILE_DOWNLOAD_URL_16)); }
	inline String_t* get_VERSIONS_FILE_DOWNLOAD_URL_16() const { return ___VERSIONS_FILE_DOWNLOAD_URL_16; }
	inline String_t** get_address_of_VERSIONS_FILE_DOWNLOAD_URL_16() { return &___VERSIONS_FILE_DOWNLOAD_URL_16; }
	inline void set_VERSIONS_FILE_DOWNLOAD_URL_16(String_t* value)
	{
		___VERSIONS_FILE_DOWNLOAD_URL_16 = value;
		Il2CppCodeGenWriteBarrier((&___VERSIONS_FILE_DOWNLOAD_URL_16), value);
	}

	inline static int32_t get_offset_of_PATCHES_DOWNLOAD_URL_17() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___PATCHES_DOWNLOAD_URL_17)); }
	inline String_t* get_PATCHES_DOWNLOAD_URL_17() const { return ___PATCHES_DOWNLOAD_URL_17; }
	inline String_t** get_address_of_PATCHES_DOWNLOAD_URL_17() { return &___PATCHES_DOWNLOAD_URL_17; }
	inline void set_PATCHES_DOWNLOAD_URL_17(String_t* value)
	{
		___PATCHES_DOWNLOAD_URL_17 = value;
		Il2CppCodeGenWriteBarrier((&___PATCHES_DOWNLOAD_URL_17), value);
	}

	inline static int32_t get_offset_of_BUILDS_DOWNLOAD_URL_18() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___BUILDS_DOWNLOAD_URL_18)); }
	inline String_t* get_BUILDS_DOWNLOAD_URL_18() const { return ___BUILDS_DOWNLOAD_URL_18; }
	inline String_t** get_address_of_BUILDS_DOWNLOAD_URL_18() { return &___BUILDS_DOWNLOAD_URL_18; }
	inline void set_BUILDS_DOWNLOAD_URL_18(String_t* value)
	{
		___BUILDS_DOWNLOAD_URL_18 = value;
		Il2CppCodeGenWriteBarrier((&___BUILDS_DOWNLOAD_URL_18), value);
	}

	inline static int32_t get_offset_of_PATCHER_DOWNLOAD_URL_19() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___PATCHER_DOWNLOAD_URL_19)); }
	inline String_t* get_PATCHER_DOWNLOAD_URL_19() const { return ___PATCHER_DOWNLOAD_URL_19; }
	inline String_t** get_address_of_PATCHER_DOWNLOAD_URL_19() { return &___PATCHER_DOWNLOAD_URL_19; }
	inline void set_PATCHER_DOWNLOAD_URL_19(String_t* value)
	{
		___PATCHER_DOWNLOAD_URL_19 = value;
		Il2CppCodeGenWriteBarrier((&___PATCHER_DOWNLOAD_URL_19), value);
	}

	inline static int32_t get_offset_of_PATCH_DOWNLOAD_RETRY_ATTEMPTS_20() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___PATCH_DOWNLOAD_RETRY_ATTEMPTS_20)); }
	inline uint16_t get_PATCH_DOWNLOAD_RETRY_ATTEMPTS_20() const { return ___PATCH_DOWNLOAD_RETRY_ATTEMPTS_20; }
	inline uint16_t* get_address_of_PATCH_DOWNLOAD_RETRY_ATTEMPTS_20() { return &___PATCH_DOWNLOAD_RETRY_ATTEMPTS_20; }
	inline void set_PATCH_DOWNLOAD_RETRY_ATTEMPTS_20(uint16_t value)
	{
		___PATCH_DOWNLOAD_RETRY_ATTEMPTS_20 = value;
	}

	inline static int32_t get_offset_of_FILE_DELETE_RETRY_ATTEMPTS_21() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___FILE_DELETE_RETRY_ATTEMPTS_21)); }
	inline uint16_t get_FILE_DELETE_RETRY_ATTEMPTS_21() const { return ___FILE_DELETE_RETRY_ATTEMPTS_21; }
	inline uint16_t* get_address_of_FILE_DELETE_RETRY_ATTEMPTS_21() { return &___FILE_DELETE_RETRY_ATTEMPTS_21; }
	inline void set_FILE_DELETE_RETRY_ATTEMPTS_21(uint16_t value)
	{
		___FILE_DELETE_RETRY_ATTEMPTS_21 = value;
	}

	inline static int32_t get_offset_of_LAUNCH_APP_22() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___LAUNCH_APP_22)); }
	inline String_t* get_LAUNCH_APP_22() const { return ___LAUNCH_APP_22; }
	inline String_t** get_address_of_LAUNCH_APP_22() { return &___LAUNCH_APP_22; }
	inline void set_LAUNCH_APP_22(String_t* value)
	{
		___LAUNCH_APP_22 = value;
		Il2CppCodeGenWriteBarrier((&___LAUNCH_APP_22), value);
	}

	inline static int32_t get_offset_of_LAUNCHER_NAME_23() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___LAUNCHER_NAME_23)); }
	inline String_t* get_LAUNCHER_NAME_23() const { return ___LAUNCHER_NAME_23; }
	inline String_t** get_address_of_LAUNCHER_NAME_23() { return &___LAUNCHER_NAME_23; }
	inline void set_LAUNCHER_NAME_23(String_t* value)
	{
		___LAUNCHER_NAME_23 = value;
		Il2CppCodeGenWriteBarrier((&___LAUNCHER_NAME_23), value);
	}

	inline static int32_t get_offset_of_LAUNCH_ARG_24() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___LAUNCH_ARG_24)); }
	inline String_t* get_LAUNCH_ARG_24() const { return ___LAUNCH_ARG_24; }
	inline String_t** get_address_of_LAUNCH_ARG_24() { return &___LAUNCH_ARG_24; }
	inline void set_LAUNCH_ARG_24(String_t* value)
	{
		___LAUNCH_ARG_24 = value;
		Il2CppCodeGenWriteBarrier((&___LAUNCH_ARG_24), value);
	}

	inline static int32_t get_offset_of_LAUNCH_COMMAND_25() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___LAUNCH_COMMAND_25)); }
	inline String_t* get_LAUNCH_COMMAND_25() const { return ___LAUNCH_COMMAND_25; }
	inline String_t** get_address_of_LAUNCH_COMMAND_25() { return &___LAUNCH_COMMAND_25; }
	inline void set_LAUNCH_COMMAND_25(String_t* value)
	{
		___LAUNCH_COMMAND_25 = value;
		Il2CppCodeGenWriteBarrier((&___LAUNCH_COMMAND_25), value);
	}

	inline static int32_t get_offset_of_USE_RAW_LAUNCH_ARG_26() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___USE_RAW_LAUNCH_ARG_26)); }
	inline bool get_USE_RAW_LAUNCH_ARG_26() const { return ___USE_RAW_LAUNCH_ARG_26; }
	inline bool* get_address_of_USE_RAW_LAUNCH_ARG_26() { return &___USE_RAW_LAUNCH_ARG_26; }
	inline void set_USE_RAW_LAUNCH_ARG_26(bool value)
	{
		___USE_RAW_LAUNCH_ARG_26 = value;
	}

	inline static int32_t get_offset_of_INSTALL_IN_LOCAL_PATH_27() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___INSTALL_IN_LOCAL_PATH_27)); }
	inline bool get_INSTALL_IN_LOCAL_PATH_27() const { return ___INSTALL_IN_LOCAL_PATH_27; }
	inline bool* get_address_of_INSTALL_IN_LOCAL_PATH_27() { return &___INSTALL_IN_LOCAL_PATH_27; }
	inline void set_INSTALL_IN_LOCAL_PATH_27(bool value)
	{
		___INSTALL_IN_LOCAL_PATH_27 = value;
	}

	inline static int32_t get_offset_of_PROGRAM_FILES_DIRECTORY_TO_INSTALL_28() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___PROGRAM_FILES_DIRECTORY_TO_INSTALL_28)); }
	inline String_t* get_PROGRAM_FILES_DIRECTORY_TO_INSTALL_28() const { return ___PROGRAM_FILES_DIRECTORY_TO_INSTALL_28; }
	inline String_t** get_address_of_PROGRAM_FILES_DIRECTORY_TO_INSTALL_28() { return &___PROGRAM_FILES_DIRECTORY_TO_INSTALL_28; }
	inline void set_PROGRAM_FILES_DIRECTORY_TO_INSTALL_28(String_t* value)
	{
		___PROGRAM_FILES_DIRECTORY_TO_INSTALL_28 = value;
		Il2CppCodeGenWriteBarrier((&___PROGRAM_FILES_DIRECTORY_TO_INSTALL_28), value);
	}

	inline static int32_t get_offset_of_PROGRAM_FILES_DIRECTORY_TO_INSTALL_ABS_PATH_29() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___PROGRAM_FILES_DIRECTORY_TO_INSTALL_ABS_PATH_29)); }
	inline String_t* get_PROGRAM_FILES_DIRECTORY_TO_INSTALL_ABS_PATH_29() const { return ___PROGRAM_FILES_DIRECTORY_TO_INSTALL_ABS_PATH_29; }
	inline String_t** get_address_of_PROGRAM_FILES_DIRECTORY_TO_INSTALL_ABS_PATH_29() { return &___PROGRAM_FILES_DIRECTORY_TO_INSTALL_ABS_PATH_29; }
	inline void set_PROGRAM_FILES_DIRECTORY_TO_INSTALL_ABS_PATH_29(String_t* value)
	{
		___PROGRAM_FILES_DIRECTORY_TO_INSTALL_ABS_PATH_29 = value;
		Il2CppCodeGenWriteBarrier((&___PROGRAM_FILES_DIRECTORY_TO_INSTALL_ABS_PATH_29), value);
	}

	inline static int32_t get_offset_of_LAUNCHER_CONFIG_PATH_30() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___LAUNCHER_CONFIG_PATH_30)); }
	inline String_t* get_LAUNCHER_CONFIG_PATH_30() const { return ___LAUNCHER_CONFIG_PATH_30; }
	inline String_t** get_address_of_LAUNCHER_CONFIG_PATH_30() { return &___LAUNCHER_CONFIG_PATH_30; }
	inline void set_LAUNCHER_CONFIG_PATH_30(String_t* value)
	{
		___LAUNCHER_CONFIG_PATH_30 = value;
		Il2CppCodeGenWriteBarrier((&___LAUNCHER_CONFIG_PATH_30), value);
	}

	inline static int32_t get_offset_of_PATCH_SAFE_BACKUP_31() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___PATCH_SAFE_BACKUP_31)); }
	inline String_t* get_PATCH_SAFE_BACKUP_31() const { return ___PATCH_SAFE_BACKUP_31; }
	inline String_t** get_address_of_PATCH_SAFE_BACKUP_31() { return &___PATCH_SAFE_BACKUP_31; }
	inline void set_PATCH_SAFE_BACKUP_31(String_t* value)
	{
		___PATCH_SAFE_BACKUP_31 = value;
		Il2CppCodeGenWriteBarrier((&___PATCH_SAFE_BACKUP_31), value);
	}

	inline static int32_t get_offset_of_VERSION_FILE_LOCAL_PATH_32() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___VERSION_FILE_LOCAL_PATH_32)); }
	inline String_t* get_VERSION_FILE_LOCAL_PATH_32() const { return ___VERSION_FILE_LOCAL_PATH_32; }
	inline String_t** get_address_of_VERSION_FILE_LOCAL_PATH_32() { return &___VERSION_FILE_LOCAL_PATH_32; }
	inline void set_VERSION_FILE_LOCAL_PATH_32(String_t* value)
	{
		___VERSION_FILE_LOCAL_PATH_32 = value;
		Il2CppCodeGenWriteBarrier((&___VERSION_FILE_LOCAL_PATH_32), value);
	}

	inline static int32_t get_offset_of_ENABLE_FTP_33() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___ENABLE_FTP_33)); }
	inline bool get_ENABLE_FTP_33() const { return ___ENABLE_FTP_33; }
	inline bool* get_address_of_ENABLE_FTP_33() { return &___ENABLE_FTP_33; }
	inline void set_ENABLE_FTP_33(bool value)
	{
		___ENABLE_FTP_33 = value;
	}

	inline static int32_t get_offset_of_FTP_USERNAME_34() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___FTP_USERNAME_34)); }
	inline String_t* get_FTP_USERNAME_34() const { return ___FTP_USERNAME_34; }
	inline String_t** get_address_of_FTP_USERNAME_34() { return &___FTP_USERNAME_34; }
	inline void set_FTP_USERNAME_34(String_t* value)
	{
		___FTP_USERNAME_34 = value;
		Il2CppCodeGenWriteBarrier((&___FTP_USERNAME_34), value);
	}

	inline static int32_t get_offset_of_FTP_PASSWORD_35() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___FTP_PASSWORD_35)); }
	inline String_t* get_FTP_PASSWORD_35() const { return ___FTP_PASSWORD_35; }
	inline String_t** get_address_of_FTP_PASSWORD_35() { return &___FTP_PASSWORD_35; }
	inline void set_FTP_PASSWORD_35(String_t* value)
	{
		___FTP_PASSWORD_35 = value;
		Il2CppCodeGenWriteBarrier((&___FTP_PASSWORD_35), value);
	}

	inline static int32_t get_offset_of_ENABLE_PATCHER_36() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___ENABLE_PATCHER_36)); }
	inline bool get_ENABLE_PATCHER_36() const { return ___ENABLE_PATCHER_36; }
	inline bool* get_address_of_ENABLE_PATCHER_36() { return &___ENABLE_PATCHER_36; }
	inline void set_ENABLE_PATCHER_36(bool value)
	{
		___ENABLE_PATCHER_36 = value;
	}

	inline static int32_t get_offset_of_ENABLE_INSTALLER_37() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___ENABLE_INSTALLER_37)); }
	inline bool get_ENABLE_INSTALLER_37() const { return ___ENABLE_INSTALLER_37; }
	inline bool* get_address_of_ENABLE_INSTALLER_37() { return &___ENABLE_INSTALLER_37; }
	inline void set_ENABLE_INSTALLER_37(bool value)
	{
		___ENABLE_INSTALLER_37 = value;
	}

	inline static int32_t get_offset_of_ENABLE_REPAIRER_38() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___ENABLE_REPAIRER_38)); }
	inline bool get_ENABLE_REPAIRER_38() const { return ___ENABLE_REPAIRER_38; }
	inline bool* get_address_of_ENABLE_REPAIRER_38() { return &___ENABLE_REPAIRER_38; }
	inline void set_ENABLE_REPAIRER_38(bool value)
	{
		___ENABLE_REPAIRER_38 = value;
	}

	inline static int32_t get_offset_of_INSTALL_PATCHER_39() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___INSTALL_PATCHER_39)); }
	inline bool get_INSTALL_PATCHER_39() const { return ___INSTALL_PATCHER_39; }
	inline bool* get_address_of_INSTALL_PATCHER_39() { return &___INSTALL_PATCHER_39; }
	inline void set_INSTALL_PATCHER_39(bool value)
	{
		___INSTALL_PATCHER_39 = value;
	}

	inline static int32_t get_offset_of_CREATE_DESKTOP_SHORTCUT_40() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___CREATE_DESKTOP_SHORTCUT_40)); }
	inline bool get_CREATE_DESKTOP_SHORTCUT_40() const { return ___CREATE_DESKTOP_SHORTCUT_40; }
	inline bool* get_address_of_CREATE_DESKTOP_SHORTCUT_40() { return &___CREATE_DESKTOP_SHORTCUT_40; }
	inline void set_CREATE_DESKTOP_SHORTCUT_40(bool value)
	{
		___CREATE_DESKTOP_SHORTCUT_40 = value;
	}

	inline static int32_t get_offset_of_PATCHNOTES_URL_41() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___PATCHNOTES_URL_41)); }
	inline String_t* get_PATCHNOTES_URL_41() const { return ___PATCHNOTES_URL_41; }
	inline String_t** get_address_of_PATCHNOTES_URL_41() { return &___PATCHNOTES_URL_41; }
	inline void set_PATCHNOTES_URL_41(String_t* value)
	{
		___PATCHNOTES_URL_41 = value;
		Il2CppCodeGenWriteBarrier((&___PATCHNOTES_URL_41), value);
	}

	inline static int32_t get_offset_of_NEWS_URL_42() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___NEWS_URL_42)); }
	inline String_t* get_NEWS_URL_42() const { return ___NEWS_URL_42; }
	inline String_t** get_address_of_NEWS_URL_42() { return &___NEWS_URL_42; }
	inline void set_NEWS_URL_42(String_t* value)
	{
		___NEWS_URL_42 = value;
		Il2CppCodeGenWriteBarrier((&___NEWS_URL_42), value);
	}

	inline static int32_t get_offset_of_DOWNLOAD_BUFFER_SIZE_43() { return static_cast<int32_t>(offsetof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields, ___DOWNLOAD_BUFFER_SIZE_43)); }
	inline int32_t get_DOWNLOAD_BUFFER_SIZE_43() const { return ___DOWNLOAD_BUFFER_SIZE_43; }
	inline int32_t* get_address_of_DOWNLOAD_BUFFER_SIZE_43() { return &___DOWNLOAD_BUFFER_SIZE_43; }
	inline void set_DOWNLOAD_BUFFER_SIZE_43(int32_t value)
	{
		___DOWNLOAD_BUFFER_SIZE_43 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSMANAGER_T617C17387413F4365C1732CCD3F763A86B9230B8_H
#ifndef SETTINGSOVERRIDER_T496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C_H
#define SETTINGSOVERRIDER_T496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Settings.SettingsOverrider
struct  SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C  : public RuntimeObject
{
public:
	// System.String MHLab.PATCH.Settings.SettingsOverrider::VERSIONS_FILE_DOWNLOAD_URL
	String_t* ___VERSIONS_FILE_DOWNLOAD_URL_0;
	// System.String MHLab.PATCH.Settings.SettingsOverrider::PATCHES_DOWNLOAD_URL
	String_t* ___PATCHES_DOWNLOAD_URL_1;
	// System.String MHLab.PATCH.Settings.SettingsOverrider::BUILDS_DOWNLOAD_URL
	String_t* ___BUILDS_DOWNLOAD_URL_2;
	// System.String MHLab.PATCH.Settings.SettingsOverrider::PATCHER_DOWNLOAD_URL
	String_t* ___PATCHER_DOWNLOAD_URL_3;
	// System.UInt16 MHLab.PATCH.Settings.SettingsOverrider::PATCH_DOWNLOAD_RETRY_ATTEMPTS
	uint16_t ___PATCH_DOWNLOAD_RETRY_ATTEMPTS_4;
	// System.String MHLab.PATCH.Settings.SettingsOverrider::PATCHNOTES_URL
	String_t* ___PATCHNOTES_URL_5;
	// System.String MHLab.PATCH.Settings.SettingsOverrider::NEWS_URL
	String_t* ___NEWS_URL_6;
	// System.String MHLab.PATCH.Settings.SettingsOverrider::LAUNCH_APP
	String_t* ___LAUNCH_APP_7;
	// System.String MHLab.PATCH.Settings.SettingsOverrider::LAUNCHER_NAME
	String_t* ___LAUNCHER_NAME_8;
	// System.String MHLab.PATCH.Settings.SettingsOverrider::LAUNCH_ARG
	String_t* ___LAUNCH_ARG_9;
	// System.Boolean MHLab.PATCH.Settings.SettingsOverrider::USE_RAW_LAUNCH
	bool ___USE_RAW_LAUNCH_10;
	// System.Boolean MHLab.PATCH.Settings.SettingsOverrider::ENABLE_FTP
	bool ___ENABLE_FTP_11;
	// System.String MHLab.PATCH.Settings.SettingsOverrider::FTP_USERNAME
	String_t* ___FTP_USERNAME_12;
	// System.String MHLab.PATCH.Settings.SettingsOverrider::FTP_PASSWORD
	String_t* ___FTP_PASSWORD_13;
	// System.Boolean MHLab.PATCH.Settings.SettingsOverrider::INSTALL_IN_LOCAL_PATH
	bool ___INSTALL_IN_LOCAL_PATH_14;
	// System.String MHLab.PATCH.Settings.SettingsOverrider::PROGRAM_FILES_DIRECTORY_TO_INSTALL
	String_t* ___PROGRAM_FILES_DIRECTORY_TO_INSTALL_15;
	// System.Boolean MHLab.PATCH.Settings.SettingsOverrider::ENABLE_PATCHER
	bool ___ENABLE_PATCHER_16;
	// System.Boolean MHLab.PATCH.Settings.SettingsOverrider::ENABLE_INSTALLER
	bool ___ENABLE_INSTALLER_17;
	// System.Boolean MHLab.PATCH.Settings.SettingsOverrider::ENABLE_REPAIRER
	bool ___ENABLE_REPAIRER_18;
	// System.Boolean MHLab.PATCH.Settings.SettingsOverrider::INSTALL_PATCHER
	bool ___INSTALL_PATCHER_19;
	// System.Boolean MHLab.PATCH.Settings.SettingsOverrider::CREATE_DESKTOP_SHORTCUT
	bool ___CREATE_DESKTOP_SHORTCUT_20;

public:
	inline static int32_t get_offset_of_VERSIONS_FILE_DOWNLOAD_URL_0() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___VERSIONS_FILE_DOWNLOAD_URL_0)); }
	inline String_t* get_VERSIONS_FILE_DOWNLOAD_URL_0() const { return ___VERSIONS_FILE_DOWNLOAD_URL_0; }
	inline String_t** get_address_of_VERSIONS_FILE_DOWNLOAD_URL_0() { return &___VERSIONS_FILE_DOWNLOAD_URL_0; }
	inline void set_VERSIONS_FILE_DOWNLOAD_URL_0(String_t* value)
	{
		___VERSIONS_FILE_DOWNLOAD_URL_0 = value;
		Il2CppCodeGenWriteBarrier((&___VERSIONS_FILE_DOWNLOAD_URL_0), value);
	}

	inline static int32_t get_offset_of_PATCHES_DOWNLOAD_URL_1() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___PATCHES_DOWNLOAD_URL_1)); }
	inline String_t* get_PATCHES_DOWNLOAD_URL_1() const { return ___PATCHES_DOWNLOAD_URL_1; }
	inline String_t** get_address_of_PATCHES_DOWNLOAD_URL_1() { return &___PATCHES_DOWNLOAD_URL_1; }
	inline void set_PATCHES_DOWNLOAD_URL_1(String_t* value)
	{
		___PATCHES_DOWNLOAD_URL_1 = value;
		Il2CppCodeGenWriteBarrier((&___PATCHES_DOWNLOAD_URL_1), value);
	}

	inline static int32_t get_offset_of_BUILDS_DOWNLOAD_URL_2() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___BUILDS_DOWNLOAD_URL_2)); }
	inline String_t* get_BUILDS_DOWNLOAD_URL_2() const { return ___BUILDS_DOWNLOAD_URL_2; }
	inline String_t** get_address_of_BUILDS_DOWNLOAD_URL_2() { return &___BUILDS_DOWNLOAD_URL_2; }
	inline void set_BUILDS_DOWNLOAD_URL_2(String_t* value)
	{
		___BUILDS_DOWNLOAD_URL_2 = value;
		Il2CppCodeGenWriteBarrier((&___BUILDS_DOWNLOAD_URL_2), value);
	}

	inline static int32_t get_offset_of_PATCHER_DOWNLOAD_URL_3() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___PATCHER_DOWNLOAD_URL_3)); }
	inline String_t* get_PATCHER_DOWNLOAD_URL_3() const { return ___PATCHER_DOWNLOAD_URL_3; }
	inline String_t** get_address_of_PATCHER_DOWNLOAD_URL_3() { return &___PATCHER_DOWNLOAD_URL_3; }
	inline void set_PATCHER_DOWNLOAD_URL_3(String_t* value)
	{
		___PATCHER_DOWNLOAD_URL_3 = value;
		Il2CppCodeGenWriteBarrier((&___PATCHER_DOWNLOAD_URL_3), value);
	}

	inline static int32_t get_offset_of_PATCH_DOWNLOAD_RETRY_ATTEMPTS_4() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___PATCH_DOWNLOAD_RETRY_ATTEMPTS_4)); }
	inline uint16_t get_PATCH_DOWNLOAD_RETRY_ATTEMPTS_4() const { return ___PATCH_DOWNLOAD_RETRY_ATTEMPTS_4; }
	inline uint16_t* get_address_of_PATCH_DOWNLOAD_RETRY_ATTEMPTS_4() { return &___PATCH_DOWNLOAD_RETRY_ATTEMPTS_4; }
	inline void set_PATCH_DOWNLOAD_RETRY_ATTEMPTS_4(uint16_t value)
	{
		___PATCH_DOWNLOAD_RETRY_ATTEMPTS_4 = value;
	}

	inline static int32_t get_offset_of_PATCHNOTES_URL_5() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___PATCHNOTES_URL_5)); }
	inline String_t* get_PATCHNOTES_URL_5() const { return ___PATCHNOTES_URL_5; }
	inline String_t** get_address_of_PATCHNOTES_URL_5() { return &___PATCHNOTES_URL_5; }
	inline void set_PATCHNOTES_URL_5(String_t* value)
	{
		___PATCHNOTES_URL_5 = value;
		Il2CppCodeGenWriteBarrier((&___PATCHNOTES_URL_5), value);
	}

	inline static int32_t get_offset_of_NEWS_URL_6() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___NEWS_URL_6)); }
	inline String_t* get_NEWS_URL_6() const { return ___NEWS_URL_6; }
	inline String_t** get_address_of_NEWS_URL_6() { return &___NEWS_URL_6; }
	inline void set_NEWS_URL_6(String_t* value)
	{
		___NEWS_URL_6 = value;
		Il2CppCodeGenWriteBarrier((&___NEWS_URL_6), value);
	}

	inline static int32_t get_offset_of_LAUNCH_APP_7() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___LAUNCH_APP_7)); }
	inline String_t* get_LAUNCH_APP_7() const { return ___LAUNCH_APP_7; }
	inline String_t** get_address_of_LAUNCH_APP_7() { return &___LAUNCH_APP_7; }
	inline void set_LAUNCH_APP_7(String_t* value)
	{
		___LAUNCH_APP_7 = value;
		Il2CppCodeGenWriteBarrier((&___LAUNCH_APP_7), value);
	}

	inline static int32_t get_offset_of_LAUNCHER_NAME_8() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___LAUNCHER_NAME_8)); }
	inline String_t* get_LAUNCHER_NAME_8() const { return ___LAUNCHER_NAME_8; }
	inline String_t** get_address_of_LAUNCHER_NAME_8() { return &___LAUNCHER_NAME_8; }
	inline void set_LAUNCHER_NAME_8(String_t* value)
	{
		___LAUNCHER_NAME_8 = value;
		Il2CppCodeGenWriteBarrier((&___LAUNCHER_NAME_8), value);
	}

	inline static int32_t get_offset_of_LAUNCH_ARG_9() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___LAUNCH_ARG_9)); }
	inline String_t* get_LAUNCH_ARG_9() const { return ___LAUNCH_ARG_9; }
	inline String_t** get_address_of_LAUNCH_ARG_9() { return &___LAUNCH_ARG_9; }
	inline void set_LAUNCH_ARG_9(String_t* value)
	{
		___LAUNCH_ARG_9 = value;
		Il2CppCodeGenWriteBarrier((&___LAUNCH_ARG_9), value);
	}

	inline static int32_t get_offset_of_USE_RAW_LAUNCH_10() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___USE_RAW_LAUNCH_10)); }
	inline bool get_USE_RAW_LAUNCH_10() const { return ___USE_RAW_LAUNCH_10; }
	inline bool* get_address_of_USE_RAW_LAUNCH_10() { return &___USE_RAW_LAUNCH_10; }
	inline void set_USE_RAW_LAUNCH_10(bool value)
	{
		___USE_RAW_LAUNCH_10 = value;
	}

	inline static int32_t get_offset_of_ENABLE_FTP_11() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___ENABLE_FTP_11)); }
	inline bool get_ENABLE_FTP_11() const { return ___ENABLE_FTP_11; }
	inline bool* get_address_of_ENABLE_FTP_11() { return &___ENABLE_FTP_11; }
	inline void set_ENABLE_FTP_11(bool value)
	{
		___ENABLE_FTP_11 = value;
	}

	inline static int32_t get_offset_of_FTP_USERNAME_12() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___FTP_USERNAME_12)); }
	inline String_t* get_FTP_USERNAME_12() const { return ___FTP_USERNAME_12; }
	inline String_t** get_address_of_FTP_USERNAME_12() { return &___FTP_USERNAME_12; }
	inline void set_FTP_USERNAME_12(String_t* value)
	{
		___FTP_USERNAME_12 = value;
		Il2CppCodeGenWriteBarrier((&___FTP_USERNAME_12), value);
	}

	inline static int32_t get_offset_of_FTP_PASSWORD_13() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___FTP_PASSWORD_13)); }
	inline String_t* get_FTP_PASSWORD_13() const { return ___FTP_PASSWORD_13; }
	inline String_t** get_address_of_FTP_PASSWORD_13() { return &___FTP_PASSWORD_13; }
	inline void set_FTP_PASSWORD_13(String_t* value)
	{
		___FTP_PASSWORD_13 = value;
		Il2CppCodeGenWriteBarrier((&___FTP_PASSWORD_13), value);
	}

	inline static int32_t get_offset_of_INSTALL_IN_LOCAL_PATH_14() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___INSTALL_IN_LOCAL_PATH_14)); }
	inline bool get_INSTALL_IN_LOCAL_PATH_14() const { return ___INSTALL_IN_LOCAL_PATH_14; }
	inline bool* get_address_of_INSTALL_IN_LOCAL_PATH_14() { return &___INSTALL_IN_LOCAL_PATH_14; }
	inline void set_INSTALL_IN_LOCAL_PATH_14(bool value)
	{
		___INSTALL_IN_LOCAL_PATH_14 = value;
	}

	inline static int32_t get_offset_of_PROGRAM_FILES_DIRECTORY_TO_INSTALL_15() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___PROGRAM_FILES_DIRECTORY_TO_INSTALL_15)); }
	inline String_t* get_PROGRAM_FILES_DIRECTORY_TO_INSTALL_15() const { return ___PROGRAM_FILES_DIRECTORY_TO_INSTALL_15; }
	inline String_t** get_address_of_PROGRAM_FILES_DIRECTORY_TO_INSTALL_15() { return &___PROGRAM_FILES_DIRECTORY_TO_INSTALL_15; }
	inline void set_PROGRAM_FILES_DIRECTORY_TO_INSTALL_15(String_t* value)
	{
		___PROGRAM_FILES_DIRECTORY_TO_INSTALL_15 = value;
		Il2CppCodeGenWriteBarrier((&___PROGRAM_FILES_DIRECTORY_TO_INSTALL_15), value);
	}

	inline static int32_t get_offset_of_ENABLE_PATCHER_16() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___ENABLE_PATCHER_16)); }
	inline bool get_ENABLE_PATCHER_16() const { return ___ENABLE_PATCHER_16; }
	inline bool* get_address_of_ENABLE_PATCHER_16() { return &___ENABLE_PATCHER_16; }
	inline void set_ENABLE_PATCHER_16(bool value)
	{
		___ENABLE_PATCHER_16 = value;
	}

	inline static int32_t get_offset_of_ENABLE_INSTALLER_17() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___ENABLE_INSTALLER_17)); }
	inline bool get_ENABLE_INSTALLER_17() const { return ___ENABLE_INSTALLER_17; }
	inline bool* get_address_of_ENABLE_INSTALLER_17() { return &___ENABLE_INSTALLER_17; }
	inline void set_ENABLE_INSTALLER_17(bool value)
	{
		___ENABLE_INSTALLER_17 = value;
	}

	inline static int32_t get_offset_of_ENABLE_REPAIRER_18() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___ENABLE_REPAIRER_18)); }
	inline bool get_ENABLE_REPAIRER_18() const { return ___ENABLE_REPAIRER_18; }
	inline bool* get_address_of_ENABLE_REPAIRER_18() { return &___ENABLE_REPAIRER_18; }
	inline void set_ENABLE_REPAIRER_18(bool value)
	{
		___ENABLE_REPAIRER_18 = value;
	}

	inline static int32_t get_offset_of_INSTALL_PATCHER_19() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___INSTALL_PATCHER_19)); }
	inline bool get_INSTALL_PATCHER_19() const { return ___INSTALL_PATCHER_19; }
	inline bool* get_address_of_INSTALL_PATCHER_19() { return &___INSTALL_PATCHER_19; }
	inline void set_INSTALL_PATCHER_19(bool value)
	{
		___INSTALL_PATCHER_19 = value;
	}

	inline static int32_t get_offset_of_CREATE_DESKTOP_SHORTCUT_20() { return static_cast<int32_t>(offsetof(SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C, ___CREATE_DESKTOP_SHORTCUT_20)); }
	inline bool get_CREATE_DESKTOP_SHORTCUT_20() const { return ___CREATE_DESKTOP_SHORTCUT_20; }
	inline bool* get_address_of_CREATE_DESKTOP_SHORTCUT_20() { return &___CREATE_DESKTOP_SHORTCUT_20; }
	inline void set_CREATE_DESKTOP_SHORTCUT_20(bool value)
	{
		___CREATE_DESKTOP_SHORTCUT_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSOVERRIDER_T496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C_H
#ifndef BINDER_T70C25F53EDE3C42C319E9E60DB682372C467C757_H
#define BINDER_T70C25F53EDE3C42C319E9E60DB682372C467C757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Binder
struct  Binder_t70C25F53EDE3C42C319E9E60DB682372C467C757  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDER_T70C25F53EDE3C42C319E9E60DB682372C467C757_H
#ifndef BINDERHELPER_T8AE8B534D7D7BCB8C34300486B365869813A9868_H
#define BINDERHELPER_T8AE8B534D7D7BCB8C34300486B365869813A9868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.BinderHelper
struct  BinderHelper_t8AE8B534D7D7BCB8C34300486B365869813A9868  : public RuntimeObject
{
public:

public:
};

struct BinderHelper_t8AE8B534D7D7BCB8C34300486B365869813A9868_StaticFields
{
public:
	// System.Reflection.MethodInfo Microsoft.CSharp.RuntimeBinder.BinderHelper::s_DoubleIsNaN
	MethodInfo_t * ___s_DoubleIsNaN_0;
	// System.Reflection.MethodInfo Microsoft.CSharp.RuntimeBinder.BinderHelper::s_SingleIsNaN
	MethodInfo_t * ___s_SingleIsNaN_1;

public:
	inline static int32_t get_offset_of_s_DoubleIsNaN_0() { return static_cast<int32_t>(offsetof(BinderHelper_t8AE8B534D7D7BCB8C34300486B365869813A9868_StaticFields, ___s_DoubleIsNaN_0)); }
	inline MethodInfo_t * get_s_DoubleIsNaN_0() const { return ___s_DoubleIsNaN_0; }
	inline MethodInfo_t ** get_address_of_s_DoubleIsNaN_0() { return &___s_DoubleIsNaN_0; }
	inline void set_s_DoubleIsNaN_0(MethodInfo_t * value)
	{
		___s_DoubleIsNaN_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_DoubleIsNaN_0), value);
	}

	inline static int32_t get_offset_of_s_SingleIsNaN_1() { return static_cast<int32_t>(offsetof(BinderHelper_t8AE8B534D7D7BCB8C34300486B365869813A9868_StaticFields, ___s_SingleIsNaN_1)); }
	inline MethodInfo_t * get_s_SingleIsNaN_1() const { return ___s_SingleIsNaN_1; }
	inline MethodInfo_t ** get_address_of_s_SingleIsNaN_1() { return &___s_SingleIsNaN_1; }
	inline void set_s_SingleIsNaN_1(MethodInfo_t * value)
	{
		___s_SingleIsNaN_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_SingleIsNaN_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDERHELPER_T8AE8B534D7D7BCB8C34300486B365869813A9868_H
#ifndef ERROR_T8C8AD64A1A9712EB98A717498D9E4632E868A660_H
#define ERROR_T8C8AD64A1A9712EB98A717498D9E4632E868A660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Error
struct  Error_t8C8AD64A1A9712EB98A717498D9E4632E868A660  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROR_T8C8AD64A1A9712EB98A717498D9E4632E868A660_H
#ifndef RUNTIMEBINDER_TB1F2154E74418B6DCD60108148C4BFFAF2D0FE03_H
#define RUNTIMEBINDER_TB1F2154E74418B6DCD60108148C4BFFAF2D0FE03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.RuntimeBinder
struct  RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.SymbolTable Microsoft.CSharp.RuntimeBinder.RuntimeBinder::_symbolTable
	SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C * ____symbolTable_1;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CSemanticChecker Microsoft.CSharp.RuntimeBinder.RuntimeBinder::_semanticChecker
	CSemanticChecker_t392A4B642F3784AA63593EAEAA5D6E36F89E2A8A * ____semanticChecker_2;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExprFactory Microsoft.CSharp.RuntimeBinder.RuntimeBinder::_exprFactory
	ExprFactory_tFD6AA538BD7633AE2E5585D41577AB6B996811DA * ____exprFactory_3;
	// Microsoft.CSharp.RuntimeBinder.Semantics.BindingContext Microsoft.CSharp.RuntimeBinder.RuntimeBinder::_bindingContext
	BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073 * ____bindingContext_4;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder Microsoft.CSharp.RuntimeBinder.RuntimeBinder::_binder
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E * ____binder_5;
	// System.Object Microsoft.CSharp.RuntimeBinder.RuntimeBinder::_bindLock
	RuntimeObject * ____bindLock_6;

public:
	inline static int32_t get_offset_of__symbolTable_1() { return static_cast<int32_t>(offsetof(RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03, ____symbolTable_1)); }
	inline SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C * get__symbolTable_1() const { return ____symbolTable_1; }
	inline SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C ** get_address_of__symbolTable_1() { return &____symbolTable_1; }
	inline void set__symbolTable_1(SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C * value)
	{
		____symbolTable_1 = value;
		Il2CppCodeGenWriteBarrier((&____symbolTable_1), value);
	}

	inline static int32_t get_offset_of__semanticChecker_2() { return static_cast<int32_t>(offsetof(RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03, ____semanticChecker_2)); }
	inline CSemanticChecker_t392A4B642F3784AA63593EAEAA5D6E36F89E2A8A * get__semanticChecker_2() const { return ____semanticChecker_2; }
	inline CSemanticChecker_t392A4B642F3784AA63593EAEAA5D6E36F89E2A8A ** get_address_of__semanticChecker_2() { return &____semanticChecker_2; }
	inline void set__semanticChecker_2(CSemanticChecker_t392A4B642F3784AA63593EAEAA5D6E36F89E2A8A * value)
	{
		____semanticChecker_2 = value;
		Il2CppCodeGenWriteBarrier((&____semanticChecker_2), value);
	}

	inline static int32_t get_offset_of__exprFactory_3() { return static_cast<int32_t>(offsetof(RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03, ____exprFactory_3)); }
	inline ExprFactory_tFD6AA538BD7633AE2E5585D41577AB6B996811DA * get__exprFactory_3() const { return ____exprFactory_3; }
	inline ExprFactory_tFD6AA538BD7633AE2E5585D41577AB6B996811DA ** get_address_of__exprFactory_3() { return &____exprFactory_3; }
	inline void set__exprFactory_3(ExprFactory_tFD6AA538BD7633AE2E5585D41577AB6B996811DA * value)
	{
		____exprFactory_3 = value;
		Il2CppCodeGenWriteBarrier((&____exprFactory_3), value);
	}

	inline static int32_t get_offset_of__bindingContext_4() { return static_cast<int32_t>(offsetof(RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03, ____bindingContext_4)); }
	inline BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073 * get__bindingContext_4() const { return ____bindingContext_4; }
	inline BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073 ** get_address_of__bindingContext_4() { return &____bindingContext_4; }
	inline void set__bindingContext_4(BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073 * value)
	{
		____bindingContext_4 = value;
		Il2CppCodeGenWriteBarrier((&____bindingContext_4), value);
	}

	inline static int32_t get_offset_of__binder_5() { return static_cast<int32_t>(offsetof(RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03, ____binder_5)); }
	inline ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E * get__binder_5() const { return ____binder_5; }
	inline ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E ** get_address_of__binder_5() { return &____binder_5; }
	inline void set__binder_5(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E * value)
	{
		____binder_5 = value;
		Il2CppCodeGenWriteBarrier((&____binder_5), value);
	}

	inline static int32_t get_offset_of__bindLock_6() { return static_cast<int32_t>(offsetof(RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03, ____bindLock_6)); }
	inline RuntimeObject * get__bindLock_6() const { return ____bindLock_6; }
	inline RuntimeObject ** get_address_of__bindLock_6() { return &____bindLock_6; }
	inline void set__bindLock_6(RuntimeObject * value)
	{
		____bindLock_6 = value;
		Il2CppCodeGenWriteBarrier((&____bindLock_6), value);
	}
};

struct RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03_StaticFields
{
public:
	// System.Lazy`1<Microsoft.CSharp.RuntimeBinder.RuntimeBinder> Microsoft.CSharp.RuntimeBinder.RuntimeBinder::s_lazyInstance
	Lazy_1_t3AB76E562D17A7F29EA76A6400E4400BC8E0A2A6 * ___s_lazyInstance_0;

public:
	inline static int32_t get_offset_of_s_lazyInstance_0() { return static_cast<int32_t>(offsetof(RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03_StaticFields, ___s_lazyInstance_0)); }
	inline Lazy_1_t3AB76E562D17A7F29EA76A6400E4400BC8E0A2A6 * get_s_lazyInstance_0() const { return ___s_lazyInstance_0; }
	inline Lazy_1_t3AB76E562D17A7F29EA76A6400E4400BC8E0A2A6 ** get_address_of_s_lazyInstance_0() { return &___s_lazyInstance_0; }
	inline void set_s_lazyInstance_0(Lazy_1_t3AB76E562D17A7F29EA76A6400E4400BC8E0A2A6 * value)
	{
		___s_lazyInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_lazyInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEBINDER_TB1F2154E74418B6DCD60108148C4BFFAF2D0FE03_H
#ifndef U3CU3EC_T0B82A5865EFFA1D783819678FC38BE8C8AA0709B_H
#define U3CU3EC_T0B82A5865EFFA1D783819678FC38BE8C8AA0709B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.RuntimeBinder/<>c
struct  U3CU3Ec_t0B82A5865EFFA1D783819678FC38BE8C8AA0709B  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t0B82A5865EFFA1D783819678FC38BE8C8AA0709B_StaticFields
{
public:
	// Microsoft.CSharp.RuntimeBinder.RuntimeBinder/<>c Microsoft.CSharp.RuntimeBinder.RuntimeBinder/<>c::<>9
	U3CU3Ec_t0B82A5865EFFA1D783819678FC38BE8C8AA0709B * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0B82A5865EFFA1D783819678FC38BE8C8AA0709B_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t0B82A5865EFFA1D783819678FC38BE8C8AA0709B * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t0B82A5865EFFA1D783819678FC38BE8C8AA0709B ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t0B82A5865EFFA1D783819678FC38BE8C8AA0709B * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T0B82A5865EFFA1D783819678FC38BE8C8AA0709B_H
#ifndef RUNTIMEBINDEREXTENSIONS_T82FB39096CA05255EC54F0643B2E7A550E243131_H
#define RUNTIMEBINDEREXTENSIONS_T82FB39096CA05255EC54F0643B2E7A550E243131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions
struct  RuntimeBinderExtensions_t82FB39096CA05255EC54F0643B2E7A550E243131  : public RuntimeObject
{
public:

public:
};

struct RuntimeBinderExtensions_t82FB39096CA05255EC54F0643B2E7A550E243131_StaticFields
{
public:
	// System.Func`3<System.Reflection.MemberInfo,System.Reflection.MemberInfo,System.Boolean> Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions::s_MemberEquivalence
	Func_3_tD31145957D9862FA32A98EED47776298D76B975C * ___s_MemberEquivalence_0;

public:
	inline static int32_t get_offset_of_s_MemberEquivalence_0() { return static_cast<int32_t>(offsetof(RuntimeBinderExtensions_t82FB39096CA05255EC54F0643B2E7A550E243131_StaticFields, ___s_MemberEquivalence_0)); }
	inline Func_3_tD31145957D9862FA32A98EED47776298D76B975C * get_s_MemberEquivalence_0() const { return ___s_MemberEquivalence_0; }
	inline Func_3_tD31145957D9862FA32A98EED47776298D76B975C ** get_address_of_s_MemberEquivalence_0() { return &___s_MemberEquivalence_0; }
	inline void set_s_MemberEquivalence_0(Func_3_tD31145957D9862FA32A98EED47776298D76B975C * value)
	{
		___s_MemberEquivalence_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_MemberEquivalence_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEBINDEREXTENSIONS_T82FB39096CA05255EC54F0643B2E7A550E243131_H
#ifndef U3CU3EC_TFD9C65E667B863A1E188A2BADA6F22F3044D3E32_H
#define U3CU3EC_TFD9C65E667B863A1E188A2BADA6F22F3044D3E32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c
struct  U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32_StaticFields
{
public:
	// Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c::<>9
	U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32 * ___U3CU3E9_0;
	// System.Func`2<System.Boolean,System.Boolean> Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c::<>9__1_1
	Func_2_t992C4FF3CDF2A4BD79586600575703C9315961A1 * ___U3CU3E9__1_1_1;
	// System.Func`2<System.Boolean,System.Boolean> Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c::<>9__1_3
	Func_2_t992C4FF3CDF2A4BD79586600575703C9315961A1 * ___U3CU3E9__1_3_2;
	// System.Func`2<System.Boolean,System.Boolean> Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c::<>9__4_1
	Func_2_t992C4FF3CDF2A4BD79586600575703C9315961A1 * ___U3CU3E9__4_1_3;
	// System.Func`3<System.Reflection.MemberInfo,System.Reflection.MemberInfo,System.Boolean> Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c::<>9__10_1
	Func_3_tD31145957D9862FA32A98EED47776298D76B975C * ___U3CU3E9__10_1_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32_StaticFields, ___U3CU3E9__1_1_1)); }
	inline Func_2_t992C4FF3CDF2A4BD79586600575703C9315961A1 * get_U3CU3E9__1_1_1() const { return ___U3CU3E9__1_1_1; }
	inline Func_2_t992C4FF3CDF2A4BD79586600575703C9315961A1 ** get_address_of_U3CU3E9__1_1_1() { return &___U3CU3E9__1_1_1; }
	inline void set_U3CU3E9__1_1_1(Func_2_t992C4FF3CDF2A4BD79586600575703C9315961A1 * value)
	{
		___U3CU3E9__1_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_3_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32_StaticFields, ___U3CU3E9__1_3_2)); }
	inline Func_2_t992C4FF3CDF2A4BD79586600575703C9315961A1 * get_U3CU3E9__1_3_2() const { return ___U3CU3E9__1_3_2; }
	inline Func_2_t992C4FF3CDF2A4BD79586600575703C9315961A1 ** get_address_of_U3CU3E9__1_3_2() { return &___U3CU3E9__1_3_2; }
	inline void set_U3CU3E9__1_3_2(Func_2_t992C4FF3CDF2A4BD79586600575703C9315961A1 * value)
	{
		___U3CU3E9__1_3_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_3_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32_StaticFields, ___U3CU3E9__4_1_3)); }
	inline Func_2_t992C4FF3CDF2A4BD79586600575703C9315961A1 * get_U3CU3E9__4_1_3() const { return ___U3CU3E9__4_1_3; }
	inline Func_2_t992C4FF3CDF2A4BD79586600575703C9315961A1 ** get_address_of_U3CU3E9__4_1_3() { return &___U3CU3E9__4_1_3; }
	inline void set_U3CU3E9__4_1_3(Func_2_t992C4FF3CDF2A4BD79586600575703C9315961A1 * value)
	{
		___U3CU3E9__4_1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32_StaticFields, ___U3CU3E9__10_1_4)); }
	inline Func_3_tD31145957D9862FA32A98EED47776298D76B975C * get_U3CU3E9__10_1_4() const { return ___U3CU3E9__10_1_4; }
	inline Func_3_tD31145957D9862FA32A98EED47776298D76B975C ** get_address_of_U3CU3E9__10_1_4() { return &___U3CU3E9__10_1_4; }
	inline void set_U3CU3E9__10_1_4(Func_3_tD31145957D9862FA32A98EED47776298D76B975C * value)
	{
		___U3CU3E9__10_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__10_1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TFD9C65E667B863A1E188A2BADA6F22F3044D3E32_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T0A2CCCF27B971C3B949B8FC3267FA35DA6FA8EA5_H
#define U3CU3EC__DISPLAYCLASS1_0_T0A2CCCF27B971C3B949B8FC3267FA35DA6FA8EA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t0A2CCCF27B971C3B949B8FC3267FA35DA6FA8EA5  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c__DisplayClass1_0::method1
	MethodInfo_t * ___method1_0;
	// System.Reflection.MethodInfo Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c__DisplayClass1_0::method2
	MethodInfo_t * ___method2_1;

public:
	inline static int32_t get_offset_of_method1_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t0A2CCCF27B971C3B949B8FC3267FA35DA6FA8EA5, ___method1_0)); }
	inline MethodInfo_t * get_method1_0() const { return ___method1_0; }
	inline MethodInfo_t ** get_address_of_method1_0() { return &___method1_0; }
	inline void set_method1_0(MethodInfo_t * value)
	{
		___method1_0 = value;
		Il2CppCodeGenWriteBarrier((&___method1_0), value);
	}

	inline static int32_t get_offset_of_method2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t0A2CCCF27B971C3B949B8FC3267FA35DA6FA8EA5, ___method2_1)); }
	inline MethodInfo_t * get_method2_1() const { return ___method2_1; }
	inline MethodInfo_t ** get_address_of_method2_1() { return &___method2_1; }
	inline void set_method2_1(MethodInfo_t * value)
	{
		___method2_1 = value;
		Il2CppCodeGenWriteBarrier((&___method2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T0A2CCCF27B971C3B949B8FC3267FA35DA6FA8EA5_H
#ifndef U3CU3EC__DISPLAYCLASS1_1_T600A9BCCDDA928475AE4FF48AE9978CE73EA3CF3_H
#define U3CU3EC__DISPLAYCLASS1_1_T600A9BCCDDA928475AE4FF48AE9978CE73EA3CF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c__DisplayClass1_1
struct  U3CU3Ec__DisplayClass1_1_t600A9BCCDDA928475AE4FF48AE9978CE73EA3CF3  : public RuntimeObject
{
public:
	// System.Reflection.ConstructorInfo Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c__DisplayClass1_1::ctor1
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ___ctor1_0;
	// System.Reflection.ConstructorInfo Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c__DisplayClass1_1::ctor2
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ___ctor2_1;

public:
	inline static int32_t get_offset_of_ctor1_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_1_t600A9BCCDDA928475AE4FF48AE9978CE73EA3CF3, ___ctor1_0)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get_ctor1_0() const { return ___ctor1_0; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of_ctor1_0() { return &___ctor1_0; }
	inline void set_ctor1_0(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		___ctor1_0 = value;
		Il2CppCodeGenWriteBarrier((&___ctor1_0), value);
	}

	inline static int32_t get_offset_of_ctor2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_1_t600A9BCCDDA928475AE4FF48AE9978CE73EA3CF3, ___ctor2_1)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get_ctor2_1() const { return ___ctor2_1; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of_ctor2_1() { return &___ctor2_1; }
	inline void set_ctor2_1(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		___ctor2_1 = value;
		Il2CppCodeGenWriteBarrier((&___ctor2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_1_T600A9BCCDDA928475AE4FF48AE9978CE73EA3CF3_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_TDA0887CF3A22682842C736714A89366EBAD41DB8_H
#define U3CU3EC__DISPLAYCLASS4_0_TDA0887CF3A22682842C736714A89366EBAD41DB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_tDA0887CF3A22682842C736714A89366EBAD41DB8  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c__DisplayClass4_0::member1
	MemberInfo_t * ___member1_0;
	// System.Reflection.MemberInfo Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c__DisplayClass4_0::member2
	MemberInfo_t * ___member2_1;

public:
	inline static int32_t get_offset_of_member1_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tDA0887CF3A22682842C736714A89366EBAD41DB8, ___member1_0)); }
	inline MemberInfo_t * get_member1_0() const { return ___member1_0; }
	inline MemberInfo_t ** get_address_of_member1_0() { return &___member1_0; }
	inline void set_member1_0(MemberInfo_t * value)
	{
		___member1_0 = value;
		Il2CppCodeGenWriteBarrier((&___member1_0), value);
	}

	inline static int32_t get_offset_of_member2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tDA0887CF3A22682842C736714A89366EBAD41DB8, ___member2_1)); }
	inline MemberInfo_t * get_member2_1() const { return ___member2_1; }
	inline MemberInfo_t ** get_address_of_member2_1() { return &___member2_1; }
	inline void set_member2_1(MemberInfo_t * value)
	{
		___member2_1 = value;
		Il2CppCodeGenWriteBarrier((&___member2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_TDA0887CF3A22682842C736714A89366EBAD41DB8_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_T1B2422A71E13FF0E9B515F402691AC3338A4C491_H
#define U3CU3EC__DISPLAYCLASS9_0_T1B2422A71E13FF0E9B515F402691AC3338A4C491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t1B2422A71E13FF0E9B515F402691AC3338A4C491  : public RuntimeObject
{
public:
	// System.String Microsoft.CSharp.RuntimeBinder.RuntimeBinderExtensions/<>c__DisplayClass9_0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t1B2422A71E13FF0E9B515F402691AC3338A4C491, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_T1B2422A71E13FF0E9B515F402691AC3338A4C491_H
#ifndef BINDINGCONTEXT_T395E94DB38328C0481E905BB742C11352BA8C073_H
#define BINDINGCONTEXT_T395E94DB38328C0481E905BB742C11352BA8C073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.BindingContext
struct  BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.SymbolLoader Microsoft.CSharp.RuntimeBinder.Semantics.BindingContext::<SymbolLoader>k__BackingField
	SymbolLoader_t44A1A145E4324210903F3FA34033B41C9A80A69F * ___U3CSymbolLoaderU3Ek__BackingField_0;
	// Microsoft.CSharp.RuntimeBinder.Semantics.AggregateDeclaration Microsoft.CSharp.RuntimeBinder.Semantics.BindingContext::<ContextForMemberLookup>k__BackingField
	AggregateDeclaration_t1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5 * ___U3CContextForMemberLookupU3Ek__BackingField_1;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CSemanticChecker Microsoft.CSharp.RuntimeBinder.Semantics.BindingContext::<SemanticChecker>k__BackingField
	CSemanticChecker_t392A4B642F3784AA63593EAEAA5D6E36F89E2A8A * ___U3CSemanticCheckerU3Ek__BackingField_2;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExprFactory Microsoft.CSharp.RuntimeBinder.Semantics.BindingContext::<ExprFactory>k__BackingField
	ExprFactory_tFD6AA538BD7633AE2E5585D41577AB6B996811DA * ___U3CExprFactoryU3Ek__BackingField_3;
	// System.Boolean Microsoft.CSharp.RuntimeBinder.Semantics.BindingContext::<CheckedNormal>k__BackingField
	bool ___U3CCheckedNormalU3Ek__BackingField_4;
	// System.Boolean Microsoft.CSharp.RuntimeBinder.Semantics.BindingContext::<CheckedConstant>k__BackingField
	bool ___U3CCheckedConstantU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CSymbolLoaderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073, ___U3CSymbolLoaderU3Ek__BackingField_0)); }
	inline SymbolLoader_t44A1A145E4324210903F3FA34033B41C9A80A69F * get_U3CSymbolLoaderU3Ek__BackingField_0() const { return ___U3CSymbolLoaderU3Ek__BackingField_0; }
	inline SymbolLoader_t44A1A145E4324210903F3FA34033B41C9A80A69F ** get_address_of_U3CSymbolLoaderU3Ek__BackingField_0() { return &___U3CSymbolLoaderU3Ek__BackingField_0; }
	inline void set_U3CSymbolLoaderU3Ek__BackingField_0(SymbolLoader_t44A1A145E4324210903F3FA34033B41C9A80A69F * value)
	{
		___U3CSymbolLoaderU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSymbolLoaderU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CContextForMemberLookupU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073, ___U3CContextForMemberLookupU3Ek__BackingField_1)); }
	inline AggregateDeclaration_t1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5 * get_U3CContextForMemberLookupU3Ek__BackingField_1() const { return ___U3CContextForMemberLookupU3Ek__BackingField_1; }
	inline AggregateDeclaration_t1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5 ** get_address_of_U3CContextForMemberLookupU3Ek__BackingField_1() { return &___U3CContextForMemberLookupU3Ek__BackingField_1; }
	inline void set_U3CContextForMemberLookupU3Ek__BackingField_1(AggregateDeclaration_t1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5 * value)
	{
		___U3CContextForMemberLookupU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContextForMemberLookupU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSemanticCheckerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073, ___U3CSemanticCheckerU3Ek__BackingField_2)); }
	inline CSemanticChecker_t392A4B642F3784AA63593EAEAA5D6E36F89E2A8A * get_U3CSemanticCheckerU3Ek__BackingField_2() const { return ___U3CSemanticCheckerU3Ek__BackingField_2; }
	inline CSemanticChecker_t392A4B642F3784AA63593EAEAA5D6E36F89E2A8A ** get_address_of_U3CSemanticCheckerU3Ek__BackingField_2() { return &___U3CSemanticCheckerU3Ek__BackingField_2; }
	inline void set_U3CSemanticCheckerU3Ek__BackingField_2(CSemanticChecker_t392A4B642F3784AA63593EAEAA5D6E36F89E2A8A * value)
	{
		___U3CSemanticCheckerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSemanticCheckerU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CExprFactoryU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073, ___U3CExprFactoryU3Ek__BackingField_3)); }
	inline ExprFactory_tFD6AA538BD7633AE2E5585D41577AB6B996811DA * get_U3CExprFactoryU3Ek__BackingField_3() const { return ___U3CExprFactoryU3Ek__BackingField_3; }
	inline ExprFactory_tFD6AA538BD7633AE2E5585D41577AB6B996811DA ** get_address_of_U3CExprFactoryU3Ek__BackingField_3() { return &___U3CExprFactoryU3Ek__BackingField_3; }
	inline void set_U3CExprFactoryU3Ek__BackingField_3(ExprFactory_tFD6AA538BD7633AE2E5585D41577AB6B996811DA * value)
	{
		___U3CExprFactoryU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExprFactoryU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CCheckedNormalU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073, ___U3CCheckedNormalU3Ek__BackingField_4)); }
	inline bool get_U3CCheckedNormalU3Ek__BackingField_4() const { return ___U3CCheckedNormalU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CCheckedNormalU3Ek__BackingField_4() { return &___U3CCheckedNormalU3Ek__BackingField_4; }
	inline void set_U3CCheckedNormalU3Ek__BackingField_4(bool value)
	{
		___U3CCheckedNormalU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CCheckedConstantU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073, ___U3CCheckedConstantU3Ek__BackingField_5)); }
	inline bool get_U3CCheckedConstantU3Ek__BackingField_5() const { return ___U3CCheckedConstantU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CCheckedConstantU3Ek__BackingField_5() { return &___U3CCheckedConstantU3Ek__BackingField_5; }
	inline void set_U3CCheckedConstantU3Ek__BackingField_5(bool value)
	{
		___U3CCheckedConstantU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGCONTEXT_T395E94DB38328C0481E905BB742C11352BA8C073_H
#ifndef CCONVERSIONS_T6C665AEE020C93DC589B0C85DB6D6D503E060F4F_H
#define CCONVERSIONS_T6C665AEE020C93DC589B0C85DB6D6D503E060F4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.CConversions
struct  CConversions_t6C665AEE020C93DC589B0C85DB6D6D503E060F4F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCONVERSIONS_T6C665AEE020C93DC589B0C85DB6D6D503E060F4F_H
#ifndef CANDIDATEFUNCTIONMEMBER_T11C0A24B60AD0F73DAF3EAD2E96C630FC2595D94_H
#define CANDIDATEFUNCTIONMEMBER_T11C0A24B60AD0F73DAF3EAD2E96C630FC2595D94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.CandidateFunctionMember
struct  CandidateFunctionMember_t11C0A24B60AD0F73DAF3EAD2E96C630FC2595D94  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.MethPropWithInst Microsoft.CSharp.RuntimeBinder.Semantics.CandidateFunctionMember::mpwi
	MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * ___mpwi_0;
	// Microsoft.CSharp.RuntimeBinder.Semantics.TypeArray Microsoft.CSharp.RuntimeBinder.Semantics.CandidateFunctionMember::params
	TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 * ___params_1;
	// System.Byte Microsoft.CSharp.RuntimeBinder.Semantics.CandidateFunctionMember::ctypeLift
	uint8_t ___ctypeLift_2;
	// System.Boolean Microsoft.CSharp.RuntimeBinder.Semantics.CandidateFunctionMember::fExpanded
	bool ___fExpanded_3;

public:
	inline static int32_t get_offset_of_mpwi_0() { return static_cast<int32_t>(offsetof(CandidateFunctionMember_t11C0A24B60AD0F73DAF3EAD2E96C630FC2595D94, ___mpwi_0)); }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * get_mpwi_0() const { return ___mpwi_0; }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C ** get_address_of_mpwi_0() { return &___mpwi_0; }
	inline void set_mpwi_0(MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * value)
	{
		___mpwi_0 = value;
		Il2CppCodeGenWriteBarrier((&___mpwi_0), value);
	}

	inline static int32_t get_offset_of_params_1() { return static_cast<int32_t>(offsetof(CandidateFunctionMember_t11C0A24B60AD0F73DAF3EAD2E96C630FC2595D94, ___params_1)); }
	inline TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 * get_params_1() const { return ___params_1; }
	inline TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 ** get_address_of_params_1() { return &___params_1; }
	inline void set_params_1(TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 * value)
	{
		___params_1 = value;
		Il2CppCodeGenWriteBarrier((&___params_1), value);
	}

	inline static int32_t get_offset_of_ctypeLift_2() { return static_cast<int32_t>(offsetof(CandidateFunctionMember_t11C0A24B60AD0F73DAF3EAD2E96C630FC2595D94, ___ctypeLift_2)); }
	inline uint8_t get_ctypeLift_2() const { return ___ctypeLift_2; }
	inline uint8_t* get_address_of_ctypeLift_2() { return &___ctypeLift_2; }
	inline void set_ctypeLift_2(uint8_t value)
	{
		___ctypeLift_2 = value;
	}

	inline static int32_t get_offset_of_fExpanded_3() { return static_cast<int32_t>(offsetof(CandidateFunctionMember_t11C0A24B60AD0F73DAF3EAD2E96C630FC2595D94, ___fExpanded_3)); }
	inline bool get_fExpanded_3() const { return ___fExpanded_3; }
	inline bool* get_address_of_fExpanded_3() { return &___fExpanded_3; }
	inline void set_fExpanded_3(bool value)
	{
		___fExpanded_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANDIDATEFUNCTIONMEMBER_T11C0A24B60AD0F73DAF3EAD2E96C630FC2595D94_H
#ifndef EXPREXTENSIONS_TD473EF9629717DE39A5FCB5EBAB6EF17E452B4B4_H
#define EXPREXTENSIONS_TD473EF9629717DE39A5FCB5EBAB6EF17E452B4B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.EXPRExtensions
struct  EXPRExtensions_tD473EF9629717DE39A5FCB5EBAB6EF17E452B4B4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPREXTENSIONS_TD473EF9629717DE39A5FCB5EBAB6EF17E452B4B4_H
#ifndef U3CTOENUMERABLEU3ED__1_TF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F_H
#define U3CTOENUMERABLEU3ED__1_TF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.EXPRExtensions/<ToEnumerable>d__1
struct  U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F  : public RuntimeObject
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.EXPRExtensions/<ToEnumerable>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Microsoft.CSharp.RuntimeBinder.Semantics.Expr Microsoft.CSharp.RuntimeBinder.Semantics.EXPRExtensions/<ToEnumerable>d__1::<>2__current
	Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * ___U3CU3E2__current_1;
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.EXPRExtensions/<ToEnumerable>d__1::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Microsoft.CSharp.RuntimeBinder.Semantics.Expr Microsoft.CSharp.RuntimeBinder.Semantics.EXPRExtensions/<ToEnumerable>d__1::expr
	Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * ___expr_3;
	// Microsoft.CSharp.RuntimeBinder.Semantics.Expr Microsoft.CSharp.RuntimeBinder.Semantics.EXPRExtensions/<ToEnumerable>d__1::<>3__expr
	Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * ___U3CU3E3__expr_4;
	// Microsoft.CSharp.RuntimeBinder.Semantics.Expr Microsoft.CSharp.RuntimeBinder.Semantics.EXPRExtensions/<ToEnumerable>d__1::<exprCur>5__1
	Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * ___U3CexprCurU3E5__1_5;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExprList Microsoft.CSharp.RuntimeBinder.Semantics.EXPRExtensions/<ToEnumerable>d__1::<list>5__2
	ExprList_tA3E91238529996C97D96CB0008512C01CBC28132 * ___U3ClistU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F, ___U3CU3E2__current_1)); }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_expr_3() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F, ___expr_3)); }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * get_expr_3() const { return ___expr_3; }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 ** get_address_of_expr_3() { return &___expr_3; }
	inline void set_expr_3(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * value)
	{
		___expr_3 = value;
		Il2CppCodeGenWriteBarrier((&___expr_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__expr_4() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F, ___U3CU3E3__expr_4)); }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * get_U3CU3E3__expr_4() const { return ___U3CU3E3__expr_4; }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 ** get_address_of_U3CU3E3__expr_4() { return &___U3CU3E3__expr_4; }
	inline void set_U3CU3E3__expr_4(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * value)
	{
		___U3CU3E3__expr_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__expr_4), value);
	}

	inline static int32_t get_offset_of_U3CexprCurU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F, ___U3CexprCurU3E5__1_5)); }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * get_U3CexprCurU3E5__1_5() const { return ___U3CexprCurU3E5__1_5; }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 ** get_address_of_U3CexprCurU3E5__1_5() { return &___U3CexprCurU3E5__1_5; }
	inline void set_U3CexprCurU3E5__1_5(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * value)
	{
		___U3CexprCurU3E5__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CexprCurU3E5__1_5), value);
	}

	inline static int32_t get_offset_of_U3ClistU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F, ___U3ClistU3E5__2_6)); }
	inline ExprList_tA3E91238529996C97D96CB0008512C01CBC28132 * get_U3ClistU3E5__2_6() const { return ___U3ClistU3E5__2_6; }
	inline ExprList_tA3E91238529996C97D96CB0008512C01CBC28132 ** get_address_of_U3ClistU3E5__2_6() { return &___U3ClistU3E5__2_6; }
	inline void set_U3ClistU3E5__2_6(ExprList_tA3E91238529996C97D96CB0008512C01CBC28132 * value)
	{
		___U3ClistU3E5__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClistU3E5__2_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTOENUMERABLEU3ED__1_TF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F_H
#ifndef EXPRFACTORY_TFD6AA538BD7633AE2E5585D41577AB6B996811DA_H
#define EXPRFACTORY_TFD6AA538BD7633AE2E5585D41577AB6B996811DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExprFactory
struct  ExprFactory_tFD6AA538BD7633AE2E5585D41577AB6B996811DA  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.GlobalSymbolContext Microsoft.CSharp.RuntimeBinder.Semantics.ExprFactory::_globalSymbolContext
	GlobalSymbolContext_t9C2C782AF29EBF932B5BB01E8A6065E80C5E3DC2 * ____globalSymbolContext_0;

public:
	inline static int32_t get_offset_of__globalSymbolContext_0() { return static_cast<int32_t>(offsetof(ExprFactory_tFD6AA538BD7633AE2E5585D41577AB6B996811DA, ____globalSymbolContext_0)); }
	inline GlobalSymbolContext_t9C2C782AF29EBF932B5BB01E8A6065E80C5E3DC2 * get__globalSymbolContext_0() const { return ____globalSymbolContext_0; }
	inline GlobalSymbolContext_t9C2C782AF29EBF932B5BB01E8A6065E80C5E3DC2 ** get_address_of__globalSymbolContext_0() { return &____globalSymbolContext_0; }
	inline void set__globalSymbolContext_0(GlobalSymbolContext_t9C2C782AF29EBF932B5BB01E8A6065E80C5E3DC2 * value)
	{
		____globalSymbolContext_0 = value;
		Il2CppCodeGenWriteBarrier((&____globalSymbolContext_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRFACTORY_TFD6AA538BD7633AE2E5585D41577AB6B996811DA_H
#ifndef EXPRVISITORBASE_T266D2BBD3F35529D28D5570ADE95D3E4C8ED8104_H
#define EXPRVISITORBASE_T266D2BBD3F35529D28D5570ADE95D3E4C8ED8104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExprVisitorBase
struct  ExprVisitorBase_t266D2BBD3F35529D28D5570ADE95D3E4C8ED8104  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRVISITORBASE_T266D2BBD3F35529D28D5570ADE95D3E4C8ED8104_H
#ifndef EXPRESSIONBINDER_T6AFB55222F11231641DF4A17EC2CBCC84142502E_H
#define EXPRESSIONBINDER_T6AFB55222F11231641DF4A17EC2CBCC84142502E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder
struct  ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.BindingContext Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder::Context
	BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073 * ___Context_5;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CNullable Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder::m_nullable
	CNullable_t7AE2FD6161F70658549CB2814F27E766A2C3085C * ___m_nullable_6;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpSig[] Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder::g_binopSignatures
	BinOpSigU5BU5D_t5FAB057C77FA31908C3C5D928EBFBDE82EB1260B* ___g_binopSignatures_9;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/UnaOpSig[] Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder::g_rguos
	UnaOpSigU5BU5D_tD8A410FEA2A14E00294B50610C9DFDF03317B702* ___g_rguos_10;

public:
	inline static int32_t get_offset_of_Context_5() { return static_cast<int32_t>(offsetof(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E, ___Context_5)); }
	inline BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073 * get_Context_5() const { return ___Context_5; }
	inline BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073 ** get_address_of_Context_5() { return &___Context_5; }
	inline void set_Context_5(BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073 * value)
	{
		___Context_5 = value;
		Il2CppCodeGenWriteBarrier((&___Context_5), value);
	}

	inline static int32_t get_offset_of_m_nullable_6() { return static_cast<int32_t>(offsetof(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E, ___m_nullable_6)); }
	inline CNullable_t7AE2FD6161F70658549CB2814F27E766A2C3085C * get_m_nullable_6() const { return ___m_nullable_6; }
	inline CNullable_t7AE2FD6161F70658549CB2814F27E766A2C3085C ** get_address_of_m_nullable_6() { return &___m_nullable_6; }
	inline void set_m_nullable_6(CNullable_t7AE2FD6161F70658549CB2814F27E766A2C3085C * value)
	{
		___m_nullable_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_nullable_6), value);
	}

	inline static int32_t get_offset_of_g_binopSignatures_9() { return static_cast<int32_t>(offsetof(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E, ___g_binopSignatures_9)); }
	inline BinOpSigU5BU5D_t5FAB057C77FA31908C3C5D928EBFBDE82EB1260B* get_g_binopSignatures_9() const { return ___g_binopSignatures_9; }
	inline BinOpSigU5BU5D_t5FAB057C77FA31908C3C5D928EBFBDE82EB1260B** get_address_of_g_binopSignatures_9() { return &___g_binopSignatures_9; }
	inline void set_g_binopSignatures_9(BinOpSigU5BU5D_t5FAB057C77FA31908C3C5D928EBFBDE82EB1260B* value)
	{
		___g_binopSignatures_9 = value;
		Il2CppCodeGenWriteBarrier((&___g_binopSignatures_9), value);
	}

	inline static int32_t get_offset_of_g_rguos_10() { return static_cast<int32_t>(offsetof(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E, ___g_rguos_10)); }
	inline UnaOpSigU5BU5D_tD8A410FEA2A14E00294B50610C9DFDF03317B702* get_g_rguos_10() const { return ___g_rguos_10; }
	inline UnaOpSigU5BU5D_tD8A410FEA2A14E00294B50610C9DFDF03317B702** get_address_of_g_rguos_10() { return &___g_rguos_10; }
	inline void set_g_rguos_10(UnaOpSigU5BU5D_tD8A410FEA2A14E00294B50610C9DFDF03317B702* value)
	{
		___g_rguos_10 = value;
		Il2CppCodeGenWriteBarrier((&___g_rguos_10), value);
	}
};

struct ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields
{
public:
	// System.Byte[][] Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder::s_betterConversionTable
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___s_betterConversionTable_0;
	// Microsoft.CSharp.RuntimeBinder.Errors.ErrorCode[] Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder::s_ReadOnlyLocalErrors
	ErrorCodeU5BU5D_tB94DF500819EF4A6CF9F0B42001C0DD3A96CEDF7* ___s_ReadOnlyLocalErrors_1;
	// Microsoft.CSharp.RuntimeBinder.Errors.ErrorCode[] Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder::s_ReadOnlyErrors
	ErrorCodeU5BU5D_tB94DF500819EF4A6CF9F0B42001C0DD3A96CEDF7* ___s_ReadOnlyErrors_2;
	// System.Byte[][] Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder::s_simpleTypeConversions
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___s_simpleTypeConversions_3;
	// System.Byte[][] Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder::s_simpleTypeBetter
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___s_simpleTypeBetter_4;
	// Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedType[] Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder::s_rgptIntOp
	PredefinedTypeU5BU5D_t7EA0AADE7719AD582C448A220A5447D60C2177C0* ___s_rgptIntOp_7;
	// Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedName[] Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder::s_EK2NAME
	PredefinedNameU5BU5D_t80E1FD2DF873DA212FE771FEF08C91DA7D13B11C* ___s_EK2NAME_8;

public:
	inline static int32_t get_offset_of_s_betterConversionTable_0() { return static_cast<int32_t>(offsetof(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields, ___s_betterConversionTable_0)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_s_betterConversionTable_0() const { return ___s_betterConversionTable_0; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_s_betterConversionTable_0() { return &___s_betterConversionTable_0; }
	inline void set_s_betterConversionTable_0(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___s_betterConversionTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_betterConversionTable_0), value);
	}

	inline static int32_t get_offset_of_s_ReadOnlyLocalErrors_1() { return static_cast<int32_t>(offsetof(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields, ___s_ReadOnlyLocalErrors_1)); }
	inline ErrorCodeU5BU5D_tB94DF500819EF4A6CF9F0B42001C0DD3A96CEDF7* get_s_ReadOnlyLocalErrors_1() const { return ___s_ReadOnlyLocalErrors_1; }
	inline ErrorCodeU5BU5D_tB94DF500819EF4A6CF9F0B42001C0DD3A96CEDF7** get_address_of_s_ReadOnlyLocalErrors_1() { return &___s_ReadOnlyLocalErrors_1; }
	inline void set_s_ReadOnlyLocalErrors_1(ErrorCodeU5BU5D_tB94DF500819EF4A6CF9F0B42001C0DD3A96CEDF7* value)
	{
		___s_ReadOnlyLocalErrors_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReadOnlyLocalErrors_1), value);
	}

	inline static int32_t get_offset_of_s_ReadOnlyErrors_2() { return static_cast<int32_t>(offsetof(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields, ___s_ReadOnlyErrors_2)); }
	inline ErrorCodeU5BU5D_tB94DF500819EF4A6CF9F0B42001C0DD3A96CEDF7* get_s_ReadOnlyErrors_2() const { return ___s_ReadOnlyErrors_2; }
	inline ErrorCodeU5BU5D_tB94DF500819EF4A6CF9F0B42001C0DD3A96CEDF7** get_address_of_s_ReadOnlyErrors_2() { return &___s_ReadOnlyErrors_2; }
	inline void set_s_ReadOnlyErrors_2(ErrorCodeU5BU5D_tB94DF500819EF4A6CF9F0B42001C0DD3A96CEDF7* value)
	{
		___s_ReadOnlyErrors_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReadOnlyErrors_2), value);
	}

	inline static int32_t get_offset_of_s_simpleTypeConversions_3() { return static_cast<int32_t>(offsetof(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields, ___s_simpleTypeConversions_3)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_s_simpleTypeConversions_3() const { return ___s_simpleTypeConversions_3; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_s_simpleTypeConversions_3() { return &___s_simpleTypeConversions_3; }
	inline void set_s_simpleTypeConversions_3(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___s_simpleTypeConversions_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_simpleTypeConversions_3), value);
	}

	inline static int32_t get_offset_of_s_simpleTypeBetter_4() { return static_cast<int32_t>(offsetof(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields, ___s_simpleTypeBetter_4)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_s_simpleTypeBetter_4() const { return ___s_simpleTypeBetter_4; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_s_simpleTypeBetter_4() { return &___s_simpleTypeBetter_4; }
	inline void set_s_simpleTypeBetter_4(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___s_simpleTypeBetter_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_simpleTypeBetter_4), value);
	}

	inline static int32_t get_offset_of_s_rgptIntOp_7() { return static_cast<int32_t>(offsetof(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields, ___s_rgptIntOp_7)); }
	inline PredefinedTypeU5BU5D_t7EA0AADE7719AD582C448A220A5447D60C2177C0* get_s_rgptIntOp_7() const { return ___s_rgptIntOp_7; }
	inline PredefinedTypeU5BU5D_t7EA0AADE7719AD582C448A220A5447D60C2177C0** get_address_of_s_rgptIntOp_7() { return &___s_rgptIntOp_7; }
	inline void set_s_rgptIntOp_7(PredefinedTypeU5BU5D_t7EA0AADE7719AD582C448A220A5447D60C2177C0* value)
	{
		___s_rgptIntOp_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_rgptIntOp_7), value);
	}

	inline static int32_t get_offset_of_s_EK2NAME_8() { return static_cast<int32_t>(offsetof(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields, ___s_EK2NAME_8)); }
	inline PredefinedNameU5BU5D_t80E1FD2DF873DA212FE771FEF08C91DA7D13B11C* get_s_EK2NAME_8() const { return ___s_EK2NAME_8; }
	inline PredefinedNameU5BU5D_t80E1FD2DF873DA212FE771FEF08C91DA7D13B11C** get_address_of_s_EK2NAME_8() { return &___s_EK2NAME_8; }
	inline void set_s_EK2NAME_8(PredefinedNameU5BU5D_t80E1FD2DF873DA212FE771FEF08C91DA7D13B11C* value)
	{
		___s_EK2NAME_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_EK2NAME_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONBINDER_T6AFB55222F11231641DF4A17EC2CBCC84142502E_H
#ifndef U3CU3EC__DISPLAYCLASS102_0_T5C5586FEC4D864AE5ACC60A2CE76ED0FB90890CD_H
#define U3CU3EC__DISPLAYCLASS102_0_T5C5586FEC4D864AE5ACC60A2CE76ED0FB90890CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/<>c__DisplayClass102_0
struct  U3CU3Ec__DisplayClass102_0_t5C5586FEC4D864AE5ACC60A2CE76ED0FB90890CD  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/<>c__DisplayClass102_0::<>4__this
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E * ___U3CU3E4__this_0;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/<>c__DisplayClass102_0::pDestType
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ___pDestType_1;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/<>c__DisplayClass102_0::pIntType
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ___pIntType_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass102_0_t5C5586FEC4D864AE5ACC60A2CE76ED0FB90890CD, ___U3CU3E4__this_0)); }
	inline ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_pDestType_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass102_0_t5C5586FEC4D864AE5ACC60A2CE76ED0FB90890CD, ___pDestType_1)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get_pDestType_1() const { return ___pDestType_1; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of_pDestType_1() { return &___pDestType_1; }
	inline void set_pDestType_1(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		___pDestType_1 = value;
		Il2CppCodeGenWriteBarrier((&___pDestType_1), value);
	}

	inline static int32_t get_offset_of_pIntType_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass102_0_t5C5586FEC4D864AE5ACC60A2CE76ED0FB90890CD, ___pIntType_2)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get_pIntType_2() const { return ___pIntType_2; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of_pIntType_2() { return &___pIntType_2; }
	inline void set_pIntType_2(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		___pIntType_2 = value;
		Il2CppCodeGenWriteBarrier((&___pIntType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS102_0_T5C5586FEC4D864AE5ACC60A2CE76ED0FB90890CD_H
#ifndef GROUPTOARGSBINDERRESULT_TABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0_H
#define GROUPTOARGSBINDERRESULT_TABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinderResult
struct  GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.MethPropWithInst Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinderResult::BestResult
	MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * ___BestResult_0;
	// Microsoft.CSharp.RuntimeBinder.Semantics.MethPropWithInst Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinderResult::AmbiguousResult
	MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * ___AmbiguousResult_1;
	// Microsoft.CSharp.RuntimeBinder.Semantics.MethPropWithInst Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinderResult::InaccessibleResult
	MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * ___InaccessibleResult_2;
	// Microsoft.CSharp.RuntimeBinder.Semantics.MethPropWithInst Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinderResult::UninferableResult
	MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * ___UninferableResult_3;
	// Microsoft.CSharp.RuntimeBinder.Semantics.MethPropWithInst Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinderResult::InconvertibleResult
	MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * ___InconvertibleResult_4;
	// System.Collections.Generic.List`1<Microsoft.CSharp.RuntimeBinder.Semantics.MethPropWithInst> Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinderResult::_inconvertibleResults
	List_1_t92334295FD181400CD37F835093DA258221B337D * ____inconvertibleResults_5;

public:
	inline static int32_t get_offset_of_BestResult_0() { return static_cast<int32_t>(offsetof(GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0, ___BestResult_0)); }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * get_BestResult_0() const { return ___BestResult_0; }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C ** get_address_of_BestResult_0() { return &___BestResult_0; }
	inline void set_BestResult_0(MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * value)
	{
		___BestResult_0 = value;
		Il2CppCodeGenWriteBarrier((&___BestResult_0), value);
	}

	inline static int32_t get_offset_of_AmbiguousResult_1() { return static_cast<int32_t>(offsetof(GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0, ___AmbiguousResult_1)); }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * get_AmbiguousResult_1() const { return ___AmbiguousResult_1; }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C ** get_address_of_AmbiguousResult_1() { return &___AmbiguousResult_1; }
	inline void set_AmbiguousResult_1(MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * value)
	{
		___AmbiguousResult_1 = value;
		Il2CppCodeGenWriteBarrier((&___AmbiguousResult_1), value);
	}

	inline static int32_t get_offset_of_InaccessibleResult_2() { return static_cast<int32_t>(offsetof(GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0, ___InaccessibleResult_2)); }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * get_InaccessibleResult_2() const { return ___InaccessibleResult_2; }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C ** get_address_of_InaccessibleResult_2() { return &___InaccessibleResult_2; }
	inline void set_InaccessibleResult_2(MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * value)
	{
		___InaccessibleResult_2 = value;
		Il2CppCodeGenWriteBarrier((&___InaccessibleResult_2), value);
	}

	inline static int32_t get_offset_of_UninferableResult_3() { return static_cast<int32_t>(offsetof(GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0, ___UninferableResult_3)); }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * get_UninferableResult_3() const { return ___UninferableResult_3; }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C ** get_address_of_UninferableResult_3() { return &___UninferableResult_3; }
	inline void set_UninferableResult_3(MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * value)
	{
		___UninferableResult_3 = value;
		Il2CppCodeGenWriteBarrier((&___UninferableResult_3), value);
	}

	inline static int32_t get_offset_of_InconvertibleResult_4() { return static_cast<int32_t>(offsetof(GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0, ___InconvertibleResult_4)); }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * get_InconvertibleResult_4() const { return ___InconvertibleResult_4; }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C ** get_address_of_InconvertibleResult_4() { return &___InconvertibleResult_4; }
	inline void set_InconvertibleResult_4(MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * value)
	{
		___InconvertibleResult_4 = value;
		Il2CppCodeGenWriteBarrier((&___InconvertibleResult_4), value);
	}

	inline static int32_t get_offset_of__inconvertibleResults_5() { return static_cast<int32_t>(offsetof(GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0, ____inconvertibleResults_5)); }
	inline List_1_t92334295FD181400CD37F835093DA258221B337D * get__inconvertibleResults_5() const { return ____inconvertibleResults_5; }
	inline List_1_t92334295FD181400CD37F835093DA258221B337D ** get_address_of__inconvertibleResults_5() { return &____inconvertibleResults_5; }
	inline void set__inconvertibleResults_5(List_1_t92334295FD181400CD37F835093DA258221B337D * value)
	{
		____inconvertibleResults_5 = value;
		Il2CppCodeGenWriteBarrier((&____inconvertibleResults_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPTOARGSBINDERRESULT_TABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0_H
#ifndef LISTEXTENSIONS_T9CEAD44FD94E94693A8C98990C678427B36E0A2B_H
#define LISTEXTENSIONS_T9CEAD44FD94E94693A8C98990C678427B36E0A2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ListExtensions
struct  ListExtensions_t9CEAD44FD94E94693A8C98990C678427B36E0A2B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTEXTENSIONS_T9CEAD44FD94E94693A8C98990C678427B36E0A2B_H
#ifndef OPERATORS_TCE07D36E96DCA68768BF8A0E22B083026541E255_H
#define OPERATORS_TCE07D36E96DCA68768BF8A0E22B083026541E255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.Operators
struct  Operators_tCE07D36E96DCA68768BF8A0E22B083026541E255  : public RuntimeObject
{
public:

public:
};

struct Operators_tCE07D36E96DCA68768BF8A0E22B083026541E255_StaticFields
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.Operators/OperatorInfo[] Microsoft.CSharp.RuntimeBinder.Semantics.Operators::s_operatorInfos
	OperatorInfoU5BU5D_t3F7DFFEA45E406A04BCC6D2B9BFE7DB54FB0FFF9* ___s_operatorInfos_0;
	// System.Collections.Generic.Dictionary`2<Microsoft.CSharp.RuntimeBinder.Syntax.Name,System.String> Microsoft.CSharp.RuntimeBinder.Semantics.Operators::s_operatorsByName
	Dictionary_2_t7E9D603770E286A4E73D896EBCF744FC1B256C3C * ___s_operatorsByName_1;

public:
	inline static int32_t get_offset_of_s_operatorInfos_0() { return static_cast<int32_t>(offsetof(Operators_tCE07D36E96DCA68768BF8A0E22B083026541E255_StaticFields, ___s_operatorInfos_0)); }
	inline OperatorInfoU5BU5D_t3F7DFFEA45E406A04BCC6D2B9BFE7DB54FB0FFF9* get_s_operatorInfos_0() const { return ___s_operatorInfos_0; }
	inline OperatorInfoU5BU5D_t3F7DFFEA45E406A04BCC6D2B9BFE7DB54FB0FFF9** get_address_of_s_operatorInfos_0() { return &___s_operatorInfos_0; }
	inline void set_s_operatorInfos_0(OperatorInfoU5BU5D_t3F7DFFEA45E406A04BCC6D2B9BFE7DB54FB0FFF9* value)
	{
		___s_operatorInfos_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_operatorInfos_0), value);
	}

	inline static int32_t get_offset_of_s_operatorsByName_1() { return static_cast<int32_t>(offsetof(Operators_tCE07D36E96DCA68768BF8A0E22B083026541E255_StaticFields, ___s_operatorsByName_1)); }
	inline Dictionary_2_t7E9D603770E286A4E73D896EBCF744FC1B256C3C * get_s_operatorsByName_1() const { return ___s_operatorsByName_1; }
	inline Dictionary_2_t7E9D603770E286A4E73D896EBCF744FC1B256C3C ** get_address_of_s_operatorsByName_1() { return &___s_operatorsByName_1; }
	inline void set_s_operatorsByName_1(Dictionary_2_t7E9D603770E286A4E73D896EBCF744FC1B256C3C * value)
	{
		___s_operatorsByName_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_operatorsByName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATORS_TCE07D36E96DCA68768BF8A0E22B083026541E255_H
#ifndef UDCONVINFO_TE0FCAD27FF1838A58B86891384B3F2F62B302FFB_H
#define UDCONVINFO_TE0FCAD27FF1838A58B86891384B3F2F62B302FFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.UdConvInfo
struct  UdConvInfo_tE0FCAD27FF1838A58B86891384B3F2F62B302FFB  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.MethWithType Microsoft.CSharp.RuntimeBinder.Semantics.UdConvInfo::mwt
	MethWithType_t4CCE9B8DC13DE224B4CE2FCAA33226ACA7380616 * ___mwt_0;
	// System.Boolean Microsoft.CSharp.RuntimeBinder.Semantics.UdConvInfo::fSrcImplicit
	bool ___fSrcImplicit_1;
	// System.Boolean Microsoft.CSharp.RuntimeBinder.Semantics.UdConvInfo::fDstImplicit
	bool ___fDstImplicit_2;

public:
	inline static int32_t get_offset_of_mwt_0() { return static_cast<int32_t>(offsetof(UdConvInfo_tE0FCAD27FF1838A58B86891384B3F2F62B302FFB, ___mwt_0)); }
	inline MethWithType_t4CCE9B8DC13DE224B4CE2FCAA33226ACA7380616 * get_mwt_0() const { return ___mwt_0; }
	inline MethWithType_t4CCE9B8DC13DE224B4CE2FCAA33226ACA7380616 ** get_address_of_mwt_0() { return &___mwt_0; }
	inline void set_mwt_0(MethWithType_t4CCE9B8DC13DE224B4CE2FCAA33226ACA7380616 * value)
	{
		___mwt_0 = value;
		Il2CppCodeGenWriteBarrier((&___mwt_0), value);
	}

	inline static int32_t get_offset_of_fSrcImplicit_1() { return static_cast<int32_t>(offsetof(UdConvInfo_tE0FCAD27FF1838A58B86891384B3F2F62B302FFB, ___fSrcImplicit_1)); }
	inline bool get_fSrcImplicit_1() const { return ___fSrcImplicit_1; }
	inline bool* get_address_of_fSrcImplicit_1() { return &___fSrcImplicit_1; }
	inline void set_fSrcImplicit_1(bool value)
	{
		___fSrcImplicit_1 = value;
	}

	inline static int32_t get_offset_of_fDstImplicit_2() { return static_cast<int32_t>(offsetof(UdConvInfo_tE0FCAD27FF1838A58B86891384B3F2F62B302FFB, ___fDstImplicit_2)); }
	inline bool get_fDstImplicit_2() const { return ___fDstImplicit_2; }
	inline bool* get_address_of_fDstImplicit_2() { return &___fDstImplicit_2; }
	inline void set_fDstImplicit_2(bool value)
	{
		___fDstImplicit_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDCONVINFO_TE0FCAD27FF1838A58B86891384B3F2F62B302FFB_H
#ifndef SYMBOLTABLE_TF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C_H
#define SYMBOLTABLE_TF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.SymbolTable
struct  SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<System.Type> Microsoft.CSharp.RuntimeBinder.SymbolTable::_typesWithConversionsLoaded
	HashSet_1_t4539A1D24467B4BCB004FE3DFB4D15C90372E5E7 * ____typesWithConversionsLoaded_0;
	// System.Collections.Generic.HashSet`1<Microsoft.CSharp.RuntimeBinder.SymbolTable/NameHashKey> Microsoft.CSharp.RuntimeBinder.SymbolTable::_namesLoadedForEachType
	HashSet_1_t10E1E5C7A746A1156C4CD5A0928A5D4F6CFD684F * ____namesLoadedForEachType_1;
	// Microsoft.CSharp.RuntimeBinder.Semantics.SYMTBL Microsoft.CSharp.RuntimeBinder.SymbolTable::_symbolTable
	SYMTBL_tD9CD6C1E5BF2477F2C025858E574A9F0624ACCB8 * ____symbolTable_2;
	// Microsoft.CSharp.RuntimeBinder.Semantics.SymFactory Microsoft.CSharp.RuntimeBinder.SymbolTable::_symFactory
	SymFactory_t132D3AF45C44031D0EA63987998943D16C46D1F1 * ____symFactory_3;
	// Microsoft.CSharp.RuntimeBinder.Syntax.NameManager Microsoft.CSharp.RuntimeBinder.SymbolTable::_nameManager
	NameManager_t965C30E738C964824296FF875F6E7B4828AEDD31 * ____nameManager_4;
	// Microsoft.CSharp.RuntimeBinder.Semantics.TypeManager Microsoft.CSharp.RuntimeBinder.SymbolTable::_typeManager
	TypeManager_t67F89DC00FB47A6EBA5060FEF0CFF62B74F073B6 * ____typeManager_5;
	// Microsoft.CSharp.RuntimeBinder.Semantics.BSYMMGR Microsoft.CSharp.RuntimeBinder.SymbolTable::_bsymmgr
	BSYMMGR_tBA5223E26DB1C60FC714AA4B84D365380236B957 * ____bsymmgr_6;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CSemanticChecker Microsoft.CSharp.RuntimeBinder.SymbolTable::_semanticChecker
	CSemanticChecker_t392A4B642F3784AA63593EAEAA5D6E36F89E2A8A * ____semanticChecker_7;
	// Microsoft.CSharp.RuntimeBinder.Semantics.NamespaceSymbol Microsoft.CSharp.RuntimeBinder.SymbolTable::_rootNamespace
	NamespaceSymbol_t09CBB22705730203A7D1B5F35B62D46C7EA94253 * ____rootNamespace_8;

public:
	inline static int32_t get_offset_of__typesWithConversionsLoaded_0() { return static_cast<int32_t>(offsetof(SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C, ____typesWithConversionsLoaded_0)); }
	inline HashSet_1_t4539A1D24467B4BCB004FE3DFB4D15C90372E5E7 * get__typesWithConversionsLoaded_0() const { return ____typesWithConversionsLoaded_0; }
	inline HashSet_1_t4539A1D24467B4BCB004FE3DFB4D15C90372E5E7 ** get_address_of__typesWithConversionsLoaded_0() { return &____typesWithConversionsLoaded_0; }
	inline void set__typesWithConversionsLoaded_0(HashSet_1_t4539A1D24467B4BCB004FE3DFB4D15C90372E5E7 * value)
	{
		____typesWithConversionsLoaded_0 = value;
		Il2CppCodeGenWriteBarrier((&____typesWithConversionsLoaded_0), value);
	}

	inline static int32_t get_offset_of__namesLoadedForEachType_1() { return static_cast<int32_t>(offsetof(SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C, ____namesLoadedForEachType_1)); }
	inline HashSet_1_t10E1E5C7A746A1156C4CD5A0928A5D4F6CFD684F * get__namesLoadedForEachType_1() const { return ____namesLoadedForEachType_1; }
	inline HashSet_1_t10E1E5C7A746A1156C4CD5A0928A5D4F6CFD684F ** get_address_of__namesLoadedForEachType_1() { return &____namesLoadedForEachType_1; }
	inline void set__namesLoadedForEachType_1(HashSet_1_t10E1E5C7A746A1156C4CD5A0928A5D4F6CFD684F * value)
	{
		____namesLoadedForEachType_1 = value;
		Il2CppCodeGenWriteBarrier((&____namesLoadedForEachType_1), value);
	}

	inline static int32_t get_offset_of__symbolTable_2() { return static_cast<int32_t>(offsetof(SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C, ____symbolTable_2)); }
	inline SYMTBL_tD9CD6C1E5BF2477F2C025858E574A9F0624ACCB8 * get__symbolTable_2() const { return ____symbolTable_2; }
	inline SYMTBL_tD9CD6C1E5BF2477F2C025858E574A9F0624ACCB8 ** get_address_of__symbolTable_2() { return &____symbolTable_2; }
	inline void set__symbolTable_2(SYMTBL_tD9CD6C1E5BF2477F2C025858E574A9F0624ACCB8 * value)
	{
		____symbolTable_2 = value;
		Il2CppCodeGenWriteBarrier((&____symbolTable_2), value);
	}

	inline static int32_t get_offset_of__symFactory_3() { return static_cast<int32_t>(offsetof(SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C, ____symFactory_3)); }
	inline SymFactory_t132D3AF45C44031D0EA63987998943D16C46D1F1 * get__symFactory_3() const { return ____symFactory_3; }
	inline SymFactory_t132D3AF45C44031D0EA63987998943D16C46D1F1 ** get_address_of__symFactory_3() { return &____symFactory_3; }
	inline void set__symFactory_3(SymFactory_t132D3AF45C44031D0EA63987998943D16C46D1F1 * value)
	{
		____symFactory_3 = value;
		Il2CppCodeGenWriteBarrier((&____symFactory_3), value);
	}

	inline static int32_t get_offset_of__nameManager_4() { return static_cast<int32_t>(offsetof(SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C, ____nameManager_4)); }
	inline NameManager_t965C30E738C964824296FF875F6E7B4828AEDD31 * get__nameManager_4() const { return ____nameManager_4; }
	inline NameManager_t965C30E738C964824296FF875F6E7B4828AEDD31 ** get_address_of__nameManager_4() { return &____nameManager_4; }
	inline void set__nameManager_4(NameManager_t965C30E738C964824296FF875F6E7B4828AEDD31 * value)
	{
		____nameManager_4 = value;
		Il2CppCodeGenWriteBarrier((&____nameManager_4), value);
	}

	inline static int32_t get_offset_of__typeManager_5() { return static_cast<int32_t>(offsetof(SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C, ____typeManager_5)); }
	inline TypeManager_t67F89DC00FB47A6EBA5060FEF0CFF62B74F073B6 * get__typeManager_5() const { return ____typeManager_5; }
	inline TypeManager_t67F89DC00FB47A6EBA5060FEF0CFF62B74F073B6 ** get_address_of__typeManager_5() { return &____typeManager_5; }
	inline void set__typeManager_5(TypeManager_t67F89DC00FB47A6EBA5060FEF0CFF62B74F073B6 * value)
	{
		____typeManager_5 = value;
		Il2CppCodeGenWriteBarrier((&____typeManager_5), value);
	}

	inline static int32_t get_offset_of__bsymmgr_6() { return static_cast<int32_t>(offsetof(SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C, ____bsymmgr_6)); }
	inline BSYMMGR_tBA5223E26DB1C60FC714AA4B84D365380236B957 * get__bsymmgr_6() const { return ____bsymmgr_6; }
	inline BSYMMGR_tBA5223E26DB1C60FC714AA4B84D365380236B957 ** get_address_of__bsymmgr_6() { return &____bsymmgr_6; }
	inline void set__bsymmgr_6(BSYMMGR_tBA5223E26DB1C60FC714AA4B84D365380236B957 * value)
	{
		____bsymmgr_6 = value;
		Il2CppCodeGenWriteBarrier((&____bsymmgr_6), value);
	}

	inline static int32_t get_offset_of__semanticChecker_7() { return static_cast<int32_t>(offsetof(SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C, ____semanticChecker_7)); }
	inline CSemanticChecker_t392A4B642F3784AA63593EAEAA5D6E36F89E2A8A * get__semanticChecker_7() const { return ____semanticChecker_7; }
	inline CSemanticChecker_t392A4B642F3784AA63593EAEAA5D6E36F89E2A8A ** get_address_of__semanticChecker_7() { return &____semanticChecker_7; }
	inline void set__semanticChecker_7(CSemanticChecker_t392A4B642F3784AA63593EAEAA5D6E36F89E2A8A * value)
	{
		____semanticChecker_7 = value;
		Il2CppCodeGenWriteBarrier((&____semanticChecker_7), value);
	}

	inline static int32_t get_offset_of__rootNamespace_8() { return static_cast<int32_t>(offsetof(SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C, ____rootNamespace_8)); }
	inline NamespaceSymbol_t09CBB22705730203A7D1B5F35B62D46C7EA94253 * get__rootNamespace_8() const { return ____rootNamespace_8; }
	inline NamespaceSymbol_t09CBB22705730203A7D1B5F35B62D46C7EA94253 ** get_address_of__rootNamespace_8() { return &____rootNamespace_8; }
	inline void set__rootNamespace_8(NamespaceSymbol_t09CBB22705730203A7D1B5F35B62D46C7EA94253 * value)
	{
		____rootNamespace_8 = value;
		Il2CppCodeGenWriteBarrier((&____rootNamespace_8), value);
	}
};

struct SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C_StaticFields
{
public:
	// System.Type Microsoft.CSharp.RuntimeBinder.SymbolTable::s_Sentinel
	Type_t * ___s_Sentinel_9;
	// System.Type Microsoft.CSharp.RuntimeBinder.SymbolTable::s_EventRegistrationTokenType
	Type_t * ___s_EventRegistrationTokenType_10;
	// System.Type Microsoft.CSharp.RuntimeBinder.SymbolTable::s_WindowsRuntimeMarshal
	Type_t * ___s_WindowsRuntimeMarshal_11;
	// System.Type Microsoft.CSharp.RuntimeBinder.SymbolTable::s_EventRegistrationTokenTable
	Type_t * ___s_EventRegistrationTokenTable_12;

public:
	inline static int32_t get_offset_of_s_Sentinel_9() { return static_cast<int32_t>(offsetof(SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C_StaticFields, ___s_Sentinel_9)); }
	inline Type_t * get_s_Sentinel_9() const { return ___s_Sentinel_9; }
	inline Type_t ** get_address_of_s_Sentinel_9() { return &___s_Sentinel_9; }
	inline void set_s_Sentinel_9(Type_t * value)
	{
		___s_Sentinel_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_Sentinel_9), value);
	}

	inline static int32_t get_offset_of_s_EventRegistrationTokenType_10() { return static_cast<int32_t>(offsetof(SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C_StaticFields, ___s_EventRegistrationTokenType_10)); }
	inline Type_t * get_s_EventRegistrationTokenType_10() const { return ___s_EventRegistrationTokenType_10; }
	inline Type_t ** get_address_of_s_EventRegistrationTokenType_10() { return &___s_EventRegistrationTokenType_10; }
	inline void set_s_EventRegistrationTokenType_10(Type_t * value)
	{
		___s_EventRegistrationTokenType_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_EventRegistrationTokenType_10), value);
	}

	inline static int32_t get_offset_of_s_WindowsRuntimeMarshal_11() { return static_cast<int32_t>(offsetof(SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C_StaticFields, ___s_WindowsRuntimeMarshal_11)); }
	inline Type_t * get_s_WindowsRuntimeMarshal_11() const { return ___s_WindowsRuntimeMarshal_11; }
	inline Type_t ** get_address_of_s_WindowsRuntimeMarshal_11() { return &___s_WindowsRuntimeMarshal_11; }
	inline void set_s_WindowsRuntimeMarshal_11(Type_t * value)
	{
		___s_WindowsRuntimeMarshal_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_WindowsRuntimeMarshal_11), value);
	}

	inline static int32_t get_offset_of_s_EventRegistrationTokenTable_12() { return static_cast<int32_t>(offsetof(SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C_StaticFields, ___s_EventRegistrationTokenTable_12)); }
	inline Type_t * get_s_EventRegistrationTokenTable_12() const { return ___s_EventRegistrationTokenTable_12; }
	inline Type_t ** get_address_of_s_EventRegistrationTokenTable_12() { return &___s_EventRegistrationTokenTable_12; }
	inline void set_s_EventRegistrationTokenTable_12(Type_t * value)
	{
		___s_EventRegistrationTokenTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_EventRegistrationTokenTable_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMBOLTABLE_TF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C_H
#ifndef U3CU3EC__DISPLAYCLASS15_0_T8EF4067FAF6A75CB6BF5EFF004BE26788651C6A8_H
#define U3CU3EC__DISPLAYCLASS15_0_T8EF4067FAF6A75CB6BF5EFF004BE26788651C6A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.SymbolTable/<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_t8EF4067FAF6A75CB6BF5EFF004BE26788651C6A8  : public RuntimeObject
{
public:
	// System.String Microsoft.CSharp.RuntimeBinder.SymbolTable/<>c__DisplayClass15_0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_t8EF4067FAF6A75CB6BF5EFF004BE26788651C6A8, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_0_T8EF4067FAF6A75CB6BF5EFF004BE26788651C6A8_H
#ifndef U3CU3EC__DISPLAYCLASS15_1_TF3C0A04AC6B7A782CA637D9A9A4AF0F81319EE96_H
#define U3CU3EC__DISPLAYCLASS15_1_TF3C0A04AC6B7A782CA637D9A9A4AF0F81319EE96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.SymbolTable/<>c__DisplayClass15_1
struct  U3CU3Ec__DisplayClass15_1_tF3C0A04AC6B7A782CA637D9A9A4AF0F81319EE96  : public RuntimeObject
{
public:
	// System.Type Microsoft.CSharp.RuntimeBinder.SymbolTable/<>c__DisplayClass15_1::type
	Type_t * ___type_0;
	// Microsoft.CSharp.RuntimeBinder.SymbolTable/<>c__DisplayClass15_0 Microsoft.CSharp.RuntimeBinder.SymbolTable/<>c__DisplayClass15_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass15_0_t8EF4067FAF6A75CB6BF5EFF004BE26788651C6A8 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_1_tF3C0A04AC6B7A782CA637D9A9A4AF0F81319EE96, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_1_tF3C0A04AC6B7A782CA637D9A9A4AF0F81319EE96, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass15_0_t8EF4067FAF6A75CB6BF5EFF004BE26788651C6A8 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass15_0_t8EF4067FAF6A75CB6BF5EFF004BE26788651C6A8 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass15_0_t8EF4067FAF6A75CB6BF5EFF004BE26788651C6A8 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_1_TF3C0A04AC6B7A782CA637D9A9A4AF0F81319EE96_H
#ifndef U3CU3EC__DISPLAYCLASS30_0_T431E47B24016E917355B79B0890BA54AEC4905B6_H
#define U3CU3EC__DISPLAYCLASS30_0_T431E47B24016E917355B79B0890BA54AEC4905B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.SymbolTable/<>c__DisplayClass30_0
struct  U3CU3Ec__DisplayClass30_0_t431E47B24016E917355B79B0890BA54AEC4905B6  : public RuntimeObject
{
public:
	// System.Reflection.MethodBase Microsoft.CSharp.RuntimeBinder.SymbolTable/<>c__DisplayClass30_0::methodBase
	MethodBase_t * ___methodBase_0;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Microsoft.CSharp.RuntimeBinder.SymbolTable/<>c__DisplayClass30_0::<>9__0
	Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * ___U3CU3E9__0_1;

public:
	inline static int32_t get_offset_of_methodBase_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t431E47B24016E917355B79B0890BA54AEC4905B6, ___methodBase_0)); }
	inline MethodBase_t * get_methodBase_0() const { return ___methodBase_0; }
	inline MethodBase_t ** get_address_of_methodBase_0() { return &___methodBase_0; }
	inline void set_methodBase_0(MethodBase_t * value)
	{
		___methodBase_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodBase_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t431E47B24016E917355B79B0890BA54AEC4905B6, ___U3CU3E9__0_1)); }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * get_U3CU3E9__0_1() const { return ___U3CU3E9__0_1; }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 ** get_address_of_U3CU3E9__0_1() { return &___U3CU3E9__0_1; }
	inline void set_U3CU3E9__0_1(Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * value)
	{
		___U3CU3E9__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS30_0_T431E47B24016E917355B79B0890BA54AEC4905B6_H
#ifndef U3CU3EC__DISPLAYCLASS50_0_TC77DA813449A11CCCC2E2457404BB440124CBE7F_H
#define U3CU3EC__DISPLAYCLASS50_0_TC77DA813449A11CCCC2E2457404BB440124CBE7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.SymbolTable/<>c__DisplayClass50_0
struct  U3CU3Ec__DisplayClass50_0_tC77DA813449A11CCCC2E2457404BB440124CBE7F  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Syntax.Name Microsoft.CSharp.RuntimeBinder.SymbolTable/<>c__DisplayClass50_0::property
	Name_t95244514D054D80B6614F5E2275EC034F1527725 * ___property_0;

public:
	inline static int32_t get_offset_of_property_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass50_0_tC77DA813449A11CCCC2E2457404BB440124CBE7F, ___property_0)); }
	inline Name_t95244514D054D80B6614F5E2275EC034F1527725 * get_property_0() const { return ___property_0; }
	inline Name_t95244514D054D80B6614F5E2275EC034F1527725 ** get_address_of_property_0() { return &___property_0; }
	inline void set_property_0(Name_t95244514D054D80B6614F5E2275EC034F1527725 * value)
	{
		___property_0 = value;
		Il2CppCodeGenWriteBarrier((&___property_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS50_0_TC77DA813449A11CCCC2E2457404BB440124CBE7F_H
#ifndef U3CU3EC__DISPLAYCLASS52_0_T4015D3895F4DF718E7581D0E54E26D381100B0FA_H
#define U3CU3EC__DISPLAYCLASS52_0_T4015D3895F4DF718E7581D0E54E26D381100B0FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.SymbolTable/<>c__DisplayClass52_0
struct  U3CU3Ec__DisplayClass52_0_t4015D3895F4DF718E7581D0E54E26D381100B0FA  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Syntax.Name Microsoft.CSharp.RuntimeBinder.SymbolTable/<>c__DisplayClass52_0::methodName
	Name_t95244514D054D80B6614F5E2275EC034F1527725 * ___methodName_0;
	// System.Type Microsoft.CSharp.RuntimeBinder.SymbolTable/<>c__DisplayClass52_0::t
	Type_t * ___t_1;

public:
	inline static int32_t get_offset_of_methodName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass52_0_t4015D3895F4DF718E7581D0E54E26D381100B0FA, ___methodName_0)); }
	inline Name_t95244514D054D80B6614F5E2275EC034F1527725 * get_methodName_0() const { return ___methodName_0; }
	inline Name_t95244514D054D80B6614F5E2275EC034F1527725 ** get_address_of_methodName_0() { return &___methodName_0; }
	inline void set_methodName_0(Name_t95244514D054D80B6614F5E2275EC034F1527725 * value)
	{
		___methodName_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodName_0), value);
	}

	inline static int32_t get_offset_of_t_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass52_0_t4015D3895F4DF718E7581D0E54E26D381100B0FA, ___t_1)); }
	inline Type_t * get_t_1() const { return ___t_1; }
	inline Type_t ** get_address_of_t_1() { return &___t_1; }
	inline void set_t_1(Type_t * value)
	{
		___t_1 = value;
		Il2CppCodeGenWriteBarrier((&___t_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS52_0_T4015D3895F4DF718E7581D0E54E26D381100B0FA_H
#ifndef NAMEHASHKEY_TC6EE60C80A020C655F03995397C8DCB469E41040_H
#define NAMEHASHKEY_TC6EE60C80A020C655F03995397C8DCB469E41040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.SymbolTable/NameHashKey
struct  NameHashKey_tC6EE60C80A020C655F03995397C8DCB469E41040  : public RuntimeObject
{
public:
	// System.Type Microsoft.CSharp.RuntimeBinder.SymbolTable/NameHashKey::type
	Type_t * ___type_0;
	// System.String Microsoft.CSharp.RuntimeBinder.SymbolTable/NameHashKey::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(NameHashKey_tC6EE60C80A020C655F03995397C8DCB469E41040, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(NameHashKey_tC6EE60C80A020C655F03995397C8DCB469E41040, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEHASHKEY_TC6EE60C80A020C655F03995397C8DCB469E41040_H
#ifndef NAME_T95244514D054D80B6614F5E2275EC034F1527725_H
#define NAME_T95244514D054D80B6614F5E2275EC034F1527725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Syntax.Name
struct  Name_t95244514D054D80B6614F5E2275EC034F1527725  : public RuntimeObject
{
public:
	// System.String Microsoft.CSharp.RuntimeBinder.Syntax.Name::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Name_t95244514D054D80B6614F5E2275EC034F1527725, ___U3CTextU3Ek__BackingField_0)); }
	inline String_t* get_U3CTextU3Ek__BackingField_0() const { return ___U3CTextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_0() { return &___U3CTextU3Ek__BackingField_0; }
	inline void set_U3CTextU3Ek__BackingField_0(String_t* value)
	{
		___U3CTextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAME_T95244514D054D80B6614F5E2275EC034F1527725_H
#ifndef NAMEMANAGER_T965C30E738C964824296FF875F6E7B4828AEDD31_H
#define NAMEMANAGER_T965C30E738C964824296FF875F6E7B4828AEDD31_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Syntax.NameManager
struct  NameManager_t965C30E738C964824296FF875F6E7B4828AEDD31  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Syntax.NameTable Microsoft.CSharp.RuntimeBinder.Syntax.NameManager::_names
	NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0 * ____names_2;

public:
	inline static int32_t get_offset_of__names_2() { return static_cast<int32_t>(offsetof(NameManager_t965C30E738C964824296FF875F6E7B4828AEDD31, ____names_2)); }
	inline NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0 * get__names_2() const { return ____names_2; }
	inline NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0 ** get_address_of__names_2() { return &____names_2; }
	inline void set__names_2(NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0 * value)
	{
		____names_2 = value;
		Il2CppCodeGenWriteBarrier((&____names_2), value);
	}
};

struct NameManager_t965C30E738C964824296FF875F6E7B4828AEDD31_StaticFields
{
public:
	// Microsoft.CSharp.RuntimeBinder.Syntax.Name[] Microsoft.CSharp.RuntimeBinder.Syntax.NameManager::s_predefinedNames
	NameU5BU5D_tAF4AAC512553ABE0CA97252D968C3910789AFAD3* ___s_predefinedNames_0;
	// Microsoft.CSharp.RuntimeBinder.Syntax.NameTable Microsoft.CSharp.RuntimeBinder.Syntax.NameManager::s_knownNames
	NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0 * ___s_knownNames_1;

public:
	inline static int32_t get_offset_of_s_predefinedNames_0() { return static_cast<int32_t>(offsetof(NameManager_t965C30E738C964824296FF875F6E7B4828AEDD31_StaticFields, ___s_predefinedNames_0)); }
	inline NameU5BU5D_tAF4AAC512553ABE0CA97252D968C3910789AFAD3* get_s_predefinedNames_0() const { return ___s_predefinedNames_0; }
	inline NameU5BU5D_tAF4AAC512553ABE0CA97252D968C3910789AFAD3** get_address_of_s_predefinedNames_0() { return &___s_predefinedNames_0; }
	inline void set_s_predefinedNames_0(NameU5BU5D_tAF4AAC512553ABE0CA97252D968C3910789AFAD3* value)
	{
		___s_predefinedNames_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_predefinedNames_0), value);
	}

	inline static int32_t get_offset_of_s_knownNames_1() { return static_cast<int32_t>(offsetof(NameManager_t965C30E738C964824296FF875F6E7B4828AEDD31_StaticFields, ___s_knownNames_1)); }
	inline NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0 * get_s_knownNames_1() const { return ___s_knownNames_1; }
	inline NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0 ** get_address_of_s_knownNames_1() { return &___s_knownNames_1; }
	inline void set_s_knownNames_1(NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0 * value)
	{
		___s_knownNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_knownNames_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEMANAGER_T965C30E738C964824296FF875F6E7B4828AEDD31_H
#ifndef NAMETABLE_T1E4EA7C1780B6B8F6D6F751D619FA151A10181A0_H
#define NAMETABLE_T1E4EA7C1780B6B8F6D6F751D619FA151A10181A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Syntax.NameTable
struct  NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Syntax.NameTable/Entry[] Microsoft.CSharp.RuntimeBinder.Syntax.NameTable::_entries
	EntryU5BU5D_t8CE801A724A0E7C021F32553EDDFA0745502D2EE* ____entries_0;
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Syntax.NameTable::_count
	int32_t ____count_1;
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Syntax.NameTable::_mask
	int32_t ____mask_2;

public:
	inline static int32_t get_offset_of__entries_0() { return static_cast<int32_t>(offsetof(NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0, ____entries_0)); }
	inline EntryU5BU5D_t8CE801A724A0E7C021F32553EDDFA0745502D2EE* get__entries_0() const { return ____entries_0; }
	inline EntryU5BU5D_t8CE801A724A0E7C021F32553EDDFA0745502D2EE** get_address_of__entries_0() { return &____entries_0; }
	inline void set__entries_0(EntryU5BU5D_t8CE801A724A0E7C021F32553EDDFA0745502D2EE* value)
	{
		____entries_0 = value;
		Il2CppCodeGenWriteBarrier((&____entries_0), value);
	}

	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}

	inline static int32_t get_offset_of__mask_2() { return static_cast<int32_t>(offsetof(NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0, ____mask_2)); }
	inline int32_t get__mask_2() const { return ____mask_2; }
	inline int32_t* get_address_of__mask_2() { return &____mask_2; }
	inline void set__mask_2(int32_t value)
	{
		____mask_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETABLE_T1E4EA7C1780B6B8F6D6F751D619FA151A10181A0_H
#ifndef ENTRY_T79E8E453C852A3AF2A2A1E82E9E41BEED4B3DEF9_H
#define ENTRY_T79E8E453C852A3AF2A2A1E82E9E41BEED4B3DEF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Syntax.NameTable/Entry
struct  Entry_t79E8E453C852A3AF2A2A1E82E9E41BEED4B3DEF9  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Syntax.Name Microsoft.CSharp.RuntimeBinder.Syntax.NameTable/Entry::Name
	Name_t95244514D054D80B6614F5E2275EC034F1527725 * ___Name_0;
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Syntax.NameTable/Entry::HashCode
	int32_t ___HashCode_1;
	// Microsoft.CSharp.RuntimeBinder.Syntax.NameTable/Entry Microsoft.CSharp.RuntimeBinder.Syntax.NameTable/Entry::Next
	Entry_t79E8E453C852A3AF2A2A1E82E9E41BEED4B3DEF9 * ___Next_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(Entry_t79E8E453C852A3AF2A2A1E82E9E41BEED4B3DEF9, ___Name_0)); }
	inline Name_t95244514D054D80B6614F5E2275EC034F1527725 * get_Name_0() const { return ___Name_0; }
	inline Name_t95244514D054D80B6614F5E2275EC034F1527725 ** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(Name_t95244514D054D80B6614F5E2275EC034F1527725 * value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_HashCode_1() { return static_cast<int32_t>(offsetof(Entry_t79E8E453C852A3AF2A2A1E82E9E41BEED4B3DEF9, ___HashCode_1)); }
	inline int32_t get_HashCode_1() const { return ___HashCode_1; }
	inline int32_t* get_address_of_HashCode_1() { return &___HashCode_1; }
	inline void set_HashCode_1(int32_t value)
	{
		___HashCode_1 = value;
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(Entry_t79E8E453C852A3AF2A2A1E82E9E41BEED4B3DEF9, ___Next_2)); }
	inline Entry_t79E8E453C852A3AF2A2A1E82E9E41BEED4B3DEF9 * get_Next_2() const { return ___Next_2; }
	inline Entry_t79E8E453C852A3AF2A2A1E82E9E41BEED4B3DEF9 ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(Entry_t79E8E453C852A3AF2A2A1E82E9E41BEED4B3DEF9 * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier((&___Next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T79E8E453C852A3AF2A2A1E82E9E41BEED4B3DEF9_H
#ifndef TOKENFACTS_T65E6B160EC1CAFB6ED1E96531A27688DBCCA0F24_H
#define TOKENFACTS_T65E6B160EC1CAFB6ED1E96531A27688DBCCA0F24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Syntax.TokenFacts
struct  TokenFacts_t65E6B160EC1CAFB6ED1E96531A27688DBCCA0F24  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENFACTS_T65E6B160EC1CAFB6ED1E96531A27688DBCCA0F24_H
#ifndef SR_T3A884991BE6F1DADB1F0F35E8C244098C970521F_H
#define SR_T3A884991BE6F1DADB1F0F35E8C244098C970521F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SR
struct  SR_t3A884991BE6F1DADB1F0F35E8C244098C970521F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SR_T3A884991BE6F1DADB1F0F35E8C244098C970521F_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef CALLSITEBINDER_T76CF754DF48E3820B386E9C71F8DA13FCDD1C889_H
#define CALLSITEBINDER_T76CF754DF48E3820B386E9C71F8DA13FCDD1C889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.CallSiteBinder
struct  CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Object> System.Runtime.CompilerServices.CallSiteBinder::Cache
	Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 * ___Cache_0;

public:
	inline static int32_t get_offset_of_Cache_0() { return static_cast<int32_t>(offsetof(CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889, ___Cache_0)); }
	inline Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 * get_Cache_0() const { return ___Cache_0; }
	inline Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 ** get_address_of_Cache_0() { return &___Cache_0; }
	inline void set_Cache_0(Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 * value)
	{
		___Cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___Cache_0), value);
	}
};

struct CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889_StaticFields
{
public:
	// System.Linq.Expressions.LabelTarget System.Runtime.CompilerServices.CallSiteBinder::<UpdateLabel>k__BackingField
	LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 * ___U3CUpdateLabelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CUpdateLabelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889_StaticFields, ___U3CUpdateLabelU3Ek__BackingField_1)); }
	inline LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 * get_U3CUpdateLabelU3Ek__BackingField_1() const { return ___U3CUpdateLabelU3Ek__BackingField_1; }
	inline LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 ** get_address_of_U3CUpdateLabelU3Ek__BackingField_1() { return &___U3CUpdateLabelU3Ek__BackingField_1; }
	inline void set_U3CUpdateLabelU3Ek__BackingField_1(LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 * value)
	{
		___U3CUpdateLabelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUpdateLabelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLSITEBINDER_T76CF754DF48E3820B386E9C71F8DA13FCDD1C889_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef DOWNLOADEVENTARGS_TE8931C7F6C9F12BB6E657D9F11491CD22D94BC6A_H
#define DOWNLOADEVENTARGS_TE8931C7F6C9F12BB6E657D9F11491CD22D94BC6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Downloader.DownloadEventArgs
struct  DownloadEventArgs_tE8931C7F6C9F12BB6E657D9F11491CD22D94BC6A  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Int32 MHLab.PATCH.Downloader.DownloadEventArgs::percentDone
	int32_t ___percentDone_1;
	// System.Int64 MHLab.PATCH.Downloader.DownloadEventArgs::totalFileSize
	int64_t ___totalFileSize_2;
	// System.Int64 MHLab.PATCH.Downloader.DownloadEventArgs::currentFileSize
	int64_t ___currentFileSize_3;

public:
	inline static int32_t get_offset_of_percentDone_1() { return static_cast<int32_t>(offsetof(DownloadEventArgs_tE8931C7F6C9F12BB6E657D9F11491CD22D94BC6A, ___percentDone_1)); }
	inline int32_t get_percentDone_1() const { return ___percentDone_1; }
	inline int32_t* get_address_of_percentDone_1() { return &___percentDone_1; }
	inline void set_percentDone_1(int32_t value)
	{
		___percentDone_1 = value;
	}

	inline static int32_t get_offset_of_totalFileSize_2() { return static_cast<int32_t>(offsetof(DownloadEventArgs_tE8931C7F6C9F12BB6E657D9F11491CD22D94BC6A, ___totalFileSize_2)); }
	inline int64_t get_totalFileSize_2() const { return ___totalFileSize_2; }
	inline int64_t* get_address_of_totalFileSize_2() { return &___totalFileSize_2; }
	inline void set_totalFileSize_2(int64_t value)
	{
		___totalFileSize_2 = value;
	}

	inline static int32_t get_offset_of_currentFileSize_3() { return static_cast<int32_t>(offsetof(DownloadEventArgs_tE8931C7F6C9F12BB6E657D9F11491CD22D94BC6A, ___currentFileSize_3)); }
	inline int64_t get_currentFileSize_3() const { return ___currentFileSize_3; }
	inline int64_t* get_address_of_currentFileSize_3() { return &___currentFileSize_3; }
	inline void set_currentFileSize_3(int64_t value)
	{
		___currentFileSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADEVENTARGS_TE8931C7F6C9F12BB6E657D9F11491CD22D94BC6A_H
#ifndef ARGUMENTOBJECT_TDAA673E7623AFB02655105AA0B4752BBDBCBAF26_H
#define ARGUMENTOBJECT_TDAA673E7623AFB02655105AA0B4752BBDBCBAF26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.ArgumentObject
struct  ArgumentObject_tDAA673E7623AFB02655105AA0B4752BBDBCBAF26 
{
public:
	// System.Object Microsoft.CSharp.RuntimeBinder.ArgumentObject::Value
	RuntimeObject * ___Value_0;
	// Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo Microsoft.CSharp.RuntimeBinder.ArgumentObject::Info
	CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4 * ___Info_1;
	// System.Type Microsoft.CSharp.RuntimeBinder.ArgumentObject::Type
	Type_t * ___Type_2;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(ArgumentObject_tDAA673E7623AFB02655105AA0B4752BBDBCBAF26, ___Value_0)); }
	inline RuntimeObject * get_Value_0() const { return ___Value_0; }
	inline RuntimeObject ** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(RuntimeObject * value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((&___Value_0), value);
	}

	inline static int32_t get_offset_of_Info_1() { return static_cast<int32_t>(offsetof(ArgumentObject_tDAA673E7623AFB02655105AA0B4752BBDBCBAF26, ___Info_1)); }
	inline CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4 * get_Info_1() const { return ___Info_1; }
	inline CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4 ** get_address_of_Info_1() { return &___Info_1; }
	inline void set_Info_1(CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4 * value)
	{
		___Info_1 = value;
		Il2CppCodeGenWriteBarrier((&___Info_1), value);
	}

	inline static int32_t get_offset_of_Type_2() { return static_cast<int32_t>(offsetof(ArgumentObject_tDAA673E7623AFB02655105AA0B4752BBDBCBAF26, ___Type_2)); }
	inline Type_t * get_Type_2() const { return ___Type_2; }
	inline Type_t ** get_address_of_Type_2() { return &___Type_2; }
	inline void set_Type_2(Type_t * value)
	{
		___Type_2 = value;
		Il2CppCodeGenWriteBarrier((&___Type_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Microsoft.CSharp.RuntimeBinder.ArgumentObject
struct ArgumentObject_tDAA673E7623AFB02655105AA0B4752BBDBCBAF26_marshaled_pinvoke
{
	Il2CppIUnknown* ___Value_0;
	CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4 * ___Info_1;
	Type_t * ___Type_2;
};
// Native definition for COM marshalling of Microsoft.CSharp.RuntimeBinder.ArgumentObject
struct ArgumentObject_tDAA673E7623AFB02655105AA0B4752BBDBCBAF26_marshaled_com
{
	Il2CppIUnknown* ___Value_0;
	CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4 * ___Info_1;
	Type_t * ___Type_2;
};
#endif // ARGUMENTOBJECT_TDAA673E7623AFB02655105AA0B4752BBDBCBAF26_H
#ifndef EXPRESSIONTREECALLREWRITER_TC488183EA41BE7E437B93B86791CFA5779CE860B_H
#define EXPRESSIONTREECALLREWRITER_TC488183EA41BE7E437B93B86791CFA5779CE860B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.ExpressionTreeCallRewriter
struct  ExpressionTreeCallRewriter_tC488183EA41BE7E437B93B86791CFA5779CE860B  : public ExprVisitorBase_t266D2BBD3F35529D28D5570ADE95D3E4C8ED8104
{
public:
	// System.Collections.Generic.Dictionary`2<Microsoft.CSharp.RuntimeBinder.Semantics.ExprCall,System.Linq.Expressions.Expression> Microsoft.CSharp.RuntimeBinder.ExpressionTreeCallRewriter::_DictionaryOfParameters
	Dictionary_2_t092C659B9D5D00294F7E3BDFF7284CAA8847E08E * ____DictionaryOfParameters_0;
	// System.Linq.Expressions.Expression[] Microsoft.CSharp.RuntimeBinder.ExpressionTreeCallRewriter::_ListOfParameters
	ExpressionU5BU5D_t69D0BBE3C10949764DC20D5769D2469085774758* ____ListOfParameters_1;
	// Microsoft.CSharp.RuntimeBinder.Semantics.TypeManager Microsoft.CSharp.RuntimeBinder.ExpressionTreeCallRewriter::_typeManager
	TypeManager_t67F89DC00FB47A6EBA5060FEF0CFF62B74F073B6 * ____typeManager_2;
	// System.Int32 Microsoft.CSharp.RuntimeBinder.ExpressionTreeCallRewriter::_currentParameterIndex
	int32_t ____currentParameterIndex_3;

public:
	inline static int32_t get_offset_of__DictionaryOfParameters_0() { return static_cast<int32_t>(offsetof(ExpressionTreeCallRewriter_tC488183EA41BE7E437B93B86791CFA5779CE860B, ____DictionaryOfParameters_0)); }
	inline Dictionary_2_t092C659B9D5D00294F7E3BDFF7284CAA8847E08E * get__DictionaryOfParameters_0() const { return ____DictionaryOfParameters_0; }
	inline Dictionary_2_t092C659B9D5D00294F7E3BDFF7284CAA8847E08E ** get_address_of__DictionaryOfParameters_0() { return &____DictionaryOfParameters_0; }
	inline void set__DictionaryOfParameters_0(Dictionary_2_t092C659B9D5D00294F7E3BDFF7284CAA8847E08E * value)
	{
		____DictionaryOfParameters_0 = value;
		Il2CppCodeGenWriteBarrier((&____DictionaryOfParameters_0), value);
	}

	inline static int32_t get_offset_of__ListOfParameters_1() { return static_cast<int32_t>(offsetof(ExpressionTreeCallRewriter_tC488183EA41BE7E437B93B86791CFA5779CE860B, ____ListOfParameters_1)); }
	inline ExpressionU5BU5D_t69D0BBE3C10949764DC20D5769D2469085774758* get__ListOfParameters_1() const { return ____ListOfParameters_1; }
	inline ExpressionU5BU5D_t69D0BBE3C10949764DC20D5769D2469085774758** get_address_of__ListOfParameters_1() { return &____ListOfParameters_1; }
	inline void set__ListOfParameters_1(ExpressionU5BU5D_t69D0BBE3C10949764DC20D5769D2469085774758* value)
	{
		____ListOfParameters_1 = value;
		Il2CppCodeGenWriteBarrier((&____ListOfParameters_1), value);
	}

	inline static int32_t get_offset_of__typeManager_2() { return static_cast<int32_t>(offsetof(ExpressionTreeCallRewriter_tC488183EA41BE7E437B93B86791CFA5779CE860B, ____typeManager_2)); }
	inline TypeManager_t67F89DC00FB47A6EBA5060FEF0CFF62B74F073B6 * get__typeManager_2() const { return ____typeManager_2; }
	inline TypeManager_t67F89DC00FB47A6EBA5060FEF0CFF62B74F073B6 ** get_address_of__typeManager_2() { return &____typeManager_2; }
	inline void set__typeManager_2(TypeManager_t67F89DC00FB47A6EBA5060FEF0CFF62B74F073B6 * value)
	{
		____typeManager_2 = value;
		Il2CppCodeGenWriteBarrier((&____typeManager_2), value);
	}

	inline static int32_t get_offset_of__currentParameterIndex_3() { return static_cast<int32_t>(offsetof(ExpressionTreeCallRewriter_tC488183EA41BE7E437B93B86791CFA5779CE860B, ____currentParameterIndex_3)); }
	inline int32_t get__currentParameterIndex_3() const { return ____currentParameterIndex_3; }
	inline int32_t* get_address_of__currentParameterIndex_3() { return &____currentParameterIndex_3; }
	inline void set__currentParameterIndex_3(int32_t value)
	{
		____currentParameterIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONTREECALLREWRITER_TC488183EA41BE7E437B93B86791CFA5779CE860B_H
#ifndef RESETBINDEXCEPTION_TF9FA2AFE9CA681532453C81BC9684082C0DA3AE7_H
#define RESETBINDEXCEPTION_TF9FA2AFE9CA681532453C81BC9684082C0DA3AE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.ResetBindException
struct  ResetBindException_tF9FA2AFE9CA681532453C81BC9684082C0DA3AE7  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESETBINDEXCEPTION_TF9FA2AFE9CA681532453C81BC9684082C0DA3AE7_H
#ifndef RUNTIMEBINDEREXCEPTION_T7B5AC6644DD8D642283F515925BDB8345D20EB23_H
#define RUNTIMEBINDEREXCEPTION_T7B5AC6644DD8D642283F515925BDB8345D20EB23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.RuntimeBinderException
struct  RuntimeBinderException_t7B5AC6644DD8D642283F515925BDB8345D20EB23  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEBINDEREXCEPTION_T7B5AC6644DD8D642283F515925BDB8345D20EB23_H
#ifndef RUNTIMEBINDERINTERNALCOMPILEREXCEPTION_T88D0D461DDC2AE7EAAD21631BE58318F84ED1920_H
#define RUNTIMEBINDERINTERNALCOMPILEREXCEPTION_T88D0D461DDC2AE7EAAD21631BE58318F84ED1920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.RuntimeBinderInternalCompilerException
struct  RuntimeBinderInternalCompilerException_t88D0D461DDC2AE7EAAD21631BE58318F84ED1920  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEBINDERINTERNALCOMPILEREXCEPTION_T88D0D461DDC2AE7EAAD21631BE58318F84ED1920_H
#ifndef CONSTVAL_T4673513268B853CEFF9ACEA67E742D46A987DC26_H
#define CONSTVAL_T4673513268B853CEFF9ACEA67E742D46A987DC26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ConstVal
struct  ConstVal_t4673513268B853CEFF9ACEA67E742D46A987DC26 
{
public:
	// System.Object Microsoft.CSharp.RuntimeBinder.Semantics.ConstVal::<ObjectVal>k__BackingField
	RuntimeObject * ___U3CObjectValU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CObjectValU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ConstVal_t4673513268B853CEFF9ACEA67E742D46A987DC26, ___U3CObjectValU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CObjectValU3Ek__BackingField_3() const { return ___U3CObjectValU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CObjectValU3Ek__BackingField_3() { return &___U3CObjectValU3Ek__BackingField_3; }
	inline void set_U3CObjectValU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CObjectValU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectValU3Ek__BackingField_3), value);
	}
};

struct ConstVal_t4673513268B853CEFF9ACEA67E742D46A987DC26_StaticFields
{
public:
	// System.Object Microsoft.CSharp.RuntimeBinder.Semantics.ConstVal::s_false
	RuntimeObject * ___s_false_0;
	// System.Object Microsoft.CSharp.RuntimeBinder.Semantics.ConstVal::s_true
	RuntimeObject * ___s_true_1;
	// System.Object Microsoft.CSharp.RuntimeBinder.Semantics.ConstVal::s_zeroInt32
	RuntimeObject * ___s_zeroInt32_2;

public:
	inline static int32_t get_offset_of_s_false_0() { return static_cast<int32_t>(offsetof(ConstVal_t4673513268B853CEFF9ACEA67E742D46A987DC26_StaticFields, ___s_false_0)); }
	inline RuntimeObject * get_s_false_0() const { return ___s_false_0; }
	inline RuntimeObject ** get_address_of_s_false_0() { return &___s_false_0; }
	inline void set_s_false_0(RuntimeObject * value)
	{
		___s_false_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_false_0), value);
	}

	inline static int32_t get_offset_of_s_true_1() { return static_cast<int32_t>(offsetof(ConstVal_t4673513268B853CEFF9ACEA67E742D46A987DC26_StaticFields, ___s_true_1)); }
	inline RuntimeObject * get_s_true_1() const { return ___s_true_1; }
	inline RuntimeObject ** get_address_of_s_true_1() { return &___s_true_1; }
	inline void set_s_true_1(RuntimeObject * value)
	{
		___s_true_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_true_1), value);
	}

	inline static int32_t get_offset_of_s_zeroInt32_2() { return static_cast<int32_t>(offsetof(ConstVal_t4673513268B853CEFF9ACEA67E742D46A987DC26_StaticFields, ___s_zeroInt32_2)); }
	inline RuntimeObject * get_s_zeroInt32_2() const { return ___s_zeroInt32_2; }
	inline RuntimeObject ** get_address_of_s_zeroInt32_2() { return &___s_zeroInt32_2; }
	inline void set_s_zeroInt32_2(RuntimeObject * value)
	{
		___s_zeroInt32_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_zeroInt32_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Microsoft.CSharp.RuntimeBinder.Semantics.ConstVal
struct ConstVal_t4673513268B853CEFF9ACEA67E742D46A987DC26_marshaled_pinvoke
{
	Il2CppIUnknown* ___U3CObjectValU3Ek__BackingField_3;
};
// Native definition for COM marshalling of Microsoft.CSharp.RuntimeBinder.Semantics.ConstVal
struct ConstVal_t4673513268B853CEFF9ACEA67E742D46A987DC26_marshaled_com
{
	Il2CppIUnknown* ___U3CObjectValU3Ek__BackingField_3;
};
#endif // CONSTVAL_T4673513268B853CEFF9ACEA67E742D46A987DC26_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DYNAMICMETAOBJECTBINDER_TE2D934F8252D47EA3F731668D13EC80EF7FD06D4_H
#define DYNAMICMETAOBJECTBINDER_TE2D934F8252D47EA3F731668D13EC80EF7FD06D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.DynamicMetaObjectBinder
struct  DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4  : public CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICMETAOBJECTBINDER_TE2D934F8252D47EA3F731668D13EC80EF7FD06D4_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COMPRESSIONTYPE_TFD4585547C8DA57E6AEE2FCC7CA7BD0471795803_H
#define COMPRESSIONTYPE_TFD4585547C8DA57E6AEE2FCC7CA7BD0471795803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Compression.CompressionType
struct  CompressionType_tFD4585547C8DA57E6AEE2FCC7CA7BD0471795803 
{
public:
	// System.Int32 MHLab.PATCH.Compression.CompressionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompressionType_tFD4585547C8DA57E6AEE2FCC7CA7BD0471795803, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONTYPE_TFD4585547C8DA57E6AEE2FCC7CA7BD0471795803_H
#ifndef INSTALLATIONSTATE_TC3C4D9DDE2082A9B86E5C0BB46D1C637A08A2AD6_H
#define INSTALLATIONSTATE_TC3C4D9DDE2082A9B86E5C0BB46D1C637A08A2AD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Install.InstallationState
struct  InstallationState_tC3C4D9DDE2082A9B86E5C0BB46D1C637A08A2AD6 
{
public:
	// System.Int32 MHLab.PATCH.Install.InstallationState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InstallationState_tC3C4D9DDE2082A9B86E5C0BB46D1C637A08A2AD6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLATIONSTATE_TC3C4D9DDE2082A9B86E5C0BB46D1C637A08A2AD6_H
#ifndef CSHARPARGUMENTINFOFLAGS_T990CD7029BB110AE2C8A0AFB0EC825B2DCF5B0AC_H
#define CSHARPARGUMENTINFOFLAGS_T990CD7029BB110AE2C8A0AFB0EC825B2DCF5B0AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfoFlags
struct  CSharpArgumentInfoFlags_t990CD7029BB110AE2C8A0AFB0EC825B2DCF5B0AC 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfoFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CSharpArgumentInfoFlags_t990CD7029BB110AE2C8A0AFB0EC825B2DCF5B0AC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHARPARGUMENTINFOFLAGS_T990CD7029BB110AE2C8A0AFB0EC825B2DCF5B0AC_H
#ifndef CSHARPBINARYOPERATIONFLAGS_T143FAC518313AB8F52837CD00B32FC180FEC12A6_H
#define CSHARPBINARYOPERATIONFLAGS_T143FAC518313AB8F52837CD00B32FC180FEC12A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.CSharpBinaryOperationFlags
struct  CSharpBinaryOperationFlags_t143FAC518313AB8F52837CD00B32FC180FEC12A6 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.CSharpBinaryOperationFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CSharpBinaryOperationFlags_t143FAC518313AB8F52837CD00B32FC180FEC12A6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHARPBINARYOPERATIONFLAGS_T143FAC518313AB8F52837CD00B32FC180FEC12A6_H
#ifndef CSHARPBINDERFLAGS_TCF49AAFD82EC095A895ACF3D49A7BF3C7A74DFFE_H
#define CSHARPBINDERFLAGS_TCF49AAFD82EC095A895ACF3D49A7BF3C7A74DFFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.CSharpBinderFlags
struct  CSharpBinderFlags_tCF49AAFD82EC095A895ACF3D49A7BF3C7A74DFFE 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.CSharpBinderFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CSharpBinderFlags_tCF49AAFD82EC095A895ACF3D49A7BF3C7A74DFFE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHARPBINDERFLAGS_TCF49AAFD82EC095A895ACF3D49A7BF3C7A74DFFE_H
#ifndef CSHARPCALLFLAGS_T280317F9DD5386E7CFE60814060132BFB841892C_H
#define CSHARPCALLFLAGS_T280317F9DD5386E7CFE60814060132BFB841892C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.CSharpCallFlags
struct  CSharpCallFlags_t280317F9DD5386E7CFE60814060132BFB841892C 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.CSharpCallFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CSharpCallFlags_t280317F9DD5386E7CFE60814060132BFB841892C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHARPCALLFLAGS_T280317F9DD5386E7CFE60814060132BFB841892C_H
#ifndef CSHARPCONVERSIONKIND_T22B959F921CFC138419590A0A8D542F36CCF505F_H
#define CSHARPCONVERSIONKIND_T22B959F921CFC138419590A0A8D542F36CCF505F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.CSharpConversionKind
struct  CSharpConversionKind_t22B959F921CFC138419590A0A8D542F36CCF505F 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.CSharpConversionKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CSharpConversionKind_t22B959F921CFC138419590A0A8D542F36CCF505F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHARPCONVERSIONKIND_T22B959F921CFC138419590A0A8D542F36CCF505F_H
#ifndef ACCESS_T51C22C5AD74F98C8A36AB99DE1190FC966E6F0E9_H
#define ACCESS_T51C22C5AD74F98C8A36AB99DE1190FC966E6F0E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ACCESS
struct  ACCESS_t51C22C5AD74F98C8A36AB99DE1190FC966E6F0E9 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.ACCESS::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ACCESS_t51C22C5AD74F98C8A36AB99DE1190FC966E6F0E9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESS_T51C22C5AD74F98C8A36AB99DE1190FC966E6F0E9_H
#ifndef BETTERTYPE_T833A834305AE83107E92381CBBC19C4B0178D37E_H
#define BETTERTYPE_T833A834305AE83107E92381CBBC19C4B0178D37E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.BetterType
struct  BetterType_t833A834305AE83107E92381CBBC19C4B0178D37E 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.BetterType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BetterType_t833A834305AE83107E92381CBBC19C4B0178D37E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BETTERTYPE_T833A834305AE83107E92381CBBC19C4B0178D37E_H
#ifndef BINOPFUNCKIND_T8A5A245559619442C361D9EFBA8C5B5A97E55AE6_H
#define BINOPFUNCKIND_T8A5A245559619442C361D9EFBA8C5B5A97E55AE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.BinOpFuncKind
struct  BinOpFuncKind_t8A5A245559619442C361D9EFBA8C5B5A97E55AE6 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.BinOpFuncKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BinOpFuncKind_t8A5A245559619442C361D9EFBA8C5B5A97E55AE6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINOPFUNCKIND_T8A5A245559619442C361D9EFBA8C5B5A97E55AE6_H
#ifndef BINOPKIND_T20BC3362D5F1A32CA225C1E71AAEF5A429D46AA0_H
#define BINOPKIND_T20BC3362D5F1A32CA225C1E71AAEF5A429D46AA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.BinOpKind
struct  BinOpKind_t20BC3362D5F1A32CA225C1E71AAEF5A429D46AA0 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.BinOpKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BinOpKind_t20BC3362D5F1A32CA225C1E71AAEF5A429D46AA0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINOPKIND_T20BC3362D5F1A32CA225C1E71AAEF5A429D46AA0_H
#ifndef BINOPMASK_T109AB0EC300AFA64332719FA88881BF49AFBBE97_H
#define BINOPMASK_T109AB0EC300AFA64332719FA88881BF49AFBBE97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.BinOpMask
struct  BinOpMask_t109AB0EC300AFA64332719FA88881BF49AFBBE97 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.BinOpMask::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BinOpMask_t109AB0EC300AFA64332719FA88881BF49AFBBE97, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINOPMASK_T109AB0EC300AFA64332719FA88881BF49AFBBE97_H
#ifndef BINDINGFLAG_T57D354B0FF6B1A0C2F4724ABD8043326D3481B74_H
#define BINDINGFLAG_T57D354B0FF6B1A0C2F4724ABD8043326D3481B74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.BindingFlag
struct  BindingFlag_t57D354B0FF6B1A0C2F4724ABD8043326D3481B74 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.BindingFlag::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlag_t57D354B0FF6B1A0C2F4724ABD8043326D3481B74, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAG_T57D354B0FF6B1A0C2F4724ABD8043326D3481B74_H
#ifndef CONVERTTYPE_TB9C5AC4C022C175CB67276214C91686872A8B0F4_H
#define CONVERTTYPE_TB9C5AC4C022C175CB67276214C91686872A8B0F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.CONVERTTYPE
struct  CONVERTTYPE_tB9C5AC4C022C175CB67276214C91686872A8B0F4 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.CONVERTTYPE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CONVERTTYPE_tB9C5AC4C022C175CB67276214C91686872A8B0F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTTYPE_TB9C5AC4C022C175CB67276214C91686872A8B0F4_H
#ifndef CONSTVALKIND_T23507BC78F5717D51436F99F8F138D54F055D690_H
#define CONSTVALKIND_T23507BC78F5717D51436F99F8F138D54F055D690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ConstValKind
struct  ConstValKind_t23507BC78F5717D51436F99F8F138D54F055D690 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.ConstValKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConstValKind_t23507BC78F5717D51436F99F8F138D54F055D690, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTVALKIND_T23507BC78F5717D51436F99F8F138D54F055D690_H
#ifndef CONVKIND_TDCD6872B2A128FD598D8A121CA47A350877B93F7_H
#define CONVKIND_TDCD6872B2A128FD598D8A121CA47A350877B93F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ConvKind
struct  ConvKind_tDCD6872B2A128FD598D8A121CA47A350877B93F7 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.ConvKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConvKind_tDCD6872B2A128FD598D8A121CA47A350877B93F7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVKIND_TDCD6872B2A128FD598D8A121CA47A350877B93F7_H
#ifndef EXPRFLAG_TFB55C8C8B4BEA8EDC0C4BA623385FB4A12FA433A_H
#define EXPRFLAG_TFB55C8C8B4BEA8EDC0C4BA623385FB4A12FA433A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.EXPRFLAG
struct  EXPRFLAG_tFB55C8C8B4BEA8EDC0C4BA623385FB4A12FA433A 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.EXPRFLAG::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EXPRFLAG_tFB55C8C8B4BEA8EDC0C4BA623385FB4A12FA433A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRFLAG_TFB55C8C8B4BEA8EDC0C4BA623385FB4A12FA433A_H
#ifndef RESULT_T9909E31E427DF6AF8FA28481E5ADF1BCC2A5DA90_H
#define RESULT_T9909E31E427DF6AF8FA28481E5ADF1BCC2A5DA90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder/Result
struct  Result_t9909E31E427DF6AF8FA28481E5ADF1BCC2A5DA90 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder/Result::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Result_t9909E31E427DF6AF8FA28481E5ADF1BCC2A5DA90, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULT_T9909E31E427DF6AF8FA28481E5ADF1BCC2A5DA90_H
#ifndef EXPRESSIONKIND_T89BF854FBC03BDCD1591963720769AAF438537AB_H
#define EXPRESSIONKIND_T89BF854FBC03BDCD1591963720769AAF438537AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionKind
struct  ExpressionKind_t89BF854FBC03BDCD1591963720769AAF438537AB 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExpressionKind_t89BF854FBC03BDCD1591963720769AAF438537AB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONKIND_T89BF854FBC03BDCD1591963720769AAF438537AB_H
#ifndef LIFTFLAGS_T327A4151A5026C0AC110B9EC40CD66354577DA92_H
#define LIFTFLAGS_T327A4151A5026C0AC110B9EC40CD66354577DA92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.LiftFlags
struct  LiftFlags_t327A4151A5026C0AC110B9EC40CD66354577DA92 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.LiftFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LiftFlags_t327A4151A5026C0AC110B9EC40CD66354577DA92, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIFTFLAGS_T327A4151A5026C0AC110B9EC40CD66354577DA92_H
#ifndef OPSIGFLAGS_T4CD235FED3B3F4B3CE7540D1A9E07E2052879446_H
#define OPSIGFLAGS_T4CD235FED3B3F4B3CE7540D1A9E07E2052879446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.OpSigFlags
struct  OpSigFlags_t4CD235FED3B3F4B3CE7540D1A9E07E2052879446 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.OpSigFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OpSigFlags_t4CD235FED3B3F4B3CE7540D1A9E07E2052879446, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPSIGFLAGS_T4CD235FED3B3F4B3CE7540D1A9E07E2052879446_H
#ifndef SYMKIND_T75FE33E844E8E99D71FEFD8BBBC3A65210704A8A_H
#define SYMKIND_T75FE33E844E8E99D71FEFD8BBBC3A65210704A8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.SYMKIND
struct  SYMKIND_t75FE33E844E8E99D71FEFD8BBBC3A65210704A8A 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.SYMKIND::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SYMKIND_t75FE33E844E8E99D71FEFD8BBBC3A65210704A8A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMKIND_T75FE33E844E8E99D71FEFD8BBBC3A65210704A8A_H
#ifndef UNAOPFUNCKIND_T2AF8A053B80B014870EAAD869CCA651338EFAA55_H
#define UNAOPFUNCKIND_T2AF8A053B80B014870EAAD869CCA651338EFAA55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.UnaOpFuncKind
struct  UnaOpFuncKind_t2AF8A053B80B014870EAAD869CCA651338EFAA55 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.UnaOpFuncKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnaOpFuncKind_t2AF8A053B80B014870EAAD869CCA651338EFAA55, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNAOPFUNCKIND_T2AF8A053B80B014870EAAD869CCA651338EFAA55_H
#ifndef UNAOPMASK_T7D146F9E6A5092E3BDB779DB7CA126121268DA66_H
#define UNAOPMASK_T7D146F9E6A5092E3BDB779DB7CA126121268DA66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.UnaOpMask
struct  UnaOpMask_t7D146F9E6A5092E3BDB779DB7CA126121268DA66 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.UnaOpMask::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnaOpMask_t7D146F9E6A5092E3BDB779DB7CA126121268DA66, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNAOPMASK_T7D146F9E6A5092E3BDB779DB7CA126121268DA66_H
#ifndef OPERATORKIND_TDB430CC83D1E2EA1E099D0386F2BA44479EBF441_H
#define OPERATORKIND_TDB430CC83D1E2EA1E099D0386F2BA44479EBF441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Syntax.OperatorKind
struct  OperatorKind_tDB430CC83D1E2EA1E099D0386F2BA44479EBF441 
{
public:
	// System.UInt32 Microsoft.CSharp.RuntimeBinder.Syntax.OperatorKind::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OperatorKind_tDB430CC83D1E2EA1E099D0386F2BA44479EBF441, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATORKIND_TDB430CC83D1E2EA1E099D0386F2BA44479EBF441_H
#ifndef PREDEFINEDNAME_T8BC14E853C03DB817D27F0CBCEF4A494D98E5251_H
#define PREDEFINEDNAME_T8BC14E853C03DB817D27F0CBCEF4A494D98E5251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedName
struct  PredefinedName_t8BC14E853C03DB817D27F0CBCEF4A494D98E5251 
{
public:
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedName::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PredefinedName_t8BC14E853C03DB817D27F0CBCEF4A494D98E5251, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDEFINEDNAME_T8BC14E853C03DB817D27F0CBCEF4A494D98E5251_H
#ifndef PREDEFINEDTYPE_TA9E6BFC4BDEB7BB4FA3F04CCE1E61E83E686DDC6_H
#define PREDEFINEDTYPE_TA9E6BFC4BDEB7BB4FA3F04CCE1E61E83E686DDC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedType
struct  PredefinedType_tA9E6BFC4BDEB7BB4FA3F04CCE1E61E83E686DDC6 
{
public:
	// System.UInt32 Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedType::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PredefinedType_tA9E6BFC4BDEB7BB4FA3F04CCE1E61E83E686DDC6, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDEFINEDTYPE_TA9E6BFC4BDEB7BB4FA3F04CCE1E61E83E686DDC6_H
#ifndef TOKENKIND_TE22E2BD7A8F2A71654B450266A78159FBE559753_H
#define TOKENKIND_TE22E2BD7A8F2A71654B450266A78159FBE559753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Syntax.TokenKind
struct  TokenKind_tE22E2BD7A8F2A71654B450266A78159FBE559753 
{
public:
	// System.Byte Microsoft.CSharp.RuntimeBinder.Syntax.TokenKind::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TokenKind_tE22E2BD7A8F2A71654B450266A78159FBE559753, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENKIND_TE22E2BD7A8F2A71654B450266A78159FBE559753_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef CONVERTBINDER_T6823370A867A471BA9B6BEA72AEE8C2B55C04A5D_H
#define CONVERTBINDER_T6823370A867A471BA9B6BEA72AEE8C2B55C04A5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ConvertBinder
struct  ConvertBinder_t6823370A867A471BA9B6BEA72AEE8C2B55C04A5D  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// System.Type System.Dynamic.ConvertBinder::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_2;
	// System.Boolean System.Dynamic.ConvertBinder::<Explicit>k__BackingField
	bool ___U3CExplicitU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConvertBinder_t6823370A867A471BA9B6BEA72AEE8C2B55C04A5D, ___U3CTypeU3Ek__BackingField_2)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CExplicitU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ConvertBinder_t6823370A867A471BA9B6BEA72AEE8C2B55C04A5D, ___U3CExplicitU3Ek__BackingField_3)); }
	inline bool get_U3CExplicitU3Ek__BackingField_3() const { return ___U3CExplicitU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CExplicitU3Ek__BackingField_3() { return &___U3CExplicitU3Ek__BackingField_3; }
	inline void set_U3CExplicitU3Ek__BackingField_3(bool value)
	{
		___U3CExplicitU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTBINDER_T6823370A867A471BA9B6BEA72AEE8C2B55C04A5D_H
#ifndef GETMEMBERBINDER_T75F1D27E6CCA45C4EA27B045B5A95E31E67325F4_H
#define GETMEMBERBINDER_T75F1D27E6CCA45C4EA27B045B5A95E31E67325F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.GetMemberBinder
struct  GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// System.String System.Dynamic.GetMemberBinder::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_2;
	// System.Boolean System.Dynamic.GetMemberBinder::<IgnoreCase>k__BackingField
	bool ___U3CIgnoreCaseU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4, ___U3CNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CNameU3Ek__BackingField_2() const { return ___U3CNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_2() { return &___U3CNameU3Ek__BackingField_2; }
	inline void set_U3CNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CIgnoreCaseU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4, ___U3CIgnoreCaseU3Ek__BackingField_3)); }
	inline bool get_U3CIgnoreCaseU3Ek__BackingField_3() const { return ___U3CIgnoreCaseU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIgnoreCaseU3Ek__BackingField_3() { return &___U3CIgnoreCaseU3Ek__BackingField_3; }
	inline void set_U3CIgnoreCaseU3Ek__BackingField_3(bool value)
	{
		___U3CIgnoreCaseU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMEMBERBINDER_T75F1D27E6CCA45C4EA27B045B5A95E31E67325F4_H
#ifndef INVOKEBINDER_T741FBA7DA7200031CB8B832A436B974836762B3B_H
#define INVOKEBINDER_T741FBA7DA7200031CB8B832A436B974836762B3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.InvokeBinder
struct  InvokeBinder_t741FBA7DA7200031CB8B832A436B974836762B3B  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// System.Dynamic.CallInfo System.Dynamic.InvokeBinder::<CallInfo>k__BackingField
	CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935 * ___U3CCallInfoU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCallInfoU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InvokeBinder_t741FBA7DA7200031CB8B832A436B974836762B3B, ___U3CCallInfoU3Ek__BackingField_2)); }
	inline CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935 * get_U3CCallInfoU3Ek__BackingField_2() const { return ___U3CCallInfoU3Ek__BackingField_2; }
	inline CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935 ** get_address_of_U3CCallInfoU3Ek__BackingField_2() { return &___U3CCallInfoU3Ek__BackingField_2; }
	inline void set_U3CCallInfoU3Ek__BackingField_2(CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935 * value)
	{
		___U3CCallInfoU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallInfoU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKEBINDER_T741FBA7DA7200031CB8B832A436B974836762B3B_H
#ifndef INVOKEMEMBERBINDER_T164DF75DC7993FD1767279610E7E1DB3D498BF3F_H
#define INVOKEMEMBERBINDER_T164DF75DC7993FD1767279610E7E1DB3D498BF3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.InvokeMemberBinder
struct  InvokeMemberBinder_t164DF75DC7993FD1767279610E7E1DB3D498BF3F  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// System.String System.Dynamic.InvokeMemberBinder::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_2;
	// System.Boolean System.Dynamic.InvokeMemberBinder::<IgnoreCase>k__BackingField
	bool ___U3CIgnoreCaseU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InvokeMemberBinder_t164DF75DC7993FD1767279610E7E1DB3D498BF3F, ___U3CNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CNameU3Ek__BackingField_2() const { return ___U3CNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_2() { return &___U3CNameU3Ek__BackingField_2; }
	inline void set_U3CNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CIgnoreCaseU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(InvokeMemberBinder_t164DF75DC7993FD1767279610E7E1DB3D498BF3F, ___U3CIgnoreCaseU3Ek__BackingField_3)); }
	inline bool get_U3CIgnoreCaseU3Ek__BackingField_3() const { return ___U3CIgnoreCaseU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIgnoreCaseU3Ek__BackingField_3() { return &___U3CIgnoreCaseU3Ek__BackingField_3; }
	inline void set_U3CIgnoreCaseU3Ek__BackingField_3(bool value)
	{
		___U3CIgnoreCaseU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKEMEMBERBINDER_T164DF75DC7993FD1767279610E7E1DB3D498BF3F_H
#ifndef EXPRESSIONTYPE_TA0C19CAF997A8592C84F0EBFE791BC19BF5AC556_H
#define EXPRESSIONTYPE_TA0C19CAF997A8592C84F0EBFE791BC19BF5AC556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ExpressionType
struct  ExpressionType_tA0C19CAF997A8592C84F0EBFE791BC19BF5AC556 
{
public:
	// System.Int32 System.Linq.Expressions.ExpressionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExpressionType_tA0C19CAF997A8592C84F0EBFE791BC19BF5AC556, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONTYPE_TA0C19CAF997A8592C84F0EBFE791BC19BF5AC556_H
#ifndef CSHARPARGUMENTINFO_TDAC8F8D0657968A42B97C5885D10D7635206CDE4_H
#define CSHARPARGUMENTINFO_TDAC8F8D0657968A42B97C5885D10D7635206CDE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo
struct  CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfoFlags Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo::<Flags>k__BackingField
	int32_t ___U3CFlagsU3Ek__BackingField_1;
	// System.String Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CFlagsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4, ___U3CFlagsU3Ek__BackingField_1)); }
	inline int32_t get_U3CFlagsU3Ek__BackingField_1() const { return ___U3CFlagsU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CFlagsU3Ek__BackingField_1() { return &___U3CFlagsU3Ek__BackingField_1; }
	inline void set_U3CFlagsU3Ek__BackingField_1(int32_t value)
	{
		___U3CFlagsU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4, ___U3CNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CNameU3Ek__BackingField_2() const { return ___U3CNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_2() { return &___U3CNameU3Ek__BackingField_2; }
	inline void set_U3CNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_2), value);
	}
};

struct CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4_StaticFields
{
public:
	// Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo::None
	CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4 * ___None_0;

public:
	inline static int32_t get_offset_of_None_0() { return static_cast<int32_t>(offsetof(CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4_StaticFields, ___None_0)); }
	inline CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4 * get_None_0() const { return ___None_0; }
	inline CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4 ** get_address_of_None_0() { return &___None_0; }
	inline void set_None_0(CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4 * value)
	{
		___None_0 = value;
		Il2CppCodeGenWriteBarrier((&___None_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHARPARGUMENTINFO_TDAC8F8D0657968A42B97C5885D10D7635206CDE4_H
#ifndef CSHARPCONVERTBINDER_T71E47849913F1A47C23CDF45508A5138018AC321_H
#define CSHARPCONVERTBINDER_T71E47849913F1A47C23CDF45508A5138018AC321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.CSharpConvertBinder
struct  CSharpConvertBinder_t71E47849913F1A47C23CDF45508A5138018AC321  : public ConvertBinder_t6823370A867A471BA9B6BEA72AEE8C2B55C04A5D
{
public:
	// Microsoft.CSharp.RuntimeBinder.CSharpConversionKind Microsoft.CSharp.RuntimeBinder.CSharpConvertBinder::<ConversionKind>k__BackingField
	int32_t ___U3CConversionKindU3Ek__BackingField_4;
	// System.Boolean Microsoft.CSharp.RuntimeBinder.CSharpConvertBinder::<IsChecked>k__BackingField
	bool ___U3CIsCheckedU3Ek__BackingField_5;
	// System.Type Microsoft.CSharp.RuntimeBinder.CSharpConvertBinder::<CallingContext>k__BackingField
	Type_t * ___U3CCallingContextU3Ek__BackingField_6;
	// Microsoft.CSharp.RuntimeBinder.RuntimeBinder Microsoft.CSharp.RuntimeBinder.CSharpConvertBinder::_binder
	RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * ____binder_7;

public:
	inline static int32_t get_offset_of_U3CConversionKindU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CSharpConvertBinder_t71E47849913F1A47C23CDF45508A5138018AC321, ___U3CConversionKindU3Ek__BackingField_4)); }
	inline int32_t get_U3CConversionKindU3Ek__BackingField_4() const { return ___U3CConversionKindU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CConversionKindU3Ek__BackingField_4() { return &___U3CConversionKindU3Ek__BackingField_4; }
	inline void set_U3CConversionKindU3Ek__BackingField_4(int32_t value)
	{
		___U3CConversionKindU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIsCheckedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CSharpConvertBinder_t71E47849913F1A47C23CDF45508A5138018AC321, ___U3CIsCheckedU3Ek__BackingField_5)); }
	inline bool get_U3CIsCheckedU3Ek__BackingField_5() const { return ___U3CIsCheckedU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsCheckedU3Ek__BackingField_5() { return &___U3CIsCheckedU3Ek__BackingField_5; }
	inline void set_U3CIsCheckedU3Ek__BackingField_5(bool value)
	{
		___U3CIsCheckedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CCallingContextU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CSharpConvertBinder_t71E47849913F1A47C23CDF45508A5138018AC321, ___U3CCallingContextU3Ek__BackingField_6)); }
	inline Type_t * get_U3CCallingContextU3Ek__BackingField_6() const { return ___U3CCallingContextU3Ek__BackingField_6; }
	inline Type_t ** get_address_of_U3CCallingContextU3Ek__BackingField_6() { return &___U3CCallingContextU3Ek__BackingField_6; }
	inline void set_U3CCallingContextU3Ek__BackingField_6(Type_t * value)
	{
		___U3CCallingContextU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallingContextU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of__binder_7() { return static_cast<int32_t>(offsetof(CSharpConvertBinder_t71E47849913F1A47C23CDF45508A5138018AC321, ____binder_7)); }
	inline RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * get__binder_7() const { return ____binder_7; }
	inline RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 ** get_address_of__binder_7() { return &____binder_7; }
	inline void set__binder_7(RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * value)
	{
		____binder_7 = value;
		Il2CppCodeGenWriteBarrier((&____binder_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHARPCONVERTBINDER_T71E47849913F1A47C23CDF45508A5138018AC321_H
#ifndef CSHARPGETMEMBERBINDER_T1C65C80D44E71EFF3DE84423690C4FEFBC34A15E_H
#define CSHARPGETMEMBERBINDER_T1C65C80D44E71EFF3DE84423690C4FEFBC34A15E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.CSharpGetMemberBinder
struct  CSharpGetMemberBinder_t1C65C80D44E71EFF3DE84423690C4FEFBC34A15E  : public GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4
{
public:
	// System.Type Microsoft.CSharp.RuntimeBinder.CSharpGetMemberBinder::<CallingContext>k__BackingField
	Type_t * ___U3CCallingContextU3Ek__BackingField_4;
	// Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo[] Microsoft.CSharp.RuntimeBinder.CSharpGetMemberBinder::_argumentInfo
	CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* ____argumentInfo_5;
	// System.Boolean Microsoft.CSharp.RuntimeBinder.CSharpGetMemberBinder::<ResultIndexed>k__BackingField
	bool ___U3CResultIndexedU3Ek__BackingField_6;
	// Microsoft.CSharp.RuntimeBinder.RuntimeBinder Microsoft.CSharp.RuntimeBinder.CSharpGetMemberBinder::_binder
	RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * ____binder_7;

public:
	inline static int32_t get_offset_of_U3CCallingContextU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CSharpGetMemberBinder_t1C65C80D44E71EFF3DE84423690C4FEFBC34A15E, ___U3CCallingContextU3Ek__BackingField_4)); }
	inline Type_t * get_U3CCallingContextU3Ek__BackingField_4() const { return ___U3CCallingContextU3Ek__BackingField_4; }
	inline Type_t ** get_address_of_U3CCallingContextU3Ek__BackingField_4() { return &___U3CCallingContextU3Ek__BackingField_4; }
	inline void set_U3CCallingContextU3Ek__BackingField_4(Type_t * value)
	{
		___U3CCallingContextU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallingContextU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of__argumentInfo_5() { return static_cast<int32_t>(offsetof(CSharpGetMemberBinder_t1C65C80D44E71EFF3DE84423690C4FEFBC34A15E, ____argumentInfo_5)); }
	inline CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* get__argumentInfo_5() const { return ____argumentInfo_5; }
	inline CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103** get_address_of__argumentInfo_5() { return &____argumentInfo_5; }
	inline void set__argumentInfo_5(CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* value)
	{
		____argumentInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&____argumentInfo_5), value);
	}

	inline static int32_t get_offset_of_U3CResultIndexedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CSharpGetMemberBinder_t1C65C80D44E71EFF3DE84423690C4FEFBC34A15E, ___U3CResultIndexedU3Ek__BackingField_6)); }
	inline bool get_U3CResultIndexedU3Ek__BackingField_6() const { return ___U3CResultIndexedU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CResultIndexedU3Ek__BackingField_6() { return &___U3CResultIndexedU3Ek__BackingField_6; }
	inline void set_U3CResultIndexedU3Ek__BackingField_6(bool value)
	{
		___U3CResultIndexedU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__binder_7() { return static_cast<int32_t>(offsetof(CSharpGetMemberBinder_t1C65C80D44E71EFF3DE84423690C4FEFBC34A15E, ____binder_7)); }
	inline RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * get__binder_7() const { return ____binder_7; }
	inline RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 ** get_address_of__binder_7() { return &____binder_7; }
	inline void set__binder_7(RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * value)
	{
		____binder_7 = value;
		Il2CppCodeGenWriteBarrier((&____binder_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHARPGETMEMBERBINDER_T1C65C80D44E71EFF3DE84423690C4FEFBC34A15E_H
#ifndef CSHARPINVOKEBINDER_TC94A443E3999EF779EF0BC4BCBB85BDF8B6B96CC_H
#define CSHARPINVOKEBINDER_TC94A443E3999EF779EF0BC4BCBB85BDF8B6B96CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.CSharpInvokeBinder
struct  CSharpInvokeBinder_tC94A443E3999EF779EF0BC4BCBB85BDF8B6B96CC  : public InvokeBinder_t741FBA7DA7200031CB8B832A436B974836762B3B
{
public:
	// Microsoft.CSharp.RuntimeBinder.CSharpCallFlags Microsoft.CSharp.RuntimeBinder.CSharpInvokeBinder::_flags
	int32_t ____flags_3;
	// System.Type Microsoft.CSharp.RuntimeBinder.CSharpInvokeBinder::<CallingContext>k__BackingField
	Type_t * ___U3CCallingContextU3Ek__BackingField_4;
	// Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo[] Microsoft.CSharp.RuntimeBinder.CSharpInvokeBinder::_argumentInfo
	CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* ____argumentInfo_5;
	// Microsoft.CSharp.RuntimeBinder.RuntimeBinder Microsoft.CSharp.RuntimeBinder.CSharpInvokeBinder::_binder
	RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * ____binder_6;

public:
	inline static int32_t get_offset_of__flags_3() { return static_cast<int32_t>(offsetof(CSharpInvokeBinder_tC94A443E3999EF779EF0BC4BCBB85BDF8B6B96CC, ____flags_3)); }
	inline int32_t get__flags_3() const { return ____flags_3; }
	inline int32_t* get_address_of__flags_3() { return &____flags_3; }
	inline void set__flags_3(int32_t value)
	{
		____flags_3 = value;
	}

	inline static int32_t get_offset_of_U3CCallingContextU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CSharpInvokeBinder_tC94A443E3999EF779EF0BC4BCBB85BDF8B6B96CC, ___U3CCallingContextU3Ek__BackingField_4)); }
	inline Type_t * get_U3CCallingContextU3Ek__BackingField_4() const { return ___U3CCallingContextU3Ek__BackingField_4; }
	inline Type_t ** get_address_of_U3CCallingContextU3Ek__BackingField_4() { return &___U3CCallingContextU3Ek__BackingField_4; }
	inline void set_U3CCallingContextU3Ek__BackingField_4(Type_t * value)
	{
		___U3CCallingContextU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallingContextU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of__argumentInfo_5() { return static_cast<int32_t>(offsetof(CSharpInvokeBinder_tC94A443E3999EF779EF0BC4BCBB85BDF8B6B96CC, ____argumentInfo_5)); }
	inline CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* get__argumentInfo_5() const { return ____argumentInfo_5; }
	inline CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103** get_address_of__argumentInfo_5() { return &____argumentInfo_5; }
	inline void set__argumentInfo_5(CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* value)
	{
		____argumentInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&____argumentInfo_5), value);
	}

	inline static int32_t get_offset_of__binder_6() { return static_cast<int32_t>(offsetof(CSharpInvokeBinder_tC94A443E3999EF779EF0BC4BCBB85BDF8B6B96CC, ____binder_6)); }
	inline RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * get__binder_6() const { return ____binder_6; }
	inline RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 ** get_address_of__binder_6() { return &____binder_6; }
	inline void set__binder_6(RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * value)
	{
		____binder_6 = value;
		Il2CppCodeGenWriteBarrier((&____binder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHARPINVOKEBINDER_TC94A443E3999EF779EF0BC4BCBB85BDF8B6B96CC_H
#ifndef CSHARPINVOKECONSTRUCTORBINDER_TDA87B0CBCDF65D256B38B756C102CD667D6C6AC1_H
#define CSHARPINVOKECONSTRUCTORBINDER_TDA87B0CBCDF65D256B38B756C102CD667D6C6AC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.CSharpInvokeConstructorBinder
struct  CSharpInvokeConstructorBinder_tDA87B0CBCDF65D256B38B756C102CD667D6C6AC1  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// Microsoft.CSharp.RuntimeBinder.CSharpCallFlags Microsoft.CSharp.RuntimeBinder.CSharpInvokeConstructorBinder::<Flags>k__BackingField
	int32_t ___U3CFlagsU3Ek__BackingField_2;
	// System.Type Microsoft.CSharp.RuntimeBinder.CSharpInvokeConstructorBinder::<CallingContext>k__BackingField
	Type_t * ___U3CCallingContextU3Ek__BackingField_3;
	// Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo[] Microsoft.CSharp.RuntimeBinder.CSharpInvokeConstructorBinder::_argumentInfo
	CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* ____argumentInfo_4;
	// Microsoft.CSharp.RuntimeBinder.RuntimeBinder Microsoft.CSharp.RuntimeBinder.CSharpInvokeConstructorBinder::_binder
	RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * ____binder_5;

public:
	inline static int32_t get_offset_of_U3CFlagsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CSharpInvokeConstructorBinder_tDA87B0CBCDF65D256B38B756C102CD667D6C6AC1, ___U3CFlagsU3Ek__BackingField_2)); }
	inline int32_t get_U3CFlagsU3Ek__BackingField_2() const { return ___U3CFlagsU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CFlagsU3Ek__BackingField_2() { return &___U3CFlagsU3Ek__BackingField_2; }
	inline void set_U3CFlagsU3Ek__BackingField_2(int32_t value)
	{
		___U3CFlagsU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CCallingContextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CSharpInvokeConstructorBinder_tDA87B0CBCDF65D256B38B756C102CD667D6C6AC1, ___U3CCallingContextU3Ek__BackingField_3)); }
	inline Type_t * get_U3CCallingContextU3Ek__BackingField_3() const { return ___U3CCallingContextU3Ek__BackingField_3; }
	inline Type_t ** get_address_of_U3CCallingContextU3Ek__BackingField_3() { return &___U3CCallingContextU3Ek__BackingField_3; }
	inline void set_U3CCallingContextU3Ek__BackingField_3(Type_t * value)
	{
		___U3CCallingContextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallingContextU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of__argumentInfo_4() { return static_cast<int32_t>(offsetof(CSharpInvokeConstructorBinder_tDA87B0CBCDF65D256B38B756C102CD667D6C6AC1, ____argumentInfo_4)); }
	inline CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* get__argumentInfo_4() const { return ____argumentInfo_4; }
	inline CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103** get_address_of__argumentInfo_4() { return &____argumentInfo_4; }
	inline void set__argumentInfo_4(CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* value)
	{
		____argumentInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&____argumentInfo_4), value);
	}

	inline static int32_t get_offset_of__binder_5() { return static_cast<int32_t>(offsetof(CSharpInvokeConstructorBinder_tDA87B0CBCDF65D256B38B756C102CD667D6C6AC1, ____binder_5)); }
	inline RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * get__binder_5() const { return ____binder_5; }
	inline RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 ** get_address_of__binder_5() { return &____binder_5; }
	inline void set__binder_5(RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * value)
	{
		____binder_5 = value;
		Il2CppCodeGenWriteBarrier((&____binder_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHARPINVOKECONSTRUCTORBINDER_TDA87B0CBCDF65D256B38B756C102CD667D6C6AC1_H
#ifndef CSHARPINVOKEMEMBERBINDER_T1B43657AF71FD88DF2707CE5FB6B9FC9534ED244_H
#define CSHARPINVOKEMEMBERBINDER_T1B43657AF71FD88DF2707CE5FB6B9FC9534ED244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.CSharpInvokeMemberBinder
struct  CSharpInvokeMemberBinder_t1B43657AF71FD88DF2707CE5FB6B9FC9534ED244  : public InvokeMemberBinder_t164DF75DC7993FD1767279610E7E1DB3D498BF3F
{
public:
	// Microsoft.CSharp.RuntimeBinder.CSharpCallFlags Microsoft.CSharp.RuntimeBinder.CSharpInvokeMemberBinder::<Flags>k__BackingField
	int32_t ___U3CFlagsU3Ek__BackingField_4;
	// System.Type Microsoft.CSharp.RuntimeBinder.CSharpInvokeMemberBinder::<CallingContext>k__BackingField
	Type_t * ___U3CCallingContextU3Ek__BackingField_5;
	// System.Type[] Microsoft.CSharp.RuntimeBinder.CSharpInvokeMemberBinder::<TypeArguments>k__BackingField
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___U3CTypeArgumentsU3Ek__BackingField_6;
	// Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo[] Microsoft.CSharp.RuntimeBinder.CSharpInvokeMemberBinder::_argumentInfo
	CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* ____argumentInfo_7;
	// Microsoft.CSharp.RuntimeBinder.RuntimeBinder Microsoft.CSharp.RuntimeBinder.CSharpInvokeMemberBinder::_binder
	RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * ____binder_8;

public:
	inline static int32_t get_offset_of_U3CFlagsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CSharpInvokeMemberBinder_t1B43657AF71FD88DF2707CE5FB6B9FC9534ED244, ___U3CFlagsU3Ek__BackingField_4)); }
	inline int32_t get_U3CFlagsU3Ek__BackingField_4() const { return ___U3CFlagsU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CFlagsU3Ek__BackingField_4() { return &___U3CFlagsU3Ek__BackingField_4; }
	inline void set_U3CFlagsU3Ek__BackingField_4(int32_t value)
	{
		___U3CFlagsU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CCallingContextU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CSharpInvokeMemberBinder_t1B43657AF71FD88DF2707CE5FB6B9FC9534ED244, ___U3CCallingContextU3Ek__BackingField_5)); }
	inline Type_t * get_U3CCallingContextU3Ek__BackingField_5() const { return ___U3CCallingContextU3Ek__BackingField_5; }
	inline Type_t ** get_address_of_U3CCallingContextU3Ek__BackingField_5() { return &___U3CCallingContextU3Ek__BackingField_5; }
	inline void set_U3CCallingContextU3Ek__BackingField_5(Type_t * value)
	{
		___U3CCallingContextU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallingContextU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CTypeArgumentsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CSharpInvokeMemberBinder_t1B43657AF71FD88DF2707CE5FB6B9FC9534ED244, ___U3CTypeArgumentsU3Ek__BackingField_6)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_U3CTypeArgumentsU3Ek__BackingField_6() const { return ___U3CTypeArgumentsU3Ek__BackingField_6; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_U3CTypeArgumentsU3Ek__BackingField_6() { return &___U3CTypeArgumentsU3Ek__BackingField_6; }
	inline void set_U3CTypeArgumentsU3Ek__BackingField_6(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___U3CTypeArgumentsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeArgumentsU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of__argumentInfo_7() { return static_cast<int32_t>(offsetof(CSharpInvokeMemberBinder_t1B43657AF71FD88DF2707CE5FB6B9FC9534ED244, ____argumentInfo_7)); }
	inline CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* get__argumentInfo_7() const { return ____argumentInfo_7; }
	inline CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103** get_address_of__argumentInfo_7() { return &____argumentInfo_7; }
	inline void set__argumentInfo_7(CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* value)
	{
		____argumentInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&____argumentInfo_7), value);
	}

	inline static int32_t get_offset_of__binder_8() { return static_cast<int32_t>(offsetof(CSharpInvokeMemberBinder_t1B43657AF71FD88DF2707CE5FB6B9FC9534ED244, ____binder_8)); }
	inline RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * get__binder_8() const { return ____binder_8; }
	inline RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 ** get_address_of__binder_8() { return &____binder_8; }
	inline void set__binder_8(RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * value)
	{
		____binder_8 = value;
		Il2CppCodeGenWriteBarrier((&____binder_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHARPINVOKEMEMBERBINDER_T1B43657AF71FD88DF2707CE5FB6B9FC9534ED244_H
#ifndef EXPR_T0ADDBD5077E06B490FF85514AC6FC3C879FD07E5_H
#define EXPR_T0ADDBD5077E06B490FF85514AC6FC3C879FD07E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.Expr
struct  Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5  : public RuntimeObject
{
public:
	// System.Object Microsoft.CSharp.RuntimeBinder.Semantics.Expr::<RuntimeObject>k__BackingField
	RuntimeObject * ___U3CRuntimeObjectU3Ek__BackingField_0;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.Expr::<RuntimeObjectActualType>k__BackingField
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ___U3CRuntimeObjectActualTypeU3Ek__BackingField_1;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionKind Microsoft.CSharp.RuntimeBinder.Semantics.Expr::<Kind>k__BackingField
	int32_t ___U3CKindU3Ek__BackingField_2;
	// Microsoft.CSharp.RuntimeBinder.Semantics.EXPRFLAG Microsoft.CSharp.RuntimeBinder.Semantics.Expr::<Flags>k__BackingField
	int32_t ___U3CFlagsU3Ek__BackingField_3;
	// System.Boolean Microsoft.CSharp.RuntimeBinder.Semantics.Expr::<IsOptionalArgument>k__BackingField
	bool ___U3CIsOptionalArgumentU3Ek__BackingField_4;
	// System.String Microsoft.CSharp.RuntimeBinder.Semantics.Expr::<ErrorString>k__BackingField
	String_t* ___U3CErrorStringU3Ek__BackingField_5;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.Expr::<Type>k__BackingField
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ___U3CTypeU3Ek__BackingField_6;
	// System.Boolean Microsoft.CSharp.RuntimeBinder.Semantics.Expr::<HasError>k__BackingField
	bool ___U3CHasErrorU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CRuntimeObjectU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5, ___U3CRuntimeObjectU3Ek__BackingField_0)); }
	inline RuntimeObject * get_U3CRuntimeObjectU3Ek__BackingField_0() const { return ___U3CRuntimeObjectU3Ek__BackingField_0; }
	inline RuntimeObject ** get_address_of_U3CRuntimeObjectU3Ek__BackingField_0() { return &___U3CRuntimeObjectU3Ek__BackingField_0; }
	inline void set_U3CRuntimeObjectU3Ek__BackingField_0(RuntimeObject * value)
	{
		___U3CRuntimeObjectU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRuntimeObjectU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CRuntimeObjectActualTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5, ___U3CRuntimeObjectActualTypeU3Ek__BackingField_1)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get_U3CRuntimeObjectActualTypeU3Ek__BackingField_1() const { return ___U3CRuntimeObjectActualTypeU3Ek__BackingField_1; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of_U3CRuntimeObjectActualTypeU3Ek__BackingField_1() { return &___U3CRuntimeObjectActualTypeU3Ek__BackingField_1; }
	inline void set_U3CRuntimeObjectActualTypeU3Ek__BackingField_1(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		___U3CRuntimeObjectActualTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRuntimeObjectActualTypeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CKindU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5, ___U3CKindU3Ek__BackingField_2)); }
	inline int32_t get_U3CKindU3Ek__BackingField_2() const { return ___U3CKindU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CKindU3Ek__BackingField_2() { return &___U3CKindU3Ek__BackingField_2; }
	inline void set_U3CKindU3Ek__BackingField_2(int32_t value)
	{
		___U3CKindU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CFlagsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5, ___U3CFlagsU3Ek__BackingField_3)); }
	inline int32_t get_U3CFlagsU3Ek__BackingField_3() const { return ___U3CFlagsU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CFlagsU3Ek__BackingField_3() { return &___U3CFlagsU3Ek__BackingField_3; }
	inline void set_U3CFlagsU3Ek__BackingField_3(int32_t value)
	{
		___U3CFlagsU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsOptionalArgumentU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5, ___U3CIsOptionalArgumentU3Ek__BackingField_4)); }
	inline bool get_U3CIsOptionalArgumentU3Ek__BackingField_4() const { return ___U3CIsOptionalArgumentU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsOptionalArgumentU3Ek__BackingField_4() { return &___U3CIsOptionalArgumentU3Ek__BackingField_4; }
	inline void set_U3CIsOptionalArgumentU3Ek__BackingField_4(bool value)
	{
		___U3CIsOptionalArgumentU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CErrorStringU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5, ___U3CErrorStringU3Ek__BackingField_5)); }
	inline String_t* get_U3CErrorStringU3Ek__BackingField_5() const { return ___U3CErrorStringU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CErrorStringU3Ek__BackingField_5() { return &___U3CErrorStringU3Ek__BackingField_5; }
	inline void set_U3CErrorStringU3Ek__BackingField_5(String_t* value)
	{
		___U3CErrorStringU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorStringU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5, ___U3CTypeU3Ek__BackingField_6)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get_U3CTypeU3Ek__BackingField_6() const { return ___U3CTypeU3Ek__BackingField_6; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of_U3CTypeU3Ek__BackingField_6() { return &___U3CTypeU3Ek__BackingField_6; }
	inline void set_U3CTypeU3Ek__BackingField_6(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		___U3CTypeU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CHasErrorU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5, ___U3CHasErrorU3Ek__BackingField_7)); }
	inline bool get_U3CHasErrorU3Ek__BackingField_7() const { return ___U3CHasErrorU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CHasErrorU3Ek__BackingField_7() { return &___U3CHasErrorU3Ek__BackingField_7; }
	inline void set_U3CHasErrorU3Ek__BackingField_7(bool value)
	{
		___U3CHasErrorU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPR_T0ADDBD5077E06B490FF85514AC6FC3C879FD07E5_H
#ifndef BINOPARGINFO_TE147F1B6BAF4747F1598D2690E9F8D9B33C86913_H
#define BINOPARGINFO_TE147F1B6BAF4747F1598D2690E9F8D9B33C86913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpArgInfo
struct  BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.Expr Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpArgInfo::arg1
	Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * ___arg1_0;
	// Microsoft.CSharp.RuntimeBinder.Semantics.Expr Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpArgInfo::arg2
	Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * ___arg2_1;
	// Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpArgInfo::pt1
	uint32_t ___pt1_2;
	// Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpArgInfo::pt2
	uint32_t ___pt2_3;
	// Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpArgInfo::ptRaw1
	uint32_t ___ptRaw1_4;
	// Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpArgInfo::ptRaw2
	uint32_t ___ptRaw2_5;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpArgInfo::type1
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ___type1_6;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpArgInfo::type2
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ___type2_7;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpArgInfo::typeRaw1
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ___typeRaw1_8;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpArgInfo::typeRaw2
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ___typeRaw2_9;
	// Microsoft.CSharp.RuntimeBinder.Semantics.BinOpKind Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpArgInfo::binopKind
	int32_t ___binopKind_10;
	// Microsoft.CSharp.RuntimeBinder.Semantics.BinOpMask Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpArgInfo::mask
	int32_t ___mask_11;

public:
	inline static int32_t get_offset_of_arg1_0() { return static_cast<int32_t>(offsetof(BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913, ___arg1_0)); }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * get_arg1_0() const { return ___arg1_0; }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 ** get_address_of_arg1_0() { return &___arg1_0; }
	inline void set_arg1_0(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * value)
	{
		___arg1_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg1_0), value);
	}

	inline static int32_t get_offset_of_arg2_1() { return static_cast<int32_t>(offsetof(BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913, ___arg2_1)); }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * get_arg2_1() const { return ___arg2_1; }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 ** get_address_of_arg2_1() { return &___arg2_1; }
	inline void set_arg2_1(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * value)
	{
		___arg2_1 = value;
		Il2CppCodeGenWriteBarrier((&___arg2_1), value);
	}

	inline static int32_t get_offset_of_pt1_2() { return static_cast<int32_t>(offsetof(BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913, ___pt1_2)); }
	inline uint32_t get_pt1_2() const { return ___pt1_2; }
	inline uint32_t* get_address_of_pt1_2() { return &___pt1_2; }
	inline void set_pt1_2(uint32_t value)
	{
		___pt1_2 = value;
	}

	inline static int32_t get_offset_of_pt2_3() { return static_cast<int32_t>(offsetof(BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913, ___pt2_3)); }
	inline uint32_t get_pt2_3() const { return ___pt2_3; }
	inline uint32_t* get_address_of_pt2_3() { return &___pt2_3; }
	inline void set_pt2_3(uint32_t value)
	{
		___pt2_3 = value;
	}

	inline static int32_t get_offset_of_ptRaw1_4() { return static_cast<int32_t>(offsetof(BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913, ___ptRaw1_4)); }
	inline uint32_t get_ptRaw1_4() const { return ___ptRaw1_4; }
	inline uint32_t* get_address_of_ptRaw1_4() { return &___ptRaw1_4; }
	inline void set_ptRaw1_4(uint32_t value)
	{
		___ptRaw1_4 = value;
	}

	inline static int32_t get_offset_of_ptRaw2_5() { return static_cast<int32_t>(offsetof(BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913, ___ptRaw2_5)); }
	inline uint32_t get_ptRaw2_5() const { return ___ptRaw2_5; }
	inline uint32_t* get_address_of_ptRaw2_5() { return &___ptRaw2_5; }
	inline void set_ptRaw2_5(uint32_t value)
	{
		___ptRaw2_5 = value;
	}

	inline static int32_t get_offset_of_type1_6() { return static_cast<int32_t>(offsetof(BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913, ___type1_6)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get_type1_6() const { return ___type1_6; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of_type1_6() { return &___type1_6; }
	inline void set_type1_6(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		___type1_6 = value;
		Il2CppCodeGenWriteBarrier((&___type1_6), value);
	}

	inline static int32_t get_offset_of_type2_7() { return static_cast<int32_t>(offsetof(BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913, ___type2_7)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get_type2_7() const { return ___type2_7; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of_type2_7() { return &___type2_7; }
	inline void set_type2_7(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		___type2_7 = value;
		Il2CppCodeGenWriteBarrier((&___type2_7), value);
	}

	inline static int32_t get_offset_of_typeRaw1_8() { return static_cast<int32_t>(offsetof(BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913, ___typeRaw1_8)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get_typeRaw1_8() const { return ___typeRaw1_8; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of_typeRaw1_8() { return &___typeRaw1_8; }
	inline void set_typeRaw1_8(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		___typeRaw1_8 = value;
		Il2CppCodeGenWriteBarrier((&___typeRaw1_8), value);
	}

	inline static int32_t get_offset_of_typeRaw2_9() { return static_cast<int32_t>(offsetof(BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913, ___typeRaw2_9)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get_typeRaw2_9() const { return ___typeRaw2_9; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of_typeRaw2_9() { return &___typeRaw2_9; }
	inline void set_typeRaw2_9(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		___typeRaw2_9 = value;
		Il2CppCodeGenWriteBarrier((&___typeRaw2_9), value);
	}

	inline static int32_t get_offset_of_binopKind_10() { return static_cast<int32_t>(offsetof(BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913, ___binopKind_10)); }
	inline int32_t get_binopKind_10() const { return ___binopKind_10; }
	inline int32_t* get_address_of_binopKind_10() { return &___binopKind_10; }
	inline void set_binopKind_10(int32_t value)
	{
		___binopKind_10 = value;
	}

	inline static int32_t get_offset_of_mask_11() { return static_cast<int32_t>(offsetof(BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913, ___mask_11)); }
	inline int32_t get_mask_11() const { return ___mask_11; }
	inline int32_t* get_address_of_mask_11() { return &___mask_11; }
	inline void set_mask_11(int32_t value)
	{
		___mask_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINOPARGINFO_TE147F1B6BAF4747F1598D2690E9F8D9B33C86913_H
#ifndef BINOPSIG_T41167D930280E1719B8F12646C526CBFC516ADD3_H
#define BINOPSIG_T41167D930280E1719B8F12646C526CBFC516ADD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpSig
struct  BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpSig::pt1
	uint32_t ___pt1_0;
	// Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpSig::pt2
	uint32_t ___pt2_1;
	// Microsoft.CSharp.RuntimeBinder.Semantics.BinOpMask Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpSig::mask
	int32_t ___mask_2;
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpSig::cbosSkip
	int32_t ___cbosSkip_3;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/PfnBindBinOp Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpSig::pfn
	PfnBindBinOp_t376819D18187F697A9950060805897A8BF537F68 * ___pfn_4;
	// Microsoft.CSharp.RuntimeBinder.Semantics.OpSigFlags Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpSig::grfos
	int32_t ___grfos_5;
	// Microsoft.CSharp.RuntimeBinder.Semantics.BinOpFuncKind Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpSig::fnkind
	int32_t ___fnkind_6;

public:
	inline static int32_t get_offset_of_pt1_0() { return static_cast<int32_t>(offsetof(BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3, ___pt1_0)); }
	inline uint32_t get_pt1_0() const { return ___pt1_0; }
	inline uint32_t* get_address_of_pt1_0() { return &___pt1_0; }
	inline void set_pt1_0(uint32_t value)
	{
		___pt1_0 = value;
	}

	inline static int32_t get_offset_of_pt2_1() { return static_cast<int32_t>(offsetof(BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3, ___pt2_1)); }
	inline uint32_t get_pt2_1() const { return ___pt2_1; }
	inline uint32_t* get_address_of_pt2_1() { return &___pt2_1; }
	inline void set_pt2_1(uint32_t value)
	{
		___pt2_1 = value;
	}

	inline static int32_t get_offset_of_mask_2() { return static_cast<int32_t>(offsetof(BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3, ___mask_2)); }
	inline int32_t get_mask_2() const { return ___mask_2; }
	inline int32_t* get_address_of_mask_2() { return &___mask_2; }
	inline void set_mask_2(int32_t value)
	{
		___mask_2 = value;
	}

	inline static int32_t get_offset_of_cbosSkip_3() { return static_cast<int32_t>(offsetof(BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3, ___cbosSkip_3)); }
	inline int32_t get_cbosSkip_3() const { return ___cbosSkip_3; }
	inline int32_t* get_address_of_cbosSkip_3() { return &___cbosSkip_3; }
	inline void set_cbosSkip_3(int32_t value)
	{
		___cbosSkip_3 = value;
	}

	inline static int32_t get_offset_of_pfn_4() { return static_cast<int32_t>(offsetof(BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3, ___pfn_4)); }
	inline PfnBindBinOp_t376819D18187F697A9950060805897A8BF537F68 * get_pfn_4() const { return ___pfn_4; }
	inline PfnBindBinOp_t376819D18187F697A9950060805897A8BF537F68 ** get_address_of_pfn_4() { return &___pfn_4; }
	inline void set_pfn_4(PfnBindBinOp_t376819D18187F697A9950060805897A8BF537F68 * value)
	{
		___pfn_4 = value;
		Il2CppCodeGenWriteBarrier((&___pfn_4), value);
	}

	inline static int32_t get_offset_of_grfos_5() { return static_cast<int32_t>(offsetof(BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3, ___grfos_5)); }
	inline int32_t get_grfos_5() const { return ___grfos_5; }
	inline int32_t* get_address_of_grfos_5() { return &___grfos_5; }
	inline void set_grfos_5(int32_t value)
	{
		___grfos_5 = value;
	}

	inline static int32_t get_offset_of_fnkind_6() { return static_cast<int32_t>(offsetof(BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3, ___fnkind_6)); }
	inline int32_t get_fnkind_6() const { return ___fnkind_6; }
	inline int32_t* get_address_of_fnkind_6() { return &___fnkind_6; }
	inline void set_fnkind_6(int32_t value)
	{
		___fnkind_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINOPSIG_T41167D930280E1719B8F12646C526CBFC516ADD3_H
#ifndef EXPLICITCONVERSION_TFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716_H
#define EXPLICITCONVERSION_TFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ExplicitConversion
struct  ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ExplicitConversion::_binder
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E * ____binder_0;
	// Microsoft.CSharp.RuntimeBinder.Semantics.Expr Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ExplicitConversion::_exprSrc
	Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * ____exprSrc_1;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ExplicitConversion::_typeSrc
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ____typeSrc_2;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ExplicitConversion::_typeDest
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ____typeDest_3;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExprClass Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ExplicitConversion::_exprTypeDest
	ExprClass_t65292AB1E432F6C18D890D791F1A736AC8233278 * ____exprTypeDest_4;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ExplicitConversion::_pDestinationTypeForLambdaErrorReporting
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ____pDestinationTypeForLambdaErrorReporting_5;
	// Microsoft.CSharp.RuntimeBinder.Semantics.Expr Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ExplicitConversion::_exprDest
	Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * ____exprDest_6;
	// System.Boolean Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ExplicitConversion::_needsExprDest
	bool ____needsExprDest_7;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CONVERTTYPE Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ExplicitConversion::_flags
	int32_t ____flags_8;

public:
	inline static int32_t get_offset_of__binder_0() { return static_cast<int32_t>(offsetof(ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716, ____binder_0)); }
	inline ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E * get__binder_0() const { return ____binder_0; }
	inline ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E ** get_address_of__binder_0() { return &____binder_0; }
	inline void set__binder_0(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E * value)
	{
		____binder_0 = value;
		Il2CppCodeGenWriteBarrier((&____binder_0), value);
	}

	inline static int32_t get_offset_of__exprSrc_1() { return static_cast<int32_t>(offsetof(ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716, ____exprSrc_1)); }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * get__exprSrc_1() const { return ____exprSrc_1; }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 ** get_address_of__exprSrc_1() { return &____exprSrc_1; }
	inline void set__exprSrc_1(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * value)
	{
		____exprSrc_1 = value;
		Il2CppCodeGenWriteBarrier((&____exprSrc_1), value);
	}

	inline static int32_t get_offset_of__typeSrc_2() { return static_cast<int32_t>(offsetof(ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716, ____typeSrc_2)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get__typeSrc_2() const { return ____typeSrc_2; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of__typeSrc_2() { return &____typeSrc_2; }
	inline void set__typeSrc_2(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		____typeSrc_2 = value;
		Il2CppCodeGenWriteBarrier((&____typeSrc_2), value);
	}

	inline static int32_t get_offset_of__typeDest_3() { return static_cast<int32_t>(offsetof(ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716, ____typeDest_3)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get__typeDest_3() const { return ____typeDest_3; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of__typeDest_3() { return &____typeDest_3; }
	inline void set__typeDest_3(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		____typeDest_3 = value;
		Il2CppCodeGenWriteBarrier((&____typeDest_3), value);
	}

	inline static int32_t get_offset_of__exprTypeDest_4() { return static_cast<int32_t>(offsetof(ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716, ____exprTypeDest_4)); }
	inline ExprClass_t65292AB1E432F6C18D890D791F1A736AC8233278 * get__exprTypeDest_4() const { return ____exprTypeDest_4; }
	inline ExprClass_t65292AB1E432F6C18D890D791F1A736AC8233278 ** get_address_of__exprTypeDest_4() { return &____exprTypeDest_4; }
	inline void set__exprTypeDest_4(ExprClass_t65292AB1E432F6C18D890D791F1A736AC8233278 * value)
	{
		____exprTypeDest_4 = value;
		Il2CppCodeGenWriteBarrier((&____exprTypeDest_4), value);
	}

	inline static int32_t get_offset_of__pDestinationTypeForLambdaErrorReporting_5() { return static_cast<int32_t>(offsetof(ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716, ____pDestinationTypeForLambdaErrorReporting_5)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get__pDestinationTypeForLambdaErrorReporting_5() const { return ____pDestinationTypeForLambdaErrorReporting_5; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of__pDestinationTypeForLambdaErrorReporting_5() { return &____pDestinationTypeForLambdaErrorReporting_5; }
	inline void set__pDestinationTypeForLambdaErrorReporting_5(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		____pDestinationTypeForLambdaErrorReporting_5 = value;
		Il2CppCodeGenWriteBarrier((&____pDestinationTypeForLambdaErrorReporting_5), value);
	}

	inline static int32_t get_offset_of__exprDest_6() { return static_cast<int32_t>(offsetof(ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716, ____exprDest_6)); }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * get__exprDest_6() const { return ____exprDest_6; }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 ** get_address_of__exprDest_6() { return &____exprDest_6; }
	inline void set__exprDest_6(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * value)
	{
		____exprDest_6 = value;
		Il2CppCodeGenWriteBarrier((&____exprDest_6), value);
	}

	inline static int32_t get_offset_of__needsExprDest_7() { return static_cast<int32_t>(offsetof(ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716, ____needsExprDest_7)); }
	inline bool get__needsExprDest_7() const { return ____needsExprDest_7; }
	inline bool* get_address_of__needsExprDest_7() { return &____needsExprDest_7; }
	inline void set__needsExprDest_7(bool value)
	{
		____needsExprDest_7 = value;
	}

	inline static int32_t get_offset_of__flags_8() { return static_cast<int32_t>(offsetof(ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716, ____flags_8)); }
	inline int32_t get__flags_8() const { return ____flags_8; }
	inline int32_t* get_address_of__flags_8() { return &____flags_8; }
	inline void set__flags_8(int32_t value)
	{
		____flags_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPLICITCONVERSION_TFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716_H
#ifndef GROUPTOARGSBINDER_TB9E500A6861864E8A15A0CE9D1B5887978ED7D82_H
#define GROUPTOARGSBINDER_TB9E500A6861864E8A15A0CE9D1B5887978ED7D82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder
struct  GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_pExprBinder
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E * ____pExprBinder_0;
	// System.Boolean Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_fCandidatesUnsupported
	bool ____fCandidatesUnsupported_1;
	// Microsoft.CSharp.RuntimeBinder.Semantics.BindingFlag Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_fBindFlags
	int32_t ____fBindFlags_2;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExprMemberGroup Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_pGroup
	ExprMemberGroup_tDD0D29E0D6B46D5922D603F22F5D67F78AE35AA4 * ____pGroup_3;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ArgInfos Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_pArguments
	ArgInfos_t8DDC052214D1A27ABDAEAB029CCF83CE18A6AE6D * ____pArguments_4;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ArgInfos Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_pOriginalArguments
	ArgInfos_t8DDC052214D1A27ABDAEAB029CCF83CE18A6AE6D * ____pOriginalArguments_5;
	// System.Boolean Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_bHasNamedArguments
	bool ____bHasNamedArguments_6;
	// Microsoft.CSharp.RuntimeBinder.Semantics.AggregateType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_pDelegate
	AggregateType_t8F8CDB34BC1F11AD3004AFE3BD62A0C6221CB3BB * ____pDelegate_7;
	// Microsoft.CSharp.RuntimeBinder.Semantics.AggregateType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_pCurrentType
	AggregateType_t8F8CDB34BC1F11AD3004AFE3BD62A0C6221CB3BB * ____pCurrentType_8;
	// Microsoft.CSharp.RuntimeBinder.Semantics.MethodOrPropertySymbol Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_pCurrentSym
	MethodOrPropertySymbol_tFCA1AC659653934F87CB627CB32229A52A0270B0 * ____pCurrentSym_9;
	// Microsoft.CSharp.RuntimeBinder.Semantics.TypeArray Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_pCurrentTypeArgs
	TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 * ____pCurrentTypeArgs_10;
	// Microsoft.CSharp.RuntimeBinder.Semantics.TypeArray Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_pCurrentParameters
	TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 * ____pCurrentParameters_11;
	// Microsoft.CSharp.RuntimeBinder.Semantics.TypeArray Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_pBestParameters
	TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 * ____pBestParameters_12;
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_nArgBest
	int32_t ____nArgBest_13;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinderResult Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_results
	GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0 * ____results_14;
	// System.Collections.Generic.List`1<Microsoft.CSharp.RuntimeBinder.Semantics.CandidateFunctionMember> Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_methList
	List_1_t9226D64AE50FE47D8B11AE1370678622D6D69428 * ____methList_15;
	// Microsoft.CSharp.RuntimeBinder.Semantics.MethPropWithInst Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_mpwiParamTypeConstraints
	MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * ____mpwiParamTypeConstraints_16;
	// Microsoft.CSharp.RuntimeBinder.Semantics.MethPropWithInst Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_mpwiBogus
	MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * ____mpwiBogus_17;
	// Microsoft.CSharp.RuntimeBinder.Semantics.MethPropWithInst Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_mpwiCantInferInstArg
	MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * ____mpwiCantInferInstArg_18;
	// Microsoft.CSharp.RuntimeBinder.Semantics.MethWithType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_mwtBadArity
	MethWithType_t4CCE9B8DC13DE224B4CE2FCAA33226ACA7380616 * ____mwtBadArity_19;
	// Microsoft.CSharp.RuntimeBinder.Syntax.Name Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_pInvalidSpecifiedName
	Name_t95244514D054D80B6614F5E2275EC034F1527725 * ____pInvalidSpecifiedName_20;
	// Microsoft.CSharp.RuntimeBinder.Syntax.Name Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_pNameUsedInPositionalArgument
	Name_t95244514D054D80B6614F5E2275EC034F1527725 * ____pNameUsedInPositionalArgument_21;
	// Microsoft.CSharp.RuntimeBinder.Syntax.Name Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_pDuplicateSpecifiedName
	Name_t95244514D054D80B6614F5E2275EC034F1527725 * ____pDuplicateSpecifiedName_22;
	// System.Collections.Generic.List`1<Microsoft.CSharp.RuntimeBinder.Semantics.CType> Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_HiddenTypes
	List_1_t8C111A83B900F2EF6541780D77CFDD38873EF3B1 * ____HiddenTypes_23;
	// System.Boolean Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/GroupToArgsBinder::_bArgumentsChangedForNamedOrOptionalArguments
	bool ____bArgumentsChangedForNamedOrOptionalArguments_24;

public:
	inline static int32_t get_offset_of__pExprBinder_0() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____pExprBinder_0)); }
	inline ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E * get__pExprBinder_0() const { return ____pExprBinder_0; }
	inline ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E ** get_address_of__pExprBinder_0() { return &____pExprBinder_0; }
	inline void set__pExprBinder_0(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E * value)
	{
		____pExprBinder_0 = value;
		Il2CppCodeGenWriteBarrier((&____pExprBinder_0), value);
	}

	inline static int32_t get_offset_of__fCandidatesUnsupported_1() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____fCandidatesUnsupported_1)); }
	inline bool get__fCandidatesUnsupported_1() const { return ____fCandidatesUnsupported_1; }
	inline bool* get_address_of__fCandidatesUnsupported_1() { return &____fCandidatesUnsupported_1; }
	inline void set__fCandidatesUnsupported_1(bool value)
	{
		____fCandidatesUnsupported_1 = value;
	}

	inline static int32_t get_offset_of__fBindFlags_2() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____fBindFlags_2)); }
	inline int32_t get__fBindFlags_2() const { return ____fBindFlags_2; }
	inline int32_t* get_address_of__fBindFlags_2() { return &____fBindFlags_2; }
	inline void set__fBindFlags_2(int32_t value)
	{
		____fBindFlags_2 = value;
	}

	inline static int32_t get_offset_of__pGroup_3() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____pGroup_3)); }
	inline ExprMemberGroup_tDD0D29E0D6B46D5922D603F22F5D67F78AE35AA4 * get__pGroup_3() const { return ____pGroup_3; }
	inline ExprMemberGroup_tDD0D29E0D6B46D5922D603F22F5D67F78AE35AA4 ** get_address_of__pGroup_3() { return &____pGroup_3; }
	inline void set__pGroup_3(ExprMemberGroup_tDD0D29E0D6B46D5922D603F22F5D67F78AE35AA4 * value)
	{
		____pGroup_3 = value;
		Il2CppCodeGenWriteBarrier((&____pGroup_3), value);
	}

	inline static int32_t get_offset_of__pArguments_4() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____pArguments_4)); }
	inline ArgInfos_t8DDC052214D1A27ABDAEAB029CCF83CE18A6AE6D * get__pArguments_4() const { return ____pArguments_4; }
	inline ArgInfos_t8DDC052214D1A27ABDAEAB029CCF83CE18A6AE6D ** get_address_of__pArguments_4() { return &____pArguments_4; }
	inline void set__pArguments_4(ArgInfos_t8DDC052214D1A27ABDAEAB029CCF83CE18A6AE6D * value)
	{
		____pArguments_4 = value;
		Il2CppCodeGenWriteBarrier((&____pArguments_4), value);
	}

	inline static int32_t get_offset_of__pOriginalArguments_5() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____pOriginalArguments_5)); }
	inline ArgInfos_t8DDC052214D1A27ABDAEAB029CCF83CE18A6AE6D * get__pOriginalArguments_5() const { return ____pOriginalArguments_5; }
	inline ArgInfos_t8DDC052214D1A27ABDAEAB029CCF83CE18A6AE6D ** get_address_of__pOriginalArguments_5() { return &____pOriginalArguments_5; }
	inline void set__pOriginalArguments_5(ArgInfos_t8DDC052214D1A27ABDAEAB029CCF83CE18A6AE6D * value)
	{
		____pOriginalArguments_5 = value;
		Il2CppCodeGenWriteBarrier((&____pOriginalArguments_5), value);
	}

	inline static int32_t get_offset_of__bHasNamedArguments_6() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____bHasNamedArguments_6)); }
	inline bool get__bHasNamedArguments_6() const { return ____bHasNamedArguments_6; }
	inline bool* get_address_of__bHasNamedArguments_6() { return &____bHasNamedArguments_6; }
	inline void set__bHasNamedArguments_6(bool value)
	{
		____bHasNamedArguments_6 = value;
	}

	inline static int32_t get_offset_of__pDelegate_7() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____pDelegate_7)); }
	inline AggregateType_t8F8CDB34BC1F11AD3004AFE3BD62A0C6221CB3BB * get__pDelegate_7() const { return ____pDelegate_7; }
	inline AggregateType_t8F8CDB34BC1F11AD3004AFE3BD62A0C6221CB3BB ** get_address_of__pDelegate_7() { return &____pDelegate_7; }
	inline void set__pDelegate_7(AggregateType_t8F8CDB34BC1F11AD3004AFE3BD62A0C6221CB3BB * value)
	{
		____pDelegate_7 = value;
		Il2CppCodeGenWriteBarrier((&____pDelegate_7), value);
	}

	inline static int32_t get_offset_of__pCurrentType_8() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____pCurrentType_8)); }
	inline AggregateType_t8F8CDB34BC1F11AD3004AFE3BD62A0C6221CB3BB * get__pCurrentType_8() const { return ____pCurrentType_8; }
	inline AggregateType_t8F8CDB34BC1F11AD3004AFE3BD62A0C6221CB3BB ** get_address_of__pCurrentType_8() { return &____pCurrentType_8; }
	inline void set__pCurrentType_8(AggregateType_t8F8CDB34BC1F11AD3004AFE3BD62A0C6221CB3BB * value)
	{
		____pCurrentType_8 = value;
		Il2CppCodeGenWriteBarrier((&____pCurrentType_8), value);
	}

	inline static int32_t get_offset_of__pCurrentSym_9() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____pCurrentSym_9)); }
	inline MethodOrPropertySymbol_tFCA1AC659653934F87CB627CB32229A52A0270B0 * get__pCurrentSym_9() const { return ____pCurrentSym_9; }
	inline MethodOrPropertySymbol_tFCA1AC659653934F87CB627CB32229A52A0270B0 ** get_address_of__pCurrentSym_9() { return &____pCurrentSym_9; }
	inline void set__pCurrentSym_9(MethodOrPropertySymbol_tFCA1AC659653934F87CB627CB32229A52A0270B0 * value)
	{
		____pCurrentSym_9 = value;
		Il2CppCodeGenWriteBarrier((&____pCurrentSym_9), value);
	}

	inline static int32_t get_offset_of__pCurrentTypeArgs_10() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____pCurrentTypeArgs_10)); }
	inline TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 * get__pCurrentTypeArgs_10() const { return ____pCurrentTypeArgs_10; }
	inline TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 ** get_address_of__pCurrentTypeArgs_10() { return &____pCurrentTypeArgs_10; }
	inline void set__pCurrentTypeArgs_10(TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 * value)
	{
		____pCurrentTypeArgs_10 = value;
		Il2CppCodeGenWriteBarrier((&____pCurrentTypeArgs_10), value);
	}

	inline static int32_t get_offset_of__pCurrentParameters_11() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____pCurrentParameters_11)); }
	inline TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 * get__pCurrentParameters_11() const { return ____pCurrentParameters_11; }
	inline TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 ** get_address_of__pCurrentParameters_11() { return &____pCurrentParameters_11; }
	inline void set__pCurrentParameters_11(TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 * value)
	{
		____pCurrentParameters_11 = value;
		Il2CppCodeGenWriteBarrier((&____pCurrentParameters_11), value);
	}

	inline static int32_t get_offset_of__pBestParameters_12() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____pBestParameters_12)); }
	inline TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 * get__pBestParameters_12() const { return ____pBestParameters_12; }
	inline TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 ** get_address_of__pBestParameters_12() { return &____pBestParameters_12; }
	inline void set__pBestParameters_12(TypeArray_t3B3C009F61B76695BDE2D2D3F8EC6F012F5536E2 * value)
	{
		____pBestParameters_12 = value;
		Il2CppCodeGenWriteBarrier((&____pBestParameters_12), value);
	}

	inline static int32_t get_offset_of__nArgBest_13() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____nArgBest_13)); }
	inline int32_t get__nArgBest_13() const { return ____nArgBest_13; }
	inline int32_t* get_address_of__nArgBest_13() { return &____nArgBest_13; }
	inline void set__nArgBest_13(int32_t value)
	{
		____nArgBest_13 = value;
	}

	inline static int32_t get_offset_of__results_14() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____results_14)); }
	inline GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0 * get__results_14() const { return ____results_14; }
	inline GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0 ** get_address_of__results_14() { return &____results_14; }
	inline void set__results_14(GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0 * value)
	{
		____results_14 = value;
		Il2CppCodeGenWriteBarrier((&____results_14), value);
	}

	inline static int32_t get_offset_of__methList_15() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____methList_15)); }
	inline List_1_t9226D64AE50FE47D8B11AE1370678622D6D69428 * get__methList_15() const { return ____methList_15; }
	inline List_1_t9226D64AE50FE47D8B11AE1370678622D6D69428 ** get_address_of__methList_15() { return &____methList_15; }
	inline void set__methList_15(List_1_t9226D64AE50FE47D8B11AE1370678622D6D69428 * value)
	{
		____methList_15 = value;
		Il2CppCodeGenWriteBarrier((&____methList_15), value);
	}

	inline static int32_t get_offset_of__mpwiParamTypeConstraints_16() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____mpwiParamTypeConstraints_16)); }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * get__mpwiParamTypeConstraints_16() const { return ____mpwiParamTypeConstraints_16; }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C ** get_address_of__mpwiParamTypeConstraints_16() { return &____mpwiParamTypeConstraints_16; }
	inline void set__mpwiParamTypeConstraints_16(MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * value)
	{
		____mpwiParamTypeConstraints_16 = value;
		Il2CppCodeGenWriteBarrier((&____mpwiParamTypeConstraints_16), value);
	}

	inline static int32_t get_offset_of__mpwiBogus_17() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____mpwiBogus_17)); }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * get__mpwiBogus_17() const { return ____mpwiBogus_17; }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C ** get_address_of__mpwiBogus_17() { return &____mpwiBogus_17; }
	inline void set__mpwiBogus_17(MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * value)
	{
		____mpwiBogus_17 = value;
		Il2CppCodeGenWriteBarrier((&____mpwiBogus_17), value);
	}

	inline static int32_t get_offset_of__mpwiCantInferInstArg_18() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____mpwiCantInferInstArg_18)); }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * get__mpwiCantInferInstArg_18() const { return ____mpwiCantInferInstArg_18; }
	inline MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C ** get_address_of__mpwiCantInferInstArg_18() { return &____mpwiCantInferInstArg_18; }
	inline void set__mpwiCantInferInstArg_18(MethPropWithInst_t9E43D7D6CF88CBAFB86458FF72D0C6D0F5982E3C * value)
	{
		____mpwiCantInferInstArg_18 = value;
		Il2CppCodeGenWriteBarrier((&____mpwiCantInferInstArg_18), value);
	}

	inline static int32_t get_offset_of__mwtBadArity_19() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____mwtBadArity_19)); }
	inline MethWithType_t4CCE9B8DC13DE224B4CE2FCAA33226ACA7380616 * get__mwtBadArity_19() const { return ____mwtBadArity_19; }
	inline MethWithType_t4CCE9B8DC13DE224B4CE2FCAA33226ACA7380616 ** get_address_of__mwtBadArity_19() { return &____mwtBadArity_19; }
	inline void set__mwtBadArity_19(MethWithType_t4CCE9B8DC13DE224B4CE2FCAA33226ACA7380616 * value)
	{
		____mwtBadArity_19 = value;
		Il2CppCodeGenWriteBarrier((&____mwtBadArity_19), value);
	}

	inline static int32_t get_offset_of__pInvalidSpecifiedName_20() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____pInvalidSpecifiedName_20)); }
	inline Name_t95244514D054D80B6614F5E2275EC034F1527725 * get__pInvalidSpecifiedName_20() const { return ____pInvalidSpecifiedName_20; }
	inline Name_t95244514D054D80B6614F5E2275EC034F1527725 ** get_address_of__pInvalidSpecifiedName_20() { return &____pInvalidSpecifiedName_20; }
	inline void set__pInvalidSpecifiedName_20(Name_t95244514D054D80B6614F5E2275EC034F1527725 * value)
	{
		____pInvalidSpecifiedName_20 = value;
		Il2CppCodeGenWriteBarrier((&____pInvalidSpecifiedName_20), value);
	}

	inline static int32_t get_offset_of__pNameUsedInPositionalArgument_21() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____pNameUsedInPositionalArgument_21)); }
	inline Name_t95244514D054D80B6614F5E2275EC034F1527725 * get__pNameUsedInPositionalArgument_21() const { return ____pNameUsedInPositionalArgument_21; }
	inline Name_t95244514D054D80B6614F5E2275EC034F1527725 ** get_address_of__pNameUsedInPositionalArgument_21() { return &____pNameUsedInPositionalArgument_21; }
	inline void set__pNameUsedInPositionalArgument_21(Name_t95244514D054D80B6614F5E2275EC034F1527725 * value)
	{
		____pNameUsedInPositionalArgument_21 = value;
		Il2CppCodeGenWriteBarrier((&____pNameUsedInPositionalArgument_21), value);
	}

	inline static int32_t get_offset_of__pDuplicateSpecifiedName_22() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____pDuplicateSpecifiedName_22)); }
	inline Name_t95244514D054D80B6614F5E2275EC034F1527725 * get__pDuplicateSpecifiedName_22() const { return ____pDuplicateSpecifiedName_22; }
	inline Name_t95244514D054D80B6614F5E2275EC034F1527725 ** get_address_of__pDuplicateSpecifiedName_22() { return &____pDuplicateSpecifiedName_22; }
	inline void set__pDuplicateSpecifiedName_22(Name_t95244514D054D80B6614F5E2275EC034F1527725 * value)
	{
		____pDuplicateSpecifiedName_22 = value;
		Il2CppCodeGenWriteBarrier((&____pDuplicateSpecifiedName_22), value);
	}

	inline static int32_t get_offset_of__HiddenTypes_23() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____HiddenTypes_23)); }
	inline List_1_t8C111A83B900F2EF6541780D77CFDD38873EF3B1 * get__HiddenTypes_23() const { return ____HiddenTypes_23; }
	inline List_1_t8C111A83B900F2EF6541780D77CFDD38873EF3B1 ** get_address_of__HiddenTypes_23() { return &____HiddenTypes_23; }
	inline void set__HiddenTypes_23(List_1_t8C111A83B900F2EF6541780D77CFDD38873EF3B1 * value)
	{
		____HiddenTypes_23 = value;
		Il2CppCodeGenWriteBarrier((&____HiddenTypes_23), value);
	}

	inline static int32_t get_offset_of__bArgumentsChangedForNamedOrOptionalArguments_24() { return static_cast<int32_t>(offsetof(GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82, ____bArgumentsChangedForNamedOrOptionalArguments_24)); }
	inline bool get__bArgumentsChangedForNamedOrOptionalArguments_24() const { return ____bArgumentsChangedForNamedOrOptionalArguments_24; }
	inline bool* get_address_of__bArgumentsChangedForNamedOrOptionalArguments_24() { return &____bArgumentsChangedForNamedOrOptionalArguments_24; }
	inline void set__bArgumentsChangedForNamedOrOptionalArguments_24(bool value)
	{
		____bArgumentsChangedForNamedOrOptionalArguments_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPTOARGSBINDER_TB9E500A6861864E8A15A0CE9D1B5887978ED7D82_H
#ifndef IMPLICITCONVERSION_T5815575A8493949A6368CD893A47A121491C2A0D_H
#define IMPLICITCONVERSION_T5815575A8493949A6368CD893A47A121491C2A0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ImplicitConversion
struct  ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.Expr Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ImplicitConversion::_exprDest
	Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * ____exprDest_0;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ImplicitConversion::_binder
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E * ____binder_1;
	// Microsoft.CSharp.RuntimeBinder.Semantics.Expr Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ImplicitConversion::_exprSrc
	Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * ____exprSrc_2;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ImplicitConversion::_typeSrc
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ____typeSrc_3;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ImplicitConversion::_typeDest
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ____typeDest_4;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExprClass Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ImplicitConversion::_exprTypeDest
	ExprClass_t65292AB1E432F6C18D890D791F1A736AC8233278 * ____exprTypeDest_5;
	// System.Boolean Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ImplicitConversion::_needsExprDest
	bool ____needsExprDest_6;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CONVERTTYPE Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ImplicitConversion::_flags
	int32_t ____flags_7;

public:
	inline static int32_t get_offset_of__exprDest_0() { return static_cast<int32_t>(offsetof(ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D, ____exprDest_0)); }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * get__exprDest_0() const { return ____exprDest_0; }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 ** get_address_of__exprDest_0() { return &____exprDest_0; }
	inline void set__exprDest_0(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * value)
	{
		____exprDest_0 = value;
		Il2CppCodeGenWriteBarrier((&____exprDest_0), value);
	}

	inline static int32_t get_offset_of__binder_1() { return static_cast<int32_t>(offsetof(ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D, ____binder_1)); }
	inline ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E * get__binder_1() const { return ____binder_1; }
	inline ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E ** get_address_of__binder_1() { return &____binder_1; }
	inline void set__binder_1(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E * value)
	{
		____binder_1 = value;
		Il2CppCodeGenWriteBarrier((&____binder_1), value);
	}

	inline static int32_t get_offset_of__exprSrc_2() { return static_cast<int32_t>(offsetof(ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D, ____exprSrc_2)); }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * get__exprSrc_2() const { return ____exprSrc_2; }
	inline Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 ** get_address_of__exprSrc_2() { return &____exprSrc_2; }
	inline void set__exprSrc_2(Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5 * value)
	{
		____exprSrc_2 = value;
		Il2CppCodeGenWriteBarrier((&____exprSrc_2), value);
	}

	inline static int32_t get_offset_of__typeSrc_3() { return static_cast<int32_t>(offsetof(ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D, ____typeSrc_3)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get__typeSrc_3() const { return ____typeSrc_3; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of__typeSrc_3() { return &____typeSrc_3; }
	inline void set__typeSrc_3(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		____typeSrc_3 = value;
		Il2CppCodeGenWriteBarrier((&____typeSrc_3), value);
	}

	inline static int32_t get_offset_of__typeDest_4() { return static_cast<int32_t>(offsetof(ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D, ____typeDest_4)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get__typeDest_4() const { return ____typeDest_4; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of__typeDest_4() { return &____typeDest_4; }
	inline void set__typeDest_4(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		____typeDest_4 = value;
		Il2CppCodeGenWriteBarrier((&____typeDest_4), value);
	}

	inline static int32_t get_offset_of__exprTypeDest_5() { return static_cast<int32_t>(offsetof(ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D, ____exprTypeDest_5)); }
	inline ExprClass_t65292AB1E432F6C18D890D791F1A736AC8233278 * get__exprTypeDest_5() const { return ____exprTypeDest_5; }
	inline ExprClass_t65292AB1E432F6C18D890D791F1A736AC8233278 ** get_address_of__exprTypeDest_5() { return &____exprTypeDest_5; }
	inline void set__exprTypeDest_5(ExprClass_t65292AB1E432F6C18D890D791F1A736AC8233278 * value)
	{
		____exprTypeDest_5 = value;
		Il2CppCodeGenWriteBarrier((&____exprTypeDest_5), value);
	}

	inline static int32_t get_offset_of__needsExprDest_6() { return static_cast<int32_t>(offsetof(ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D, ____needsExprDest_6)); }
	inline bool get__needsExprDest_6() const { return ____needsExprDest_6; }
	inline bool* get_address_of__needsExprDest_6() { return &____needsExprDest_6; }
	inline void set__needsExprDest_6(bool value)
	{
		____needsExprDest_6 = value;
	}

	inline static int32_t get_offset_of__flags_7() { return static_cast<int32_t>(offsetof(ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D, ____flags_7)); }
	inline int32_t get__flags_7() const { return ____flags_7; }
	inline int32_t* get_address_of__flags_7() { return &____flags_7; }
	inline void set__flags_7(int32_t value)
	{
		____flags_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPLICITCONVERSION_T5815575A8493949A6368CD893A47A121491C2A0D_H
#ifndef UNAOPSIG_T66B0230ED69A79A4288B6C38326A1A1EB4E44AB1_H
#define UNAOPSIG_T66B0230ED69A79A4288B6C38326A1A1EB4E44AB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/UnaOpSig
struct  UnaOpSig_t66B0230ED69A79A4288B6C38326A1A1EB4E44AB1  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/UnaOpSig::pt
	uint32_t ___pt_0;
	// Microsoft.CSharp.RuntimeBinder.Semantics.UnaOpMask Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/UnaOpSig::grfuom
	int32_t ___grfuom_1;
	// System.Int32 Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/UnaOpSig::cuosSkip
	int32_t ___cuosSkip_2;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/PfnBindUnaOp Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/UnaOpSig::pfn
	PfnBindUnaOp_tF887D64C6A968258E18C38389610C6E1E70E6944 * ___pfn_3;
	// Microsoft.CSharp.RuntimeBinder.Semantics.UnaOpFuncKind Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/UnaOpSig::fnkind
	int32_t ___fnkind_4;

public:
	inline static int32_t get_offset_of_pt_0() { return static_cast<int32_t>(offsetof(UnaOpSig_t66B0230ED69A79A4288B6C38326A1A1EB4E44AB1, ___pt_0)); }
	inline uint32_t get_pt_0() const { return ___pt_0; }
	inline uint32_t* get_address_of_pt_0() { return &___pt_0; }
	inline void set_pt_0(uint32_t value)
	{
		___pt_0 = value;
	}

	inline static int32_t get_offset_of_grfuom_1() { return static_cast<int32_t>(offsetof(UnaOpSig_t66B0230ED69A79A4288B6C38326A1A1EB4E44AB1, ___grfuom_1)); }
	inline int32_t get_grfuom_1() const { return ___grfuom_1; }
	inline int32_t* get_address_of_grfuom_1() { return &___grfuom_1; }
	inline void set_grfuom_1(int32_t value)
	{
		___grfuom_1 = value;
	}

	inline static int32_t get_offset_of_cuosSkip_2() { return static_cast<int32_t>(offsetof(UnaOpSig_t66B0230ED69A79A4288B6C38326A1A1EB4E44AB1, ___cuosSkip_2)); }
	inline int32_t get_cuosSkip_2() const { return ___cuosSkip_2; }
	inline int32_t* get_address_of_cuosSkip_2() { return &___cuosSkip_2; }
	inline void set_cuosSkip_2(int32_t value)
	{
		___cuosSkip_2 = value;
	}

	inline static int32_t get_offset_of_pfn_3() { return static_cast<int32_t>(offsetof(UnaOpSig_t66B0230ED69A79A4288B6C38326A1A1EB4E44AB1, ___pfn_3)); }
	inline PfnBindUnaOp_tF887D64C6A968258E18C38389610C6E1E70E6944 * get_pfn_3() const { return ___pfn_3; }
	inline PfnBindUnaOp_tF887D64C6A968258E18C38389610C6E1E70E6944 ** get_address_of_pfn_3() { return &___pfn_3; }
	inline void set_pfn_3(PfnBindUnaOp_tF887D64C6A968258E18C38389610C6E1E70E6944 * value)
	{
		___pfn_3 = value;
		Il2CppCodeGenWriteBarrier((&___pfn_3), value);
	}

	inline static int32_t get_offset_of_fnkind_4() { return static_cast<int32_t>(offsetof(UnaOpSig_t66B0230ED69A79A4288B6C38326A1A1EB4E44AB1, ___fnkind_4)); }
	inline int32_t get_fnkind_4() const { return ___fnkind_4; }
	inline int32_t* get_address_of_fnkind_4() { return &___fnkind_4; }
	inline void set_fnkind_4(int32_t value)
	{
		___fnkind_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNAOPSIG_T66B0230ED69A79A4288B6C38326A1A1EB4E44AB1_H
#ifndef OPERATORINFO_T3F4B81310F0F4ADCDC95EAA974BADAC5A40FAFBF_H
#define OPERATORINFO_T3F4B81310F0F4ADCDC95EAA974BADAC5A40FAFBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.Operators/OperatorInfo
struct  OperatorInfo_t3F4B81310F0F4ADCDC95EAA974BADAC5A40FAFBF  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Syntax.TokenKind Microsoft.CSharp.RuntimeBinder.Semantics.Operators/OperatorInfo::TokenKind
	uint8_t ___TokenKind_0;
	// Microsoft.CSharp.RuntimeBinder.Syntax.PredefinedName Microsoft.CSharp.RuntimeBinder.Semantics.Operators/OperatorInfo::MethodName
	int32_t ___MethodName_1;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionKind Microsoft.CSharp.RuntimeBinder.Semantics.Operators/OperatorInfo::ExpressionKind
	int32_t ___ExpressionKind_2;

public:
	inline static int32_t get_offset_of_TokenKind_0() { return static_cast<int32_t>(offsetof(OperatorInfo_t3F4B81310F0F4ADCDC95EAA974BADAC5A40FAFBF, ___TokenKind_0)); }
	inline uint8_t get_TokenKind_0() const { return ___TokenKind_0; }
	inline uint8_t* get_address_of_TokenKind_0() { return &___TokenKind_0; }
	inline void set_TokenKind_0(uint8_t value)
	{
		___TokenKind_0 = value;
	}

	inline static int32_t get_offset_of_MethodName_1() { return static_cast<int32_t>(offsetof(OperatorInfo_t3F4B81310F0F4ADCDC95EAA974BADAC5A40FAFBF, ___MethodName_1)); }
	inline int32_t get_MethodName_1() const { return ___MethodName_1; }
	inline int32_t* get_address_of_MethodName_1() { return &___MethodName_1; }
	inline void set_MethodName_1(int32_t value)
	{
		___MethodName_1 = value;
	}

	inline static int32_t get_offset_of_ExpressionKind_2() { return static_cast<int32_t>(offsetof(OperatorInfo_t3F4B81310F0F4ADCDC95EAA974BADAC5A40FAFBF, ___ExpressionKind_2)); }
	inline int32_t get_ExpressionKind_2() const { return ___ExpressionKind_2; }
	inline int32_t* get_address_of_ExpressionKind_2() { return &___ExpressionKind_2; }
	inline void set_ExpressionKind_2(int32_t value)
	{
		___ExpressionKind_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATORINFO_T3F4B81310F0F4ADCDC95EAA974BADAC5A40FAFBF_H
#ifndef SYMBOL_T1F71FF12CC2541A1C59524E642D827284E8E67EA_H
#define SYMBOL_T1F71FF12CC2541A1C59524E642D827284E8E67EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.Symbol
struct  Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA  : public RuntimeObject
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.SYMKIND Microsoft.CSharp.RuntimeBinder.Semantics.Symbol::_kind
	int32_t ____kind_0;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ACCESS Microsoft.CSharp.RuntimeBinder.Semantics.Symbol::_access
	int32_t ____access_1;
	// Microsoft.CSharp.RuntimeBinder.Syntax.Name Microsoft.CSharp.RuntimeBinder.Semantics.Symbol::name
	Name_t95244514D054D80B6614F5E2275EC034F1527725 * ___name_2;
	// Microsoft.CSharp.RuntimeBinder.Semantics.ParentSymbol Microsoft.CSharp.RuntimeBinder.Semantics.Symbol::parent
	ParentSymbol_t5CA9C0921CCD816CF7FCF2D10B913499758EA593 * ___parent_3;
	// Microsoft.CSharp.RuntimeBinder.Semantics.Symbol Microsoft.CSharp.RuntimeBinder.Semantics.Symbol::nextChild
	Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA * ___nextChild_4;
	// Microsoft.CSharp.RuntimeBinder.Semantics.Symbol Microsoft.CSharp.RuntimeBinder.Semantics.Symbol::nextSameName
	Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA * ___nextSameName_5;

public:
	inline static int32_t get_offset_of__kind_0() { return static_cast<int32_t>(offsetof(Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA, ____kind_0)); }
	inline int32_t get__kind_0() const { return ____kind_0; }
	inline int32_t* get_address_of__kind_0() { return &____kind_0; }
	inline void set__kind_0(int32_t value)
	{
		____kind_0 = value;
	}

	inline static int32_t get_offset_of__access_1() { return static_cast<int32_t>(offsetof(Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA, ____access_1)); }
	inline int32_t get__access_1() const { return ____access_1; }
	inline int32_t* get_address_of__access_1() { return &____access_1; }
	inline void set__access_1(int32_t value)
	{
		____access_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA, ___name_2)); }
	inline Name_t95244514D054D80B6614F5E2275EC034F1527725 * get_name_2() const { return ___name_2; }
	inline Name_t95244514D054D80B6614F5E2275EC034F1527725 ** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(Name_t95244514D054D80B6614F5E2275EC034F1527725 * value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA, ___parent_3)); }
	inline ParentSymbol_t5CA9C0921CCD816CF7FCF2D10B913499758EA593 * get_parent_3() const { return ___parent_3; }
	inline ParentSymbol_t5CA9C0921CCD816CF7FCF2D10B913499758EA593 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(ParentSymbol_t5CA9C0921CCD816CF7FCF2D10B913499758EA593 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier((&___parent_3), value);
	}

	inline static int32_t get_offset_of_nextChild_4() { return static_cast<int32_t>(offsetof(Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA, ___nextChild_4)); }
	inline Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA * get_nextChild_4() const { return ___nextChild_4; }
	inline Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA ** get_address_of_nextChild_4() { return &___nextChild_4; }
	inline void set_nextChild_4(Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA * value)
	{
		___nextChild_4 = value;
		Il2CppCodeGenWriteBarrier((&___nextChild_4), value);
	}

	inline static int32_t get_offset_of_nextSameName_5() { return static_cast<int32_t>(offsetof(Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA, ___nextSameName_5)); }
	inline Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA * get_nextSameName_5() const { return ___nextSameName_5; }
	inline Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA ** get_address_of_nextSameName_5() { return &___nextSameName_5; }
	inline void set_nextSameName_5(Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA * value)
	{
		___nextSameName_5 = value;
		Il2CppCodeGenWriteBarrier((&___nextSameName_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMBOL_T1F71FF12CC2541A1C59524E642D827284E8E67EA_H
#ifndef BINARYOPERATIONBINDER_T62EB955B6CBE8A50ADDD56ABF893CDA60319FC62_H
#define BINARYOPERATIONBINDER_T62EB955B6CBE8A50ADDD56ABF893CDA60319FC62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BinaryOperationBinder
struct  BinaryOperationBinder_t62EB955B6CBE8A50ADDD56ABF893CDA60319FC62  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// System.Linq.Expressions.ExpressionType System.Dynamic.BinaryOperationBinder::<Operation>k__BackingField
	int32_t ___U3COperationU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3COperationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BinaryOperationBinder_t62EB955B6CBE8A50ADDD56ABF893CDA60319FC62, ___U3COperationU3Ek__BackingField_2)); }
	inline int32_t get_U3COperationU3Ek__BackingField_2() const { return ___U3COperationU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3COperationU3Ek__BackingField_2() { return &___U3COperationU3Ek__BackingField_2; }
	inline void set_U3COperationU3Ek__BackingField_2(int32_t value)
	{
		___U3COperationU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYOPERATIONBINDER_T62EB955B6CBE8A50ADDD56ABF893CDA60319FC62_H
#ifndef UNARYOPERATIONBINDER_T19843F0B998806F5A61BA545DC09511ECECB65BF_H
#define UNARYOPERATIONBINDER_T19843F0B998806F5A61BA545DC09511ECECB65BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.UnaryOperationBinder
struct  UnaryOperationBinder_t19843F0B998806F5A61BA545DC09511ECECB65BF  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// System.Linq.Expressions.ExpressionType System.Dynamic.UnaryOperationBinder::<Operation>k__BackingField
	int32_t ___U3COperationU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3COperationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnaryOperationBinder_t19843F0B998806F5A61BA545DC09511ECECB65BF, ___U3COperationU3Ek__BackingField_2)); }
	inline int32_t get_U3COperationU3Ek__BackingField_2() const { return ___U3COperationU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3COperationU3Ek__BackingField_2() { return &___U3COperationU3Ek__BackingField_2; }
	inline void set_U3COperationU3Ek__BackingField_2(int32_t value)
	{
		___U3COperationU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNARYOPERATIONBINDER_T19843F0B998806F5A61BA545DC09511ECECB65BF_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef DOWNLOADPROGRESSHANDLER_T9F6C1F118EC7695FC511C7DD4B25EEFBDA7C1B9F_H
#define DOWNLOADPROGRESSHANDLER_T9F6C1F118EC7695FC511C7DD4B25EEFBDA7C1B9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHLab.PATCH.Downloader.DownloadProgressHandler
struct  DownloadProgressHandler_t9F6C1F118EC7695FC511C7DD4B25EEFBDA7C1B9F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADPROGRESSHANDLER_T9F6C1F118EC7695FC511C7DD4B25EEFBDA7C1B9F_H
#ifndef CSHARPBINARYOPERATIONBINDER_TA65105D67E0480E5D7455613052C32F850B92A7A_H
#define CSHARPBINARYOPERATIONBINDER_TA65105D67E0480E5D7455613052C32F850B92A7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.CSharpBinaryOperationBinder
struct  CSharpBinaryOperationBinder_tA65105D67E0480E5D7455613052C32F850B92A7A  : public BinaryOperationBinder_t62EB955B6CBE8A50ADDD56ABF893CDA60319FC62
{
public:
	// System.Boolean Microsoft.CSharp.RuntimeBinder.CSharpBinaryOperationBinder::<IsChecked>k__BackingField
	bool ___U3CIsCheckedU3Ek__BackingField_3;
	// Microsoft.CSharp.RuntimeBinder.CSharpBinaryOperationFlags Microsoft.CSharp.RuntimeBinder.CSharpBinaryOperationBinder::_binopFlags
	int32_t ____binopFlags_4;
	// System.Type Microsoft.CSharp.RuntimeBinder.CSharpBinaryOperationBinder::<CallingContext>k__BackingField
	Type_t * ___U3CCallingContextU3Ek__BackingField_5;
	// Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo[] Microsoft.CSharp.RuntimeBinder.CSharpBinaryOperationBinder::_argumentInfo
	CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* ____argumentInfo_6;
	// Microsoft.CSharp.RuntimeBinder.RuntimeBinder Microsoft.CSharp.RuntimeBinder.CSharpBinaryOperationBinder::_binder
	RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * ____binder_7;

public:
	inline static int32_t get_offset_of_U3CIsCheckedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CSharpBinaryOperationBinder_tA65105D67E0480E5D7455613052C32F850B92A7A, ___U3CIsCheckedU3Ek__BackingField_3)); }
	inline bool get_U3CIsCheckedU3Ek__BackingField_3() const { return ___U3CIsCheckedU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsCheckedU3Ek__BackingField_3() { return &___U3CIsCheckedU3Ek__BackingField_3; }
	inline void set_U3CIsCheckedU3Ek__BackingField_3(bool value)
	{
		___U3CIsCheckedU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of__binopFlags_4() { return static_cast<int32_t>(offsetof(CSharpBinaryOperationBinder_tA65105D67E0480E5D7455613052C32F850B92A7A, ____binopFlags_4)); }
	inline int32_t get__binopFlags_4() const { return ____binopFlags_4; }
	inline int32_t* get_address_of__binopFlags_4() { return &____binopFlags_4; }
	inline void set__binopFlags_4(int32_t value)
	{
		____binopFlags_4 = value;
	}

	inline static int32_t get_offset_of_U3CCallingContextU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CSharpBinaryOperationBinder_tA65105D67E0480E5D7455613052C32F850B92A7A, ___U3CCallingContextU3Ek__BackingField_5)); }
	inline Type_t * get_U3CCallingContextU3Ek__BackingField_5() const { return ___U3CCallingContextU3Ek__BackingField_5; }
	inline Type_t ** get_address_of_U3CCallingContextU3Ek__BackingField_5() { return &___U3CCallingContextU3Ek__BackingField_5; }
	inline void set_U3CCallingContextU3Ek__BackingField_5(Type_t * value)
	{
		___U3CCallingContextU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallingContextU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of__argumentInfo_6() { return static_cast<int32_t>(offsetof(CSharpBinaryOperationBinder_tA65105D67E0480E5D7455613052C32F850B92A7A, ____argumentInfo_6)); }
	inline CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* get__argumentInfo_6() const { return ____argumentInfo_6; }
	inline CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103** get_address_of__argumentInfo_6() { return &____argumentInfo_6; }
	inline void set__argumentInfo_6(CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* value)
	{
		____argumentInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&____argumentInfo_6), value);
	}

	inline static int32_t get_offset_of__binder_7() { return static_cast<int32_t>(offsetof(CSharpBinaryOperationBinder_tA65105D67E0480E5D7455613052C32F850B92A7A, ____binder_7)); }
	inline RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * get__binder_7() const { return ____binder_7; }
	inline RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 ** get_address_of__binder_7() { return &____binder_7; }
	inline void set__binder_7(RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * value)
	{
		____binder_7 = value;
		Il2CppCodeGenWriteBarrier((&____binder_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHARPBINARYOPERATIONBINDER_TA65105D67E0480E5D7455613052C32F850B92A7A_H
#ifndef CSHARPUNARYOPERATIONBINDER_TD7C89C448BD1C4D50D848B8E644FACFECA21C2EC_H
#define CSHARPUNARYOPERATIONBINDER_TD7C89C448BD1C4D50D848B8E644FACFECA21C2EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.CSharpUnaryOperationBinder
struct  CSharpUnaryOperationBinder_tD7C89C448BD1C4D50D848B8E644FACFECA21C2EC  : public UnaryOperationBinder_t19843F0B998806F5A61BA545DC09511ECECB65BF
{
public:
	// System.Boolean Microsoft.CSharp.RuntimeBinder.CSharpUnaryOperationBinder::<IsChecked>k__BackingField
	bool ___U3CIsCheckedU3Ek__BackingField_3;
	// System.Type Microsoft.CSharp.RuntimeBinder.CSharpUnaryOperationBinder::<CallingContext>k__BackingField
	Type_t * ___U3CCallingContextU3Ek__BackingField_4;
	// Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo[] Microsoft.CSharp.RuntimeBinder.CSharpUnaryOperationBinder::_argumentInfo
	CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* ____argumentInfo_5;
	// Microsoft.CSharp.RuntimeBinder.RuntimeBinder Microsoft.CSharp.RuntimeBinder.CSharpUnaryOperationBinder::_binder
	RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * ____binder_6;

public:
	inline static int32_t get_offset_of_U3CIsCheckedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CSharpUnaryOperationBinder_tD7C89C448BD1C4D50D848B8E644FACFECA21C2EC, ___U3CIsCheckedU3Ek__BackingField_3)); }
	inline bool get_U3CIsCheckedU3Ek__BackingField_3() const { return ___U3CIsCheckedU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsCheckedU3Ek__BackingField_3() { return &___U3CIsCheckedU3Ek__BackingField_3; }
	inline void set_U3CIsCheckedU3Ek__BackingField_3(bool value)
	{
		___U3CIsCheckedU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CCallingContextU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CSharpUnaryOperationBinder_tD7C89C448BD1C4D50D848B8E644FACFECA21C2EC, ___U3CCallingContextU3Ek__BackingField_4)); }
	inline Type_t * get_U3CCallingContextU3Ek__BackingField_4() const { return ___U3CCallingContextU3Ek__BackingField_4; }
	inline Type_t ** get_address_of_U3CCallingContextU3Ek__BackingField_4() { return &___U3CCallingContextU3Ek__BackingField_4; }
	inline void set_U3CCallingContextU3Ek__BackingField_4(Type_t * value)
	{
		___U3CCallingContextU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallingContextU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of__argumentInfo_5() { return static_cast<int32_t>(offsetof(CSharpUnaryOperationBinder_tD7C89C448BD1C4D50D848B8E644FACFECA21C2EC, ____argumentInfo_5)); }
	inline CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* get__argumentInfo_5() const { return ____argumentInfo_5; }
	inline CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103** get_address_of__argumentInfo_5() { return &____argumentInfo_5; }
	inline void set__argumentInfo_5(CSharpArgumentInfoU5BU5D_tD8F48E9E44E12A685FE16E5BBB5243F6F309A103* value)
	{
		____argumentInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&____argumentInfo_5), value);
	}

	inline static int32_t get_offset_of__binder_6() { return static_cast<int32_t>(offsetof(CSharpUnaryOperationBinder_tD7C89C448BD1C4D50D848B8E644FACFECA21C2EC, ____binder_6)); }
	inline RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * get__binder_6() const { return ____binder_6; }
	inline RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 ** get_address_of__binder_6() { return &____binder_6; }
	inline void set__binder_6(RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03 * value)
	{
		____binder_6 = value;
		Il2CppCodeGenWriteBarrier((&____binder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHARPUNARYOPERATIONBINDER_TD7C89C448BD1C4D50D848B8E644FACFECA21C2EC_H
#ifndef EXPRESSIONEXPR_T76ED44FDB900D5FBE6BC31968A88B628D0D0C2C2_H
#define EXPRESSIONEXPR_T76ED44FDB900D5FBE6BC31968A88B628D0D0C2C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.ExpressionTreeCallRewriter/ExpressionExpr
struct  ExpressionExpr_t76ED44FDB900D5FBE6BC31968A88B628D0D0C2C2  : public Expr_t0ADDBD5077E06B490FF85514AC6FC3C879FD07E5
{
public:
	// System.Linq.Expressions.Expression Microsoft.CSharp.RuntimeBinder.ExpressionTreeCallRewriter/ExpressionExpr::Expression
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___Expression_8;

public:
	inline static int32_t get_offset_of_Expression_8() { return static_cast<int32_t>(offsetof(ExpressionExpr_t76ED44FDB900D5FBE6BC31968A88B628D0D0C2C2, ___Expression_8)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get_Expression_8() const { return ___Expression_8; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of_Expression_8() { return &___Expression_8; }
	inline void set_Expression_8(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		___Expression_8 = value;
		Il2CppCodeGenWriteBarrier((&___Expression_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONEXPR_T76ED44FDB900D5FBE6BC31968A88B628D0D0C2C2_H
#ifndef BINOPFULLSIG_T004AF42FF106C3A5FB62DF0EE8CCF68546B2A7F5_H
#define BINOPFULLSIG_T004AF42FF106C3A5FB62DF0EE8CCF68546B2A7F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpFullSig
struct  BinOpFullSig_t004AF42FF106C3A5FB62DF0EE8CCF68546B2A7F5  : public BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.LiftFlags Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpFullSig::_grflt
	int32_t ____grflt_7;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpFullSig::_type1
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ____type1_8;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/BinOpFullSig::_type2
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ____type2_9;

public:
	inline static int32_t get_offset_of__grflt_7() { return static_cast<int32_t>(offsetof(BinOpFullSig_t004AF42FF106C3A5FB62DF0EE8CCF68546B2A7F5, ____grflt_7)); }
	inline int32_t get__grflt_7() const { return ____grflt_7; }
	inline int32_t* get_address_of__grflt_7() { return &____grflt_7; }
	inline void set__grflt_7(int32_t value)
	{
		____grflt_7 = value;
	}

	inline static int32_t get_offset_of__type1_8() { return static_cast<int32_t>(offsetof(BinOpFullSig_t004AF42FF106C3A5FB62DF0EE8CCF68546B2A7F5, ____type1_8)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get__type1_8() const { return ____type1_8; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of__type1_8() { return &____type1_8; }
	inline void set__type1_8(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		____type1_8 = value;
		Il2CppCodeGenWriteBarrier((&____type1_8), value);
	}

	inline static int32_t get_offset_of__type2_9() { return static_cast<int32_t>(offsetof(BinOpFullSig_t004AF42FF106C3A5FB62DF0EE8CCF68546B2A7F5, ____type2_9)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get__type2_9() const { return ____type2_9; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of__type2_9() { return &____type2_9; }
	inline void set__type2_9(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		____type2_9 = value;
		Il2CppCodeGenWriteBarrier((&____type2_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINOPFULLSIG_T004AF42FF106C3A5FB62DF0EE8CCF68546B2A7F5_H
#ifndef CONVERSIONFUNC_TFA8AAED736762954309492560704CBC4FEEBC1D8_H
#define CONVERSIONFUNC_TFA8AAED736762954309492560704CBC4FEEBC1D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/ConversionFunc
struct  ConversionFunc_tFA8AAED736762954309492560704CBC4FEEBC1D8  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERSIONFUNC_TFA8AAED736762954309492560704CBC4FEEBC1D8_H
#ifndef PFNBINDBINOP_T376819D18187F697A9950060805897A8BF537F68_H
#define PFNBINDBINOP_T376819D18187F697A9950060805897A8BF537F68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/PfnBindBinOp
struct  PfnBindBinOp_t376819D18187F697A9950060805897A8BF537F68  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PFNBINDBINOP_T376819D18187F697A9950060805897A8BF537F68_H
#ifndef PFNBINDUNAOP_TF887D64C6A968258E18C38389610C6E1E70E6944_H
#define PFNBINDUNAOP_TF887D64C6A968258E18C38389610C6E1E70E6944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/PfnBindUnaOp
struct  PfnBindUnaOp_tF887D64C6A968258E18C38389610C6E1E70E6944  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PFNBINDUNAOP_TF887D64C6A968258E18C38389610C6E1E70E6944_H
#ifndef UNAOPFULLSIG_T3BB590F0D7FEE3BE56C22C87919B27471A631E11_H
#define UNAOPFULLSIG_T3BB590F0D7FEE3BE56C22C87919B27471A631E11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/UnaOpFullSig
struct  UnaOpFullSig_t3BB590F0D7FEE3BE56C22C87919B27471A631E11  : public UnaOpSig_t66B0230ED69A79A4288B6C38326A1A1EB4E44AB1
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.LiftFlags Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/UnaOpFullSig::_grflt
	int32_t ____grflt_5;
	// Microsoft.CSharp.RuntimeBinder.Semantics.CType Microsoft.CSharp.RuntimeBinder.Semantics.ExpressionBinder/UnaOpFullSig::_type
	CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * ____type_6;

public:
	inline static int32_t get_offset_of__grflt_5() { return static_cast<int32_t>(offsetof(UnaOpFullSig_t3BB590F0D7FEE3BE56C22C87919B27471A631E11, ____grflt_5)); }
	inline int32_t get__grflt_5() const { return ____grflt_5; }
	inline int32_t* get_address_of__grflt_5() { return &____grflt_5; }
	inline void set__grflt_5(int32_t value)
	{
		____grflt_5 = value;
	}

	inline static int32_t get_offset_of__type_6() { return static_cast<int32_t>(offsetof(UnaOpFullSig_t3BB590F0D7FEE3BE56C22C87919B27471A631E11, ____type_6)); }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * get__type_6() const { return ____type_6; }
	inline CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA ** get_address_of__type_6() { return &____type_6; }
	inline void set__type_6(CType_t8F0CF03E2B0108192E3E5364146816EB230B3BDA * value)
	{
		____type_6 = value;
		Il2CppCodeGenWriteBarrier((&____type_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNAOPFULLSIG_T3BB590F0D7FEE3BE56C22C87919B27471A631E11_H
#ifndef PARENTSYMBOL_T5CA9C0921CCD816CF7FCF2D10B913499758EA593_H
#define PARENTSYMBOL_T5CA9C0921CCD816CF7FCF2D10B913499758EA593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.ParentSymbol
struct  ParentSymbol_t5CA9C0921CCD816CF7FCF2D10B913499758EA593  : public Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.Symbol Microsoft.CSharp.RuntimeBinder.Semantics.ParentSymbol::firstChild
	Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA * ___firstChild_6;
	// Microsoft.CSharp.RuntimeBinder.Semantics.Symbol Microsoft.CSharp.RuntimeBinder.Semantics.ParentSymbol::_lastChild
	Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA * ____lastChild_7;

public:
	inline static int32_t get_offset_of_firstChild_6() { return static_cast<int32_t>(offsetof(ParentSymbol_t5CA9C0921CCD816CF7FCF2D10B913499758EA593, ___firstChild_6)); }
	inline Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA * get_firstChild_6() const { return ___firstChild_6; }
	inline Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA ** get_address_of_firstChild_6() { return &___firstChild_6; }
	inline void set_firstChild_6(Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA * value)
	{
		___firstChild_6 = value;
		Il2CppCodeGenWriteBarrier((&___firstChild_6), value);
	}

	inline static int32_t get_offset_of__lastChild_7() { return static_cast<int32_t>(offsetof(ParentSymbol_t5CA9C0921CCD816CF7FCF2D10B913499758EA593, ____lastChild_7)); }
	inline Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA * get__lastChild_7() const { return ____lastChild_7; }
	inline Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA ** get_address_of__lastChild_7() { return &____lastChild_7; }
	inline void set__lastChild_7(Symbol_t1F71FF12CC2541A1C59524E642D827284E8E67EA * value)
	{
		____lastChild_7 = value;
		Il2CppCodeGenWriteBarrier((&____lastChild_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARENTSYMBOL_T5CA9C0921CCD816CF7FCF2D10B913499758EA593_H
#ifndef AGGREGATEDECLARATION_T1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5_H
#define AGGREGATEDECLARATION_T1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CSharp.RuntimeBinder.Semantics.AggregateDeclaration
struct  AggregateDeclaration_t1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5  : public ParentSymbol_t5CA9C0921CCD816CF7FCF2D10B913499758EA593
{
public:
	// Microsoft.CSharp.RuntimeBinder.Semantics.NamespaceOrAggregateSymbol Microsoft.CSharp.RuntimeBinder.Semantics.AggregateDeclaration::bag
	NamespaceOrAggregateSymbol_t1B1F54E58D34F283E61D89E7703F9E56A1248357 * ___bag_8;
	// Microsoft.CSharp.RuntimeBinder.Semantics.AggregateDeclaration Microsoft.CSharp.RuntimeBinder.Semantics.AggregateDeclaration::declNext
	AggregateDeclaration_t1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5 * ___declNext_9;

public:
	inline static int32_t get_offset_of_bag_8() { return static_cast<int32_t>(offsetof(AggregateDeclaration_t1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5, ___bag_8)); }
	inline NamespaceOrAggregateSymbol_t1B1F54E58D34F283E61D89E7703F9E56A1248357 * get_bag_8() const { return ___bag_8; }
	inline NamespaceOrAggregateSymbol_t1B1F54E58D34F283E61D89E7703F9E56A1248357 ** get_address_of_bag_8() { return &___bag_8; }
	inline void set_bag_8(NamespaceOrAggregateSymbol_t1B1F54E58D34F283E61D89E7703F9E56A1248357 * value)
	{
		___bag_8 = value;
		Il2CppCodeGenWriteBarrier((&___bag_8), value);
	}

	inline static int32_t get_offset_of_declNext_9() { return static_cast<int32_t>(offsetof(AggregateDeclaration_t1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5, ___declNext_9)); }
	inline AggregateDeclaration_t1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5 * get_declNext_9() const { return ___declNext_9; }
	inline AggregateDeclaration_t1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5 ** get_address_of_declNext_9() { return &___declNext_9; }
	inline void set_declNext_9(AggregateDeclaration_t1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5 * value)
	{
		___declNext_9 = value;
		Il2CppCodeGenWriteBarrier((&___declNext_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGREGATEDECLARATION_T1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5800 = { sizeof (SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8), -1, sizeof(SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5800[44] = 
{
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_APP_PATH_0(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_CURRENT_BUILD_PATH_1(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_BUILDS_PATH_2(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_PATCHES_PATH_3(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_SIGNATURES_PATH_4(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_FINAL_PATCHES_PATH_5(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_DEPLOY_PATH_6(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_PATCHER_FILES_PATH_7(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_LOGS_ERROR_PATH_8(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_PATCHES_TMP_FOLDER_9(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_LAUNCHER_CONFIG_GENERATION_PATH_10(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_PATCHES_SYMBOL_SEPARATOR_11(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_PATCH_VERSION_ENCRYPTION_PASSWORD_12(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_PATCH_EXTENSION_13(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_PATCH_DELETE_FILE_EXTENSION_14(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_PATCH_VERSION_PATH_15(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_VERSIONS_FILE_DOWNLOAD_URL_16(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_PATCHES_DOWNLOAD_URL_17(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_BUILDS_DOWNLOAD_URL_18(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_PATCHER_DOWNLOAD_URL_19(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_PATCH_DOWNLOAD_RETRY_ATTEMPTS_20(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_FILE_DELETE_RETRY_ATTEMPTS_21(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_LAUNCH_APP_22(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_LAUNCHER_NAME_23(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_LAUNCH_ARG_24(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_LAUNCH_COMMAND_25(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_USE_RAW_LAUNCH_ARG_26(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_INSTALL_IN_LOCAL_PATH_27(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_PROGRAM_FILES_DIRECTORY_TO_INSTALL_28(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_PROGRAM_FILES_DIRECTORY_TO_INSTALL_ABS_PATH_29(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_LAUNCHER_CONFIG_PATH_30(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_PATCH_SAFE_BACKUP_31(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_VERSION_FILE_LOCAL_PATH_32(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_ENABLE_FTP_33(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_FTP_USERNAME_34(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_FTP_PASSWORD_35(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_ENABLE_PATCHER_36(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_ENABLE_INSTALLER_37(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_ENABLE_REPAIRER_38(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_INSTALL_PATCHER_39(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_CREATE_DESKTOP_SHORTCUT_40(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_PATCHNOTES_URL_41(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_NEWS_URL_42(),
	SettingsManager_t617C17387413F4365C1732CCD3F763A86B9230B8_StaticFields::get_offset_of_DOWNLOAD_BUFFER_SIZE_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5801 = { sizeof (SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5801[21] = 
{
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_VERSIONS_FILE_DOWNLOAD_URL_0(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_PATCHES_DOWNLOAD_URL_1(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_BUILDS_DOWNLOAD_URL_2(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_PATCHER_DOWNLOAD_URL_3(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_PATCH_DOWNLOAD_RETRY_ATTEMPTS_4(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_PATCHNOTES_URL_5(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_NEWS_URL_6(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_LAUNCH_APP_7(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_LAUNCHER_NAME_8(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_LAUNCH_ARG_9(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_USE_RAW_LAUNCH_10(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_ENABLE_FTP_11(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_FTP_USERNAME_12(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_FTP_PASSWORD_13(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_INSTALL_IN_LOCAL_PATH_14(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_PROGRAM_FILES_DIRECTORY_TO_INSTALL_15(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_ENABLE_PATCHER_16(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_ENABLE_INSTALLER_17(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_ENABLE_REPAIRER_18(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_INSTALL_PATCHER_19(),
	SettingsOverrider_t496BC9E1C938ADBDAD4E09FB6FA35BCCB6703D3C::get_offset_of_CREATE_DESKTOP_SHORTCUT_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5802 = { sizeof (InstallationState_tC3C4D9DDE2082A9B86E5C0BB46D1C637A08A2AD6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5802[4] = 
{
	InstallationState_tC3C4D9DDE2082A9B86E5C0BB46D1C637A08A2AD6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5803 = { sizeof (Installer_t4AA6D9961392D2013E285491910531239D5A2932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5803[15] = 
{
	Installer_t4AA6D9961392D2013E285491910531239D5A2932::get_offset_of_availableBuilds_0(),
	Installer_t4AA6D9961392D2013E285491910531239D5A2932::get_offset_of_OnFileProcessed_1(),
	Installer_t4AA6D9961392D2013E285491910531239D5A2932::get_offset_of_OnFileProcessing_2(),
	Installer_t4AA6D9961392D2013E285491910531239D5A2932::get_offset_of_OnLog_3(),
	Installer_t4AA6D9961392D2013E285491910531239D5A2932::get_offset_of_OnError_4(),
	Installer_t4AA6D9961392D2013E285491910531239D5A2932::get_offset_of_OnFatalError_5(),
	Installer_t4AA6D9961392D2013E285491910531239D5A2932::get_offset_of_OnTaskStarted_6(),
	Installer_t4AA6D9961392D2013E285491910531239D5A2932::get_offset_of_OnTaskCompleted_7(),
	Installer_t4AA6D9961392D2013E285491910531239D5A2932::get_offset_of_OnSetMainProgressBar_8(),
	Installer_t4AA6D9961392D2013E285491910531239D5A2932::get_offset_of_OnSetDetailProgressBar_9(),
	Installer_t4AA6D9961392D2013E285491910531239D5A2932::get_offset_of_OnIncreaseMainProgressBar_10(),
	Installer_t4AA6D9961392D2013E285491910531239D5A2932::get_offset_of_OnIncreaseDetailProgressBar_11(),
	Installer_t4AA6D9961392D2013E285491910531239D5A2932::get_offset_of_OnDownloadProgress_12(),
	Installer_t4AA6D9961392D2013E285491910531239D5A2932::get_offset_of_OnDownloadCompleted_13(),
	Installer_t4AA6D9961392D2013E285491910531239D5A2932::get_offset_of_downloader_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5804 = { sizeof (U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC), -1, sizeof(U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5804[15] = 
{
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields::get_offset_of_U3CU3E9__14_0_1(),
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields::get_offset_of_U3CU3E9__14_1_2(),
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields::get_offset_of_U3CU3E9__14_2_3(),
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields::get_offset_of_U3CU3E9__14_3_4(),
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields::get_offset_of_U3CU3E9__14_4_5(),
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields::get_offset_of_U3CU3E9__14_5_6(),
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields::get_offset_of_U3CU3E9__14_6_7(),
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields::get_offset_of_U3CU3E9__14_7_8(),
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields::get_offset_of_U3CU3E9__14_8_9(),
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields::get_offset_of_U3CU3E9__14_9_10(),
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields::get_offset_of_U3CU3E9__14_10_11(),
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields::get_offset_of_U3CU3E9__14_11_12(),
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields::get_offset_of_U3CU3E9__14_12_13(),
	U3CU3Ec_tE05096997AC3C296BA928EE56F3E5365ADB8B8DC_StaticFields::get_offset_of_U3CU3E9__24_0_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5805 = { sizeof (InstallManager_t6A16071C2DDC3B23EF861B128B2E2EC152A30990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5805[2] = 
{
	InstallManager_t6A16071C2DDC3B23EF861B128B2E2EC152A30990::get_offset_of_m_installer_0(),
	InstallManager_t6A16071C2DDC3B23EF861B128B2E2EC152A30990::get_offset_of_SETTINGS_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5806 = { sizeof (FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5806[6] = 
{
	FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8::get_offset_of_downloadBlockSize_0(),
	FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8::get_offset_of_canceled_1(),
	FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8::get_offset_of_downloadingTo_2(),
	FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8::get_offset_of_ProgressChanged_3(),
	FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8::get_offset_of_proxy_4(),
	FileDownloader_tEEDF59D489CB432A2B34B5C7765CFF6EF91444D8::get_offset_of_DownloadComplete_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5807 = { sizeof (DownloadData_tEB2C132BB12298D726F8840BDE898573130365C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5807[5] = 
{
	DownloadData_tEB2C132BB12298D726F8840BDE898573130365C0::get_offset_of_response_0(),
	DownloadData_tEB2C132BB12298D726F8840BDE898573130365C0::get_offset_of_stream_1(),
	DownloadData_tEB2C132BB12298D726F8840BDE898573130365C0::get_offset_of_size_2(),
	DownloadData_tEB2C132BB12298D726F8840BDE898573130365C0::get_offset_of_start_3(),
	DownloadData_tEB2C132BB12298D726F8840BDE898573130365C0::get_offset_of_proxy_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5808 = { sizeof (DownloadEventArgs_tE8931C7F6C9F12BB6E657D9F11491CD22D94BC6A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5808[3] = 
{
	DownloadEventArgs_tE8931C7F6C9F12BB6E657D9F11491CD22D94BC6A::get_offset_of_percentDone_1(),
	DownloadEventArgs_tE8931C7F6C9F12BB6E657D9F11491CD22D94BC6A::get_offset_of_totalFileSize_2(),
	DownloadEventArgs_tE8931C7F6C9F12BB6E657D9F11491CD22D94BC6A::get_offset_of_currentFileSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5809 = { sizeof (DownloadProgressHandler_t9F6C1F118EC7695FC511C7DD4B25EEFBDA7C1B9F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5810 = { sizeof (Debugger_t1E0CE4A97B210FE0CCBA830D22EA6D0769519A70), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5811 = { sizeof (CompressionType_tFD4585547C8DA57E6AEE2FCC7CA7BD0471795803)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5811[4] = 
{
	CompressionType_tFD4585547C8DA57E6AEE2FCC7CA7BD0471795803::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5812 = { sizeof (Compressor_t40F613A221FDE851F69B704F2E9BF0D600C799F3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5813 = { sizeof (ZIPCompressor_tC0AD4EADD989CDA689C2686C91CE1385B3873CC5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5814 = { sizeof (TARGZCompressor_t89910D0FCADAC20E2E9C4F8C6D24063F8DD1FA7C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5815 = { sizeof (U3CModuleU3E_tC6BFFD591588D685E886F92456B1C92F17411C71), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5816 = { sizeof (SR_t3A884991BE6F1DADB1F0F35E8C244098C970521F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5817 = { sizeof (ArgumentObject_tDAA673E7623AFB02655105AA0B4752BBDBCBAF26)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5817[3] = 
{
	ArgumentObject_tDAA673E7623AFB02655105AA0B4752BBDBCBAF26::get_offset_of_Value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ArgumentObject_tDAA673E7623AFB02655105AA0B4752BBDBCBAF26::get_offset_of_Info_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ArgumentObject_tDAA673E7623AFB02655105AA0B4752BBDBCBAF26::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5818 = { sizeof (Binder_t70C25F53EDE3C42C319E9E60DB682372C467C757), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5819 = { sizeof (BinderHelper_t8AE8B534D7D7BCB8C34300486B365869813A9868), -1, sizeof(BinderHelper_t8AE8B534D7D7BCB8C34300486B365869813A9868_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5819[2] = 
{
	BinderHelper_t8AE8B534D7D7BCB8C34300486B365869813A9868_StaticFields::get_offset_of_s_DoubleIsNaN_0(),
	BinderHelper_t8AE8B534D7D7BCB8C34300486B365869813A9868_StaticFields::get_offset_of_s_SingleIsNaN_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5820 = { sizeof (CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4), -1, sizeof(CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5820[3] = 
{
	CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4_StaticFields::get_offset_of_None_0(),
	CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4::get_offset_of_U3CFlagsU3Ek__BackingField_1(),
	CSharpArgumentInfo_tDAC8F8D0657968A42B97C5885D10D7635206CDE4::get_offset_of_U3CNameU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5821 = { sizeof (CSharpArgumentInfoFlags_t990CD7029BB110AE2C8A0AFB0EC825B2DCF5B0AC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5821[8] = 
{
	CSharpArgumentInfoFlags_t990CD7029BB110AE2C8A0AFB0EC825B2DCF5B0AC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5822 = { sizeof (CSharpBinaryOperationBinder_tA65105D67E0480E5D7455613052C32F850B92A7A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5822[5] = 
{
	CSharpBinaryOperationBinder_tA65105D67E0480E5D7455613052C32F850B92A7A::get_offset_of_U3CIsCheckedU3Ek__BackingField_3(),
	CSharpBinaryOperationBinder_tA65105D67E0480E5D7455613052C32F850B92A7A::get_offset_of__binopFlags_4(),
	CSharpBinaryOperationBinder_tA65105D67E0480E5D7455613052C32F850B92A7A::get_offset_of_U3CCallingContextU3Ek__BackingField_5(),
	CSharpBinaryOperationBinder_tA65105D67E0480E5D7455613052C32F850B92A7A::get_offset_of__argumentInfo_6(),
	CSharpBinaryOperationBinder_tA65105D67E0480E5D7455613052C32F850B92A7A::get_offset_of__binder_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5823 = { sizeof (CSharpBinaryOperationFlags_t143FAC518313AB8F52837CD00B32FC180FEC12A6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5823[4] = 
{
	CSharpBinaryOperationFlags_t143FAC518313AB8F52837CD00B32FC180FEC12A6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5824 = { sizeof (CSharpBinderFlags_tCF49AAFD82EC095A895ACF3D49A7BF3C7A74DFFE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5824[11] = 
{
	CSharpBinderFlags_tCF49AAFD82EC095A895ACF3D49A7BF3C7A74DFFE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5825 = { sizeof (CSharpCallFlags_t280317F9DD5386E7CFE60814060132BFB841892C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5825[5] = 
{
	CSharpCallFlags_t280317F9DD5386E7CFE60814060132BFB841892C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5826 = { sizeof (CSharpConversionKind_t22B959F921CFC138419590A0A8D542F36CCF505F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5826[4] = 
{
	CSharpConversionKind_t22B959F921CFC138419590A0A8D542F36CCF505F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5827 = { sizeof (CSharpConvertBinder_t71E47849913F1A47C23CDF45508A5138018AC321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5827[4] = 
{
	CSharpConvertBinder_t71E47849913F1A47C23CDF45508A5138018AC321::get_offset_of_U3CConversionKindU3Ek__BackingField_4(),
	CSharpConvertBinder_t71E47849913F1A47C23CDF45508A5138018AC321::get_offset_of_U3CIsCheckedU3Ek__BackingField_5(),
	CSharpConvertBinder_t71E47849913F1A47C23CDF45508A5138018AC321::get_offset_of_U3CCallingContextU3Ek__BackingField_6(),
	CSharpConvertBinder_t71E47849913F1A47C23CDF45508A5138018AC321::get_offset_of__binder_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5828 = { sizeof (CSharpGetMemberBinder_t1C65C80D44E71EFF3DE84423690C4FEFBC34A15E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5828[4] = 
{
	CSharpGetMemberBinder_t1C65C80D44E71EFF3DE84423690C4FEFBC34A15E::get_offset_of_U3CCallingContextU3Ek__BackingField_4(),
	CSharpGetMemberBinder_t1C65C80D44E71EFF3DE84423690C4FEFBC34A15E::get_offset_of__argumentInfo_5(),
	CSharpGetMemberBinder_t1C65C80D44E71EFF3DE84423690C4FEFBC34A15E::get_offset_of_U3CResultIndexedU3Ek__BackingField_6(),
	CSharpGetMemberBinder_t1C65C80D44E71EFF3DE84423690C4FEFBC34A15E::get_offset_of__binder_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5829 = { sizeof (CSharpInvokeBinder_tC94A443E3999EF779EF0BC4BCBB85BDF8B6B96CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5829[4] = 
{
	CSharpInvokeBinder_tC94A443E3999EF779EF0BC4BCBB85BDF8B6B96CC::get_offset_of__flags_3(),
	CSharpInvokeBinder_tC94A443E3999EF779EF0BC4BCBB85BDF8B6B96CC::get_offset_of_U3CCallingContextU3Ek__BackingField_4(),
	CSharpInvokeBinder_tC94A443E3999EF779EF0BC4BCBB85BDF8B6B96CC::get_offset_of__argumentInfo_5(),
	CSharpInvokeBinder_tC94A443E3999EF779EF0BC4BCBB85BDF8B6B96CC::get_offset_of__binder_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5830 = { sizeof (CSharpInvokeConstructorBinder_tDA87B0CBCDF65D256B38B756C102CD667D6C6AC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5830[4] = 
{
	CSharpInvokeConstructorBinder_tDA87B0CBCDF65D256B38B756C102CD667D6C6AC1::get_offset_of_U3CFlagsU3Ek__BackingField_2(),
	CSharpInvokeConstructorBinder_tDA87B0CBCDF65D256B38B756C102CD667D6C6AC1::get_offset_of_U3CCallingContextU3Ek__BackingField_3(),
	CSharpInvokeConstructorBinder_tDA87B0CBCDF65D256B38B756C102CD667D6C6AC1::get_offset_of__argumentInfo_4(),
	CSharpInvokeConstructorBinder_tDA87B0CBCDF65D256B38B756C102CD667D6C6AC1::get_offset_of__binder_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5831 = { sizeof (CSharpInvokeMemberBinder_t1B43657AF71FD88DF2707CE5FB6B9FC9534ED244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5831[5] = 
{
	CSharpInvokeMemberBinder_t1B43657AF71FD88DF2707CE5FB6B9FC9534ED244::get_offset_of_U3CFlagsU3Ek__BackingField_4(),
	CSharpInvokeMemberBinder_t1B43657AF71FD88DF2707CE5FB6B9FC9534ED244::get_offset_of_U3CCallingContextU3Ek__BackingField_5(),
	CSharpInvokeMemberBinder_t1B43657AF71FD88DF2707CE5FB6B9FC9534ED244::get_offset_of_U3CTypeArgumentsU3Ek__BackingField_6(),
	CSharpInvokeMemberBinder_t1B43657AF71FD88DF2707CE5FB6B9FC9534ED244::get_offset_of__argumentInfo_7(),
	CSharpInvokeMemberBinder_t1B43657AF71FD88DF2707CE5FB6B9FC9534ED244::get_offset_of__binder_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5832 = { sizeof (CSharpUnaryOperationBinder_tD7C89C448BD1C4D50D848B8E644FACFECA21C2EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5832[4] = 
{
	CSharpUnaryOperationBinder_tD7C89C448BD1C4D50D848B8E644FACFECA21C2EC::get_offset_of_U3CIsCheckedU3Ek__BackingField_3(),
	CSharpUnaryOperationBinder_tD7C89C448BD1C4D50D848B8E644FACFECA21C2EC::get_offset_of_U3CCallingContextU3Ek__BackingField_4(),
	CSharpUnaryOperationBinder_tD7C89C448BD1C4D50D848B8E644FACFECA21C2EC::get_offset_of__argumentInfo_5(),
	CSharpUnaryOperationBinder_tD7C89C448BD1C4D50D848B8E644FACFECA21C2EC::get_offset_of__binder_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5833 = { sizeof (Error_t8C8AD64A1A9712EB98A717498D9E4632E868A660), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5834 = { sizeof (ExpressionTreeCallRewriter_tC488183EA41BE7E437B93B86791CFA5779CE860B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5834[4] = 
{
	ExpressionTreeCallRewriter_tC488183EA41BE7E437B93B86791CFA5779CE860B::get_offset_of__DictionaryOfParameters_0(),
	ExpressionTreeCallRewriter_tC488183EA41BE7E437B93B86791CFA5779CE860B::get_offset_of__ListOfParameters_1(),
	ExpressionTreeCallRewriter_tC488183EA41BE7E437B93B86791CFA5779CE860B::get_offset_of__typeManager_2(),
	ExpressionTreeCallRewriter_tC488183EA41BE7E437B93B86791CFA5779CE860B::get_offset_of__currentParameterIndex_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5835 = { sizeof (ExpressionExpr_t76ED44FDB900D5FBE6BC31968A88B628D0D0C2C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5835[1] = 
{
	ExpressionExpr_t76ED44FDB900D5FBE6BC31968A88B628D0D0C2C2::get_offset_of_Expression_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5836 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5837 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5838 = { sizeof (ResetBindException_tF9FA2AFE9CA681532453C81BC9684082C0DA3AE7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5839 = { sizeof (RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03), -1, sizeof(RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5839[7] = 
{
	RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03_StaticFields::get_offset_of_s_lazyInstance_0(),
	RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03::get_offset_of__symbolTable_1(),
	RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03::get_offset_of__semanticChecker_2(),
	RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03::get_offset_of__exprFactory_3(),
	RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03::get_offset_of__bindingContext_4(),
	RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03::get_offset_of__binder_5(),
	RuntimeBinder_tB1F2154E74418B6DCD60108148C4BFFAF2D0FE03::get_offset_of__bindLock_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5840 = { sizeof (U3CU3Ec_t0B82A5865EFFA1D783819678FC38BE8C8AA0709B), -1, sizeof(U3CU3Ec_t0B82A5865EFFA1D783819678FC38BE8C8AA0709B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5840[1] = 
{
	U3CU3Ec_t0B82A5865EFFA1D783819678FC38BE8C8AA0709B_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5841 = { sizeof (RuntimeBinderException_t7B5AC6644DD8D642283F515925BDB8345D20EB23), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5842 = { sizeof (RuntimeBinderExtensions_t82FB39096CA05255EC54F0643B2E7A550E243131), -1, sizeof(RuntimeBinderExtensions_t82FB39096CA05255EC54F0643B2E7A550E243131_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5842[1] = 
{
	RuntimeBinderExtensions_t82FB39096CA05255EC54F0643B2E7A550E243131_StaticFields::get_offset_of_s_MemberEquivalence_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5843 = { sizeof (U3CU3Ec__DisplayClass1_0_t0A2CCCF27B971C3B949B8FC3267FA35DA6FA8EA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5843[2] = 
{
	U3CU3Ec__DisplayClass1_0_t0A2CCCF27B971C3B949B8FC3267FA35DA6FA8EA5::get_offset_of_method1_0(),
	U3CU3Ec__DisplayClass1_0_t0A2CCCF27B971C3B949B8FC3267FA35DA6FA8EA5::get_offset_of_method2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5844 = { sizeof (U3CU3Ec__DisplayClass1_1_t600A9BCCDDA928475AE4FF48AE9978CE73EA3CF3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5844[2] = 
{
	U3CU3Ec__DisplayClass1_1_t600A9BCCDDA928475AE4FF48AE9978CE73EA3CF3::get_offset_of_ctor1_0(),
	U3CU3Ec__DisplayClass1_1_t600A9BCCDDA928475AE4FF48AE9978CE73EA3CF3::get_offset_of_ctor2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5845 = { sizeof (U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32), -1, sizeof(U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5845[5] = 
{
	U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32_StaticFields::get_offset_of_U3CU3E9__1_1_1(),
	U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32_StaticFields::get_offset_of_U3CU3E9__1_3_2(),
	U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32_StaticFields::get_offset_of_U3CU3E9__4_1_3(),
	U3CU3Ec_tFD9C65E667B863A1E188A2BADA6F22F3044D3E32_StaticFields::get_offset_of_U3CU3E9__10_1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5846 = { sizeof (U3CU3Ec__DisplayClass4_0_tDA0887CF3A22682842C736714A89366EBAD41DB8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5846[2] = 
{
	U3CU3Ec__DisplayClass4_0_tDA0887CF3A22682842C736714A89366EBAD41DB8::get_offset_of_member1_0(),
	U3CU3Ec__DisplayClass4_0_tDA0887CF3A22682842C736714A89366EBAD41DB8::get_offset_of_member2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5847 = { sizeof (U3CU3Ec__DisplayClass9_0_t1B2422A71E13FF0E9B515F402691AC3338A4C491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5847[1] = 
{
	U3CU3Ec__DisplayClass9_0_t1B2422A71E13FF0E9B515F402691AC3338A4C491::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5848 = { sizeof (RuntimeBinderInternalCompilerException_t88D0D461DDC2AE7EAAD21631BE58318F84ED1920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5849 = { sizeof (SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C), -1, sizeof(SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5849[13] = 
{
	SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C::get_offset_of__typesWithConversionsLoaded_0(),
	SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C::get_offset_of__namesLoadedForEachType_1(),
	SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C::get_offset_of__symbolTable_2(),
	SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C::get_offset_of__symFactory_3(),
	SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C::get_offset_of__nameManager_4(),
	SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C::get_offset_of__typeManager_5(),
	SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C::get_offset_of__bsymmgr_6(),
	SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C::get_offset_of__semanticChecker_7(),
	SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C::get_offset_of__rootNamespace_8(),
	SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C_StaticFields::get_offset_of_s_Sentinel_9(),
	SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C_StaticFields::get_offset_of_s_EventRegistrationTokenType_10(),
	SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C_StaticFields::get_offset_of_s_WindowsRuntimeMarshal_11(),
	SymbolTable_tF3B5FBAEAD415120E05E8C4BF152C769EBCB2F1C_StaticFields::get_offset_of_s_EventRegistrationTokenTable_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5850 = { sizeof (NameHashKey_tC6EE60C80A020C655F03995397C8DCB469E41040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5850[2] = 
{
	NameHashKey_tC6EE60C80A020C655F03995397C8DCB469E41040::get_offset_of_type_0(),
	NameHashKey_tC6EE60C80A020C655F03995397C8DCB469E41040::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5851 = { sizeof (U3CU3Ec__DisplayClass15_0_t8EF4067FAF6A75CB6BF5EFF004BE26788651C6A8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5851[1] = 
{
	U3CU3Ec__DisplayClass15_0_t8EF4067FAF6A75CB6BF5EFF004BE26788651C6A8::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5852 = { sizeof (U3CU3Ec__DisplayClass15_1_tF3C0A04AC6B7A782CA637D9A9A4AF0F81319EE96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5852[2] = 
{
	U3CU3Ec__DisplayClass15_1_tF3C0A04AC6B7A782CA637D9A9A4AF0F81319EE96::get_offset_of_type_0(),
	U3CU3Ec__DisplayClass15_1_tF3C0A04AC6B7A782CA637D9A9A4AF0F81319EE96::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5853 = { sizeof (U3CU3Ec__DisplayClass30_0_t431E47B24016E917355B79B0890BA54AEC4905B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5853[2] = 
{
	U3CU3Ec__DisplayClass30_0_t431E47B24016E917355B79B0890BA54AEC4905B6::get_offset_of_methodBase_0(),
	U3CU3Ec__DisplayClass30_0_t431E47B24016E917355B79B0890BA54AEC4905B6::get_offset_of_U3CU3E9__0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5854 = { sizeof (U3CU3Ec__DisplayClass50_0_tC77DA813449A11CCCC2E2457404BB440124CBE7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5854[1] = 
{
	U3CU3Ec__DisplayClass50_0_tC77DA813449A11CCCC2E2457404BB440124CBE7F::get_offset_of_property_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5855 = { sizeof (U3CU3Ec__DisplayClass52_0_t4015D3895F4DF718E7581D0E54E26D381100B0FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5855[2] = 
{
	U3CU3Ec__DisplayClass52_0_t4015D3895F4DF718E7581D0E54E26D381100B0FA::get_offset_of_methodName_0(),
	U3CU3Ec__DisplayClass52_0_t4015D3895F4DF718E7581D0E54E26D381100B0FA::get_offset_of_t_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5856 = { sizeof (NameManager_t965C30E738C964824296FF875F6E7B4828AEDD31), -1, sizeof(NameManager_t965C30E738C964824296FF875F6E7B4828AEDD31_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5856[3] = 
{
	NameManager_t965C30E738C964824296FF875F6E7B4828AEDD31_StaticFields::get_offset_of_s_predefinedNames_0(),
	NameManager_t965C30E738C964824296FF875F6E7B4828AEDD31_StaticFields::get_offset_of_s_knownNames_1(),
	NameManager_t965C30E738C964824296FF875F6E7B4828AEDD31::get_offset_of__names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5857 = { sizeof (NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5857[3] = 
{
	NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0::get_offset_of__entries_0(),
	NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0::get_offset_of__count_1(),
	NameTable_t1E4EA7C1780B6B8F6D6F751D619FA151A10181A0::get_offset_of__mask_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5858 = { sizeof (Entry_t79E8E453C852A3AF2A2A1E82E9E41BEED4B3DEF9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5858[3] = 
{
	Entry_t79E8E453C852A3AF2A2A1E82E9E41BEED4B3DEF9::get_offset_of_Name_0(),
	Entry_t79E8E453C852A3AF2A2A1E82E9E41BEED4B3DEF9::get_offset_of_HashCode_1(),
	Entry_t79E8E453C852A3AF2A2A1E82E9E41BEED4B3DEF9::get_offset_of_Next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5859 = { sizeof (Name_t95244514D054D80B6614F5E2275EC034F1527725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5859[1] = 
{
	Name_t95244514D054D80B6614F5E2275EC034F1527725::get_offset_of_U3CTextU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5860 = { sizeof (OperatorKind_tDB430CC83D1E2EA1E099D0386F2BA44479EBF441)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5860[70] = 
{
	OperatorKind_tDB430CC83D1E2EA1E099D0386F2BA44479EBF441::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5861 = { sizeof (PredefinedName_t8BC14E853C03DB817D27F0CBCEF4A494D98E5251)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5861[105] = 
{
	PredefinedName_t8BC14E853C03DB817D27F0CBCEF4A494D98E5251::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5862 = { sizeof (PredefinedType_tA9E6BFC4BDEB7BB4FA3F04CCE1E61E83E686DDC6)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5862[53] = 
{
	PredefinedType_tA9E6BFC4BDEB7BB4FA3F04CCE1E61E83E686DDC6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5863 = { sizeof (TokenFacts_t65E6B160EC1CAFB6ED1E96531A27688DBCCA0F24), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5864 = { sizeof (TokenKind_tE22E2BD7A8F2A71654B450266A78159FBE559753)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5864[57] = 
{
	TokenKind_tE22E2BD7A8F2A71654B450266A78159FBE559753::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5865 = { sizeof (ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E), -1, sizeof(ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5865[11] = 
{
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields::get_offset_of_s_betterConversionTable_0(),
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields::get_offset_of_s_ReadOnlyLocalErrors_1(),
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields::get_offset_of_s_ReadOnlyErrors_2(),
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields::get_offset_of_s_simpleTypeConversions_3(),
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields::get_offset_of_s_simpleTypeBetter_4(),
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E::get_offset_of_Context_5(),
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E::get_offset_of_m_nullable_6(),
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields::get_offset_of_s_rgptIntOp_7(),
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E_StaticFields::get_offset_of_s_EK2NAME_8(),
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E::get_offset_of_g_binopSignatures_9(),
	ExpressionBinder_t6AFB55222F11231641DF4A17EC2CBCC84142502E::get_offset_of_g_rguos_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5866 = { sizeof (BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5866[12] = 
{
	BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913::get_offset_of_arg1_0(),
	BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913::get_offset_of_arg2_1(),
	BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913::get_offset_of_pt1_2(),
	BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913::get_offset_of_pt2_3(),
	BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913::get_offset_of_ptRaw1_4(),
	BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913::get_offset_of_ptRaw2_5(),
	BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913::get_offset_of_type1_6(),
	BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913::get_offset_of_type2_7(),
	BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913::get_offset_of_typeRaw1_8(),
	BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913::get_offset_of_typeRaw2_9(),
	BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913::get_offset_of_binopKind_10(),
	BinOpArgInfo_tE147F1B6BAF4747F1598D2690E9F8D9B33C86913::get_offset_of_mask_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5867 = { sizeof (BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5867[7] = 
{
	BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3::get_offset_of_pt1_0(),
	BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3::get_offset_of_pt2_1(),
	BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3::get_offset_of_mask_2(),
	BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3::get_offset_of_cbosSkip_3(),
	BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3::get_offset_of_pfn_4(),
	BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3::get_offset_of_grfos_5(),
	BinOpSig_t41167D930280E1719B8F12646C526CBFC516ADD3::get_offset_of_fnkind_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5868 = { sizeof (BinOpFullSig_t004AF42FF106C3A5FB62DF0EE8CCF68546B2A7F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5868[3] = 
{
	BinOpFullSig_t004AF42FF106C3A5FB62DF0EE8CCF68546B2A7F5::get_offset_of__grflt_7(),
	BinOpFullSig_t004AF42FF106C3A5FB62DF0EE8CCF68546B2A7F5::get_offset_of__type1_8(),
	BinOpFullSig_t004AF42FF106C3A5FB62DF0EE8CCF68546B2A7F5::get_offset_of__type2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5869 = { sizeof (ConversionFunc_tFA8AAED736762954309492560704CBC4FEEBC1D8), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5870 = { sizeof (ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5870[9] = 
{
	ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716::get_offset_of__binder_0(),
	ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716::get_offset_of__exprSrc_1(),
	ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716::get_offset_of__typeSrc_2(),
	ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716::get_offset_of__typeDest_3(),
	ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716::get_offset_of__exprTypeDest_4(),
	ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716::get_offset_of__pDestinationTypeForLambdaErrorReporting_5(),
	ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716::get_offset_of__exprDest_6(),
	ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716::get_offset_of__needsExprDest_7(),
	ExplicitConversion_tFD7F7BB13C79E4F0D25EB53B767BE00FE4CAC716::get_offset_of__flags_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5871 = { sizeof (PfnBindBinOp_t376819D18187F697A9950060805897A8BF537F68), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5872 = { sizeof (PfnBindUnaOp_tF887D64C6A968258E18C38389610C6E1E70E6944), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5873 = { sizeof (GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5873[25] = 
{
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__pExprBinder_0(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__fCandidatesUnsupported_1(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__fBindFlags_2(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__pGroup_3(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__pArguments_4(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__pOriginalArguments_5(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__bHasNamedArguments_6(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__pDelegate_7(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__pCurrentType_8(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__pCurrentSym_9(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__pCurrentTypeArgs_10(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__pCurrentParameters_11(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__pBestParameters_12(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__nArgBest_13(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__results_14(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__methList_15(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__mpwiParamTypeConstraints_16(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__mpwiBogus_17(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__mpwiCantInferInstArg_18(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__mwtBadArity_19(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__pInvalidSpecifiedName_20(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__pNameUsedInPositionalArgument_21(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__pDuplicateSpecifiedName_22(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__HiddenTypes_23(),
	GroupToArgsBinder_tB9E500A6861864E8A15A0CE9D1B5887978ED7D82::get_offset_of__bArgumentsChangedForNamedOrOptionalArguments_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5874 = { sizeof (Result_t9909E31E427DF6AF8FA28481E5ADF1BCC2A5DA90)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5874[4] = 
{
	Result_t9909E31E427DF6AF8FA28481E5ADF1BCC2A5DA90::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5875 = { sizeof (GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5875[6] = 
{
	GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0::get_offset_of_BestResult_0(),
	GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0::get_offset_of_AmbiguousResult_1(),
	GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0::get_offset_of_InaccessibleResult_2(),
	GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0::get_offset_of_UninferableResult_3(),
	GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0::get_offset_of_InconvertibleResult_4(),
	GroupToArgsBinderResult_tABA33A05689DFE1D5ECF1A77159EC864FA9C1EC0::get_offset_of__inconvertibleResults_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5876 = { sizeof (ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5876[8] = 
{
	ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D::get_offset_of__exprDest_0(),
	ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D::get_offset_of__binder_1(),
	ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D::get_offset_of__exprSrc_2(),
	ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D::get_offset_of__typeSrc_3(),
	ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D::get_offset_of__typeDest_4(),
	ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D::get_offset_of__exprTypeDest_5(),
	ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D::get_offset_of__needsExprDest_6(),
	ImplicitConversion_t5815575A8493949A6368CD893A47A121491C2A0D::get_offset_of__flags_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5877 = { sizeof (UnaOpSig_t66B0230ED69A79A4288B6C38326A1A1EB4E44AB1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5877[5] = 
{
	UnaOpSig_t66B0230ED69A79A4288B6C38326A1A1EB4E44AB1::get_offset_of_pt_0(),
	UnaOpSig_t66B0230ED69A79A4288B6C38326A1A1EB4E44AB1::get_offset_of_grfuom_1(),
	UnaOpSig_t66B0230ED69A79A4288B6C38326A1A1EB4E44AB1::get_offset_of_cuosSkip_2(),
	UnaOpSig_t66B0230ED69A79A4288B6C38326A1A1EB4E44AB1::get_offset_of_pfn_3(),
	UnaOpSig_t66B0230ED69A79A4288B6C38326A1A1EB4E44AB1::get_offset_of_fnkind_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5878 = { sizeof (UnaOpFullSig_t3BB590F0D7FEE3BE56C22C87919B27471A631E11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5878[2] = 
{
	UnaOpFullSig_t3BB590F0D7FEE3BE56C22C87919B27471A631E11::get_offset_of__grflt_5(),
	UnaOpFullSig_t3BB590F0D7FEE3BE56C22C87919B27471A631E11::get_offset_of__type_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5879 = { sizeof (U3CU3Ec__DisplayClass102_0_t5C5586FEC4D864AE5ACC60A2CE76ED0FB90890CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5879[3] = 
{
	U3CU3Ec__DisplayClass102_0_t5C5586FEC4D864AE5ACC60A2CE76ED0FB90890CD::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass102_0_t5C5586FEC4D864AE5ACC60A2CE76ED0FB90890CD::get_offset_of_pDestType_1(),
	U3CU3Ec__DisplayClass102_0_t5C5586FEC4D864AE5ACC60A2CE76ED0FB90890CD::get_offset_of_pIntType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5880 = { sizeof (BinOpKind_t20BC3362D5F1A32CA225C1E71AAEF5A429D46AA0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5880[11] = 
{
	BinOpKind_t20BC3362D5F1A32CA225C1E71AAEF5A429D46AA0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5881 = { sizeof (BinOpMask_t109AB0EC300AFA64332719FA88881BF49AFBBE97)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5881[22] = 
{
	BinOpMask_t109AB0EC300AFA64332719FA88881BF49AFBBE97::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5882 = { sizeof (BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5882[6] = 
{
	BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073::get_offset_of_U3CSymbolLoaderU3Ek__BackingField_0(),
	BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073::get_offset_of_U3CContextForMemberLookupU3Ek__BackingField_1(),
	BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073::get_offset_of_U3CSemanticCheckerU3Ek__BackingField_2(),
	BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073::get_offset_of_U3CExprFactoryU3Ek__BackingField_3(),
	BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073::get_offset_of_U3CCheckedNormalU3Ek__BackingField_4(),
	BindingContext_t395E94DB38328C0481E905BB742C11352BA8C073::get_offset_of_U3CCheckedConstantU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5883 = { sizeof (BindingFlag_t57D354B0FF6B1A0C2F4724ABD8043326D3481B74)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5883[14] = 
{
	BindingFlag_t57D354B0FF6B1A0C2F4724ABD8043326D3481B74::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5884 = { sizeof (Operators_tCE07D36E96DCA68768BF8A0E22B083026541E255), -1, sizeof(Operators_tCE07D36E96DCA68768BF8A0E22B083026541E255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5884[2] = 
{
	Operators_tCE07D36E96DCA68768BF8A0E22B083026541E255_StaticFields::get_offset_of_s_operatorInfos_0(),
	Operators_tCE07D36E96DCA68768BF8A0E22B083026541E255_StaticFields::get_offset_of_s_operatorsByName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5885 = { sizeof (OperatorInfo_t3F4B81310F0F4ADCDC95EAA974BADAC5A40FAFBF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5885[3] = 
{
	OperatorInfo_t3F4B81310F0F4ADCDC95EAA974BADAC5A40FAFBF::get_offset_of_TokenKind_0(),
	OperatorInfo_t3F4B81310F0F4ADCDC95EAA974BADAC5A40FAFBF::get_offset_of_MethodName_1(),
	OperatorInfo_t3F4B81310F0F4ADCDC95EAA974BADAC5A40FAFBF::get_offset_of_ExpressionKind_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5886 = { sizeof (CandidateFunctionMember_t11C0A24B60AD0F73DAF3EAD2E96C630FC2595D94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5886[4] = 
{
	CandidateFunctionMember_t11C0A24B60AD0F73DAF3EAD2E96C630FC2595D94::get_offset_of_mpwi_0(),
	CandidateFunctionMember_t11C0A24B60AD0F73DAF3EAD2E96C630FC2595D94::get_offset_of_params_1(),
	CandidateFunctionMember_t11C0A24B60AD0F73DAF3EAD2E96C630FC2595D94::get_offset_of_ctypeLift_2(),
	CandidateFunctionMember_t11C0A24B60AD0F73DAF3EAD2E96C630FC2595D94::get_offset_of_fExpanded_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5887 = { sizeof (ConstValKind_t23507BC78F5717D51436F99F8F138D54F055D690)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5887[9] = 
{
	ConstValKind_t23507BC78F5717D51436F99F8F138D54F055D690::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5888 = { sizeof (ConstVal_t4673513268B853CEFF9ACEA67E742D46A987DC26)+ sizeof (RuntimeObject), sizeof(ConstVal_t4673513268B853CEFF9ACEA67E742D46A987DC26_marshaled_pinvoke), sizeof(ConstVal_t4673513268B853CEFF9ACEA67E742D46A987DC26_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5888[4] = 
{
	ConstVal_t4673513268B853CEFF9ACEA67E742D46A987DC26_StaticFields::get_offset_of_s_false_0(),
	ConstVal_t4673513268B853CEFF9ACEA67E742D46A987DC26_StaticFields::get_offset_of_s_true_1(),
	ConstVal_t4673513268B853CEFF9ACEA67E742D46A987DC26_StaticFields::get_offset_of_s_zeroInt32_2(),
	ConstVal_t4673513268B853CEFF9ACEA67E742D46A987DC26::get_offset_of_U3CObjectValU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5889 = { sizeof (ConvKind_tDCD6872B2A128FD598D8A121CA47A350877B93F7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5889[6] = 
{
	ConvKind_tDCD6872B2A128FD598D8A121CA47A350877B93F7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5890 = { sizeof (CONVERTTYPE_tB9C5AC4C022C175CB67276214C91686872A8B0F4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5890[7] = 
{
	CONVERTTYPE_tB9C5AC4C022C175CB67276214C91686872A8B0F4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5891 = { sizeof (BetterType_t833A834305AE83107E92381CBBC19C4B0178D37E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5891[5] = 
{
	BetterType_t833A834305AE83107E92381CBBC19C4B0178D37E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5892 = { sizeof (ListExtensions_t9CEAD44FD94E94693A8C98990C678427B36E0A2B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5893 = { sizeof (CConversions_t6C665AEE020C93DC589B0C85DB6D6D503E060F4F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5894 = { sizeof (AggregateDeclaration_t1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5894[2] = 
{
	AggregateDeclaration_t1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5::get_offset_of_bag_8(),
	AggregateDeclaration_t1763F3CEB4B045275C0EF3B12A2522D5EC1A04B5::get_offset_of_declNext_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5895 = { sizeof (EXPRExtensions_tD473EF9629717DE39A5FCB5EBAB6EF17E452B4B4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5896 = { sizeof (U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5896[7] = 
{
	U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F::get_offset_of_U3CU3E1__state_0(),
	U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F::get_offset_of_U3CU3E2__current_1(),
	U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F::get_offset_of_expr_3(),
	U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F::get_offset_of_U3CU3E3__expr_4(),
	U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F::get_offset_of_U3CexprCurU3E5__1_5(),
	U3CToEnumerableU3Ed__1_tF4C84A4AB0BB9AE6B30A29D2E8941B0A696FF87F::get_offset_of_U3ClistU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5897 = { sizeof (EXPRFLAG_tFB55C8C8B4BEA8EDC0C4BA623385FB4A12FA433A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5897[63] = 
{
	EXPRFLAG_tFB55C8C8B4BEA8EDC0C4BA623385FB4A12FA433A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5898 = { sizeof (ExprFactory_tFD6AA538BD7633AE2E5585D41577AB6B996811DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5898[1] = 
{
	ExprFactory_tFD6AA538BD7633AE2E5585D41577AB6B996811DA::get_offset_of__globalSymbolContext_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5899 = { sizeof (UdConvInfo_tE0FCAD27FF1838A58B86891384B3F2F62B302FFB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5899[3] = 
{
	UdConvInfo_tE0FCAD27FF1838A58B86891384B3F2F62B302FFB::get_offset_of_mwt_0(),
	UdConvInfo_tE0FCAD27FF1838A58B86891384B3F2F62B302FFB::get_offset_of_fSrcImplicit_1(),
	UdConvInfo_tE0FCAD27FF1838A58B86891384B3F2F62B302FFB::get_offset_of_fDstImplicit_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
