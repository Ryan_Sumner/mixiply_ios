﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Mono.Unity.UnityTls/unitytls_interface_struct
struct unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_errorstate_create_t
struct unitytls_errorstate_create_t_t104BADBBE1265BD1A3F84C153EB7A67CDDBF35A9;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_errorstate_raise_error_t
struct unitytls_errorstate_raise_error_t_tC441A37D4A6F1BAC1AFCA0108D4F7570EFF9E0D1;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_free_t
struct unitytls_key_free_t_tCC7AD95D3B758BB99785645E65EDCD65A1D243AB;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_get_ref_t
struct unitytls_key_get_ref_t_t2F4EF4CD2F6AFC4F2D166953E834C6F0A13382A7;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_parse_der_t
struct unitytls_key_parse_der_t_t2ABD1C146C8B9405F6E5A78CD59A779EA607741B;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_parse_pem_t
struct unitytls_key_parse_pem_t_tB4BCEBA4194442C8C85FA19E80B808D0EDA462AB;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_random_generate_bytes_t
struct unitytls_random_generate_bytes_t_t494B8599A6D4247BB0C8AB7341DDC73BE42623F7;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_create_client_t
struct unitytls_tlsctx_create_client_t_tD9DFBDB5559983F0E11A67FA92E0F7182114C8F2;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_create_server_t
struct unitytls_tlsctx_create_server_t_t6E7812D40DDD91958E3CFBB92B5F5748D477E19D;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_free_t
struct unitytls_tlsctx_free_t_tB27A3B6F9D1B784ABE082849EAB6B81F51FAC8E2;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_get_ciphersuite_t
struct unitytls_tlsctx_get_ciphersuite_t_t94A91CB42A2EBB2CC598EF3E278770AFD80696A0;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_get_protocol_t
struct unitytls_tlsctx_get_protocol_t_tB29092875D3CBD25E4461BFD165B5373FA54DB14;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_notify_close_t
struct unitytls_tlsctx_notify_close_t_t2FC4C08BACF1AEA509ABCAF3B22475E196E74A0D;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_process_handshake_t
struct unitytls_tlsctx_process_handshake_t_tC8AAF317CBE4CA216F22BF031ECF89315B963C9D;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_read_t
struct unitytls_tlsctx_read_t_tA8D1209D5F488E02F826EE2362F5AA61C8FF2EE2;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_server_require_client_authentication_t
struct unitytls_tlsctx_server_require_client_authentication_t_t77B3CAFF25690A45405E3C957E40CC4FF83B49C6;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_certificate_callback_t
struct unitytls_tlsctx_set_certificate_callback_t_tC4864FE0F6A3398A572F2511AA64C72126640937;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_supported_ciphersuites_t
struct unitytls_tlsctx_set_supported_ciphersuites_t_t6914054EA0F7A59C8A4ED4B9CDD5AF143F7D8BFE;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_trace_callback_t
struct unitytls_tlsctx_set_trace_callback_t_tA11F424F68D297B6FD2B2EA26C6764F80146662A;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_x509verify_callback_t
struct unitytls_tlsctx_set_x509verify_callback_t_t34EEB7BA38CA2C86F847416785ADB22BC4A04F4B;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_write_t
struct unitytls_tlsctx_write_t_t0B4A49BBA592FE4EC0630B490463AE116AF07C9C;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509_export_der_t
struct unitytls_x509_export_der_t_tB0D0A02DE7E75757AFCA780298BF95467BF82856;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_append_der_t
struct unitytls_x509list_append_der_t_tDA1C93A382058FB563F8772B119D5B598DC37A5C;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_append_t
struct unitytls_x509list_append_t_tAB1C185C77DFD6BD96DF7909370AA1FAD6BB90AF;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_create_t
struct unitytls_x509list_create_t_tC040C2CF47D5426B7F6B1D6A2751507DC681CFF3;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_free_t
struct unitytls_x509list_free_t_tE3FC523507F07BD9901D84E9F6968CD8A583FF09;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_get_ref_t
struct unitytls_x509list_get_ref_t_t1FAB0CD82E536E0C9EB5255B145FC5AF434B3986;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_get_x509_t
struct unitytls_x509list_get_x509_t_t028BB06EEB95E8F62511F2301B90D8181F4FFDB5;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509verify_default_ca_t
struct unitytls_x509verify_default_ca_t_t4BACB6B49AA85C025AF9B18B3F30F63C9881DE2D;
// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509verify_explicit_ca_t
struct unitytls_x509verify_explicit_ca_t_t6C8BE964C5EE9B24D3734F028EFCD83F5893D2E6;
// Mono.Unity.UnityTls/unitytls_tlsctx_read_callback
struct unitytls_tlsctx_read_callback_tD85E7923018681355C1D851137CEC527F04093F5;
// Mono.Unity.UnityTls/unitytls_tlsctx_write_callback
struct unitytls_tlsctx_write_callback_tBDF40F27E011F577C3E834B14788491861F870D6;
// Mono.Unity.UnityTls/unitytls_x509verify_callback
struct unitytls_x509verify_callback_t90C02C529DB2B9F434C18797BACC456BCB5400A9;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[0...,0...]
struct BooleanU5BU2CU5D_tCD5854E1A55166EC93998DDED119F97FF5A5ECDC;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.Stack
struct Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.Xml.HWStack
struct HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE;
// System.Xml.IDtdInfo
struct IDtdInfo_t5971A8C09914EDB816FE7A86A38288FDC4B6F80C;
// System.Xml.IValidationEventHandling
struct IValidationEventHandling_tABD39B6B973C0A0DC259D55D8C4179A43ACAB41B;
// System.Xml.IXmlLineInfo
struct IXmlLineInfo_tD6D8818DFB22D29FC2397C76BA6BFFF54604284A;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t252EBD93E225063727450B6A8B4BE94F5F2E8427;
// System.Xml.PositionInfo
struct PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684;
// System.Xml.Schema.IdRefNode
struct IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2;
// System.Xml.Schema.Parser
struct Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920;
// System.Xml.Schema.SchemaAttDef
struct SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41;
// System.Xml.Schema.SchemaNames
struct SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF;
// System.Xml.Schema.ValidationState
struct ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B;
// System.Xml.Schema.XmlSchema
struct XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F;
// System.Xml.Schema.XmlSchemaAll
struct XmlSchemaAll_t395D3C374B5E11F2B2338F0C33E9F267D88CBA3B;
// System.Xml.Schema.XmlSchemaAnnotation
struct XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725;
// System.Xml.Schema.XmlSchemaAny
struct XmlSchemaAny_tDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2;
// System.Xml.Schema.XmlSchemaAnyAttribute
struct XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078;
// System.Xml.Schema.XmlSchemaAppInfo
struct XmlSchemaAppInfo_t5E2FEE4B31F35CD076CC2F47DA5CB82DA139A860;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B;
// System.Xml.Schema.XmlSchemaAttributeGroup
struct XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E;
// System.Xml.Schema.XmlSchemaAttributeGroupRef
struct XmlSchemaAttributeGroupRef_t5DA5E994E5C8A3E844A6B88B5E9AEA8195B09BD4;
// System.Xml.Schema.XmlSchemaAttribute[]
struct XmlSchemaAttributeU5BU5D_tFDBB08A9CF45CD6E2DE3A8AADCC8487BA5CD896F;
// System.Xml.Schema.XmlSchemaChoice
struct XmlSchemaChoice_tEC175BFF045FDA4DECF5A938922D6CDC4216653B;
// System.Xml.Schema.XmlSchemaCollection
struct XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5;
// System.Xml.Schema.XmlSchemaComplexContent
struct XmlSchemaComplexContent_t0C5B5B9FA891EAAD3F277E4C494956082E127CF3;
// System.Xml.Schema.XmlSchemaComplexContentExtension
struct XmlSchemaComplexContentExtension_t3B6C614515825C627D8DE0D040CD97753829004B;
// System.Xml.Schema.XmlSchemaComplexContentRestriction
struct XmlSchemaComplexContentRestriction_t05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0;
// System.Xml.Schema.XmlSchemaComplexType
struct XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550;
// System.Xml.Schema.XmlSchemaDocumentation
struct XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D;
// System.Xml.Schema.XmlSchemaFacet
struct XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250;
// System.Xml.Schema.XmlSchemaGroup
struct XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2;
// System.Xml.Schema.XmlSchemaGroupRef
struct XmlSchemaGroupRef_tEC541A27220ABE0F02B3B1FD72730E536FAEF6F3;
// System.Xml.Schema.XmlSchemaIdentityConstraint
struct XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95;
// System.Xml.Schema.XmlSchemaImport
struct XmlSchemaImport_t4FC11FF359600C711AFFEF2DC569810456D8CC93;
// System.Xml.Schema.XmlSchemaInclude
struct XmlSchemaInclude_tD337A52B31C9B3195CE4272AF057E286CDD34001;
// System.Xml.Schema.XmlSchemaNotation
struct XmlSchemaNotation_tA67925FC817C85C8CB753EBAFBB7EA0AE414D41D;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B;
// System.Xml.Schema.XmlSchemaParticle
struct XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671;
// System.Xml.Schema.XmlSchemaParticle[]
struct XmlSchemaParticleU5BU5D_tCA99C9F23120839B45A59D3FD77E88564CEAD800;
// System.Xml.Schema.XmlSchemaRedefine
struct XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B;
// System.Xml.Schema.XmlSchemaSequence
struct XmlSchemaSequence_tBF8498445C0FD553EF64CC240B1236CEB83FB215;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F;
// System.Xml.Schema.XmlSchemaSimpleContent
struct XmlSchemaSimpleContent_t47CF5CDB950EC1B3CB066BA2F9E3DAE993AD3550;
// System.Xml.Schema.XmlSchemaSimpleContentExtension
struct XmlSchemaSimpleContentExtension_t235FC077FDE8C8796FA6A961658C42D60F4CB812;
// System.Xml.Schema.XmlSchemaSimpleContentRestriction
struct XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A;
// System.Xml.Schema.XmlSchemaSimpleTypeList
struct XmlSchemaSimpleTypeList_tE5804144CB4035DE27AA3C43233579A075469403;
// System.Xml.Schema.XmlSchemaSimpleTypeRestriction
struct XmlSchemaSimpleTypeRestriction_t948C7680F4D262DA9DA7590CCA8E2F96078E490C;
// System.Xml.Schema.XmlSchemaSimpleTypeUnion
struct XmlSchemaSimpleTypeUnion_t3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9;
// System.Xml.Schema.XmlSchemaXPath
struct XmlSchemaXPath_t6D090E4243F2A15A6A5E4EF5B17B0740711C6C34;
// System.Xml.Schema.XmlTypeCode[]
struct XmlTypeCodeU5BU5D_t43BF24D43F5D8E1233D96C0CE555091AE495642B;
// System.Xml.Schema.XmlValueConverter
struct XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E;
// System.Xml.Schema.XmlValueConverter[]
struct XmlValueConverterU5BU5D_t0C08DAE703E09DAE455F80F5975EB50F0D34CAB3;
// System.Xml.Schema.XsdBuilder
struct XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3;
// System.Xml.Schema.XsdBuilder/State[]
struct StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0;
// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[]
struct XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0;
// System.Xml.Schema.XsdBuilder/XsdBuildFunction
struct XsdBuildFunction_tCE6DE66119181E60067FBE2AD08DE0D013B7F41E;
// System.Xml.Schema.XsdBuilder/XsdEndChildFunction
struct XsdEndChildFunction_t7FDE0B391499702A4683AB55B900623C4F8DFB17;
// System.Xml.Schema.XsdBuilder/XsdEntry
struct XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3;
// System.Xml.Schema.XsdBuilder/XsdEntry[]
struct XsdEntryU5BU5D_t612F7D8AE3D932202C80449A380B9D5A54E3106A;
// System.Xml.Schema.XsdBuilder/XsdInitFunction
struct XsdInitFunction_t8B6E839BF5C55B9A918D919F005B971BFAEC5DF3;
// System.Xml.XmlNameTable
struct XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F;
// System.Xml.XmlNamespaceManager/NamespaceDeclaration[]
struct NamespaceDeclarationU5BU5D_t92995CBBEC52166860E77B72B8EEFF00E4D64E84;
// System.Xml.XmlNode[]
struct XmlNodeU5BU5D_tBF402FAC28AE903AC3FF845775405A2274B701AF;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD;
// System.Xml.XmlReader
struct XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB;
// System.Xml.XmlResolver
struct XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280;
// System.Xml.XmlValidatingReaderImpl
struct XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E;




#ifndef U3CMODULEU3E_TD81C26B45B0FBFD466203EB631D6270174412EE8_H
#define U3CMODULEU3E_TD81C26B45B0FBFD466203EB631D6270174412EE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tD81C26B45B0FBFD466203EB631D6270174412EE8 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TD81C26B45B0FBFD466203EB631D6270174412EE8_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CERTHELPER_T603EE753116E3B83EC5DB1CAC369F51DF9DC5A60_H
#define CERTHELPER_T603EE753116E3B83EC5DB1CAC369F51DF9DC5A60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.CertHelper
struct  CertHelper_t603EE753116E3B83EC5DB1CAC369F51DF9DC5A60  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTHELPER_T603EE753116E3B83EC5DB1CAC369F51DF9DC5A60_H
#ifndef DEBUG_T61CBAA915E7373BA24D59D69221A160BCE7EB406_H
#define DEBUG_T61CBAA915E7373BA24D59D69221A160BCE7EB406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.Debug
struct  Debug_t61CBAA915E7373BA24D59D69221A160BCE7EB406  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUG_T61CBAA915E7373BA24D59D69221A160BCE7EB406_H
#ifndef UNITYTLS_TFD87351846C601F716262E015308C6BB66000DEC_H
#define UNITYTLS_TFD87351846C601F716262E015308C6BB66000DEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls
struct  UnityTls_tFD87351846C601F716262E015308C6BB66000DEC  : public RuntimeObject
{
public:

public:
};

struct UnityTls_tFD87351846C601F716262E015308C6BB66000DEC_StaticFields
{
public:
	// Mono.Unity.UnityTls/unitytls_interface_struct Mono.Unity.UnityTls::marshalledInterface
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF * ___marshalledInterface_0;

public:
	inline static int32_t get_offset_of_marshalledInterface_0() { return static_cast<int32_t>(offsetof(UnityTls_tFD87351846C601F716262E015308C6BB66000DEC_StaticFields, ___marshalledInterface_0)); }
	inline unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF * get_marshalledInterface_0() const { return ___marshalledInterface_0; }
	inline unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF ** get_address_of_marshalledInterface_0() { return &___marshalledInterface_0; }
	inline void set_marshalledInterface_0(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF * value)
	{
		___marshalledInterface_0 = value;
		Il2CppCodeGenWriteBarrier((&___marshalledInterface_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_TFD87351846C601F716262E015308C6BB66000DEC_H
#ifndef SR_TF6C259D0ADF333DE679CC02B526A504888CDD8AC_H
#define SR_TF6C259D0ADF333DE679CC02B526A504888CDD8AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SR
struct  SR_tF6C259D0ADF333DE679CC02B526A504888CDD8AC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SR_TF6C259D0ADF333DE679CC02B526A504888CDD8AC_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BASEVALIDATOR_TB0AD5FCB23C61EC8841C153CEB7C77E569246B43_H
#define BASEVALIDATOR_TB0AD5FCB23C61EC8841C153CEB7C77E569246B43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BaseValidator
struct  BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaCollection System.Xml.Schema.BaseValidator::schemaCollection
	XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * ___schemaCollection_0;
	// System.Xml.IValidationEventHandling System.Xml.Schema.BaseValidator::eventHandling
	RuntimeObject* ___eventHandling_1;
	// System.Xml.XmlNameTable System.Xml.Schema.BaseValidator::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_2;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.BaseValidator::schemaNames
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * ___schemaNames_3;
	// System.Xml.PositionInfo System.Xml.Schema.BaseValidator::positionInfo
	PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * ___positionInfo_4;
	// System.Xml.XmlResolver System.Xml.Schema.BaseValidator::xmlResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___xmlResolver_5;
	// System.Uri System.Xml.Schema.BaseValidator::baseUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___baseUri_6;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.BaseValidator::schemaInfo
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * ___schemaInfo_7;
	// System.Xml.XmlValidatingReaderImpl System.Xml.Schema.BaseValidator::reader
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * ___reader_8;
	// System.Xml.XmlQualifiedName System.Xml.Schema.BaseValidator::elementName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___elementName_9;
	// System.Xml.Schema.ValidationState System.Xml.Schema.BaseValidator::context
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * ___context_10;
	// System.Text.StringBuilder System.Xml.Schema.BaseValidator::textValue
	StringBuilder_t * ___textValue_11;
	// System.String System.Xml.Schema.BaseValidator::textString
	String_t* ___textString_12;
	// System.Boolean System.Xml.Schema.BaseValidator::hasSibling
	bool ___hasSibling_13;
	// System.Boolean System.Xml.Schema.BaseValidator::checkDatatype
	bool ___checkDatatype_14;

public:
	inline static int32_t get_offset_of_schemaCollection_0() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___schemaCollection_0)); }
	inline XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * get_schemaCollection_0() const { return ___schemaCollection_0; }
	inline XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 ** get_address_of_schemaCollection_0() { return &___schemaCollection_0; }
	inline void set_schemaCollection_0(XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * value)
	{
		___schemaCollection_0 = value;
		Il2CppCodeGenWriteBarrier((&___schemaCollection_0), value);
	}

	inline static int32_t get_offset_of_eventHandling_1() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___eventHandling_1)); }
	inline RuntimeObject* get_eventHandling_1() const { return ___eventHandling_1; }
	inline RuntimeObject** get_address_of_eventHandling_1() { return &___eventHandling_1; }
	inline void set_eventHandling_1(RuntimeObject* value)
	{
		___eventHandling_1 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandling_1), value);
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___nameTable_2)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_schemaNames_3() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___schemaNames_3)); }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * get_schemaNames_3() const { return ___schemaNames_3; }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 ** get_address_of_schemaNames_3() { return &___schemaNames_3; }
	inline void set_schemaNames_3(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * value)
	{
		___schemaNames_3 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_3), value);
	}

	inline static int32_t get_offset_of_positionInfo_4() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___positionInfo_4)); }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * get_positionInfo_4() const { return ___positionInfo_4; }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 ** get_address_of_positionInfo_4() { return &___positionInfo_4; }
	inline void set_positionInfo_4(PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * value)
	{
		___positionInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___positionInfo_4), value);
	}

	inline static int32_t get_offset_of_xmlResolver_5() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___xmlResolver_5)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_xmlResolver_5() const { return ___xmlResolver_5; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_xmlResolver_5() { return &___xmlResolver_5; }
	inline void set_xmlResolver_5(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___xmlResolver_5 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_5), value);
	}

	inline static int32_t get_offset_of_baseUri_6() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___baseUri_6)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_baseUri_6() const { return ___baseUri_6; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_baseUri_6() { return &___baseUri_6; }
	inline void set_baseUri_6(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___baseUri_6 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_6), value);
	}

	inline static int32_t get_offset_of_schemaInfo_7() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___schemaInfo_7)); }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * get_schemaInfo_7() const { return ___schemaInfo_7; }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 ** get_address_of_schemaInfo_7() { return &___schemaInfo_7; }
	inline void set_schemaInfo_7(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * value)
	{
		___schemaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_7), value);
	}

	inline static int32_t get_offset_of_reader_8() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___reader_8)); }
	inline XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * get_reader_8() const { return ___reader_8; }
	inline XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E ** get_address_of_reader_8() { return &___reader_8; }
	inline void set_reader_8(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * value)
	{
		___reader_8 = value;
		Il2CppCodeGenWriteBarrier((&___reader_8), value);
	}

	inline static int32_t get_offset_of_elementName_9() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___elementName_9)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_elementName_9() const { return ___elementName_9; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_elementName_9() { return &___elementName_9; }
	inline void set_elementName_9(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___elementName_9 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_9), value);
	}

	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___context_10)); }
	inline ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * get_context_10() const { return ___context_10; }
	inline ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier((&___context_10), value);
	}

	inline static int32_t get_offset_of_textValue_11() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___textValue_11)); }
	inline StringBuilder_t * get_textValue_11() const { return ___textValue_11; }
	inline StringBuilder_t ** get_address_of_textValue_11() { return &___textValue_11; }
	inline void set_textValue_11(StringBuilder_t * value)
	{
		___textValue_11 = value;
		Il2CppCodeGenWriteBarrier((&___textValue_11), value);
	}

	inline static int32_t get_offset_of_textString_12() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___textString_12)); }
	inline String_t* get_textString_12() const { return ___textString_12; }
	inline String_t** get_address_of_textString_12() { return &___textString_12; }
	inline void set_textString_12(String_t* value)
	{
		___textString_12 = value;
		Il2CppCodeGenWriteBarrier((&___textString_12), value);
	}

	inline static int32_t get_offset_of_hasSibling_13() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___hasSibling_13)); }
	inline bool get_hasSibling_13() const { return ___hasSibling_13; }
	inline bool* get_address_of_hasSibling_13() { return &___hasSibling_13; }
	inline void set_hasSibling_13(bool value)
	{
		___hasSibling_13 = value;
	}

	inline static int32_t get_offset_of_checkDatatype_14() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___checkDatatype_14)); }
	inline bool get_checkDatatype_14() const { return ___checkDatatype_14; }
	inline bool* get_address_of_checkDatatype_14() { return &___checkDatatype_14; }
	inline void set_checkDatatype_14(bool value)
	{
		___checkDatatype_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVALIDATOR_TB0AD5FCB23C61EC8841C153CEB7C77E569246B43_H
#ifndef IDREFNODE_T8E92E33D4D596CFC17454D41C07CD7627A8825E2_H
#define IDREFNODE_T8E92E33D4D596CFC17454D41C07CD7627A8825E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.IdRefNode
struct  IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2  : public RuntimeObject
{
public:
	// System.String System.Xml.Schema.IdRefNode::Id
	String_t* ___Id_0;
	// System.Int32 System.Xml.Schema.IdRefNode::LineNo
	int32_t ___LineNo_1;
	// System.Int32 System.Xml.Schema.IdRefNode::LinePos
	int32_t ___LinePos_2;
	// System.Xml.Schema.IdRefNode System.Xml.Schema.IdRefNode::Next
	IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 * ___Next_3;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_LineNo_1() { return static_cast<int32_t>(offsetof(IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2, ___LineNo_1)); }
	inline int32_t get_LineNo_1() const { return ___LineNo_1; }
	inline int32_t* get_address_of_LineNo_1() { return &___LineNo_1; }
	inline void set_LineNo_1(int32_t value)
	{
		___LineNo_1 = value;
	}

	inline static int32_t get_offset_of_LinePos_2() { return static_cast<int32_t>(offsetof(IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2, ___LinePos_2)); }
	inline int32_t get_LinePos_2() const { return ___LinePos_2; }
	inline int32_t* get_address_of_LinePos_2() { return &___LinePos_2; }
	inline void set_LinePos_2(int32_t value)
	{
		___LinePos_2 = value;
	}

	inline static int32_t get_offset_of_Next_3() { return static_cast<int32_t>(offsetof(IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2, ___Next_3)); }
	inline IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 * get_Next_3() const { return ___Next_3; }
	inline IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 ** get_address_of_Next_3() { return &___Next_3; }
	inline void set_Next_3(IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 * value)
	{
		___Next_3 = value;
		Il2CppCodeGenWriteBarrier((&___Next_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDREFNODE_T8E92E33D4D596CFC17454D41C07CD7627A8825E2_H
#ifndef SCHEMABUILDER_T6B2D813207751A3A8F89AB247E37E371F17450B7_H
#define SCHEMABUILDER_T6B2D813207751A3A8F89AB247E37E371F17450B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaBuilder
struct  SchemaBuilder_t6B2D813207751A3A8F89AB247E37E371F17450B7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMABUILDER_T6B2D813207751A3A8F89AB247E37E371F17450B7_H
#ifndef XMLVALUECONVERTER_TC6BABF8791F0E88864F8707A10F63FBE9EA0D69E_H
#define XMLVALUECONVERTER_TC6BABF8791F0E88864F8707A10F63FBE9EA0D69E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlValueConverter
struct  XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLVALUECONVERTER_TC6BABF8791F0E88864F8707A10F63FBE9EA0D69E_H
#ifndef XMLREADERSECTION_TFCE70D088C6038C6BB6BA8A6A66F97D91AD84AC7_H
#define XMLREADERSECTION_TFCE70D088C6038C6BB6BA8A6A66F97D91AD84AC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlConfiguration.XmlReaderSection
struct  XmlReaderSection_tFCE70D088C6038C6BB6BA8A6A66F97D91AD84AC7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADERSECTION_TFCE70D088C6038C6BB6BA8A6A66F97D91AD84AC7_H
#ifndef XMLNAMESPACEMANAGER_T8323BEB96BBE8F75207DC2AAFE9430E7F473658F_H
#define XMLNAMESPACEMANAGER_T8323BEB96BBE8F75207DC2AAFE9430E7F473658F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager
struct  XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F  : public RuntimeObject
{
public:
	// System.Xml.XmlNamespaceManager/NamespaceDeclaration[] System.Xml.XmlNamespaceManager::nsdecls
	NamespaceDeclarationU5BU5D_t92995CBBEC52166860E77B72B8EEFF00E4D64E84* ___nsdecls_0;
	// System.Int32 System.Xml.XmlNamespaceManager::lastDecl
	int32_t ___lastDecl_1;
	// System.Xml.XmlNameTable System.Xml.XmlNamespaceManager::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_2;
	// System.Int32 System.Xml.XmlNamespaceManager::scopeId
	int32_t ___scopeId_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNamespaceManager::hashTable
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___hashTable_4;
	// System.Boolean System.Xml.XmlNamespaceManager::useHashtable
	bool ___useHashtable_5;
	// System.String System.Xml.XmlNamespaceManager::xml
	String_t* ___xml_6;
	// System.String System.Xml.XmlNamespaceManager::xmlNs
	String_t* ___xmlNs_7;

public:
	inline static int32_t get_offset_of_nsdecls_0() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___nsdecls_0)); }
	inline NamespaceDeclarationU5BU5D_t92995CBBEC52166860E77B72B8EEFF00E4D64E84* get_nsdecls_0() const { return ___nsdecls_0; }
	inline NamespaceDeclarationU5BU5D_t92995CBBEC52166860E77B72B8EEFF00E4D64E84** get_address_of_nsdecls_0() { return &___nsdecls_0; }
	inline void set_nsdecls_0(NamespaceDeclarationU5BU5D_t92995CBBEC52166860E77B72B8EEFF00E4D64E84* value)
	{
		___nsdecls_0 = value;
		Il2CppCodeGenWriteBarrier((&___nsdecls_0), value);
	}

	inline static int32_t get_offset_of_lastDecl_1() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___lastDecl_1)); }
	inline int32_t get_lastDecl_1() const { return ___lastDecl_1; }
	inline int32_t* get_address_of_lastDecl_1() { return &___lastDecl_1; }
	inline void set_lastDecl_1(int32_t value)
	{
		___lastDecl_1 = value;
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___nameTable_2)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_scopeId_3() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___scopeId_3)); }
	inline int32_t get_scopeId_3() const { return ___scopeId_3; }
	inline int32_t* get_address_of_scopeId_3() { return &___scopeId_3; }
	inline void set_scopeId_3(int32_t value)
	{
		___scopeId_3 = value;
	}

	inline static int32_t get_offset_of_hashTable_4() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___hashTable_4)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_hashTable_4() const { return ___hashTable_4; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_hashTable_4() { return &___hashTable_4; }
	inline void set_hashTable_4(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___hashTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___hashTable_4), value);
	}

	inline static int32_t get_offset_of_useHashtable_5() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___useHashtable_5)); }
	inline bool get_useHashtable_5() const { return ___useHashtable_5; }
	inline bool* get_address_of_useHashtable_5() { return &___useHashtable_5; }
	inline void set_useHashtable_5(bool value)
	{
		___useHashtable_5 = value;
	}

	inline static int32_t get_offset_of_xml_6() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___xml_6)); }
	inline String_t* get_xml_6() const { return ___xml_6; }
	inline String_t** get_address_of_xml_6() { return &___xml_6; }
	inline void set_xml_6(String_t* value)
	{
		___xml_6 = value;
		Il2CppCodeGenWriteBarrier((&___xml_6), value);
	}

	inline static int32_t get_offset_of_xmlNs_7() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___xmlNs_7)); }
	inline String_t* get_xmlNs_7() const { return ___xmlNs_7; }
	inline String_t** get_address_of_xmlNs_7() { return &___xmlNs_7; }
	inline void set_xmlNs_7(String_t* value)
	{
		___xmlNs_7 = value;
		Il2CppCodeGenWriteBarrier((&___xmlNs_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEMANAGER_T8323BEB96BBE8F75207DC2AAFE9430E7F473658F_H
#ifndef __STATICARRAYINITTYPESIZEU3D112_TCA120B5FDB821F63EF42C3B78E8B2A74B77D117E_H
#define __STATICARRAYINITTYPESIZEU3D112_TCA120B5FDB821F63EF42C3B78E8B2A74B77D117E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=112
struct  __StaticArrayInitTypeSizeU3D112_tCA120B5FDB821F63EF42C3B78E8B2A74B77D117E 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D112_tCA120B5FDB821F63EF42C3B78E8B2A74B77D117E__padding[112];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D112_TCA120B5FDB821F63EF42C3B78E8B2A74B77D117E_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_TF92030B0FB73CC4EB587C7FD3177E5229C5B8749_H
#define __STATICARRAYINITTYPESIZEU3D12_TF92030B0FB73CC4EB587C7FD3177E5229C5B8749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_TF92030B0FB73CC4EB587C7FD3177E5229C5B8749_H
#ifndef __STATICARRAYINITTYPESIZEU3D1212_T528E4585D66D23345F608EEE97F869A0D1665EB8_H
#define __STATICARRAYINITTYPESIZEU3D1212_T528E4585D66D23345F608EEE97F869A0D1665EB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=1212
struct  __StaticArrayInitTypeSizeU3D1212_t528E4585D66D23345F608EEE97F869A0D1665EB8 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D1212_t528E4585D66D23345F608EEE97F869A0D1665EB8__padding[1212];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D1212_T528E4585D66D23345F608EEE97F869A0D1665EB8_H
#ifndef __STATICARRAYINITTYPESIZEU3D144_T9C3F15E672A756D2D700C19E37BF64DBD63DD0F7_H
#define __STATICARRAYINITTYPESIZEU3D144_T9C3F15E672A756D2D700C19E37BF64DBD63DD0F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=144
struct  __StaticArrayInitTypeSizeU3D144_t9C3F15E672A756D2D700C19E37BF64DBD63DD0F7 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D144_t9C3F15E672A756D2D700C19E37BF64DBD63DD0F7__padding[144];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D144_T9C3F15E672A756D2D700C19E37BF64DBD63DD0F7_H
#ifndef __STATICARRAYINITTYPESIZEU3D16_T1022F965C0CFEE56E88F01CFB36C134A0AB27CCD_H
#define __STATICARRAYINITTYPESIZEU3D16_T1022F965C0CFEE56E88F01CFB36C134A0AB27CCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16
struct  __StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD__padding[16];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D16_T1022F965C0CFEE56E88F01CFB36C134A0AB27CCD_H
#ifndef __STATICARRAYINITTYPESIZEU3D20_T25C626683DA421874F122B40243142FF7832A1BF_H
#define __STATICARRAYINITTYPESIZEU3D20_T25C626683DA421874F122B40243142FF7832A1BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20
struct  __StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF__padding[20];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D20_T25C626683DA421874F122B40243142FF7832A1BF_H
#ifndef __STATICARRAYINITTYPESIZEU3D24_TFAA45C2D269AF27FBFEF2E0076065EE57863E223_H
#define __STATICARRAYINITTYPESIZEU3D24_TFAA45C2D269AF27FBFEF2E0076065EE57863E223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D24_TFAA45C2D269AF27FBFEF2E0076065EE57863E223_H
#ifndef __STATICARRAYINITTYPESIZEU3D28_T5092DE69B1A24897AC8FBEFE716755EA1FE14C24_H
#define __STATICARRAYINITTYPESIZEU3D28_T5092DE69B1A24897AC8FBEFE716755EA1FE14C24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28
struct  __StaticArrayInitTypeSizeU3D28_t5092DE69B1A24897AC8FBEFE716755EA1FE14C24 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_t5092DE69B1A24897AC8FBEFE716755EA1FE14C24__padding[28];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D28_T5092DE69B1A24897AC8FBEFE716755EA1FE14C24_H
#ifndef __STATICARRAYINITTYPESIZEU3D32_TFEC269E612F5A23CB7315224D57AF2DA72F821E7_H
#define __STATICARRAYINITTYPESIZEU3D32_TFEC269E612F5A23CB7315224D57AF2DA72F821E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32
struct  __StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7__padding[32];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D32_TFEC269E612F5A23CB7315224D57AF2DA72F821E7_H
#ifndef __STATICARRAYINITTYPESIZEU3D36_T5217F3350ECED222B6441CF50B97CE496CFF3C15_H
#define __STATICARRAYINITTYPESIZEU3D36_T5217F3350ECED222B6441CF50B97CE496CFF3C15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36
struct  __StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15__padding[36];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D36_T5217F3350ECED222B6441CF50B97CE496CFF3C15_H
#ifndef __STATICARRAYINITTYPESIZEU3D38_T93A8ADCEAFC78CEEBA1473ADD1938275574C49E1_H
#define __STATICARRAYINITTYPESIZEU3D38_T93A8ADCEAFC78CEEBA1473ADD1938275574C49E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=38
struct  __StaticArrayInitTypeSizeU3D38_t93A8ADCEAFC78CEEBA1473ADD1938275574C49E1 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D38_t93A8ADCEAFC78CEEBA1473ADD1938275574C49E1__padding[38];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D38_T93A8ADCEAFC78CEEBA1473ADD1938275574C49E1_H
#ifndef __STATICARRAYINITTYPESIZEU3D40_T2C1C40E1ED6F4BAA5E535BCF7574AD6781A49AD4_H
#define __STATICARRAYINITTYPESIZEU3D40_T2C1C40E1ED6F4BAA5E535BCF7574AD6781A49AD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40
struct  __StaticArrayInitTypeSizeU3D40_t2C1C40E1ED6F4BAA5E535BCF7574AD6781A49AD4 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_t2C1C40E1ED6F4BAA5E535BCF7574AD6781A49AD4__padding[40];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D40_T2C1C40E1ED6F4BAA5E535BCF7574AD6781A49AD4_H
#ifndef __STATICARRAYINITTYPESIZEU3D416_TDC7E75F123C5FA6CED5F4949F6976ADFF285E704_H
#define __STATICARRAYINITTYPESIZEU3D416_TDC7E75F123C5FA6CED5F4949F6976ADFF285E704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=416
struct  __StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704__padding[416];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D416_TDC7E75F123C5FA6CED5F4949F6976ADFF285E704_H
#ifndef __STATICARRAYINITTYPESIZEU3D44_T497B1CD0DA7F519C08603DFB716118F291F18E40_H
#define __STATICARRAYINITTYPESIZEU3D44_T497B1CD0DA7F519C08603DFB716118F291F18E40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44
struct  __StaticArrayInitTypeSizeU3D44_t497B1CD0DA7F519C08603DFB716118F291F18E40 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D44_t497B1CD0DA7F519C08603DFB716118F291F18E40__padding[44];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D44_T497B1CD0DA7F519C08603DFB716118F291F18E40_H
#ifndef __STATICARRAYINITTYPESIZEU3D56_TA1FD048C977FA4FB4F0B2359C884E3002DC36737_H
#define __STATICARRAYINITTYPESIZEU3D56_TA1FD048C977FA4FB4F0B2359C884E3002DC36737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=56
struct  __StaticArrayInitTypeSizeU3D56_tA1FD048C977FA4FB4F0B2359C884E3002DC36737 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D56_tA1FD048C977FA4FB4F0B2359C884E3002DC36737__padding[56];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D56_TA1FD048C977FA4FB4F0B2359C884E3002DC36737_H
#ifndef __STATICARRAYINITTYPESIZEU3D6_TB657E692303B443FF0E24AE8F75A675A844348C4_H
#define __STATICARRAYINITTYPESIZEU3D6_TB657E692303B443FF0E24AE8F75A675A844348C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6
struct  __StaticArrayInitTypeSizeU3D6_tB657E692303B443FF0E24AE8F75A675A844348C4 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D6_tB657E692303B443FF0E24AE8F75A675A844348C4__padding[6];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D6_TB657E692303B443FF0E24AE8F75A675A844348C4_H
#ifndef __STATICARRAYINITTYPESIZEU3D64_TE9F2C6D5C2D961C95B98D342E25A70CE875C2387_H
#define __STATICARRAYINITTYPESIZEU3D64_TE9F2C6D5C2D961C95B98D342E25A70CE875C2387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64
struct  __StaticArrayInitTypeSizeU3D64_tE9F2C6D5C2D961C95B98D342E25A70CE875C2387 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D64_tE9F2C6D5C2D961C95B98D342E25A70CE875C2387__padding[64];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D64_TE9F2C6D5C2D961C95B98D342E25A70CE875C2387_H
#ifndef __STATICARRAYINITTYPESIZEU3D664_T3741AEF321779B117CACFCF3A0CE45075DD15EBA_H
#define __STATICARRAYINITTYPESIZEU3D664_T3741AEF321779B117CACFCF3A0CE45075DD15EBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=664
struct  __StaticArrayInitTypeSizeU3D664_t3741AEF321779B117CACFCF3A0CE45075DD15EBA 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D664_t3741AEF321779B117CACFCF3A0CE45075DD15EBA__padding[664];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D664_T3741AEF321779B117CACFCF3A0CE45075DD15EBA_H
#ifndef __STATICARRAYINITTYPESIZEU3D68_T5DF8EE2FE382E89E2BE20E276315E7E5850B86A2_H
#define __STATICARRAYINITTYPESIZEU3D68_T5DF8EE2FE382E89E2BE20E276315E7E5850B86A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=68
struct  __StaticArrayInitTypeSizeU3D68_t5DF8EE2FE382E89E2BE20E276315E7E5850B86A2 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D68_t5DF8EE2FE382E89E2BE20E276315E7E5850B86A2__padding[68];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D68_T5DF8EE2FE382E89E2BE20E276315E7E5850B86A2_H
#ifndef __STATICARRAYINITTYPESIZEU3D960_T90BB40F76231B6054859C4B179BC097521A54C32_H
#define __STATICARRAYINITTYPESIZEU3D960_T90BB40F76231B6054859C4B179BC097521A54C32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=960
struct  __StaticArrayInitTypeSizeU3D960_t90BB40F76231B6054859C4B179BC097521A54C32 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D960_t90BB40F76231B6054859C4B179BC097521A54C32__padding[960];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D960_T90BB40F76231B6054859C4B179BC097521A54C32_H
#ifndef UNITYTLS_KEY_TABB3A1A923B658DC99FFDB3ED633FBBA64EF1BB9_H
#define UNITYTLS_KEY_TABB3A1A923B658DC99FFDB3ED633FBBA64EF1BB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_key
struct  unitytls_key_tABB3A1A923B658DC99FFDB3ED633FBBA64EF1BB9 
{
public:
	union
	{
		struct
		{
		};
		uint8_t unitytls_key_tABB3A1A923B658DC99FFDB3ED633FBBA64EF1BB9__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_KEY_TABB3A1A923B658DC99FFDB3ED633FBBA64EF1BB9_H
#ifndef UNITYTLS_KEY_REF_TE908606656A7C49CA1EB734722E4C3DED7CE6E5B_H
#define UNITYTLS_KEY_REF_TE908606656A7C49CA1EB734722E4C3DED7CE6E5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_key_ref
struct  unitytls_key_ref_tE908606656A7C49CA1EB734722E4C3DED7CE6E5B 
{
public:
	// System.UInt64 Mono.Unity.UnityTls/unitytls_key_ref::handle
	uint64_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(unitytls_key_ref_tE908606656A7C49CA1EB734722E4C3DED7CE6E5B, ___handle_0)); }
	inline uint64_t get_handle_0() const { return ___handle_0; }
	inline uint64_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(uint64_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_KEY_REF_TE908606656A7C49CA1EB734722E4C3DED7CE6E5B_H
#ifndef UNITYTLS_TLSCTX_T6B948536BDFA3AAC0135FF136ABD7779A0B96A74_H
#define UNITYTLS_TLSCTX_T6B948536BDFA3AAC0135FF136ABD7779A0B96A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_tlsctx
struct  unitytls_tlsctx_t6B948536BDFA3AAC0135FF136ABD7779A0B96A74 
{
public:
	union
	{
		struct
		{
		};
		uint8_t unitytls_tlsctx_t6B948536BDFA3AAC0135FF136ABD7779A0B96A74__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_TLSCTX_T6B948536BDFA3AAC0135FF136ABD7779A0B96A74_H
#ifndef UNITYTLS_TLSCTX_CALLBACKS_T7BB5F622E014A8EC300C578657E2B0550DA828B2_H
#define UNITYTLS_TLSCTX_CALLBACKS_T7BB5F622E014A8EC300C578657E2B0550DA828B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_tlsctx_callbacks
struct  unitytls_tlsctx_callbacks_t7BB5F622E014A8EC300C578657E2B0550DA828B2 
{
public:
	// Mono.Unity.UnityTls/unitytls_tlsctx_read_callback Mono.Unity.UnityTls/unitytls_tlsctx_callbacks::read
	unitytls_tlsctx_read_callback_tD85E7923018681355C1D851137CEC527F04093F5 * ___read_0;
	// Mono.Unity.UnityTls/unitytls_tlsctx_write_callback Mono.Unity.UnityTls/unitytls_tlsctx_callbacks::write
	unitytls_tlsctx_write_callback_tBDF40F27E011F577C3E834B14788491861F870D6 * ___write_1;
	// System.Void* Mono.Unity.UnityTls/unitytls_tlsctx_callbacks::data
	void* ___data_2;

public:
	inline static int32_t get_offset_of_read_0() { return static_cast<int32_t>(offsetof(unitytls_tlsctx_callbacks_t7BB5F622E014A8EC300C578657E2B0550DA828B2, ___read_0)); }
	inline unitytls_tlsctx_read_callback_tD85E7923018681355C1D851137CEC527F04093F5 * get_read_0() const { return ___read_0; }
	inline unitytls_tlsctx_read_callback_tD85E7923018681355C1D851137CEC527F04093F5 ** get_address_of_read_0() { return &___read_0; }
	inline void set_read_0(unitytls_tlsctx_read_callback_tD85E7923018681355C1D851137CEC527F04093F5 * value)
	{
		___read_0 = value;
		Il2CppCodeGenWriteBarrier((&___read_0), value);
	}

	inline static int32_t get_offset_of_write_1() { return static_cast<int32_t>(offsetof(unitytls_tlsctx_callbacks_t7BB5F622E014A8EC300C578657E2B0550DA828B2, ___write_1)); }
	inline unitytls_tlsctx_write_callback_tBDF40F27E011F577C3E834B14788491861F870D6 * get_write_1() const { return ___write_1; }
	inline unitytls_tlsctx_write_callback_tBDF40F27E011F577C3E834B14788491861F870D6 ** get_address_of_write_1() { return &___write_1; }
	inline void set_write_1(unitytls_tlsctx_write_callback_tBDF40F27E011F577C3E834B14788491861F870D6 * value)
	{
		___write_1 = value;
		Il2CppCodeGenWriteBarrier((&___write_1), value);
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(unitytls_tlsctx_callbacks_t7BB5F622E014A8EC300C578657E2B0550DA828B2, ___data_2)); }
	inline void* get_data_2() const { return ___data_2; }
	inline void** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(void* value)
	{
		___data_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.Unity.UnityTls/unitytls_tlsctx_callbacks
struct unitytls_tlsctx_callbacks_t7BB5F622E014A8EC300C578657E2B0550DA828B2_marshaled_pinvoke
{
	Il2CppMethodPointer ___read_0;
	Il2CppMethodPointer ___write_1;
	void* ___data_2;
};
// Native definition for COM marshalling of Mono.Unity.UnityTls/unitytls_tlsctx_callbacks
struct unitytls_tlsctx_callbacks_t7BB5F622E014A8EC300C578657E2B0550DA828B2_marshaled_com
{
	Il2CppMethodPointer ___read_0;
	Il2CppMethodPointer ___write_1;
	void* ___data_2;
};
#endif // UNITYTLS_TLSCTX_CALLBACKS_T7BB5F622E014A8EC300C578657E2B0550DA828B2_H
#ifndef UNITYTLS_X509_REF_TE1ED17887226610A1328A57FF787396C9457E7B7_H
#define UNITYTLS_X509_REF_TE1ED17887226610A1328A57FF787396C9457E7B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_x509_ref
struct  unitytls_x509_ref_tE1ED17887226610A1328A57FF787396C9457E7B7 
{
public:
	// System.UInt64 Mono.Unity.UnityTls/unitytls_x509_ref::handle
	uint64_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(unitytls_x509_ref_tE1ED17887226610A1328A57FF787396C9457E7B7, ___handle_0)); }
	inline uint64_t get_handle_0() const { return ___handle_0; }
	inline uint64_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(uint64_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_X509_REF_TE1ED17887226610A1328A57FF787396C9457E7B7_H
#ifndef UNITYTLS_X509LIST_TDFEEABB4254CDE9475890D0F2AE361B45EC357C7_H
#define UNITYTLS_X509LIST_TDFEEABB4254CDE9475890D0F2AE361B45EC357C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_x509list
struct  unitytls_x509list_tDFEEABB4254CDE9475890D0F2AE361B45EC357C7 
{
public:
	union
	{
		struct
		{
		};
		uint8_t unitytls_x509list_tDFEEABB4254CDE9475890D0F2AE361B45EC357C7__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_X509LIST_TDFEEABB4254CDE9475890D0F2AE361B45EC357C7_H
#ifndef UNITYTLS_X509LIST_REF_TF01A6BF5ADA9C454E6B975D2669AF22D27555BF6_H
#define UNITYTLS_X509LIST_REF_TF01A6BF5ADA9C454E6B975D2669AF22D27555BF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_x509list_ref
struct  unitytls_x509list_ref_tF01A6BF5ADA9C454E6B975D2669AF22D27555BF6 
{
public:
	// System.UInt64 Mono.Unity.UnityTls/unitytls_x509list_ref::handle
	uint64_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(unitytls_x509list_ref_tF01A6BF5ADA9C454E6B975D2669AF22D27555BF6, ___handle_0)); }
	inline uint64_t get_handle_0() const { return ___handle_0; }
	inline uint64_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(uint64_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_X509LIST_REF_TF01A6BF5ADA9C454E6B975D2669AF22D27555BF6_H
#ifndef UNITYTLS_X509NAME_T551F433869F1BAA39C78962C7ACA1BAB9A4D6337_H
#define UNITYTLS_X509NAME_T551F433869F1BAA39C78962C7ACA1BAB9A4D6337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_x509name
struct  unitytls_x509name_t551F433869F1BAA39C78962C7ACA1BAB9A4D6337 
{
public:
	union
	{
		struct
		{
		};
		uint8_t unitytls_x509name_t551F433869F1BAA39C78962C7ACA1BAB9A4D6337__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_X509NAME_T551F433869F1BAA39C78962C7ACA1BAB9A4D6337_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_T4D5E5DC1B6616AD7983047AC217DE7BD9400E0CD_H
#define MONOPINVOKECALLBACKATTRIBUTE_T4D5E5DC1B6616AD7983047AC217DE7BD9400E0CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Util.MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t4D5E5DC1B6616AD7983047AC217DE7BD9400E0CD  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_T4D5E5DC1B6616AD7983047AC217DE7BD9400E0CD_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef XSDBUILDER_TBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_H
#define XSDBUILDER_TBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdBuilder
struct  XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3  : public SchemaBuilder_t6B2D813207751A3A8F89AB247E37E371F17450B7
{
public:
	// System.Xml.XmlReader System.Xml.Schema.XsdBuilder::reader
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * ___reader_61;
	// System.Xml.PositionInfo System.Xml.Schema.XsdBuilder::positionInfo
	PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * ___positionInfo_62;
	// System.Xml.Schema.XsdBuilder/XsdEntry System.Xml.Schema.XsdBuilder::currentEntry
	XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3 * ___currentEntry_63;
	// System.Xml.Schema.XsdBuilder/XsdEntry System.Xml.Schema.XsdBuilder::nextEntry
	XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3 * ___nextEntry_64;
	// System.Boolean System.Xml.Schema.XsdBuilder::hasChild
	bool ___hasChild_65;
	// System.Xml.HWStack System.Xml.Schema.XsdBuilder::stateHistory
	HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * ___stateHistory_66;
	// System.Collections.Stack System.Xml.Schema.XsdBuilder::containerStack
	Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 * ___containerStack_67;
	// System.Xml.XmlNameTable System.Xml.Schema.XsdBuilder::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_68;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.XsdBuilder::schemaNames
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * ___schemaNames_69;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.XsdBuilder::namespaceManager
	XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * ___namespaceManager_70;
	// System.Boolean System.Xml.Schema.XsdBuilder::canIncludeImport
	bool ___canIncludeImport_71;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XsdBuilder::schema
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___schema_72;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XsdBuilder::xso
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * ___xso_73;
	// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.XsdBuilder::element
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D * ___element_74;
	// System.Xml.Schema.XmlSchemaAny System.Xml.Schema.XsdBuilder::anyElement
	XmlSchemaAny_tDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2 * ___anyElement_75;
	// System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.XsdBuilder::attribute
	XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B * ___attribute_76;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XsdBuilder::anyAttribute
	XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * ___anyAttribute_77;
	// System.Xml.Schema.XmlSchemaComplexType System.Xml.Schema.XsdBuilder::complexType
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * ___complexType_78;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XsdBuilder::simpleType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___simpleType_79;
	// System.Xml.Schema.XmlSchemaComplexContent System.Xml.Schema.XsdBuilder::complexContent
	XmlSchemaComplexContent_t0C5B5B9FA891EAAD3F277E4C494956082E127CF3 * ___complexContent_80;
	// System.Xml.Schema.XmlSchemaComplexContentExtension System.Xml.Schema.XsdBuilder::complexContentExtension
	XmlSchemaComplexContentExtension_t3B6C614515825C627D8DE0D040CD97753829004B * ___complexContentExtension_81;
	// System.Xml.Schema.XmlSchemaComplexContentRestriction System.Xml.Schema.XsdBuilder::complexContentRestriction
	XmlSchemaComplexContentRestriction_t05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0 * ___complexContentRestriction_82;
	// System.Xml.Schema.XmlSchemaSimpleContent System.Xml.Schema.XsdBuilder::simpleContent
	XmlSchemaSimpleContent_t47CF5CDB950EC1B3CB066BA2F9E3DAE993AD3550 * ___simpleContent_83;
	// System.Xml.Schema.XmlSchemaSimpleContentExtension System.Xml.Schema.XsdBuilder::simpleContentExtension
	XmlSchemaSimpleContentExtension_t235FC077FDE8C8796FA6A961658C42D60F4CB812 * ___simpleContentExtension_84;
	// System.Xml.Schema.XmlSchemaSimpleContentRestriction System.Xml.Schema.XsdBuilder::simpleContentRestriction
	XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621 * ___simpleContentRestriction_85;
	// System.Xml.Schema.XmlSchemaSimpleTypeUnion System.Xml.Schema.XsdBuilder::simpleTypeUnion
	XmlSchemaSimpleTypeUnion_t3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A * ___simpleTypeUnion_86;
	// System.Xml.Schema.XmlSchemaSimpleTypeList System.Xml.Schema.XsdBuilder::simpleTypeList
	XmlSchemaSimpleTypeList_tE5804144CB4035DE27AA3C43233579A075469403 * ___simpleTypeList_87;
	// System.Xml.Schema.XmlSchemaSimpleTypeRestriction System.Xml.Schema.XsdBuilder::simpleTypeRestriction
	XmlSchemaSimpleTypeRestriction_t948C7680F4D262DA9DA7590CCA8E2F96078E490C * ___simpleTypeRestriction_88;
	// System.Xml.Schema.XmlSchemaGroup System.Xml.Schema.XsdBuilder::group
	XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2 * ___group_89;
	// System.Xml.Schema.XmlSchemaGroupRef System.Xml.Schema.XsdBuilder::groupRef
	XmlSchemaGroupRef_tEC541A27220ABE0F02B3B1FD72730E536FAEF6F3 * ___groupRef_90;
	// System.Xml.Schema.XmlSchemaAll System.Xml.Schema.XsdBuilder::all
	XmlSchemaAll_t395D3C374B5E11F2B2338F0C33E9F267D88CBA3B * ___all_91;
	// System.Xml.Schema.XmlSchemaChoice System.Xml.Schema.XsdBuilder::choice
	XmlSchemaChoice_tEC175BFF045FDA4DECF5A938922D6CDC4216653B * ___choice_92;
	// System.Xml.Schema.XmlSchemaSequence System.Xml.Schema.XsdBuilder::sequence
	XmlSchemaSequence_tBF8498445C0FD553EF64CC240B1236CEB83FB215 * ___sequence_93;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XsdBuilder::particle
	XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * ___particle_94;
	// System.Xml.Schema.XmlSchemaAttributeGroup System.Xml.Schema.XsdBuilder::attributeGroup
	XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E * ___attributeGroup_95;
	// System.Xml.Schema.XmlSchemaAttributeGroupRef System.Xml.Schema.XsdBuilder::attributeGroupRef
	XmlSchemaAttributeGroupRef_t5DA5E994E5C8A3E844A6B88B5E9AEA8195B09BD4 * ___attributeGroupRef_96;
	// System.Xml.Schema.XmlSchemaNotation System.Xml.Schema.XsdBuilder::notation
	XmlSchemaNotation_tA67925FC817C85C8CB753EBAFBB7EA0AE414D41D * ___notation_97;
	// System.Xml.Schema.XmlSchemaIdentityConstraint System.Xml.Schema.XsdBuilder::identityConstraint
	XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95 * ___identityConstraint_98;
	// System.Xml.Schema.XmlSchemaXPath System.Xml.Schema.XsdBuilder::xpath
	XmlSchemaXPath_t6D090E4243F2A15A6A5E4EF5B17B0740711C6C34 * ___xpath_99;
	// System.Xml.Schema.XmlSchemaInclude System.Xml.Schema.XsdBuilder::include
	XmlSchemaInclude_tD337A52B31C9B3195CE4272AF057E286CDD34001 * ___include_100;
	// System.Xml.Schema.XmlSchemaImport System.Xml.Schema.XsdBuilder::import
	XmlSchemaImport_t4FC11FF359600C711AFFEF2DC569810456D8CC93 * ___import_101;
	// System.Xml.Schema.XmlSchemaAnnotation System.Xml.Schema.XsdBuilder::annotation
	XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 * ___annotation_102;
	// System.Xml.Schema.XmlSchemaAppInfo System.Xml.Schema.XsdBuilder::appInfo
	XmlSchemaAppInfo_t5E2FEE4B31F35CD076CC2F47DA5CB82DA139A860 * ___appInfo_103;
	// System.Xml.Schema.XmlSchemaDocumentation System.Xml.Schema.XsdBuilder::documentation
	XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE * ___documentation_104;
	// System.Xml.Schema.XmlSchemaFacet System.Xml.Schema.XsdBuilder::facet
	XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250 * ___facet_105;
	// System.Xml.XmlNode[] System.Xml.Schema.XsdBuilder::markup
	XmlNodeU5BU5D_tBF402FAC28AE903AC3FF845775405A2274B701AF* ___markup_106;
	// System.Xml.Schema.XmlSchemaRedefine System.Xml.Schema.XsdBuilder::redefine
	XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B * ___redefine_107;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XsdBuilder::validationEventHandler
	ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * ___validationEventHandler_108;
	// System.Collections.ArrayList System.Xml.Schema.XsdBuilder::unhandledAttributes
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___unhandledAttributes_109;
	// System.Collections.Hashtable System.Xml.Schema.XsdBuilder::namespaces
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___namespaces_110;

public:
	inline static int32_t get_offset_of_reader_61() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___reader_61)); }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * get_reader_61() const { return ___reader_61; }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB ** get_address_of_reader_61() { return &___reader_61; }
	inline void set_reader_61(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * value)
	{
		___reader_61 = value;
		Il2CppCodeGenWriteBarrier((&___reader_61), value);
	}

	inline static int32_t get_offset_of_positionInfo_62() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___positionInfo_62)); }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * get_positionInfo_62() const { return ___positionInfo_62; }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 ** get_address_of_positionInfo_62() { return &___positionInfo_62; }
	inline void set_positionInfo_62(PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * value)
	{
		___positionInfo_62 = value;
		Il2CppCodeGenWriteBarrier((&___positionInfo_62), value);
	}

	inline static int32_t get_offset_of_currentEntry_63() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___currentEntry_63)); }
	inline XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3 * get_currentEntry_63() const { return ___currentEntry_63; }
	inline XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3 ** get_address_of_currentEntry_63() { return &___currentEntry_63; }
	inline void set_currentEntry_63(XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3 * value)
	{
		___currentEntry_63 = value;
		Il2CppCodeGenWriteBarrier((&___currentEntry_63), value);
	}

	inline static int32_t get_offset_of_nextEntry_64() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___nextEntry_64)); }
	inline XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3 * get_nextEntry_64() const { return ___nextEntry_64; }
	inline XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3 ** get_address_of_nextEntry_64() { return &___nextEntry_64; }
	inline void set_nextEntry_64(XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3 * value)
	{
		___nextEntry_64 = value;
		Il2CppCodeGenWriteBarrier((&___nextEntry_64), value);
	}

	inline static int32_t get_offset_of_hasChild_65() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___hasChild_65)); }
	inline bool get_hasChild_65() const { return ___hasChild_65; }
	inline bool* get_address_of_hasChild_65() { return &___hasChild_65; }
	inline void set_hasChild_65(bool value)
	{
		___hasChild_65 = value;
	}

	inline static int32_t get_offset_of_stateHistory_66() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___stateHistory_66)); }
	inline HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * get_stateHistory_66() const { return ___stateHistory_66; }
	inline HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE ** get_address_of_stateHistory_66() { return &___stateHistory_66; }
	inline void set_stateHistory_66(HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * value)
	{
		___stateHistory_66 = value;
		Il2CppCodeGenWriteBarrier((&___stateHistory_66), value);
	}

	inline static int32_t get_offset_of_containerStack_67() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___containerStack_67)); }
	inline Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 * get_containerStack_67() const { return ___containerStack_67; }
	inline Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 ** get_address_of_containerStack_67() { return &___containerStack_67; }
	inline void set_containerStack_67(Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 * value)
	{
		___containerStack_67 = value;
		Il2CppCodeGenWriteBarrier((&___containerStack_67), value);
	}

	inline static int32_t get_offset_of_nameTable_68() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___nameTable_68)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_68() const { return ___nameTable_68; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_68() { return &___nameTable_68; }
	inline void set_nameTable_68(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_68 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_68), value);
	}

	inline static int32_t get_offset_of_schemaNames_69() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___schemaNames_69)); }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * get_schemaNames_69() const { return ___schemaNames_69; }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 ** get_address_of_schemaNames_69() { return &___schemaNames_69; }
	inline void set_schemaNames_69(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * value)
	{
		___schemaNames_69 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_69), value);
	}

	inline static int32_t get_offset_of_namespaceManager_70() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___namespaceManager_70)); }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * get_namespaceManager_70() const { return ___namespaceManager_70; }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F ** get_address_of_namespaceManager_70() { return &___namespaceManager_70; }
	inline void set_namespaceManager_70(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * value)
	{
		___namespaceManager_70 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceManager_70), value);
	}

	inline static int32_t get_offset_of_canIncludeImport_71() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___canIncludeImport_71)); }
	inline bool get_canIncludeImport_71() const { return ___canIncludeImport_71; }
	inline bool* get_address_of_canIncludeImport_71() { return &___canIncludeImport_71; }
	inline void set_canIncludeImport_71(bool value)
	{
		___canIncludeImport_71 = value;
	}

	inline static int32_t get_offset_of_schema_72() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___schema_72)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_schema_72() const { return ___schema_72; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_schema_72() { return &___schema_72; }
	inline void set_schema_72(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___schema_72 = value;
		Il2CppCodeGenWriteBarrier((&___schema_72), value);
	}

	inline static int32_t get_offset_of_xso_73() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___xso_73)); }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * get_xso_73() const { return ___xso_73; }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B ** get_address_of_xso_73() { return &___xso_73; }
	inline void set_xso_73(XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * value)
	{
		___xso_73 = value;
		Il2CppCodeGenWriteBarrier((&___xso_73), value);
	}

	inline static int32_t get_offset_of_element_74() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___element_74)); }
	inline XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D * get_element_74() const { return ___element_74; }
	inline XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D ** get_address_of_element_74() { return &___element_74; }
	inline void set_element_74(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D * value)
	{
		___element_74 = value;
		Il2CppCodeGenWriteBarrier((&___element_74), value);
	}

	inline static int32_t get_offset_of_anyElement_75() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___anyElement_75)); }
	inline XmlSchemaAny_tDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2 * get_anyElement_75() const { return ___anyElement_75; }
	inline XmlSchemaAny_tDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2 ** get_address_of_anyElement_75() { return &___anyElement_75; }
	inline void set_anyElement_75(XmlSchemaAny_tDD1955591E3FE5B6731F6E7F7F55F46C65C8C7A2 * value)
	{
		___anyElement_75 = value;
		Il2CppCodeGenWriteBarrier((&___anyElement_75), value);
	}

	inline static int32_t get_offset_of_attribute_76() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___attribute_76)); }
	inline XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B * get_attribute_76() const { return ___attribute_76; }
	inline XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B ** get_address_of_attribute_76() { return &___attribute_76; }
	inline void set_attribute_76(XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B * value)
	{
		___attribute_76 = value;
		Il2CppCodeGenWriteBarrier((&___attribute_76), value);
	}

	inline static int32_t get_offset_of_anyAttribute_77() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___anyAttribute_77)); }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * get_anyAttribute_77() const { return ___anyAttribute_77; }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 ** get_address_of_anyAttribute_77() { return &___anyAttribute_77; }
	inline void set_anyAttribute_77(XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * value)
	{
		___anyAttribute_77 = value;
		Il2CppCodeGenWriteBarrier((&___anyAttribute_77), value);
	}

	inline static int32_t get_offset_of_complexType_78() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___complexType_78)); }
	inline XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * get_complexType_78() const { return ___complexType_78; }
	inline XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 ** get_address_of_complexType_78() { return &___complexType_78; }
	inline void set_complexType_78(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * value)
	{
		___complexType_78 = value;
		Il2CppCodeGenWriteBarrier((&___complexType_78), value);
	}

	inline static int32_t get_offset_of_simpleType_79() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___simpleType_79)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_simpleType_79() const { return ___simpleType_79; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_simpleType_79() { return &___simpleType_79; }
	inline void set_simpleType_79(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___simpleType_79 = value;
		Il2CppCodeGenWriteBarrier((&___simpleType_79), value);
	}

	inline static int32_t get_offset_of_complexContent_80() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___complexContent_80)); }
	inline XmlSchemaComplexContent_t0C5B5B9FA891EAAD3F277E4C494956082E127CF3 * get_complexContent_80() const { return ___complexContent_80; }
	inline XmlSchemaComplexContent_t0C5B5B9FA891EAAD3F277E4C494956082E127CF3 ** get_address_of_complexContent_80() { return &___complexContent_80; }
	inline void set_complexContent_80(XmlSchemaComplexContent_t0C5B5B9FA891EAAD3F277E4C494956082E127CF3 * value)
	{
		___complexContent_80 = value;
		Il2CppCodeGenWriteBarrier((&___complexContent_80), value);
	}

	inline static int32_t get_offset_of_complexContentExtension_81() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___complexContentExtension_81)); }
	inline XmlSchemaComplexContentExtension_t3B6C614515825C627D8DE0D040CD97753829004B * get_complexContentExtension_81() const { return ___complexContentExtension_81; }
	inline XmlSchemaComplexContentExtension_t3B6C614515825C627D8DE0D040CD97753829004B ** get_address_of_complexContentExtension_81() { return &___complexContentExtension_81; }
	inline void set_complexContentExtension_81(XmlSchemaComplexContentExtension_t3B6C614515825C627D8DE0D040CD97753829004B * value)
	{
		___complexContentExtension_81 = value;
		Il2CppCodeGenWriteBarrier((&___complexContentExtension_81), value);
	}

	inline static int32_t get_offset_of_complexContentRestriction_82() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___complexContentRestriction_82)); }
	inline XmlSchemaComplexContentRestriction_t05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0 * get_complexContentRestriction_82() const { return ___complexContentRestriction_82; }
	inline XmlSchemaComplexContentRestriction_t05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0 ** get_address_of_complexContentRestriction_82() { return &___complexContentRestriction_82; }
	inline void set_complexContentRestriction_82(XmlSchemaComplexContentRestriction_t05A88BEFCA2A78A2C90D4158D22A8C7AC9BDD4D0 * value)
	{
		___complexContentRestriction_82 = value;
		Il2CppCodeGenWriteBarrier((&___complexContentRestriction_82), value);
	}

	inline static int32_t get_offset_of_simpleContent_83() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___simpleContent_83)); }
	inline XmlSchemaSimpleContent_t47CF5CDB950EC1B3CB066BA2F9E3DAE993AD3550 * get_simpleContent_83() const { return ___simpleContent_83; }
	inline XmlSchemaSimpleContent_t47CF5CDB950EC1B3CB066BA2F9E3DAE993AD3550 ** get_address_of_simpleContent_83() { return &___simpleContent_83; }
	inline void set_simpleContent_83(XmlSchemaSimpleContent_t47CF5CDB950EC1B3CB066BA2F9E3DAE993AD3550 * value)
	{
		___simpleContent_83 = value;
		Il2CppCodeGenWriteBarrier((&___simpleContent_83), value);
	}

	inline static int32_t get_offset_of_simpleContentExtension_84() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___simpleContentExtension_84)); }
	inline XmlSchemaSimpleContentExtension_t235FC077FDE8C8796FA6A961658C42D60F4CB812 * get_simpleContentExtension_84() const { return ___simpleContentExtension_84; }
	inline XmlSchemaSimpleContentExtension_t235FC077FDE8C8796FA6A961658C42D60F4CB812 ** get_address_of_simpleContentExtension_84() { return &___simpleContentExtension_84; }
	inline void set_simpleContentExtension_84(XmlSchemaSimpleContentExtension_t235FC077FDE8C8796FA6A961658C42D60F4CB812 * value)
	{
		___simpleContentExtension_84 = value;
		Il2CppCodeGenWriteBarrier((&___simpleContentExtension_84), value);
	}

	inline static int32_t get_offset_of_simpleContentRestriction_85() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___simpleContentRestriction_85)); }
	inline XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621 * get_simpleContentRestriction_85() const { return ___simpleContentRestriction_85; }
	inline XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621 ** get_address_of_simpleContentRestriction_85() { return &___simpleContentRestriction_85; }
	inline void set_simpleContentRestriction_85(XmlSchemaSimpleContentRestriction_t6B9375A26517CBAA49F980C44A3A378A36087621 * value)
	{
		___simpleContentRestriction_85 = value;
		Il2CppCodeGenWriteBarrier((&___simpleContentRestriction_85), value);
	}

	inline static int32_t get_offset_of_simpleTypeUnion_86() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___simpleTypeUnion_86)); }
	inline XmlSchemaSimpleTypeUnion_t3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A * get_simpleTypeUnion_86() const { return ___simpleTypeUnion_86; }
	inline XmlSchemaSimpleTypeUnion_t3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A ** get_address_of_simpleTypeUnion_86() { return &___simpleTypeUnion_86; }
	inline void set_simpleTypeUnion_86(XmlSchemaSimpleTypeUnion_t3AA9DA2FE384FF51E560E05EAE5DE78F4D3AC91A * value)
	{
		___simpleTypeUnion_86 = value;
		Il2CppCodeGenWriteBarrier((&___simpleTypeUnion_86), value);
	}

	inline static int32_t get_offset_of_simpleTypeList_87() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___simpleTypeList_87)); }
	inline XmlSchemaSimpleTypeList_tE5804144CB4035DE27AA3C43233579A075469403 * get_simpleTypeList_87() const { return ___simpleTypeList_87; }
	inline XmlSchemaSimpleTypeList_tE5804144CB4035DE27AA3C43233579A075469403 ** get_address_of_simpleTypeList_87() { return &___simpleTypeList_87; }
	inline void set_simpleTypeList_87(XmlSchemaSimpleTypeList_tE5804144CB4035DE27AA3C43233579A075469403 * value)
	{
		___simpleTypeList_87 = value;
		Il2CppCodeGenWriteBarrier((&___simpleTypeList_87), value);
	}

	inline static int32_t get_offset_of_simpleTypeRestriction_88() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___simpleTypeRestriction_88)); }
	inline XmlSchemaSimpleTypeRestriction_t948C7680F4D262DA9DA7590CCA8E2F96078E490C * get_simpleTypeRestriction_88() const { return ___simpleTypeRestriction_88; }
	inline XmlSchemaSimpleTypeRestriction_t948C7680F4D262DA9DA7590CCA8E2F96078E490C ** get_address_of_simpleTypeRestriction_88() { return &___simpleTypeRestriction_88; }
	inline void set_simpleTypeRestriction_88(XmlSchemaSimpleTypeRestriction_t948C7680F4D262DA9DA7590CCA8E2F96078E490C * value)
	{
		___simpleTypeRestriction_88 = value;
		Il2CppCodeGenWriteBarrier((&___simpleTypeRestriction_88), value);
	}

	inline static int32_t get_offset_of_group_89() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___group_89)); }
	inline XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2 * get_group_89() const { return ___group_89; }
	inline XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2 ** get_address_of_group_89() { return &___group_89; }
	inline void set_group_89(XmlSchemaGroup_t574051591397CB6582B04CC50578E18BB78B5EB2 * value)
	{
		___group_89 = value;
		Il2CppCodeGenWriteBarrier((&___group_89), value);
	}

	inline static int32_t get_offset_of_groupRef_90() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___groupRef_90)); }
	inline XmlSchemaGroupRef_tEC541A27220ABE0F02B3B1FD72730E536FAEF6F3 * get_groupRef_90() const { return ___groupRef_90; }
	inline XmlSchemaGroupRef_tEC541A27220ABE0F02B3B1FD72730E536FAEF6F3 ** get_address_of_groupRef_90() { return &___groupRef_90; }
	inline void set_groupRef_90(XmlSchemaGroupRef_tEC541A27220ABE0F02B3B1FD72730E536FAEF6F3 * value)
	{
		___groupRef_90 = value;
		Il2CppCodeGenWriteBarrier((&___groupRef_90), value);
	}

	inline static int32_t get_offset_of_all_91() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___all_91)); }
	inline XmlSchemaAll_t395D3C374B5E11F2B2338F0C33E9F267D88CBA3B * get_all_91() const { return ___all_91; }
	inline XmlSchemaAll_t395D3C374B5E11F2B2338F0C33E9F267D88CBA3B ** get_address_of_all_91() { return &___all_91; }
	inline void set_all_91(XmlSchemaAll_t395D3C374B5E11F2B2338F0C33E9F267D88CBA3B * value)
	{
		___all_91 = value;
		Il2CppCodeGenWriteBarrier((&___all_91), value);
	}

	inline static int32_t get_offset_of_choice_92() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___choice_92)); }
	inline XmlSchemaChoice_tEC175BFF045FDA4DECF5A938922D6CDC4216653B * get_choice_92() const { return ___choice_92; }
	inline XmlSchemaChoice_tEC175BFF045FDA4DECF5A938922D6CDC4216653B ** get_address_of_choice_92() { return &___choice_92; }
	inline void set_choice_92(XmlSchemaChoice_tEC175BFF045FDA4DECF5A938922D6CDC4216653B * value)
	{
		___choice_92 = value;
		Il2CppCodeGenWriteBarrier((&___choice_92), value);
	}

	inline static int32_t get_offset_of_sequence_93() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___sequence_93)); }
	inline XmlSchemaSequence_tBF8498445C0FD553EF64CC240B1236CEB83FB215 * get_sequence_93() const { return ___sequence_93; }
	inline XmlSchemaSequence_tBF8498445C0FD553EF64CC240B1236CEB83FB215 ** get_address_of_sequence_93() { return &___sequence_93; }
	inline void set_sequence_93(XmlSchemaSequence_tBF8498445C0FD553EF64CC240B1236CEB83FB215 * value)
	{
		___sequence_93 = value;
		Il2CppCodeGenWriteBarrier((&___sequence_93), value);
	}

	inline static int32_t get_offset_of_particle_94() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___particle_94)); }
	inline XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * get_particle_94() const { return ___particle_94; }
	inline XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 ** get_address_of_particle_94() { return &___particle_94; }
	inline void set_particle_94(XmlSchemaParticle_tB790C2363C7041009BE94F64F031BDDA1E389671 * value)
	{
		___particle_94 = value;
		Il2CppCodeGenWriteBarrier((&___particle_94), value);
	}

	inline static int32_t get_offset_of_attributeGroup_95() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___attributeGroup_95)); }
	inline XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E * get_attributeGroup_95() const { return ___attributeGroup_95; }
	inline XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E ** get_address_of_attributeGroup_95() { return &___attributeGroup_95; }
	inline void set_attributeGroup_95(XmlSchemaAttributeGroup_tE80117D629E6793A9E1DA428A472324C4243509E * value)
	{
		___attributeGroup_95 = value;
		Il2CppCodeGenWriteBarrier((&___attributeGroup_95), value);
	}

	inline static int32_t get_offset_of_attributeGroupRef_96() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___attributeGroupRef_96)); }
	inline XmlSchemaAttributeGroupRef_t5DA5E994E5C8A3E844A6B88B5E9AEA8195B09BD4 * get_attributeGroupRef_96() const { return ___attributeGroupRef_96; }
	inline XmlSchemaAttributeGroupRef_t5DA5E994E5C8A3E844A6B88B5E9AEA8195B09BD4 ** get_address_of_attributeGroupRef_96() { return &___attributeGroupRef_96; }
	inline void set_attributeGroupRef_96(XmlSchemaAttributeGroupRef_t5DA5E994E5C8A3E844A6B88B5E9AEA8195B09BD4 * value)
	{
		___attributeGroupRef_96 = value;
		Il2CppCodeGenWriteBarrier((&___attributeGroupRef_96), value);
	}

	inline static int32_t get_offset_of_notation_97() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___notation_97)); }
	inline XmlSchemaNotation_tA67925FC817C85C8CB753EBAFBB7EA0AE414D41D * get_notation_97() const { return ___notation_97; }
	inline XmlSchemaNotation_tA67925FC817C85C8CB753EBAFBB7EA0AE414D41D ** get_address_of_notation_97() { return &___notation_97; }
	inline void set_notation_97(XmlSchemaNotation_tA67925FC817C85C8CB753EBAFBB7EA0AE414D41D * value)
	{
		___notation_97 = value;
		Il2CppCodeGenWriteBarrier((&___notation_97), value);
	}

	inline static int32_t get_offset_of_identityConstraint_98() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___identityConstraint_98)); }
	inline XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95 * get_identityConstraint_98() const { return ___identityConstraint_98; }
	inline XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95 ** get_address_of_identityConstraint_98() { return &___identityConstraint_98; }
	inline void set_identityConstraint_98(XmlSchemaIdentityConstraint_t9351F3E86E52F1C2FC65C0F2266BE3FAF466EA95 * value)
	{
		___identityConstraint_98 = value;
		Il2CppCodeGenWriteBarrier((&___identityConstraint_98), value);
	}

	inline static int32_t get_offset_of_xpath_99() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___xpath_99)); }
	inline XmlSchemaXPath_t6D090E4243F2A15A6A5E4EF5B17B0740711C6C34 * get_xpath_99() const { return ___xpath_99; }
	inline XmlSchemaXPath_t6D090E4243F2A15A6A5E4EF5B17B0740711C6C34 ** get_address_of_xpath_99() { return &___xpath_99; }
	inline void set_xpath_99(XmlSchemaXPath_t6D090E4243F2A15A6A5E4EF5B17B0740711C6C34 * value)
	{
		___xpath_99 = value;
		Il2CppCodeGenWriteBarrier((&___xpath_99), value);
	}

	inline static int32_t get_offset_of_include_100() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___include_100)); }
	inline XmlSchemaInclude_tD337A52B31C9B3195CE4272AF057E286CDD34001 * get_include_100() const { return ___include_100; }
	inline XmlSchemaInclude_tD337A52B31C9B3195CE4272AF057E286CDD34001 ** get_address_of_include_100() { return &___include_100; }
	inline void set_include_100(XmlSchemaInclude_tD337A52B31C9B3195CE4272AF057E286CDD34001 * value)
	{
		___include_100 = value;
		Il2CppCodeGenWriteBarrier((&___include_100), value);
	}

	inline static int32_t get_offset_of_import_101() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___import_101)); }
	inline XmlSchemaImport_t4FC11FF359600C711AFFEF2DC569810456D8CC93 * get_import_101() const { return ___import_101; }
	inline XmlSchemaImport_t4FC11FF359600C711AFFEF2DC569810456D8CC93 ** get_address_of_import_101() { return &___import_101; }
	inline void set_import_101(XmlSchemaImport_t4FC11FF359600C711AFFEF2DC569810456D8CC93 * value)
	{
		___import_101 = value;
		Il2CppCodeGenWriteBarrier((&___import_101), value);
	}

	inline static int32_t get_offset_of_annotation_102() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___annotation_102)); }
	inline XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 * get_annotation_102() const { return ___annotation_102; }
	inline XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 ** get_address_of_annotation_102() { return &___annotation_102; }
	inline void set_annotation_102(XmlSchemaAnnotation_tC8DD8E9A26CD3A12B16C25F0341600FF69C3B725 * value)
	{
		___annotation_102 = value;
		Il2CppCodeGenWriteBarrier((&___annotation_102), value);
	}

	inline static int32_t get_offset_of_appInfo_103() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___appInfo_103)); }
	inline XmlSchemaAppInfo_t5E2FEE4B31F35CD076CC2F47DA5CB82DA139A860 * get_appInfo_103() const { return ___appInfo_103; }
	inline XmlSchemaAppInfo_t5E2FEE4B31F35CD076CC2F47DA5CB82DA139A860 ** get_address_of_appInfo_103() { return &___appInfo_103; }
	inline void set_appInfo_103(XmlSchemaAppInfo_t5E2FEE4B31F35CD076CC2F47DA5CB82DA139A860 * value)
	{
		___appInfo_103 = value;
		Il2CppCodeGenWriteBarrier((&___appInfo_103), value);
	}

	inline static int32_t get_offset_of_documentation_104() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___documentation_104)); }
	inline XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE * get_documentation_104() const { return ___documentation_104; }
	inline XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE ** get_address_of_documentation_104() { return &___documentation_104; }
	inline void set_documentation_104(XmlSchemaDocumentation_t95A86FC828CDFF9FCC86D8F7774C6496591E67EE * value)
	{
		___documentation_104 = value;
		Il2CppCodeGenWriteBarrier((&___documentation_104), value);
	}

	inline static int32_t get_offset_of_facet_105() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___facet_105)); }
	inline XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250 * get_facet_105() const { return ___facet_105; }
	inline XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250 ** get_address_of_facet_105() { return &___facet_105; }
	inline void set_facet_105(XmlSchemaFacet_tB6316660F75E032055B12043237A0132A1919250 * value)
	{
		___facet_105 = value;
		Il2CppCodeGenWriteBarrier((&___facet_105), value);
	}

	inline static int32_t get_offset_of_markup_106() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___markup_106)); }
	inline XmlNodeU5BU5D_tBF402FAC28AE903AC3FF845775405A2274B701AF* get_markup_106() const { return ___markup_106; }
	inline XmlNodeU5BU5D_tBF402FAC28AE903AC3FF845775405A2274B701AF** get_address_of_markup_106() { return &___markup_106; }
	inline void set_markup_106(XmlNodeU5BU5D_tBF402FAC28AE903AC3FF845775405A2274B701AF* value)
	{
		___markup_106 = value;
		Il2CppCodeGenWriteBarrier((&___markup_106), value);
	}

	inline static int32_t get_offset_of_redefine_107() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___redefine_107)); }
	inline XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B * get_redefine_107() const { return ___redefine_107; }
	inline XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B ** get_address_of_redefine_107() { return &___redefine_107; }
	inline void set_redefine_107(XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B * value)
	{
		___redefine_107 = value;
		Il2CppCodeGenWriteBarrier((&___redefine_107), value);
	}

	inline static int32_t get_offset_of_validationEventHandler_108() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___validationEventHandler_108)); }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * get_validationEventHandler_108() const { return ___validationEventHandler_108; }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF ** get_address_of_validationEventHandler_108() { return &___validationEventHandler_108; }
	inline void set_validationEventHandler_108(ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * value)
	{
		___validationEventHandler_108 = value;
		Il2CppCodeGenWriteBarrier((&___validationEventHandler_108), value);
	}

	inline static int32_t get_offset_of_unhandledAttributes_109() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___unhandledAttributes_109)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_unhandledAttributes_109() const { return ___unhandledAttributes_109; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_unhandledAttributes_109() { return &___unhandledAttributes_109; }
	inline void set_unhandledAttributes_109(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___unhandledAttributes_109 = value;
		Il2CppCodeGenWriteBarrier((&___unhandledAttributes_109), value);
	}

	inline static int32_t get_offset_of_namespaces_110() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3, ___namespaces_110)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_namespaces_110() const { return ___namespaces_110; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_namespaces_110() { return &___namespaces_110; }
	inline void set_namespaces_110(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___namespaces_110 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_110), value);
	}
};

struct XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields
{
public:
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SchemaElement
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___SchemaElement_0;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SchemaSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___SchemaSubelements_1;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::AttributeSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___AttributeSubelements_2;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::ElementSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___ElementSubelements_3;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::ComplexTypeSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___ComplexTypeSubelements_4;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SimpleContentSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___SimpleContentSubelements_5;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SimpleContentExtensionSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___SimpleContentExtensionSubelements_6;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SimpleContentRestrictionSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___SimpleContentRestrictionSubelements_7;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::ComplexContentSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___ComplexContentSubelements_8;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::ComplexContentExtensionSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___ComplexContentExtensionSubelements_9;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::ComplexContentRestrictionSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___ComplexContentRestrictionSubelements_10;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SimpleTypeSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___SimpleTypeSubelements_11;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SimpleTypeRestrictionSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___SimpleTypeRestrictionSubelements_12;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SimpleTypeListSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___SimpleTypeListSubelements_13;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SimpleTypeUnionSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___SimpleTypeUnionSubelements_14;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::RedefineSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___RedefineSubelements_15;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::AttributeGroupSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___AttributeGroupSubelements_16;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::GroupSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___GroupSubelements_17;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::AllSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___AllSubelements_18;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::ChoiceSequenceSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___ChoiceSequenceSubelements_19;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::IdentityConstraintSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___IdentityConstraintSubelements_20;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::AnnotationSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___AnnotationSubelements_21;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::AnnotatedSubelements
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___AnnotatedSubelements_22;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SchemaAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___SchemaAttributes_23;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::AttributeAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___AttributeAttributes_24;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::ElementAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___ElementAttributes_25;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::ComplexTypeAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___ComplexTypeAttributes_26;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SimpleContentAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___SimpleContentAttributes_27;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SimpleContentExtensionAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___SimpleContentExtensionAttributes_28;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SimpleContentRestrictionAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___SimpleContentRestrictionAttributes_29;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::ComplexContentAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___ComplexContentAttributes_30;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::ComplexContentExtensionAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___ComplexContentExtensionAttributes_31;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::ComplexContentRestrictionAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___ComplexContentRestrictionAttributes_32;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SimpleTypeAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___SimpleTypeAttributes_33;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SimpleTypeRestrictionAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___SimpleTypeRestrictionAttributes_34;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SimpleTypeUnionAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___SimpleTypeUnionAttributes_35;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SimpleTypeListAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___SimpleTypeListAttributes_36;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::AttributeGroupAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___AttributeGroupAttributes_37;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::AttributeGroupRefAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___AttributeGroupRefAttributes_38;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::GroupAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___GroupAttributes_39;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::GroupRefAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___GroupRefAttributes_40;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::ParticleAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___ParticleAttributes_41;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::AnyAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___AnyAttributes_42;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::IdentityConstraintAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___IdentityConstraintAttributes_43;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SelectorAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___SelectorAttributes_44;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::FieldAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___FieldAttributes_45;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::NotationAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___NotationAttributes_46;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::IncludeAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___IncludeAttributes_47;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::ImportAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___ImportAttributes_48;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::FacetAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___FacetAttributes_49;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::AnyAttributeAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___AnyAttributeAttributes_50;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::DocumentationAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___DocumentationAttributes_51;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::AppinfoAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___AppinfoAttributes_52;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::RedefineAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___RedefineAttributes_53;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::AnnotationAttributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___AnnotationAttributes_54;
	// System.Xml.Schema.XsdBuilder/XsdEntry[] System.Xml.Schema.XsdBuilder::SchemaEntries
	XsdEntryU5BU5D_t612F7D8AE3D932202C80449A380B9D5A54E3106A* ___SchemaEntries_55;
	// System.Int32[] System.Xml.Schema.XsdBuilder::DerivationMethodValues
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DerivationMethodValues_56;
	// System.String[] System.Xml.Schema.XsdBuilder::DerivationMethodStrings
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___DerivationMethodStrings_57;
	// System.String[] System.Xml.Schema.XsdBuilder::FormStringValues
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___FormStringValues_58;
	// System.String[] System.Xml.Schema.XsdBuilder::UseStringValues
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___UseStringValues_59;
	// System.String[] System.Xml.Schema.XsdBuilder::ProcessContentsStringValues
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___ProcessContentsStringValues_60;

public:
	inline static int32_t get_offset_of_SchemaElement_0() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SchemaElement_0)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_SchemaElement_0() const { return ___SchemaElement_0; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_SchemaElement_0() { return &___SchemaElement_0; }
	inline void set_SchemaElement_0(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___SchemaElement_0 = value;
		Il2CppCodeGenWriteBarrier((&___SchemaElement_0), value);
	}

	inline static int32_t get_offset_of_SchemaSubelements_1() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SchemaSubelements_1)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_SchemaSubelements_1() const { return ___SchemaSubelements_1; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_SchemaSubelements_1() { return &___SchemaSubelements_1; }
	inline void set_SchemaSubelements_1(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___SchemaSubelements_1 = value;
		Il2CppCodeGenWriteBarrier((&___SchemaSubelements_1), value);
	}

	inline static int32_t get_offset_of_AttributeSubelements_2() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___AttributeSubelements_2)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_AttributeSubelements_2() const { return ___AttributeSubelements_2; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_AttributeSubelements_2() { return &___AttributeSubelements_2; }
	inline void set_AttributeSubelements_2(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___AttributeSubelements_2 = value;
		Il2CppCodeGenWriteBarrier((&___AttributeSubelements_2), value);
	}

	inline static int32_t get_offset_of_ElementSubelements_3() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___ElementSubelements_3)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_ElementSubelements_3() const { return ___ElementSubelements_3; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_ElementSubelements_3() { return &___ElementSubelements_3; }
	inline void set_ElementSubelements_3(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___ElementSubelements_3 = value;
		Il2CppCodeGenWriteBarrier((&___ElementSubelements_3), value);
	}

	inline static int32_t get_offset_of_ComplexTypeSubelements_4() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___ComplexTypeSubelements_4)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_ComplexTypeSubelements_4() const { return ___ComplexTypeSubelements_4; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_ComplexTypeSubelements_4() { return &___ComplexTypeSubelements_4; }
	inline void set_ComplexTypeSubelements_4(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___ComplexTypeSubelements_4 = value;
		Il2CppCodeGenWriteBarrier((&___ComplexTypeSubelements_4), value);
	}

	inline static int32_t get_offset_of_SimpleContentSubelements_5() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SimpleContentSubelements_5)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_SimpleContentSubelements_5() const { return ___SimpleContentSubelements_5; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_SimpleContentSubelements_5() { return &___SimpleContentSubelements_5; }
	inline void set_SimpleContentSubelements_5(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___SimpleContentSubelements_5 = value;
		Il2CppCodeGenWriteBarrier((&___SimpleContentSubelements_5), value);
	}

	inline static int32_t get_offset_of_SimpleContentExtensionSubelements_6() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SimpleContentExtensionSubelements_6)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_SimpleContentExtensionSubelements_6() const { return ___SimpleContentExtensionSubelements_6; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_SimpleContentExtensionSubelements_6() { return &___SimpleContentExtensionSubelements_6; }
	inline void set_SimpleContentExtensionSubelements_6(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___SimpleContentExtensionSubelements_6 = value;
		Il2CppCodeGenWriteBarrier((&___SimpleContentExtensionSubelements_6), value);
	}

	inline static int32_t get_offset_of_SimpleContentRestrictionSubelements_7() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SimpleContentRestrictionSubelements_7)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_SimpleContentRestrictionSubelements_7() const { return ___SimpleContentRestrictionSubelements_7; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_SimpleContentRestrictionSubelements_7() { return &___SimpleContentRestrictionSubelements_7; }
	inline void set_SimpleContentRestrictionSubelements_7(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___SimpleContentRestrictionSubelements_7 = value;
		Il2CppCodeGenWriteBarrier((&___SimpleContentRestrictionSubelements_7), value);
	}

	inline static int32_t get_offset_of_ComplexContentSubelements_8() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___ComplexContentSubelements_8)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_ComplexContentSubelements_8() const { return ___ComplexContentSubelements_8; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_ComplexContentSubelements_8() { return &___ComplexContentSubelements_8; }
	inline void set_ComplexContentSubelements_8(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___ComplexContentSubelements_8 = value;
		Il2CppCodeGenWriteBarrier((&___ComplexContentSubelements_8), value);
	}

	inline static int32_t get_offset_of_ComplexContentExtensionSubelements_9() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___ComplexContentExtensionSubelements_9)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_ComplexContentExtensionSubelements_9() const { return ___ComplexContentExtensionSubelements_9; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_ComplexContentExtensionSubelements_9() { return &___ComplexContentExtensionSubelements_9; }
	inline void set_ComplexContentExtensionSubelements_9(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___ComplexContentExtensionSubelements_9 = value;
		Il2CppCodeGenWriteBarrier((&___ComplexContentExtensionSubelements_9), value);
	}

	inline static int32_t get_offset_of_ComplexContentRestrictionSubelements_10() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___ComplexContentRestrictionSubelements_10)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_ComplexContentRestrictionSubelements_10() const { return ___ComplexContentRestrictionSubelements_10; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_ComplexContentRestrictionSubelements_10() { return &___ComplexContentRestrictionSubelements_10; }
	inline void set_ComplexContentRestrictionSubelements_10(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___ComplexContentRestrictionSubelements_10 = value;
		Il2CppCodeGenWriteBarrier((&___ComplexContentRestrictionSubelements_10), value);
	}

	inline static int32_t get_offset_of_SimpleTypeSubelements_11() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SimpleTypeSubelements_11)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_SimpleTypeSubelements_11() const { return ___SimpleTypeSubelements_11; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_SimpleTypeSubelements_11() { return &___SimpleTypeSubelements_11; }
	inline void set_SimpleTypeSubelements_11(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___SimpleTypeSubelements_11 = value;
		Il2CppCodeGenWriteBarrier((&___SimpleTypeSubelements_11), value);
	}

	inline static int32_t get_offset_of_SimpleTypeRestrictionSubelements_12() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SimpleTypeRestrictionSubelements_12)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_SimpleTypeRestrictionSubelements_12() const { return ___SimpleTypeRestrictionSubelements_12; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_SimpleTypeRestrictionSubelements_12() { return &___SimpleTypeRestrictionSubelements_12; }
	inline void set_SimpleTypeRestrictionSubelements_12(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___SimpleTypeRestrictionSubelements_12 = value;
		Il2CppCodeGenWriteBarrier((&___SimpleTypeRestrictionSubelements_12), value);
	}

	inline static int32_t get_offset_of_SimpleTypeListSubelements_13() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SimpleTypeListSubelements_13)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_SimpleTypeListSubelements_13() const { return ___SimpleTypeListSubelements_13; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_SimpleTypeListSubelements_13() { return &___SimpleTypeListSubelements_13; }
	inline void set_SimpleTypeListSubelements_13(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___SimpleTypeListSubelements_13 = value;
		Il2CppCodeGenWriteBarrier((&___SimpleTypeListSubelements_13), value);
	}

	inline static int32_t get_offset_of_SimpleTypeUnionSubelements_14() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SimpleTypeUnionSubelements_14)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_SimpleTypeUnionSubelements_14() const { return ___SimpleTypeUnionSubelements_14; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_SimpleTypeUnionSubelements_14() { return &___SimpleTypeUnionSubelements_14; }
	inline void set_SimpleTypeUnionSubelements_14(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___SimpleTypeUnionSubelements_14 = value;
		Il2CppCodeGenWriteBarrier((&___SimpleTypeUnionSubelements_14), value);
	}

	inline static int32_t get_offset_of_RedefineSubelements_15() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___RedefineSubelements_15)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_RedefineSubelements_15() const { return ___RedefineSubelements_15; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_RedefineSubelements_15() { return &___RedefineSubelements_15; }
	inline void set_RedefineSubelements_15(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___RedefineSubelements_15 = value;
		Il2CppCodeGenWriteBarrier((&___RedefineSubelements_15), value);
	}

	inline static int32_t get_offset_of_AttributeGroupSubelements_16() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___AttributeGroupSubelements_16)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_AttributeGroupSubelements_16() const { return ___AttributeGroupSubelements_16; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_AttributeGroupSubelements_16() { return &___AttributeGroupSubelements_16; }
	inline void set_AttributeGroupSubelements_16(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___AttributeGroupSubelements_16 = value;
		Il2CppCodeGenWriteBarrier((&___AttributeGroupSubelements_16), value);
	}

	inline static int32_t get_offset_of_GroupSubelements_17() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___GroupSubelements_17)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_GroupSubelements_17() const { return ___GroupSubelements_17; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_GroupSubelements_17() { return &___GroupSubelements_17; }
	inline void set_GroupSubelements_17(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___GroupSubelements_17 = value;
		Il2CppCodeGenWriteBarrier((&___GroupSubelements_17), value);
	}

	inline static int32_t get_offset_of_AllSubelements_18() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___AllSubelements_18)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_AllSubelements_18() const { return ___AllSubelements_18; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_AllSubelements_18() { return &___AllSubelements_18; }
	inline void set_AllSubelements_18(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___AllSubelements_18 = value;
		Il2CppCodeGenWriteBarrier((&___AllSubelements_18), value);
	}

	inline static int32_t get_offset_of_ChoiceSequenceSubelements_19() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___ChoiceSequenceSubelements_19)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_ChoiceSequenceSubelements_19() const { return ___ChoiceSequenceSubelements_19; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_ChoiceSequenceSubelements_19() { return &___ChoiceSequenceSubelements_19; }
	inline void set_ChoiceSequenceSubelements_19(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___ChoiceSequenceSubelements_19 = value;
		Il2CppCodeGenWriteBarrier((&___ChoiceSequenceSubelements_19), value);
	}

	inline static int32_t get_offset_of_IdentityConstraintSubelements_20() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___IdentityConstraintSubelements_20)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_IdentityConstraintSubelements_20() const { return ___IdentityConstraintSubelements_20; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_IdentityConstraintSubelements_20() { return &___IdentityConstraintSubelements_20; }
	inline void set_IdentityConstraintSubelements_20(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___IdentityConstraintSubelements_20 = value;
		Il2CppCodeGenWriteBarrier((&___IdentityConstraintSubelements_20), value);
	}

	inline static int32_t get_offset_of_AnnotationSubelements_21() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___AnnotationSubelements_21)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_AnnotationSubelements_21() const { return ___AnnotationSubelements_21; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_AnnotationSubelements_21() { return &___AnnotationSubelements_21; }
	inline void set_AnnotationSubelements_21(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___AnnotationSubelements_21 = value;
		Il2CppCodeGenWriteBarrier((&___AnnotationSubelements_21), value);
	}

	inline static int32_t get_offset_of_AnnotatedSubelements_22() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___AnnotatedSubelements_22)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_AnnotatedSubelements_22() const { return ___AnnotatedSubelements_22; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_AnnotatedSubelements_22() { return &___AnnotatedSubelements_22; }
	inline void set_AnnotatedSubelements_22(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___AnnotatedSubelements_22 = value;
		Il2CppCodeGenWriteBarrier((&___AnnotatedSubelements_22), value);
	}

	inline static int32_t get_offset_of_SchemaAttributes_23() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SchemaAttributes_23)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_SchemaAttributes_23() const { return ___SchemaAttributes_23; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_SchemaAttributes_23() { return &___SchemaAttributes_23; }
	inline void set_SchemaAttributes_23(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___SchemaAttributes_23 = value;
		Il2CppCodeGenWriteBarrier((&___SchemaAttributes_23), value);
	}

	inline static int32_t get_offset_of_AttributeAttributes_24() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___AttributeAttributes_24)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_AttributeAttributes_24() const { return ___AttributeAttributes_24; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_AttributeAttributes_24() { return &___AttributeAttributes_24; }
	inline void set_AttributeAttributes_24(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___AttributeAttributes_24 = value;
		Il2CppCodeGenWriteBarrier((&___AttributeAttributes_24), value);
	}

	inline static int32_t get_offset_of_ElementAttributes_25() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___ElementAttributes_25)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_ElementAttributes_25() const { return ___ElementAttributes_25; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_ElementAttributes_25() { return &___ElementAttributes_25; }
	inline void set_ElementAttributes_25(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___ElementAttributes_25 = value;
		Il2CppCodeGenWriteBarrier((&___ElementAttributes_25), value);
	}

	inline static int32_t get_offset_of_ComplexTypeAttributes_26() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___ComplexTypeAttributes_26)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_ComplexTypeAttributes_26() const { return ___ComplexTypeAttributes_26; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_ComplexTypeAttributes_26() { return &___ComplexTypeAttributes_26; }
	inline void set_ComplexTypeAttributes_26(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___ComplexTypeAttributes_26 = value;
		Il2CppCodeGenWriteBarrier((&___ComplexTypeAttributes_26), value);
	}

	inline static int32_t get_offset_of_SimpleContentAttributes_27() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SimpleContentAttributes_27)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_SimpleContentAttributes_27() const { return ___SimpleContentAttributes_27; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_SimpleContentAttributes_27() { return &___SimpleContentAttributes_27; }
	inline void set_SimpleContentAttributes_27(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___SimpleContentAttributes_27 = value;
		Il2CppCodeGenWriteBarrier((&___SimpleContentAttributes_27), value);
	}

	inline static int32_t get_offset_of_SimpleContentExtensionAttributes_28() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SimpleContentExtensionAttributes_28)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_SimpleContentExtensionAttributes_28() const { return ___SimpleContentExtensionAttributes_28; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_SimpleContentExtensionAttributes_28() { return &___SimpleContentExtensionAttributes_28; }
	inline void set_SimpleContentExtensionAttributes_28(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___SimpleContentExtensionAttributes_28 = value;
		Il2CppCodeGenWriteBarrier((&___SimpleContentExtensionAttributes_28), value);
	}

	inline static int32_t get_offset_of_SimpleContentRestrictionAttributes_29() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SimpleContentRestrictionAttributes_29)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_SimpleContentRestrictionAttributes_29() const { return ___SimpleContentRestrictionAttributes_29; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_SimpleContentRestrictionAttributes_29() { return &___SimpleContentRestrictionAttributes_29; }
	inline void set_SimpleContentRestrictionAttributes_29(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___SimpleContentRestrictionAttributes_29 = value;
		Il2CppCodeGenWriteBarrier((&___SimpleContentRestrictionAttributes_29), value);
	}

	inline static int32_t get_offset_of_ComplexContentAttributes_30() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___ComplexContentAttributes_30)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_ComplexContentAttributes_30() const { return ___ComplexContentAttributes_30; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_ComplexContentAttributes_30() { return &___ComplexContentAttributes_30; }
	inline void set_ComplexContentAttributes_30(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___ComplexContentAttributes_30 = value;
		Il2CppCodeGenWriteBarrier((&___ComplexContentAttributes_30), value);
	}

	inline static int32_t get_offset_of_ComplexContentExtensionAttributes_31() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___ComplexContentExtensionAttributes_31)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_ComplexContentExtensionAttributes_31() const { return ___ComplexContentExtensionAttributes_31; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_ComplexContentExtensionAttributes_31() { return &___ComplexContentExtensionAttributes_31; }
	inline void set_ComplexContentExtensionAttributes_31(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___ComplexContentExtensionAttributes_31 = value;
		Il2CppCodeGenWriteBarrier((&___ComplexContentExtensionAttributes_31), value);
	}

	inline static int32_t get_offset_of_ComplexContentRestrictionAttributes_32() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___ComplexContentRestrictionAttributes_32)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_ComplexContentRestrictionAttributes_32() const { return ___ComplexContentRestrictionAttributes_32; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_ComplexContentRestrictionAttributes_32() { return &___ComplexContentRestrictionAttributes_32; }
	inline void set_ComplexContentRestrictionAttributes_32(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___ComplexContentRestrictionAttributes_32 = value;
		Il2CppCodeGenWriteBarrier((&___ComplexContentRestrictionAttributes_32), value);
	}

	inline static int32_t get_offset_of_SimpleTypeAttributes_33() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SimpleTypeAttributes_33)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_SimpleTypeAttributes_33() const { return ___SimpleTypeAttributes_33; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_SimpleTypeAttributes_33() { return &___SimpleTypeAttributes_33; }
	inline void set_SimpleTypeAttributes_33(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___SimpleTypeAttributes_33 = value;
		Il2CppCodeGenWriteBarrier((&___SimpleTypeAttributes_33), value);
	}

	inline static int32_t get_offset_of_SimpleTypeRestrictionAttributes_34() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SimpleTypeRestrictionAttributes_34)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_SimpleTypeRestrictionAttributes_34() const { return ___SimpleTypeRestrictionAttributes_34; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_SimpleTypeRestrictionAttributes_34() { return &___SimpleTypeRestrictionAttributes_34; }
	inline void set_SimpleTypeRestrictionAttributes_34(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___SimpleTypeRestrictionAttributes_34 = value;
		Il2CppCodeGenWriteBarrier((&___SimpleTypeRestrictionAttributes_34), value);
	}

	inline static int32_t get_offset_of_SimpleTypeUnionAttributes_35() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SimpleTypeUnionAttributes_35)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_SimpleTypeUnionAttributes_35() const { return ___SimpleTypeUnionAttributes_35; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_SimpleTypeUnionAttributes_35() { return &___SimpleTypeUnionAttributes_35; }
	inline void set_SimpleTypeUnionAttributes_35(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___SimpleTypeUnionAttributes_35 = value;
		Il2CppCodeGenWriteBarrier((&___SimpleTypeUnionAttributes_35), value);
	}

	inline static int32_t get_offset_of_SimpleTypeListAttributes_36() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SimpleTypeListAttributes_36)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_SimpleTypeListAttributes_36() const { return ___SimpleTypeListAttributes_36; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_SimpleTypeListAttributes_36() { return &___SimpleTypeListAttributes_36; }
	inline void set_SimpleTypeListAttributes_36(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___SimpleTypeListAttributes_36 = value;
		Il2CppCodeGenWriteBarrier((&___SimpleTypeListAttributes_36), value);
	}

	inline static int32_t get_offset_of_AttributeGroupAttributes_37() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___AttributeGroupAttributes_37)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_AttributeGroupAttributes_37() const { return ___AttributeGroupAttributes_37; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_AttributeGroupAttributes_37() { return &___AttributeGroupAttributes_37; }
	inline void set_AttributeGroupAttributes_37(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___AttributeGroupAttributes_37 = value;
		Il2CppCodeGenWriteBarrier((&___AttributeGroupAttributes_37), value);
	}

	inline static int32_t get_offset_of_AttributeGroupRefAttributes_38() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___AttributeGroupRefAttributes_38)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_AttributeGroupRefAttributes_38() const { return ___AttributeGroupRefAttributes_38; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_AttributeGroupRefAttributes_38() { return &___AttributeGroupRefAttributes_38; }
	inline void set_AttributeGroupRefAttributes_38(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___AttributeGroupRefAttributes_38 = value;
		Il2CppCodeGenWriteBarrier((&___AttributeGroupRefAttributes_38), value);
	}

	inline static int32_t get_offset_of_GroupAttributes_39() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___GroupAttributes_39)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_GroupAttributes_39() const { return ___GroupAttributes_39; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_GroupAttributes_39() { return &___GroupAttributes_39; }
	inline void set_GroupAttributes_39(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___GroupAttributes_39 = value;
		Il2CppCodeGenWriteBarrier((&___GroupAttributes_39), value);
	}

	inline static int32_t get_offset_of_GroupRefAttributes_40() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___GroupRefAttributes_40)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_GroupRefAttributes_40() const { return ___GroupRefAttributes_40; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_GroupRefAttributes_40() { return &___GroupRefAttributes_40; }
	inline void set_GroupRefAttributes_40(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___GroupRefAttributes_40 = value;
		Il2CppCodeGenWriteBarrier((&___GroupRefAttributes_40), value);
	}

	inline static int32_t get_offset_of_ParticleAttributes_41() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___ParticleAttributes_41)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_ParticleAttributes_41() const { return ___ParticleAttributes_41; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_ParticleAttributes_41() { return &___ParticleAttributes_41; }
	inline void set_ParticleAttributes_41(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___ParticleAttributes_41 = value;
		Il2CppCodeGenWriteBarrier((&___ParticleAttributes_41), value);
	}

	inline static int32_t get_offset_of_AnyAttributes_42() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___AnyAttributes_42)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_AnyAttributes_42() const { return ___AnyAttributes_42; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_AnyAttributes_42() { return &___AnyAttributes_42; }
	inline void set_AnyAttributes_42(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___AnyAttributes_42 = value;
		Il2CppCodeGenWriteBarrier((&___AnyAttributes_42), value);
	}

	inline static int32_t get_offset_of_IdentityConstraintAttributes_43() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___IdentityConstraintAttributes_43)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_IdentityConstraintAttributes_43() const { return ___IdentityConstraintAttributes_43; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_IdentityConstraintAttributes_43() { return &___IdentityConstraintAttributes_43; }
	inline void set_IdentityConstraintAttributes_43(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___IdentityConstraintAttributes_43 = value;
		Il2CppCodeGenWriteBarrier((&___IdentityConstraintAttributes_43), value);
	}

	inline static int32_t get_offset_of_SelectorAttributes_44() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SelectorAttributes_44)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_SelectorAttributes_44() const { return ___SelectorAttributes_44; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_SelectorAttributes_44() { return &___SelectorAttributes_44; }
	inline void set_SelectorAttributes_44(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___SelectorAttributes_44 = value;
		Il2CppCodeGenWriteBarrier((&___SelectorAttributes_44), value);
	}

	inline static int32_t get_offset_of_FieldAttributes_45() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___FieldAttributes_45)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_FieldAttributes_45() const { return ___FieldAttributes_45; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_FieldAttributes_45() { return &___FieldAttributes_45; }
	inline void set_FieldAttributes_45(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___FieldAttributes_45 = value;
		Il2CppCodeGenWriteBarrier((&___FieldAttributes_45), value);
	}

	inline static int32_t get_offset_of_NotationAttributes_46() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___NotationAttributes_46)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_NotationAttributes_46() const { return ___NotationAttributes_46; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_NotationAttributes_46() { return &___NotationAttributes_46; }
	inline void set_NotationAttributes_46(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___NotationAttributes_46 = value;
		Il2CppCodeGenWriteBarrier((&___NotationAttributes_46), value);
	}

	inline static int32_t get_offset_of_IncludeAttributes_47() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___IncludeAttributes_47)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_IncludeAttributes_47() const { return ___IncludeAttributes_47; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_IncludeAttributes_47() { return &___IncludeAttributes_47; }
	inline void set_IncludeAttributes_47(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___IncludeAttributes_47 = value;
		Il2CppCodeGenWriteBarrier((&___IncludeAttributes_47), value);
	}

	inline static int32_t get_offset_of_ImportAttributes_48() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___ImportAttributes_48)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_ImportAttributes_48() const { return ___ImportAttributes_48; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_ImportAttributes_48() { return &___ImportAttributes_48; }
	inline void set_ImportAttributes_48(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___ImportAttributes_48 = value;
		Il2CppCodeGenWriteBarrier((&___ImportAttributes_48), value);
	}

	inline static int32_t get_offset_of_FacetAttributes_49() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___FacetAttributes_49)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_FacetAttributes_49() const { return ___FacetAttributes_49; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_FacetAttributes_49() { return &___FacetAttributes_49; }
	inline void set_FacetAttributes_49(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___FacetAttributes_49 = value;
		Il2CppCodeGenWriteBarrier((&___FacetAttributes_49), value);
	}

	inline static int32_t get_offset_of_AnyAttributeAttributes_50() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___AnyAttributeAttributes_50)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_AnyAttributeAttributes_50() const { return ___AnyAttributeAttributes_50; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_AnyAttributeAttributes_50() { return &___AnyAttributeAttributes_50; }
	inline void set_AnyAttributeAttributes_50(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___AnyAttributeAttributes_50 = value;
		Il2CppCodeGenWriteBarrier((&___AnyAttributeAttributes_50), value);
	}

	inline static int32_t get_offset_of_DocumentationAttributes_51() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___DocumentationAttributes_51)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_DocumentationAttributes_51() const { return ___DocumentationAttributes_51; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_DocumentationAttributes_51() { return &___DocumentationAttributes_51; }
	inline void set_DocumentationAttributes_51(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___DocumentationAttributes_51 = value;
		Il2CppCodeGenWriteBarrier((&___DocumentationAttributes_51), value);
	}

	inline static int32_t get_offset_of_AppinfoAttributes_52() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___AppinfoAttributes_52)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_AppinfoAttributes_52() const { return ___AppinfoAttributes_52; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_AppinfoAttributes_52() { return &___AppinfoAttributes_52; }
	inline void set_AppinfoAttributes_52(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___AppinfoAttributes_52 = value;
		Il2CppCodeGenWriteBarrier((&___AppinfoAttributes_52), value);
	}

	inline static int32_t get_offset_of_RedefineAttributes_53() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___RedefineAttributes_53)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_RedefineAttributes_53() const { return ___RedefineAttributes_53; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_RedefineAttributes_53() { return &___RedefineAttributes_53; }
	inline void set_RedefineAttributes_53(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___RedefineAttributes_53 = value;
		Il2CppCodeGenWriteBarrier((&___RedefineAttributes_53), value);
	}

	inline static int32_t get_offset_of_AnnotationAttributes_54() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___AnnotationAttributes_54)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_AnnotationAttributes_54() const { return ___AnnotationAttributes_54; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_AnnotationAttributes_54() { return &___AnnotationAttributes_54; }
	inline void set_AnnotationAttributes_54(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___AnnotationAttributes_54 = value;
		Il2CppCodeGenWriteBarrier((&___AnnotationAttributes_54), value);
	}

	inline static int32_t get_offset_of_SchemaEntries_55() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___SchemaEntries_55)); }
	inline XsdEntryU5BU5D_t612F7D8AE3D932202C80449A380B9D5A54E3106A* get_SchemaEntries_55() const { return ___SchemaEntries_55; }
	inline XsdEntryU5BU5D_t612F7D8AE3D932202C80449A380B9D5A54E3106A** get_address_of_SchemaEntries_55() { return &___SchemaEntries_55; }
	inline void set_SchemaEntries_55(XsdEntryU5BU5D_t612F7D8AE3D932202C80449A380B9D5A54E3106A* value)
	{
		___SchemaEntries_55 = value;
		Il2CppCodeGenWriteBarrier((&___SchemaEntries_55), value);
	}

	inline static int32_t get_offset_of_DerivationMethodValues_56() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___DerivationMethodValues_56)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DerivationMethodValues_56() const { return ___DerivationMethodValues_56; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DerivationMethodValues_56() { return &___DerivationMethodValues_56; }
	inline void set_DerivationMethodValues_56(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DerivationMethodValues_56 = value;
		Il2CppCodeGenWriteBarrier((&___DerivationMethodValues_56), value);
	}

	inline static int32_t get_offset_of_DerivationMethodStrings_57() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___DerivationMethodStrings_57)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_DerivationMethodStrings_57() const { return ___DerivationMethodStrings_57; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_DerivationMethodStrings_57() { return &___DerivationMethodStrings_57; }
	inline void set_DerivationMethodStrings_57(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___DerivationMethodStrings_57 = value;
		Il2CppCodeGenWriteBarrier((&___DerivationMethodStrings_57), value);
	}

	inline static int32_t get_offset_of_FormStringValues_58() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___FormStringValues_58)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_FormStringValues_58() const { return ___FormStringValues_58; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_FormStringValues_58() { return &___FormStringValues_58; }
	inline void set_FormStringValues_58(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___FormStringValues_58 = value;
		Il2CppCodeGenWriteBarrier((&___FormStringValues_58), value);
	}

	inline static int32_t get_offset_of_UseStringValues_59() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___UseStringValues_59)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_UseStringValues_59() const { return ___UseStringValues_59; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_UseStringValues_59() { return &___UseStringValues_59; }
	inline void set_UseStringValues_59(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___UseStringValues_59 = value;
		Il2CppCodeGenWriteBarrier((&___UseStringValues_59), value);
	}

	inline static int32_t get_offset_of_ProcessContentsStringValues_60() { return static_cast<int32_t>(offsetof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields, ___ProcessContentsStringValues_60)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_ProcessContentsStringValues_60() const { return ___ProcessContentsStringValues_60; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_ProcessContentsStringValues_60() { return &___ProcessContentsStringValues_60; }
	inline void set_ProcessContentsStringValues_60(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___ProcessContentsStringValues_60 = value;
		Il2CppCodeGenWriteBarrier((&___ProcessContentsStringValues_60), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBUILDER_TBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_H
#ifndef BUILDERNAMESPACEMANAGER_T87105DBB7709F55B4371C1E746AE7CFDA950679E_H
#define BUILDERNAMESPACEMANAGER_T87105DBB7709F55B4371C1E746AE7CFDA950679E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdBuilder/BuilderNamespaceManager
struct  BuilderNamespaceManager_t87105DBB7709F55B4371C1E746AE7CFDA950679E  : public XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F
{
public:
	// System.Xml.XmlNamespaceManager System.Xml.Schema.XsdBuilder/BuilderNamespaceManager::nsMgr
	XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * ___nsMgr_8;
	// System.Xml.XmlReader System.Xml.Schema.XsdBuilder/BuilderNamespaceManager::reader
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * ___reader_9;

public:
	inline static int32_t get_offset_of_nsMgr_8() { return static_cast<int32_t>(offsetof(BuilderNamespaceManager_t87105DBB7709F55B4371C1E746AE7CFDA950679E, ___nsMgr_8)); }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * get_nsMgr_8() const { return ___nsMgr_8; }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F ** get_address_of_nsMgr_8() { return &___nsMgr_8; }
	inline void set_nsMgr_8(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * value)
	{
		___nsMgr_8 = value;
		Il2CppCodeGenWriteBarrier((&___nsMgr_8), value);
	}

	inline static int32_t get_offset_of_reader_9() { return static_cast<int32_t>(offsetof(BuilderNamespaceManager_t87105DBB7709F55B4371C1E746AE7CFDA950679E, ___reader_9)); }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * get_reader_9() const { return ___reader_9; }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB ** get_address_of_reader_9() { return &___reader_9; }
	inline void set_reader_9(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * value)
	{
		___reader_9 = value;
		Il2CppCodeGenWriteBarrier((&___reader_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDERNAMESPACEMANAGER_T87105DBB7709F55B4371C1E746AE7CFDA950679E_H
#ifndef XSDDURATION_T10E257E1794C97860274A62B208A93B6DBBC7CE5_H
#define XSDDURATION_T10E257E1794C97860274A62B208A93B6DBBC7CE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDuration
struct  XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5 
{
public:
	// System.Int32 System.Xml.Schema.XsdDuration::years
	int32_t ___years_0;
	// System.Int32 System.Xml.Schema.XsdDuration::months
	int32_t ___months_1;
	// System.Int32 System.Xml.Schema.XsdDuration::days
	int32_t ___days_2;
	// System.Int32 System.Xml.Schema.XsdDuration::hours
	int32_t ___hours_3;
	// System.Int32 System.Xml.Schema.XsdDuration::minutes
	int32_t ___minutes_4;
	// System.Int32 System.Xml.Schema.XsdDuration::seconds
	int32_t ___seconds_5;
	// System.UInt32 System.Xml.Schema.XsdDuration::nanoseconds
	uint32_t ___nanoseconds_6;

public:
	inline static int32_t get_offset_of_years_0() { return static_cast<int32_t>(offsetof(XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5, ___years_0)); }
	inline int32_t get_years_0() const { return ___years_0; }
	inline int32_t* get_address_of_years_0() { return &___years_0; }
	inline void set_years_0(int32_t value)
	{
		___years_0 = value;
	}

	inline static int32_t get_offset_of_months_1() { return static_cast<int32_t>(offsetof(XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5, ___months_1)); }
	inline int32_t get_months_1() const { return ___months_1; }
	inline int32_t* get_address_of_months_1() { return &___months_1; }
	inline void set_months_1(int32_t value)
	{
		___months_1 = value;
	}

	inline static int32_t get_offset_of_days_2() { return static_cast<int32_t>(offsetof(XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5, ___days_2)); }
	inline int32_t get_days_2() const { return ___days_2; }
	inline int32_t* get_address_of_days_2() { return &___days_2; }
	inline void set_days_2(int32_t value)
	{
		___days_2 = value;
	}

	inline static int32_t get_offset_of_hours_3() { return static_cast<int32_t>(offsetof(XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5, ___hours_3)); }
	inline int32_t get_hours_3() const { return ___hours_3; }
	inline int32_t* get_address_of_hours_3() { return &___hours_3; }
	inline void set_hours_3(int32_t value)
	{
		___hours_3 = value;
	}

	inline static int32_t get_offset_of_minutes_4() { return static_cast<int32_t>(offsetof(XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5, ___minutes_4)); }
	inline int32_t get_minutes_4() const { return ___minutes_4; }
	inline int32_t* get_address_of_minutes_4() { return &___minutes_4; }
	inline void set_minutes_4(int32_t value)
	{
		___minutes_4 = value;
	}

	inline static int32_t get_offset_of_seconds_5() { return static_cast<int32_t>(offsetof(XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5, ___seconds_5)); }
	inline int32_t get_seconds_5() const { return ___seconds_5; }
	inline int32_t* get_address_of_seconds_5() { return &___seconds_5; }
	inline void set_seconds_5(int32_t value)
	{
		___seconds_5 = value;
	}

	inline static int32_t get_offset_of_nanoseconds_6() { return static_cast<int32_t>(offsetof(XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5, ___nanoseconds_6)); }
	inline uint32_t get_nanoseconds_6() const { return ___nanoseconds_6; }
	inline uint32_t* get_address_of_nanoseconds_6() { return &___nanoseconds_6; }
	inline void set_nanoseconds_6(uint32_t value)
	{
		___nanoseconds_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDURATION_T10E257E1794C97860274A62B208A93B6DBBC7CE5_H
#ifndef XMLCHARTYPE_T7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_H
#define XMLCHARTYPE_T7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharType
struct  XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 
{
public:
	// System.Byte[] System.Xml.XmlCharType::charProperties
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___charProperties_2;

public:
	inline static int32_t get_offset_of_charProperties_2() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9, ___charProperties_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_charProperties_2() const { return ___charProperties_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_charProperties_2() { return &___charProperties_2; }
	inline void set_charProperties_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___charProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___charProperties_2), value);
	}
};

struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields
{
public:
	// System.Object System.Xml.XmlCharType::s_Lock
	RuntimeObject * ___s_Lock_0;
	// System.Byte[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlCharType::s_CharProperties
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___s_CharProperties_1;

public:
	inline static int32_t get_offset_of_s_Lock_0() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields, ___s_Lock_0)); }
	inline RuntimeObject * get_s_Lock_0() const { return ___s_Lock_0; }
	inline RuntimeObject ** get_address_of_s_Lock_0() { return &___s_Lock_0; }
	inline void set_s_Lock_0(RuntimeObject * value)
	{
		___s_Lock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Lock_0), value);
	}

	inline static int32_t get_offset_of_s_CharProperties_1() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields, ___s_CharProperties_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_s_CharProperties_1() const { return ___s_CharProperties_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_s_CharProperties_1() { return &___s_CharProperties_1; }
	inline void set_s_CharProperties_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___s_CharProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CharProperties_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlCharType
struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_pinvoke
{
	uint8_t* ___charProperties_2;
};
// Native definition for COM marshalling of System.Xml.XmlCharType
struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_com
{
	uint8_t* ___charProperties_2;
};
#endif // XMLCHARTYPE_T7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_H
#ifndef STRINGCONCAT_T506391CC6FC326F2114A013043BA36C3066B5C43_H
#define STRINGCONCAT_T506391CC6FC326F2114A013043BA36C3066B5C43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Xsl.Runtime.StringConcat
struct  StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43 
{
public:
	// System.String System.Xml.Xsl.Runtime.StringConcat::s1
	String_t* ___s1_0;
	// System.String System.Xml.Xsl.Runtime.StringConcat::s2
	String_t* ___s2_1;
	// System.String System.Xml.Xsl.Runtime.StringConcat::s3
	String_t* ___s3_2;
	// System.String System.Xml.Xsl.Runtime.StringConcat::s4
	String_t* ___s4_3;
	// System.String System.Xml.Xsl.Runtime.StringConcat::delimiter
	String_t* ___delimiter_4;
	// System.Collections.Generic.List`1<System.String> System.Xml.Xsl.Runtime.StringConcat::strList
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___strList_5;
	// System.Int32 System.Xml.Xsl.Runtime.StringConcat::idxStr
	int32_t ___idxStr_6;

public:
	inline static int32_t get_offset_of_s1_0() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___s1_0)); }
	inline String_t* get_s1_0() const { return ___s1_0; }
	inline String_t** get_address_of_s1_0() { return &___s1_0; }
	inline void set_s1_0(String_t* value)
	{
		___s1_0 = value;
		Il2CppCodeGenWriteBarrier((&___s1_0), value);
	}

	inline static int32_t get_offset_of_s2_1() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___s2_1)); }
	inline String_t* get_s2_1() const { return ___s2_1; }
	inline String_t** get_address_of_s2_1() { return &___s2_1; }
	inline void set_s2_1(String_t* value)
	{
		___s2_1 = value;
		Il2CppCodeGenWriteBarrier((&___s2_1), value);
	}

	inline static int32_t get_offset_of_s3_2() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___s3_2)); }
	inline String_t* get_s3_2() const { return ___s3_2; }
	inline String_t** get_address_of_s3_2() { return &___s3_2; }
	inline void set_s3_2(String_t* value)
	{
		___s3_2 = value;
		Il2CppCodeGenWriteBarrier((&___s3_2), value);
	}

	inline static int32_t get_offset_of_s4_3() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___s4_3)); }
	inline String_t* get_s4_3() const { return ___s4_3; }
	inline String_t** get_address_of_s4_3() { return &___s4_3; }
	inline void set_s4_3(String_t* value)
	{
		___s4_3 = value;
		Il2CppCodeGenWriteBarrier((&___s4_3), value);
	}

	inline static int32_t get_offset_of_delimiter_4() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___delimiter_4)); }
	inline String_t* get_delimiter_4() const { return ___delimiter_4; }
	inline String_t** get_address_of_delimiter_4() { return &___delimiter_4; }
	inline void set_delimiter_4(String_t* value)
	{
		___delimiter_4 = value;
		Il2CppCodeGenWriteBarrier((&___delimiter_4), value);
	}

	inline static int32_t get_offset_of_strList_5() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___strList_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_strList_5() const { return ___strList_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_strList_5() { return &___strList_5; }
	inline void set_strList_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___strList_5 = value;
		Il2CppCodeGenWriteBarrier((&___strList_5), value);
	}

	inline static int32_t get_offset_of_idxStr_6() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___idxStr_6)); }
	inline int32_t get_idxStr_6() const { return ___idxStr_6; }
	inline int32_t* get_address_of_idxStr_6() { return &___idxStr_6; }
	inline void set_idxStr_6(int32_t value)
	{
		___idxStr_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Xsl.Runtime.StringConcat
struct StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43_marshaled_pinvoke
{
	char* ___s1_0;
	char* ___s2_1;
	char* ___s3_2;
	char* ___s4_3;
	char* ___delimiter_4;
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___strList_5;
	int32_t ___idxStr_6;
};
// Native definition for COM marshalling of System.Xml.Xsl.Runtime.StringConcat
struct StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43_marshaled_com
{
	Il2CppChar* ___s1_0;
	Il2CppChar* ___s2_1;
	Il2CppChar* ___s3_2;
	Il2CppChar* ___s4_3;
	Il2CppChar* ___delimiter_4;
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___strList_5;
	int32_t ___idxStr_6;
};
#endif // STRINGCONCAT_T506391CC6FC326F2114A013043BA36C3066B5C43_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TBA431F51A4722F0776A8592A8C2A8770D6D54EFB_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TBA431F51A4722F0776A8592A8C2A8770D6D54EFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::0701435C4E2C38EFE43C51BD22C114AB8B80124D
	__StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749  ___0701435C4E2C38EFE43C51BD22C114AB8B80124D_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::0EE6555EB2C89F29655BD23FAB0573D8D684331A
	__StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15  ___0EE6555EB2C89F29655BD23FAB0573D8D684331A_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=68 <PrivateImplementationDetails>::0F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1
	__StaticArrayInitTypeSizeU3D68_t5DF8EE2FE382E89E2BE20E276315E7E5850B86A2  ___0F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::161F91CE1721D8F16622810CBB39887D21C47031
	__StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749  ___161F91CE1721D8F16622810CBB39887D21C47031_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::2051D7520B96DCC12F2E4DE851CB9F203D623805
	__StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7  ___2051D7520B96DCC12F2E4DE851CB9F203D623805_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44 <PrivateImplementationDetails>::221CE291CD044114B4369175B9B91177F5932876
	__StaticArrayInitTypeSizeU3D44_t497B1CD0DA7F519C08603DFB716118F291F18E40  ___221CE291CD044114B4369175B9B91177F5932876_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=38 <PrivateImplementationDetails>::2A4F1BD548EC71F652E24985361CD72F0FE1BE7D
	__StaticArrayInitTypeSizeU3D38_t93A8ADCEAFC78CEEBA1473ADD1938275574C49E1  ___2A4F1BD548EC71F652E24985361CD72F0FE1BE7D_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::360487BE4278986419B568EFD887F6145383168A
	__StaticArrayInitTypeSizeU3D40_t2C1C40E1ED6F4BAA5E535BCF7574AD6781A49AD4  ___360487BE4278986419B568EFD887F6145383168A_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::42DDBEE388AB59C20A3D7D4D6555E78D74A45AE1
	__StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF  ___42DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::485F43E332C2F7530815B17C08DAC169A8F697E0
	__StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7  ___485F43E332C2F7530815B17C08DAC169A8F697E0_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=112 <PrivateImplementationDetails>::49C5BA13401986EC93E4677F52CBE2248184DFBD
	__StaticArrayInitTypeSizeU3D112_tCA120B5FDB821F63EF42C3B78E8B2A74B77D117E  ___49C5BA13401986EC93E4677F52CBE2248184DFBD_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::51E4CA1C2B009A2876C6E57D8E69E3502BCA3440
	__StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223  ___51E4CA1C2B009A2876C6E57D8E69E3502BCA3440_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=960 <PrivateImplementationDetails>::553E235E202D57C9F1156E7D232E02BBDC920165
	__StaticArrayInitTypeSizeU3D960_t90BB40F76231B6054859C4B179BC097521A54C32  ___553E235E202D57C9F1156E7D232E02BBDC920165_12;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=56 <PrivateImplementationDetails>::574B9D4E4C39F6E8004181E5765B627B75EB1AD1
	__StaticArrayInitTypeSizeU3D56_tA1FD048C977FA4FB4F0B2359C884E3002DC36737  ___574B9D4E4C39F6E8004181E5765B627B75EB1AD1_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD
	__StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749  ___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_14;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98
	__StaticArrayInitTypeSizeU3D6_tB657E692303B443FF0E24AE8F75A675A844348C4  ___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_15;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=664 <PrivateImplementationDetails>::68D0F86889D5D656483EEE829BCEECDFEC91D8EA
	__StaticArrayInitTypeSizeU3D664_t3741AEF321779B117CACFCF3A0CE45075DD15EBA  ___68D0F86889D5D656483EEE829BCEECDFEC91D8EA_16;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=416 <PrivateImplementationDetails>::6A0D50D692745A6663128CD315B71079584F3E59
	__StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704  ___6A0D50D692745A6663128CD315B71079584F3E59_17;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::702F6A3276CBE481D247A77C20B5459FB94D07D2
	__StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223  ___702F6A3276CBE481D247A77C20B5459FB94D07D2_18;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::7A32E1A19C182315E4263A65A72066492550D8CD
	__StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7  ___7A32E1A19C182315E4263A65A72066492550D8CD_19;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::8B4E5E81A88D29642679AFCE41DCA380F9000462
	__StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD  ___8B4E5E81A88D29642679AFCE41DCA380F9000462_20;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=960 <PrivateImplementationDetails>::977375E4E1ED54F588076ACA36CC17E6C2195CB9
	__StaticArrayInitTypeSizeU3D960_t90BB40F76231B6054859C4B179BC097521A54C32  ___977375E4E1ED54F588076ACA36CC17E6C2195CB9_21;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::99F0664C2AC8464B51252D92FC24F3834C6FB90C
	__StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749  ___99F0664C2AC8464B51252D92FC24F3834C6FB90C_22;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::9E31F24F64765FCAA589F589324D17C9FCF6A06D
	__StaticArrayInitTypeSizeU3D28_t5092DE69B1A24897AC8FBEFE716755EA1FE14C24  ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_23;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=144 <PrivateImplementationDetails>::9E374D7263B2452E25DE3D6E617F6A728D98A439
	__StaticArrayInitTypeSizeU3D144_t9C3F15E672A756D2D700C19E37BF64DBD63DD0F7  ___9E374D7263B2452E25DE3D6E617F6A728D98A439_24;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=68 <PrivateImplementationDetails>::A933F173482FF50754B4942AF8DFC584EF14A45B
	__StaticArrayInitTypeSizeU3D68_t5DF8EE2FE382E89E2BE20E276315E7E5850B86A2  ___A933F173482FF50754B4942AF8DFC584EF14A45B_25;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C
	__StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223  ___AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_26;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=416 <PrivateImplementationDetails>::B368804F0C6DAB083B253A6B106D0783D5C32E90
	__StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704  ___B368804F0C6DAB083B253A6B106D0783D5C32E90_27;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::B9F0004E3873FDDCABFDA6174EA18F0859B637B4
	__StaticArrayInitTypeSizeU3D40_t2C1C40E1ED6F4BAA5E535BCF7574AD6781A49AD4  ___B9F0004E3873FDDCABFDA6174EA18F0859B637B4_28;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::BAD037B714E1CD1052149B51238A3D4351DD10B5
	__StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF  ___BAD037B714E1CD1052149B51238A3D4351DD10B5_29;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C
	__StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD  ___C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_30;
	// System.Int64 <PrivateImplementationDetails>::DB9F879BE80CC2DA24DEF633D23DBB2B1ADBCC81
	int64_t ___DB9F879BE80CC2DA24DEF633D23DBB2B1ADBCC81_31;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::DCF398750721AA7A27A6BA56E99350329B06E8B1
	__StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD  ___DCF398750721AA7A27A6BA56E99350329B06E8B1_32;
	// System.Int64 <PrivateImplementationDetails>::EBC658B067B5C785A3F0BB67D73755F6FEE7F70C
	int64_t ___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_33;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4
	__StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15  ___ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_34;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::EE3413A2C088FF9432054D6E60A7CB6A498D25F0
	__StaticArrayInitTypeSizeU3D64_tE9F2C6D5C2D961C95B98D342E25A70CE875C2387  ___EE3413A2C088FF9432054D6E60A7CB6A498D25F0_35;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::F64F25EAE9A3D7A356813C4218000185541D7779
	__StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15  ___F64F25EAE9A3D7A356813C4218000185541D7779_36;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=1212 <PrivateImplementationDetails>::FB0C58D8B3094F018764CC6E3094B9576DB08069
	__StaticArrayInitTypeSizeU3D1212_t528E4585D66D23345F608EEE97F869A0D1665EB8  ___FB0C58D8B3094F018764CC6E3094B9576DB08069_37;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::FFE3F15642234E7FAD6951D432F1134D5AD15922
	__StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF  ___FFE3F15642234E7FAD6951D432F1134D5AD15922_38;

public:
	inline static int32_t get_offset_of_U30701435C4E2C38EFE43C51BD22C114AB8B80124D_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___0701435C4E2C38EFE43C51BD22C114AB8B80124D_0)); }
	inline __StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749  get_U30701435C4E2C38EFE43C51BD22C114AB8B80124D_0() const { return ___0701435C4E2C38EFE43C51BD22C114AB8B80124D_0; }
	inline __StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749 * get_address_of_U30701435C4E2C38EFE43C51BD22C114AB8B80124D_0() { return &___0701435C4E2C38EFE43C51BD22C114AB8B80124D_0; }
	inline void set_U30701435C4E2C38EFE43C51BD22C114AB8B80124D_0(__StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749  value)
	{
		___0701435C4E2C38EFE43C51BD22C114AB8B80124D_0 = value;
	}

	inline static int32_t get_offset_of_U30EE6555EB2C89F29655BD23FAB0573D8D684331A_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___0EE6555EB2C89F29655BD23FAB0573D8D684331A_1)); }
	inline __StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15  get_U30EE6555EB2C89F29655BD23FAB0573D8D684331A_1() const { return ___0EE6555EB2C89F29655BD23FAB0573D8D684331A_1; }
	inline __StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15 * get_address_of_U30EE6555EB2C89F29655BD23FAB0573D8D684331A_1() { return &___0EE6555EB2C89F29655BD23FAB0573D8D684331A_1; }
	inline void set_U30EE6555EB2C89F29655BD23FAB0573D8D684331A_1(__StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15  value)
	{
		___0EE6555EB2C89F29655BD23FAB0573D8D684331A_1 = value;
	}

	inline static int32_t get_offset_of_U30F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___0F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_2)); }
	inline __StaticArrayInitTypeSizeU3D68_t5DF8EE2FE382E89E2BE20E276315E7E5850B86A2  get_U30F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_2() const { return ___0F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_2; }
	inline __StaticArrayInitTypeSizeU3D68_t5DF8EE2FE382E89E2BE20E276315E7E5850B86A2 * get_address_of_U30F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_2() { return &___0F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_2; }
	inline void set_U30F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_2(__StaticArrayInitTypeSizeU3D68_t5DF8EE2FE382E89E2BE20E276315E7E5850B86A2  value)
	{
		___0F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_2 = value;
	}

	inline static int32_t get_offset_of_U3161F91CE1721D8F16622810CBB39887D21C47031_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___161F91CE1721D8F16622810CBB39887D21C47031_3)); }
	inline __StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749  get_U3161F91CE1721D8F16622810CBB39887D21C47031_3() const { return ___161F91CE1721D8F16622810CBB39887D21C47031_3; }
	inline __StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749 * get_address_of_U3161F91CE1721D8F16622810CBB39887D21C47031_3() { return &___161F91CE1721D8F16622810CBB39887D21C47031_3; }
	inline void set_U3161F91CE1721D8F16622810CBB39887D21C47031_3(__StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749  value)
	{
		___161F91CE1721D8F16622810CBB39887D21C47031_3 = value;
	}

	inline static int32_t get_offset_of_U32051D7520B96DCC12F2E4DE851CB9F203D623805_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___2051D7520B96DCC12F2E4DE851CB9F203D623805_4)); }
	inline __StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7  get_U32051D7520B96DCC12F2E4DE851CB9F203D623805_4() const { return ___2051D7520B96DCC12F2E4DE851CB9F203D623805_4; }
	inline __StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7 * get_address_of_U32051D7520B96DCC12F2E4DE851CB9F203D623805_4() { return &___2051D7520B96DCC12F2E4DE851CB9F203D623805_4; }
	inline void set_U32051D7520B96DCC12F2E4DE851CB9F203D623805_4(__StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7  value)
	{
		___2051D7520B96DCC12F2E4DE851CB9F203D623805_4 = value;
	}

	inline static int32_t get_offset_of_U3221CE291CD044114B4369175B9B91177F5932876_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___221CE291CD044114B4369175B9B91177F5932876_5)); }
	inline __StaticArrayInitTypeSizeU3D44_t497B1CD0DA7F519C08603DFB716118F291F18E40  get_U3221CE291CD044114B4369175B9B91177F5932876_5() const { return ___221CE291CD044114B4369175B9B91177F5932876_5; }
	inline __StaticArrayInitTypeSizeU3D44_t497B1CD0DA7F519C08603DFB716118F291F18E40 * get_address_of_U3221CE291CD044114B4369175B9B91177F5932876_5() { return &___221CE291CD044114B4369175B9B91177F5932876_5; }
	inline void set_U3221CE291CD044114B4369175B9B91177F5932876_5(__StaticArrayInitTypeSizeU3D44_t497B1CD0DA7F519C08603DFB716118F291F18E40  value)
	{
		___221CE291CD044114B4369175B9B91177F5932876_5 = value;
	}

	inline static int32_t get_offset_of_U32A4F1BD548EC71F652E24985361CD72F0FE1BE7D_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___2A4F1BD548EC71F652E24985361CD72F0FE1BE7D_6)); }
	inline __StaticArrayInitTypeSizeU3D38_t93A8ADCEAFC78CEEBA1473ADD1938275574C49E1  get_U32A4F1BD548EC71F652E24985361CD72F0FE1BE7D_6() const { return ___2A4F1BD548EC71F652E24985361CD72F0FE1BE7D_6; }
	inline __StaticArrayInitTypeSizeU3D38_t93A8ADCEAFC78CEEBA1473ADD1938275574C49E1 * get_address_of_U32A4F1BD548EC71F652E24985361CD72F0FE1BE7D_6() { return &___2A4F1BD548EC71F652E24985361CD72F0FE1BE7D_6; }
	inline void set_U32A4F1BD548EC71F652E24985361CD72F0FE1BE7D_6(__StaticArrayInitTypeSizeU3D38_t93A8ADCEAFC78CEEBA1473ADD1938275574C49E1  value)
	{
		___2A4F1BD548EC71F652E24985361CD72F0FE1BE7D_6 = value;
	}

	inline static int32_t get_offset_of_U3360487BE4278986419B568EFD887F6145383168A_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___360487BE4278986419B568EFD887F6145383168A_7)); }
	inline __StaticArrayInitTypeSizeU3D40_t2C1C40E1ED6F4BAA5E535BCF7574AD6781A49AD4  get_U3360487BE4278986419B568EFD887F6145383168A_7() const { return ___360487BE4278986419B568EFD887F6145383168A_7; }
	inline __StaticArrayInitTypeSizeU3D40_t2C1C40E1ED6F4BAA5E535BCF7574AD6781A49AD4 * get_address_of_U3360487BE4278986419B568EFD887F6145383168A_7() { return &___360487BE4278986419B568EFD887F6145383168A_7; }
	inline void set_U3360487BE4278986419B568EFD887F6145383168A_7(__StaticArrayInitTypeSizeU3D40_t2C1C40E1ED6F4BAA5E535BCF7574AD6781A49AD4  value)
	{
		___360487BE4278986419B568EFD887F6145383168A_7 = value;
	}

	inline static int32_t get_offset_of_U342DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___42DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_8)); }
	inline __StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF  get_U342DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_8() const { return ___42DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_8; }
	inline __StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF * get_address_of_U342DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_8() { return &___42DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_8; }
	inline void set_U342DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_8(__StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF  value)
	{
		___42DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_8 = value;
	}

	inline static int32_t get_offset_of_U3485F43E332C2F7530815B17C08DAC169A8F697E0_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___485F43E332C2F7530815B17C08DAC169A8F697E0_9)); }
	inline __StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7  get_U3485F43E332C2F7530815B17C08DAC169A8F697E0_9() const { return ___485F43E332C2F7530815B17C08DAC169A8F697E0_9; }
	inline __StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7 * get_address_of_U3485F43E332C2F7530815B17C08DAC169A8F697E0_9() { return &___485F43E332C2F7530815B17C08DAC169A8F697E0_9; }
	inline void set_U3485F43E332C2F7530815B17C08DAC169A8F697E0_9(__StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7  value)
	{
		___485F43E332C2F7530815B17C08DAC169A8F697E0_9 = value;
	}

	inline static int32_t get_offset_of_U349C5BA13401986EC93E4677F52CBE2248184DFBD_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___49C5BA13401986EC93E4677F52CBE2248184DFBD_10)); }
	inline __StaticArrayInitTypeSizeU3D112_tCA120B5FDB821F63EF42C3B78E8B2A74B77D117E  get_U349C5BA13401986EC93E4677F52CBE2248184DFBD_10() const { return ___49C5BA13401986EC93E4677F52CBE2248184DFBD_10; }
	inline __StaticArrayInitTypeSizeU3D112_tCA120B5FDB821F63EF42C3B78E8B2A74B77D117E * get_address_of_U349C5BA13401986EC93E4677F52CBE2248184DFBD_10() { return &___49C5BA13401986EC93E4677F52CBE2248184DFBD_10; }
	inline void set_U349C5BA13401986EC93E4677F52CBE2248184DFBD_10(__StaticArrayInitTypeSizeU3D112_tCA120B5FDB821F63EF42C3B78E8B2A74B77D117E  value)
	{
		___49C5BA13401986EC93E4677F52CBE2248184DFBD_10 = value;
	}

	inline static int32_t get_offset_of_U351E4CA1C2B009A2876C6E57D8E69E3502BCA3440_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___51E4CA1C2B009A2876C6E57D8E69E3502BCA3440_11)); }
	inline __StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223  get_U351E4CA1C2B009A2876C6E57D8E69E3502BCA3440_11() const { return ___51E4CA1C2B009A2876C6E57D8E69E3502BCA3440_11; }
	inline __StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223 * get_address_of_U351E4CA1C2B009A2876C6E57D8E69E3502BCA3440_11() { return &___51E4CA1C2B009A2876C6E57D8E69E3502BCA3440_11; }
	inline void set_U351E4CA1C2B009A2876C6E57D8E69E3502BCA3440_11(__StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223  value)
	{
		___51E4CA1C2B009A2876C6E57D8E69E3502BCA3440_11 = value;
	}

	inline static int32_t get_offset_of_U3553E235E202D57C9F1156E7D232E02BBDC920165_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___553E235E202D57C9F1156E7D232E02BBDC920165_12)); }
	inline __StaticArrayInitTypeSizeU3D960_t90BB40F76231B6054859C4B179BC097521A54C32  get_U3553E235E202D57C9F1156E7D232E02BBDC920165_12() const { return ___553E235E202D57C9F1156E7D232E02BBDC920165_12; }
	inline __StaticArrayInitTypeSizeU3D960_t90BB40F76231B6054859C4B179BC097521A54C32 * get_address_of_U3553E235E202D57C9F1156E7D232E02BBDC920165_12() { return &___553E235E202D57C9F1156E7D232E02BBDC920165_12; }
	inline void set_U3553E235E202D57C9F1156E7D232E02BBDC920165_12(__StaticArrayInitTypeSizeU3D960_t90BB40F76231B6054859C4B179BC097521A54C32  value)
	{
		___553E235E202D57C9F1156E7D232E02BBDC920165_12 = value;
	}

	inline static int32_t get_offset_of_U3574B9D4E4C39F6E8004181E5765B627B75EB1AD1_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___574B9D4E4C39F6E8004181E5765B627B75EB1AD1_13)); }
	inline __StaticArrayInitTypeSizeU3D56_tA1FD048C977FA4FB4F0B2359C884E3002DC36737  get_U3574B9D4E4C39F6E8004181E5765B627B75EB1AD1_13() const { return ___574B9D4E4C39F6E8004181E5765B627B75EB1AD1_13; }
	inline __StaticArrayInitTypeSizeU3D56_tA1FD048C977FA4FB4F0B2359C884E3002DC36737 * get_address_of_U3574B9D4E4C39F6E8004181E5765B627B75EB1AD1_13() { return &___574B9D4E4C39F6E8004181E5765B627B75EB1AD1_13; }
	inline void set_U3574B9D4E4C39F6E8004181E5765B627B75EB1AD1_13(__StaticArrayInitTypeSizeU3D56_tA1FD048C977FA4FB4F0B2359C884E3002DC36737  value)
	{
		___574B9D4E4C39F6E8004181E5765B627B75EB1AD1_13 = value;
	}

	inline static int32_t get_offset_of_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_14)); }
	inline __StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749  get_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_14() const { return ___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_14; }
	inline __StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749 * get_address_of_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_14() { return &___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_14; }
	inline void set_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_14(__StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749  value)
	{
		___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_14 = value;
	}

	inline static int32_t get_offset_of_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_15)); }
	inline __StaticArrayInitTypeSizeU3D6_tB657E692303B443FF0E24AE8F75A675A844348C4  get_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_15() const { return ___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_15; }
	inline __StaticArrayInitTypeSizeU3D6_tB657E692303B443FF0E24AE8F75A675A844348C4 * get_address_of_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_15() { return &___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_15; }
	inline void set_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_15(__StaticArrayInitTypeSizeU3D6_tB657E692303B443FF0E24AE8F75A675A844348C4  value)
	{
		___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_15 = value;
	}

	inline static int32_t get_offset_of_U368D0F86889D5D656483EEE829BCEECDFEC91D8EA_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___68D0F86889D5D656483EEE829BCEECDFEC91D8EA_16)); }
	inline __StaticArrayInitTypeSizeU3D664_t3741AEF321779B117CACFCF3A0CE45075DD15EBA  get_U368D0F86889D5D656483EEE829BCEECDFEC91D8EA_16() const { return ___68D0F86889D5D656483EEE829BCEECDFEC91D8EA_16; }
	inline __StaticArrayInitTypeSizeU3D664_t3741AEF321779B117CACFCF3A0CE45075DD15EBA * get_address_of_U368D0F86889D5D656483EEE829BCEECDFEC91D8EA_16() { return &___68D0F86889D5D656483EEE829BCEECDFEC91D8EA_16; }
	inline void set_U368D0F86889D5D656483EEE829BCEECDFEC91D8EA_16(__StaticArrayInitTypeSizeU3D664_t3741AEF321779B117CACFCF3A0CE45075DD15EBA  value)
	{
		___68D0F86889D5D656483EEE829BCEECDFEC91D8EA_16 = value;
	}

	inline static int32_t get_offset_of_U36A0D50D692745A6663128CD315B71079584F3E59_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___6A0D50D692745A6663128CD315B71079584F3E59_17)); }
	inline __StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704  get_U36A0D50D692745A6663128CD315B71079584F3E59_17() const { return ___6A0D50D692745A6663128CD315B71079584F3E59_17; }
	inline __StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704 * get_address_of_U36A0D50D692745A6663128CD315B71079584F3E59_17() { return &___6A0D50D692745A6663128CD315B71079584F3E59_17; }
	inline void set_U36A0D50D692745A6663128CD315B71079584F3E59_17(__StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704  value)
	{
		___6A0D50D692745A6663128CD315B71079584F3E59_17 = value;
	}

	inline static int32_t get_offset_of_U3702F6A3276CBE481D247A77C20B5459FB94D07D2_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___702F6A3276CBE481D247A77C20B5459FB94D07D2_18)); }
	inline __StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223  get_U3702F6A3276CBE481D247A77C20B5459FB94D07D2_18() const { return ___702F6A3276CBE481D247A77C20B5459FB94D07D2_18; }
	inline __StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223 * get_address_of_U3702F6A3276CBE481D247A77C20B5459FB94D07D2_18() { return &___702F6A3276CBE481D247A77C20B5459FB94D07D2_18; }
	inline void set_U3702F6A3276CBE481D247A77C20B5459FB94D07D2_18(__StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223  value)
	{
		___702F6A3276CBE481D247A77C20B5459FB94D07D2_18 = value;
	}

	inline static int32_t get_offset_of_U37A32E1A19C182315E4263A65A72066492550D8CD_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___7A32E1A19C182315E4263A65A72066492550D8CD_19)); }
	inline __StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7  get_U37A32E1A19C182315E4263A65A72066492550D8CD_19() const { return ___7A32E1A19C182315E4263A65A72066492550D8CD_19; }
	inline __StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7 * get_address_of_U37A32E1A19C182315E4263A65A72066492550D8CD_19() { return &___7A32E1A19C182315E4263A65A72066492550D8CD_19; }
	inline void set_U37A32E1A19C182315E4263A65A72066492550D8CD_19(__StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7  value)
	{
		___7A32E1A19C182315E4263A65A72066492550D8CD_19 = value;
	}

	inline static int32_t get_offset_of_U38B4E5E81A88D29642679AFCE41DCA380F9000462_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___8B4E5E81A88D29642679AFCE41DCA380F9000462_20)); }
	inline __StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD  get_U38B4E5E81A88D29642679AFCE41DCA380F9000462_20() const { return ___8B4E5E81A88D29642679AFCE41DCA380F9000462_20; }
	inline __StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD * get_address_of_U38B4E5E81A88D29642679AFCE41DCA380F9000462_20() { return &___8B4E5E81A88D29642679AFCE41DCA380F9000462_20; }
	inline void set_U38B4E5E81A88D29642679AFCE41DCA380F9000462_20(__StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD  value)
	{
		___8B4E5E81A88D29642679AFCE41DCA380F9000462_20 = value;
	}

	inline static int32_t get_offset_of_U3977375E4E1ED54F588076ACA36CC17E6C2195CB9_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___977375E4E1ED54F588076ACA36CC17E6C2195CB9_21)); }
	inline __StaticArrayInitTypeSizeU3D960_t90BB40F76231B6054859C4B179BC097521A54C32  get_U3977375E4E1ED54F588076ACA36CC17E6C2195CB9_21() const { return ___977375E4E1ED54F588076ACA36CC17E6C2195CB9_21; }
	inline __StaticArrayInitTypeSizeU3D960_t90BB40F76231B6054859C4B179BC097521A54C32 * get_address_of_U3977375E4E1ED54F588076ACA36CC17E6C2195CB9_21() { return &___977375E4E1ED54F588076ACA36CC17E6C2195CB9_21; }
	inline void set_U3977375E4E1ED54F588076ACA36CC17E6C2195CB9_21(__StaticArrayInitTypeSizeU3D960_t90BB40F76231B6054859C4B179BC097521A54C32  value)
	{
		___977375E4E1ED54F588076ACA36CC17E6C2195CB9_21 = value;
	}

	inline static int32_t get_offset_of_U399F0664C2AC8464B51252D92FC24F3834C6FB90C_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___99F0664C2AC8464B51252D92FC24F3834C6FB90C_22)); }
	inline __StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749  get_U399F0664C2AC8464B51252D92FC24F3834C6FB90C_22() const { return ___99F0664C2AC8464B51252D92FC24F3834C6FB90C_22; }
	inline __StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749 * get_address_of_U399F0664C2AC8464B51252D92FC24F3834C6FB90C_22() { return &___99F0664C2AC8464B51252D92FC24F3834C6FB90C_22; }
	inline void set_U399F0664C2AC8464B51252D92FC24F3834C6FB90C_22(__StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749  value)
	{
		___99F0664C2AC8464B51252D92FC24F3834C6FB90C_22 = value;
	}

	inline static int32_t get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_23)); }
	inline __StaticArrayInitTypeSizeU3D28_t5092DE69B1A24897AC8FBEFE716755EA1FE14C24  get_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_23() const { return ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_23; }
	inline __StaticArrayInitTypeSizeU3D28_t5092DE69B1A24897AC8FBEFE716755EA1FE14C24 * get_address_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_23() { return &___9E31F24F64765FCAA589F589324D17C9FCF6A06D_23; }
	inline void set_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_23(__StaticArrayInitTypeSizeU3D28_t5092DE69B1A24897AC8FBEFE716755EA1FE14C24  value)
	{
		___9E31F24F64765FCAA589F589324D17C9FCF6A06D_23 = value;
	}

	inline static int32_t get_offset_of_U39E374D7263B2452E25DE3D6E617F6A728D98A439_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___9E374D7263B2452E25DE3D6E617F6A728D98A439_24)); }
	inline __StaticArrayInitTypeSizeU3D144_t9C3F15E672A756D2D700C19E37BF64DBD63DD0F7  get_U39E374D7263B2452E25DE3D6E617F6A728D98A439_24() const { return ___9E374D7263B2452E25DE3D6E617F6A728D98A439_24; }
	inline __StaticArrayInitTypeSizeU3D144_t9C3F15E672A756D2D700C19E37BF64DBD63DD0F7 * get_address_of_U39E374D7263B2452E25DE3D6E617F6A728D98A439_24() { return &___9E374D7263B2452E25DE3D6E617F6A728D98A439_24; }
	inline void set_U39E374D7263B2452E25DE3D6E617F6A728D98A439_24(__StaticArrayInitTypeSizeU3D144_t9C3F15E672A756D2D700C19E37BF64DBD63DD0F7  value)
	{
		___9E374D7263B2452E25DE3D6E617F6A728D98A439_24 = value;
	}

	inline static int32_t get_offset_of_A933F173482FF50754B4942AF8DFC584EF14A45B_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___A933F173482FF50754B4942AF8DFC584EF14A45B_25)); }
	inline __StaticArrayInitTypeSizeU3D68_t5DF8EE2FE382E89E2BE20E276315E7E5850B86A2  get_A933F173482FF50754B4942AF8DFC584EF14A45B_25() const { return ___A933F173482FF50754B4942AF8DFC584EF14A45B_25; }
	inline __StaticArrayInitTypeSizeU3D68_t5DF8EE2FE382E89E2BE20E276315E7E5850B86A2 * get_address_of_A933F173482FF50754B4942AF8DFC584EF14A45B_25() { return &___A933F173482FF50754B4942AF8DFC584EF14A45B_25; }
	inline void set_A933F173482FF50754B4942AF8DFC584EF14A45B_25(__StaticArrayInitTypeSizeU3D68_t5DF8EE2FE382E89E2BE20E276315E7E5850B86A2  value)
	{
		___A933F173482FF50754B4942AF8DFC584EF14A45B_25 = value;
	}

	inline static int32_t get_offset_of_AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_26)); }
	inline __StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223  get_AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_26() const { return ___AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_26; }
	inline __StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223 * get_address_of_AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_26() { return &___AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_26; }
	inline void set_AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_26(__StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223  value)
	{
		___AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_26 = value;
	}

	inline static int32_t get_offset_of_B368804F0C6DAB083B253A6B106D0783D5C32E90_27() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___B368804F0C6DAB083B253A6B106D0783D5C32E90_27)); }
	inline __StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704  get_B368804F0C6DAB083B253A6B106D0783D5C32E90_27() const { return ___B368804F0C6DAB083B253A6B106D0783D5C32E90_27; }
	inline __StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704 * get_address_of_B368804F0C6DAB083B253A6B106D0783D5C32E90_27() { return &___B368804F0C6DAB083B253A6B106D0783D5C32E90_27; }
	inline void set_B368804F0C6DAB083B253A6B106D0783D5C32E90_27(__StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704  value)
	{
		___B368804F0C6DAB083B253A6B106D0783D5C32E90_27 = value;
	}

	inline static int32_t get_offset_of_B9F0004E3873FDDCABFDA6174EA18F0859B637B4_28() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___B9F0004E3873FDDCABFDA6174EA18F0859B637B4_28)); }
	inline __StaticArrayInitTypeSizeU3D40_t2C1C40E1ED6F4BAA5E535BCF7574AD6781A49AD4  get_B9F0004E3873FDDCABFDA6174EA18F0859B637B4_28() const { return ___B9F0004E3873FDDCABFDA6174EA18F0859B637B4_28; }
	inline __StaticArrayInitTypeSizeU3D40_t2C1C40E1ED6F4BAA5E535BCF7574AD6781A49AD4 * get_address_of_B9F0004E3873FDDCABFDA6174EA18F0859B637B4_28() { return &___B9F0004E3873FDDCABFDA6174EA18F0859B637B4_28; }
	inline void set_B9F0004E3873FDDCABFDA6174EA18F0859B637B4_28(__StaticArrayInitTypeSizeU3D40_t2C1C40E1ED6F4BAA5E535BCF7574AD6781A49AD4  value)
	{
		___B9F0004E3873FDDCABFDA6174EA18F0859B637B4_28 = value;
	}

	inline static int32_t get_offset_of_BAD037B714E1CD1052149B51238A3D4351DD10B5_29() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___BAD037B714E1CD1052149B51238A3D4351DD10B5_29)); }
	inline __StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF  get_BAD037B714E1CD1052149B51238A3D4351DD10B5_29() const { return ___BAD037B714E1CD1052149B51238A3D4351DD10B5_29; }
	inline __StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF * get_address_of_BAD037B714E1CD1052149B51238A3D4351DD10B5_29() { return &___BAD037B714E1CD1052149B51238A3D4351DD10B5_29; }
	inline void set_BAD037B714E1CD1052149B51238A3D4351DD10B5_29(__StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF  value)
	{
		___BAD037B714E1CD1052149B51238A3D4351DD10B5_29 = value;
	}

	inline static int32_t get_offset_of_C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_30() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_30)); }
	inline __StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD  get_C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_30() const { return ___C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_30; }
	inline __StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD * get_address_of_C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_30() { return &___C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_30; }
	inline void set_C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_30(__StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD  value)
	{
		___C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_30 = value;
	}

	inline static int32_t get_offset_of_DB9F879BE80CC2DA24DEF633D23DBB2B1ADBCC81_31() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___DB9F879BE80CC2DA24DEF633D23DBB2B1ADBCC81_31)); }
	inline int64_t get_DB9F879BE80CC2DA24DEF633D23DBB2B1ADBCC81_31() const { return ___DB9F879BE80CC2DA24DEF633D23DBB2B1ADBCC81_31; }
	inline int64_t* get_address_of_DB9F879BE80CC2DA24DEF633D23DBB2B1ADBCC81_31() { return &___DB9F879BE80CC2DA24DEF633D23DBB2B1ADBCC81_31; }
	inline void set_DB9F879BE80CC2DA24DEF633D23DBB2B1ADBCC81_31(int64_t value)
	{
		___DB9F879BE80CC2DA24DEF633D23DBB2B1ADBCC81_31 = value;
	}

	inline static int32_t get_offset_of_DCF398750721AA7A27A6BA56E99350329B06E8B1_32() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___DCF398750721AA7A27A6BA56E99350329B06E8B1_32)); }
	inline __StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD  get_DCF398750721AA7A27A6BA56E99350329B06E8B1_32() const { return ___DCF398750721AA7A27A6BA56E99350329B06E8B1_32; }
	inline __StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD * get_address_of_DCF398750721AA7A27A6BA56E99350329B06E8B1_32() { return &___DCF398750721AA7A27A6BA56E99350329B06E8B1_32; }
	inline void set_DCF398750721AA7A27A6BA56E99350329B06E8B1_32(__StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD  value)
	{
		___DCF398750721AA7A27A6BA56E99350329B06E8B1_32 = value;
	}

	inline static int32_t get_offset_of_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_33() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_33)); }
	inline int64_t get_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_33() const { return ___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_33; }
	inline int64_t* get_address_of_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_33() { return &___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_33; }
	inline void set_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_33(int64_t value)
	{
		___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_33 = value;
	}

	inline static int32_t get_offset_of_ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_34() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_34)); }
	inline __StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15  get_ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_34() const { return ___ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_34; }
	inline __StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15 * get_address_of_ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_34() { return &___ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_34; }
	inline void set_ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_34(__StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15  value)
	{
		___ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_34 = value;
	}

	inline static int32_t get_offset_of_EE3413A2C088FF9432054D6E60A7CB6A498D25F0_35() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___EE3413A2C088FF9432054D6E60A7CB6A498D25F0_35)); }
	inline __StaticArrayInitTypeSizeU3D64_tE9F2C6D5C2D961C95B98D342E25A70CE875C2387  get_EE3413A2C088FF9432054D6E60A7CB6A498D25F0_35() const { return ___EE3413A2C088FF9432054D6E60A7CB6A498D25F0_35; }
	inline __StaticArrayInitTypeSizeU3D64_tE9F2C6D5C2D961C95B98D342E25A70CE875C2387 * get_address_of_EE3413A2C088FF9432054D6E60A7CB6A498D25F0_35() { return &___EE3413A2C088FF9432054D6E60A7CB6A498D25F0_35; }
	inline void set_EE3413A2C088FF9432054D6E60A7CB6A498D25F0_35(__StaticArrayInitTypeSizeU3D64_tE9F2C6D5C2D961C95B98D342E25A70CE875C2387  value)
	{
		___EE3413A2C088FF9432054D6E60A7CB6A498D25F0_35 = value;
	}

	inline static int32_t get_offset_of_F64F25EAE9A3D7A356813C4218000185541D7779_36() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___F64F25EAE9A3D7A356813C4218000185541D7779_36)); }
	inline __StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15  get_F64F25EAE9A3D7A356813C4218000185541D7779_36() const { return ___F64F25EAE9A3D7A356813C4218000185541D7779_36; }
	inline __StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15 * get_address_of_F64F25EAE9A3D7A356813C4218000185541D7779_36() { return &___F64F25EAE9A3D7A356813C4218000185541D7779_36; }
	inline void set_F64F25EAE9A3D7A356813C4218000185541D7779_36(__StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15  value)
	{
		___F64F25EAE9A3D7A356813C4218000185541D7779_36 = value;
	}

	inline static int32_t get_offset_of_FB0C58D8B3094F018764CC6E3094B9576DB08069_37() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___FB0C58D8B3094F018764CC6E3094B9576DB08069_37)); }
	inline __StaticArrayInitTypeSizeU3D1212_t528E4585D66D23345F608EEE97F869A0D1665EB8  get_FB0C58D8B3094F018764CC6E3094B9576DB08069_37() const { return ___FB0C58D8B3094F018764CC6E3094B9576DB08069_37; }
	inline __StaticArrayInitTypeSizeU3D1212_t528E4585D66D23345F608EEE97F869A0D1665EB8 * get_address_of_FB0C58D8B3094F018764CC6E3094B9576DB08069_37() { return &___FB0C58D8B3094F018764CC6E3094B9576DB08069_37; }
	inline void set_FB0C58D8B3094F018764CC6E3094B9576DB08069_37(__StaticArrayInitTypeSizeU3D1212_t528E4585D66D23345F608EEE97F869A0D1665EB8  value)
	{
		___FB0C58D8B3094F018764CC6E3094B9576DB08069_37 = value;
	}

	inline static int32_t get_offset_of_FFE3F15642234E7FAD6951D432F1134D5AD15922_38() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___FFE3F15642234E7FAD6951D432F1134D5AD15922_38)); }
	inline __StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF  get_FFE3F15642234E7FAD6951D432F1134D5AD15922_38() const { return ___FFE3F15642234E7FAD6951D432F1134D5AD15922_38; }
	inline __StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF * get_address_of_FFE3F15642234E7FAD6951D432F1134D5AD15922_38() { return &___FFE3F15642234E7FAD6951D432F1134D5AD15922_38; }
	inline void set_FFE3F15642234E7FAD6951D432F1134D5AD15922_38(__StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF  value)
	{
		___FFE3F15642234E7FAD6951D432F1134D5AD15922_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TBA431F51A4722F0776A8592A8C2A8770D6D54EFB_H
#ifndef UNITYTLS_CIPHERSUITE_TA99DBE84EF3BB26740AEF7A5CC6DEA1227B5F9DC_H
#define UNITYTLS_CIPHERSUITE_TA99DBE84EF3BB26740AEF7A5CC6DEA1227B5F9DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_ciphersuite
struct  unitytls_ciphersuite_tA99DBE84EF3BB26740AEF7A5CC6DEA1227B5F9DC 
{
public:
	// System.UInt32 Mono.Unity.UnityTls/unitytls_ciphersuite::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(unitytls_ciphersuite_tA99DBE84EF3BB26740AEF7A5CC6DEA1227B5F9DC, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_CIPHERSUITE_TA99DBE84EF3BB26740AEF7A5CC6DEA1227B5F9DC_H
#ifndef UNITYTLS_ERROR_CODE_TA9272A71D51F7FE555C03185AE244FB01EAF998F_H
#define UNITYTLS_ERROR_CODE_TA9272A71D51F7FE555C03185AE244FB01EAF998F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_error_code
struct  unitytls_error_code_tA9272A71D51F7FE555C03185AE244FB01EAF998F 
{
public:
	// System.UInt32 Mono.Unity.UnityTls/unitytls_error_code::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(unitytls_error_code_tA9272A71D51F7FE555C03185AE244FB01EAF998F, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_ERROR_CODE_TA9272A71D51F7FE555C03185AE244FB01EAF998F_H
#ifndef UNITYTLS_PROTOCOL_T4E7BF0D3D575EDD09EA7BF6593AAB40BBAC60DF8_H
#define UNITYTLS_PROTOCOL_T4E7BF0D3D575EDD09EA7BF6593AAB40BBAC60DF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_protocol
struct  unitytls_protocol_t4E7BF0D3D575EDD09EA7BF6593AAB40BBAC60DF8 
{
public:
	// System.UInt32 Mono.Unity.UnityTls/unitytls_protocol::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(unitytls_protocol_t4E7BF0D3D575EDD09EA7BF6593AAB40BBAC60DF8, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_PROTOCOL_T4E7BF0D3D575EDD09EA7BF6593AAB40BBAC60DF8_H
#ifndef UNITYTLS_X509VERIFY_RESULT_T835FEA0265EFD70F0762B220C663474E03402278_H
#define UNITYTLS_X509VERIFY_RESULT_T835FEA0265EFD70F0762B220C663474E03402278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_x509verify_result
struct  unitytls_x509verify_result_t835FEA0265EFD70F0762B220C663474E03402278 
{
public:
	// System.UInt32 Mono.Unity.UnityTls/unitytls_x509verify_result::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(unitytls_x509verify_result_t835FEA0265EFD70F0762B220C663474E03402278, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_X509VERIFY_RESULT_T835FEA0265EFD70F0762B220C663474E03402278_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef TOKEN_T29EB684F3183FA0B300324C6C6BA75403A81FC9A_H
#define TOKEN_T29EB684F3183FA0B300324C6C6BA75403A81FC9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaNames/Token
struct  Token_t29EB684F3183FA0B300324C6C6BA75403A81FC9A 
{
public:
	// System.Int32 System.Xml.Schema.SchemaNames/Token::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Token_t29EB684F3183FA0B300324C6C6BA75403A81FC9A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T29EB684F3183FA0B300324C6C6BA75403A81FC9A_H
#ifndef VALIDATORSTATE_TBFB3D6A7BF982DA0A31422C4EAAA1631AF81EE55_H
#define VALIDATORSTATE_TBFB3D6A7BF982DA0A31422C4EAAA1631AF81EE55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ValidatorState
struct  ValidatorState_tBFB3D6A7BF982DA0A31422C4EAAA1631AF81EE55 
{
public:
	// System.Int32 System.Xml.Schema.ValidatorState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ValidatorState_tBFB3D6A7BF982DA0A31422C4EAAA1631AF81EE55, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATORSTATE_TBFB3D6A7BF982DA0A31422C4EAAA1631AF81EE55_H
#ifndef XMLSCHEMACONTENTPROCESSING_TD85295E4C6FA245DAE6F9E4E1C927E76678B197A_H
#define XMLSCHEMACONTENTPROCESSING_TD85295E4C6FA245DAE6F9E4E1C927E76678B197A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentProcessing
struct  XmlSchemaContentProcessing_tD85295E4C6FA245DAE6F9E4E1C927E76678B197A 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentProcessing::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaContentProcessing_tD85295E4C6FA245DAE6F9E4E1C927E76678B197A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTPROCESSING_TD85295E4C6FA245DAE6F9E4E1C927E76678B197A_H
#ifndef XMLSCHEMAVALIDATIONFLAGS_T866C695263C623EE4F1746A541248AE12D311A73_H
#define XMLSCHEMAVALIDATIONFLAGS_T866C695263C623EE4F1746A541248AE12D311A73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidationFlags
struct  XmlSchemaValidationFlags_t866C695263C623EE4F1746A541248AE12D311A73 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidationFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaValidationFlags_t866C695263C623EE4F1746A541248AE12D311A73, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATIONFLAGS_T866C695263C623EE4F1746A541248AE12D311A73_H
#ifndef XMLSCHEMAVALIDITY_TFB9AEE7708C4FF08DC1564482F46991CF8FD0621_H
#define XMLSCHEMAVALIDITY_TFB9AEE7708C4FF08DC1564482F46991CF8FD0621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidity
struct  XmlSchemaValidity_tFB9AEE7708C4FF08DC1564482F46991CF8FD0621 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidity::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaValidity_tFB9AEE7708C4FF08DC1564482F46991CF8FD0621, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDITY_TFB9AEE7708C4FF08DC1564482F46991CF8FD0621_H
#ifndef XMLSEVERITYTYPE_TC3AD578830B568AE8D5455F146A99305BEE0501A_H
#define XMLSEVERITYTYPE_TC3AD578830B568AE8D5455F146A99305BEE0501A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSeverityType
struct  XmlSeverityType_tC3AD578830B568AE8D5455F146A99305BEE0501A 
{
public:
	// System.Int32 System.Xml.Schema.XmlSeverityType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSeverityType_tC3AD578830B568AE8D5455F146A99305BEE0501A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSEVERITYTYPE_TC3AD578830B568AE8D5455F146A99305BEE0501A_H
#ifndef XMLTYPECODE_T9C4AD5729574C591993F2559021E198BED5252A2_H
#define XMLTYPECODE_T9C4AD5729574C591993F2559021E198BED5252A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlTypeCode
struct  XmlTypeCode_t9C4AD5729574C591993F2559021E198BED5252A2 
{
public:
	// System.Int32 System.Xml.Schema.XmlTypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlTypeCode_t9C4AD5729574C591993F2559021E198BED5252A2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPECODE_T9C4AD5729574C591993F2559021E198BED5252A2_H
#ifndef STATE_TB6574821F5A18DCBE926226EFF424FD6FF924BE9_H
#define STATE_TB6574821F5A18DCBE926226EFF424FD6FF924BE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdBuilder/State
struct  State_tB6574821F5A18DCBE926226EFF424FD6FF924BE9 
{
public:
	// System.Int32 System.Xml.Schema.XsdBuilder/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_tB6574821F5A18DCBE926226EFF424FD6FF924BE9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_TB6574821F5A18DCBE926226EFF424FD6FF924BE9_H
#ifndef XSDDATETIME_TEA54A4A77DD9458E97F1306F2013714582663CC5_H
#define XSDDATETIME_TEA54A4A77DD9458E97F1306F2013714582663CC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDateTime
struct  XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5 
{
public:
	// System.DateTime System.Xml.Schema.XsdDateTime::dt
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___dt_0;
	// System.UInt32 System.Xml.Schema.XsdDateTime::extra
	uint32_t ___extra_1;

public:
	inline static int32_t get_offset_of_dt_0() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5, ___dt_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_dt_0() const { return ___dt_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_dt_0() { return &___dt_0; }
	inline void set_dt_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___dt_0 = value;
	}

	inline static int32_t get_offset_of_extra_1() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5, ___extra_1)); }
	inline uint32_t get_extra_1() const { return ___extra_1; }
	inline uint32_t* get_address_of_extra_1() { return &___extra_1; }
	inline void set_extra_1(uint32_t value)
	{
		___extra_1 = value;
	}
};

struct XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields
{
public:
	// System.Int32 System.Xml.Schema.XsdDateTime::Lzyyyy
	int32_t ___Lzyyyy_2;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lzyyyy_
	int32_t ___Lzyyyy__3;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lzyyyy_MM
	int32_t ___Lzyyyy_MM_4;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lzyyyy_MM_
	int32_t ___Lzyyyy_MM__5;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lzyyyy_MM_dd
	int32_t ___Lzyyyy_MM_dd_6;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lzyyyy_MM_ddT
	int32_t ___Lzyyyy_MM_ddT_7;
	// System.Int32 System.Xml.Schema.XsdDateTime::LzHH
	int32_t ___LzHH_8;
	// System.Int32 System.Xml.Schema.XsdDateTime::LzHH_
	int32_t ___LzHH__9;
	// System.Int32 System.Xml.Schema.XsdDateTime::LzHH_mm
	int32_t ___LzHH_mm_10;
	// System.Int32 System.Xml.Schema.XsdDateTime::LzHH_mm_
	int32_t ___LzHH_mm__11;
	// System.Int32 System.Xml.Schema.XsdDateTime::LzHH_mm_ss
	int32_t ___LzHH_mm_ss_12;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz_
	int32_t ___Lz__13;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz_zz
	int32_t ___Lz_zz_14;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz_zz_
	int32_t ___Lz_zz__15;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz_zz_zz
	int32_t ___Lz_zz_zz_16;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz__
	int32_t ___Lz___17;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz__mm
	int32_t ___Lz__mm_18;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz__mm_
	int32_t ___Lz__mm__19;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz__mm__
	int32_t ___Lz__mm___20;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz__mm_dd
	int32_t ___Lz__mm_dd_21;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz___
	int32_t ___Lz____22;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz___dd
	int32_t ___Lz___dd_23;
	// System.Xml.Schema.XmlTypeCode[] System.Xml.Schema.XsdDateTime::typeCodes
	XmlTypeCodeU5BU5D_t43BF24D43F5D8E1233D96C0CE555091AE495642B* ___typeCodes_24;

public:
	inline static int32_t get_offset_of_Lzyyyy_2() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lzyyyy_2)); }
	inline int32_t get_Lzyyyy_2() const { return ___Lzyyyy_2; }
	inline int32_t* get_address_of_Lzyyyy_2() { return &___Lzyyyy_2; }
	inline void set_Lzyyyy_2(int32_t value)
	{
		___Lzyyyy_2 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy__3() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lzyyyy__3)); }
	inline int32_t get_Lzyyyy__3() const { return ___Lzyyyy__3; }
	inline int32_t* get_address_of_Lzyyyy__3() { return &___Lzyyyy__3; }
	inline void set_Lzyyyy__3(int32_t value)
	{
		___Lzyyyy__3 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_4() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lzyyyy_MM_4)); }
	inline int32_t get_Lzyyyy_MM_4() const { return ___Lzyyyy_MM_4; }
	inline int32_t* get_address_of_Lzyyyy_MM_4() { return &___Lzyyyy_MM_4; }
	inline void set_Lzyyyy_MM_4(int32_t value)
	{
		___Lzyyyy_MM_4 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM__5() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lzyyyy_MM__5)); }
	inline int32_t get_Lzyyyy_MM__5() const { return ___Lzyyyy_MM__5; }
	inline int32_t* get_address_of_Lzyyyy_MM__5() { return &___Lzyyyy_MM__5; }
	inline void set_Lzyyyy_MM__5(int32_t value)
	{
		___Lzyyyy_MM__5 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_dd_6() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lzyyyy_MM_dd_6)); }
	inline int32_t get_Lzyyyy_MM_dd_6() const { return ___Lzyyyy_MM_dd_6; }
	inline int32_t* get_address_of_Lzyyyy_MM_dd_6() { return &___Lzyyyy_MM_dd_6; }
	inline void set_Lzyyyy_MM_dd_6(int32_t value)
	{
		___Lzyyyy_MM_dd_6 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_ddT_7() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lzyyyy_MM_ddT_7)); }
	inline int32_t get_Lzyyyy_MM_ddT_7() const { return ___Lzyyyy_MM_ddT_7; }
	inline int32_t* get_address_of_Lzyyyy_MM_ddT_7() { return &___Lzyyyy_MM_ddT_7; }
	inline void set_Lzyyyy_MM_ddT_7(int32_t value)
	{
		___Lzyyyy_MM_ddT_7 = value;
	}

	inline static int32_t get_offset_of_LzHH_8() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___LzHH_8)); }
	inline int32_t get_LzHH_8() const { return ___LzHH_8; }
	inline int32_t* get_address_of_LzHH_8() { return &___LzHH_8; }
	inline void set_LzHH_8(int32_t value)
	{
		___LzHH_8 = value;
	}

	inline static int32_t get_offset_of_LzHH__9() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___LzHH__9)); }
	inline int32_t get_LzHH__9() const { return ___LzHH__9; }
	inline int32_t* get_address_of_LzHH__9() { return &___LzHH__9; }
	inline void set_LzHH__9(int32_t value)
	{
		___LzHH__9 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm_10() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___LzHH_mm_10)); }
	inline int32_t get_LzHH_mm_10() const { return ___LzHH_mm_10; }
	inline int32_t* get_address_of_LzHH_mm_10() { return &___LzHH_mm_10; }
	inline void set_LzHH_mm_10(int32_t value)
	{
		___LzHH_mm_10 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm__11() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___LzHH_mm__11)); }
	inline int32_t get_LzHH_mm__11() const { return ___LzHH_mm__11; }
	inline int32_t* get_address_of_LzHH_mm__11() { return &___LzHH_mm__11; }
	inline void set_LzHH_mm__11(int32_t value)
	{
		___LzHH_mm__11 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm_ss_12() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___LzHH_mm_ss_12)); }
	inline int32_t get_LzHH_mm_ss_12() const { return ___LzHH_mm_ss_12; }
	inline int32_t* get_address_of_LzHH_mm_ss_12() { return &___LzHH_mm_ss_12; }
	inline void set_LzHH_mm_ss_12(int32_t value)
	{
		___LzHH_mm_ss_12 = value;
	}

	inline static int32_t get_offset_of_Lz__13() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lz__13)); }
	inline int32_t get_Lz__13() const { return ___Lz__13; }
	inline int32_t* get_address_of_Lz__13() { return &___Lz__13; }
	inline void set_Lz__13(int32_t value)
	{
		___Lz__13 = value;
	}

	inline static int32_t get_offset_of_Lz_zz_14() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lz_zz_14)); }
	inline int32_t get_Lz_zz_14() const { return ___Lz_zz_14; }
	inline int32_t* get_address_of_Lz_zz_14() { return &___Lz_zz_14; }
	inline void set_Lz_zz_14(int32_t value)
	{
		___Lz_zz_14 = value;
	}

	inline static int32_t get_offset_of_Lz_zz__15() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lz_zz__15)); }
	inline int32_t get_Lz_zz__15() const { return ___Lz_zz__15; }
	inline int32_t* get_address_of_Lz_zz__15() { return &___Lz_zz__15; }
	inline void set_Lz_zz__15(int32_t value)
	{
		___Lz_zz__15 = value;
	}

	inline static int32_t get_offset_of_Lz_zz_zz_16() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lz_zz_zz_16)); }
	inline int32_t get_Lz_zz_zz_16() const { return ___Lz_zz_zz_16; }
	inline int32_t* get_address_of_Lz_zz_zz_16() { return &___Lz_zz_zz_16; }
	inline void set_Lz_zz_zz_16(int32_t value)
	{
		___Lz_zz_zz_16 = value;
	}

	inline static int32_t get_offset_of_Lz___17() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lz___17)); }
	inline int32_t get_Lz___17() const { return ___Lz___17; }
	inline int32_t* get_address_of_Lz___17() { return &___Lz___17; }
	inline void set_Lz___17(int32_t value)
	{
		___Lz___17 = value;
	}

	inline static int32_t get_offset_of_Lz__mm_18() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lz__mm_18)); }
	inline int32_t get_Lz__mm_18() const { return ___Lz__mm_18; }
	inline int32_t* get_address_of_Lz__mm_18() { return &___Lz__mm_18; }
	inline void set_Lz__mm_18(int32_t value)
	{
		___Lz__mm_18 = value;
	}

	inline static int32_t get_offset_of_Lz__mm__19() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lz__mm__19)); }
	inline int32_t get_Lz__mm__19() const { return ___Lz__mm__19; }
	inline int32_t* get_address_of_Lz__mm__19() { return &___Lz__mm__19; }
	inline void set_Lz__mm__19(int32_t value)
	{
		___Lz__mm__19 = value;
	}

	inline static int32_t get_offset_of_Lz__mm___20() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lz__mm___20)); }
	inline int32_t get_Lz__mm___20() const { return ___Lz__mm___20; }
	inline int32_t* get_address_of_Lz__mm___20() { return &___Lz__mm___20; }
	inline void set_Lz__mm___20(int32_t value)
	{
		___Lz__mm___20 = value;
	}

	inline static int32_t get_offset_of_Lz__mm_dd_21() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lz__mm_dd_21)); }
	inline int32_t get_Lz__mm_dd_21() const { return ___Lz__mm_dd_21; }
	inline int32_t* get_address_of_Lz__mm_dd_21() { return &___Lz__mm_dd_21; }
	inline void set_Lz__mm_dd_21(int32_t value)
	{
		___Lz__mm_dd_21 = value;
	}

	inline static int32_t get_offset_of_Lz____22() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lz____22)); }
	inline int32_t get_Lz____22() const { return ___Lz____22; }
	inline int32_t* get_address_of_Lz____22() { return &___Lz____22; }
	inline void set_Lz____22(int32_t value)
	{
		___Lz____22 = value;
	}

	inline static int32_t get_offset_of_Lz___dd_23() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___Lz___dd_23)); }
	inline int32_t get_Lz___dd_23() const { return ___Lz___dd_23; }
	inline int32_t* get_address_of_Lz___dd_23() { return &___Lz___dd_23; }
	inline void set_Lz___dd_23(int32_t value)
	{
		___Lz___dd_23 = value;
	}

	inline static int32_t get_offset_of_typeCodes_24() { return static_cast<int32_t>(offsetof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields, ___typeCodes_24)); }
	inline XmlTypeCodeU5BU5D_t43BF24D43F5D8E1233D96C0CE555091AE495642B* get_typeCodes_24() const { return ___typeCodes_24; }
	inline XmlTypeCodeU5BU5D_t43BF24D43F5D8E1233D96C0CE555091AE495642B** get_address_of_typeCodes_24() { return &___typeCodes_24; }
	inline void set_typeCodes_24(XmlTypeCodeU5BU5D_t43BF24D43F5D8E1233D96C0CE555091AE495642B* value)
	{
		___typeCodes_24 = value;
		Il2CppCodeGenWriteBarrier((&___typeCodes_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.XsdDateTime
struct XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_marshaled_pinvoke
{
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___dt_0;
	uint32_t ___extra_1;
};
// Native definition for COM marshalling of System.Xml.Schema.XsdDateTime
struct XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_marshaled_com
{
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___dt_0;
	uint32_t ___extra_1;
};
#endif // XSDDATETIME_TEA54A4A77DD9458E97F1306F2013714582663CC5_H
#ifndef DATETIMETYPECODE_TAD1D7DFBB2EE73A81A0009853B3C79586C83820F_H
#define DATETIMETYPECODE_TAD1D7DFBB2EE73A81A0009853B3C79586C83820F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDateTime/DateTimeTypeCode
struct  DateTimeTypeCode_tAD1D7DFBB2EE73A81A0009853B3C79586C83820F 
{
public:
	// System.Int32 System.Xml.Schema.XsdDateTime/DateTimeTypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeTypeCode_tAD1D7DFBB2EE73A81A0009853B3C79586C83820F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMETYPECODE_TAD1D7DFBB2EE73A81A0009853B3C79586C83820F_H
#ifndef XSDDATETIMEKIND_T1E716A8B5DF21C68A40457C07DFBF6F284D6A8BA_H
#define XSDDATETIMEKIND_T1E716A8B5DF21C68A40457C07DFBF6F284D6A8BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDateTime/XsdDateTimeKind
struct  XsdDateTimeKind_t1E716A8B5DF21C68A40457C07DFBF6F284D6A8BA 
{
public:
	// System.Int32 System.Xml.Schema.XsdDateTime/XsdDateTimeKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XsdDateTimeKind_t1E716A8B5DF21C68A40457C07DFBF6F284D6A8BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDATETIMEKIND_T1E716A8B5DF21C68A40457C07DFBF6F284D6A8BA_H
#ifndef XSDDATETIMEFLAGS_TC892A580DE494CDB02FF5D29F914ECDC49F858C8_H
#define XSDDATETIMEFLAGS_TC892A580DE494CDB02FF5D29F914ECDC49F858C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDateTimeFlags
struct  XsdDateTimeFlags_tC892A580DE494CDB02FF5D29F914ECDC49F858C8 
{
public:
	// System.Int32 System.Xml.Schema.XsdDateTimeFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XsdDateTimeFlags_tC892A580DE494CDB02FF5D29F914ECDC49F858C8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDATETIMEFLAGS_TC892A580DE494CDB02FF5D29F914ECDC49F858C8_H
#ifndef DURATIONTYPE_T687DA1675447204D8E7F5E2B06C3AAD0A9ABC48A_H
#define DURATIONTYPE_T687DA1675447204D8E7F5E2B06C3AAD0A9ABC48A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDuration/DurationType
struct  DurationType_t687DA1675447204D8E7F5E2B06C3AAD0A9ABC48A 
{
public:
	// System.Int32 System.Xml.Schema.XsdDuration/DurationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DurationType_t687DA1675447204D8E7F5E2B06C3AAD0A9ABC48A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DURATIONTYPE_T687DA1675447204D8E7F5E2B06C3AAD0A9ABC48A_H
#ifndef PARTS_T167B7D7072AE6549A8D8024EF757C8196FAFAA55_H
#define PARTS_T167B7D7072AE6549A8D8024EF757C8196FAFAA55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDuration/Parts
struct  Parts_t167B7D7072AE6549A8D8024EF757C8196FAFAA55 
{
public:
	// System.Int32 System.Xml.Schema.XsdDuration/Parts::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Parts_t167B7D7072AE6549A8D8024EF757C8196FAFAA55, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTS_T167B7D7072AE6549A8D8024EF757C8196FAFAA55_H
#ifndef UNITYTLS_ERRORSTATE_T64FA817A583B1CD3CB1AFCFF9606F1F2782ABBE6_H
#define UNITYTLS_ERRORSTATE_T64FA817A583B1CD3CB1AFCFF9606F1F2782ABBE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_errorstate
struct  unitytls_errorstate_t64FA817A583B1CD3CB1AFCFF9606F1F2782ABBE6 
{
public:
	// System.UInt32 Mono.Unity.UnityTls/unitytls_errorstate::magic
	uint32_t ___magic_0;
	// Mono.Unity.UnityTls/unitytls_error_code Mono.Unity.UnityTls/unitytls_errorstate::code
	uint32_t ___code_1;
	// System.UInt64 Mono.Unity.UnityTls/unitytls_errorstate::reserved
	uint64_t ___reserved_2;

public:
	inline static int32_t get_offset_of_magic_0() { return static_cast<int32_t>(offsetof(unitytls_errorstate_t64FA817A583B1CD3CB1AFCFF9606F1F2782ABBE6, ___magic_0)); }
	inline uint32_t get_magic_0() const { return ___magic_0; }
	inline uint32_t* get_address_of_magic_0() { return &___magic_0; }
	inline void set_magic_0(uint32_t value)
	{
		___magic_0 = value;
	}

	inline static int32_t get_offset_of_code_1() { return static_cast<int32_t>(offsetof(unitytls_errorstate_t64FA817A583B1CD3CB1AFCFF9606F1F2782ABBE6, ___code_1)); }
	inline uint32_t get_code_1() const { return ___code_1; }
	inline uint32_t* get_address_of_code_1() { return &___code_1; }
	inline void set_code_1(uint32_t value)
	{
		___code_1 = value;
	}

	inline static int32_t get_offset_of_reserved_2() { return static_cast<int32_t>(offsetof(unitytls_errorstate_t64FA817A583B1CD3CB1AFCFF9606F1F2782ABBE6, ___reserved_2)); }
	inline uint64_t get_reserved_2() const { return ___reserved_2; }
	inline uint64_t* get_address_of_reserved_2() { return &___reserved_2; }
	inline void set_reserved_2(uint64_t value)
	{
		___reserved_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_ERRORSTATE_T64FA817A583B1CD3CB1AFCFF9606F1F2782ABBE6_H
#ifndef UNITYTLS_TLSCTX_PROTOCOLRANGE_T36243D72F83DAD47C95928314F58026DE8D38F47_H
#define UNITYTLS_TLSCTX_PROTOCOLRANGE_T36243D72F83DAD47C95928314F58026DE8D38F47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_tlsctx_protocolrange
struct  unitytls_tlsctx_protocolrange_t36243D72F83DAD47C95928314F58026DE8D38F47 
{
public:
	// Mono.Unity.UnityTls/unitytls_protocol Mono.Unity.UnityTls/unitytls_tlsctx_protocolrange::min
	uint32_t ___min_0;
	// Mono.Unity.UnityTls/unitytls_protocol Mono.Unity.UnityTls/unitytls_tlsctx_protocolrange::max
	uint32_t ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(unitytls_tlsctx_protocolrange_t36243D72F83DAD47C95928314F58026DE8D38F47, ___min_0)); }
	inline uint32_t get_min_0() const { return ___min_0; }
	inline uint32_t* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(uint32_t value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(unitytls_tlsctx_protocolrange_t36243D72F83DAD47C95928314F58026DE8D38F47, ___max_1)); }
	inline uint32_t get_max_1() const { return ___max_1; }
	inline uint32_t* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(uint32_t value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_TLSCTX_PROTOCOLRANGE_T36243D72F83DAD47C95928314F58026DE8D38F47_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef XMLBASECONVERTER_T2E1FE7137987814D59918B27A1807DAE7E78CFD8_H
#define XMLBASECONVERTER_T2E1FE7137987814D59918B27A1807DAE7E78CFD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlBaseConverter
struct  XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8  : public XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E
{
public:
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlBaseConverter::schemaType
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * ___schemaType_0;
	// System.Xml.Schema.XmlTypeCode System.Xml.Schema.XmlBaseConverter::typeCode
	int32_t ___typeCode_1;
	// System.Type System.Xml.Schema.XmlBaseConverter::clrTypeDefault
	Type_t * ___clrTypeDefault_2;

public:
	inline static int32_t get_offset_of_schemaType_0() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8, ___schemaType_0)); }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * get_schemaType_0() const { return ___schemaType_0; }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 ** get_address_of_schemaType_0() { return &___schemaType_0; }
	inline void set_schemaType_0(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * value)
	{
		___schemaType_0 = value;
		Il2CppCodeGenWriteBarrier((&___schemaType_0), value);
	}

	inline static int32_t get_offset_of_typeCode_1() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8, ___typeCode_1)); }
	inline int32_t get_typeCode_1() const { return ___typeCode_1; }
	inline int32_t* get_address_of_typeCode_1() { return &___typeCode_1; }
	inline void set_typeCode_1(int32_t value)
	{
		___typeCode_1 = value;
	}

	inline static int32_t get_offset_of_clrTypeDefault_2() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8, ___clrTypeDefault_2)); }
	inline Type_t * get_clrTypeDefault_2() const { return ___clrTypeDefault_2; }
	inline Type_t ** get_address_of_clrTypeDefault_2() { return &___clrTypeDefault_2; }
	inline void set_clrTypeDefault_2(Type_t * value)
	{
		___clrTypeDefault_2 = value;
		Il2CppCodeGenWriteBarrier((&___clrTypeDefault_2), value);
	}
};

struct XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields
{
public:
	// System.Type System.Xml.Schema.XmlBaseConverter::ICollectionType
	Type_t * ___ICollectionType_3;
	// System.Type System.Xml.Schema.XmlBaseConverter::IEnumerableType
	Type_t * ___IEnumerableType_4;
	// System.Type System.Xml.Schema.XmlBaseConverter::IListType
	Type_t * ___IListType_5;
	// System.Type System.Xml.Schema.XmlBaseConverter::ObjectArrayType
	Type_t * ___ObjectArrayType_6;
	// System.Type System.Xml.Schema.XmlBaseConverter::StringArrayType
	Type_t * ___StringArrayType_7;
	// System.Type System.Xml.Schema.XmlBaseConverter::XmlAtomicValueArrayType
	Type_t * ___XmlAtomicValueArrayType_8;
	// System.Type System.Xml.Schema.XmlBaseConverter::DecimalType
	Type_t * ___DecimalType_9;
	// System.Type System.Xml.Schema.XmlBaseConverter::Int32Type
	Type_t * ___Int32Type_10;
	// System.Type System.Xml.Schema.XmlBaseConverter::Int64Type
	Type_t * ___Int64Type_11;
	// System.Type System.Xml.Schema.XmlBaseConverter::StringType
	Type_t * ___StringType_12;
	// System.Type System.Xml.Schema.XmlBaseConverter::XmlAtomicValueType
	Type_t * ___XmlAtomicValueType_13;
	// System.Type System.Xml.Schema.XmlBaseConverter::ObjectType
	Type_t * ___ObjectType_14;
	// System.Type System.Xml.Schema.XmlBaseConverter::ByteType
	Type_t * ___ByteType_15;
	// System.Type System.Xml.Schema.XmlBaseConverter::Int16Type
	Type_t * ___Int16Type_16;
	// System.Type System.Xml.Schema.XmlBaseConverter::SByteType
	Type_t * ___SByteType_17;
	// System.Type System.Xml.Schema.XmlBaseConverter::UInt16Type
	Type_t * ___UInt16Type_18;
	// System.Type System.Xml.Schema.XmlBaseConverter::UInt32Type
	Type_t * ___UInt32Type_19;
	// System.Type System.Xml.Schema.XmlBaseConverter::UInt64Type
	Type_t * ___UInt64Type_20;
	// System.Type System.Xml.Schema.XmlBaseConverter::XPathItemType
	Type_t * ___XPathItemType_21;
	// System.Type System.Xml.Schema.XmlBaseConverter::DoubleType
	Type_t * ___DoubleType_22;
	// System.Type System.Xml.Schema.XmlBaseConverter::SingleType
	Type_t * ___SingleType_23;
	// System.Type System.Xml.Schema.XmlBaseConverter::DateTimeType
	Type_t * ___DateTimeType_24;
	// System.Type System.Xml.Schema.XmlBaseConverter::DateTimeOffsetType
	Type_t * ___DateTimeOffsetType_25;
	// System.Type System.Xml.Schema.XmlBaseConverter::BooleanType
	Type_t * ___BooleanType_26;
	// System.Type System.Xml.Schema.XmlBaseConverter::ByteArrayType
	Type_t * ___ByteArrayType_27;
	// System.Type System.Xml.Schema.XmlBaseConverter::XmlQualifiedNameType
	Type_t * ___XmlQualifiedNameType_28;
	// System.Type System.Xml.Schema.XmlBaseConverter::UriType
	Type_t * ___UriType_29;
	// System.Type System.Xml.Schema.XmlBaseConverter::TimeSpanType
	Type_t * ___TimeSpanType_30;
	// System.Type System.Xml.Schema.XmlBaseConverter::XPathNavigatorType
	Type_t * ___XPathNavigatorType_31;

public:
	inline static int32_t get_offset_of_ICollectionType_3() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___ICollectionType_3)); }
	inline Type_t * get_ICollectionType_3() const { return ___ICollectionType_3; }
	inline Type_t ** get_address_of_ICollectionType_3() { return &___ICollectionType_3; }
	inline void set_ICollectionType_3(Type_t * value)
	{
		___ICollectionType_3 = value;
		Il2CppCodeGenWriteBarrier((&___ICollectionType_3), value);
	}

	inline static int32_t get_offset_of_IEnumerableType_4() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___IEnumerableType_4)); }
	inline Type_t * get_IEnumerableType_4() const { return ___IEnumerableType_4; }
	inline Type_t ** get_address_of_IEnumerableType_4() { return &___IEnumerableType_4; }
	inline void set_IEnumerableType_4(Type_t * value)
	{
		___IEnumerableType_4 = value;
		Il2CppCodeGenWriteBarrier((&___IEnumerableType_4), value);
	}

	inline static int32_t get_offset_of_IListType_5() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___IListType_5)); }
	inline Type_t * get_IListType_5() const { return ___IListType_5; }
	inline Type_t ** get_address_of_IListType_5() { return &___IListType_5; }
	inline void set_IListType_5(Type_t * value)
	{
		___IListType_5 = value;
		Il2CppCodeGenWriteBarrier((&___IListType_5), value);
	}

	inline static int32_t get_offset_of_ObjectArrayType_6() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___ObjectArrayType_6)); }
	inline Type_t * get_ObjectArrayType_6() const { return ___ObjectArrayType_6; }
	inline Type_t ** get_address_of_ObjectArrayType_6() { return &___ObjectArrayType_6; }
	inline void set_ObjectArrayType_6(Type_t * value)
	{
		___ObjectArrayType_6 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectArrayType_6), value);
	}

	inline static int32_t get_offset_of_StringArrayType_7() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___StringArrayType_7)); }
	inline Type_t * get_StringArrayType_7() const { return ___StringArrayType_7; }
	inline Type_t ** get_address_of_StringArrayType_7() { return &___StringArrayType_7; }
	inline void set_StringArrayType_7(Type_t * value)
	{
		___StringArrayType_7 = value;
		Il2CppCodeGenWriteBarrier((&___StringArrayType_7), value);
	}

	inline static int32_t get_offset_of_XmlAtomicValueArrayType_8() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___XmlAtomicValueArrayType_8)); }
	inline Type_t * get_XmlAtomicValueArrayType_8() const { return ___XmlAtomicValueArrayType_8; }
	inline Type_t ** get_address_of_XmlAtomicValueArrayType_8() { return &___XmlAtomicValueArrayType_8; }
	inline void set_XmlAtomicValueArrayType_8(Type_t * value)
	{
		___XmlAtomicValueArrayType_8 = value;
		Il2CppCodeGenWriteBarrier((&___XmlAtomicValueArrayType_8), value);
	}

	inline static int32_t get_offset_of_DecimalType_9() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___DecimalType_9)); }
	inline Type_t * get_DecimalType_9() const { return ___DecimalType_9; }
	inline Type_t ** get_address_of_DecimalType_9() { return &___DecimalType_9; }
	inline void set_DecimalType_9(Type_t * value)
	{
		___DecimalType_9 = value;
		Il2CppCodeGenWriteBarrier((&___DecimalType_9), value);
	}

	inline static int32_t get_offset_of_Int32Type_10() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___Int32Type_10)); }
	inline Type_t * get_Int32Type_10() const { return ___Int32Type_10; }
	inline Type_t ** get_address_of_Int32Type_10() { return &___Int32Type_10; }
	inline void set_Int32Type_10(Type_t * value)
	{
		___Int32Type_10 = value;
		Il2CppCodeGenWriteBarrier((&___Int32Type_10), value);
	}

	inline static int32_t get_offset_of_Int64Type_11() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___Int64Type_11)); }
	inline Type_t * get_Int64Type_11() const { return ___Int64Type_11; }
	inline Type_t ** get_address_of_Int64Type_11() { return &___Int64Type_11; }
	inline void set_Int64Type_11(Type_t * value)
	{
		___Int64Type_11 = value;
		Il2CppCodeGenWriteBarrier((&___Int64Type_11), value);
	}

	inline static int32_t get_offset_of_StringType_12() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___StringType_12)); }
	inline Type_t * get_StringType_12() const { return ___StringType_12; }
	inline Type_t ** get_address_of_StringType_12() { return &___StringType_12; }
	inline void set_StringType_12(Type_t * value)
	{
		___StringType_12 = value;
		Il2CppCodeGenWriteBarrier((&___StringType_12), value);
	}

	inline static int32_t get_offset_of_XmlAtomicValueType_13() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___XmlAtomicValueType_13)); }
	inline Type_t * get_XmlAtomicValueType_13() const { return ___XmlAtomicValueType_13; }
	inline Type_t ** get_address_of_XmlAtomicValueType_13() { return &___XmlAtomicValueType_13; }
	inline void set_XmlAtomicValueType_13(Type_t * value)
	{
		___XmlAtomicValueType_13 = value;
		Il2CppCodeGenWriteBarrier((&___XmlAtomicValueType_13), value);
	}

	inline static int32_t get_offset_of_ObjectType_14() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___ObjectType_14)); }
	inline Type_t * get_ObjectType_14() const { return ___ObjectType_14; }
	inline Type_t ** get_address_of_ObjectType_14() { return &___ObjectType_14; }
	inline void set_ObjectType_14(Type_t * value)
	{
		___ObjectType_14 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectType_14), value);
	}

	inline static int32_t get_offset_of_ByteType_15() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___ByteType_15)); }
	inline Type_t * get_ByteType_15() const { return ___ByteType_15; }
	inline Type_t ** get_address_of_ByteType_15() { return &___ByteType_15; }
	inline void set_ByteType_15(Type_t * value)
	{
		___ByteType_15 = value;
		Il2CppCodeGenWriteBarrier((&___ByteType_15), value);
	}

	inline static int32_t get_offset_of_Int16Type_16() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___Int16Type_16)); }
	inline Type_t * get_Int16Type_16() const { return ___Int16Type_16; }
	inline Type_t ** get_address_of_Int16Type_16() { return &___Int16Type_16; }
	inline void set_Int16Type_16(Type_t * value)
	{
		___Int16Type_16 = value;
		Il2CppCodeGenWriteBarrier((&___Int16Type_16), value);
	}

	inline static int32_t get_offset_of_SByteType_17() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___SByteType_17)); }
	inline Type_t * get_SByteType_17() const { return ___SByteType_17; }
	inline Type_t ** get_address_of_SByteType_17() { return &___SByteType_17; }
	inline void set_SByteType_17(Type_t * value)
	{
		___SByteType_17 = value;
		Il2CppCodeGenWriteBarrier((&___SByteType_17), value);
	}

	inline static int32_t get_offset_of_UInt16Type_18() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___UInt16Type_18)); }
	inline Type_t * get_UInt16Type_18() const { return ___UInt16Type_18; }
	inline Type_t ** get_address_of_UInt16Type_18() { return &___UInt16Type_18; }
	inline void set_UInt16Type_18(Type_t * value)
	{
		___UInt16Type_18 = value;
		Il2CppCodeGenWriteBarrier((&___UInt16Type_18), value);
	}

	inline static int32_t get_offset_of_UInt32Type_19() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___UInt32Type_19)); }
	inline Type_t * get_UInt32Type_19() const { return ___UInt32Type_19; }
	inline Type_t ** get_address_of_UInt32Type_19() { return &___UInt32Type_19; }
	inline void set_UInt32Type_19(Type_t * value)
	{
		___UInt32Type_19 = value;
		Il2CppCodeGenWriteBarrier((&___UInt32Type_19), value);
	}

	inline static int32_t get_offset_of_UInt64Type_20() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___UInt64Type_20)); }
	inline Type_t * get_UInt64Type_20() const { return ___UInt64Type_20; }
	inline Type_t ** get_address_of_UInt64Type_20() { return &___UInt64Type_20; }
	inline void set_UInt64Type_20(Type_t * value)
	{
		___UInt64Type_20 = value;
		Il2CppCodeGenWriteBarrier((&___UInt64Type_20), value);
	}

	inline static int32_t get_offset_of_XPathItemType_21() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___XPathItemType_21)); }
	inline Type_t * get_XPathItemType_21() const { return ___XPathItemType_21; }
	inline Type_t ** get_address_of_XPathItemType_21() { return &___XPathItemType_21; }
	inline void set_XPathItemType_21(Type_t * value)
	{
		___XPathItemType_21 = value;
		Il2CppCodeGenWriteBarrier((&___XPathItemType_21), value);
	}

	inline static int32_t get_offset_of_DoubleType_22() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___DoubleType_22)); }
	inline Type_t * get_DoubleType_22() const { return ___DoubleType_22; }
	inline Type_t ** get_address_of_DoubleType_22() { return &___DoubleType_22; }
	inline void set_DoubleType_22(Type_t * value)
	{
		___DoubleType_22 = value;
		Il2CppCodeGenWriteBarrier((&___DoubleType_22), value);
	}

	inline static int32_t get_offset_of_SingleType_23() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___SingleType_23)); }
	inline Type_t * get_SingleType_23() const { return ___SingleType_23; }
	inline Type_t ** get_address_of_SingleType_23() { return &___SingleType_23; }
	inline void set_SingleType_23(Type_t * value)
	{
		___SingleType_23 = value;
		Il2CppCodeGenWriteBarrier((&___SingleType_23), value);
	}

	inline static int32_t get_offset_of_DateTimeType_24() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___DateTimeType_24)); }
	inline Type_t * get_DateTimeType_24() const { return ___DateTimeType_24; }
	inline Type_t ** get_address_of_DateTimeType_24() { return &___DateTimeType_24; }
	inline void set_DateTimeType_24(Type_t * value)
	{
		___DateTimeType_24 = value;
		Il2CppCodeGenWriteBarrier((&___DateTimeType_24), value);
	}

	inline static int32_t get_offset_of_DateTimeOffsetType_25() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___DateTimeOffsetType_25)); }
	inline Type_t * get_DateTimeOffsetType_25() const { return ___DateTimeOffsetType_25; }
	inline Type_t ** get_address_of_DateTimeOffsetType_25() { return &___DateTimeOffsetType_25; }
	inline void set_DateTimeOffsetType_25(Type_t * value)
	{
		___DateTimeOffsetType_25 = value;
		Il2CppCodeGenWriteBarrier((&___DateTimeOffsetType_25), value);
	}

	inline static int32_t get_offset_of_BooleanType_26() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___BooleanType_26)); }
	inline Type_t * get_BooleanType_26() const { return ___BooleanType_26; }
	inline Type_t ** get_address_of_BooleanType_26() { return &___BooleanType_26; }
	inline void set_BooleanType_26(Type_t * value)
	{
		___BooleanType_26 = value;
		Il2CppCodeGenWriteBarrier((&___BooleanType_26), value);
	}

	inline static int32_t get_offset_of_ByteArrayType_27() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___ByteArrayType_27)); }
	inline Type_t * get_ByteArrayType_27() const { return ___ByteArrayType_27; }
	inline Type_t ** get_address_of_ByteArrayType_27() { return &___ByteArrayType_27; }
	inline void set_ByteArrayType_27(Type_t * value)
	{
		___ByteArrayType_27 = value;
		Il2CppCodeGenWriteBarrier((&___ByteArrayType_27), value);
	}

	inline static int32_t get_offset_of_XmlQualifiedNameType_28() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___XmlQualifiedNameType_28)); }
	inline Type_t * get_XmlQualifiedNameType_28() const { return ___XmlQualifiedNameType_28; }
	inline Type_t ** get_address_of_XmlQualifiedNameType_28() { return &___XmlQualifiedNameType_28; }
	inline void set_XmlQualifiedNameType_28(Type_t * value)
	{
		___XmlQualifiedNameType_28 = value;
		Il2CppCodeGenWriteBarrier((&___XmlQualifiedNameType_28), value);
	}

	inline static int32_t get_offset_of_UriType_29() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___UriType_29)); }
	inline Type_t * get_UriType_29() const { return ___UriType_29; }
	inline Type_t ** get_address_of_UriType_29() { return &___UriType_29; }
	inline void set_UriType_29(Type_t * value)
	{
		___UriType_29 = value;
		Il2CppCodeGenWriteBarrier((&___UriType_29), value);
	}

	inline static int32_t get_offset_of_TimeSpanType_30() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___TimeSpanType_30)); }
	inline Type_t * get_TimeSpanType_30() const { return ___TimeSpanType_30; }
	inline Type_t ** get_address_of_TimeSpanType_30() { return &___TimeSpanType_30; }
	inline void set_TimeSpanType_30(Type_t * value)
	{
		___TimeSpanType_30 = value;
		Il2CppCodeGenWriteBarrier((&___TimeSpanType_30), value);
	}

	inline static int32_t get_offset_of_XPathNavigatorType_31() { return static_cast<int32_t>(offsetof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields, ___XPathNavigatorType_31)); }
	inline Type_t * get_XPathNavigatorType_31() const { return ___XPathNavigatorType_31; }
	inline Type_t ** get_address_of_XPathNavigatorType_31() { return &___XPathNavigatorType_31; }
	inline void set_XPathNavigatorType_31(Type_t * value)
	{
		___XPathNavigatorType_31 = value;
		Il2CppCodeGenWriteBarrier((&___XPathNavigatorType_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLBASECONVERTER_T2E1FE7137987814D59918B27A1807DAE7E78CFD8_H
#ifndef XMLSCHEMAVALIDATOR_TF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_H
#define XMLSCHEMAVALIDATOR_TF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidator
struct  XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaSet System.Xml.Schema.XmlSchemaValidator::schemaSet
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F * ___schemaSet_0;
	// System.Xml.Schema.XmlSchemaValidationFlags System.Xml.Schema.XmlSchemaValidator::validationFlags
	int32_t ___validationFlags_1;
	// System.Int32 System.Xml.Schema.XmlSchemaValidator::startIDConstraint
	int32_t ___startIDConstraint_2;
	// System.Boolean System.Xml.Schema.XmlSchemaValidator::isRoot
	bool ___isRoot_3;
	// System.Boolean System.Xml.Schema.XmlSchemaValidator::rootHasSchema
	bool ___rootHasSchema_4;
	// System.Boolean System.Xml.Schema.XmlSchemaValidator::attrValid
	bool ___attrValid_5;
	// System.Boolean System.Xml.Schema.XmlSchemaValidator::checkEntity
	bool ___checkEntity_6;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.XmlSchemaValidator::compiledSchemaInfo
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * ___compiledSchemaInfo_7;
	// System.Xml.IDtdInfo System.Xml.Schema.XmlSchemaValidator::dtdSchemaInfo
	RuntimeObject* ___dtdSchemaInfo_8;
	// System.Collections.Hashtable System.Xml.Schema.XmlSchemaValidator::validatedNamespaces
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___validatedNamespaces_9;
	// System.Xml.HWStack System.Xml.Schema.XmlSchemaValidator::validationStack
	HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * ___validationStack_10;
	// System.Xml.Schema.ValidationState System.Xml.Schema.XmlSchemaValidator::context
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * ___context_11;
	// System.Xml.Schema.ValidatorState System.Xml.Schema.XmlSchemaValidator::currentState
	int32_t ___currentState_12;
	// System.Collections.Hashtable System.Xml.Schema.XmlSchemaValidator::attPresence
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___attPresence_13;
	// System.Xml.Schema.SchemaAttDef System.Xml.Schema.XmlSchemaValidator::wildID
	SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 * ___wildID_14;
	// System.Collections.Hashtable System.Xml.Schema.XmlSchemaValidator::IDs
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___IDs_15;
	// System.Xml.Schema.IdRefNode System.Xml.Schema.XmlSchemaValidator::idRefListHead
	IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 * ___idRefListHead_16;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaValidator::contextQName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___contextQName_17;
	// System.String System.Xml.Schema.XmlSchemaValidator::NsXs
	String_t* ___NsXs_18;
	// System.String System.Xml.Schema.XmlSchemaValidator::NsXsi
	String_t* ___NsXsi_19;
	// System.String System.Xml.Schema.XmlSchemaValidator::NsXmlNs
	String_t* ___NsXmlNs_20;
	// System.String System.Xml.Schema.XmlSchemaValidator::NsXml
	String_t* ___NsXml_21;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaValidator::partialValidationType
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * ___partialValidationType_22;
	// System.Text.StringBuilder System.Xml.Schema.XmlSchemaValidator::textValue
	StringBuilder_t * ___textValue_23;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XmlSchemaValidator::eventHandler
	ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * ___eventHandler_24;
	// System.Object System.Xml.Schema.XmlSchemaValidator::validationEventSender
	RuntimeObject * ___validationEventSender_25;
	// System.Xml.XmlNameTable System.Xml.Schema.XmlSchemaValidator::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_26;
	// System.Xml.IXmlLineInfo System.Xml.Schema.XmlSchemaValidator::positionInfo
	RuntimeObject* ___positionInfo_27;
	// System.Xml.IXmlLineInfo System.Xml.Schema.XmlSchemaValidator::dummyPositionInfo
	RuntimeObject* ___dummyPositionInfo_28;
	// System.Xml.XmlResolver System.Xml.Schema.XmlSchemaValidator::xmlResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___xmlResolver_29;
	// System.Uri System.Xml.Schema.XmlSchemaValidator::sourceUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___sourceUri_30;
	// System.String System.Xml.Schema.XmlSchemaValidator::sourceUriString
	String_t* ___sourceUriString_31;
	// System.Xml.IXmlNamespaceResolver System.Xml.Schema.XmlSchemaValidator::nsResolver
	RuntimeObject* ___nsResolver_32;
	// System.Xml.Schema.XmlSchemaContentProcessing System.Xml.Schema.XmlSchemaValidator::processContents
	int32_t ___processContents_33;
	// System.String System.Xml.Schema.XmlSchemaValidator::xsiTypeString
	String_t* ___xsiTypeString_34;
	// System.String System.Xml.Schema.XmlSchemaValidator::xsiNilString
	String_t* ___xsiNilString_35;
	// System.String System.Xml.Schema.XmlSchemaValidator::xsiSchemaLocationString
	String_t* ___xsiSchemaLocationString_36;
	// System.String System.Xml.Schema.XmlSchemaValidator::xsiNoNamespaceSchemaLocationString
	String_t* ___xsiNoNamespaceSchemaLocationString_37;
	// System.Xml.XmlCharType System.Xml.Schema.XmlSchemaValidator::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_43;

public:
	inline static int32_t get_offset_of_schemaSet_0() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___schemaSet_0)); }
	inline XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F * get_schemaSet_0() const { return ___schemaSet_0; }
	inline XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F ** get_address_of_schemaSet_0() { return &___schemaSet_0; }
	inline void set_schemaSet_0(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F * value)
	{
		___schemaSet_0 = value;
		Il2CppCodeGenWriteBarrier((&___schemaSet_0), value);
	}

	inline static int32_t get_offset_of_validationFlags_1() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___validationFlags_1)); }
	inline int32_t get_validationFlags_1() const { return ___validationFlags_1; }
	inline int32_t* get_address_of_validationFlags_1() { return &___validationFlags_1; }
	inline void set_validationFlags_1(int32_t value)
	{
		___validationFlags_1 = value;
	}

	inline static int32_t get_offset_of_startIDConstraint_2() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___startIDConstraint_2)); }
	inline int32_t get_startIDConstraint_2() const { return ___startIDConstraint_2; }
	inline int32_t* get_address_of_startIDConstraint_2() { return &___startIDConstraint_2; }
	inline void set_startIDConstraint_2(int32_t value)
	{
		___startIDConstraint_2 = value;
	}

	inline static int32_t get_offset_of_isRoot_3() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___isRoot_3)); }
	inline bool get_isRoot_3() const { return ___isRoot_3; }
	inline bool* get_address_of_isRoot_3() { return &___isRoot_3; }
	inline void set_isRoot_3(bool value)
	{
		___isRoot_3 = value;
	}

	inline static int32_t get_offset_of_rootHasSchema_4() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___rootHasSchema_4)); }
	inline bool get_rootHasSchema_4() const { return ___rootHasSchema_4; }
	inline bool* get_address_of_rootHasSchema_4() { return &___rootHasSchema_4; }
	inline void set_rootHasSchema_4(bool value)
	{
		___rootHasSchema_4 = value;
	}

	inline static int32_t get_offset_of_attrValid_5() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___attrValid_5)); }
	inline bool get_attrValid_5() const { return ___attrValid_5; }
	inline bool* get_address_of_attrValid_5() { return &___attrValid_5; }
	inline void set_attrValid_5(bool value)
	{
		___attrValid_5 = value;
	}

	inline static int32_t get_offset_of_checkEntity_6() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___checkEntity_6)); }
	inline bool get_checkEntity_6() const { return ___checkEntity_6; }
	inline bool* get_address_of_checkEntity_6() { return &___checkEntity_6; }
	inline void set_checkEntity_6(bool value)
	{
		___checkEntity_6 = value;
	}

	inline static int32_t get_offset_of_compiledSchemaInfo_7() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___compiledSchemaInfo_7)); }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * get_compiledSchemaInfo_7() const { return ___compiledSchemaInfo_7; }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 ** get_address_of_compiledSchemaInfo_7() { return &___compiledSchemaInfo_7; }
	inline void set_compiledSchemaInfo_7(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * value)
	{
		___compiledSchemaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___compiledSchemaInfo_7), value);
	}

	inline static int32_t get_offset_of_dtdSchemaInfo_8() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___dtdSchemaInfo_8)); }
	inline RuntimeObject* get_dtdSchemaInfo_8() const { return ___dtdSchemaInfo_8; }
	inline RuntimeObject** get_address_of_dtdSchemaInfo_8() { return &___dtdSchemaInfo_8; }
	inline void set_dtdSchemaInfo_8(RuntimeObject* value)
	{
		___dtdSchemaInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___dtdSchemaInfo_8), value);
	}

	inline static int32_t get_offset_of_validatedNamespaces_9() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___validatedNamespaces_9)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_validatedNamespaces_9() const { return ___validatedNamespaces_9; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_validatedNamespaces_9() { return &___validatedNamespaces_9; }
	inline void set_validatedNamespaces_9(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___validatedNamespaces_9 = value;
		Il2CppCodeGenWriteBarrier((&___validatedNamespaces_9), value);
	}

	inline static int32_t get_offset_of_validationStack_10() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___validationStack_10)); }
	inline HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * get_validationStack_10() const { return ___validationStack_10; }
	inline HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE ** get_address_of_validationStack_10() { return &___validationStack_10; }
	inline void set_validationStack_10(HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * value)
	{
		___validationStack_10 = value;
		Il2CppCodeGenWriteBarrier((&___validationStack_10), value);
	}

	inline static int32_t get_offset_of_context_11() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___context_11)); }
	inline ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * get_context_11() const { return ___context_11; }
	inline ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B ** get_address_of_context_11() { return &___context_11; }
	inline void set_context_11(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * value)
	{
		___context_11 = value;
		Il2CppCodeGenWriteBarrier((&___context_11), value);
	}

	inline static int32_t get_offset_of_currentState_12() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___currentState_12)); }
	inline int32_t get_currentState_12() const { return ___currentState_12; }
	inline int32_t* get_address_of_currentState_12() { return &___currentState_12; }
	inline void set_currentState_12(int32_t value)
	{
		___currentState_12 = value;
	}

	inline static int32_t get_offset_of_attPresence_13() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___attPresence_13)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_attPresence_13() const { return ___attPresence_13; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_attPresence_13() { return &___attPresence_13; }
	inline void set_attPresence_13(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___attPresence_13 = value;
		Il2CppCodeGenWriteBarrier((&___attPresence_13), value);
	}

	inline static int32_t get_offset_of_wildID_14() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___wildID_14)); }
	inline SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 * get_wildID_14() const { return ___wildID_14; }
	inline SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 ** get_address_of_wildID_14() { return &___wildID_14; }
	inline void set_wildID_14(SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 * value)
	{
		___wildID_14 = value;
		Il2CppCodeGenWriteBarrier((&___wildID_14), value);
	}

	inline static int32_t get_offset_of_IDs_15() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___IDs_15)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_IDs_15() const { return ___IDs_15; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_IDs_15() { return &___IDs_15; }
	inline void set_IDs_15(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___IDs_15 = value;
		Il2CppCodeGenWriteBarrier((&___IDs_15), value);
	}

	inline static int32_t get_offset_of_idRefListHead_16() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___idRefListHead_16)); }
	inline IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 * get_idRefListHead_16() const { return ___idRefListHead_16; }
	inline IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 ** get_address_of_idRefListHead_16() { return &___idRefListHead_16; }
	inline void set_idRefListHead_16(IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 * value)
	{
		___idRefListHead_16 = value;
		Il2CppCodeGenWriteBarrier((&___idRefListHead_16), value);
	}

	inline static int32_t get_offset_of_contextQName_17() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___contextQName_17)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_contextQName_17() const { return ___contextQName_17; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_contextQName_17() { return &___contextQName_17; }
	inline void set_contextQName_17(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___contextQName_17 = value;
		Il2CppCodeGenWriteBarrier((&___contextQName_17), value);
	}

	inline static int32_t get_offset_of_NsXs_18() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___NsXs_18)); }
	inline String_t* get_NsXs_18() const { return ___NsXs_18; }
	inline String_t** get_address_of_NsXs_18() { return &___NsXs_18; }
	inline void set_NsXs_18(String_t* value)
	{
		___NsXs_18 = value;
		Il2CppCodeGenWriteBarrier((&___NsXs_18), value);
	}

	inline static int32_t get_offset_of_NsXsi_19() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___NsXsi_19)); }
	inline String_t* get_NsXsi_19() const { return ___NsXsi_19; }
	inline String_t** get_address_of_NsXsi_19() { return &___NsXsi_19; }
	inline void set_NsXsi_19(String_t* value)
	{
		___NsXsi_19 = value;
		Il2CppCodeGenWriteBarrier((&___NsXsi_19), value);
	}

	inline static int32_t get_offset_of_NsXmlNs_20() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___NsXmlNs_20)); }
	inline String_t* get_NsXmlNs_20() const { return ___NsXmlNs_20; }
	inline String_t** get_address_of_NsXmlNs_20() { return &___NsXmlNs_20; }
	inline void set_NsXmlNs_20(String_t* value)
	{
		___NsXmlNs_20 = value;
		Il2CppCodeGenWriteBarrier((&___NsXmlNs_20), value);
	}

	inline static int32_t get_offset_of_NsXml_21() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___NsXml_21)); }
	inline String_t* get_NsXml_21() const { return ___NsXml_21; }
	inline String_t** get_address_of_NsXml_21() { return &___NsXml_21; }
	inline void set_NsXml_21(String_t* value)
	{
		___NsXml_21 = value;
		Il2CppCodeGenWriteBarrier((&___NsXml_21), value);
	}

	inline static int32_t get_offset_of_partialValidationType_22() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___partialValidationType_22)); }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * get_partialValidationType_22() const { return ___partialValidationType_22; }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B ** get_address_of_partialValidationType_22() { return &___partialValidationType_22; }
	inline void set_partialValidationType_22(XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * value)
	{
		___partialValidationType_22 = value;
		Il2CppCodeGenWriteBarrier((&___partialValidationType_22), value);
	}

	inline static int32_t get_offset_of_textValue_23() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___textValue_23)); }
	inline StringBuilder_t * get_textValue_23() const { return ___textValue_23; }
	inline StringBuilder_t ** get_address_of_textValue_23() { return &___textValue_23; }
	inline void set_textValue_23(StringBuilder_t * value)
	{
		___textValue_23 = value;
		Il2CppCodeGenWriteBarrier((&___textValue_23), value);
	}

	inline static int32_t get_offset_of_eventHandler_24() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___eventHandler_24)); }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * get_eventHandler_24() const { return ___eventHandler_24; }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF ** get_address_of_eventHandler_24() { return &___eventHandler_24; }
	inline void set_eventHandler_24(ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * value)
	{
		___eventHandler_24 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandler_24), value);
	}

	inline static int32_t get_offset_of_validationEventSender_25() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___validationEventSender_25)); }
	inline RuntimeObject * get_validationEventSender_25() const { return ___validationEventSender_25; }
	inline RuntimeObject ** get_address_of_validationEventSender_25() { return &___validationEventSender_25; }
	inline void set_validationEventSender_25(RuntimeObject * value)
	{
		___validationEventSender_25 = value;
		Il2CppCodeGenWriteBarrier((&___validationEventSender_25), value);
	}

	inline static int32_t get_offset_of_nameTable_26() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___nameTable_26)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_26() const { return ___nameTable_26; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_26() { return &___nameTable_26; }
	inline void set_nameTable_26(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_26 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_26), value);
	}

	inline static int32_t get_offset_of_positionInfo_27() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___positionInfo_27)); }
	inline RuntimeObject* get_positionInfo_27() const { return ___positionInfo_27; }
	inline RuntimeObject** get_address_of_positionInfo_27() { return &___positionInfo_27; }
	inline void set_positionInfo_27(RuntimeObject* value)
	{
		___positionInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___positionInfo_27), value);
	}

	inline static int32_t get_offset_of_dummyPositionInfo_28() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___dummyPositionInfo_28)); }
	inline RuntimeObject* get_dummyPositionInfo_28() const { return ___dummyPositionInfo_28; }
	inline RuntimeObject** get_address_of_dummyPositionInfo_28() { return &___dummyPositionInfo_28; }
	inline void set_dummyPositionInfo_28(RuntimeObject* value)
	{
		___dummyPositionInfo_28 = value;
		Il2CppCodeGenWriteBarrier((&___dummyPositionInfo_28), value);
	}

	inline static int32_t get_offset_of_xmlResolver_29() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___xmlResolver_29)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_xmlResolver_29() const { return ___xmlResolver_29; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_xmlResolver_29() { return &___xmlResolver_29; }
	inline void set_xmlResolver_29(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___xmlResolver_29 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_29), value);
	}

	inline static int32_t get_offset_of_sourceUri_30() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___sourceUri_30)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_sourceUri_30() const { return ___sourceUri_30; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_sourceUri_30() { return &___sourceUri_30; }
	inline void set_sourceUri_30(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___sourceUri_30 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_30), value);
	}

	inline static int32_t get_offset_of_sourceUriString_31() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___sourceUriString_31)); }
	inline String_t* get_sourceUriString_31() const { return ___sourceUriString_31; }
	inline String_t** get_address_of_sourceUriString_31() { return &___sourceUriString_31; }
	inline void set_sourceUriString_31(String_t* value)
	{
		___sourceUriString_31 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUriString_31), value);
	}

	inline static int32_t get_offset_of_nsResolver_32() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___nsResolver_32)); }
	inline RuntimeObject* get_nsResolver_32() const { return ___nsResolver_32; }
	inline RuntimeObject** get_address_of_nsResolver_32() { return &___nsResolver_32; }
	inline void set_nsResolver_32(RuntimeObject* value)
	{
		___nsResolver_32 = value;
		Il2CppCodeGenWriteBarrier((&___nsResolver_32), value);
	}

	inline static int32_t get_offset_of_processContents_33() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___processContents_33)); }
	inline int32_t get_processContents_33() const { return ___processContents_33; }
	inline int32_t* get_address_of_processContents_33() { return &___processContents_33; }
	inline void set_processContents_33(int32_t value)
	{
		___processContents_33 = value;
	}

	inline static int32_t get_offset_of_xsiTypeString_34() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___xsiTypeString_34)); }
	inline String_t* get_xsiTypeString_34() const { return ___xsiTypeString_34; }
	inline String_t** get_address_of_xsiTypeString_34() { return &___xsiTypeString_34; }
	inline void set_xsiTypeString_34(String_t* value)
	{
		___xsiTypeString_34 = value;
		Il2CppCodeGenWriteBarrier((&___xsiTypeString_34), value);
	}

	inline static int32_t get_offset_of_xsiNilString_35() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___xsiNilString_35)); }
	inline String_t* get_xsiNilString_35() const { return ___xsiNilString_35; }
	inline String_t** get_address_of_xsiNilString_35() { return &___xsiNilString_35; }
	inline void set_xsiNilString_35(String_t* value)
	{
		___xsiNilString_35 = value;
		Il2CppCodeGenWriteBarrier((&___xsiNilString_35), value);
	}

	inline static int32_t get_offset_of_xsiSchemaLocationString_36() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___xsiSchemaLocationString_36)); }
	inline String_t* get_xsiSchemaLocationString_36() const { return ___xsiSchemaLocationString_36; }
	inline String_t** get_address_of_xsiSchemaLocationString_36() { return &___xsiSchemaLocationString_36; }
	inline void set_xsiSchemaLocationString_36(String_t* value)
	{
		___xsiSchemaLocationString_36 = value;
		Il2CppCodeGenWriteBarrier((&___xsiSchemaLocationString_36), value);
	}

	inline static int32_t get_offset_of_xsiNoNamespaceSchemaLocationString_37() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___xsiNoNamespaceSchemaLocationString_37)); }
	inline String_t* get_xsiNoNamespaceSchemaLocationString_37() const { return ___xsiNoNamespaceSchemaLocationString_37; }
	inline String_t** get_address_of_xsiNoNamespaceSchemaLocationString_37() { return &___xsiNoNamespaceSchemaLocationString_37; }
	inline void set_xsiNoNamespaceSchemaLocationString_37(String_t* value)
	{
		___xsiNoNamespaceSchemaLocationString_37 = value;
		Il2CppCodeGenWriteBarrier((&___xsiNoNamespaceSchemaLocationString_37), value);
	}

	inline static int32_t get_offset_of_xmlCharType_43() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C, ___xmlCharType_43)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_43() const { return ___xmlCharType_43; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_43() { return &___xmlCharType_43; }
	inline void set_xmlCharType_43(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_43 = value;
	}
};

struct XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaValidator::dtQName
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___dtQName_38;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaValidator::dtCDATA
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___dtCDATA_39;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaValidator::dtStringArray
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___dtStringArray_40;
	// System.Xml.Schema.XmlSchemaParticle[] System.Xml.Schema.XmlSchemaValidator::EmptyParticleArray
	XmlSchemaParticleU5BU5D_tCA99C9F23120839B45A59D3FD77E88564CEAD800* ___EmptyParticleArray_41;
	// System.Xml.Schema.XmlSchemaAttribute[] System.Xml.Schema.XmlSchemaValidator::EmptyAttributeArray
	XmlSchemaAttributeU5BU5D_tFDBB08A9CF45CD6E2DE3A8AADCC8487BA5CD896F* ___EmptyAttributeArray_42;
	// System.Boolean[0...,0...] System.Xml.Schema.XmlSchemaValidator::ValidStates
	BooleanU5BU2CU5D_tCD5854E1A55166EC93998DDED119F97FF5A5ECDC* ___ValidStates_44;
	// System.String[] System.Xml.Schema.XmlSchemaValidator::MethodNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___MethodNames_45;

public:
	inline static int32_t get_offset_of_dtQName_38() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields, ___dtQName_38)); }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * get_dtQName_38() const { return ___dtQName_38; }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 ** get_address_of_dtQName_38() { return &___dtQName_38; }
	inline void set_dtQName_38(XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * value)
	{
		___dtQName_38 = value;
		Il2CppCodeGenWriteBarrier((&___dtQName_38), value);
	}

	inline static int32_t get_offset_of_dtCDATA_39() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields, ___dtCDATA_39)); }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * get_dtCDATA_39() const { return ___dtCDATA_39; }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 ** get_address_of_dtCDATA_39() { return &___dtCDATA_39; }
	inline void set_dtCDATA_39(XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * value)
	{
		___dtCDATA_39 = value;
		Il2CppCodeGenWriteBarrier((&___dtCDATA_39), value);
	}

	inline static int32_t get_offset_of_dtStringArray_40() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields, ___dtStringArray_40)); }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * get_dtStringArray_40() const { return ___dtStringArray_40; }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 ** get_address_of_dtStringArray_40() { return &___dtStringArray_40; }
	inline void set_dtStringArray_40(XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * value)
	{
		___dtStringArray_40 = value;
		Il2CppCodeGenWriteBarrier((&___dtStringArray_40), value);
	}

	inline static int32_t get_offset_of_EmptyParticleArray_41() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields, ___EmptyParticleArray_41)); }
	inline XmlSchemaParticleU5BU5D_tCA99C9F23120839B45A59D3FD77E88564CEAD800* get_EmptyParticleArray_41() const { return ___EmptyParticleArray_41; }
	inline XmlSchemaParticleU5BU5D_tCA99C9F23120839B45A59D3FD77E88564CEAD800** get_address_of_EmptyParticleArray_41() { return &___EmptyParticleArray_41; }
	inline void set_EmptyParticleArray_41(XmlSchemaParticleU5BU5D_tCA99C9F23120839B45A59D3FD77E88564CEAD800* value)
	{
		___EmptyParticleArray_41 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyParticleArray_41), value);
	}

	inline static int32_t get_offset_of_EmptyAttributeArray_42() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields, ___EmptyAttributeArray_42)); }
	inline XmlSchemaAttributeU5BU5D_tFDBB08A9CF45CD6E2DE3A8AADCC8487BA5CD896F* get_EmptyAttributeArray_42() const { return ___EmptyAttributeArray_42; }
	inline XmlSchemaAttributeU5BU5D_tFDBB08A9CF45CD6E2DE3A8AADCC8487BA5CD896F** get_address_of_EmptyAttributeArray_42() { return &___EmptyAttributeArray_42; }
	inline void set_EmptyAttributeArray_42(XmlSchemaAttributeU5BU5D_tFDBB08A9CF45CD6E2DE3A8AADCC8487BA5CD896F* value)
	{
		___EmptyAttributeArray_42 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyAttributeArray_42), value);
	}

	inline static int32_t get_offset_of_ValidStates_44() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields, ___ValidStates_44)); }
	inline BooleanU5BU2CU5D_tCD5854E1A55166EC93998DDED119F97FF5A5ECDC* get_ValidStates_44() const { return ___ValidStates_44; }
	inline BooleanU5BU2CU5D_tCD5854E1A55166EC93998DDED119F97FF5A5ECDC** get_address_of_ValidStates_44() { return &___ValidStates_44; }
	inline void set_ValidStates_44(BooleanU5BU2CU5D_tCD5854E1A55166EC93998DDED119F97FF5A5ECDC* value)
	{
		___ValidStates_44 = value;
		Il2CppCodeGenWriteBarrier((&___ValidStates_44), value);
	}

	inline static int32_t get_offset_of_MethodNames_45() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields, ___MethodNames_45)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_MethodNames_45() const { return ___MethodNames_45; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_MethodNames_45() { return &___MethodNames_45; }
	inline void set_MethodNames_45(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___MethodNames_45 = value;
		Il2CppCodeGenWriteBarrier((&___MethodNames_45), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATOR_TF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_H
#ifndef XSDATTRIBUTEENTRY_TE7E6C3E5854B31AD709DCD4F9E66111050DF9664_H
#define XSDATTRIBUTEENTRY_TE7E6C3E5854B31AD709DCD4F9E66111050DF9664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdBuilder/XsdAttributeEntry
struct  XsdAttributeEntry_tE7E6C3E5854B31AD709DCD4F9E66111050DF9664  : public RuntimeObject
{
public:
	// System.Xml.Schema.SchemaNames/Token System.Xml.Schema.XsdBuilder/XsdAttributeEntry::Attribute
	int32_t ___Attribute_0;
	// System.Xml.Schema.XsdBuilder/XsdBuildFunction System.Xml.Schema.XsdBuilder/XsdAttributeEntry::BuildFunc
	XsdBuildFunction_tCE6DE66119181E60067FBE2AD08DE0D013B7F41E * ___BuildFunc_1;

public:
	inline static int32_t get_offset_of_Attribute_0() { return static_cast<int32_t>(offsetof(XsdAttributeEntry_tE7E6C3E5854B31AD709DCD4F9E66111050DF9664, ___Attribute_0)); }
	inline int32_t get_Attribute_0() const { return ___Attribute_0; }
	inline int32_t* get_address_of_Attribute_0() { return &___Attribute_0; }
	inline void set_Attribute_0(int32_t value)
	{
		___Attribute_0 = value;
	}

	inline static int32_t get_offset_of_BuildFunc_1() { return static_cast<int32_t>(offsetof(XsdAttributeEntry_tE7E6C3E5854B31AD709DCD4F9E66111050DF9664, ___BuildFunc_1)); }
	inline XsdBuildFunction_tCE6DE66119181E60067FBE2AD08DE0D013B7F41E * get_BuildFunc_1() const { return ___BuildFunc_1; }
	inline XsdBuildFunction_tCE6DE66119181E60067FBE2AD08DE0D013B7F41E ** get_address_of_BuildFunc_1() { return &___BuildFunc_1; }
	inline void set_BuildFunc_1(XsdBuildFunction_tCE6DE66119181E60067FBE2AD08DE0D013B7F41E * value)
	{
		___BuildFunc_1 = value;
		Il2CppCodeGenWriteBarrier((&___BuildFunc_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDATTRIBUTEENTRY_TE7E6C3E5854B31AD709DCD4F9E66111050DF9664_H
#ifndef XSDENTRY_T96072848EB8D77A59E66E17AB46097648D7692C3_H
#define XSDENTRY_T96072848EB8D77A59E66E17AB46097648D7692C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdBuilder/XsdEntry
struct  XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3  : public RuntimeObject
{
public:
	// System.Xml.Schema.SchemaNames/Token System.Xml.Schema.XsdBuilder/XsdEntry::Name
	int32_t ___Name_0;
	// System.Xml.Schema.XsdBuilder/State System.Xml.Schema.XsdBuilder/XsdEntry::CurrentState
	int32_t ___CurrentState_1;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder/XsdEntry::NextStates
	StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* ___NextStates_2;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder/XsdEntry::Attributes
	XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* ___Attributes_3;
	// System.Xml.Schema.XsdBuilder/XsdInitFunction System.Xml.Schema.XsdBuilder/XsdEntry::InitFunc
	XsdInitFunction_t8B6E839BF5C55B9A918D919F005B971BFAEC5DF3 * ___InitFunc_4;
	// System.Xml.Schema.XsdBuilder/XsdEndChildFunction System.Xml.Schema.XsdBuilder/XsdEntry::EndChildFunc
	XsdEndChildFunction_t7FDE0B391499702A4683AB55B900623C4F8DFB17 * ___EndChildFunc_5;
	// System.Boolean System.Xml.Schema.XsdBuilder/XsdEntry::ParseContent
	bool ___ParseContent_6;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3, ___Name_0)); }
	inline int32_t get_Name_0() const { return ___Name_0; }
	inline int32_t* get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(int32_t value)
	{
		___Name_0 = value;
	}

	inline static int32_t get_offset_of_CurrentState_1() { return static_cast<int32_t>(offsetof(XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3, ___CurrentState_1)); }
	inline int32_t get_CurrentState_1() const { return ___CurrentState_1; }
	inline int32_t* get_address_of_CurrentState_1() { return &___CurrentState_1; }
	inline void set_CurrentState_1(int32_t value)
	{
		___CurrentState_1 = value;
	}

	inline static int32_t get_offset_of_NextStates_2() { return static_cast<int32_t>(offsetof(XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3, ___NextStates_2)); }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* get_NextStates_2() const { return ___NextStates_2; }
	inline StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0** get_address_of_NextStates_2() { return &___NextStates_2; }
	inline void set_NextStates_2(StateU5BU5D_tBD26CB5FFD3D15D0320E1E06102A4295F282D2E0* value)
	{
		___NextStates_2 = value;
		Il2CppCodeGenWriteBarrier((&___NextStates_2), value);
	}

	inline static int32_t get_offset_of_Attributes_3() { return static_cast<int32_t>(offsetof(XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3, ___Attributes_3)); }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* get_Attributes_3() const { return ___Attributes_3; }
	inline XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0** get_address_of_Attributes_3() { return &___Attributes_3; }
	inline void set_Attributes_3(XsdAttributeEntryU5BU5D_t7A92146B7073CF020D39672BB05FB4DC1B4C83A0* value)
	{
		___Attributes_3 = value;
		Il2CppCodeGenWriteBarrier((&___Attributes_3), value);
	}

	inline static int32_t get_offset_of_InitFunc_4() { return static_cast<int32_t>(offsetof(XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3, ___InitFunc_4)); }
	inline XsdInitFunction_t8B6E839BF5C55B9A918D919F005B971BFAEC5DF3 * get_InitFunc_4() const { return ___InitFunc_4; }
	inline XsdInitFunction_t8B6E839BF5C55B9A918D919F005B971BFAEC5DF3 ** get_address_of_InitFunc_4() { return &___InitFunc_4; }
	inline void set_InitFunc_4(XsdInitFunction_t8B6E839BF5C55B9A918D919F005B971BFAEC5DF3 * value)
	{
		___InitFunc_4 = value;
		Il2CppCodeGenWriteBarrier((&___InitFunc_4), value);
	}

	inline static int32_t get_offset_of_EndChildFunc_5() { return static_cast<int32_t>(offsetof(XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3, ___EndChildFunc_5)); }
	inline XsdEndChildFunction_t7FDE0B391499702A4683AB55B900623C4F8DFB17 * get_EndChildFunc_5() const { return ___EndChildFunc_5; }
	inline XsdEndChildFunction_t7FDE0B391499702A4683AB55B900623C4F8DFB17 ** get_address_of_EndChildFunc_5() { return &___EndChildFunc_5; }
	inline void set_EndChildFunc_5(XsdEndChildFunction_t7FDE0B391499702A4683AB55B900623C4F8DFB17 * value)
	{
		___EndChildFunc_5 = value;
		Il2CppCodeGenWriteBarrier((&___EndChildFunc_5), value);
	}

	inline static int32_t get_offset_of_ParseContent_6() { return static_cast<int32_t>(offsetof(XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3, ___ParseContent_6)); }
	inline bool get_ParseContent_6() const { return ___ParseContent_6; }
	inline bool* get_address_of_ParseContent_6() { return &___ParseContent_6; }
	inline void set_ParseContent_6(bool value)
	{
		___ParseContent_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDENTRY_T96072848EB8D77A59E66E17AB46097648D7692C3_H
#ifndef PARSER_T402903C4103D1084228988A8DA76C1FCB8D890B9_H
#define PARSER_T402903C4103D1084228988A8DA76C1FCB8D890B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDateTime/Parser
struct  Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9 
{
public:
	// System.Xml.Schema.XsdDateTime/DateTimeTypeCode System.Xml.Schema.XsdDateTime/Parser::typeCode
	int32_t ___typeCode_0;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::year
	int32_t ___year_1;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::month
	int32_t ___month_2;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::day
	int32_t ___day_3;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::hour
	int32_t ___hour_4;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::minute
	int32_t ___minute_5;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::second
	int32_t ___second_6;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::fraction
	int32_t ___fraction_7;
	// System.Xml.Schema.XsdDateTime/XsdDateTimeKind System.Xml.Schema.XsdDateTime/Parser::kind
	int32_t ___kind_8;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::zoneHour
	int32_t ___zoneHour_9;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::zoneMinute
	int32_t ___zoneMinute_10;
	// System.String System.Xml.Schema.XsdDateTime/Parser::text
	String_t* ___text_11;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::length
	int32_t ___length_12;

public:
	inline static int32_t get_offset_of_typeCode_0() { return static_cast<int32_t>(offsetof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9, ___typeCode_0)); }
	inline int32_t get_typeCode_0() const { return ___typeCode_0; }
	inline int32_t* get_address_of_typeCode_0() { return &___typeCode_0; }
	inline void set_typeCode_0(int32_t value)
	{
		___typeCode_0 = value;
	}

	inline static int32_t get_offset_of_year_1() { return static_cast<int32_t>(offsetof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9, ___year_1)); }
	inline int32_t get_year_1() const { return ___year_1; }
	inline int32_t* get_address_of_year_1() { return &___year_1; }
	inline void set_year_1(int32_t value)
	{
		___year_1 = value;
	}

	inline static int32_t get_offset_of_month_2() { return static_cast<int32_t>(offsetof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9, ___month_2)); }
	inline int32_t get_month_2() const { return ___month_2; }
	inline int32_t* get_address_of_month_2() { return &___month_2; }
	inline void set_month_2(int32_t value)
	{
		___month_2 = value;
	}

	inline static int32_t get_offset_of_day_3() { return static_cast<int32_t>(offsetof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9, ___day_3)); }
	inline int32_t get_day_3() const { return ___day_3; }
	inline int32_t* get_address_of_day_3() { return &___day_3; }
	inline void set_day_3(int32_t value)
	{
		___day_3 = value;
	}

	inline static int32_t get_offset_of_hour_4() { return static_cast<int32_t>(offsetof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9, ___hour_4)); }
	inline int32_t get_hour_4() const { return ___hour_4; }
	inline int32_t* get_address_of_hour_4() { return &___hour_4; }
	inline void set_hour_4(int32_t value)
	{
		___hour_4 = value;
	}

	inline static int32_t get_offset_of_minute_5() { return static_cast<int32_t>(offsetof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9, ___minute_5)); }
	inline int32_t get_minute_5() const { return ___minute_5; }
	inline int32_t* get_address_of_minute_5() { return &___minute_5; }
	inline void set_minute_5(int32_t value)
	{
		___minute_5 = value;
	}

	inline static int32_t get_offset_of_second_6() { return static_cast<int32_t>(offsetof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9, ___second_6)); }
	inline int32_t get_second_6() const { return ___second_6; }
	inline int32_t* get_address_of_second_6() { return &___second_6; }
	inline void set_second_6(int32_t value)
	{
		___second_6 = value;
	}

	inline static int32_t get_offset_of_fraction_7() { return static_cast<int32_t>(offsetof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9, ___fraction_7)); }
	inline int32_t get_fraction_7() const { return ___fraction_7; }
	inline int32_t* get_address_of_fraction_7() { return &___fraction_7; }
	inline void set_fraction_7(int32_t value)
	{
		___fraction_7 = value;
	}

	inline static int32_t get_offset_of_kind_8() { return static_cast<int32_t>(offsetof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9, ___kind_8)); }
	inline int32_t get_kind_8() const { return ___kind_8; }
	inline int32_t* get_address_of_kind_8() { return &___kind_8; }
	inline void set_kind_8(int32_t value)
	{
		___kind_8 = value;
	}

	inline static int32_t get_offset_of_zoneHour_9() { return static_cast<int32_t>(offsetof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9, ___zoneHour_9)); }
	inline int32_t get_zoneHour_9() const { return ___zoneHour_9; }
	inline int32_t* get_address_of_zoneHour_9() { return &___zoneHour_9; }
	inline void set_zoneHour_9(int32_t value)
	{
		___zoneHour_9 = value;
	}

	inline static int32_t get_offset_of_zoneMinute_10() { return static_cast<int32_t>(offsetof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9, ___zoneMinute_10)); }
	inline int32_t get_zoneMinute_10() const { return ___zoneMinute_10; }
	inline int32_t* get_address_of_zoneMinute_10() { return &___zoneMinute_10; }
	inline void set_zoneMinute_10(int32_t value)
	{
		___zoneMinute_10 = value;
	}

	inline static int32_t get_offset_of_text_11() { return static_cast<int32_t>(offsetof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9, ___text_11)); }
	inline String_t* get_text_11() const { return ___text_11; }
	inline String_t** get_address_of_text_11() { return &___text_11; }
	inline void set_text_11(String_t* value)
	{
		___text_11 = value;
		Il2CppCodeGenWriteBarrier((&___text_11), value);
	}

	inline static int32_t get_offset_of_length_12() { return static_cast<int32_t>(offsetof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9, ___length_12)); }
	inline int32_t get_length_12() const { return ___length_12; }
	inline int32_t* get_address_of_length_12() { return &___length_12; }
	inline void set_length_12(int32_t value)
	{
		___length_12 = value;
	}
};

struct Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9_StaticFields
{
public:
	// System.Int32[] System.Xml.Schema.XsdDateTime/Parser::Power10
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Power10_13;

public:
	inline static int32_t get_offset_of_Power10_13() { return static_cast<int32_t>(offsetof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9_StaticFields, ___Power10_13)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Power10_13() const { return ___Power10_13; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Power10_13() { return &___Power10_13; }
	inline void set_Power10_13(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Power10_13 = value;
		Il2CppCodeGenWriteBarrier((&___Power10_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.XsdDateTime/Parser
struct Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9_marshaled_pinvoke
{
	int32_t ___typeCode_0;
	int32_t ___year_1;
	int32_t ___month_2;
	int32_t ___day_3;
	int32_t ___hour_4;
	int32_t ___minute_5;
	int32_t ___second_6;
	int32_t ___fraction_7;
	int32_t ___kind_8;
	int32_t ___zoneHour_9;
	int32_t ___zoneMinute_10;
	char* ___text_11;
	int32_t ___length_12;
};
// Native definition for COM marshalling of System.Xml.Schema.XsdDateTime/Parser
struct Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9_marshaled_com
{
	int32_t ___typeCode_0;
	int32_t ___year_1;
	int32_t ___month_2;
	int32_t ___day_3;
	int32_t ___hour_4;
	int32_t ___minute_5;
	int32_t ___second_6;
	int32_t ___fraction_7;
	int32_t ___kind_8;
	int32_t ___zoneHour_9;
	int32_t ___zoneMinute_10;
	Il2CppChar* ___text_11;
	int32_t ___length_12;
};
#endif // PARSER_T402903C4103D1084228988A8DA76C1FCB8D890B9_H
#ifndef XSDVALIDATOR_T3CE76515AAD3C18388E441A25640966E274B1D8A_H
#define XSDVALIDATOR_T3CE76515AAD3C18388E441A25640966E274B1D8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdValidator
struct  XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A  : public BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43
{
public:
	// System.Int32 System.Xml.Schema.XsdValidator::startIDConstraint
	int32_t ___startIDConstraint_15;
	// System.Xml.HWStack System.Xml.Schema.XsdValidator::validationStack
	HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * ___validationStack_16;
	// System.Collections.Hashtable System.Xml.Schema.XsdValidator::attPresence
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___attPresence_17;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.XsdValidator::nsManager
	XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * ___nsManager_18;
	// System.Boolean System.Xml.Schema.XsdValidator::bManageNamespaces
	bool ___bManageNamespaces_19;
	// System.Collections.Hashtable System.Xml.Schema.XsdValidator::IDs
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___IDs_20;
	// System.Xml.Schema.IdRefNode System.Xml.Schema.XsdValidator::idRefListHead
	IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 * ___idRefListHead_21;
	// System.Xml.Schema.Parser System.Xml.Schema.XsdValidator::inlineSchemaParser
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920 * ___inlineSchemaParser_22;
	// System.Xml.Schema.XmlSchemaContentProcessing System.Xml.Schema.XsdValidator::processContents
	int32_t ___processContents_23;
	// System.String System.Xml.Schema.XsdValidator::NsXmlNs
	String_t* ___NsXmlNs_27;
	// System.String System.Xml.Schema.XsdValidator::NsXs
	String_t* ___NsXs_28;
	// System.String System.Xml.Schema.XsdValidator::NsXsi
	String_t* ___NsXsi_29;
	// System.String System.Xml.Schema.XsdValidator::XsiType
	String_t* ___XsiType_30;
	// System.String System.Xml.Schema.XsdValidator::XsiNil
	String_t* ___XsiNil_31;
	// System.String System.Xml.Schema.XsdValidator::XsiSchemaLocation
	String_t* ___XsiSchemaLocation_32;
	// System.String System.Xml.Schema.XsdValidator::XsiNoNamespaceSchemaLocation
	String_t* ___XsiNoNamespaceSchemaLocation_33;
	// System.String System.Xml.Schema.XsdValidator::XsdSchema
	String_t* ___XsdSchema_34;

public:
	inline static int32_t get_offset_of_startIDConstraint_15() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___startIDConstraint_15)); }
	inline int32_t get_startIDConstraint_15() const { return ___startIDConstraint_15; }
	inline int32_t* get_address_of_startIDConstraint_15() { return &___startIDConstraint_15; }
	inline void set_startIDConstraint_15(int32_t value)
	{
		___startIDConstraint_15 = value;
	}

	inline static int32_t get_offset_of_validationStack_16() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___validationStack_16)); }
	inline HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * get_validationStack_16() const { return ___validationStack_16; }
	inline HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE ** get_address_of_validationStack_16() { return &___validationStack_16; }
	inline void set_validationStack_16(HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * value)
	{
		___validationStack_16 = value;
		Il2CppCodeGenWriteBarrier((&___validationStack_16), value);
	}

	inline static int32_t get_offset_of_attPresence_17() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___attPresence_17)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_attPresence_17() const { return ___attPresence_17; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_attPresence_17() { return &___attPresence_17; }
	inline void set_attPresence_17(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___attPresence_17 = value;
		Il2CppCodeGenWriteBarrier((&___attPresence_17), value);
	}

	inline static int32_t get_offset_of_nsManager_18() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___nsManager_18)); }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * get_nsManager_18() const { return ___nsManager_18; }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F ** get_address_of_nsManager_18() { return &___nsManager_18; }
	inline void set_nsManager_18(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * value)
	{
		___nsManager_18 = value;
		Il2CppCodeGenWriteBarrier((&___nsManager_18), value);
	}

	inline static int32_t get_offset_of_bManageNamespaces_19() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___bManageNamespaces_19)); }
	inline bool get_bManageNamespaces_19() const { return ___bManageNamespaces_19; }
	inline bool* get_address_of_bManageNamespaces_19() { return &___bManageNamespaces_19; }
	inline void set_bManageNamespaces_19(bool value)
	{
		___bManageNamespaces_19 = value;
	}

	inline static int32_t get_offset_of_IDs_20() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___IDs_20)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_IDs_20() const { return ___IDs_20; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_IDs_20() { return &___IDs_20; }
	inline void set_IDs_20(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___IDs_20 = value;
		Il2CppCodeGenWriteBarrier((&___IDs_20), value);
	}

	inline static int32_t get_offset_of_idRefListHead_21() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___idRefListHead_21)); }
	inline IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 * get_idRefListHead_21() const { return ___idRefListHead_21; }
	inline IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 ** get_address_of_idRefListHead_21() { return &___idRefListHead_21; }
	inline void set_idRefListHead_21(IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 * value)
	{
		___idRefListHead_21 = value;
		Il2CppCodeGenWriteBarrier((&___idRefListHead_21), value);
	}

	inline static int32_t get_offset_of_inlineSchemaParser_22() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___inlineSchemaParser_22)); }
	inline Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920 * get_inlineSchemaParser_22() const { return ___inlineSchemaParser_22; }
	inline Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920 ** get_address_of_inlineSchemaParser_22() { return &___inlineSchemaParser_22; }
	inline void set_inlineSchemaParser_22(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920 * value)
	{
		___inlineSchemaParser_22 = value;
		Il2CppCodeGenWriteBarrier((&___inlineSchemaParser_22), value);
	}

	inline static int32_t get_offset_of_processContents_23() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___processContents_23)); }
	inline int32_t get_processContents_23() const { return ___processContents_23; }
	inline int32_t* get_address_of_processContents_23() { return &___processContents_23; }
	inline void set_processContents_23(int32_t value)
	{
		___processContents_23 = value;
	}

	inline static int32_t get_offset_of_NsXmlNs_27() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___NsXmlNs_27)); }
	inline String_t* get_NsXmlNs_27() const { return ___NsXmlNs_27; }
	inline String_t** get_address_of_NsXmlNs_27() { return &___NsXmlNs_27; }
	inline void set_NsXmlNs_27(String_t* value)
	{
		___NsXmlNs_27 = value;
		Il2CppCodeGenWriteBarrier((&___NsXmlNs_27), value);
	}

	inline static int32_t get_offset_of_NsXs_28() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___NsXs_28)); }
	inline String_t* get_NsXs_28() const { return ___NsXs_28; }
	inline String_t** get_address_of_NsXs_28() { return &___NsXs_28; }
	inline void set_NsXs_28(String_t* value)
	{
		___NsXs_28 = value;
		Il2CppCodeGenWriteBarrier((&___NsXs_28), value);
	}

	inline static int32_t get_offset_of_NsXsi_29() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___NsXsi_29)); }
	inline String_t* get_NsXsi_29() const { return ___NsXsi_29; }
	inline String_t** get_address_of_NsXsi_29() { return &___NsXsi_29; }
	inline void set_NsXsi_29(String_t* value)
	{
		___NsXsi_29 = value;
		Il2CppCodeGenWriteBarrier((&___NsXsi_29), value);
	}

	inline static int32_t get_offset_of_XsiType_30() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___XsiType_30)); }
	inline String_t* get_XsiType_30() const { return ___XsiType_30; }
	inline String_t** get_address_of_XsiType_30() { return &___XsiType_30; }
	inline void set_XsiType_30(String_t* value)
	{
		___XsiType_30 = value;
		Il2CppCodeGenWriteBarrier((&___XsiType_30), value);
	}

	inline static int32_t get_offset_of_XsiNil_31() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___XsiNil_31)); }
	inline String_t* get_XsiNil_31() const { return ___XsiNil_31; }
	inline String_t** get_address_of_XsiNil_31() { return &___XsiNil_31; }
	inline void set_XsiNil_31(String_t* value)
	{
		___XsiNil_31 = value;
		Il2CppCodeGenWriteBarrier((&___XsiNil_31), value);
	}

	inline static int32_t get_offset_of_XsiSchemaLocation_32() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___XsiSchemaLocation_32)); }
	inline String_t* get_XsiSchemaLocation_32() const { return ___XsiSchemaLocation_32; }
	inline String_t** get_address_of_XsiSchemaLocation_32() { return &___XsiSchemaLocation_32; }
	inline void set_XsiSchemaLocation_32(String_t* value)
	{
		___XsiSchemaLocation_32 = value;
		Il2CppCodeGenWriteBarrier((&___XsiSchemaLocation_32), value);
	}

	inline static int32_t get_offset_of_XsiNoNamespaceSchemaLocation_33() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___XsiNoNamespaceSchemaLocation_33)); }
	inline String_t* get_XsiNoNamespaceSchemaLocation_33() const { return ___XsiNoNamespaceSchemaLocation_33; }
	inline String_t** get_address_of_XsiNoNamespaceSchemaLocation_33() { return &___XsiNoNamespaceSchemaLocation_33; }
	inline void set_XsiNoNamespaceSchemaLocation_33(String_t* value)
	{
		___XsiNoNamespaceSchemaLocation_33 = value;
		Il2CppCodeGenWriteBarrier((&___XsiNoNamespaceSchemaLocation_33), value);
	}

	inline static int32_t get_offset_of_XsdSchema_34() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A, ___XsdSchema_34)); }
	inline String_t* get_XsdSchema_34() const { return ___XsdSchema_34; }
	inline String_t** get_address_of_XsdSchema_34() { return &___XsdSchema_34; }
	inline void set_XsdSchema_34(String_t* value)
	{
		___XsdSchema_34 = value;
		Il2CppCodeGenWriteBarrier((&___XsdSchema_34), value);
	}
};

struct XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XsdValidator::dtCDATA
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___dtCDATA_24;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XsdValidator::dtQName
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___dtQName_25;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XsdValidator::dtStringArray
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___dtStringArray_26;

public:
	inline static int32_t get_offset_of_dtCDATA_24() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A_StaticFields, ___dtCDATA_24)); }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * get_dtCDATA_24() const { return ___dtCDATA_24; }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 ** get_address_of_dtCDATA_24() { return &___dtCDATA_24; }
	inline void set_dtCDATA_24(XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * value)
	{
		___dtCDATA_24 = value;
		Il2CppCodeGenWriteBarrier((&___dtCDATA_24), value);
	}

	inline static int32_t get_offset_of_dtQName_25() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A_StaticFields, ___dtQName_25)); }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * get_dtQName_25() const { return ___dtQName_25; }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 ** get_address_of_dtQName_25() { return &___dtQName_25; }
	inline void set_dtQName_25(XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * value)
	{
		___dtQName_25 = value;
		Il2CppCodeGenWriteBarrier((&___dtQName_25), value);
	}

	inline static int32_t get_offset_of_dtStringArray_26() { return static_cast<int32_t>(offsetof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A_StaticFields, ___dtStringArray_26)); }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * get_dtStringArray_26() const { return ___dtStringArray_26; }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 ** get_address_of_dtStringArray_26() { return &___dtStringArray_26; }
	inline void set_dtStringArray_26(XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * value)
	{
		___dtStringArray_26 = value;
		Il2CppCodeGenWriteBarrier((&___dtStringArray_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDVALIDATOR_T3CE76515AAD3C18388E441A25640966E274B1D8A_H
#ifndef UNITYTLS_INTERFACE_STRUCT_T0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF_H
#define UNITYTLS_INTERFACE_STRUCT_T0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct
struct  unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF  : public RuntimeObject
{
public:
	// System.UInt64 Mono.Unity.UnityTls/unitytls_interface_struct::UNITYTLS_INVALID_HANDLE
	uint64_t ___UNITYTLS_INVALID_HANDLE_0;
	// Mono.Unity.UnityTls/unitytls_tlsctx_protocolrange Mono.Unity.UnityTls/unitytls_interface_struct::UNITYTLS_TLSCTX_PROTOCOLRANGE_DEFAULT
	unitytls_tlsctx_protocolrange_t36243D72F83DAD47C95928314F58026DE8D38F47  ___UNITYTLS_TLSCTX_PROTOCOLRANGE_DEFAULT_1;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_errorstate_create_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_errorstate_create
	unitytls_errorstate_create_t_t104BADBBE1265BD1A3F84C153EB7A67CDDBF35A9 * ___unitytls_errorstate_create_2;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_errorstate_raise_error_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_errorstate_raise_error
	unitytls_errorstate_raise_error_t_tC441A37D4A6F1BAC1AFCA0108D4F7570EFF9E0D1 * ___unitytls_errorstate_raise_error_3;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_get_ref_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_key_get_ref
	unitytls_key_get_ref_t_t2F4EF4CD2F6AFC4F2D166953E834C6F0A13382A7 * ___unitytls_key_get_ref_4;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_parse_der_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_key_parse_der
	unitytls_key_parse_der_t_t2ABD1C146C8B9405F6E5A78CD59A779EA607741B * ___unitytls_key_parse_der_5;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_parse_pem_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_key_parse_pem
	unitytls_key_parse_pem_t_tB4BCEBA4194442C8C85FA19E80B808D0EDA462AB * ___unitytls_key_parse_pem_6;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_free_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_key_free
	unitytls_key_free_t_tCC7AD95D3B758BB99785645E65EDCD65A1D243AB * ___unitytls_key_free_7;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509_export_der_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_x509_export_der
	unitytls_x509_export_der_t_tB0D0A02DE7E75757AFCA780298BF95467BF82856 * ___unitytls_x509_export_der_8;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_get_ref_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_x509list_get_ref
	unitytls_x509list_get_ref_t_t1FAB0CD82E536E0C9EB5255B145FC5AF434B3986 * ___unitytls_x509list_get_ref_9;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_get_x509_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_x509list_get_x509
	unitytls_x509list_get_x509_t_t028BB06EEB95E8F62511F2301B90D8181F4FFDB5 * ___unitytls_x509list_get_x509_10;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_create_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_x509list_create
	unitytls_x509list_create_t_tC040C2CF47D5426B7F6B1D6A2751507DC681CFF3 * ___unitytls_x509list_create_11;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_append_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_x509list_append
	unitytls_x509list_append_t_tAB1C185C77DFD6BD96DF7909370AA1FAD6BB90AF * ___unitytls_x509list_append_12;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_append_der_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_x509list_append_der
	unitytls_x509list_append_der_t_tDA1C93A382058FB563F8772B119D5B598DC37A5C * ___unitytls_x509list_append_der_13;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_append_der_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_x509list_append_pem
	unitytls_x509list_append_der_t_tDA1C93A382058FB563F8772B119D5B598DC37A5C * ___unitytls_x509list_append_pem_14;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_free_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_x509list_free
	unitytls_x509list_free_t_tE3FC523507F07BD9901D84E9F6968CD8A583FF09 * ___unitytls_x509list_free_15;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509verify_default_ca_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_x509verify_default_ca
	unitytls_x509verify_default_ca_t_t4BACB6B49AA85C025AF9B18B3F30F63C9881DE2D * ___unitytls_x509verify_default_ca_16;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509verify_explicit_ca_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_x509verify_explicit_ca
	unitytls_x509verify_explicit_ca_t_t6C8BE964C5EE9B24D3734F028EFCD83F5893D2E6 * ___unitytls_x509verify_explicit_ca_17;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_create_server_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_tlsctx_create_server
	unitytls_tlsctx_create_server_t_t6E7812D40DDD91958E3CFBB92B5F5748D477E19D * ___unitytls_tlsctx_create_server_18;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_create_client_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_tlsctx_create_client
	unitytls_tlsctx_create_client_t_tD9DFBDB5559983F0E11A67FA92E0F7182114C8F2 * ___unitytls_tlsctx_create_client_19;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_server_require_client_authentication_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_tlsctx_server_require_client_authentication
	unitytls_tlsctx_server_require_client_authentication_t_t77B3CAFF25690A45405E3C957E40CC4FF83B49C6 * ___unitytls_tlsctx_server_require_client_authentication_20;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_certificate_callback_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_tlsctx_set_certificate_callback
	unitytls_tlsctx_set_certificate_callback_t_tC4864FE0F6A3398A572F2511AA64C72126640937 * ___unitytls_tlsctx_set_certificate_callback_21;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_trace_callback_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_tlsctx_set_trace_callback
	unitytls_tlsctx_set_trace_callback_t_tA11F424F68D297B6FD2B2EA26C6764F80146662A * ___unitytls_tlsctx_set_trace_callback_22;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_x509verify_callback_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_tlsctx_set_x509verify_callback
	unitytls_tlsctx_set_x509verify_callback_t_t34EEB7BA38CA2C86F847416785ADB22BC4A04F4B * ___unitytls_tlsctx_set_x509verify_callback_23;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_supported_ciphersuites_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_tlsctx_set_supported_ciphersuites
	unitytls_tlsctx_set_supported_ciphersuites_t_t6914054EA0F7A59C8A4ED4B9CDD5AF143F7D8BFE * ___unitytls_tlsctx_set_supported_ciphersuites_24;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_get_ciphersuite_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_tlsctx_get_ciphersuite
	unitytls_tlsctx_get_ciphersuite_t_t94A91CB42A2EBB2CC598EF3E278770AFD80696A0 * ___unitytls_tlsctx_get_ciphersuite_25;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_get_protocol_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_tlsctx_get_protocol
	unitytls_tlsctx_get_protocol_t_tB29092875D3CBD25E4461BFD165B5373FA54DB14 * ___unitytls_tlsctx_get_protocol_26;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_process_handshake_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_tlsctx_process_handshake
	unitytls_tlsctx_process_handshake_t_tC8AAF317CBE4CA216F22BF031ECF89315B963C9D * ___unitytls_tlsctx_process_handshake_27;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_read_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_tlsctx_read
	unitytls_tlsctx_read_t_tA8D1209D5F488E02F826EE2362F5AA61C8FF2EE2 * ___unitytls_tlsctx_read_28;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_write_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_tlsctx_write
	unitytls_tlsctx_write_t_t0B4A49BBA592FE4EC0630B490463AE116AF07C9C * ___unitytls_tlsctx_write_29;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_notify_close_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_tlsctx_notify_close
	unitytls_tlsctx_notify_close_t_t2FC4C08BACF1AEA509ABCAF3B22475E196E74A0D * ___unitytls_tlsctx_notify_close_30;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_free_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_tlsctx_free
	unitytls_tlsctx_free_t_tB27A3B6F9D1B784ABE082849EAB6B81F51FAC8E2 * ___unitytls_tlsctx_free_31;
	// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_random_generate_bytes_t Mono.Unity.UnityTls/unitytls_interface_struct::unitytls_random_generate_bytes
	unitytls_random_generate_bytes_t_t494B8599A6D4247BB0C8AB7341DDC73BE42623F7 * ___unitytls_random_generate_bytes_32;

public:
	inline static int32_t get_offset_of_UNITYTLS_INVALID_HANDLE_0() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___UNITYTLS_INVALID_HANDLE_0)); }
	inline uint64_t get_UNITYTLS_INVALID_HANDLE_0() const { return ___UNITYTLS_INVALID_HANDLE_0; }
	inline uint64_t* get_address_of_UNITYTLS_INVALID_HANDLE_0() { return &___UNITYTLS_INVALID_HANDLE_0; }
	inline void set_UNITYTLS_INVALID_HANDLE_0(uint64_t value)
	{
		___UNITYTLS_INVALID_HANDLE_0 = value;
	}

	inline static int32_t get_offset_of_UNITYTLS_TLSCTX_PROTOCOLRANGE_DEFAULT_1() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___UNITYTLS_TLSCTX_PROTOCOLRANGE_DEFAULT_1)); }
	inline unitytls_tlsctx_protocolrange_t36243D72F83DAD47C95928314F58026DE8D38F47  get_UNITYTLS_TLSCTX_PROTOCOLRANGE_DEFAULT_1() const { return ___UNITYTLS_TLSCTX_PROTOCOLRANGE_DEFAULT_1; }
	inline unitytls_tlsctx_protocolrange_t36243D72F83DAD47C95928314F58026DE8D38F47 * get_address_of_UNITYTLS_TLSCTX_PROTOCOLRANGE_DEFAULT_1() { return &___UNITYTLS_TLSCTX_PROTOCOLRANGE_DEFAULT_1; }
	inline void set_UNITYTLS_TLSCTX_PROTOCOLRANGE_DEFAULT_1(unitytls_tlsctx_protocolrange_t36243D72F83DAD47C95928314F58026DE8D38F47  value)
	{
		___UNITYTLS_TLSCTX_PROTOCOLRANGE_DEFAULT_1 = value;
	}

	inline static int32_t get_offset_of_unitytls_errorstate_create_2() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_errorstate_create_2)); }
	inline unitytls_errorstate_create_t_t104BADBBE1265BD1A3F84C153EB7A67CDDBF35A9 * get_unitytls_errorstate_create_2() const { return ___unitytls_errorstate_create_2; }
	inline unitytls_errorstate_create_t_t104BADBBE1265BD1A3F84C153EB7A67CDDBF35A9 ** get_address_of_unitytls_errorstate_create_2() { return &___unitytls_errorstate_create_2; }
	inline void set_unitytls_errorstate_create_2(unitytls_errorstate_create_t_t104BADBBE1265BD1A3F84C153EB7A67CDDBF35A9 * value)
	{
		___unitytls_errorstate_create_2 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_errorstate_create_2), value);
	}

	inline static int32_t get_offset_of_unitytls_errorstate_raise_error_3() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_errorstate_raise_error_3)); }
	inline unitytls_errorstate_raise_error_t_tC441A37D4A6F1BAC1AFCA0108D4F7570EFF9E0D1 * get_unitytls_errorstate_raise_error_3() const { return ___unitytls_errorstate_raise_error_3; }
	inline unitytls_errorstate_raise_error_t_tC441A37D4A6F1BAC1AFCA0108D4F7570EFF9E0D1 ** get_address_of_unitytls_errorstate_raise_error_3() { return &___unitytls_errorstate_raise_error_3; }
	inline void set_unitytls_errorstate_raise_error_3(unitytls_errorstate_raise_error_t_tC441A37D4A6F1BAC1AFCA0108D4F7570EFF9E0D1 * value)
	{
		___unitytls_errorstate_raise_error_3 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_errorstate_raise_error_3), value);
	}

	inline static int32_t get_offset_of_unitytls_key_get_ref_4() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_key_get_ref_4)); }
	inline unitytls_key_get_ref_t_t2F4EF4CD2F6AFC4F2D166953E834C6F0A13382A7 * get_unitytls_key_get_ref_4() const { return ___unitytls_key_get_ref_4; }
	inline unitytls_key_get_ref_t_t2F4EF4CD2F6AFC4F2D166953E834C6F0A13382A7 ** get_address_of_unitytls_key_get_ref_4() { return &___unitytls_key_get_ref_4; }
	inline void set_unitytls_key_get_ref_4(unitytls_key_get_ref_t_t2F4EF4CD2F6AFC4F2D166953E834C6F0A13382A7 * value)
	{
		___unitytls_key_get_ref_4 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_key_get_ref_4), value);
	}

	inline static int32_t get_offset_of_unitytls_key_parse_der_5() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_key_parse_der_5)); }
	inline unitytls_key_parse_der_t_t2ABD1C146C8B9405F6E5A78CD59A779EA607741B * get_unitytls_key_parse_der_5() const { return ___unitytls_key_parse_der_5; }
	inline unitytls_key_parse_der_t_t2ABD1C146C8B9405F6E5A78CD59A779EA607741B ** get_address_of_unitytls_key_parse_der_5() { return &___unitytls_key_parse_der_5; }
	inline void set_unitytls_key_parse_der_5(unitytls_key_parse_der_t_t2ABD1C146C8B9405F6E5A78CD59A779EA607741B * value)
	{
		___unitytls_key_parse_der_5 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_key_parse_der_5), value);
	}

	inline static int32_t get_offset_of_unitytls_key_parse_pem_6() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_key_parse_pem_6)); }
	inline unitytls_key_parse_pem_t_tB4BCEBA4194442C8C85FA19E80B808D0EDA462AB * get_unitytls_key_parse_pem_6() const { return ___unitytls_key_parse_pem_6; }
	inline unitytls_key_parse_pem_t_tB4BCEBA4194442C8C85FA19E80B808D0EDA462AB ** get_address_of_unitytls_key_parse_pem_6() { return &___unitytls_key_parse_pem_6; }
	inline void set_unitytls_key_parse_pem_6(unitytls_key_parse_pem_t_tB4BCEBA4194442C8C85FA19E80B808D0EDA462AB * value)
	{
		___unitytls_key_parse_pem_6 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_key_parse_pem_6), value);
	}

	inline static int32_t get_offset_of_unitytls_key_free_7() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_key_free_7)); }
	inline unitytls_key_free_t_tCC7AD95D3B758BB99785645E65EDCD65A1D243AB * get_unitytls_key_free_7() const { return ___unitytls_key_free_7; }
	inline unitytls_key_free_t_tCC7AD95D3B758BB99785645E65EDCD65A1D243AB ** get_address_of_unitytls_key_free_7() { return &___unitytls_key_free_7; }
	inline void set_unitytls_key_free_7(unitytls_key_free_t_tCC7AD95D3B758BB99785645E65EDCD65A1D243AB * value)
	{
		___unitytls_key_free_7 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_key_free_7), value);
	}

	inline static int32_t get_offset_of_unitytls_x509_export_der_8() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_x509_export_der_8)); }
	inline unitytls_x509_export_der_t_tB0D0A02DE7E75757AFCA780298BF95467BF82856 * get_unitytls_x509_export_der_8() const { return ___unitytls_x509_export_der_8; }
	inline unitytls_x509_export_der_t_tB0D0A02DE7E75757AFCA780298BF95467BF82856 ** get_address_of_unitytls_x509_export_der_8() { return &___unitytls_x509_export_der_8; }
	inline void set_unitytls_x509_export_der_8(unitytls_x509_export_der_t_tB0D0A02DE7E75757AFCA780298BF95467BF82856 * value)
	{
		___unitytls_x509_export_der_8 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_x509_export_der_8), value);
	}

	inline static int32_t get_offset_of_unitytls_x509list_get_ref_9() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_x509list_get_ref_9)); }
	inline unitytls_x509list_get_ref_t_t1FAB0CD82E536E0C9EB5255B145FC5AF434B3986 * get_unitytls_x509list_get_ref_9() const { return ___unitytls_x509list_get_ref_9; }
	inline unitytls_x509list_get_ref_t_t1FAB0CD82E536E0C9EB5255B145FC5AF434B3986 ** get_address_of_unitytls_x509list_get_ref_9() { return &___unitytls_x509list_get_ref_9; }
	inline void set_unitytls_x509list_get_ref_9(unitytls_x509list_get_ref_t_t1FAB0CD82E536E0C9EB5255B145FC5AF434B3986 * value)
	{
		___unitytls_x509list_get_ref_9 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_x509list_get_ref_9), value);
	}

	inline static int32_t get_offset_of_unitytls_x509list_get_x509_10() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_x509list_get_x509_10)); }
	inline unitytls_x509list_get_x509_t_t028BB06EEB95E8F62511F2301B90D8181F4FFDB5 * get_unitytls_x509list_get_x509_10() const { return ___unitytls_x509list_get_x509_10; }
	inline unitytls_x509list_get_x509_t_t028BB06EEB95E8F62511F2301B90D8181F4FFDB5 ** get_address_of_unitytls_x509list_get_x509_10() { return &___unitytls_x509list_get_x509_10; }
	inline void set_unitytls_x509list_get_x509_10(unitytls_x509list_get_x509_t_t028BB06EEB95E8F62511F2301B90D8181F4FFDB5 * value)
	{
		___unitytls_x509list_get_x509_10 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_x509list_get_x509_10), value);
	}

	inline static int32_t get_offset_of_unitytls_x509list_create_11() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_x509list_create_11)); }
	inline unitytls_x509list_create_t_tC040C2CF47D5426B7F6B1D6A2751507DC681CFF3 * get_unitytls_x509list_create_11() const { return ___unitytls_x509list_create_11; }
	inline unitytls_x509list_create_t_tC040C2CF47D5426B7F6B1D6A2751507DC681CFF3 ** get_address_of_unitytls_x509list_create_11() { return &___unitytls_x509list_create_11; }
	inline void set_unitytls_x509list_create_11(unitytls_x509list_create_t_tC040C2CF47D5426B7F6B1D6A2751507DC681CFF3 * value)
	{
		___unitytls_x509list_create_11 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_x509list_create_11), value);
	}

	inline static int32_t get_offset_of_unitytls_x509list_append_12() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_x509list_append_12)); }
	inline unitytls_x509list_append_t_tAB1C185C77DFD6BD96DF7909370AA1FAD6BB90AF * get_unitytls_x509list_append_12() const { return ___unitytls_x509list_append_12; }
	inline unitytls_x509list_append_t_tAB1C185C77DFD6BD96DF7909370AA1FAD6BB90AF ** get_address_of_unitytls_x509list_append_12() { return &___unitytls_x509list_append_12; }
	inline void set_unitytls_x509list_append_12(unitytls_x509list_append_t_tAB1C185C77DFD6BD96DF7909370AA1FAD6BB90AF * value)
	{
		___unitytls_x509list_append_12 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_x509list_append_12), value);
	}

	inline static int32_t get_offset_of_unitytls_x509list_append_der_13() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_x509list_append_der_13)); }
	inline unitytls_x509list_append_der_t_tDA1C93A382058FB563F8772B119D5B598DC37A5C * get_unitytls_x509list_append_der_13() const { return ___unitytls_x509list_append_der_13; }
	inline unitytls_x509list_append_der_t_tDA1C93A382058FB563F8772B119D5B598DC37A5C ** get_address_of_unitytls_x509list_append_der_13() { return &___unitytls_x509list_append_der_13; }
	inline void set_unitytls_x509list_append_der_13(unitytls_x509list_append_der_t_tDA1C93A382058FB563F8772B119D5B598DC37A5C * value)
	{
		___unitytls_x509list_append_der_13 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_x509list_append_der_13), value);
	}

	inline static int32_t get_offset_of_unitytls_x509list_append_pem_14() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_x509list_append_pem_14)); }
	inline unitytls_x509list_append_der_t_tDA1C93A382058FB563F8772B119D5B598DC37A5C * get_unitytls_x509list_append_pem_14() const { return ___unitytls_x509list_append_pem_14; }
	inline unitytls_x509list_append_der_t_tDA1C93A382058FB563F8772B119D5B598DC37A5C ** get_address_of_unitytls_x509list_append_pem_14() { return &___unitytls_x509list_append_pem_14; }
	inline void set_unitytls_x509list_append_pem_14(unitytls_x509list_append_der_t_tDA1C93A382058FB563F8772B119D5B598DC37A5C * value)
	{
		___unitytls_x509list_append_pem_14 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_x509list_append_pem_14), value);
	}

	inline static int32_t get_offset_of_unitytls_x509list_free_15() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_x509list_free_15)); }
	inline unitytls_x509list_free_t_tE3FC523507F07BD9901D84E9F6968CD8A583FF09 * get_unitytls_x509list_free_15() const { return ___unitytls_x509list_free_15; }
	inline unitytls_x509list_free_t_tE3FC523507F07BD9901D84E9F6968CD8A583FF09 ** get_address_of_unitytls_x509list_free_15() { return &___unitytls_x509list_free_15; }
	inline void set_unitytls_x509list_free_15(unitytls_x509list_free_t_tE3FC523507F07BD9901D84E9F6968CD8A583FF09 * value)
	{
		___unitytls_x509list_free_15 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_x509list_free_15), value);
	}

	inline static int32_t get_offset_of_unitytls_x509verify_default_ca_16() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_x509verify_default_ca_16)); }
	inline unitytls_x509verify_default_ca_t_t4BACB6B49AA85C025AF9B18B3F30F63C9881DE2D * get_unitytls_x509verify_default_ca_16() const { return ___unitytls_x509verify_default_ca_16; }
	inline unitytls_x509verify_default_ca_t_t4BACB6B49AA85C025AF9B18B3F30F63C9881DE2D ** get_address_of_unitytls_x509verify_default_ca_16() { return &___unitytls_x509verify_default_ca_16; }
	inline void set_unitytls_x509verify_default_ca_16(unitytls_x509verify_default_ca_t_t4BACB6B49AA85C025AF9B18B3F30F63C9881DE2D * value)
	{
		___unitytls_x509verify_default_ca_16 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_x509verify_default_ca_16), value);
	}

	inline static int32_t get_offset_of_unitytls_x509verify_explicit_ca_17() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_x509verify_explicit_ca_17)); }
	inline unitytls_x509verify_explicit_ca_t_t6C8BE964C5EE9B24D3734F028EFCD83F5893D2E6 * get_unitytls_x509verify_explicit_ca_17() const { return ___unitytls_x509verify_explicit_ca_17; }
	inline unitytls_x509verify_explicit_ca_t_t6C8BE964C5EE9B24D3734F028EFCD83F5893D2E6 ** get_address_of_unitytls_x509verify_explicit_ca_17() { return &___unitytls_x509verify_explicit_ca_17; }
	inline void set_unitytls_x509verify_explicit_ca_17(unitytls_x509verify_explicit_ca_t_t6C8BE964C5EE9B24D3734F028EFCD83F5893D2E6 * value)
	{
		___unitytls_x509verify_explicit_ca_17 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_x509verify_explicit_ca_17), value);
	}

	inline static int32_t get_offset_of_unitytls_tlsctx_create_server_18() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_tlsctx_create_server_18)); }
	inline unitytls_tlsctx_create_server_t_t6E7812D40DDD91958E3CFBB92B5F5748D477E19D * get_unitytls_tlsctx_create_server_18() const { return ___unitytls_tlsctx_create_server_18; }
	inline unitytls_tlsctx_create_server_t_t6E7812D40DDD91958E3CFBB92B5F5748D477E19D ** get_address_of_unitytls_tlsctx_create_server_18() { return &___unitytls_tlsctx_create_server_18; }
	inline void set_unitytls_tlsctx_create_server_18(unitytls_tlsctx_create_server_t_t6E7812D40DDD91958E3CFBB92B5F5748D477E19D * value)
	{
		___unitytls_tlsctx_create_server_18 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_tlsctx_create_server_18), value);
	}

	inline static int32_t get_offset_of_unitytls_tlsctx_create_client_19() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_tlsctx_create_client_19)); }
	inline unitytls_tlsctx_create_client_t_tD9DFBDB5559983F0E11A67FA92E0F7182114C8F2 * get_unitytls_tlsctx_create_client_19() const { return ___unitytls_tlsctx_create_client_19; }
	inline unitytls_tlsctx_create_client_t_tD9DFBDB5559983F0E11A67FA92E0F7182114C8F2 ** get_address_of_unitytls_tlsctx_create_client_19() { return &___unitytls_tlsctx_create_client_19; }
	inline void set_unitytls_tlsctx_create_client_19(unitytls_tlsctx_create_client_t_tD9DFBDB5559983F0E11A67FA92E0F7182114C8F2 * value)
	{
		___unitytls_tlsctx_create_client_19 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_tlsctx_create_client_19), value);
	}

	inline static int32_t get_offset_of_unitytls_tlsctx_server_require_client_authentication_20() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_tlsctx_server_require_client_authentication_20)); }
	inline unitytls_tlsctx_server_require_client_authentication_t_t77B3CAFF25690A45405E3C957E40CC4FF83B49C6 * get_unitytls_tlsctx_server_require_client_authentication_20() const { return ___unitytls_tlsctx_server_require_client_authentication_20; }
	inline unitytls_tlsctx_server_require_client_authentication_t_t77B3CAFF25690A45405E3C957E40CC4FF83B49C6 ** get_address_of_unitytls_tlsctx_server_require_client_authentication_20() { return &___unitytls_tlsctx_server_require_client_authentication_20; }
	inline void set_unitytls_tlsctx_server_require_client_authentication_20(unitytls_tlsctx_server_require_client_authentication_t_t77B3CAFF25690A45405E3C957E40CC4FF83B49C6 * value)
	{
		___unitytls_tlsctx_server_require_client_authentication_20 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_tlsctx_server_require_client_authentication_20), value);
	}

	inline static int32_t get_offset_of_unitytls_tlsctx_set_certificate_callback_21() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_tlsctx_set_certificate_callback_21)); }
	inline unitytls_tlsctx_set_certificate_callback_t_tC4864FE0F6A3398A572F2511AA64C72126640937 * get_unitytls_tlsctx_set_certificate_callback_21() const { return ___unitytls_tlsctx_set_certificate_callback_21; }
	inline unitytls_tlsctx_set_certificate_callback_t_tC4864FE0F6A3398A572F2511AA64C72126640937 ** get_address_of_unitytls_tlsctx_set_certificate_callback_21() { return &___unitytls_tlsctx_set_certificate_callback_21; }
	inline void set_unitytls_tlsctx_set_certificate_callback_21(unitytls_tlsctx_set_certificate_callback_t_tC4864FE0F6A3398A572F2511AA64C72126640937 * value)
	{
		___unitytls_tlsctx_set_certificate_callback_21 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_tlsctx_set_certificate_callback_21), value);
	}

	inline static int32_t get_offset_of_unitytls_tlsctx_set_trace_callback_22() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_tlsctx_set_trace_callback_22)); }
	inline unitytls_tlsctx_set_trace_callback_t_tA11F424F68D297B6FD2B2EA26C6764F80146662A * get_unitytls_tlsctx_set_trace_callback_22() const { return ___unitytls_tlsctx_set_trace_callback_22; }
	inline unitytls_tlsctx_set_trace_callback_t_tA11F424F68D297B6FD2B2EA26C6764F80146662A ** get_address_of_unitytls_tlsctx_set_trace_callback_22() { return &___unitytls_tlsctx_set_trace_callback_22; }
	inline void set_unitytls_tlsctx_set_trace_callback_22(unitytls_tlsctx_set_trace_callback_t_tA11F424F68D297B6FD2B2EA26C6764F80146662A * value)
	{
		___unitytls_tlsctx_set_trace_callback_22 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_tlsctx_set_trace_callback_22), value);
	}

	inline static int32_t get_offset_of_unitytls_tlsctx_set_x509verify_callback_23() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_tlsctx_set_x509verify_callback_23)); }
	inline unitytls_tlsctx_set_x509verify_callback_t_t34EEB7BA38CA2C86F847416785ADB22BC4A04F4B * get_unitytls_tlsctx_set_x509verify_callback_23() const { return ___unitytls_tlsctx_set_x509verify_callback_23; }
	inline unitytls_tlsctx_set_x509verify_callback_t_t34EEB7BA38CA2C86F847416785ADB22BC4A04F4B ** get_address_of_unitytls_tlsctx_set_x509verify_callback_23() { return &___unitytls_tlsctx_set_x509verify_callback_23; }
	inline void set_unitytls_tlsctx_set_x509verify_callback_23(unitytls_tlsctx_set_x509verify_callback_t_t34EEB7BA38CA2C86F847416785ADB22BC4A04F4B * value)
	{
		___unitytls_tlsctx_set_x509verify_callback_23 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_tlsctx_set_x509verify_callback_23), value);
	}

	inline static int32_t get_offset_of_unitytls_tlsctx_set_supported_ciphersuites_24() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_tlsctx_set_supported_ciphersuites_24)); }
	inline unitytls_tlsctx_set_supported_ciphersuites_t_t6914054EA0F7A59C8A4ED4B9CDD5AF143F7D8BFE * get_unitytls_tlsctx_set_supported_ciphersuites_24() const { return ___unitytls_tlsctx_set_supported_ciphersuites_24; }
	inline unitytls_tlsctx_set_supported_ciphersuites_t_t6914054EA0F7A59C8A4ED4B9CDD5AF143F7D8BFE ** get_address_of_unitytls_tlsctx_set_supported_ciphersuites_24() { return &___unitytls_tlsctx_set_supported_ciphersuites_24; }
	inline void set_unitytls_tlsctx_set_supported_ciphersuites_24(unitytls_tlsctx_set_supported_ciphersuites_t_t6914054EA0F7A59C8A4ED4B9CDD5AF143F7D8BFE * value)
	{
		___unitytls_tlsctx_set_supported_ciphersuites_24 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_tlsctx_set_supported_ciphersuites_24), value);
	}

	inline static int32_t get_offset_of_unitytls_tlsctx_get_ciphersuite_25() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_tlsctx_get_ciphersuite_25)); }
	inline unitytls_tlsctx_get_ciphersuite_t_t94A91CB42A2EBB2CC598EF3E278770AFD80696A0 * get_unitytls_tlsctx_get_ciphersuite_25() const { return ___unitytls_tlsctx_get_ciphersuite_25; }
	inline unitytls_tlsctx_get_ciphersuite_t_t94A91CB42A2EBB2CC598EF3E278770AFD80696A0 ** get_address_of_unitytls_tlsctx_get_ciphersuite_25() { return &___unitytls_tlsctx_get_ciphersuite_25; }
	inline void set_unitytls_tlsctx_get_ciphersuite_25(unitytls_tlsctx_get_ciphersuite_t_t94A91CB42A2EBB2CC598EF3E278770AFD80696A0 * value)
	{
		___unitytls_tlsctx_get_ciphersuite_25 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_tlsctx_get_ciphersuite_25), value);
	}

	inline static int32_t get_offset_of_unitytls_tlsctx_get_protocol_26() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_tlsctx_get_protocol_26)); }
	inline unitytls_tlsctx_get_protocol_t_tB29092875D3CBD25E4461BFD165B5373FA54DB14 * get_unitytls_tlsctx_get_protocol_26() const { return ___unitytls_tlsctx_get_protocol_26; }
	inline unitytls_tlsctx_get_protocol_t_tB29092875D3CBD25E4461BFD165B5373FA54DB14 ** get_address_of_unitytls_tlsctx_get_protocol_26() { return &___unitytls_tlsctx_get_protocol_26; }
	inline void set_unitytls_tlsctx_get_protocol_26(unitytls_tlsctx_get_protocol_t_tB29092875D3CBD25E4461BFD165B5373FA54DB14 * value)
	{
		___unitytls_tlsctx_get_protocol_26 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_tlsctx_get_protocol_26), value);
	}

	inline static int32_t get_offset_of_unitytls_tlsctx_process_handshake_27() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_tlsctx_process_handshake_27)); }
	inline unitytls_tlsctx_process_handshake_t_tC8AAF317CBE4CA216F22BF031ECF89315B963C9D * get_unitytls_tlsctx_process_handshake_27() const { return ___unitytls_tlsctx_process_handshake_27; }
	inline unitytls_tlsctx_process_handshake_t_tC8AAF317CBE4CA216F22BF031ECF89315B963C9D ** get_address_of_unitytls_tlsctx_process_handshake_27() { return &___unitytls_tlsctx_process_handshake_27; }
	inline void set_unitytls_tlsctx_process_handshake_27(unitytls_tlsctx_process_handshake_t_tC8AAF317CBE4CA216F22BF031ECF89315B963C9D * value)
	{
		___unitytls_tlsctx_process_handshake_27 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_tlsctx_process_handshake_27), value);
	}

	inline static int32_t get_offset_of_unitytls_tlsctx_read_28() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_tlsctx_read_28)); }
	inline unitytls_tlsctx_read_t_tA8D1209D5F488E02F826EE2362F5AA61C8FF2EE2 * get_unitytls_tlsctx_read_28() const { return ___unitytls_tlsctx_read_28; }
	inline unitytls_tlsctx_read_t_tA8D1209D5F488E02F826EE2362F5AA61C8FF2EE2 ** get_address_of_unitytls_tlsctx_read_28() { return &___unitytls_tlsctx_read_28; }
	inline void set_unitytls_tlsctx_read_28(unitytls_tlsctx_read_t_tA8D1209D5F488E02F826EE2362F5AA61C8FF2EE2 * value)
	{
		___unitytls_tlsctx_read_28 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_tlsctx_read_28), value);
	}

	inline static int32_t get_offset_of_unitytls_tlsctx_write_29() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_tlsctx_write_29)); }
	inline unitytls_tlsctx_write_t_t0B4A49BBA592FE4EC0630B490463AE116AF07C9C * get_unitytls_tlsctx_write_29() const { return ___unitytls_tlsctx_write_29; }
	inline unitytls_tlsctx_write_t_t0B4A49BBA592FE4EC0630B490463AE116AF07C9C ** get_address_of_unitytls_tlsctx_write_29() { return &___unitytls_tlsctx_write_29; }
	inline void set_unitytls_tlsctx_write_29(unitytls_tlsctx_write_t_t0B4A49BBA592FE4EC0630B490463AE116AF07C9C * value)
	{
		___unitytls_tlsctx_write_29 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_tlsctx_write_29), value);
	}

	inline static int32_t get_offset_of_unitytls_tlsctx_notify_close_30() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_tlsctx_notify_close_30)); }
	inline unitytls_tlsctx_notify_close_t_t2FC4C08BACF1AEA509ABCAF3B22475E196E74A0D * get_unitytls_tlsctx_notify_close_30() const { return ___unitytls_tlsctx_notify_close_30; }
	inline unitytls_tlsctx_notify_close_t_t2FC4C08BACF1AEA509ABCAF3B22475E196E74A0D ** get_address_of_unitytls_tlsctx_notify_close_30() { return &___unitytls_tlsctx_notify_close_30; }
	inline void set_unitytls_tlsctx_notify_close_30(unitytls_tlsctx_notify_close_t_t2FC4C08BACF1AEA509ABCAF3B22475E196E74A0D * value)
	{
		___unitytls_tlsctx_notify_close_30 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_tlsctx_notify_close_30), value);
	}

	inline static int32_t get_offset_of_unitytls_tlsctx_free_31() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_tlsctx_free_31)); }
	inline unitytls_tlsctx_free_t_tB27A3B6F9D1B784ABE082849EAB6B81F51FAC8E2 * get_unitytls_tlsctx_free_31() const { return ___unitytls_tlsctx_free_31; }
	inline unitytls_tlsctx_free_t_tB27A3B6F9D1B784ABE082849EAB6B81F51FAC8E2 ** get_address_of_unitytls_tlsctx_free_31() { return &___unitytls_tlsctx_free_31; }
	inline void set_unitytls_tlsctx_free_31(unitytls_tlsctx_free_t_tB27A3B6F9D1B784ABE082849EAB6B81F51FAC8E2 * value)
	{
		___unitytls_tlsctx_free_31 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_tlsctx_free_31), value);
	}

	inline static int32_t get_offset_of_unitytls_random_generate_bytes_32() { return static_cast<int32_t>(offsetof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF, ___unitytls_random_generate_bytes_32)); }
	inline unitytls_random_generate_bytes_t_t494B8599A6D4247BB0C8AB7341DDC73BE42623F7 * get_unitytls_random_generate_bytes_32() const { return ___unitytls_random_generate_bytes_32; }
	inline unitytls_random_generate_bytes_t_t494B8599A6D4247BB0C8AB7341DDC73BE42623F7 ** get_address_of_unitytls_random_generate_bytes_32() { return &___unitytls_random_generate_bytes_32; }
	inline void set_unitytls_random_generate_bytes_32(unitytls_random_generate_bytes_t_t494B8599A6D4247BB0C8AB7341DDC73BE42623F7 * value)
	{
		___unitytls_random_generate_bytes_32 = value;
		Il2CppCodeGenWriteBarrier((&___unitytls_random_generate_bytes_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.Unity.UnityTls/unitytls_interface_struct
struct unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF_marshaled_pinvoke
{
	uint64_t ___UNITYTLS_INVALID_HANDLE_0;
	unitytls_tlsctx_protocolrange_t36243D72F83DAD47C95928314F58026DE8D38F47  ___UNITYTLS_TLSCTX_PROTOCOLRANGE_DEFAULT_1;
	Il2CppMethodPointer ___unitytls_errorstate_create_2;
	Il2CppMethodPointer ___unitytls_errorstate_raise_error_3;
	Il2CppMethodPointer ___unitytls_key_get_ref_4;
	Il2CppMethodPointer ___unitytls_key_parse_der_5;
	Il2CppMethodPointer ___unitytls_key_parse_pem_6;
	Il2CppMethodPointer ___unitytls_key_free_7;
	Il2CppMethodPointer ___unitytls_x509_export_der_8;
	Il2CppMethodPointer ___unitytls_x509list_get_ref_9;
	Il2CppMethodPointer ___unitytls_x509list_get_x509_10;
	Il2CppMethodPointer ___unitytls_x509list_create_11;
	Il2CppMethodPointer ___unitytls_x509list_append_12;
	Il2CppMethodPointer ___unitytls_x509list_append_der_13;
	Il2CppMethodPointer ___unitytls_x509list_append_pem_14;
	Il2CppMethodPointer ___unitytls_x509list_free_15;
	Il2CppMethodPointer ___unitytls_x509verify_default_ca_16;
	Il2CppMethodPointer ___unitytls_x509verify_explicit_ca_17;
	Il2CppMethodPointer ___unitytls_tlsctx_create_server_18;
	Il2CppMethodPointer ___unitytls_tlsctx_create_client_19;
	Il2CppMethodPointer ___unitytls_tlsctx_server_require_client_authentication_20;
	Il2CppMethodPointer ___unitytls_tlsctx_set_certificate_callback_21;
	Il2CppMethodPointer ___unitytls_tlsctx_set_trace_callback_22;
	Il2CppMethodPointer ___unitytls_tlsctx_set_x509verify_callback_23;
	Il2CppMethodPointer ___unitytls_tlsctx_set_supported_ciphersuites_24;
	Il2CppMethodPointer ___unitytls_tlsctx_get_ciphersuite_25;
	Il2CppMethodPointer ___unitytls_tlsctx_get_protocol_26;
	Il2CppMethodPointer ___unitytls_tlsctx_process_handshake_27;
	Il2CppMethodPointer ___unitytls_tlsctx_read_28;
	Il2CppMethodPointer ___unitytls_tlsctx_write_29;
	Il2CppMethodPointer ___unitytls_tlsctx_notify_close_30;
	Il2CppMethodPointer ___unitytls_tlsctx_free_31;
	Il2CppMethodPointer ___unitytls_random_generate_bytes_32;
};
// Native definition for COM marshalling of Mono.Unity.UnityTls/unitytls_interface_struct
struct unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF_marshaled_com
{
	uint64_t ___UNITYTLS_INVALID_HANDLE_0;
	unitytls_tlsctx_protocolrange_t36243D72F83DAD47C95928314F58026DE8D38F47  ___UNITYTLS_TLSCTX_PROTOCOLRANGE_DEFAULT_1;
	Il2CppMethodPointer ___unitytls_errorstate_create_2;
	Il2CppMethodPointer ___unitytls_errorstate_raise_error_3;
	Il2CppMethodPointer ___unitytls_key_get_ref_4;
	Il2CppMethodPointer ___unitytls_key_parse_der_5;
	Il2CppMethodPointer ___unitytls_key_parse_pem_6;
	Il2CppMethodPointer ___unitytls_key_free_7;
	Il2CppMethodPointer ___unitytls_x509_export_der_8;
	Il2CppMethodPointer ___unitytls_x509list_get_ref_9;
	Il2CppMethodPointer ___unitytls_x509list_get_x509_10;
	Il2CppMethodPointer ___unitytls_x509list_create_11;
	Il2CppMethodPointer ___unitytls_x509list_append_12;
	Il2CppMethodPointer ___unitytls_x509list_append_der_13;
	Il2CppMethodPointer ___unitytls_x509list_append_pem_14;
	Il2CppMethodPointer ___unitytls_x509list_free_15;
	Il2CppMethodPointer ___unitytls_x509verify_default_ca_16;
	Il2CppMethodPointer ___unitytls_x509verify_explicit_ca_17;
	Il2CppMethodPointer ___unitytls_tlsctx_create_server_18;
	Il2CppMethodPointer ___unitytls_tlsctx_create_client_19;
	Il2CppMethodPointer ___unitytls_tlsctx_server_require_client_authentication_20;
	Il2CppMethodPointer ___unitytls_tlsctx_set_certificate_callback_21;
	Il2CppMethodPointer ___unitytls_tlsctx_set_trace_callback_22;
	Il2CppMethodPointer ___unitytls_tlsctx_set_x509verify_callback_23;
	Il2CppMethodPointer ___unitytls_tlsctx_set_supported_ciphersuites_24;
	Il2CppMethodPointer ___unitytls_tlsctx_get_ciphersuite_25;
	Il2CppMethodPointer ___unitytls_tlsctx_get_protocol_26;
	Il2CppMethodPointer ___unitytls_tlsctx_process_handshake_27;
	Il2CppMethodPointer ___unitytls_tlsctx_read_28;
	Il2CppMethodPointer ___unitytls_tlsctx_write_29;
	Il2CppMethodPointer ___unitytls_tlsctx_notify_close_30;
	Il2CppMethodPointer ___unitytls_tlsctx_free_31;
	Il2CppMethodPointer ___unitytls_random_generate_bytes_32;
};
#endif // UNITYTLS_INTERFACE_STRUCT_T0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF_H
#ifndef UNITYTLS_ERRORSTATE_CREATE_T_T104BADBBE1265BD1A3F84C153EB7A67CDDBF35A9_H
#define UNITYTLS_ERRORSTATE_CREATE_T_T104BADBBE1265BD1A3F84C153EB7A67CDDBF35A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_errorstate_create_t
struct  unitytls_errorstate_create_t_t104BADBBE1265BD1A3F84C153EB7A67CDDBF35A9  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_ERRORSTATE_CREATE_T_T104BADBBE1265BD1A3F84C153EB7A67CDDBF35A9_H
#ifndef UNITYTLS_ERRORSTATE_RAISE_ERROR_T_TC441A37D4A6F1BAC1AFCA0108D4F7570EFF9E0D1_H
#define UNITYTLS_ERRORSTATE_RAISE_ERROR_T_TC441A37D4A6F1BAC1AFCA0108D4F7570EFF9E0D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_errorstate_raise_error_t
struct  unitytls_errorstate_raise_error_t_tC441A37D4A6F1BAC1AFCA0108D4F7570EFF9E0D1  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_ERRORSTATE_RAISE_ERROR_T_TC441A37D4A6F1BAC1AFCA0108D4F7570EFF9E0D1_H
#ifndef UNITYTLS_KEY_FREE_T_TCC7AD95D3B758BB99785645E65EDCD65A1D243AB_H
#define UNITYTLS_KEY_FREE_T_TCC7AD95D3B758BB99785645E65EDCD65A1D243AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_free_t
struct  unitytls_key_free_t_tCC7AD95D3B758BB99785645E65EDCD65A1D243AB  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_KEY_FREE_T_TCC7AD95D3B758BB99785645E65EDCD65A1D243AB_H
#ifndef UNITYTLS_KEY_GET_REF_T_T2F4EF4CD2F6AFC4F2D166953E834C6F0A13382A7_H
#define UNITYTLS_KEY_GET_REF_T_T2F4EF4CD2F6AFC4F2D166953E834C6F0A13382A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_get_ref_t
struct  unitytls_key_get_ref_t_t2F4EF4CD2F6AFC4F2D166953E834C6F0A13382A7  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_KEY_GET_REF_T_T2F4EF4CD2F6AFC4F2D166953E834C6F0A13382A7_H
#ifndef UNITYTLS_KEY_PARSE_DER_T_T2ABD1C146C8B9405F6E5A78CD59A779EA607741B_H
#define UNITYTLS_KEY_PARSE_DER_T_T2ABD1C146C8B9405F6E5A78CD59A779EA607741B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_parse_der_t
struct  unitytls_key_parse_der_t_t2ABD1C146C8B9405F6E5A78CD59A779EA607741B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_KEY_PARSE_DER_T_T2ABD1C146C8B9405F6E5A78CD59A779EA607741B_H
#ifndef UNITYTLS_KEY_PARSE_PEM_T_TB4BCEBA4194442C8C85FA19E80B808D0EDA462AB_H
#define UNITYTLS_KEY_PARSE_PEM_T_TB4BCEBA4194442C8C85FA19E80B808D0EDA462AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_parse_pem_t
struct  unitytls_key_parse_pem_t_tB4BCEBA4194442C8C85FA19E80B808D0EDA462AB  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_KEY_PARSE_PEM_T_TB4BCEBA4194442C8C85FA19E80B808D0EDA462AB_H
#ifndef UNITYTLS_X509_EXPORT_DER_T_TB0D0A02DE7E75757AFCA780298BF95467BF82856_H
#define UNITYTLS_X509_EXPORT_DER_T_TB0D0A02DE7E75757AFCA780298BF95467BF82856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509_export_der_t
struct  unitytls_x509_export_der_t_tB0D0A02DE7E75757AFCA780298BF95467BF82856  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_X509_EXPORT_DER_T_TB0D0A02DE7E75757AFCA780298BF95467BF82856_H
#ifndef UNITYTLS_X509LIST_APPEND_DER_T_TDA1C93A382058FB563F8772B119D5B598DC37A5C_H
#define UNITYTLS_X509LIST_APPEND_DER_T_TDA1C93A382058FB563F8772B119D5B598DC37A5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_append_der_t
struct  unitytls_x509list_append_der_t_tDA1C93A382058FB563F8772B119D5B598DC37A5C  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_X509LIST_APPEND_DER_T_TDA1C93A382058FB563F8772B119D5B598DC37A5C_H
#ifndef UNITYTLS_X509LIST_APPEND_T_TAB1C185C77DFD6BD96DF7909370AA1FAD6BB90AF_H
#define UNITYTLS_X509LIST_APPEND_T_TAB1C185C77DFD6BD96DF7909370AA1FAD6BB90AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_append_t
struct  unitytls_x509list_append_t_tAB1C185C77DFD6BD96DF7909370AA1FAD6BB90AF  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_X509LIST_APPEND_T_TAB1C185C77DFD6BD96DF7909370AA1FAD6BB90AF_H
#ifndef UNITYTLS_X509LIST_CREATE_T_TC040C2CF47D5426B7F6B1D6A2751507DC681CFF3_H
#define UNITYTLS_X509LIST_CREATE_T_TC040C2CF47D5426B7F6B1D6A2751507DC681CFF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_create_t
struct  unitytls_x509list_create_t_tC040C2CF47D5426B7F6B1D6A2751507DC681CFF3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_X509LIST_CREATE_T_TC040C2CF47D5426B7F6B1D6A2751507DC681CFF3_H
#ifndef UNITYTLS_X509LIST_FREE_T_TE3FC523507F07BD9901D84E9F6968CD8A583FF09_H
#define UNITYTLS_X509LIST_FREE_T_TE3FC523507F07BD9901D84E9F6968CD8A583FF09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_free_t
struct  unitytls_x509list_free_t_tE3FC523507F07BD9901D84E9F6968CD8A583FF09  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_X509LIST_FREE_T_TE3FC523507F07BD9901D84E9F6968CD8A583FF09_H
#ifndef UNITYTLS_X509LIST_GET_REF_T_T1FAB0CD82E536E0C9EB5255B145FC5AF434B3986_H
#define UNITYTLS_X509LIST_GET_REF_T_T1FAB0CD82E536E0C9EB5255B145FC5AF434B3986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_get_ref_t
struct  unitytls_x509list_get_ref_t_t1FAB0CD82E536E0C9EB5255B145FC5AF434B3986  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_X509LIST_GET_REF_T_T1FAB0CD82E536E0C9EB5255B145FC5AF434B3986_H
#ifndef UNITYTLS_X509LIST_GET_X509_T_T028BB06EEB95E8F62511F2301B90D8181F4FFDB5_H
#define UNITYTLS_X509LIST_GET_X509_T_T028BB06EEB95E8F62511F2301B90D8181F4FFDB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_get_x509_t
struct  unitytls_x509list_get_x509_t_t028BB06EEB95E8F62511F2301B90D8181F4FFDB5  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_X509LIST_GET_X509_T_T028BB06EEB95E8F62511F2301B90D8181F4FFDB5_H
#ifndef UNITYTLS_X509VERIFY_DEFAULT_CA_T_T4BACB6B49AA85C025AF9B18B3F30F63C9881DE2D_H
#define UNITYTLS_X509VERIFY_DEFAULT_CA_T_T4BACB6B49AA85C025AF9B18B3F30F63C9881DE2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509verify_default_ca_t
struct  unitytls_x509verify_default_ca_t_t4BACB6B49AA85C025AF9B18B3F30F63C9881DE2D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_X509VERIFY_DEFAULT_CA_T_T4BACB6B49AA85C025AF9B18B3F30F63C9881DE2D_H
#ifndef UNITYTLS_X509VERIFY_EXPLICIT_CA_T_T6C8BE964C5EE9B24D3734F028EFCD83F5893D2E6_H
#define UNITYTLS_X509VERIFY_EXPLICIT_CA_T_T6C8BE964C5EE9B24D3734F028EFCD83F5893D2E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509verify_explicit_ca_t
struct  unitytls_x509verify_explicit_ca_t_t6C8BE964C5EE9B24D3734F028EFCD83F5893D2E6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_X509VERIFY_EXPLICIT_CA_T_T6C8BE964C5EE9B24D3734F028EFCD83F5893D2E6_H
#ifndef UNITYTLS_TLSCTX_CERTIFICATE_CALLBACK_T55149A988CA1CE32772ACAC0031DAF4DC0F6D740_H
#define UNITYTLS_TLSCTX_CERTIFICATE_CALLBACK_T55149A988CA1CE32772ACAC0031DAF4DC0F6D740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_tlsctx_certificate_callback
struct  unitytls_tlsctx_certificate_callback_t55149A988CA1CE32772ACAC0031DAF4DC0F6D740  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_TLSCTX_CERTIFICATE_CALLBACK_T55149A988CA1CE32772ACAC0031DAF4DC0F6D740_H
#ifndef UNITYTLS_TLSCTX_READ_CALLBACK_TD85E7923018681355C1D851137CEC527F04093F5_H
#define UNITYTLS_TLSCTX_READ_CALLBACK_TD85E7923018681355C1D851137CEC527F04093F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_tlsctx_read_callback
struct  unitytls_tlsctx_read_callback_tD85E7923018681355C1D851137CEC527F04093F5  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_TLSCTX_READ_CALLBACK_TD85E7923018681355C1D851137CEC527F04093F5_H
#ifndef UNITYTLS_TLSCTX_TRACE_CALLBACK_T2C8F0895EF17ECAC042835D68A6BFDB9CBC7F2AA_H
#define UNITYTLS_TLSCTX_TRACE_CALLBACK_T2C8F0895EF17ECAC042835D68A6BFDB9CBC7F2AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_tlsctx_trace_callback
struct  unitytls_tlsctx_trace_callback_t2C8F0895EF17ECAC042835D68A6BFDB9CBC7F2AA  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_TLSCTX_TRACE_CALLBACK_T2C8F0895EF17ECAC042835D68A6BFDB9CBC7F2AA_H
#ifndef UNITYTLS_TLSCTX_WRITE_CALLBACK_TBDF40F27E011F577C3E834B14788491861F870D6_H
#define UNITYTLS_TLSCTX_WRITE_CALLBACK_TBDF40F27E011F577C3E834B14788491861F870D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_tlsctx_write_callback
struct  unitytls_tlsctx_write_callback_tBDF40F27E011F577C3E834B14788491861F870D6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_TLSCTX_WRITE_CALLBACK_TBDF40F27E011F577C3E834B14788491861F870D6_H
#ifndef UNITYTLS_TLSCTX_X509VERIFY_CALLBACK_T5FCF0307C4AB263BC611FE396EC4D2B9CF93528A_H
#define UNITYTLS_TLSCTX_X509VERIFY_CALLBACK_T5FCF0307C4AB263BC611FE396EC4D2B9CF93528A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_tlsctx_x509verify_callback
struct  unitytls_tlsctx_x509verify_callback_t5FCF0307C4AB263BC611FE396EC4D2B9CF93528A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_TLSCTX_X509VERIFY_CALLBACK_T5FCF0307C4AB263BC611FE396EC4D2B9CF93528A_H
#ifndef UNITYTLS_X509VERIFY_CALLBACK_T90C02C529DB2B9F434C18797BACC456BCB5400A9_H
#define UNITYTLS_X509VERIFY_CALLBACK_T90C02C529DB2B9F434C18797BACC456BCB5400A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unity.UnityTls/unitytls_x509verify_callback
struct  unitytls_x509verify_callback_t90C02C529DB2B9F434C18797BACC456BCB5400A9  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTLS_X509VERIFY_CALLBACK_T90C02C529DB2B9F434C18797BACC456BCB5400A9_H
#ifndef XMLANYCONVERTER_T4D431617E0BF65D3436F651CFC9024F17ACEB8D6_H
#define XMLANYCONVERTER_T4D431617E0BF65D3436F651CFC9024F17ACEB8D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlAnyConverter
struct  XmlAnyConverter_t4D431617E0BF65D3436F651CFC9024F17ACEB8D6  : public XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8
{
public:

public:
};

struct XmlAnyConverter_t4D431617E0BF65D3436F651CFC9024F17ACEB8D6_StaticFields
{
public:
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.XmlAnyConverter::Item
	XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * ___Item_32;
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.XmlAnyConverter::AnyAtomic
	XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * ___AnyAtomic_33;

public:
	inline static int32_t get_offset_of_Item_32() { return static_cast<int32_t>(offsetof(XmlAnyConverter_t4D431617E0BF65D3436F651CFC9024F17ACEB8D6_StaticFields, ___Item_32)); }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * get_Item_32() const { return ___Item_32; }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E ** get_address_of_Item_32() { return &___Item_32; }
	inline void set_Item_32(XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * value)
	{
		___Item_32 = value;
		Il2CppCodeGenWriteBarrier((&___Item_32), value);
	}

	inline static int32_t get_offset_of_AnyAtomic_33() { return static_cast<int32_t>(offsetof(XmlAnyConverter_t4D431617E0BF65D3436F651CFC9024F17ACEB8D6_StaticFields, ___AnyAtomic_33)); }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * get_AnyAtomic_33() const { return ___AnyAtomic_33; }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E ** get_address_of_AnyAtomic_33() { return &___AnyAtomic_33; }
	inline void set_AnyAtomic_33(XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * value)
	{
		___AnyAtomic_33 = value;
		Il2CppCodeGenWriteBarrier((&___AnyAtomic_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLANYCONVERTER_T4D431617E0BF65D3436F651CFC9024F17ACEB8D6_H
#ifndef XMLBOOLEANCONVERTER_T7CF33BCC1DB4F476BD12DEC2A024D9576250E395_H
#define XMLBOOLEANCONVERTER_T7CF33BCC1DB4F476BD12DEC2A024D9576250E395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlBooleanConverter
struct  XmlBooleanConverter_t7CF33BCC1DB4F476BD12DEC2A024D9576250E395  : public XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLBOOLEANCONVERTER_T7CF33BCC1DB4F476BD12DEC2A024D9576250E395_H
#ifndef XMLDATETIMECONVERTER_TEE6CF164478ED0ECBF4C48904585A05ED2F411FD_H
#define XMLDATETIMECONVERTER_TEE6CF164478ED0ECBF4C48904585A05ED2F411FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlDateTimeConverter
struct  XmlDateTimeConverter_tEE6CF164478ED0ECBF4C48904585A05ED2F411FD  : public XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDATETIMECONVERTER_TEE6CF164478ED0ECBF4C48904585A05ED2F411FD_H
#ifndef XMLLISTCONVERTER_TC0C9B12C2CB4282697C8A738419B3595FB013DC2_H
#define XMLLISTCONVERTER_TC0C9B12C2CB4282697C8A738419B3595FB013DC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlListConverter
struct  XmlListConverter_tC0C9B12C2CB4282697C8A738419B3595FB013DC2  : public XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8
{
public:
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.XmlListConverter::atomicConverter
	XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * ___atomicConverter_32;

public:
	inline static int32_t get_offset_of_atomicConverter_32() { return static_cast<int32_t>(offsetof(XmlListConverter_tC0C9B12C2CB4282697C8A738419B3595FB013DC2, ___atomicConverter_32)); }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * get_atomicConverter_32() const { return ___atomicConverter_32; }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E ** get_address_of_atomicConverter_32() { return &___atomicConverter_32; }
	inline void set_atomicConverter_32(XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * value)
	{
		___atomicConverter_32 = value;
		Il2CppCodeGenWriteBarrier((&___atomicConverter_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLLISTCONVERTER_TC0C9B12C2CB4282697C8A738419B3595FB013DC2_H
#ifndef XMLMISCCONVERTER_T1AE3CF83A199556955635974EA33D3CE45442BA1_H
#define XMLMISCCONVERTER_T1AE3CF83A199556955635974EA33D3CE45442BA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlMiscConverter
struct  XmlMiscConverter_t1AE3CF83A199556955635974EA33D3CE45442BA1  : public XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLMISCCONVERTER_T1AE3CF83A199556955635974EA33D3CE45442BA1_H
#ifndef XMLNUMERIC10CONVERTER_T3A3B412CBFC756A6226402095D3213113C01DD8C_H
#define XMLNUMERIC10CONVERTER_T3A3B412CBFC756A6226402095D3213113C01DD8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlNumeric10Converter
struct  XmlNumeric10Converter_t3A3B412CBFC756A6226402095D3213113C01DD8C  : public XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNUMERIC10CONVERTER_T3A3B412CBFC756A6226402095D3213113C01DD8C_H
#ifndef XMLNUMERIC2CONVERTER_T09483B096DF1E361A87EF40B03A2A5E945F03693_H
#define XMLNUMERIC2CONVERTER_T09483B096DF1E361A87EF40B03A2A5E945F03693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlNumeric2Converter
struct  XmlNumeric2Converter_t09483B096DF1E361A87EF40B03A2A5E945F03693  : public XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNUMERIC2CONVERTER_T09483B096DF1E361A87EF40B03A2A5E945F03693_H
#ifndef XMLSTRINGCONVERTER_TF53CC838C65F771EDC900AED7F1E34DEA77FDE12_H
#define XMLSTRINGCONVERTER_TF53CC838C65F771EDC900AED7F1E34DEA77FDE12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlStringConverter
struct  XmlStringConverter_tF53CC838C65F771EDC900AED7F1E34DEA77FDE12  : public XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSTRINGCONVERTER_TF53CC838C65F771EDC900AED7F1E34DEA77FDE12_H
#ifndef XMLUNIONCONVERTER_T31D47DFAB36753F8C83188973656457C9E55BE96_H
#define XMLUNIONCONVERTER_T31D47DFAB36753F8C83188973656457C9E55BE96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlUnionConverter
struct  XmlUnionConverter_t31D47DFAB36753F8C83188973656457C9E55BE96  : public XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8
{
public:
	// System.Xml.Schema.XmlValueConverter[] System.Xml.Schema.XmlUnionConverter::converters
	XmlValueConverterU5BU5D_t0C08DAE703E09DAE455F80F5975EB50F0D34CAB3* ___converters_32;
	// System.Boolean System.Xml.Schema.XmlUnionConverter::hasAtomicMember
	bool ___hasAtomicMember_33;
	// System.Boolean System.Xml.Schema.XmlUnionConverter::hasListMember
	bool ___hasListMember_34;

public:
	inline static int32_t get_offset_of_converters_32() { return static_cast<int32_t>(offsetof(XmlUnionConverter_t31D47DFAB36753F8C83188973656457C9E55BE96, ___converters_32)); }
	inline XmlValueConverterU5BU5D_t0C08DAE703E09DAE455F80F5975EB50F0D34CAB3* get_converters_32() const { return ___converters_32; }
	inline XmlValueConverterU5BU5D_t0C08DAE703E09DAE455F80F5975EB50F0D34CAB3** get_address_of_converters_32() { return &___converters_32; }
	inline void set_converters_32(XmlValueConverterU5BU5D_t0C08DAE703E09DAE455F80F5975EB50F0D34CAB3* value)
	{
		___converters_32 = value;
		Il2CppCodeGenWriteBarrier((&___converters_32), value);
	}

	inline static int32_t get_offset_of_hasAtomicMember_33() { return static_cast<int32_t>(offsetof(XmlUnionConverter_t31D47DFAB36753F8C83188973656457C9E55BE96, ___hasAtomicMember_33)); }
	inline bool get_hasAtomicMember_33() const { return ___hasAtomicMember_33; }
	inline bool* get_address_of_hasAtomicMember_33() { return &___hasAtomicMember_33; }
	inline void set_hasAtomicMember_33(bool value)
	{
		___hasAtomicMember_33 = value;
	}

	inline static int32_t get_offset_of_hasListMember_34() { return static_cast<int32_t>(offsetof(XmlUnionConverter_t31D47DFAB36753F8C83188973656457C9E55BE96, ___hasListMember_34)); }
	inline bool get_hasListMember_34() const { return ___hasListMember_34; }
	inline bool* get_address_of_hasListMember_34() { return &___hasListMember_34; }
	inline void set_hasListMember_34(bool value)
	{
		___hasListMember_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLUNIONCONVERTER_T31D47DFAB36753F8C83188973656457C9E55BE96_H
#ifndef XSDBUILDFUNCTION_TCE6DE66119181E60067FBE2AD08DE0D013B7F41E_H
#define XSDBUILDFUNCTION_TCE6DE66119181E60067FBE2AD08DE0D013B7F41E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdBuilder/XsdBuildFunction
struct  XsdBuildFunction_tCE6DE66119181E60067FBE2AD08DE0D013B7F41E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBUILDFUNCTION_TCE6DE66119181E60067FBE2AD08DE0D013B7F41E_H
#ifndef XSDENDCHILDFUNCTION_T7FDE0B391499702A4683AB55B900623C4F8DFB17_H
#define XSDENDCHILDFUNCTION_T7FDE0B391499702A4683AB55B900623C4F8DFB17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdBuilder/XsdEndChildFunction
struct  XsdEndChildFunction_t7FDE0B391499702A4683AB55B900623C4F8DFB17  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDENDCHILDFUNCTION_T7FDE0B391499702A4683AB55B900623C4F8DFB17_H
#ifndef XSDINITFUNCTION_T8B6E839BF5C55B9A918D919F005B971BFAEC5DF3_H
#define XSDINITFUNCTION_T8B6E839BF5C55B9A918D919F005B971BFAEC5DF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdBuilder/XsdInitFunction
struct  XsdInitFunction_t8B6E839BF5C55B9A918D919F005B971BFAEC5DF3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDINITFUNCTION_T8B6E839BF5C55B9A918D919F005B971BFAEC5DF3_H
#ifndef XMLANYLISTCONVERTER_T1150077B9E7B2D0AEDC3450117300B021E43BF4A_H
#define XMLANYLISTCONVERTER_T1150077B9E7B2D0AEDC3450117300B021E43BF4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlAnyListConverter
struct  XmlAnyListConverter_t1150077B9E7B2D0AEDC3450117300B021E43BF4A  : public XmlListConverter_tC0C9B12C2CB4282697C8A738419B3595FB013DC2
{
public:

public:
};

struct XmlAnyListConverter_t1150077B9E7B2D0AEDC3450117300B021E43BF4A_StaticFields
{
public:
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.XmlAnyListConverter::ItemList
	XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * ___ItemList_33;
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.XmlAnyListConverter::AnyAtomicList
	XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * ___AnyAtomicList_34;

public:
	inline static int32_t get_offset_of_ItemList_33() { return static_cast<int32_t>(offsetof(XmlAnyListConverter_t1150077B9E7B2D0AEDC3450117300B021E43BF4A_StaticFields, ___ItemList_33)); }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * get_ItemList_33() const { return ___ItemList_33; }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E ** get_address_of_ItemList_33() { return &___ItemList_33; }
	inline void set_ItemList_33(XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * value)
	{
		___ItemList_33 = value;
		Il2CppCodeGenWriteBarrier((&___ItemList_33), value);
	}

	inline static int32_t get_offset_of_AnyAtomicList_34() { return static_cast<int32_t>(offsetof(XmlAnyListConverter_t1150077B9E7B2D0AEDC3450117300B021E43BF4A_StaticFields, ___AnyAtomicList_34)); }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * get_AnyAtomicList_34() const { return ___AnyAtomicList_34; }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E ** get_address_of_AnyAtomicList_34() { return &___AnyAtomicList_34; }
	inline void set_AnyAtomicList_34(XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * value)
	{
		___AnyAtomicList_34 = value;
		Il2CppCodeGenWriteBarrier((&___AnyAtomicList_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLANYLISTCONVERTER_T1150077B9E7B2D0AEDC3450117300B021E43BF4A_H
#ifndef XMLUNTYPEDCONVERTER_T320F0E666810AFC92D49F4758E6B6A152B264D1E_H
#define XMLUNTYPEDCONVERTER_T320F0E666810AFC92D49F4758E6B6A152B264D1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlUntypedConverter
struct  XmlUntypedConverter_t320F0E666810AFC92D49F4758E6B6A152B264D1E  : public XmlListConverter_tC0C9B12C2CB4282697C8A738419B3595FB013DC2
{
public:
	// System.Boolean System.Xml.Schema.XmlUntypedConverter::allowListToList
	bool ___allowListToList_33;

public:
	inline static int32_t get_offset_of_allowListToList_33() { return static_cast<int32_t>(offsetof(XmlUntypedConverter_t320F0E666810AFC92D49F4758E6B6A152B264D1E, ___allowListToList_33)); }
	inline bool get_allowListToList_33() const { return ___allowListToList_33; }
	inline bool* get_address_of_allowListToList_33() { return &___allowListToList_33; }
	inline void set_allowListToList_33(bool value)
	{
		___allowListToList_33 = value;
	}
};

struct XmlUntypedConverter_t320F0E666810AFC92D49F4758E6B6A152B264D1E_StaticFields
{
public:
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.XmlUntypedConverter::Untyped
	XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * ___Untyped_34;
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.XmlUntypedConverter::UntypedList
	XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * ___UntypedList_35;

public:
	inline static int32_t get_offset_of_Untyped_34() { return static_cast<int32_t>(offsetof(XmlUntypedConverter_t320F0E666810AFC92D49F4758E6B6A152B264D1E_StaticFields, ___Untyped_34)); }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * get_Untyped_34() const { return ___Untyped_34; }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E ** get_address_of_Untyped_34() { return &___Untyped_34; }
	inline void set_Untyped_34(XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * value)
	{
		___Untyped_34 = value;
		Il2CppCodeGenWriteBarrier((&___Untyped_34), value);
	}

	inline static int32_t get_offset_of_UntypedList_35() { return static_cast<int32_t>(offsetof(XmlUntypedConverter_t320F0E666810AFC92D49F4758E6B6A152B264D1E_StaticFields, ___UntypedList_35)); }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * get_UntypedList_35() const { return ___UntypedList_35; }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E ** get_address_of_UntypedList_35() { return &___UntypedList_35; }
	inline void set_UntypedList_35(XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * value)
	{
		___UntypedList_35 = value;
		Il2CppCodeGenWriteBarrier((&___UntypedList_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLUNTYPEDCONVERTER_T320F0E666810AFC92D49F4758E6B6A152B264D1E_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[4] = 
{
	IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2::get_offset_of_Id_0(),
	IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2::get_offset_of_LineNo_1(),
	IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2::get_offset_of_LinePos_2(),
	IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2::get_offset_of_Next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C), -1, sizeof(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2201[46] = 
{
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_schemaSet_0(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_validationFlags_1(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_startIDConstraint_2(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_isRoot_3(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_rootHasSchema_4(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_attrValid_5(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_checkEntity_6(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_compiledSchemaInfo_7(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_dtdSchemaInfo_8(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_validatedNamespaces_9(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_validationStack_10(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_context_11(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_currentState_12(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_attPresence_13(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_wildID_14(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_IDs_15(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_idRefListHead_16(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_contextQName_17(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_NsXs_18(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_NsXsi_19(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_NsXmlNs_20(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_NsXml_21(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_partialValidationType_22(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_textValue_23(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_eventHandler_24(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_validationEventSender_25(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_nameTable_26(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_positionInfo_27(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_dummyPositionInfo_28(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_xmlResolver_29(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_sourceUri_30(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_sourceUriString_31(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_nsResolver_32(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_processContents_33(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_xsiTypeString_34(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_xsiNilString_35(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_xsiSchemaLocationString_36(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_xsiNoNamespaceSchemaLocationString_37(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields::get_offset_of_dtQName_38(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields::get_offset_of_dtCDATA_39(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields::get_offset_of_dtStringArray_40(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields::get_offset_of_EmptyParticleArray_41(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields::get_offset_of_EmptyAttributeArray_42(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C::get_offset_of_xmlCharType_43(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields::get_offset_of_ValidStates_44(),
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C_StaticFields::get_offset_of_MethodNames_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (XmlSchemaValidity_tFB9AEE7708C4FF08DC1564482F46991CF8FD0621)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2202[4] = 
{
	XmlSchemaValidity_tFB9AEE7708C4FF08DC1564482F46991CF8FD0621::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (XmlSeverityType_tC3AD578830B568AE8D5455F146A99305BEE0501A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2203[3] = 
{
	XmlSeverityType_tC3AD578830B568AE8D5455F146A99305BEE0501A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (XmlTypeCode_t9C4AD5729574C591993F2559021E198BED5252A2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2204[56] = 
{
	XmlTypeCode_t9C4AD5729574C591993F2559021E198BED5252A2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8), -1, sizeof(XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2206[32] = 
{
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8::get_offset_of_schemaType_0(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8::get_offset_of_typeCode_1(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8::get_offset_of_clrTypeDefault_2(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_ICollectionType_3(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_IEnumerableType_4(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_IListType_5(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_ObjectArrayType_6(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_StringArrayType_7(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_XmlAtomicValueArrayType_8(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_DecimalType_9(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_Int32Type_10(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_Int64Type_11(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_StringType_12(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_XmlAtomicValueType_13(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_ObjectType_14(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_ByteType_15(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_Int16Type_16(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_SByteType_17(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_UInt16Type_18(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_UInt32Type_19(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_UInt64Type_20(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_XPathItemType_21(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_DoubleType_22(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_SingleType_23(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_DateTimeType_24(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_DateTimeOffsetType_25(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_BooleanType_26(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_ByteArrayType_27(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_XmlQualifiedNameType_28(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_UriType_29(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_TimeSpanType_30(),
	XmlBaseConverter_t2E1FE7137987814D59918B27A1807DAE7E78CFD8_StaticFields::get_offset_of_XPathNavigatorType_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (XmlNumeric10Converter_t3A3B412CBFC756A6226402095D3213113C01DD8C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (XmlNumeric2Converter_t09483B096DF1E361A87EF40B03A2A5E945F03693), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (XmlDateTimeConverter_tEE6CF164478ED0ECBF4C48904585A05ED2F411FD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (XmlBooleanConverter_t7CF33BCC1DB4F476BD12DEC2A024D9576250E395), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (XmlMiscConverter_t1AE3CF83A199556955635974EA33D3CE45442BA1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (XmlStringConverter_tF53CC838C65F771EDC900AED7F1E34DEA77FDE12), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (XmlUntypedConverter_t320F0E666810AFC92D49F4758E6B6A152B264D1E), -1, sizeof(XmlUntypedConverter_t320F0E666810AFC92D49F4758E6B6A152B264D1E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2213[3] = 
{
	XmlUntypedConverter_t320F0E666810AFC92D49F4758E6B6A152B264D1E::get_offset_of_allowListToList_33(),
	XmlUntypedConverter_t320F0E666810AFC92D49F4758E6B6A152B264D1E_StaticFields::get_offset_of_Untyped_34(),
	XmlUntypedConverter_t320F0E666810AFC92D49F4758E6B6A152B264D1E_StaticFields::get_offset_of_UntypedList_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (XmlAnyConverter_t4D431617E0BF65D3436F651CFC9024F17ACEB8D6), -1, sizeof(XmlAnyConverter_t4D431617E0BF65D3436F651CFC9024F17ACEB8D6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2214[2] = 
{
	XmlAnyConverter_t4D431617E0BF65D3436F651CFC9024F17ACEB8D6_StaticFields::get_offset_of_Item_32(),
	XmlAnyConverter_t4D431617E0BF65D3436F651CFC9024F17ACEB8D6_StaticFields::get_offset_of_AnyAtomic_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (XmlAnyListConverter_t1150077B9E7B2D0AEDC3450117300B021E43BF4A), -1, sizeof(XmlAnyListConverter_t1150077B9E7B2D0AEDC3450117300B021E43BF4A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2215[2] = 
{
	XmlAnyListConverter_t1150077B9E7B2D0AEDC3450117300B021E43BF4A_StaticFields::get_offset_of_ItemList_33(),
	XmlAnyListConverter_t1150077B9E7B2D0AEDC3450117300B021E43BF4A_StaticFields::get_offset_of_AnyAtomicList_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (XmlListConverter_tC0C9B12C2CB4282697C8A738419B3595FB013DC2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[1] = 
{
	XmlListConverter_tC0C9B12C2CB4282697C8A738419B3595FB013DC2::get_offset_of_atomicConverter_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (XmlUnionConverter_t31D47DFAB36753F8C83188973656457C9E55BE96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[3] = 
{
	XmlUnionConverter_t31D47DFAB36753F8C83188973656457C9E55BE96::get_offset_of_converters_32(),
	XmlUnionConverter_t31D47DFAB36753F8C83188973656457C9E55BE96::get_offset_of_hasAtomicMember_33(),
	XmlUnionConverter_t31D47DFAB36753F8C83188973656457C9E55BE96::get_offset_of_hasListMember_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3), -1, sizeof(XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2218[111] = 
{
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SchemaElement_0(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SchemaSubelements_1(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_AttributeSubelements_2(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_ElementSubelements_3(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_ComplexTypeSubelements_4(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SimpleContentSubelements_5(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SimpleContentExtensionSubelements_6(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SimpleContentRestrictionSubelements_7(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_ComplexContentSubelements_8(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_ComplexContentExtensionSubelements_9(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_ComplexContentRestrictionSubelements_10(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SimpleTypeSubelements_11(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SimpleTypeRestrictionSubelements_12(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SimpleTypeListSubelements_13(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SimpleTypeUnionSubelements_14(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_RedefineSubelements_15(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_AttributeGroupSubelements_16(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_GroupSubelements_17(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_AllSubelements_18(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_ChoiceSequenceSubelements_19(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_IdentityConstraintSubelements_20(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_AnnotationSubelements_21(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_AnnotatedSubelements_22(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SchemaAttributes_23(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_AttributeAttributes_24(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_ElementAttributes_25(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_ComplexTypeAttributes_26(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SimpleContentAttributes_27(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SimpleContentExtensionAttributes_28(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SimpleContentRestrictionAttributes_29(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_ComplexContentAttributes_30(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_ComplexContentExtensionAttributes_31(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_ComplexContentRestrictionAttributes_32(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SimpleTypeAttributes_33(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SimpleTypeRestrictionAttributes_34(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SimpleTypeUnionAttributes_35(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SimpleTypeListAttributes_36(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_AttributeGroupAttributes_37(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_AttributeGroupRefAttributes_38(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_GroupAttributes_39(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_GroupRefAttributes_40(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_ParticleAttributes_41(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_AnyAttributes_42(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_IdentityConstraintAttributes_43(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SelectorAttributes_44(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_FieldAttributes_45(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_NotationAttributes_46(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_IncludeAttributes_47(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_ImportAttributes_48(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_FacetAttributes_49(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_AnyAttributeAttributes_50(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_DocumentationAttributes_51(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_AppinfoAttributes_52(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_RedefineAttributes_53(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_AnnotationAttributes_54(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_SchemaEntries_55(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_DerivationMethodValues_56(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_DerivationMethodStrings_57(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_FormStringValues_58(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_UseStringValues_59(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3_StaticFields::get_offset_of_ProcessContentsStringValues_60(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_reader_61(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_positionInfo_62(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_currentEntry_63(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_nextEntry_64(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_hasChild_65(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_stateHistory_66(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_containerStack_67(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_nameTable_68(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_schemaNames_69(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_namespaceManager_70(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_canIncludeImport_71(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_schema_72(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_xso_73(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_element_74(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_anyElement_75(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_attribute_76(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_anyAttribute_77(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_complexType_78(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_simpleType_79(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_complexContent_80(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_complexContentExtension_81(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_complexContentRestriction_82(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_simpleContent_83(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_simpleContentExtension_84(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_simpleContentRestriction_85(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_simpleTypeUnion_86(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_simpleTypeList_87(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_simpleTypeRestriction_88(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_group_89(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_groupRef_90(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_all_91(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_choice_92(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_sequence_93(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_particle_94(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_attributeGroup_95(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_attributeGroupRef_96(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_notation_97(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_identityConstraint_98(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_xpath_99(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_include_100(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_import_101(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_annotation_102(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_appInfo_103(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_documentation_104(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_facet_105(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_markup_106(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_redefine_107(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_validationEventHandler_108(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_unhandledAttributes_109(),
	XsdBuilder_tBA5B42B2125776A4A505D3B06C5EBB10F1D19FF3::get_offset_of_namespaces_110(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (State_tB6574821F5A18DCBE926226EFF424FD6FF924BE9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2219[49] = 
{
	State_tB6574821F5A18DCBE926226EFF424FD6FF924BE9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (XsdBuildFunction_tCE6DE66119181E60067FBE2AD08DE0D013B7F41E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (XsdInitFunction_t8B6E839BF5C55B9A918D919F005B971BFAEC5DF3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (XsdEndChildFunction_t7FDE0B391499702A4683AB55B900623C4F8DFB17), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (XsdAttributeEntry_tE7E6C3E5854B31AD709DCD4F9E66111050DF9664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[2] = 
{
	XsdAttributeEntry_tE7E6C3E5854B31AD709DCD4F9E66111050DF9664::get_offset_of_Attribute_0(),
	XsdAttributeEntry_tE7E6C3E5854B31AD709DCD4F9E66111050DF9664::get_offset_of_BuildFunc_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[7] = 
{
	XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3::get_offset_of_Name_0(),
	XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3::get_offset_of_CurrentState_1(),
	XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3::get_offset_of_NextStates_2(),
	XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3::get_offset_of_Attributes_3(),
	XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3::get_offset_of_InitFunc_4(),
	XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3::get_offset_of_EndChildFunc_5(),
	XsdEntry_t96072848EB8D77A59E66E17AB46097648D7692C3::get_offset_of_ParseContent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (BuilderNamespaceManager_t87105DBB7709F55B4371C1E746AE7CFDA950679E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[2] = 
{
	BuilderNamespaceManager_t87105DBB7709F55B4371C1E746AE7CFDA950679E::get_offset_of_nsMgr_8(),
	BuilderNamespaceManager_t87105DBB7709F55B4371C1E746AE7CFDA950679E::get_offset_of_reader_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (XsdDateTimeFlags_tC892A580DE494CDB02FF5D29F914ECDC49F858C8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2226[13] = 
{
	XsdDateTimeFlags_tC892A580DE494CDB02FF5D29F914ECDC49F858C8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5)+ sizeof (RuntimeObject), -1, sizeof(XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2227[25] = 
{
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5::get_offset_of_dt_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5::get_offset_of_extra_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lzyyyy_2(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lzyyyy__3(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lzyyyy_MM_4(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lzyyyy_MM__5(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lzyyyy_MM_dd_6(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lzyyyy_MM_ddT_7(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_LzHH_8(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_LzHH__9(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_LzHH_mm_10(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_LzHH_mm__11(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_LzHH_mm_ss_12(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lz__13(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lz_zz_14(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lz_zz__15(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lz_zz_zz_16(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lz___17(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lz__mm_18(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lz__mm__19(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lz__mm___20(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lz__mm_dd_21(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lz____22(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_Lz___dd_23(),
	XsdDateTime_tEA54A4A77DD9458E97F1306F2013714582663CC5_StaticFields::get_offset_of_typeCodes_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (DateTimeTypeCode_tAD1D7DFBB2EE73A81A0009853B3C79586C83820F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2228[10] = 
{
	DateTimeTypeCode_tAD1D7DFBB2EE73A81A0009853B3C79586C83820F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (XsdDateTimeKind_t1E716A8B5DF21C68A40457C07DFBF6F284D6A8BA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2229[5] = 
{
	XsdDateTimeKind_t1E716A8B5DF21C68A40457C07DFBF6F284D6A8BA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9)+ sizeof (RuntimeObject), sizeof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9_marshaled_pinvoke), sizeof(Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2230[14] = 
{
	Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9::get_offset_of_typeCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9::get_offset_of_year_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9::get_offset_of_month_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9::get_offset_of_day_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9::get_offset_of_hour_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9::get_offset_of_minute_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9::get_offset_of_second_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9::get_offset_of_fraction_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9::get_offset_of_kind_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9::get_offset_of_zoneHour_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9::get_offset_of_zoneMinute_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9::get_offset_of_text_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9::get_offset_of_length_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t402903C4103D1084228988A8DA76C1FCB8D890B9_StaticFields::get_offset_of_Power10_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5)+ sizeof (RuntimeObject), sizeof(XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2231[7] = 
{
	XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5::get_offset_of_years_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5::get_offset_of_months_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5::get_offset_of_days_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5::get_offset_of_hours_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5::get_offset_of_minutes_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5::get_offset_of_seconds_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XsdDuration_t10E257E1794C97860274A62B208A93B6DBBC7CE5::get_offset_of_nanoseconds_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (Parts_t167B7D7072AE6549A8D8024EF757C8196FAFAA55)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2232[8] = 
{
	Parts_t167B7D7072AE6549A8D8024EF757C8196FAFAA55::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (DurationType_t687DA1675447204D8E7F5E2B06C3AAD0A9ABC48A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2233[4] = 
{
	DurationType_t687DA1675447204D8E7F5E2B06C3AAD0A9ABC48A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A), -1, sizeof(XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2234[20] = 
{
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_startIDConstraint_15(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_validationStack_16(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_attPresence_17(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_nsManager_18(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_bManageNamespaces_19(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_IDs_20(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_idRefListHead_21(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_inlineSchemaParser_22(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_processContents_23(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A_StaticFields::get_offset_of_dtCDATA_24(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A_StaticFields::get_offset_of_dtQName_25(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A_StaticFields::get_offset_of_dtStringArray_26(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_NsXmlNs_27(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_NsXs_28(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_NsXsi_29(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_XsiType_30(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_XsiNil_31(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_XsiSchemaLocation_32(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_XsiNoNamespaceSchemaLocation_33(),
	XsdValidator_t3CE76515AAD3C18388E441A25640966E274B1D8A::get_offset_of_XsdSchema_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (XmlReaderSection_tFCE70D088C6038C6BB6BA8A6A66F97D91AD84AC7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[7] = 
{
	StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43::get_offset_of_s1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43::get_offset_of_s2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43::get_offset_of_s3_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43::get_offset_of_s4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43::get_offset_of_delimiter_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43::get_offset_of_strList_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43::get_offset_of_idxStr_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB), -1, sizeof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2237[39] = 
{
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U30701435C4E2C38EFE43C51BD22C114AB8B80124D_0(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U30EE6555EB2C89F29655BD23FAB0573D8D684331A_1(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U30F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_2(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U3161F91CE1721D8F16622810CBB39887D21C47031_3(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U32051D7520B96DCC12F2E4DE851CB9F203D623805_4(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U3221CE291CD044114B4369175B9B91177F5932876_5(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U32A4F1BD548EC71F652E24985361CD72F0FE1BE7D_6(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U3360487BE4278986419B568EFD887F6145383168A_7(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U342DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_8(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U3485F43E332C2F7530815B17C08DAC169A8F697E0_9(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U349C5BA13401986EC93E4677F52CBE2248184DFBD_10(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U351E4CA1C2B009A2876C6E57D8E69E3502BCA3440_11(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U3553E235E202D57C9F1156E7D232E02BBDC920165_12(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U3574B9D4E4C39F6E8004181E5765B627B75EB1AD1_13(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_14(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_15(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U368D0F86889D5D656483EEE829BCEECDFEC91D8EA_16(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U36A0D50D692745A6663128CD315B71079584F3E59_17(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U3702F6A3276CBE481D247A77C20B5459FB94D07D2_18(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U37A32E1A19C182315E4263A65A72066492550D8CD_19(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U38B4E5E81A88D29642679AFCE41DCA380F9000462_20(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U3977375E4E1ED54F588076ACA36CC17E6C2195CB9_21(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U399F0664C2AC8464B51252D92FC24F3834C6FB90C_22(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_23(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_U39E374D7263B2452E25DE3D6E617F6A728D98A439_24(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_A933F173482FF50754B4942AF8DFC584EF14A45B_25(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_26(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_B368804F0C6DAB083B253A6B106D0783D5C32E90_27(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_B9F0004E3873FDDCABFDA6174EA18F0859B637B4_28(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_BAD037B714E1CD1052149B51238A3D4351DD10B5_29(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_30(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_DB9F879BE80CC2DA24DEF633D23DBB2B1ADBCC81_31(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_DCF398750721AA7A27A6BA56E99350329B06E8B1_32(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_33(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_34(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_EE3413A2C088FF9432054D6E60A7CB6A498D25F0_35(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_F64F25EAE9A3D7A356813C4218000185541D7779_36(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_FB0C58D8B3094F018764CC6E3094B9576DB08069_37(),
	U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields::get_offset_of_FFE3F15642234E7FAD6951D432F1134D5AD15922_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (__StaticArrayInitTypeSizeU3D6_tB657E692303B443FF0E24AE8F75A675A844348C4)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D6_tB657E692303B443FF0E24AE8F75A675A844348C4 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (__StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_tF92030B0FB73CC4EB587C7FD3177E5229C5B8749 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (__StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D16_t1022F965C0CFEE56E88F01CFB36C134A0AB27CCD ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (__StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D20_t25C626683DA421874F122B40243142FF7832A1BF ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (__StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_tFAA45C2D269AF27FBFEF2E0076065EE57863E223 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (__StaticArrayInitTypeSizeU3D28_t5092DE69B1A24897AC8FBEFE716755EA1FE14C24)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D28_t5092DE69B1A24897AC8FBEFE716755EA1FE14C24 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (__StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_tFEC269E612F5A23CB7315224D57AF2DA72F821E7 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (__StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D36_t5217F3350ECED222B6441CF50B97CE496CFF3C15 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (__StaticArrayInitTypeSizeU3D38_t93A8ADCEAFC78CEEBA1473ADD1938275574C49E1)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D38_t93A8ADCEAFC78CEEBA1473ADD1938275574C49E1 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (__StaticArrayInitTypeSizeU3D40_t2C1C40E1ED6F4BAA5E535BCF7574AD6781A49AD4)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D40_t2C1C40E1ED6F4BAA5E535BCF7574AD6781A49AD4 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (__StaticArrayInitTypeSizeU3D44_t497B1CD0DA7F519C08603DFB716118F291F18E40)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D44_t497B1CD0DA7F519C08603DFB716118F291F18E40 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (__StaticArrayInitTypeSizeU3D56_tA1FD048C977FA4FB4F0B2359C884E3002DC36737)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D56_tA1FD048C977FA4FB4F0B2359C884E3002DC36737 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (__StaticArrayInitTypeSizeU3D64_tE9F2C6D5C2D961C95B98D342E25A70CE875C2387)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D64_tE9F2C6D5C2D961C95B98D342E25A70CE875C2387 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (__StaticArrayInitTypeSizeU3D68_t5DF8EE2FE382E89E2BE20E276315E7E5850B86A2)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D68_t5DF8EE2FE382E89E2BE20E276315E7E5850B86A2 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (__StaticArrayInitTypeSizeU3D112_tCA120B5FDB821F63EF42C3B78E8B2A74B77D117E)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D112_tCA120B5FDB821F63EF42C3B78E8B2A74B77D117E ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (__StaticArrayInitTypeSizeU3D144_t9C3F15E672A756D2D700C19E37BF64DBD63DD0F7)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D144_t9C3F15E672A756D2D700C19E37BF64DBD63DD0F7 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (__StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (__StaticArrayInitTypeSizeU3D664_t3741AEF321779B117CACFCF3A0CE45075DD15EBA)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D664_t3741AEF321779B117CACFCF3A0CE45075DD15EBA ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (__StaticArrayInitTypeSizeU3D960_t90BB40F76231B6054859C4B179BC097521A54C32)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D960_t90BB40F76231B6054859C4B179BC097521A54C32 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (__StaticArrayInitTypeSizeU3D1212_t528E4585D66D23345F608EEE97F869A0D1665EB8)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D1212_t528E4585D66D23345F608EEE97F869A0D1665EB8 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (U3CModuleU3E_tD81C26B45B0FBFD466203EB631D6270174412EE8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (SR_tF6C259D0ADF333DE679CC02B526A504888CDD8AC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (MonoPInvokeCallbackAttribute_t4D5E5DC1B6616AD7983047AC217DE7BD9400E0CD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (CertHelper_t603EE753116E3B83EC5DB1CAC369F51DF9DC5A60), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (Debug_t61CBAA915E7373BA24D59D69221A160BCE7EB406), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (UnityTls_tFD87351846C601F716262E015308C6BB66000DEC), -1, sizeof(UnityTls_tFD87351846C601F716262E015308C6BB66000DEC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2263[1] = 
{
	UnityTls_tFD87351846C601F716262E015308C6BB66000DEC_StaticFields::get_offset_of_marshalledInterface_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (unitytls_error_code_tA9272A71D51F7FE555C03185AE244FB01EAF998F)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2264[18] = 
{
	unitytls_error_code_tA9272A71D51F7FE555C03185AE244FB01EAF998F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (unitytls_errorstate_t64FA817A583B1CD3CB1AFCFF9606F1F2782ABBE6)+ sizeof (RuntimeObject), sizeof(unitytls_errorstate_t64FA817A583B1CD3CB1AFCFF9606F1F2782ABBE6 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2265[3] = 
{
	unitytls_errorstate_t64FA817A583B1CD3CB1AFCFF9606F1F2782ABBE6::get_offset_of_magic_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	unitytls_errorstate_t64FA817A583B1CD3CB1AFCFF9606F1F2782ABBE6::get_offset_of_code_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	unitytls_errorstate_t64FA817A583B1CD3CB1AFCFF9606F1F2782ABBE6::get_offset_of_reserved_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (unitytls_key_tABB3A1A923B658DC99FFDB3ED633FBBA64EF1BB9)+ sizeof (RuntimeObject), sizeof(unitytls_key_tABB3A1A923B658DC99FFDB3ED633FBBA64EF1BB9 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (unitytls_key_ref_tE908606656A7C49CA1EB734722E4C3DED7CE6E5B)+ sizeof (RuntimeObject), sizeof(unitytls_key_ref_tE908606656A7C49CA1EB734722E4C3DED7CE6E5B ), 0, 0 };
extern const int32_t g_FieldOffsetTable2267[1] = 
{
	unitytls_key_ref_tE908606656A7C49CA1EB734722E4C3DED7CE6E5B::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (unitytls_x509_ref_tE1ED17887226610A1328A57FF787396C9457E7B7)+ sizeof (RuntimeObject), sizeof(unitytls_x509_ref_tE1ED17887226610A1328A57FF787396C9457E7B7 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2268[1] = 
{
	unitytls_x509_ref_tE1ED17887226610A1328A57FF787396C9457E7B7::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (unitytls_x509list_tDFEEABB4254CDE9475890D0F2AE361B45EC357C7)+ sizeof (RuntimeObject), sizeof(unitytls_x509list_tDFEEABB4254CDE9475890D0F2AE361B45EC357C7 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (unitytls_x509list_ref_tF01A6BF5ADA9C454E6B975D2669AF22D27555BF6)+ sizeof (RuntimeObject), sizeof(unitytls_x509list_ref_tF01A6BF5ADA9C454E6B975D2669AF22D27555BF6 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2270[1] = 
{
	unitytls_x509list_ref_tF01A6BF5ADA9C454E6B975D2669AF22D27555BF6::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (unitytls_x509verify_result_t835FEA0265EFD70F0762B220C663474E03402278)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2271[17] = 
{
	unitytls_x509verify_result_t835FEA0265EFD70F0762B220C663474E03402278::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (unitytls_x509verify_callback_t90C02C529DB2B9F434C18797BACC456BCB5400A9), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (unitytls_tlsctx_t6B948536BDFA3AAC0135FF136ABD7779A0B96A74)+ sizeof (RuntimeObject), sizeof(unitytls_tlsctx_t6B948536BDFA3AAC0135FF136ABD7779A0B96A74 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (unitytls_x509name_t551F433869F1BAA39C78962C7ACA1BAB9A4D6337)+ sizeof (RuntimeObject), sizeof(unitytls_x509name_t551F433869F1BAA39C78962C7ACA1BAB9A4D6337 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (unitytls_ciphersuite_tA99DBE84EF3BB26740AEF7A5CC6DEA1227B5F9DC)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2275[2] = 
{
	unitytls_ciphersuite_tA99DBE84EF3BB26740AEF7A5CC6DEA1227B5F9DC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (unitytls_protocol_t4E7BF0D3D575EDD09EA7BF6593AAB40BBAC60DF8)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2276[5] = 
{
	unitytls_protocol_t4E7BF0D3D575EDD09EA7BF6593AAB40BBAC60DF8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (unitytls_tlsctx_protocolrange_t36243D72F83DAD47C95928314F58026DE8D38F47)+ sizeof (RuntimeObject), sizeof(unitytls_tlsctx_protocolrange_t36243D72F83DAD47C95928314F58026DE8D38F47 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2277[2] = 
{
	unitytls_tlsctx_protocolrange_t36243D72F83DAD47C95928314F58026DE8D38F47::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	unitytls_tlsctx_protocolrange_t36243D72F83DAD47C95928314F58026DE8D38F47::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (unitytls_tlsctx_write_callback_tBDF40F27E011F577C3E834B14788491861F870D6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (unitytls_tlsctx_read_callback_tD85E7923018681355C1D851137CEC527F04093F5), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (unitytls_tlsctx_trace_callback_t2C8F0895EF17ECAC042835D68A6BFDB9CBC7F2AA), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (unitytls_tlsctx_certificate_callback_t55149A988CA1CE32772ACAC0031DAF4DC0F6D740), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (unitytls_tlsctx_x509verify_callback_t5FCF0307C4AB263BC611FE396EC4D2B9CF93528A), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (unitytls_tlsctx_callbacks_t7BB5F622E014A8EC300C578657E2B0550DA828B2)+ sizeof (RuntimeObject), sizeof(unitytls_tlsctx_callbacks_t7BB5F622E014A8EC300C578657E2B0550DA828B2_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2283[3] = 
{
	unitytls_tlsctx_callbacks_t7BB5F622E014A8EC300C578657E2B0550DA828B2::get_offset_of_read_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	unitytls_tlsctx_callbacks_t7BB5F622E014A8EC300C578657E2B0550DA828B2::get_offset_of_write_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	unitytls_tlsctx_callbacks_t7BB5F622E014A8EC300C578657E2B0550DA828B2::get_offset_of_data_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF), sizeof(unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2284[33] = 
{
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_UNITYTLS_INVALID_HANDLE_0(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_UNITYTLS_TLSCTX_PROTOCOLRANGE_DEFAULT_1(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_errorstate_create_2(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_errorstate_raise_error_3(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_key_get_ref_4(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_key_parse_der_5(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_key_parse_pem_6(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_key_free_7(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_x509_export_der_8(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_x509list_get_ref_9(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_x509list_get_x509_10(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_x509list_create_11(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_x509list_append_12(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_x509list_append_der_13(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_x509list_append_pem_14(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_x509list_free_15(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_x509verify_default_ca_16(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_x509verify_explicit_ca_17(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_tlsctx_create_server_18(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_tlsctx_create_client_19(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_tlsctx_server_require_client_authentication_20(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_tlsctx_set_certificate_callback_21(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_tlsctx_set_trace_callback_22(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_tlsctx_set_x509verify_callback_23(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_tlsctx_set_supported_ciphersuites_24(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_tlsctx_get_ciphersuite_25(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_tlsctx_get_protocol_26(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_tlsctx_process_handshake_27(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_tlsctx_read_28(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_tlsctx_write_29(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_tlsctx_notify_close_30(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_tlsctx_free_31(),
	unitytls_interface_struct_t0AD7ED5EDF9F15F1879FC9140A7D40C8D95A1BAF::get_offset_of_unitytls_random_generate_bytes_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (unitytls_errorstate_create_t_t104BADBBE1265BD1A3F84C153EB7A67CDDBF35A9), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (unitytls_errorstate_raise_error_t_tC441A37D4A6F1BAC1AFCA0108D4F7570EFF9E0D1), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (unitytls_key_get_ref_t_t2F4EF4CD2F6AFC4F2D166953E834C6F0A13382A7), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (unitytls_key_parse_der_t_t2ABD1C146C8B9405F6E5A78CD59A779EA607741B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (unitytls_key_parse_pem_t_tB4BCEBA4194442C8C85FA19E80B808D0EDA462AB), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (unitytls_key_free_t_tCC7AD95D3B758BB99785645E65EDCD65A1D243AB), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (unitytls_x509_export_der_t_tB0D0A02DE7E75757AFCA780298BF95467BF82856), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (unitytls_x509list_get_ref_t_t1FAB0CD82E536E0C9EB5255B145FC5AF434B3986), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (unitytls_x509list_get_x509_t_t028BB06EEB95E8F62511F2301B90D8181F4FFDB5), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (unitytls_x509list_create_t_tC040C2CF47D5426B7F6B1D6A2751507DC681CFF3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (unitytls_x509list_append_t_tAB1C185C77DFD6BD96DF7909370AA1FAD6BB90AF), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (unitytls_x509list_append_der_t_tDA1C93A382058FB563F8772B119D5B598DC37A5C), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (unitytls_x509list_free_t_tE3FC523507F07BD9901D84E9F6968CD8A583FF09), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (unitytls_x509verify_default_ca_t_t4BACB6B49AA85C025AF9B18B3F30F63C9881DE2D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (unitytls_x509verify_explicit_ca_t_t6C8BE964C5EE9B24D3734F028EFCD83F5893D2E6), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
