﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Microsoft.Win32.SafeHandles.SafeFileHandle
struct SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.Dictionary`2<System.Net.HttpConnection,System.Net.HttpConnection>
struct Dictionary_2_tE9B05628397C4CAFDAD92B83F0A416E933524B04;
// System.Collections.Generic.LinkedList`1<System.WeakReference>
struct LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t3102D0F5BABD60224F6DFF4815BCA1045831FB7C;
// System.Collections.SortedList
struct SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E;
// System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry
struct NameObjectEntry_tC137E0E1F256300B1F9F0ED88EE02B6611918B54;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1;
// System.ComponentModel.AsyncOperation
struct AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4;
// System.ComponentModel.ISite
struct ISite_t6804B48BC23ABB5F4141903F878589BCEF6097A2;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.MemoryStream
struct MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.IO.StreamReader
struct StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Net.AutoWebProxyScriptEngine
struct AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455;
// System.Net.Cache.RequestCacheBinding
struct RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724;
// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61;
// System.Net.Cache.RequestCacheProtocol
struct RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D;
// System.Net.Comparer
struct Comparer_tFC5265AD65740F9DB39C75F1E3EF8032982F45AB;
// System.Net.CompletionDelegate
struct CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674;
// System.Net.CookieCollection
struct CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3;
// System.Net.CookieTokenizer
struct CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD;
// System.Net.CookieTokenizer/RecognizedAttribute[]
struct RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55;
// System.Net.DigestHeaderParser
struct DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9;
// System.Net.EndPointListener
struct EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2;
// System.Net.FileWebRequest
struct FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F;
// System.Net.FtpAsyncResult
struct FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3;
// System.Net.FtpWebRequest
struct FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA;
// System.Net.FtpWebResponse
struct FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205;
// System.Net.HeaderInfo
struct HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8;
// System.Net.HeaderInfoTable
struct HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF;
// System.Net.HeaderParser
struct HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E;
// System.Net.HeaderVariantInfo[]
struct HeaderVariantInfoU5BU5D_t0E01B2AC4A3A836E5AC79344A8F0CBD547CC8012;
// System.Net.HttpListener
struct HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E;
// System.Net.HttpListenerContext
struct HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA;
// System.Net.HttpStreamAsyncResult
struct HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271;
// System.Net.ICredentialPolicy
struct ICredentialPolicy_t09FC19BE60498729D1F75F5EC8B35166F1FDD3C7;
// System.Net.ICredentials
struct ICredentials_t1A41F1096B037CAB53AE01434DF0747881455344;
// System.Net.IPEndPoint
struct IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F;
// System.Net.IPHostEntry
struct IPHostEntry_tB00EABDF75DB19AEAD4F3E7D93BFD7BAE558149D;
// System.Net.IWebProxy
struct IWebProxy_tA24C0862A1ACA35D20FD079E2672CA5786C1A67E;
// System.Net.LazyAsyncResult
struct LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3;
// System.Net.LazyAsyncResult/ThreadContext
struct ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082;
// System.Net.ListenerPrefix
struct ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7;
// System.Net.MonoChunkStream
struct MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5;
// System.Net.NetworkCredential
struct NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062;
// System.Net.RequestStream
struct RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35;
// System.Net.ResponseStream
struct ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032;
// System.Net.ScatterGatherBuffers
struct ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B;
// System.Net.ScatterGatherBuffers/MemoryChunk
struct MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E;
// System.Net.Security.SslStream
struct SslStream_t9CEE8F6E125C734DD807D9289C86860FFEE81087;
// System.Net.ServicePoint
struct ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4;
// System.Net.Sockets.Socket
struct Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8;
// System.Net.TimerThread/Callback
struct Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3;
// System.Net.TimerThread/Queue
struct Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643;
// System.Net.TimerThread/Timer
struct Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F;
// System.Net.TimerThread/TimerNode
struct TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B;
// System.Net.WebClient
struct WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A;
// System.Net.WebClient/ProgressData
struct ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304;
// System.Net.WebHeaderCollection/RfcChar[]
struct RfcCharU5BU5D_t27AD0ADBD612E10FCEF4917B5E70094398C6EC4E;
// System.Net.WebProxy
struct WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417;
// System.Net.WebRequest
struct WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8;
// System.Net.WebRequest/DesignerWebRequestCreate
struct DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3;
// System.Net.WebResponse
struct WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26;
// System.SByte[]
struct SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889;
// System.Security.Authentication.ExtendedProtection.ServiceNameCollection
struct ServiceNameCollection_t3C60F59BA95A4A706BB70439AE03C07D09796C2C;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_tC1C49EB4CFD571C2FFDE940C24BC69651A058F73;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538;
// System.Security.Principal.WindowsIdentity
struct WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87;
// System.String
struct String_t;
// System.StringComparer
struct StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Text.RegularExpressions.Regex[]
struct RegexU5BU5D_t9CA70F985DE1C94823B06BD0B2FCCC97927E6C53;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7;
// System.Threading.ExecutionContext
struct ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70;
// System.Threading.ManualResetEvent
struct ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01;
// System.Threading.Timer
struct Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553;
// System.Threading.WaitCallback
struct WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC;
// System.Threading.WaitHandle[]
struct WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef NAMEOBJECTCOLLECTIONBASE_T593D97BF1A2AEA0C7FC1684B447BF92A5383883D_H
#define NAMEOBJECTCOLLECTIONBASE_T593D97BF1A2AEA0C7FC1684B447BF92A5383883D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase
struct  NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D  : public RuntimeObject
{
public:
	// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::_readOnly
	bool ____readOnly_0;
	// System.Collections.ArrayList System.Collections.Specialized.NameObjectCollectionBase::_entriesArray
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____entriesArray_1;
	// System.Collections.IEqualityComparer System.Collections.Specialized.NameObjectCollectionBase::_keyComparer
	RuntimeObject* ____keyComparer_2;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.NameObjectCollectionBase::_entriesTable
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____entriesTable_3;
	// System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.NameObjectCollectionBase::_nullKeyEntry
	NameObjectEntry_tC137E0E1F256300B1F9F0ED88EE02B6611918B54 * ____nullKeyEntry_4;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.NameObjectCollectionBase::_serializationInfo
	SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * ____serializationInfo_5;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::_version
	int32_t ____version_6;
	// System.Object System.Collections.Specialized.NameObjectCollectionBase::_syncRoot
	RuntimeObject * ____syncRoot_7;

public:
	inline static int32_t get_offset_of__readOnly_0() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____readOnly_0)); }
	inline bool get__readOnly_0() const { return ____readOnly_0; }
	inline bool* get_address_of__readOnly_0() { return &____readOnly_0; }
	inline void set__readOnly_0(bool value)
	{
		____readOnly_0 = value;
	}

	inline static int32_t get_offset_of__entriesArray_1() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____entriesArray_1)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__entriesArray_1() const { return ____entriesArray_1; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__entriesArray_1() { return &____entriesArray_1; }
	inline void set__entriesArray_1(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____entriesArray_1 = value;
		Il2CppCodeGenWriteBarrier((&____entriesArray_1), value);
	}

	inline static int32_t get_offset_of__keyComparer_2() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____keyComparer_2)); }
	inline RuntimeObject* get__keyComparer_2() const { return ____keyComparer_2; }
	inline RuntimeObject** get_address_of__keyComparer_2() { return &____keyComparer_2; }
	inline void set__keyComparer_2(RuntimeObject* value)
	{
		____keyComparer_2 = value;
		Il2CppCodeGenWriteBarrier((&____keyComparer_2), value);
	}

	inline static int32_t get_offset_of__entriesTable_3() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____entriesTable_3)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__entriesTable_3() const { return ____entriesTable_3; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__entriesTable_3() { return &____entriesTable_3; }
	inline void set__entriesTable_3(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____entriesTable_3 = value;
		Il2CppCodeGenWriteBarrier((&____entriesTable_3), value);
	}

	inline static int32_t get_offset_of__nullKeyEntry_4() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____nullKeyEntry_4)); }
	inline NameObjectEntry_tC137E0E1F256300B1F9F0ED88EE02B6611918B54 * get__nullKeyEntry_4() const { return ____nullKeyEntry_4; }
	inline NameObjectEntry_tC137E0E1F256300B1F9F0ED88EE02B6611918B54 ** get_address_of__nullKeyEntry_4() { return &____nullKeyEntry_4; }
	inline void set__nullKeyEntry_4(NameObjectEntry_tC137E0E1F256300B1F9F0ED88EE02B6611918B54 * value)
	{
		____nullKeyEntry_4 = value;
		Il2CppCodeGenWriteBarrier((&____nullKeyEntry_4), value);
	}

	inline static int32_t get_offset_of__serializationInfo_5() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____serializationInfo_5)); }
	inline SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * get__serializationInfo_5() const { return ____serializationInfo_5; }
	inline SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 ** get_address_of__serializationInfo_5() { return &____serializationInfo_5; }
	inline void set__serializationInfo_5(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * value)
	{
		____serializationInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&____serializationInfo_5), value);
	}

	inline static int32_t get_offset_of__version_6() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____version_6)); }
	inline int32_t get__version_6() const { return ____version_6; }
	inline int32_t* get_address_of__version_6() { return &____version_6; }
	inline void set__version_6(int32_t value)
	{
		____version_6 = value;
	}

	inline static int32_t get_offset_of__syncRoot_7() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____syncRoot_7)); }
	inline RuntimeObject * get__syncRoot_7() const { return ____syncRoot_7; }
	inline RuntimeObject ** get_address_of__syncRoot_7() { return &____syncRoot_7; }
	inline void set__syncRoot_7(RuntimeObject * value)
	{
		____syncRoot_7 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_7), value);
	}
};

struct NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D_StaticFields
{
public:
	// System.StringComparer System.Collections.Specialized.NameObjectCollectionBase::defaultComparer
	StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * ___defaultComparer_8;

public:
	inline static int32_t get_offset_of_defaultComparer_8() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D_StaticFields, ___defaultComparer_8)); }
	inline StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * get_defaultComparer_8() const { return ___defaultComparer_8; }
	inline StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE ** get_address_of_defaultComparer_8() { return &___defaultComparer_8; }
	inline void set_defaultComparer_8(StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * value)
	{
		___defaultComparer_8 = value;
		Il2CppCodeGenWriteBarrier((&___defaultComparer_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTCOLLECTIONBASE_T593D97BF1A2AEA0C7FC1684B447BF92A5383883D_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef AUTHENTICATIONMANAGER_T0C973C7282FB47EAA7E2A239421304D47571B012_H
#define AUTHENTICATIONMANAGER_T0C973C7282FB47EAA7E2A239421304D47571B012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.AuthenticationManager
struct  AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012  : public RuntimeObject
{
public:

public:
};

struct AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields
{
public:
	// System.Collections.ArrayList System.Net.AuthenticationManager::modules
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___modules_0;
	// System.Object System.Net.AuthenticationManager::locker
	RuntimeObject * ___locker_1;
	// System.Net.ICredentialPolicy System.Net.AuthenticationManager::credential_policy
	RuntimeObject* ___credential_policy_2;

public:
	inline static int32_t get_offset_of_modules_0() { return static_cast<int32_t>(offsetof(AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields, ___modules_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_modules_0() const { return ___modules_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_modules_0() { return &___modules_0; }
	inline void set_modules_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___modules_0 = value;
		Il2CppCodeGenWriteBarrier((&___modules_0), value);
	}

	inline static int32_t get_offset_of_locker_1() { return static_cast<int32_t>(offsetof(AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields, ___locker_1)); }
	inline RuntimeObject * get_locker_1() const { return ___locker_1; }
	inline RuntimeObject ** get_address_of_locker_1() { return &___locker_1; }
	inline void set_locker_1(RuntimeObject * value)
	{
		___locker_1 = value;
		Il2CppCodeGenWriteBarrier((&___locker_1), value);
	}

	inline static int32_t get_offset_of_credential_policy_2() { return static_cast<int32_t>(offsetof(AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields, ___credential_policy_2)); }
	inline RuntimeObject* get_credential_policy_2() const { return ___credential_policy_2; }
	inline RuntimeObject** get_address_of_credential_policy_2() { return &___credential_policy_2; }
	inline void set_credential_policy_2(RuntimeObject* value)
	{
		___credential_policy_2 = value;
		Il2CppCodeGenWriteBarrier((&___credential_policy_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONMANAGER_T0C973C7282FB47EAA7E2A239421304D47571B012_H
#ifndef AUTOWEBPROXYSCRIPTENGINE_TA3B7EF6B73AD21A750868072B07936408AB3B455_H
#define AUTOWEBPROXYSCRIPTENGINE_TA3B7EF6B73AD21A750868072B07936408AB3B455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.AutoWebProxyScriptEngine
struct  AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOWEBPROXYSCRIPTENGINE_TA3B7EF6B73AD21A750868072B07936408AB3B455_H
#ifndef BASICCLIENT_T691369603F87465F4B5A78CD356545B56ABCA18C_H
#define BASICCLIENT_T691369603F87465F4B5A78CD356545B56ABCA18C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.BasicClient
struct  BasicClient_t691369603F87465F4B5A78CD356545B56ABCA18C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCLIENT_T691369603F87465F4B5A78CD356545B56ABCA18C_H
#ifndef BUFFEROFFSETSIZE_T167DA604FC5C1B4C7784C746315EABFE6D40F317_H
#define BUFFEROFFSETSIZE_T167DA604FC5C1B4C7784C746315EABFE6D40F317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.BufferOffsetSize
struct  BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317  : public RuntimeObject
{
public:
	// System.Byte[] System.Net.BufferOffsetSize::Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Buffer_0;
	// System.Int32 System.Net.BufferOffsetSize::Offset
	int32_t ___Offset_1;
	// System.Int32 System.Net.BufferOffsetSize::Size
	int32_t ___Size_2;

public:
	inline static int32_t get_offset_of_Buffer_0() { return static_cast<int32_t>(offsetof(BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317, ___Buffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Buffer_0() const { return ___Buffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Buffer_0() { return &___Buffer_0; }
	inline void set_Buffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_0), value);
	}

	inline static int32_t get_offset_of_Offset_1() { return static_cast<int32_t>(offsetof(BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317, ___Offset_1)); }
	inline int32_t get_Offset_1() const { return ___Offset_1; }
	inline int32_t* get_address_of_Offset_1() { return &___Offset_1; }
	inline void set_Offset_1(int32_t value)
	{
		___Offset_1 = value;
	}

	inline static int32_t get_offset_of_Size_2() { return static_cast<int32_t>(offsetof(BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317, ___Size_2)); }
	inline int32_t get_Size_2() const { return ___Size_2; }
	inline int32_t* get_address_of_Size_2() { return &___Size_2; }
	inline void set_Size_2(int32_t value)
	{
		___Size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEROFFSETSIZE_T167DA604FC5C1B4C7784C746315EABFE6D40F317_H
#ifndef CASEINSENSITIVEASCII_TAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_H
#define CASEINSENSITIVEASCII_TAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CaseInsensitiveAscii
struct  CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B  : public RuntimeObject
{
public:

public:
};

struct CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_StaticFields
{
public:
	// System.Net.CaseInsensitiveAscii System.Net.CaseInsensitiveAscii::StaticInstance
	CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B * ___StaticInstance_0;
	// System.Byte[] System.Net.CaseInsensitiveAscii::AsciiToLower
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___AsciiToLower_1;

public:
	inline static int32_t get_offset_of_StaticInstance_0() { return static_cast<int32_t>(offsetof(CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_StaticFields, ___StaticInstance_0)); }
	inline CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B * get_StaticInstance_0() const { return ___StaticInstance_0; }
	inline CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B ** get_address_of_StaticInstance_0() { return &___StaticInstance_0; }
	inline void set_StaticInstance_0(CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B * value)
	{
		___StaticInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___StaticInstance_0), value);
	}

	inline static int32_t get_offset_of_AsciiToLower_1() { return static_cast<int32_t>(offsetof(CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_StaticFields, ___AsciiToLower_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_AsciiToLower_1() const { return ___AsciiToLower_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_AsciiToLower_1() { return &___AsciiToLower_1; }
	inline void set_AsciiToLower_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___AsciiToLower_1 = value;
		Il2CppCodeGenWriteBarrier((&___AsciiToLower_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASEINSENSITIVEASCII_TAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_H
#ifndef READBUFFERSTATE_TBD41DF9062E5FA86A5BFC35E08C1B9224E00585B_H
#define READBUFFERSTATE_TBD41DF9062E5FA86A5BFC35E08C1B9224E00585B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ChunkedInputStream/ReadBufferState
struct  ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B  : public RuntimeObject
{
public:
	// System.Byte[] System.Net.ChunkedInputStream/ReadBufferState::Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Buffer_0;
	// System.Int32 System.Net.ChunkedInputStream/ReadBufferState::Offset
	int32_t ___Offset_1;
	// System.Int32 System.Net.ChunkedInputStream/ReadBufferState::Count
	int32_t ___Count_2;
	// System.Int32 System.Net.ChunkedInputStream/ReadBufferState::InitialCount
	int32_t ___InitialCount_3;
	// System.Net.HttpStreamAsyncResult System.Net.ChunkedInputStream/ReadBufferState::Ares
	HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271 * ___Ares_4;

public:
	inline static int32_t get_offset_of_Buffer_0() { return static_cast<int32_t>(offsetof(ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B, ___Buffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Buffer_0() const { return ___Buffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Buffer_0() { return &___Buffer_0; }
	inline void set_Buffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_0), value);
	}

	inline static int32_t get_offset_of_Offset_1() { return static_cast<int32_t>(offsetof(ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B, ___Offset_1)); }
	inline int32_t get_Offset_1() const { return ___Offset_1; }
	inline int32_t* get_address_of_Offset_1() { return &___Offset_1; }
	inline void set_Offset_1(int32_t value)
	{
		___Offset_1 = value;
	}

	inline static int32_t get_offset_of_Count_2() { return static_cast<int32_t>(offsetof(ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B, ___Count_2)); }
	inline int32_t get_Count_2() const { return ___Count_2; }
	inline int32_t* get_address_of_Count_2() { return &___Count_2; }
	inline void set_Count_2(int32_t value)
	{
		___Count_2 = value;
	}

	inline static int32_t get_offset_of_InitialCount_3() { return static_cast<int32_t>(offsetof(ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B, ___InitialCount_3)); }
	inline int32_t get_InitialCount_3() const { return ___InitialCount_3; }
	inline int32_t* get_address_of_InitialCount_3() { return &___InitialCount_3; }
	inline void set_InitialCount_3(int32_t value)
	{
		___InitialCount_3 = value;
	}

	inline static int32_t get_offset_of_Ares_4() { return static_cast<int32_t>(offsetof(ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B, ___Ares_4)); }
	inline HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271 * get_Ares_4() const { return ___Ares_4; }
	inline HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271 ** get_address_of_Ares_4() { return &___Ares_4; }
	inline void set_Ares_4(HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271 * value)
	{
		___Ares_4 = value;
		Il2CppCodeGenWriteBarrier((&___Ares_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READBUFFERSTATE_TBD41DF9062E5FA86A5BFC35E08C1B9224E00585B_H
#ifndef COMPARER_TFC5265AD65740F9DB39C75F1E3EF8032982F45AB_H
#define COMPARER_TFC5265AD65740F9DB39C75F1E3EF8032982F45AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Comparer
struct  Comparer_tFC5265AD65740F9DB39C75F1E3EF8032982F45AB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_TFC5265AD65740F9DB39C75F1E3EF8032982F45AB_H
#ifndef COOKIECOLLECTIONENUMERATOR_TDADB2721F8B45D4F815C846DCE2EF92E3760A48D_H
#define COOKIECOLLECTIONENUMERATOR_TDADB2721F8B45D4F815C846DCE2EF92E3760A48D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieCollection/CookieCollectionEnumerator
struct  CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D  : public RuntimeObject
{
public:
	// System.Net.CookieCollection System.Net.CookieCollection/CookieCollectionEnumerator::m_cookies
	CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 * ___m_cookies_0;
	// System.Int32 System.Net.CookieCollection/CookieCollectionEnumerator::m_count
	int32_t ___m_count_1;
	// System.Int32 System.Net.CookieCollection/CookieCollectionEnumerator::m_index
	int32_t ___m_index_2;
	// System.Int32 System.Net.CookieCollection/CookieCollectionEnumerator::m_version
	int32_t ___m_version_3;

public:
	inline static int32_t get_offset_of_m_cookies_0() { return static_cast<int32_t>(offsetof(CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D, ___m_cookies_0)); }
	inline CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 * get_m_cookies_0() const { return ___m_cookies_0; }
	inline CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 ** get_address_of_m_cookies_0() { return &___m_cookies_0; }
	inline void set_m_cookies_0(CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 * value)
	{
		___m_cookies_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_cookies_0), value);
	}

	inline static int32_t get_offset_of_m_count_1() { return static_cast<int32_t>(offsetof(CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D, ___m_count_1)); }
	inline int32_t get_m_count_1() const { return ___m_count_1; }
	inline int32_t* get_address_of_m_count_1() { return &___m_count_1; }
	inline void set_m_count_1(int32_t value)
	{
		___m_count_1 = value;
	}

	inline static int32_t get_offset_of_m_index_2() { return static_cast<int32_t>(offsetof(CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D, ___m_index_2)); }
	inline int32_t get_m_index_2() const { return ___m_index_2; }
	inline int32_t* get_address_of_m_index_2() { return &___m_index_2; }
	inline void set_m_index_2(int32_t value)
	{
		___m_index_2 = value;
	}

	inline static int32_t get_offset_of_m_version_3() { return static_cast<int32_t>(offsetof(CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D, ___m_version_3)); }
	inline int32_t get_m_version_3() const { return ___m_version_3; }
	inline int32_t* get_address_of_m_version_3() { return &___m_version_3; }
	inline void set_m_version_3(int32_t value)
	{
		___m_version_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIECOLLECTIONENUMERATOR_TDADB2721F8B45D4F815C846DCE2EF92E3760A48D_H
#ifndef COOKIECONTAINER_T7E062D04BAED9F3B30DDEC14B09660BB506A2A73_H
#define COOKIECONTAINER_T7E062D04BAED9F3B30DDEC14B09660BB506A2A73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieContainer
struct  CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Net.CookieContainer::m_domainTable
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___m_domainTable_1;
	// System.Int32 System.Net.CookieContainer::m_maxCookieSize
	int32_t ___m_maxCookieSize_2;
	// System.Int32 System.Net.CookieContainer::m_maxCookies
	int32_t ___m_maxCookies_3;
	// System.Int32 System.Net.CookieContainer::m_maxCookiesPerDomain
	int32_t ___m_maxCookiesPerDomain_4;
	// System.Int32 System.Net.CookieContainer::m_count
	int32_t ___m_count_5;
	// System.String System.Net.CookieContainer::m_fqdnMyDomain
	String_t* ___m_fqdnMyDomain_6;

public:
	inline static int32_t get_offset_of_m_domainTable_1() { return static_cast<int32_t>(offsetof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73, ___m_domainTable_1)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_m_domainTable_1() const { return ___m_domainTable_1; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_m_domainTable_1() { return &___m_domainTable_1; }
	inline void set_m_domainTable_1(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___m_domainTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_domainTable_1), value);
	}

	inline static int32_t get_offset_of_m_maxCookieSize_2() { return static_cast<int32_t>(offsetof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73, ___m_maxCookieSize_2)); }
	inline int32_t get_m_maxCookieSize_2() const { return ___m_maxCookieSize_2; }
	inline int32_t* get_address_of_m_maxCookieSize_2() { return &___m_maxCookieSize_2; }
	inline void set_m_maxCookieSize_2(int32_t value)
	{
		___m_maxCookieSize_2 = value;
	}

	inline static int32_t get_offset_of_m_maxCookies_3() { return static_cast<int32_t>(offsetof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73, ___m_maxCookies_3)); }
	inline int32_t get_m_maxCookies_3() const { return ___m_maxCookies_3; }
	inline int32_t* get_address_of_m_maxCookies_3() { return &___m_maxCookies_3; }
	inline void set_m_maxCookies_3(int32_t value)
	{
		___m_maxCookies_3 = value;
	}

	inline static int32_t get_offset_of_m_maxCookiesPerDomain_4() { return static_cast<int32_t>(offsetof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73, ___m_maxCookiesPerDomain_4)); }
	inline int32_t get_m_maxCookiesPerDomain_4() const { return ___m_maxCookiesPerDomain_4; }
	inline int32_t* get_address_of_m_maxCookiesPerDomain_4() { return &___m_maxCookiesPerDomain_4; }
	inline void set_m_maxCookiesPerDomain_4(int32_t value)
	{
		___m_maxCookiesPerDomain_4 = value;
	}

	inline static int32_t get_offset_of_m_count_5() { return static_cast<int32_t>(offsetof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73, ___m_count_5)); }
	inline int32_t get_m_count_5() const { return ___m_count_5; }
	inline int32_t* get_address_of_m_count_5() { return &___m_count_5; }
	inline void set_m_count_5(int32_t value)
	{
		___m_count_5 = value;
	}

	inline static int32_t get_offset_of_m_fqdnMyDomain_6() { return static_cast<int32_t>(offsetof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73, ___m_fqdnMyDomain_6)); }
	inline String_t* get_m_fqdnMyDomain_6() const { return ___m_fqdnMyDomain_6; }
	inline String_t** get_address_of_m_fqdnMyDomain_6() { return &___m_fqdnMyDomain_6; }
	inline void set_m_fqdnMyDomain_6(String_t* value)
	{
		___m_fqdnMyDomain_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_fqdnMyDomain_6), value);
	}
};

struct CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73_StaticFields
{
public:
	// System.Net.HeaderVariantInfo[] System.Net.CookieContainer::HeaderInfo
	HeaderVariantInfoU5BU5D_t0E01B2AC4A3A836E5AC79344A8F0CBD547CC8012* ___HeaderInfo_0;

public:
	inline static int32_t get_offset_of_HeaderInfo_0() { return static_cast<int32_t>(offsetof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73_StaticFields, ___HeaderInfo_0)); }
	inline HeaderVariantInfoU5BU5D_t0E01B2AC4A3A836E5AC79344A8F0CBD547CC8012* get_HeaderInfo_0() const { return ___HeaderInfo_0; }
	inline HeaderVariantInfoU5BU5D_t0E01B2AC4A3A836E5AC79344A8F0CBD547CC8012** get_address_of_HeaderInfo_0() { return &___HeaderInfo_0; }
	inline void set_HeaderInfo_0(HeaderVariantInfoU5BU5D_t0E01B2AC4A3A836E5AC79344A8F0CBD547CC8012* value)
	{
		___HeaderInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___HeaderInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIECONTAINER_T7E062D04BAED9F3B30DDEC14B09660BB506A2A73_H
#ifndef COOKIEPARSER_T6034725CF7B5A3842FEC753620D331478F74B396_H
#define COOKIEPARSER_T6034725CF7B5A3842FEC753620D331478F74B396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieParser
struct  CookieParser_t6034725CF7B5A3842FEC753620D331478F74B396  : public RuntimeObject
{
public:
	// System.Net.CookieTokenizer System.Net.CookieParser::m_tokenizer
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD * ___m_tokenizer_0;

public:
	inline static int32_t get_offset_of_m_tokenizer_0() { return static_cast<int32_t>(offsetof(CookieParser_t6034725CF7B5A3842FEC753620D331478F74B396, ___m_tokenizer_0)); }
	inline CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD * get_m_tokenizer_0() const { return ___m_tokenizer_0; }
	inline CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD ** get_address_of_m_tokenizer_0() { return &___m_tokenizer_0; }
	inline void set_m_tokenizer_0(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD * value)
	{
		___m_tokenizer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_tokenizer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIEPARSER_T6034725CF7B5A3842FEC753620D331478F74B396_H
#ifndef DEFAULTCERTIFICATEPOLICY_T2B28BF921CE86F4956EF998580E48C314B14A674_H
#define DEFAULTCERTIFICATEPOLICY_T2B28BF921CE86F4956EF998580E48C314B14A674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DefaultCertificatePolicy
struct  DefaultCertificatePolicy_t2B28BF921CE86F4956EF998580E48C314B14A674  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCERTIFICATEPOLICY_T2B28BF921CE86F4956EF998580E48C314B14A674_H
#ifndef DIGESTCLIENT_T2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C_H
#define DIGESTCLIENT_T2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DigestClient
struct  DigestClient_t2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C  : public RuntimeObject
{
public:

public:
};

struct DigestClient_t2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C_StaticFields
{
public:
	// System.Collections.Hashtable System.Net.DigestClient::cache
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___cache_0;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(DigestClient_t2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C_StaticFields, ___cache_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_cache_0() const { return ___cache_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___cache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTCLIENT_T2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C_H
#ifndef DIGESTHEADERPARSER_T86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9_H
#define DIGESTHEADERPARSER_T86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DigestHeaderParser
struct  DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9  : public RuntimeObject
{
public:
	// System.String System.Net.DigestHeaderParser::header
	String_t* ___header_0;
	// System.Int32 System.Net.DigestHeaderParser::length
	int32_t ___length_1;
	// System.Int32 System.Net.DigestHeaderParser::pos
	int32_t ___pos_2;
	// System.String[] System.Net.DigestHeaderParser::values
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___values_4;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((&___header_0), value);
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_pos_2() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9, ___pos_2)); }
	inline int32_t get_pos_2() const { return ___pos_2; }
	inline int32_t* get_address_of_pos_2() { return &___pos_2; }
	inline void set_pos_2(int32_t value)
	{
		___pos_2 = value;
	}

	inline static int32_t get_offset_of_values_4() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9, ___values_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_values_4() const { return ___values_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_values_4() { return &___values_4; }
	inline void set_values_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___values_4 = value;
		Il2CppCodeGenWriteBarrier((&___values_4), value);
	}
};

struct DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9_StaticFields
{
public:
	// System.String[] System.Net.DigestHeaderParser::keywords
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___keywords_3;

public:
	inline static int32_t get_offset_of_keywords_3() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9_StaticFields, ___keywords_3)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_keywords_3() const { return ___keywords_3; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_keywords_3() { return &___keywords_3; }
	inline void set_keywords_3(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___keywords_3 = value;
		Il2CppCodeGenWriteBarrier((&___keywords_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTHEADERPARSER_T86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9_H
#ifndef DNS_T0E6B5B77C654107F106B577875FE899BAF8ADCF9_H
#define DNS_T0E6B5B77C654107F106B577875FE899BAF8ADCF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Dns
struct  Dns_t0E6B5B77C654107F106B577875FE899BAF8ADCF9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNS_T0E6B5B77C654107F106B577875FE899BAF8ADCF9_H
#ifndef EMPTYWEBPROXY_TF6CEF11A280246455534D46AD1710271B8BEE22D_H
#define EMPTYWEBPROXY_TF6CEF11A280246455534D46AD1710271B8BEE22D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EmptyWebProxy
struct  EmptyWebProxy_tF6CEF11A280246455534D46AD1710271B8BEE22D  : public RuntimeObject
{
public:
	// System.Net.ICredentials System.Net.EmptyWebProxy::m_credentials
	RuntimeObject* ___m_credentials_0;

public:
	inline static int32_t get_offset_of_m_credentials_0() { return static_cast<int32_t>(offsetof(EmptyWebProxy_tF6CEF11A280246455534D46AD1710271B8BEE22D, ___m_credentials_0)); }
	inline RuntimeObject* get_m_credentials_0() const { return ___m_credentials_0; }
	inline RuntimeObject** get_address_of_m_credentials_0() { return &___m_credentials_0; }
	inline void set_m_credentials_0(RuntimeObject* value)
	{
		___m_credentials_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_credentials_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYWEBPROXY_TF6CEF11A280246455534D46AD1710271B8BEE22D_H
#ifndef ENDPOINTLISTENER_TF1D36E022EAFCD075CD0398D2868363AA9FB10C2_H
#define ENDPOINTLISTENER_TF1D36E022EAFCD075CD0398D2868363AA9FB10C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EndPointListener
struct  EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2  : public RuntimeObject
{
public:
	// System.Net.HttpListener System.Net.EndPointListener::listener
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * ___listener_0;
	// System.Net.IPEndPoint System.Net.EndPointListener::endpoint
	IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * ___endpoint_1;
	// System.Net.Sockets.Socket System.Net.EndPointListener::sock
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___sock_2;
	// System.Collections.Hashtable System.Net.EndPointListener::prefixes
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___prefixes_3;
	// System.Collections.ArrayList System.Net.EndPointListener::unhandled
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___unhandled_4;
	// System.Collections.ArrayList System.Net.EndPointListener::all
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___all_5;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.EndPointListener::cert
	X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * ___cert_6;
	// System.Boolean System.Net.EndPointListener::secure
	bool ___secure_7;
	// System.Collections.Generic.Dictionary`2<System.Net.HttpConnection,System.Net.HttpConnection> System.Net.EndPointListener::unregistered
	Dictionary_2_tE9B05628397C4CAFDAD92B83F0A416E933524B04 * ___unregistered_8;

public:
	inline static int32_t get_offset_of_listener_0() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___listener_0)); }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * get_listener_0() const { return ___listener_0; }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E ** get_address_of_listener_0() { return &___listener_0; }
	inline void set_listener_0(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * value)
	{
		___listener_0 = value;
		Il2CppCodeGenWriteBarrier((&___listener_0), value);
	}

	inline static int32_t get_offset_of_endpoint_1() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___endpoint_1)); }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * get_endpoint_1() const { return ___endpoint_1; }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F ** get_address_of_endpoint_1() { return &___endpoint_1; }
	inline void set_endpoint_1(IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * value)
	{
		___endpoint_1 = value;
		Il2CppCodeGenWriteBarrier((&___endpoint_1), value);
	}

	inline static int32_t get_offset_of_sock_2() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___sock_2)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_sock_2() const { return ___sock_2; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_sock_2() { return &___sock_2; }
	inline void set_sock_2(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___sock_2 = value;
		Il2CppCodeGenWriteBarrier((&___sock_2), value);
	}

	inline static int32_t get_offset_of_prefixes_3() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___prefixes_3)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_prefixes_3() const { return ___prefixes_3; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_prefixes_3() { return &___prefixes_3; }
	inline void set_prefixes_3(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___prefixes_3 = value;
		Il2CppCodeGenWriteBarrier((&___prefixes_3), value);
	}

	inline static int32_t get_offset_of_unhandled_4() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___unhandled_4)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_unhandled_4() const { return ___unhandled_4; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_unhandled_4() { return &___unhandled_4; }
	inline void set_unhandled_4(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___unhandled_4 = value;
		Il2CppCodeGenWriteBarrier((&___unhandled_4), value);
	}

	inline static int32_t get_offset_of_all_5() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___all_5)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_all_5() const { return ___all_5; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_all_5() { return &___all_5; }
	inline void set_all_5(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___all_5 = value;
		Il2CppCodeGenWriteBarrier((&___all_5), value);
	}

	inline static int32_t get_offset_of_cert_6() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___cert_6)); }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * get_cert_6() const { return ___cert_6; }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 ** get_address_of_cert_6() { return &___cert_6; }
	inline void set_cert_6(X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * value)
	{
		___cert_6 = value;
		Il2CppCodeGenWriteBarrier((&___cert_6), value);
	}

	inline static int32_t get_offset_of_secure_7() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___secure_7)); }
	inline bool get_secure_7() const { return ___secure_7; }
	inline bool* get_address_of_secure_7() { return &___secure_7; }
	inline void set_secure_7(bool value)
	{
		___secure_7 = value;
	}

	inline static int32_t get_offset_of_unregistered_8() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___unregistered_8)); }
	inline Dictionary_2_tE9B05628397C4CAFDAD92B83F0A416E933524B04 * get_unregistered_8() const { return ___unregistered_8; }
	inline Dictionary_2_tE9B05628397C4CAFDAD92B83F0A416E933524B04 ** get_address_of_unregistered_8() { return &___unregistered_8; }
	inline void set_unregistered_8(Dictionary_2_tE9B05628397C4CAFDAD92B83F0A416E933524B04 * value)
	{
		___unregistered_8 = value;
		Il2CppCodeGenWriteBarrier((&___unregistered_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINTLISTENER_TF1D36E022EAFCD075CD0398D2868363AA9FB10C2_H
#ifndef ENDPOINTMANAGER_TEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2_H
#define ENDPOINTMANAGER_TEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EndPointManager
struct  EndPointManager_tEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2  : public RuntimeObject
{
public:

public:
};

struct EndPointManager_tEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2_StaticFields
{
public:
	// System.Collections.Hashtable System.Net.EndPointManager::ip_to_endpoints
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___ip_to_endpoints_0;

public:
	inline static int32_t get_offset_of_ip_to_endpoints_0() { return static_cast<int32_t>(offsetof(EndPointManager_tEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2_StaticFields, ___ip_to_endpoints_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_ip_to_endpoints_0() const { return ___ip_to_endpoints_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_ip_to_endpoints_0() { return &___ip_to_endpoints_0; }
	inline void set_ip_to_endpoints_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___ip_to_endpoints_0 = value;
		Il2CppCodeGenWriteBarrier((&___ip_to_endpoints_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINTMANAGER_TEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2_H
#ifndef FILEWEBREQUESTCREATOR_TC02A1A70722C45B078D759F22AE10256A6900C6D_H
#define FILEWEBREQUESTCREATOR_TC02A1A70722C45B078D759F22AE10256A6900C6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebRequestCreator
struct  FileWebRequestCreator_tC02A1A70722C45B078D759F22AE10256A6900C6D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBREQUESTCREATOR_TC02A1A70722C45B078D759F22AE10256A6900C6D_H
#ifndef FTPASYNCRESULT_TB318D495766A9449055B1D8C8C801095CF2CDEA3_H
#define FTPASYNCRESULT_TB318D495766A9449055B1D8C8C801095CF2CDEA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpAsyncResult
struct  FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3  : public RuntimeObject
{
public:
	// System.Net.FtpWebResponse System.Net.FtpAsyncResult::response
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 * ___response_0;
	// System.Threading.ManualResetEvent System.Net.FtpAsyncResult::waitHandle
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___waitHandle_1;
	// System.Exception System.Net.FtpAsyncResult::exception
	Exception_t * ___exception_2;
	// System.AsyncCallback System.Net.FtpAsyncResult::callback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback_3;
	// System.IO.Stream System.Net.FtpAsyncResult::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_4;
	// System.Object System.Net.FtpAsyncResult::state
	RuntimeObject * ___state_5;
	// System.Boolean System.Net.FtpAsyncResult::completed
	bool ___completed_6;
	// System.Boolean System.Net.FtpAsyncResult::synch
	bool ___synch_7;
	// System.Object System.Net.FtpAsyncResult::locker
	RuntimeObject * ___locker_8;

public:
	inline static int32_t get_offset_of_response_0() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___response_0)); }
	inline FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 * get_response_0() const { return ___response_0; }
	inline FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 ** get_address_of_response_0() { return &___response_0; }
	inline void set_response_0(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 * value)
	{
		___response_0 = value;
		Il2CppCodeGenWriteBarrier((&___response_0), value);
	}

	inline static int32_t get_offset_of_waitHandle_1() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___waitHandle_1)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_waitHandle_1() const { return ___waitHandle_1; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_waitHandle_1() { return &___waitHandle_1; }
	inline void set_waitHandle_1(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___waitHandle_1 = value;
		Il2CppCodeGenWriteBarrier((&___waitHandle_1), value);
	}

	inline static int32_t get_offset_of_exception_2() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___exception_2)); }
	inline Exception_t * get_exception_2() const { return ___exception_2; }
	inline Exception_t ** get_address_of_exception_2() { return &___exception_2; }
	inline void set_exception_2(Exception_t * value)
	{
		___exception_2 = value;
		Il2CppCodeGenWriteBarrier((&___exception_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___callback_3)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_callback_3() const { return ___callback_3; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_stream_4() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___stream_4)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_4() const { return ___stream_4; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_4() { return &___stream_4; }
	inline void set_stream_4(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_4 = value;
		Il2CppCodeGenWriteBarrier((&___stream_4), value);
	}

	inline static int32_t get_offset_of_state_5() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___state_5)); }
	inline RuntimeObject * get_state_5() const { return ___state_5; }
	inline RuntimeObject ** get_address_of_state_5() { return &___state_5; }
	inline void set_state_5(RuntimeObject * value)
	{
		___state_5 = value;
		Il2CppCodeGenWriteBarrier((&___state_5), value);
	}

	inline static int32_t get_offset_of_completed_6() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___completed_6)); }
	inline bool get_completed_6() const { return ___completed_6; }
	inline bool* get_address_of_completed_6() { return &___completed_6; }
	inline void set_completed_6(bool value)
	{
		___completed_6 = value;
	}

	inline static int32_t get_offset_of_synch_7() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___synch_7)); }
	inline bool get_synch_7() const { return ___synch_7; }
	inline bool* get_address_of_synch_7() { return &___synch_7; }
	inline void set_synch_7(bool value)
	{
		___synch_7 = value;
	}

	inline static int32_t get_offset_of_locker_8() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___locker_8)); }
	inline RuntimeObject * get_locker_8() const { return ___locker_8; }
	inline RuntimeObject ** get_address_of_locker_8() { return &___locker_8; }
	inline void set_locker_8(RuntimeObject * value)
	{
		___locker_8 = value;
		Il2CppCodeGenWriteBarrier((&___locker_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPASYNCRESULT_TB318D495766A9449055B1D8C8C801095CF2CDEA3_H
#ifndef FTPREQUESTCREATOR_T2C5CC32221C790FB648AF6276DA861B4ABAC357F_H
#define FTPREQUESTCREATOR_T2C5CC32221C790FB648AF6276DA861B4ABAC357F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpRequestCreator
struct  FtpRequestCreator_t2C5CC32221C790FB648AF6276DA861B4ABAC357F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPREQUESTCREATOR_T2C5CC32221C790FB648AF6276DA861B4ABAC357F_H
#ifndef HEADERINFO_T08A38618F1A42BEE66373512B83B804DAEAF2EB8_H
#define HEADERINFO_T08A38618F1A42BEE66373512B83B804DAEAF2EB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HeaderInfo
struct  HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8  : public RuntimeObject
{
public:
	// System.Boolean System.Net.HeaderInfo::IsRequestRestricted
	bool ___IsRequestRestricted_0;
	// System.Boolean System.Net.HeaderInfo::IsResponseRestricted
	bool ___IsResponseRestricted_1;
	// System.Net.HeaderParser System.Net.HeaderInfo::Parser
	HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * ___Parser_2;
	// System.String System.Net.HeaderInfo::HeaderName
	String_t* ___HeaderName_3;
	// System.Boolean System.Net.HeaderInfo::AllowMultiValues
	bool ___AllowMultiValues_4;

public:
	inline static int32_t get_offset_of_IsRequestRestricted_0() { return static_cast<int32_t>(offsetof(HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8, ___IsRequestRestricted_0)); }
	inline bool get_IsRequestRestricted_0() const { return ___IsRequestRestricted_0; }
	inline bool* get_address_of_IsRequestRestricted_0() { return &___IsRequestRestricted_0; }
	inline void set_IsRequestRestricted_0(bool value)
	{
		___IsRequestRestricted_0 = value;
	}

	inline static int32_t get_offset_of_IsResponseRestricted_1() { return static_cast<int32_t>(offsetof(HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8, ___IsResponseRestricted_1)); }
	inline bool get_IsResponseRestricted_1() const { return ___IsResponseRestricted_1; }
	inline bool* get_address_of_IsResponseRestricted_1() { return &___IsResponseRestricted_1; }
	inline void set_IsResponseRestricted_1(bool value)
	{
		___IsResponseRestricted_1 = value;
	}

	inline static int32_t get_offset_of_Parser_2() { return static_cast<int32_t>(offsetof(HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8, ___Parser_2)); }
	inline HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * get_Parser_2() const { return ___Parser_2; }
	inline HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E ** get_address_of_Parser_2() { return &___Parser_2; }
	inline void set_Parser_2(HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * value)
	{
		___Parser_2 = value;
		Il2CppCodeGenWriteBarrier((&___Parser_2), value);
	}

	inline static int32_t get_offset_of_HeaderName_3() { return static_cast<int32_t>(offsetof(HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8, ___HeaderName_3)); }
	inline String_t* get_HeaderName_3() const { return ___HeaderName_3; }
	inline String_t** get_address_of_HeaderName_3() { return &___HeaderName_3; }
	inline void set_HeaderName_3(String_t* value)
	{
		___HeaderName_3 = value;
		Il2CppCodeGenWriteBarrier((&___HeaderName_3), value);
	}

	inline static int32_t get_offset_of_AllowMultiValues_4() { return static_cast<int32_t>(offsetof(HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8, ___AllowMultiValues_4)); }
	inline bool get_AllowMultiValues_4() const { return ___AllowMultiValues_4; }
	inline bool* get_address_of_AllowMultiValues_4() { return &___AllowMultiValues_4; }
	inline void set_AllowMultiValues_4(bool value)
	{
		___AllowMultiValues_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERINFO_T08A38618F1A42BEE66373512B83B804DAEAF2EB8_H
#ifndef HEADERINFOTABLE_T16B4CA77715B871579C8DE60EBD92E8EDD332BAF_H
#define HEADERINFOTABLE_T16B4CA77715B871579C8DE60EBD92E8EDD332BAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HeaderInfoTable
struct  HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF  : public RuntimeObject
{
public:

public:
};

struct HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields
{
public:
	// System.Collections.Hashtable System.Net.HeaderInfoTable::HeaderHashTable
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___HeaderHashTable_0;
	// System.Net.HeaderInfo System.Net.HeaderInfoTable::UnknownHeaderInfo
	HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8 * ___UnknownHeaderInfo_1;
	// System.Net.HeaderParser System.Net.HeaderInfoTable::SingleParser
	HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * ___SingleParser_2;
	// System.Net.HeaderParser System.Net.HeaderInfoTable::MultiParser
	HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * ___MultiParser_3;

public:
	inline static int32_t get_offset_of_HeaderHashTable_0() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields, ___HeaderHashTable_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_HeaderHashTable_0() const { return ___HeaderHashTable_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_HeaderHashTable_0() { return &___HeaderHashTable_0; }
	inline void set_HeaderHashTable_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___HeaderHashTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___HeaderHashTable_0), value);
	}

	inline static int32_t get_offset_of_UnknownHeaderInfo_1() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields, ___UnknownHeaderInfo_1)); }
	inline HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8 * get_UnknownHeaderInfo_1() const { return ___UnknownHeaderInfo_1; }
	inline HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8 ** get_address_of_UnknownHeaderInfo_1() { return &___UnknownHeaderInfo_1; }
	inline void set_UnknownHeaderInfo_1(HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8 * value)
	{
		___UnknownHeaderInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___UnknownHeaderInfo_1), value);
	}

	inline static int32_t get_offset_of_SingleParser_2() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields, ___SingleParser_2)); }
	inline HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * get_SingleParser_2() const { return ___SingleParser_2; }
	inline HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E ** get_address_of_SingleParser_2() { return &___SingleParser_2; }
	inline void set_SingleParser_2(HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * value)
	{
		___SingleParser_2 = value;
		Il2CppCodeGenWriteBarrier((&___SingleParser_2), value);
	}

	inline static int32_t get_offset_of_MultiParser_3() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields, ___MultiParser_3)); }
	inline HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * get_MultiParser_3() const { return ___MultiParser_3; }
	inline HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E ** get_address_of_MultiParser_3() { return &___MultiParser_3; }
	inline void set_MultiParser_3(HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * value)
	{
		___MultiParser_3 = value;
		Il2CppCodeGenWriteBarrier((&___MultiParser_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERINFOTABLE_T16B4CA77715B871579C8DE60EBD92E8EDD332BAF_H
#ifndef LAZYASYNCRESULT_T6D867D275402699126BB3DC89093BD94CFFDA5D3_H
#define LAZYASYNCRESULT_T6D867D275402699126BB3DC89093BD94CFFDA5D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.LazyAsyncResult
struct  LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3  : public RuntimeObject
{
public:
	// System.Object System.Net.LazyAsyncResult::m_AsyncObject
	RuntimeObject * ___m_AsyncObject_1;
	// System.Object System.Net.LazyAsyncResult::m_AsyncState
	RuntimeObject * ___m_AsyncState_2;
	// System.AsyncCallback System.Net.LazyAsyncResult::m_AsyncCallback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___m_AsyncCallback_3;
	// System.Object System.Net.LazyAsyncResult::m_Result
	RuntimeObject * ___m_Result_4;
	// System.Int32 System.Net.LazyAsyncResult::m_IntCompleted
	int32_t ___m_IntCompleted_5;
	// System.Boolean System.Net.LazyAsyncResult::m_UserEvent
	bool ___m_UserEvent_6;
	// System.Object System.Net.LazyAsyncResult::m_Event
	RuntimeObject * ___m_Event_7;

public:
	inline static int32_t get_offset_of_m_AsyncObject_1() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_AsyncObject_1)); }
	inline RuntimeObject * get_m_AsyncObject_1() const { return ___m_AsyncObject_1; }
	inline RuntimeObject ** get_address_of_m_AsyncObject_1() { return &___m_AsyncObject_1; }
	inline void set_m_AsyncObject_1(RuntimeObject * value)
	{
		___m_AsyncObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncObject_1), value);
	}

	inline static int32_t get_offset_of_m_AsyncState_2() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_AsyncState_2)); }
	inline RuntimeObject * get_m_AsyncState_2() const { return ___m_AsyncState_2; }
	inline RuntimeObject ** get_address_of_m_AsyncState_2() { return &___m_AsyncState_2; }
	inline void set_m_AsyncState_2(RuntimeObject * value)
	{
		___m_AsyncState_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncState_2), value);
	}

	inline static int32_t get_offset_of_m_AsyncCallback_3() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_AsyncCallback_3)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_m_AsyncCallback_3() const { return ___m_AsyncCallback_3; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_m_AsyncCallback_3() { return &___m_AsyncCallback_3; }
	inline void set_m_AsyncCallback_3(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___m_AsyncCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncCallback_3), value);
	}

	inline static int32_t get_offset_of_m_Result_4() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_Result_4)); }
	inline RuntimeObject * get_m_Result_4() const { return ___m_Result_4; }
	inline RuntimeObject ** get_address_of_m_Result_4() { return &___m_Result_4; }
	inline void set_m_Result_4(RuntimeObject * value)
	{
		___m_Result_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Result_4), value);
	}

	inline static int32_t get_offset_of_m_IntCompleted_5() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_IntCompleted_5)); }
	inline int32_t get_m_IntCompleted_5() const { return ___m_IntCompleted_5; }
	inline int32_t* get_address_of_m_IntCompleted_5() { return &___m_IntCompleted_5; }
	inline void set_m_IntCompleted_5(int32_t value)
	{
		___m_IntCompleted_5 = value;
	}

	inline static int32_t get_offset_of_m_UserEvent_6() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_UserEvent_6)); }
	inline bool get_m_UserEvent_6() const { return ___m_UserEvent_6; }
	inline bool* get_address_of_m_UserEvent_6() { return &___m_UserEvent_6; }
	inline void set_m_UserEvent_6(bool value)
	{
		___m_UserEvent_6 = value;
	}

	inline static int32_t get_offset_of_m_Event_7() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_Event_7)); }
	inline RuntimeObject * get_m_Event_7() const { return ___m_Event_7; }
	inline RuntimeObject ** get_address_of_m_Event_7() { return &___m_Event_7; }
	inline void set_m_Event_7(RuntimeObject * value)
	{
		___m_Event_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Event_7), value);
	}
};

struct LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3_ThreadStaticFields
{
public:
	// System.Net.LazyAsyncResult/ThreadContext System.Net.LazyAsyncResult::t_ThreadContext
	ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082 * ___t_ThreadContext_0;

public:
	inline static int32_t get_offset_of_t_ThreadContext_0() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3_ThreadStaticFields, ___t_ThreadContext_0)); }
	inline ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082 * get_t_ThreadContext_0() const { return ___t_ThreadContext_0; }
	inline ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082 ** get_address_of_t_ThreadContext_0() { return &___t_ThreadContext_0; }
	inline void set_t_ThreadContext_0(ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082 * value)
	{
		___t_ThreadContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_ThreadContext_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAZYASYNCRESULT_T6D867D275402699126BB3DC89093BD94CFFDA5D3_H
#ifndef THREADCONTEXT_TCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082_H
#define THREADCONTEXT_TCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.LazyAsyncResult/ThreadContext
struct  ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082  : public RuntimeObject
{
public:
	// System.Int32 System.Net.LazyAsyncResult/ThreadContext::m_NestedIOCount
	int32_t ___m_NestedIOCount_0;

public:
	inline static int32_t get_offset_of_m_NestedIOCount_0() { return static_cast<int32_t>(offsetof(ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082, ___m_NestedIOCount_0)); }
	inline int32_t get_m_NestedIOCount_0() const { return ___m_NestedIOCount_0; }
	inline int32_t* get_address_of_m_NestedIOCount_0() { return &___m_NestedIOCount_0; }
	inline void set_m_NestedIOCount_0(int32_t value)
	{
		___m_NestedIOCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADCONTEXT_TCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082_H
#ifndef LOGGING_T16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70_H
#define LOGGING_T16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Logging
struct  Logging_t16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70  : public RuntimeObject
{
public:

public:
};

struct Logging_t16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70_StaticFields
{
public:
	// System.Boolean System.Net.Logging::On
	bool ___On_0;

public:
	inline static int32_t get_offset_of_On_0() { return static_cast<int32_t>(offsetof(Logging_t16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70_StaticFields, ___On_0)); }
	inline bool get_On_0() const { return ___On_0; }
	inline bool* get_address_of_On_0() { return &___On_0; }
	inline void set_On_0(bool value)
	{
		___On_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGING_T16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70_H
#ifndef NETRES_TB18DF1FAF98D8D7505D72FA149E57F0D31E2653B_H
#define NETRES_TB18DF1FAF98D8D7505D72FA149E57F0D31E2653B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetRes
struct  NetRes_tB18DF1FAF98D8D7505D72FA149E57F0D31E2653B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETRES_TB18DF1FAF98D8D7505D72FA149E57F0D31E2653B_H
#ifndef PATHLIST_TE89F0E044B0D96268DB20C9B0FC852C690C2DC8A_H
#define PATHLIST_TE89F0E044B0D96268DB20C9B0FC852C690C2DC8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.PathList
struct  PathList_tE89F0E044B0D96268DB20C9B0FC852C690C2DC8A  : public RuntimeObject
{
public:
	// System.Collections.SortedList System.Net.PathList::m_list
	SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E * ___m_list_0;

public:
	inline static int32_t get_offset_of_m_list_0() { return static_cast<int32_t>(offsetof(PathList_tE89F0E044B0D96268DB20C9B0FC852C690C2DC8A, ___m_list_0)); }
	inline SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E * get_m_list_0() const { return ___m_list_0; }
	inline SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E ** get_address_of_m_list_0() { return &___m_list_0; }
	inline void set_m_list_0(SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E * value)
	{
		___m_list_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHLIST_TE89F0E044B0D96268DB20C9B0FC852C690C2DC8A_H
#ifndef PATHLISTCOMPARER_TDBE17A93599D72911D22973B739F500E8C26ADCF_H
#define PATHLISTCOMPARER_TDBE17A93599D72911D22973B739F500E8C26ADCF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.PathList/PathListComparer
struct  PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF  : public RuntimeObject
{
public:

public:
};

struct PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF_StaticFields
{
public:
	// System.Net.PathList/PathListComparer System.Net.PathList/PathListComparer::StaticInstance
	PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF * ___StaticInstance_0;

public:
	inline static int32_t get_offset_of_StaticInstance_0() { return static_cast<int32_t>(offsetof(PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF_StaticFields, ___StaticInstance_0)); }
	inline PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF * get_StaticInstance_0() const { return ___StaticInstance_0; }
	inline PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF ** get_address_of_StaticInstance_0() { return &___StaticInstance_0; }
	inline void set_StaticInstance_0(PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF * value)
	{
		___StaticInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___StaticInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHLISTCOMPARER_TDBE17A93599D72911D22973B739F500E8C26ADCF_H
#ifndef SCATTERGATHERBUFFERS_T5090EA13155952525395C6141ACA51BDBE255E6B_H
#define SCATTERGATHERBUFFERS_T5090EA13155952525395C6141ACA51BDBE255E6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ScatterGatherBuffers
struct  ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B  : public RuntimeObject
{
public:
	// System.Net.ScatterGatherBuffers/MemoryChunk System.Net.ScatterGatherBuffers::headChunk
	MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * ___headChunk_0;
	// System.Net.ScatterGatherBuffers/MemoryChunk System.Net.ScatterGatherBuffers::currentChunk
	MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * ___currentChunk_1;
	// System.Int32 System.Net.ScatterGatherBuffers::nextChunkLength
	int32_t ___nextChunkLength_2;
	// System.Int32 System.Net.ScatterGatherBuffers::totalLength
	int32_t ___totalLength_3;
	// System.Int32 System.Net.ScatterGatherBuffers::chunkCount
	int32_t ___chunkCount_4;

public:
	inline static int32_t get_offset_of_headChunk_0() { return static_cast<int32_t>(offsetof(ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B, ___headChunk_0)); }
	inline MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * get_headChunk_0() const { return ___headChunk_0; }
	inline MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F ** get_address_of_headChunk_0() { return &___headChunk_0; }
	inline void set_headChunk_0(MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * value)
	{
		___headChunk_0 = value;
		Il2CppCodeGenWriteBarrier((&___headChunk_0), value);
	}

	inline static int32_t get_offset_of_currentChunk_1() { return static_cast<int32_t>(offsetof(ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B, ___currentChunk_1)); }
	inline MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * get_currentChunk_1() const { return ___currentChunk_1; }
	inline MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F ** get_address_of_currentChunk_1() { return &___currentChunk_1; }
	inline void set_currentChunk_1(MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * value)
	{
		___currentChunk_1 = value;
		Il2CppCodeGenWriteBarrier((&___currentChunk_1), value);
	}

	inline static int32_t get_offset_of_nextChunkLength_2() { return static_cast<int32_t>(offsetof(ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B, ___nextChunkLength_2)); }
	inline int32_t get_nextChunkLength_2() const { return ___nextChunkLength_2; }
	inline int32_t* get_address_of_nextChunkLength_2() { return &___nextChunkLength_2; }
	inline void set_nextChunkLength_2(int32_t value)
	{
		___nextChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_totalLength_3() { return static_cast<int32_t>(offsetof(ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B, ___totalLength_3)); }
	inline int32_t get_totalLength_3() const { return ___totalLength_3; }
	inline int32_t* get_address_of_totalLength_3() { return &___totalLength_3; }
	inline void set_totalLength_3(int32_t value)
	{
		___totalLength_3 = value;
	}

	inline static int32_t get_offset_of_chunkCount_4() { return static_cast<int32_t>(offsetof(ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B, ___chunkCount_4)); }
	inline int32_t get_chunkCount_4() const { return ___chunkCount_4; }
	inline int32_t* get_address_of_chunkCount_4() { return &___chunkCount_4; }
	inline void set_chunkCount_4(int32_t value)
	{
		___chunkCount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCATTERGATHERBUFFERS_T5090EA13155952525395C6141ACA51BDBE255E6B_H
#ifndef MEMORYCHUNK_T2B4F05956C1526847FF22066DC4E91A9A297951F_H
#define MEMORYCHUNK_T2B4F05956C1526847FF22066DC4E91A9A297951F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ScatterGatherBuffers/MemoryChunk
struct  MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F  : public RuntimeObject
{
public:
	// System.Byte[] System.Net.ScatterGatherBuffers/MemoryChunk::Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Buffer_0;
	// System.Int32 System.Net.ScatterGatherBuffers/MemoryChunk::FreeOffset
	int32_t ___FreeOffset_1;
	// System.Net.ScatterGatherBuffers/MemoryChunk System.Net.ScatterGatherBuffers/MemoryChunk::Next
	MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * ___Next_2;

public:
	inline static int32_t get_offset_of_Buffer_0() { return static_cast<int32_t>(offsetof(MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F, ___Buffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Buffer_0() const { return ___Buffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Buffer_0() { return &___Buffer_0; }
	inline void set_Buffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_0), value);
	}

	inline static int32_t get_offset_of_FreeOffset_1() { return static_cast<int32_t>(offsetof(MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F, ___FreeOffset_1)); }
	inline int32_t get_FreeOffset_1() const { return ___FreeOffset_1; }
	inline int32_t* get_address_of_FreeOffset_1() { return &___FreeOffset_1; }
	inline void set_FreeOffset_1(int32_t value)
	{
		___FreeOffset_1 = value;
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F, ___Next_2)); }
	inline MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * get_Next_2() const { return ___Next_2; }
	inline MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier((&___Next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYCHUNK_T2B4F05956C1526847FF22066DC4E91A9A297951F_H
#ifndef SERVERCERTVALIDATIONCALLBACK_T431E949AECAE20901007813737F5B26311F5F9FB_H
#define SERVERCERTVALIDATIONCALLBACK_T431E949AECAE20901007813737F5B26311F5F9FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServerCertValidationCallback
struct  ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB  : public RuntimeObject
{
public:
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.ServerCertValidationCallback::m_ValidationCallback
	RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * ___m_ValidationCallback_0;
	// System.Threading.ExecutionContext System.Net.ServerCertValidationCallback::m_Context
	ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 * ___m_Context_1;

public:
	inline static int32_t get_offset_of_m_ValidationCallback_0() { return static_cast<int32_t>(offsetof(ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB, ___m_ValidationCallback_0)); }
	inline RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * get_m_ValidationCallback_0() const { return ___m_ValidationCallback_0; }
	inline RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E ** get_address_of_m_ValidationCallback_0() { return &___m_ValidationCallback_0; }
	inline void set_m_ValidationCallback_0(RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * value)
	{
		___m_ValidationCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidationCallback_0), value);
	}

	inline static int32_t get_offset_of_m_Context_1() { return static_cast<int32_t>(offsetof(ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB, ___m_Context_1)); }
	inline ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 * get_m_Context_1() const { return ___m_Context_1; }
	inline ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 ** get_address_of_m_Context_1() { return &___m_Context_1; }
	inline void set_m_Context_1(ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 * value)
	{
		___m_Context_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Context_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERCERTVALIDATIONCALLBACK_T431E949AECAE20901007813737F5B26311F5F9FB_H
#ifndef SERVICENAMESTORE_TF45F4346CE113F34D3E08E515FD32642512CEA37_H
#define SERVICENAMESTORE_TF45F4346CE113F34D3E08E515FD32642512CEA37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServiceNameStore
struct  ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> System.Net.ServiceNameStore::serviceNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___serviceNames_0;
	// System.Security.Authentication.ExtendedProtection.ServiceNameCollection System.Net.ServiceNameStore::serviceNameCollection
	ServiceNameCollection_t3C60F59BA95A4A706BB70439AE03C07D09796C2C * ___serviceNameCollection_1;

public:
	inline static int32_t get_offset_of_serviceNames_0() { return static_cast<int32_t>(offsetof(ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37, ___serviceNames_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_serviceNames_0() const { return ___serviceNames_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_serviceNames_0() { return &___serviceNames_0; }
	inline void set_serviceNames_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___serviceNames_0 = value;
		Il2CppCodeGenWriteBarrier((&___serviceNames_0), value);
	}

	inline static int32_t get_offset_of_serviceNameCollection_1() { return static_cast<int32_t>(offsetof(ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37, ___serviceNameCollection_1)); }
	inline ServiceNameCollection_t3C60F59BA95A4A706BB70439AE03C07D09796C2C * get_serviceNameCollection_1() const { return ___serviceNameCollection_1; }
	inline ServiceNameCollection_t3C60F59BA95A4A706BB70439AE03C07D09796C2C ** get_address_of_serviceNameCollection_1() { return &___serviceNameCollection_1; }
	inline void set_serviceNameCollection_1(ServiceNameCollection_t3C60F59BA95A4A706BB70439AE03C07D09796C2C * value)
	{
		___serviceNameCollection_1 = value;
		Il2CppCodeGenWriteBarrier((&___serviceNameCollection_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICENAMESTORE_TF45F4346CE113F34D3E08E515FD32642512CEA37_H
#ifndef SOCKETADDRESS_TFD1A629405590229D8DAA15D03083147B767C969_H
#define SOCKETADDRESS_TFD1A629405590229D8DAA15D03083147B767C969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SocketAddress
struct  SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969  : public RuntimeObject
{
public:
	// System.Int32 System.Net.SocketAddress::m_Size
	int32_t ___m_Size_0;
	// System.Byte[] System.Net.SocketAddress::m_Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_Buffer_1;
	// System.Boolean System.Net.SocketAddress::m_changed
	bool ___m_changed_2;
	// System.Int32 System.Net.SocketAddress::m_hash
	int32_t ___m_hash_3;

public:
	inline static int32_t get_offset_of_m_Size_0() { return static_cast<int32_t>(offsetof(SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969, ___m_Size_0)); }
	inline int32_t get_m_Size_0() const { return ___m_Size_0; }
	inline int32_t* get_address_of_m_Size_0() { return &___m_Size_0; }
	inline void set_m_Size_0(int32_t value)
	{
		___m_Size_0 = value;
	}

	inline static int32_t get_offset_of_m_Buffer_1() { return static_cast<int32_t>(offsetof(SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969, ___m_Buffer_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_Buffer_1() const { return ___m_Buffer_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_Buffer_1() { return &___m_Buffer_1; }
	inline void set_m_Buffer_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_Buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Buffer_1), value);
	}

	inline static int32_t get_offset_of_m_changed_2() { return static_cast<int32_t>(offsetof(SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969, ___m_changed_2)); }
	inline bool get_m_changed_2() const { return ___m_changed_2; }
	inline bool* get_address_of_m_changed_2() { return &___m_changed_2; }
	inline void set_m_changed_2(bool value)
	{
		___m_changed_2 = value;
	}

	inline static int32_t get_offset_of_m_hash_3() { return static_cast<int32_t>(offsetof(SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969, ___m_hash_3)); }
	inline int32_t get_m_hash_3() const { return ___m_hash_3; }
	inline int32_t* get_address_of_m_hash_3() { return &___m_hash_3; }
	inline void set_m_hash_3(int32_t value)
	{
		___m_hash_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETADDRESS_TFD1A629405590229D8DAA15D03083147B767C969_H
#ifndef TIMERTHREAD_T834F44C51FF3F25350F8B4E03670F941F352DF01_H
#define TIMERTHREAD_T834F44C51FF3F25350F8B4E03670F941F352DF01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread
struct  TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01  : public RuntimeObject
{
public:

public:
};

struct TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields
{
public:
	// System.Collections.Generic.LinkedList`1<System.WeakReference> System.Net.TimerThread::s_Queues
	LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 * ___s_Queues_0;
	// System.Collections.Generic.LinkedList`1<System.WeakReference> System.Net.TimerThread::s_NewQueues
	LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 * ___s_NewQueues_1;
	// System.Int32 System.Net.TimerThread::s_ThreadState
	int32_t ___s_ThreadState_2;
	// System.Threading.AutoResetEvent System.Net.TimerThread::s_ThreadReadyEvent
	AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 * ___s_ThreadReadyEvent_3;
	// System.Threading.ManualResetEvent System.Net.TimerThread::s_ThreadShutdownEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___s_ThreadShutdownEvent_4;
	// System.Threading.WaitHandle[] System.Net.TimerThread::s_ThreadEvents
	WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* ___s_ThreadEvents_5;
	// System.Collections.Hashtable System.Net.TimerThread::s_QueuesCache
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___s_QueuesCache_6;

public:
	inline static int32_t get_offset_of_s_Queues_0() { return static_cast<int32_t>(offsetof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields, ___s_Queues_0)); }
	inline LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 * get_s_Queues_0() const { return ___s_Queues_0; }
	inline LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 ** get_address_of_s_Queues_0() { return &___s_Queues_0; }
	inline void set_s_Queues_0(LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 * value)
	{
		___s_Queues_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Queues_0), value);
	}

	inline static int32_t get_offset_of_s_NewQueues_1() { return static_cast<int32_t>(offsetof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields, ___s_NewQueues_1)); }
	inline LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 * get_s_NewQueues_1() const { return ___s_NewQueues_1; }
	inline LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 ** get_address_of_s_NewQueues_1() { return &___s_NewQueues_1; }
	inline void set_s_NewQueues_1(LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 * value)
	{
		___s_NewQueues_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_NewQueues_1), value);
	}

	inline static int32_t get_offset_of_s_ThreadState_2() { return static_cast<int32_t>(offsetof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields, ___s_ThreadState_2)); }
	inline int32_t get_s_ThreadState_2() const { return ___s_ThreadState_2; }
	inline int32_t* get_address_of_s_ThreadState_2() { return &___s_ThreadState_2; }
	inline void set_s_ThreadState_2(int32_t value)
	{
		___s_ThreadState_2 = value;
	}

	inline static int32_t get_offset_of_s_ThreadReadyEvent_3() { return static_cast<int32_t>(offsetof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields, ___s_ThreadReadyEvent_3)); }
	inline AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 * get_s_ThreadReadyEvent_3() const { return ___s_ThreadReadyEvent_3; }
	inline AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 ** get_address_of_s_ThreadReadyEvent_3() { return &___s_ThreadReadyEvent_3; }
	inline void set_s_ThreadReadyEvent_3(AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 * value)
	{
		___s_ThreadReadyEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_ThreadReadyEvent_3), value);
	}

	inline static int32_t get_offset_of_s_ThreadShutdownEvent_4() { return static_cast<int32_t>(offsetof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields, ___s_ThreadShutdownEvent_4)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_s_ThreadShutdownEvent_4() const { return ___s_ThreadShutdownEvent_4; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_s_ThreadShutdownEvent_4() { return &___s_ThreadShutdownEvent_4; }
	inline void set_s_ThreadShutdownEvent_4(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___s_ThreadShutdownEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_ThreadShutdownEvent_4), value);
	}

	inline static int32_t get_offset_of_s_ThreadEvents_5() { return static_cast<int32_t>(offsetof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields, ___s_ThreadEvents_5)); }
	inline WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* get_s_ThreadEvents_5() const { return ___s_ThreadEvents_5; }
	inline WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC** get_address_of_s_ThreadEvents_5() { return &___s_ThreadEvents_5; }
	inline void set_s_ThreadEvents_5(WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* value)
	{
		___s_ThreadEvents_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_ThreadEvents_5), value);
	}

	inline static int32_t get_offset_of_s_QueuesCache_6() { return static_cast<int32_t>(offsetof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields, ___s_QueuesCache_6)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_s_QueuesCache_6() const { return ___s_QueuesCache_6; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_s_QueuesCache_6() { return &___s_QueuesCache_6; }
	inline void set_s_QueuesCache_6(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___s_QueuesCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_QueuesCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERTHREAD_T834F44C51FF3F25350F8B4E03670F941F352DF01_H
#ifndef QUEUE_TCCFF6A2FCF584216AEDA04A483FB808E2D493643_H
#define QUEUE_TCCFF6A2FCF584216AEDA04A483FB808E2D493643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread/Queue
struct  Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643  : public RuntimeObject
{
public:
	// System.Int32 System.Net.TimerThread/Queue::m_DurationMilliseconds
	int32_t ___m_DurationMilliseconds_0;

public:
	inline static int32_t get_offset_of_m_DurationMilliseconds_0() { return static_cast<int32_t>(offsetof(Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643, ___m_DurationMilliseconds_0)); }
	inline int32_t get_m_DurationMilliseconds_0() const { return ___m_DurationMilliseconds_0; }
	inline int32_t* get_address_of_m_DurationMilliseconds_0() { return &___m_DurationMilliseconds_0; }
	inline void set_m_DurationMilliseconds_0(int32_t value)
	{
		___m_DurationMilliseconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUE_TCCFF6A2FCF584216AEDA04A483FB808E2D493643_H
#ifndef TIMER_T3B21B1013E27B5DC9FED14EC0921A5F51230D46F_H
#define TIMER_T3B21B1013E27B5DC9FED14EC0921A5F51230D46F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread/Timer
struct  Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F  : public RuntimeObject
{
public:
	// System.Int32 System.Net.TimerThread/Timer::m_StartTimeMilliseconds
	int32_t ___m_StartTimeMilliseconds_0;
	// System.Int32 System.Net.TimerThread/Timer::m_DurationMilliseconds
	int32_t ___m_DurationMilliseconds_1;

public:
	inline static int32_t get_offset_of_m_StartTimeMilliseconds_0() { return static_cast<int32_t>(offsetof(Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F, ___m_StartTimeMilliseconds_0)); }
	inline int32_t get_m_StartTimeMilliseconds_0() const { return ___m_StartTimeMilliseconds_0; }
	inline int32_t* get_address_of_m_StartTimeMilliseconds_0() { return &___m_StartTimeMilliseconds_0; }
	inline void set_m_StartTimeMilliseconds_0(int32_t value)
	{
		___m_StartTimeMilliseconds_0 = value;
	}

	inline static int32_t get_offset_of_m_DurationMilliseconds_1() { return static_cast<int32_t>(offsetof(Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F, ___m_DurationMilliseconds_1)); }
	inline int32_t get_m_DurationMilliseconds_1() const { return ___m_DurationMilliseconds_1; }
	inline int32_t* get_address_of_m_DurationMilliseconds_1() { return &___m_DurationMilliseconds_1; }
	inline void set_m_DurationMilliseconds_1(int32_t value)
	{
		___m_DurationMilliseconds_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMER_T3B21B1013E27B5DC9FED14EC0921A5F51230D46F_H
#ifndef TRANSPORTCONTEXT_T1753CC7BFFA637B35BE353AAE4452C4371F89A9F_H
#define TRANSPORTCONTEXT_T1753CC7BFFA637B35BE353AAE4452C4371F89A9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TransportContext
struct  TransportContext_t1753CC7BFFA637B35BE353AAE4452C4371F89A9F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTCONTEXT_T1753CC7BFFA637B35BE353AAE4452C4371F89A9F_H
#ifndef UNSAFENCLNATIVEMETHODS_T6130A140E270DE50A48DAA13D71A71F5BD9F1537_H
#define UNSAFENCLNATIVEMETHODS_T6130A140E270DE50A48DAA13D71A71F5BD9F1537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UnsafeNclNativeMethods
struct  UnsafeNclNativeMethods_t6130A140E270DE50A48DAA13D71A71F5BD9F1537  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSAFENCLNATIVEMETHODS_T6130A140E270DE50A48DAA13D71A71F5BD9F1537_H
#ifndef HTTPAPI_TDD21E6C468ACB472A3D1CE51865417E65113B12F_H
#define HTTPAPI_TDD21E6C468ACB472A3D1CE51865417E65113B12F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UnsafeNclNativeMethods/HttpApi
struct  HttpApi_tDD21E6C468ACB472A3D1CE51865417E65113B12F  : public RuntimeObject
{
public:

public:
};

struct HttpApi_tDD21E6C468ACB472A3D1CE51865417E65113B12F_StaticFields
{
public:
	// System.String[] System.Net.UnsafeNclNativeMethods/HttpApi::m_Strings
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_Strings_0;

public:
	inline static int32_t get_offset_of_m_Strings_0() { return static_cast<int32_t>(offsetof(HttpApi_tDD21E6C468ACB472A3D1CE51865417E65113B12F_StaticFields, ___m_Strings_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_Strings_0() const { return ___m_Strings_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_Strings_0() { return &___m_Strings_0; }
	inline void set_m_Strings_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_Strings_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Strings_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPAPI_TDD21E6C468ACB472A3D1CE51865417E65113B12F_H
#ifndef HTTP_REQUEST_HEADER_ID_TB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE_H
#define HTTP_REQUEST_HEADER_ID_TB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UnsafeNclNativeMethods/HttpApi/HTTP_REQUEST_HEADER_ID
struct  HTTP_REQUEST_HEADER_ID_tB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE  : public RuntimeObject
{
public:

public:
};

struct HTTP_REQUEST_HEADER_ID_tB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE_StaticFields
{
public:
	// System.String[] System.Net.UnsafeNclNativeMethods/HttpApi/HTTP_REQUEST_HEADER_ID::m_Strings
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_Strings_0;

public:
	inline static int32_t get_offset_of_m_Strings_0() { return static_cast<int32_t>(offsetof(HTTP_REQUEST_HEADER_ID_tB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE_StaticFields, ___m_Strings_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_Strings_0() const { return ___m_Strings_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_Strings_0() { return &___m_Strings_0; }
	inline void set_m_Strings_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_Strings_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Strings_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP_REQUEST_HEADER_ID_TB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE_H
#ifndef SECURESTRINGHELPER_T9F5A5E822AB08545A97B612C217CC6C760FF78F5_H
#define SECURESTRINGHELPER_T9F5A5E822AB08545A97B612C217CC6C760FF78F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UnsafeNclNativeMethods/SecureStringHelper
struct  SecureStringHelper_t9F5A5E822AB08545A97B612C217CC6C760FF78F5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURESTRINGHELPER_T9F5A5E822AB08545A97B612C217CC6C760FF78F5_H
#ifndef DOWNLOADBITSSTATE_T294B3B4DB78D93D45120CA51692E089D96B9DB44_H
#define DOWNLOADBITSSTATE_T294B3B4DB78D93D45120CA51692E089D96B9DB44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient/DownloadBitsState
struct  DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44  : public RuntimeObject
{
public:
	// System.Net.WebClient System.Net.WebClient/DownloadBitsState::WebClient
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * ___WebClient_0;
	// System.IO.Stream System.Net.WebClient/DownloadBitsState::WriteStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___WriteStream_1;
	// System.Byte[] System.Net.WebClient/DownloadBitsState::InnerBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___InnerBuffer_2;
	// System.ComponentModel.AsyncOperation System.Net.WebClient/DownloadBitsState::AsyncOp
	AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 * ___AsyncOp_3;
	// System.Net.WebRequest System.Net.WebClient/DownloadBitsState::Request
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * ___Request_4;
	// System.Net.CompletionDelegate System.Net.WebClient/DownloadBitsState::CompletionDelegate
	CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 * ___CompletionDelegate_5;
	// System.IO.Stream System.Net.WebClient/DownloadBitsState::ReadStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___ReadStream_6;
	// System.Net.ScatterGatherBuffers System.Net.WebClient/DownloadBitsState::SgBuffers
	ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B * ___SgBuffers_7;
	// System.Int64 System.Net.WebClient/DownloadBitsState::ContentLength
	int64_t ___ContentLength_8;
	// System.Int64 System.Net.WebClient/DownloadBitsState::Length
	int64_t ___Length_9;
	// System.Net.WebClient/ProgressData System.Net.WebClient/DownloadBitsState::Progress
	ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 * ___Progress_10;

public:
	inline static int32_t get_offset_of_WebClient_0() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___WebClient_0)); }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * get_WebClient_0() const { return ___WebClient_0; }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A ** get_address_of_WebClient_0() { return &___WebClient_0; }
	inline void set_WebClient_0(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * value)
	{
		___WebClient_0 = value;
		Il2CppCodeGenWriteBarrier((&___WebClient_0), value);
	}

	inline static int32_t get_offset_of_WriteStream_1() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___WriteStream_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_WriteStream_1() const { return ___WriteStream_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_WriteStream_1() { return &___WriteStream_1; }
	inline void set_WriteStream_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___WriteStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___WriteStream_1), value);
	}

	inline static int32_t get_offset_of_InnerBuffer_2() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___InnerBuffer_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_InnerBuffer_2() const { return ___InnerBuffer_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_InnerBuffer_2() { return &___InnerBuffer_2; }
	inline void set_InnerBuffer_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___InnerBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___InnerBuffer_2), value);
	}

	inline static int32_t get_offset_of_AsyncOp_3() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___AsyncOp_3)); }
	inline AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 * get_AsyncOp_3() const { return ___AsyncOp_3; }
	inline AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 ** get_address_of_AsyncOp_3() { return &___AsyncOp_3; }
	inline void set_AsyncOp_3(AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 * value)
	{
		___AsyncOp_3 = value;
		Il2CppCodeGenWriteBarrier((&___AsyncOp_3), value);
	}

	inline static int32_t get_offset_of_Request_4() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___Request_4)); }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * get_Request_4() const { return ___Request_4; }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 ** get_address_of_Request_4() { return &___Request_4; }
	inline void set_Request_4(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * value)
	{
		___Request_4 = value;
		Il2CppCodeGenWriteBarrier((&___Request_4), value);
	}

	inline static int32_t get_offset_of_CompletionDelegate_5() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___CompletionDelegate_5)); }
	inline CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 * get_CompletionDelegate_5() const { return ___CompletionDelegate_5; }
	inline CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 ** get_address_of_CompletionDelegate_5() { return &___CompletionDelegate_5; }
	inline void set_CompletionDelegate_5(CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 * value)
	{
		___CompletionDelegate_5 = value;
		Il2CppCodeGenWriteBarrier((&___CompletionDelegate_5), value);
	}

	inline static int32_t get_offset_of_ReadStream_6() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___ReadStream_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_ReadStream_6() const { return ___ReadStream_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_ReadStream_6() { return &___ReadStream_6; }
	inline void set_ReadStream_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___ReadStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___ReadStream_6), value);
	}

	inline static int32_t get_offset_of_SgBuffers_7() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___SgBuffers_7)); }
	inline ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B * get_SgBuffers_7() const { return ___SgBuffers_7; }
	inline ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B ** get_address_of_SgBuffers_7() { return &___SgBuffers_7; }
	inline void set_SgBuffers_7(ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B * value)
	{
		___SgBuffers_7 = value;
		Il2CppCodeGenWriteBarrier((&___SgBuffers_7), value);
	}

	inline static int32_t get_offset_of_ContentLength_8() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___ContentLength_8)); }
	inline int64_t get_ContentLength_8() const { return ___ContentLength_8; }
	inline int64_t* get_address_of_ContentLength_8() { return &___ContentLength_8; }
	inline void set_ContentLength_8(int64_t value)
	{
		___ContentLength_8 = value;
	}

	inline static int32_t get_offset_of_Length_9() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___Length_9)); }
	inline int64_t get_Length_9() const { return ___Length_9; }
	inline int64_t* get_address_of_Length_9() { return &___Length_9; }
	inline void set_Length_9(int64_t value)
	{
		___Length_9 = value;
	}

	inline static int32_t get_offset_of_Progress_10() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___Progress_10)); }
	inline ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 * get_Progress_10() const { return ___Progress_10; }
	inline ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 ** get_address_of_Progress_10() { return &___Progress_10; }
	inline void set_Progress_10(ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 * value)
	{
		___Progress_10 = value;
		Il2CppCodeGenWriteBarrier((&___Progress_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADBITSSTATE_T294B3B4DB78D93D45120CA51692E089D96B9DB44_H
#ifndef PROGRESSDATA_TE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20_H
#define PROGRESSDATA_TE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient/ProgressData
struct  ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20  : public RuntimeObject
{
public:
	// System.Int64 System.Net.WebClient/ProgressData::BytesSent
	int64_t ___BytesSent_0;
	// System.Int64 System.Net.WebClient/ProgressData::TotalBytesToSend
	int64_t ___TotalBytesToSend_1;
	// System.Int64 System.Net.WebClient/ProgressData::BytesReceived
	int64_t ___BytesReceived_2;
	// System.Int64 System.Net.WebClient/ProgressData::TotalBytesToReceive
	int64_t ___TotalBytesToReceive_3;
	// System.Boolean System.Net.WebClient/ProgressData::HasUploadPhase
	bool ___HasUploadPhase_4;

public:
	inline static int32_t get_offset_of_BytesSent_0() { return static_cast<int32_t>(offsetof(ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20, ___BytesSent_0)); }
	inline int64_t get_BytesSent_0() const { return ___BytesSent_0; }
	inline int64_t* get_address_of_BytesSent_0() { return &___BytesSent_0; }
	inline void set_BytesSent_0(int64_t value)
	{
		___BytesSent_0 = value;
	}

	inline static int32_t get_offset_of_TotalBytesToSend_1() { return static_cast<int32_t>(offsetof(ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20, ___TotalBytesToSend_1)); }
	inline int64_t get_TotalBytesToSend_1() const { return ___TotalBytesToSend_1; }
	inline int64_t* get_address_of_TotalBytesToSend_1() { return &___TotalBytesToSend_1; }
	inline void set_TotalBytesToSend_1(int64_t value)
	{
		___TotalBytesToSend_1 = value;
	}

	inline static int32_t get_offset_of_BytesReceived_2() { return static_cast<int32_t>(offsetof(ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20, ___BytesReceived_2)); }
	inline int64_t get_BytesReceived_2() const { return ___BytesReceived_2; }
	inline int64_t* get_address_of_BytesReceived_2() { return &___BytesReceived_2; }
	inline void set_BytesReceived_2(int64_t value)
	{
		___BytesReceived_2 = value;
	}

	inline static int32_t get_offset_of_TotalBytesToReceive_3() { return static_cast<int32_t>(offsetof(ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20, ___TotalBytesToReceive_3)); }
	inline int64_t get_TotalBytesToReceive_3() const { return ___TotalBytesToReceive_3; }
	inline int64_t* get_address_of_TotalBytesToReceive_3() { return &___TotalBytesToReceive_3; }
	inline void set_TotalBytesToReceive_3(int64_t value)
	{
		___TotalBytesToReceive_3 = value;
	}

	inline static int32_t get_offset_of_HasUploadPhase_4() { return static_cast<int32_t>(offsetof(ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20, ___HasUploadPhase_4)); }
	inline bool get_HasUploadPhase_4() const { return ___HasUploadPhase_4; }
	inline bool* get_address_of_HasUploadPhase_4() { return &___HasUploadPhase_4; }
	inline void set_HasUploadPhase_4(bool value)
	{
		___HasUploadPhase_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSDATA_TE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20_H
#ifndef WEBEXCEPTIONMAPPING_T4E7EF581D0224FFC2F2C8556EF320557517A378A_H
#define WEBEXCEPTIONMAPPING_T4E7EF581D0224FFC2F2C8556EF320557517A378A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionMapping
struct  WebExceptionMapping_t4E7EF581D0224FFC2F2C8556EF320557517A378A  : public RuntimeObject
{
public:

public:
};

struct WebExceptionMapping_t4E7EF581D0224FFC2F2C8556EF320557517A378A_StaticFields
{
public:
	// System.String[] System.Net.WebExceptionMapping::s_Mapping
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___s_Mapping_0;

public:
	inline static int32_t get_offset_of_s_Mapping_0() { return static_cast<int32_t>(offsetof(WebExceptionMapping_t4E7EF581D0224FFC2F2C8556EF320557517A378A_StaticFields, ___s_Mapping_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_s_Mapping_0() const { return ___s_Mapping_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_s_Mapping_0() { return &___s_Mapping_0; }
	inline void set_s_Mapping_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___s_Mapping_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mapping_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONMAPPING_T4E7EF581D0224FFC2F2C8556EF320557517A378A_H
#ifndef WEBPROXY_T075305900B1D4D8BC8FAB08852842E42BC000417_H
#define WEBPROXY_T075305900B1D4D8BC8FAB08852842E42BC000417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebProxy
struct  WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417  : public RuntimeObject
{
public:
	// System.Boolean System.Net.WebProxy::_UseRegistry
	bool ____UseRegistry_0;
	// System.Boolean System.Net.WebProxy::_BypassOnLocal
	bool ____BypassOnLocal_1;
	// System.Boolean System.Net.WebProxy::m_EnableAutoproxy
	bool ___m_EnableAutoproxy_2;
	// System.Uri System.Net.WebProxy::_ProxyAddress
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ____ProxyAddress_3;
	// System.Collections.ArrayList System.Net.WebProxy::_BypassList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____BypassList_4;
	// System.Net.ICredentials System.Net.WebProxy::_Credentials
	RuntimeObject* ____Credentials_5;
	// System.Text.RegularExpressions.Regex[] System.Net.WebProxy::_RegExBypassList
	RegexU5BU5D_t9CA70F985DE1C94823B06BD0B2FCCC97927E6C53* ____RegExBypassList_6;
	// System.Collections.Hashtable System.Net.WebProxy::_ProxyHostAddresses
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____ProxyHostAddresses_7;
	// System.Net.AutoWebProxyScriptEngine System.Net.WebProxy::m_ScriptEngine
	AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455 * ___m_ScriptEngine_8;

public:
	inline static int32_t get_offset_of__UseRegistry_0() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ____UseRegistry_0)); }
	inline bool get__UseRegistry_0() const { return ____UseRegistry_0; }
	inline bool* get_address_of__UseRegistry_0() { return &____UseRegistry_0; }
	inline void set__UseRegistry_0(bool value)
	{
		____UseRegistry_0 = value;
	}

	inline static int32_t get_offset_of__BypassOnLocal_1() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ____BypassOnLocal_1)); }
	inline bool get__BypassOnLocal_1() const { return ____BypassOnLocal_1; }
	inline bool* get_address_of__BypassOnLocal_1() { return &____BypassOnLocal_1; }
	inline void set__BypassOnLocal_1(bool value)
	{
		____BypassOnLocal_1 = value;
	}

	inline static int32_t get_offset_of_m_EnableAutoproxy_2() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ___m_EnableAutoproxy_2)); }
	inline bool get_m_EnableAutoproxy_2() const { return ___m_EnableAutoproxy_2; }
	inline bool* get_address_of_m_EnableAutoproxy_2() { return &___m_EnableAutoproxy_2; }
	inline void set_m_EnableAutoproxy_2(bool value)
	{
		___m_EnableAutoproxy_2 = value;
	}

	inline static int32_t get_offset_of__ProxyAddress_3() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ____ProxyAddress_3)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get__ProxyAddress_3() const { return ____ProxyAddress_3; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of__ProxyAddress_3() { return &____ProxyAddress_3; }
	inline void set__ProxyAddress_3(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		____ProxyAddress_3 = value;
		Il2CppCodeGenWriteBarrier((&____ProxyAddress_3), value);
	}

	inline static int32_t get_offset_of__BypassList_4() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ____BypassList_4)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__BypassList_4() const { return ____BypassList_4; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__BypassList_4() { return &____BypassList_4; }
	inline void set__BypassList_4(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____BypassList_4 = value;
		Il2CppCodeGenWriteBarrier((&____BypassList_4), value);
	}

	inline static int32_t get_offset_of__Credentials_5() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ____Credentials_5)); }
	inline RuntimeObject* get__Credentials_5() const { return ____Credentials_5; }
	inline RuntimeObject** get_address_of__Credentials_5() { return &____Credentials_5; }
	inline void set__Credentials_5(RuntimeObject* value)
	{
		____Credentials_5 = value;
		Il2CppCodeGenWriteBarrier((&____Credentials_5), value);
	}

	inline static int32_t get_offset_of__RegExBypassList_6() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ____RegExBypassList_6)); }
	inline RegexU5BU5D_t9CA70F985DE1C94823B06BD0B2FCCC97927E6C53* get__RegExBypassList_6() const { return ____RegExBypassList_6; }
	inline RegexU5BU5D_t9CA70F985DE1C94823B06BD0B2FCCC97927E6C53** get_address_of__RegExBypassList_6() { return &____RegExBypassList_6; }
	inline void set__RegExBypassList_6(RegexU5BU5D_t9CA70F985DE1C94823B06BD0B2FCCC97927E6C53* value)
	{
		____RegExBypassList_6 = value;
		Il2CppCodeGenWriteBarrier((&____RegExBypassList_6), value);
	}

	inline static int32_t get_offset_of__ProxyHostAddresses_7() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ____ProxyHostAddresses_7)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__ProxyHostAddresses_7() const { return ____ProxyHostAddresses_7; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__ProxyHostAddresses_7() { return &____ProxyHostAddresses_7; }
	inline void set__ProxyHostAddresses_7(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____ProxyHostAddresses_7 = value;
		Il2CppCodeGenWriteBarrier((&____ProxyHostAddresses_7), value);
	}

	inline static int32_t get_offset_of_m_ScriptEngine_8() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ___m_ScriptEngine_8)); }
	inline AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455 * get_m_ScriptEngine_8() const { return ___m_ScriptEngine_8; }
	inline AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455 ** get_address_of_m_ScriptEngine_8() { return &___m_ScriptEngine_8; }
	inline void set_m_ScriptEngine_8(AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455 * value)
	{
		___m_ScriptEngine_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScriptEngine_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPROXY_T075305900B1D4D8BC8FAB08852842E42BC000417_H
#ifndef U3CU3EC__DISPLAYCLASS78_0_TC7AC5DA365F8D5A75E48F83C5D127EF753913861_H
#define U3CU3EC__DISPLAYCLASS78_0_TC7AC5DA365F8D5A75E48F83C5D127EF753913861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest/<>c__DisplayClass78_0
struct  U3CU3Ec__DisplayClass78_0_tC7AC5DA365F8D5A75E48F83C5D127EF753913861  : public RuntimeObject
{
public:
	// System.Security.Principal.WindowsIdentity System.Net.WebRequest/<>c__DisplayClass78_0::currentUser
	WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 * ___currentUser_0;
	// System.Net.WebRequest System.Net.WebRequest/<>c__DisplayClass78_0::<>4__this
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_currentUser_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass78_0_tC7AC5DA365F8D5A75E48F83C5D127EF753913861, ___currentUser_0)); }
	inline WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 * get_currentUser_0() const { return ___currentUser_0; }
	inline WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 ** get_address_of_currentUser_0() { return &___currentUser_0; }
	inline void set_currentUser_0(WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 * value)
	{
		___currentUser_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentUser_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass78_0_tC7AC5DA365F8D5A75E48F83C5D127EF753913861, ___U3CU3E4__this_1)); }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS78_0_TC7AC5DA365F8D5A75E48F83C5D127EF753913861_H
#ifndef U3CU3EC__DISPLAYCLASS79_0_T4CA87BE9AA29F9315C6AD70F8C79A62C7727013F_H
#define U3CU3EC__DISPLAYCLASS79_0_T4CA87BE9AA29F9315C6AD70F8C79A62C7727013F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest/<>c__DisplayClass79_0
struct  U3CU3Ec__DisplayClass79_0_t4CA87BE9AA29F9315C6AD70F8C79A62C7727013F  : public RuntimeObject
{
public:
	// System.Security.Principal.WindowsIdentity System.Net.WebRequest/<>c__DisplayClass79_0::currentUser
	WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 * ___currentUser_0;
	// System.Net.WebRequest System.Net.WebRequest/<>c__DisplayClass79_0::<>4__this
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_currentUser_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass79_0_t4CA87BE9AA29F9315C6AD70F8C79A62C7727013F, ___currentUser_0)); }
	inline WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 * get_currentUser_0() const { return ___currentUser_0; }
	inline WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 ** get_address_of_currentUser_0() { return &___currentUser_0; }
	inline void set_currentUser_0(WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 * value)
	{
		___currentUser_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentUser_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass79_0_t4CA87BE9AA29F9315C6AD70F8C79A62C7727013F, ___U3CU3E4__this_1)); }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS79_0_T4CA87BE9AA29F9315C6AD70F8C79A62C7727013F_H
#ifndef DESIGNERWEBREQUESTCREATE_T613DD91D4F07703DC65E847B367F4DCD5710E2A3_H
#define DESIGNERWEBREQUESTCREATE_T613DD91D4F07703DC65E847B367F4DCD5710E2A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest/DesignerWebRequestCreate
struct  DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESIGNERWEBREQUESTCREATE_T613DD91D4F07703DC65E847B367F4DCD5710E2A3_H
#ifndef WEBPROXYWRAPPEROPAQUE_T6CC216364481C2A8254832AA0897F770BB494A62_H
#define WEBPROXYWRAPPEROPAQUE_T6CC216364481C2A8254832AA0897F770BB494A62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest/WebProxyWrapperOpaque
struct  WebProxyWrapperOpaque_t6CC216364481C2A8254832AA0897F770BB494A62  : public RuntimeObject
{
public:
	// System.Net.WebProxy System.Net.WebRequest/WebProxyWrapperOpaque::webProxy
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417 * ___webProxy_0;

public:
	inline static int32_t get_offset_of_webProxy_0() { return static_cast<int32_t>(offsetof(WebProxyWrapperOpaque_t6CC216364481C2A8254832AA0897F770BB494A62, ___webProxy_0)); }
	inline WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417 * get_webProxy_0() const { return ___webProxy_0; }
	inline WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417 ** get_address_of_webProxy_0() { return &___webProxy_0; }
	inline void set_webProxy_0(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417 * value)
	{
		___webProxy_0 = value;
		Il2CppCodeGenWriteBarrier((&___webProxy_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPROXYWRAPPEROPAQUE_T6CC216364481C2A8254832AA0897F770BB494A62_H
#ifndef URLDECODER_T9C62102EA32FB43498BED35B0526E09C666C83FC_H
#define URLDECODER_T9C62102EA32FB43498BED35B0526E09C666C83FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebUtility/UrlDecoder
struct  UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC  : public RuntimeObject
{
public:
	// System.Int32 System.Net.WebUtility/UrlDecoder::_bufferSize
	int32_t ____bufferSize_0;
	// System.Int32 System.Net.WebUtility/UrlDecoder::_numChars
	int32_t ____numChars_1;
	// System.Char[] System.Net.WebUtility/UrlDecoder::_charBuffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____charBuffer_2;
	// System.Int32 System.Net.WebUtility/UrlDecoder::_numBytes
	int32_t ____numBytes_3;
	// System.Byte[] System.Net.WebUtility/UrlDecoder::_byteBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____byteBuffer_4;
	// System.Text.Encoding System.Net.WebUtility/UrlDecoder::_encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ____encoding_5;

public:
	inline static int32_t get_offset_of__bufferSize_0() { return static_cast<int32_t>(offsetof(UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC, ____bufferSize_0)); }
	inline int32_t get__bufferSize_0() const { return ____bufferSize_0; }
	inline int32_t* get_address_of__bufferSize_0() { return &____bufferSize_0; }
	inline void set__bufferSize_0(int32_t value)
	{
		____bufferSize_0 = value;
	}

	inline static int32_t get_offset_of__numChars_1() { return static_cast<int32_t>(offsetof(UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC, ____numChars_1)); }
	inline int32_t get__numChars_1() const { return ____numChars_1; }
	inline int32_t* get_address_of__numChars_1() { return &____numChars_1; }
	inline void set__numChars_1(int32_t value)
	{
		____numChars_1 = value;
	}

	inline static int32_t get_offset_of__charBuffer_2() { return static_cast<int32_t>(offsetof(UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC, ____charBuffer_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__charBuffer_2() const { return ____charBuffer_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__charBuffer_2() { return &____charBuffer_2; }
	inline void set__charBuffer_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____charBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&____charBuffer_2), value);
	}

	inline static int32_t get_offset_of__numBytes_3() { return static_cast<int32_t>(offsetof(UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC, ____numBytes_3)); }
	inline int32_t get__numBytes_3() const { return ____numBytes_3; }
	inline int32_t* get_address_of__numBytes_3() { return &____numBytes_3; }
	inline void set__numBytes_3(int32_t value)
	{
		____numBytes_3 = value;
	}

	inline static int32_t get_offset_of__byteBuffer_4() { return static_cast<int32_t>(offsetof(UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC, ____byteBuffer_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__byteBuffer_4() const { return ____byteBuffer_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__byteBuffer_4() { return &____byteBuffer_4; }
	inline void set__byteBuffer_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____byteBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&____byteBuffer_4), value);
	}

	inline static int32_t get_offset_of__encoding_5() { return static_cast<int32_t>(offsetof(UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC, ____encoding_5)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get__encoding_5() const { return ____encoding_5; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of__encoding_5() { return &____encoding_5; }
	inline void set__encoding_5(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		____encoding_5 = value;
		Il2CppCodeGenWriteBarrier((&____encoding_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URLDECODER_T9C62102EA32FB43498BED35B0526E09C666C83FC_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef NAMEVALUECOLLECTION_T7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1_H
#define NAMEVALUECOLLECTION_T7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameValueCollection
struct  NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1  : public NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D
{
public:
	// System.String[] System.Collections.Specialized.NameValueCollection::_all
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____all_9;
	// System.String[] System.Collections.Specialized.NameValueCollection::_allKeys
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____allKeys_10;

public:
	inline static int32_t get_offset_of__all_9() { return static_cast<int32_t>(offsetof(NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1, ____all_9)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__all_9() const { return ____all_9; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__all_9() { return &____all_9; }
	inline void set__all_9(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____all_9 = value;
		Il2CppCodeGenWriteBarrier((&____all_9), value);
	}

	inline static int32_t get_offset_of__allKeys_10() { return static_cast<int32_t>(offsetof(NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1, ____allKeys_10)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__allKeys_10() const { return ____allKeys_10; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__allKeys_10() { return &____allKeys_10; }
	inline void set__allKeys_10(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____allKeys_10 = value;
		Il2CppCodeGenWriteBarrier((&____allKeys_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUECOLLECTION_T7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1_H
#ifndef COMPONENT_T7AEFE153F6778CF52E1981BC3E811A9604B29473_H
#define COMPONENT_T7AEFE153F6778CF52E1981BC3E811A9604B29473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Component
struct  Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.ComponentModel.ISite System.ComponentModel.Component::site
	RuntimeObject* ___site_2;
	// System.ComponentModel.EventHandlerList System.ComponentModel.Component::events
	EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * ___events_3;

public:
	inline static int32_t get_offset_of_site_2() { return static_cast<int32_t>(offsetof(Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473, ___site_2)); }
	inline RuntimeObject* get_site_2() const { return ___site_2; }
	inline RuntimeObject** get_address_of_site_2() { return &___site_2; }
	inline void set_site_2(RuntimeObject* value)
	{
		___site_2 = value;
		Il2CppCodeGenWriteBarrier((&___site_2), value);
	}

	inline static int32_t get_offset_of_events_3() { return static_cast<int32_t>(offsetof(Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473, ___events_3)); }
	inline EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * get_events_3() const { return ___events_3; }
	inline EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 ** get_address_of_events_3() { return &___events_3; }
	inline void set_events_3(EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * value)
	{
		___events_3 = value;
		Il2CppCodeGenWriteBarrier((&___events_3), value);
	}
};

struct Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473_StaticFields
{
public:
	// System.Object System.ComponentModel.Component::EventDisposed
	RuntimeObject * ___EventDisposed_1;

public:
	inline static int32_t get_offset_of_EventDisposed_1() { return static_cast<int32_t>(offsetof(Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473_StaticFields, ___EventDisposed_1)); }
	inline RuntimeObject * get_EventDisposed_1() const { return ___EventDisposed_1; }
	inline RuntimeObject ** get_address_of_EventDisposed_1() { return &___EventDisposed_1; }
	inline void set_EventDisposed_1(RuntimeObject * value)
	{
		___EventDisposed_1 = value;
		Il2CppCodeGenWriteBarrier((&___EventDisposed_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T7AEFE153F6778CF52E1981BC3E811A9604B29473_H
#ifndef PROGRESSCHANGEDEVENTARGS_TC79597AB8E4151EDFF627F615FE36B8D91811F3F_H
#define PROGRESSCHANGEDEVENTARGS_TC79597AB8E4151EDFF627F615FE36B8D91811F3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ProgressChangedEventArgs
struct  ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Int32 System.ComponentModel.ProgressChangedEventArgs::progressPercentage
	int32_t ___progressPercentage_1;
	// System.Object System.ComponentModel.ProgressChangedEventArgs::userState
	RuntimeObject * ___userState_2;

public:
	inline static int32_t get_offset_of_progressPercentage_1() { return static_cast<int32_t>(offsetof(ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F, ___progressPercentage_1)); }
	inline int32_t get_progressPercentage_1() const { return ___progressPercentage_1; }
	inline int32_t* get_address_of_progressPercentage_1() { return &___progressPercentage_1; }
	inline void set_progressPercentage_1(int32_t value)
	{
		___progressPercentage_1 = value;
	}

	inline static int32_t get_offset_of_userState_2() { return static_cast<int32_t>(offsetof(ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F, ___userState_2)); }
	inline RuntimeObject * get_userState_2() const { return ___userState_2; }
	inline RuntimeObject ** get_address_of_userState_2() { return &___userState_2; }
	inline void set_userState_2(RuntimeObject * value)
	{
		___userState_2 = value;
		Il2CppCodeGenWriteBarrier((&___userState_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSCHANGEDEVENTARGS_TC79597AB8E4151EDFF627F615FE36B8D91811F3F_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_2), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_3), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INFINITETIMERQUEUE_T141BA98635EDB34E2BAAFE8BA5C91795E7CCAB51_H
#define INFINITETIMERQUEUE_T141BA98635EDB34E2BAAFE8BA5C91795E7CCAB51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread/InfiniteTimerQueue
struct  InfiniteTimerQueue_t141BA98635EDB34E2BAAFE8BA5C91795E7CCAB51  : public Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFINITETIMERQUEUE_T141BA98635EDB34E2BAAFE8BA5C91795E7CCAB51_H
#ifndef TIMERQUEUE_T8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C_H
#define TIMERQUEUE_T8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread/TimerQueue
struct  TimerQueue_t8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C  : public Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643
{
public:
	// System.Net.TimerThread/TimerNode System.Net.TimerThread/TimerQueue::m_Timers
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * ___m_Timers_1;

public:
	inline static int32_t get_offset_of_m_Timers_1() { return static_cast<int32_t>(offsetof(TimerQueue_t8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C, ___m_Timers_1)); }
	inline TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * get_m_Timers_1() const { return ___m_Timers_1; }
	inline TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B ** get_address_of_m_Timers_1() { return &___m_Timers_1; }
	inline void set_m_Timers_1(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * value)
	{
		___m_Timers_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Timers_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERQUEUE_T8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C_H
#ifndef WEBPROXYWRAPPER_T47B30DCD77853C5079F4944A6FCA329026D84E3B_H
#define WEBPROXYWRAPPER_T47B30DCD77853C5079F4944A6FCA329026D84E3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest/WebProxyWrapper
struct  WebProxyWrapper_t47B30DCD77853C5079F4944A6FCA329026D84E3B  : public WebProxyWrapperOpaque_t6CC216364481C2A8254832AA0897F770BB494A62
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPROXYWRAPPER_T47B30DCD77853C5079F4944A6FCA329026D84E3B_H
#ifndef WEBRESPONSE_T5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD_H
#define WEBRESPONSE_T5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebResponse
struct  WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBRESPONSE_T5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef FORMATEXCEPTION_T2808E076CDE4650AF89F55FD78F49290D0EC5BDC_H
#define FORMATEXCEPTION_T2808E076CDE4650AF89F55FD78F49290D0EC5BDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FormatException
struct  FormatException_t2808E076CDE4650AF89F55FD78F49290D0EC5BDC  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T2808E076CDE4650AF89F55FD78F49290D0EC5BDC_H
#ifndef FILEACCESS_T31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6_H
#define FILEACCESS_T31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAccess
struct  FileAccess_t31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6 
{
public:
	// System.Int32 System.IO.FileAccess::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAccess_t31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEACCESS_T31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6_H
#ifndef INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#define INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#ifndef CLOSEEXSTATE_T7AD30E3EACEBBAF7661B1AC45F7BC018DA33E429_H
#define CLOSEEXSTATE_T7AD30E3EACEBBAF7661B1AC45F7BC018DA33E429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CloseExState
struct  CloseExState_t7AD30E3EACEBBAF7661B1AC45F7BC018DA33E429 
{
public:
	// System.Int32 System.Net.CloseExState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CloseExState_t7AD30E3EACEBBAF7661B1AC45F7BC018DA33E429, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSEEXSTATE_T7AD30E3EACEBBAF7661B1AC45F7BC018DA33E429_H
#ifndef UNICODEDECODINGCONFORMANCE_T9467A928E4A46098930CA0A67315E0159230771E_H
#define UNICODEDECODINGCONFORMANCE_T9467A928E4A46098930CA0A67315E0159230771E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.UnicodeDecodingConformance
struct  UnicodeDecodingConformance_t9467A928E4A46098930CA0A67315E0159230771E 
{
public:
	// System.Int32 System.Net.Configuration.UnicodeDecodingConformance::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnicodeDecodingConformance_t9467A928E4A46098930CA0A67315E0159230771E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNICODEDECODINGCONFORMANCE_T9467A928E4A46098930CA0A67315E0159230771E_H
#ifndef UNICODEENCODINGCONFORMANCE_TB184A12AA9972C115D899779A92DCB82719B487A_H
#define UNICODEENCODINGCONFORMANCE_TB184A12AA9972C115D899779A92DCB82719B487A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.UnicodeEncodingConformance
struct  UnicodeEncodingConformance_tB184A12AA9972C115D899779A92DCB82719B487A 
{
public:
	// System.Int32 System.Net.Configuration.UnicodeEncodingConformance::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnicodeEncodingConformance_tB184A12AA9972C115D899779A92DCB82719B487A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNICODEENCODINGCONFORMANCE_TB184A12AA9972C115D899779A92DCB82719B487A_H
#ifndef COOKIECOLLECTION_T69ADF0ABD99419E54AB4740B341D94F443D995A3_H
#define COOKIECOLLECTION_T69ADF0ABD99419E54AB4740B341D94F443D995A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieCollection
struct  CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3  : public RuntimeObject
{
public:
	// System.Int32 System.Net.CookieCollection::m_version
	int32_t ___m_version_0;
	// System.Collections.ArrayList System.Net.CookieCollection::m_list
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___m_list_1;
	// System.DateTime System.Net.CookieCollection::m_TimeStamp
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_TimeStamp_2;
	// System.Boolean System.Net.CookieCollection::m_has_other_versions
	bool ___m_has_other_versions_3;
	// System.Boolean System.Net.CookieCollection::m_IsReadOnly
	bool ___m_IsReadOnly_4;

public:
	inline static int32_t get_offset_of_m_version_0() { return static_cast<int32_t>(offsetof(CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3, ___m_version_0)); }
	inline int32_t get_m_version_0() const { return ___m_version_0; }
	inline int32_t* get_address_of_m_version_0() { return &___m_version_0; }
	inline void set_m_version_0(int32_t value)
	{
		___m_version_0 = value;
	}

	inline static int32_t get_offset_of_m_list_1() { return static_cast<int32_t>(offsetof(CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3, ___m_list_1)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_m_list_1() const { return ___m_list_1; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_m_list_1() { return &___m_list_1; }
	inline void set_m_list_1(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___m_list_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_list_1), value);
	}

	inline static int32_t get_offset_of_m_TimeStamp_2() { return static_cast<int32_t>(offsetof(CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3, ___m_TimeStamp_2)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_TimeStamp_2() const { return ___m_TimeStamp_2; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_TimeStamp_2() { return &___m_TimeStamp_2; }
	inline void set_m_TimeStamp_2(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_TimeStamp_2 = value;
	}

	inline static int32_t get_offset_of_m_has_other_versions_3() { return static_cast<int32_t>(offsetof(CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3, ___m_has_other_versions_3)); }
	inline bool get_m_has_other_versions_3() const { return ___m_has_other_versions_3; }
	inline bool* get_address_of_m_has_other_versions_3() { return &___m_has_other_versions_3; }
	inline void set_m_has_other_versions_3(bool value)
	{
		___m_has_other_versions_3 = value;
	}

	inline static int32_t get_offset_of_m_IsReadOnly_4() { return static_cast<int32_t>(offsetof(CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3, ___m_IsReadOnly_4)); }
	inline bool get_m_IsReadOnly_4() const { return ___m_IsReadOnly_4; }
	inline bool* get_address_of_m_IsReadOnly_4() { return &___m_IsReadOnly_4; }
	inline void set_m_IsReadOnly_4(bool value)
	{
		___m_IsReadOnly_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIECOLLECTION_T69ADF0ABD99419E54AB4740B341D94F443D995A3_H
#ifndef STAMP_T06B0F70FA36D78E86543007609E79740E8BB87BE_H
#define STAMP_T06B0F70FA36D78E86543007609E79740E8BB87BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieCollection/Stamp
struct  Stamp_t06B0F70FA36D78E86543007609E79740E8BB87BE 
{
public:
	// System.Int32 System.Net.CookieCollection/Stamp::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Stamp_t06B0F70FA36D78E86543007609E79740E8BB87BE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STAMP_T06B0F70FA36D78E86543007609E79740E8BB87BE_H
#ifndef COOKIETOKEN_TB2F88831DE62615EAABB9CBF6CCFDFCD0A0D88B8_H
#define COOKIETOKEN_TB2F88831DE62615EAABB9CBF6CCFDFCD0A0D88B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieToken
struct  CookieToken_tB2F88831DE62615EAABB9CBF6CCFDFCD0A0D88B8 
{
public:
	// System.Int32 System.Net.CookieToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CookieToken_tB2F88831DE62615EAABB9CBF6CCFDFCD0A0D88B8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIETOKEN_TB2F88831DE62615EAABB9CBF6CCFDFCD0A0D88B8_H
#ifndef COOKIEVARIANT_T896D38AC6FBE01ADFB532B04C5E0E19842256CFC_H
#define COOKIEVARIANT_T896D38AC6FBE01ADFB532B04C5E0E19842256CFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieVariant
struct  CookieVariant_t896D38AC6FBE01ADFB532B04C5E0E19842256CFC 
{
public:
	// System.Int32 System.Net.CookieVariant::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CookieVariant_t896D38AC6FBE01ADFB532B04C5E0E19842256CFC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIEVARIANT_T896D38AC6FBE01ADFB532B04C5E0E19842256CFC_H
#ifndef DECOMPRESSIONMETHODS_T828950DA24A3D2B4A635E51125685CDB629ED51D_H
#define DECOMPRESSIONMETHODS_T828950DA24A3D2B4A635E51125685CDB629ED51D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DecompressionMethods
struct  DecompressionMethods_t828950DA24A3D2B4A635E51125685CDB629ED51D 
{
public:
	// System.Int32 System.Net.DecompressionMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DecompressionMethods_t828950DA24A3D2B4A635E51125685CDB629ED51D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECOMPRESSIONMETHODS_T828950DA24A3D2B4A635E51125685CDB629ED51D_H
#ifndef DIGESTSESSION_TCFF843AD732355E50F57213B29E91E9016A17627_H
#define DIGESTSESSION_TCFF843AD732355E50F57213B29E91E9016A17627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DigestSession
struct  DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627  : public RuntimeObject
{
public:
	// System.DateTime System.Net.DigestSession::lastUse
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___lastUse_1;
	// System.Int32 System.Net.DigestSession::_nc
	int32_t ____nc_2;
	// System.Security.Cryptography.HashAlgorithm System.Net.DigestSession::hash
	HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * ___hash_3;
	// System.Net.DigestHeaderParser System.Net.DigestSession::parser
	DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9 * ___parser_4;
	// System.String System.Net.DigestSession::_cnonce
	String_t* ____cnonce_5;

public:
	inline static int32_t get_offset_of_lastUse_1() { return static_cast<int32_t>(offsetof(DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627, ___lastUse_1)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_lastUse_1() const { return ___lastUse_1; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_lastUse_1() { return &___lastUse_1; }
	inline void set_lastUse_1(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___lastUse_1 = value;
	}

	inline static int32_t get_offset_of__nc_2() { return static_cast<int32_t>(offsetof(DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627, ____nc_2)); }
	inline int32_t get__nc_2() const { return ____nc_2; }
	inline int32_t* get_address_of__nc_2() { return &____nc_2; }
	inline void set__nc_2(int32_t value)
	{
		____nc_2 = value;
	}

	inline static int32_t get_offset_of_hash_3() { return static_cast<int32_t>(offsetof(DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627, ___hash_3)); }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * get_hash_3() const { return ___hash_3; }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA ** get_address_of_hash_3() { return &___hash_3; }
	inline void set_hash_3(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * value)
	{
		___hash_3 = value;
		Il2CppCodeGenWriteBarrier((&___hash_3), value);
	}

	inline static int32_t get_offset_of_parser_4() { return static_cast<int32_t>(offsetof(DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627, ___parser_4)); }
	inline DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9 * get_parser_4() const { return ___parser_4; }
	inline DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9 ** get_address_of_parser_4() { return &___parser_4; }
	inline void set_parser_4(DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9 * value)
	{
		___parser_4 = value;
		Il2CppCodeGenWriteBarrier((&___parser_4), value);
	}

	inline static int32_t get_offset_of__cnonce_5() { return static_cast<int32_t>(offsetof(DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627, ____cnonce_5)); }
	inline String_t* get__cnonce_5() const { return ____cnonce_5; }
	inline String_t** get_address_of__cnonce_5() { return &____cnonce_5; }
	inline void set__cnonce_5(String_t* value)
	{
		____cnonce_5 = value;
		Il2CppCodeGenWriteBarrier((&____cnonce_5), value);
	}
};

struct DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627_StaticFields
{
public:
	// System.Security.Cryptography.RandomNumberGenerator System.Net.DigestSession::rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ___rng_0;

public:
	inline static int32_t get_offset_of_rng_0() { return static_cast<int32_t>(offsetof(DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627_StaticFields, ___rng_0)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get_rng_0() const { return ___rng_0; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of_rng_0() { return &___rng_0; }
	inline void set_rng_0(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		___rng_0 = value;
		Il2CppCodeGenWriteBarrier((&___rng_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTSESSION_TCFF843AD732355E50F57213B29E91E9016A17627_H
#ifndef DOWNLOADPROGRESSCHANGEDEVENTARGS_TACD8549B8D84C4FBA17C2D19FA87E966CCCD4522_H
#define DOWNLOADPROGRESSCHANGEDEVENTARGS_TACD8549B8D84C4FBA17C2D19FA87E966CCCD4522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DownloadProgressChangedEventArgs
struct  DownloadProgressChangedEventArgs_tACD8549B8D84C4FBA17C2D19FA87E966CCCD4522  : public ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F
{
public:
	// System.Int64 System.Net.DownloadProgressChangedEventArgs::m_BytesReceived
	int64_t ___m_BytesReceived_3;
	// System.Int64 System.Net.DownloadProgressChangedEventArgs::m_TotalBytesToReceive
	int64_t ___m_TotalBytesToReceive_4;

public:
	inline static int32_t get_offset_of_m_BytesReceived_3() { return static_cast<int32_t>(offsetof(DownloadProgressChangedEventArgs_tACD8549B8D84C4FBA17C2D19FA87E966CCCD4522, ___m_BytesReceived_3)); }
	inline int64_t get_m_BytesReceived_3() const { return ___m_BytesReceived_3; }
	inline int64_t* get_address_of_m_BytesReceived_3() { return &___m_BytesReceived_3; }
	inline void set_m_BytesReceived_3(int64_t value)
	{
		___m_BytesReceived_3 = value;
	}

	inline static int32_t get_offset_of_m_TotalBytesToReceive_4() { return static_cast<int32_t>(offsetof(DownloadProgressChangedEventArgs_tACD8549B8D84C4FBA17C2D19FA87E966CCCD4522, ___m_TotalBytesToReceive_4)); }
	inline int64_t get_m_TotalBytesToReceive_4() const { return ___m_TotalBytesToReceive_4; }
	inline int64_t* get_address_of_m_TotalBytesToReceive_4() { return &___m_TotalBytesToReceive_4; }
	inline void set_m_TotalBytesToReceive_4(int64_t value)
	{
		___m_TotalBytesToReceive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADPROGRESSCHANGEDEVENTARGS_TACD8549B8D84C4FBA17C2D19FA87E966CCCD4522_H
#ifndef FTPDATASTREAM_TBF423F55CA0947ED2BF909BEA79DA349338DD3B1_H
#define FTPDATASTREAM_TBF423F55CA0947ED2BF909BEA79DA349338DD3B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpDataStream
struct  FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Net.FtpWebRequest System.Net.FtpDataStream::request
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA * ___request_4;
	// System.IO.Stream System.Net.FtpDataStream::networkStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___networkStream_5;
	// System.Boolean System.Net.FtpDataStream::disposed
	bool ___disposed_6;
	// System.Boolean System.Net.FtpDataStream::isRead
	bool ___isRead_7;
	// System.Int32 System.Net.FtpDataStream::totalRead
	int32_t ___totalRead_8;

public:
	inline static int32_t get_offset_of_request_4() { return static_cast<int32_t>(offsetof(FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1, ___request_4)); }
	inline FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA * get_request_4() const { return ___request_4; }
	inline FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA ** get_address_of_request_4() { return &___request_4; }
	inline void set_request_4(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA * value)
	{
		___request_4 = value;
		Il2CppCodeGenWriteBarrier((&___request_4), value);
	}

	inline static int32_t get_offset_of_networkStream_5() { return static_cast<int32_t>(offsetof(FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1, ___networkStream_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_networkStream_5() const { return ___networkStream_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_networkStream_5() { return &___networkStream_5; }
	inline void set_networkStream_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___networkStream_5 = value;
		Il2CppCodeGenWriteBarrier((&___networkStream_5), value);
	}

	inline static int32_t get_offset_of_disposed_6() { return static_cast<int32_t>(offsetof(FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1, ___disposed_6)); }
	inline bool get_disposed_6() const { return ___disposed_6; }
	inline bool* get_address_of_disposed_6() { return &___disposed_6; }
	inline void set_disposed_6(bool value)
	{
		___disposed_6 = value;
	}

	inline static int32_t get_offset_of_isRead_7() { return static_cast<int32_t>(offsetof(FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1, ___isRead_7)); }
	inline bool get_isRead_7() const { return ___isRead_7; }
	inline bool* get_address_of_isRead_7() { return &___isRead_7; }
	inline void set_isRead_7(bool value)
	{
		___isRead_7 = value;
	}

	inline static int32_t get_offset_of_totalRead_8() { return static_cast<int32_t>(offsetof(FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1, ___totalRead_8)); }
	inline int32_t get_totalRead_8() const { return ___totalRead_8; }
	inline int32_t* get_address_of_totalRead_8() { return &___totalRead_8; }
	inline void set_totalRead_8(int32_t value)
	{
		___totalRead_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPDATASTREAM_TBF423F55CA0947ED2BF909BEA79DA349338DD3B1_H
#ifndef FTPSTATUSCODE_T25AB6DADF4DE44C0973C59F53A7D797F009F8C67_H
#define FTPSTATUSCODE_T25AB6DADF4DE44C0973C59F53A7D797F009F8C67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpStatusCode
struct  FtpStatusCode_t25AB6DADF4DE44C0973C59F53A7D797F009F8C67 
{
public:
	// System.Int32 System.Net.FtpStatusCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FtpStatusCode_t25AB6DADF4DE44C0973C59F53A7D797F009F8C67, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPSTATUSCODE_T25AB6DADF4DE44C0973C59F53A7D797F009F8C67_H
#ifndef REQUESTSTATE_T850C56F50136642DB235E32D764586B31C248731_H
#define REQUESTSTATE_T850C56F50136642DB235E32D764586B31C248731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpWebRequest/RequestState
struct  RequestState_t850C56F50136642DB235E32D764586B31C248731 
{
public:
	// System.Int32 System.Net.FtpWebRequest/RequestState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RequestState_t850C56F50136642DB235E32D764586B31C248731, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTSTATE_T850C56F50136642DB235E32D764586B31C248731_H
#ifndef INPUTSTATE_TC9C07C35B1B422E948BD606C0AF277DFDAD48D86_H
#define INPUTSTATE_TC9C07C35B1B422E948BD606C0AF277DFDAD48D86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpConnection/InputState
struct  InputState_tC9C07C35B1B422E948BD606C0AF277DFDAD48D86 
{
public:
	// System.Int32 System.Net.HttpConnection/InputState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputState_tC9C07C35B1B422E948BD606C0AF277DFDAD48D86, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSTATE_TC9C07C35B1B422E948BD606C0AF277DFDAD48D86_H
#ifndef LINESTATE_TE99D1029981D06DF9C2614BBDEA270C09E2EA666_H
#define LINESTATE_TE99D1029981D06DF9C2614BBDEA270C09E2EA666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpConnection/LineState
struct  LineState_tE99D1029981D06DF9C2614BBDEA270C09E2EA666 
{
public:
	// System.Int32 System.Net.HttpConnection/LineState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LineState_tE99D1029981D06DF9C2614BBDEA270C09E2EA666, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESTATE_TE99D1029981D06DF9C2614BBDEA270C09E2EA666_H
#ifndef REQUESTSTREAM_T4BF6FD18AB7261094F557E867606B91FECF74D35_H
#define REQUESTSTREAM_T4BF6FD18AB7261094F557E867606B91FECF74D35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.RequestStream
struct  RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.Net.RequestStream::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_4;
	// System.Int32 System.Net.RequestStream::offset
	int32_t ___offset_5;
	// System.Int32 System.Net.RequestStream::length
	int32_t ___length_6;
	// System.Int64 System.Net.RequestStream::remaining_body
	int64_t ___remaining_body_7;
	// System.Boolean System.Net.RequestStream::disposed
	bool ___disposed_8;
	// System.IO.Stream System.Net.RequestStream::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_9;

public:
	inline static int32_t get_offset_of_buffer_4() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___buffer_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_4() const { return ___buffer_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_4() { return &___buffer_4; }
	inline void set_buffer_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_4), value);
	}

	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___offset_5)); }
	inline int32_t get_offset_5() const { return ___offset_5; }
	inline int32_t* get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(int32_t value)
	{
		___offset_5 = value;
	}

	inline static int32_t get_offset_of_length_6() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___length_6)); }
	inline int32_t get_length_6() const { return ___length_6; }
	inline int32_t* get_address_of_length_6() { return &___length_6; }
	inline void set_length_6(int32_t value)
	{
		___length_6 = value;
	}

	inline static int32_t get_offset_of_remaining_body_7() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___remaining_body_7)); }
	inline int64_t get_remaining_body_7() const { return ___remaining_body_7; }
	inline int64_t* get_address_of_remaining_body_7() { return &___remaining_body_7; }
	inline void set_remaining_body_7(int64_t value)
	{
		___remaining_body_7 = value;
	}

	inline static int32_t get_offset_of_disposed_8() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___disposed_8)); }
	inline bool get_disposed_8() const { return ___disposed_8; }
	inline bool* get_address_of_disposed_8() { return &___disposed_8; }
	inline void set_disposed_8(bool value)
	{
		___disposed_8 = value;
	}

	inline static int32_t get_offset_of_stream_9() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___stream_9)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_9() const { return ___stream_9; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_9() { return &___stream_9; }
	inline void set_stream_9(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_9 = value;
		Il2CppCodeGenWriteBarrier((&___stream_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTSTREAM_T4BF6FD18AB7261094F557E867606B91FECF74D35_H
#ifndef AUTHENTICATIONLEVEL_TC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B_H
#define AUTHENTICATIONLEVEL_TC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.AuthenticationLevel
struct  AuthenticationLevel_tC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B 
{
public:
	// System.Int32 System.Net.Security.AuthenticationLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AuthenticationLevel_tC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONLEVEL_TC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B_H
#ifndef SSLPOLICYERRORS_TD39D8AA1FDBFBC6745122C5A899F10A1C9258671_H
#define SSLPOLICYERRORS_TD39D8AA1FDBFBC6745122C5A899F10A1C9258671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.SslPolicyErrors
struct  SslPolicyErrors_tD39D8AA1FDBFBC6745122C5A899F10A1C9258671 
{
public:
	// System.Int32 System.Net.Security.SslPolicyErrors::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SslPolicyErrors_tD39D8AA1FDBFBC6745122C5A899F10A1C9258671, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLPOLICYERRORS_TD39D8AA1FDBFBC6745122C5A899F10A1C9258671_H
#ifndef TIMERSTATE_TD555FD971FFAFF7BE6F94885EC160D206354BD35_H
#define TIMERSTATE_TD555FD971FFAFF7BE6F94885EC160D206354BD35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread/TimerNode/TimerState
struct  TimerState_tD555FD971FFAFF7BE6F94885EC160D206354BD35 
{
public:
	// System.Int32 System.Net.TimerThread/TimerNode/TimerState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TimerState_tD555FD971FFAFF7BE6F94885EC160D206354BD35, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERSTATE_TD555FD971FFAFF7BE6F94885EC160D206354BD35_H
#ifndef UPLOADPROGRESSCHANGEDEVENTARGS_TE606124E632B46CA14E4ED59011DBE3203BE975E_H
#define UPLOADPROGRESSCHANGEDEVENTARGS_TE606124E632B46CA14E4ED59011DBE3203BE975E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadProgressChangedEventArgs
struct  UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E  : public ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F
{
public:
	// System.Int64 System.Net.UploadProgressChangedEventArgs::m_BytesReceived
	int64_t ___m_BytesReceived_3;
	// System.Int64 System.Net.UploadProgressChangedEventArgs::m_TotalBytesToReceive
	int64_t ___m_TotalBytesToReceive_4;
	// System.Int64 System.Net.UploadProgressChangedEventArgs::m_BytesSent
	int64_t ___m_BytesSent_5;
	// System.Int64 System.Net.UploadProgressChangedEventArgs::m_TotalBytesToSend
	int64_t ___m_TotalBytesToSend_6;

public:
	inline static int32_t get_offset_of_m_BytesReceived_3() { return static_cast<int32_t>(offsetof(UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E, ___m_BytesReceived_3)); }
	inline int64_t get_m_BytesReceived_3() const { return ___m_BytesReceived_3; }
	inline int64_t* get_address_of_m_BytesReceived_3() { return &___m_BytesReceived_3; }
	inline void set_m_BytesReceived_3(int64_t value)
	{
		___m_BytesReceived_3 = value;
	}

	inline static int32_t get_offset_of_m_TotalBytesToReceive_4() { return static_cast<int32_t>(offsetof(UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E, ___m_TotalBytesToReceive_4)); }
	inline int64_t get_m_TotalBytesToReceive_4() const { return ___m_TotalBytesToReceive_4; }
	inline int64_t* get_address_of_m_TotalBytesToReceive_4() { return &___m_TotalBytesToReceive_4; }
	inline void set_m_TotalBytesToReceive_4(int64_t value)
	{
		___m_TotalBytesToReceive_4 = value;
	}

	inline static int32_t get_offset_of_m_BytesSent_5() { return static_cast<int32_t>(offsetof(UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E, ___m_BytesSent_5)); }
	inline int64_t get_m_BytesSent_5() const { return ___m_BytesSent_5; }
	inline int64_t* get_address_of_m_BytesSent_5() { return &___m_BytesSent_5; }
	inline void set_m_BytesSent_5(int64_t value)
	{
		___m_BytesSent_5 = value;
	}

	inline static int32_t get_offset_of_m_TotalBytesToSend_6() { return static_cast<int32_t>(offsetof(UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E, ___m_TotalBytesToSend_6)); }
	inline int64_t get_m_TotalBytesToSend_6() const { return ___m_TotalBytesToSend_6; }
	inline int64_t* get_address_of_m_TotalBytesToSend_6() { return &___m_TotalBytesToSend_6; }
	inline void set_m_TotalBytesToSend_6(int64_t value)
	{
		___m_TotalBytesToSend_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADPROGRESSCHANGEDEVENTARGS_TE606124E632B46CA14E4ED59011DBE3203BE975E_H
#ifndef WEBCLIENT_T0BFD938623FF98FA7EEE0031AB9E8293DD295F6A_H
#define WEBCLIENT_T0BFD938623FF98FA7EEE0031AB9E8293DD295F6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient
struct  WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A  : public Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473
{
public:
	// System.Uri System.Net.WebClient::m_baseAddress
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___m_baseAddress_4;
	// System.Net.ICredentials System.Net.WebClient::m_credentials
	RuntimeObject* ___m_credentials_5;
	// System.Net.WebHeaderCollection System.Net.WebClient::m_headers
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * ___m_headers_6;
	// System.Collections.Specialized.NameValueCollection System.Net.WebClient::m_requestParameters
	NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 * ___m_requestParameters_7;
	// System.Net.WebResponse System.Net.WebClient::m_WebResponse
	WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * ___m_WebResponse_8;
	// System.Net.WebRequest System.Net.WebClient::m_WebRequest
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * ___m_WebRequest_9;
	// System.Text.Encoding System.Net.WebClient::m_Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___m_Encoding_10;
	// System.String System.Net.WebClient::m_Method
	String_t* ___m_Method_11;
	// System.Int64 System.Net.WebClient::m_ContentLength
	int64_t ___m_ContentLength_12;
	// System.Boolean System.Net.WebClient::m_Cancelled
	bool ___m_Cancelled_13;
	// System.Net.WebClient/ProgressData System.Net.WebClient::m_Progress
	ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 * ___m_Progress_14;
	// System.Net.IWebProxy System.Net.WebClient::m_Proxy
	RuntimeObject* ___m_Proxy_15;
	// System.Boolean System.Net.WebClient::m_ProxySet
	bool ___m_ProxySet_16;
	// System.Net.Cache.RequestCachePolicy System.Net.WebClient::m_CachePolicy
	RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * ___m_CachePolicy_17;
	// System.Int32 System.Net.WebClient::m_CallNesting
	int32_t ___m_CallNesting_18;
	// System.Threading.SendOrPostCallback System.Net.WebClient::reportDownloadProgressChanged
	SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * ___reportDownloadProgressChanged_19;
	// System.Threading.SendOrPostCallback System.Net.WebClient::reportUploadProgressChanged
	SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * ___reportUploadProgressChanged_20;

public:
	inline static int32_t get_offset_of_m_baseAddress_4() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_baseAddress_4)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_m_baseAddress_4() const { return ___m_baseAddress_4; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_m_baseAddress_4() { return &___m_baseAddress_4; }
	inline void set_m_baseAddress_4(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___m_baseAddress_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_baseAddress_4), value);
	}

	inline static int32_t get_offset_of_m_credentials_5() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_credentials_5)); }
	inline RuntimeObject* get_m_credentials_5() const { return ___m_credentials_5; }
	inline RuntimeObject** get_address_of_m_credentials_5() { return &___m_credentials_5; }
	inline void set_m_credentials_5(RuntimeObject* value)
	{
		___m_credentials_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_credentials_5), value);
	}

	inline static int32_t get_offset_of_m_headers_6() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_headers_6)); }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * get_m_headers_6() const { return ___m_headers_6; }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 ** get_address_of_m_headers_6() { return &___m_headers_6; }
	inline void set_m_headers_6(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * value)
	{
		___m_headers_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_headers_6), value);
	}

	inline static int32_t get_offset_of_m_requestParameters_7() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_requestParameters_7)); }
	inline NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 * get_m_requestParameters_7() const { return ___m_requestParameters_7; }
	inline NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 ** get_address_of_m_requestParameters_7() { return &___m_requestParameters_7; }
	inline void set_m_requestParameters_7(NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 * value)
	{
		___m_requestParameters_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_requestParameters_7), value);
	}

	inline static int32_t get_offset_of_m_WebResponse_8() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_WebResponse_8)); }
	inline WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * get_m_WebResponse_8() const { return ___m_WebResponse_8; }
	inline WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD ** get_address_of_m_WebResponse_8() { return &___m_WebResponse_8; }
	inline void set_m_WebResponse_8(WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * value)
	{
		___m_WebResponse_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_WebResponse_8), value);
	}

	inline static int32_t get_offset_of_m_WebRequest_9() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_WebRequest_9)); }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * get_m_WebRequest_9() const { return ___m_WebRequest_9; }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 ** get_address_of_m_WebRequest_9() { return &___m_WebRequest_9; }
	inline void set_m_WebRequest_9(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * value)
	{
		___m_WebRequest_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_WebRequest_9), value);
	}

	inline static int32_t get_offset_of_m_Encoding_10() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_Encoding_10)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_m_Encoding_10() const { return ___m_Encoding_10; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_m_Encoding_10() { return &___m_Encoding_10; }
	inline void set_m_Encoding_10(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___m_Encoding_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Encoding_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_Method_11)); }
	inline String_t* get_m_Method_11() const { return ___m_Method_11; }
	inline String_t** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(String_t* value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}

	inline static int32_t get_offset_of_m_ContentLength_12() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_ContentLength_12)); }
	inline int64_t get_m_ContentLength_12() const { return ___m_ContentLength_12; }
	inline int64_t* get_address_of_m_ContentLength_12() { return &___m_ContentLength_12; }
	inline void set_m_ContentLength_12(int64_t value)
	{
		___m_ContentLength_12 = value;
	}

	inline static int32_t get_offset_of_m_Cancelled_13() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_Cancelled_13)); }
	inline bool get_m_Cancelled_13() const { return ___m_Cancelled_13; }
	inline bool* get_address_of_m_Cancelled_13() { return &___m_Cancelled_13; }
	inline void set_m_Cancelled_13(bool value)
	{
		___m_Cancelled_13 = value;
	}

	inline static int32_t get_offset_of_m_Progress_14() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_Progress_14)); }
	inline ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 * get_m_Progress_14() const { return ___m_Progress_14; }
	inline ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 ** get_address_of_m_Progress_14() { return &___m_Progress_14; }
	inline void set_m_Progress_14(ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 * value)
	{
		___m_Progress_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Progress_14), value);
	}

	inline static int32_t get_offset_of_m_Proxy_15() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_Proxy_15)); }
	inline RuntimeObject* get_m_Proxy_15() const { return ___m_Proxy_15; }
	inline RuntimeObject** get_address_of_m_Proxy_15() { return &___m_Proxy_15; }
	inline void set_m_Proxy_15(RuntimeObject* value)
	{
		___m_Proxy_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Proxy_15), value);
	}

	inline static int32_t get_offset_of_m_ProxySet_16() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_ProxySet_16)); }
	inline bool get_m_ProxySet_16() const { return ___m_ProxySet_16; }
	inline bool* get_address_of_m_ProxySet_16() { return &___m_ProxySet_16; }
	inline void set_m_ProxySet_16(bool value)
	{
		___m_ProxySet_16 = value;
	}

	inline static int32_t get_offset_of_m_CachePolicy_17() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_CachePolicy_17)); }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * get_m_CachePolicy_17() const { return ___m_CachePolicy_17; }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 ** get_address_of_m_CachePolicy_17() { return &___m_CachePolicy_17; }
	inline void set_m_CachePolicy_17(RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * value)
	{
		___m_CachePolicy_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachePolicy_17), value);
	}

	inline static int32_t get_offset_of_m_CallNesting_18() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_CallNesting_18)); }
	inline int32_t get_m_CallNesting_18() const { return ___m_CallNesting_18; }
	inline int32_t* get_address_of_m_CallNesting_18() { return &___m_CallNesting_18; }
	inline void set_m_CallNesting_18(int32_t value)
	{
		___m_CallNesting_18 = value;
	}

	inline static int32_t get_offset_of_reportDownloadProgressChanged_19() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___reportDownloadProgressChanged_19)); }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * get_reportDownloadProgressChanged_19() const { return ___reportDownloadProgressChanged_19; }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 ** get_address_of_reportDownloadProgressChanged_19() { return &___reportDownloadProgressChanged_19; }
	inline void set_reportDownloadProgressChanged_19(SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * value)
	{
		___reportDownloadProgressChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___reportDownloadProgressChanged_19), value);
	}

	inline static int32_t get_offset_of_reportUploadProgressChanged_20() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___reportUploadProgressChanged_20)); }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * get_reportUploadProgressChanged_20() const { return ___reportUploadProgressChanged_20; }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 ** get_address_of_reportUploadProgressChanged_20() { return &___reportUploadProgressChanged_20; }
	inline void set_reportUploadProgressChanged_20(SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * value)
	{
		___reportUploadProgressChanged_20 = value;
		Il2CppCodeGenWriteBarrier((&___reportUploadProgressChanged_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCLIENT_T0BFD938623FF98FA7EEE0031AB9E8293DD295F6A_H
#ifndef WEBEXCEPTIONINTERNALSTATUS_T2B50725020F5BAB7DCBE324ADF308881FEB3B64D_H
#define WEBEXCEPTIONINTERNALSTATUS_T2B50725020F5BAB7DCBE324ADF308881FEB3B64D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionInternalStatus
struct  WebExceptionInternalStatus_t2B50725020F5BAB7DCBE324ADF308881FEB3B64D 
{
public:
	// System.Int32 System.Net.WebExceptionInternalStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebExceptionInternalStatus_t2B50725020F5BAB7DCBE324ADF308881FEB3B64D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONINTERNALSTATUS_T2B50725020F5BAB7DCBE324ADF308881FEB3B64D_H
#ifndef WEBEXCEPTIONSTATUS_T97365CBADE462C1E2A1A0FACF18F3B111900F8DC_H
#define WEBEXCEPTIONSTATUS_T97365CBADE462C1E2A1A0FACF18F3B111900F8DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionStatus
struct  WebExceptionStatus_t97365CBADE462C1E2A1A0FACF18F3B111900F8DC 
{
public:
	// System.Int32 System.Net.WebExceptionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebExceptionStatus_t97365CBADE462C1E2A1A0FACF18F3B111900F8DC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONSTATUS_T97365CBADE462C1E2A1A0FACF18F3B111900F8DC_H
#ifndef RFCCHAR_TD4173F085F19DF711D550AC6CAD7EF61939EF27F_H
#define RFCCHAR_TD4173F085F19DF711D550AC6CAD7EF61939EF27F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebHeaderCollection/RfcChar
struct  RfcChar_tD4173F085F19DF711D550AC6CAD7EF61939EF27F 
{
public:
	// System.Byte System.Net.WebHeaderCollection/RfcChar::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RfcChar_tD4173F085F19DF711D550AC6CAD7EF61939EF27F, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RFCCHAR_TD4173F085F19DF711D550AC6CAD7EF61939EF27F_H
#ifndef WEBHEADERCOLLECTIONTYPE_T2994510EB856AC407AB0757A9814CDF80185A862_H
#define WEBHEADERCOLLECTIONTYPE_T2994510EB856AC407AB0757A9814CDF80185A862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebHeaderCollectionType
struct  WebHeaderCollectionType_t2994510EB856AC407AB0757A9814CDF80185A862 
{
public:
	// System.UInt16 System.Net.WebHeaderCollectionType::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebHeaderCollectionType_t2994510EB856AC407AB0757A9814CDF80185A862, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBHEADERCOLLECTIONTYPE_T2994510EB856AC407AB0757A9814CDF80185A862_H
#ifndef TOKENIMPERSONATIONLEVEL_TED478ED25688E978F79556E1A2335F7262023D26_H
#define TOKENIMPERSONATIONLEVEL_TED478ED25688E978F79556E1A2335F7262023D26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Principal.TokenImpersonationLevel
struct  TokenImpersonationLevel_tED478ED25688E978F79556E1A2335F7262023D26 
{
public:
	// System.Int32 System.Security.Principal.TokenImpersonationLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TokenImpersonationLevel_tED478ED25688E978F79556E1A2335F7262023D26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENIMPERSONATIONLEVEL_TED478ED25688E978F79556E1A2335F7262023D26_H
#ifndef FILESTREAM_TA770BF9AF0906644D43C81B962C7DBC3BC79A418_H
#define FILESTREAM_TA770BF9AF0906644D43C81B962C7DBC3BC79A418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileStream
struct  FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.IO.FileStream::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_6;
	// System.String System.IO.FileStream::name
	String_t* ___name_7;
	// Microsoft.Win32.SafeHandles.SafeFileHandle System.IO.FileStream::safeHandle
	SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB * ___safeHandle_8;
	// System.Boolean System.IO.FileStream::isExposed
	bool ___isExposed_9;
	// System.Int64 System.IO.FileStream::append_startpos
	int64_t ___append_startpos_10;
	// System.IO.FileAccess System.IO.FileStream::access
	int32_t ___access_11;
	// System.Boolean System.IO.FileStream::owner
	bool ___owner_12;
	// System.Boolean System.IO.FileStream::async
	bool ___async_13;
	// System.Boolean System.IO.FileStream::canseek
	bool ___canseek_14;
	// System.Boolean System.IO.FileStream::anonymous
	bool ___anonymous_15;
	// System.Boolean System.IO.FileStream::buf_dirty
	bool ___buf_dirty_16;
	// System.Int32 System.IO.FileStream::buf_size
	int32_t ___buf_size_17;
	// System.Int32 System.IO.FileStream::buf_length
	int32_t ___buf_length_18;
	// System.Int32 System.IO.FileStream::buf_offset
	int32_t ___buf_offset_19;
	// System.Int64 System.IO.FileStream::buf_start
	int64_t ___buf_start_20;

public:
	inline static int32_t get_offset_of_buf_6() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_6() const { return ___buf_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_6() { return &___buf_6; }
	inline void set_buf_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_6 = value;
		Il2CppCodeGenWriteBarrier((&___buf_6), value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_safeHandle_8() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___safeHandle_8)); }
	inline SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB * get_safeHandle_8() const { return ___safeHandle_8; }
	inline SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB ** get_address_of_safeHandle_8() { return &___safeHandle_8; }
	inline void set_safeHandle_8(SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB * value)
	{
		___safeHandle_8 = value;
		Il2CppCodeGenWriteBarrier((&___safeHandle_8), value);
	}

	inline static int32_t get_offset_of_isExposed_9() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___isExposed_9)); }
	inline bool get_isExposed_9() const { return ___isExposed_9; }
	inline bool* get_address_of_isExposed_9() { return &___isExposed_9; }
	inline void set_isExposed_9(bool value)
	{
		___isExposed_9 = value;
	}

	inline static int32_t get_offset_of_append_startpos_10() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___append_startpos_10)); }
	inline int64_t get_append_startpos_10() const { return ___append_startpos_10; }
	inline int64_t* get_address_of_append_startpos_10() { return &___append_startpos_10; }
	inline void set_append_startpos_10(int64_t value)
	{
		___append_startpos_10 = value;
	}

	inline static int32_t get_offset_of_access_11() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___access_11)); }
	inline int32_t get_access_11() const { return ___access_11; }
	inline int32_t* get_address_of_access_11() { return &___access_11; }
	inline void set_access_11(int32_t value)
	{
		___access_11 = value;
	}

	inline static int32_t get_offset_of_owner_12() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___owner_12)); }
	inline bool get_owner_12() const { return ___owner_12; }
	inline bool* get_address_of_owner_12() { return &___owner_12; }
	inline void set_owner_12(bool value)
	{
		___owner_12 = value;
	}

	inline static int32_t get_offset_of_async_13() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___async_13)); }
	inline bool get_async_13() const { return ___async_13; }
	inline bool* get_address_of_async_13() { return &___async_13; }
	inline void set_async_13(bool value)
	{
		___async_13 = value;
	}

	inline static int32_t get_offset_of_canseek_14() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___canseek_14)); }
	inline bool get_canseek_14() const { return ___canseek_14; }
	inline bool* get_address_of_canseek_14() { return &___canseek_14; }
	inline void set_canseek_14(bool value)
	{
		___canseek_14 = value;
	}

	inline static int32_t get_offset_of_anonymous_15() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___anonymous_15)); }
	inline bool get_anonymous_15() const { return ___anonymous_15; }
	inline bool* get_address_of_anonymous_15() { return &___anonymous_15; }
	inline void set_anonymous_15(bool value)
	{
		___anonymous_15 = value;
	}

	inline static int32_t get_offset_of_buf_dirty_16() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_dirty_16)); }
	inline bool get_buf_dirty_16() const { return ___buf_dirty_16; }
	inline bool* get_address_of_buf_dirty_16() { return &___buf_dirty_16; }
	inline void set_buf_dirty_16(bool value)
	{
		___buf_dirty_16 = value;
	}

	inline static int32_t get_offset_of_buf_size_17() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_size_17)); }
	inline int32_t get_buf_size_17() const { return ___buf_size_17; }
	inline int32_t* get_address_of_buf_size_17() { return &___buf_size_17; }
	inline void set_buf_size_17(int32_t value)
	{
		___buf_size_17 = value;
	}

	inline static int32_t get_offset_of_buf_length_18() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_length_18)); }
	inline int32_t get_buf_length_18() const { return ___buf_length_18; }
	inline int32_t* get_address_of_buf_length_18() { return &___buf_length_18; }
	inline void set_buf_length_18(int32_t value)
	{
		___buf_length_18 = value;
	}

	inline static int32_t get_offset_of_buf_offset_19() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_offset_19)); }
	inline int32_t get_buf_offset_19() const { return ___buf_offset_19; }
	inline int32_t* get_address_of_buf_offset_19() { return &___buf_offset_19; }
	inline void set_buf_offset_19(int32_t value)
	{
		___buf_offset_19 = value;
	}

	inline static int32_t get_offset_of_buf_start_20() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_start_20)); }
	inline int64_t get_buf_start_20() const { return ___buf_start_20; }
	inline int64_t* get_address_of_buf_start_20() { return &___buf_start_20; }
	inline void set_buf_start_20(int64_t value)
	{
		___buf_start_20 = value;
	}
};

struct FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418_StaticFields
{
public:
	// System.Byte[] System.IO.FileStream::buf_recycle
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_recycle_4;
	// System.Object System.IO.FileStream::buf_recycle_lock
	RuntimeObject * ___buf_recycle_lock_5;

public:
	inline static int32_t get_offset_of_buf_recycle_4() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418_StaticFields, ___buf_recycle_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_recycle_4() const { return ___buf_recycle_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_recycle_4() { return &___buf_recycle_4; }
	inline void set_buf_recycle_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_recycle_4 = value;
		Il2CppCodeGenWriteBarrier((&___buf_recycle_4), value);
	}

	inline static int32_t get_offset_of_buf_recycle_lock_5() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418_StaticFields, ___buf_recycle_lock_5)); }
	inline RuntimeObject * get_buf_recycle_lock_5() const { return ___buf_recycle_lock_5; }
	inline RuntimeObject ** get_address_of_buf_recycle_lock_5() { return &___buf_recycle_lock_5; }
	inline void set_buf_recycle_lock_5(RuntimeObject * value)
	{
		___buf_recycle_lock_5 = value;
		Il2CppCodeGenWriteBarrier((&___buf_recycle_lock_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESTREAM_TA770BF9AF0906644D43C81B962C7DBC3BC79A418_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef CHUNKEDINPUTSTREAM_T10E9B9112B748C89333D9C33D519EDAF17181930_H
#define CHUNKEDINPUTSTREAM_T10E9B9112B748C89333D9C33D519EDAF17181930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ChunkedInputStream
struct  ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930  : public RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35
{
public:
	// System.Boolean System.Net.ChunkedInputStream::disposed
	bool ___disposed_10;
	// System.Net.MonoChunkStream System.Net.ChunkedInputStream::decoder
	MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5 * ___decoder_11;
	// System.Net.HttpListenerContext System.Net.ChunkedInputStream::context
	HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * ___context_12;
	// System.Boolean System.Net.ChunkedInputStream::no_more_data
	bool ___no_more_data_13;

public:
	inline static int32_t get_offset_of_disposed_10() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930, ___disposed_10)); }
	inline bool get_disposed_10() const { return ___disposed_10; }
	inline bool* get_address_of_disposed_10() { return &___disposed_10; }
	inline void set_disposed_10(bool value)
	{
		___disposed_10 = value;
	}

	inline static int32_t get_offset_of_decoder_11() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930, ___decoder_11)); }
	inline MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5 * get_decoder_11() const { return ___decoder_11; }
	inline MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5 ** get_address_of_decoder_11() { return &___decoder_11; }
	inline void set_decoder_11(MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5 * value)
	{
		___decoder_11 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_11), value);
	}

	inline static int32_t get_offset_of_context_12() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930, ___context_12)); }
	inline HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * get_context_12() const { return ___context_12; }
	inline HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA ** get_address_of_context_12() { return &___context_12; }
	inline void set_context_12(HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * value)
	{
		___context_12 = value;
		Il2CppCodeGenWriteBarrier((&___context_12), value);
	}

	inline static int32_t get_offset_of_no_more_data_13() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930, ___no_more_data_13)); }
	inline bool get_no_more_data_13() const { return ___no_more_data_13; }
	inline bool* get_address_of_no_more_data_13() { return &___no_more_data_13; }
	inline void set_no_more_data_13(bool value)
	{
		___no_more_data_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHUNKEDINPUTSTREAM_T10E9B9112B748C89333D9C33D519EDAF17181930_H
#ifndef COOKIE_T595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_H
#define COOKIE_T595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Cookie
struct  Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90  : public RuntimeObject
{
public:
	// System.String System.Net.Cookie::m_comment
	String_t* ___m_comment_4;
	// System.Uri System.Net.Cookie::m_commentUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___m_commentUri_5;
	// System.Net.CookieVariant System.Net.Cookie::m_cookieVariant
	int32_t ___m_cookieVariant_6;
	// System.Boolean System.Net.Cookie::m_discard
	bool ___m_discard_7;
	// System.String System.Net.Cookie::m_domain
	String_t* ___m_domain_8;
	// System.Boolean System.Net.Cookie::m_domain_implicit
	bool ___m_domain_implicit_9;
	// System.DateTime System.Net.Cookie::m_expires
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_expires_10;
	// System.String System.Net.Cookie::m_name
	String_t* ___m_name_11;
	// System.String System.Net.Cookie::m_path
	String_t* ___m_path_12;
	// System.Boolean System.Net.Cookie::m_path_implicit
	bool ___m_path_implicit_13;
	// System.String System.Net.Cookie::m_port
	String_t* ___m_port_14;
	// System.Boolean System.Net.Cookie::m_port_implicit
	bool ___m_port_implicit_15;
	// System.Int32[] System.Net.Cookie::m_port_list
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_port_list_16;
	// System.Boolean System.Net.Cookie::m_secure
	bool ___m_secure_17;
	// System.Boolean System.Net.Cookie::m_httpOnly
	bool ___m_httpOnly_18;
	// System.DateTime System.Net.Cookie::m_timeStamp
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_timeStamp_19;
	// System.String System.Net.Cookie::m_value
	String_t* ___m_value_20;
	// System.Int32 System.Net.Cookie::m_version
	int32_t ___m_version_21;
	// System.String System.Net.Cookie::m_domainKey
	String_t* ___m_domainKey_22;
	// System.Boolean System.Net.Cookie::IsQuotedVersion
	bool ___IsQuotedVersion_23;
	// System.Boolean System.Net.Cookie::IsQuotedDomain
	bool ___IsQuotedDomain_24;

public:
	inline static int32_t get_offset_of_m_comment_4() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_comment_4)); }
	inline String_t* get_m_comment_4() const { return ___m_comment_4; }
	inline String_t** get_address_of_m_comment_4() { return &___m_comment_4; }
	inline void set_m_comment_4(String_t* value)
	{
		___m_comment_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_comment_4), value);
	}

	inline static int32_t get_offset_of_m_commentUri_5() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_commentUri_5)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_m_commentUri_5() const { return ___m_commentUri_5; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_m_commentUri_5() { return &___m_commentUri_5; }
	inline void set_m_commentUri_5(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___m_commentUri_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_commentUri_5), value);
	}

	inline static int32_t get_offset_of_m_cookieVariant_6() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_cookieVariant_6)); }
	inline int32_t get_m_cookieVariant_6() const { return ___m_cookieVariant_6; }
	inline int32_t* get_address_of_m_cookieVariant_6() { return &___m_cookieVariant_6; }
	inline void set_m_cookieVariant_6(int32_t value)
	{
		___m_cookieVariant_6 = value;
	}

	inline static int32_t get_offset_of_m_discard_7() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_discard_7)); }
	inline bool get_m_discard_7() const { return ___m_discard_7; }
	inline bool* get_address_of_m_discard_7() { return &___m_discard_7; }
	inline void set_m_discard_7(bool value)
	{
		___m_discard_7 = value;
	}

	inline static int32_t get_offset_of_m_domain_8() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_domain_8)); }
	inline String_t* get_m_domain_8() const { return ___m_domain_8; }
	inline String_t** get_address_of_m_domain_8() { return &___m_domain_8; }
	inline void set_m_domain_8(String_t* value)
	{
		___m_domain_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_domain_8), value);
	}

	inline static int32_t get_offset_of_m_domain_implicit_9() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_domain_implicit_9)); }
	inline bool get_m_domain_implicit_9() const { return ___m_domain_implicit_9; }
	inline bool* get_address_of_m_domain_implicit_9() { return &___m_domain_implicit_9; }
	inline void set_m_domain_implicit_9(bool value)
	{
		___m_domain_implicit_9 = value;
	}

	inline static int32_t get_offset_of_m_expires_10() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_expires_10)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_expires_10() const { return ___m_expires_10; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_expires_10() { return &___m_expires_10; }
	inline void set_m_expires_10(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_expires_10 = value;
	}

	inline static int32_t get_offset_of_m_name_11() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_name_11)); }
	inline String_t* get_m_name_11() const { return ___m_name_11; }
	inline String_t** get_address_of_m_name_11() { return &___m_name_11; }
	inline void set_m_name_11(String_t* value)
	{
		___m_name_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_11), value);
	}

	inline static int32_t get_offset_of_m_path_12() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_path_12)); }
	inline String_t* get_m_path_12() const { return ___m_path_12; }
	inline String_t** get_address_of_m_path_12() { return &___m_path_12; }
	inline void set_m_path_12(String_t* value)
	{
		___m_path_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_path_12), value);
	}

	inline static int32_t get_offset_of_m_path_implicit_13() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_path_implicit_13)); }
	inline bool get_m_path_implicit_13() const { return ___m_path_implicit_13; }
	inline bool* get_address_of_m_path_implicit_13() { return &___m_path_implicit_13; }
	inline void set_m_path_implicit_13(bool value)
	{
		___m_path_implicit_13 = value;
	}

	inline static int32_t get_offset_of_m_port_14() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_port_14)); }
	inline String_t* get_m_port_14() const { return ___m_port_14; }
	inline String_t** get_address_of_m_port_14() { return &___m_port_14; }
	inline void set_m_port_14(String_t* value)
	{
		___m_port_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_port_14), value);
	}

	inline static int32_t get_offset_of_m_port_implicit_15() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_port_implicit_15)); }
	inline bool get_m_port_implicit_15() const { return ___m_port_implicit_15; }
	inline bool* get_address_of_m_port_implicit_15() { return &___m_port_implicit_15; }
	inline void set_m_port_implicit_15(bool value)
	{
		___m_port_implicit_15 = value;
	}

	inline static int32_t get_offset_of_m_port_list_16() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_port_list_16)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_port_list_16() const { return ___m_port_list_16; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_port_list_16() { return &___m_port_list_16; }
	inline void set_m_port_list_16(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_port_list_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_port_list_16), value);
	}

	inline static int32_t get_offset_of_m_secure_17() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_secure_17)); }
	inline bool get_m_secure_17() const { return ___m_secure_17; }
	inline bool* get_address_of_m_secure_17() { return &___m_secure_17; }
	inline void set_m_secure_17(bool value)
	{
		___m_secure_17 = value;
	}

	inline static int32_t get_offset_of_m_httpOnly_18() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_httpOnly_18)); }
	inline bool get_m_httpOnly_18() const { return ___m_httpOnly_18; }
	inline bool* get_address_of_m_httpOnly_18() { return &___m_httpOnly_18; }
	inline void set_m_httpOnly_18(bool value)
	{
		___m_httpOnly_18 = value;
	}

	inline static int32_t get_offset_of_m_timeStamp_19() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_timeStamp_19)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_timeStamp_19() const { return ___m_timeStamp_19; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_timeStamp_19() { return &___m_timeStamp_19; }
	inline void set_m_timeStamp_19(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_timeStamp_19 = value;
	}

	inline static int32_t get_offset_of_m_value_20() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_value_20)); }
	inline String_t* get_m_value_20() const { return ___m_value_20; }
	inline String_t** get_address_of_m_value_20() { return &___m_value_20; }
	inline void set_m_value_20(String_t* value)
	{
		___m_value_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_value_20), value);
	}

	inline static int32_t get_offset_of_m_version_21() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_version_21)); }
	inline int32_t get_m_version_21() const { return ___m_version_21; }
	inline int32_t* get_address_of_m_version_21() { return &___m_version_21; }
	inline void set_m_version_21(int32_t value)
	{
		___m_version_21 = value;
	}

	inline static int32_t get_offset_of_m_domainKey_22() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_domainKey_22)); }
	inline String_t* get_m_domainKey_22() const { return ___m_domainKey_22; }
	inline String_t** get_address_of_m_domainKey_22() { return &___m_domainKey_22; }
	inline void set_m_domainKey_22(String_t* value)
	{
		___m_domainKey_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_domainKey_22), value);
	}

	inline static int32_t get_offset_of_IsQuotedVersion_23() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___IsQuotedVersion_23)); }
	inline bool get_IsQuotedVersion_23() const { return ___IsQuotedVersion_23; }
	inline bool* get_address_of_IsQuotedVersion_23() { return &___IsQuotedVersion_23; }
	inline void set_IsQuotedVersion_23(bool value)
	{
		___IsQuotedVersion_23 = value;
	}

	inline static int32_t get_offset_of_IsQuotedDomain_24() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___IsQuotedDomain_24)); }
	inline bool get_IsQuotedDomain_24() const { return ___IsQuotedDomain_24; }
	inline bool* get_address_of_IsQuotedDomain_24() { return &___IsQuotedDomain_24; }
	inline void set_IsQuotedDomain_24(bool value)
	{
		___IsQuotedDomain_24 = value;
	}
};

struct Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields
{
public:
	// System.Char[] System.Net.Cookie::PortSplitDelimiters
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___PortSplitDelimiters_0;
	// System.Char[] System.Net.Cookie::Reserved2Name
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___Reserved2Name_1;
	// System.Char[] System.Net.Cookie::Reserved2Value
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___Reserved2Value_2;
	// System.Net.Comparer System.Net.Cookie::staticComparer
	Comparer_tFC5265AD65740F9DB39C75F1E3EF8032982F45AB * ___staticComparer_3;

public:
	inline static int32_t get_offset_of_PortSplitDelimiters_0() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields, ___PortSplitDelimiters_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_PortSplitDelimiters_0() const { return ___PortSplitDelimiters_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_PortSplitDelimiters_0() { return &___PortSplitDelimiters_0; }
	inline void set_PortSplitDelimiters_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___PortSplitDelimiters_0 = value;
		Il2CppCodeGenWriteBarrier((&___PortSplitDelimiters_0), value);
	}

	inline static int32_t get_offset_of_Reserved2Name_1() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields, ___Reserved2Name_1)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_Reserved2Name_1() const { return ___Reserved2Name_1; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_Reserved2Name_1() { return &___Reserved2Name_1; }
	inline void set_Reserved2Name_1(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___Reserved2Name_1 = value;
		Il2CppCodeGenWriteBarrier((&___Reserved2Name_1), value);
	}

	inline static int32_t get_offset_of_Reserved2Value_2() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields, ___Reserved2Value_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_Reserved2Value_2() const { return ___Reserved2Value_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_Reserved2Value_2() { return &___Reserved2Value_2; }
	inline void set_Reserved2Value_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___Reserved2Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___Reserved2Value_2), value);
	}

	inline static int32_t get_offset_of_staticComparer_3() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields, ___staticComparer_3)); }
	inline Comparer_tFC5265AD65740F9DB39C75F1E3EF8032982F45AB * get_staticComparer_3() const { return ___staticComparer_3; }
	inline Comparer_tFC5265AD65740F9DB39C75F1E3EF8032982F45AB ** get_address_of_staticComparer_3() { return &___staticComparer_3; }
	inline void set_staticComparer_3(Comparer_tFC5265AD65740F9DB39C75F1E3EF8032982F45AB * value)
	{
		___staticComparer_3 = value;
		Il2CppCodeGenWriteBarrier((&___staticComparer_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIE_T595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_H
#ifndef COOKIEEXCEPTION_T1366ADFB475F67C6BAD72CE2EC1AB504861C2FA4_H
#define COOKIEEXCEPTION_T1366ADFB475F67C6BAD72CE2EC1AB504861C2FA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieException
struct  CookieException_t1366ADFB475F67C6BAD72CE2EC1AB504861C2FA4  : public FormatException_t2808E076CDE4650AF89F55FD78F49290D0EC5BDC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIEEXCEPTION_T1366ADFB475F67C6BAD72CE2EC1AB504861C2FA4_H
#ifndef COOKIETOKENIZER_T5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_H
#define COOKIETOKENIZER_T5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieTokenizer
struct  CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD  : public RuntimeObject
{
public:
	// System.Boolean System.Net.CookieTokenizer::m_eofCookie
	bool ___m_eofCookie_0;
	// System.Int32 System.Net.CookieTokenizer::m_index
	int32_t ___m_index_1;
	// System.Int32 System.Net.CookieTokenizer::m_length
	int32_t ___m_length_2;
	// System.String System.Net.CookieTokenizer::m_name
	String_t* ___m_name_3;
	// System.Boolean System.Net.CookieTokenizer::m_quoted
	bool ___m_quoted_4;
	// System.Int32 System.Net.CookieTokenizer::m_start
	int32_t ___m_start_5;
	// System.Net.CookieToken System.Net.CookieTokenizer::m_token
	int32_t ___m_token_6;
	// System.Int32 System.Net.CookieTokenizer::m_tokenLength
	int32_t ___m_tokenLength_7;
	// System.String System.Net.CookieTokenizer::m_tokenStream
	String_t* ___m_tokenStream_8;
	// System.String System.Net.CookieTokenizer::m_value
	String_t* ___m_value_9;

public:
	inline static int32_t get_offset_of_m_eofCookie_0() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_eofCookie_0)); }
	inline bool get_m_eofCookie_0() const { return ___m_eofCookie_0; }
	inline bool* get_address_of_m_eofCookie_0() { return &___m_eofCookie_0; }
	inline void set_m_eofCookie_0(bool value)
	{
		___m_eofCookie_0 = value;
	}

	inline static int32_t get_offset_of_m_index_1() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_index_1)); }
	inline int32_t get_m_index_1() const { return ___m_index_1; }
	inline int32_t* get_address_of_m_index_1() { return &___m_index_1; }
	inline void set_m_index_1(int32_t value)
	{
		___m_index_1 = value;
	}

	inline static int32_t get_offset_of_m_length_2() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_length_2)); }
	inline int32_t get_m_length_2() const { return ___m_length_2; }
	inline int32_t* get_address_of_m_length_2() { return &___m_length_2; }
	inline void set_m_length_2(int32_t value)
	{
		___m_length_2 = value;
	}

	inline static int32_t get_offset_of_m_name_3() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_name_3)); }
	inline String_t* get_m_name_3() const { return ___m_name_3; }
	inline String_t** get_address_of_m_name_3() { return &___m_name_3; }
	inline void set_m_name_3(String_t* value)
	{
		___m_name_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_3), value);
	}

	inline static int32_t get_offset_of_m_quoted_4() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_quoted_4)); }
	inline bool get_m_quoted_4() const { return ___m_quoted_4; }
	inline bool* get_address_of_m_quoted_4() { return &___m_quoted_4; }
	inline void set_m_quoted_4(bool value)
	{
		___m_quoted_4 = value;
	}

	inline static int32_t get_offset_of_m_start_5() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_start_5)); }
	inline int32_t get_m_start_5() const { return ___m_start_5; }
	inline int32_t* get_address_of_m_start_5() { return &___m_start_5; }
	inline void set_m_start_5(int32_t value)
	{
		___m_start_5 = value;
	}

	inline static int32_t get_offset_of_m_token_6() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_token_6)); }
	inline int32_t get_m_token_6() const { return ___m_token_6; }
	inline int32_t* get_address_of_m_token_6() { return &___m_token_6; }
	inline void set_m_token_6(int32_t value)
	{
		___m_token_6 = value;
	}

	inline static int32_t get_offset_of_m_tokenLength_7() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_tokenLength_7)); }
	inline int32_t get_m_tokenLength_7() const { return ___m_tokenLength_7; }
	inline int32_t* get_address_of_m_tokenLength_7() { return &___m_tokenLength_7; }
	inline void set_m_tokenLength_7(int32_t value)
	{
		___m_tokenLength_7 = value;
	}

	inline static int32_t get_offset_of_m_tokenStream_8() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_tokenStream_8)); }
	inline String_t* get_m_tokenStream_8() const { return ___m_tokenStream_8; }
	inline String_t** get_address_of_m_tokenStream_8() { return &___m_tokenStream_8; }
	inline void set_m_tokenStream_8(String_t* value)
	{
		___m_tokenStream_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_tokenStream_8), value);
	}

	inline static int32_t get_offset_of_m_value_9() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_value_9)); }
	inline String_t* get_m_value_9() const { return ___m_value_9; }
	inline String_t** get_address_of_m_value_9() { return &___m_value_9; }
	inline void set_m_value_9(String_t* value)
	{
		___m_value_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_value_9), value);
	}
};

struct CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_StaticFields
{
public:
	// System.Net.CookieTokenizer/RecognizedAttribute[] System.Net.CookieTokenizer::RecognizedAttributes
	RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55* ___RecognizedAttributes_10;
	// System.Net.CookieTokenizer/RecognizedAttribute[] System.Net.CookieTokenizer::RecognizedServerAttributes
	RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55* ___RecognizedServerAttributes_11;

public:
	inline static int32_t get_offset_of_RecognizedAttributes_10() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_StaticFields, ___RecognizedAttributes_10)); }
	inline RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55* get_RecognizedAttributes_10() const { return ___RecognizedAttributes_10; }
	inline RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55** get_address_of_RecognizedAttributes_10() { return &___RecognizedAttributes_10; }
	inline void set_RecognizedAttributes_10(RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55* value)
	{
		___RecognizedAttributes_10 = value;
		Il2CppCodeGenWriteBarrier((&___RecognizedAttributes_10), value);
	}

	inline static int32_t get_offset_of_RecognizedServerAttributes_11() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_StaticFields, ___RecognizedServerAttributes_11)); }
	inline RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55* get_RecognizedServerAttributes_11() const { return ___RecognizedServerAttributes_11; }
	inline RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55** get_address_of_RecognizedServerAttributes_11() { return &___RecognizedServerAttributes_11; }
	inline void set_RecognizedServerAttributes_11(RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55* value)
	{
		___RecognizedServerAttributes_11 = value;
		Il2CppCodeGenWriteBarrier((&___RecognizedServerAttributes_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIETOKENIZER_T5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_H
#ifndef RECOGNIZEDATTRIBUTE_T300D9F628CDAED6F665BFE996936B9CE0FA0D95B_H
#define RECOGNIZEDATTRIBUTE_T300D9F628CDAED6F665BFE996936B9CE0FA0D95B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieTokenizer/RecognizedAttribute
struct  RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B 
{
public:
	// System.String System.Net.CookieTokenizer/RecognizedAttribute::m_name
	String_t* ___m_name_0;
	// System.Net.CookieToken System.Net.CookieTokenizer/RecognizedAttribute::m_token
	int32_t ___m_token_1;

public:
	inline static int32_t get_offset_of_m_name_0() { return static_cast<int32_t>(offsetof(RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B, ___m_name_0)); }
	inline String_t* get_m_name_0() const { return ___m_name_0; }
	inline String_t** get_address_of_m_name_0() { return &___m_name_0; }
	inline void set_m_name_0(String_t* value)
	{
		___m_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_0), value);
	}

	inline static int32_t get_offset_of_m_token_1() { return static_cast<int32_t>(offsetof(RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B, ___m_token_1)); }
	inline int32_t get_m_token_1() const { return ___m_token_1; }
	inline int32_t* get_address_of_m_token_1() { return &___m_token_1; }
	inline void set_m_token_1(int32_t value)
	{
		___m_token_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Net.CookieTokenizer/RecognizedAttribute
struct RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B_marshaled_pinvoke
{
	char* ___m_name_0;
	int32_t ___m_token_1;
};
// Native definition for COM marshalling of System.Net.CookieTokenizer/RecognizedAttribute
struct RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B_marshaled_com
{
	Il2CppChar* ___m_name_0;
	int32_t ___m_token_1;
};
#endif // RECOGNIZEDATTRIBUTE_T300D9F628CDAED6F665BFE996936B9CE0FA0D95B_H
#ifndef FILEWEBRESPONSE_T0F58570D82C33733C9D899AB113B862803A5C325_H
#define FILEWEBRESPONSE_T0F58570D82C33733C9D899AB113B862803A5C325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebResponse
struct  FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325  : public WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD
{
public:
	// System.Boolean System.Net.FileWebResponse::m_closed
	bool ___m_closed_1;
	// System.Int64 System.Net.FileWebResponse::m_contentLength
	int64_t ___m_contentLength_2;
	// System.IO.FileAccess System.Net.FileWebResponse::m_fileAccess
	int32_t ___m_fileAccess_3;
	// System.Net.WebHeaderCollection System.Net.FileWebResponse::m_headers
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * ___m_headers_4;
	// System.IO.Stream System.Net.FileWebResponse::m_stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___m_stream_5;
	// System.Uri System.Net.FileWebResponse::m_uri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___m_uri_6;

public:
	inline static int32_t get_offset_of_m_closed_1() { return static_cast<int32_t>(offsetof(FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325, ___m_closed_1)); }
	inline bool get_m_closed_1() const { return ___m_closed_1; }
	inline bool* get_address_of_m_closed_1() { return &___m_closed_1; }
	inline void set_m_closed_1(bool value)
	{
		___m_closed_1 = value;
	}

	inline static int32_t get_offset_of_m_contentLength_2() { return static_cast<int32_t>(offsetof(FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325, ___m_contentLength_2)); }
	inline int64_t get_m_contentLength_2() const { return ___m_contentLength_2; }
	inline int64_t* get_address_of_m_contentLength_2() { return &___m_contentLength_2; }
	inline void set_m_contentLength_2(int64_t value)
	{
		___m_contentLength_2 = value;
	}

	inline static int32_t get_offset_of_m_fileAccess_3() { return static_cast<int32_t>(offsetof(FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325, ___m_fileAccess_3)); }
	inline int32_t get_m_fileAccess_3() const { return ___m_fileAccess_3; }
	inline int32_t* get_address_of_m_fileAccess_3() { return &___m_fileAccess_3; }
	inline void set_m_fileAccess_3(int32_t value)
	{
		___m_fileAccess_3 = value;
	}

	inline static int32_t get_offset_of_m_headers_4() { return static_cast<int32_t>(offsetof(FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325, ___m_headers_4)); }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * get_m_headers_4() const { return ___m_headers_4; }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 ** get_address_of_m_headers_4() { return &___m_headers_4; }
	inline void set_m_headers_4(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * value)
	{
		___m_headers_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_headers_4), value);
	}

	inline static int32_t get_offset_of_m_stream_5() { return static_cast<int32_t>(offsetof(FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325, ___m_stream_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_m_stream_5() const { return ___m_stream_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_m_stream_5() { return &___m_stream_5; }
	inline void set_m_stream_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___m_stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_stream_5), value);
	}

	inline static int32_t get_offset_of_m_uri_6() { return static_cast<int32_t>(offsetof(FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325, ___m_uri_6)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_m_uri_6() const { return ___m_uri_6; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_m_uri_6() { return &___m_uri_6; }
	inline void set_m_uri_6(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___m_uri_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_uri_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBRESPONSE_T0F58570D82C33733C9D899AB113B862803A5C325_H
#ifndef FTPSTATUS_TC736CA78D396A33659145A9183F15038E66B2876_H
#define FTPSTATUS_TC736CA78D396A33659145A9183F15038E66B2876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpStatus
struct  FtpStatus_tC736CA78D396A33659145A9183F15038E66B2876  : public RuntimeObject
{
public:
	// System.Net.FtpStatusCode System.Net.FtpStatus::statusCode
	int32_t ___statusCode_0;
	// System.String System.Net.FtpStatus::statusDescription
	String_t* ___statusDescription_1;

public:
	inline static int32_t get_offset_of_statusCode_0() { return static_cast<int32_t>(offsetof(FtpStatus_tC736CA78D396A33659145A9183F15038E66B2876, ___statusCode_0)); }
	inline int32_t get_statusCode_0() const { return ___statusCode_0; }
	inline int32_t* get_address_of_statusCode_0() { return &___statusCode_0; }
	inline void set_statusCode_0(int32_t value)
	{
		___statusCode_0 = value;
	}

	inline static int32_t get_offset_of_statusDescription_1() { return static_cast<int32_t>(offsetof(FtpStatus_tC736CA78D396A33659145A9183F15038E66B2876, ___statusDescription_1)); }
	inline String_t* get_statusDescription_1() const { return ___statusDescription_1; }
	inline String_t** get_address_of_statusDescription_1() { return &___statusDescription_1; }
	inline void set_statusDescription_1(String_t* value)
	{
		___statusDescription_1 = value;
		Il2CppCodeGenWriteBarrier((&___statusDescription_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPSTATUS_TC736CA78D396A33659145A9183F15038E66B2876_H
#ifndef FTPWEBRESPONSE_T8775110950F0637C1A8A495892122865F05FC205_H
#define FTPWEBRESPONSE_T8775110950F0637C1A8A495892122865F05FC205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpWebResponse
struct  FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205  : public WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD
{
public:
	// System.IO.Stream System.Net.FtpWebResponse::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_1;
	// System.Uri System.Net.FtpWebResponse::uri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___uri_2;
	// System.Net.FtpStatusCode System.Net.FtpWebResponse::statusCode
	int32_t ___statusCode_3;
	// System.DateTime System.Net.FtpWebResponse::lastModified
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___lastModified_4;
	// System.String System.Net.FtpWebResponse::bannerMessage
	String_t* ___bannerMessage_5;
	// System.String System.Net.FtpWebResponse::welcomeMessage
	String_t* ___welcomeMessage_6;
	// System.String System.Net.FtpWebResponse::exitMessage
	String_t* ___exitMessage_7;
	// System.String System.Net.FtpWebResponse::statusDescription
	String_t* ___statusDescription_8;
	// System.String System.Net.FtpWebResponse::method
	String_t* ___method_9;
	// System.Boolean System.Net.FtpWebResponse::disposed
	bool ___disposed_10;
	// System.Net.FtpWebRequest System.Net.FtpWebResponse::request
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA * ___request_11;
	// System.Int64 System.Net.FtpWebResponse::contentLength
	int64_t ___contentLength_12;

public:
	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___stream_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_1() const { return ___stream_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___stream_1), value);
	}

	inline static int32_t get_offset_of_uri_2() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___uri_2)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_uri_2() const { return ___uri_2; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_uri_2() { return &___uri_2; }
	inline void set_uri_2(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___uri_2 = value;
		Il2CppCodeGenWriteBarrier((&___uri_2), value);
	}

	inline static int32_t get_offset_of_statusCode_3() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___statusCode_3)); }
	inline int32_t get_statusCode_3() const { return ___statusCode_3; }
	inline int32_t* get_address_of_statusCode_3() { return &___statusCode_3; }
	inline void set_statusCode_3(int32_t value)
	{
		___statusCode_3 = value;
	}

	inline static int32_t get_offset_of_lastModified_4() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___lastModified_4)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_lastModified_4() const { return ___lastModified_4; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_lastModified_4() { return &___lastModified_4; }
	inline void set_lastModified_4(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___lastModified_4 = value;
	}

	inline static int32_t get_offset_of_bannerMessage_5() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___bannerMessage_5)); }
	inline String_t* get_bannerMessage_5() const { return ___bannerMessage_5; }
	inline String_t** get_address_of_bannerMessage_5() { return &___bannerMessage_5; }
	inline void set_bannerMessage_5(String_t* value)
	{
		___bannerMessage_5 = value;
		Il2CppCodeGenWriteBarrier((&___bannerMessage_5), value);
	}

	inline static int32_t get_offset_of_welcomeMessage_6() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___welcomeMessage_6)); }
	inline String_t* get_welcomeMessage_6() const { return ___welcomeMessage_6; }
	inline String_t** get_address_of_welcomeMessage_6() { return &___welcomeMessage_6; }
	inline void set_welcomeMessage_6(String_t* value)
	{
		___welcomeMessage_6 = value;
		Il2CppCodeGenWriteBarrier((&___welcomeMessage_6), value);
	}

	inline static int32_t get_offset_of_exitMessage_7() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___exitMessage_7)); }
	inline String_t* get_exitMessage_7() const { return ___exitMessage_7; }
	inline String_t** get_address_of_exitMessage_7() { return &___exitMessage_7; }
	inline void set_exitMessage_7(String_t* value)
	{
		___exitMessage_7 = value;
		Il2CppCodeGenWriteBarrier((&___exitMessage_7), value);
	}

	inline static int32_t get_offset_of_statusDescription_8() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___statusDescription_8)); }
	inline String_t* get_statusDescription_8() const { return ___statusDescription_8; }
	inline String_t** get_address_of_statusDescription_8() { return &___statusDescription_8; }
	inline void set_statusDescription_8(String_t* value)
	{
		___statusDescription_8 = value;
		Il2CppCodeGenWriteBarrier((&___statusDescription_8), value);
	}

	inline static int32_t get_offset_of_method_9() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___method_9)); }
	inline String_t* get_method_9() const { return ___method_9; }
	inline String_t** get_address_of_method_9() { return &___method_9; }
	inline void set_method_9(String_t* value)
	{
		___method_9 = value;
		Il2CppCodeGenWriteBarrier((&___method_9), value);
	}

	inline static int32_t get_offset_of_disposed_10() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___disposed_10)); }
	inline bool get_disposed_10() const { return ___disposed_10; }
	inline bool* get_address_of_disposed_10() { return &___disposed_10; }
	inline void set_disposed_10(bool value)
	{
		___disposed_10 = value;
	}

	inline static int32_t get_offset_of_request_11() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___request_11)); }
	inline FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA * get_request_11() const { return ___request_11; }
	inline FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA ** get_address_of_request_11() { return &___request_11; }
	inline void set_request_11(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA * value)
	{
		___request_11 = value;
		Il2CppCodeGenWriteBarrier((&___request_11), value);
	}

	inline static int32_t get_offset_of_contentLength_12() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___contentLength_12)); }
	inline int64_t get_contentLength_12() const { return ___contentLength_12; }
	inline int64_t* get_address_of_contentLength_12() { return &___contentLength_12; }
	inline void set_contentLength_12(int64_t value)
	{
		___contentLength_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPWEBRESPONSE_T8775110950F0637C1A8A495892122865F05FC205_H
#ifndef HEADERVARIANTINFO_TFF12EDB71F2B9508779B160689F99BA209DA9E64_H
#define HEADERVARIANTINFO_TFF12EDB71F2B9508779B160689F99BA209DA9E64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HeaderVariantInfo
struct  HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64 
{
public:
	// System.String System.Net.HeaderVariantInfo::m_name
	String_t* ___m_name_0;
	// System.Net.CookieVariant System.Net.HeaderVariantInfo::m_variant
	int32_t ___m_variant_1;

public:
	inline static int32_t get_offset_of_m_name_0() { return static_cast<int32_t>(offsetof(HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64, ___m_name_0)); }
	inline String_t* get_m_name_0() const { return ___m_name_0; }
	inline String_t** get_address_of_m_name_0() { return &___m_name_0; }
	inline void set_m_name_0(String_t* value)
	{
		___m_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_0), value);
	}

	inline static int32_t get_offset_of_m_variant_1() { return static_cast<int32_t>(offsetof(HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64, ___m_variant_1)); }
	inline int32_t get_m_variant_1() const { return ___m_variant_1; }
	inline int32_t* get_address_of_m_variant_1() { return &___m_variant_1; }
	inline void set_m_variant_1(int32_t value)
	{
		___m_variant_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Net.HeaderVariantInfo
struct HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64_marshaled_pinvoke
{
	char* ___m_name_0;
	int32_t ___m_variant_1;
};
// Native definition for COM marshalling of System.Net.HeaderVariantInfo
struct HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64_marshaled_com
{
	Il2CppChar* ___m_name_0;
	int32_t ___m_variant_1;
};
#endif // HEADERVARIANTINFO_TFF12EDB71F2B9508779B160689F99BA209DA9E64_H
#ifndef HTTPCONNECTION_TD93CEDEFFD0DCBC29F9B704487A0A25D065D466E_H
#define HTTPCONNECTION_TD93CEDEFFD0DCBC29F9B704487A0A25D065D466E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpConnection
struct  HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E  : public RuntimeObject
{
public:
	// System.Net.Sockets.Socket System.Net.HttpConnection::sock
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___sock_1;
	// System.IO.Stream System.Net.HttpConnection::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_2;
	// System.Net.EndPointListener System.Net.HttpConnection::epl
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2 * ___epl_3;
	// System.IO.MemoryStream System.Net.HttpConnection::ms
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___ms_4;
	// System.Byte[] System.Net.HttpConnection::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_5;
	// System.Net.HttpListenerContext System.Net.HttpConnection::context
	HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * ___context_6;
	// System.Text.StringBuilder System.Net.HttpConnection::current_line
	StringBuilder_t * ___current_line_7;
	// System.Net.ListenerPrefix System.Net.HttpConnection::prefix
	ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7 * ___prefix_8;
	// System.Net.RequestStream System.Net.HttpConnection::i_stream
	RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35 * ___i_stream_9;
	// System.Net.ResponseStream System.Net.HttpConnection::o_stream
	ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032 * ___o_stream_10;
	// System.Boolean System.Net.HttpConnection::chunked
	bool ___chunked_11;
	// System.Int32 System.Net.HttpConnection::reuses
	int32_t ___reuses_12;
	// System.Boolean System.Net.HttpConnection::context_bound
	bool ___context_bound_13;
	// System.Boolean System.Net.HttpConnection::secure
	bool ___secure_14;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.HttpConnection::cert
	X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * ___cert_15;
	// System.Int32 System.Net.HttpConnection::s_timeout
	int32_t ___s_timeout_16;
	// System.Threading.Timer System.Net.HttpConnection::timer
	Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 * ___timer_17;
	// System.Net.IPEndPoint System.Net.HttpConnection::local_ep
	IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * ___local_ep_18;
	// System.Net.HttpListener System.Net.HttpConnection::last_listener
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * ___last_listener_19;
	// System.Int32[] System.Net.HttpConnection::client_cert_errors
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___client_cert_errors_20;
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.Net.HttpConnection::client_cert
	X509Certificate2_tC1C49EB4CFD571C2FFDE940C24BC69651A058F73 * ___client_cert_21;
	// System.Net.Security.SslStream System.Net.HttpConnection::ssl_stream
	SslStream_t9CEE8F6E125C734DD807D9289C86860FFEE81087 * ___ssl_stream_22;
	// System.Net.HttpConnection/InputState System.Net.HttpConnection::input_state
	int32_t ___input_state_23;
	// System.Net.HttpConnection/LineState System.Net.HttpConnection::line_state
	int32_t ___line_state_24;
	// System.Int32 System.Net.HttpConnection::position
	int32_t ___position_25;

public:
	inline static int32_t get_offset_of_sock_1() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___sock_1)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_sock_1() const { return ___sock_1; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_sock_1() { return &___sock_1; }
	inline void set_sock_1(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___sock_1 = value;
		Il2CppCodeGenWriteBarrier((&___sock_1), value);
	}

	inline static int32_t get_offset_of_stream_2() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___stream_2)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_2() const { return ___stream_2; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_2() { return &___stream_2; }
	inline void set_stream_2(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_2 = value;
		Il2CppCodeGenWriteBarrier((&___stream_2), value);
	}

	inline static int32_t get_offset_of_epl_3() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___epl_3)); }
	inline EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2 * get_epl_3() const { return ___epl_3; }
	inline EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2 ** get_address_of_epl_3() { return &___epl_3; }
	inline void set_epl_3(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2 * value)
	{
		___epl_3 = value;
		Il2CppCodeGenWriteBarrier((&___epl_3), value);
	}

	inline static int32_t get_offset_of_ms_4() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___ms_4)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_ms_4() const { return ___ms_4; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_ms_4() { return &___ms_4; }
	inline void set_ms_4(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___ms_4 = value;
		Il2CppCodeGenWriteBarrier((&___ms_4), value);
	}

	inline static int32_t get_offset_of_buffer_5() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___buffer_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_5() const { return ___buffer_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_5() { return &___buffer_5; }
	inline void set_buffer_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_5), value);
	}

	inline static int32_t get_offset_of_context_6() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___context_6)); }
	inline HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * get_context_6() const { return ___context_6; }
	inline HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA ** get_address_of_context_6() { return &___context_6; }
	inline void set_context_6(HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * value)
	{
		___context_6 = value;
		Il2CppCodeGenWriteBarrier((&___context_6), value);
	}

	inline static int32_t get_offset_of_current_line_7() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___current_line_7)); }
	inline StringBuilder_t * get_current_line_7() const { return ___current_line_7; }
	inline StringBuilder_t ** get_address_of_current_line_7() { return &___current_line_7; }
	inline void set_current_line_7(StringBuilder_t * value)
	{
		___current_line_7 = value;
		Il2CppCodeGenWriteBarrier((&___current_line_7), value);
	}

	inline static int32_t get_offset_of_prefix_8() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___prefix_8)); }
	inline ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7 * get_prefix_8() const { return ___prefix_8; }
	inline ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7 ** get_address_of_prefix_8() { return &___prefix_8; }
	inline void set_prefix_8(ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7 * value)
	{
		___prefix_8 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_8), value);
	}

	inline static int32_t get_offset_of_i_stream_9() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___i_stream_9)); }
	inline RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35 * get_i_stream_9() const { return ___i_stream_9; }
	inline RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35 ** get_address_of_i_stream_9() { return &___i_stream_9; }
	inline void set_i_stream_9(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35 * value)
	{
		___i_stream_9 = value;
		Il2CppCodeGenWriteBarrier((&___i_stream_9), value);
	}

	inline static int32_t get_offset_of_o_stream_10() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___o_stream_10)); }
	inline ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032 * get_o_stream_10() const { return ___o_stream_10; }
	inline ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032 ** get_address_of_o_stream_10() { return &___o_stream_10; }
	inline void set_o_stream_10(ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032 * value)
	{
		___o_stream_10 = value;
		Il2CppCodeGenWriteBarrier((&___o_stream_10), value);
	}

	inline static int32_t get_offset_of_chunked_11() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___chunked_11)); }
	inline bool get_chunked_11() const { return ___chunked_11; }
	inline bool* get_address_of_chunked_11() { return &___chunked_11; }
	inline void set_chunked_11(bool value)
	{
		___chunked_11 = value;
	}

	inline static int32_t get_offset_of_reuses_12() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___reuses_12)); }
	inline int32_t get_reuses_12() const { return ___reuses_12; }
	inline int32_t* get_address_of_reuses_12() { return &___reuses_12; }
	inline void set_reuses_12(int32_t value)
	{
		___reuses_12 = value;
	}

	inline static int32_t get_offset_of_context_bound_13() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___context_bound_13)); }
	inline bool get_context_bound_13() const { return ___context_bound_13; }
	inline bool* get_address_of_context_bound_13() { return &___context_bound_13; }
	inline void set_context_bound_13(bool value)
	{
		___context_bound_13 = value;
	}

	inline static int32_t get_offset_of_secure_14() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___secure_14)); }
	inline bool get_secure_14() const { return ___secure_14; }
	inline bool* get_address_of_secure_14() { return &___secure_14; }
	inline void set_secure_14(bool value)
	{
		___secure_14 = value;
	}

	inline static int32_t get_offset_of_cert_15() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___cert_15)); }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * get_cert_15() const { return ___cert_15; }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 ** get_address_of_cert_15() { return &___cert_15; }
	inline void set_cert_15(X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * value)
	{
		___cert_15 = value;
		Il2CppCodeGenWriteBarrier((&___cert_15), value);
	}

	inline static int32_t get_offset_of_s_timeout_16() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___s_timeout_16)); }
	inline int32_t get_s_timeout_16() const { return ___s_timeout_16; }
	inline int32_t* get_address_of_s_timeout_16() { return &___s_timeout_16; }
	inline void set_s_timeout_16(int32_t value)
	{
		___s_timeout_16 = value;
	}

	inline static int32_t get_offset_of_timer_17() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___timer_17)); }
	inline Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 * get_timer_17() const { return ___timer_17; }
	inline Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 ** get_address_of_timer_17() { return &___timer_17; }
	inline void set_timer_17(Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 * value)
	{
		___timer_17 = value;
		Il2CppCodeGenWriteBarrier((&___timer_17), value);
	}

	inline static int32_t get_offset_of_local_ep_18() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___local_ep_18)); }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * get_local_ep_18() const { return ___local_ep_18; }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F ** get_address_of_local_ep_18() { return &___local_ep_18; }
	inline void set_local_ep_18(IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * value)
	{
		___local_ep_18 = value;
		Il2CppCodeGenWriteBarrier((&___local_ep_18), value);
	}

	inline static int32_t get_offset_of_last_listener_19() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___last_listener_19)); }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * get_last_listener_19() const { return ___last_listener_19; }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E ** get_address_of_last_listener_19() { return &___last_listener_19; }
	inline void set_last_listener_19(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * value)
	{
		___last_listener_19 = value;
		Il2CppCodeGenWriteBarrier((&___last_listener_19), value);
	}

	inline static int32_t get_offset_of_client_cert_errors_20() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___client_cert_errors_20)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_client_cert_errors_20() const { return ___client_cert_errors_20; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_client_cert_errors_20() { return &___client_cert_errors_20; }
	inline void set_client_cert_errors_20(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___client_cert_errors_20 = value;
		Il2CppCodeGenWriteBarrier((&___client_cert_errors_20), value);
	}

	inline static int32_t get_offset_of_client_cert_21() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___client_cert_21)); }
	inline X509Certificate2_tC1C49EB4CFD571C2FFDE940C24BC69651A058F73 * get_client_cert_21() const { return ___client_cert_21; }
	inline X509Certificate2_tC1C49EB4CFD571C2FFDE940C24BC69651A058F73 ** get_address_of_client_cert_21() { return &___client_cert_21; }
	inline void set_client_cert_21(X509Certificate2_tC1C49EB4CFD571C2FFDE940C24BC69651A058F73 * value)
	{
		___client_cert_21 = value;
		Il2CppCodeGenWriteBarrier((&___client_cert_21), value);
	}

	inline static int32_t get_offset_of_ssl_stream_22() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___ssl_stream_22)); }
	inline SslStream_t9CEE8F6E125C734DD807D9289C86860FFEE81087 * get_ssl_stream_22() const { return ___ssl_stream_22; }
	inline SslStream_t9CEE8F6E125C734DD807D9289C86860FFEE81087 ** get_address_of_ssl_stream_22() { return &___ssl_stream_22; }
	inline void set_ssl_stream_22(SslStream_t9CEE8F6E125C734DD807D9289C86860FFEE81087 * value)
	{
		___ssl_stream_22 = value;
		Il2CppCodeGenWriteBarrier((&___ssl_stream_22), value);
	}

	inline static int32_t get_offset_of_input_state_23() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___input_state_23)); }
	inline int32_t get_input_state_23() const { return ___input_state_23; }
	inline int32_t* get_address_of_input_state_23() { return &___input_state_23; }
	inline void set_input_state_23(int32_t value)
	{
		___input_state_23 = value;
	}

	inline static int32_t get_offset_of_line_state_24() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___line_state_24)); }
	inline int32_t get_line_state_24() const { return ___line_state_24; }
	inline int32_t* get_address_of_line_state_24() { return &___line_state_24; }
	inline void set_line_state_24(int32_t value)
	{
		___line_state_24 = value;
	}

	inline static int32_t get_offset_of_position_25() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___position_25)); }
	inline int32_t get_position_25() const { return ___position_25; }
	inline int32_t* get_address_of_position_25() { return &___position_25; }
	inline void set_position_25(int32_t value)
	{
		___position_25 = value;
	}
};

struct HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E_StaticFields
{
public:
	// System.AsyncCallback System.Net.HttpConnection::onread_cb
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___onread_cb_0;

public:
	inline static int32_t get_offset_of_onread_cb_0() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E_StaticFields, ___onread_cb_0)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_onread_cb_0() const { return ___onread_cb_0; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_onread_cb_0() { return &___onread_cb_0; }
	inline void set_onread_cb_0(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___onread_cb_0 = value;
		Il2CppCodeGenWriteBarrier((&___onread_cb_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCONNECTION_TD93CEDEFFD0DCBC29F9B704487A0A25D065D466E_H
#ifndef PROTOCOLVIOLATIONEXCEPTION_T287E1EFCC1BC7BB76C74A681581BF3A67C68BDFB_H
#define PROTOCOLVIOLATIONEXCEPTION_T287E1EFCC1BC7BB76C74A681581BF3A67C68BDFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ProtocolViolationException
struct  ProtocolViolationException_t287E1EFCC1BC7BB76C74A681581BF3A67C68BDFB  : public InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLVIOLATIONEXCEPTION_T287E1EFCC1BC7BB76C74A681581BF3A67C68BDFB_H
#ifndef CALLBACKCONTEXT_T1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8_H
#define CALLBACKCONTEXT_T1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServerCertValidationCallback/CallbackContext
struct  CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8  : public RuntimeObject
{
public:
	// System.Object System.Net.ServerCertValidationCallback/CallbackContext::request
	RuntimeObject * ___request_0;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.ServerCertValidationCallback/CallbackContext::certificate
	X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * ___certificate_1;
	// System.Security.Cryptography.X509Certificates.X509Chain System.Net.ServerCertValidationCallback/CallbackContext::chain
	X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 * ___chain_2;
	// System.Net.Security.SslPolicyErrors System.Net.ServerCertValidationCallback/CallbackContext::sslPolicyErrors
	int32_t ___sslPolicyErrors_3;
	// System.Boolean System.Net.ServerCertValidationCallback/CallbackContext::result
	bool ___result_4;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8, ___request_0)); }
	inline RuntimeObject * get_request_0() const { return ___request_0; }
	inline RuntimeObject ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(RuntimeObject * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_certificate_1() { return static_cast<int32_t>(offsetof(CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8, ___certificate_1)); }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * get_certificate_1() const { return ___certificate_1; }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 ** get_address_of_certificate_1() { return &___certificate_1; }
	inline void set_certificate_1(X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * value)
	{
		___certificate_1 = value;
		Il2CppCodeGenWriteBarrier((&___certificate_1), value);
	}

	inline static int32_t get_offset_of_chain_2() { return static_cast<int32_t>(offsetof(CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8, ___chain_2)); }
	inline X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 * get_chain_2() const { return ___chain_2; }
	inline X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 ** get_address_of_chain_2() { return &___chain_2; }
	inline void set_chain_2(X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 * value)
	{
		___chain_2 = value;
		Il2CppCodeGenWriteBarrier((&___chain_2), value);
	}

	inline static int32_t get_offset_of_sslPolicyErrors_3() { return static_cast<int32_t>(offsetof(CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8, ___sslPolicyErrors_3)); }
	inline int32_t get_sslPolicyErrors_3() const { return ___sslPolicyErrors_3; }
	inline int32_t* get_address_of_sslPolicyErrors_3() { return &___sslPolicyErrors_3; }
	inline void set_sslPolicyErrors_3(int32_t value)
	{
		___sslPolicyErrors_3 = value;
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8, ___result_4)); }
	inline bool get_result_4() const { return ___result_4; }
	inline bool* get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(bool value)
	{
		___result_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKCONTEXT_T1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8_H
#ifndef TIMERNODE_T5596C0BAF3E85D0C2444ECA74917630FDEF5087B_H
#define TIMERNODE_T5596C0BAF3E85D0C2444ECA74917630FDEF5087B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread/TimerNode
struct  TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B  : public Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F
{
public:
	// System.Net.TimerThread/TimerNode/TimerState System.Net.TimerThread/TimerNode::m_TimerState
	int32_t ___m_TimerState_2;
	// System.Net.TimerThread/Callback System.Net.TimerThread/TimerNode::m_Callback
	Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 * ___m_Callback_3;
	// System.Object System.Net.TimerThread/TimerNode::m_Context
	RuntimeObject * ___m_Context_4;
	// System.Object System.Net.TimerThread/TimerNode::m_QueueLock
	RuntimeObject * ___m_QueueLock_5;
	// System.Net.TimerThread/TimerNode System.Net.TimerThread/TimerNode::next
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * ___next_6;
	// System.Net.TimerThread/TimerNode System.Net.TimerThread/TimerNode::prev
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * ___prev_7;

public:
	inline static int32_t get_offset_of_m_TimerState_2() { return static_cast<int32_t>(offsetof(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B, ___m_TimerState_2)); }
	inline int32_t get_m_TimerState_2() const { return ___m_TimerState_2; }
	inline int32_t* get_address_of_m_TimerState_2() { return &___m_TimerState_2; }
	inline void set_m_TimerState_2(int32_t value)
	{
		___m_TimerState_2 = value;
	}

	inline static int32_t get_offset_of_m_Callback_3() { return static_cast<int32_t>(offsetof(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B, ___m_Callback_3)); }
	inline Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 * get_m_Callback_3() const { return ___m_Callback_3; }
	inline Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 ** get_address_of_m_Callback_3() { return &___m_Callback_3; }
	inline void set_m_Callback_3(Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 * value)
	{
		___m_Callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_3), value);
	}

	inline static int32_t get_offset_of_m_Context_4() { return static_cast<int32_t>(offsetof(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B, ___m_Context_4)); }
	inline RuntimeObject * get_m_Context_4() const { return ___m_Context_4; }
	inline RuntimeObject ** get_address_of_m_Context_4() { return &___m_Context_4; }
	inline void set_m_Context_4(RuntimeObject * value)
	{
		___m_Context_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Context_4), value);
	}

	inline static int32_t get_offset_of_m_QueueLock_5() { return static_cast<int32_t>(offsetof(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B, ___m_QueueLock_5)); }
	inline RuntimeObject * get_m_QueueLock_5() const { return ___m_QueueLock_5; }
	inline RuntimeObject ** get_address_of_m_QueueLock_5() { return &___m_QueueLock_5; }
	inline void set_m_QueueLock_5(RuntimeObject * value)
	{
		___m_QueueLock_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_QueueLock_5), value);
	}

	inline static int32_t get_offset_of_next_6() { return static_cast<int32_t>(offsetof(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B, ___next_6)); }
	inline TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * get_next_6() const { return ___next_6; }
	inline TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B ** get_address_of_next_6() { return &___next_6; }
	inline void set_next_6(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * value)
	{
		___next_6 = value;
		Il2CppCodeGenWriteBarrier((&___next_6), value);
	}

	inline static int32_t get_offset_of_prev_7() { return static_cast<int32_t>(offsetof(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B, ___prev_7)); }
	inline TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * get_prev_7() const { return ___prev_7; }
	inline TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B ** get_address_of_prev_7() { return &___prev_7; }
	inline void set_prev_7(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * value)
	{
		___prev_7 = value;
		Il2CppCodeGenWriteBarrier((&___prev_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERNODE_T5596C0BAF3E85D0C2444ECA74917630FDEF5087B_H
#ifndef WEBEXCEPTION_TD400C9DEBEBB6AEDA77500E634D20692E27A993D_H
#define WEBEXCEPTION_TD400C9DEBEBB6AEDA77500E634D20692E27A993D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebException
struct  WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D  : public InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1
{
public:
	// System.Net.WebExceptionStatus System.Net.WebException::m_Status
	int32_t ___m_Status_17;
	// System.Net.WebResponse System.Net.WebException::m_Response
	WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * ___m_Response_18;
	// System.Net.WebExceptionInternalStatus System.Net.WebException::m_InternalStatus
	int32_t ___m_InternalStatus_19;

public:
	inline static int32_t get_offset_of_m_Status_17() { return static_cast<int32_t>(offsetof(WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D, ___m_Status_17)); }
	inline int32_t get_m_Status_17() const { return ___m_Status_17; }
	inline int32_t* get_address_of_m_Status_17() { return &___m_Status_17; }
	inline void set_m_Status_17(int32_t value)
	{
		___m_Status_17 = value;
	}

	inline static int32_t get_offset_of_m_Response_18() { return static_cast<int32_t>(offsetof(WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D, ___m_Response_18)); }
	inline WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * get_m_Response_18() const { return ___m_Response_18; }
	inline WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD ** get_address_of_m_Response_18() { return &___m_Response_18; }
	inline void set_m_Response_18(WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * value)
	{
		___m_Response_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Response_18), value);
	}

	inline static int32_t get_offset_of_m_InternalStatus_19() { return static_cast<int32_t>(offsetof(WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D, ___m_InternalStatus_19)); }
	inline int32_t get_m_InternalStatus_19() const { return ___m_InternalStatus_19; }
	inline int32_t* get_address_of_m_InternalStatus_19() { return &___m_InternalStatus_19; }
	inline void set_m_InternalStatus_19(int32_t value)
	{
		___m_InternalStatus_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTION_TD400C9DEBEBB6AEDA77500E634D20692E27A993D_H
#ifndef WEBHEADERCOLLECTION_TB57EC4CD795CACE87271D6887BBED385DC37B304_H
#define WEBHEADERCOLLECTION_TB57EC4CD795CACE87271D6887BBED385DC37B304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebHeaderCollection
struct  WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304  : public NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1
{
public:
	// System.String[] System.Net.WebHeaderCollection::m_CommonHeaders
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_CommonHeaders_12;
	// System.Int32 System.Net.WebHeaderCollection::m_NumCommonHeaders
	int32_t ___m_NumCommonHeaders_13;
	// System.Collections.Specialized.NameValueCollection System.Net.WebHeaderCollection::m_InnerCollection
	NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 * ___m_InnerCollection_16;
	// System.Net.WebHeaderCollectionType System.Net.WebHeaderCollection::m_Type
	uint16_t ___m_Type_17;

public:
	inline static int32_t get_offset_of_m_CommonHeaders_12() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304, ___m_CommonHeaders_12)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_CommonHeaders_12() const { return ___m_CommonHeaders_12; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_CommonHeaders_12() { return &___m_CommonHeaders_12; }
	inline void set_m_CommonHeaders_12(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_CommonHeaders_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_CommonHeaders_12), value);
	}

	inline static int32_t get_offset_of_m_NumCommonHeaders_13() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304, ___m_NumCommonHeaders_13)); }
	inline int32_t get_m_NumCommonHeaders_13() const { return ___m_NumCommonHeaders_13; }
	inline int32_t* get_address_of_m_NumCommonHeaders_13() { return &___m_NumCommonHeaders_13; }
	inline void set_m_NumCommonHeaders_13(int32_t value)
	{
		___m_NumCommonHeaders_13 = value;
	}

	inline static int32_t get_offset_of_m_InnerCollection_16() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304, ___m_InnerCollection_16)); }
	inline NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 * get_m_InnerCollection_16() const { return ___m_InnerCollection_16; }
	inline NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 ** get_address_of_m_InnerCollection_16() { return &___m_InnerCollection_16; }
	inline void set_m_InnerCollection_16(NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 * value)
	{
		___m_InnerCollection_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_InnerCollection_16), value);
	}

	inline static int32_t get_offset_of_m_Type_17() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304, ___m_Type_17)); }
	inline uint16_t get_m_Type_17() const { return ___m_Type_17; }
	inline uint16_t* get_address_of_m_Type_17() { return &___m_Type_17; }
	inline void set_m_Type_17(uint16_t value)
	{
		___m_Type_17 = value;
	}
};

struct WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields
{
public:
	// System.Net.HeaderInfoTable System.Net.WebHeaderCollection::HInfo
	HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF * ___HInfo_11;
	// System.String[] System.Net.WebHeaderCollection::s_CommonHeaderNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___s_CommonHeaderNames_14;
	// System.SByte[] System.Net.WebHeaderCollection::s_CommonHeaderHints
	SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* ___s_CommonHeaderHints_15;
	// System.Char[] System.Net.WebHeaderCollection::HttpTrimCharacters
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___HttpTrimCharacters_18;
	// System.Net.WebHeaderCollection/RfcChar[] System.Net.WebHeaderCollection::RfcCharMap
	RfcCharU5BU5D_t27AD0ADBD612E10FCEF4917B5E70094398C6EC4E* ___RfcCharMap_19;

public:
	inline static int32_t get_offset_of_HInfo_11() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields, ___HInfo_11)); }
	inline HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF * get_HInfo_11() const { return ___HInfo_11; }
	inline HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF ** get_address_of_HInfo_11() { return &___HInfo_11; }
	inline void set_HInfo_11(HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF * value)
	{
		___HInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___HInfo_11), value);
	}

	inline static int32_t get_offset_of_s_CommonHeaderNames_14() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields, ___s_CommonHeaderNames_14)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_s_CommonHeaderNames_14() const { return ___s_CommonHeaderNames_14; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_s_CommonHeaderNames_14() { return &___s_CommonHeaderNames_14; }
	inline void set_s_CommonHeaderNames_14(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___s_CommonHeaderNames_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_CommonHeaderNames_14), value);
	}

	inline static int32_t get_offset_of_s_CommonHeaderHints_15() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields, ___s_CommonHeaderHints_15)); }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* get_s_CommonHeaderHints_15() const { return ___s_CommonHeaderHints_15; }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889** get_address_of_s_CommonHeaderHints_15() { return &___s_CommonHeaderHints_15; }
	inline void set_s_CommonHeaderHints_15(SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* value)
	{
		___s_CommonHeaderHints_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_CommonHeaderHints_15), value);
	}

	inline static int32_t get_offset_of_HttpTrimCharacters_18() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields, ___HttpTrimCharacters_18)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_HttpTrimCharacters_18() const { return ___HttpTrimCharacters_18; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_HttpTrimCharacters_18() { return &___HttpTrimCharacters_18; }
	inline void set_HttpTrimCharacters_18(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___HttpTrimCharacters_18 = value;
		Il2CppCodeGenWriteBarrier((&___HttpTrimCharacters_18), value);
	}

	inline static int32_t get_offset_of_RfcCharMap_19() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields, ___RfcCharMap_19)); }
	inline RfcCharU5BU5D_t27AD0ADBD612E10FCEF4917B5E70094398C6EC4E* get_RfcCharMap_19() const { return ___RfcCharMap_19; }
	inline RfcCharU5BU5D_t27AD0ADBD612E10FCEF4917B5E70094398C6EC4E** get_address_of_RfcCharMap_19() { return &___RfcCharMap_19; }
	inline void set_RfcCharMap_19(RfcCharU5BU5D_t27AD0ADBD612E10FCEF4917B5E70094398C6EC4E* value)
	{
		___RfcCharMap_19 = value;
		Il2CppCodeGenWriteBarrier((&___RfcCharMap_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBHEADERCOLLECTION_TB57EC4CD795CACE87271D6887BBED385DC37B304_H
#ifndef WEBREQUEST_T5668DA48802E9FE2F1DE5F5A770B218608B918C8_H
#define WEBREQUEST_T5668DA48802E9FE2F1DE5F5A770B218608B918C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest
struct  WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.Net.Security.AuthenticationLevel System.Net.WebRequest::m_AuthenticationLevel
	int32_t ___m_AuthenticationLevel_4;
	// System.Security.Principal.TokenImpersonationLevel System.Net.WebRequest::m_ImpersonationLevel
	int32_t ___m_ImpersonationLevel_5;
	// System.Net.Cache.RequestCachePolicy System.Net.WebRequest::m_CachePolicy
	RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * ___m_CachePolicy_6;
	// System.Net.Cache.RequestCacheProtocol System.Net.WebRequest::m_CacheProtocol
	RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D * ___m_CacheProtocol_7;
	// System.Net.Cache.RequestCacheBinding System.Net.WebRequest::m_CacheBinding
	RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 * ___m_CacheBinding_8;

public:
	inline static int32_t get_offset_of_m_AuthenticationLevel_4() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_AuthenticationLevel_4)); }
	inline int32_t get_m_AuthenticationLevel_4() const { return ___m_AuthenticationLevel_4; }
	inline int32_t* get_address_of_m_AuthenticationLevel_4() { return &___m_AuthenticationLevel_4; }
	inline void set_m_AuthenticationLevel_4(int32_t value)
	{
		___m_AuthenticationLevel_4 = value;
	}

	inline static int32_t get_offset_of_m_ImpersonationLevel_5() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_ImpersonationLevel_5)); }
	inline int32_t get_m_ImpersonationLevel_5() const { return ___m_ImpersonationLevel_5; }
	inline int32_t* get_address_of_m_ImpersonationLevel_5() { return &___m_ImpersonationLevel_5; }
	inline void set_m_ImpersonationLevel_5(int32_t value)
	{
		___m_ImpersonationLevel_5 = value;
	}

	inline static int32_t get_offset_of_m_CachePolicy_6() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_CachePolicy_6)); }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * get_m_CachePolicy_6() const { return ___m_CachePolicy_6; }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 ** get_address_of_m_CachePolicy_6() { return &___m_CachePolicy_6; }
	inline void set_m_CachePolicy_6(RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * value)
	{
		___m_CachePolicy_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachePolicy_6), value);
	}

	inline static int32_t get_offset_of_m_CacheProtocol_7() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_CacheProtocol_7)); }
	inline RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D * get_m_CacheProtocol_7() const { return ___m_CacheProtocol_7; }
	inline RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D ** get_address_of_m_CacheProtocol_7() { return &___m_CacheProtocol_7; }
	inline void set_m_CacheProtocol_7(RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D * value)
	{
		___m_CacheProtocol_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheProtocol_7), value);
	}

	inline static int32_t get_offset_of_m_CacheBinding_8() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_CacheBinding_8)); }
	inline RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 * get_m_CacheBinding_8() const { return ___m_CacheBinding_8; }
	inline RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 ** get_address_of_m_CacheBinding_8() { return &___m_CacheBinding_8; }
	inline void set_m_CacheBinding_8(RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 * value)
	{
		___m_CacheBinding_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheBinding_8), value);
	}
};

struct WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields
{
public:
	// System.Collections.ArrayList modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_PrefixList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___s_PrefixList_1;
	// System.Object System.Net.WebRequest::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_2;
	// System.Net.TimerThread/Queue System.Net.WebRequest::s_DefaultTimerQueue
	Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * ___s_DefaultTimerQueue_3;
	// System.Net.WebRequest/DesignerWebRequestCreate System.Net.WebRequest::webRequestCreate
	DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 * ___webRequestCreate_9;
	// System.Net.IWebProxy modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_DefaultWebProxy
	RuntimeObject* ___s_DefaultWebProxy_10;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_DefaultWebProxyInitialized
	bool ___s_DefaultWebProxyInitialized_11;

public:
	inline static int32_t get_offset_of_s_PrefixList_1() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_PrefixList_1)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_s_PrefixList_1() const { return ___s_PrefixList_1; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_s_PrefixList_1() { return &___s_PrefixList_1; }
	inline void set_s_PrefixList_1(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___s_PrefixList_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_PrefixList_1), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_2() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_InternalSyncObject_2)); }
	inline RuntimeObject * get_s_InternalSyncObject_2() const { return ___s_InternalSyncObject_2; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_2() { return &___s_InternalSyncObject_2; }
	inline void set_s_InternalSyncObject_2(RuntimeObject * value)
	{
		___s_InternalSyncObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_2), value);
	}

	inline static int32_t get_offset_of_s_DefaultTimerQueue_3() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_DefaultTimerQueue_3)); }
	inline Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * get_s_DefaultTimerQueue_3() const { return ___s_DefaultTimerQueue_3; }
	inline Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 ** get_address_of_s_DefaultTimerQueue_3() { return &___s_DefaultTimerQueue_3; }
	inline void set_s_DefaultTimerQueue_3(Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * value)
	{
		___s_DefaultTimerQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultTimerQueue_3), value);
	}

	inline static int32_t get_offset_of_webRequestCreate_9() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___webRequestCreate_9)); }
	inline DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 * get_webRequestCreate_9() const { return ___webRequestCreate_9; }
	inline DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 ** get_address_of_webRequestCreate_9() { return &___webRequestCreate_9; }
	inline void set_webRequestCreate_9(DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 * value)
	{
		___webRequestCreate_9 = value;
		Il2CppCodeGenWriteBarrier((&___webRequestCreate_9), value);
	}

	inline static int32_t get_offset_of_s_DefaultWebProxy_10() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_DefaultWebProxy_10)); }
	inline RuntimeObject* get_s_DefaultWebProxy_10() const { return ___s_DefaultWebProxy_10; }
	inline RuntimeObject** get_address_of_s_DefaultWebProxy_10() { return &___s_DefaultWebProxy_10; }
	inline void set_s_DefaultWebProxy_10(RuntimeObject* value)
	{
		___s_DefaultWebProxy_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultWebProxy_10), value);
	}

	inline static int32_t get_offset_of_s_DefaultWebProxyInitialized_11() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_DefaultWebProxyInitialized_11)); }
	inline bool get_s_DefaultWebProxyInitialized_11() const { return ___s_DefaultWebProxyInitialized_11; }
	inline bool* get_address_of_s_DefaultWebProxyInitialized_11() { return &___s_DefaultWebProxyInitialized_11; }
	inline void set_s_DefaultWebProxyInitialized_11(bool value)
	{
		___s_DefaultWebProxyInitialized_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUEST_T5668DA48802E9FE2F1DE5F5A770B218608B918C8_H
#ifndef WEBUTILITY_T405BCFEA1292946A0C732330025C9D3C02AF6C1A_H
#define WEBUTILITY_T405BCFEA1292946A0C732330025C9D3C02AF6C1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebUtility
struct  WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A  : public RuntimeObject
{
public:

public:
};

struct WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields
{
public:
	// System.Char[] System.Net.WebUtility::_htmlEntityEndingChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____htmlEntityEndingChars_0;
	// System.Net.Configuration.UnicodeDecodingConformance modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebUtility::_htmlDecodeConformance
	int32_t ____htmlDecodeConformance_1;
	// System.Net.Configuration.UnicodeEncodingConformance modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebUtility::_htmlEncodeConformance
	int32_t ____htmlEncodeConformance_2;

public:
	inline static int32_t get_offset_of__htmlEntityEndingChars_0() { return static_cast<int32_t>(offsetof(WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields, ____htmlEntityEndingChars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__htmlEntityEndingChars_0() const { return ____htmlEntityEndingChars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__htmlEntityEndingChars_0() { return &____htmlEntityEndingChars_0; }
	inline void set__htmlEntityEndingChars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____htmlEntityEndingChars_0 = value;
		Il2CppCodeGenWriteBarrier((&____htmlEntityEndingChars_0), value);
	}

	inline static int32_t get_offset_of__htmlDecodeConformance_1() { return static_cast<int32_t>(offsetof(WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields, ____htmlDecodeConformance_1)); }
	inline int32_t get__htmlDecodeConformance_1() const { return ____htmlDecodeConformance_1; }
	inline int32_t* get_address_of__htmlDecodeConformance_1() { return &____htmlDecodeConformance_1; }
	inline void set__htmlDecodeConformance_1(int32_t value)
	{
		____htmlDecodeConformance_1 = value;
	}

	inline static int32_t get_offset_of__htmlEncodeConformance_2() { return static_cast<int32_t>(offsetof(WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields, ____htmlEncodeConformance_2)); }
	inline int32_t get__htmlEncodeConformance_2() const { return ____htmlEncodeConformance_2; }
	inline int32_t* get_address_of__htmlEncodeConformance_2() { return &____htmlEncodeConformance_2; }
	inline void set__htmlEncodeConformance_2(int32_t value)
	{
		____htmlEncodeConformance_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBUTILITY_T405BCFEA1292946A0C732330025C9D3C02AF6C1A_H
#ifndef BINDIPENDPOINT_T6B179B1AD32AF233C8C8E6440DFEF78153A851B9_H
#define BINDIPENDPOINT_T6B179B1AD32AF233C8C8E6440DFEF78153A851B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.BindIPEndPoint
struct  BindIPEndPoint_t6B179B1AD32AF233C8C8E6440DFEF78153A851B9  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDIPENDPOINT_T6B179B1AD32AF233C8C8E6440DFEF78153A851B9_H
#ifndef COMPLETIONDELEGATE_T69E611769216705025C6437A9D18BAC6D3F0E674_H
#define COMPLETIONDELEGATE_T69E611769216705025C6437A9D18BAC6D3F0E674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CompletionDelegate
struct  CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPLETIONDELEGATE_T69E611769216705025C6437A9D18BAC6D3F0E674_H
#ifndef FILEWEBREQUEST_T198368018F8EBC18AD1A835D585A4B8E235C6E4F_H
#define FILEWEBREQUEST_T198368018F8EBC18AD1A835D585A4B8E235C6E4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebRequest
struct  FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F  : public WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8
{
public:
	// System.String System.Net.FileWebRequest::m_connectionGroupName
	String_t* ___m_connectionGroupName_14;
	// System.Int64 System.Net.FileWebRequest::m_contentLength
	int64_t ___m_contentLength_15;
	// System.Net.ICredentials System.Net.FileWebRequest::m_credentials
	RuntimeObject* ___m_credentials_16;
	// System.IO.FileAccess System.Net.FileWebRequest::m_fileAccess
	int32_t ___m_fileAccess_17;
	// System.Net.WebHeaderCollection System.Net.FileWebRequest::m_headers
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * ___m_headers_18;
	// System.String System.Net.FileWebRequest::m_method
	String_t* ___m_method_19;
	// System.Boolean System.Net.FileWebRequest::m_preauthenticate
	bool ___m_preauthenticate_20;
	// System.Net.IWebProxy System.Net.FileWebRequest::m_proxy
	RuntimeObject* ___m_proxy_21;
	// System.Threading.ManualResetEvent System.Net.FileWebRequest::m_readerEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___m_readerEvent_22;
	// System.Boolean System.Net.FileWebRequest::m_readPending
	bool ___m_readPending_23;
	// System.Net.WebResponse System.Net.FileWebRequest::m_response
	WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * ___m_response_24;
	// System.IO.Stream System.Net.FileWebRequest::m_stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___m_stream_25;
	// System.Boolean System.Net.FileWebRequest::m_syncHint
	bool ___m_syncHint_26;
	// System.Int32 System.Net.FileWebRequest::m_timeout
	int32_t ___m_timeout_27;
	// System.Uri System.Net.FileWebRequest::m_uri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___m_uri_28;
	// System.Boolean System.Net.FileWebRequest::m_writePending
	bool ___m_writePending_29;
	// System.Boolean System.Net.FileWebRequest::m_writing
	bool ___m_writing_30;
	// System.Net.LazyAsyncResult System.Net.FileWebRequest::m_WriteAResult
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 * ___m_WriteAResult_31;
	// System.Net.LazyAsyncResult System.Net.FileWebRequest::m_ReadAResult
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 * ___m_ReadAResult_32;
	// System.Int32 System.Net.FileWebRequest::m_Aborted
	int32_t ___m_Aborted_33;

public:
	inline static int32_t get_offset_of_m_connectionGroupName_14() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_connectionGroupName_14)); }
	inline String_t* get_m_connectionGroupName_14() const { return ___m_connectionGroupName_14; }
	inline String_t** get_address_of_m_connectionGroupName_14() { return &___m_connectionGroupName_14; }
	inline void set_m_connectionGroupName_14(String_t* value)
	{
		___m_connectionGroupName_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_connectionGroupName_14), value);
	}

	inline static int32_t get_offset_of_m_contentLength_15() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_contentLength_15)); }
	inline int64_t get_m_contentLength_15() const { return ___m_contentLength_15; }
	inline int64_t* get_address_of_m_contentLength_15() { return &___m_contentLength_15; }
	inline void set_m_contentLength_15(int64_t value)
	{
		___m_contentLength_15 = value;
	}

	inline static int32_t get_offset_of_m_credentials_16() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_credentials_16)); }
	inline RuntimeObject* get_m_credentials_16() const { return ___m_credentials_16; }
	inline RuntimeObject** get_address_of_m_credentials_16() { return &___m_credentials_16; }
	inline void set_m_credentials_16(RuntimeObject* value)
	{
		___m_credentials_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_credentials_16), value);
	}

	inline static int32_t get_offset_of_m_fileAccess_17() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_fileAccess_17)); }
	inline int32_t get_m_fileAccess_17() const { return ___m_fileAccess_17; }
	inline int32_t* get_address_of_m_fileAccess_17() { return &___m_fileAccess_17; }
	inline void set_m_fileAccess_17(int32_t value)
	{
		___m_fileAccess_17 = value;
	}

	inline static int32_t get_offset_of_m_headers_18() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_headers_18)); }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * get_m_headers_18() const { return ___m_headers_18; }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 ** get_address_of_m_headers_18() { return &___m_headers_18; }
	inline void set_m_headers_18(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * value)
	{
		___m_headers_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_headers_18), value);
	}

	inline static int32_t get_offset_of_m_method_19() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_method_19)); }
	inline String_t* get_m_method_19() const { return ___m_method_19; }
	inline String_t** get_address_of_m_method_19() { return &___m_method_19; }
	inline void set_m_method_19(String_t* value)
	{
		___m_method_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_method_19), value);
	}

	inline static int32_t get_offset_of_m_preauthenticate_20() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_preauthenticate_20)); }
	inline bool get_m_preauthenticate_20() const { return ___m_preauthenticate_20; }
	inline bool* get_address_of_m_preauthenticate_20() { return &___m_preauthenticate_20; }
	inline void set_m_preauthenticate_20(bool value)
	{
		___m_preauthenticate_20 = value;
	}

	inline static int32_t get_offset_of_m_proxy_21() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_proxy_21)); }
	inline RuntimeObject* get_m_proxy_21() const { return ___m_proxy_21; }
	inline RuntimeObject** get_address_of_m_proxy_21() { return &___m_proxy_21; }
	inline void set_m_proxy_21(RuntimeObject* value)
	{
		___m_proxy_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_proxy_21), value);
	}

	inline static int32_t get_offset_of_m_readerEvent_22() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_readerEvent_22)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_m_readerEvent_22() const { return ___m_readerEvent_22; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_m_readerEvent_22() { return &___m_readerEvent_22; }
	inline void set_m_readerEvent_22(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___m_readerEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_readerEvent_22), value);
	}

	inline static int32_t get_offset_of_m_readPending_23() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_readPending_23)); }
	inline bool get_m_readPending_23() const { return ___m_readPending_23; }
	inline bool* get_address_of_m_readPending_23() { return &___m_readPending_23; }
	inline void set_m_readPending_23(bool value)
	{
		___m_readPending_23 = value;
	}

	inline static int32_t get_offset_of_m_response_24() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_response_24)); }
	inline WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * get_m_response_24() const { return ___m_response_24; }
	inline WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD ** get_address_of_m_response_24() { return &___m_response_24; }
	inline void set_m_response_24(WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * value)
	{
		___m_response_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_response_24), value);
	}

	inline static int32_t get_offset_of_m_stream_25() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_stream_25)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_m_stream_25() const { return ___m_stream_25; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_m_stream_25() { return &___m_stream_25; }
	inline void set_m_stream_25(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___m_stream_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_stream_25), value);
	}

	inline static int32_t get_offset_of_m_syncHint_26() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_syncHint_26)); }
	inline bool get_m_syncHint_26() const { return ___m_syncHint_26; }
	inline bool* get_address_of_m_syncHint_26() { return &___m_syncHint_26; }
	inline void set_m_syncHint_26(bool value)
	{
		___m_syncHint_26 = value;
	}

	inline static int32_t get_offset_of_m_timeout_27() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_timeout_27)); }
	inline int32_t get_m_timeout_27() const { return ___m_timeout_27; }
	inline int32_t* get_address_of_m_timeout_27() { return &___m_timeout_27; }
	inline void set_m_timeout_27(int32_t value)
	{
		___m_timeout_27 = value;
	}

	inline static int32_t get_offset_of_m_uri_28() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_uri_28)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_m_uri_28() const { return ___m_uri_28; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_m_uri_28() { return &___m_uri_28; }
	inline void set_m_uri_28(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___m_uri_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_uri_28), value);
	}

	inline static int32_t get_offset_of_m_writePending_29() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_writePending_29)); }
	inline bool get_m_writePending_29() const { return ___m_writePending_29; }
	inline bool* get_address_of_m_writePending_29() { return &___m_writePending_29; }
	inline void set_m_writePending_29(bool value)
	{
		___m_writePending_29 = value;
	}

	inline static int32_t get_offset_of_m_writing_30() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_writing_30)); }
	inline bool get_m_writing_30() const { return ___m_writing_30; }
	inline bool* get_address_of_m_writing_30() { return &___m_writing_30; }
	inline void set_m_writing_30(bool value)
	{
		___m_writing_30 = value;
	}

	inline static int32_t get_offset_of_m_WriteAResult_31() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_WriteAResult_31)); }
	inline LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 * get_m_WriteAResult_31() const { return ___m_WriteAResult_31; }
	inline LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 ** get_address_of_m_WriteAResult_31() { return &___m_WriteAResult_31; }
	inline void set_m_WriteAResult_31(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 * value)
	{
		___m_WriteAResult_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_WriteAResult_31), value);
	}

	inline static int32_t get_offset_of_m_ReadAResult_32() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_ReadAResult_32)); }
	inline LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 * get_m_ReadAResult_32() const { return ___m_ReadAResult_32; }
	inline LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 ** get_address_of_m_ReadAResult_32() { return &___m_ReadAResult_32; }
	inline void set_m_ReadAResult_32(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 * value)
	{
		___m_ReadAResult_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReadAResult_32), value);
	}

	inline static int32_t get_offset_of_m_Aborted_33() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_Aborted_33)); }
	inline int32_t get_m_Aborted_33() const { return ___m_Aborted_33; }
	inline int32_t* get_address_of_m_Aborted_33() { return &___m_Aborted_33; }
	inline void set_m_Aborted_33(int32_t value)
	{
		___m_Aborted_33 = value;
	}
};

struct FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F_StaticFields
{
public:
	// System.Threading.WaitCallback System.Net.FileWebRequest::s_GetRequestStreamCallback
	WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * ___s_GetRequestStreamCallback_12;
	// System.Threading.WaitCallback System.Net.FileWebRequest::s_GetResponseCallback
	WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * ___s_GetResponseCallback_13;

public:
	inline static int32_t get_offset_of_s_GetRequestStreamCallback_12() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F_StaticFields, ___s_GetRequestStreamCallback_12)); }
	inline WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * get_s_GetRequestStreamCallback_12() const { return ___s_GetRequestStreamCallback_12; }
	inline WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC ** get_address_of_s_GetRequestStreamCallback_12() { return &___s_GetRequestStreamCallback_12; }
	inline void set_s_GetRequestStreamCallback_12(WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * value)
	{
		___s_GetRequestStreamCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetRequestStreamCallback_12), value);
	}

	inline static int32_t get_offset_of_s_GetResponseCallback_13() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F_StaticFields, ___s_GetResponseCallback_13)); }
	inline WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * get_s_GetResponseCallback_13() const { return ___s_GetResponseCallback_13; }
	inline WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC ** get_address_of_s_GetResponseCallback_13() { return &___s_GetResponseCallback_13; }
	inline void set_s_GetResponseCallback_13(WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * value)
	{
		___s_GetResponseCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetResponseCallback_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBREQUEST_T198368018F8EBC18AD1A835D585A4B8E235C6E4F_H
#ifndef FILEWEBSTREAM_T67DDC539EC81FFB9F3615EBE17649E53E1CCA766_H
#define FILEWEBSTREAM_T67DDC539EC81FFB9F3615EBE17649E53E1CCA766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebStream
struct  FileWebStream_t67DDC539EC81FFB9F3615EBE17649E53E1CCA766  : public FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418
{
public:
	// System.Net.FileWebRequest System.Net.FileWebStream::m_request
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F * ___m_request_21;

public:
	inline static int32_t get_offset_of_m_request_21() { return static_cast<int32_t>(offsetof(FileWebStream_t67DDC539EC81FFB9F3615EBE17649E53E1CCA766, ___m_request_21)); }
	inline FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F * get_m_request_21() const { return ___m_request_21; }
	inline FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F ** get_address_of_m_request_21() { return &___m_request_21; }
	inline void set_m_request_21(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F * value)
	{
		___m_request_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_request_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBSTREAM_T67DDC539EC81FFB9F3615EBE17649E53E1CCA766_H
#ifndef READDELEGATE_TBC77AE628966A21E63D8BB344BC3D7C79441A6DE_H
#define READDELEGATE_TBC77AE628966A21E63D8BB344BC3D7C79441A6DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpDataStream/ReadDelegate
struct  ReadDelegate_tBC77AE628966A21E63D8BB344BC3D7C79441A6DE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READDELEGATE_TBC77AE628966A21E63D8BB344BC3D7C79441A6DE_H
#ifndef WRITEDELEGATE_TCA763F3444D2578FB21239EDFC1C3632E469FC49_H
#define WRITEDELEGATE_TCA763F3444D2578FB21239EDFC1C3632E469FC49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpDataStream/WriteDelegate
struct  WriteDelegate_tCA763F3444D2578FB21239EDFC1C3632E469FC49  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEDELEGATE_TCA763F3444D2578FB21239EDFC1C3632E469FC49_H
#ifndef FTPWEBREQUEST_T9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA_H
#define FTPWEBREQUEST_T9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpWebRequest
struct  FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA  : public WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8
{
public:
	// System.Uri System.Net.FtpWebRequest::requestUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___requestUri_12;
	// System.String System.Net.FtpWebRequest::file_name
	String_t* ___file_name_13;
	// System.Net.ServicePoint System.Net.FtpWebRequest::servicePoint
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * ___servicePoint_14;
	// System.IO.Stream System.Net.FtpWebRequest::origDataStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___origDataStream_15;
	// System.IO.Stream System.Net.FtpWebRequest::dataStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___dataStream_16;
	// System.IO.Stream System.Net.FtpWebRequest::controlStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___controlStream_17;
	// System.IO.StreamReader System.Net.FtpWebRequest::controlReader
	StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * ___controlReader_18;
	// System.Net.NetworkCredential System.Net.FtpWebRequest::credentials
	NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062 * ___credentials_19;
	// System.Net.IPHostEntry System.Net.FtpWebRequest::hostEntry
	IPHostEntry_tB00EABDF75DB19AEAD4F3E7D93BFD7BAE558149D * ___hostEntry_20;
	// System.Net.IPEndPoint System.Net.FtpWebRequest::localEndPoint
	IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * ___localEndPoint_21;
	// System.Net.IPEndPoint System.Net.FtpWebRequest::remoteEndPoint
	IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * ___remoteEndPoint_22;
	// System.Net.IWebProxy System.Net.FtpWebRequest::proxy
	RuntimeObject* ___proxy_23;
	// System.Int32 System.Net.FtpWebRequest::timeout
	int32_t ___timeout_24;
	// System.Int32 System.Net.FtpWebRequest::rwTimeout
	int32_t ___rwTimeout_25;
	// System.Int64 System.Net.FtpWebRequest::offset
	int64_t ___offset_26;
	// System.Boolean System.Net.FtpWebRequest::binary
	bool ___binary_27;
	// System.Boolean System.Net.FtpWebRequest::enableSsl
	bool ___enableSsl_28;
	// System.Boolean System.Net.FtpWebRequest::usePassive
	bool ___usePassive_29;
	// System.Boolean System.Net.FtpWebRequest::keepAlive
	bool ___keepAlive_30;
	// System.String System.Net.FtpWebRequest::method
	String_t* ___method_31;
	// System.String System.Net.FtpWebRequest::renameTo
	String_t* ___renameTo_32;
	// System.Object System.Net.FtpWebRequest::locker
	RuntimeObject * ___locker_33;
	// System.Net.FtpWebRequest/RequestState System.Net.FtpWebRequest::requestState
	int32_t ___requestState_34;
	// System.Net.FtpAsyncResult System.Net.FtpWebRequest::asyncResult
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3 * ___asyncResult_35;
	// System.Net.FtpWebResponse System.Net.FtpWebRequest::ftpResponse
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 * ___ftpResponse_36;
	// System.IO.Stream System.Net.FtpWebRequest::requestStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___requestStream_37;
	// System.String System.Net.FtpWebRequest::initial_path
	String_t* ___initial_path_38;
	// System.Text.Encoding System.Net.FtpWebRequest::dataEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___dataEncoding_40;

public:
	inline static int32_t get_offset_of_requestUri_12() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___requestUri_12)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_requestUri_12() const { return ___requestUri_12; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_requestUri_12() { return &___requestUri_12; }
	inline void set_requestUri_12(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___requestUri_12 = value;
		Il2CppCodeGenWriteBarrier((&___requestUri_12), value);
	}

	inline static int32_t get_offset_of_file_name_13() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___file_name_13)); }
	inline String_t* get_file_name_13() const { return ___file_name_13; }
	inline String_t** get_address_of_file_name_13() { return &___file_name_13; }
	inline void set_file_name_13(String_t* value)
	{
		___file_name_13 = value;
		Il2CppCodeGenWriteBarrier((&___file_name_13), value);
	}

	inline static int32_t get_offset_of_servicePoint_14() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___servicePoint_14)); }
	inline ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * get_servicePoint_14() const { return ___servicePoint_14; }
	inline ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 ** get_address_of_servicePoint_14() { return &___servicePoint_14; }
	inline void set_servicePoint_14(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * value)
	{
		___servicePoint_14 = value;
		Il2CppCodeGenWriteBarrier((&___servicePoint_14), value);
	}

	inline static int32_t get_offset_of_origDataStream_15() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___origDataStream_15)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_origDataStream_15() const { return ___origDataStream_15; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_origDataStream_15() { return &___origDataStream_15; }
	inline void set_origDataStream_15(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___origDataStream_15 = value;
		Il2CppCodeGenWriteBarrier((&___origDataStream_15), value);
	}

	inline static int32_t get_offset_of_dataStream_16() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___dataStream_16)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_dataStream_16() const { return ___dataStream_16; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_dataStream_16() { return &___dataStream_16; }
	inline void set_dataStream_16(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___dataStream_16 = value;
		Il2CppCodeGenWriteBarrier((&___dataStream_16), value);
	}

	inline static int32_t get_offset_of_controlStream_17() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___controlStream_17)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_controlStream_17() const { return ___controlStream_17; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_controlStream_17() { return &___controlStream_17; }
	inline void set_controlStream_17(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___controlStream_17 = value;
		Il2CppCodeGenWriteBarrier((&___controlStream_17), value);
	}

	inline static int32_t get_offset_of_controlReader_18() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___controlReader_18)); }
	inline StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * get_controlReader_18() const { return ___controlReader_18; }
	inline StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E ** get_address_of_controlReader_18() { return &___controlReader_18; }
	inline void set_controlReader_18(StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * value)
	{
		___controlReader_18 = value;
		Il2CppCodeGenWriteBarrier((&___controlReader_18), value);
	}

	inline static int32_t get_offset_of_credentials_19() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___credentials_19)); }
	inline NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062 * get_credentials_19() const { return ___credentials_19; }
	inline NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062 ** get_address_of_credentials_19() { return &___credentials_19; }
	inline void set_credentials_19(NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062 * value)
	{
		___credentials_19 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_19), value);
	}

	inline static int32_t get_offset_of_hostEntry_20() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___hostEntry_20)); }
	inline IPHostEntry_tB00EABDF75DB19AEAD4F3E7D93BFD7BAE558149D * get_hostEntry_20() const { return ___hostEntry_20; }
	inline IPHostEntry_tB00EABDF75DB19AEAD4F3E7D93BFD7BAE558149D ** get_address_of_hostEntry_20() { return &___hostEntry_20; }
	inline void set_hostEntry_20(IPHostEntry_tB00EABDF75DB19AEAD4F3E7D93BFD7BAE558149D * value)
	{
		___hostEntry_20 = value;
		Il2CppCodeGenWriteBarrier((&___hostEntry_20), value);
	}

	inline static int32_t get_offset_of_localEndPoint_21() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___localEndPoint_21)); }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * get_localEndPoint_21() const { return ___localEndPoint_21; }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F ** get_address_of_localEndPoint_21() { return &___localEndPoint_21; }
	inline void set_localEndPoint_21(IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * value)
	{
		___localEndPoint_21 = value;
		Il2CppCodeGenWriteBarrier((&___localEndPoint_21), value);
	}

	inline static int32_t get_offset_of_remoteEndPoint_22() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___remoteEndPoint_22)); }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * get_remoteEndPoint_22() const { return ___remoteEndPoint_22; }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F ** get_address_of_remoteEndPoint_22() { return &___remoteEndPoint_22; }
	inline void set_remoteEndPoint_22(IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * value)
	{
		___remoteEndPoint_22 = value;
		Il2CppCodeGenWriteBarrier((&___remoteEndPoint_22), value);
	}

	inline static int32_t get_offset_of_proxy_23() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___proxy_23)); }
	inline RuntimeObject* get_proxy_23() const { return ___proxy_23; }
	inline RuntimeObject** get_address_of_proxy_23() { return &___proxy_23; }
	inline void set_proxy_23(RuntimeObject* value)
	{
		___proxy_23 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_23), value);
	}

	inline static int32_t get_offset_of_timeout_24() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___timeout_24)); }
	inline int32_t get_timeout_24() const { return ___timeout_24; }
	inline int32_t* get_address_of_timeout_24() { return &___timeout_24; }
	inline void set_timeout_24(int32_t value)
	{
		___timeout_24 = value;
	}

	inline static int32_t get_offset_of_rwTimeout_25() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___rwTimeout_25)); }
	inline int32_t get_rwTimeout_25() const { return ___rwTimeout_25; }
	inline int32_t* get_address_of_rwTimeout_25() { return &___rwTimeout_25; }
	inline void set_rwTimeout_25(int32_t value)
	{
		___rwTimeout_25 = value;
	}

	inline static int32_t get_offset_of_offset_26() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___offset_26)); }
	inline int64_t get_offset_26() const { return ___offset_26; }
	inline int64_t* get_address_of_offset_26() { return &___offset_26; }
	inline void set_offset_26(int64_t value)
	{
		___offset_26 = value;
	}

	inline static int32_t get_offset_of_binary_27() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___binary_27)); }
	inline bool get_binary_27() const { return ___binary_27; }
	inline bool* get_address_of_binary_27() { return &___binary_27; }
	inline void set_binary_27(bool value)
	{
		___binary_27 = value;
	}

	inline static int32_t get_offset_of_enableSsl_28() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___enableSsl_28)); }
	inline bool get_enableSsl_28() const { return ___enableSsl_28; }
	inline bool* get_address_of_enableSsl_28() { return &___enableSsl_28; }
	inline void set_enableSsl_28(bool value)
	{
		___enableSsl_28 = value;
	}

	inline static int32_t get_offset_of_usePassive_29() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___usePassive_29)); }
	inline bool get_usePassive_29() const { return ___usePassive_29; }
	inline bool* get_address_of_usePassive_29() { return &___usePassive_29; }
	inline void set_usePassive_29(bool value)
	{
		___usePassive_29 = value;
	}

	inline static int32_t get_offset_of_keepAlive_30() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___keepAlive_30)); }
	inline bool get_keepAlive_30() const { return ___keepAlive_30; }
	inline bool* get_address_of_keepAlive_30() { return &___keepAlive_30; }
	inline void set_keepAlive_30(bool value)
	{
		___keepAlive_30 = value;
	}

	inline static int32_t get_offset_of_method_31() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___method_31)); }
	inline String_t* get_method_31() const { return ___method_31; }
	inline String_t** get_address_of_method_31() { return &___method_31; }
	inline void set_method_31(String_t* value)
	{
		___method_31 = value;
		Il2CppCodeGenWriteBarrier((&___method_31), value);
	}

	inline static int32_t get_offset_of_renameTo_32() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___renameTo_32)); }
	inline String_t* get_renameTo_32() const { return ___renameTo_32; }
	inline String_t** get_address_of_renameTo_32() { return &___renameTo_32; }
	inline void set_renameTo_32(String_t* value)
	{
		___renameTo_32 = value;
		Il2CppCodeGenWriteBarrier((&___renameTo_32), value);
	}

	inline static int32_t get_offset_of_locker_33() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___locker_33)); }
	inline RuntimeObject * get_locker_33() const { return ___locker_33; }
	inline RuntimeObject ** get_address_of_locker_33() { return &___locker_33; }
	inline void set_locker_33(RuntimeObject * value)
	{
		___locker_33 = value;
		Il2CppCodeGenWriteBarrier((&___locker_33), value);
	}

	inline static int32_t get_offset_of_requestState_34() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___requestState_34)); }
	inline int32_t get_requestState_34() const { return ___requestState_34; }
	inline int32_t* get_address_of_requestState_34() { return &___requestState_34; }
	inline void set_requestState_34(int32_t value)
	{
		___requestState_34 = value;
	}

	inline static int32_t get_offset_of_asyncResult_35() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___asyncResult_35)); }
	inline FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3 * get_asyncResult_35() const { return ___asyncResult_35; }
	inline FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3 ** get_address_of_asyncResult_35() { return &___asyncResult_35; }
	inline void set_asyncResult_35(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3 * value)
	{
		___asyncResult_35 = value;
		Il2CppCodeGenWriteBarrier((&___asyncResult_35), value);
	}

	inline static int32_t get_offset_of_ftpResponse_36() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___ftpResponse_36)); }
	inline FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 * get_ftpResponse_36() const { return ___ftpResponse_36; }
	inline FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 ** get_address_of_ftpResponse_36() { return &___ftpResponse_36; }
	inline void set_ftpResponse_36(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 * value)
	{
		___ftpResponse_36 = value;
		Il2CppCodeGenWriteBarrier((&___ftpResponse_36), value);
	}

	inline static int32_t get_offset_of_requestStream_37() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___requestStream_37)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_requestStream_37() const { return ___requestStream_37; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_requestStream_37() { return &___requestStream_37; }
	inline void set_requestStream_37(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___requestStream_37 = value;
		Il2CppCodeGenWriteBarrier((&___requestStream_37), value);
	}

	inline static int32_t get_offset_of_initial_path_38() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___initial_path_38)); }
	inline String_t* get_initial_path_38() const { return ___initial_path_38; }
	inline String_t** get_address_of_initial_path_38() { return &___initial_path_38; }
	inline void set_initial_path_38(String_t* value)
	{
		___initial_path_38 = value;
		Il2CppCodeGenWriteBarrier((&___initial_path_38), value);
	}

	inline static int32_t get_offset_of_dataEncoding_40() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___dataEncoding_40)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_dataEncoding_40() const { return ___dataEncoding_40; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_dataEncoding_40() { return &___dataEncoding_40; }
	inline void set_dataEncoding_40(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___dataEncoding_40 = value;
		Il2CppCodeGenWriteBarrier((&___dataEncoding_40), value);
	}
};

struct FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA_StaticFields
{
public:
	// System.String[] System.Net.FtpWebRequest::supportedCommands
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___supportedCommands_39;

public:
	inline static int32_t get_offset_of_supportedCommands_39() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA_StaticFields, ___supportedCommands_39)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_supportedCommands_39() const { return ___supportedCommands_39; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_supportedCommands_39() { return &___supportedCommands_39; }
	inline void set_supportedCommands_39(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___supportedCommands_39 = value;
		Il2CppCodeGenWriteBarrier((&___supportedCommands_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPWEBREQUEST_T9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA_H
#ifndef HEADERPARSER_T6B59FF0FD79FFD511A019AE5383DCEF641BA822E_H
#define HEADERPARSER_T6B59FF0FD79FFD511A019AE5383DCEF641BA822E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HeaderParser
struct  HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERPARSER_T6B59FF0FD79FFD511A019AE5383DCEF641BA822E_H
#ifndef CALLBACK_T8DF3FD84AB632709C486978BE28ED721EB3A40E3_H
#define CALLBACK_T8DF3FD84AB632709C486978BE28ED721EB3A40E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread/Callback
struct  Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACK_T8DF3FD84AB632709C486978BE28ED721EB3A40E3_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (ProtocolViolationException_t287E1EFCC1BC7BB76C74A681581BF3A67C68BDFB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[4] = 
{
	SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969::get_offset_of_m_Size_0(),
	SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969::get_offset_of_m_Buffer_1(),
	SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969::get_offset_of_m_changed_2(),
	SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969::get_offset_of_m_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (TransportContext_t1753CC7BFFA637B35BE353AAE4452C4371F89A9F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[3] = 
{
	WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D::get_offset_of_m_Status_17(),
	WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D::get_offset_of_m_Response_18(),
	WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D::get_offset_of_m_InternalStatus_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (WebExceptionInternalStatus_t2B50725020F5BAB7DCBE324ADF308881FEB3B64D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2704[5] = 
{
	WebExceptionInternalStatus_t2B50725020F5BAB7DCBE324ADF308881FEB3B64D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (WebExceptionStatus_t97365CBADE462C1E2A1A0FACF18F3B111900F8DC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2705[22] = 
{
	WebExceptionStatus_t97365CBADE462C1E2A1A0FACF18F3B111900F8DC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (WebExceptionMapping_t4E7EF581D0224FFC2F2C8556EF320557517A378A), -1, sizeof(WebExceptionMapping_t4E7EF581D0224FFC2F2C8556EF320557517A378A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2706[1] = 
{
	WebExceptionMapping_t4E7EF581D0224FFC2F2C8556EF320557517A378A_StaticFields::get_offset_of_s_Mapping_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (WebHeaderCollectionType_t2994510EB856AC407AB0757A9814CDF80185A862)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2707[12] = 
{
	WebHeaderCollectionType_t2994510EB856AC407AB0757A9814CDF80185A862::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304), -1, sizeof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2708[9] = 
{
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields::get_offset_of_HInfo_11(),
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304::get_offset_of_m_CommonHeaders_12(),
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304::get_offset_of_m_NumCommonHeaders_13(),
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields::get_offset_of_s_CommonHeaderNames_14(),
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields::get_offset_of_s_CommonHeaderHints_15(),
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304::get_offset_of_m_InnerCollection_16(),
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304::get_offset_of_m_Type_17(),
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields::get_offset_of_HttpTrimCharacters_18(),
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields::get_offset_of_RfcCharMap_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (RfcChar_tD4173F085F19DF711D550AC6CAD7EF61939EF27F)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2709[9] = 
{
	RfcChar_tD4173F085F19DF711D550AC6CAD7EF61939EF27F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B), -1, sizeof(CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2710[2] = 
{
	CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_StaticFields::get_offset_of_StaticInstance_0(),
	CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_StaticFields::get_offset_of_AsciiToLower_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8), -1, sizeof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2711[11] = 
{
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields::get_offset_of_s_PrefixList_1(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields::get_offset_of_s_InternalSyncObject_2(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields::get_offset_of_s_DefaultTimerQueue_3(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8::get_offset_of_m_AuthenticationLevel_4(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8::get_offset_of_m_ImpersonationLevel_5(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8::get_offset_of_m_CachePolicy_6(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8::get_offset_of_m_CacheProtocol_7(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8::get_offset_of_m_CacheBinding_8(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields::get_offset_of_webRequestCreate_9(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields::get_offset_of_s_DefaultWebProxy_10(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields::get_offset_of_s_DefaultWebProxyInitialized_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (WebProxyWrapperOpaque_t6CC216364481C2A8254832AA0897F770BB494A62), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[1] = 
{
	WebProxyWrapperOpaque_t6CC216364481C2A8254832AA0897F770BB494A62::get_offset_of_webProxy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (WebProxyWrapper_t47B30DCD77853C5079F4944A6FCA329026D84E3B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (U3CU3Ec__DisplayClass78_0_tC7AC5DA365F8D5A75E48F83C5D127EF753913861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2715[2] = 
{
	U3CU3Ec__DisplayClass78_0_tC7AC5DA365F8D5A75E48F83C5D127EF753913861::get_offset_of_currentUser_0(),
	U3CU3Ec__DisplayClass78_0_tC7AC5DA365F8D5A75E48F83C5D127EF753913861::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (U3CU3Ec__DisplayClass79_0_t4CA87BE9AA29F9315C6AD70F8C79A62C7727013F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2716[2] = 
{
	U3CU3Ec__DisplayClass79_0_t4CA87BE9AA29F9315C6AD70F8C79A62C7727013F::get_offset_of_currentUser_0(),
	U3CU3Ec__DisplayClass79_0_t4CA87BE9AA29F9315C6AD70F8C79A62C7727013F::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A), -1, sizeof(WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2718[3] = 
{
	WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields::get_offset_of__htmlEntityEndingChars_0(),
	WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields::get_offset_of__htmlDecodeConformance_1(),
	WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields::get_offset_of__htmlEncodeConformance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[6] = 
{
	UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC::get_offset_of__bufferSize_0(),
	UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC::get_offset_of__numChars_1(),
	UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC::get_offset_of__charBuffer_2(),
	UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC::get_offset_of__numBytes_3(),
	UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC::get_offset_of__byteBuffer_4(),
	UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC::get_offset_of__encoding_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2720[3] = 
{
	BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317::get_offset_of_Buffer_0(),
	BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317::get_offset_of_Offset_1(),
	BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317::get_offset_of_Size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[5] = 
{
	HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8::get_offset_of_IsRequestRestricted_0(),
	HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8::get_offset_of_IsResponseRestricted_1(),
	HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8::get_offset_of_Parser_2(),
	HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8::get_offset_of_HeaderName_3(),
	HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8::get_offset_of_AllowMultiValues_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF), -1, sizeof(HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2723[4] = 
{
	HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields::get_offset_of_HeaderHashTable_0(),
	HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields::get_offset_of_UnknownHeaderInfo_1(),
	HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields::get_offset_of_SingleParser_2(),
	HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields::get_offset_of_MultiParser_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (CloseExState_t7AD30E3EACEBBAF7661B1AC45F7BC018DA33E429)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2724[4] = 
{
	CloseExState_t7AD30E3EACEBBAF7661B1AC45F7BC018DA33E429::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3), -1, 0, sizeof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2726[8] = 
{
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3_ThreadStaticFields::get_offset_of_t_ThreadContext_0() | THREAD_LOCAL_STATIC_MASK,
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_AsyncObject_1(),
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_AsyncState_2(),
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_AsyncCallback_3(),
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_Result_4(),
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_IntCompleted_5(),
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_UserEvent_6(),
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_Event_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[1] = 
{
	ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082::get_offset_of_m_NestedIOCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (NetRes_tB18DF1FAF98D8D7505D72FA149E57F0D31E2653B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[5] = 
{
	ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B::get_offset_of_headChunk_0(),
	ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B::get_offset_of_currentChunk_1(),
	ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B::get_offset_of_nextChunkLength_2(),
	ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B::get_offset_of_totalLength_3(),
	ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B::get_offset_of_chunkCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2731[3] = 
{
	MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F::get_offset_of_Buffer_0(),
	MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F::get_offset_of_FreeOffset_1(),
	MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F::get_offset_of_Next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[2] = 
{
	ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37::get_offset_of_serviceNames_0(),
	ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37::get_offset_of_serviceNameCollection_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01), -1, sizeof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2733[7] = 
{
	TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields::get_offset_of_s_Queues_0(),
	TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields::get_offset_of_s_NewQueues_1(),
	TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields::get_offset_of_s_ThreadState_2(),
	TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields::get_offset_of_s_ThreadReadyEvent_3(),
	TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields::get_offset_of_s_ThreadShutdownEvent_4(),
	TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields::get_offset_of_s_ThreadEvents_5(),
	TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields::get_offset_of_s_QueuesCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2734[1] = 
{
	Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643::get_offset_of_m_DurationMilliseconds_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[2] = 
{
	Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F::get_offset_of_m_StartTimeMilliseconds_0(),
	Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F::get_offset_of_m_DurationMilliseconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (TimerQueue_t8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[1] = 
{
	TimerQueue_t8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C::get_offset_of_m_Timers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (InfiniteTimerQueue_t141BA98635EDB34E2BAAFE8BA5C91795E7CCAB51), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[6] = 
{
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B::get_offset_of_m_TimerState_2(),
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B::get_offset_of_m_Callback_3(),
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B::get_offset_of_m_Context_4(),
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B::get_offset_of_m_QueueLock_5(),
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B::get_offset_of_next_6(),
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B::get_offset_of_prev_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (TimerState_tD555FD971FFAFF7BE6F94885EC160D206354BD35)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2740[5] = 
{
	TimerState_tD555FD971FFAFF7BE6F94885EC160D206354BD35::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (EmptyWebProxy_tF6CEF11A280246455534D46AD1710271B8BEE22D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2741[1] = 
{
	EmptyWebProxy_tF6CEF11A280246455534D46AD1710271B8BEE22D::get_offset_of_m_credentials_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (CookieVariant_t896D38AC6FBE01ADFB532B04C5E0E19842256CFC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2742[6] = 
{
	CookieVariant_t896D38AC6FBE01ADFB532B04C5E0E19842256CFC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90), -1, sizeof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2743[25] = 
{
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields::get_offset_of_PortSplitDelimiters_0(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields::get_offset_of_Reserved2Name_1(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields::get_offset_of_Reserved2Value_2(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields::get_offset_of_staticComparer_3(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_comment_4(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_commentUri_5(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_cookieVariant_6(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_discard_7(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_domain_8(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_domain_implicit_9(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_expires_10(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_name_11(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_path_12(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_path_implicit_13(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_port_14(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_port_implicit_15(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_port_list_16(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_secure_17(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_httpOnly_18(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_timeStamp_19(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_value_20(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_version_21(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_domainKey_22(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_IsQuotedVersion_23(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_IsQuotedDomain_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (CookieToken_tB2F88831DE62615EAABB9CBF6CCFDFCD0A0D88B8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2744[21] = 
{
	CookieToken_tB2F88831DE62615EAABB9CBF6CCFDFCD0A0D88B8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD), -1, sizeof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2745[12] = 
{
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_eofCookie_0(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_index_1(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_length_2(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_name_3(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_quoted_4(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_start_5(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_token_6(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_tokenLength_7(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_tokenStream_8(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_value_9(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_StaticFields::get_offset_of_RecognizedAttributes_10(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_StaticFields::get_offset_of_RecognizedServerAttributes_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B)+ sizeof (RuntimeObject), sizeof(RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2746[2] = 
{
	RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B::get_offset_of_m_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B::get_offset_of_m_token_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (CookieParser_t6034725CF7B5A3842FEC753620D331478F74B396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[1] = 
{
	CookieParser_t6034725CF7B5A3842FEC753620D331478F74B396::get_offset_of_m_tokenizer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (Comparer_tFC5265AD65740F9DB39C75F1E3EF8032982F45AB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[5] = 
{
	CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3::get_offset_of_m_version_0(),
	CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3::get_offset_of_m_list_1(),
	CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3::get_offset_of_m_TimeStamp_2(),
	CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3::get_offset_of_m_has_other_versions_3(),
	CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3::get_offset_of_m_IsReadOnly_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (Stamp_t06B0F70FA36D78E86543007609E79740E8BB87BE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2750[5] = 
{
	Stamp_t06B0F70FA36D78E86543007609E79740E8BB87BE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2751[4] = 
{
	CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D::get_offset_of_m_cookies_0(),
	CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D::get_offset_of_m_count_1(),
	CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D::get_offset_of_m_index_2(),
	CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D::get_offset_of_m_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64)+ sizeof (RuntimeObject), sizeof(HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2752[2] = 
{
	HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64::get_offset_of_m_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64::get_offset_of_m_variant_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73), -1, sizeof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2753[7] = 
{
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73_StaticFields::get_offset_of_HeaderInfo_0(),
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73::get_offset_of_m_domainTable_1(),
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73::get_offset_of_m_maxCookieSize_2(),
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73::get_offset_of_m_maxCookies_3(),
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73::get_offset_of_m_maxCookiesPerDomain_4(),
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73::get_offset_of_m_count_5(),
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73::get_offset_of_m_fqdnMyDomain_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (PathList_tE89F0E044B0D96268DB20C9B0FC852C690C2DC8A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[1] = 
{
	PathList_tE89F0E044B0D96268DB20C9B0FC852C690C2DC8A::get_offset_of_m_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF), -1, sizeof(PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2755[1] = 
{
	PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF_StaticFields::get_offset_of_StaticInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (CookieException_t1366ADFB475F67C6BAD72CE2EC1AB504861C2FA4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F), -1, sizeof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2757[22] = 
{
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F_StaticFields::get_offset_of_s_GetRequestStreamCallback_12(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F_StaticFields::get_offset_of_s_GetResponseCallback_13(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_connectionGroupName_14(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_contentLength_15(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_credentials_16(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_fileAccess_17(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_headers_18(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_method_19(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_preauthenticate_20(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_proxy_21(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_readerEvent_22(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_readPending_23(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_response_24(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_stream_25(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_syncHint_26(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_timeout_27(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_uri_28(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_writePending_29(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_writing_30(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_WriteAResult_31(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_ReadAResult_32(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_Aborted_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (FileWebRequestCreator_tC02A1A70722C45B078D759F22AE10256A6900C6D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (FileWebStream_t67DDC539EC81FFB9F3615EBE17649E53E1CCA766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[1] = 
{
	FileWebStream_t67DDC539EC81FFB9F3615EBE17649E53E1CCA766::get_offset_of_m_request_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2760[6] = 
{
	FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325::get_offset_of_m_closed_1(),
	FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325::get_offset_of_m_contentLength_2(),
	FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325::get_offset_of_m_fileAccess_3(),
	FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325::get_offset_of_m_headers_4(),
	FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325::get_offset_of_m_stream_5(),
	FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325::get_offset_of_m_uri_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2762[17] = 
{
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_baseAddress_4(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_credentials_5(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_headers_6(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_requestParameters_7(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_WebResponse_8(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_WebRequest_9(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_Encoding_10(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_Method_11(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_ContentLength_12(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_Cancelled_13(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_Progress_14(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_Proxy_15(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_ProxySet_16(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_CachePolicy_17(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_CallNesting_18(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_reportDownloadProgressChanged_19(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_reportUploadProgressChanged_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2763[5] = 
{
	ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20::get_offset_of_BytesSent_0(),
	ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20::get_offset_of_TotalBytesToSend_1(),
	ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20::get_offset_of_BytesReceived_2(),
	ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20::get_offset_of_TotalBytesToReceive_3(),
	ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20::get_offset_of_HasUploadPhase_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2764[11] = 
{
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_WebClient_0(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_WriteStream_1(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_InnerBuffer_2(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_AsyncOp_3(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_Request_4(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_CompletionDelegate_5(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_ReadStream_6(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_SgBuffers_7(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_ContentLength_8(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_Length_9(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_Progress_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (DownloadProgressChangedEventArgs_tACD8549B8D84C4FBA17C2D19FA87E966CCCD4522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2766[2] = 
{
	DownloadProgressChangedEventArgs_tACD8549B8D84C4FBA17C2D19FA87E966CCCD4522::get_offset_of_m_BytesReceived_3(),
	DownloadProgressChangedEventArgs_tACD8549B8D84C4FBA17C2D19FA87E966CCCD4522::get_offset_of_m_TotalBytesToReceive_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2767[4] = 
{
	UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E::get_offset_of_m_BytesReceived_3(),
	UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E::get_offset_of_m_TotalBytesToReceive_4(),
	UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E::get_offset_of_m_BytesSent_5(),
	UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E::get_offset_of_m_TotalBytesToSend_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2768[9] = 
{
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of__UseRegistry_0(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of__BypassOnLocal_1(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of_m_EnableAutoproxy_2(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of__ProxyAddress_3(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of__BypassList_4(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of__Credentials_5(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of__RegExBypassList_6(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of__ProxyHostAddresses_7(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of_m_ScriptEngine_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (UnsafeNclNativeMethods_t6130A140E270DE50A48DAA13D71A71F5BD9F1537), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (HttpApi_tDD21E6C468ACB472A3D1CE51865417E65113B12F), -1, sizeof(HttpApi_tDD21E6C468ACB472A3D1CE51865417E65113B12F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2771[1] = 
{
	HttpApi_tDD21E6C468ACB472A3D1CE51865417E65113B12F_StaticFields::get_offset_of_m_Strings_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (HTTP_REQUEST_HEADER_ID_tB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE), -1, sizeof(HTTP_REQUEST_HEADER_ID_tB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2772[1] = 
{
	HTTP_REQUEST_HEADER_ID_tB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE_StaticFields::get_offset_of_m_Strings_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (SecureStringHelper_t9F5A5E822AB08545A97B612C217CC6C760FF78F5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (Logging_t16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70), -1, sizeof(Logging_t16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2774[1] = 
{
	Logging_t16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70_StaticFields::get_offset_of_On_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[2] = 
{
	ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB::get_offset_of_m_ValidationCallback_0(),
	ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB::get_offset_of_m_Context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2776[5] = 
{
	CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8::get_offset_of_request_0(),
	CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8::get_offset_of_certificate_1(),
	CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8::get_offset_of_chain_2(),
	CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8::get_offset_of_sslPolicyErrors_3(),
	CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8::get_offset_of_result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012), -1, sizeof(AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2777[3] = 
{
	AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields::get_offset_of_modules_0(),
	AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields::get_offset_of_locker_1(),
	AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields::get_offset_of_credential_policy_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (BasicClient_t691369603F87465F4B5A78CD356545B56ABCA18C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (BindIPEndPoint_t6B179B1AD32AF233C8C8E6440DFEF78153A851B9), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2780[4] = 
{
	ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930::get_offset_of_disposed_10(),
	ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930::get_offset_of_decoder_11(),
	ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930::get_offset_of_context_12(),
	ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930::get_offset_of_no_more_data_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2781[5] = 
{
	ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B::get_offset_of_Buffer_0(),
	ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B::get_offset_of_Offset_1(),
	ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B::get_offset_of_Count_2(),
	ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B::get_offset_of_InitialCount_3(),
	ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B::get_offset_of_Ares_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (DecompressionMethods_t828950DA24A3D2B4A635E51125685CDB629ED51D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2782[4] = 
{
	DecompressionMethods_t828950DA24A3D2B4A635E51125685CDB629ED51D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (DefaultCertificatePolicy_t2B28BF921CE86F4956EF998580E48C314B14A674), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9), -1, sizeof(DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2784[5] = 
{
	DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9::get_offset_of_header_0(),
	DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9::get_offset_of_length_1(),
	DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9::get_offset_of_pos_2(),
	DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9_StaticFields::get_offset_of_keywords_3(),
	DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9::get_offset_of_values_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627), -1, sizeof(DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2785[6] = 
{
	DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627_StaticFields::get_offset_of_rng_0(),
	DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627::get_offset_of_lastUse_1(),
	DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627::get_offset_of__nc_2(),
	DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627::get_offset_of_hash_3(),
	DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627::get_offset_of_parser_4(),
	DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627::get_offset_of__cnonce_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (DigestClient_t2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C), -1, sizeof(DigestClient_t2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2786[1] = 
{
	DigestClient_t2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C_StaticFields::get_offset_of_cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (Dns_t0E6B5B77C654107F106B577875FE899BAF8ADCF9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2788[9] = 
{
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_listener_0(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_endpoint_1(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_sock_2(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_prefixes_3(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_unhandled_4(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_all_5(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_cert_6(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_secure_7(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_unregistered_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (EndPointManager_tEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2), -1, sizeof(EndPointManager_tEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2789[1] = 
{
	EndPointManager_tEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2_StaticFields::get_offset_of_ip_to_endpoints_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2790[9] = 
{
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_response_0(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_waitHandle_1(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_exception_2(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_callback_3(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_stream_4(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_state_5(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_completed_6(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_synch_7(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_locker_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[5] = 
{
	FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1::get_offset_of_request_4(),
	FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1::get_offset_of_networkStream_5(),
	FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1::get_offset_of_disposed_6(),
	FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1::get_offset_of_isRead_7(),
	FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1::get_offset_of_totalRead_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (WriteDelegate_tCA763F3444D2578FB21239EDFC1C3632E469FC49), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (ReadDelegate_tBC77AE628966A21E63D8BB344BC3D7C79441A6DE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (FtpRequestCreator_t2C5CC32221C790FB648AF6276DA861B4ABAC357F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (FtpStatus_tC736CA78D396A33659145A9183F15038E66B2876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2795[2] = 
{
	FtpStatus_tC736CA78D396A33659145A9183F15038E66B2876::get_offset_of_statusCode_0(),
	FtpStatus_tC736CA78D396A33659145A9183F15038E66B2876::get_offset_of_statusDescription_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA), -1, sizeof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2796[29] = 
{
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_requestUri_12(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_file_name_13(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_servicePoint_14(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_origDataStream_15(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_dataStream_16(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_controlStream_17(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_controlReader_18(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_credentials_19(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_hostEntry_20(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_localEndPoint_21(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_remoteEndPoint_22(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_proxy_23(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_timeout_24(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_rwTimeout_25(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_offset_26(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_binary_27(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_enableSsl_28(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_usePassive_29(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_keepAlive_30(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_method_31(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_renameTo_32(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_locker_33(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_requestState_34(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_asyncResult_35(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_ftpResponse_36(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_requestStream_37(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_initial_path_38(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA_StaticFields::get_offset_of_supportedCommands_39(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_dataEncoding_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (RequestState_t850C56F50136642DB235E32D764586B31C248731)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2797[10] = 
{
	RequestState_t850C56F50136642DB235E32D764586B31C248731::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2798[12] = 
{
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_stream_1(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_uri_2(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_statusCode_3(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_lastModified_4(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_bannerMessage_5(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_welcomeMessage_6(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_exitMessage_7(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_statusDescription_8(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_method_9(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_disposed_10(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_request_11(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_contentLength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E), -1, sizeof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2799[26] = 
{
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E_StaticFields::get_offset_of_onread_cb_0(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_sock_1(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_stream_2(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_epl_3(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_ms_4(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_buffer_5(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_context_6(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_current_line_7(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_prefix_8(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_i_stream_9(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_o_stream_10(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_chunked_11(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_reuses_12(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_context_bound_13(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_secure_14(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_cert_15(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_s_timeout_16(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_timer_17(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_local_ep_18(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_last_listener_19(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_client_cert_errors_20(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_client_cert_21(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_ssl_stream_22(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_input_state_23(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_line_state_24(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_position_25(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
